namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PARTY_ROLE_IDENTIFIER
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.PartyRoleIdentifierSpecified
                    || this.PartyRoleIdentifierCertificationStatusCodeSpecified
                    || this.PartyRoleIdentifierCertificationStatusDateSpecified
                    || this.PartyRoleIdentifierDescriptionSpecified
                    || this.PartyRoleIdentifierExpiryDateSpecified
                    || this.PartyRoleIdentifierExpiryReasonCodeSpecified
                    || this.PartyRoleIdentifierExpiryReasonDescriptionSpecified
                    || this.PartyRoleIdentifierFirstAssignmentDateSpecified
                    || this.PartyRoleIdentifierLastUpdateDateSpecified
                    || this.PartyRoleIdentifierOfficialRegistryNameSpecified
                    || this.PartyRoleIdentifierReferenceDescriptionSpecified
                    || this.PartyRoleIdentifierScheduledCertificationStatusDateSpecified
                    || this.PartyRoleIdentifierStatusCodeSpecified
                    || this.PartyRoleIdentifierStatusDateSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PartyRoleIdentifier", Order = 0)]
        public MISMOIdentifier PartyRoleIdentifier { get; set; }
    
        [XmlIgnore]
        public bool PartyRoleIdentifierSpecified
        {
            get { return this.PartyRoleIdentifier != null; }
            set { }
        }
    
        [XmlElement("PartyRoleIdentifierCertificationStatusCode", Order = 1)]
        public MISMOCode PartyRoleIdentifierCertificationStatusCode { get; set; }
    
        [XmlIgnore]
        public bool PartyRoleIdentifierCertificationStatusCodeSpecified
        {
            get { return this.PartyRoleIdentifierCertificationStatusCode != null; }
            set { }
        }
    
        [XmlElement("PartyRoleIdentifierCertificationStatusDate", Order = 2)]
        public MISMODate PartyRoleIdentifierCertificationStatusDate { get; set; }
    
        [XmlIgnore]
        public bool PartyRoleIdentifierCertificationStatusDateSpecified
        {
            get { return this.PartyRoleIdentifierCertificationStatusDate != null; }
            set { }
        }
    
        [XmlElement("PartyRoleIdentifierDescription", Order = 3)]
        public MISMOString PartyRoleIdentifierDescription { get; set; }
    
        [XmlIgnore]
        public bool PartyRoleIdentifierDescriptionSpecified
        {
            get { return this.PartyRoleIdentifierDescription != null; }
            set { }
        }
    
        [XmlElement("PartyRoleIdentifierExpiryDate", Order = 4)]
        public MISMODate PartyRoleIdentifierExpiryDate { get; set; }
    
        [XmlIgnore]
        public bool PartyRoleIdentifierExpiryDateSpecified
        {
            get { return this.PartyRoleIdentifierExpiryDate != null; }
            set { }
        }
    
        [XmlElement("PartyRoleIdentifierExpiryReasonCode", Order = 5)]
        public MISMOCode PartyRoleIdentifierExpiryReasonCode { get; set; }
    
        [XmlIgnore]
        public bool PartyRoleIdentifierExpiryReasonCodeSpecified
        {
            get { return this.PartyRoleIdentifierExpiryReasonCode != null; }
            set { }
        }
    
        [XmlElement("PartyRoleIdentifierExpiryReasonDescription", Order = 6)]
        public MISMOString PartyRoleIdentifierExpiryReasonDescription { get; set; }
    
        [XmlIgnore]
        public bool PartyRoleIdentifierExpiryReasonDescriptionSpecified
        {
            get { return this.PartyRoleIdentifierExpiryReasonDescription != null; }
            set { }
        }
    
        [XmlElement("PartyRoleIdentifierFirstAssignmentDate", Order = 7)]
        public MISMODate PartyRoleIdentifierFirstAssignmentDate { get; set; }
    
        [XmlIgnore]
        public bool PartyRoleIdentifierFirstAssignmentDateSpecified
        {
            get { return this.PartyRoleIdentifierFirstAssignmentDate != null; }
            set { }
        }
    
        [XmlElement("PartyRoleIdentifierLastUpdateDate", Order = 8)]
        public MISMODate PartyRoleIdentifierLastUpdateDate { get; set; }
    
        [XmlIgnore]
        public bool PartyRoleIdentifierLastUpdateDateSpecified
        {
            get { return this.PartyRoleIdentifierLastUpdateDate != null; }
            set { }
        }
    
        [XmlElement("PartyRoleIdentifierOfficialRegistryName", Order = 9)]
        public MISMOString PartyRoleIdentifierOfficialRegistryName { get; set; }
    
        [XmlIgnore]
        public bool PartyRoleIdentifierOfficialRegistryNameSpecified
        {
            get { return this.PartyRoleIdentifierOfficialRegistryName != null; }
            set { }
        }
    
        [XmlElement("PartyRoleIdentifierReferenceDescription", Order = 10)]
        public MISMOString PartyRoleIdentifierReferenceDescription { get; set; }
    
        [XmlIgnore]
        public bool PartyRoleIdentifierReferenceDescriptionSpecified
        {
            get { return this.PartyRoleIdentifierReferenceDescription != null; }
            set { }
        }
    
        [XmlElement("PartyRoleIdentifierScheduledCertificationStatusDate", Order = 11)]
        public MISMODate PartyRoleIdentifierScheduledCertificationStatusDate { get; set; }
    
        [XmlIgnore]
        public bool PartyRoleIdentifierScheduledCertificationStatusDateSpecified
        {
            get { return this.PartyRoleIdentifierScheduledCertificationStatusDate != null; }
            set { }
        }
    
        [XmlElement("PartyRoleIdentifierStatusCode", Order = 12)]
        public MISMOCode PartyRoleIdentifierStatusCode { get; set; }
    
        [XmlIgnore]
        public bool PartyRoleIdentifierStatusCodeSpecified
        {
            get { return this.PartyRoleIdentifierStatusCode != null; }
            set { }
        }
    
        [XmlElement("PartyRoleIdentifierStatusDate", Order = 13)]
        public MISMODate PartyRoleIdentifierStatusDate { get; set; }
    
        [XmlIgnore]
        public bool PartyRoleIdentifierStatusDateSpecified
        {
            get { return this.PartyRoleIdentifierStatusDate != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 14)]
        public PARTY_ROLE_IDENTIFIER_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
