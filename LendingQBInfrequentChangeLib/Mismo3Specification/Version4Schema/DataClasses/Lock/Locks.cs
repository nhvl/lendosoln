namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class LOCKS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.LockListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("LOCK", Order = 0)]
        public List<LOCK> LockList { get; set; } = new List<LOCK>();
    
        [XmlIgnore]
        public bool LockListSpecified
        {
            get { return this.LockList != null && this.LockList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public LOCKS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
