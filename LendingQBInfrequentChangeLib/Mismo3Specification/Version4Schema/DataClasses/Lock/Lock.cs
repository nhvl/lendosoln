namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class LOCK
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.LoanPriceQuoteIdentifierSpecified
                    || this.LoanPriceQuoteTypeSpecified
                    || this.LoanPriceQuoteTypeOtherDescriptionSpecified
                    || this.LockDatetimeSpecified
                    || this.LockDurationDaysCountSpecified
                    || this.LockExpirationDatetimeSpecified
                    || this.LockIdentifierSpecified
                    || this.LockRequestedExtensionDaysCountSpecified
                    || this.LockStatusTypeSpecified
                    || this.LockStatusTypeOtherDescriptionSpecified
                    || this.PriceRequestIdentifierSpecified
                    || this.PriceResponseIdentifierSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("LoanPriceQuoteIdentifier", Order = 0)]
        public MISMOIdentifier LoanPriceQuoteIdentifier { get; set; }
    
        [XmlIgnore]
        public bool LoanPriceQuoteIdentifierSpecified
        {
            get { return this.LoanPriceQuoteIdentifier != null; }
            set { }
        }
    
        [XmlElement("LoanPriceQuoteType", Order = 1)]
        public MISMOEnum<LoanPriceQuoteBase> LoanPriceQuoteType { get; set; }
    
        [XmlIgnore]
        public bool LoanPriceQuoteTypeSpecified
        {
            get { return this.LoanPriceQuoteType != null; }
            set { }
        }
    
        [XmlElement("LoanPriceQuoteTypeOtherDescription", Order = 2)]
        public MISMOString LoanPriceQuoteTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool LoanPriceQuoteTypeOtherDescriptionSpecified
        {
            get { return this.LoanPriceQuoteTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("LockDatetime", Order = 3)]
        public MISMODatetime LockDatetime { get; set; }
    
        [XmlIgnore]
        public bool LockDatetimeSpecified
        {
            get { return this.LockDatetime != null; }
            set { }
        }
    
        [XmlElement("LockDurationDaysCount", Order = 4)]
        public MISMOCount LockDurationDaysCount { get; set; }
    
        [XmlIgnore]
        public bool LockDurationDaysCountSpecified
        {
            get { return this.LockDurationDaysCount != null; }
            set { }
        }
    
        [XmlElement("LockExpirationDatetime", Order = 5)]
        public MISMODatetime LockExpirationDatetime { get; set; }
    
        [XmlIgnore]
        public bool LockExpirationDatetimeSpecified
        {
            get { return this.LockExpirationDatetime != null; }
            set { }
        }
    
        [XmlElement("LockIdentifier", Order = 6)]
        public MISMOIdentifier LockIdentifier { get; set; }
    
        [XmlIgnore]
        public bool LockIdentifierSpecified
        {
            get { return this.LockIdentifier != null; }
            set { }
        }
    
        [XmlElement("LockRequestedExtensionDaysCount", Order = 7)]
        public MISMOCount LockRequestedExtensionDaysCount { get; set; }
    
        [XmlIgnore]
        public bool LockRequestedExtensionDaysCountSpecified
        {
            get { return this.LockRequestedExtensionDaysCount != null; }
            set { }
        }
    
        [XmlElement("LockStatusType", Order = 8)]
        public MISMOEnum<LockStatusBase> LockStatusType { get; set; }
    
        [XmlIgnore]
        public bool LockStatusTypeSpecified
        {
            get { return this.LockStatusType != null; }
            set { }
        }
    
        [XmlElement("LockStatusTypeOtherDescription", Order = 9)]
        public MISMOString LockStatusTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool LockStatusTypeOtherDescriptionSpecified
        {
            get { return this.LockStatusTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("PriceRequestIdentifier", Order = 10)]
        public MISMOIdentifier PriceRequestIdentifier { get; set; }
    
        [XmlIgnore]
        public bool PriceRequestIdentifierSpecified
        {
            get { return this.PriceRequestIdentifier != null; }
            set { }
        }
    
        [XmlElement("PriceResponseIdentifier", Order = 11)]
        public MISMOIdentifier PriceResponseIdentifier { get; set; }
    
        [XmlIgnore]
        public bool PriceResponseIdentifierSpecified
        {
            get { return this.PriceResponseIdentifier != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 12)]
        public LOCK_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
