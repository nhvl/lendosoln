namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class INDEX_RULES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.IndexRuleListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("INDEX_RULE", Order = 0)]
        public List<INDEX_RULE> IndexRuleList { get; set; } = new List<INDEX_RULE>();
    
        [XmlIgnore]
        public bool IndexRuleListSpecified
        {
            get { return this.IndexRuleList != null && this.IndexRuleList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public INDEX_RULES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
