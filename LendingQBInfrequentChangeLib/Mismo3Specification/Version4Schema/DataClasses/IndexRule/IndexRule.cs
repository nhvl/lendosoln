namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class INDEX_RULE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.IndexAveragingIndicatorSpecified
                    || this.IndexAveragingTypeSpecified
                    || this.IndexAveragingValueCountSpecified
                    || this.IndexCalculationMethodTypeSpecified
                    || this.IndexDesignationTypeSpecified
                    || this.IndexLeadMonthsCountSpecified
                    || this.IndexLookbackTypeSpecified
                    || this.IndexLookbackTypeOtherDescriptionSpecified
                    || this.IndexRoundingPercentSpecified
                    || this.IndexRoundingTimingTypeSpecified
                    || this.IndexRoundingTypeSpecified
                    || this.IndexSourceTypeSpecified
                    || this.IndexSourceTypeOtherDescriptionSpecified
                    || this.IndexTypeSpecified
                    || this.IndexTypeOtherDescriptionSpecified
                    || this.InterestAdjustmentIndexLeadDaysCountSpecified
                    || this.InterestAndPaymentAdjustmentIndexLeadDaysCountSpecified
                    || this.MinimumIndexMovementRatePercentSpecified
                    || this.PaymentsBetweenIndexValuesCountSpecified
                    || this.PaymentsToFirstIndexValueCountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("IndexAveragingIndicator", Order = 0)]
        public MISMOIndicator IndexAveragingIndicator { get; set; }
    
        [XmlIgnore]
        public bool IndexAveragingIndicatorSpecified
        {
            get { return this.IndexAveragingIndicator != null; }
            set { }
        }
    
        [XmlElement("IndexAveragingType", Order = 1)]
        public MISMOEnum<IndexAveragingBase> IndexAveragingType { get; set; }
    
        [XmlIgnore]
        public bool IndexAveragingTypeSpecified
        {
            get { return this.IndexAveragingType != null; }
            set { }
        }
    
        [XmlElement("IndexAveragingValueCount", Order = 2)]
        public MISMOCount IndexAveragingValueCount { get; set; }
    
        [XmlIgnore]
        public bool IndexAveragingValueCountSpecified
        {
            get { return this.IndexAveragingValueCount != null; }
            set { }
        }
    
        [XmlElement("IndexCalculationMethodType", Order = 3)]
        public MISMOEnum<IndexCalculationMethodBase> IndexCalculationMethodType { get; set; }
    
        [XmlIgnore]
        public bool IndexCalculationMethodTypeSpecified
        {
            get { return this.IndexCalculationMethodType != null; }
            set { }
        }
    
        [XmlElement("IndexDesignationType", Order = 4)]
        public MISMOEnum<IndexDesignationBase> IndexDesignationType { get; set; }
    
        [XmlIgnore]
        public bool IndexDesignationTypeSpecified
        {
            get { return this.IndexDesignationType != null; }
            set { }
        }
    
        [XmlElement("IndexLeadMonthsCount", Order = 5)]
        public MISMOCount IndexLeadMonthsCount { get; set; }
    
        [XmlIgnore]
        public bool IndexLeadMonthsCountSpecified
        {
            get { return this.IndexLeadMonthsCount != null; }
            set { }
        }
    
        [XmlElement("IndexLookbackType", Order = 6)]
        public MISMOEnum<IndexLookbackBase> IndexLookbackType { get; set; }
    
        [XmlIgnore]
        public bool IndexLookbackTypeSpecified
        {
            get { return this.IndexLookbackType != null; }
            set { }
        }
    
        [XmlElement("IndexLookbackTypeOtherDescription", Order = 7)]
        public MISMOString IndexLookbackTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool IndexLookbackTypeOtherDescriptionSpecified
        {
            get { return this.IndexLookbackTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("IndexRoundingPercent", Order = 8)]
        public MISMOPercent IndexRoundingPercent { get; set; }
    
        [XmlIgnore]
        public bool IndexRoundingPercentSpecified
        {
            get { return this.IndexRoundingPercent != null; }
            set { }
        }
    
        [XmlElement("IndexRoundingTimingType", Order = 9)]
        public MISMOEnum<IndexRoundingTimingBase> IndexRoundingTimingType { get; set; }
    
        [XmlIgnore]
        public bool IndexRoundingTimingTypeSpecified
        {
            get { return this.IndexRoundingTimingType != null; }
            set { }
        }
    
        [XmlElement("IndexRoundingType", Order = 10)]
        public MISMOEnum<IndexRoundingBase> IndexRoundingType { get; set; }
    
        [XmlIgnore]
        public bool IndexRoundingTypeSpecified
        {
            get { return this.IndexRoundingType != null; }
            set { }
        }
    
        [XmlElement("IndexSourceType", Order = 11)]
        public MISMOEnum<IndexSourceBase> IndexSourceType { get; set; }
    
        [XmlIgnore]
        public bool IndexSourceTypeSpecified
        {
            get { return this.IndexSourceType != null; }
            set { }
        }
    
        [XmlElement("IndexSourceTypeOtherDescription", Order = 12)]
        public MISMOString IndexSourceTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool IndexSourceTypeOtherDescriptionSpecified
        {
            get { return this.IndexSourceTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("IndexType", Order = 13)]
        public MISMOEnum<IndexBase> IndexType { get; set; }
    
        [XmlIgnore]
        public bool IndexTypeSpecified
        {
            get { return this.IndexType != null; }
            set { }
        }
    
        [XmlElement("IndexTypeOtherDescription", Order = 14)]
        public MISMOString IndexTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool IndexTypeOtherDescriptionSpecified
        {
            get { return this.IndexTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("InterestAdjustmentIndexLeadDaysCount", Order = 15)]
        public MISMOCount InterestAdjustmentIndexLeadDaysCount { get; set; }
    
        [XmlIgnore]
        public bool InterestAdjustmentIndexLeadDaysCountSpecified
        {
            get { return this.InterestAdjustmentIndexLeadDaysCount != null; }
            set { }
        }
    
        [XmlElement("InterestAndPaymentAdjustmentIndexLeadDaysCount", Order = 16)]
        public MISMOCount InterestAndPaymentAdjustmentIndexLeadDaysCount { get; set; }
    
        [XmlIgnore]
        public bool InterestAndPaymentAdjustmentIndexLeadDaysCountSpecified
        {
            get { return this.InterestAndPaymentAdjustmentIndexLeadDaysCount != null; }
            set { }
        }
    
        [XmlElement("MinimumIndexMovementRatePercent", Order = 17)]
        public MISMOPercent MinimumIndexMovementRatePercent { get; set; }
    
        [XmlIgnore]
        public bool MinimumIndexMovementRatePercentSpecified
        {
            get { return this.MinimumIndexMovementRatePercent != null; }
            set { }
        }
    
        [XmlElement("PaymentsBetweenIndexValuesCount", Order = 18)]
        public MISMOCount PaymentsBetweenIndexValuesCount { get; set; }
    
        [XmlIgnore]
        public bool PaymentsBetweenIndexValuesCountSpecified
        {
            get { return this.PaymentsBetweenIndexValuesCount != null; }
            set { }
        }
    
        [XmlElement("PaymentsToFirstIndexValueCount", Order = 19)]
        public MISMOCount PaymentsToFirstIndexValueCount { get; set; }
    
        [XmlIgnore]
        public bool PaymentsToFirstIndexValueCountSpecified
        {
            get { return this.PaymentsToFirstIndexValueCount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 20)]
        public INDEX_RULE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
