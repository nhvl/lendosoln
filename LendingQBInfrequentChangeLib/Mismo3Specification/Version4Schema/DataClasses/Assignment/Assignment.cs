namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class ASSIGNMENT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AssignmentDateSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AssignmentDate", Order = 0)]
        public MISMODate AssignmentDate { get; set; }
    
        [XmlIgnore]
        public bool AssignmentDateSpecified
        {
            get { return this.AssignmentDate != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public ASSIGNMENT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
