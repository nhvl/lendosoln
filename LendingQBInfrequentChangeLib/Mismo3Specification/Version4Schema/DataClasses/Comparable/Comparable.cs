namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class COMPARABLE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ComparableAdjustmentsSpecified
                    || this.ComparableDetailSpecified
                    || this.ComparableSummarySpecified
                    || this.ResearchSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("COMPARABLE_ADJUSTMENTS", Order = 0)]
        public COMPARABLE_ADJUSTMENTS ComparableAdjustments { get; set; }
    
        [XmlIgnore]
        public bool ComparableAdjustmentsSpecified
        {
            get { return this.ComparableAdjustments != null && this.ComparableAdjustments.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("COMPARABLE_DETAIL", Order = 1)]
        public COMPARABLE_DETAIL ComparableDetail { get; set; }
    
        [XmlIgnore]
        public bool ComparableDetailSpecified
        {
            get { return this.ComparableDetail != null && this.ComparableDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("COMPARABLE_SUMMARY", Order = 2)]
        public COMPARABLE_SUMMARY ComparableSummary { get; set; }
    
        [XmlIgnore]
        public bool ComparableSummarySpecified
        {
            get { return this.ComparableSummary != null && this.ComparableSummary.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("RESEARCH", Order = 3)]
        public RESEARCH Research { get; set; }
    
        [XmlIgnore]
        public bool ResearchSpecified
        {
            get { return this.Research != null && this.Research.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public COMPARABLE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
