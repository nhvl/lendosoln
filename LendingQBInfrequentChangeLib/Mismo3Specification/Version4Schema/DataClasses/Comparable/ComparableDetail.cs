namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class COMPARABLE_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AdjustedSalesPriceAmountSpecified
                    || this.AdjustedSalesPricePerBedroomAmountSpecified
                    || this.AdjustedSalesPricePerRoomAmountSpecified
                    || this.AdjustedSalesPricePerUnitAmountSpecified
                    || this.ClosedSaleCommentDescriptionSpecified
                    || this.ComparableIdentifierSpecified
                    || this.ComparableListingCommentDescriptionSpecified
                    || this.ComparablePropertyCommentDescriptionSpecified
                    || this.ComparableSaleFurnishedIndicatorSpecified
                    || this.ComparableTypeSpecified
                    || this.ComparableTypeOtherDescriptionSpecified
                    || this.CompetitiveListingsCommentDescriptionSpecified
                    || this.ContractDateUnknownIndicatorSpecified
                    || this.GrossMonthlyRentMultiplierPercentSpecified
                    || this.MLSNumberIdentifierSpecified
                    || this.MonthlyRentAmountSpecified
                    || this.MortgageTypeSpecified
                    || this.MortgageTypeOtherDescriptionSpecified
                    || this.NoFinancingTransactionIndicatorSpecified
                    || this.PricePerSquareFootAmountSpecified
                    || this.ProximityToSubjectDescriptionSpecified
                    || this.RentControlStatusTypeSpecified
                    || this.SalePriceTotalAdjustmentAmountSpecified
                    || this.SalePriceTotalAdjustmentNetPercentSpecified
                    || this.SalesPricePerBedroomAmountSpecified
                    || this.SalesPricePerGrossBuildingAreaAmountSpecified
                    || this.SalesPricePerGrossLivingAreaAmountSpecified
                    || this.SalesPricePerOwnershipShareAmountSpecified
                    || this.SalesPricePerRoomAmountSpecified
                    || this.SalesPricePerUnitAmountSpecified
                    || this.SalesPriceToListPriceRatePercentSpecified
                    || this.SalesPriceTotalAdjustmentGrossPercentSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AdjustedSalesPriceAmount", Order = 0)]
        public MISMOAmount AdjustedSalesPriceAmount { get; set; }
    
        [XmlIgnore]
        public bool AdjustedSalesPriceAmountSpecified
        {
            get { return this.AdjustedSalesPriceAmount != null; }
            set { }
        }
    
        [XmlElement("AdjustedSalesPricePerBedroomAmount", Order = 1)]
        public MISMOAmount AdjustedSalesPricePerBedroomAmount { get; set; }
    
        [XmlIgnore]
        public bool AdjustedSalesPricePerBedroomAmountSpecified
        {
            get { return this.AdjustedSalesPricePerBedroomAmount != null; }
            set { }
        }
    
        [XmlElement("AdjustedSalesPricePerRoomAmount", Order = 2)]
        public MISMOAmount AdjustedSalesPricePerRoomAmount { get; set; }
    
        [XmlIgnore]
        public bool AdjustedSalesPricePerRoomAmountSpecified
        {
            get { return this.AdjustedSalesPricePerRoomAmount != null; }
            set { }
        }
    
        [XmlElement("AdjustedSalesPricePerUnitAmount", Order = 3)]
        public MISMOAmount AdjustedSalesPricePerUnitAmount { get; set; }
    
        [XmlIgnore]
        public bool AdjustedSalesPricePerUnitAmountSpecified
        {
            get { return this.AdjustedSalesPricePerUnitAmount != null; }
            set { }
        }
    
        [XmlElement("ClosedSaleCommentDescription", Order = 4)]
        public MISMOString ClosedSaleCommentDescription { get; set; }
    
        [XmlIgnore]
        public bool ClosedSaleCommentDescriptionSpecified
        {
            get { return this.ClosedSaleCommentDescription != null; }
            set { }
        }
    
        [XmlElement("ComparableIdentifier", Order = 5)]
        public MISMOIdentifier ComparableIdentifier { get; set; }
    
        [XmlIgnore]
        public bool ComparableIdentifierSpecified
        {
            get { return this.ComparableIdentifier != null; }
            set { }
        }
    
        [XmlElement("ComparableListingCommentDescription", Order = 6)]
        public MISMOString ComparableListingCommentDescription { get; set; }
    
        [XmlIgnore]
        public bool ComparableListingCommentDescriptionSpecified
        {
            get { return this.ComparableListingCommentDescription != null; }
            set { }
        }
    
        [XmlElement("ComparablePropertyCommentDescription", Order = 7)]
        public MISMOString ComparablePropertyCommentDescription { get; set; }
    
        [XmlIgnore]
        public bool ComparablePropertyCommentDescriptionSpecified
        {
            get { return this.ComparablePropertyCommentDescription != null; }
            set { }
        }
    
        [XmlElement("ComparableSaleFurnishedIndicator", Order = 8)]
        public MISMOIndicator ComparableSaleFurnishedIndicator { get; set; }
    
        [XmlIgnore]
        public bool ComparableSaleFurnishedIndicatorSpecified
        {
            get { return this.ComparableSaleFurnishedIndicator != null; }
            set { }
        }
    
        [XmlElement("ComparableType", Order = 9)]
        public MISMOEnum<ComparableBase> ComparableType { get; set; }
    
        [XmlIgnore]
        public bool ComparableTypeSpecified
        {
            get { return this.ComparableType != null; }
            set { }
        }
    
        [XmlElement("ComparableTypeOtherDescription", Order = 10)]
        public MISMOString ComparableTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ComparableTypeOtherDescriptionSpecified
        {
            get { return this.ComparableTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("CompetitiveListingsCommentDescription", Order = 11)]
        public MISMOString CompetitiveListingsCommentDescription { get; set; }
    
        [XmlIgnore]
        public bool CompetitiveListingsCommentDescriptionSpecified
        {
            get { return this.CompetitiveListingsCommentDescription != null; }
            set { }
        }
    
        [XmlElement("ContractDateUnknownIndicator", Order = 12)]
        public MISMOIndicator ContractDateUnknownIndicator { get; set; }
    
        [XmlIgnore]
        public bool ContractDateUnknownIndicatorSpecified
        {
            get { return this.ContractDateUnknownIndicator != null; }
            set { }
        }
    
        [XmlElement("GrossMonthlyRentMultiplierPercent", Order = 13)]
        public MISMOPercent GrossMonthlyRentMultiplierPercent { get; set; }
    
        [XmlIgnore]
        public bool GrossMonthlyRentMultiplierPercentSpecified
        {
            get { return this.GrossMonthlyRentMultiplierPercent != null; }
            set { }
        }
    
        [XmlElement("MLSNumberIdentifier", Order = 14)]
        public MISMOIdentifier MLSNumberIdentifier { get; set; }
    
        [XmlIgnore]
        public bool MLSNumberIdentifierSpecified
        {
            get { return this.MLSNumberIdentifier != null; }
            set { }
        }
    
        [XmlElement("MonthlyRentAmount", Order = 15)]
        public MISMOAmount MonthlyRentAmount { get; set; }
    
        [XmlIgnore]
        public bool MonthlyRentAmountSpecified
        {
            get { return this.MonthlyRentAmount != null; }
            set { }
        }
    
        [XmlElement("MortgageType", Order = 16)]
        public MISMOEnum<MortgageBase> MortgageType { get; set; }
    
        [XmlIgnore]
        public bool MortgageTypeSpecified
        {
            get { return this.MortgageType != null; }
            set { }
        }
    
        [XmlElement("MortgageTypeOtherDescription", Order = 17)]
        public MISMOString MortgageTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool MortgageTypeOtherDescriptionSpecified
        {
            get { return this.MortgageTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("NoFinancingTransactionIndicator", Order = 18)]
        public MISMOIndicator NoFinancingTransactionIndicator { get; set; }
    
        [XmlIgnore]
        public bool NoFinancingTransactionIndicatorSpecified
        {
            get { return this.NoFinancingTransactionIndicator != null; }
            set { }
        }
    
        [XmlElement("PricePerSquareFootAmount", Order = 19)]
        public MISMOAmount PricePerSquareFootAmount { get; set; }
    
        [XmlIgnore]
        public bool PricePerSquareFootAmountSpecified
        {
            get { return this.PricePerSquareFootAmount != null; }
            set { }
        }
    
        [XmlElement("ProximityToSubjectDescription", Order = 20)]
        public MISMOString ProximityToSubjectDescription { get; set; }
    
        [XmlIgnore]
        public bool ProximityToSubjectDescriptionSpecified
        {
            get { return this.ProximityToSubjectDescription != null; }
            set { }
        }
    
        [XmlElement("RentControlStatusType", Order = 21)]
        public MISMOEnum<RentControlStatusBase> RentControlStatusType { get; set; }
    
        [XmlIgnore]
        public bool RentControlStatusTypeSpecified
        {
            get { return this.RentControlStatusType != null; }
            set { }
        }
    
        [XmlElement("SalePriceTotalAdjustmentAmount", Order = 22)]
        public MISMOAmount SalePriceTotalAdjustmentAmount { get; set; }
    
        [XmlIgnore]
        public bool SalePriceTotalAdjustmentAmountSpecified
        {
            get { return this.SalePriceTotalAdjustmentAmount != null; }
            set { }
        }
    
        [XmlElement("SalePriceTotalAdjustmentNetPercent", Order = 23)]
        public MISMOPercent SalePriceTotalAdjustmentNetPercent { get; set; }
    
        [XmlIgnore]
        public bool SalePriceTotalAdjustmentNetPercentSpecified
        {
            get { return this.SalePriceTotalAdjustmentNetPercent != null; }
            set { }
        }
    
        [XmlElement("SalesPricePerBedroomAmount", Order = 24)]
        public MISMOAmount SalesPricePerBedroomAmount { get; set; }
    
        [XmlIgnore]
        public bool SalesPricePerBedroomAmountSpecified
        {
            get { return this.SalesPricePerBedroomAmount != null; }
            set { }
        }
    
        [XmlElement("SalesPricePerGrossBuildingAreaAmount", Order = 25)]
        public MISMOAmount SalesPricePerGrossBuildingAreaAmount { get; set; }
    
        [XmlIgnore]
        public bool SalesPricePerGrossBuildingAreaAmountSpecified
        {
            get { return this.SalesPricePerGrossBuildingAreaAmount != null; }
            set { }
        }
    
        [XmlElement("SalesPricePerGrossLivingAreaAmount", Order = 26)]
        public MISMOAmount SalesPricePerGrossLivingAreaAmount { get; set; }
    
        [XmlIgnore]
        public bool SalesPricePerGrossLivingAreaAmountSpecified
        {
            get { return this.SalesPricePerGrossLivingAreaAmount != null; }
            set { }
        }
    
        [XmlElement("SalesPricePerOwnershipShareAmount", Order = 27)]
        public MISMOAmount SalesPricePerOwnershipShareAmount { get; set; }
    
        [XmlIgnore]
        public bool SalesPricePerOwnershipShareAmountSpecified
        {
            get { return this.SalesPricePerOwnershipShareAmount != null; }
            set { }
        }
    
        [XmlElement("SalesPricePerRoomAmount", Order = 28)]
        public MISMOAmount SalesPricePerRoomAmount { get; set; }
    
        [XmlIgnore]
        public bool SalesPricePerRoomAmountSpecified
        {
            get { return this.SalesPricePerRoomAmount != null; }
            set { }
        }
    
        [XmlElement("SalesPricePerUnitAmount", Order = 29)]
        public MISMOAmount SalesPricePerUnitAmount { get; set; }
    
        [XmlIgnore]
        public bool SalesPricePerUnitAmountSpecified
        {
            get { return this.SalesPricePerUnitAmount != null; }
            set { }
        }
    
        [XmlElement("SalesPriceToListPriceRatePercent", Order = 30)]
        public MISMOPercent SalesPriceToListPriceRatePercent { get; set; }
    
        [XmlIgnore]
        public bool SalesPriceToListPriceRatePercentSpecified
        {
            get { return this.SalesPriceToListPriceRatePercent != null; }
            set { }
        }
    
        [XmlElement("SalesPriceTotalAdjustmentGrossPercent", Order = 31)]
        public MISMOPercent SalesPriceTotalAdjustmentGrossPercent { get; set; }
    
        [XmlIgnore]
        public bool SalesPriceTotalAdjustmentGrossPercentSpecified
        {
            get { return this.SalesPriceTotalAdjustmentGrossPercent != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 32)]
        public COMPARABLE_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
