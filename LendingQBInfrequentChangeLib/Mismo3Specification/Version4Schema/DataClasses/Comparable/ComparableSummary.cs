namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class COMPARABLE_SUMMARY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.SalesComparisonCurrentSalesAgreementAnalysisCommentDescriptionSpecified
                    || this.SalesComparisonTotalBedroomValueAmountSpecified
                    || this.SalesComparisonTotalGrossBuildingAreaValueAmountSpecified
                    || this.SalesComparisonTotalRoomValueAmountSpecified
                    || this.SalesComparisonTotalUnitValueAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("SalesComparisonCurrentSalesAgreementAnalysisCommentDescription", Order = 0)]
        public MISMOString SalesComparisonCurrentSalesAgreementAnalysisCommentDescription { get; set; }
    
        [XmlIgnore]
        public bool SalesComparisonCurrentSalesAgreementAnalysisCommentDescriptionSpecified
        {
            get { return this.SalesComparisonCurrentSalesAgreementAnalysisCommentDescription != null; }
            set { }
        }
    
        [XmlElement("SalesComparisonTotalBedroomValueAmount", Order = 1)]
        public MISMOAmount SalesComparisonTotalBedroomValueAmount { get; set; }
    
        [XmlIgnore]
        public bool SalesComparisonTotalBedroomValueAmountSpecified
        {
            get { return this.SalesComparisonTotalBedroomValueAmount != null; }
            set { }
        }
    
        [XmlElement("SalesComparisonTotalGrossBuildingAreaValueAmount", Order = 2)]
        public MISMOAmount SalesComparisonTotalGrossBuildingAreaValueAmount { get; set; }
    
        [XmlIgnore]
        public bool SalesComparisonTotalGrossBuildingAreaValueAmountSpecified
        {
            get { return this.SalesComparisonTotalGrossBuildingAreaValueAmount != null; }
            set { }
        }
    
        [XmlElement("SalesComparisonTotalRoomValueAmount", Order = 3)]
        public MISMOAmount SalesComparisonTotalRoomValueAmount { get; set; }
    
        [XmlIgnore]
        public bool SalesComparisonTotalRoomValueAmountSpecified
        {
            get { return this.SalesComparisonTotalRoomValueAmount != null; }
            set { }
        }
    
        [XmlElement("SalesComparisonTotalUnitValueAmount", Order = 4)]
        public MISMOAmount SalesComparisonTotalUnitValueAmount { get; set; }
    
        [XmlIgnore]
        public bool SalesComparisonTotalUnitValueAmountSpecified
        {
            get { return this.SalesComparisonTotalUnitValueAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 5)]
        public COMPARABLE_SUMMARY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
