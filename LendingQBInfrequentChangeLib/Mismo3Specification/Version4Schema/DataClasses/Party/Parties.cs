namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class PARTIES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.PartyListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PARTY", Order = 0)]
        public List<PARTY> PartyList { get; set; } = new List<PARTY>();
    
        [XmlIgnore]
        public bool PartyListSpecified
        {
            get { return this.PartyList != null && this.PartyList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public PARTIES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
