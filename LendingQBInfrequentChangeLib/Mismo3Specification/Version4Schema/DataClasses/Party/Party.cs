namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Xml.Schema;
    using System.Xml.Serialization;

    public class PARTY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                if (Mismo3Utilities.ExceedsThreshold(1,
                    new List<bool> {
                        this.ReferenceSpecified,
                        this.AddressesSpecified
                            || this.ExtensionSpecified
                            || this.IndividualSpecified
                            || this.LanguagesSpecified
                            || this.LegalEntitySpecified
                            || this.RolesSpecified
                            || this.TaxpayerIdentifiersSpecified }))
                {
                    throw new System.Xml.Schema.XmlSchemaValidationException(
                        Mismo3Utilities.GetXSDChoiceExceptionMessage(
                        "PARTY",
                        new List<string> { "REFERENCE", "Any other child element of the PARTY" }));
                }

                if (Mismo3Utilities.ExceedsThreshold(1,
                    new List<bool> { this.IndividualSpecified, this.LegalEntitySpecified }))
                {
                    throw new System.Xml.Schema.XmlSchemaValidationException(
                        Mismo3Utilities.GetXSDChoiceExceptionMessage(
                        "PARTY",
                        new List<string> { "INDIVIDUAL", "LEGAL_ENTITY" }));
                }

                return this.AddressesSpecified
                    || this.ExtensionSpecified
                    || this.IndividualSpecified
                    || this.LanguagesSpecified
                    || this.LegalEntitySpecified
                    || this.ReferenceSpecified
                    || this.RolesSpecified
                    || this.TaxpayerIdentifiersSpecified;
            }
        }

        [XmlElement("REFERENCE", Order = 0)]
        public REFERENCE Reference { get; set; }

        [XmlIgnore]
        public bool ReferenceSpecified
        {
            get { return this.Reference != null && this.Reference.ShouldSerialize; }
            set { }
        }

        [XmlElement("INDIVIDUAL", Order = 1)]
        public INDIVIDUAL Individual { get; set; }

        [XmlIgnore]
        public bool IndividualSpecified
        {
            get { return this.Individual != null && this.Individual.ShouldSerialize; }
            set { }
        }

        [XmlElement("LEGAL_ENTITY", Order = 2)]
        public LEGAL_ENTITY LegalEntity { get; set; }

        [XmlIgnore]
        public bool LegalEntitySpecified
        {
            get { return this.LegalEntity != null && this.LegalEntity.ShouldSerialize; }
            set { }
        }

        [XmlElement("ADDRESSES", Order = 3)]
        public ADDRESSES Addresses { get; set; }
    
        [XmlIgnore]
        public bool AddressesSpecified
        {
            get { return this.Addresses != null && this.Addresses.ShouldSerialize; }
            set { }
        }

        [XmlElement("LANGUAGES", Order = 4)]
        public LANGUAGES Languages { get; set; }

        [XmlIgnore]
        public bool LanguagesSpecified
        {
            get { return this.Languages != null && this.Languages.ShouldSerialize; }
            set { }
        }

        [XmlElement("ROLES", Order = 5)]
        public ROLES Roles { get; set; }

        [XmlIgnore]
        public bool RolesSpecified
        {
            get { return this.Roles != null && this.Roles.ShouldSerialize; }
            set { }
        }

        [XmlElement("TAXPAYER_IDENTIFIERS", Order = 6)]
        public TAXPAYER_IDENTIFIERS TaxpayerIdentifiers { get; set; }

        [XmlIgnore]
        public bool TaxpayerIdentifiersSpecified
        {
            get { return this.TaxpayerIdentifiers != null && this.TaxpayerIdentifiers.ShouldSerialize; }
            set { }
        }

        [XmlElement("EXTENSION", Order = 7)]
        public PARTY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }

        [XmlAttribute("label", Form = XmlSchemaForm.Qualified, Namespace = Mismo3Constants.XlinkNamespace)]
        public string XlinkLabel { get; set; }

        [XmlIgnore]
        public bool XlinkLabelSpecified
        {
            get { return !string.IsNullOrEmpty(this.XlinkLabel); }
            set { }
        }
    }
}
