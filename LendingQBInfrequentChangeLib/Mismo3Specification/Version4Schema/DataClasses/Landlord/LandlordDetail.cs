namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class LANDLORD_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.MonthlyRentAmountSpecified
                    || this.MonthlyRentCurrentRatingTypeSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("MonthlyRentAmount", Order = 0)]
        public MISMOAmount MonthlyRentAmount { get; set; }
    
        [XmlIgnore]
        public bool MonthlyRentAmountSpecified
        {
            get { return this.MonthlyRentAmount != null; }
            set { }
        }
    
        [XmlElement("MonthlyRentCurrentRatingType", Order = 1)]
        public MISMOEnum<MonthlyRentCurrentRatingBase> MonthlyRentCurrentRatingType { get; set; }
    
        [XmlIgnore]
        public bool MonthlyRentCurrentRatingTypeSpecified
        {
            get { return this.MonthlyRentCurrentRatingType != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public LANDLORD_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
