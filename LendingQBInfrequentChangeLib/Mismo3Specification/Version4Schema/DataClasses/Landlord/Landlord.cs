namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class LANDLORD
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AddressSpecified
                    || this.ContactsSpecified
                    || this.CreditCommentsSpecified
                    || this.LandlordDetailSpecified
                    || this.NameSpecified
                    || this.VerificationSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ADDRESS", Order = 0)]
        public ADDRESS Address { get; set; }
    
        [XmlIgnore]
        public bool AddressSpecified
        {
            get { return this.Address != null && this.Address.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CONTACTS", Order = 1)]
        public CONTACTS Contacts { get; set; }
    
        [XmlIgnore]
        public bool ContactsSpecified
        {
            get { return this.Contacts != null && this.Contacts.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CREDIT_COMMENTS", Order = 2)]
        public CREDIT_COMMENTS CreditComments { get; set; }
    
        [XmlIgnore]
        public bool CreditCommentsSpecified
        {
            get { return this.CreditComments != null && this.CreditComments.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("LANDLORD_DETAIL", Order = 3)]
        public LANDLORD_DETAIL LandlordDetail { get; set; }
    
        [XmlIgnore]
        public bool LandlordDetailSpecified
        {
            get { return this.LandlordDetail != null && this.LandlordDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("NAME", Order = 4)]
        public NAME Name { get; set; }
    
        [XmlIgnore]
        public bool NameSpecified
        {
            get { return this.Name != null && this.Name.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("VERIFICATION", Order = 5)]
        public VERIFICATION Verification { get; set; }
    
        [XmlIgnore]
        public bool VerificationSpecified
        {
            get { return this.Verification != null && this.Verification.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 6)]
        public LANDLORD_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
