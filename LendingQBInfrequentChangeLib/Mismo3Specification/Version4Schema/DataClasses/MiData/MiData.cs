namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class MI_DATA
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.MiDataDetailSpecified
                    || this.MI_PAID_TOSpecified
                    || this.MiPoolInsuranceSpecified
                    || this.MiPremiumTaxesSpecified
                    || this.MiPremiumsSpecified
                    || this.SelectedServiceProviderSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("MI_DATA_DETAIL", Order = 0)]
        public MI_DATA_DETAIL MiDataDetail { get; set; }
    
        [XmlIgnore]
        public bool MiDataDetailSpecified
        {
            get { return this.MiDataDetail != null && this.MiDataDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("MI_PAID_TO", Order = 1)]
        public PAID_TO MI_PAID_TO { get; set; }
    
        [XmlIgnore]
        public bool MI_PAID_TOSpecified
        {
            get { return this.MI_PAID_TO != null; }
            set { }
        }
    
        [XmlElement("MI_POOL_INSURANCE", Order = 2)]
        public MI_POOL_INSURANCE MiPoolInsurance { get; set; }
    
        [XmlIgnore]
        public bool MiPoolInsuranceSpecified
        {
            get { return this.MiPoolInsurance != null && this.MiPoolInsurance.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("MI_PREMIUM_TAXES", Order = 3)]
        public MI_PREMIUM_TAXES MiPremiumTaxes { get; set; }
    
        [XmlIgnore]
        public bool MiPremiumTaxesSpecified
        {
            get { return this.MiPremiumTaxes != null && this.MiPremiumTaxes.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("MI_PREMIUMS", Order = 4)]
        public MI_PREMIUMS MiPremiums { get; set; }
    
        [XmlIgnore]
        public bool MiPremiumsSpecified
        {
            get { return this.MiPremiums != null && this.MiPremiums.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SELECTED_SERVICE_PROVIDER", Order = 5)]
        public SELECTED_SERVICE_PROVIDER SelectedServiceProvider { get; set; }
    
        [XmlIgnore]
        public bool SelectedServiceProviderSpecified
        {
            get { return this.SelectedServiceProvider != null && this.SelectedServiceProvider.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 6)]
        public MI_DATA_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
