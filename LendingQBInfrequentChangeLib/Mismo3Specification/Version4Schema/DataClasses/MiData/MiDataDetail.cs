namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class MI_DATA_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BorrowerMITerminationDateSpecified
                    || this.FeePaidToTypeSpecified
                    || this.FeePaidToTypeOtherDescriptionSpecified
                    || this.IntegratedDisclosureLineNumberValueSpecified
                    || this.IntegratedDisclosureSectionTypeSpecified
                    || this.IntegratedDisclosureSectionTypeOtherDescriptionSpecified
                    || this.LenderPaidMIInterestRateAdjustmentPercentSpecified
                    || this.MI_LTVCutoffPercentSpecified
                    || this.MI_LTVCutoffTypeSpecified
                    || this.MICancellationDateSpecified
                    || this.MICertificateIdentifierSpecified
                    || this.MICertificationStatusTypeSpecified
                    || this.MICertificationStatusTypeOtherDescriptionSpecified
                    || this.MICollectedNumberOfMonthsCountSpecified
                    || this.MICompanyNameTypeSpecified
                    || this.MICompanyNameTypeOtherDescriptionSpecified
                    || this.MICoverageEffectiveDateSpecified
                    || this.MICoveragePercentSpecified
                    || this.MICoveragePlanTypeSpecified
                    || this.MICoveragePlanTypeOtherDescriptionSpecified
                    || this.MICushionNumberOfMonthsCountSpecified
                    || this.MIDurationTypeSpecified
                    || this.MIDurationTypeOtherDescriptionSpecified
                    || this.MIEscrowedIndicatorSpecified
                    || this.MIEscrowIncludedInAggregateIndicatorSpecified
                    || this.MIInitialPremiumAmountSpecified
                    || this.MIInitialPremiumAtClosingTypeSpecified
                    || this.MIInitialPremiumAtClosingTypeOtherDescriptionSpecified
                    || this.MIInitialPremiumRatePercentSpecified
                    || this.MIPremiumCalendarYearAmountSpecified
                    || this.MIPremiumFinancedAmountSpecified
                    || this.MIPremiumFinancedIndicatorSpecified
                    || this.MIPremiumFromClosingAmountSpecified
                    || this.MIPremiumPaymentTypeSpecified
                    || this.MIPremiumPaymentTypeOtherDescriptionSpecified
                    || this.MIPremiumRefundableAmountSpecified
                    || this.MIPremiumRefundableConditionsDescriptionSpecified
                    || this.MIPremiumRefundableTypeSpecified
                    || this.MIPremiumRefundableTypeOtherDescriptionSpecified
                    || this.MIPremiumSourceTypeSpecified
                    || this.MIPremiumSourceTypeOtherDescriptionSpecified
                    || this.MIRemovalIdentifierSpecified
                    || this.MIScheduledTerminationDateSpecified
                    || this.MISourceTypeSpecified
                    || this.MISourceTypeOtherDescriptionSpecified
                    || this.PrimaryMIAbsenceReasonTypeSpecified
                    || this.PrimaryMIAbsenceReasonTypeOtherDescriptionSpecified
                    || this.RequiredProviderOfServiceIndicatorSpecified
                    || this.ScheduledAmortizationMidpointDateSpecified
                    || this.SellerMIPaidToDateSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("BorrowerMITerminationDate", Order = 0)]
        public MISMODate BorrowerMITerminationDate { get; set; }
    
        [XmlIgnore]
        public bool BorrowerMITerminationDateSpecified
        {
            get { return this.BorrowerMITerminationDate != null; }
            set { }
        }
    
        [XmlElement("FeePaidToType", Order = 1)]
        public MISMOEnum<FeePaidToBase> FeePaidToType { get; set; }
    
        [XmlIgnore]
        public bool FeePaidToTypeSpecified
        {
            get { return this.FeePaidToType != null; }
            set { }
        }
    
        [XmlElement("FeePaidToTypeOtherDescription", Order = 2)]
        public MISMOString FeePaidToTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool FeePaidToTypeOtherDescriptionSpecified
        {
            get { return this.FeePaidToTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("IntegratedDisclosureLineNumberValue", Order = 3)]
        public MISMOValue IntegratedDisclosureLineNumberValue { get; set; }
    
        [XmlIgnore]
        public bool IntegratedDisclosureLineNumberValueSpecified
        {
            get { return this.IntegratedDisclosureLineNumberValue != null; }
            set { }
        }
    
        [XmlElement("IntegratedDisclosureSectionType", Order = 4)]
        public MISMOEnum<IntegratedDisclosureSectionBase> IntegratedDisclosureSectionType { get; set; }
    
        [XmlIgnore]
        public bool IntegratedDisclosureSectionTypeSpecified
        {
            get { return this.IntegratedDisclosureSectionType != null; }
            set { }
        }
    
        [XmlElement("IntegratedDisclosureSectionTypeOtherDescription", Order = 5)]
        public MISMOString IntegratedDisclosureSectionTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool IntegratedDisclosureSectionTypeOtherDescriptionSpecified
        {
            get { return this.IntegratedDisclosureSectionTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("LenderPaidMIInterestRateAdjustmentPercent", Order = 6)]
        public MISMOPercent LenderPaidMIInterestRateAdjustmentPercent { get; set; }
    
        [XmlIgnore]
        public bool LenderPaidMIInterestRateAdjustmentPercentSpecified
        {
            get { return this.LenderPaidMIInterestRateAdjustmentPercent != null; }
            set { }
        }
    
        [XmlElement("MI_LTVCutoffPercent", Order = 7)]
        public MISMOPercent MI_LTVCutoffPercent { get; set; }
    
        [XmlIgnore]
        public bool MI_LTVCutoffPercentSpecified
        {
            get { return this.MI_LTVCutoffPercent != null; }
            set { }
        }
    
        [XmlElement("MI_LTVCutoffType", Order = 8)]
        public MISMOEnum<MI_LTVCutoffBase> MI_LTVCutoffType { get; set; }
    
        [XmlIgnore]
        public bool MI_LTVCutoffTypeSpecified
        {
            get { return this.MI_LTVCutoffType != null; }
            set { }
        }
    
        [XmlElement("MICancellationDate", Order = 9)]
        public MISMODate MICancellationDate { get; set; }
    
        [XmlIgnore]
        public bool MICancellationDateSpecified
        {
            get { return this.MICancellationDate != null; }
            set { }
        }
    
        [XmlElement("MICertificateIdentifier", Order = 10)]
        public MISMOIdentifier MICertificateIdentifier { get; set; }
    
        [XmlIgnore]
        public bool MICertificateIdentifierSpecified
        {
            get { return this.MICertificateIdentifier != null; }
            set { }
        }
    
        [XmlElement("MICertificationStatusType", Order = 11)]
        public MISMOEnum<MICertificationStatusBase> MICertificationStatusType { get; set; }
    
        [XmlIgnore]
        public bool MICertificationStatusTypeSpecified
        {
            get { return this.MICertificationStatusType != null; }
            set { }
        }
    
        [XmlElement("MICertificationStatusTypeOtherDescription", Order = 12)]
        public MISMOString MICertificationStatusTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool MICertificationStatusTypeOtherDescriptionSpecified
        {
            get { return this.MICertificationStatusTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("MICollectedNumberOfMonthsCount", Order = 13)]
        public MISMOCount MICollectedNumberOfMonthsCount { get; set; }
    
        [XmlIgnore]
        public bool MICollectedNumberOfMonthsCountSpecified
        {
            get { return this.MICollectedNumberOfMonthsCount != null; }
            set { }
        }
    
        [XmlElement("MICompanyNameType", Order = 14)]
        public MISMOEnum<MICompanyNameBase> MICompanyNameType { get; set; }
    
        [XmlIgnore]
        public bool MICompanyNameTypeSpecified
        {
            get { return this.MICompanyNameType != null; }
            set { }
        }
    
        [XmlElement("MICompanyNameTypeOtherDescription", Order = 15)]
        public MISMOString MICompanyNameTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool MICompanyNameTypeOtherDescriptionSpecified
        {
            get { return this.MICompanyNameTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("MICoverageEffectiveDate", Order = 16)]
        public MISMODate MICoverageEffectiveDate { get; set; }
    
        [XmlIgnore]
        public bool MICoverageEffectiveDateSpecified
        {
            get { return this.MICoverageEffectiveDate != null; }
            set { }
        }
    
        [XmlElement("MICoveragePercent", Order = 17)]
        public MISMOPercent MICoveragePercent { get; set; }
    
        [XmlIgnore]
        public bool MICoveragePercentSpecified
        {
            get { return this.MICoveragePercent != null; }
            set { }
        }
    
        [XmlElement("MICoveragePlanType", Order = 18)]
        public MISMOEnum<MICoveragePlanBase> MICoveragePlanType { get; set; }
    
        [XmlIgnore]
        public bool MICoveragePlanTypeSpecified
        {
            get { return this.MICoveragePlanType != null; }
            set { }
        }
    
        [XmlElement("MICoveragePlanTypeOtherDescription", Order = 19)]
        public MISMOString MICoveragePlanTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool MICoveragePlanTypeOtherDescriptionSpecified
        {
            get { return this.MICoveragePlanTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("MICushionNumberOfMonthsCount", Order = 20)]
        public MISMOCount MICushionNumberOfMonthsCount { get; set; }
    
        [XmlIgnore]
        public bool MICushionNumberOfMonthsCountSpecified
        {
            get { return this.MICushionNumberOfMonthsCount != null; }
            set { }
        }
    
        [XmlElement("MIDurationType", Order = 21)]
        public MISMOEnum<MIDurationBase> MIDurationType { get; set; }
    
        [XmlIgnore]
        public bool MIDurationTypeSpecified
        {
            get { return this.MIDurationType != null; }
            set { }
        }
    
        [XmlElement("MIDurationTypeOtherDescription", Order = 22)]
        public MISMOString MIDurationTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool MIDurationTypeOtherDescriptionSpecified
        {
            get { return this.MIDurationTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("MIEscrowedIndicator", Order = 23)]
        public MISMOIndicator MIEscrowedIndicator { get; set; }
    
        [XmlIgnore]
        public bool MIEscrowedIndicatorSpecified
        {
            get { return this.MIEscrowedIndicator != null; }
            set { }
        }
    
        [XmlElement("MIEscrowIncludedInAggregateIndicator", Order = 24)]
        public MISMOIndicator MIEscrowIncludedInAggregateIndicator { get; set; }
    
        [XmlIgnore]
        public bool MIEscrowIncludedInAggregateIndicatorSpecified
        {
            get { return this.MIEscrowIncludedInAggregateIndicator != null; }
            set { }
        }
    
        [XmlElement("MIInitialPremiumAmount", Order = 25)]
        public MISMOAmount MIInitialPremiumAmount { get; set; }
    
        [XmlIgnore]
        public bool MIInitialPremiumAmountSpecified
        {
            get { return this.MIInitialPremiumAmount != null; }
            set { }
        }
    
        [XmlElement("MIInitialPremiumAtClosingType", Order = 26)]
        public MISMOEnum<MIInitialPremiumAtClosingBase> MIInitialPremiumAtClosingType { get; set; }
    
        [XmlIgnore]
        public bool MIInitialPremiumAtClosingTypeSpecified
        {
            get { return this.MIInitialPremiumAtClosingType != null; }
            set { }
        }
    
        [XmlElement("MIInitialPremiumAtClosingTypeOtherDescription", Order = 27)]
        public MISMOString MIInitialPremiumAtClosingTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool MIInitialPremiumAtClosingTypeOtherDescriptionSpecified
        {
            get { return this.MIInitialPremiumAtClosingTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("MIInitialPremiumRatePercent", Order = 28)]
        public MISMOPercent MIInitialPremiumRatePercent { get; set; }
    
        [XmlIgnore]
        public bool MIInitialPremiumRatePercentSpecified
        {
            get { return this.MIInitialPremiumRatePercent != null; }
            set { }
        }
    
        [XmlElement("MIPremiumCalendarYearAmount", Order = 29)]
        public MISMOAmount MIPremiumCalendarYearAmount { get; set; }
    
        [XmlIgnore]
        public bool MIPremiumCalendarYearAmountSpecified
        {
            get { return this.MIPremiumCalendarYearAmount != null; }
            set { }
        }
    
        [XmlElement("MIPremiumFinancedAmount", Order = 30)]
        public MISMOAmount MIPremiumFinancedAmount { get; set; }
    
        [XmlIgnore]
        public bool MIPremiumFinancedAmountSpecified
        {
            get { return this.MIPremiumFinancedAmount != null; }
            set { }
        }
    
        [XmlElement("MIPremiumFinancedIndicator", Order = 31)]
        public MISMOIndicator MIPremiumFinancedIndicator { get; set; }
    
        [XmlIgnore]
        public bool MIPremiumFinancedIndicatorSpecified
        {
            get { return this.MIPremiumFinancedIndicator != null; }
            set { }
        }
    
        [XmlElement("MIPremiumFromClosingAmount", Order = 32)]
        public MISMOAmount MIPremiumFromClosingAmount { get; set; }
    
        [XmlIgnore]
        public bool MIPremiumFromClosingAmountSpecified
        {
            get { return this.MIPremiumFromClosingAmount != null; }
            set { }
        }
    
        [XmlElement("MIPremiumPaymentType", Order = 33)]
        public MISMOEnum<MIPremiumPaymentBase> MIPremiumPaymentType { get; set; }
    
        [XmlIgnore]
        public bool MIPremiumPaymentTypeSpecified
        {
            get { return this.MIPremiumPaymentType != null; }
            set { }
        }
    
        [XmlElement("MIPremiumPaymentTypeOtherDescription", Order = 34)]
        public MISMOString MIPremiumPaymentTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool MIPremiumPaymentTypeOtherDescriptionSpecified
        {
            get { return this.MIPremiumPaymentTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("MIPremiumRefundableAmount", Order = 35)]
        public MISMOAmount MIPremiumRefundableAmount { get; set; }
    
        [XmlIgnore]
        public bool MIPremiumRefundableAmountSpecified
        {
            get { return this.MIPremiumRefundableAmount != null; }
            set { }
        }
    
        [XmlElement("MIPremiumRefundableConditionsDescription", Order = 36)]
        public MISMOString MIPremiumRefundableConditionsDescription { get; set; }
    
        [XmlIgnore]
        public bool MIPremiumRefundableConditionsDescriptionSpecified
        {
            get { return this.MIPremiumRefundableConditionsDescription != null; }
            set { }
        }
    
        [XmlElement("MIPremiumRefundableType", Order = 37)]
        public MISMOEnum<MIPremiumRefundableBase> MIPremiumRefundableType { get; set; }
    
        [XmlIgnore]
        public bool MIPremiumRefundableTypeSpecified
        {
            get { return this.MIPremiumRefundableType != null; }
            set { }
        }
    
        [XmlElement("MIPremiumRefundableTypeOtherDescription", Order = 38)]
        public MISMOString MIPremiumRefundableTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool MIPremiumRefundableTypeOtherDescriptionSpecified
        {
            get { return this.MIPremiumRefundableTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("MIPremiumSourceType", Order = 39)]
        public MISMOEnum<MIPremiumSourceBase> MIPremiumSourceType { get; set; }
    
        [XmlIgnore]
        public bool MIPremiumSourceTypeSpecified
        {
            get { return this.MIPremiumSourceType != null; }
            set { }
        }
    
        [XmlElement("MIPremiumSourceTypeOtherDescription", Order = 40)]
        public MISMOString MIPremiumSourceTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool MIPremiumSourceTypeOtherDescriptionSpecified
        {
            get { return this.MIPremiumSourceTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("MIRemovalIdentifier", Order = 41)]
        public MISMOIdentifier MIRemovalIdentifier { get; set; }
    
        [XmlIgnore]
        public bool MIRemovalIdentifierSpecified
        {
            get { return this.MIRemovalIdentifier != null; }
            set { }
        }
    
        [XmlElement("MIScheduledTerminationDate", Order = 42)]
        public MISMODate MIScheduledTerminationDate { get; set; }
    
        [XmlIgnore]
        public bool MIScheduledTerminationDateSpecified
        {
            get { return this.MIScheduledTerminationDate != null; }
            set { }
        }
    
        [XmlElement("MISourceType", Order = 43)]
        public MISMOEnum<MISourceBase> MISourceType { get; set; }
    
        [XmlIgnore]
        public bool MISourceTypeSpecified
        {
            get { return this.MISourceType != null; }
            set { }
        }
    
        [XmlElement("MISourceTypeOtherDescription", Order = 44)]
        public MISMOString MISourceTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool MISourceTypeOtherDescriptionSpecified
        {
            get { return this.MISourceTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("PrimaryMIAbsenceReasonType", Order = 45)]
        public MISMOEnum<PrimaryMIAbsenceReasonBase> PrimaryMIAbsenceReasonType { get; set; }
    
        [XmlIgnore]
        public bool PrimaryMIAbsenceReasonTypeSpecified
        {
            get { return this.PrimaryMIAbsenceReasonType != null; }
            set { }
        }
    
        [XmlElement("PrimaryMIAbsenceReasonTypeOtherDescription", Order = 46)]
        public MISMOString PrimaryMIAbsenceReasonTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool PrimaryMIAbsenceReasonTypeOtherDescriptionSpecified
        {
            get { return this.PrimaryMIAbsenceReasonTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("RequiredProviderOfServiceIndicator", Order = 47)]
        public MISMOIndicator RequiredProviderOfServiceIndicator { get; set; }
    
        [XmlIgnore]
        public bool RequiredProviderOfServiceIndicatorSpecified
        {
            get { return this.RequiredProviderOfServiceIndicator != null; }
            set { }
        }
    
        [XmlElement("ScheduledAmortizationMidpointDate", Order = 48)]
        public MISMODate ScheduledAmortizationMidpointDate { get; set; }
    
        [XmlIgnore]
        public bool ScheduledAmortizationMidpointDateSpecified
        {
            get { return this.ScheduledAmortizationMidpointDate != null; }
            set { }
        }
    
        [XmlElement("SellerMIPaidToDate", Order = 49)]
        public MISMODate SellerMIPaidToDate { get; set; }
    
        [XmlIgnore]
        public bool SellerMIPaidToDateSpecified
        {
            get { return this.SellerMIPaidToDate != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 50)]
        public MI_DATA_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
