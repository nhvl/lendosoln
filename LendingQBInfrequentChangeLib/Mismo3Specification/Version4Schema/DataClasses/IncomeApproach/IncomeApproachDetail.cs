namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class INCOME_APPROACH_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.EstimatedMarketMonthlyRentAmountSpecified
                    || this.GrossRentMultiplierFactorPercentSpecified
                    || this.IncomeAnalysisCommentDescriptionSpecified
                    || this.ValueIndicatedByIncomeApproachAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("EstimatedMarketMonthlyRentAmount", Order = 0)]
        public MISMOAmount EstimatedMarketMonthlyRentAmount { get; set; }
    
        [XmlIgnore]
        public bool EstimatedMarketMonthlyRentAmountSpecified
        {
            get { return this.EstimatedMarketMonthlyRentAmount != null; }
            set { }
        }
    
        [XmlElement("GrossRentMultiplierFactorPercent", Order = 1)]
        public MISMOPercent GrossRentMultiplierFactorPercent { get; set; }
    
        [XmlIgnore]
        public bool GrossRentMultiplierFactorPercentSpecified
        {
            get { return this.GrossRentMultiplierFactorPercent != null; }
            set { }
        }
    
        [XmlElement("IncomeAnalysisCommentDescription", Order = 2)]
        public MISMOString IncomeAnalysisCommentDescription { get; set; }
    
        [XmlIgnore]
        public bool IncomeAnalysisCommentDescriptionSpecified
        {
            get { return this.IncomeAnalysisCommentDescription != null; }
            set { }
        }
    
        [XmlElement("ValueIndicatedByIncomeApproachAmount", Order = 3)]
        public MISMOAmount ValueIndicatedByIncomeApproachAmount { get; set; }
    
        [XmlIgnore]
        public bool ValueIndicatedByIncomeApproachAmountSpecified
        {
            get { return this.ValueIndicatedByIncomeApproachAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public INCOME_APPROACH_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
