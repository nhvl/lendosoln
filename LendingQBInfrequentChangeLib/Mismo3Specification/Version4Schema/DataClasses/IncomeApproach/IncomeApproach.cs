namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class INCOME_APPROACH
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.IncomeApproachDetailSpecified
                    || this.MultifamilyRentScheduleSpecified
                    || this.MultifamilyRentalsSpecified
                    || this.ResidentialRentScheduleSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("INCOME_APPROACH_DETAIL", Order = 0)]
        public INCOME_APPROACH_DETAIL IncomeApproachDetail { get; set; }
    
        [XmlIgnore]
        public bool IncomeApproachDetailSpecified
        {
            get { return this.IncomeApproachDetail != null && this.IncomeApproachDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("MULTIFAMILY_RENT_SCHEDULE", Order = 1)]
        public MULTIFAMILY_RENT_SCHEDULE MultifamilyRentSchedule { get; set; }
    
        [XmlIgnore]
        public bool MultifamilyRentScheduleSpecified
        {
            get { return this.MultifamilyRentSchedule != null && this.MultifamilyRentSchedule.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("MULTIFAMILY_RENTALS", Order = 2)]
        public MULTIFAMILY_RENTALS MultifamilyRentals { get; set; }
    
        [XmlIgnore]
        public bool MultifamilyRentalsSpecified
        {
            get { return this.MultifamilyRentals != null && this.MultifamilyRentals.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("RESIDENTIAL_RENT_SCHEDULE", Order = 3)]
        public RESIDENTIAL_RENT_SCHEDULE ResidentialRentSchedule { get; set; }
    
        [XmlIgnore]
        public bool ResidentialRentScheduleSpecified
        {
            get { return this.ResidentialRentSchedule != null && this.ResidentialRentSchedule.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public INCOME_APPROACH_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
