namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PERIODIC_LATE_COUNT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.Periodic120DaysLateCountSpecified
                    || this.Periodic30DaysLateCountSpecified
                    || this.Periodic60DaysLateCountSpecified
                    || this.Periodic90DaysLateCountSpecified
                    || this.PeriodicLateCountTypeSpecified
                    || this.PeriodicLateCountTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("Periodic120DaysLateCount", Order = 0)]
        public MISMOCount Periodic120DaysLateCount { get; set; }
    
        [XmlIgnore]
        public bool Periodic120DaysLateCountSpecified
        {
            get { return this.Periodic120DaysLateCount != null; }
            set { }
        }
    
        [XmlElement("Periodic30DaysLateCount", Order = 1)]
        public MISMOCount Periodic30DaysLateCount { get; set; }
    
        [XmlIgnore]
        public bool Periodic30DaysLateCountSpecified
        {
            get { return this.Periodic30DaysLateCount != null; }
            set { }
        }
    
        [XmlElement("Periodic60DaysLateCount", Order = 2)]
        public MISMOCount Periodic60DaysLateCount { get; set; }
    
        [XmlIgnore]
        public bool Periodic60DaysLateCountSpecified
        {
            get { return this.Periodic60DaysLateCount != null; }
            set { }
        }
    
        [XmlElement("Periodic90DaysLateCount", Order = 3)]
        public MISMOCount Periodic90DaysLateCount { get; set; }
    
        [XmlIgnore]
        public bool Periodic90DaysLateCountSpecified
        {
            get { return this.Periodic90DaysLateCount != null; }
            set { }
        }
    
        [XmlElement("PeriodicLateCountType", Order = 4)]
        public MISMOEnum<PeriodicLateCountBase> PeriodicLateCountType { get; set; }
    
        [XmlIgnore]
        public bool PeriodicLateCountTypeSpecified
        {
            get { return this.PeriodicLateCountType != null; }
            set { }
        }
    
        [XmlElement("PeriodicLateCountTypeOtherDescription", Order = 5)]
        public MISMOString PeriodicLateCountTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool PeriodicLateCountTypeOtherDescriptionSpecified
        {
            get { return this.PeriodicLateCountTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 6)]
        public PERIODIC_LATE_COUNT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
