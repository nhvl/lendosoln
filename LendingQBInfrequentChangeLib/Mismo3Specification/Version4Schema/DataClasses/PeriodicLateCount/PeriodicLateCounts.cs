namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class PERIODIC_LATE_COUNTS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.PeriodicLateCountListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PERIODIC_LATE_COUNT", Order = 0)]
        public List<PERIODIC_LATE_COUNT> PeriodicLateCountList { get; set; } = new List<PERIODIC_LATE_COUNT>();
    
        [XmlIgnore]
        public bool PeriodicLateCountListSpecified
        {
            get { return this.PeriodicLateCountList != null && this.PeriodicLateCountList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public PERIODIC_LATE_COUNTS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
