namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class ARREARAGE_COMPONENTS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ArrearageComponentListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ARREARAGE_COMPONENT", Order = 0)]
        public List<ARREARAGE_COMPONENT> ArrearageComponentList { get; set; } = new List<ARREARAGE_COMPONENT>();
    
        [XmlIgnore]
        public bool ArrearageComponentListSpecified
        {
            get { return this.ArrearageComponentList != null && this.ArrearageComponentList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public ARREARAGE_COMPONENTS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
