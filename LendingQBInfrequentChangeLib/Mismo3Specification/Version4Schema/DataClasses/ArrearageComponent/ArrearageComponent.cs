namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class ARREARAGE_COMPONENT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ArrearageAmountSpecified
                    || this.ArrearageComponentTypeSpecified
                    || this.ArrearageComponentTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ArrearageAmount", Order = 0)]
        public MISMOAmount ArrearageAmount { get; set; }
    
        [XmlIgnore]
        public bool ArrearageAmountSpecified
        {
            get { return this.ArrearageAmount != null; }
            set { }
        }
    
        [XmlElement("ArrearageComponentType", Order = 1)]
        public MISMOEnum<ArrearageComponentBase> ArrearageComponentType { get; set; }
    
        [XmlIgnore]
        public bool ArrearageComponentTypeSpecified
        {
            get { return this.ArrearageComponentType != null; }
            set { }
        }
    
        [XmlElement("ArrearageComponentTypeOtherDescription", Order = 2)]
        public MISMOString ArrearageComponentTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ArrearageComponentTypeOtherDescriptionSpecified
        {
            get { return this.ArrearageComponentTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public ARREARAGE_COMPONENT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
