namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class SALES_CONTINGENCIES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.SalesContingencyListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("SALES_CONTINGENCY", Order = 0)]
        public List<SALES_CONTINGENCY> SalesContingencyList { get; set; } = new List<SALES_CONTINGENCY>();
    
        [XmlIgnore]
        public bool SalesContingencyListSpecified
        {
            get { return this.SalesContingencyList != null && this.SalesContingencyList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public SALES_CONTINGENCIES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
