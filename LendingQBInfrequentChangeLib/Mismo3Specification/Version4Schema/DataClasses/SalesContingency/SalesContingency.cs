namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class SALES_CONTINGENCY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.OfferContingencyTypeSpecified
                    || this.OfferContingencyTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("OfferContingencyType", Order = 0)]
        public MISMOEnum<OfferContingencyBase> OfferContingencyType { get; set; }
    
        [XmlIgnore]
        public bool OfferContingencyTypeSpecified
        {
            get { return this.OfferContingencyType != null; }
            set { }
        }
    
        [XmlElement("OfferContingencyTypeOtherDescription", Order = 1)]
        public MISMOString OfferContingencyTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool OfferContingencyTypeOtherDescriptionSpecified
        {
            get { return this.OfferContingencyTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public SALES_CONTINGENCY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
