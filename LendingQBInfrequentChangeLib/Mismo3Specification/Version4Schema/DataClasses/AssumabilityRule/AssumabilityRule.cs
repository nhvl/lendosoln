namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class ASSUMABILITY_RULE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AssumabilityBeginDateSpecified
                    || this.AssumabilityTermMonthsCountSpecified
                    || this.AssumabilityTypeSpecified
                    || this.AssumabilityTypeOtherDescriptionSpecified
                    || this.ConditionsToAssumabilityIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AssumabilityBeginDate", Order = 0)]
        public MISMODate AssumabilityBeginDate { get; set; }
    
        [XmlIgnore]
        public bool AssumabilityBeginDateSpecified
        {
            get { return this.AssumabilityBeginDate != null; }
            set { }
        }
    
        [XmlElement("AssumabilityTermMonthsCount", Order = 1)]
        public MISMOCount AssumabilityTermMonthsCount { get; set; }
    
        [XmlIgnore]
        public bool AssumabilityTermMonthsCountSpecified
        {
            get { return this.AssumabilityTermMonthsCount != null; }
            set { }
        }
    
        [XmlElement("AssumabilityType", Order = 2)]
        public MISMOEnum<AssumabilityBase> AssumabilityType { get; set; }
    
        [XmlIgnore]
        public bool AssumabilityTypeSpecified
        {
            get { return this.AssumabilityType != null; }
            set { }
        }
    
        [XmlElement("AssumabilityTypeOtherDescription", Order = 3)]
        public MISMOString AssumabilityTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool AssumabilityTypeOtherDescriptionSpecified
        {
            get { return this.AssumabilityTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("ConditionsToAssumabilityIndicator", Order = 4)]
        public MISMOIndicator ConditionsToAssumabilityIndicator { get; set; }
    
        [XmlIgnore]
        public bool ConditionsToAssumabilityIndicatorSpecified
        {
            get { return this.ConditionsToAssumabilityIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 5)]
        public ASSUMABILITY_RULE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
