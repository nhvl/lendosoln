namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class REGULATORY_AGENCY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.RegulatoryAgencyTypeSpecified
                    || this.RegulatoryAgencyTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("RegulatoryAgencyType", Order = 0)]
        public MISMOEnum<RegulatoryAgencyBase> RegulatoryAgencyType { get; set; }
    
        [XmlIgnore]
        public bool RegulatoryAgencyTypeSpecified
        {
            get { return this.RegulatoryAgencyType != null; }
            set { }
        }
    
        [XmlElement("RegulatoryAgencyTypeOtherDescription", Order = 1)]
        public MISMOString RegulatoryAgencyTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool RegulatoryAgencyTypeOtherDescriptionSpecified
        {
            get { return this.RegulatoryAgencyTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public REGULATORY_AGENCY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
