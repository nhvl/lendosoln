namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PROPERTY_SELLER
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.MaritalStatusTypeSpecified
                    || this.MaritalStatusTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("MaritalStatusType", Order = 0)]
        public MISMOEnum<MaritalStatusBase> MaritalStatusType { get; set; }
    
        [XmlIgnore]
        public bool MaritalStatusTypeSpecified
        {
            get { return this.MaritalStatusType != null; }
            set { }
        }
    
        [XmlElement("MaritalStatusTypeOtherDescription", Order = 1)]
        public MISMOString MaritalStatusTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool MaritalStatusTypeOtherDescriptionSpecified
        {
            get { return this.MaritalStatusTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public PROPERTY_SELLER_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
