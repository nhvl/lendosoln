namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CURRENT_INCOME_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.IncomeReportedTypeSpecified
                    || this.URLABorrowerTotalMonthlyIncomeAmountSpecified
                    || this.URLABorrowerTotalOtherIncomeAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("IncomeReportedType", Order = 0)]
        public MISMOEnum<IncomeReportedBase> IncomeReportedType { get; set; }
    
        [XmlIgnore]
        public bool IncomeReportedTypeSpecified
        {
            get { return this.IncomeReportedType != null; }
            set { }
        }
    
        [XmlElement("URLABorrowerTotalMonthlyIncomeAmount", Order = 1)]
        public MISMOAmount URLABorrowerTotalMonthlyIncomeAmount { get; set; }
    
        [XmlIgnore]
        public bool URLABorrowerTotalMonthlyIncomeAmountSpecified
        {
            get { return this.URLABorrowerTotalMonthlyIncomeAmount != null; }
            set { }
        }
    
        [XmlElement("URLABorrowerTotalOtherIncomeAmount", Order = 2)]
        public MISMOAmount URLABorrowerTotalOtherIncomeAmount { get; set; }
    
        [XmlIgnore]
        public bool URLABorrowerTotalOtherIncomeAmountSpecified
        {
            get { return this.URLABorrowerTotalOtherIncomeAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public CURRENT_INCOME_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
