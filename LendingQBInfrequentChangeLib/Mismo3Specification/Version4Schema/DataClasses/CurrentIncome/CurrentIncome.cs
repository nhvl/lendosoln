namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CURRENT_INCOME
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CurrentIncomeDetailSpecified
                    || this.CurrentIncomeItemsSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CURRENT_INCOME_DETAIL", Order = 0)]
        public CURRENT_INCOME_DETAIL CurrentIncomeDetail { get; set; }
    
        [XmlIgnore]
        public bool CurrentIncomeDetailSpecified
        {
            get { return this.CurrentIncomeDetail != null && this.CurrentIncomeDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CURRENT_INCOME_ITEMS", Order = 1)]
        public CURRENT_INCOME_ITEMS CurrentIncomeItems { get; set; }
    
        [XmlIgnore]
        public bool CurrentIncomeItemsSpecified
        {
            get { return this.CurrentIncomeItems != null && this.CurrentIncomeItems.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public CURRENT_INCOME_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
