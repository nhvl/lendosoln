namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class DEPENDENTS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DependentListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DEPENDENT", Order = 0)]
        public List<DEPENDENT> DependentList { get; set; } = new List<DEPENDENT>();
    
        [XmlIgnore]
        public bool DependentListSpecified
        {
            get { return this.DependentList != null && this.DependentList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public DEPENDENTS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
