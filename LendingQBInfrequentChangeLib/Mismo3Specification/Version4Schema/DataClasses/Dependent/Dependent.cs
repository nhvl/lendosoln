namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class DEPENDENT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DependentAgeYearsCountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DependentAgeYearsCount", Order = 0)]
        public MISMOCount DependentAgeYearsCount { get; set; }
    
        [XmlIgnore]
        public bool DependentAgeYearsCountSpecified
        {
            get { return this.DependentAgeYearsCount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public DEPENDENT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
