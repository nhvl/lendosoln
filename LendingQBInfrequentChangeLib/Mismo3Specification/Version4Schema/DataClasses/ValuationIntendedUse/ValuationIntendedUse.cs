namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class VALUATION_INTENDED_USE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ValuationIntendedUseDescriptionSpecified
                    || this.ValuationIntendedUseTypeSpecified
                    || this.ValuationIntendedUseTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ValuationIntendedUseDescription", Order = 0)]
        public MISMOString ValuationIntendedUseDescription { get; set; }
    
        [XmlIgnore]
        public bool ValuationIntendedUseDescriptionSpecified
        {
            get { return this.ValuationIntendedUseDescription != null; }
            set { }
        }
    
        [XmlElement("ValuationIntendedUseType", Order = 1)]
        public MISMOEnum<ValuationIntendedUseBase> ValuationIntendedUseType { get; set; }
    
        [XmlIgnore]
        public bool ValuationIntendedUseTypeSpecified
        {
            get { return this.ValuationIntendedUseType != null; }
            set { }
        }
    
        [XmlElement("ValuationIntendedUseTypeOtherDescription", Order = 2)]
        public MISMOString ValuationIntendedUseTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ValuationIntendedUseTypeOtherDescriptionSpecified
        {
            get { return this.ValuationIntendedUseTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public VALUATION_INTENDED_USE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
