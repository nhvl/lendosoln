namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class VALUATION_INTENDED_USES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ValuationIntendedUseListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("VALUATION_INTENDED_USE", Order = 0)]
        public List<VALUATION_INTENDED_USE> ValuationIntendedUseList { get; set; } = new List<VALUATION_INTENDED_USE>();
    
        [XmlIgnore]
        public bool ValuationIntendedUseListSpecified
        {
            get { return this.ValuationIntendedUseList != null && this.ValuationIntendedUseList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public VALUATION_INTENDED_USES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
