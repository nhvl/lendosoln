namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class BUYDOWN_FUND_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BuydownAmountSpecified
                    || this.BuydownFundingTypeSpecified
                    || this.BuydownFundingTypeOtherDescriptionSpecified
                    || this.BuydownPercentSpecified
                    || this.FundsSourceTypeSpecified
                    || this.FundsSourceTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("BuydownAmount", Order = 0)]
        public MISMOAmount BuydownAmount { get; set; }
    
        [XmlIgnore]
        public bool BuydownAmountSpecified
        {
            get { return this.BuydownAmount != null; }
            set { }
        }
    
        [XmlElement("BuydownFundingType", Order = 1)]
        public MISMOEnum<BuydownFundingBase> BuydownFundingType { get; set; }
    
        [XmlIgnore]
        public bool BuydownFundingTypeSpecified
        {
            get { return this.BuydownFundingType != null; }
            set { }
        }
    
        [XmlElement("BuydownFundingTypeOtherDescription", Order = 2)]
        public MISMOString BuydownFundingTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool BuydownFundingTypeOtherDescriptionSpecified
        {
            get { return this.BuydownFundingTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("BuydownPercent", Order = 3)]
        public MISMOPercent BuydownPercent { get; set; }
    
        [XmlIgnore]
        public bool BuydownPercentSpecified
        {
            get { return this.BuydownPercent != null; }
            set { }
        }
    
        [XmlElement("FundsSourceType", Order = 4)]
        public MISMOEnum<FundsSourceBase> FundsSourceType { get; set; }
    
        [XmlIgnore]
        public bool FundsSourceTypeSpecified
        {
            get { return this.FundsSourceType != null; }
            set { }
        }
    
        [XmlElement("FundsSourceTypeOtherDescription", Order = 5)]
        public MISMOString FundsSourceTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool FundsSourceTypeOtherDescriptionSpecified
        {
            get { return this.FundsSourceTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 6)]
        public BUYDOWN_FUND_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
