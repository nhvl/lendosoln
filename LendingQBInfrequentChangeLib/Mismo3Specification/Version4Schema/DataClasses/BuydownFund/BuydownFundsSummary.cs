namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class BUYDOWN_FUNDS_SUMMARY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BuydownTotalSubsidyAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("BuydownTotalSubsidyAmount", Order = 0)]
        public MISMOAmount BuydownTotalSubsidyAmount { get; set; }
    
        [XmlIgnore]
        public bool BuydownTotalSubsidyAmountSpecified
        {
            get { return this.BuydownTotalSubsidyAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public BUYDOWN_FUNDS_SUMMARY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
