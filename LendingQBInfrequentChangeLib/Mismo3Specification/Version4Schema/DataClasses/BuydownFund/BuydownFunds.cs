namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class BUYDOWN_FUNDS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BuydownFundListSpecified
                    || this.BuydownFundsSummarySpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("BUYDOWN_FUND", Order = 0)]
        public List<BUYDOWN_FUND> BuydownFundList { get; set; } = new List<BUYDOWN_FUND>();
    
        [XmlIgnore]
        public bool BuydownFundListSpecified
        {
            get { return this.BuydownFundList != null && this.BuydownFundList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("BUYDOWN_FUNDS_SUMMARY", Order = 1)]
        public BUYDOWN_FUNDS_SUMMARY BuydownFundsSummary { get; set; }
    
        [XmlIgnore]
        public bool BuydownFundsSummarySpecified
        {
            get { return this.BuydownFundsSummary != null && this.BuydownFundsSummary.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public BUYDOWN_FUNDS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
