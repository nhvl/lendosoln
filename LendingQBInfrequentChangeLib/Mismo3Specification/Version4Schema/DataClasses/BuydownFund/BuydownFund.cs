namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class BUYDOWN_FUND
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BuydownFundDetailSpecified
                    || this.NameSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("BUYDOWN_FUND_DETAIL", Order = 0)]
        public BUYDOWN_FUND_DETAIL BuydownFundDetail { get; set; }
    
        [XmlIgnore]
        public bool BuydownFundDetailSpecified
        {
            get { return this.BuydownFundDetail != null && this.BuydownFundDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("NAME", Order = 1)]
        public NAME Name { get; set; }
    
        [XmlIgnore]
        public bool NameSpecified
        {
            get { return this.Name != null && this.Name.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public BUYDOWN_FUND_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
