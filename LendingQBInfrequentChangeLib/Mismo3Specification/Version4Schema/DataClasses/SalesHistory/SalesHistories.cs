namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class SALES_HISTORIES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.SalesHistoryListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("SALES_HISTORY", Order = 0)]
        public List<SALES_HISTORY> SalesHistoryList { get; set; } = new List<SALES_HISTORY>();
    
        [XmlIgnore]
        public bool SalesHistoryListSpecified
        {
            get { return this.SalesHistoryList != null && this.SalesHistoryList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public SALES_HISTORIES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
