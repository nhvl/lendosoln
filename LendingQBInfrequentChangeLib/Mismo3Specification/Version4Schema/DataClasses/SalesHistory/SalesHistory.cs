namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class SALES_HISTORY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ArmsLengthIndicatorSpecified
                    || this.ForSaleByOwnerIndicatorSpecified
                    || this.OwnershipTransferCommentDescriptionSpecified
                    || this.OwnershipTransferDateSpecified
                    || this.OwnershipTransferTransactionAmountSpecified
                    || this.OwnershipTransferTransactionTypeSpecified
                    || this.OwnershipTransferTransactionTypeOtherDescriptionSpecified
                    || this.PurchasePriceAmountSpecified
                    || this.SaleTypeSpecified
                    || this.SaleTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ArmsLengthIndicator", Order = 0)]
        public MISMOIndicator ArmsLengthIndicator { get; set; }
    
        [XmlIgnore]
        public bool ArmsLengthIndicatorSpecified
        {
            get { return this.ArmsLengthIndicator != null; }
            set { }
        }
    
        [XmlElement("ForSaleByOwnerIndicator", Order = 1)]
        public MISMOIndicator ForSaleByOwnerIndicator { get; set; }
    
        [XmlIgnore]
        public bool ForSaleByOwnerIndicatorSpecified
        {
            get { return this.ForSaleByOwnerIndicator != null; }
            set { }
        }
    
        [XmlElement("OwnershipTransferCommentDescription", Order = 2)]
        public MISMOString OwnershipTransferCommentDescription { get; set; }
    
        [XmlIgnore]
        public bool OwnershipTransferCommentDescriptionSpecified
        {
            get { return this.OwnershipTransferCommentDescription != null; }
            set { }
        }
    
        [XmlElement("OwnershipTransferDate", Order = 3)]
        public MISMODate OwnershipTransferDate { get; set; }
    
        [XmlIgnore]
        public bool OwnershipTransferDateSpecified
        {
            get { return this.OwnershipTransferDate != null; }
            set { }
        }
    
        [XmlElement("OwnershipTransferTransactionAmount", Order = 4)]
        public MISMOAmount OwnershipTransferTransactionAmount { get; set; }
    
        [XmlIgnore]
        public bool OwnershipTransferTransactionAmountSpecified
        {
            get { return this.OwnershipTransferTransactionAmount != null; }
            set { }
        }
    
        [XmlElement("OwnershipTransferTransactionType", Order = 5)]
        public MISMOEnum<OwnershipTransferTransactionBase> OwnershipTransferTransactionType { get; set; }
    
        [XmlIgnore]
        public bool OwnershipTransferTransactionTypeSpecified
        {
            get { return this.OwnershipTransferTransactionType != null; }
            set { }
        }
    
        [XmlElement("OwnershipTransferTransactionTypeOtherDescription", Order = 6)]
        public MISMOString OwnershipTransferTransactionTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool OwnershipTransferTransactionTypeOtherDescriptionSpecified
        {
            get { return this.OwnershipTransferTransactionTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("PurchasePriceAmount", Order = 7)]
        public MISMOAmount PurchasePriceAmount { get; set; }
    
        [XmlIgnore]
        public bool PurchasePriceAmountSpecified
        {
            get { return this.PurchasePriceAmount != null; }
            set { }
        }
    
        [XmlElement("SaleType", Order = 8)]
        public MISMOEnum<SaleBase> SaleType { get; set; }
    
        [XmlIgnore]
        public bool SaleTypeSpecified
        {
            get { return this.SaleType != null; }
            set { }
        }
    
        [XmlElement("SaleTypeOtherDescription", Order = 9)]
        public MISMOString SaleTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool SaleTypeOtherDescriptionSpecified
        {
            get { return this.SaleTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 10)]
        public SALES_HISTORY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
