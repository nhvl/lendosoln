namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class VALUATION_RECONCILIATION_SUMMARY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AppraisalReportEffectiveDateSpecified
                    || this.OpinionOfValueAmountSpecified
                    || this.PropertyValuationConditionalConclusionTypeSpecified
                    || this.PropertyValuationConditionalConclusionTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AppraisalReportEffectiveDate", Order = 0)]
        public MISMODate AppraisalReportEffectiveDate { get; set; }
    
        [XmlIgnore]
        public bool AppraisalReportEffectiveDateSpecified
        {
            get { return this.AppraisalReportEffectiveDate != null; }
            set { }
        }
    
        [XmlElement("OpinionOfValueAmount", Order = 1)]
        public MISMOAmount OpinionOfValueAmount { get; set; }
    
        [XmlIgnore]
        public bool OpinionOfValueAmountSpecified
        {
            get { return this.OpinionOfValueAmount != null; }
            set { }
        }
    
        [XmlElement("PropertyValuationConditionalConclusionType", Order = 2)]
        public MISMOEnum<PropertyValuationConditionalConclusionBase> PropertyValuationConditionalConclusionType { get; set; }
    
        [XmlIgnore]
        public bool PropertyValuationConditionalConclusionTypeSpecified
        {
            get { return this.PropertyValuationConditionalConclusionType != null; }
            set { }
        }
    
        [XmlElement("PropertyValuationConditionalConclusionTypeOtherDescription", Order = 3)]
        public MISMOString PropertyValuationConditionalConclusionTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool PropertyValuationConditionalConclusionTypeOtherDescriptionSpecified
        {
            get { return this.PropertyValuationConditionalConclusionTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public VALUATION_RECONCILIATION_SUMMARY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
