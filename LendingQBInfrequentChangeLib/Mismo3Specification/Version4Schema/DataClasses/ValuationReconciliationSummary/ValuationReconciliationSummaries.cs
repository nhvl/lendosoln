namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class VALUATION_RECONCILIATION_SUMMARIES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ValuationReconciliationSummaryListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("VALUATION_RECONCILIATION_SUMMARY", Order = 0)]
        public List<VALUATION_RECONCILIATION_SUMMARY> ValuationReconciliationSummaryList { get; set; } = new List<VALUATION_RECONCILIATION_SUMMARY>();
    
        [XmlIgnore]
        public bool ValuationReconciliationSummaryListSpecified
        {
            get { return this.ValuationReconciliationSummaryList != null && this.ValuationReconciliationSummaryList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public VALUATION_RECONCILIATION_SUMMARIES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
