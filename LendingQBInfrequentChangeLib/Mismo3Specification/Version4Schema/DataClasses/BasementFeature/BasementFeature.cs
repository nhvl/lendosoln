namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class BASEMENT_FEATURE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BasementFeatureDescriptionSpecified
                    || this.BasementFeatureTypeSpecified
                    || this.BasementFeatureTypeOtherDescriptionSpecified
                    || this.ConditionRatingTypeSpecified
                    || this.MaterialDescriptionSpecified
                    || this.QualityRatingDescriptionSpecified
                    || this.QualityRatingTypeSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("BasementFeatureDescription", Order = 0)]
        public MISMOString BasementFeatureDescription { get; set; }
    
        [XmlIgnore]
        public bool BasementFeatureDescriptionSpecified
        {
            get { return this.BasementFeatureDescription != null; }
            set { }
        }
    
        [XmlElement("BasementFeatureType", Order = 1)]
        public MISMOEnum<BasementFeatureBase> BasementFeatureType { get; set; }
    
        [XmlIgnore]
        public bool BasementFeatureTypeSpecified
        {
            get { return this.BasementFeatureType != null; }
            set { }
        }
    
        [XmlElement("BasementFeatureTypeOtherDescription", Order = 2)]
        public MISMOString BasementFeatureTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool BasementFeatureTypeOtherDescriptionSpecified
        {
            get { return this.BasementFeatureTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("ConditionRatingType", Order = 3)]
        public MISMOEnum<ConditionRatingBase> ConditionRatingType { get; set; }
    
        [XmlIgnore]
        public bool ConditionRatingTypeSpecified
        {
            get { return this.ConditionRatingType != null; }
            set { }
        }
    
        [XmlElement("MaterialDescription", Order = 4)]
        public MISMOString MaterialDescription { get; set; }
    
        [XmlIgnore]
        public bool MaterialDescriptionSpecified
        {
            get { return this.MaterialDescription != null; }
            set { }
        }
    
        [XmlElement("QualityRatingDescription", Order = 5)]
        public MISMOString QualityRatingDescription { get; set; }
    
        [XmlIgnore]
        public bool QualityRatingDescriptionSpecified
        {
            get { return this.QualityRatingDescription != null; }
            set { }
        }
    
        [XmlElement("QualityRatingType", Order = 6)]
        public MISMOEnum<QualityRatingBase> QualityRatingType { get; set; }
    
        [XmlIgnore]
        public bool QualityRatingTypeSpecified
        {
            get { return this.QualityRatingType != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 7)]
        public BASEMENT_FEATURE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
