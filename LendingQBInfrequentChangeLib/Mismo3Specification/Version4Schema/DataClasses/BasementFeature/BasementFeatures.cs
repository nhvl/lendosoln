namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class BASEMENT_FEATURES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BasementFeatureListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("BASEMENT_FEATURE", Order = 0)]
        public List<BASEMENT_FEATURE> BasementFeatureList { get; set; } = new List<BASEMENT_FEATURE>();
    
        [XmlIgnore]
        public bool BasementFeatureListSpecified
        {
            get { return this.BasementFeatureList != null && this.BasementFeatureList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public BASEMENT_FEATURES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
