namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class INVESTOR_LOAN_INFORMATION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BaseGuarantyFeePercentSpecified
                    || this.ConcurrentServicingTransferIndicatorSpecified
                    || this.ExceptionInterestAdjustmentDayMethodTypeSpecified
                    || this.ExcessServicingFeeRatePercentSpecified
                    || this.ExecutionTypeSpecified
                    || this.ExecutionTypeOtherDescriptionSpecified
                    || this.FREPrimaryMortgageMarketSurveyPublishedDateSpecified
                    || this.FREPrimaryMortgageMarketSurveyPublishedRatePercentSpecified
                    || this.FREPrimaryMortgageMarketSurveyRoundedRatePercentSpecified
                    || this.FundingInterestAdjustmentDayMethodTypeSpecified
                    || this.GuarantyFeeAddOnIndicatorSpecified
                    || this.GuarantyFeeAfterAlternatePaymentMethodPercentSpecified
                    || this.GuarantyFeePercentSpecified
                    || this.InterestVarianceToleranceMaximumAmountSpecified
                    || this.InvestorCollateralProgramIdentifierSpecified
                    || this.InvestorCommitmentTakeoutPricePercentSpecified
                    || this.InvestorCreditPolicyEffectiveDateSpecified
                    || this.InvestorDealIdentifierSpecified
                    || this.InvestorGroupingIdentifierSpecified
                    || this.InvestorGuarantyFeeAfterBuyupBuydownPercentSpecified
                    || this.InvestorHeldCollateralPercentSpecified
                    || this.InvestorInstitutionIdentifierSpecified
                    || this.InvestorLoanCommitmentExpirationDateSpecified
                    || this.InvestorLoanEarlyPayoffPenaltyWaiverIndicatorSpecified
                    || this.InvestorLoanEffectiveDateSpecified
                    || this.InvestorLoanPartialPrepaymentPenaltyWaiverIndicatorSpecified
                    || this.InvestorMasterCommitmentIdentifierSpecified
                    || this.InvestorOwnershipPercentSpecified
                    || this.InvestorOwnershipTypeSpecified
                    || this.InvestorProductPlanIdentifierSpecified
                    || this.InvestorRemittanceDaySpecified
                    || this.InvestorRemittanceDaysCountSpecified
                    || this.InvestorRemittanceIdentifierSpecified
                    || this.InvestorRemittanceTypeSpecified
                    || this.InvestorRemittanceTypeOtherDescriptionSpecified
                    || this.InvestorReportingMethodTypeSpecified
                    || this.InvestorRequiredMarginPercentSpecified
                    || this.InvestorServicingFeeAmountSpecified
                    || this.InvestorServicingFeeRatePercentSpecified
                    || this.LastReportedToInvestorPrincipalBalanceAmountSpecified
                    || this.LateDeliveryGraceDaysCountSpecified
                    || this.LenderTargetFundingDateSpecified
                    || this.LoanAcquisitionScheduledUPBAmountSpecified
                    || this.LoanBuyupBuydownBasisPointNumberSpecified
                    || this.LoanBuyupBuydownTypeSpecified
                    || this.LoanBuyupBuydownTypeOtherDescriptionSpecified
                    || this.LoanDefaultedReportingFrequencyTypeSpecified
                    || this.LoanDefaultLossPartyTypeSpecified
                    || this.LoanDelinquencyAdvanceDaysCountSpecified
                    || this.LoanFundingAdvancedAmountSpecified
                    || this.LoanPriorOwnerNameSpecified
                    || this.LoanPurchaseDiscountAmountSpecified
                    || this.LoanPurchasePremiumAmountSpecified
                    || this.LoanServicingFeeBasisPointNumberSpecified
                    || this.LoanServicingIndicatorSpecified
                    || this.LoanServicingRightsPurchaseOnlyIndicatorSpecified
                    || this.LoanYieldSpreadPremiumAmountSpecified
                    || this.LoanYieldSpreadPremiumRatePercentSpecified
                    || this.MICoverageServicingFeeRatePercentSpecified
                    || this.NonStandardServicingFeeRetainedByServicerAmountSpecified
                    || this.PassThroughCalculationMethodTypeSpecified
                    || this.PassThroughCeilingRatePercentSpecified
                    || this.PassThroughRatePercentSpecified
                    || this.PrincipalVarianceToleranceMaximumAmountSpecified
                    || this.RelatedInvestorLoanIdentifierSpecified
                    || this.RelatedLoanInvestorTypeSpecified
                    || this.RelatedLoanInvestorTypeOtherDescriptionSpecified
                    || this.RemoveFromTransferIndicatorSpecified
                    || this.REOMarketingPartyTypeSpecified
                    || this.REOMarketingPartyTypeOtherDescriptionSpecified
                    || this.ServicingFeeMinimumRatePercentSpecified
                    || this.ThirdPartyLoanAcquisitionPriceAmountSpecified
                    || this.UPBVarianceToleranceMaximumAmountSpecified
                    || this.WarehouseAdvanceAmountSpecified
                    || this.WarehouseCommitmentSubcategoryIdentifierSpecified
                    || this.WholeLoanInterestDueAmountSpecified
                    || this.WholeLoanPrincipalAmountSpecified
                    || this.WholeLoanSaleDateSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("BaseGuarantyFeePercent", Order = 0)]
        public MISMOPercent BaseGuarantyFeePercent { get; set; }
    
        [XmlIgnore]
        public bool BaseGuarantyFeePercentSpecified
        {
            get { return this.BaseGuarantyFeePercent != null; }
            set { }
        }
    
        [XmlElement("ConcurrentServicingTransferIndicator", Order = 1)]
        public MISMOIndicator ConcurrentServicingTransferIndicator { get; set; }
    
        [XmlIgnore]
        public bool ConcurrentServicingTransferIndicatorSpecified
        {
            get { return this.ConcurrentServicingTransferIndicator != null; }
            set { }
        }
    
        [XmlElement("ExceptionInterestAdjustmentDayMethodType", Order = 2)]
        public MISMOEnum<ExceptionInterestAdjustmentDayMethodBase> ExceptionInterestAdjustmentDayMethodType { get; set; }
    
        [XmlIgnore]
        public bool ExceptionInterestAdjustmentDayMethodTypeSpecified
        {
            get { return this.ExceptionInterestAdjustmentDayMethodType != null; }
            set { }
        }
    
        [XmlElement("ExcessServicingFeeRatePercent", Order = 3)]
        public MISMOPercent ExcessServicingFeeRatePercent { get; set; }
    
        [XmlIgnore]
        public bool ExcessServicingFeeRatePercentSpecified
        {
            get { return this.ExcessServicingFeeRatePercent != null; }
            set { }
        }
    
        [XmlElement("ExecutionType", Order = 4)]
        public MISMOEnum<ExecutionBase> ExecutionType { get; set; }
    
        [XmlIgnore]
        public bool ExecutionTypeSpecified
        {
            get { return this.ExecutionType != null; }
            set { }
        }
    
        [XmlElement("ExecutionTypeOtherDescription", Order = 5)]
        public MISMOString ExecutionTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ExecutionTypeOtherDescriptionSpecified
        {
            get { return this.ExecutionTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("FREPrimaryMortgageMarketSurveyPublishedDate", Order = 6)]
        public MISMODate FREPrimaryMortgageMarketSurveyPublishedDate { get; set; }
    
        [XmlIgnore]
        public bool FREPrimaryMortgageMarketSurveyPublishedDateSpecified
        {
            get { return this.FREPrimaryMortgageMarketSurveyPublishedDate != null; }
            set { }
        }
    
        [XmlElement("FREPrimaryMortgageMarketSurveyPublishedRatePercent", Order = 7)]
        public MISMOPercent FREPrimaryMortgageMarketSurveyPublishedRatePercent { get; set; }
    
        [XmlIgnore]
        public bool FREPrimaryMortgageMarketSurveyPublishedRatePercentSpecified
        {
            get { return this.FREPrimaryMortgageMarketSurveyPublishedRatePercent != null; }
            set { }
        }
    
        [XmlElement("FREPrimaryMortgageMarketSurveyRoundedRatePercent", Order = 8)]
        public MISMOPercent FREPrimaryMortgageMarketSurveyRoundedRatePercent { get; set; }
    
        [XmlIgnore]
        public bool FREPrimaryMortgageMarketSurveyRoundedRatePercentSpecified
        {
            get { return this.FREPrimaryMortgageMarketSurveyRoundedRatePercent != null; }
            set { }
        }
    
        [XmlElement("FundingInterestAdjustmentDayMethodType", Order = 9)]
        public MISMOEnum<FundingInterestAdjustmentDayMethodBase> FundingInterestAdjustmentDayMethodType { get; set; }
    
        [XmlIgnore]
        public bool FundingInterestAdjustmentDayMethodTypeSpecified
        {
            get { return this.FundingInterestAdjustmentDayMethodType != null; }
            set { }
        }
    
        [XmlElement("GuarantyFeeAddOnIndicator", Order = 10)]
        public MISMOIndicator GuarantyFeeAddOnIndicator { get; set; }
    
        [XmlIgnore]
        public bool GuarantyFeeAddOnIndicatorSpecified
        {
            get { return this.GuarantyFeeAddOnIndicator != null; }
            set { }
        }
    
        [XmlElement("GuarantyFeeAfterAlternatePaymentMethodPercent", Order = 11)]
        public MISMOPercent GuarantyFeeAfterAlternatePaymentMethodPercent { get; set; }
    
        [XmlIgnore]
        public bool GuarantyFeeAfterAlternatePaymentMethodPercentSpecified
        {
            get { return this.GuarantyFeeAfterAlternatePaymentMethodPercent != null; }
            set { }
        }
    
        [XmlElement("GuarantyFeePercent", Order = 12)]
        public MISMOPercent GuarantyFeePercent { get; set; }
    
        [XmlIgnore]
        public bool GuarantyFeePercentSpecified
        {
            get { return this.GuarantyFeePercent != null; }
            set { }
        }
    
        [XmlElement("InterestVarianceToleranceMaximumAmount", Order = 13)]
        public MISMOAmount InterestVarianceToleranceMaximumAmount { get; set; }
    
        [XmlIgnore]
        public bool InterestVarianceToleranceMaximumAmountSpecified
        {
            get { return this.InterestVarianceToleranceMaximumAmount != null; }
            set { }
        }
    
        [XmlElement("InvestorCollateralProgramIdentifier", Order = 14)]
        public MISMOIdentifier InvestorCollateralProgramIdentifier { get; set; }
    
        [XmlIgnore]
        public bool InvestorCollateralProgramIdentifierSpecified
        {
            get { return this.InvestorCollateralProgramIdentifier != null; }
            set { }
        }
    
        [XmlElement("InvestorCommitmentTakeoutPricePercent", Order = 15)]
        public MISMOPercent InvestorCommitmentTakeoutPricePercent { get; set; }
    
        [XmlIgnore]
        public bool InvestorCommitmentTakeoutPricePercentSpecified
        {
            get { return this.InvestorCommitmentTakeoutPricePercent != null; }
            set { }
        }
    
        [XmlElement("InvestorCreditPolicyEffectiveDate", Order = 16)]
        public MISMODate InvestorCreditPolicyEffectiveDate { get; set; }
    
        [XmlIgnore]
        public bool InvestorCreditPolicyEffectiveDateSpecified
        {
            get { return this.InvestorCreditPolicyEffectiveDate != null; }
            set { }
        }
    
        [XmlElement("InvestorDealIdentifier", Order = 17)]
        public MISMOIdentifier InvestorDealIdentifier { get; set; }
    
        [XmlIgnore]
        public bool InvestorDealIdentifierSpecified
        {
            get { return this.InvestorDealIdentifier != null; }
            set { }
        }
    
        [XmlElement("InvestorGroupingIdentifier", Order = 18)]
        public MISMOIdentifier InvestorGroupingIdentifier { get; set; }
    
        [XmlIgnore]
        public bool InvestorGroupingIdentifierSpecified
        {
            get { return this.InvestorGroupingIdentifier != null; }
            set { }
        }
    
        [XmlElement("InvestorGuarantyFeeAfterBuyupBuydownPercent", Order = 19)]
        public MISMOPercent InvestorGuarantyFeeAfterBuyupBuydownPercent { get; set; }
    
        [XmlIgnore]
        public bool InvestorGuarantyFeeAfterBuyupBuydownPercentSpecified
        {
            get { return this.InvestorGuarantyFeeAfterBuyupBuydownPercent != null; }
            set { }
        }
    
        [XmlElement("InvestorHeldCollateralPercent", Order = 20)]
        public MISMOPercent InvestorHeldCollateralPercent { get; set; }
    
        [XmlIgnore]
        public bool InvestorHeldCollateralPercentSpecified
        {
            get { return this.InvestorHeldCollateralPercent != null; }
            set { }
        }
    
        [XmlElement("InvestorInstitutionIdentifier", Order = 21)]
        public MISMOIdentifier InvestorInstitutionIdentifier { get; set; }
    
        [XmlIgnore]
        public bool InvestorInstitutionIdentifierSpecified
        {
            get { return this.InvestorInstitutionIdentifier != null; }
            set { }
        }
    
        [XmlElement("InvestorLoanCommitmentExpirationDate", Order = 22)]
        public MISMODate InvestorLoanCommitmentExpirationDate { get; set; }
    
        [XmlIgnore]
        public bool InvestorLoanCommitmentExpirationDateSpecified
        {
            get { return this.InvestorLoanCommitmentExpirationDate != null; }
            set { }
        }
    
        [XmlElement("InvestorLoanEarlyPayoffPenaltyWaiverIndicator", Order = 23)]
        public MISMOIndicator InvestorLoanEarlyPayoffPenaltyWaiverIndicator { get; set; }
    
        [XmlIgnore]
        public bool InvestorLoanEarlyPayoffPenaltyWaiverIndicatorSpecified
        {
            get { return this.InvestorLoanEarlyPayoffPenaltyWaiverIndicator != null; }
            set { }
        }
    
        [XmlElement("InvestorLoanEffectiveDate", Order = 24)]
        public MISMODate InvestorLoanEffectiveDate { get; set; }
    
        [XmlIgnore]
        public bool InvestorLoanEffectiveDateSpecified
        {
            get { return this.InvestorLoanEffectiveDate != null; }
            set { }
        }
    
        [XmlElement("InvestorLoanPartialPrepaymentPenaltyWaiverIndicator", Order = 25)]
        public MISMOIndicator InvestorLoanPartialPrepaymentPenaltyWaiverIndicator { get; set; }
    
        [XmlIgnore]
        public bool InvestorLoanPartialPrepaymentPenaltyWaiverIndicatorSpecified
        {
            get { return this.InvestorLoanPartialPrepaymentPenaltyWaiverIndicator != null; }
            set { }
        }
    
        [XmlElement("InvestorMasterCommitmentIdentifier", Order = 26)]
        public MISMOIdentifier InvestorMasterCommitmentIdentifier { get; set; }
    
        [XmlIgnore]
        public bool InvestorMasterCommitmentIdentifierSpecified
        {
            get { return this.InvestorMasterCommitmentIdentifier != null; }
            set { }
        }
    
        [XmlElement("InvestorOwnershipPercent", Order = 27)]
        public MISMOPercent InvestorOwnershipPercent { get; set; }
    
        [XmlIgnore]
        public bool InvestorOwnershipPercentSpecified
        {
            get { return this.InvestorOwnershipPercent != null; }
            set { }
        }
    
        [XmlElement("InvestorOwnershipType", Order = 28)]
        public MISMOEnum<InvestorOwnershipBase> InvestorOwnershipType { get; set; }
    
        [XmlIgnore]
        public bool InvestorOwnershipTypeSpecified
        {
            get { return this.InvestorOwnershipType != null; }
            set { }
        }
    
        [XmlElement("InvestorProductPlanIdentifier", Order = 29)]
        public MISMOIdentifier InvestorProductPlanIdentifier { get; set; }
    
        [XmlIgnore]
        public bool InvestorProductPlanIdentifierSpecified
        {
            get { return this.InvestorProductPlanIdentifier != null; }
            set { }
        }
    
        [XmlElement("InvestorRemittanceDay", Order = 30)]
        public MISMODay InvestorRemittanceDay { get; set; }
    
        [XmlIgnore]
        public bool InvestorRemittanceDaySpecified
        {
            get { return this.InvestorRemittanceDay != null; }
            set { }
        }
    
        [XmlElement("InvestorRemittanceDaysCount", Order = 31)]
        public MISMOCount InvestorRemittanceDaysCount { get; set; }
    
        [XmlIgnore]
        public bool InvestorRemittanceDaysCountSpecified
        {
            get { return this.InvestorRemittanceDaysCount != null; }
            set { }
        }
    
        [XmlElement("InvestorRemittanceIdentifier", Order = 32)]
        public MISMOIdentifier InvestorRemittanceIdentifier { get; set; }
    
        [XmlIgnore]
        public bool InvestorRemittanceIdentifierSpecified
        {
            get { return this.InvestorRemittanceIdentifier != null; }
            set { }
        }
    
        [XmlElement("InvestorRemittanceType", Order = 33)]
        public MISMOEnum<InvestorRemittanceBase> InvestorRemittanceType { get; set; }
    
        [XmlIgnore]
        public bool InvestorRemittanceTypeSpecified
        {
            get { return this.InvestorRemittanceType != null; }
            set { }
        }
    
        [XmlElement("InvestorRemittanceTypeOtherDescription", Order = 34)]
        public MISMOString InvestorRemittanceTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool InvestorRemittanceTypeOtherDescriptionSpecified
        {
            get { return this.InvestorRemittanceTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("InvestorReportingMethodType", Order = 35)]
        public MISMOEnum<InvestorReportingMethodBase> InvestorReportingMethodType { get; set; }
    
        [XmlIgnore]
        public bool InvestorReportingMethodTypeSpecified
        {
            get { return this.InvestorReportingMethodType != null; }
            set { }
        }
    
        [XmlElement("InvestorRequiredMarginPercent", Order = 36)]
        public MISMOPercent InvestorRequiredMarginPercent { get; set; }
    
        [XmlIgnore]
        public bool InvestorRequiredMarginPercentSpecified
        {
            get { return this.InvestorRequiredMarginPercent != null; }
            set { }
        }
    
        [XmlElement("InvestorServicingFeeAmount", Order = 37)]
        public MISMOAmount InvestorServicingFeeAmount { get; set; }
    
        [XmlIgnore]
        public bool InvestorServicingFeeAmountSpecified
        {
            get { return this.InvestorServicingFeeAmount != null; }
            set { }
        }
    
        [XmlElement("InvestorServicingFeeRatePercent", Order = 38)]
        public MISMOPercent InvestorServicingFeeRatePercent { get; set; }
    
        [XmlIgnore]
        public bool InvestorServicingFeeRatePercentSpecified
        {
            get { return this.InvestorServicingFeeRatePercent != null; }
            set { }
        }
    
        [XmlElement("LastReportedToInvestorPrincipalBalanceAmount", Order = 39)]
        public MISMOAmount LastReportedToInvestorPrincipalBalanceAmount { get; set; }
    
        [XmlIgnore]
        public bool LastReportedToInvestorPrincipalBalanceAmountSpecified
        {
            get { return this.LastReportedToInvestorPrincipalBalanceAmount != null; }
            set { }
        }
    
        [XmlElement("LateDeliveryGraceDaysCount", Order = 40)]
        public MISMOCount LateDeliveryGraceDaysCount { get; set; }
    
        [XmlIgnore]
        public bool LateDeliveryGraceDaysCountSpecified
        {
            get { return this.LateDeliveryGraceDaysCount != null; }
            set { }
        }
    
        [XmlElement("LenderTargetFundingDate", Order = 41)]
        public MISMODate LenderTargetFundingDate { get; set; }
    
        [XmlIgnore]
        public bool LenderTargetFundingDateSpecified
        {
            get { return this.LenderTargetFundingDate != null; }
            set { }
        }
    
        [XmlElement("LoanAcquisitionScheduledUPBAmount", Order = 42)]
        public MISMOAmount LoanAcquisitionScheduledUPBAmount { get; set; }
    
        [XmlIgnore]
        public bool LoanAcquisitionScheduledUPBAmountSpecified
        {
            get { return this.LoanAcquisitionScheduledUPBAmount != null; }
            set { }
        }
    
        [XmlElement("LoanBuyupBuydownBasisPointNumber", Order = 43)]
        public MISMONumeric LoanBuyupBuydownBasisPointNumber { get; set; }
    
        [XmlIgnore]
        public bool LoanBuyupBuydownBasisPointNumberSpecified
        {
            get { return this.LoanBuyupBuydownBasisPointNumber != null; }
            set { }
        }
    
        [XmlElement("LoanBuyupBuydownType", Order = 44)]
        public MISMOEnum<LoanBuyupBuydownBase> LoanBuyupBuydownType { get; set; }
    
        [XmlIgnore]
        public bool LoanBuyupBuydownTypeSpecified
        {
            get { return this.LoanBuyupBuydownType != null; }
            set { }
        }
    
        [XmlElement("LoanBuyupBuydownTypeOtherDescription", Order = 45)]
        public MISMOString LoanBuyupBuydownTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool LoanBuyupBuydownTypeOtherDescriptionSpecified
        {
            get { return this.LoanBuyupBuydownTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("LoanDefaultedReportingFrequencyType", Order = 46)]
        public MISMOEnum<LoanDefaultedReportingFrequencyBase> LoanDefaultedReportingFrequencyType { get; set; }
    
        [XmlIgnore]
        public bool LoanDefaultedReportingFrequencyTypeSpecified
        {
            get { return this.LoanDefaultedReportingFrequencyType != null; }
            set { }
        }
    
        [XmlElement("LoanDefaultLossPartyType", Order = 47)]
        public MISMOEnum<LoanDefaultLossPartyBase> LoanDefaultLossPartyType { get; set; }
    
        [XmlIgnore]
        public bool LoanDefaultLossPartyTypeSpecified
        {
            get { return this.LoanDefaultLossPartyType != null; }
            set { }
        }
    
        [XmlElement("LoanDelinquencyAdvanceDaysCount", Order = 48)]
        public MISMOCount LoanDelinquencyAdvanceDaysCount { get; set; }
    
        [XmlIgnore]
        public bool LoanDelinquencyAdvanceDaysCountSpecified
        {
            get { return this.LoanDelinquencyAdvanceDaysCount != null; }
            set { }
        }
    
        [XmlElement("LoanFundingAdvancedAmount", Order = 49)]
        public MISMOAmount LoanFundingAdvancedAmount { get; set; }
    
        [XmlIgnore]
        public bool LoanFundingAdvancedAmountSpecified
        {
            get { return this.LoanFundingAdvancedAmount != null; }
            set { }
        }
    
        [XmlElement("LoanPriorOwnerName", Order = 50)]
        public MISMOString LoanPriorOwnerName { get; set; }
    
        [XmlIgnore]
        public bool LoanPriorOwnerNameSpecified
        {
            get { return this.LoanPriorOwnerName != null; }
            set { }
        }
    
        [XmlElement("LoanPurchaseDiscountAmount", Order = 51)]
        public MISMOAmount LoanPurchaseDiscountAmount { get; set; }
    
        [XmlIgnore]
        public bool LoanPurchaseDiscountAmountSpecified
        {
            get { return this.LoanPurchaseDiscountAmount != null; }
            set { }
        }
    
        [XmlElement("LoanPurchasePremiumAmount", Order = 52)]
        public MISMOAmount LoanPurchasePremiumAmount { get; set; }
    
        [XmlIgnore]
        public bool LoanPurchasePremiumAmountSpecified
        {
            get { return this.LoanPurchasePremiumAmount != null; }
            set { }
        }
    
        [XmlElement("LoanServicingFeeBasisPointNumber", Order = 53)]
        public MISMONumeric LoanServicingFeeBasisPointNumber { get; set; }
    
        [XmlIgnore]
        public bool LoanServicingFeeBasisPointNumberSpecified
        {
            get { return this.LoanServicingFeeBasisPointNumber != null; }
            set { }
        }
    
        [XmlElement("LoanServicingIndicator", Order = 54)]
        public MISMOIndicator LoanServicingIndicator { get; set; }
    
        [XmlIgnore]
        public bool LoanServicingIndicatorSpecified
        {
            get { return this.LoanServicingIndicator != null; }
            set { }
        }
    
        [XmlElement("LoanServicingRightsPurchaseOnlyIndicator", Order = 55)]
        public MISMOIndicator LoanServicingRightsPurchaseOnlyIndicator { get; set; }
    
        [XmlIgnore]
        public bool LoanServicingRightsPurchaseOnlyIndicatorSpecified
        {
            get { return this.LoanServicingRightsPurchaseOnlyIndicator != null; }
            set { }
        }
    
        [XmlElement("LoanYieldSpreadPremiumAmount", Order = 56)]
        public MISMOAmount LoanYieldSpreadPremiumAmount { get; set; }
    
        [XmlIgnore]
        public bool LoanYieldSpreadPremiumAmountSpecified
        {
            get { return this.LoanYieldSpreadPremiumAmount != null; }
            set { }
        }
    
        [XmlElement("LoanYieldSpreadPremiumRatePercent", Order = 57)]
        public MISMOPercent LoanYieldSpreadPremiumRatePercent { get; set; }
    
        [XmlIgnore]
        public bool LoanYieldSpreadPremiumRatePercentSpecified
        {
            get { return this.LoanYieldSpreadPremiumRatePercent != null; }
            set { }
        }
    
        [XmlElement("MICoverageServicingFeeRatePercent", Order = 58)]
        public MISMOPercent MICoverageServicingFeeRatePercent { get; set; }
    
        [XmlIgnore]
        public bool MICoverageServicingFeeRatePercentSpecified
        {
            get { return this.MICoverageServicingFeeRatePercent != null; }
            set { }
        }
    
        [XmlElement("NonStandardServicingFeeRetainedByServicerAmount", Order = 59)]
        public MISMOAmount NonStandardServicingFeeRetainedByServicerAmount { get; set; }
    
        [XmlIgnore]
        public bool NonStandardServicingFeeRetainedByServicerAmountSpecified
        {
            get { return this.NonStandardServicingFeeRetainedByServicerAmount != null; }
            set { }
        }
    
        [XmlElement("PassThroughCalculationMethodType", Order = 60)]
        public MISMOEnum<PassThroughCalculationMethodBase> PassThroughCalculationMethodType { get; set; }
    
        [XmlIgnore]
        public bool PassThroughCalculationMethodTypeSpecified
        {
            get { return this.PassThroughCalculationMethodType != null; }
            set { }
        }
    
        [XmlElement("PassThroughCeilingRatePercent", Order = 61)]
        public MISMOPercent PassThroughCeilingRatePercent { get; set; }
    
        [XmlIgnore]
        public bool PassThroughCeilingRatePercentSpecified
        {
            get { return this.PassThroughCeilingRatePercent != null; }
            set { }
        }
    
        [XmlElement("PassThroughRatePercent", Order = 62)]
        public MISMOPercent PassThroughRatePercent { get; set; }
    
        [XmlIgnore]
        public bool PassThroughRatePercentSpecified
        {
            get { return this.PassThroughRatePercent != null; }
            set { }
        }
    
        [XmlElement("PrincipalVarianceToleranceMaximumAmount", Order = 63)]
        public MISMOAmount PrincipalVarianceToleranceMaximumAmount { get; set; }
    
        [XmlIgnore]
        public bool PrincipalVarianceToleranceMaximumAmountSpecified
        {
            get { return this.PrincipalVarianceToleranceMaximumAmount != null; }
            set { }
        }
    
        [XmlElement("RelatedInvestorLoanIdentifier", Order = 64)]
        public MISMOIdentifier RelatedInvestorLoanIdentifier { get; set; }
    
        [XmlIgnore]
        public bool RelatedInvestorLoanIdentifierSpecified
        {
            get { return this.RelatedInvestorLoanIdentifier != null; }
            set { }
        }
    
        [XmlElement("RelatedLoanInvestorType", Order = 65)]
        public MISMOEnum<RelatedLoanInvestorBase> RelatedLoanInvestorType { get; set; }
    
        [XmlIgnore]
        public bool RelatedLoanInvestorTypeSpecified
        {
            get { return this.RelatedLoanInvestorType != null; }
            set { }
        }
    
        [XmlElement("RelatedLoanInvestorTypeOtherDescription", Order = 66)]
        public MISMOString RelatedLoanInvestorTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool RelatedLoanInvestorTypeOtherDescriptionSpecified
        {
            get { return this.RelatedLoanInvestorTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("RemoveFromTransferIndicator", Order = 67)]
        public MISMOIndicator RemoveFromTransferIndicator { get; set; }
    
        [XmlIgnore]
        public bool RemoveFromTransferIndicatorSpecified
        {
            get { return this.RemoveFromTransferIndicator != null; }
            set { }
        }
    
        [XmlElement("REOMarketingPartyType", Order = 68)]
        public MISMOEnum<REOMarketingPartyBase> REOMarketingPartyType { get; set; }
    
        [XmlIgnore]
        public bool REOMarketingPartyTypeSpecified
        {
            get { return this.REOMarketingPartyType != null; }
            set { }
        }
    
        [XmlElement("REOMarketingPartyTypeOtherDescription", Order = 69)]
        public MISMOString REOMarketingPartyTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool REOMarketingPartyTypeOtherDescriptionSpecified
        {
            get { return this.REOMarketingPartyTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("ServicingFeeMinimumRatePercent", Order = 70)]
        public MISMOPercent ServicingFeeMinimumRatePercent { get; set; }
    
        [XmlIgnore]
        public bool ServicingFeeMinimumRatePercentSpecified
        {
            get { return this.ServicingFeeMinimumRatePercent != null; }
            set { }
        }
    
        [XmlElement("ThirdPartyLoanAcquisitionPriceAmount", Order = 71)]
        public MISMOAmount ThirdPartyLoanAcquisitionPriceAmount { get; set; }
    
        [XmlIgnore]
        public bool ThirdPartyLoanAcquisitionPriceAmountSpecified
        {
            get { return this.ThirdPartyLoanAcquisitionPriceAmount != null; }
            set { }
        }
    
        [XmlElement("UPBVarianceToleranceMaximumAmount", Order = 72)]
        public MISMOAmount UPBVarianceToleranceMaximumAmount { get; set; }
    
        [XmlIgnore]
        public bool UPBVarianceToleranceMaximumAmountSpecified
        {
            get { return this.UPBVarianceToleranceMaximumAmount != null; }
            set { }
        }
    
        [XmlElement("WarehouseAdvanceAmount", Order = 73)]
        public MISMOAmount WarehouseAdvanceAmount { get; set; }
    
        [XmlIgnore]
        public bool WarehouseAdvanceAmountSpecified
        {
            get { return this.WarehouseAdvanceAmount != null; }
            set { }
        }
    
        [XmlElement("WarehouseCommitmentSubcategoryIdentifier", Order = 74)]
        public MISMOIdentifier WarehouseCommitmentSubcategoryIdentifier { get; set; }
    
        [XmlIgnore]
        public bool WarehouseCommitmentSubcategoryIdentifierSpecified
        {
            get { return this.WarehouseCommitmentSubcategoryIdentifier != null; }
            set { }
        }
    
        [XmlElement("WholeLoanInterestDueAmount", Order = 75)]
        public MISMOAmount WholeLoanInterestDueAmount { get; set; }
    
        [XmlIgnore]
        public bool WholeLoanInterestDueAmountSpecified
        {
            get { return this.WholeLoanInterestDueAmount != null; }
            set { }
        }
    
        [XmlElement("WholeLoanPrincipalAmount", Order = 76)]
        public MISMOAmount WholeLoanPrincipalAmount { get; set; }
    
        [XmlIgnore]
        public bool WholeLoanPrincipalAmountSpecified
        {
            get { return this.WholeLoanPrincipalAmount != null; }
            set { }
        }
    
        [XmlElement("WholeLoanSaleDate", Order = 77)]
        public MISMODate WholeLoanSaleDate { get; set; }
    
        [XmlIgnore]
        public bool WholeLoanSaleDateSpecified
        {
            get { return this.WholeLoanSaleDate != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 78)]
        public INVESTOR_LOAN_INFORMATION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
