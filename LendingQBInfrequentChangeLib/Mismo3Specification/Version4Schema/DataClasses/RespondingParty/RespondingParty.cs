namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class RESPONDING_PARTY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.RespondingPartyTransactionIdentifierSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("RespondingPartyTransactionIdentifier", Order = 0)]
        public MISMOIdentifier RespondingPartyTransactionIdentifier { get; set; }
    
        [XmlIgnore]
        public bool RespondingPartyTransactionIdentifierSpecified
        {
            get { return this.RespondingPartyTransactionIdentifier != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public RESPONDING_PARTY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
