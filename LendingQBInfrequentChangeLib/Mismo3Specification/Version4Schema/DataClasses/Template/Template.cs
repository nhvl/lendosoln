namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Xml.Serialization;

    public class TEMPLATE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                if (Mismo3Utilities.ExceedsThreshold(1,
                    new List<bool> { this.TemplateFilesSpecified, this.TemplatePagesSpecified }))
                {
                    throw new System.Xml.Schema.XmlSchemaValidationException(
                        Mismo3Utilities.GetXSDChoiceExceptionMessage(
                        "TEMPLATE",
                        new List<string> { "TEMPLATE_FILES", "TEMPLATE_PAGES" }));
                }

                return this.ExtensionSpecified
                    || this.TemplateFilesSpecified
                    || this.TemplatePagesSpecified;
            }
        }
    
        [XmlElement("TEMPLATE_FILES", Order = 0)]
        public TEMPLATE_FILES TemplateFiles { get; set; }
    
        [XmlIgnore]
        public bool TemplateFilesSpecified
        {
            get { return this.TemplateFiles != null && this.TemplateFiles.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("TEMPLATE_PAGES", Order = 1)]
        public TEMPLATE_PAGES TemplatePages { get; set; }
    
        [XmlIgnore]
        public bool TemplatePagesSpecified
        {
            get { return this.TemplatePages != null && this.TemplatePages.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public TEMPLATE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
