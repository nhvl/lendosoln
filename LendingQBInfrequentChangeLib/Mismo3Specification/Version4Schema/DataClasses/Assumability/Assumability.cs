namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class ASSUMABILITY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AssumabilityOccurrencesSpecified
                    || this.AssumabilityRuleSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ASSUMABILITY_OCCURRENCES", Order = 0)]
        public ASSUMABILITY_OCCURRENCES AssumabilityOccurrences { get; set; }
    
        [XmlIgnore]
        public bool AssumabilityOccurrencesSpecified
        {
            get { return this.AssumabilityOccurrences != null && this.AssumabilityOccurrences.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("ASSUMABILITY_RULE", Order = 1)]
        public ASSUMABILITY_RULE AssumabilityRule { get; set; }
    
        [XmlIgnore]
        public bool AssumabilityRuleSpecified
        {
            get { return this.AssumabilityRule != null && this.AssumabilityRule.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public ASSUMABILITY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
