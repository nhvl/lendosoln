namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class INSPECTION_SERVICE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.InspectionServiceRequestSpecified
                    || this.InspectionServiceResponseSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("INSPECTION_SERVICE_REQUEST", Order = 0)]
        public INSPECTION_SERVICE_REQUEST InspectionServiceRequest { get; set; }
    
        [XmlIgnore]
        public bool InspectionServiceRequestSpecified
        {
            get { return this.InspectionServiceRequest != null && this.InspectionServiceRequest.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("INSPECTION_SERVICE_RESPONSE", Order = 1)]
        public INSPECTION_SERVICE_RESPONSE InspectionServiceResponse { get; set; }
    
        [XmlIgnore]
        public bool InspectionServiceResponseSpecified
        {
            get { return this.InspectionServiceResponse != null && this.InspectionServiceResponse.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public INSPECTION_SERVICE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
