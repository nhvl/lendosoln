namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class DELINQUENCY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DelinquencyDetailSpecified
                    || this.DelinquencyEventsSpecified
                    || this.DelinquencyHardshipsSpecified
                    || this.DelinquencyStatusesSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DELINQUENCY_DETAIL", Order = 0)]
        public DELINQUENCY_DETAIL DelinquencyDetail { get; set; }
    
        [XmlIgnore]
        public bool DelinquencyDetailSpecified
        {
            get { return this.DelinquencyDetail != null && this.DelinquencyDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("DELINQUENCY_EVENTS", Order = 1)]
        public DELINQUENCY_EVENTS DelinquencyEvents { get; set; }
    
        [XmlIgnore]
        public bool DelinquencyEventsSpecified
        {
            get { return this.DelinquencyEvents != null && this.DelinquencyEvents.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("DELINQUENCY_HARDSHIPS", Order = 2)]
        public DELINQUENCY_HARDSHIPS DelinquencyHardships { get; set; }
    
        [XmlIgnore]
        public bool DelinquencyHardshipsSpecified
        {
            get { return this.DelinquencyHardships != null && this.DelinquencyHardships.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("DELINQUENCY_STATUSES", Order = 3)]
        public DELINQUENCY_STATUSES DelinquencyStatuses { get; set; }
    
        [XmlIgnore]
        public bool DelinquencyStatusesSpecified
        {
            get { return this.DelinquencyStatuses != null && this.DelinquencyStatuses.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public DELINQUENCY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
