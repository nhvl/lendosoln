namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class DELINQUENCY_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BankruptcySuspenseBalanceAmountSpecified
                    || this.BorrowerSolicitationExpirationDateSpecified
                    || this.BorrowerVacancyReasonTypeSpecified
                    || this.BorrowerVacancyReasonTypeOtherDescriptionSpecified
                    || this.BreachLetterExpirationDateSpecified
                    || this.DelinquencyEffectiveDateSpecified
                    || this.DelinquencyReportingPropertyOverallConditionTypeSpecified
                    || this.DelinquencyReportingPropertyOverallConditionTypeOtherDescriptionSpecified
                    || this.EmailAttemptsCountSpecified
                    || this.FaceToFaceInterviewsCountSpecified
                    || this.FormLettersCountSpecified
                    || this.LienRecommendationTypeSpecified
                    || this.LienRecommendationTypeOtherDescriptionSpecified
                    || this.NumberOfCallAttemptsCountSpecified
                    || this.OutgoingCallMethodTypeSpecified
                    || this.PropertyCurrentOccupancyTypeSpecified
                    || this.RightPartyContactCountSpecified
                    || this.SubordinateLienHolderPaymentAmountSpecified
                    || this.TaxAdvancesOnAccountsNotEscrowedIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("BankruptcySuspenseBalanceAmount", Order = 0)]
        public MISMOAmount BankruptcySuspenseBalanceAmount { get; set; }
    
        [XmlIgnore]
        public bool BankruptcySuspenseBalanceAmountSpecified
        {
            get { return this.BankruptcySuspenseBalanceAmount != null; }
            set { }
        }
    
        [XmlElement("BorrowerSolicitationExpirationDate", Order = 1)]
        public MISMODate BorrowerSolicitationExpirationDate { get; set; }
    
        [XmlIgnore]
        public bool BorrowerSolicitationExpirationDateSpecified
        {
            get { return this.BorrowerSolicitationExpirationDate != null; }
            set { }
        }
    
        [XmlElement("BorrowerVacancyReasonType", Order = 2)]
        public MISMOEnum<BorrowerVacancyReasonBase> BorrowerVacancyReasonType { get; set; }
    
        [XmlIgnore]
        public bool BorrowerVacancyReasonTypeSpecified
        {
            get { return this.BorrowerVacancyReasonType != null; }
            set { }
        }
    
        [XmlElement("BorrowerVacancyReasonTypeOtherDescription", Order = 3)]
        public MISMOString BorrowerVacancyReasonTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool BorrowerVacancyReasonTypeOtherDescriptionSpecified
        {
            get { return this.BorrowerVacancyReasonTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("BreachLetterExpirationDate", Order = 4)]
        public MISMODate BreachLetterExpirationDate { get; set; }
    
        [XmlIgnore]
        public bool BreachLetterExpirationDateSpecified
        {
            get { return this.BreachLetterExpirationDate != null; }
            set { }
        }
    
        [XmlElement("DelinquencyEffectiveDate", Order = 5)]
        public MISMODate DelinquencyEffectiveDate { get; set; }
    
        [XmlIgnore]
        public bool DelinquencyEffectiveDateSpecified
        {
            get { return this.DelinquencyEffectiveDate != null; }
            set { }
        }
    
        [XmlElement("DelinquencyReportingPropertyOverallConditionType", Order = 6)]
        public MISMOEnum<DelinquencyReportingPropertyOverallConditionBase> DelinquencyReportingPropertyOverallConditionType { get; set; }
    
        [XmlIgnore]
        public bool DelinquencyReportingPropertyOverallConditionTypeSpecified
        {
            get { return this.DelinquencyReportingPropertyOverallConditionType != null; }
            set { }
        }
    
        [XmlElement("DelinquencyReportingPropertyOverallConditionTypeOtherDescription", Order = 7)]
        public MISMOString DelinquencyReportingPropertyOverallConditionTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool DelinquencyReportingPropertyOverallConditionTypeOtherDescriptionSpecified
        {
            get { return this.DelinquencyReportingPropertyOverallConditionTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EmailAttemptsCount", Order = 8)]
        public MISMOCount EmailAttemptsCount { get; set; }
    
        [XmlIgnore]
        public bool EmailAttemptsCountSpecified
        {
            get { return this.EmailAttemptsCount != null; }
            set { }
        }
    
        [XmlElement("FaceToFaceInterviewsCount", Order = 9)]
        public MISMOCount FaceToFaceInterviewsCount { get; set; }
    
        [XmlIgnore]
        public bool FaceToFaceInterviewsCountSpecified
        {
            get { return this.FaceToFaceInterviewsCount != null; }
            set { }
        }
    
        [XmlElement("FormLettersCount", Order = 10)]
        public MISMOCount FormLettersCount { get; set; }
    
        [XmlIgnore]
        public bool FormLettersCountSpecified
        {
            get { return this.FormLettersCount != null; }
            set { }
        }
    
        [XmlElement("LienRecommendationType", Order = 11)]
        public MISMOEnum<LienRecommendationBase> LienRecommendationType { get; set; }
    
        [XmlIgnore]
        public bool LienRecommendationTypeSpecified
        {
            get { return this.LienRecommendationType != null; }
            set { }
        }
    
        [XmlElement("LienRecommendationTypeOtherDescription", Order = 12)]
        public MISMOString LienRecommendationTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool LienRecommendationTypeOtherDescriptionSpecified
        {
            get { return this.LienRecommendationTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("NumberOfCallAttemptsCount", Order = 13)]
        public MISMOCount NumberOfCallAttemptsCount { get; set; }
    
        [XmlIgnore]
        public bool NumberOfCallAttemptsCountSpecified
        {
            get { return this.NumberOfCallAttemptsCount != null; }
            set { }
        }
    
        [XmlElement("OutgoingCallMethodType", Order = 14)]
        public MISMOEnum<OutgoingCallMethodBase> OutgoingCallMethodType { get; set; }
    
        [XmlIgnore]
        public bool OutgoingCallMethodTypeSpecified
        {
            get { return this.OutgoingCallMethodType != null; }
            set { }
        }
    
        [XmlElement("PropertyCurrentOccupancyType", Order = 15)]
        public MISMOEnum<PropertyCurrentOccupancyBase> PropertyCurrentOccupancyType { get; set; }
    
        [XmlIgnore]
        public bool PropertyCurrentOccupancyTypeSpecified
        {
            get { return this.PropertyCurrentOccupancyType != null; }
            set { }
        }
    
        [XmlElement("RightPartyContactCount", Order = 16)]
        public MISMOCount RightPartyContactCount { get; set; }
    
        [XmlIgnore]
        public bool RightPartyContactCountSpecified
        {
            get { return this.RightPartyContactCount != null; }
            set { }
        }
    
        [XmlElement("SubordinateLienHolderPaymentAmount", Order = 17)]
        public MISMOAmount SubordinateLienHolderPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool SubordinateLienHolderPaymentAmountSpecified
        {
            get { return this.SubordinateLienHolderPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("TaxAdvancesOnAccountsNotEscrowedIndicator", Order = 18)]
        public MISMOIndicator TaxAdvancesOnAccountsNotEscrowedIndicator { get; set; }
    
        [XmlIgnore]
        public bool TaxAdvancesOnAccountsNotEscrowedIndicatorSpecified
        {
            get { return this.TaxAdvancesOnAccountsNotEscrowedIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 19)]
        public DELINQUENCY_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
