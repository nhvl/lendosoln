namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class DELINQUENCIES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DelinquencyListSpecified
                    || this.DelinquencySummarySpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DELINQUENCY", Order = 0)]
        public List<DELINQUENCY> DelinquencyList { get; set; } = new List<DELINQUENCY>();
    
        [XmlIgnore]
        public bool DelinquencyListSpecified
        {
            get { return this.DelinquencyList != null && this.DelinquencyList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("DELINQUENCY_SUMMARY", Order = 1)]
        public DELINQUENCY_SUMMARY DelinquencySummary { get; set; }
    
        [XmlIgnore]
        public bool DelinquencySummarySpecified
        {
            get { return this.DelinquencySummary != null && this.DelinquencySummary.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public DELINQUENCIES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
