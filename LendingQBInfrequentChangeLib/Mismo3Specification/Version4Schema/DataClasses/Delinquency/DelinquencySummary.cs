namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class DELINQUENCY_SUMMARY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DelinquentPaymentCountSpecified
                    || this.DelinquentPaymentsOverPastTwelveMonthsCountSpecified
                    || this.LoanDelinquencyHistoryPeriodMonthsCountSpecified
                    || this.LoanReinstateTotalAmountSpecified
                    || this.NetLiquidationProceedsAmountSpecified
                    || this.OnTimePaymentCountSpecified
                    || this.PaymentDelinquentDaysCountSpecified
                    || this.TotalDelinquentInterestAmountSpecified
                    || this.TotalDelinquentInterestReportedDateSpecified
                    || this.TotalLiquidationExpenseAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DelinquentPaymentCount", Order = 0)]
        public MISMOCount DelinquentPaymentCount { get; set; }
    
        [XmlIgnore]
        public bool DelinquentPaymentCountSpecified
        {
            get { return this.DelinquentPaymentCount != null; }
            set { }
        }
    
        [XmlElement("DelinquentPaymentsOverPastTwelveMonthsCount", Order = 1)]
        public MISMOCount DelinquentPaymentsOverPastTwelveMonthsCount { get; set; }
    
        [XmlIgnore]
        public bool DelinquentPaymentsOverPastTwelveMonthsCountSpecified
        {
            get { return this.DelinquentPaymentsOverPastTwelveMonthsCount != null; }
            set { }
        }
    
        [XmlElement("LoanDelinquencyHistoryPeriodMonthsCount", Order = 2)]
        public MISMOCount LoanDelinquencyHistoryPeriodMonthsCount { get; set; }
    
        [XmlIgnore]
        public bool LoanDelinquencyHistoryPeriodMonthsCountSpecified
        {
            get { return this.LoanDelinquencyHistoryPeriodMonthsCount != null; }
            set { }
        }
    
        [XmlElement("LoanReinstateTotalAmount", Order = 3)]
        public MISMOAmount LoanReinstateTotalAmount { get; set; }
    
        [XmlIgnore]
        public bool LoanReinstateTotalAmountSpecified
        {
            get { return this.LoanReinstateTotalAmount != null; }
            set { }
        }
    
        [XmlElement("NetLiquidationProceedsAmount", Order = 4)]
        public MISMOAmount NetLiquidationProceedsAmount { get; set; }
    
        [XmlIgnore]
        public bool NetLiquidationProceedsAmountSpecified
        {
            get { return this.NetLiquidationProceedsAmount != null; }
            set { }
        }
    
        [XmlElement("OnTimePaymentCount", Order = 5)]
        public MISMOCount OnTimePaymentCount { get; set; }
    
        [XmlIgnore]
        public bool OnTimePaymentCountSpecified
        {
            get { return this.OnTimePaymentCount != null; }
            set { }
        }
    
        [XmlElement("PaymentDelinquentDaysCount", Order = 6)]
        public MISMOCount PaymentDelinquentDaysCount { get; set; }
    
        [XmlIgnore]
        public bool PaymentDelinquentDaysCountSpecified
        {
            get { return this.PaymentDelinquentDaysCount != null; }
            set { }
        }
    
        [XmlElement("TotalDelinquentInterestAmount", Order = 7)]
        public MISMOAmount TotalDelinquentInterestAmount { get; set; }
    
        [XmlIgnore]
        public bool TotalDelinquentInterestAmountSpecified
        {
            get { return this.TotalDelinquentInterestAmount != null; }
            set { }
        }
    
        [XmlElement("TotalDelinquentInterestReportedDate", Order = 8)]
        public MISMODate TotalDelinquentInterestReportedDate { get; set; }
    
        [XmlIgnore]
        public bool TotalDelinquentInterestReportedDateSpecified
        {
            get { return this.TotalDelinquentInterestReportedDate != null; }
            set { }
        }
    
        [XmlElement("TotalLiquidationExpenseAmount", Order = 9)]
        public MISMOAmount TotalLiquidationExpenseAmount { get; set; }
    
        [XmlIgnore]
        public bool TotalLiquidationExpenseAmountSpecified
        {
            get { return this.TotalLiquidationExpenseAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 10)]
        public DELINQUENCY_SUMMARY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
