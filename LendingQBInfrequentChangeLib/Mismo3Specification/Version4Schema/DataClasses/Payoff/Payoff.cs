namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PAYOFF
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.IntegratedDisclosureSectionTypeSpecified
                    || this.IntegratedDisclosureSectionTypeOtherDescriptionSpecified
                    || this.LiabilityAccountRemainsOpenIndicatorSpecified
                    || this.PayoffAccountNumberIdentifierSpecified
                    || this.PayoffActionTypeSpecified
                    || this.PayoffActionTypeOtherDescriptionSpecified
                    || this.PayoffAmountSpecified
                    || this.PayoffApplicationSequenceTypeSpecified
                    || this.PayoffOrderedDatetimeSpecified
                    || this.PayoffPartialIndicatorSpecified
                    || this.PayoffPerDiemAmountSpecified
                    || this.PayoffPrepaymentPenaltyAmountSpecified
                    || this.PayoffRequestedByTypeSpecified
                    || this.PayoffRequestedByTypeOtherDescriptionSpecified
                    || this.PayoffRequestedDateSpecified
                    || this.PayoffSpecifiedHUD1LineNumberValueSpecified
                    || this.PayoffThroughDateSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("IntegratedDisclosureSectionType", Order = 0)]
        public MISMOEnum<IntegratedDisclosureSectionBase> IntegratedDisclosureSectionType { get; set; }
    
        [XmlIgnore]
        public bool IntegratedDisclosureSectionTypeSpecified
        {
            get { return this.IntegratedDisclosureSectionType != null; }
            set { }
        }
    
        [XmlElement("IntegratedDisclosureSectionTypeOtherDescription", Order = 1)]
        public MISMOString IntegratedDisclosureSectionTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool IntegratedDisclosureSectionTypeOtherDescriptionSpecified
        {
            get { return this.IntegratedDisclosureSectionTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("LiabilityAccountRemainsOpenIndicator", Order = 2)]
        public MISMOIndicator LiabilityAccountRemainsOpenIndicator { get; set; }
    
        [XmlIgnore]
        public bool LiabilityAccountRemainsOpenIndicatorSpecified
        {
            get { return this.LiabilityAccountRemainsOpenIndicator != null; }
            set { }
        }
    
        [XmlElement("PayoffAccountNumberIdentifier", Order = 3)]
        public MISMOIdentifier PayoffAccountNumberIdentifier { get; set; }
    
        [XmlIgnore]
        public bool PayoffAccountNumberIdentifierSpecified
        {
            get { return this.PayoffAccountNumberIdentifier != null; }
            set { }
        }
    
        [XmlElement("PayoffActionType", Order = 4)]
        public MISMOEnum<PayoffActionBase> PayoffActionType { get; set; }
    
        [XmlIgnore]
        public bool PayoffActionTypeSpecified
        {
            get { return this.PayoffActionType != null; }
            set { }
        }
    
        [XmlElement("PayoffActionTypeOtherDescription", Order = 5)]
        public MISMOString PayoffActionTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool PayoffActionTypeOtherDescriptionSpecified
        {
            get { return this.PayoffActionTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("PayoffAmount", Order = 6)]
        public MISMOAmount PayoffAmount { get; set; }
    
        [XmlIgnore]
        public bool PayoffAmountSpecified
        {
            get { return this.PayoffAmount != null; }
            set { }
        }
    
        [XmlElement("PayoffApplicationSequenceType", Order = 7)]
        public MISMOEnum<PayoffApplicationSequenceBase> PayoffApplicationSequenceType { get; set; }
    
        [XmlIgnore]
        public bool PayoffApplicationSequenceTypeSpecified
        {
            get { return this.PayoffApplicationSequenceType != null; }
            set { }
        }
    
        [XmlElement("PayoffOrderedDatetime", Order = 8)]
        public MISMODatetime PayoffOrderedDatetime { get; set; }
    
        [XmlIgnore]
        public bool PayoffOrderedDatetimeSpecified
        {
            get { return this.PayoffOrderedDatetime != null; }
            set { }
        }
    
        [XmlElement("PayoffPartialIndicator", Order = 9)]
        public MISMOIndicator PayoffPartialIndicator { get; set; }
    
        [XmlIgnore]
        public bool PayoffPartialIndicatorSpecified
        {
            get { return this.PayoffPartialIndicator != null; }
            set { }
        }
    
        [XmlElement("PayoffPerDiemAmount", Order = 10)]
        public MISMOAmount PayoffPerDiemAmount { get; set; }
    
        [XmlIgnore]
        public bool PayoffPerDiemAmountSpecified
        {
            get { return this.PayoffPerDiemAmount != null; }
            set { }
        }
    
        [XmlElement("PayoffPrepaymentPenaltyAmount", Order = 11)]
        public MISMOAmount PayoffPrepaymentPenaltyAmount { get; set; }
    
        [XmlIgnore]
        public bool PayoffPrepaymentPenaltyAmountSpecified
        {
            get { return this.PayoffPrepaymentPenaltyAmount != null; }
            set { }
        }
    
        [XmlElement("PayoffRequestedByType", Order = 12)]
        public MISMOEnum<PayoffRequestedByBase> PayoffRequestedByType { get; set; }
    
        [XmlIgnore]
        public bool PayoffRequestedByTypeSpecified
        {
            get { return this.PayoffRequestedByType != null; }
            set { }
        }
    
        [XmlElement("PayoffRequestedByTypeOtherDescription", Order = 13)]
        public MISMOString PayoffRequestedByTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool PayoffRequestedByTypeOtherDescriptionSpecified
        {
            get { return this.PayoffRequestedByTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("PayoffRequestedDate", Order = 14)]
        public MISMODate PayoffRequestedDate { get; set; }
    
        [XmlIgnore]
        public bool PayoffRequestedDateSpecified
        {
            get { return this.PayoffRequestedDate != null; }
            set { }
        }
    
        [XmlElement("PayoffSpecifiedHUD1LineNumberValue", Order = 15)]
        public MISMOValue PayoffSpecifiedHUD1LineNumberValue { get; set; }
    
        [XmlIgnore]
        public bool PayoffSpecifiedHUD1LineNumberValueSpecified
        {
            get { return this.PayoffSpecifiedHUD1LineNumberValue != null; }
            set { }
        }
    
        [XmlElement("PayoffThroughDate", Order = 16)]
        public MISMODate PayoffThroughDate { get; set; }
    
        [XmlIgnore]
        public bool PayoffThroughDateSpecified
        {
            get { return this.PayoffThroughDate != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 17)]
        public PAYOFF_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
