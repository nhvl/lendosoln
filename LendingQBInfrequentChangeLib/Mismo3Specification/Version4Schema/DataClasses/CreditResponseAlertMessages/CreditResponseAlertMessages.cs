namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class CREDIT_RESPONSE_ALERT_MESSAGES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditResponseAlertMessageListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CREDIT_RESPONSE_ALERT_MESSAGE", Order = 0)]
        public List<CREDIT_RESPONSE_ALERT_MESSAGE> CreditResponseAlertMessageList { get; set; } = new List<CREDIT_RESPONSE_ALERT_MESSAGE>();
    
        [XmlIgnore]
        public bool CreditResponseAlertMessageListSpecified
        {
            get { return this.CreditResponseAlertMessageList != null && this.CreditResponseAlertMessageList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public CREDIT_RESPONSE_ALERT_MESSAGES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
