namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class SERVICING_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ActiveServicingTransferIndicatorSpecified
                    || this.DefaultStatusTypeSpecified
                    || this.DefaultStatusTypeOtherDescriptionSpecified
                    || this.LoanAcquisitionActualUPBAmountSpecified
                    || this.LoanActivityReportingTransactionIdentifierSpecified
                    || this.LoanImminentDefaultSourceTypeSpecified
                    || this.LoanImminentDefaultSourceTypeOtherDescriptionSpecified
                    || this.PreviouslyReportedInformationRevisionIndicatorSpecified
                    || this.ServicerWelcomeCallPerformedDateSpecified
                    || this.SFDMSAutomatedDefaultProcessingIdentifierSpecified
                    || this.TitleReportLastReceivedDateSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ActiveServicingTransferIndicator", Order = 0)]
        public MISMOIndicator ActiveServicingTransferIndicator { get; set; }
    
        [XmlIgnore]
        public bool ActiveServicingTransferIndicatorSpecified
        {
            get { return this.ActiveServicingTransferIndicator != null; }
            set { }
        }
    
        [XmlElement("DefaultStatusType", Order = 1)]
        public MISMOEnum<DefaultStatusBase> DefaultStatusType { get; set; }
    
        [XmlIgnore]
        public bool DefaultStatusTypeSpecified
        {
            get { return this.DefaultStatusType != null; }
            set { }
        }
    
        [XmlElement("DefaultStatusTypeOtherDescription", Order = 2)]
        public MISMOString DefaultStatusTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool DefaultStatusTypeOtherDescriptionSpecified
        {
            get { return this.DefaultStatusTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("LoanAcquisitionActualUPBAmount", Order = 3)]
        public MISMOAmount LoanAcquisitionActualUPBAmount { get; set; }
    
        [XmlIgnore]
        public bool LoanAcquisitionActualUPBAmountSpecified
        {
            get { return this.LoanAcquisitionActualUPBAmount != null; }
            set { }
        }
    
        [XmlElement("LoanActivityReportingTransactionIdentifier", Order = 4)]
        public MISMOIdentifier LoanActivityReportingTransactionIdentifier { get; set; }
    
        [XmlIgnore]
        public bool LoanActivityReportingTransactionIdentifierSpecified
        {
            get { return this.LoanActivityReportingTransactionIdentifier != null; }
            set { }
        }
    
        [XmlElement("LoanImminentDefaultSourceType", Order = 5)]
        public MISMOEnum<LoanImminentDefaultSourceBase> LoanImminentDefaultSourceType { get; set; }
    
        [XmlIgnore]
        public bool LoanImminentDefaultSourceTypeSpecified
        {
            get { return this.LoanImminentDefaultSourceType != null; }
            set { }
        }
    
        [XmlElement("LoanImminentDefaultSourceTypeOtherDescription", Order = 6)]
        public MISMOString LoanImminentDefaultSourceTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool LoanImminentDefaultSourceTypeOtherDescriptionSpecified
        {
            get { return this.LoanImminentDefaultSourceTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("PreviouslyReportedInformationRevisionIndicator", Order = 7)]
        public MISMOIndicator PreviouslyReportedInformationRevisionIndicator { get; set; }
    
        [XmlIgnore]
        public bool PreviouslyReportedInformationRevisionIndicatorSpecified
        {
            get { return this.PreviouslyReportedInformationRevisionIndicator != null; }
            set { }
        }
    
        [XmlElement("ServicerWelcomeCallPerformedDate", Order = 8)]
        public MISMODate ServicerWelcomeCallPerformedDate { get; set; }
    
        [XmlIgnore]
        public bool ServicerWelcomeCallPerformedDateSpecified
        {
            get { return this.ServicerWelcomeCallPerformedDate != null; }
            set { }
        }
    
        [XmlElement("SFDMSAutomatedDefaultProcessingIdentifier", Order = 9)]
        public MISMOIdentifier SFDMSAutomatedDefaultProcessingIdentifier { get; set; }
    
        [XmlIgnore]
        public bool SFDMSAutomatedDefaultProcessingIdentifierSpecified
        {
            get { return this.SFDMSAutomatedDefaultProcessingIdentifier != null; }
            set { }
        }
    
        [XmlElement("TitleReportLastReceivedDate", Order = 10)]
        public MISMODate TitleReportLastReceivedDate { get; set; }
    
        [XmlIgnore]
        public bool TitleReportLastReceivedDateSpecified
        {
            get { return this.TitleReportLastReceivedDate != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 11)]
        public SERVICING_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
