namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class SERVICING
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AdvancesSpecified
                    || this.CreditBureauReportingSpecified
                    || this.DelinquenciesSpecified
                    || this.DisclosureOnServicerSpecified
                    || this.MonetaryEventSummariesSpecified
                    || this.MonetaryEventsSpecified
                    || this.ServicerQualifiedWrittenRequestMailToSpecified
                    || this.ServicingCommentsSpecified
                    || this.ServicingDetailSpecified
                    || this.StopCodesSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ADVANCES", Order = 0)]
        public ADVANCES Advances { get; set; }
    
        [XmlIgnore]
        public bool AdvancesSpecified
        {
            get { return this.Advances != null && this.Advances.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CREDIT_BUREAU_REPORTING", Order = 1)]
        public CREDIT_BUREAU_REPORTING CreditBureauReporting { get; set; }
    
        [XmlIgnore]
        public bool CreditBureauReportingSpecified
        {
            get { return this.CreditBureauReporting != null && this.CreditBureauReporting.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("DELINQUENCIES", Order = 2)]
        public DELINQUENCIES Delinquencies { get; set; }
    
        [XmlIgnore]
        public bool DelinquenciesSpecified
        {
            get { return this.Delinquencies != null && this.Delinquencies.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("DISCLOSURE_ON_SERVICER", Order = 3)]
        public DISCLOSURE_ON_SERVICER DisclosureOnServicer { get; set; }
    
        [XmlIgnore]
        public bool DisclosureOnServicerSpecified
        {
            get { return this.DisclosureOnServicer != null && this.DisclosureOnServicer.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("MONETARY_EVENT_SUMMARIES", Order = 4)]
        public MONETARY_EVENT_SUMMARIES MonetaryEventSummaries { get; set; }
    
        [XmlIgnore]
        public bool MonetaryEventSummariesSpecified
        {
            get { return this.MonetaryEventSummaries != null && this.MonetaryEventSummaries.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("MONETARY_EVENTS", Order = 5)]
        public MONETARY_EVENTS MonetaryEvents { get; set; }
    
        [XmlIgnore]
        public bool MonetaryEventsSpecified
        {
            get { return this.MonetaryEvents != null && this.MonetaryEvents.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SERVICER_QUALIFIED_WRITTEN_REQUEST_MAIL_TO", Order = 6)]
        public SERVICER_QUALIFIED_WRITTEN_REQUEST_MAIL_TO ServicerQualifiedWrittenRequestMailTo { get; set; }
    
        [XmlIgnore]
        public bool ServicerQualifiedWrittenRequestMailToSpecified
        {
            get { return this.ServicerQualifiedWrittenRequestMailTo != null && this.ServicerQualifiedWrittenRequestMailTo.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SERVICING_COMMENTS", Order = 7)]
        public SERVICING_COMMENTS ServicingComments { get; set; }
    
        [XmlIgnore]
        public bool ServicingCommentsSpecified
        {
            get { return this.ServicingComments != null && this.ServicingComments.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SERVICING_DETAIL", Order = 8)]
        public SERVICING_DETAIL ServicingDetail { get; set; }
    
        [XmlIgnore]
        public bool ServicingDetailSpecified
        {
            get { return this.ServicingDetail != null && this.ServicingDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("STOP_CODES", Order = 9)]
        public STOP_CODES StopCodes { get; set; }
    
        [XmlIgnore]
        public bool StopCodesSpecified
        {
            get { return this.StopCodes != null && this.StopCodes.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 10)]
        public SERVICING_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
