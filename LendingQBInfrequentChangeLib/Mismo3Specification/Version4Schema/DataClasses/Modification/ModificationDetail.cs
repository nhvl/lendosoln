namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class MODIFICATION_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BorrowerModificationClosingCostAmountSpecified
                    || this.ForbearancePrincipalReductionAmountSpecified
                    || this.InterestBearingUPBAmountSpecified
                    || this.InterestForgivenessAmountSpecified
                    || this.LoanModificationActionTypeSpecified
                    || this.LoanModificationActionTypeOtherDescriptionSpecified
                    || this.LoanModificationEffectiveDateSpecified
                    || this.ModificationAdditionalEndorsementIndicatorSpecified
                    || this.ModificationAgreementAcceptanceCutoffDateSpecified
                    || this.ModificationAgreementReceivedIndicatorSpecified
                    || this.ModificationEstimatedRequestProcessingDaysCountSpecified
                    || this.ModificationSignedDateSpecified
                    || this.ModificationSubmissionCutoffDateSpecified
                    || this.ModificationTrialPlanEffectiveDateSpecified
                    || this.ModificationTrialPlanEndDateSpecified
                    || this.ModificationTrialPlanReturnOfDocumentsCutoffDateSpecified
                    || this.NonInterestBearingUPBAmountSpecified
                    || this.PreModificationInterestRatePercentSpecified
                    || this.PreModificationPrincipalAndInterestPaymentAmountSpecified
                    || this.PreModificationTotalPaymentAmountSpecified
                    || this.PreModificationUPBAmountSpecified
                    || this.PrincipalForgivenessAmountSpecified
                    || this.PrincipalReductionOfUnsecuredAmountSpecified
                    || this.ReimbursableServicerModificationCostAmountSpecified
                    || this.ServicerAttestsOnSubmissionIndicatorSpecified
                    || this.ServicerOfficerModificationSignatureDateSpecified
                    || this.TotalForgivenessAmountSpecified
                    || this.TotalStepCountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("BorrowerModificationClosingCostAmount", Order = 0)]
        public MISMOAmount BorrowerModificationClosingCostAmount { get; set; }
    
        [XmlIgnore]
        public bool BorrowerModificationClosingCostAmountSpecified
        {
            get { return this.BorrowerModificationClosingCostAmount != null; }
            set { }
        }
    
        [XmlElement("ForbearancePrincipalReductionAmount", Order = 1)]
        public MISMOAmount ForbearancePrincipalReductionAmount { get; set; }
    
        [XmlIgnore]
        public bool ForbearancePrincipalReductionAmountSpecified
        {
            get { return this.ForbearancePrincipalReductionAmount != null; }
            set { }
        }
    
        [XmlElement("InterestBearingUPBAmount", Order = 2)]
        public MISMOAmount InterestBearingUPBAmount { get; set; }
    
        [XmlIgnore]
        public bool InterestBearingUPBAmountSpecified
        {
            get { return this.InterestBearingUPBAmount != null; }
            set { }
        }
    
        [XmlElement("InterestForgivenessAmount", Order = 3)]
        public MISMOAmount InterestForgivenessAmount { get; set; }
    
        [XmlIgnore]
        public bool InterestForgivenessAmountSpecified
        {
            get { return this.InterestForgivenessAmount != null; }
            set { }
        }
    
        [XmlElement("LoanModificationActionType", Order = 4)]
        public MISMOEnum<LoanModificationActionBase> LoanModificationActionType { get; set; }
    
        [XmlIgnore]
        public bool LoanModificationActionTypeSpecified
        {
            get { return this.LoanModificationActionType != null; }
            set { }
        }
    
        [XmlElement("LoanModificationActionTypeOtherDescription", Order = 5)]
        public MISMOString LoanModificationActionTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool LoanModificationActionTypeOtherDescriptionSpecified
        {
            get { return this.LoanModificationActionTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("LoanModificationEffectiveDate", Order = 6)]
        public MISMODate LoanModificationEffectiveDate { get; set; }
    
        [XmlIgnore]
        public bool LoanModificationEffectiveDateSpecified
        {
            get { return this.LoanModificationEffectiveDate != null; }
            set { }
        }
    
        [XmlElement("ModificationAdditionalEndorsementIndicator", Order = 7)]
        public MISMOIndicator ModificationAdditionalEndorsementIndicator { get; set; }
    
        [XmlIgnore]
        public bool ModificationAdditionalEndorsementIndicatorSpecified
        {
            get { return this.ModificationAdditionalEndorsementIndicator != null; }
            set { }
        }
    
        [XmlElement("ModificationAgreementAcceptanceCutoffDate", Order = 8)]
        public MISMODate ModificationAgreementAcceptanceCutoffDate { get; set; }
    
        [XmlIgnore]
        public bool ModificationAgreementAcceptanceCutoffDateSpecified
        {
            get { return this.ModificationAgreementAcceptanceCutoffDate != null; }
            set { }
        }
    
        [XmlElement("ModificationAgreementReceivedIndicator", Order = 9)]
        public MISMOIndicator ModificationAgreementReceivedIndicator { get; set; }
    
        [XmlIgnore]
        public bool ModificationAgreementReceivedIndicatorSpecified
        {
            get { return this.ModificationAgreementReceivedIndicator != null; }
            set { }
        }
    
        [XmlElement("ModificationEstimatedRequestProcessingDaysCount", Order = 10)]
        public MISMOCount ModificationEstimatedRequestProcessingDaysCount { get; set; }
    
        [XmlIgnore]
        public bool ModificationEstimatedRequestProcessingDaysCountSpecified
        {
            get { return this.ModificationEstimatedRequestProcessingDaysCount != null; }
            set { }
        }
    
        [XmlElement("ModificationSignedDate", Order = 11)]
        public MISMODate ModificationSignedDate { get; set; }
    
        [XmlIgnore]
        public bool ModificationSignedDateSpecified
        {
            get { return this.ModificationSignedDate != null; }
            set { }
        }
    
        [XmlElement("ModificationSubmissionCutoffDate", Order = 12)]
        public MISMODate ModificationSubmissionCutoffDate { get; set; }
    
        [XmlIgnore]
        public bool ModificationSubmissionCutoffDateSpecified
        {
            get { return this.ModificationSubmissionCutoffDate != null; }
            set { }
        }
    
        [XmlElement("ModificationTrialPlanEffectiveDate", Order = 13)]
        public MISMODate ModificationTrialPlanEffectiveDate { get; set; }
    
        [XmlIgnore]
        public bool ModificationTrialPlanEffectiveDateSpecified
        {
            get { return this.ModificationTrialPlanEffectiveDate != null; }
            set { }
        }
    
        [XmlElement("ModificationTrialPlanEndDate", Order = 14)]
        public MISMODate ModificationTrialPlanEndDate { get; set; }
    
        [XmlIgnore]
        public bool ModificationTrialPlanEndDateSpecified
        {
            get { return this.ModificationTrialPlanEndDate != null; }
            set { }
        }
    
        [XmlElement("ModificationTrialPlanReturnOfDocumentsCutoffDate", Order = 15)]
        public MISMODate ModificationTrialPlanReturnOfDocumentsCutoffDate { get; set; }
    
        [XmlIgnore]
        public bool ModificationTrialPlanReturnOfDocumentsCutoffDateSpecified
        {
            get { return this.ModificationTrialPlanReturnOfDocumentsCutoffDate != null; }
            set { }
        }
    
        [XmlElement("NonInterestBearingUPBAmount", Order = 16)]
        public MISMOAmount NonInterestBearingUPBAmount { get; set; }
    
        [XmlIgnore]
        public bool NonInterestBearingUPBAmountSpecified
        {
            get { return this.NonInterestBearingUPBAmount != null; }
            set { }
        }
    
        [XmlElement("PreModificationInterestRatePercent", Order = 17)]
        public MISMOPercent PreModificationInterestRatePercent { get; set; }
    
        [XmlIgnore]
        public bool PreModificationInterestRatePercentSpecified
        {
            get { return this.PreModificationInterestRatePercent != null; }
            set { }
        }
    
        [XmlElement("PreModificationPrincipalAndInterestPaymentAmount", Order = 18)]
        public MISMOAmount PreModificationPrincipalAndInterestPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool PreModificationPrincipalAndInterestPaymentAmountSpecified
        {
            get { return this.PreModificationPrincipalAndInterestPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("PreModificationTotalPaymentAmount", Order = 19)]
        public MISMOAmount PreModificationTotalPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool PreModificationTotalPaymentAmountSpecified
        {
            get { return this.PreModificationTotalPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("PreModificationUPBAmount", Order = 20)]
        public MISMOAmount PreModificationUPBAmount { get; set; }
    
        [XmlIgnore]
        public bool PreModificationUPBAmountSpecified
        {
            get { return this.PreModificationUPBAmount != null; }
            set { }
        }
    
        [XmlElement("PrincipalForgivenessAmount", Order = 21)]
        public MISMOAmount PrincipalForgivenessAmount { get; set; }
    
        [XmlIgnore]
        public bool PrincipalForgivenessAmountSpecified
        {
            get { return this.PrincipalForgivenessAmount != null; }
            set { }
        }
    
        [XmlElement("PrincipalReductionOfUnsecuredAmount", Order = 22)]
        public MISMOAmount PrincipalReductionOfUnsecuredAmount { get; set; }
    
        [XmlIgnore]
        public bool PrincipalReductionOfUnsecuredAmountSpecified
        {
            get { return this.PrincipalReductionOfUnsecuredAmount != null; }
            set { }
        }
    
        [XmlElement("ReimbursableServicerModificationCostAmount", Order = 23)]
        public MISMOAmount ReimbursableServicerModificationCostAmount { get; set; }
    
        [XmlIgnore]
        public bool ReimbursableServicerModificationCostAmountSpecified
        {
            get { return this.ReimbursableServicerModificationCostAmount != null; }
            set { }
        }
    
        [XmlElement("ServicerAttestsOnSubmissionIndicator", Order = 24)]
        public MISMOIndicator ServicerAttestsOnSubmissionIndicator { get; set; }
    
        [XmlIgnore]
        public bool ServicerAttestsOnSubmissionIndicatorSpecified
        {
            get { return this.ServicerAttestsOnSubmissionIndicator != null; }
            set { }
        }
    
        [XmlElement("ServicerOfficerModificationSignatureDate", Order = 25)]
        public MISMODate ServicerOfficerModificationSignatureDate { get; set; }
    
        [XmlIgnore]
        public bool ServicerOfficerModificationSignatureDateSpecified
        {
            get { return this.ServicerOfficerModificationSignatureDate != null; }
            set { }
        }
    
        [XmlElement("TotalForgivenessAmount", Order = 26)]
        public MISMOAmount TotalForgivenessAmount { get; set; }
    
        [XmlIgnore]
        public bool TotalForgivenessAmountSpecified
        {
            get { return this.TotalForgivenessAmount != null; }
            set { }
        }
    
        [XmlElement("TotalStepCount", Order = 27)]
        public MISMOCount TotalStepCount { get; set; }
    
        [XmlIgnore]
        public bool TotalStepCountSpecified
        {
            get { return this.TotalStepCount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 28)]
        public MODIFICATION_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
