namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class MODIFICATION_SUMMARY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.LifeOfLoanTotalForgivenInterestAmountSpecified
                    || this.LifeOfLoanTotalForgivenPrincipalAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("LifeOfLoanTotalForgivenInterestAmount", Order = 0)]
        public MISMOAmount LifeOfLoanTotalForgivenInterestAmount { get; set; }
    
        [XmlIgnore]
        public bool LifeOfLoanTotalForgivenInterestAmountSpecified
        {
            get { return this.LifeOfLoanTotalForgivenInterestAmount != null; }
            set { }
        }
    
        [XmlElement("LifeOfLoanTotalForgivenPrincipalAmount", Order = 1)]
        public MISMOAmount LifeOfLoanTotalForgivenPrincipalAmount { get; set; }
    
        [XmlIgnore]
        public bool LifeOfLoanTotalForgivenPrincipalAmountSpecified
        {
            get { return this.LifeOfLoanTotalForgivenPrincipalAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public MODIFICATION_SUMMARY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
