namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class MODIFICATIONS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ModificationListSpecified
                    || this.ModificationSummarySpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("MODIFICATION", Order = 0)]
        public List<MODIFICATION> ModificationList { get; set; } = new List<MODIFICATION>();
    
        [XmlIgnore]
        public bool ModificationListSpecified
        {
            get { return this.ModificationList != null && this.ModificationList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("MODIFICATION_SUMMARY", Order = 1)]
        public MODIFICATION_SUMMARY ModificationSummary { get; set; }
    
        [XmlIgnore]
        public bool ModificationSummarySpecified
        {
            get { return this.ModificationSummary != null && this.ModificationSummary.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public MODIFICATIONS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
