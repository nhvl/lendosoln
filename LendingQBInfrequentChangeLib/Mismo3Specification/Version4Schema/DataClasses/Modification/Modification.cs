namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class MODIFICATION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ModificationAspectsSpecified
                    || this.ModificationDetailSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("MODIFICATION_ASPECTS", Order = 0)]
        public MODIFICATION_ASPECTS ModificationAspects { get; set; }
    
        [XmlIgnore]
        public bool ModificationAspectsSpecified
        {
            get { return this.ModificationAspects != null && this.ModificationAspects.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("MODIFICATION_DETAIL", Order = 1)]
        public MODIFICATION_DETAIL ModificationDetail { get; set; }
    
        [XmlIgnore]
        public bool ModificationDetailSpecified
        {
            get { return this.ModificationDetail != null && this.ModificationDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public MODIFICATION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
