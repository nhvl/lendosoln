namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CASH_REMITTANCE_SUMMARY_NOTIFICATION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.InvestorRemittanceAmountSpecified
                    || this.InvestorRemittanceEffectiveDateSpecified
                    || this.InvestorRemittanceTypeSpecified
                    || this.InvestorRemittanceTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("InvestorRemittanceAmount", Order = 0)]
        public MISMOAmount InvestorRemittanceAmount { get; set; }
    
        [XmlIgnore]
        public bool InvestorRemittanceAmountSpecified
        {
            get { return this.InvestorRemittanceAmount != null; }
            set { }
        }
    
        [XmlElement("InvestorRemittanceEffectiveDate", Order = 1)]
        public MISMODate InvestorRemittanceEffectiveDate { get; set; }
    
        [XmlIgnore]
        public bool InvestorRemittanceEffectiveDateSpecified
        {
            get { return this.InvestorRemittanceEffectiveDate != null; }
            set { }
        }
    
        [XmlElement("InvestorRemittanceType", Order = 2)]
        public MISMOEnum<InvestorRemittanceBase> InvestorRemittanceType { get; set; }
    
        [XmlIgnore]
        public bool InvestorRemittanceTypeSpecified
        {
            get { return this.InvestorRemittanceType != null; }
            set { }
        }
    
        [XmlElement("InvestorRemittanceTypeOtherDescription", Order = 3)]
        public MISMOString InvestorRemittanceTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool InvestorRemittanceTypeOtherDescriptionSpecified
        {
            get { return this.InvestorRemittanceTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public CASH_REMITTANCE_SUMMARY_NOTIFICATION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
