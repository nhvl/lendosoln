namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class FORECLOSURES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ForeclosureListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("FORECLOSURE", Order = 0)]
        public List<FORECLOSURE> ForeclosureList { get; set; } = new List<FORECLOSURE>();
    
        [XmlIgnore]
        public bool ForeclosureListSpecified
        {
            get { return this.ForeclosureList != null && this.ForeclosureList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public FORECLOSURES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
