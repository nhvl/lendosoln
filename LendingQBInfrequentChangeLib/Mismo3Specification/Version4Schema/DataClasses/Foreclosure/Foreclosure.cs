namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class FORECLOSURE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ForeclosureActionsSpecified
                    || this.ForeclosureDelaysSpecified
                    || this.ForeclosureDetailSpecified
                    || this.ForeclosureExpensesSpecified
                    || this.ForeclosureStatusesSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("FORECLOSURE_ACTIONS", Order = 0)]
        public FORECLOSURE_ACTIONS ForeclosureActions { get; set; }
    
        [XmlIgnore]
        public bool ForeclosureActionsSpecified
        {
            get { return this.ForeclosureActions != null && this.ForeclosureActions.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("FORECLOSURE_DELAYS", Order = 1)]
        public FORECLOSURE_DELAYS ForeclosureDelays { get; set; }
    
        [XmlIgnore]
        public bool ForeclosureDelaysSpecified
        {
            get { return this.ForeclosureDelays != null && this.ForeclosureDelays.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("FORECLOSURE_DETAIL", Order = 2)]
        public FORECLOSURE_DETAIL ForeclosureDetail { get; set; }
    
        [XmlIgnore]
        public bool ForeclosureDetailSpecified
        {
            get { return this.ForeclosureDetail != null && this.ForeclosureDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("FORECLOSURE_EXPENSES", Order = 3)]
        public FORECLOSURE_EXPENSES ForeclosureExpenses { get; set; }
    
        [XmlIgnore]
        public bool ForeclosureExpensesSpecified
        {
            get { return this.ForeclosureExpenses != null && this.ForeclosureExpenses.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("FORECLOSURE_STATUSES", Order = 4)]
        public FORECLOSURE_STATUSES ForeclosureStatuses { get; set; }
    
        [XmlIgnore]
        public bool ForeclosureStatusesSpecified
        {
            get { return this.ForeclosureStatuses != null && this.ForeclosureStatuses.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 5)]
        public FORECLOSURE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
