namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class FORECLOSURE_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ActiveForeclosureIndicatorSpecified
                    || this.BidActualAmountSpecified
                    || this.BidMinimumAmountSpecified
                    || this.BidOpenAmountSpecified
                    || this.DaysToForeclosureSaleCountSpecified
                    || this.DeficiencyRightsPreservedIndicatorSpecified
                    || this.ForeclosureCaseDismissedReasonTypeSpecified
                    || this.ForeclosureCaseDismissedReasonTypeOtherDescriptionSpecified
                    || this.ForeclosureCaseIdentifierSpecified
                    || this.ForeclosureDelayIndicatorSpecified
                    || this.ForeclosureFeeAmountSpecified
                    || this.ForeclosureMethodTypeSpecified
                    || this.ForeclosureMethodTypeOtherDescriptionSpecified
                    || this.ForeclosurePerformanceMaximumDaysCountSpecified
                    || this.ForeclosureProcedureEndedDateSpecified
                    || this.ForeclosureSaleProceedsAmountSpecified
                    || this.ForeclosureSaleProjectedDateSpecified
                    || this.ForeclosureSaleTotalSubordinateLienholderPayoutAmountSpecified
                    || this.IRSLienNotificationIndicatorSpecified
                    || this.RedemptionExercisedIndicatorSpecified
                    || this.RedemptionPeriodExpirationDateSpecified
                    || this.RedemptionPeriodTermMonthsCountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ActiveForeclosureIndicator", Order = 0)]
        public MISMOIndicator ActiveForeclosureIndicator { get; set; }
    
        [XmlIgnore]
        public bool ActiveForeclosureIndicatorSpecified
        {
            get { return this.ActiveForeclosureIndicator != null; }
            set { }
        }
    
        [XmlElement("BidActualAmount", Order = 1)]
        public MISMOAmount BidActualAmount { get; set; }
    
        [XmlIgnore]
        public bool BidActualAmountSpecified
        {
            get { return this.BidActualAmount != null; }
            set { }
        }
    
        [XmlElement("BidMinimumAmount", Order = 2)]
        public MISMOAmount BidMinimumAmount { get; set; }
    
        [XmlIgnore]
        public bool BidMinimumAmountSpecified
        {
            get { return this.BidMinimumAmount != null; }
            set { }
        }
    
        [XmlElement("BidOpenAmount", Order = 3)]
        public MISMOAmount BidOpenAmount { get; set; }
    
        [XmlIgnore]
        public bool BidOpenAmountSpecified
        {
            get { return this.BidOpenAmount != null; }
            set { }
        }
    
        [XmlElement("DaysToForeclosureSaleCount", Order = 4)]
        public MISMOCount DaysToForeclosureSaleCount { get; set; }
    
        [XmlIgnore]
        public bool DaysToForeclosureSaleCountSpecified
        {
            get { return this.DaysToForeclosureSaleCount != null; }
            set { }
        }
    
        [XmlElement("DeficiencyRightsPreservedIndicator", Order = 5)]
        public MISMOIndicator DeficiencyRightsPreservedIndicator { get; set; }
    
        [XmlIgnore]
        public bool DeficiencyRightsPreservedIndicatorSpecified
        {
            get { return this.DeficiencyRightsPreservedIndicator != null; }
            set { }
        }
    
        [XmlElement("ForeclosureCaseDismissedReasonType", Order = 6)]
        public MISMOEnum<ForeclosureCaseDismissedReasonBase> ForeclosureCaseDismissedReasonType { get; set; }
    
        [XmlIgnore]
        public bool ForeclosureCaseDismissedReasonTypeSpecified
        {
            get { return this.ForeclosureCaseDismissedReasonType != null; }
            set { }
        }
    
        [XmlElement("ForeclosureCaseDismissedReasonTypeOtherDescription", Order = 7)]
        public MISMOString ForeclosureCaseDismissedReasonTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ForeclosureCaseDismissedReasonTypeOtherDescriptionSpecified
        {
            get { return this.ForeclosureCaseDismissedReasonTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("ForeclosureCaseIdentifier", Order = 8)]
        public MISMOIdentifier ForeclosureCaseIdentifier { get; set; }
    
        [XmlIgnore]
        public bool ForeclosureCaseIdentifierSpecified
        {
            get { return this.ForeclosureCaseIdentifier != null; }
            set { }
        }
    
        [XmlElement("ForeclosureDelayIndicator", Order = 9)]
        public MISMOIndicator ForeclosureDelayIndicator { get; set; }
    
        [XmlIgnore]
        public bool ForeclosureDelayIndicatorSpecified
        {
            get { return this.ForeclosureDelayIndicator != null; }
            set { }
        }
    
        [XmlElement("ForeclosureFeeAmount", Order = 10)]
        public MISMOAmount ForeclosureFeeAmount { get; set; }
    
        [XmlIgnore]
        public bool ForeclosureFeeAmountSpecified
        {
            get { return this.ForeclosureFeeAmount != null; }
            set { }
        }
    
        [XmlElement("ForeclosureMethodType", Order = 11)]
        public MISMOEnum<ForeclosureMethodBase> ForeclosureMethodType { get; set; }
    
        [XmlIgnore]
        public bool ForeclosureMethodTypeSpecified
        {
            get { return this.ForeclosureMethodType != null; }
            set { }
        }
    
        [XmlElement("ForeclosureMethodTypeOtherDescription", Order = 12)]
        public MISMOString ForeclosureMethodTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ForeclosureMethodTypeOtherDescriptionSpecified
        {
            get { return this.ForeclosureMethodTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("ForeclosurePerformanceMaximumDaysCount", Order = 13)]
        public MISMOCount ForeclosurePerformanceMaximumDaysCount { get; set; }
    
        [XmlIgnore]
        public bool ForeclosurePerformanceMaximumDaysCountSpecified
        {
            get { return this.ForeclosurePerformanceMaximumDaysCount != null; }
            set { }
        }
    
        [XmlElement("ForeclosureProcedureEndedDate", Order = 14)]
        public MISMODate ForeclosureProcedureEndedDate { get; set; }
    
        [XmlIgnore]
        public bool ForeclosureProcedureEndedDateSpecified
        {
            get { return this.ForeclosureProcedureEndedDate != null; }
            set { }
        }
    
        [XmlElement("ForeclosureSaleProceedsAmount", Order = 15)]
        public MISMOAmount ForeclosureSaleProceedsAmount { get; set; }
    
        [XmlIgnore]
        public bool ForeclosureSaleProceedsAmountSpecified
        {
            get { return this.ForeclosureSaleProceedsAmount != null; }
            set { }
        }
    
        [XmlElement("ForeclosureSaleProjectedDate", Order = 16)]
        public MISMODate ForeclosureSaleProjectedDate { get; set; }
    
        [XmlIgnore]
        public bool ForeclosureSaleProjectedDateSpecified
        {
            get { return this.ForeclosureSaleProjectedDate != null; }
            set { }
        }
    
        [XmlElement("ForeclosureSaleTotalSubordinateLienholderPayoutAmount", Order = 17)]
        public MISMOAmount ForeclosureSaleTotalSubordinateLienholderPayoutAmount { get; set; }
    
        [XmlIgnore]
        public bool ForeclosureSaleTotalSubordinateLienholderPayoutAmountSpecified
        {
            get { return this.ForeclosureSaleTotalSubordinateLienholderPayoutAmount != null; }
            set { }
        }
    
        [XmlElement("IRSLienNotificationIndicator", Order = 18)]
        public MISMOIndicator IRSLienNotificationIndicator { get; set; }
    
        [XmlIgnore]
        public bool IRSLienNotificationIndicatorSpecified
        {
            get { return this.IRSLienNotificationIndicator != null; }
            set { }
        }
    
        [XmlElement("RedemptionExercisedIndicator", Order = 19)]
        public MISMOIndicator RedemptionExercisedIndicator { get; set; }
    
        [XmlIgnore]
        public bool RedemptionExercisedIndicatorSpecified
        {
            get { return this.RedemptionExercisedIndicator != null; }
            set { }
        }
    
        [XmlElement("RedemptionPeriodExpirationDate", Order = 20)]
        public MISMODate RedemptionPeriodExpirationDate { get; set; }
    
        [XmlIgnore]
        public bool RedemptionPeriodExpirationDateSpecified
        {
            get { return this.RedemptionPeriodExpirationDate != null; }
            set { }
        }
    
        [XmlElement("RedemptionPeriodTermMonthsCount", Order = 21)]
        public MISMOCount RedemptionPeriodTermMonthsCount { get; set; }
    
        [XmlIgnore]
        public bool RedemptionPeriodTermMonthsCountSpecified
        {
            get { return this.RedemptionPeriodTermMonthsCount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 22)]
        public FORECLOSURE_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
