namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CREDIT_LIABILITY_PAYMENT_PATTERN
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditLiabilityPaymentPatternDataTextSpecified
                    || this.CreditLiabilityPaymentPatternStartDateSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CreditLiabilityPaymentPatternDataText", Order = 0)]
        public MISMOString CreditLiabilityPaymentPatternDataText { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityPaymentPatternDataTextSpecified
        {
            get { return this.CreditLiabilityPaymentPatternDataText != null; }
            set { }
        }
    
        [XmlElement("CreditLiabilityPaymentPatternStartDate", Order = 1)]
        public MISMODate CreditLiabilityPaymentPatternStartDate { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityPaymentPatternStartDateSpecified
        {
            get { return this.CreditLiabilityPaymentPatternStartDate != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public CREDIT_LIABILITY_PAYMENT_PATTERN_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
