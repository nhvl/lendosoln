namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class OTHER_LOAN_CONSIDERATIONS_AND_DISCLOSURES_ITEMS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.OtherLoanConsiderationsAndDisclosuresItemListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("OTHER_LOAN_CONSIDERATIONS_AND_DISCLOSURES_ITEM", Order = 0)]
        public List<OTHER_LOAN_CONSIDERATIONS_AND_DISCLOSURES_ITEM> OtherLoanConsiderationsAndDisclosuresItemList { get; set; } = new List<OTHER_LOAN_CONSIDERATIONS_AND_DISCLOSURES_ITEM>();
    
        [XmlIgnore]
        public bool OtherLoanConsiderationsAndDisclosuresItemListSpecified
        {
            get { return this.OtherLoanConsiderationsAndDisclosuresItemList != null && this.OtherLoanConsiderationsAndDisclosuresItemList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public OTHER_LOAN_CONSIDERATIONS_AND_DISCLOSURES_ITEMS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
