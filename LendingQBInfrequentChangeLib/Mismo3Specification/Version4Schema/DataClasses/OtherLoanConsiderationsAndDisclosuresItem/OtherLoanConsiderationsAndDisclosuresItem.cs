namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class OTHER_LOAN_CONSIDERATIONS_AND_DISCLOSURES_ITEM
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.LoanConsiderationDisclosureStatementDescriptionSpecified
                    || this.LoanConsiderationDisclosureStatementTypeSpecified
                    || this.LoanConsiderationDisclosureStatementTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("LoanConsiderationDisclosureStatementDescription", Order = 0)]
        public MISMOString LoanConsiderationDisclosureStatementDescription { get; set; }
    
        [XmlIgnore]
        public bool LoanConsiderationDisclosureStatementDescriptionSpecified
        {
            get { return this.LoanConsiderationDisclosureStatementDescription != null; }
            set { }
        }
    
        [XmlElement("LoanConsiderationDisclosureStatementType", Order = 1)]
        public MISMOEnum<LoanConsiderationDisclosureStatementBase> LoanConsiderationDisclosureStatementType { get; set; }
    
        [XmlIgnore]
        public bool LoanConsiderationDisclosureStatementTypeSpecified
        {
            get { return this.LoanConsiderationDisclosureStatementType != null; }
            set { }
        }
    
        [XmlElement("LoanConsiderationDisclosureStatementTypeOtherDescription", Order = 2)]
        public MISMOString LoanConsiderationDisclosureStatementTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool LoanConsiderationDisclosureStatementTypeOtherDescriptionSpecified
        {
            get { return this.LoanConsiderationDisclosureStatementTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public OTHER_LOAN_CONSIDERATIONS_AND_DISCLOSURES_ITEM_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
