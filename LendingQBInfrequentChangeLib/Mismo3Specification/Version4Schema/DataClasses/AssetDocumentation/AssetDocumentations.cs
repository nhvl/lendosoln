namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class ASSET_DOCUMENTATIONS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AssetDocumentationListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ASSET_DOCUMENTATION", Order = 0)]
        public List<ASSET_DOCUMENTATION> AssetDocumentationList { get; set; } = new List<ASSET_DOCUMENTATION>();
    
        [XmlIgnore]
        public bool AssetDocumentationListSpecified
        {
            get { return this.AssetDocumentationList != null && this.AssetDocumentationList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public ASSET_DOCUMENTATIONS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
