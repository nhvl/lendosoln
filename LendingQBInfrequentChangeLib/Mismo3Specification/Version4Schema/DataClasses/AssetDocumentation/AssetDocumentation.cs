namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class ASSET_DOCUMENTATION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AssetDocumentTypeSpecified
                    || this.AssetDocumentTypeOtherDescriptionSpecified
                    || this.AssetVerificationRangeCountSpecified
                    || this.AssetVerificationRangeTypeSpecified
                    || this.AssetVerificationRangeTypeOtherDescriptionSpecified
                    || this.DocumentationStateTypeSpecified
                    || this.DocumentationStateTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AssetDocumentType", Order = 0)]
        public MISMOEnum<AssetDocumentBase> AssetDocumentType { get; set; }
    
        [XmlIgnore]
        public bool AssetDocumentTypeSpecified
        {
            get { return this.AssetDocumentType != null; }
            set { }
        }
    
        [XmlElement("AssetDocumentTypeOtherDescription", Order = 1)]
        public MISMOString AssetDocumentTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool AssetDocumentTypeOtherDescriptionSpecified
        {
            get { return this.AssetDocumentTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("AssetVerificationRangeCount", Order = 2)]
        public MISMOCount AssetVerificationRangeCount { get; set; }
    
        [XmlIgnore]
        public bool AssetVerificationRangeCountSpecified
        {
            get { return this.AssetVerificationRangeCount != null; }
            set { }
        }
    
        [XmlElement("AssetVerificationRangeType", Order = 3)]
        public MISMOEnum<VerificationRangeBase> AssetVerificationRangeType { get; set; }
    
        [XmlIgnore]
        public bool AssetVerificationRangeTypeSpecified
        {
            get { return this.AssetVerificationRangeType != null; }
            set { }
        }
    
        [XmlElement("AssetVerificationRangeTypeOtherDescription", Order = 4)]
        public MISMOString AssetVerificationRangeTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool AssetVerificationRangeTypeOtherDescriptionSpecified
        {
            get { return this.AssetVerificationRangeTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("DocumentationStateType", Order = 5)]
        public MISMOEnum<DocumentationStateBase> DocumentationStateType { get; set; }
    
        [XmlIgnore]
        public bool DocumentationStateTypeSpecified
        {
            get { return this.DocumentationStateType != null; }
            set { }
        }
    
        [XmlElement("DocumentationStateTypeOtherDescription", Order = 6)]
        public MISMOString DocumentationStateTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool DocumentationStateTypeOtherDescriptionSpecified
        {
            get { return this.DocumentationStateTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 7)]
        public ASSET_DOCUMENTATION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
