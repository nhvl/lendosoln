namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class NOTARY_CERTIFICATE_SIGNER_IDENTIFICATION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.NotaryCertificateSignerIdentificationDescriptionSpecified
                    || this.NotaryCertificateSignerIdentificationTypeSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("NotaryCertificateSignerIdentificationDescription", Order = 0)]
        public MISMOString NotaryCertificateSignerIdentificationDescription { get; set; }
    
        [XmlIgnore]
        public bool NotaryCertificateSignerIdentificationDescriptionSpecified
        {
            get { return this.NotaryCertificateSignerIdentificationDescription != null; }
            set { }
        }
    
        [XmlElement("NotaryCertificateSignerIdentificationType", Order = 1)]
        public MISMOEnum<NotaryCertificateSignerIdentificationBase> NotaryCertificateSignerIdentificationType { get; set; }
    
        [XmlIgnore]
        public bool NotaryCertificateSignerIdentificationTypeSpecified
        {
            get { return this.NotaryCertificateSignerIdentificationType != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public NOTARY_CERTIFICATE_SIGNER_IDENTIFICATION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
