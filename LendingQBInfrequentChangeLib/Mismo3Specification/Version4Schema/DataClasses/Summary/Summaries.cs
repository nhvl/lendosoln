namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class SUMMARIES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.SummaryListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("SUMMARY", Order = 0)]
        public List<SUMMARY> SummaryList { get; set; } = new List<SUMMARY>();
    
        [XmlIgnore]
        public bool SummaryListSpecified
        {
            get { return this.SummaryList != null && this.SummaryList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public SUMMARIES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
