namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class SUMMARY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.SummaryAmountSpecified
                    || this.SummaryAmountTypeSpecified
                    || this.SummaryAmountTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("SummaryAmount", Order = 0)]
        public MISMOAmount SummaryAmount { get; set; }
    
        [XmlIgnore]
        public bool SummaryAmountSpecified
        {
            get { return this.SummaryAmount != null; }
            set { }
        }
    
        [XmlElement("SummaryAmountType", Order = 1)]
        public MISMOEnum<SummaryAmountBase> SummaryAmountType { get; set; }
    
        [XmlIgnore]
        public bool SummaryAmountTypeSpecified
        {
            get { return this.SummaryAmountType != null; }
            set { }
        }
    
        [XmlElement("SummaryAmountTypeOtherDescription", Order = 2)]
        public MISMOString SummaryAmountTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool SummaryAmountTypeOtherDescriptionSpecified
        {
            get { return this.SummaryAmountTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public SUMMARY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
