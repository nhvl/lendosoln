namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CLOSING_AGENT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ClosingAgentTypeSpecified
                    || this.ClosingAgentTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ClosingAgentType", Order = 0)]
        public MISMOEnum<ClosingAgentBase> ClosingAgentType { get; set; }
    
        [XmlIgnore]
        public bool ClosingAgentTypeSpecified
        {
            get { return this.ClosingAgentType != null; }
            set { }
        }
    
        [XmlElement("ClosingAgentTypeOtherDescription", Order = 1)]
        public MISMOString ClosingAgentTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ClosingAgentTypeOtherDescriptionSpecified
        {
            get { return this.ClosingAgentTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public CLOSING_AGENT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
