namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class SUPPORTING_RECORD_SET_DELIVERY_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DeliveryDescriptionSpecified
                    || this.DeliveryInitiatedDatetimeSpecified
                    || this.DeliveryMethodTypeSpecified
                    || this.DeliveryMethodTypeAdditionalDescriptionSpecified
                    || this.DeliveryMethodTypeOtherDescriptionSpecified
                    || this.DeliveryReceivedDatetimeSpecified
                    || this.DeliveryReceivedStatusTypeSpecified
                    || this.DeliveryReceivedStatusTypeAdditionalDescriptionSpecified
                    || this.DeliveryReceivedStatusTypeOtherDescriptionSpecified
                    || this.DeliveryTrackingIdentifierSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DeliveryDescription", Order = 0)]
        public MISMOString DeliveryDescription { get; set; }
    
        [XmlIgnore]
        public bool DeliveryDescriptionSpecified
        {
            get { return this.DeliveryDescription != null; }
            set { }
        }
    
        [XmlElement("DeliveryInitiatedDatetime", Order = 1)]
        public MISMODatetime DeliveryInitiatedDatetime { get; set; }
    
        [XmlIgnore]
        public bool DeliveryInitiatedDatetimeSpecified
        {
            get { return this.DeliveryInitiatedDatetime != null; }
            set { }
        }
    
        [XmlElement("DeliveryMethodType", Order = 2)]
        public MISMOEnum<DeliveryMethodBase> DeliveryMethodType { get; set; }
    
        [XmlIgnore]
        public bool DeliveryMethodTypeSpecified
        {
            get { return this.DeliveryMethodType != null; }
            set { }
        }
    
        [XmlElement("DeliveryMethodTypeAdditionalDescription", Order = 3)]
        public MISMOString DeliveryMethodTypeAdditionalDescription { get; set; }
    
        [XmlIgnore]
        public bool DeliveryMethodTypeAdditionalDescriptionSpecified
        {
            get { return this.DeliveryMethodTypeAdditionalDescription != null; }
            set { }
        }
    
        [XmlElement("DeliveryMethodTypeOtherDescription", Order = 4)]
        public MISMOString DeliveryMethodTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool DeliveryMethodTypeOtherDescriptionSpecified
        {
            get { return this.DeliveryMethodTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("DeliveryReceivedDatetime", Order = 5)]
        public MISMODatetime DeliveryReceivedDatetime { get; set; }
    
        [XmlIgnore]
        public bool DeliveryReceivedDatetimeSpecified
        {
            get { return this.DeliveryReceivedDatetime != null; }
            set { }
        }
    
        [XmlElement("DeliveryReceivedStatusType", Order = 6)]
        public MISMOEnum<DeliveryReceivedStatusBase> DeliveryReceivedStatusType { get; set; }
    
        [XmlIgnore]
        public bool DeliveryReceivedStatusTypeSpecified
        {
            get { return this.DeliveryReceivedStatusType != null; }
            set { }
        }
    
        [XmlElement("DeliveryReceivedStatusTypeAdditionalDescription", Order = 7)]
        public MISMOString DeliveryReceivedStatusTypeAdditionalDescription { get; set; }
    
        [XmlIgnore]
        public bool DeliveryReceivedStatusTypeAdditionalDescriptionSpecified
        {
            get { return this.DeliveryReceivedStatusTypeAdditionalDescription != null; }
            set { }
        }
    
        [XmlElement("DeliveryReceivedStatusTypeOtherDescription", Order = 8)]
        public MISMOString DeliveryReceivedStatusTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool DeliveryReceivedStatusTypeOtherDescriptionSpecified
        {
            get { return this.DeliveryReceivedStatusTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("DeliveryTrackingIdentifier", Order = 9)]
        public MISMOIdentifier DeliveryTrackingIdentifier { get; set; }
    
        [XmlIgnore]
        public bool DeliveryTrackingIdentifierSpecified
        {
            get { return this.DeliveryTrackingIdentifier != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 10)]
        public SUPPORTING_RECORD_SET_DELIVERY_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
