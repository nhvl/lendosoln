namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class SUPPORTING_RECORD_SET_DELIVERIES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.SupportingRecordSetDeliveryListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("SUPPORTING_RECORD_SET_DELIVERY", Order = 0)]
        public List<SUPPORTING_RECORD_SET_DELIVERY> SupportingRecordSetDeliveryList { get; set; } = new List<SUPPORTING_RECORD_SET_DELIVERY>();
    
        [XmlIgnore]
        public bool SupportingRecordSetDeliveryListSpecified
        {
            get { return this.SupportingRecordSetDeliveryList != null && this.SupportingRecordSetDeliveryList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public SUPPORTING_RECORD_SET_DELIVERIES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
