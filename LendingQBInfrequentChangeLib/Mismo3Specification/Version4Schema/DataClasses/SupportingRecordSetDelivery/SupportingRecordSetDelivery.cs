namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class SUPPORTING_RECORD_SET_DELIVERY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.SupportingRecordSetDeliveryDetailSpecified
                    || this.SupportingRecordSetDeliveryEvidenceSpecified
                    || this.SupportingRecordSetDeliveryParticipantsSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("SUPPORTING_RECORD_SET_DELIVERY_DETAIL", Order = 0)]
        public SUPPORTING_RECORD_SET_DELIVERY_DETAIL SupportingRecordSetDeliveryDetail { get; set; }
    
        [XmlIgnore]
        public bool SupportingRecordSetDeliveryDetailSpecified
        {
            get { return this.SupportingRecordSetDeliveryDetail != null && this.SupportingRecordSetDeliveryDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SUPPORTING_RECORD_SET_DELIVERY_EVIDENCE", Order = 1)]
        public SUPPORTING_RECORD_SET_DELIVERY_EVIDENCE SupportingRecordSetDeliveryEvidence { get; set; }
    
        [XmlIgnore]
        public bool SupportingRecordSetDeliveryEvidenceSpecified
        {
            get { return this.SupportingRecordSetDeliveryEvidence != null && this.SupportingRecordSetDeliveryEvidence.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SUPPORTING_RECORD_SET_DELIVERY_PARTICIPANTS", Order = 2)]
        public SUPPORTING_RECORD_SET_DELIVERY_PARTICIPANTS SupportingRecordSetDeliveryParticipants { get; set; }
    
        [XmlIgnore]
        public bool SupportingRecordSetDeliveryParticipantsSpecified
        {
            get { return this.SupportingRecordSetDeliveryParticipants != null && this.SupportingRecordSetDeliveryParticipants.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public SUPPORTING_RECORD_SET_DELIVERY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
