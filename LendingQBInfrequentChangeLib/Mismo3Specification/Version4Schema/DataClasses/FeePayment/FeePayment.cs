namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class FEE_PAYMENT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.FeeActualPaymentAmountSpecified
                    || this.FeeEstimatedPaymentAmountSpecified
                    || this.FeePaymentAllowableFHAClosingCostIndicatorSpecified
                    || this.FeePaymentCollectedByTypeSpecified
                    || this.FeePaymentCollectedByTypeOtherDescriptionSpecified
                    || this.FeePaymentNetDueAmountSpecified
                    || this.FeePaymentPaidByTypeSpecified
                    || this.FeePaymentPaidByTypeThirdPartyNameSpecified
                    || this.FeePaymentPaidOutsideOfClosingIndicatorSpecified
                    || this.FeePaymentPercentSpecified
                    || this.FeePaymentProcessTypeSpecified
                    || this.FeePaymentRefundableAmountSpecified
                    || this.FeePaymentRefundableConditionsDescriptionSpecified
                    || this.FeePaymentRefundableIndicatorSpecified
                    || this.FeePaymentResponsiblePartyTypeSpecified
                    || this.FeePaymentResponsiblePartyTypeOtherDescriptionSpecified
                    || this.PaymentFinancedIndicatorSpecified
                    || this.PaymentIncludedInAPRIndicatorSpecified
                    || this.PaymentIncludedInJurisdictionHighCostIndicatorSpecified
                    || this.PrepaidFinanceChargeIndicatorSpecified
                    || this.RegulationZPointsAndFeesIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("FeeActualPaymentAmount", Order = 0)]
        public MISMOAmount FeeActualPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool FeeActualPaymentAmountSpecified
        {
            get { return this.FeeActualPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("FeeEstimatedPaymentAmount", Order = 1)]
        public MISMOAmount FeeEstimatedPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool FeeEstimatedPaymentAmountSpecified
        {
            get { return this.FeeEstimatedPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("FeePaymentAllowableFHAClosingCostIndicator", Order = 2)]
        public MISMOIndicator FeePaymentAllowableFHAClosingCostIndicator { get; set; }
    
        [XmlIgnore]
        public bool FeePaymentAllowableFHAClosingCostIndicatorSpecified
        {
            get { return this.FeePaymentAllowableFHAClosingCostIndicator != null; }
            set { }
        }
    
        [XmlElement("FeePaymentCollectedByType", Order = 3)]
        public MISMOEnum<FeePaymentCollectedByBase> FeePaymentCollectedByType { get; set; }
    
        [XmlIgnore]
        public bool FeePaymentCollectedByTypeSpecified
        {
            get { return this.FeePaymentCollectedByType != null; }
            set { }
        }
    
        [XmlElement("FeePaymentCollectedByTypeOtherDescription", Order = 4)]
        public MISMOString FeePaymentCollectedByTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool FeePaymentCollectedByTypeOtherDescriptionSpecified
        {
            get { return this.FeePaymentCollectedByTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("FeePaymentNetDueAmount", Order = 5)]
        public MISMOAmount FeePaymentNetDueAmount { get; set; }
    
        [XmlIgnore]
        public bool FeePaymentNetDueAmountSpecified
        {
            get { return this.FeePaymentNetDueAmount != null; }
            set { }
        }
    
        [XmlElement("FeePaymentPaidByType", Order = 6)]
        public MISMOEnum<FeePaymentPaidByBase> FeePaymentPaidByType { get; set; }
    
        [XmlIgnore]
        public bool FeePaymentPaidByTypeSpecified
        {
            get { return this.FeePaymentPaidByType != null; }
            set { }
        }
    
        [XmlElement("FeePaymentPaidByTypeThirdPartyName", Order = 7)]
        public MISMOString FeePaymentPaidByTypeThirdPartyName { get; set; }
    
        [XmlIgnore]
        public bool FeePaymentPaidByTypeThirdPartyNameSpecified
        {
            get { return this.FeePaymentPaidByTypeThirdPartyName != null; }
            set { }
        }
    
        [XmlElement("FeePaymentPaidOutsideOfClosingIndicator", Order = 8)]
        public MISMOIndicator FeePaymentPaidOutsideOfClosingIndicator { get; set; }
    
        [XmlIgnore]
        public bool FeePaymentPaidOutsideOfClosingIndicatorSpecified
        {
            get { return this.FeePaymentPaidOutsideOfClosingIndicator != null; }
            set { }
        }
    
        [XmlElement("FeePaymentPercent", Order = 9)]
        public MISMOPercent FeePaymentPercent { get; set; }
    
        [XmlIgnore]
        public bool FeePaymentPercentSpecified
        {
            get { return this.FeePaymentPercent != null; }
            set { }
        }
    
        [XmlElement("FeePaymentProcessType", Order = 10)]
        public MISMOEnum<FeePaymentProcessBase> FeePaymentProcessType { get; set; }
    
        [XmlIgnore]
        public bool FeePaymentProcessTypeSpecified
        {
            get { return this.FeePaymentProcessType != null; }
            set { }
        }
    
        [XmlElement("FeePaymentRefundableAmount", Order = 11)]
        public MISMOAmount FeePaymentRefundableAmount { get; set; }
    
        [XmlIgnore]
        public bool FeePaymentRefundableAmountSpecified
        {
            get { return this.FeePaymentRefundableAmount != null; }
            set { }
        }
    
        [XmlElement("FeePaymentRefundableConditionsDescription", Order = 12)]
        public MISMOString FeePaymentRefundableConditionsDescription { get; set; }
    
        [XmlIgnore]
        public bool FeePaymentRefundableConditionsDescriptionSpecified
        {
            get { return this.FeePaymentRefundableConditionsDescription != null; }
            set { }
        }
    
        [XmlElement("FeePaymentRefundableIndicator", Order = 13)]
        public MISMOIndicator FeePaymentRefundableIndicator { get; set; }
    
        [XmlIgnore]
        public bool FeePaymentRefundableIndicatorSpecified
        {
            get { return this.FeePaymentRefundableIndicator != null; }
            set { }
        }
    
        [XmlElement("FeePaymentResponsiblePartyType", Order = 14)]
        public MISMOEnum<FeePaymentResponsiblePartyBase> FeePaymentResponsiblePartyType { get; set; }
    
        [XmlIgnore]
        public bool FeePaymentResponsiblePartyTypeSpecified
        {
            get { return this.FeePaymentResponsiblePartyType != null; }
            set { }
        }
    
        [XmlElement("FeePaymentResponsiblePartyTypeOtherDescription", Order = 15)]
        public MISMOString FeePaymentResponsiblePartyTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool FeePaymentResponsiblePartyTypeOtherDescriptionSpecified
        {
            get { return this.FeePaymentResponsiblePartyTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("PaymentFinancedIndicator", Order = 16)]
        public MISMOIndicator PaymentFinancedIndicator { get; set; }
    
        [XmlIgnore]
        public bool PaymentFinancedIndicatorSpecified
        {
            get { return this.PaymentFinancedIndicator != null; }
            set { }
        }
    
        [XmlElement("PaymentIncludedInAPRIndicator", Order = 17)]
        public MISMOIndicator PaymentIncludedInAPRIndicator { get; set; }
    
        [XmlIgnore]
        public bool PaymentIncludedInAPRIndicatorSpecified
        {
            get { return this.PaymentIncludedInAPRIndicator != null; }
            set { }
        }
    
        [XmlElement("PaymentIncludedInJurisdictionHighCostIndicator", Order = 18)]
        public MISMOIndicator PaymentIncludedInJurisdictionHighCostIndicator { get; set; }
    
        [XmlIgnore]
        public bool PaymentIncludedInJurisdictionHighCostIndicatorSpecified
        {
            get { return this.PaymentIncludedInJurisdictionHighCostIndicator != null; }
            set { }
        }
    
        [XmlElement("PrepaidFinanceChargeIndicator", Order = 19)]
        public MISMOIndicator PrepaidFinanceChargeIndicator { get; set; }
    
        [XmlIgnore]
        public bool PrepaidFinanceChargeIndicatorSpecified
        {
            get { return this.PrepaidFinanceChargeIndicator != null; }
            set { }
        }
    
        [XmlElement("RegulationZPointsAndFeesIndicator", Order = 20)]
        public MISMOIndicator RegulationZPointsAndFeesIndicator { get; set; }
    
        [XmlIgnore]
        public bool RegulationZPointsAndFeesIndicatorSpecified
        {
            get { return this.RegulationZPointsAndFeesIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 21)]
        public FEE_PAYMENT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
