namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class FEE_PAYMENTS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.FeePaymentListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("FEE_PAYMENT", Order = 0)]
        public List<FEE_PAYMENT> FeePaymentList { get; set; } = new List<FEE_PAYMENT>();
    
        [XmlIgnore]
        public bool FeePaymentListSpecified
        {
            get { return this.FeePaymentList != null && this.FeePaymentList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public FEE_PAYMENTS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
