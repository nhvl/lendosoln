namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class VIEW_FILES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ViewFileListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("VIEW_FILE", Order = 0)]
        public List<VIEW_FILE> ViewFileList { get; set; } = new List<VIEW_FILE>();
    
        [XmlIgnore]
        public bool ViewFileListSpecified
        {
            get { return this.ViewFileList != null && this.ViewFileList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public VIEW_FILES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
