namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class SITE_FEATURE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.IrrigationSystemSourceTypeSpecified
                    || this.IrrigationSystemSourceTypeOtherDescriptionSpecified
                    || this.LandscapeLightingPowerSourceTypeSpecified
                    || this.LandscapeLightingPowerSourceTypeOtherDescriptionSpecified
                    || this.LandscapingTypeSpecified
                    || this.LandscapingTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("IrrigationSystemSourceType", Order = 0)]
        public MISMOEnum<IrrigationSystemSourceBase> IrrigationSystemSourceType { get; set; }
    
        [XmlIgnore]
        public bool IrrigationSystemSourceTypeSpecified
        {
            get { return this.IrrigationSystemSourceType != null; }
            set { }
        }
    
        [XmlElement("IrrigationSystemSourceTypeOtherDescription", Order = 1)]
        public MISMOString IrrigationSystemSourceTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool IrrigationSystemSourceTypeOtherDescriptionSpecified
        {
            get { return this.IrrigationSystemSourceTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("LandscapeLightingPowerSourceType", Order = 2)]
        public MISMOEnum<LandscapeLightingPowerSourceBase> LandscapeLightingPowerSourceType { get; set; }
    
        [XmlIgnore]
        public bool LandscapeLightingPowerSourceTypeSpecified
        {
            get { return this.LandscapeLightingPowerSourceType != null; }
            set { }
        }
    
        [XmlElement("LandscapeLightingPowerSourceTypeOtherDescription", Order = 3)]
        public MISMOString LandscapeLightingPowerSourceTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool LandscapeLightingPowerSourceTypeOtherDescriptionSpecified
        {
            get { return this.LandscapeLightingPowerSourceTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("LandscapingType", Order = 4)]
        public MISMOEnum<LandscapingBase> LandscapingType { get; set; }
    
        [XmlIgnore]
        public bool LandscapingTypeSpecified
        {
            get { return this.LandscapingType != null; }
            set { }
        }
    
        [XmlElement("LandscapingTypeOtherDescription", Order = 5)]
        public MISMOString LandscapingTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool LandscapingTypeOtherDescriptionSpecified
        {
            get { return this.LandscapingTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 6)]
        public SITE_FEATURE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
