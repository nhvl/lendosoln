namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class SITE_FEATURES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.SiteFeatureListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("SITE_FEATURE", Order = 0)]
        public List<SITE_FEATURE> SiteFeatureList { get; set; } = new List<SITE_FEATURE>();
    
        [XmlIgnore]
        public bool SiteFeatureListSpecified
        {
            get { return this.SiteFeatureList != null && this.SiteFeatureList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public SITE_FEATURES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
