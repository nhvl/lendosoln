namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class TITLE_POLICY_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AgentPortionAmountSpecified
                    || this.BasisOfInsuranceDiscountAmountSpecified
                    || this.GrossPremiumAmountSpecified
                    || this.NamedInsuredDescriptionSpecified
                    || this.NamedInsuredTypeSpecified
                    || this.TitleAssociationTypeSpecified
                    || this.TitleAssociationTypeOtherDescriptionSpecified
                    || this.TitleClearanceGradeIdentifierSpecified
                    || this.TitleInsuranceAmountSpecified
                    || this.TitlePolicyCommentDescriptionSpecified
                    || this.TitlePolicyEffectiveDateSpecified
                    || this.TitlePolicyIdentifierSpecified
                    || this.TitlePolicyWitnessClauseDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AgentPortionAmount", Order = 0)]
        public MISMOAmount AgentPortionAmount { get; set; }
    
        [XmlIgnore]
        public bool AgentPortionAmountSpecified
        {
            get { return this.AgentPortionAmount != null; }
            set { }
        }
    
        [XmlElement("BasisOfInsuranceDiscountAmount", Order = 1)]
        public MISMOAmount BasisOfInsuranceDiscountAmount { get; set; }
    
        [XmlIgnore]
        public bool BasisOfInsuranceDiscountAmountSpecified
        {
            get { return this.BasisOfInsuranceDiscountAmount != null; }
            set { }
        }
    
        [XmlElement("GrossPremiumAmount", Order = 2)]
        public MISMOAmount GrossPremiumAmount { get; set; }
    
        [XmlIgnore]
        public bool GrossPremiumAmountSpecified
        {
            get { return this.GrossPremiumAmount != null; }
            set { }
        }
    
        [XmlElement("NamedInsuredDescription", Order = 3)]
        public MISMOString NamedInsuredDescription { get; set; }
    
        [XmlIgnore]
        public bool NamedInsuredDescriptionSpecified
        {
            get { return this.NamedInsuredDescription != null; }
            set { }
        }
    
        [XmlElement("NamedInsuredType", Order = 4)]
        public MISMOEnum<NamedInsuredBase> NamedInsuredType { get; set; }
    
        [XmlIgnore]
        public bool NamedInsuredTypeSpecified
        {
            get { return this.NamedInsuredType != null; }
            set { }
        }
    
        [XmlElement("TitleAssociationType", Order = 5)]
        public MISMOEnum<TitleAssociationBase> TitleAssociationType { get; set; }
    
        [XmlIgnore]
        public bool TitleAssociationTypeSpecified
        {
            get { return this.TitleAssociationType != null; }
            set { }
        }
    
        [XmlElement("TitleAssociationTypeOtherDescription", Order = 6)]
        public MISMOString TitleAssociationTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool TitleAssociationTypeOtherDescriptionSpecified
        {
            get { return this.TitleAssociationTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("TitleClearanceGradeIdentifier", Order = 7)]
        public MISMOIdentifier TitleClearanceGradeIdentifier { get; set; }
    
        [XmlIgnore]
        public bool TitleClearanceGradeIdentifierSpecified
        {
            get { return this.TitleClearanceGradeIdentifier != null; }
            set { }
        }
    
        [XmlElement("TitleInsuranceAmount", Order = 8)]
        public MISMOAmount TitleInsuranceAmount { get; set; }
    
        [XmlIgnore]
        public bool TitleInsuranceAmountSpecified
        {
            get { return this.TitleInsuranceAmount != null; }
            set { }
        }
    
        [XmlElement("TitlePolicyCommentDescription", Order = 9)]
        public MISMOString TitlePolicyCommentDescription { get; set; }
    
        [XmlIgnore]
        public bool TitlePolicyCommentDescriptionSpecified
        {
            get { return this.TitlePolicyCommentDescription != null; }
            set { }
        }
    
        [XmlElement("TitlePolicyEffectiveDate", Order = 10)]
        public MISMODate TitlePolicyEffectiveDate { get; set; }
    
        [XmlIgnore]
        public bool TitlePolicyEffectiveDateSpecified
        {
            get { return this.TitlePolicyEffectiveDate != null; }
            set { }
        }
    
        [XmlElement("TitlePolicyIdentifier", Order = 11)]
        public MISMOIdentifier TitlePolicyIdentifier { get; set; }
    
        [XmlIgnore]
        public bool TitlePolicyIdentifierSpecified
        {
            get { return this.TitlePolicyIdentifier != null; }
            set { }
        }
    
        [XmlElement("TitlePolicyWitnessClauseDescription", Order = 12)]
        public MISMOString TitlePolicyWitnessClauseDescription { get; set; }
    
        [XmlIgnore]
        public bool TitlePolicyWitnessClauseDescriptionSpecified
        {
            get { return this.TitlePolicyWitnessClauseDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 13)]
        public TITLE_POLICY_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
