namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class TITLE_POLICY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExecutionsSpecified
                    || this.LegalAndVestingsSpecified
                    || this.LoansSpecified
                    || this.PartiesSpecified
                    || this.PropertySpecified
                    || this.RecordingEndorsementInformationSpecified
                    || this.TitleAdditionalExceptionsSpecified
                    || this.TitleEndorsementsSpecified
                    || this.TitlePolicyDetailSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("EXECUTIONS", Order = 0)]
        public EXECUTIONS Executions { get; set; }
    
        [XmlIgnore]
        public bool ExecutionsSpecified
        {
            get { return this.Executions != null && this.Executions.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("LEGAL_AND_VESTINGS", Order = 1)]
        public LEGAL_AND_VESTINGS LegalAndVestings { get; set; }
    
        [XmlIgnore]
        public bool LegalAndVestingsSpecified
        {
            get { return this.LegalAndVestings != null && this.LegalAndVestings.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("LOANS", Order = 2)]
        public LOANS Loans { get; set; }
    
        [XmlIgnore]
        public bool LoansSpecified
        {
            get { return this.Loans != null && this.Loans.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PARTIES", Order = 3)]
        public PARTIES Parties { get; set; }
    
        [XmlIgnore]
        public bool PartiesSpecified
        {
            get { return this.Parties != null && this.Parties.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PROPERTY", Order = 4)]
        public PROPERTY Property { get; set; }
    
        [XmlIgnore]
        public bool PropertySpecified
        {
            get { return this.Property != null && this.Property.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("RECORDING_ENDORSEMENT_INFORMATION", Order = 5)]
        public RECORDING_ENDORSEMENT_INFORMATION RecordingEndorsementInformation { get; set; }
    
        [XmlIgnore]
        public bool RecordingEndorsementInformationSpecified
        {
            get { return this.RecordingEndorsementInformation != null && this.RecordingEndorsementInformation.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("TITLE_ADDITIONAL_EXCEPTIONS", Order = 6)]
        public TITLE_ADDITIONAL_EXCEPTIONS TitleAdditionalExceptions { get; set; }
    
        [XmlIgnore]
        public bool TitleAdditionalExceptionsSpecified
        {
            get { return this.TitleAdditionalExceptions != null && this.TitleAdditionalExceptions.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("TITLE_ENDORSEMENTS", Order = 7)]
        public TITLE_ENDORSEMENTS TitleEndorsements { get; set; }
    
        [XmlIgnore]
        public bool TitleEndorsementsSpecified
        {
            get { return this.TitleEndorsements != null && this.TitleEndorsements.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("TITLE_POLICY_DETAIL", Order = 8)]
        public TITLE_POLICY_DETAIL TitlePolicyDetail { get; set; }
    
        [XmlIgnore]
        public bool TitlePolicyDetailSpecified
        {
            get { return this.TitlePolicyDetail != null && this.TitlePolicyDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 9)]
        public TITLE_POLICY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
