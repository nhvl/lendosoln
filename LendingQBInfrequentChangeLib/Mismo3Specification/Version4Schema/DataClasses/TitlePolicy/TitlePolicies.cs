namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class TITLE_POLICIES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.TitlePolicyListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("TITLE_POLICY", Order = 0)]
        public List<TITLE_POLICY> TitlePolicyList { get; set; } = new List<TITLE_POLICY>();
    
        [XmlIgnore]
        public bool TitlePolicyListSpecified
        {
            get { return this.TitlePolicyList != null && this.TitlePolicyList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public TITLE_POLICIES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
