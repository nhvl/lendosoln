namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class SECURITY_INSTRUMENT_ADDENDUM
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.SecurityInstrumentAddendumTypeSpecified
                    || this.SecurityInstrumentAddendumTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("SecurityInstrumentAddendumType", Order = 0)]
        public MISMOEnum<SecurityInstrumentAddendumBase> SecurityInstrumentAddendumType { get; set; }
    
        [XmlIgnore]
        public bool SecurityInstrumentAddendumTypeSpecified
        {
            get { return this.SecurityInstrumentAddendumType != null; }
            set { }
        }
    
        [XmlElement("SecurityInstrumentAddendumTypeOtherDescription", Order = 1)]
        public MISMOString SecurityInstrumentAddendumTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool SecurityInstrumentAddendumTypeOtherDescriptionSpecified
        {
            get { return this.SecurityInstrumentAddendumTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public SECURITY_INSTRUMENT_ADDENDUM_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
