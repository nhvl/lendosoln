namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class SECURITY_INSTRUMENT_ADDENDUMS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.SecurityInstrumentAddendumListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("SECURITY_INSTRUMENT_ADDENDUM", Order = 0)]
        public List<SECURITY_INSTRUMENT_ADDENDUM> SecurityInstrumentAddendumList { get; set; } = new List<SECURITY_INSTRUMENT_ADDENDUM>();
    
        [XmlIgnore]
        public bool SecurityInstrumentAddendumListSpecified
        {
            get { return this.SecurityInstrumentAddendumList != null && this.SecurityInstrumentAddendumList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public SECURITY_INSTRUMENT_ADDENDUMS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
