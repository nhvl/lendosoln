namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class FLOOD_RESPONSE_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DisputeResolutionIndicatorSpecified
                    || this.FEMAAdditionalLenderDescriptionSpecified
                    || this.FloodTransactionIdentifierSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DisputeResolutionIndicator", Order = 0)]
        public MISMOIndicator DisputeResolutionIndicator { get; set; }
    
        [XmlIgnore]
        public bool DisputeResolutionIndicatorSpecified
        {
            get { return this.DisputeResolutionIndicator != null; }
            set { }
        }
    
        [XmlElement("FEMAAdditionalLenderDescription", Order = 1)]
        public MISMOString FEMAAdditionalLenderDescription { get; set; }
    
        [XmlIgnore]
        public bool FEMAAdditionalLenderDescriptionSpecified
        {
            get { return this.FEMAAdditionalLenderDescription != null; }
            set { }
        }
    
        [XmlElement("FloodTransactionIdentifier", Order = 2)]
        public MISMOIdentifier FloodTransactionIdentifier { get; set; }
    
        [XmlIgnore]
        public bool FloodTransactionIdentifierSpecified
        {
            get { return this.FloodTransactionIdentifier != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public FLOOD_RESPONSE_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
