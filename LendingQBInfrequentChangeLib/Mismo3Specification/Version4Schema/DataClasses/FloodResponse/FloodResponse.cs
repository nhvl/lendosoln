namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class FLOOD_RESPONSE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.FloodDeterminationSpecified
                    || this.FloodResponseDetailSpecified
                    || this.LoansSpecified
                    || this.PartiesSpecified
                    || this.PropertySpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("FLOOD_DETERMINATION", Order = 0)]
        public FLOOD_DETERMINATION FloodDetermination { get; set; }
    
        [XmlIgnore]
        public bool FloodDeterminationSpecified
        {
            get { return this.FloodDetermination != null && this.FloodDetermination.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("FLOOD_RESPONSE_DETAIL", Order = 1)]
        public FLOOD_RESPONSE_DETAIL FloodResponseDetail { get; set; }
    
        [XmlIgnore]
        public bool FloodResponseDetailSpecified
        {
            get { return this.FloodResponseDetail != null && this.FloodResponseDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("LOANS", Order = 2)]
        public LOANS Loans { get; set; }
    
        [XmlIgnore]
        public bool LoansSpecified
        {
            get { return this.Loans != null && this.Loans.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PARTIES", Order = 3)]
        public PARTIES Parties { get; set; }
    
        [XmlIgnore]
        public bool PartiesSpecified
        {
            get { return this.Parties != null && this.Parties.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PROPERTY", Order = 4)]
        public PROPERTY Property { get; set; }
    
        [XmlIgnore]
        public bool PropertySpecified
        {
            get { return this.Property != null && this.Property.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 5)]
        public FLOOD_RESPONSE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
