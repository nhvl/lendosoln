namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class PROPERTY_VALUATIONS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.PropertyValuationListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PROPERTY_VALUATION", Order = 0)]
        public List<PROPERTY_VALUATION> PropertyValuationList { get; set; } = new List<PROPERTY_VALUATION>();
    
        [XmlIgnore]
        public bool PropertyValuationListSpecified
        {
            get { return this.PropertyValuationList != null && this.PropertyValuationList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public PROPERTY_VALUATIONS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
