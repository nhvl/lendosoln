namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PROPERTY_VALUATION_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AppraisalIdentifierSpecified
                    || this.PropertyAppraisedWithinPreviousYearDescriptionSpecified
                    || this.PropertyAppraisedWithinPreviousYearIndicatorSpecified
                    || this.PropertyInspectionRequestCommentDescriptionSpecified
                    || this.PropertyInspectionResultCommentDescriptionSpecified
                    || this.PropertyInspectionTypeSpecified
                    || this.PropertyInspectionTypeOtherDescriptionSpecified
                    || this.PropertyMostRecentValuationOrderDateSpecified
                    || this.PropertyReplacementValueAmountSpecified
                    || this.PropertyValuationAgeDaysCountSpecified
                    || this.PropertyValuationAmountSpecified
                    || this.PropertyValuationCommentTextSpecified
                    || this.PropertyValuationConditionalConclusionTypeSpecified
                    || this.PropertyValuationConditionalConclusionTypeOtherDescriptionSpecified
                    || this.PropertyValuationEffectiveDateSpecified
                    || this.PropertyValuationFormTypeSpecified
                    || this.PropertyValuationFormTypeOtherDescriptionSpecified
                    || this.PropertyValuationMethodTypeSpecified
                    || this.PropertyValuationMethodTypeOtherDescriptionSpecified
                    || this.PropertyValuationServiceTypeSpecified
                    || this.PropertyValuationServiceTypeOtherDescriptionSpecified
                    || this.PropertyValuationStateTypeSpecified
                    || this.RepairsTotalCostAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AppraisalIdentifier", Order = 0)]
        public MISMOIdentifier AppraisalIdentifier { get; set; }
    
        [XmlIgnore]
        public bool AppraisalIdentifierSpecified
        {
            get { return this.AppraisalIdentifier != null; }
            set { }
        }
    
        [XmlElement("PropertyAppraisedWithinPreviousYearDescription", Order = 1)]
        public MISMOString PropertyAppraisedWithinPreviousYearDescription { get; set; }
    
        [XmlIgnore]
        public bool PropertyAppraisedWithinPreviousYearDescriptionSpecified
        {
            get { return this.PropertyAppraisedWithinPreviousYearDescription != null; }
            set { }
        }
    
        [XmlElement("PropertyAppraisedWithinPreviousYearIndicator", Order = 2)]
        public MISMOIndicator PropertyAppraisedWithinPreviousYearIndicator { get; set; }
    
        [XmlIgnore]
        public bool PropertyAppraisedWithinPreviousYearIndicatorSpecified
        {
            get { return this.PropertyAppraisedWithinPreviousYearIndicator != null; }
            set { }
        }
    
        [XmlElement("PropertyInspectionRequestCommentDescription", Order = 3)]
        public MISMOString PropertyInspectionRequestCommentDescription { get; set; }
    
        [XmlIgnore]
        public bool PropertyInspectionRequestCommentDescriptionSpecified
        {
            get { return this.PropertyInspectionRequestCommentDescription != null; }
            set { }
        }
    
        [XmlElement("PropertyInspectionResultCommentDescription", Order = 4)]
        public MISMOString PropertyInspectionResultCommentDescription { get; set; }
    
        [XmlIgnore]
        public bool PropertyInspectionResultCommentDescriptionSpecified
        {
            get { return this.PropertyInspectionResultCommentDescription != null; }
            set { }
        }
    
        [XmlElement("PropertyInspectionType", Order = 5)]
        public MISMOEnum<PropertyInspectionBase> PropertyInspectionType { get; set; }
    
        [XmlIgnore]
        public bool PropertyInspectionTypeSpecified
        {
            get { return this.PropertyInspectionType != null; }
            set { }
        }
    
        [XmlElement("PropertyInspectionTypeOtherDescription", Order = 6)]
        public MISMOString PropertyInspectionTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool PropertyInspectionTypeOtherDescriptionSpecified
        {
            get { return this.PropertyInspectionTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("PropertyMostRecentValuationOrderDate", Order = 7)]
        public MISMODate PropertyMostRecentValuationOrderDate { get; set; }
    
        [XmlIgnore]
        public bool PropertyMostRecentValuationOrderDateSpecified
        {
            get { return this.PropertyMostRecentValuationOrderDate != null; }
            set { }
        }
    
        [XmlElement("PropertyReplacementValueAmount", Order = 8)]
        public MISMOAmount PropertyReplacementValueAmount { get; set; }
    
        [XmlIgnore]
        public bool PropertyReplacementValueAmountSpecified
        {
            get { return this.PropertyReplacementValueAmount != null; }
            set { }
        }
    
        [XmlElement("PropertyValuationAgeDaysCount", Order = 9)]
        public MISMOCount PropertyValuationAgeDaysCount { get; set; }
    
        [XmlIgnore]
        public bool PropertyValuationAgeDaysCountSpecified
        {
            get { return this.PropertyValuationAgeDaysCount != null; }
            set { }
        }
    
        [XmlElement("PropertyValuationAmount", Order = 10)]
        public MISMOAmount PropertyValuationAmount { get; set; }
    
        [XmlIgnore]
        public bool PropertyValuationAmountSpecified
        {
            get { return this.PropertyValuationAmount != null; }
            set { }
        }
    
        [XmlElement("PropertyValuationCommentText", Order = 11)]
        public MISMOString PropertyValuationCommentText { get; set; }
    
        [XmlIgnore]
        public bool PropertyValuationCommentTextSpecified
        {
            get { return this.PropertyValuationCommentText != null; }
            set { }
        }
    
        [XmlElement("PropertyValuationConditionalConclusionType", Order = 12)]
        public MISMOEnum<PropertyValuationConditionalConclusionBase> PropertyValuationConditionalConclusionType { get; set; }
    
        [XmlIgnore]
        public bool PropertyValuationConditionalConclusionTypeSpecified
        {
            get { return this.PropertyValuationConditionalConclusionType != null; }
            set { }
        }
    
        [XmlElement("PropertyValuationConditionalConclusionTypeOtherDescription", Order = 13)]
        public MISMOString PropertyValuationConditionalConclusionTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool PropertyValuationConditionalConclusionTypeOtherDescriptionSpecified
        {
            get { return this.PropertyValuationConditionalConclusionTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("PropertyValuationEffectiveDate", Order = 14)]
        public MISMODate PropertyValuationEffectiveDate { get; set; }
    
        [XmlIgnore]
        public bool PropertyValuationEffectiveDateSpecified
        {
            get { return this.PropertyValuationEffectiveDate != null; }
            set { }
        }
    
        [XmlElement("PropertyValuationFormType", Order = 15)]
        public MISMOEnum<PropertyValuationFormBase> PropertyValuationFormType { get; set; }
    
        [XmlIgnore]
        public bool PropertyValuationFormTypeSpecified
        {
            get { return this.PropertyValuationFormType != null; }
            set { }
        }
    
        [XmlElement("PropertyValuationFormTypeOtherDescription", Order = 16)]
        public MISMOString PropertyValuationFormTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool PropertyValuationFormTypeOtherDescriptionSpecified
        {
            get { return this.PropertyValuationFormTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("PropertyValuationMethodType", Order = 17)]
        public MISMOEnum<PropertyValuationMethodBase> PropertyValuationMethodType { get; set; }
    
        [XmlIgnore]
        public bool PropertyValuationMethodTypeSpecified
        {
            get { return this.PropertyValuationMethodType != null; }
            set { }
        }
    
        [XmlElement("PropertyValuationMethodTypeOtherDescription", Order = 18)]
        public MISMOString PropertyValuationMethodTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool PropertyValuationMethodTypeOtherDescriptionSpecified
        {
            get { return this.PropertyValuationMethodTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("PropertyValuationServiceType", Order = 19)]
        public MISMOEnum<PropertyValuationServiceBase> PropertyValuationServiceType { get; set; }
    
        [XmlIgnore]
        public bool PropertyValuationServiceTypeSpecified
        {
            get { return this.PropertyValuationServiceType != null; }
            set { }
        }
    
        [XmlElement("PropertyValuationServiceTypeOtherDescription", Order = 20)]
        public MISMOString PropertyValuationServiceTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool PropertyValuationServiceTypeOtherDescriptionSpecified
        {
            get { return this.PropertyValuationServiceTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("PropertyValuationStateType", Order = 21)]
        public MISMOEnum<PropertyValuationStateBase> PropertyValuationStateType { get; set; }
    
        [XmlIgnore]
        public bool PropertyValuationStateTypeSpecified
        {
            get { return this.PropertyValuationStateType != null; }
            set { }
        }
    
        [XmlElement("RepairsTotalCostAmount", Order = 22)]
        public MISMOAmount RepairsTotalCostAmount { get; set; }
    
        [XmlIgnore]
        public bool RepairsTotalCostAmountSpecified
        {
            get { return this.RepairsTotalCostAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 23)]
        public PROPERTY_VALUATION_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
