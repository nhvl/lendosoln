namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PROPERTY_VALUATION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AvmsSpecified
                    || this.PropertyValuationDetailSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AVMS", Order = 0)]
        public AVMS Avms { get; set; }
    
        [XmlIgnore]
        public bool AvmsSpecified
        {
            get { return this.Avms != null && this.Avms.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PROPERTY_VALUATION_DETAIL", Order = 1)]
        public PROPERTY_VALUATION_DETAIL PropertyValuationDetail { get; set; }
    
        [XmlIgnore]
        public bool PropertyValuationDetailSpecified
        {
            get { return this.PropertyValuationDetail != null && this.PropertyValuationDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public PROPERTY_VALUATION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
