namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PAYMENT_RULE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CurtailmentApplicationSequenceTypeSpecified
                    || this.FinalPaymentAmountSpecified
                    || this.FirstPrincipalReductionDateSpecified
                    || this.FullyIndexedInitialPrincipalAndInterestPaymentAmountSpecified
                    || this.InitialInterestOnlyPaymentAmountSpecified
                    || this.InitialPaymentRatePercentSpecified
                    || this.InitialPITIPaymentAmountSpecified
                    || this.InitialPrincipalAndInterestAndMIMonthlyPaymentAmountSpecified
                    || this.InitialPrincipalAndInterestPaymentAmountSpecified
                    || this.InitialTaxAndInsurancePaymentAmountSpecified
                    || this.LoanPaymentAccumulationAmountSpecified
                    || this.PartialPaymentAllowedIndicatorSpecified
                    || this.PartialPaymentPenaltyIndicatorSpecified
                    || this.PaymentBillingMethodTypeSpecified
                    || this.PaymentBillingStatementFrequencyTypeSpecified
                    || this.PaymentBillingStatementLeadDaysCountSpecified
                    || this.PaymentFrequencyTypeSpecified
                    || this.PaymentFrequencyTypeOtherDescriptionSpecified
                    || this.PaymentOptionIndicatorSpecified
                    || this.PaymentRemittanceDaySpecified
                    || this.ScheduledAnnualPaymentCountSpecified
                    || this.ScheduledFirstPaymentDateSpecified
                    || this.ScheduledTotalPaymentCountSpecified
                    || this.SeasonalPaymentPeriodEndMonthSpecified
                    || this.SeasonalPaymentPeriodStartMonthSpecified
                    || this.ThirdPartyInvestorFeePassThroughPercentSpecified
                    || this.ThirdPartyInvestorInterestPassThroughPercentSpecified
                    || this.ThirdPartyInvestorPrincipalPassThroughPercentSpecified
                    || this.TotalOptionalPaymentCountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CurtailmentApplicationSequenceType", Order = 0)]
        public MISMOEnum<CurtailmentApplicationSequenceBase> CurtailmentApplicationSequenceType { get; set; }
    
        [XmlIgnore]
        public bool CurtailmentApplicationSequenceTypeSpecified
        {
            get { return this.CurtailmentApplicationSequenceType != null; }
            set { }
        }
    
        [XmlElement("FinalPaymentAmount", Order = 1)]
        public MISMOAmount FinalPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool FinalPaymentAmountSpecified
        {
            get { return this.FinalPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("FirstPrincipalReductionDate", Order = 2)]
        public MISMODate FirstPrincipalReductionDate { get; set; }
    
        [XmlIgnore]
        public bool FirstPrincipalReductionDateSpecified
        {
            get { return this.FirstPrincipalReductionDate != null; }
            set { }
        }
    
        [XmlElement("FullyIndexedInitialPrincipalAndInterestPaymentAmount", Order = 3)]
        public MISMOAmount FullyIndexedInitialPrincipalAndInterestPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool FullyIndexedInitialPrincipalAndInterestPaymentAmountSpecified
        {
            get { return this.FullyIndexedInitialPrincipalAndInterestPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("InitialInterestOnlyPaymentAmount", Order = 4)]
        public MISMOAmount InitialInterestOnlyPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool InitialInterestOnlyPaymentAmountSpecified
        {
            get { return this.InitialInterestOnlyPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("InitialPaymentRatePercent", Order = 5)]
        public MISMOPercent InitialPaymentRatePercent { get; set; }
    
        [XmlIgnore]
        public bool InitialPaymentRatePercentSpecified
        {
            get { return this.InitialPaymentRatePercent != null; }
            set { }
        }
    
        [XmlElement("InitialPITIPaymentAmount", Order = 6)]
        public MISMOAmount InitialPITIPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool InitialPITIPaymentAmountSpecified
        {
            get { return this.InitialPITIPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("InitialPrincipalAndInterestAndMIMonthlyPaymentAmount", Order = 7)]
        public MISMOAmount InitialPrincipalAndInterestAndMIMonthlyPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool InitialPrincipalAndInterestAndMIMonthlyPaymentAmountSpecified
        {
            get { return this.InitialPrincipalAndInterestAndMIMonthlyPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("InitialPrincipalAndInterestPaymentAmount", Order = 8)]
        public MISMOAmount InitialPrincipalAndInterestPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool InitialPrincipalAndInterestPaymentAmountSpecified
        {
            get { return this.InitialPrincipalAndInterestPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("InitialTaxAndInsurancePaymentAmount", Order = 9)]
        public MISMOAmount InitialTaxAndInsurancePaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool InitialTaxAndInsurancePaymentAmountSpecified
        {
            get { return this.InitialTaxAndInsurancePaymentAmount != null; }
            set { }
        }
    
        [XmlElement("LoanPaymentAccumulationAmount", Order = 10)]
        public MISMOAmount LoanPaymentAccumulationAmount { get; set; }
    
        [XmlIgnore]
        public bool LoanPaymentAccumulationAmountSpecified
        {
            get { return this.LoanPaymentAccumulationAmount != null; }
            set { }
        }
    
        [XmlElement("PartialPaymentAllowedIndicator", Order = 11)]
        public MISMOIndicator PartialPaymentAllowedIndicator { get; set; }
    
        [XmlIgnore]
        public bool PartialPaymentAllowedIndicatorSpecified
        {
            get { return this.PartialPaymentAllowedIndicator != null; }
            set { }
        }
    
        [XmlElement("PartialPaymentPenaltyIndicator", Order = 12)]
        public MISMOIndicator PartialPaymentPenaltyIndicator { get; set; }
    
        [XmlIgnore]
        public bool PartialPaymentPenaltyIndicatorSpecified
        {
            get { return this.PartialPaymentPenaltyIndicator != null; }
            set { }
        }
    
        [XmlElement("PaymentBillingMethodType", Order = 13)]
        public MISMOEnum<PaymentBillingMethodBase> PaymentBillingMethodType { get; set; }
    
        [XmlIgnore]
        public bool PaymentBillingMethodTypeSpecified
        {
            get { return this.PaymentBillingMethodType != null; }
            set { }
        }
    
        [XmlElement("PaymentBillingStatementFrequencyType", Order = 14)]
        public MISMOEnum<PaymentBillingStatementFrequencyBase> PaymentBillingStatementFrequencyType { get; set; }
    
        [XmlIgnore]
        public bool PaymentBillingStatementFrequencyTypeSpecified
        {
            get { return this.PaymentBillingStatementFrequencyType != null; }
            set { }
        }
    
        [XmlElement("PaymentBillingStatementLeadDaysCount", Order = 15)]
        public MISMOCount PaymentBillingStatementLeadDaysCount { get; set; }
    
        [XmlIgnore]
        public bool PaymentBillingStatementLeadDaysCountSpecified
        {
            get { return this.PaymentBillingStatementLeadDaysCount != null; }
            set { }
        }
    
        [XmlElement("PaymentFrequencyType", Order = 16)]
        public MISMOEnum<PaymentFrequencyBase> PaymentFrequencyType { get; set; }
    
        [XmlIgnore]
        public bool PaymentFrequencyTypeSpecified
        {
            get { return this.PaymentFrequencyType != null; }
            set { }
        }
    
        [XmlElement("PaymentFrequencyTypeOtherDescription", Order = 17)]
        public MISMOString PaymentFrequencyTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool PaymentFrequencyTypeOtherDescriptionSpecified
        {
            get { return this.PaymentFrequencyTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("PaymentOptionIndicator", Order = 18)]
        public MISMOIndicator PaymentOptionIndicator { get; set; }
    
        [XmlIgnore]
        public bool PaymentOptionIndicatorSpecified
        {
            get { return this.PaymentOptionIndicator != null; }
            set { }
        }
    
        [XmlElement("PaymentRemittanceDay", Order = 19)]
        public MISMODay PaymentRemittanceDay { get; set; }
    
        [XmlIgnore]
        public bool PaymentRemittanceDaySpecified
        {
            get { return this.PaymentRemittanceDay != null; }
            set { }
        }
    
        [XmlElement("ScheduledAnnualPaymentCount", Order = 20)]
        public MISMOCount ScheduledAnnualPaymentCount { get; set; }
    
        [XmlIgnore]
        public bool ScheduledAnnualPaymentCountSpecified
        {
            get { return this.ScheduledAnnualPaymentCount != null; }
            set { }
        }
    
        [XmlElement("ScheduledFirstPaymentDate", Order = 21)]
        public MISMODate ScheduledFirstPaymentDate { get; set; }
    
        [XmlIgnore]
        public bool ScheduledFirstPaymentDateSpecified
        {
            get { return this.ScheduledFirstPaymentDate != null; }
            set { }
        }
    
        [XmlElement("ScheduledTotalPaymentCount", Order = 22)]
        public MISMOCount ScheduledTotalPaymentCount { get; set; }
    
        [XmlIgnore]
        public bool ScheduledTotalPaymentCountSpecified
        {
            get { return this.ScheduledTotalPaymentCount != null; }
            set { }
        }
    
        [XmlElement("SeasonalPaymentPeriodEndMonth", Order = 23)]
        public MISMOMonth SeasonalPaymentPeriodEndMonth { get; set; }
    
        [XmlIgnore]
        public bool SeasonalPaymentPeriodEndMonthSpecified
        {
            get { return this.SeasonalPaymentPeriodEndMonth != null; }
            set { }
        }
    
        [XmlElement("SeasonalPaymentPeriodStartMonth", Order = 24)]
        public MISMOMonth SeasonalPaymentPeriodStartMonth { get; set; }
    
        [XmlIgnore]
        public bool SeasonalPaymentPeriodStartMonthSpecified
        {
            get { return this.SeasonalPaymentPeriodStartMonth != null; }
            set { }
        }
    
        [XmlElement("ThirdPartyInvestorFeePassThroughPercent", Order = 25)]
        public MISMOPercent ThirdPartyInvestorFeePassThroughPercent { get; set; }
    
        [XmlIgnore]
        public bool ThirdPartyInvestorFeePassThroughPercentSpecified
        {
            get { return this.ThirdPartyInvestorFeePassThroughPercent != null; }
            set { }
        }
    
        [XmlElement("ThirdPartyInvestorInterestPassThroughPercent", Order = 26)]
        public MISMOPercent ThirdPartyInvestorInterestPassThroughPercent { get; set; }
    
        [XmlIgnore]
        public bool ThirdPartyInvestorInterestPassThroughPercentSpecified
        {
            get { return this.ThirdPartyInvestorInterestPassThroughPercent != null; }
            set { }
        }
    
        [XmlElement("ThirdPartyInvestorPrincipalPassThroughPercent", Order = 27)]
        public MISMOPercent ThirdPartyInvestorPrincipalPassThroughPercent { get; set; }
    
        [XmlIgnore]
        public bool ThirdPartyInvestorPrincipalPassThroughPercentSpecified
        {
            get { return this.ThirdPartyInvestorPrincipalPassThroughPercent != null; }
            set { }
        }
    
        [XmlElement("TotalOptionalPaymentCount", Order = 28)]
        public MISMOCount TotalOptionalPaymentCount { get; set; }
    
        [XmlIgnore]
        public bool TotalOptionalPaymentCountSpecified
        {
            get { return this.TotalOptionalPaymentCount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 29)]
        public PAYMENT_RULE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
