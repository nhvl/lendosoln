namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class TRANSFORM_FILES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.TransformFileListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("TRANSFORM_FILE", Order = 0)]
        public List<TRANSFORM_FILE> TransformFileList { get; set; } = new List<TRANSFORM_FILE>();
    
        [XmlIgnore]
        public bool TransformFileListSpecified
        {
            get { return this.TransformFileList != null && this.TransformFileList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public TRANSFORM_FILES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
