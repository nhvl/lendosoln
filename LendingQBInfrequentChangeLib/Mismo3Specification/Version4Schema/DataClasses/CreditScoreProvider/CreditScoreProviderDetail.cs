namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CREDIT_SCORE_PROVIDER_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditScoreProviderNameSpecified
                    || this.CreditScoreProviderURLSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CreditScoreProviderName", Order = 0)]
        public MISMOString CreditScoreProviderName { get; set; }
    
        [XmlIgnore]
        public bool CreditScoreProviderNameSpecified
        {
            get { return this.CreditScoreProviderName != null; }
            set { }
        }
    
        [XmlElement("CreditScoreProviderURL", Order = 1)]
        public MISMOURL CreditScoreProviderURL { get; set; }
    
        [XmlIgnore]
        public bool CreditScoreProviderURLSpecified
        {
            get { return this.CreditScoreProviderURL != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public CREDIT_SCORE_PROVIDER_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
