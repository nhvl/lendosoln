namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CREDIT_SCORE_PROVIDER
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AddressSpecified
                    || this.CreditScoreProviderDetailSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ADDRESS", Order = 0)]
        public ADDRESS Address { get; set; }
    
        [XmlIgnore]
        public bool AddressSpecified
        {
            get { return this.Address != null && this.Address.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CREDIT_SCORE_PROVIDER_DETAIL", Order = 1)]
        public CREDIT_SCORE_PROVIDER_DETAIL CreditScoreProviderDetail { get; set; }
    
        [XmlIgnore]
        public bool CreditScoreProviderDetailSpecified
        {
            get { return this.CreditScoreProviderDetail != null && this.CreditScoreProviderDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public CREDIT_SCORE_PROVIDER_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
