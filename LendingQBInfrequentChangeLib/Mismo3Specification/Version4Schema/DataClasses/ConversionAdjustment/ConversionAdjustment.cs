namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CONVERSION_ADJUSTMENT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ConversionAdjustmentLifetimeAdjustmentRuleSpecified
                    || this.ConversionOptionPeriodAdjustmentRulesSpecified
                    || this.IndexRulesSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CONVERSION_ADJUSTMENT_LIFETIME_ADJUSTMENT_RULE", Order = 0)]
        public CONVERSION_ADJUSTMENT_LIFETIME_ADJUSTMENT_RULE ConversionAdjustmentLifetimeAdjustmentRule { get; set; }
    
        [XmlIgnore]
        public bool ConversionAdjustmentLifetimeAdjustmentRuleSpecified
        {
            get { return this.ConversionAdjustmentLifetimeAdjustmentRule != null && this.ConversionAdjustmentLifetimeAdjustmentRule.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CONVERSION_OPTION_PERIOD_ADJUSTMENT_RULES", Order = 1)]
        public CONVERSION_OPTION_PERIOD_ADJUSTMENT_RULES ConversionOptionPeriodAdjustmentRules { get; set; }
    
        [XmlIgnore]
        public bool ConversionOptionPeriodAdjustmentRulesSpecified
        {
            get { return this.ConversionOptionPeriodAdjustmentRules != null && this.ConversionOptionPeriodAdjustmentRules.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("INDEX_RULES", Order = 2)]
        public INDEX_RULES IndexRules { get; set; }
    
        [XmlIgnore]
        public bool IndexRulesSpecified
        {
            get { return this.IndexRules != null && this.IndexRules.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public CONVERSION_ADJUSTMENT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
