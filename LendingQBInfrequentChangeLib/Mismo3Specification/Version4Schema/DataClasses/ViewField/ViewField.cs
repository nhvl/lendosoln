namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Xml.Serialization;

    public class VIEW_FIELD
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                if (Mismo3Utilities.ExceedsThreshold(1,
                    new List<bool> {
                        this.ForeignSystemSignatureFieldSpecified,
                        this.InteractiveFieldSpecified,
                        this.NotarySignatureFieldSpecified,
                        this.RecordingEndorsementFieldSpecified,
                        this.StakeholderSignatureFieldSpecified,
                        this.WitnessSignatureFieldSpecified }))
                {
                    throw new System.Xml.Schema.XmlSchemaValidationException(
                        Mismo3Utilities.GetXSDChoiceExceptionMessage(
                        "VIEW_FIELD",
                        new List<string> {
                            "FOREIGN_SYSTEM_SIGNATURE_FIELD",
                            "INTERACTIVE_FIELD",
                            "NOTARY_SIGNATURE_FIELD",
                            "RECORDING_ENDORSEMENT_FIELD",
                            "STAKEHOLDER_SIGNATURE_FIELD",
                            "WITNESS_SIGNATURE_FIELD"}));
                }

                return this.ForeignSystemSignatureFieldSpecified
                    || this.InteractiveFieldSpecified
                    || this.NotarySignatureFieldSpecified
                    || this.RecordingEndorsementFieldSpecified
                    || this.StakeholderSignatureFieldSpecified
                    || this.WitnessSignatureFieldSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("FOREIGN_SYSTEM_SIGNATURE_FIELD", Order = 0)]
        public FOREIGN_SYSTEM_SIGNATURE_FIELD ForeignSystemSignatureField { get; set; }
    
        [XmlIgnore]
        public bool ForeignSystemSignatureFieldSpecified
        {
            get { return this.ForeignSystemSignatureField != null && this.ForeignSystemSignatureField.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("INTERACTIVE_FIELD", Order = 1)]
        public INTERACTIVE_FIELD InteractiveField { get; set; }
    
        [XmlIgnore]
        public bool InteractiveFieldSpecified
        {
            get { return this.InteractiveField != null && this.InteractiveField.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("NOTARY_SIGNATURE_FIELD", Order = 2)]
        public NOTARY_SIGNATURE_FIELD NotarySignatureField { get; set; }
    
        [XmlIgnore]
        public bool NotarySignatureFieldSpecified
        {
            get { return this.NotarySignatureField != null && this.NotarySignatureField.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("RECORDING_ENDORSEMENT_FIELD", Order = 3)]
        public RECORDING_ENDORSEMENT_FIELD RecordingEndorsementField { get; set; }
    
        [XmlIgnore]
        public bool RecordingEndorsementFieldSpecified
        {
            get { return this.RecordingEndorsementField != null && this.RecordingEndorsementField.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("STAKEHOLDER_SIGNATURE_FIELD", Order = 4)]
        public STAKEHOLDER_SIGNATURE_FIELD StakeholderSignatureField { get; set; }
    
        [XmlIgnore]
        public bool StakeholderSignatureFieldSpecified
        {
            get { return this.StakeholderSignatureField != null && this.StakeholderSignatureField.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("WITNESS_SIGNATURE_FIELD", Order = 5)]
        public WITNESS_SIGNATURE_FIELD WitnessSignatureField { get; set; }
    
        [XmlIgnore]
        public bool WitnessSignatureFieldSpecified
        {
            get { return this.WitnessSignatureField != null && this.WitnessSignatureField.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 6)]
        public VIEW_FIELD_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
