namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class VIEW_FIELDS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ViewFieldListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("VIEW_FIELD", Order = 0)]
        public List<VIEW_FIELD> ViewFieldList { get; set; } = new List<VIEW_FIELD>();
    
        [XmlIgnore]
        public bool ViewFieldListSpecified
        {
            get { return this.ViewFieldList != null && this.ViewFieldList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public VIEW_FIELDS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
