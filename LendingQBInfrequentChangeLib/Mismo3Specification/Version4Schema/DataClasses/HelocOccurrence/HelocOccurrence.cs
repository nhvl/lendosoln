namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class HELOC_OCCURRENCE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CurrentHELOCMaximumBalanceAmountSpecified
                    || this.HELOCBalanceAmountSpecified
                    || this.HELOCDailyPeriodicInterestRatePercentSpecified
                    || this.HELOCTeaserTermEndDateSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CurrentHELOCMaximumBalanceAmount", Order = 0)]
        public MISMOAmount CurrentHELOCMaximumBalanceAmount { get; set; }
    
        [XmlIgnore]
        public bool CurrentHELOCMaximumBalanceAmountSpecified
        {
            get { return this.CurrentHELOCMaximumBalanceAmount != null; }
            set { }
        }
    
        [XmlElement("HELOCBalanceAmount", Order = 1)]
        public MISMOAmount HELOCBalanceAmount { get; set; }
    
        [XmlIgnore]
        public bool HELOCBalanceAmountSpecified
        {
            get { return this.HELOCBalanceAmount != null; }
            set { }
        }
    
        [XmlElement("HELOCDailyPeriodicInterestRatePercent", Order = 2)]
        public MISMOPercent HELOCDailyPeriodicInterestRatePercent { get; set; }
    
        [XmlIgnore]
        public bool HELOCDailyPeriodicInterestRatePercentSpecified
        {
            get { return this.HELOCDailyPeriodicInterestRatePercent != null; }
            set { }
        }
    
        [XmlElement("HELOCTeaserTermEndDate", Order = 3)]
        public MISMODate HELOCTeaserTermEndDate { get; set; }
    
        [XmlIgnore]
        public bool HELOCTeaserTermEndDateSpecified
        {
            get { return this.HELOCTeaserTermEndDate != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public HELOC_OCCURRENCE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
