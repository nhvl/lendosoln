namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class HELOC_OCCURRENCES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.HelocOccurrenceListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("HELOC_OCCURRENCE", Order = 0)]
        public List<HELOC_OCCURRENCE> HelocOccurrenceList { get; set; } = new List<HELOC_OCCURRENCE>();
    
        [XmlIgnore]
        public bool HelocOccurrenceListSpecified
        {
            get { return this.HelocOccurrenceList != null && this.HelocOccurrenceList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public HELOC_OCCURRENCES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
