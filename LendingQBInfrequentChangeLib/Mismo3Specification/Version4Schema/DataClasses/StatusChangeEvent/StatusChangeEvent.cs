namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class STATUS_CHANGE_EVENT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.InvestorReportingActionDateSpecified
                    || this.InvestorReportingActionDescriptionSpecified
                    || this.InvestorReportingActionReversalIndicatorSpecified
                    || this.InvestorReportingActionTypeSpecified
                    || this.InvestorReportingActionTypeOtherDescriptionSpecified
                    || this.ReverseMortgageReportingActionDescriptionSpecified
                    || this.ReverseMortgageReportingActionTypeSpecified
                    || this.ReverseMortgageReportingActionTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("InvestorReportingActionDate", Order = 0)]
        public MISMODate InvestorReportingActionDate { get; set; }
    
        [XmlIgnore]
        public bool InvestorReportingActionDateSpecified
        {
            get { return this.InvestorReportingActionDate != null; }
            set { }
        }
    
        [XmlElement("InvestorReportingActionDescription", Order = 1)]
        public MISMOString InvestorReportingActionDescription { get; set; }
    
        [XmlIgnore]
        public bool InvestorReportingActionDescriptionSpecified
        {
            get { return this.InvestorReportingActionDescription != null; }
            set { }
        }
    
        [XmlElement("InvestorReportingActionReversalIndicator", Order = 2)]
        public MISMOIndicator InvestorReportingActionReversalIndicator { get; set; }
    
        [XmlIgnore]
        public bool InvestorReportingActionReversalIndicatorSpecified
        {
            get { return this.InvestorReportingActionReversalIndicator != null; }
            set { }
        }
    
        [XmlElement("InvestorReportingActionType", Order = 3)]
        public MISMOEnum<InvestorReportingActionBase> InvestorReportingActionType { get; set; }
    
        [XmlIgnore]
        public bool InvestorReportingActionTypeSpecified
        {
            get { return this.InvestorReportingActionType != null; }
            set { }
        }
    
        [XmlElement("InvestorReportingActionTypeOtherDescription", Order = 4)]
        public MISMOString InvestorReportingActionTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool InvestorReportingActionTypeOtherDescriptionSpecified
        {
            get { return this.InvestorReportingActionTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("ReverseMortgageReportingActionDescription", Order = 5)]
        public MISMOString ReverseMortgageReportingActionDescription { get; set; }
    
        [XmlIgnore]
        public bool ReverseMortgageReportingActionDescriptionSpecified
        {
            get { return this.ReverseMortgageReportingActionDescription != null; }
            set { }
        }
    
        [XmlElement("ReverseMortgageReportingActionType", Order = 6)]
        public MISMOEnum<ReverseMortgageReportingActionBase> ReverseMortgageReportingActionType { get; set; }
    
        [XmlIgnore]
        public bool ReverseMortgageReportingActionTypeSpecified
        {
            get { return this.ReverseMortgageReportingActionType != null; }
            set { }
        }
    
        [XmlElement("ReverseMortgageReportingActionTypeOtherDescription", Order = 7)]
        public MISMOString ReverseMortgageReportingActionTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ReverseMortgageReportingActionTypeOtherDescriptionSpecified
        {
            get { return this.ReverseMortgageReportingActionTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 8)]
        public STATUS_CHANGE_EVENT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
