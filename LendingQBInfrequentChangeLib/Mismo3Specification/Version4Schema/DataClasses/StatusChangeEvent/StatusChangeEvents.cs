namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class STATUS_CHANGE_EVENTS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.StatusChangeEventListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("STATUS_CHANGE_EVENT", Order = 0)]
        public List<STATUS_CHANGE_EVENT> StatusChangeEventList { get; set; } = new List<STATUS_CHANGE_EVENT>();
    
        [XmlIgnore]
        public bool StatusChangeEventListSpecified
        {
            get { return this.StatusChangeEventList != null && this.StatusChangeEventList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public STATUS_CHANGE_EVENTS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
