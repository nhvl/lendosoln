namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class LOAN_ORIGINATOR
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.LoanOriginatorTypeSpecified
                    || this.LoanOriginatorTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("LoanOriginatorType", Order = 0)]
        public MISMOEnum<LoanOriginatorBase> LoanOriginatorType { get; set; }
    
        [XmlIgnore]
        public bool LoanOriginatorTypeSpecified
        {
            get { return this.LoanOriginatorType != null; }
            set { }
        }
    
        [XmlElement("LoanOriginatorTypeOtherDescription", Order = 1)]
        public MISMOString LoanOriginatorTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool LoanOriginatorTypeOtherDescriptionSpecified
        {
            get { return this.LoanOriginatorTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public LOAN_ORIGINATOR_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
