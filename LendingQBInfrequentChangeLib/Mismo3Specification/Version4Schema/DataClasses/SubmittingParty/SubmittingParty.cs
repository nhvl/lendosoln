namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class SUBMITTING_PARTY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.SubmittingPartySequenceNumberSpecified
                    || this.SubmittingPartyTransactionIdentifierSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("SubmittingPartySequenceNumber", Order = 0)]
        public MISMOSequenceNumber SubmittingPartySequenceNumber { get; set; }
    
        [XmlIgnore]
        public bool SubmittingPartySequenceNumberSpecified
        {
            get { return this.SubmittingPartySequenceNumber != null; }
            set { }
        }
    
        [XmlElement("SubmittingPartyTransactionIdentifier", Order = 1)]
        public MISMOIdentifier SubmittingPartyTransactionIdentifier { get; set; }
    
        [XmlIgnore]
        public bool SubmittingPartyTransactionIdentifierSpecified
        {
            get { return this.SubmittingPartyTransactionIdentifier != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public SUBMITTING_PARTY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
