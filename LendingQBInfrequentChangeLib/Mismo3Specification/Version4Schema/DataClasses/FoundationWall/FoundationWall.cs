namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class FOUNDATION_WALL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ComponentAdjustmentAmountSpecified
                    || this.ComponentClassificationTypeSpecified
                    || this.ConditionRatingDescriptionSpecified
                    || this.ConditionRatingTypeSpecified
                    || this.QualityRatingDescriptionSpecified
                    || this.QualityRatingTypeSpecified
                    || this.WallMaterialTypeSpecified
                    || this.WallMaterialTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ComponentAdjustmentAmount", Order = 0)]
        public MISMOAmount ComponentAdjustmentAmount { get; set; }
    
        [XmlIgnore]
        public bool ComponentAdjustmentAmountSpecified
        {
            get { return this.ComponentAdjustmentAmount != null; }
            set { }
        }
    
        [XmlElement("ComponentClassificationType", Order = 1)]
        public MISMOEnum<ComponentClassificationBase> ComponentClassificationType { get; set; }
    
        [XmlIgnore]
        public bool ComponentClassificationTypeSpecified
        {
            get { return this.ComponentClassificationType != null; }
            set { }
        }
    
        [XmlElement("ConditionRatingDescription", Order = 2)]
        public MISMOString ConditionRatingDescription { get; set; }
    
        [XmlIgnore]
        public bool ConditionRatingDescriptionSpecified
        {
            get { return this.ConditionRatingDescription != null; }
            set { }
        }
    
        [XmlElement("ConditionRatingType", Order = 3)]
        public MISMOEnum<ConditionRatingBase> ConditionRatingType { get; set; }
    
        [XmlIgnore]
        public bool ConditionRatingTypeSpecified
        {
            get { return this.ConditionRatingType != null; }
            set { }
        }
    
        [XmlElement("QualityRatingDescription", Order = 4)]
        public MISMOString QualityRatingDescription { get; set; }
    
        [XmlIgnore]
        public bool QualityRatingDescriptionSpecified
        {
            get { return this.QualityRatingDescription != null; }
            set { }
        }
    
        [XmlElement("QualityRatingType", Order = 5)]
        public MISMOEnum<QualityRatingBase> QualityRatingType { get; set; }
    
        [XmlIgnore]
        public bool QualityRatingTypeSpecified
        {
            get { return this.QualityRatingType != null; }
            set { }
        }
    
        [XmlElement("WallMaterialType", Order = 6)]
        public MISMOEnum<WallMaterialBase> WallMaterialType { get; set; }
    
        [XmlIgnore]
        public bool WallMaterialTypeSpecified
        {
            get { return this.WallMaterialType != null; }
            set { }
        }
    
        [XmlElement("WallMaterialTypeOtherDescription", Order = 7)]
        public MISMOString WallMaterialTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool WallMaterialTypeOtherDescriptionSpecified
        {
            get { return this.WallMaterialTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 8)]
        public FOUNDATION_WALL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
