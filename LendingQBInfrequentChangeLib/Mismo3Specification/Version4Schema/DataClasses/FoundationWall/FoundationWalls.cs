namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class FOUNDATION_WALLS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.FoundationWallListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("FOUNDATION_WALL", Order = 0)]
        public List<FOUNDATION_WALL> FoundationWallList { get; set; } = new List<FOUNDATION_WALL>();
    
        [XmlIgnore]
        public bool FoundationWallListSpecified
        {
            get { return this.FoundationWallList != null && this.FoundationWallList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public FOUNDATION_WALLS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
