namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class POOL_CERTIFICATE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.PoolCertificateCurrentAmountSpecified
                    || this.PoolCertificateHolderPayeeIdentifierSpecified
                    || this.PoolCertificateIdentifierSpecified
                    || this.PoolCertificateInitialPaymentDateSpecified
                    || this.PoolCertificateIssueDateSpecified
                    || this.PoolCertificateMaturityDateSpecified
                    || this.PoolCertificateOriginalAmountSpecified
                    || this.PoolCertificatePrincipalPaidAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PoolCertificateCurrentAmount", Order = 0)]
        public MISMOAmount PoolCertificateCurrentAmount { get; set; }
    
        [XmlIgnore]
        public bool PoolCertificateCurrentAmountSpecified
        {
            get { return this.PoolCertificateCurrentAmount != null; }
            set { }
        }
    
        [XmlElement("PoolCertificateHolderPayeeIdentifier", Order = 1)]
        public MISMOIdentifier PoolCertificateHolderPayeeIdentifier { get; set; }
    
        [XmlIgnore]
        public bool PoolCertificateHolderPayeeIdentifierSpecified
        {
            get { return this.PoolCertificateHolderPayeeIdentifier != null; }
            set { }
        }
    
        [XmlElement("PoolCertificateIdentifier", Order = 2)]
        public MISMOIdentifier PoolCertificateIdentifier { get; set; }
    
        [XmlIgnore]
        public bool PoolCertificateIdentifierSpecified
        {
            get { return this.PoolCertificateIdentifier != null; }
            set { }
        }
    
        [XmlElement("PoolCertificateInitialPaymentDate", Order = 3)]
        public MISMODate PoolCertificateInitialPaymentDate { get; set; }
    
        [XmlIgnore]
        public bool PoolCertificateInitialPaymentDateSpecified
        {
            get { return this.PoolCertificateInitialPaymentDate != null; }
            set { }
        }
    
        [XmlElement("PoolCertificateIssueDate", Order = 4)]
        public MISMODate PoolCertificateIssueDate { get; set; }
    
        [XmlIgnore]
        public bool PoolCertificateIssueDateSpecified
        {
            get { return this.PoolCertificateIssueDate != null; }
            set { }
        }
    
        [XmlElement("PoolCertificateMaturityDate", Order = 5)]
        public MISMODate PoolCertificateMaturityDate { get; set; }
    
        [XmlIgnore]
        public bool PoolCertificateMaturityDateSpecified
        {
            get { return this.PoolCertificateMaturityDate != null; }
            set { }
        }
    
        [XmlElement("PoolCertificateOriginalAmount", Order = 6)]
        public MISMOAmount PoolCertificateOriginalAmount { get; set; }
    
        [XmlIgnore]
        public bool PoolCertificateOriginalAmountSpecified
        {
            get { return this.PoolCertificateOriginalAmount != null; }
            set { }
        }
    
        [XmlElement("PoolCertificatePrincipalPaidAmount", Order = 7)]
        public MISMOAmount PoolCertificatePrincipalPaidAmount { get; set; }
    
        [XmlIgnore]
        public bool PoolCertificatePrincipalPaidAmountSpecified
        {
            get { return this.PoolCertificatePrincipalPaidAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 8)]
        public POOL_CERTIFICATE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
