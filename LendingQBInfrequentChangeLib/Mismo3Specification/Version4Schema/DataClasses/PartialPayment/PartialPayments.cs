namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class PARTIAL_PAYMENTS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.PartialPaymentListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PARTIAL_PAYMENT", Order = 0)]
        public List<PARTIAL_PAYMENT> PartialPaymentList { get; set; } = new List<PARTIAL_PAYMENT>();
    
        [XmlIgnore]
        public bool PartialPaymentListSpecified
        {
            get { return this.PartialPaymentList != null && this.PartialPaymentList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public PARTIAL_PAYMENTS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
