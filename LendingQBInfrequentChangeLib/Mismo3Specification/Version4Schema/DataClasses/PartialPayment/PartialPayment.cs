namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PARTIAL_PAYMENT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.PartialPaymentApplicationItemTypeSpecified
                    || this.PartialPaymentApplicationMethodTypeSpecified
                    || this.PartialPaymentApplicationMethodTypeOtherDescriptionSpecified
                    || this.PartialPaymentApplicationOrderTypeSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PartialPaymentApplicationItemType", Order = 0)]
        public MISMOEnum<PartialPaymentApplicationItemBase> PartialPaymentApplicationItemType { get; set; }
    
        [XmlIgnore]
        public bool PartialPaymentApplicationItemTypeSpecified
        {
            get { return this.PartialPaymentApplicationItemType != null; }
            set { }
        }
    
        [XmlElement("PartialPaymentApplicationMethodType", Order = 1)]
        public MISMOEnum<PartialPaymentApplicationMethodBase> PartialPaymentApplicationMethodType { get; set; }
    
        [XmlIgnore]
        public bool PartialPaymentApplicationMethodTypeSpecified
        {
            get { return this.PartialPaymentApplicationMethodType != null; }
            set { }
        }
    
        [XmlElement("PartialPaymentApplicationMethodTypeOtherDescription", Order = 2)]
        public MISMOString PartialPaymentApplicationMethodTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool PartialPaymentApplicationMethodTypeOtherDescriptionSpecified
        {
            get { return this.PartialPaymentApplicationMethodTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("PartialPaymentApplicationOrderType", Order = 3)]
        public MISMOEnum<PartialPaymentApplicationOrderBase> PartialPaymentApplicationOrderType { get; set; }
    
        [XmlIgnore]
        public bool PartialPaymentApplicationOrderTypeSpecified
        {
            get { return this.PartialPaymentApplicationOrderType != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public PARTIAL_PAYMENT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
