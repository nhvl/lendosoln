namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CONVERSION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ProjectConversionDateSpecified
                    || this.ProjectConversionIndicatorSpecified
                    || this.ProjectConversionOriginalUseDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ProjectConversionDate", Order = 0)]
        public MISMODate ProjectConversionDate { get; set; }
    
        [XmlIgnore]
        public bool ProjectConversionDateSpecified
        {
            get { return this.ProjectConversionDate != null; }
            set { }
        }
    
        [XmlElement("ProjectConversionIndicator", Order = 1)]
        public MISMOIndicator ProjectConversionIndicator { get; set; }
    
        [XmlIgnore]
        public bool ProjectConversionIndicatorSpecified
        {
            get { return this.ProjectConversionIndicator != null; }
            set { }
        }
    
        [XmlElement("ProjectConversionOriginalUseDescription", Order = 2)]
        public MISMOString ProjectConversionOriginalUseDescription { get; set; }
    
        [XmlIgnore]
        public bool ProjectConversionOriginalUseDescriptionSpecified
        {
            get { return this.ProjectConversionOriginalUseDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public CONVERSION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
