namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class RECORDING_ENDORSEMENT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.RecordingEndorsementExemptionsSpecified
                    || this.RecordingEndorsementFeesSpecified
                    || this.RecordingEndorsementInformationSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("RECORDING_ENDORSEMENT_EXEMPTIONS", Order = 0)]
        public RECORDING_ENDORSEMENT_EXEMPTIONS RecordingEndorsementExemptions { get; set; }
    
        [XmlIgnore]
        public bool RecordingEndorsementExemptionsSpecified
        {
            get { return this.RecordingEndorsementExemptions != null && this.RecordingEndorsementExemptions.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("RECORDING_ENDORSEMENT_FEES", Order = 1)]
        public RECORDING_ENDORSEMENT_FEES RecordingEndorsementFees { get; set; }
    
        [XmlIgnore]
        public bool RecordingEndorsementFeesSpecified
        {
            get { return this.RecordingEndorsementFees != null && this.RecordingEndorsementFees.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("RECORDING_ENDORSEMENT_INFORMATION", Order = 2)]
        public RECORDING_ENDORSEMENT_INFORMATION RecordingEndorsementInformation { get; set; }
    
        [XmlIgnore]
        public bool RecordingEndorsementInformationSpecified
        {
            get { return this.RecordingEndorsementInformation != null && this.RecordingEndorsementInformation.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public RECORDING_ENDORSEMENT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
