namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class RECORDING_ENDORSEMENTS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.RecordingEndorsementListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("RECORDING_ENDORSEMENT", Order = 0)]
        public List<RECORDING_ENDORSEMENT> RecordingEndorsementList { get; set; } = new List<RECORDING_ENDORSEMENT>();
    
        [XmlIgnore]
        public bool RecordingEndorsementListSpecified
        {
            get { return this.RecordingEndorsementList != null && this.RecordingEndorsementList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public RECORDING_ENDORSEMENTS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
