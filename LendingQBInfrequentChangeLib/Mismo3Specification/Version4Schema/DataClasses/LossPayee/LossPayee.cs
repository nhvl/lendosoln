namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class LOSS_PAYEE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.LossPayeeTypeSpecified
                    || this.LossPayeeTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("LossPayeeType", Order = 0)]
        public MISMOEnum<LossPayeeBase> LossPayeeType { get; set; }
    
        [XmlIgnore]
        public bool LossPayeeTypeSpecified
        {
            get { return this.LossPayeeType != null; }
            set { }
        }
    
        [XmlElement("LossPayeeTypeOtherDescription", Order = 1)]
        public MISMOString LossPayeeTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool LossPayeeTypeOtherDescriptionSpecified
        {
            get { return this.LossPayeeTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public LOSS_PAYEE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
