namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class DRAW_ACTIVITIES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DrawActivityListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DRAW_ACTIVITY", Order = 0)]
        public List<DRAW_ACTIVITY> DrawActivityList { get; set; } = new List<DRAW_ACTIVITY>();
    
        [XmlIgnore]
        public bool DrawActivityListSpecified
        {
            get { return this.DrawActivityList != null && this.DrawActivityList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public DRAW_ACTIVITIES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
