namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class DRAW_ACTIVITY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.InitialDrawIndicatorSpecified
                    || this.LoanDrawAmountSpecified
                    || this.LoanDrawDateSpecified
                    || this.LoanTotalDrawAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("InitialDrawIndicator", Order = 0)]
        public MISMOIndicator InitialDrawIndicator { get; set; }
    
        [XmlIgnore]
        public bool InitialDrawIndicatorSpecified
        {
            get { return this.InitialDrawIndicator != null; }
            set { }
        }
    
        [XmlElement("LoanDrawAmount", Order = 1)]
        public MISMOAmount LoanDrawAmount { get; set; }
    
        [XmlIgnore]
        public bool LoanDrawAmountSpecified
        {
            get { return this.LoanDrawAmount != null; }
            set { }
        }
    
        [XmlElement("LoanDrawDate", Order = 2)]
        public MISMODate LoanDrawDate { get; set; }
    
        [XmlIgnore]
        public bool LoanDrawDateSpecified
        {
            get { return this.LoanDrawDate != null; }
            set { }
        }
    
        [XmlElement("LoanTotalDrawAmount", Order = 3)]
        public MISMOAmount LoanTotalDrawAmount { get; set; }
    
        [XmlIgnore]
        public bool LoanTotalDrawAmountSpecified
        {
            get { return this.LoanTotalDrawAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public DRAW_ACTIVITY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
