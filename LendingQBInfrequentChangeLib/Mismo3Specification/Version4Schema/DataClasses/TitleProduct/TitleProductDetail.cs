namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class TITLE_PRODUCT_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.TitleProductEffectiveDatetimeSpecified
                    || this.TitleProductTitleSearchPerformedDescriptionSpecified
                    || this.TitleProductTypeSpecified
                    || this.TitleProductTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("TitleProductEffectiveDatetime", Order = 0)]
        public MISMODatetime TitleProductEffectiveDatetime { get; set; }
    
        [XmlIgnore]
        public bool TitleProductEffectiveDatetimeSpecified
        {
            get { return this.TitleProductEffectiveDatetime != null; }
            set { }
        }
    
        [XmlElement("TitleProductTitleSearchPerformedDescription", Order = 1)]
        public MISMOString TitleProductTitleSearchPerformedDescription { get; set; }
    
        [XmlIgnore]
        public bool TitleProductTitleSearchPerformedDescriptionSpecified
        {
            get { return this.TitleProductTitleSearchPerformedDescription != null; }
            set { }
        }
    
        [XmlElement("TitleProductType", Order = 2)]
        public MISMOEnum<TitleProductBase> TitleProductType { get; set; }
    
        [XmlIgnore]
        public bool TitleProductTypeSpecified
        {
            get { return this.TitleProductType != null; }
            set { }
        }
    
        [XmlElement("TitleProductTypeOtherDescription", Order = 3)]
        public MISMOString TitleProductTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool TitleProductTypeOtherDescriptionSpecified
        {
            get { return this.TitleProductTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public TITLE_PRODUCT_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
