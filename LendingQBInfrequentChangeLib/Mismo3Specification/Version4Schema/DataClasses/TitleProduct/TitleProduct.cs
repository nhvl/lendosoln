namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Xml.Serialization;

    public class TITLE_PRODUCT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                if (Mismo3Utilities.ExceedsThreshold(1,
                    new List<bool> { this.ClosingProtectionLetterSpecified, this.TitleCommitmentSpecified, this.TitlePoliciesSpecified }))
                {
                    throw new System.Xml.Schema.XmlSchemaValidationException(
                        Mismo3Utilities.GetXSDChoiceExceptionMessage(
                        "TITLE_PRODUCT",
                        new List<string> { "CLOSING_PROTECTION_LETTER", "TITLE_COMMITMENT", "TITLE_POLICIES" }));
                }

                return this.ClosingProtectionLetterSpecified
                    || this.TitleCommitmentSpecified
                    || this.TitlePoliciesSpecified
                    || this.PropertySpecified
                    || this.TitleProductDetailSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CLOSING_PROTECTION_LETTER", Order = 0)]
        public CLOSING_PROTECTION_LETTER ClosingProtectionLetter { get; set; }
    
        [XmlIgnore]
        public bool ClosingProtectionLetterSpecified
        {
            get { return this.ClosingProtectionLetter != null && this.ClosingProtectionLetter.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("TITLE_COMMITMENT", Order = 1)]
        public TITLE_COMMITMENT TitleCommitment { get; set; }
    
        [XmlIgnore]
        public bool TitleCommitmentSpecified
        {
            get { return this.TitleCommitment != null && this.TitleCommitment.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("TITLE_POLICIES", Order = 2)]
        public TITLE_POLICIES TitlePolicies { get; set; }
    
        [XmlIgnore]
        public bool TitlePoliciesSpecified
        {
            get { return this.TitlePolicies != null && this.TitlePolicies.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PROPERTY", Order = 4)]
        public PROPERTY Property { get; set; }
    
        [XmlIgnore]
        public bool PropertySpecified
        {
            get { return this.Property != null && this.Property.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("TITLE_PRODUCT_DETAIL", Order = 5)]
        public TITLE_PRODUCT_DETAIL TitleProductDetail { get; set; }
    
        [XmlIgnore]
        public bool TitleProductDetailSpecified
        {
            get { return this.TitleProductDetail != null && this.TitleProductDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 6)]
        public TITLE_PRODUCT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
