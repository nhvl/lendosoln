namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class TITLE_PRODUCTS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.TitleProductListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("TITLE_PRODUCT", Order = 0)]
        public List<TITLE_PRODUCT> TitleProductList { get; set; } = new List<TITLE_PRODUCT>();
    
        [XmlIgnore]
        public bool TitleProductListSpecified
        {
            get { return this.TitleProductList != null && this.TitleProductList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public TITLE_PRODUCTS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
