namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class AVMS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AvmListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AVM", Order = 0)]
        public List<AVM> AvmList { get; set; } = new List<AVM>();
    
        [XmlIgnore]
        public bool AvmListSpecified
        {
            get { return this.AvmList != null && this.AvmList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public AVMS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
