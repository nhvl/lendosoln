namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class AVM
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AVMCascadePreferenceIdentifierSpecified
                    || this.AVMConfidenceLevelIdentifierSpecified
                    || this.AVMConfidenceScoreIndicatorSpecified
                    || this.AVMConfidenceScoreValueSpecified
                    || this.AVMDateSpecified
                    || this.AVMHighValueRangeAmountSpecified
                    || this.AVMIndexTypeSpecified
                    || this.AVMIndexTypeOtherDescriptionSpecified
                    || this.AVMLowValueRangeAmountSpecified
                    || this.AVMMethodTypeSpecified
                    || this.AVMMethodTypeOtherDescriptionSpecified
                    || this.AVMModelEffectiveDateSpecified
                    || this.AVMModelNameTypeSpecified
                    || this.AVMModelNameTypeOtherDescriptionSpecified
                    || this.AVMOutcomeTypeSpecified
                    || this.AVMOutcomeTypeOtherDescriptionSpecified
                    || this.AVMServiceProviderNameSpecified
                    || this.AVMValueAmountSpecified
                    || this.ForecastStandardDeviationScoreValueSpecified
                    || this.ForecastStandardDeviationScoringModelNameSpecified
                    || this.ValuationRequestCascadeRuleIdentifierSpecified
                    || this.ValuationRequestCascadingReturnTypeSpecified
                    || this.ValuationRequestCascadingReturnTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AVMCascadePreferenceIdentifier", Order = 0)]
        public MISMOIdentifier AVMCascadePreferenceIdentifier { get; set; }
    
        [XmlIgnore]
        public bool AVMCascadePreferenceIdentifierSpecified
        {
            get { return this.AVMCascadePreferenceIdentifier != null; }
            set { }
        }
    
        [XmlElement("AVMConfidenceLevelIdentifier", Order = 1)]
        public MISMOIdentifier AVMConfidenceLevelIdentifier { get; set; }
    
        [XmlIgnore]
        public bool AVMConfidenceLevelIdentifierSpecified
        {
            get { return this.AVMConfidenceLevelIdentifier != null; }
            set { }
        }
    
        [XmlElement("AVMConfidenceScoreIndicator", Order = 2)]
        public MISMOIndicator AVMConfidenceScoreIndicator { get; set; }
    
        [XmlIgnore]
        public bool AVMConfidenceScoreIndicatorSpecified
        {
            get { return this.AVMConfidenceScoreIndicator != null; }
            set { }
        }
    
        [XmlElement("AVMConfidenceScoreValue", Order = 3)]
        public MISMOValue AVMConfidenceScoreValue { get; set; }
    
        [XmlIgnore]
        public bool AVMConfidenceScoreValueSpecified
        {
            get { return this.AVMConfidenceScoreValue != null; }
            set { }
        }
    
        [XmlElement("AVMDate", Order = 4)]
        public MISMODate AVMDate { get; set; }
    
        [XmlIgnore]
        public bool AVMDateSpecified
        {
            get { return this.AVMDate != null; }
            set { }
        }
    
        [XmlElement("AVMHighValueRangeAmount", Order = 5)]
        public MISMOAmount AVMHighValueRangeAmount { get; set; }
    
        [XmlIgnore]
        public bool AVMHighValueRangeAmountSpecified
        {
            get { return this.AVMHighValueRangeAmount != null; }
            set { }
        }
    
        [XmlElement("AVMIndexType", Order = 6)]
        public MISMOEnum<AVMIndexBase> AVMIndexType { get; set; }
    
        [XmlIgnore]
        public bool AVMIndexTypeSpecified
        {
            get { return this.AVMIndexType != null; }
            set { }
        }
    
        [XmlElement("AVMIndexTypeOtherDescription", Order = 7)]
        public MISMOString AVMIndexTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool AVMIndexTypeOtherDescriptionSpecified
        {
            get { return this.AVMIndexTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("AVMLowValueRangeAmount", Order = 8)]
        public MISMOAmount AVMLowValueRangeAmount { get; set; }
    
        [XmlIgnore]
        public bool AVMLowValueRangeAmountSpecified
        {
            get { return this.AVMLowValueRangeAmount != null; }
            set { }
        }
    
        [XmlElement("AVMMethodType", Order = 9)]
        public MISMOEnum<AVMMethodBase> AVMMethodType { get; set; }
    
        [XmlIgnore]
        public bool AVMMethodTypeSpecified
        {
            get { return this.AVMMethodType != null; }
            set { }
        }
    
        [XmlElement("AVMMethodTypeOtherDescription", Order = 10)]
        public MISMOString AVMMethodTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool AVMMethodTypeOtherDescriptionSpecified
        {
            get { return this.AVMMethodTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("AVMModelEffectiveDate", Order = 11)]
        public MISMODate AVMModelEffectiveDate { get; set; }
    
        [XmlIgnore]
        public bool AVMModelEffectiveDateSpecified
        {
            get { return this.AVMModelEffectiveDate != null; }
            set { }
        }
    
        [XmlElement("AVMModelNameType", Order = 12)]
        public MISMOEnum<AVMModelNameBase> AVMModelNameType { get; set; }
    
        [XmlIgnore]
        public bool AVMModelNameTypeSpecified
        {
            get { return this.AVMModelNameType != null; }
            set { }
        }
    
        [XmlElement("AVMModelNameTypeOtherDescription", Order = 13)]
        public MISMOString AVMModelNameTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool AVMModelNameTypeOtherDescriptionSpecified
        {
            get { return this.AVMModelNameTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("AVMOutcomeType", Order = 14)]
        public MISMOEnum<AVMOutcomeBase> AVMOutcomeType { get; set; }
    
        [XmlIgnore]
        public bool AVMOutcomeTypeSpecified
        {
            get { return this.AVMOutcomeType != null; }
            set { }
        }
    
        [XmlElement("AVMOutcomeTypeOtherDescription", Order = 15)]
        public MISMOString AVMOutcomeTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool AVMOutcomeTypeOtherDescriptionSpecified
        {
            get { return this.AVMOutcomeTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("AVMServiceProviderName", Order = 16)]
        public MISMOString AVMServiceProviderName { get; set; }
    
        [XmlIgnore]
        public bool AVMServiceProviderNameSpecified
        {
            get { return this.AVMServiceProviderName != null; }
            set { }
        }
    
        [XmlElement("AVMValueAmount", Order = 17)]
        public MISMOAmount AVMValueAmount { get; set; }
    
        [XmlIgnore]
        public bool AVMValueAmountSpecified
        {
            get { return this.AVMValueAmount != null; }
            set { }
        }
    
        [XmlElement("ForecastStandardDeviationScoreValue", Order = 18)]
        public MISMOValue ForecastStandardDeviationScoreValue { get; set; }
    
        [XmlIgnore]
        public bool ForecastStandardDeviationScoreValueSpecified
        {
            get { return this.ForecastStandardDeviationScoreValue != null; }
            set { }
        }
    
        [XmlElement("ForecastStandardDeviationScoringModelName", Order = 19)]
        public MISMOString ForecastStandardDeviationScoringModelName { get; set; }
    
        [XmlIgnore]
        public bool ForecastStandardDeviationScoringModelNameSpecified
        {
            get { return this.ForecastStandardDeviationScoringModelName != null; }
            set { }
        }
    
        [XmlElement("ValuationRequestCascadeRuleIdentifier", Order = 20)]
        public MISMOIdentifier ValuationRequestCascadeRuleIdentifier { get; set; }
    
        [XmlIgnore]
        public bool ValuationRequestCascadeRuleIdentifierSpecified
        {
            get { return this.ValuationRequestCascadeRuleIdentifier != null; }
            set { }
        }
    
        [XmlElement("ValuationRequestCascadingReturnType", Order = 21)]
        public MISMOEnum<ValuationRequestCascadingReturnBase> ValuationRequestCascadingReturnType { get; set; }
    
        [XmlIgnore]
        public bool ValuationRequestCascadingReturnTypeSpecified
        {
            get { return this.ValuationRequestCascadingReturnType != null; }
            set { }
        }
    
        [XmlElement("ValuationRequestCascadingReturnTypeOtherDescription", Order = 22)]
        public MISMOString ValuationRequestCascadingReturnTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ValuationRequestCascadingReturnTypeOtherDescriptionSpecified
        {
            get { return this.ValuationRequestCascadingReturnTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 23)]
        public AVM_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
