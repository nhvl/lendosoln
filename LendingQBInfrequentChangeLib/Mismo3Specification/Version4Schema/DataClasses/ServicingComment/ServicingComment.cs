namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class SERVICING_COMMENT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ServicingCommentDatetimeSpecified
                    || this.ServicingCommentSourceDescriptionSpecified
                    || this.ServicingCommentTextSpecified
                    || this.ServicingCommentTypeSpecified
                    || this.ServicingCommentTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ServicingCommentDatetime", Order = 0)]
        public MISMODatetime ServicingCommentDatetime { get; set; }
    
        [XmlIgnore]
        public bool ServicingCommentDatetimeSpecified
        {
            get { return this.ServicingCommentDatetime != null; }
            set { }
        }
    
        [XmlElement("ServicingCommentSourceDescription", Order = 1)]
        public MISMOString ServicingCommentSourceDescription { get; set; }
    
        [XmlIgnore]
        public bool ServicingCommentSourceDescriptionSpecified
        {
            get { return this.ServicingCommentSourceDescription != null; }
            set { }
        }
    
        [XmlElement("ServicingCommentText", Order = 2)]
        public MISMOString ServicingCommentText { get; set; }
    
        [XmlIgnore]
        public bool ServicingCommentTextSpecified
        {
            get { return this.ServicingCommentText != null; }
            set { }
        }
    
        [XmlElement("ServicingCommentType", Order = 3)]
        public MISMOEnum<ServicingCommentBase> ServicingCommentType { get; set; }
    
        [XmlIgnore]
        public bool ServicingCommentTypeSpecified
        {
            get { return this.ServicingCommentType != null; }
            set { }
        }
    
        [XmlElement("ServicingCommentTypeOtherDescription", Order = 4)]
        public MISMOString ServicingCommentTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ServicingCommentTypeOtherDescriptionSpecified
        {
            get { return this.ServicingCommentTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 5)]
        public SERVICING_COMMENT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
