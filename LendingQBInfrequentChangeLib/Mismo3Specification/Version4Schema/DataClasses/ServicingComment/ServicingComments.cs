namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class SERVICING_COMMENTS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ServicingCommentListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("SERVICING_COMMENT", Order = 0)]
        public List<SERVICING_COMMENT> ServicingCommentList { get; set; } = new List<SERVICING_COMMENT>();
    
        [XmlIgnore]
        public bool ServicingCommentListSpecified
        {
            get { return this.ServicingCommentList != null && this.ServicingCommentList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public SERVICING_COMMENTS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
