namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class SERVICER_REPORTING_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.InvestorReportingCycleDateSpecified
                    || this.InvestorReportingCycleDescriptionSpecified
                    || this.PoolReportingRevisionIndicatorSpecified
                    || this.PortfolioDelinquencyIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("InvestorReportingCycleDate", Order = 0)]
        public MISMODate InvestorReportingCycleDate { get; set; }
    
        [XmlIgnore]
        public bool InvestorReportingCycleDateSpecified
        {
            get { return this.InvestorReportingCycleDate != null; }
            set { }
        }
    
        [XmlElement("InvestorReportingCycleDescription", Order = 1)]
        public MISMOString InvestorReportingCycleDescription { get; set; }
    
        [XmlIgnore]
        public bool InvestorReportingCycleDescriptionSpecified
        {
            get { return this.InvestorReportingCycleDescription != null; }
            set { }
        }
    
        [XmlElement("PoolReportingRevisionIndicator", Order = 2)]
        public MISMOIndicator PoolReportingRevisionIndicator { get; set; }
    
        [XmlIgnore]
        public bool PoolReportingRevisionIndicatorSpecified
        {
            get { return this.PoolReportingRevisionIndicator != null; }
            set { }
        }
    
        [XmlElement("PortfolioDelinquencyIndicator", Order = 3)]
        public MISMOIndicator PortfolioDelinquencyIndicator { get; set; }
    
        [XmlIgnore]
        public bool PortfolioDelinquencyIndicatorSpecified
        {
            get { return this.PortfolioDelinquencyIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public SERVICER_REPORTING_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
