namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class SERVICER_REPORTING
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ServicerReportingDetailSpecified
                    || this.ServicingCommentsSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("SERVICER_REPORTING_DETAIL", Order = 0)]
        public SERVICER_REPORTING_DETAIL ServicerReportingDetail { get; set; }
    
        [XmlIgnore]
        public bool ServicerReportingDetailSpecified
        {
            get { return this.ServicerReportingDetail != null && this.ServicerReportingDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SERVICING_COMMENTS", Order = 1)]
        public SERVICING_COMMENTS ServicingComments { get; set; }
    
        [XmlIgnore]
        public bool ServicingCommentsSpecified
        {
            get { return this.ServicingComments != null && this.ServicingComments.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public SERVICER_REPORTING_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
