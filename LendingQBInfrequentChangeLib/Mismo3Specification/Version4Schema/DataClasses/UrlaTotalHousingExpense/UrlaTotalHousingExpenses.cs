namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class URLA_TOTAL_HOUSING_EXPENSES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.UrlaTotalHousingExpenseListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("URLA_TOTAL_HOUSING_EXPENSE", Order = 0)]
        public List<URLA_TOTAL_HOUSING_EXPENSE> UrlaTotalHousingExpenseList { get; set; } = new List<URLA_TOTAL_HOUSING_EXPENSE>();
    
        [XmlIgnore]
        public bool UrlaTotalHousingExpenseListSpecified
        {
            get { return this.UrlaTotalHousingExpenseList != null && this.UrlaTotalHousingExpenseList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public URLA_TOTAL_HOUSING_EXPENSES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
