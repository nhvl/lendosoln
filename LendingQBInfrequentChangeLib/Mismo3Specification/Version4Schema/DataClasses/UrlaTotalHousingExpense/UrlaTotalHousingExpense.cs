namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class URLA_TOTAL_HOUSING_EXPENSE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.HousingExpenseTypeSpecified
                    || this.HousingExpenseTypeOtherDescriptionSpecified
                    || this.URLATotalHousingExpensePaymentAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("HousingExpenseType", Order = 0)]
        public MISMOEnum<HousingExpenseBase> HousingExpenseType { get; set; }
    
        [XmlIgnore]
        public bool HousingExpenseTypeSpecified
        {
            get { return this.HousingExpenseType != null; }
            set { }
        }
    
        [XmlElement("HousingExpenseTypeOtherDescription", Order = 1)]
        public MISMOString HousingExpenseTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool HousingExpenseTypeOtherDescriptionSpecified
        {
            get { return this.HousingExpenseTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("URLATotalHousingExpensePaymentAmount", Order = 2)]
        public MISMOAmount URLATotalHousingExpensePaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool URLATotalHousingExpensePaymentAmountSpecified
        {
            get { return this.URLATotalHousingExpensePaymentAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public URLA_TOTAL_HOUSING_EXPENSE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
