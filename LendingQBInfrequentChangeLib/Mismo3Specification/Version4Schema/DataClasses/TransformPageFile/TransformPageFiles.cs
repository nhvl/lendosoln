namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class TRANSFORM_PAGE_FILES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.TransformPageFileListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("TRANSFORM_PAGE_FILE", Order = 0)]
        public List<TRANSFORM_PAGE_FILE> TransformPageFileList { get; set; } = new List<TRANSFORM_PAGE_FILE>();
    
        [XmlIgnore]
        public bool TransformPageFileListSpecified
        {
            get { return this.TransformPageFileList != null && this.TransformPageFileList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public TRANSFORM_PAGE_FILES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
