namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class LTV
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BaseLTVRatioPercentSpecified
                    || this.LTVCalculationDateSpecified
                    || this.LTVRatioPercentSpecified
                    || this.PostCapitalizationMarkToMarketLTVRatioPercentSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("BaseLTVRatioPercent", Order = 0)]
        public MISMOPercent BaseLTVRatioPercent { get; set; }
    
        [XmlIgnore]
        public bool BaseLTVRatioPercentSpecified
        {
            get { return this.BaseLTVRatioPercent != null; }
            set { }
        }
    
        [XmlElement("LTVCalculationDate", Order = 1)]
        public MISMODate LTVCalculationDate { get; set; }
    
        [XmlIgnore]
        public bool LTVCalculationDateSpecified
        {
            get { return this.LTVCalculationDate != null; }
            set { }
        }
    
        [XmlElement("LTVRatioPercent", Order = 2)]
        public MISMOPercent LTVRatioPercent { get; set; }
    
        [XmlIgnore]
        public bool LTVRatioPercentSpecified
        {
            get { return this.LTVRatioPercent != null; }
            set { }
        }
    
        [XmlElement("PostCapitalizationMarkToMarketLTVRatioPercent", Order = 3)]
        public MISMOPercent PostCapitalizationMarkToMarketLTVRatioPercent { get; set; }
    
        [XmlIgnore]
        public bool PostCapitalizationMarkToMarketLTVRatioPercentSpecified
        {
            get { return this.PostCapitalizationMarkToMarketLTVRatioPercent != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public LTV_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
