namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class MARKET_INVENTORY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.MarketInventoryAmountSpecified
                    || this.MarketInventoryCountSpecified
                    || this.MarketInventoryMonthRangeTypeSpecified
                    || this.MarketInventoryRatioPercentSpecified
                    || this.MarketInventoryTrendTypeSpecified
                    || this.MarketInventoryTypeSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("MarketInventoryAmount", Order = 0)]
        public MISMOAmount MarketInventoryAmount { get; set; }
    
        [XmlIgnore]
        public bool MarketInventoryAmountSpecified
        {
            get { return this.MarketInventoryAmount != null; }
            set { }
        }
    
        [XmlElement("MarketInventoryCount", Order = 1)]
        public MISMOCount MarketInventoryCount { get; set; }
    
        [XmlIgnore]
        public bool MarketInventoryCountSpecified
        {
            get { return this.MarketInventoryCount != null; }
            set { }
        }
    
        [XmlElement("MarketInventoryMonthRangeType", Order = 2)]
        public MISMOEnum<MarketInventoryMonthRangeBase> MarketInventoryMonthRangeType { get; set; }
    
        [XmlIgnore]
        public bool MarketInventoryMonthRangeTypeSpecified
        {
            get { return this.MarketInventoryMonthRangeType != null; }
            set { }
        }
    
        [XmlElement("MarketInventoryRatioPercent", Order = 3)]
        public MISMOPercent MarketInventoryRatioPercent { get; set; }
    
        [XmlIgnore]
        public bool MarketInventoryRatioPercentSpecified
        {
            get { return this.MarketInventoryRatioPercent != null; }
            set { }
        }
    
        [XmlElement("MarketInventoryTrendType", Order = 4)]
        public MISMOEnum<MarketInventoryTrendBase> MarketInventoryTrendType { get; set; }
    
        [XmlIgnore]
        public bool MarketInventoryTrendTypeSpecified
        {
            get { return this.MarketInventoryTrendType != null; }
            set { }
        }
    
        [XmlElement("MarketInventoryType", Order = 5)]
        public MISMOEnum<MarketInventoryBase> MarketInventoryType { get; set; }
    
        [XmlIgnore]
        public bool MarketInventoryTypeSpecified
        {
            get { return this.MarketInventoryType != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 6)]
        public MARKET_INVENTORY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
