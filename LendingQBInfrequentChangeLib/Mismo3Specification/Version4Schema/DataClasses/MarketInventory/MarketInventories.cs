namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class MARKET_INVENTORIES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.MarketInventoryListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("MARKET_INVENTORY", Order = 0)]
        public List<MARKET_INVENTORY> MarketInventoryList { get; set; } = new List<MARKET_INVENTORY>();
    
        [XmlIgnore]
        public bool MarketInventoryListSpecified
        {
            get { return this.MarketInventoryList != null && this.MarketInventoryList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public MARKET_INVENTORIES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
