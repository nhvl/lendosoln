namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class UNPLATTED_LAND
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AbstractIdentifierSpecified
                    || this.BaseIdentifierSpecified
                    || this.LegalTractIdentifierSpecified
                    || this.MeridianIdentifierSpecified
                    || this.MetesAndBoundsRemainingDescriptionSpecified
                    || this.QuarterSectionIdentifierSpecified
                    || this.RangeIdentifierSpecified
                    || this.SectionIdentifierSpecified
                    || this.TownshipIdentifierSpecified
                    || this.UnplattedLandTypeSpecified
                    || this.UnplattedLandTypeIdentifierSpecified
                    || this.UnplattedLandTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AbstractIdentifier", Order = 0)]
        public MISMOIdentifier AbstractIdentifier { get; set; }
    
        [XmlIgnore]
        public bool AbstractIdentifierSpecified
        {
            get { return this.AbstractIdentifier != null; }
            set { }
        }
    
        [XmlElement("BaseIdentifier", Order = 1)]
        public MISMOIdentifier BaseIdentifier { get; set; }
    
        [XmlIgnore]
        public bool BaseIdentifierSpecified
        {
            get { return this.BaseIdentifier != null; }
            set { }
        }
    
        [XmlElement("LegalTractIdentifier", Order = 2)]
        public MISMOIdentifier LegalTractIdentifier { get; set; }
    
        [XmlIgnore]
        public bool LegalTractIdentifierSpecified
        {
            get { return this.LegalTractIdentifier != null; }
            set { }
        }
    
        [XmlElement("MeridianIdentifier", Order = 3)]
        public MISMOIdentifier MeridianIdentifier { get; set; }
    
        [XmlIgnore]
        public bool MeridianIdentifierSpecified
        {
            get { return this.MeridianIdentifier != null; }
            set { }
        }
    
        [XmlElement("MetesAndBoundsRemainingDescription", Order = 4)]
        public MISMOString MetesAndBoundsRemainingDescription { get; set; }
    
        [XmlIgnore]
        public bool MetesAndBoundsRemainingDescriptionSpecified
        {
            get { return this.MetesAndBoundsRemainingDescription != null; }
            set { }
        }
    
        [XmlElement("QuarterSectionIdentifier", Order = 5)]
        public MISMOIdentifier QuarterSectionIdentifier { get; set; }
    
        [XmlIgnore]
        public bool QuarterSectionIdentifierSpecified
        {
            get { return this.QuarterSectionIdentifier != null; }
            set { }
        }
    
        [XmlElement("RangeIdentifier", Order = 6)]
        public MISMOIdentifier RangeIdentifier { get; set; }
    
        [XmlIgnore]
        public bool RangeIdentifierSpecified
        {
            get { return this.RangeIdentifier != null; }
            set { }
        }
    
        [XmlElement("SectionIdentifier", Order = 7)]
        public MISMOIdentifier SectionIdentifier { get; set; }
    
        [XmlIgnore]
        public bool SectionIdentifierSpecified
        {
            get { return this.SectionIdentifier != null; }
            set { }
        }
    
        [XmlElement("TownshipIdentifier", Order = 8)]
        public MISMOIdentifier TownshipIdentifier { get; set; }
    
        [XmlIgnore]
        public bool TownshipIdentifierSpecified
        {
            get { return this.TownshipIdentifier != null; }
            set { }
        }
    
        [XmlElement("UnplattedLandType", Order = 9)]
        public MISMOEnum<UnplattedLandBase> UnplattedLandType { get; set; }
    
        [XmlIgnore]
        public bool UnplattedLandTypeSpecified
        {
            get { return this.UnplattedLandType != null; }
            set { }
        }
    
        [XmlElement("UnplattedLandTypeIdentifier", Order = 10)]
        public MISMOIdentifier UnplattedLandTypeIdentifier { get; set; }
    
        [XmlIgnore]
        public bool UnplattedLandTypeIdentifierSpecified
        {
            get { return this.UnplattedLandTypeIdentifier != null; }
            set { }
        }
    
        [XmlElement("UnplattedLandTypeOtherDescription", Order = 11)]
        public MISMOString UnplattedLandTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool UnplattedLandTypeOtherDescriptionSpecified
        {
            get { return this.UnplattedLandTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 12)]
        public UNPLATTED_LAND_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
