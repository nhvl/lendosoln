namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class UNPLATTED_LANDS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.UnplattedLandListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("UNPLATTED_LAND", Order = 0)]
        public List<UNPLATTED_LAND> UnplattedLandList { get; set; } = new List<UNPLATTED_LAND>();
    
        [XmlIgnore]
        public bool UnplattedLandListSpecified
        {
            get { return this.UnplattedLandList != null && this.UnplattedLandList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public UNPLATTED_LANDS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
