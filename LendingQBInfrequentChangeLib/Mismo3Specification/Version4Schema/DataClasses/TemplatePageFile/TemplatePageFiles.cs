namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class TEMPLATE_PAGE_FILES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.TemplatePageFileListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("TEMPLATE_PAGE_FILE", Order = 0)]
        public List<TEMPLATE_PAGE_FILE> TemplatePageFileList { get; set; } = new List<TEMPLATE_PAGE_FILE>();
    
        [XmlIgnore]
        public bool TemplatePageFileListSpecified
        {
            get { return this.TemplatePageFileList != null && this.TemplatePageFileList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public TEMPLATE_PAGE_FILES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
