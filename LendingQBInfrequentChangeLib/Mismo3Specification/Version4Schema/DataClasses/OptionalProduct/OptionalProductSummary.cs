namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class OPTIONAL_PRODUCT_SUMMARY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.OptionalProductPremiumsTotalAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("OptionalProductPremiumsTotalAmount", Order = 0)]
        public MISMOAmount OptionalProductPremiumsTotalAmount { get; set; }
    
        [XmlIgnore]
        public bool OptionalProductPremiumsTotalAmountSpecified
        {
            get { return this.OptionalProductPremiumsTotalAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public OPTIONAL_PRODUCT_SUMMARY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
