namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class OPTIONAL_PRODUCTS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.OptionalProductListSpecified
                    || this.OptionalProductSummarySpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("OPTIONAL_PRODUCT", Order = 0)]
        public List<OPTIONAL_PRODUCT> OptionalProductList { get; set; } = new List<OPTIONAL_PRODUCT>();
    
        [XmlIgnore]
        public bool OptionalProductListSpecified
        {
            get { return this.OptionalProductList != null && this.OptionalProductList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("OPTIONAL_PRODUCT_SUMMARY", Order = 1)]
        public OPTIONAL_PRODUCT_SUMMARY OptionalProductSummary { get; set; }
    
        [XmlIgnore]
        public bool OptionalProductSummarySpecified
        {
            get { return this.OptionalProductSummary != null && this.OptionalProductSummary.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public OPTIONAL_PRODUCTS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
