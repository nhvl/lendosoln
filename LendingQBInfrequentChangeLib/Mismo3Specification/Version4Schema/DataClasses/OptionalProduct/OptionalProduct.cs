namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class OPTIONAL_PRODUCT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.OptionalProductCancellationReasonTypeSpecified
                    || this.OptionalProductCancellationReasonTypeOtherDescriptionSpecified
                    || this.OptionalProductChangeTypeSpecified
                    || this.OptionalProductChangeTypeOtherDescriptionSpecified
                    || this.OptionalProductEffectiveDateSpecified
                    || this.OptionalProductExpirationDateSpecified
                    || this.OptionalProductPayeeIdentifierSpecified
                    || this.OptionalProductPaymentAmountSpecified
                    || this.OptionalProductPendingChangeEffectiveDateSpecified
                    || this.OptionalProductPendingPaymentAmountSpecified
                    || this.OptionalProductPlanTypeSpecified
                    || this.OptionalProductPlanTypeAvailableFromLenderIndicatorSpecified
                    || this.OptionalProductPlanTypeOtherDescriptionSpecified
                    || this.OptionalProductPlanTypeRequiredIndicatorSpecified
                    || this.OptionalProductPremiumAmountSpecified
                    || this.OptionalProductPremiumTermMonthsCountSpecified
                    || this.OptionalProductProviderAccountIdentifierSpecified
                    || this.OptionalProductProvidersPlanIdentifierSpecified
                    || this.OptionalProductRemittanceDueDateSpecified
                    || this.OptionalProductRemittancePerYearCountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("OptionalProductCancellationReasonType", Order = 0)]
        public MISMOEnum<OptionalProductCancellationReasonBase> OptionalProductCancellationReasonType { get; set; }
    
        [XmlIgnore]
        public bool OptionalProductCancellationReasonTypeSpecified
        {
            get { return this.OptionalProductCancellationReasonType != null; }
            set { }
        }
    
        [XmlElement("OptionalProductCancellationReasonTypeOtherDescription", Order = 1)]
        public MISMOString OptionalProductCancellationReasonTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool OptionalProductCancellationReasonTypeOtherDescriptionSpecified
        {
            get { return this.OptionalProductCancellationReasonTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("OptionalProductChangeType", Order = 2)]
        public MISMOEnum<OptionalProductChangeBase> OptionalProductChangeType { get; set; }
    
        [XmlIgnore]
        public bool OptionalProductChangeTypeSpecified
        {
            get { return this.OptionalProductChangeType != null; }
            set { }
        }
    
        [XmlElement("OptionalProductChangeTypeOtherDescription", Order = 3)]
        public MISMOString OptionalProductChangeTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool OptionalProductChangeTypeOtherDescriptionSpecified
        {
            get { return this.OptionalProductChangeTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("OptionalProductEffectiveDate", Order = 4)]
        public MISMODate OptionalProductEffectiveDate { get; set; }
    
        [XmlIgnore]
        public bool OptionalProductEffectiveDateSpecified
        {
            get { return this.OptionalProductEffectiveDate != null; }
            set { }
        }
    
        [XmlElement("OptionalProductExpirationDate", Order = 5)]
        public MISMODate OptionalProductExpirationDate { get; set; }
    
        [XmlIgnore]
        public bool OptionalProductExpirationDateSpecified
        {
            get { return this.OptionalProductExpirationDate != null; }
            set { }
        }
    
        [XmlElement("OptionalProductPayeeIdentifier", Order = 6)]
        public MISMOIdentifier OptionalProductPayeeIdentifier { get; set; }
    
        [XmlIgnore]
        public bool OptionalProductPayeeIdentifierSpecified
        {
            get { return this.OptionalProductPayeeIdentifier != null; }
            set { }
        }
    
        [XmlElement("OptionalProductPaymentAmount", Order = 7)]
        public MISMOAmount OptionalProductPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool OptionalProductPaymentAmountSpecified
        {
            get { return this.OptionalProductPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("OptionalProductPendingChangeEffectiveDate", Order = 8)]
        public MISMODate OptionalProductPendingChangeEffectiveDate { get; set; }
    
        [XmlIgnore]
        public bool OptionalProductPendingChangeEffectiveDateSpecified
        {
            get { return this.OptionalProductPendingChangeEffectiveDate != null; }
            set { }
        }
    
        [XmlElement("OptionalProductPendingPaymentAmount", Order = 9)]
        public MISMOAmount OptionalProductPendingPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool OptionalProductPendingPaymentAmountSpecified
        {
            get { return this.OptionalProductPendingPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("OptionalProductPlanType", Order = 10)]
        public MISMOEnum<OptionalProductPlanBase> OptionalProductPlanType { get; set; }
    
        [XmlIgnore]
        public bool OptionalProductPlanTypeSpecified
        {
            get { return this.OptionalProductPlanType != null; }
            set { }
        }
    
        [XmlElement("OptionalProductPlanTypeAvailableFromLenderIndicator", Order = 11)]
        public MISMOIndicator OptionalProductPlanTypeAvailableFromLenderIndicator { get; set; }
    
        [XmlIgnore]
        public bool OptionalProductPlanTypeAvailableFromLenderIndicatorSpecified
        {
            get { return this.OptionalProductPlanTypeAvailableFromLenderIndicator != null; }
            set { }
        }
    
        [XmlElement("OptionalProductPlanTypeOtherDescription", Order = 12)]
        public MISMOString OptionalProductPlanTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool OptionalProductPlanTypeOtherDescriptionSpecified
        {
            get { return this.OptionalProductPlanTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("OptionalProductPlanTypeRequiredIndicator", Order = 13)]
        public MISMOIndicator OptionalProductPlanTypeRequiredIndicator { get; set; }
    
        [XmlIgnore]
        public bool OptionalProductPlanTypeRequiredIndicatorSpecified
        {
            get { return this.OptionalProductPlanTypeRequiredIndicator != null; }
            set { }
        }
    
        [XmlElement("OptionalProductPremiumAmount", Order = 14)]
        public MISMOAmount OptionalProductPremiumAmount { get; set; }
    
        [XmlIgnore]
        public bool OptionalProductPremiumAmountSpecified
        {
            get { return this.OptionalProductPremiumAmount != null; }
            set { }
        }
    
        [XmlElement("OptionalProductPremiumTermMonthsCount", Order = 15)]
        public MISMOCount OptionalProductPremiumTermMonthsCount { get; set; }
    
        [XmlIgnore]
        public bool OptionalProductPremiumTermMonthsCountSpecified
        {
            get { return this.OptionalProductPremiumTermMonthsCount != null; }
            set { }
        }
    
        [XmlElement("OptionalProductProviderAccountIdentifier", Order = 16)]
        public MISMOIdentifier OptionalProductProviderAccountIdentifier { get; set; }
    
        [XmlIgnore]
        public bool OptionalProductProviderAccountIdentifierSpecified
        {
            get { return this.OptionalProductProviderAccountIdentifier != null; }
            set { }
        }
    
        [XmlElement("OptionalProductProvidersPlanIdentifier", Order = 17)]
        public MISMOIdentifier OptionalProductProvidersPlanIdentifier { get; set; }
    
        [XmlIgnore]
        public bool OptionalProductProvidersPlanIdentifierSpecified
        {
            get { return this.OptionalProductProvidersPlanIdentifier != null; }
            set { }
        }
    
        [XmlElement("OptionalProductRemittanceDueDate", Order = 18)]
        public MISMODate OptionalProductRemittanceDueDate { get; set; }
    
        [XmlIgnore]
        public bool OptionalProductRemittanceDueDateSpecified
        {
            get { return this.OptionalProductRemittanceDueDate != null; }
            set { }
        }
    
        [XmlElement("OptionalProductRemittancePerYearCount", Order = 19)]
        public MISMOCount OptionalProductRemittancePerYearCount { get; set; }
    
        [XmlIgnore]
        public bool OptionalProductRemittancePerYearCountSpecified
        {
            get { return this.OptionalProductRemittancePerYearCount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 20)]
        public OPTIONAL_PRODUCT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
