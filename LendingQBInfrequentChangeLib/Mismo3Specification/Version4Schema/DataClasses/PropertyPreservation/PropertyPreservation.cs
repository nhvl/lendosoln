namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PROPERTY_PRESERVATION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.PropertyPreservationActionCompletionDateSpecified
                    || this.PropertyPreservationActionTypeSpecified
                    || this.PropertyPreservationActionTypeOtherDescriptionSpecified
                    || this.PropertyPreservationStatusDateSpecified
                    || this.PropertyPreservationStatusTypeSpecified
                    || this.PropertyPreservationStatusTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PropertyPreservationActionCompletionDate", Order = 0)]
        public MISMODate PropertyPreservationActionCompletionDate { get; set; }
    
        [XmlIgnore]
        public bool PropertyPreservationActionCompletionDateSpecified
        {
            get { return this.PropertyPreservationActionCompletionDate != null; }
            set { }
        }
    
        [XmlElement("PropertyPreservationActionType", Order = 1)]
        public MISMOEnum<PropertyPreservationActionBase> PropertyPreservationActionType { get; set; }
    
        [XmlIgnore]
        public bool PropertyPreservationActionTypeSpecified
        {
            get { return this.PropertyPreservationActionType != null; }
            set { }
        }
    
        [XmlElement("PropertyPreservationActionTypeOtherDescription", Order = 2)]
        public MISMOString PropertyPreservationActionTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool PropertyPreservationActionTypeOtherDescriptionSpecified
        {
            get { return this.PropertyPreservationActionTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("PropertyPreservationStatusDate", Order = 3)]
        public MISMODate PropertyPreservationStatusDate { get; set; }
    
        [XmlIgnore]
        public bool PropertyPreservationStatusDateSpecified
        {
            get { return this.PropertyPreservationStatusDate != null; }
            set { }
        }
    
        [XmlElement("PropertyPreservationStatusType", Order = 4)]
        public MISMOEnum<PropertyPreservationStatusBase> PropertyPreservationStatusType { get; set; }
    
        [XmlIgnore]
        public bool PropertyPreservationStatusTypeSpecified
        {
            get { return this.PropertyPreservationStatusType != null; }
            set { }
        }
    
        [XmlElement("PropertyPreservationStatusTypeOtherDescription", Order = 5)]
        public MISMOString PropertyPreservationStatusTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool PropertyPreservationStatusTypeOtherDescriptionSpecified
        {
            get { return this.PropertyPreservationStatusTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 6)]
        public PROPERTY_PRESERVATION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
