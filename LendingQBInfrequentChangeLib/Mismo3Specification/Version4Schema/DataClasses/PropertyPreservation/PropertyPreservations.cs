namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class PROPERTY_PRESERVATIONS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.PropertyPreservationListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PROPERTY_PRESERVATION", Order = 0)]
        public List<PROPERTY_PRESERVATION> PropertyPreservationList { get; set; } = new List<PROPERTY_PRESERVATION>();
    
        [XmlIgnore]
        public bool PropertyPreservationListSpecified
        {
            get { return this.PropertyPreservationList != null && this.PropertyPreservationList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public PROPERTY_PRESERVATIONS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
