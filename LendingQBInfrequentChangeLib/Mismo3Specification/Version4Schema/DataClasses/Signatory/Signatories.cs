namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class SIGNATORIES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.SignatoryListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("SIGNATORY", Order = 0)]
        public List<SIGNATORY> SignatoryList { get; set; } = new List<SIGNATORY>();
    
        [XmlIgnore]
        public bool SignatoryListSpecified
        {
            get { return this.SignatoryList != null && this.SignatoryList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public SIGNATORIES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
