namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class SIGNATORY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ElectronicSignatureSpecified
                    || this.ExecutionSpecified
                    || this.NotaryCertificateSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ELECTRONIC_SIGNATURE", Order = 0)]
        public ELECTRONIC_SIGNATURE ElectronicSignature { get; set; }
    
        [XmlIgnore]
        public bool ElectronicSignatureSpecified
        {
            get { return this.ElectronicSignature != null && this.ElectronicSignature.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXECUTION", Order = 1)]
        public EXECUTION Execution { get; set; }
    
        [XmlIgnore]
        public bool ExecutionSpecified
        {
            get { return this.Execution != null && this.Execution.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("NOTARY_CERTIFICATE", Order = 2)]
        public NOTARY_CERTIFICATE NotaryCertificate { get; set; }
    
        [XmlIgnore]
        public bool NotaryCertificateSpecified
        {
            get { return this.NotaryCertificate != null && this.NotaryCertificate.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public SIGNATORY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
