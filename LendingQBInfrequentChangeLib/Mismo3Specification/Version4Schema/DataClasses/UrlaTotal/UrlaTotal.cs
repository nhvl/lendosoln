namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class URLA_TOTAL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.URLATotalAssetsAmountSpecified
                    || this.URLATotalBaseIncomeAmountSpecified
                    || this.URLATotalBonusIncomeAmountSpecified
                    || this.URLATotalCashFromToBorrowerAmountSpecified
                    || this.URLATotalCombinedPresentHousingExpenseAmountSpecified
                    || this.URLATotalCombinedProposedHousingExpenseAmountSpecified
                    || this.URLATotalCommissionsIncomeAmountSpecified
                    || this.URLATotalDividendsInterestIncomeAmountSpecified
                    || this.URLATotalDueFromBorrowerAtClosingAmountSpecified
                    || this.URLATotalLiabilityMonthlyPaymentsAmountSpecified
                    || this.URLATotalLiabilityUPBAmountSpecified
                    || this.URLATotalLotAndImprovementsAmountSpecified
                    || this.URLATotalMonthlyIncomeAmountSpecified
                    || this.URLATotalNetRentalIncomeAmountSpecified
                    || this.URLATotalNetWorthAmountSpecified
                    || this.URLATotalOtherTypesOfIncomeAmountSpecified
                    || this.URLATotalOvertimeIncomeAmountSpecified
                    || this.URLATotalREOLienInstallmentAmountSpecified
                    || this.URLATotalREOLienUPBAmountSpecified
                    || this.URLATotalREOMaintenanceExpenseAmountSpecified
                    || this.URLATotalREOMarketValueAmountSpecified
                    || this.URLATotalREORentalIncomeGrossAmountSpecified
                    || this.URLATotalREORentalIncomeNetAmountSpecified
                    || this.URLATotalTransactionCostAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("URLATotalAssetsAmount", Order = 0)]
        public MISMOAmount URLATotalAssetsAmount { get; set; }
    
        [XmlIgnore]
        public bool URLATotalAssetsAmountSpecified
        {
            get { return this.URLATotalAssetsAmount != null; }
            set { }
        }
    
        [XmlElement("URLATotalBaseIncomeAmount", Order = 1)]
        public MISMOAmount URLATotalBaseIncomeAmount { get; set; }
    
        [XmlIgnore]
        public bool URLATotalBaseIncomeAmountSpecified
        {
            get { return this.URLATotalBaseIncomeAmount != null; }
            set { }
        }
    
        [XmlElement("URLATotalBonusIncomeAmount", Order = 2)]
        public MISMOAmount URLATotalBonusIncomeAmount { get; set; }
    
        [XmlIgnore]
        public bool URLATotalBonusIncomeAmountSpecified
        {
            get { return this.URLATotalBonusIncomeAmount != null; }
            set { }
        }
    
        [XmlElement("URLATotalCashFromToBorrowerAmount", Order = 3)]
        public MISMOAmount URLATotalCashFromToBorrowerAmount { get; set; }
    
        [XmlIgnore]
        public bool URLATotalCashFromToBorrowerAmountSpecified
        {
            get { return this.URLATotalCashFromToBorrowerAmount != null; }
            set { }
        }
    
        [XmlElement("URLATotalCombinedPresentHousingExpenseAmount", Order = 4)]
        public MISMOAmount URLATotalCombinedPresentHousingExpenseAmount { get; set; }
    
        [XmlIgnore]
        public bool URLATotalCombinedPresentHousingExpenseAmountSpecified
        {
            get { return this.URLATotalCombinedPresentHousingExpenseAmount != null; }
            set { }
        }
    
        [XmlElement("URLATotalCombinedProposedHousingExpenseAmount", Order = 5)]
        public MISMOAmount URLATotalCombinedProposedHousingExpenseAmount { get; set; }
    
        [XmlIgnore]
        public bool URLATotalCombinedProposedHousingExpenseAmountSpecified
        {
            get { return this.URLATotalCombinedProposedHousingExpenseAmount != null; }
            set { }
        }
    
        [XmlElement("URLATotalCommissionsIncomeAmount", Order = 6)]
        public MISMOAmount URLATotalCommissionsIncomeAmount { get; set; }
    
        [XmlIgnore]
        public bool URLATotalCommissionsIncomeAmountSpecified
        {
            get { return this.URLATotalCommissionsIncomeAmount != null; }
            set { }
        }
    
        [XmlElement("URLATotalDividendsInterestIncomeAmount", Order = 7)]
        public MISMOAmount URLATotalDividendsInterestIncomeAmount { get; set; }
    
        [XmlIgnore]
        public bool URLATotalDividendsInterestIncomeAmountSpecified
        {
            get { return this.URLATotalDividendsInterestIncomeAmount != null; }
            set { }
        }
    
        [XmlElement("URLATotalDueFromBorrowerAtClosingAmount", Order = 8)]
        public MISMOAmount URLATotalDueFromBorrowerAtClosingAmount { get; set; }
    
        [XmlIgnore]
        public bool URLATotalDueFromBorrowerAtClosingAmountSpecified
        {
            get { return this.URLATotalDueFromBorrowerAtClosingAmount != null; }
            set { }
        }
    
        [XmlElement("URLATotalLiabilityMonthlyPaymentsAmount", Order = 9)]
        public MISMOAmount URLATotalLiabilityMonthlyPaymentsAmount { get; set; }
    
        [XmlIgnore]
        public bool URLATotalLiabilityMonthlyPaymentsAmountSpecified
        {
            get { return this.URLATotalLiabilityMonthlyPaymentsAmount != null; }
            set { }
        }
    
        [XmlElement("URLATotalLiabilityUPBAmount", Order = 10)]
        public MISMOAmount URLATotalLiabilityUPBAmount { get; set; }
    
        [XmlIgnore]
        public bool URLATotalLiabilityUPBAmountSpecified
        {
            get { return this.URLATotalLiabilityUPBAmount != null; }
            set { }
        }
    
        [XmlElement("URLATotalLotAndImprovementsAmount", Order = 11)]
        public MISMOAmount URLATotalLotAndImprovementsAmount { get; set; }
    
        [XmlIgnore]
        public bool URLATotalLotAndImprovementsAmountSpecified
        {
            get { return this.URLATotalLotAndImprovementsAmount != null; }
            set { }
        }
    
        [XmlElement("URLATotalMonthlyIncomeAmount", Order = 12)]
        public MISMOAmount URLATotalMonthlyIncomeAmount { get; set; }
    
        [XmlIgnore]
        public bool URLATotalMonthlyIncomeAmountSpecified
        {
            get { return this.URLATotalMonthlyIncomeAmount != null; }
            set { }
        }
    
        [XmlElement("URLATotalNetRentalIncomeAmount", Order = 13)]
        public MISMOAmount URLATotalNetRentalIncomeAmount { get; set; }
    
        [XmlIgnore]
        public bool URLATotalNetRentalIncomeAmountSpecified
        {
            get { return this.URLATotalNetRentalIncomeAmount != null; }
            set { }
        }
    
        [XmlElement("URLATotalNetWorthAmount", Order = 14)]
        public MISMOAmount URLATotalNetWorthAmount { get; set; }
    
        [XmlIgnore]
        public bool URLATotalNetWorthAmountSpecified
        {
            get { return this.URLATotalNetWorthAmount != null; }
            set { }
        }
    
        [XmlElement("URLATotalOtherTypesOfIncomeAmount", Order = 15)]
        public MISMOAmount URLATotalOtherTypesOfIncomeAmount { get; set; }
    
        [XmlIgnore]
        public bool URLATotalOtherTypesOfIncomeAmountSpecified
        {
            get { return this.URLATotalOtherTypesOfIncomeAmount != null; }
            set { }
        }
    
        [XmlElement("URLATotalOvertimeIncomeAmount", Order = 16)]
        public MISMOAmount URLATotalOvertimeIncomeAmount { get; set; }
    
        [XmlIgnore]
        public bool URLATotalOvertimeIncomeAmountSpecified
        {
            get { return this.URLATotalOvertimeIncomeAmount != null; }
            set { }
        }
    
        [XmlElement("URLATotalREOLienInstallmentAmount", Order = 17)]
        public MISMOAmount URLATotalREOLienInstallmentAmount { get; set; }
    
        [XmlIgnore]
        public bool URLATotalREOLienInstallmentAmountSpecified
        {
            get { return this.URLATotalREOLienInstallmentAmount != null; }
            set { }
        }
    
        [XmlElement("URLATotalREOLienUPBAmount", Order = 18)]
        public MISMOAmount URLATotalREOLienUPBAmount { get; set; }
    
        [XmlIgnore]
        public bool URLATotalREOLienUPBAmountSpecified
        {
            get { return this.URLATotalREOLienUPBAmount != null; }
            set { }
        }
    
        [XmlElement("URLATotalREOMaintenanceExpenseAmount", Order = 19)]
        public MISMOAmount URLATotalREOMaintenanceExpenseAmount { get; set; }
    
        [XmlIgnore]
        public bool URLATotalREOMaintenanceExpenseAmountSpecified
        {
            get { return this.URLATotalREOMaintenanceExpenseAmount != null; }
            set { }
        }
    
        [XmlElement("URLATotalREOMarketValueAmount", Order = 20)]
        public MISMOAmount URLATotalREOMarketValueAmount { get; set; }
    
        [XmlIgnore]
        public bool URLATotalREOMarketValueAmountSpecified
        {
            get { return this.URLATotalREOMarketValueAmount != null; }
            set { }
        }
    
        [XmlElement("URLATotalREORentalIncomeGrossAmount", Order = 21)]
        public MISMOAmount URLATotalREORentalIncomeGrossAmount { get; set; }
    
        [XmlIgnore]
        public bool URLATotalREORentalIncomeGrossAmountSpecified
        {
            get { return this.URLATotalREORentalIncomeGrossAmount != null; }
            set { }
        }
    
        [XmlElement("URLATotalREORentalIncomeNetAmount", Order = 22)]
        public MISMOAmount URLATotalREORentalIncomeNetAmount { get; set; }
    
        [XmlIgnore]
        public bool URLATotalREORentalIncomeNetAmountSpecified
        {
            get { return this.URLATotalREORentalIncomeNetAmount != null; }
            set { }
        }
    
        [XmlElement("URLATotalTransactionCostAmount", Order = 23)]
        public MISMOAmount URLATotalTransactionCostAmount { get; set; }
    
        [XmlIgnore]
        public bool URLATotalTransactionCostAmountSpecified
        {
            get { return this.URLATotalTransactionCostAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 24)]
        public URLA_TOTAL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
