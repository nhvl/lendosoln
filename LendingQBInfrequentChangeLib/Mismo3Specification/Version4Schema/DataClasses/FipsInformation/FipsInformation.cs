namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class FIPS_INFORMATION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.FIPSCountryCodeSpecified
                    || this.FIPSCountyCodeSpecified
                    || this.FIPSCountySubdivisionCodeSpecified
                    || this.FIPSCountySubdivisionNameSpecified
                    || this.FIPSCountySubdivisionTypeSpecified
                    || this.FIPSPlaceCodeSpecified
                    || this.FIPSPlaceNameSpecified
                    || this.FIPSStateAlphaCodeSpecified
                    || this.FIPSStateNumericCodeSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("FIPSCountryCode", Order = 0)]
        public MISMOCode FIPSCountryCode { get; set; }
    
        [XmlIgnore]
        public bool FIPSCountryCodeSpecified
        {
            get { return this.FIPSCountryCode != null; }
            set { }
        }
    
        [XmlElement("FIPSCountyCode", Order = 1)]
        public MISMOCode FIPSCountyCode { get; set; }
    
        [XmlIgnore]
        public bool FIPSCountyCodeSpecified
        {
            get { return this.FIPSCountyCode != null; }
            set { }
        }
    
        [XmlElement("FIPSCountySubdivisionCode", Order = 2)]
        public MISMOCode FIPSCountySubdivisionCode { get; set; }
    
        [XmlIgnore]
        public bool FIPSCountySubdivisionCodeSpecified
        {
            get { return this.FIPSCountySubdivisionCode != null; }
            set { }
        }
    
        [XmlElement("FIPSCountySubdivisionName", Order = 3)]
        public MISMOString FIPSCountySubdivisionName { get; set; }
    
        [XmlIgnore]
        public bool FIPSCountySubdivisionNameSpecified
        {
            get { return this.FIPSCountySubdivisionName != null; }
            set { }
        }
    
        [XmlElement("FIPSCountySubdivisionType", Order = 4)]
        public MISMOEnum<FIPSCountySubdivisionBase> FIPSCountySubdivisionType { get; set; }
    
        [XmlIgnore]
        public bool FIPSCountySubdivisionTypeSpecified
        {
            get { return this.FIPSCountySubdivisionType != null; }
            set { }
        }
    
        [XmlElement("FIPSPlaceCode", Order = 5)]
        public MISMOCode FIPSPlaceCode { get; set; }
    
        [XmlIgnore]
        public bool FIPSPlaceCodeSpecified
        {
            get { return this.FIPSPlaceCode != null; }
            set { }
        }
    
        [XmlElement("FIPSPlaceName", Order = 6)]
        public MISMOString FIPSPlaceName { get; set; }
    
        [XmlIgnore]
        public bool FIPSPlaceNameSpecified
        {
            get { return this.FIPSPlaceName != null; }
            set { }
        }
    
        [XmlElement("FIPSStateAlphaCode", Order = 7)]
        public MISMOCode FIPSStateAlphaCode { get; set; }
    
        [XmlIgnore]
        public bool FIPSStateAlphaCodeSpecified
        {
            get { return this.FIPSStateAlphaCode != null; }
            set { }
        }
    
        [XmlElement("FIPSStateNumericCode", Order = 8)]
        public MISMOCode FIPSStateNumericCode { get; set; }
    
        [XmlIgnore]
        public bool FIPSStateNumericCodeSpecified
        {
            get { return this.FIPSStateNumericCode != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 9)]
        public FIPS_INFORMATION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
