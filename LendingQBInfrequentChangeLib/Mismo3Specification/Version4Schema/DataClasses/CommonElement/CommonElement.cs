namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class COMMON_ELEMENT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ProjectCommonElementCompletedIndicatorSpecified
                    || this.ProjectCommonElementDescriptionSpecified
                    || this.ProjectCommonElementStatusDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ProjectCommonElementCompletedIndicator", Order = 0)]
        public MISMOIndicator ProjectCommonElementCompletedIndicator { get; set; }
    
        [XmlIgnore]
        public bool ProjectCommonElementCompletedIndicatorSpecified
        {
            get { return this.ProjectCommonElementCompletedIndicator != null; }
            set { }
        }
    
        [XmlElement("ProjectCommonElementDescription", Order = 1)]
        public MISMOString ProjectCommonElementDescription { get; set; }
    
        [XmlIgnore]
        public bool ProjectCommonElementDescriptionSpecified
        {
            get { return this.ProjectCommonElementDescription != null; }
            set { }
        }
    
        [XmlElement("ProjectCommonElementStatusDescription", Order = 2)]
        public MISMOString ProjectCommonElementStatusDescription { get; set; }
    
        [XmlIgnore]
        public bool ProjectCommonElementStatusDescriptionSpecified
        {
            get { return this.ProjectCommonElementStatusDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public COMMON_ELEMENT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
