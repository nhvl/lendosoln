namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class COMMON_ELEMENTS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CommonElementListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("COMMON_ELEMENT", Order = 0)]
        public List<COMMON_ELEMENT> CommonElementList { get; set; } = new List<COMMON_ELEMENT>();
    
        [XmlIgnore]
        public bool CommonElementListSpecified
        {
            get { return this.CommonElementList != null && this.CommonElementList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public COMMON_ELEMENTS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
