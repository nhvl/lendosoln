namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class SITE_INFLUENCES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.SiteInfluenceListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("SITE_INFLUENCE", Order = 0)]
        public List<SITE_INFLUENCE> SiteInfluenceList { get; set; } = new List<SITE_INFLUENCE>();
    
        [XmlIgnore]
        public bool SiteInfluenceListSpecified
        {
            get { return this.SiteInfluenceList != null && this.SiteInfluenceList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public SITE_INFLUENCES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
