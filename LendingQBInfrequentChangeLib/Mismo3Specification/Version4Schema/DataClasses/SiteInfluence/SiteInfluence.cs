namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class SITE_INFLUENCE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.InfluenceImpactTypeSpecified
                    || this.SiteInfluenceDescriptionSpecified
                    || this.SiteInfluenceTypeSpecified
                    || this.SiteInfluenceTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("InfluenceImpactType", Order = 0)]
        public MISMOEnum<InfluenceImpactBase> InfluenceImpactType { get; set; }
    
        [XmlIgnore]
        public bool InfluenceImpactTypeSpecified
        {
            get { return this.InfluenceImpactType != null; }
            set { }
        }
    
        [XmlElement("SiteInfluenceDescription", Order = 1)]
        public MISMOString SiteInfluenceDescription { get; set; }
    
        [XmlIgnore]
        public bool SiteInfluenceDescriptionSpecified
        {
            get { return this.SiteInfluenceDescription != null; }
            set { }
        }
    
        [XmlElement("SiteInfluenceType", Order = 2)]
        public MISMOEnum<SiteInfluenceBase> SiteInfluenceType { get; set; }
    
        [XmlIgnore]
        public bool SiteInfluenceTypeSpecified
        {
            get { return this.SiteInfluenceType != null; }
            set { }
        }
    
        [XmlElement("SiteInfluenceTypeOtherDescription", Order = 3)]
        public MISMOString SiteInfluenceTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool SiteInfluenceTypeOtherDescriptionSpecified
        {
            get { return this.SiteInfluenceTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public SITE_INFLUENCE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
