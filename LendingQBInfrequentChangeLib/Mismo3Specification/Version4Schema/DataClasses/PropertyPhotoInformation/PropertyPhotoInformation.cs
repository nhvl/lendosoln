namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PROPERTY_PHOTO_INFORMATION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.PropertyPhotoRequiredIndicatorSpecified
                    || this.PropertyPhotoTypeSpecified
                    || this.PropertyPhotoTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PropertyPhotoRequiredIndicator", Order = 0)]
        public MISMOIndicator PropertyPhotoRequiredIndicator { get; set; }
    
        [XmlIgnore]
        public bool PropertyPhotoRequiredIndicatorSpecified
        {
            get { return this.PropertyPhotoRequiredIndicator != null; }
            set { }
        }
    
        [XmlElement("PropertyPhotoType", Order = 1)]
        public MISMOEnum<PropertyPhotoBase> PropertyPhotoType { get; set; }
    
        [XmlIgnore]
        public bool PropertyPhotoTypeSpecified
        {
            get { return this.PropertyPhotoType != null; }
            set { }
        }
    
        [XmlElement("PropertyPhotoTypeOtherDescription", Order = 2)]
        public MISMOString PropertyPhotoTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool PropertyPhotoTypeOtherDescriptionSpecified
        {
            get { return this.PropertyPhotoTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public PROPERTY_PHOTO_INFORMATION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
