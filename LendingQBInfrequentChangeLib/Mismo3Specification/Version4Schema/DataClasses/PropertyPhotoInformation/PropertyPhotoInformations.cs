namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class PROPERTY_PHOTO_INFORMATIONS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.PropertyPhotoInformationListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PROPERTY_PHOTO_INFORMATION", Order = 0)]
        public List<PROPERTY_PHOTO_INFORMATION> PropertyPhotoInformationList { get; set; } = new List<PROPERTY_PHOTO_INFORMATION>();
    
        [XmlIgnore]
        public bool PropertyPhotoInformationListSpecified
        {
            get { return this.PropertyPhotoInformationList != null && this.PropertyPhotoInformationList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public PROPERTY_PHOTO_INFORMATIONS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
