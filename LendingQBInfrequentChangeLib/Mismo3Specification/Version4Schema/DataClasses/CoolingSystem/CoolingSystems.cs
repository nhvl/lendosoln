namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class COOLING_SYSTEMS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CoolingSystemListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("COOLING_SYSTEM", Order = 0)]
        public List<COOLING_SYSTEM> CoolingSystemList { get; set; } = new List<COOLING_SYSTEM>();
    
        [XmlIgnore]
        public bool CoolingSystemListSpecified
        {
            get { return this.CoolingSystemList != null && this.CoolingSystemList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public COOLING_SYSTEMS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
