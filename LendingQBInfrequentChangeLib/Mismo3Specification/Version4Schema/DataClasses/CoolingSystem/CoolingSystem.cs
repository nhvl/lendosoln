namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class COOLING_SYSTEM
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AdequateIndicatorSpecified
                    || this.ComponentAdjustmentAmountSpecified
                    || this.ComponentClassificationTypeSpecified
                    || this.ConditionRatingDescriptionSpecified
                    || this.ConditionRatingTypeSpecified
                    || this.CoolingSystemDescriptionSpecified
                    || this.CoolingSystemPrimaryIndicatorSpecified
                    || this.CoolingSystemTypeSpecified
                    || this.CoolingSystemTypeOtherDescriptionSpecified
                    || this.QualityRatingDescriptionSpecified
                    || this.QualityRatingTypeSpecified
                    || this.SystemPermanentIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AdequateIndicator", Order = 0)]
        public MISMOIndicator AdequateIndicator { get; set; }
    
        [XmlIgnore]
        public bool AdequateIndicatorSpecified
        {
            get { return this.AdequateIndicator != null; }
            set { }
        }
    
        [XmlElement("ComponentAdjustmentAmount", Order = 1)]
        public MISMOAmount ComponentAdjustmentAmount { get; set; }
    
        [XmlIgnore]
        public bool ComponentAdjustmentAmountSpecified
        {
            get { return this.ComponentAdjustmentAmount != null; }
            set { }
        }
    
        [XmlElement("ComponentClassificationType", Order = 2)]
        public MISMOEnum<ComponentClassificationBase> ComponentClassificationType { get; set; }
    
        [XmlIgnore]
        public bool ComponentClassificationTypeSpecified
        {
            get { return this.ComponentClassificationType != null; }
            set { }
        }
    
        [XmlElement("ConditionRatingDescription", Order = 3)]
        public MISMOString ConditionRatingDescription { get; set; }
    
        [XmlIgnore]
        public bool ConditionRatingDescriptionSpecified
        {
            get { return this.ConditionRatingDescription != null; }
            set { }
        }
    
        [XmlElement("ConditionRatingType", Order = 4)]
        public MISMOEnum<ConditionRatingBase> ConditionRatingType { get; set; }
    
        [XmlIgnore]
        public bool ConditionRatingTypeSpecified
        {
            get { return this.ConditionRatingType != null; }
            set { }
        }
    
        [XmlElement("CoolingSystemDescription", Order = 5)]
        public MISMOString CoolingSystemDescription { get; set; }
    
        [XmlIgnore]
        public bool CoolingSystemDescriptionSpecified
        {
            get { return this.CoolingSystemDescription != null; }
            set { }
        }
    
        [XmlElement("CoolingSystemPrimaryIndicator", Order = 6)]
        public MISMOIndicator CoolingSystemPrimaryIndicator { get; set; }
    
        [XmlIgnore]
        public bool CoolingSystemPrimaryIndicatorSpecified
        {
            get { return this.CoolingSystemPrimaryIndicator != null; }
            set { }
        }
    
        [XmlElement("CoolingSystemType", Order = 7)]
        public MISMOEnum<CoolingSystemBase> CoolingSystemType { get; set; }
    
        [XmlIgnore]
        public bool CoolingSystemTypeSpecified
        {
            get { return this.CoolingSystemType != null; }
            set { }
        }
    
        [XmlElement("CoolingSystemTypeOtherDescription", Order = 8)]
        public MISMOString CoolingSystemTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool CoolingSystemTypeOtherDescriptionSpecified
        {
            get { return this.CoolingSystemTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("QualityRatingDescription", Order = 9)]
        public MISMOString QualityRatingDescription { get; set; }
    
        [XmlIgnore]
        public bool QualityRatingDescriptionSpecified
        {
            get { return this.QualityRatingDescription != null; }
            set { }
        }
    
        [XmlElement("QualityRatingType", Order = 10)]
        public MISMOEnum<QualityRatingBase> QualityRatingType { get; set; }
    
        [XmlIgnore]
        public bool QualityRatingTypeSpecified
        {
            get { return this.QualityRatingType != null; }
            set { }
        }
    
        [XmlElement("SystemPermanentIndicator", Order = 11)]
        public MISMOIndicator SystemPermanentIndicator { get; set; }
    
        [XmlIgnore]
        public bool SystemPermanentIndicatorSpecified
        {
            get { return this.SystemPermanentIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 12)]
        public COOLING_SYSTEM_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
