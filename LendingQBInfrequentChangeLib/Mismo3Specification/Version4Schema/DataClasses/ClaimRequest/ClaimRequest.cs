namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CLAIM_REQUEST
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExpenseClaimsSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("EXPENSE_CLAIMS", Order = 0)]
        public EXPENSE_CLAIMS ExpenseClaims { get; set; }
    
        [XmlIgnore]
        public bool ExpenseClaimsSpecified
        {
            get { return this.ExpenseClaims != null && this.ExpenseClaims.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public CLAIM_REQUEST_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
