namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class INTERIOR_ROOM_SUMMARIES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.InteriorRoomSummaryListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("INTERIOR_ROOM_SUMMARY", Order = 0)]
        public List<INTERIOR_ROOM_SUMMARY> InteriorRoomSummaryList { get; set; } = new List<INTERIOR_ROOM_SUMMARY>();
    
        [XmlIgnore]
        public bool InteriorRoomSummaryListSpecified
        {
            get { return this.InteriorRoomSummaryList != null && this.InteriorRoomSummaryList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public INTERIOR_ROOM_SUMMARIES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
