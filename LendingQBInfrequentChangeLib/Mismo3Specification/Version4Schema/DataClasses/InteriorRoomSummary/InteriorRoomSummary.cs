namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class INTERIOR_ROOM_SUMMARY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AboveGradeIndicatorSpecified
                    || this.RoomTypeSpecified
                    || this.RoomTypeOtherDescriptionSpecified
                    || this.TotalNumberOfRoomsAboveGradeCountSpecified
                    || this.TotalNumberOfRoomsBelowGradeCountSpecified
                    || this.TotalRoomCountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AboveGradeIndicator", Order = 0)]
        public MISMOIndicator AboveGradeIndicator { get; set; }
    
        [XmlIgnore]
        public bool AboveGradeIndicatorSpecified
        {
            get { return this.AboveGradeIndicator != null; }
            set { }
        }
    
        [XmlElement("RoomType", Order = 1)]
        public MISMOEnum<RoomBase> RoomType { get; set; }
    
        [XmlIgnore]
        public bool RoomTypeSpecified
        {
            get { return this.RoomType != null; }
            set { }
        }
    
        [XmlElement("RoomTypeOtherDescription", Order = 2)]
        public MISMOString RoomTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool RoomTypeOtherDescriptionSpecified
        {
            get { return this.RoomTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("TotalNumberOfRoomsAboveGradeCount", Order = 3)]
        public MISMOCount TotalNumberOfRoomsAboveGradeCount { get; set; }
    
        [XmlIgnore]
        public bool TotalNumberOfRoomsAboveGradeCountSpecified
        {
            get { return this.TotalNumberOfRoomsAboveGradeCount != null; }
            set { }
        }
    
        [XmlElement("TotalNumberOfRoomsBelowGradeCount", Order = 4)]
        public MISMOCount TotalNumberOfRoomsBelowGradeCount { get; set; }
    
        [XmlIgnore]
        public bool TotalNumberOfRoomsBelowGradeCountSpecified
        {
            get { return this.TotalNumberOfRoomsBelowGradeCount != null; }
            set { }
        }
    
        [XmlElement("TotalRoomCount", Order = 5)]
        public MISMOCount TotalRoomCount { get; set; }
    
        [XmlIgnore]
        public bool TotalRoomCountSpecified
        {
            get { return this.TotalRoomCount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 6)]
        public INTERIOR_ROOM_SUMMARY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
