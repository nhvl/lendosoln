namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class BANKRUPTCY_RESULT_CLASS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BankruptcyResultClassificationCodeSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("BankruptcyResultClassificationCode", Order = 0)]
        public MISMOCode BankruptcyResultClassificationCode { get; set; }
    
        [XmlIgnore]
        public bool BankruptcyResultClassificationCodeSpecified
        {
            get { return this.BankruptcyResultClassificationCode != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public BANKRUPTCY_RESULT_CLASS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
