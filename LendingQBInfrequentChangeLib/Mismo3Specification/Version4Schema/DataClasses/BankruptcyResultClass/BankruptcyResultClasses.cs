namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class BANKRUPTCY_RESULT_CLASSES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BankruptcyResultClassListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("BANKRUPTCY_RESULT_CLASS", Order = 0)]
        public List<BANKRUPTCY_RESULT_CLASS> BankruptcyResultClassList { get; set; } = new List<BANKRUPTCY_RESULT_CLASS>();
    
        [XmlIgnore]
        public bool BankruptcyResultClassListSpecified
        {
            get { return this.BankruptcyResultClassList != null && this.BankruptcyResultClassList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public BANKRUPTCY_RESULT_CLASSES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
