namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class CREDIT_ENHANCEMENTS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditEnhancementListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CREDIT_ENHANCEMENT", Order = 0)]
        public List<CREDIT_ENHANCEMENT> CreditEnhancementList { get; set; } = new List<CREDIT_ENHANCEMENT>();
    
        [XmlIgnore]
        public bool CreditEnhancementListSpecified
        {
            get { return this.CreditEnhancementList != null && this.CreditEnhancementList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public CREDIT_ENHANCEMENTS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
