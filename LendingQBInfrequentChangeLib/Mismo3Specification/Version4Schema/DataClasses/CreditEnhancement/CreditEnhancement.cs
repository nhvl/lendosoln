namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CREDIT_ENHANCEMENT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditEnhancementEffectiveDateSpecified
                    || this.CreditEnhancementEffectivePeriodTypeSpecified
                    || this.CreditEnhancementExpirationDateSpecified
                    || this.CreditEnhancementPartyRoleTypeSpecified
                    || this.CreditEnhancementPartyRoleTypeOtherDescriptionSpecified
                    || this.CreditEnhancementPeriodBasedIndicatorSpecified
                    || this.CreditEnhancementTypeSpecified
                    || this.CreditEnhancementTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CreditEnhancementEffectiveDate", Order = 0)]
        public MISMODate CreditEnhancementEffectiveDate { get; set; }
    
        [XmlIgnore]
        public bool CreditEnhancementEffectiveDateSpecified
        {
            get { return this.CreditEnhancementEffectiveDate != null; }
            set { }
        }
    
        [XmlElement("CreditEnhancementEffectivePeriodType", Order = 1)]
        public MISMOEnum<CreditEnhancementEffectivePeriodBase> CreditEnhancementEffectivePeriodType { get; set; }
    
        [XmlIgnore]
        public bool CreditEnhancementEffectivePeriodTypeSpecified
        {
            get { return this.CreditEnhancementEffectivePeriodType != null; }
            set { }
        }
    
        [XmlElement("CreditEnhancementExpirationDate", Order = 2)]
        public MISMODate CreditEnhancementExpirationDate { get; set; }
    
        [XmlIgnore]
        public bool CreditEnhancementExpirationDateSpecified
        {
            get { return this.CreditEnhancementExpirationDate != null; }
            set { }
        }
    
        [XmlElement("CreditEnhancementPartyRoleType", Order = 3)]
        public MISMOEnum<CreditEnhancementPartyRoleBase> CreditEnhancementPartyRoleType { get; set; }
    
        [XmlIgnore]
        public bool CreditEnhancementPartyRoleTypeSpecified
        {
            get { return this.CreditEnhancementPartyRoleType != null; }
            set { }
        }
    
        [XmlElement("CreditEnhancementPartyRoleTypeOtherDescription", Order = 4)]
        public MISMOString CreditEnhancementPartyRoleTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool CreditEnhancementPartyRoleTypeOtherDescriptionSpecified
        {
            get { return this.CreditEnhancementPartyRoleTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("CreditEnhancementPeriodBasedIndicator", Order = 5)]
        public MISMOIndicator CreditEnhancementPeriodBasedIndicator { get; set; }
    
        [XmlIgnore]
        public bool CreditEnhancementPeriodBasedIndicatorSpecified
        {
            get { return this.CreditEnhancementPeriodBasedIndicator != null; }
            set { }
        }
    
        [XmlElement("CreditEnhancementType", Order = 6)]
        public MISMOEnum<CreditEnhancementBase> CreditEnhancementType { get; set; }
    
        [XmlIgnore]
        public bool CreditEnhancementTypeSpecified
        {
            get { return this.CreditEnhancementType != null; }
            set { }
        }
    
        [XmlElement("CreditEnhancementTypeOtherDescription", Order = 7)]
        public MISMOString CreditEnhancementTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool CreditEnhancementTypeOtherDescriptionSpecified
        {
            get { return this.CreditEnhancementTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 8)]
        public CREDIT_ENHANCEMENT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
