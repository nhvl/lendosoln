namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class LOAN_LEVEL_CREDIT_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditReferenceTypeSpecified
                    || this.CreditReferenceTypeOtherDescriptionSpecified
                    || this.CreditScoreCategoryTypeSpecified
                    || this.CreditScoreCategoryTypeOtherDescriptionSpecified
                    || this.CreditScoreCategoryVersionTypeSpecified
                    || this.CreditScoreCategoryVersionTypeOtherDescriptionSpecified
                    || this.CreditScoreImpairmentTypeSpecified
                    || this.CreditScoreImpairmentTypeOtherDescriptionSpecified
                    || this.CreditScoreModelNameTypeSpecified
                    || this.CreditScoreModelNameTypeOtherDescriptionSpecified
                    || this.LoanCreditHistoryAgeTypeSpecified
                    || this.LoanCreditHistoryAgeTypeOtherDescriptionSpecified
                    || this.LoanLevelCreditScoreSelectionMethodTypeSpecified
                    || this.LoanLevelCreditScoreSelectionMethodTypeOtherDescriptionSpecified
                    || this.LoanLevelCreditScoreValueSpecified
                    || this.RiskUpgradeIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CreditReferenceType", Order = 0)]
        public MISMOEnum<CreditReferenceBase> CreditReferenceType { get; set; }
    
        [XmlIgnore]
        public bool CreditReferenceTypeSpecified
        {
            get { return this.CreditReferenceType != null; }
            set { }
        }
    
        [XmlElement("CreditReferenceTypeOtherDescription", Order = 1)]
        public MISMOString CreditReferenceTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool CreditReferenceTypeOtherDescriptionSpecified
        {
            get { return this.CreditReferenceTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("CreditScoreCategoryType", Order = 2)]
        public MISMOEnum<CreditScoreCategoryBase> CreditScoreCategoryType { get; set; }
    
        [XmlIgnore]
        public bool CreditScoreCategoryTypeSpecified
        {
            get { return this.CreditScoreCategoryType != null; }
            set { }
        }
    
        [XmlElement("CreditScoreCategoryTypeOtherDescription", Order = 3)]
        public MISMOString CreditScoreCategoryTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool CreditScoreCategoryTypeOtherDescriptionSpecified
        {
            get { return this.CreditScoreCategoryTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("CreditScoreCategoryVersionType", Order = 4)]
        public MISMOEnum<CreditScoreCategoryVersionBase> CreditScoreCategoryVersionType { get; set; }
    
        [XmlIgnore]
        public bool CreditScoreCategoryVersionTypeSpecified
        {
            get { return this.CreditScoreCategoryVersionType != null; }
            set { }
        }
    
        [XmlElement("CreditScoreCategoryVersionTypeOtherDescription", Order = 5)]
        public MISMOString CreditScoreCategoryVersionTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool CreditScoreCategoryVersionTypeOtherDescriptionSpecified
        {
            get { return this.CreditScoreCategoryVersionTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("CreditScoreImpairmentType", Order = 6)]
        public MISMOEnum<CreditScoreImpairmentBase> CreditScoreImpairmentType { get; set; }
    
        [XmlIgnore]
        public bool CreditScoreImpairmentTypeSpecified
        {
            get { return this.CreditScoreImpairmentType != null; }
            set { }
        }
    
        [XmlElement("CreditScoreImpairmentTypeOtherDescription", Order = 7)]
        public MISMOString CreditScoreImpairmentTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool CreditScoreImpairmentTypeOtherDescriptionSpecified
        {
            get { return this.CreditScoreImpairmentTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("CreditScoreModelNameType", Order = 8)]
        public MISMOEnum<CreditScoreModelNameBase> CreditScoreModelNameType { get; set; }
    
        [XmlIgnore]
        public bool CreditScoreModelNameTypeSpecified
        {
            get { return this.CreditScoreModelNameType != null; }
            set { }
        }
    
        [XmlElement("CreditScoreModelNameTypeOtherDescription", Order = 9)]
        public MISMOString CreditScoreModelNameTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool CreditScoreModelNameTypeOtherDescriptionSpecified
        {
            get { return this.CreditScoreModelNameTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("LoanCreditHistoryAgeType", Order = 10)]
        public MISMOEnum<LoanCreditHistoryAgeBase> LoanCreditHistoryAgeType { get; set; }
    
        [XmlIgnore]
        public bool LoanCreditHistoryAgeTypeSpecified
        {
            get { return this.LoanCreditHistoryAgeType != null; }
            set { }
        }
    
        [XmlElement("LoanCreditHistoryAgeTypeOtherDescription", Order = 11)]
        public MISMOString LoanCreditHistoryAgeTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool LoanCreditHistoryAgeTypeOtherDescriptionSpecified
        {
            get { return this.LoanCreditHistoryAgeTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("LoanLevelCreditScoreSelectionMethodType", Order = 12)]
        public MISMOEnum<LoanLevelCreditScoreSelectionMethodBase> LoanLevelCreditScoreSelectionMethodType { get; set; }
    
        [XmlIgnore]
        public bool LoanLevelCreditScoreSelectionMethodTypeSpecified
        {
            get { return this.LoanLevelCreditScoreSelectionMethodType != null; }
            set { }
        }
    
        [XmlElement("LoanLevelCreditScoreSelectionMethodTypeOtherDescription", Order = 13)]
        public MISMOString LoanLevelCreditScoreSelectionMethodTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool LoanLevelCreditScoreSelectionMethodTypeOtherDescriptionSpecified
        {
            get { return this.LoanLevelCreditScoreSelectionMethodTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("LoanLevelCreditScoreValue", Order = 14)]
        public MISMOValue LoanLevelCreditScoreValue { get; set; }
    
        [XmlIgnore]
        public bool LoanLevelCreditScoreValueSpecified
        {
            get { return this.LoanLevelCreditScoreValue != null; }
            set { }
        }
    
        [XmlElement("RiskUpgradeIndicator", Order = 15)]
        public MISMOIndicator RiskUpgradeIndicator { get; set; }
    
        [XmlIgnore]
        public bool RiskUpgradeIndicatorSpecified
        {
            get { return this.RiskUpgradeIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 16)]
        public LOAN_LEVEL_CREDIT_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
