namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class LOAN_LEVEL_CREDIT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditScoreProviderSpecified
                    || this.LoanLevelCreditDetailSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CREDIT_SCORE_PROVIDER", Order = 0)]
        public CREDIT_SCORE_PROVIDER CreditScoreProvider { get; set; }
    
        [XmlIgnore]
        public bool CreditScoreProviderSpecified
        {
            get { return this.CreditScoreProvider != null && this.CreditScoreProvider.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("LOAN_LEVEL_CREDIT_DETAIL", Order = 1)]
        public LOAN_LEVEL_CREDIT_DETAIL LoanLevelCreditDetail { get; set; }
    
        [XmlIgnore]
        public bool LoanLevelCreditDetailSpecified
        {
            get { return this.LoanLevelCreditDetail != null && this.LoanLevelCreditDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public LOAN_LEVEL_CREDIT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
