namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PREPAYMENT_PENALTY_PER_CHANGE_RULE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.PrepaymentPenaltyCalculationValueTypeSpecified
                    || this.PrepaymentPenaltyCalculationValueTypeOtherDescriptionSpecified
                    || this.PrepaymentPenaltyCurtailmentAmountSpecified
                    || this.PrepaymentPenaltyCurtailmentPercentSpecified
                    || this.PrepaymentPenaltyEffectiveDateSpecified
                    || this.PrepaymentPenaltyFixedAmountSpecified
                    || this.PrepaymentPenaltyMaximumAmountSpecified
                    || this.PrepaymentPenaltyOptionTypeSpecified
                    || this.PrepaymentPenaltyPercentSpecified
                    || this.PrepaymentPenaltyPeriodCountSpecified
                    || this.PrepaymentPenaltyPeriodicDaysCountSpecified
                    || this.PrepaymentPenaltyPeriodTypeSpecified
                    || this.PrepaymentPenaltyPrincipalBalanceTypeSpecified
                    || this.PrepaymentPenaltyPrincipalBalanceTypeOtherDescriptionSpecified
                    || this.PrepaymentPenaltyTypeSpecified
                    || this.PrepaymentPenaltyTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PrepaymentPenaltyCalculationValueType", Order = 0)]
        public MISMOEnum<PrepaymentPenaltyCalculationValueBase> PrepaymentPenaltyCalculationValueType { get; set; }
    
        [XmlIgnore]
        public bool PrepaymentPenaltyCalculationValueTypeSpecified
        {
            get { return this.PrepaymentPenaltyCalculationValueType != null; }
            set { }
        }
    
        [XmlElement("PrepaymentPenaltyCalculationValueTypeOtherDescription", Order = 1)]
        public MISMOString PrepaymentPenaltyCalculationValueTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool PrepaymentPenaltyCalculationValueTypeOtherDescriptionSpecified
        {
            get { return this.PrepaymentPenaltyCalculationValueTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("PrepaymentPenaltyCurtailmentAmount", Order = 2)]
        public MISMOAmount PrepaymentPenaltyCurtailmentAmount { get; set; }
    
        [XmlIgnore]
        public bool PrepaymentPenaltyCurtailmentAmountSpecified
        {
            get { return this.PrepaymentPenaltyCurtailmentAmount != null; }
            set { }
        }
    
        [XmlElement("PrepaymentPenaltyCurtailmentPercent", Order = 3)]
        public MISMOPercent PrepaymentPenaltyCurtailmentPercent { get; set; }
    
        [XmlIgnore]
        public bool PrepaymentPenaltyCurtailmentPercentSpecified
        {
            get { return this.PrepaymentPenaltyCurtailmentPercent != null; }
            set { }
        }
    
        [XmlElement("PrepaymentPenaltyEffectiveDate", Order = 4)]
        public MISMODate PrepaymentPenaltyEffectiveDate { get; set; }
    
        [XmlIgnore]
        public bool PrepaymentPenaltyEffectiveDateSpecified
        {
            get { return this.PrepaymentPenaltyEffectiveDate != null; }
            set { }
        }
    
        [XmlElement("PrepaymentPenaltyFixedAmount", Order = 5)]
        public MISMOAmount PrepaymentPenaltyFixedAmount { get; set; }
    
        [XmlIgnore]
        public bool PrepaymentPenaltyFixedAmountSpecified
        {
            get { return this.PrepaymentPenaltyFixedAmount != null; }
            set { }
        }
    
        [XmlElement("PrepaymentPenaltyMaximumAmount", Order = 6)]
        public MISMOAmount PrepaymentPenaltyMaximumAmount { get; set; }
    
        [XmlIgnore]
        public bool PrepaymentPenaltyMaximumAmountSpecified
        {
            get { return this.PrepaymentPenaltyMaximumAmount != null; }
            set { }
        }
    
        [XmlElement("PrepaymentPenaltyOptionType", Order = 7)]
        public MISMOEnum<PrepaymentPenaltyOptionBase> PrepaymentPenaltyOptionType { get; set; }
    
        [XmlIgnore]
        public bool PrepaymentPenaltyOptionTypeSpecified
        {
            get { return this.PrepaymentPenaltyOptionType != null; }
            set { }
        }
    
        [XmlElement("PrepaymentPenaltyPercent", Order = 8)]
        public MISMOPercent PrepaymentPenaltyPercent { get; set; }
    
        [XmlIgnore]
        public bool PrepaymentPenaltyPercentSpecified
        {
            get { return this.PrepaymentPenaltyPercent != null; }
            set { }
        }
    
        [XmlElement("PrepaymentPenaltyPeriodCount", Order = 9)]
        public MISMOCount PrepaymentPenaltyPeriodCount { get; set; }
    
        [XmlIgnore]
        public bool PrepaymentPenaltyPeriodCountSpecified
        {
            get { return this.PrepaymentPenaltyPeriodCount != null; }
            set { }
        }
    
        [XmlElement("PrepaymentPenaltyPeriodicDaysCount", Order = 10)]
        public MISMOCount PrepaymentPenaltyPeriodicDaysCount { get; set; }
    
        [XmlIgnore]
        public bool PrepaymentPenaltyPeriodicDaysCountSpecified
        {
            get { return this.PrepaymentPenaltyPeriodicDaysCount != null; }
            set { }
        }
    
        [XmlElement("PrepaymentPenaltyPeriodType", Order = 11)]
        public MISMOEnum<PrepaymentPenaltyPeriodBase> PrepaymentPenaltyPeriodType { get; set; }
    
        [XmlIgnore]
        public bool PrepaymentPenaltyPeriodTypeSpecified
        {
            get { return this.PrepaymentPenaltyPeriodType != null; }
            set { }
        }
    
        [XmlElement("PrepaymentPenaltyPrincipalBalanceType", Order = 12)]
        public MISMOEnum<PrepaymentPenaltyPrincipalBalanceBase> PrepaymentPenaltyPrincipalBalanceType { get; set; }
    
        [XmlIgnore]
        public bool PrepaymentPenaltyPrincipalBalanceTypeSpecified
        {
            get { return this.PrepaymentPenaltyPrincipalBalanceType != null; }
            set { }
        }
    
        [XmlElement("PrepaymentPenaltyPrincipalBalanceTypeOtherDescription", Order = 13)]
        public MISMOString PrepaymentPenaltyPrincipalBalanceTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool PrepaymentPenaltyPrincipalBalanceTypeOtherDescriptionSpecified
        {
            get { return this.PrepaymentPenaltyPrincipalBalanceTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("PrepaymentPenaltyType", Order = 14)]
        public MISMOEnum<PrepaymentPenaltyBase> PrepaymentPenaltyType { get; set; }
    
        [XmlIgnore]
        public bool PrepaymentPenaltyTypeSpecified
        {
            get { return this.PrepaymentPenaltyType != null; }
            set { }
        }
    
        [XmlElement("PrepaymentPenaltyTypeOtherDescription", Order = 15)]
        public MISMOString PrepaymentPenaltyTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool PrepaymentPenaltyTypeOtherDescriptionSpecified
        {
            get { return this.PrepaymentPenaltyTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 16)]
        public PREPAYMENT_PENALTY_PER_CHANGE_RULE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
