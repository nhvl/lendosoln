namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class ENERGY_EFFICIENCY_FINANCIAL_INCENTIVE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.EnergyEfficiencyAuthorityIncentiveAmountSpecified
                    || this.EnergyEfficiencyAuthorityLevelTypeSpecified
                    || this.EnergyEfficiencyIncentiveCommentDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("EnergyEfficiencyAuthorityIncentiveAmount", Order = 0)]
        public MISMOAmount EnergyEfficiencyAuthorityIncentiveAmount { get; set; }
    
        [XmlIgnore]
        public bool EnergyEfficiencyAuthorityIncentiveAmountSpecified
        {
            get { return this.EnergyEfficiencyAuthorityIncentiveAmount != null; }
            set { }
        }
    
        [XmlElement("EnergyEfficiencyAuthorityLevelType", Order = 1)]
        public MISMOEnum<EnergyEfficiencyAuthorityLevelBase> EnergyEfficiencyAuthorityLevelType { get; set; }
    
        [XmlIgnore]
        public bool EnergyEfficiencyAuthorityLevelTypeSpecified
        {
            get { return this.EnergyEfficiencyAuthorityLevelType != null; }
            set { }
        }
    
        [XmlElement("EnergyEfficiencyIncentiveCommentDescription", Order = 2)]
        public MISMOString EnergyEfficiencyIncentiveCommentDescription { get; set; }
    
        [XmlIgnore]
        public bool EnergyEfficiencyIncentiveCommentDescriptionSpecified
        {
            get { return this.EnergyEfficiencyIncentiveCommentDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public ENERGY_EFFICIENCY_FINANCIAL_INCENTIVE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
