namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class ENERGY_EFFICIENCY_FINANCIAL_INCENTIVES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.EnergyEfficiencyFinancialIncentiveListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ENERGY_EFFICIENCY_FINANCIAL_INCENTIVE", Order = 0)]
        public List<ENERGY_EFFICIENCY_FINANCIAL_INCENTIVE> EnergyEfficiencyFinancialIncentiveList { get; set; } = new List<ENERGY_EFFICIENCY_FINANCIAL_INCENTIVE>();
    
        [XmlIgnore]
        public bool EnergyEfficiencyFinancialIncentiveListSpecified
        {
            get { return this.EnergyEfficiencyFinancialIncentiveList != null && this.EnergyEfficiencyFinancialIncentiveList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public ENERGY_EFFICIENCY_FINANCIAL_INCENTIVES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
