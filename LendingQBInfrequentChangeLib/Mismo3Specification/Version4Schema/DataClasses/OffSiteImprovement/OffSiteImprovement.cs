namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class OFF_SITE_IMPROVEMENT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ConditionRatingTypeSpecified
                    || this.OffSiteImprovementDescriptionSpecified
                    || this.OffSiteImprovementOwnershipTypeSpecified
                    || this.OffSiteImprovementsUtilityNotTypicalDescriptionSpecified
                    || this.OffSiteImprovementsUtilityTypicalIndicatorSpecified
                    || this.OffSiteImprovementTypeSpecified
                    || this.OffSiteImprovementTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ConditionRatingType", Order = 0)]
        public MISMOEnum<ConditionRatingBase> ConditionRatingType { get; set; }
    
        [XmlIgnore]
        public bool ConditionRatingTypeSpecified
        {
            get { return this.ConditionRatingType != null; }
            set { }
        }
    
        [XmlElement("OffSiteImprovementDescription", Order = 1)]
        public MISMOString OffSiteImprovementDescription { get; set; }
    
        [XmlIgnore]
        public bool OffSiteImprovementDescriptionSpecified
        {
            get { return this.OffSiteImprovementDescription != null; }
            set { }
        }
    
        [XmlElement("OffSiteImprovementOwnershipType", Order = 2)]
        public MISMOEnum<OffSiteImprovementOwnershipBase> OffSiteImprovementOwnershipType { get; set; }
    
        [XmlIgnore]
        public bool OffSiteImprovementOwnershipTypeSpecified
        {
            get { return this.OffSiteImprovementOwnershipType != null; }
            set { }
        }
    
        [XmlElement("OffSiteImprovementsUtilityNotTypicalDescription", Order = 3)]
        public MISMOString OffSiteImprovementsUtilityNotTypicalDescription { get; set; }
    
        [XmlIgnore]
        public bool OffSiteImprovementsUtilityNotTypicalDescriptionSpecified
        {
            get { return this.OffSiteImprovementsUtilityNotTypicalDescription != null; }
            set { }
        }
    
        [XmlElement("OffSiteImprovementsUtilityTypicalIndicator", Order = 4)]
        public MISMOIndicator OffSiteImprovementsUtilityTypicalIndicator { get; set; }
    
        [XmlIgnore]
        public bool OffSiteImprovementsUtilityTypicalIndicatorSpecified
        {
            get { return this.OffSiteImprovementsUtilityTypicalIndicator != null; }
            set { }
        }
    
        [XmlElement("OffSiteImprovementType", Order = 5)]
        public MISMOEnum<OffSiteImprovementBase> OffSiteImprovementType { get; set; }
    
        [XmlIgnore]
        public bool OffSiteImprovementTypeSpecified
        {
            get { return this.OffSiteImprovementType != null; }
            set { }
        }
    
        [XmlElement("OffSiteImprovementTypeOtherDescription", Order = 6)]
        public MISMOString OffSiteImprovementTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool OffSiteImprovementTypeOtherDescriptionSpecified
        {
            get { return this.OffSiteImprovementTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 7)]
        public OFF_SITE_IMPROVEMENT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
