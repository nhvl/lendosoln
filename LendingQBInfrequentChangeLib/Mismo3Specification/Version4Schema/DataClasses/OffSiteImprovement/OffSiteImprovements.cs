namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class OFF_SITE_IMPROVEMENTS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.OffSiteImprovementListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("OFF_SITE_IMPROVEMENT", Order = 0)]
        public List<OFF_SITE_IMPROVEMENT> OffSiteImprovementList { get; set; } = new List<OFF_SITE_IMPROVEMENT>();
    
        [XmlIgnore]
        public bool OffSiteImprovementListSpecified
        {
            get { return this.OffSiteImprovementList != null && this.OffSiteImprovementList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public OFF_SITE_IMPROVEMENTS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
