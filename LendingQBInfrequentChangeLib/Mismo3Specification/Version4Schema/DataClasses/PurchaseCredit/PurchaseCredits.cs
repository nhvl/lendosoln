namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class PURCHASE_CREDITS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.PurchaseCreditListSpecified
                    || this.PurchaseCreditSummarySpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PURCHASE_CREDIT", Order = 0)]
        public List<PURCHASE_CREDIT> PurchaseCreditList { get; set; } = new List<PURCHASE_CREDIT>();
    
        [XmlIgnore]
        public bool PurchaseCreditListSpecified
        {
            get { return this.PurchaseCreditList != null && this.PurchaseCreditList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("PURCHASE_CREDIT_SUMMARY", Order = 1)]
        public PURCHASE_CREDIT_SUMMARY PurchaseCreditSummary { get; set; }
    
        [XmlIgnore]
        public bool PurchaseCreditSummarySpecified
        {
            get { return this.PurchaseCreditSummary != null && this.PurchaseCreditSummary.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public PURCHASE_CREDITS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
