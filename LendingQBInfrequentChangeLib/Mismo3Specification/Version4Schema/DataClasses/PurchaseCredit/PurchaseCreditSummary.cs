namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PURCHASE_CREDIT_SUMMARY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.TotalPurchaseCreditAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("TotalPurchaseCreditAmount", Order = 0)]
        public MISMOAmount TotalPurchaseCreditAmount { get; set; }
    
        [XmlIgnore]
        public bool TotalPurchaseCreditAmountSpecified
        {
            get { return this.TotalPurchaseCreditAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public PURCHASE_CREDIT_SUMMARY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
