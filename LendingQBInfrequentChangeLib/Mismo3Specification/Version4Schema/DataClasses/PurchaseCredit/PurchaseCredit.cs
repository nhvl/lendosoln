namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PURCHASE_CREDIT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.PurchaseCreditAmountSpecified
                    || this.PurchaseCreditSourceTypeSpecified
                    || this.PurchaseCreditSourceTypeOtherDescriptionSpecified
                    || this.PurchaseCreditTypeSpecified
                    || this.PurchaseCreditTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PurchaseCreditAmount", Order = 0)]
        public MISMOAmount PurchaseCreditAmount { get; set; }
    
        [XmlIgnore]
        public bool PurchaseCreditAmountSpecified
        {
            get { return this.PurchaseCreditAmount != null; }
            set { }
        }
    
        [XmlElement("PurchaseCreditSourceType", Order = 1)]
        public MISMOEnum<PurchaseCreditSourceBase> PurchaseCreditSourceType { get; set; }
    
        [XmlIgnore]
        public bool PurchaseCreditSourceTypeSpecified
        {
            get { return this.PurchaseCreditSourceType != null; }
            set { }
        }
    
        [XmlElement("PurchaseCreditSourceTypeOtherDescription", Order = 2)]
        public MISMOString PurchaseCreditSourceTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool PurchaseCreditSourceTypeOtherDescriptionSpecified
        {
            get { return this.PurchaseCreditSourceTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("PurchaseCreditType", Order = 3)]
        public MISMOEnum<PurchaseCreditBase> PurchaseCreditType { get; set; }
    
        [XmlIgnore]
        public bool PurchaseCreditTypeSpecified
        {
            get { return this.PurchaseCreditType != null; }
            set { }
        }
    
        [XmlElement("PurchaseCreditTypeOtherDescription", Order = 4)]
        public MISMOString PurchaseCreditTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool PurchaseCreditTypeOtherDescriptionSpecified
        {
            get { return this.PurchaseCreditTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 5)]
        public PURCHASE_CREDIT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
