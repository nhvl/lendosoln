namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PROPERTY_LICENSE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.PropertyLicenseTypeSpecified
                    || this.PropertyLicenseTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PropertyLicenseType", Order = 0)]
        public MISMOEnum<PropertyLicenseBase> PropertyLicenseType { get; set; }
    
        [XmlIgnore]
        public bool PropertyLicenseTypeSpecified
        {
            get { return this.PropertyLicenseType != null; }
            set { }
        }
    
        [XmlElement("PropertyLicenseTypeOtherDescription", Order = 1)]
        public MISMOString PropertyLicenseTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool PropertyLicenseTypeOtherDescriptionSpecified
        {
            get { return this.PropertyLicenseTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public PROPERTY_LICENSE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
