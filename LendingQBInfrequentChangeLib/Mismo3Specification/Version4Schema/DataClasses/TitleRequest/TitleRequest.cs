namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class TITLE_REQUEST
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExecutionsSpecified
                    || this.TitleRequestDetailSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("EXECUTIONS", Order = 0)]
        public EXECUTIONS Executions { get; set; }
    
        [XmlIgnore]
        public bool ExecutionsSpecified
        {
            get { return this.Executions != null && this.Executions.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("TITLE_REQUEST_DETAIL", Order = 1)]
        public TITLE_REQUEST_DETAIL TitleRequestDetail { get; set; }
    
        [XmlIgnore]
        public bool TitleRequestDetailSpecified
        {
            get { return this.TitleRequestDetail != null && this.TitleRequestDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public TITLE_REQUEST_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
