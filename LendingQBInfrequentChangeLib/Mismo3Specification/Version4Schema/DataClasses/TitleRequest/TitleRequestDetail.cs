namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class TITLE_REQUEST_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.InsuredNameSpecified
                    || this.NAICTitlePolicyClassificationTypeSpecified
                    || this.NamedInsuredTypeSpecified
                    || this.ProcessorIdentifierSpecified
                    || this.RequestedClosingDateSpecified
                    || this.RequestedClosingTimeSpecified
                    || this.TitleAgentValidationReasonTypeSpecified
                    || this.TitleAgentValidationReasonTypeOtherDescriptionSpecified
                    || this.TitleAssociationTypeSpecified
                    || this.TitleAssociationTypeOtherDescriptionSpecified
                    || this.TitleOfficeIdentifierSpecified
                    || this.TitleOwnershipTypeSpecified
                    || this.TitleOwnershipTypeOtherDescriptionSpecified
                    || this.TitleRequestActionTypeSpecified
                    || this.TitleRequestCommentDescriptionSpecified
                    || this.TitleRequestProposedTitleInsuranceCoverageAmountSpecified
                    || this.VendorOrderIdentifierSpecified
                    || this.VendorTransactionIdentifierSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("InsuredName", Order = 0)]
        public MISMOString InsuredName { get; set; }
    
        [XmlIgnore]
        public bool InsuredNameSpecified
        {
            get { return this.InsuredName != null; }
            set { }
        }
    
        [XmlElement("NAICTitlePolicyClassificationType", Order = 1)]
        public MISMOEnum<NAICTitlePolicyClassificationBase> NAICTitlePolicyClassificationType { get; set; }
    
        [XmlIgnore]
        public bool NAICTitlePolicyClassificationTypeSpecified
        {
            get { return this.NAICTitlePolicyClassificationType != null; }
            set { }
        }
    
        [XmlElement("NamedInsuredType", Order = 2)]
        public MISMOEnum<NamedInsuredBase> NamedInsuredType { get; set; }
    
        [XmlIgnore]
        public bool NamedInsuredTypeSpecified
        {
            get { return this.NamedInsuredType != null; }
            set { }
        }
    
        [XmlElement("ProcessorIdentifier", Order = 3)]
        public MISMOIdentifier ProcessorIdentifier { get; set; }
    
        [XmlIgnore]
        public bool ProcessorIdentifierSpecified
        {
            get { return this.ProcessorIdentifier != null; }
            set { }
        }
    
        [XmlElement("RequestedClosingDate", Order = 4)]
        public MISMODate RequestedClosingDate { get; set; }
    
        [XmlIgnore]
        public bool RequestedClosingDateSpecified
        {
            get { return this.RequestedClosingDate != null; }
            set { }
        }
    
        [XmlElement("RequestedClosingTime", Order = 5)]
        public MISMOTime RequestedClosingTime { get; set; }
    
        [XmlIgnore]
        public bool RequestedClosingTimeSpecified
        {
            get { return this.RequestedClosingTime != null; }
            set { }
        }
    
        [XmlElement("TitleAgentValidationReasonType", Order = 6)]
        public MISMOEnum<TitleAgentValidationReasonBase> TitleAgentValidationReasonType { get; set; }
    
        [XmlIgnore]
        public bool TitleAgentValidationReasonTypeSpecified
        {
            get { return this.TitleAgentValidationReasonType != null; }
            set { }
        }
    
        [XmlElement("TitleAgentValidationReasonTypeOtherDescription", Order = 7)]
        public MISMOString TitleAgentValidationReasonTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool TitleAgentValidationReasonTypeOtherDescriptionSpecified
        {
            get { return this.TitleAgentValidationReasonTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("TitleAssociationType", Order = 8)]
        public MISMOEnum<TitleAssociationBase> TitleAssociationType { get; set; }
    
        [XmlIgnore]
        public bool TitleAssociationTypeSpecified
        {
            get { return this.TitleAssociationType != null; }
            set { }
        }
    
        [XmlElement("TitleAssociationTypeOtherDescription", Order = 9)]
        public MISMOString TitleAssociationTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool TitleAssociationTypeOtherDescriptionSpecified
        {
            get { return this.TitleAssociationTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("TitleOfficeIdentifier", Order = 10)]
        public MISMOIdentifier TitleOfficeIdentifier { get; set; }
    
        [XmlIgnore]
        public bool TitleOfficeIdentifierSpecified
        {
            get { return this.TitleOfficeIdentifier != null; }
            set { }
        }
    
        [XmlElement("TitleOwnershipType", Order = 11)]
        public MISMOEnum<TitleOwnershipBase> TitleOwnershipType { get; set; }
    
        [XmlIgnore]
        public bool TitleOwnershipTypeSpecified
        {
            get { return this.TitleOwnershipType != null; }
            set { }
        }
    
        [XmlElement("TitleOwnershipTypeOtherDescription", Order = 12)]
        public MISMOString TitleOwnershipTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool TitleOwnershipTypeOtherDescriptionSpecified
        {
            get { return this.TitleOwnershipTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("TitleRequestActionType", Order = 13)]
        public MISMOEnum<TitleRequestActionBase> TitleRequestActionType { get; set; }
    
        [XmlIgnore]
        public bool TitleRequestActionTypeSpecified
        {
            get { return this.TitleRequestActionType != null; }
            set { }
        }
    
        [XmlElement("TitleRequestCommentDescription", Order = 14)]
        public MISMOString TitleRequestCommentDescription { get; set; }
    
        [XmlIgnore]
        public bool TitleRequestCommentDescriptionSpecified
        {
            get { return this.TitleRequestCommentDescription != null; }
            set { }
        }
    
        [XmlElement("TitleRequestProposedTitleInsuranceCoverageAmount", Order = 15)]
        public MISMOAmount TitleRequestProposedTitleInsuranceCoverageAmount { get; set; }
    
        [XmlIgnore]
        public bool TitleRequestProposedTitleInsuranceCoverageAmountSpecified
        {
            get { return this.TitleRequestProposedTitleInsuranceCoverageAmount != null; }
            set { }
        }
    
        [XmlElement("VendorOrderIdentifier", Order = 16)]
        public MISMOIdentifier VendorOrderIdentifier { get; set; }
    
        [XmlIgnore]
        public bool VendorOrderIdentifierSpecified
        {
            get { return this.VendorOrderIdentifier != null; }
            set { }
        }
    
        [XmlElement("VendorTransactionIdentifier", Order = 17)]
        public MISMOIdentifier VendorTransactionIdentifier { get; set; }
    
        [XmlIgnore]
        public bool VendorTransactionIdentifierSpecified
        {
            get { return this.VendorTransactionIdentifier != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 18)]
        public TITLE_REQUEST_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
