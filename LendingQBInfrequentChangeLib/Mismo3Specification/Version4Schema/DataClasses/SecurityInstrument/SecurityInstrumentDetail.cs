namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class SECURITY_INSTRUMENT_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DefaultAcknowledgmentOfCashAdvanceAgainstNonHomesteadPropertyIndicatorSpecified
                    || this.DefaultApplicationFeesAmountSpecified
                    || this.DefaultClosingPreparationFeesAmountSpecified
                    || this.DefaultLendingInstitutionPostOfficeBoxIdentifierSpecified
                    || this.DocumentDrawnInNameSpecified
                    || this.NameDocumentsDrawnInTypeSpecified
                    || this.NameDocumentsDrawnInTypeOtherDescriptionSpecified
                    || this.SecurityInstrumentAssumptionFeeAmountSpecified
                    || this.SecurityInstrumentAttorneyFeeMinimumAmountSpecified
                    || this.SecurityInstrumentAttorneyFeePercentSpecified
                    || this.SecurityInstrumentCertifyingAttorneyNameSpecified
                    || this.SecurityInstrumentDateSpecified
                    || this.SecurityInstrumentMaximumPrincipalIndebtednessAmountSpecified
                    || this.SecurityInstrumentMultipleUnitsRealPropertyIsImprovedOrToBeImprovedIndicatorSpecified
                    || this.SecurityInstrumentNoteHolderNameSpecified
                    || this.SecurityInstrumentNoticeOfConfidentialityRightsDescriptionSpecified
                    || this.SecurityInstrumentOtherFeesAmountSpecified
                    || this.SecurityInstrumentOtherFeesDescriptionSpecified
                    || this.SecurityInstrumentOweltyOfPartitionIndicatorSpecified
                    || this.SecurityInstrumentPersonAuthorizedToReleaseLienNameSpecified
                    || this.SecurityInstrumentPersonAuthorizedToReleaseLienTelephoneValueSpecified
                    || this.SecurityInstrumentPersonAuthorizedToReleaseLienTitleDescriptionSpecified
                    || this.SecurityInstrumentPurchaseMoneyIndicatorSpecified
                    || this.SecurityInstrumentRealPropertyImprovedOrToBeImprovedIndicatorSpecified
                    || this.SecurityInstrumentRealPropertyImprovementsNotCoveredIndicatorSpecified
                    || this.SecurityInstrumentRecordingRequestedByNameSpecified
                    || this.SecurityInstrumentRenewalAndExtensionOfLiensAgainstHomesteadPropertyIndicatorSpecified
                    || this.SecurityInstrumentSignerForRegistersOfficeNameSpecified
                    || this.SecurityInstrumentTaxSerialNumberIdentifierSpecified
                    || this.SecurityInstrumentTrusteeFeePercentSpecified
                    || this.SecurityInstrumentVendorsLienDescriptionSpecified
                    || this.SecurityInstrumentVendorsLienIndicatorSpecified
                    || this.SecurityInstrumentVestingDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DefaultAcknowledgmentOfCashAdvanceAgainstNonHomesteadPropertyIndicator", Order = 0)]
        public MISMOIndicator DefaultAcknowledgmentOfCashAdvanceAgainstNonHomesteadPropertyIndicator { get; set; }
    
        [XmlIgnore]
        public bool DefaultAcknowledgmentOfCashAdvanceAgainstNonHomesteadPropertyIndicatorSpecified
        {
            get { return this.DefaultAcknowledgmentOfCashAdvanceAgainstNonHomesteadPropertyIndicator != null; }
            set { }
        }
    
        [XmlElement("DefaultApplicationFeesAmount", Order = 1)]
        public MISMOAmount DefaultApplicationFeesAmount { get; set; }
    
        [XmlIgnore]
        public bool DefaultApplicationFeesAmountSpecified
        {
            get { return this.DefaultApplicationFeesAmount != null; }
            set { }
        }
    
        [XmlElement("DefaultClosingPreparationFeesAmount", Order = 2)]
        public MISMOAmount DefaultClosingPreparationFeesAmount { get; set; }
    
        [XmlIgnore]
        public bool DefaultClosingPreparationFeesAmountSpecified
        {
            get { return this.DefaultClosingPreparationFeesAmount != null; }
            set { }
        }
    
        [XmlElement("DefaultLendingInstitutionPostOfficeBoxIdentifier", Order = 3)]
        public MISMOIdentifier DefaultLendingInstitutionPostOfficeBoxIdentifier { get; set; }
    
        [XmlIgnore]
        public bool DefaultLendingInstitutionPostOfficeBoxIdentifierSpecified
        {
            get { return this.DefaultLendingInstitutionPostOfficeBoxIdentifier != null; }
            set { }
        }
    
        [XmlElement("DocumentDrawnInName", Order = 4)]
        public MISMOString DocumentDrawnInName { get; set; }
    
        [XmlIgnore]
        public bool DocumentDrawnInNameSpecified
        {
            get { return this.DocumentDrawnInName != null; }
            set { }
        }
    
        [XmlElement("NameDocumentsDrawnInType", Order = 5)]
        public MISMOEnum<NameDocumentsDrawnInBase> NameDocumentsDrawnInType { get; set; }
    
        [XmlIgnore]
        public bool NameDocumentsDrawnInTypeSpecified
        {
            get { return this.NameDocumentsDrawnInType != null; }
            set { }
        }
    
        [XmlElement("NameDocumentsDrawnInTypeOtherDescription", Order = 6)]
        public MISMOString NameDocumentsDrawnInTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool NameDocumentsDrawnInTypeOtherDescriptionSpecified
        {
            get { return this.NameDocumentsDrawnInTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("SecurityInstrumentAssumptionFeeAmount", Order = 7)]
        public MISMOAmount SecurityInstrumentAssumptionFeeAmount { get; set; }
    
        [XmlIgnore]
        public bool SecurityInstrumentAssumptionFeeAmountSpecified
        {
            get { return this.SecurityInstrumentAssumptionFeeAmount != null; }
            set { }
        }
    
        [XmlElement("SecurityInstrumentAttorneyFeeMinimumAmount", Order = 8)]
        public MISMOAmount SecurityInstrumentAttorneyFeeMinimumAmount { get; set; }
    
        [XmlIgnore]
        public bool SecurityInstrumentAttorneyFeeMinimumAmountSpecified
        {
            get { return this.SecurityInstrumentAttorneyFeeMinimumAmount != null; }
            set { }
        }
    
        [XmlElement("SecurityInstrumentAttorneyFeePercent", Order = 9)]
        public MISMOPercent SecurityInstrumentAttorneyFeePercent { get; set; }
    
        [XmlIgnore]
        public bool SecurityInstrumentAttorneyFeePercentSpecified
        {
            get { return this.SecurityInstrumentAttorneyFeePercent != null; }
            set { }
        }
    
        [XmlElement("SecurityInstrumentCertifyingAttorneyName", Order = 10)]
        public MISMOString SecurityInstrumentCertifyingAttorneyName { get; set; }
    
        [XmlIgnore]
        public bool SecurityInstrumentCertifyingAttorneyNameSpecified
        {
            get { return this.SecurityInstrumentCertifyingAttorneyName != null; }
            set { }
        }
    
        [XmlElement("SecurityInstrumentDate", Order = 11)]
        public MISMODate SecurityInstrumentDate { get; set; }
    
        [XmlIgnore]
        public bool SecurityInstrumentDateSpecified
        {
            get { return this.SecurityInstrumentDate != null; }
            set { }
        }
    
        [XmlElement("SecurityInstrumentMaximumPrincipalIndebtednessAmount", Order = 12)]
        public MISMOAmount SecurityInstrumentMaximumPrincipalIndebtednessAmount { get; set; }
    
        [XmlIgnore]
        public bool SecurityInstrumentMaximumPrincipalIndebtednessAmountSpecified
        {
            get { return this.SecurityInstrumentMaximumPrincipalIndebtednessAmount != null; }
            set { }
        }
    
        [XmlElement("SecurityInstrumentMultipleUnitsRealPropertyIsImprovedOrToBeImprovedIndicator", Order = 13)]
        public MISMOIndicator SecurityInstrumentMultipleUnitsRealPropertyIsImprovedOrToBeImprovedIndicator { get; set; }
    
        [XmlIgnore]
        public bool SecurityInstrumentMultipleUnitsRealPropertyIsImprovedOrToBeImprovedIndicatorSpecified
        {
            get { return this.SecurityInstrumentMultipleUnitsRealPropertyIsImprovedOrToBeImprovedIndicator != null; }
            set { }
        }
    
        [XmlElement("SecurityInstrumentNoteHolderName", Order = 14)]
        public MISMOString SecurityInstrumentNoteHolderName { get; set; }
    
        [XmlIgnore]
        public bool SecurityInstrumentNoteHolderNameSpecified
        {
            get { return this.SecurityInstrumentNoteHolderName != null; }
            set { }
        }
    
        [XmlElement("SecurityInstrumentNoticeOfConfidentialityRightsDescription", Order = 15)]
        public MISMOString SecurityInstrumentNoticeOfConfidentialityRightsDescription { get; set; }
    
        [XmlIgnore]
        public bool SecurityInstrumentNoticeOfConfidentialityRightsDescriptionSpecified
        {
            get { return this.SecurityInstrumentNoticeOfConfidentialityRightsDescription != null; }
            set { }
        }
    
        [XmlElement("SecurityInstrumentOtherFeesAmount", Order = 16)]
        public MISMOAmount SecurityInstrumentOtherFeesAmount { get; set; }
    
        [XmlIgnore]
        public bool SecurityInstrumentOtherFeesAmountSpecified
        {
            get { return this.SecurityInstrumentOtherFeesAmount != null; }
            set { }
        }
    
        [XmlElement("SecurityInstrumentOtherFeesDescription", Order = 17)]
        public MISMOString SecurityInstrumentOtherFeesDescription { get; set; }
    
        [XmlIgnore]
        public bool SecurityInstrumentOtherFeesDescriptionSpecified
        {
            get { return this.SecurityInstrumentOtherFeesDescription != null; }
            set { }
        }
    
        [XmlElement("SecurityInstrumentOweltyOfPartitionIndicator", Order = 18)]
        public MISMOIndicator SecurityInstrumentOweltyOfPartitionIndicator { get; set; }
    
        [XmlIgnore]
        public bool SecurityInstrumentOweltyOfPartitionIndicatorSpecified
        {
            get { return this.SecurityInstrumentOweltyOfPartitionIndicator != null; }
            set { }
        }
    
        [XmlElement("SecurityInstrumentPersonAuthorizedToReleaseLienName", Order = 19)]
        public MISMOString SecurityInstrumentPersonAuthorizedToReleaseLienName { get; set; }
    
        [XmlIgnore]
        public bool SecurityInstrumentPersonAuthorizedToReleaseLienNameSpecified
        {
            get { return this.SecurityInstrumentPersonAuthorizedToReleaseLienName != null; }
            set { }
        }
    
        [XmlElement("SecurityInstrumentPersonAuthorizedToReleaseLienTelephoneValue", Order = 20)]
        public MISMONumericString SecurityInstrumentPersonAuthorizedToReleaseLienTelephoneValue { get; set; }
    
        [XmlIgnore]
        public bool SecurityInstrumentPersonAuthorizedToReleaseLienTelephoneValueSpecified
        {
            get { return this.SecurityInstrumentPersonAuthorizedToReleaseLienTelephoneValue != null; }
            set { }
        }
    
        [XmlElement("SecurityInstrumentPersonAuthorizedToReleaseLienTitleDescription", Order = 21)]
        public MISMOString SecurityInstrumentPersonAuthorizedToReleaseLienTitleDescription { get; set; }
    
        [XmlIgnore]
        public bool SecurityInstrumentPersonAuthorizedToReleaseLienTitleDescriptionSpecified
        {
            get { return this.SecurityInstrumentPersonAuthorizedToReleaseLienTitleDescription != null; }
            set { }
        }
    
        [XmlElement("SecurityInstrumentPurchaseMoneyIndicator", Order = 22)]
        public MISMOIndicator SecurityInstrumentPurchaseMoneyIndicator { get; set; }
    
        [XmlIgnore]
        public bool SecurityInstrumentPurchaseMoneyIndicatorSpecified
        {
            get { return this.SecurityInstrumentPurchaseMoneyIndicator != null; }
            set { }
        }
    
        [XmlElement("SecurityInstrumentRealPropertyImprovedOrToBeImprovedIndicator", Order = 23)]
        public MISMOIndicator SecurityInstrumentRealPropertyImprovedOrToBeImprovedIndicator { get; set; }
    
        [XmlIgnore]
        public bool SecurityInstrumentRealPropertyImprovedOrToBeImprovedIndicatorSpecified
        {
            get { return this.SecurityInstrumentRealPropertyImprovedOrToBeImprovedIndicator != null; }
            set { }
        }
    
        [XmlElement("SecurityInstrumentRealPropertyImprovementsNotCoveredIndicator", Order = 24)]
        public MISMOIndicator SecurityInstrumentRealPropertyImprovementsNotCoveredIndicator { get; set; }
    
        [XmlIgnore]
        public bool SecurityInstrumentRealPropertyImprovementsNotCoveredIndicatorSpecified
        {
            get { return this.SecurityInstrumentRealPropertyImprovementsNotCoveredIndicator != null; }
            set { }
        }
    
        [XmlElement("SecurityInstrumentRecordingRequestedByName", Order = 25)]
        public MISMOString SecurityInstrumentRecordingRequestedByName { get; set; }
    
        [XmlIgnore]
        public bool SecurityInstrumentRecordingRequestedByNameSpecified
        {
            get { return this.SecurityInstrumentRecordingRequestedByName != null; }
            set { }
        }
    
        [XmlElement("SecurityInstrumentRenewalAndExtensionOfLiensAgainstHomesteadPropertyIndicator", Order = 26)]
        public MISMOIndicator SecurityInstrumentRenewalAndExtensionOfLiensAgainstHomesteadPropertyIndicator { get; set; }
    
        [XmlIgnore]
        public bool SecurityInstrumentRenewalAndExtensionOfLiensAgainstHomesteadPropertyIndicatorSpecified
        {
            get { return this.SecurityInstrumentRenewalAndExtensionOfLiensAgainstHomesteadPropertyIndicator != null; }
            set { }
        }
    
        [XmlElement("SecurityInstrumentSignerForRegistersOfficeName", Order = 27)]
        public MISMOString SecurityInstrumentSignerForRegistersOfficeName { get; set; }
    
        [XmlIgnore]
        public bool SecurityInstrumentSignerForRegistersOfficeNameSpecified
        {
            get { return this.SecurityInstrumentSignerForRegistersOfficeName != null; }
            set { }
        }
    
        [XmlElement("SecurityInstrumentTaxSerialNumberIdentifier", Order = 28)]
        public MISMOIdentifier SecurityInstrumentTaxSerialNumberIdentifier { get; set; }
    
        [XmlIgnore]
        public bool SecurityInstrumentTaxSerialNumberIdentifierSpecified
        {
            get { return this.SecurityInstrumentTaxSerialNumberIdentifier != null; }
            set { }
        }
    
        [XmlElement("SecurityInstrumentTrusteeFeePercent", Order = 29)]
        public MISMOPercent SecurityInstrumentTrusteeFeePercent { get; set; }
    
        [XmlIgnore]
        public bool SecurityInstrumentTrusteeFeePercentSpecified
        {
            get { return this.SecurityInstrumentTrusteeFeePercent != null; }
            set { }
        }
    
        [XmlElement("SecurityInstrumentVendorsLienDescription", Order = 30)]
        public MISMOString SecurityInstrumentVendorsLienDescription { get; set; }
    
        [XmlIgnore]
        public bool SecurityInstrumentVendorsLienDescriptionSpecified
        {
            get { return this.SecurityInstrumentVendorsLienDescription != null; }
            set { }
        }
    
        [XmlElement("SecurityInstrumentVendorsLienIndicator", Order = 31)]
        public MISMOIndicator SecurityInstrumentVendorsLienIndicator { get; set; }
    
        [XmlIgnore]
        public bool SecurityInstrumentVendorsLienIndicatorSpecified
        {
            get { return this.SecurityInstrumentVendorsLienIndicator != null; }
            set { }
        }
    
        [XmlElement("SecurityInstrumentVestingDescription", Order = 32)]
        public MISMOString SecurityInstrumentVestingDescription { get; set; }
    
        [XmlIgnore]
        public bool SecurityInstrumentVestingDescriptionSpecified
        {
            get { return this.SecurityInstrumentVestingDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 33)]
        public SECURITY_INSTRUMENT_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
