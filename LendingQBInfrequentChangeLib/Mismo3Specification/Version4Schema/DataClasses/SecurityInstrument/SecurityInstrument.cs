namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class SECURITY_INSTRUMENT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.SecurityInstrumentAddendumsSpecified
                    || this.SecurityInstrumentDetailSpecified
                    || this.SecurityInstrumentRidersSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("SECURITY_INSTRUMENT_ADDENDUMS", Order = 0)]
        public SECURITY_INSTRUMENT_ADDENDUMS SecurityInstrumentAddendums { get; set; }
    
        [XmlIgnore]
        public bool SecurityInstrumentAddendumsSpecified
        {
            get { return this.SecurityInstrumentAddendums != null && this.SecurityInstrumentAddendums.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SECURITY_INSTRUMENT_DETAIL", Order = 1)]
        public SECURITY_INSTRUMENT_DETAIL SecurityInstrumentDetail { get; set; }
    
        [XmlIgnore]
        public bool SecurityInstrumentDetailSpecified
        {
            get { return this.SecurityInstrumentDetail != null && this.SecurityInstrumentDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SECURITY_INSTRUMENT_RIDERS", Order = 2)]
        public SECURITY_INSTRUMENT_RIDERS SecurityInstrumentRiders { get; set; }
    
        [XmlIgnore]
        public bool SecurityInstrumentRidersSpecified
        {
            get { return this.SecurityInstrumentRiders != null && this.SecurityInstrumentRiders.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public SECURITY_INSTRUMENT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
