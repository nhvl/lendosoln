namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    [XmlRoot(nameof(MESSAGE), Namespace = Mismo3Constants.MismoNamespace, IsNullable = false)]
    public class MESSAGE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AboutVersionsSpecified
                    || this.DealSetsSpecified
                    || this.DocumentSetsSpecified
                    || this.RelationshipsSpecified
                    || this.SystemSignaturesSpecified
                    || this.ExtensionSpecified
                    || this.MISMOLogicalDataDictionaryIdentifierSpecified
                    || this.MISMOReferenceModelIdentifierSpecified;
            }
        }
    
        [XmlElement("ABOUT_VERSIONS", Order = 0)]
        public ABOUT_VERSIONS AboutVersions { get; set; }
    
        [XmlIgnore]
        public bool AboutVersionsSpecified
        {
            get { return this.AboutVersions != null && this.AboutVersions.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("DEAL_SETS", Order = 1)]
        public DEAL_SETS DealSets { get; set; }
    
        [XmlIgnore]
        public bool DealSetsSpecified
        {
            get { return this.DealSets != null && this.DealSets.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("DOCUMENT_SETS", Order = 2)]
        public DOCUMENT_SETS DocumentSets { get; set; }
    
        [XmlIgnore]
        public bool DocumentSetsSpecified
        {
            get { return this.DocumentSets != null && this.DocumentSets.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("RELATIONSHIPS", Order = 3)]
        public RELATIONSHIPS Relationships { get; set; }
    
        [XmlIgnore]
        public bool RelationshipsSpecified
        {
            get { return this.Relationships != null && this.Relationships.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SYSTEM_SIGNATURES", Order = 4)]
        public SYSTEM_SIGNATURES SystemSignatures { get; set; }
    
        [XmlIgnore]
        public bool SystemSignaturesSpecified
        {
            get { return this.SystemSignatures != null && this.SystemSignatures.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 5)]
        public MESSAGE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlAttribute(AttributeName = "MISMOLogicalDataDictionaryIdentifier")]
        public string MISMOLogicalDataDictionaryIdentifier { get; set; } = "3.4.0[B324]";

        [XmlIgnore]
        public bool MISMOLogicalDataDictionaryIdentifierSpecified
        {
            get { return !string.IsNullOrEmpty(this.MISMOLogicalDataDictionaryIdentifier); }
            set { }
        }
    
        [XmlAttribute(AttributeName = "MISMOReferenceModelIdentifier")]
        public string MISMOReferenceModelIdentifier { get; set; } = "3.4.0[B324]";

        [XmlIgnore]
        public bool MISMOReferenceModelIdentifierSpecified
        {
            get { return !string.IsNullOrEmpty(this.MISMOReferenceModelIdentifier); }
            set { }
        }

        [XmlNamespaceDeclarations]
        public virtual XmlSerializerNamespaces Xmlns { get; set; } = ExtensionNamespaces.LQBNamespaces;
    }
}
