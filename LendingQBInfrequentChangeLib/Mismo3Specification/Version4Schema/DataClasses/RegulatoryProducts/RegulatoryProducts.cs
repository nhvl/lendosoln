namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class REGULATORY_PRODUCTS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.RegulatoryProductListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("REGULATORY_PRODUCT", Order = 0)]
        public List<REGULATORY_PRODUCT> RegulatoryProductList { get; set; } = new List<REGULATORY_PRODUCT>();
    
        [XmlIgnore]
        public bool RegulatoryProductListSpecified
        {
            get { return this.RegulatoryProductList != null && this.RegulatoryProductList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public REGULATORY_PRODUCTS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
