namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class VERIFICATION_OF_INCOME
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.VerificationOfIncomeRequestSpecified
                    || this.VerificationOfIncomeResponseSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("VERIFICATION_OF_INCOME_REQUEST", Order = 0)]
        public VERIFICATION_OF_INCOME_REQUEST VerificationOfIncomeRequest { get; set; }
    
        [XmlIgnore]
        public bool VerificationOfIncomeRequestSpecified
        {
            get { return this.VerificationOfIncomeRequest != null && this.VerificationOfIncomeRequest.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("VERIFICATION_OF_INCOME_RESPONSE", Order = 1)]
        public VERIFICATION_OF_INCOME_RESPONSE VerificationOfIncomeResponse { get; set; }
    
        [XmlIgnore]
        public bool VerificationOfIncomeResponseSpecified
        {
            get { return this.VerificationOfIncomeResponse != null && this.VerificationOfIncomeResponse.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public VERIFICATION_OF_INCOME_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
