namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class MULTIFAMILY_RENT_SCHEDULE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.MultifamilyRentScheduleDetailSpecified
                    || this.RentIncludesUtilitiesSpecified
                    || this.UnitRentSchedulesSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("MULTIFAMILY_RENT_SCHEDULE_DETAIL", Order = 0)]
        public MULTIFAMILY_RENT_SCHEDULE_DETAIL MultifamilyRentScheduleDetail { get; set; }
    
        [XmlIgnore]
        public bool MultifamilyRentScheduleDetailSpecified
        {
            get { return this.MultifamilyRentScheduleDetail != null && this.MultifamilyRentScheduleDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("RENT_INCLUDES_UTILITIES", Order = 1)]
        public RENT_INCLUDES_UTILITIES RentIncludesUtilities { get; set; }
    
        [XmlIgnore]
        public bool RentIncludesUtilitiesSpecified
        {
            get { return this.RentIncludesUtilities != null && this.RentIncludesUtilities.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("UNIT_RENT_SCHEDULES", Order = 2)]
        public UNIT_RENT_SCHEDULES UnitRentSchedules { get; set; }
    
        [XmlIgnore]
        public bool UnitRentSchedulesSpecified
        {
            get { return this.UnitRentSchedules != null && this.UnitRentSchedules.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public MULTIFAMILY_RENT_SCHEDULE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
