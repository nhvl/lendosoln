namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class MULTIFAMILY_RENT_SCHEDULE_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.MarketRentalDataCommentDescriptionSpecified
                    || this.RentalActualAdditionalMonthlyIncomeAmountSpecified
                    || this.RentalActualGrossMonthlyRentAmountSpecified
                    || this.RentalActualTotalMonthlyIncomeAmountSpecified
                    || this.RentalEstimatedAdditionalMonthlyIncomeAmountSpecified
                    || this.RentalEstimatedGrossMonthlyRentAmountSpecified
                    || this.RentalEstimatedTotalMonthlyIncomeAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("MarketRentalDataCommentDescription", Order = 0)]
        public MISMOString MarketRentalDataCommentDescription { get; set; }
    
        [XmlIgnore]
        public bool MarketRentalDataCommentDescriptionSpecified
        {
            get { return this.MarketRentalDataCommentDescription != null; }
            set { }
        }
    
        [XmlElement("RentalActualAdditionalMonthlyIncomeAmount", Order = 1)]
        public MISMOAmount RentalActualAdditionalMonthlyIncomeAmount { get; set; }
    
        [XmlIgnore]
        public bool RentalActualAdditionalMonthlyIncomeAmountSpecified
        {
            get { return this.RentalActualAdditionalMonthlyIncomeAmount != null; }
            set { }
        }
    
        [XmlElement("RentalActualGrossMonthlyRentAmount", Order = 2)]
        public MISMOAmount RentalActualGrossMonthlyRentAmount { get; set; }
    
        [XmlIgnore]
        public bool RentalActualGrossMonthlyRentAmountSpecified
        {
            get { return this.RentalActualGrossMonthlyRentAmount != null; }
            set { }
        }
    
        [XmlElement("RentalActualTotalMonthlyIncomeAmount", Order = 3)]
        public MISMOAmount RentalActualTotalMonthlyIncomeAmount { get; set; }
    
        [XmlIgnore]
        public bool RentalActualTotalMonthlyIncomeAmountSpecified
        {
            get { return this.RentalActualTotalMonthlyIncomeAmount != null; }
            set { }
        }
    
        [XmlElement("RentalEstimatedAdditionalMonthlyIncomeAmount", Order = 4)]
        public MISMOAmount RentalEstimatedAdditionalMonthlyIncomeAmount { get; set; }
    
        [XmlIgnore]
        public bool RentalEstimatedAdditionalMonthlyIncomeAmountSpecified
        {
            get { return this.RentalEstimatedAdditionalMonthlyIncomeAmount != null; }
            set { }
        }
    
        [XmlElement("RentalEstimatedGrossMonthlyRentAmount", Order = 5)]
        public MISMOAmount RentalEstimatedGrossMonthlyRentAmount { get; set; }
    
        [XmlIgnore]
        public bool RentalEstimatedGrossMonthlyRentAmountSpecified
        {
            get { return this.RentalEstimatedGrossMonthlyRentAmount != null; }
            set { }
        }
    
        [XmlElement("RentalEstimatedTotalMonthlyIncomeAmount", Order = 6)]
        public MISMOAmount RentalEstimatedTotalMonthlyIncomeAmount { get; set; }
    
        [XmlIgnore]
        public bool RentalEstimatedTotalMonthlyIncomeAmountSpecified
        {
            get { return this.RentalEstimatedTotalMonthlyIncomeAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 7)]
        public MULTIFAMILY_RENT_SCHEDULE_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
