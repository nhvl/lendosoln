namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PRIA
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.PriaRequestSpecified
                    || this.PriaResponseSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PRIA_REQUEST", Order = 0)]
        public PRIA_REQUEST PriaRequest { get; set; }
    
        [XmlIgnore]
        public bool PriaRequestSpecified
        {
            get { return this.PriaRequest != null && this.PriaRequest.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PRIA_RESPONSE", Order = 1)]
        public PRIA_RESPONSE PriaResponse { get; set; }
    
        [XmlIgnore]
        public bool PriaResponseSpecified
        {
            get { return this.PriaResponse != null && this.PriaResponse.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public PRIA_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
