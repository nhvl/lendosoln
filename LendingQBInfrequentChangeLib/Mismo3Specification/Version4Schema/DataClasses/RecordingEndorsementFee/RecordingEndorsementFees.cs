namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class RECORDING_ENDORSEMENT_FEES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.RecordingEndorsementFeeListSpecified
                    || this.RecordingEndorsementFeesSummarySpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("RECORDING_ENDORSEMENT_FEE", Order = 0)]
        public List<RECORDING_ENDORSEMENT_FEE> RecordingEndorsementFeeList { get; set; } = new List<RECORDING_ENDORSEMENT_FEE>();
    
        [XmlIgnore]
        public bool RecordingEndorsementFeeListSpecified
        {
            get { return this.RecordingEndorsementFeeList != null && this.RecordingEndorsementFeeList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("RECORDING_ENDORSEMENT_FEES_SUMMARY", Order = 1)]
        public RECORDING_ENDORSEMENT_FEES_SUMMARY RecordingEndorsementFeesSummary { get; set; }
    
        [XmlIgnore]
        public bool RecordingEndorsementFeesSummarySpecified
        {
            get { return this.RecordingEndorsementFeesSummary != null && this.RecordingEndorsementFeesSummary.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public RECORDING_ENDORSEMENT_FEES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
