namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class RECORDING_ENDORSEMENT_FEES_SUMMARY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.RecordingEndorsementFeesTotalAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("RecordingEndorsementFeesTotalAmount", Order = 0)]
        public MISMOAmount RecordingEndorsementFeesTotalAmount { get; set; }
    
        [XmlIgnore]
        public bool RecordingEndorsementFeesTotalAmountSpecified
        {
            get { return this.RecordingEndorsementFeesTotalAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public RECORDING_ENDORSEMENT_FEES_SUMMARY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
