namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class RECORDING_ENDORSEMENT_FEE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.RecordingEndorsementFeeAmountSpecified
                    || this.RecordingEndorsementFeeDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("RecordingEndorsementFeeAmount", Order = 0)]
        public MISMOAmount RecordingEndorsementFeeAmount { get; set; }
    
        [XmlIgnore]
        public bool RecordingEndorsementFeeAmountSpecified
        {
            get { return this.RecordingEndorsementFeeAmount != null; }
            set { }
        }
    
        [XmlElement("RecordingEndorsementFeeDescription", Order = 1)]
        public MISMOString RecordingEndorsementFeeDescription { get; set; }
    
        [XmlIgnore]
        public bool RecordingEndorsementFeeDescriptionSpecified
        {
            get { return this.RecordingEndorsementFeeDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public RECORDING_ENDORSEMENT_FEE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
