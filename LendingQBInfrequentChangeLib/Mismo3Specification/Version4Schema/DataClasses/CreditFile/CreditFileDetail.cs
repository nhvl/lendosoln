namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CREDIT_FILE_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditFileInfileDateSpecified
                    || this.CreditFileResultStatusTypeSpecified
                    || this.CreditFileResultStatusTypeOtherDescriptionSpecified
                    || this.CreditRepositorySourceTypeSpecified
                    || this.CreditRepositorySourceTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CreditFileInfileDate", Order = 0)]
        public MISMODate CreditFileInfileDate { get; set; }
    
        [XmlIgnore]
        public bool CreditFileInfileDateSpecified
        {
            get { return this.CreditFileInfileDate != null; }
            set { }
        }
    
        [XmlElement("CreditFileResultStatusType", Order = 1)]
        public MISMOEnum<CreditFileResultStatusBase> CreditFileResultStatusType { get; set; }
    
        [XmlIgnore]
        public bool CreditFileResultStatusTypeSpecified
        {
            get { return this.CreditFileResultStatusType != null; }
            set { }
        }
    
        [XmlElement("CreditFileResultStatusTypeOtherDescription", Order = 2)]
        public MISMOString CreditFileResultStatusTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool CreditFileResultStatusTypeOtherDescriptionSpecified
        {
            get { return this.CreditFileResultStatusTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("CreditRepositorySourceType", Order = 3)]
        public MISMOEnum<CreditRepositorySourceBase> CreditRepositorySourceType { get; set; }
    
        [XmlIgnore]
        public bool CreditRepositorySourceTypeSpecified
        {
            get { return this.CreditRepositorySourceType != null; }
            set { }
        }
    
        [XmlElement("CreditRepositorySourceTypeOtherDescription", Order = 4)]
        public MISMOString CreditRepositorySourceTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool CreditRepositorySourceTypeOtherDescriptionSpecified
        {
            get { return this.CreditRepositorySourceTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 5)]
        public CREDIT_FILE_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
