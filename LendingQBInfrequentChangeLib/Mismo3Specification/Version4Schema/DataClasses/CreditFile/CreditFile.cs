namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CREDIT_FILE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditCommentsSpecified
                    || this.CreditErrorMessagesSpecified
                    || this.CreditFileDetailSpecified
                    || this.CreditFileOwningBureauSpecified
                    || this.CreditFileVariationsSpecified
                    || this.PartySpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CREDIT_COMMENTS", Order = 0)]
        public CREDIT_COMMENTS CreditComments { get; set; }
    
        [XmlIgnore]
        public bool CreditCommentsSpecified
        {
            get { return this.CreditComments != null && this.CreditComments.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CREDIT_ERROR_MESSAGES", Order = 1)]
        public CREDIT_ERROR_MESSAGES CreditErrorMessages { get; set; }
    
        [XmlIgnore]
        public bool CreditErrorMessagesSpecified
        {
            get { return this.CreditErrorMessages != null && this.CreditErrorMessages.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CREDIT_FILE_DETAIL", Order = 2)]
        public CREDIT_FILE_DETAIL CreditFileDetail { get; set; }
    
        [XmlIgnore]
        public bool CreditFileDetailSpecified
        {
            get { return this.CreditFileDetail != null && this.CreditFileDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CREDIT_FILE_OWNING_BUREAU", Order = 3)]
        public CREDIT_FILE_OWNING_BUREAU CreditFileOwningBureau { get; set; }
    
        [XmlIgnore]
        public bool CreditFileOwningBureauSpecified
        {
            get { return this.CreditFileOwningBureau != null && this.CreditFileOwningBureau.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CREDIT_FILE_VARIATIONS", Order = 4)]
        public CREDIT_FILE_VARIATIONS CreditFileVariations { get; set; }
    
        [XmlIgnore]
        public bool CreditFileVariationsSpecified
        {
            get { return this.CreditFileVariations != null && this.CreditFileVariations.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PARTY", Order = 5)]
        public PARTY Party { get; set; }
    
        [XmlIgnore]
        public bool PartySpecified
        {
            get { return this.Party != null && this.Party.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 6)]
        public CREDIT_FILE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
