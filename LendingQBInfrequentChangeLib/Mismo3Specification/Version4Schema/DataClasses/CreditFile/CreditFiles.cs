namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class CREDIT_FILES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditFileListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CREDIT_FILE", Order = 0)]
        public List<CREDIT_FILE> CreditFileList { get; set; } = new List<CREDIT_FILE>();
    
        [XmlIgnore]
        public bool CreditFileListSpecified
        {
            get { return this.CreditFileList != null && this.CreditFileList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public CREDIT_FILES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
