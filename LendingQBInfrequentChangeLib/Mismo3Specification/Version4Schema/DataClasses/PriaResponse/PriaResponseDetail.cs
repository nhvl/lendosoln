namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PRIA_RESPONSE_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.PRIAResponseNoteTextSpecified
                    || this.PRIAResponseRelatedDocumentsIndicatorSpecified
                    || this.PRIAResponseTypeSpecified
                    || this.PRIAResponseTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PRIAResponseNoteText", Order = 0)]
        public MISMOString PRIAResponseNoteText { get; set; }
    
        [XmlIgnore]
        public bool PRIAResponseNoteTextSpecified
        {
            get { return this.PRIAResponseNoteText != null; }
            set { }
        }
    
        [XmlElement("PRIAResponseRelatedDocumentsIndicator", Order = 1)]
        public MISMOIndicator PRIAResponseRelatedDocumentsIndicator { get; set; }
    
        [XmlIgnore]
        public bool PRIAResponseRelatedDocumentsIndicatorSpecified
        {
            get { return this.PRIAResponseRelatedDocumentsIndicator != null; }
            set { }
        }
    
        [XmlElement("PRIAResponseType", Order = 2)]
        public MISMOEnum<PRIAResponseBase> PRIAResponseType { get; set; }
    
        [XmlIgnore]
        public bool PRIAResponseTypeSpecified
        {
            get { return this.PRIAResponseType != null; }
            set { }
        }
    
        [XmlElement("PRIAResponseTypeOtherDescription", Order = 3)]
        public MISMOString PRIAResponseTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool PRIAResponseTypeOtherDescriptionSpecified
        {
            get { return this.PRIAResponseTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public PRIA_RESPONSE_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
