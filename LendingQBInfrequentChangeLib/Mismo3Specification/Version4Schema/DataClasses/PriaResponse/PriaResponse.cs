namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PRIA_RESPONSE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.OriginatingRecordingRequestSpecified
                    || this.PriaResponseDetailSpecified
                    || this.RecordingEndorsementSpecified
                    || this.RecordingErrorsSpecified
                    || this.RecordingTransactionIdentifierSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ORIGINATING_RECORDING_REQUEST", Order = 0)]
        public ORIGINATING_RECORDING_REQUEST OriginatingRecordingRequest { get; set; }
    
        [XmlIgnore]
        public bool OriginatingRecordingRequestSpecified
        {
            get { return this.OriginatingRecordingRequest != null && this.OriginatingRecordingRequest.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PRIA_RESPONSE_DETAIL", Order = 1)]
        public PRIA_RESPONSE_DETAIL PriaResponseDetail { get; set; }
    
        [XmlIgnore]
        public bool PriaResponseDetailSpecified
        {
            get { return this.PriaResponseDetail != null && this.PriaResponseDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("RECORDING_ENDORSEMENT", Order = 2)]
        public RECORDING_ENDORSEMENT RecordingEndorsement { get; set; }
    
        [XmlIgnore]
        public bool RecordingEndorsementSpecified
        {
            get { return this.RecordingEndorsement != null && this.RecordingEndorsement.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("RECORDING_ERRORS", Order = 3)]
        public RECORDING_ERRORS RecordingErrors { get; set; }
    
        [XmlIgnore]
        public bool RecordingErrorsSpecified
        {
            get { return this.RecordingErrors != null && this.RecordingErrors.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("RECORDING_TRANSACTION_IDENTIFIER", Order = 4)]
        public RECORDING_TRANSACTION_IDENTIFIER RecordingTransactionIdentifier { get; set; }
    
        [XmlIgnore]
        public bool RecordingTransactionIdentifierSpecified
        {
            get { return this.RecordingTransactionIdentifier != null && this.RecordingTransactionIdentifier.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 5)]
        public PRIA_RESPONSE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
