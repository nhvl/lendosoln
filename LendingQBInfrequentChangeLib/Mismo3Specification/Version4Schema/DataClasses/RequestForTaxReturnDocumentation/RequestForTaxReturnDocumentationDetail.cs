namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class REQUEST_FOR_TAX_RETURN_DOCUMENTATION_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.NumberOfReturnsCountSpecified
                    || this.RefundToThirdPartyIndicatorSpecified
                    || this.TaxpayerIdentityTheftIndicatorSpecified
                    || this.TotalCostForReturnsAmountSpecified
                    || this.TranscriptCertifiedCopyIndicatorSpecified
                    || this.VerificationServicerDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("NumberOfReturnsCount", Order = 0)]
        public MISMOCount NumberOfReturnsCount { get; set; }
    
        [XmlIgnore]
        public bool NumberOfReturnsCountSpecified
        {
            get { return this.NumberOfReturnsCount != null; }
            set { }
        }
    
        [XmlElement("RefundToThirdPartyIndicator", Order = 1)]
        public MISMOIndicator RefundToThirdPartyIndicator { get; set; }
    
        [XmlIgnore]
        public bool RefundToThirdPartyIndicatorSpecified
        {
            get { return this.RefundToThirdPartyIndicator != null; }
            set { }
        }
    
        [XmlElement("TaxpayerIdentityTheftIndicator", Order = 2)]
        public MISMOIndicator TaxpayerIdentityTheftIndicator { get; set; }
    
        [XmlIgnore]
        public bool TaxpayerIdentityTheftIndicatorSpecified
        {
            get { return this.TaxpayerIdentityTheftIndicator != null; }
            set { }
        }
    
        [XmlElement("TotalCostForReturnsAmount", Order = 3)]
        public MISMOAmount TotalCostForReturnsAmount { get; set; }
    
        [XmlIgnore]
        public bool TotalCostForReturnsAmountSpecified
        {
            get { return this.TotalCostForReturnsAmount != null; }
            set { }
        }
    
        [XmlElement("TranscriptCertifiedCopyIndicator", Order = 4)]
        public MISMOIndicator TranscriptCertifiedCopyIndicator { get; set; }
    
        [XmlIgnore]
        public bool TranscriptCertifiedCopyIndicatorSpecified
        {
            get { return this.TranscriptCertifiedCopyIndicator != null; }
            set { }
        }
    
        [XmlElement("VerificationServicerDescription", Order = 5)]
        public MISMOString VerificationServicerDescription { get; set; }
    
        [XmlIgnore]
        public bool VerificationServicerDescriptionSpecified
        {
            get { return this.VerificationServicerDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 6)]
        public REQUEST_FOR_TAX_RETURN_DOCUMENTATION_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
