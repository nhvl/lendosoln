namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class REQUEST_FOR_TAX_RETURN_DOCUMENTATION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AddressesSpecified
                    || this.RequestForTaxReturnDocumentationDetailSpecified
                    || this.TaxReturnDocumentationsSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ADDRESSES", Order = 0)]
        public ADDRESSES Addresses { get; set; }
    
        [XmlIgnore]
        public bool AddressesSpecified
        {
            get { return this.Addresses != null && this.Addresses.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("REQUEST_FOR_TAX_RETURN_DOCUMENTATION_DETAIL", Order = 1)]
        public REQUEST_FOR_TAX_RETURN_DOCUMENTATION_DETAIL RequestForTaxReturnDocumentationDetail { get; set; }
    
        [XmlIgnore]
        public bool RequestForTaxReturnDocumentationDetailSpecified
        {
            get { return this.RequestForTaxReturnDocumentationDetail != null && this.RequestForTaxReturnDocumentationDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("TAX_RETURN_DOCUMENTATIONS", Order = 2)]
        public TAX_RETURN_DOCUMENTATIONS TaxReturnDocumentations { get; set; }
    
        [XmlIgnore]
        public bool TaxReturnDocumentationsSpecified
        {
            get { return this.TaxReturnDocumentations != null && this.TaxReturnDocumentations.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public REQUEST_FOR_TAX_RETURN_DOCUMENTATION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
