namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class AGENT_VALIDATION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AgentValidationAffiliatedWithTitleCompanyIndicatorSpecified
                    || this.AgentValidationAgentFoundIndicatorSpecified
                    || this.AgentValidationAuthorizedForTransactionIndicatorSpecified
                    || this.TitleAgentValidationDescriptionSpecified
                    || this.TitleAgentValidationReasonTypeSpecified
                    || this.TitleAgentValidationReasonTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AgentValidationAffiliatedWithTitleCompanyIndicator", Order = 0)]
        public MISMOIndicator AgentValidationAffiliatedWithTitleCompanyIndicator { get; set; }
    
        [XmlIgnore]
        public bool AgentValidationAffiliatedWithTitleCompanyIndicatorSpecified
        {
            get { return this.AgentValidationAffiliatedWithTitleCompanyIndicator != null; }
            set { }
        }
    
        [XmlElement("AgentValidationAgentFoundIndicator", Order = 1)]
        public MISMOIndicator AgentValidationAgentFoundIndicator { get; set; }
    
        [XmlIgnore]
        public bool AgentValidationAgentFoundIndicatorSpecified
        {
            get { return this.AgentValidationAgentFoundIndicator != null; }
            set { }
        }
    
        [XmlElement("AgentValidationAuthorizedForTransactionIndicator", Order = 2)]
        public MISMOIndicator AgentValidationAuthorizedForTransactionIndicator { get; set; }
    
        [XmlIgnore]
        public bool AgentValidationAuthorizedForTransactionIndicatorSpecified
        {
            get { return this.AgentValidationAuthorizedForTransactionIndicator != null; }
            set { }
        }
    
        [XmlElement("TitleAgentValidationDescription", Order = 3)]
        public MISMOString TitleAgentValidationDescription { get; set; }
    
        [XmlIgnore]
        public bool TitleAgentValidationDescriptionSpecified
        {
            get { return this.TitleAgentValidationDescription != null; }
            set { }
        }
    
        [XmlElement("TitleAgentValidationReasonType", Order = 4)]
        public MISMOEnum<TitleAgentValidationReasonBase> TitleAgentValidationReasonType { get; set; }
    
        [XmlIgnore]
        public bool TitleAgentValidationReasonTypeSpecified
        {
            get { return this.TitleAgentValidationReasonType != null; }
            set { }
        }
    
        [XmlElement("TitleAgentValidationReasonTypeOtherDescription", Order = 5)]
        public MISMOString TitleAgentValidationReasonTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool TitleAgentValidationReasonTypeOtherDescriptionSpecified
        {
            get { return this.TitleAgentValidationReasonTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 6)]
        public AGENT_VALIDATION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
