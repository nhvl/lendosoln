namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum NoteAddendumBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AffordableMeritRate")]
        AffordableMeritRate,
        
        [XmlEnum("Arbitration")]
        Arbitration,
        
        [XmlEnum("Balloon")]
        Balloon,
        
        [XmlEnum("Construction")]
        Construction,
        
        [XmlEnum("FixedRateOption")]
        FixedRateOption,
        
        [XmlEnum("GEM")]
        GEM,
        
        [XmlEnum("GPM")]
        GPM,
        
        [XmlEnum("InterestOnly")]
        InterestOnly,
        
        [XmlEnum("InterVivosRevocableTrust")]
        InterVivosRevocableTrust,
        
        [XmlEnum("OccupancyDeclaration")]
        OccupancyDeclaration,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Prepayment")]
        Prepayment,
        
        [XmlEnum("RateImprovement")]
        RateImprovement,
    }
}
