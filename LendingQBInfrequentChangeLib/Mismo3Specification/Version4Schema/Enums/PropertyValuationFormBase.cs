namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PropertyValuationFormBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AppraisalUpdateAndOrCompletionReport")]
        AppraisalUpdateAndOrCompletionReport,
        
        [XmlEnum("ComplianceInspectionReport")]
        ComplianceInspectionReport,
        
        [XmlEnum("ConditionalCommitmentDirectEndorsementStatementOfAppraisedValue")]
        ConditionalCommitmentDirectEndorsementStatementOfAppraisedValue,
        
        [XmlEnum("DefinitionsStatementOfLimitingConditionsAndAppraisersCertification")]
        DefinitionsStatementOfLimitingConditionsAndAppraisersCertification,
        
        [XmlEnum("DesktopUnderwriterPropertyInspectionReport")]
        DesktopUnderwriterPropertyInspectionReport,
        
        [XmlEnum("EmployeeRelocationCouncil2001")]
        EmployeeRelocationCouncil2001,
        
        [XmlEnum("ExteriorOnlyInspectionIndividualCondominiumUnitAppraisalReport")]
        ExteriorOnlyInspectionIndividualCondominiumUnitAppraisalReport,
        
        [XmlEnum("ExteriorOnlyInspectionIndividualCooperativeInterestAppraisalReport")]
        ExteriorOnlyInspectionIndividualCooperativeInterestAppraisalReport,
        
        [XmlEnum("ExteriorOnlyInspectionResidentialAppraisalReport")]
        ExteriorOnlyInspectionResidentialAppraisalReport,
        
        [XmlEnum("ForYourProtectionGetAHomeInspection")]
        ForYourProtectionGetAHomeInspection,
        
        [XmlEnum("GeneralAddendum")]
        GeneralAddendum,
        
        [XmlEnum("HUD_VAAddendumToURLA")]
        HUD_VAAddendumToURLA,
        
        [XmlEnum("ImportantNoticeToHomebuyers")]
        ImportantNoticeToHomebuyers,
        
        [XmlEnum("IndividualCondominiumUnitAppraisalReport")]
        IndividualCondominiumUnitAppraisalReport,
        
        [XmlEnum("IndividualCooperativeInterestAppraisalReport")]
        IndividualCooperativeInterestAppraisalReport,
        
        [XmlEnum("LoanProspectorConditionAndMarketability")]
        LoanProspectorConditionAndMarketability,
        
        [XmlEnum("ManufacturedHomeAppraisalReport")]
        ManufacturedHomeAppraisalReport,
        
        [XmlEnum("ManufacturedHomeAppraisalReportVA")]
        ManufacturedHomeAppraisalReportVA,
        
        [XmlEnum("MarketConditionsAddendumToTheAppraisalReport")]
        MarketConditionsAddendumToTheAppraisalReport,
        
        [XmlEnum("MortgageesAssuranceOfCompletion")]
        MortgageesAssuranceOfCompletion,
        
        [XmlEnum("NewConstructionSubterraneanTermiteSoilTreatmentRecord")]
        NewConstructionSubterraneanTermiteSoilTreatmentRecord,
        
        [XmlEnum("OneUnitResidentialAppraisalFieldReviewReport")]
        OneUnitResidentialAppraisalFieldReviewReport,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("RequestForDeterminationOfReasonableValue")]
        RequestForDeterminationOfReasonableValue,
        
        [XmlEnum("SmallResidentialIncomePropertyAppraisalReport")]
        SmallResidentialIncomePropertyAppraisalReport,
        
        [XmlEnum("SubterraneanTermiteSoilTreatmentBuildersGuarantee")]
        SubterraneanTermiteSoilTreatmentBuildersGuarantee,
        
        [XmlEnum("TwoToFourUnitResidentialAppraisalFieldReviewReport")]
        TwoToFourUnitResidentialAppraisalFieldReviewReport,
        
        [XmlEnum("UniformResidentialAppraisalReport")]
        UniformResidentialAppraisalReport,
        
        [XmlEnum("WorldwideEmployeeRelocationCouncilSummaryAppraisalReport")]
        WorldwideEmployeeRelocationCouncilSummaryAppraisalReport,
    }
}
