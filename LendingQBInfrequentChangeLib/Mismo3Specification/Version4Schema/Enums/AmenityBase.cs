namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum AmenityBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Balcony")]
        Balcony,
        
        [XmlEnum("Deck")]
        Deck,
        
        [XmlEnum("Fence")]
        Fence,
        
        [XmlEnum("Fireplace")]
        Fireplace,
        
        [XmlEnum("HomeTheater")]
        HomeTheater,
        
        [XmlEnum("Intercom")]
        Intercom,
        
        [XmlEnum("JettedTub")]
        JettedTub,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Patio")]
        Patio,
        
        [XmlEnum("Pool")]
        Pool,
        
        [XmlEnum("Porch")]
        Porch,
        
        [XmlEnum("SecuritySystem")]
        SecuritySystem,
        
        [XmlEnum("Spa")]
        Spa,
        
        [XmlEnum("WoodStove")]
        WoodStove,
    }
}
