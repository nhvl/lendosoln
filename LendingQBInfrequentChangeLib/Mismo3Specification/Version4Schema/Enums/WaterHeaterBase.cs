namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum WaterHeaterBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Solar")]
        Solar,
        
        [XmlEnum("StandardTank")]
        StandardTank,
        
        [XmlEnum("Tankless")]
        Tankless,
    }
}
