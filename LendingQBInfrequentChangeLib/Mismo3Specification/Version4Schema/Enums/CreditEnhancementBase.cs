namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum CreditEnhancementBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("CashAsset")]
        CashAsset,
        
        [XmlEnum("CollateralTaxAssessmentValue")]
        CollateralTaxAssessmentValue,
        
        [XmlEnum("CommercialCollateral")]
        CommercialCollateral,
        
        [XmlEnum("Excluded")]
        Excluded,
        
        [XmlEnum("Indemnification")]
        Indemnification,
        
        [XmlEnum("LetterOfCredit")]
        LetterOfCredit,
        
        [XmlEnum("LimitedRecourse")]
        LimitedRecourse,
        
        [XmlEnum("LossParticipation")]
        LossParticipation,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PoolInsurance")]
        PoolInsurance,
        
        [XmlEnum("Recourse")]
        Recourse,
        
        [XmlEnum("SecondTierMI")]
        SecondTierMI,
        
        [XmlEnum("SubordinationAgreement")]
        SubordinationAgreement,
    }
}
