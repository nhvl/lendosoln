namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum InvestorReportingAdditionalChargeAssessedToPartyBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Borrower")]
        Borrower,
        
        [XmlEnum("Investor")]
        Investor,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Servicer")]
        Servicer,
    }
}
