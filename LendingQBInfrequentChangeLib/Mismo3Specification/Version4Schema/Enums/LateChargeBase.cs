namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum LateChargeBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("FlatDollarAmount")]
        FlatDollarAmount,
        
        [XmlEnum("NoLateCharges")]
        NoLateCharges,
        
        [XmlEnum("PercentageOfDelinquentInterest")]
        PercentageOfDelinquentInterest,
        
        [XmlEnum("PercentageOfNetPayment")]
        PercentageOfNetPayment,
        
        [XmlEnum("PercentageOfPrincipalBalance")]
        PercentageOfPrincipalBalance,
        
        [XmlEnum("PercentageOfTotalPayment")]
        PercentageOfTotalPayment,
        
        [XmlEnum("PercentOfPrincipalAndInterest")]
        PercentOfPrincipalAndInterest,
    }
}
