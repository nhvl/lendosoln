namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum LandscapingBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("HardScape")]
        HardScape,
        
        [XmlEnum("Natural")]
        Natural,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("ProfessionalPlantings")]
        ProfessionalPlantings,
        
        [XmlEnum("XeriScape")]
        XeriScape,
        
        [XmlEnum("ZeroImpact")]
        ZeroImpact,
    }
}
