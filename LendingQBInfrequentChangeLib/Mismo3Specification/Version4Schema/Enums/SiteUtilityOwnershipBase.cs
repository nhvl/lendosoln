namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum SiteUtilityOwnershipBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("NonPublic")]
        NonPublic,
        
        [XmlEnum("Public")]
        Public,
        
        [XmlEnum("Unknown")]
        Unknown,
    }
}
