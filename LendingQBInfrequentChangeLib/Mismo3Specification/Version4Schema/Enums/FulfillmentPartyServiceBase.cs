namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum FulfillmentPartyServiceBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Flood")]
        Flood,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Title")]
        Title,
    }
}
