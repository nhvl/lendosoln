namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum AVMMethodBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Assisted")]
        Assisted,
        
        [XmlEnum("HedonicModel")]
        HedonicModel,
        
        [XmlEnum("Hybrid")]
        Hybrid,
        
        [XmlEnum("Interactive")]
        Interactive,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("RepeatSalesModel")]
        RepeatSalesModel,
        
        [XmlEnum("Unknown")]
        Unknown,
    }
}
