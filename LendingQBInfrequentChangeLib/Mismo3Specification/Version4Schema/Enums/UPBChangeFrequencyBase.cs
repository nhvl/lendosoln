namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum UPBChangeFrequencyBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Annually")]
        Annually,
        
        [XmlEnum("Biweekly")]
        Biweekly,
        
        [XmlEnum("Daily")]
        Daily,
        
        [XmlEnum("Monthly")]
        Monthly,
        
        [XmlEnum("Quarterly")]
        Quarterly,
        
        [XmlEnum("Semiannually")]
        Semiannually,
        
        [XmlEnum("Semimonthly")]
        Semimonthly,
        
        [XmlEnum("Weekly")]
        Weekly,
    }
}
