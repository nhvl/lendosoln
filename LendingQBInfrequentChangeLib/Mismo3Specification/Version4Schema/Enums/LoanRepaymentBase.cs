namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum LoanRepaymentBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("ConstantPrincipal")]
        ConstantPrincipal,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PrincipalPaymentOption")]
        PrincipalPaymentOption,
        
        [XmlEnum("ScheduledAmortization")]
        ScheduledAmortization,
    }
}
