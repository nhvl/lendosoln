namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum TitleAgentValidationReasonBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AgentAddressInvalid")]
        AgentAddressInvalid,
        
        [XmlEnum("AgentIdentifierFormatInvalid")]
        AgentIdentifierFormatInvalid,
        
        [XmlEnum("AgentJurisdictionInvalid")]
        AgentJurisdictionInvalid,
        
        [XmlEnum("AmountExceedsAuthorizedLimit")]
        AmountExceedsAuthorizedLimit,
        
        [XmlEnum("Cancelled")]
        Cancelled,
        
        [XmlEnum("ClosingDateInvalid")]
        ClosingDateInvalid,
        
        [XmlEnum("NotActive")]
        NotActive,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("TitleUnderwriterInvalid")]
        TitleUnderwriterInvalid,
    }
}
