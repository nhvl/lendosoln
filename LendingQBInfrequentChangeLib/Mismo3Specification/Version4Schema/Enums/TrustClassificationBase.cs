namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum TrustClassificationBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("CommunityLandTrust")]
        CommunityLandTrust,
        
        [XmlEnum("IllinoisLandTrust")]
        IllinoisLandTrust,
        
        [XmlEnum("LandTrust")]
        LandTrust,
        
        [XmlEnum("LivingTrust")]
        LivingTrust,
        
        [XmlEnum("NativeAmericanLandTrust")]
        NativeAmericanLandTrust,
        
        [XmlEnum("Other")]
        Other,
    }
}
