namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum OfficeOfRecordationBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("ClerkOfCourts")]
        ClerkOfCourts,
        
        [XmlEnum("CountyClerk")]
        CountyClerk,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("RegisterOfDeeds")]
        RegisterOfDeeds,
    }
}
