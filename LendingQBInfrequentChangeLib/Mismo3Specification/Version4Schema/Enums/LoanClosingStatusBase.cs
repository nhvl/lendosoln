namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum LoanClosingStatusBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Closed")]
        Closed,
        
        [XmlEnum("TableFunded")]
        TableFunded,
    }
}
