namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum VendorSpecialtyBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Agricultural")]
        Agricultural,
        
        [XmlEnum("Bilingual")]
        Bilingual,
        
        [XmlEnum("ComplexProperties")]
        ComplexProperties,
        
        [XmlEnum("HistoricProperties")]
        HistoricProperties,
        
        [XmlEnum("LitigationExperience")]
        LitigationExperience,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("VacantLand")]
        VacantLand,
    }
}
