namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum DeedRestrictionBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("ModeratelyPricedDwellingUnit")]
        ModeratelyPricedDwellingUnit,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Servitude")]
        Servitude,
    }
}
