namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ConversionOptionPeriodFeeBalanceCalculationBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("CurrentBalanceAtConversion")]
        CurrentBalanceAtConversion,
        
        [XmlEnum("OriginalBalance")]
        OriginalBalance,
    }
}
