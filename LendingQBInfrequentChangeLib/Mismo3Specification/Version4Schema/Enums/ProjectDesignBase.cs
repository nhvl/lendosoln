namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ProjectDesignBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("GardenProject")]
        GardenProject,
        
        [XmlEnum("HighriseProject")]
        HighriseProject,
        
        [XmlEnum("MidriseProject")]
        MidriseProject,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("TownhouseRowhouse")]
        TownhouseRowhouse,
    }
}
