namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum BuildingPermitWorkPurposeBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Addition")]
        Addition,
        
        [XmlEnum("Demolition")]
        Demolition,
        
        [XmlEnum("NewConstruction")]
        NewConstruction,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Remodel")]
        Remodel,
        
        [XmlEnum("Repair")]
        Repair,
        
        [XmlEnum("Replace")]
        Replace,
        
        [XmlEnum("Unknown")]
        Unknown,
    }
}
