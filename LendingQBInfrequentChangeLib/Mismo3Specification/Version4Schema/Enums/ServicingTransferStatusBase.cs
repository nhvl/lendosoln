namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ServicingTransferStatusBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Released")]
        Released,
        
        [XmlEnum("Retained")]
        Retained,
    }
}
