namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PlatBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("CertifiedSurveyMap")]
        CertifiedSurveyMap,
        
        [XmlEnum("Condominium")]
        Condominium,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PlannedUnitDevelopment")]
        PlannedUnitDevelopment,
        
        [XmlEnum("Subdivision")]
        Subdivision,
        
        [XmlEnum("Timeshare")]
        Timeshare,
    }
}
