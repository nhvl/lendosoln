namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum VerificationRangeBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("MostRecentDays")]
        MostRecentDays,
        
        [XmlEnum("MostRecentMonths")]
        MostRecentMonths,
        
        [XmlEnum("MostRecentYear")]
        MostRecentYear,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PaymentPeriod")]
        PaymentPeriod,
        
        [XmlEnum("StatementPeriod")]
        StatementPeriod,
    }
}
