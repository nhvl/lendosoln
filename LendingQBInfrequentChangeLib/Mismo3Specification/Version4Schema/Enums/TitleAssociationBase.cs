namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum TitleAssociationBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("ALTA")]
        ALTA,
        
        [XmlEnum("CLTA")]
        CLTA,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("TLTA")]
        TLTA,
    }
}
