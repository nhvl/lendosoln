namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ConstructionPhaseInterestPaymentMethodBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AddToPermanentLoanAmount")]
        AddToPermanentLoanAmount,
        
        [XmlEnum("BilledToBorrower")]
        BilledToBorrower,
        
        [XmlEnum("Other")]
        Other,
    }
}
