namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum RoomFeatureBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("BathroomFloors")]
        BathroomFloors,
        
        [XmlEnum("BathroomWainscot")]
        BathroomWainscot,
        
        [XmlEnum("Doors")]
        Doors,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("TrimAndFinish")]
        TrimAndFinish,
    }
}
