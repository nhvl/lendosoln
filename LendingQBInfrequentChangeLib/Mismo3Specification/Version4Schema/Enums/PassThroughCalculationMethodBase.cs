namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PassThroughCalculationMethodBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("BottomUp")]
        BottomUp,
        
        [XmlEnum("TopDown")]
        TopDown,
    }
}
