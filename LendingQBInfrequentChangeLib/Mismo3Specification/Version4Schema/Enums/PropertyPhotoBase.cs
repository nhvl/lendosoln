namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PropertyPhotoBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AreaOfConcern")]
        AreaOfConcern,
        
        [XmlEnum("CompletedRepair")]
        CompletedRepair,
        
        [XmlEnum("Exterior")]
        Exterior,
        
        [XmlEnum("Interior")]
        Interior,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("StreetView")]
        StreetView,
    }
}
