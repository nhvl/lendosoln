namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum MIPremiumCalculationBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AverageAnnualScheduledUnpaidPrincipalBalance")]
        AverageAnnualScheduledUnpaidPrincipalBalance,
        
        [XmlEnum("BaseLoanAmount")]
        BaseLoanAmount,
        
        [XmlEnum("Constant")]
        Constant,
        
        [XmlEnum("Declining")]
        Declining,
        
        [XmlEnum("NoRenewals")]
        NoRenewals,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PercentOfCurrentBalance")]
        PercentOfCurrentBalance,
        
        [XmlEnum("PercentOfOriginalBalance")]
        PercentOfOriginalBalance,
    }
}
