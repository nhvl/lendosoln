namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum CreditScoreExclusionReasonBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("InvalidScoreRequest")]
        InvalidScoreRequest,
        
        [XmlEnum("NotScoredCreditDataNotAvailable")]
        NotScoredCreditDataNotAvailable,
        
        [XmlEnum("NotScoredFileCannotBeScored")]
        NotScoredFileCannotBeScored,
        
        [XmlEnum("NotScoredFileIsUnderReview")]
        NotScoredFileIsUnderReview,
        
        [XmlEnum("NotScoredFileTooLong")]
        NotScoredFileTooLong,
        
        [XmlEnum("NotScoredInsufficientCredit")]
        NotScoredInsufficientCredit,
        
        [XmlEnum("NotScoredNoQualifyingAccount")]
        NotScoredNoQualifyingAccount,
        
        [XmlEnum("NotScoredNoRecentAccountInformation")]
        NotScoredNoRecentAccountInformation,
        
        [XmlEnum("NotScoredRequirementsNotMet")]
        NotScoredRequirementsNotMet,
        
        [XmlEnum("NotScoredSubjectDeceased")]
        NotScoredSubjectDeceased,
        
        [XmlEnum("ScoringNotAvailable")]
        ScoringNotAvailable,
        
        [XmlEnum("UnauthorizedScoreRequest")]
        UnauthorizedScoreRequest,
    }
}
