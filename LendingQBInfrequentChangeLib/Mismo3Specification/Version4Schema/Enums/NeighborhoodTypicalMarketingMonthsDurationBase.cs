namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum NeighborhoodTypicalMarketingMonthsDurationBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("OverSixMonths")]
        OverSixMonths,
        
        [XmlEnum("ThreeToSixMonths")]
        ThreeToSixMonths,
        
        [XmlEnum("UnderThreeMonths")]
        UnderThreeMonths,
    }
}
