namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum BankruptcyChapterBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("ChapterEleven")]
        ChapterEleven,
        
        [XmlEnum("ChapterFifteen")]
        ChapterFifteen,
        
        [XmlEnum("ChapterSeven")]
        ChapterSeven,
        
        [XmlEnum("ChapterThirteen")]
        ChapterThirteen,
        
        [XmlEnum("ChapterTwelve")]
        ChapterTwelve,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Unknown")]
        Unknown,
    }
}
