namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum SalesConcessionBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Automobile")]
        Automobile,
        
        [XmlEnum("ClosingCosts")]
        ClosingCosts,
        
        [XmlEnum("Downpayment")]
        Downpayment,
        
        [XmlEnum("InteriorDecorationAllowance")]
        InteriorDecorationAllowance,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Prepaids")]
        Prepaids,
        
        [XmlEnum("Repairs")]
        Repairs,
        
        [XmlEnum("ThirdPartyFinancingDiscounts")]
        ThirdPartyFinancingDiscounts,
        
        [XmlEnum("Unknown")]
        Unknown,
    }
}
