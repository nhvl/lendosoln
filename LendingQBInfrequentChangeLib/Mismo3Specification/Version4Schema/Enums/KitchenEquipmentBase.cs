namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum KitchenEquipmentBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Compactor")]
        Compactor,
        
        [XmlEnum("Cooktop")]
        Cooktop,
        
        [XmlEnum("Dishwasher")]
        Dishwasher,
        
        [XmlEnum("Disposal")]
        Disposal,
        
        [XmlEnum("FanHood")]
        FanHood,
        
        [XmlEnum("Microwave")]
        Microwave,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("RangeOven")]
        RangeOven,
        
        [XmlEnum("Refrigerator")]
        Refrigerator,
        
        [XmlEnum("SolidSurfaceManmadeCountertop")]
        SolidSurfaceManmadeCountertop,
        
        [XmlEnum("SolidSurfaceNaturalCountertop")]
        SolidSurfaceNaturalCountertop,
        
        [XmlEnum("WasherDryer")]
        WasherDryer,
        
        [XmlEnum("WaterEfficientFaucet")]
        WaterEfficientFaucet,
    }
}
