namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PaymentDelinquencyStatusBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("MoreThan120Days")]
        MoreThan120Days,
        
        [XmlEnum("MoreThan30Days")]
        MoreThan30Days,
        
        [XmlEnum("MoreThan60Days")]
        MoreThan60Days,
        
        [XmlEnum("MoreThan90Days")]
        MoreThan90Days,
        
        [XmlEnum("NotDelinquent")]
        NotDelinquent,
    }
}
