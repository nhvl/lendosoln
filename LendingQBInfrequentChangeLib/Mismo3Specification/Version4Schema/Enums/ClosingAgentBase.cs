namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ClosingAgentBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Attorney")]
        Attorney,
        
        [XmlEnum("EscrowCompany")]
        EscrowCompany,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("TitleCompany")]
        TitleCompany,
    }
}
