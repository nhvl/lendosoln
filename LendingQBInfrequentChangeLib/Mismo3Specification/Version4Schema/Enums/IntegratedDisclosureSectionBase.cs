namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum IntegratedDisclosureSectionBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("DueFromBorrowerAtClosing")]
        DueFromBorrowerAtClosing,
        
        [XmlEnum("DueFromSellerAtClosing")]
        DueFromSellerAtClosing,
        
        [XmlEnum("DueToSellerAtClosing")]
        DueToSellerAtClosing,
        
        [XmlEnum("InitialEscrowPaymentAtClosing")]
        InitialEscrowPaymentAtClosing,
        
        [XmlEnum("OriginationCharges")]
        OriginationCharges,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("OtherCosts")]
        OtherCosts,
        
        [XmlEnum("PaidAlreadyByOrOnBehalfOfBorrowerAtClosing")]
        PaidAlreadyByOrOnBehalfOfBorrowerAtClosing,
        
        [XmlEnum("PayoffsAndPayments")]
        PayoffsAndPayments,
        
        [XmlEnum("Prepaids")]
        Prepaids,
        
        [XmlEnum("ServicesBorrowerDidNotShopFor")]
        ServicesBorrowerDidNotShopFor,
        
        [XmlEnum("ServicesBorrowerDidShopFor")]
        ServicesBorrowerDidShopFor,
        
        [XmlEnum("ServicesYouCannotShopFor")]
        ServicesYouCannotShopFor,
        
        [XmlEnum("ServicesYouCanShopFor")]
        ServicesYouCanShopFor,
        
        [XmlEnum("TaxesAndOtherGovernmentFees")]
        TaxesAndOtherGovernmentFees,
        
        [XmlEnum("TotalClosingCosts")]
        TotalClosingCosts,
        
        [XmlEnum("TotalLoanCosts")]
        TotalLoanCosts,
        
        [XmlEnum("TotalOtherCosts")]
        TotalOtherCosts,
    }
}
