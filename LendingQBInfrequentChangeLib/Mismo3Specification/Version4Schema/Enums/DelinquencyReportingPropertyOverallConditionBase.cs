namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum DelinquencyReportingPropertyOverallConditionBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Condemned")]
        Condemned,
        
        [XmlEnum("Excellent")]
        Excellent,
        
        [XmlEnum("Fair")]
        Fair,
        
        [XmlEnum("Good")]
        Good,
        
        [XmlEnum("NoPropertyInspection")]
        NoPropertyInspection,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Poor")]
        Poor,
        
        [XmlEnum("PropertyInaccessible")]
        PropertyInaccessible,
        
        [XmlEnum("RemovedOrDestroyed")]
        RemovedOrDestroyed,
        
        [XmlEnum("Unknown")]
        Unknown,
    }
}
