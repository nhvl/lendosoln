namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum StructureStateBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Complete")]
        Complete,
        
        [XmlEnum("Incomplete")]
        Incomplete,
        
        [XmlEnum("NotStarted")]
        NotStarted,
    }
}
