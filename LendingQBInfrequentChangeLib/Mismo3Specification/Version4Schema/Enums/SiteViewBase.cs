namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum SiteViewBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("CityStreet")]
        CityStreet,
        
        [XmlEnum("GolfCourse")]
        GolfCourse,
        
        [XmlEnum("Industrial")]
        Industrial,
        
        [XmlEnum("LimitedSight")]
        LimitedSight,
        
        [XmlEnum("MountainView")]
        MountainView,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Park")]
        Park,
        
        [XmlEnum("Pastoral")]
        Pastoral,
        
        [XmlEnum("PowerLines")]
        PowerLines,
        
        [XmlEnum("Residential")]
        Residential,
        
        [XmlEnum("Skyline")]
        Skyline,
        
        [XmlEnum("Traffic")]
        Traffic,
        
        [XmlEnum("WaterView")]
        WaterView,
        
        [XmlEnum("Woods")]
        Woods,
    }
}
