namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum IndexRoundingTimingBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("RoundIndexAfterCheckingMinimumIndexMovement")]
        RoundIndexAfterCheckingMinimumIndexMovement,
        
        [XmlEnum("RoundIndexBeforeCheckingMinimumIndexMovement")]
        RoundIndexBeforeCheckingMinimumIndexMovement,
    }
}
