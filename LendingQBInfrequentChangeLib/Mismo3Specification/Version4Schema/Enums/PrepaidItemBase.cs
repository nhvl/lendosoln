namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PrepaidItemBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("BoroughPropertyTax")]
        BoroughPropertyTax,
        
        [XmlEnum("CityPropertyTax")]
        CityPropertyTax,
        
        [XmlEnum("CondominiumAssociationDues")]
        CondominiumAssociationDues,
        
        [XmlEnum("CondominiumAssociationSpecialAssessment")]
        CondominiumAssociationSpecialAssessment,
        
        [XmlEnum("CooperativeAssociationDues")]
        CooperativeAssociationDues,
        
        [XmlEnum("CooperativeAssociationSpecialAssessment")]
        CooperativeAssociationSpecialAssessment,
        
        [XmlEnum("CountyPropertyTax")]
        CountyPropertyTax,
        
        [XmlEnum("DistrictPropertyTax")]
        DistrictPropertyTax,
        
        [XmlEnum("EarthquakeInsurancePremium")]
        EarthquakeInsurancePremium,
        
        [XmlEnum("FloodInsurancePremium")]
        FloodInsurancePremium,
        
        [XmlEnum("HailInsurancePremium")]
        HailInsurancePremium,
        
        [XmlEnum("HazardInsurancePremium")]
        HazardInsurancePremium,
        
        [XmlEnum("HomeownersAssociationDues")]
        HomeownersAssociationDues,
        
        [XmlEnum("HomeownersAssociationSpecialAssessment")]
        HomeownersAssociationSpecialAssessment,
        
        [XmlEnum("HomeownersInsurancePremium")]
        HomeownersInsurancePremium,
        
        [XmlEnum("MortgageInsurancePremium")]
        MortgageInsurancePremium,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PrepaidInterest")]
        PrepaidInterest,
        
        [XmlEnum("StatePropertyTax")]
        StatePropertyTax,
        
        [XmlEnum("TownPropertyTax")]
        TownPropertyTax,
        
        [XmlEnum("USDARuralDevelopmentGuaranteeFee")]
        USDARuralDevelopmentGuaranteeFee,
        
        [XmlEnum("VAFundingFee")]
        VAFundingFee,
        
        [XmlEnum("VolcanoInsurancePremium")]
        VolcanoInsurancePremium,
        
        [XmlEnum("WindAndStormInsurancePremium")]
        WindAndStormInsurancePremium,
    }
}
