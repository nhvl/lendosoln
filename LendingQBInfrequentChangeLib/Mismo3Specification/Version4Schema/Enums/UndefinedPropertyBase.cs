namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum UndefinedPropertyBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("NonDescriptive")]
        NonDescriptive,
        
        [XmlEnum("Unavailable")]
        Unavailable,
        
        [XmlEnum("Unknown")]
        Unknown,
    }
}
