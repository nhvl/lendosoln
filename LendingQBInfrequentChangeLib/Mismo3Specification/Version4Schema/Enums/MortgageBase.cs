namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum MortgageBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Conventional")]
        Conventional,
        
        [XmlEnum("FHA")]
        FHA,
        
        [XmlEnum("LocalAgency")]
        LocalAgency,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PublicAndIndianHousing")]
        PublicAndIndianHousing,
        
        [XmlEnum("StateAgency")]
        StateAgency,
        
        [XmlEnum("USDARuralDevelopment")]
        USDARuralDevelopment,
        
        [XmlEnum("VA")]
        VA,
    }
}
