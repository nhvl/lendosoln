namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum CounselingTopicBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("FairHousingPrePurchaseEducationWorkshops")]
        FairHousingPrePurchaseEducationWorkshops,
        
        [XmlEnum("FinancialBudgetingAndCreditRepairWorkshops")]
        FinancialBudgetingAndCreditRepairWorkshops,
        
        [XmlEnum("FinancialManagementBudgetCounseling")]
        FinancialManagementBudgetCounseling,
        
        [XmlEnum("HomeImprovementAndRehabilitationCounseling")]
        HomeImprovementAndRehabilitationCounseling,
        
        [XmlEnum("LossMitigation")]
        LossMitigation,
        
        [XmlEnum("MarketingAndOutreachInitiatives")]
        MarketingAndOutreachInitiatives,
        
        [XmlEnum("MobilityAndRelocationCounseling")]
        MobilityAndRelocationCounseling,
        
        [XmlEnum("MortgageDelinquencyAndDefaultResolutionCounseling")]
        MortgageDelinquencyAndDefaultResolutionCounseling,
        
        [XmlEnum("NonDelinquencyAndPostPurchaseWorkshops")]
        NonDelinquencyAndPostPurchaseWorkshops,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PredatoryLendingEducationWorkshops")]
        PredatoryLendingEducationWorkshops,
        
        [XmlEnum("PrePurchaseCounseling")]
        PrePurchaseCounseling,
        
        [XmlEnum("PrePurchaseHomebuyerEducationWorkshops")]
        PrePurchaseHomebuyerEducationWorkshops,
        
        [XmlEnum("RentalHousingCounseling")]
        RentalHousingCounseling,
        
        [XmlEnum("RentalHousingWorkshops")]
        RentalHousingWorkshops,
        
        [XmlEnum("ResolvingPreventingMortgageDelinquencyWorkshop")]
        ResolvingPreventingMortgageDelinquencyWorkshop,
        
        [XmlEnum("ReverseMortgageCounseling")]
        ReverseMortgageCounseling,
        
        [XmlEnum("ServicesForHomelessCounseling")]
        ServicesForHomelessCounseling,
    }
}
