namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum CoolingSystemBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Centralized")]
        Centralized,
        
        [XmlEnum("Individual")]
        Individual,
        
        [XmlEnum("Other")]
        Other,
    }
}
