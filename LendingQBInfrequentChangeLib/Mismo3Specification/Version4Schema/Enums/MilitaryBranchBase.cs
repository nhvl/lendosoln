namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum MilitaryBranchBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AirForce")]
        AirForce,
        
        [XmlEnum("AirNationalGuard")]
        AirNationalGuard,
        
        [XmlEnum("Army")]
        Army,
        
        [XmlEnum("ArmyNationalGuard")]
        ArmyNationalGuard,
        
        [XmlEnum("ArmyReserves")]
        ArmyReserves,
        
        [XmlEnum("CoastGuard")]
        CoastGuard,
        
        [XmlEnum("Marines")]
        Marines,
        
        [XmlEnum("MarinesReserves")]
        MarinesReserves,
        
        [XmlEnum("Navy")]
        Navy,
        
        [XmlEnum("NavyReserves")]
        NavyReserves,
        
        [XmlEnum("Other")]
        Other,
    }
}
