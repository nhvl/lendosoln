namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum IncomeDocumentBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("IRSW2")]
        IRSW2,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PayStub")]
        PayStub,
        
        [XmlEnum("RentalIncomeAnalysisStatement")]
        RentalIncomeAnalysisStatement,
        
        [XmlEnum("RetirementAccountStatement")]
        RetirementAccountStatement,
        
        [XmlEnum("TaxReturn")]
        TaxReturn,
        
        [XmlEnum("ThirdPartyEmploymentStatement")]
        ThirdPartyEmploymentStatement,
        
        [XmlEnum("VerbalStatement")]
        VerbalStatement,
        
        [XmlEnum("VerificationOfIncome")]
        VerificationOfIncome,
    }
}
