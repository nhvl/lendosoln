namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ProjectAnalysisCompetitiveProjectComparisonBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Average")]
        Average,
        
        [XmlEnum("High")]
        High,
        
        [XmlEnum("Low")]
        Low,
    }
}
