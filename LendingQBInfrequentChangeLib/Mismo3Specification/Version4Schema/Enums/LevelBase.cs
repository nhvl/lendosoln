namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum LevelBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Attic")]
        Attic,
        
        [XmlEnum("Basement")]
        Basement,
        
        [XmlEnum("LevelFive")]
        LevelFive,
        
        [XmlEnum("LevelFour")]
        LevelFour,
        
        [XmlEnum("LevelOne")]
        LevelOne,
        
        [XmlEnum("LevelThree")]
        LevelThree,
        
        [XmlEnum("LevelTwo")]
        LevelTwo,
        
        [XmlEnum("Other")]
        Other,
    }
}
