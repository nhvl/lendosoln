namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum LoanConsiderationDisclosureStatementBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Appraisal")]
        Appraisal,
        
        [XmlEnum("Assumption")]
        Assumption,
        
        [XmlEnum("Construction")]
        Construction,
        
        [XmlEnum("ContractDetails")]
        ContractDetails,
        
        [XmlEnum("DemandFeature")]
        DemandFeature,
        
        [XmlEnum("EscrowAccountCurrent")]
        EscrowAccountCurrent,
        
        [XmlEnum("EscrowAccountFuture")]
        EscrowAccountFuture,
        
        [XmlEnum("HomeownersInsurance")]
        HomeownersInsurance,
        
        [XmlEnum("LatePayment")]
        LatePayment,
        
        [XmlEnum("LiabilityAfterForeclosure")]
        LiabilityAfterForeclosure,
        
        [XmlEnum("LoanAcceptance")]
        LoanAcceptance,
        
        [XmlEnum("NegativeAmortization")]
        NegativeAmortization,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PartialPayment")]
        PartialPayment,
        
        [XmlEnum("Refinance")]
        Refinance,
        
        [XmlEnum("SecurityInterest")]
        SecurityInterest,
        
        [XmlEnum("Servicing")]
        Servicing,
        
        [XmlEnum("TaxDeductions")]
        TaxDeductions,
    }
}
