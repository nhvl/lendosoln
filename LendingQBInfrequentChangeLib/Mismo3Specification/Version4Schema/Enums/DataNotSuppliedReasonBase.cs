namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum DataNotSuppliedReasonBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("NotCollected")]
        NotCollected,
        
        [XmlEnum("NotReceived")]
        NotReceived,
        
        [XmlEnum("NotRelevant")]
        NotRelevant,
        
        [XmlEnum("Omitted")]
        Omitted,
        
        [XmlEnum("Other")]
        Other,
    }
}
