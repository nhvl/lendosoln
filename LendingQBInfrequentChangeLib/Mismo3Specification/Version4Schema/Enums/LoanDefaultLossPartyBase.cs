namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum LoanDefaultLossPartyBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Investor")]
        Investor,
        
        [XmlEnum("Lender")]
        Lender,
        
        [XmlEnum("LoanSeller")]
        LoanSeller,
        
        [XmlEnum("MortgageInsuranceCompany")]
        MortgageInsuranceCompany,
        
        [XmlEnum("Servicer")]
        Servicer,
        
        [XmlEnum("Shared")]
        Shared,
    }
}
