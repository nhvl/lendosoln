namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum IssueBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AdditionalErrorIdentified")]
        AdditionalErrorIdentified,
        
        [XmlEnum("AssertionOfDissatisfaction")]
        AssertionOfDissatisfaction,
        
        [XmlEnum("AssertionOfError")]
        AssertionOfError,
        
        [XmlEnum("InformationRequest")]
        InformationRequest,
        
        [XmlEnum("MaintenanceRequest")]
        MaintenanceRequest,
        
        [XmlEnum("Notification")]
        Notification,
        
        [XmlEnum("Other")]
        Other,
    }
}
