namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ConstructionPhaseInterestPaymentFrequencyBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Biweekly")]
        Biweekly,
        
        [XmlEnum("Monthly")]
        Monthly,
        
        [XmlEnum("Other")]
        Other,
    }
}
