namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum MI_LTVCutoffBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AppraisedValue")]
        AppraisedValue,
        
        [XmlEnum("SalesPrice")]
        SalesPrice,
    }
}
