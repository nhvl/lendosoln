namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum RateLockBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("BestEfforts")]
        BestEfforts,
        
        [XmlEnum("Mandatory")]
        Mandatory,
    }
}
