namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum DealSetIncomeBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("TransferFee")]
        TransferFee,
    }
}
