namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PropertyValuationStateBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Original")]
        Original,
        
        [XmlEnum("Subsequent")]
        Subsequent,
    }
}
