namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum RemittanceRecordBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("ClosingProtectionLetterFee")]
        ClosingProtectionLetterFee,
        
        [XmlEnum("Endorsement")]
        Endorsement,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PolicyFee")]
        PolicyFee,
        
        [XmlEnum("PolicyPremium")]
        PolicyPremium,
        
        [XmlEnum("Tax")]
        Tax,
    }
}
