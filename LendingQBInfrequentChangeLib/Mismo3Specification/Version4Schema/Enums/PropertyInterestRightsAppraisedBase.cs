namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PropertyInterestRightsAppraisedBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Easement")]
        Easement,
        
        [XmlEnum("Encroachment")]
        Encroachment,
        
        [XmlEnum("FeeSimpleEstate")]
        FeeSimpleEstate,
        
        [XmlEnum("LeaseFeeEstate")]
        LeaseFeeEstate,
        
        [XmlEnum("LeaseholdEstate")]
        LeaseholdEstate,
        
        [XmlEnum("License")]
        License,
        
        [XmlEnum("LifeEstate")]
        LifeEstate,
        
        [XmlEnum("Other")]
        Other,
    }
}
