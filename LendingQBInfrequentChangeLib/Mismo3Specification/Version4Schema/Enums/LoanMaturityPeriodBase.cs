namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum LoanMaturityPeriodBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Biweekly")]
        Biweekly,
        
        [XmlEnum("Day")]
        Day,
        
        [XmlEnum("Month")]
        Month,
        
        [XmlEnum("Quarter")]
        Quarter,
        
        [XmlEnum("Semimonthly")]
        Semimonthly,
        
        [XmlEnum("Week")]
        Week,
        
        [XmlEnum("Year")]
        Year,
    }
}
