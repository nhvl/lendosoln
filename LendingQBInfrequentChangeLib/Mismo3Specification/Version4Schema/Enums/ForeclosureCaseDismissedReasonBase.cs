namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ForeclosureCaseDismissedReasonBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("CourtOrdered")]
        CourtOrdered,
        
        [XmlEnum("LossMitigationCompleted")]
        LossMitigationCompleted,
        
        [XmlEnum("LossMitigationInReview")]
        LossMitigationInReview,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Reinstatement")]
        Reinstatement,
        
        [XmlEnum("Unknown")]
        Unknown,
    }
}
