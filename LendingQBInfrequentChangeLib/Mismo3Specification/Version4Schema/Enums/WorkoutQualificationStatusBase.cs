namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum WorkoutQualificationStatusBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("ConditionallyQualified")]
        ConditionallyQualified,
        
        [XmlEnum("NotQualified")]
        NotQualified,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PreQualified")]
        PreQualified,
        
        [XmlEnum("Qualified")]
        Qualified,
    }
}
