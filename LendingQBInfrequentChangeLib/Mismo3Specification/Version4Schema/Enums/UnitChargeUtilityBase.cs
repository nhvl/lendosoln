namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum UnitChargeUtilityBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Cable")]
        Cable,
        
        [XmlEnum("Cooling")]
        Cooling,
        
        [XmlEnum("Electricity")]
        Electricity,
        
        [XmlEnum("Garbage")]
        Garbage,
        
        [XmlEnum("Gas")]
        Gas,
        
        [XmlEnum("Heating")]
        Heating,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Sewer")]
        Sewer,
        
        [XmlEnum("Water")]
        Water,
    }
}
