namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum CreditCommentBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("BureauRemarks")]
        BureauRemarks,
        
        [XmlEnum("ConsumerStatement")]
        ConsumerStatement,
        
        [XmlEnum("Instruction")]
        Instruction,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PublicRecordText")]
        PublicRecordText,
        
        [XmlEnum("StatusCode")]
        StatusCode,
        
        [XmlEnum("Warning")]
        Warning,
    }
}
