namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum IndexRoundingBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Nearest")]
        Nearest,
        
        [XmlEnum("NoRounding")]
        NoRounding,
        
        [XmlEnum("Up")]
        Up,
    }
}
