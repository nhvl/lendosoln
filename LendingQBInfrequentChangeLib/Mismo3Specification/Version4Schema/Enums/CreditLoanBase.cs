namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum CreditLoanBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Agriculture")]
        Agriculture,
        
        [XmlEnum("Airplane")]
        Airplane,
        
        [XmlEnum("ApplianceOrFurniture")]
        ApplianceOrFurniture,
        
        [XmlEnum("AttorneyFees")]
        AttorneyFees,
        
        [XmlEnum("AutoLease")]
        AutoLease,
        
        [XmlEnum("AutoLoanEquityTransfer")]
        AutoLoanEquityTransfer,
        
        [XmlEnum("Automobile")]
        Automobile,
        
        [XmlEnum("AutoRefinance")]
        AutoRefinance,
        
        [XmlEnum("BiMonthlyMortgageTermsInYears")]
        BiMonthlyMortgageTermsInYears,
        
        [XmlEnum("Boat")]
        Boat,
        
        [XmlEnum("Business")]
        Business,
        
        [XmlEnum("BusinessCreditCard")]
        BusinessCreditCard,
        
        [XmlEnum("Camper")]
        Camper,
        
        [XmlEnum("ChargeAccount")]
        ChargeAccount,
        
        [XmlEnum("CheckCreditOrLineOfCredit")]
        CheckCreditOrLineOfCredit,
        
        [XmlEnum("ChildSupport")]
        ChildSupport,
        
        [XmlEnum("Collection")]
        Collection,
        
        [XmlEnum("CollectionAttorney")]
        CollectionAttorney,
        
        [XmlEnum("Comaker")]
        Comaker,
        
        [XmlEnum("CombinedCreditPlan")]
        CombinedCreditPlan,
        
        [XmlEnum("CommercialCreditObligation")]
        CommercialCreditObligation,
        
        [XmlEnum("CommercialLineOfCredit")]
        CommercialLineOfCredit,
        
        [XmlEnum("CommercialMortgage")]
        CommercialMortgage,
        
        [XmlEnum("ConditionalSalesContract")]
        ConditionalSalesContract,
        
        [XmlEnum("ConditionalSalesContractRefinance")]
        ConditionalSalesContractRefinance,
        
        [XmlEnum("Consolidation")]
        Consolidation,
        
        [XmlEnum("ConstructionLoan")]
        ConstructionLoan,
        
        [XmlEnum("ConventionalRealEstateMortgage")]
        ConventionalRealEstateMortgage,
        
        [XmlEnum("CreditCard")]
        CreditCard,
        
        [XmlEnum("CreditLineSecured")]
        CreditLineSecured,
        
        [XmlEnum("DebitCard")]
        DebitCard,
        
        [XmlEnum("DebtCounselingService")]
        DebtCounselingService,
        
        [XmlEnum("DeferredStudentLoan")]
        DeferredStudentLoan,
        
        [XmlEnum("DepositRelated")]
        DepositRelated,
        
        [XmlEnum("Educational")]
        Educational,
        
        [XmlEnum("Employment")]
        Employment,
        
        [XmlEnum("FactoringCompanyAccount")]
        FactoringCompanyAccount,
        
        [XmlEnum("FamilySupport")]
        FamilySupport,
        
        [XmlEnum("FarmersHomeAdministrationFHMA")]
        FarmersHomeAdministrationFHMA,
        
        [XmlEnum("FederalConsolidatedLoan")]
        FederalConsolidatedLoan,
        
        [XmlEnum("FHAComakerNotBorrower")]
        FHAComakerNotBorrower,
        
        [XmlEnum("FHAHomeImprovement")]
        FHAHomeImprovement,
        
        [XmlEnum("FHARealEstateMortgage")]
        FHARealEstateMortgage,
        
        [XmlEnum("FinanceStatement")]
        FinanceStatement,
        
        [XmlEnum("GovernmentBenefit")]
        GovernmentBenefit,
        
        [XmlEnum("GovernmentEmployeeAdvance")]
        GovernmentEmployeeAdvance,
        
        [XmlEnum("GovernmentFeeForService")]
        GovernmentFeeForService,
        
        [XmlEnum("GovernmentFine")]
        GovernmentFine,
        
        [XmlEnum("GovernmentGrant")]
        GovernmentGrant,
        
        [XmlEnum("GovernmentMiscellaneousDebt")]
        GovernmentMiscellaneousDebt,
        
        [XmlEnum("GovernmentOverpayment")]
        GovernmentOverpayment,
        
        [XmlEnum("GovernmentSecuredDirectLoan")]
        GovernmentSecuredDirectLoan,
        
        [XmlEnum("GovernmentSecuredGuaranteeLoan")]
        GovernmentSecuredGuaranteeLoan,
        
        [XmlEnum("GovernmentUnsecuredDirectLoan")]
        GovernmentUnsecuredDirectLoan,
        
        [XmlEnum("GovernmentUnsecuredGuaranteeLoan")]
        GovernmentUnsecuredGuaranteeLoan,
        
        [XmlEnum("HomeEquity")]
        HomeEquity,
        
        [XmlEnum("HomeEquityLineOfCredit")]
        HomeEquityLineOfCredit,
        
        [XmlEnum("HomeImprovement")]
        HomeImprovement,
        
        [XmlEnum("HouseholdGoods")]
        HouseholdGoods,
        
        [XmlEnum("HouseholdGoodsAndOtherCollateralAuto")]
        HouseholdGoodsAndOtherCollateralAuto,
        
        [XmlEnum("HouseholdGoodsSecured")]
        HouseholdGoodsSecured,
        
        [XmlEnum("InstallmentLoan")]
        InstallmentLoan,
        
        [XmlEnum("InstallmentSalesContract")]
        InstallmentSalesContract,
        
        [XmlEnum("InsuranceClaims")]
        InsuranceClaims,
        
        [XmlEnum("Lease")]
        Lease,
        
        [XmlEnum("LenderPlacedInsurance")]
        LenderPlacedInsurance,
        
        [XmlEnum("ManualMortgage")]
        ManualMortgage,
        
        [XmlEnum("ManufacturedHome")]
        ManufacturedHome,
        
        [XmlEnum("MedicalDebt")]
        MedicalDebt,
        
        [XmlEnum("MobileHome")]
        MobileHome,
        
        [XmlEnum("MobilePhone")]
        MobilePhone,
        
        [XmlEnum("Mortgage")]
        Mortgage,
        
        [XmlEnum("NoteLoan")]
        NoteLoan,
        
        [XmlEnum("NoteLoanWithComaker")]
        NoteLoanWithComaker,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PartiallySecured")]
        PartiallySecured,
        
        [XmlEnum("PersonalLoan")]
        PersonalLoan,
        
        [XmlEnum("RealEstateJuniorLiens")]
        RealEstateJuniorLiens,
        
        [XmlEnum("RealEstateLoanEquityTransfer")]
        RealEstateLoanEquityTransfer,
        
        [XmlEnum("RealEstateMortgageWithoutOtherCollateral")]
        RealEstateMortgageWithoutOtherCollateral,
        
        [XmlEnum("RealEstateSpecificTypeUnknown")]
        RealEstateSpecificTypeUnknown,
        
        [XmlEnum("Recreational")]
        Recreational,
        
        [XmlEnum("RecreationalVehicle")]
        RecreationalVehicle,
        
        [XmlEnum("Refinance")]
        Refinance,
        
        [XmlEnum("RefundAnticipationLoan")]
        RefundAnticipationLoan,
        
        [XmlEnum("RentalAgreement")]
        RentalAgreement,
        
        [XmlEnum("ResidentialLoan")]
        ResidentialLoan,
        
        [XmlEnum("ReturnedCheck")]
        ReturnedCheck,
        
        [XmlEnum("RevolvingBusinessLines")]
        RevolvingBusinessLines,
        
        [XmlEnum("SecondMortgage")]
        SecondMortgage,
        
        [XmlEnum("Secured")]
        Secured,
        
        [XmlEnum("SecuredByCosigner")]
        SecuredByCosigner,
        
        [XmlEnum("SecuredCreditCard")]
        SecuredCreditCard,
        
        [XmlEnum("SecuredHomeImprovement")]
        SecuredHomeImprovement,
        
        [XmlEnum("SemiMonthlyMortgagePayment")]
        SemiMonthlyMortgagePayment,
        
        [XmlEnum("SinglePaymentLoan")]
        SinglePaymentLoan,
        
        [XmlEnum("SpouseSupport")]
        SpouseSupport,
        
        [XmlEnum("SummaryOfAccountsWithSameStatus")]
        SummaryOfAccountsWithSameStatus,
        
        [XmlEnum("TimeSharedLoan")]
        TimeSharedLoan,
        
        [XmlEnum("Title1Loan")]
        Title1Loan,
        
        [XmlEnum("UnknownLoanType")]
        UnknownLoanType,
        
        [XmlEnum("Unsecured")]
        Unsecured,
        
        [XmlEnum("UtilityCompany")]
        UtilityCompany,
        
        [XmlEnum("VeteransAdministrationLoan")]
        VeteransAdministrationLoan,
        
        [XmlEnum("VeteransAdministrationRealEstateMortgage")]
        VeteransAdministrationRealEstateMortgage,
    }
}
