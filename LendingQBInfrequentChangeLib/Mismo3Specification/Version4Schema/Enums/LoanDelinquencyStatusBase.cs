namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum LoanDelinquencyStatusBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AssignmentPursued")]
        AssignmentPursued,
        
        [XmlEnum("Assumption")]
        Assumption,
        
        [XmlEnum("ChapterElevenBankruptcy")]
        ChapterElevenBankruptcy,
        
        [XmlEnum("ChapterSevenBankruptcy")]
        ChapterSevenBankruptcy,
        
        [XmlEnum("ChapterThirteenBankruptcy")]
        ChapterThirteenBankruptcy,
        
        [XmlEnum("ChapterTwelveBankruptcy")]
        ChapterTwelveBankruptcy,
        
        [XmlEnum("ChargeOffPursued")]
        ChargeOffPursued,
        
        [XmlEnum("ContestedForeclosure")]
        ContestedForeclosure,
        
        [XmlEnum("Current")]
        Current,
        
        [XmlEnum("DelinquentNoAction")]
        DelinquentNoAction,
        
        [XmlEnum("DrugSeizure")]
        DrugSeizure,
        
        [XmlEnum("Forbearance")]
        Forbearance,
        
        [XmlEnum("Foreclosure")]
        Foreclosure,
        
        [XmlEnum("GovernmentSeizure")]
        GovernmentSeizure,
        
        [XmlEnum("MediationReferral")]
        MediationReferral,
        
        [XmlEnum("MilitaryIndulgence")]
        MilitaryIndulgence,
        
        [XmlEnum("Modification")]
        Modification,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PreforeclosureSalePlanned")]
        PreforeclosureSalePlanned,
        
        [XmlEnum("Probate")]
        Probate,
        
        [XmlEnum("Refinance")]
        Refinance,
        
        [XmlEnum("RepaymentPlan")]
        RepaymentPlan,
        
        [XmlEnum("SecondLienConsiderations")]
        SecondLienConsiderations,
        
        [XmlEnum("ShortSalePlanning")]
        ShortSalePlanning,
        
        [XmlEnum("TrialModificationActive")]
        TrialModificationActive,
        
        [XmlEnum("VeteransAffairsBuydown")]
        VeteransAffairsBuydown,
        
        [XmlEnum("VeteransAffairsNoBids")]
        VeteransAffairsNoBids,
        
        [XmlEnum("VeteransAffairsRefundPursued")]
        VeteransAffairsRefundPursued,
        
        [XmlEnum("Workout")]
        Workout,
    }
}
