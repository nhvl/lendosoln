namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum HeatingSystemBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("ForcedWarmAir")]
        ForcedWarmAir,
        
        [XmlEnum("HotWaterBaseboard")]
        HotWaterBaseboard,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Radiant")]
        Radiant,
    }
}
