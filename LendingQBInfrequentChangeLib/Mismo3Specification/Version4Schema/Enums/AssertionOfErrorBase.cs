namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum AssertionOfErrorBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("DisputedFee")]
        DisputedFee,
        
        [XmlEnum("FailureToAcceptPayment")]
        FailureToAcceptPayment,
        
        [XmlEnum("FailureToApplyPayment")]
        FailureToApplyPayment,
        
        [XmlEnum("FailureToCreditPayment")]
        FailureToCreditPayment,
        
        [XmlEnum("FailureToIdentifyOwnerAssignee")]
        FailureToIdentifyOwnerAssignee,
        
        [XmlEnum("FailureToPayAgreedEscrows")]
        FailureToPayAgreedEscrows,
        
        [XmlEnum("FailureToProperlyHandleBorrowerEstate")]
        FailureToProperlyHandleBorrowerEstate,
        
        [XmlEnum("FailureToProvideAccurateLossMitigationOptions")]
        FailureToProvideAccurateLossMitigationOptions,
        
        [XmlEnum("FailureToProvideAccuratePayoffStatement")]
        FailureToProvideAccuratePayoffStatement,
        
        [XmlEnum("FailureToProvideAccurateServicingRecord")]
        FailureToProvideAccurateServicingRecord,
        
        [XmlEnum("FilingErrorOfFirstForeclosureNotice")]
        FilingErrorOfFirstForeclosureNotice,
        
        [XmlEnum("ForeclosureSaleInViolation")]
        ForeclosureSaleInViolation,
        
        [XmlEnum("None")]
        None,
        
        [XmlEnum("Other")]
        Other,
    }
}
