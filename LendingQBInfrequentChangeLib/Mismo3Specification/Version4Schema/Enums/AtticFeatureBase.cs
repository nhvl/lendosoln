namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum AtticFeatureBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("DropStair")]
        DropStair,
        
        [XmlEnum("Finished")]
        Finished,
        
        [XmlEnum("Floor")]
        Floor,
        
        [XmlEnum("Heated")]
        Heated,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Scuttle")]
        Scuttle,
        
        [XmlEnum("Stairs")]
        Stairs,
    }
}
