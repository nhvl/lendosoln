namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum JointAssetLiabilityReportingBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Jointly")]
        Jointly,
        
        [XmlEnum("NotJointly")]
        NotJointly,
    }
}
