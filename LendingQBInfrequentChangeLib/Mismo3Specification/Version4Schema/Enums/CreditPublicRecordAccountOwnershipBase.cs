namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum CreditPublicRecordAccountOwnershipBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Individual")]
        Individual,
        
        [XmlEnum("Joint")]
        Joint,
        
        [XmlEnum("Terminated")]
        Terminated,
    }
}
