namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum AntiSteeringComparisonBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("LowestInterestRate")]
        LowestInterestRate,
        
        [XmlEnum("LowestInterestRateWithoutCertainFeatures")]
        LowestInterestRateWithoutCertainFeatures,
        
        [XmlEnum("LowestTotalAmountForOriginationPointsOrFeesAndDiscountPoints")]
        LowestTotalAmountForOriginationPointsOrFeesAndDiscountPoints,
        
        [XmlEnum("NotApplicable")]
        NotApplicable,
        
        [XmlEnum("Other")]
        Other,
    }
}
