namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum MICertificateBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("BothPrimaryAndPool")]
        BothPrimaryAndPool,
        
        [XmlEnum("NoCoverage")]
        NoCoverage,
        
        [XmlEnum("Pool")]
        Pool,
        
        [XmlEnum("Primary")]
        Primary,
    }
}
