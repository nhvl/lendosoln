namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum WorkoutStatusBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AcceptedByBorrower")]
        AcceptedByBorrower,
        
        [XmlEnum("Approved")]
        Approved,
        
        [XmlEnum("Closed")]
        Closed,
        
        [XmlEnum("Completed")]
        Completed,
        
        [XmlEnum("DeclinedByBorrower")]
        DeclinedByBorrower,
        
        [XmlEnum("Denied")]
        Denied,
        
        [XmlEnum("Eligible")]
        Eligible,
        
        [XmlEnum("Failed")]
        Failed,
        
        [XmlEnum("Initiated")]
        Initiated,
        
        [XmlEnum("InTrial")]
        InTrial,
        
        [XmlEnum("OfferedToBorrower")]
        OfferedToBorrower,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("UnderReview")]
        UnderReview,
        
        [XmlEnum("Unknown")]
        Unknown,
    }
}
