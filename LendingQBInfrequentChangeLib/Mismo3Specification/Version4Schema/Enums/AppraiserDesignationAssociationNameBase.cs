namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum AppraiserDesignationAssociationNameBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AmericanSocietyOfAppraisers")]
        AmericanSocietyOfAppraisers,
        
        [XmlEnum("AmericanSocietyOfFarmManagersAndRuralAppraisers")]
        AmericanSocietyOfFarmManagersAndRuralAppraisers,
        
        [XmlEnum("AppraisalInstitute")]
        AppraisalInstitute,
        
        [XmlEnum("NationalAssociationIndependentFeeAppraisers")]
        NationalAssociationIndependentFeeAppraisers,
        
        [XmlEnum("NationalAssociationOfRealtors")]
        NationalAssociationOfRealtors,
        
        [XmlEnum("RoyalInstituteOfCharteredSurveyors")]
        RoyalInstituteOfCharteredSurveyors,
    }
}
