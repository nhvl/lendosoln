namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum CreditEnhancementEffectivePeriodBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("FixedPeriod")]
        FixedPeriod,
        
        [XmlEnum("LifeOfLoan")]
        LifeOfLoan,
    }
}
