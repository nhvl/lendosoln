namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum GFECreditOrChargeForChosenInterestRateBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("BorrowerCharge")]
        BorrowerCharge,
        
        [XmlEnum("BorrowerCredit")]
        BorrowerCredit,
        
        [XmlEnum("CreditOrChargeIncludedInOriginationCharge")]
        CreditOrChargeIncludedInOriginationCharge,
        
        [XmlEnum("Other")]
        Other,
    }
}
