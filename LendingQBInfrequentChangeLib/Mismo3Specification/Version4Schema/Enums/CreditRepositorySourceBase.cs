namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum CreditRepositorySourceBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Equifax")]
        Equifax,
        
        [XmlEnum("Experian")]
        Experian,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("TransUnion")]
        TransUnion,
        
        [XmlEnum("Unspecified")]
        Unspecified,
    }
}
