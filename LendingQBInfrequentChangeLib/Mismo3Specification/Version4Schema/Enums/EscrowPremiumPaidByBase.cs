namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum EscrowPremiumPaidByBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Buyer")]
        Buyer,
        
        [XmlEnum("Lender")]
        Lender,
        
        [XmlEnum("Seller")]
        Seller,
    }
}
