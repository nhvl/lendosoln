namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ServitudeBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Condition")]
        Condition,
        
        [XmlEnum("Covenant")]
        Covenant,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PrivatelyCreated")]
        PrivatelyCreated,
        
        [XmlEnum("Restriction")]
        Restriction,
    }
}
