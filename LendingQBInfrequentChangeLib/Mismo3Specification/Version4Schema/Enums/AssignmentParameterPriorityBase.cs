namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum AssignmentParameterPriorityBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("ClientSpecificOnly")]
        ClientSpecificOnly,
        
        [XmlEnum("Combined")]
        Combined,
        
        [XmlEnum("MutuallyExclusive")]
        MutuallyExclusive,
        
        [XmlEnum("Override")]
        Override,
        
        [XmlEnum("SequentiallyApplied")]
        SequentiallyApplied,
    }
}
