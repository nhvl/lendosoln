namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PoolAccrualRateStructureBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("StatedStructure")]
        StatedStructure,
        
        [XmlEnum("WeightedAverageStructure")]
        WeightedAverageStructure,
    }
}
