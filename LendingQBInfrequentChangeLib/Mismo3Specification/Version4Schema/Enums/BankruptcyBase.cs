namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum BankruptcyBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Business")]
        Business,
        
        [XmlEnum("Personal")]
        Personal,
    }
}
