namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum LienRecommendationBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("BuyOut")]
        BuyOut,
        
        [XmlEnum("ChargeOffSubordinate")]
        ChargeOffSubordinate,
        
        [XmlEnum("NoRecommendation")]
        NoRecommendation,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Reinstate")]
        Reinstate,
    }
}
