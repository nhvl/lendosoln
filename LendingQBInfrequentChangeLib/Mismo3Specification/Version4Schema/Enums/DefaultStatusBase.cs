namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum DefaultStatusBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Bankruptcy")]
        Bankruptcy,
        
        [XmlEnum("DefaultImminent")]
        DefaultImminent,
        
        [XmlEnum("Forbearance")]
        Forbearance,
        
        [XmlEnum("Foreclosure")]
        Foreclosure,
        
        [XmlEnum("InDefault")]
        InDefault,
        
        [XmlEnum("Other")]
        Other,
    }
}
