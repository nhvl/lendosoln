namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum LoanRoleBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("HistoricalLoan")]
        HistoricalLoan,
        
        [XmlEnum("RelatedLoan")]
        RelatedLoan,
        
        [XmlEnum("SubjectLoan")]
        SubjectLoan,
    }
}
