namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum AliasBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AlsoKnownAs")]
        AlsoKnownAs,
        
        [XmlEnum("FormerlyKnownAs")]
        FormerlyKnownAs,
        
        [XmlEnum("NowKnownAs")]
        NowKnownAs,
        
        [XmlEnum("Other")]
        Other,
    }
}
