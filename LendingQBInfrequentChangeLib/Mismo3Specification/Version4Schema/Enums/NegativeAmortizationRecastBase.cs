namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum NegativeAmortizationRecastBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("BothScheduledAndMaximumPercent")]
        BothScheduledAndMaximumPercent,
        
        [XmlEnum("MaximumPercent")]
        MaximumPercent,
        
        [XmlEnum("Scheduled")]
        Scheduled,
    }
}
