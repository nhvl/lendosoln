namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum NeighborhoodPropertyValueTrendBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Declining")]
        Declining,
        
        [XmlEnum("Increasing")]
        Increasing,
        
        [XmlEnum("Stable")]
        Stable,
    }
}
