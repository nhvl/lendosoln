namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum CreditLiabilityAccountOwnershipBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AuthorizedUser")]
        AuthorizedUser,
        
        [XmlEnum("Comaker")]
        Comaker,
        
        [XmlEnum("Deceased")]
        Deceased,
        
        [XmlEnum("Individual")]
        Individual,
        
        [XmlEnum("JointContractualLiability")]
        JointContractualLiability,
        
        [XmlEnum("JointParticipating")]
        JointParticipating,
        
        [XmlEnum("Maker")]
        Maker,
        
        [XmlEnum("OnBehalfOf")]
        OnBehalfOf,
        
        [XmlEnum("Terminated")]
        Terminated,
        
        [XmlEnum("Undesignated")]
        Undesignated,
    }
}
