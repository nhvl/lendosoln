namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum CostOfAnalysisComponentStatusBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Completed")]
        Completed,
        
        [XmlEnum("Proposed")]
        Proposed,
    }
}
