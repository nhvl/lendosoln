namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ComponentStatusTimeframeBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("ElevenToFifteenYears")]
        ElevenToFifteenYears,
        
        [XmlEnum("LessThanOneYear")]
        LessThanOneYear,
        
        [XmlEnum("OneToFiveYears")]
        OneToFiveYears,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("SixToTenYears")]
        SixToTenYears,
        
        [XmlEnum("Unknown")]
        Unknown,
    }
}
