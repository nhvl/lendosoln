namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PartialPaymentApplicationMethodBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("ApplyPartialPayment")]
        ApplyPartialPayment,
        
        [XmlEnum("HoldUntilCompleteAmount")]
        HoldUntilCompleteAmount,
        
        [XmlEnum("Other")]
        Other,
    }
}
