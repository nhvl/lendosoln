namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum WindowBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Aluminum")]
        Aluminum,
        
        [XmlEnum("Awning")]
        Awning,
        
        [XmlEnum("BayWindow")]
        BayWindow,
        
        [XmlEnum("BeveledGlass")]
        BeveledGlass,
        
        [XmlEnum("BowWindow")]
        BowWindow,
        
        [XmlEnum("Casement")]
        Casement,
        
        [XmlEnum("CenterPivot")]
        CenterPivot,
        
        [XmlEnum("CircleHead")]
        CircleHead,
        
        [XmlEnum("Clerestory")]
        Clerestory,
        
        [XmlEnum("Combination")]
        Combination,
        
        [XmlEnum("CrankOut")]
        CrankOut,
        
        [XmlEnum("Decorative")]
        Decorative,
        
        [XmlEnum("Dormer")]
        Dormer,
        
        [XmlEnum("DoubleHung")]
        DoubleHung,
        
        [XmlEnum("DoublePane")]
        DoublePane,
        
        [XmlEnum("EtchedGlass")]
        EtchedGlass,
        
        [XmlEnum("FixedPane")]
        FixedPane,
        
        [XmlEnum("French")]
        French,
        
        [XmlEnum("Hopper")]
        Hopper,
        
        [XmlEnum("HorizontalSliding")]
        HorizontalSliding,
        
        [XmlEnum("Insulated")]
        Insulated,
        
        [XmlEnum("Jalousie")]
        Jalousie,
        
        [XmlEnum("LeadedGlass")]
        LeadedGlass,
        
        [XmlEnum("LowE")]
        LowE,
        
        [XmlEnum("Oriel")]
        Oriel,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Palladian")]
        Palladian,
        
        [XmlEnum("Picture")]
        Picture,
        
        [XmlEnum("SingleHung")]
        SingleHung,
        
        [XmlEnum("Sliding")]
        Sliding,
        
        [XmlEnum("StainedGlass")]
        StainedGlass,
        
        [XmlEnum("Steel")]
        Steel,
        
        [XmlEnum("ThermalPane")]
        ThermalPane,
        
        [XmlEnum("Thermopane")]
        Thermopane,
        
        [XmlEnum("Transom")]
        Transom,
        
        [XmlEnum("Transverse")]
        Transverse,
        
        [XmlEnum("Vinyl")]
        Vinyl,
        
        [XmlEnum("Wood")]
        Wood,
    }
}
