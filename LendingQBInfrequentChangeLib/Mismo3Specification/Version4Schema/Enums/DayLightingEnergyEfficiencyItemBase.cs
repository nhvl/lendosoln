namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum DayLightingEnergyEfficiencyItemBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Skylights")]
        Skylights,
        
        [XmlEnum("SolarTubes")]
        SolarTubes,
    }
}
