namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ConstructionLoanBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("ConstructionOnly")]
        ConstructionOnly,
        
        [XmlEnum("ConstructionToPermanent")]
        ConstructionToPermanent,
    }
}
