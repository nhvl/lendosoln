namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum LoanBuyupBuydownBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Buydown")]
        Buydown,
        
        [XmlEnum("Buyup")]
        Buyup,
        
        [XmlEnum("BuyupBuydownDoesNotApply")]
        BuyupBuydownDoesNotApply,
        
        [XmlEnum("Other")]
        Other,
    }
}
