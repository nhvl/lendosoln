namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum WorkoutUnsuccessfulReasonBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("BankruptcyCourtDeclined")]
        BankruptcyCourtDeclined,
        
        [XmlEnum("BorrowerFailedToRespondToSolicitation")]
        BorrowerFailedToRespondToSolicitation,
        
        [XmlEnum("DefaultNotImminent")]
        DefaultNotImminent,
        
        [XmlEnum("ExcessiveForbearance")]
        ExcessiveForbearance,
        
        [XmlEnum("FederallyDeclaredDisasterArea")]
        FederallyDeclaredDisasterArea,
        
        [XmlEnum("IncomeDocumentationNotProvided")]
        IncomeDocumentationNotProvided,
        
        [XmlEnum("IncomeMisrepresented")]
        IncomeMisrepresented,
        
        [XmlEnum("IneligibleBorrower")]
        IneligibleBorrower,
        
        [XmlEnum("IneligibleMortgage")]
        IneligibleMortgage,
        
        [XmlEnum("IneligibleProperty")]
        IneligibleProperty,
        
        [XmlEnum("InvestorGuarantorNotParticipating")]
        InvestorGuarantorNotParticipating,
        
        [XmlEnum("LoanPaidOffOrReinstated")]
        LoanPaidOffOrReinstated,
        
        [XmlEnum("NegativeNPV")]
        NegativeNPV,
        
        [XmlEnum("OfferNotAcceptedByBorrower")]
        OfferNotAcceptedByBorrower,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PreviousOfficialHAMPModificationLastTwelveMonths")]
        PreviousOfficialHAMPModificationLastTwelveMonths,
        
        [XmlEnum("PropertyNotOwnerOccupied")]
        PropertyNotOwnerOccupied,
        
        [XmlEnum("RequestWithdrawnByBorrower")]
        RequestWithdrawnByBorrower,
        
        [XmlEnum("TrialPlanDefault")]
        TrialPlanDefault,
        
        [XmlEnum("UnemploymentForbearanceProgram")]
        UnemploymentForbearanceProgram,
        
        [XmlEnum("WorkoutRequestIncomplete")]
        WorkoutRequestIncomplete,
    }
}
