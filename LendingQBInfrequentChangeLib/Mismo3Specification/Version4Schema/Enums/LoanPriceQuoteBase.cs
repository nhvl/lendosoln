namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum LoanPriceQuoteBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("BuySide")]
        BuySide,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("SellSide")]
        SellSide,
    }
}
