namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum RentalFeatureBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Age")]
        Age,
        
        [XmlEnum("Condition")]
        Condition,
        
        [XmlEnum("Lease")]
        Lease,
        
        [XmlEnum("Location")]
        Location,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("UtilitiesIncluded")]
        UtilitiesIncluded,
    }
}
