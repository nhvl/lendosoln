namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum BasementFeatureBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("FloorDrain")]
        FloorDrain,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("OutsideEntry")]
        OutsideEntry,
        
        [XmlEnum("SumpPump")]
        SumpPump,
    }
}
