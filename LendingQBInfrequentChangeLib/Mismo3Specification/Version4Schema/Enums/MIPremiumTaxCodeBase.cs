namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum MIPremiumTaxCodeBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AllTaxes")]
        AllTaxes,
        
        [XmlEnum("County")]
        County,
        
        [XmlEnum("Municipal")]
        Municipal,
        
        [XmlEnum("State")]
        State,
    }
}
