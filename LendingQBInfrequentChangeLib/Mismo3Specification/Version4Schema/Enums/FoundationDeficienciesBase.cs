namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum FoundationDeficienciesBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Dampness")]
        Dampness,
        
        [XmlEnum("Infestation")]
        Infestation,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Settlement")]
        Settlement,
    }
}
