namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ForeclosureExpenseBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AttorneyFees")]
        AttorneyFees,
        
        [XmlEnum("Eviction")]
        Eviction,
        
        [XmlEnum("LegalFees")]
        LegalFees,
        
        [XmlEnum("Other")]
        Other,
    }
}
