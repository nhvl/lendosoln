namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum CreditResponseAlertMessageCodeSourceBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("EquifaxFraudVictimAlert")]
        EquifaxFraudVictimAlert,
        
        [XmlEnum("EquifaxHitNoHitDesignator")]
        EquifaxHitNoHitDesignator,
        
        [XmlEnum("EquifaxOFAC")]
        EquifaxOFAC,
        
        [XmlEnum("EquifaxSafescan")]
        EquifaxSafescan,
        
        [XmlEnum("ExperianFraudServicesIndicator")]
        ExperianFraudServicesIndicator,
        
        [XmlEnum("ExperianMessage")]
        ExperianMessage,
        
        [XmlEnum("ExperianStatementType")]
        ExperianStatementType,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("TransUnionAuthorizedUser")]
        TransUnionAuthorizedUser,
        
        [XmlEnum("TransUnionFileHit")]
        TransUnionFileHit,
        
        [XmlEnum("TransUnionFraudVictimAlert")]
        TransUnionFraudVictimAlert,
        
        [XmlEnum("TransUnionHighRiskFraudAlert")]
        TransUnionHighRiskFraudAlert,
        
        [XmlEnum("TransUnionIdentifierMismatchAlert")]
        TransUnionIdentifierMismatchAlert,
        
        [XmlEnum("TransUnionOFAC")]
        TransUnionOFAC,
        
        [XmlEnum("TransUnionSuppressionIndicator")]
        TransUnionSuppressionIndicator,
    }
}
