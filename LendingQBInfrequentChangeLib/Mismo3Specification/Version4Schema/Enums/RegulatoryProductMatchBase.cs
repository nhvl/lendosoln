namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum RegulatoryProductMatchBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Address")]
        Address,
        
        [XmlEnum("City")]
        City,
        
        [XmlEnum("FirstName")]
        FirstName,
        
        [XmlEnum("LastName")]
        LastName,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PostalCode")]
        PostalCode,
        
        [XmlEnum("State")]
        State,
    }
}
