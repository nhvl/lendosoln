namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ForeclosureProcedureTerminatedReasonBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("ActiveMilitaryIndulgence")]
        ActiveMilitaryIndulgence,
        
        [XmlEnum("Bankruptcy")]
        Bankruptcy,
        
        [XmlEnum("ChargedOff")]
        ChargedOff,
        
        [XmlEnum("CourtOrdered")]
        CourtOrdered,
        
        [XmlEnum("FundsApplied")]
        FundsApplied,
        
        [XmlEnum("HazardClaimOrPropertyDamage")]
        HazardClaimOrPropertyDamage,
        
        [XmlEnum("LossMitigation")]
        LossMitigation,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PaidInFull")]
        PaidInFull,
        
        [XmlEnum("Redeemed")]
        Redeemed,
        
        [XmlEnum("Reinstated")]
        Reinstated,
        
        [XmlEnum("ServicingTransfer")]
        ServicingTransfer,
        
        [XmlEnum("StartedInError")]
        StartedInError,
    }
}
