namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum CreditPublicRecordPaymentFrequencyBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Biweekly")]
        Biweekly,
        
        [XmlEnum("LumpSum")]
        LumpSum,
        
        [XmlEnum("Monthly")]
        Monthly,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Unknown")]
        Unknown,
        
        [XmlEnum("Weekly")]
        Weekly,
        
        [XmlEnum("Yearly")]
        Yearly,
    }
}
