namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum DetailCreditBusinessBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AdvertisingAgencies")]
        AdvertisingAgencies,
        
        [XmlEnum("AdvertisingMiscellaneous")]
        AdvertisingMiscellaneous,
        
        [XmlEnum("AdvertisingNewsMedia")]
        AdvertisingNewsMedia,
        
        [XmlEnum("AutomotiveAuctionsWholesale")]
        AutomotiveAuctionsWholesale,
        
        [XmlEnum("AutomotiveDealersNew")]
        AutomotiveDealersNew,
        
        [XmlEnum("AutomotiveDealersUsed")]
        AutomotiveDealersUsed,
        
        [XmlEnum("AutomotiveFarmImplementDealers")]
        AutomotiveFarmImplementDealers,
        
        [XmlEnum("AutomotiveLeasing")]
        AutomotiveLeasing,
        
        [XmlEnum("AutomotiveMiscellaneous")]
        AutomotiveMiscellaneous,
        
        [XmlEnum("AutomotiveParts")]
        AutomotiveParts,
        
        [XmlEnum("AutomotiveRental")]
        AutomotiveRental,
        
        [XmlEnum("AutomotiveRepairBodyShops")]
        AutomotiveRepairBodyShops,
        
        [XmlEnum("AutomotiveServiceStations")]
        AutomotiveServiceStations,
        
        [XmlEnum("AutomotiveTBAStoresTireDealers")]
        AutomotiveTBAStoresTireDealers,
        
        [XmlEnum("AutomotiveTruckDealers")]
        AutomotiveTruckDealers,
        
        [XmlEnum("BankingAllBanks")]
        BankingAllBanks,
        
        [XmlEnum("BankingBankcard")]
        BankingBankcard,
        
        [XmlEnum("BankingCommercialIndustrial")]
        BankingCommercialIndustrial,
        
        [XmlEnum("BankingInstallmentLoansCreditLine")]
        BankingInstallmentLoansCreditLine,
        
        [XmlEnum("BankingMiscellaneous")]
        BankingMiscellaneous,
        
        [XmlEnum("BankingMortgageDepartment")]
        BankingMortgageDepartment,
        
        [XmlEnum("BankingSavingsBanks")]
        BankingSavingsBanks,
        
        [XmlEnum("ClothingGeneral")]
        ClothingGeneral,
        
        [XmlEnum("ClothingMiscellaneous")]
        ClothingMiscellaneous,
        
        [XmlEnum("ClothingSpecialtyShoeHatEtc")]
        ClothingSpecialtyShoeHatEtc,
        
        [XmlEnum("CollectionServicesACBOfA")]
        CollectionServicesACBOfA,
        
        [XmlEnum("CollectionServicesOther")]
        CollectionServicesOther,
        
        [XmlEnum("ContractorsGeneral")]
        ContractorsGeneral,
        
        [XmlEnum("ContractorsHomeImprovement")]
        ContractorsHomeImprovement,
        
        [XmlEnum("ContractorsMiscellaneous")]
        ContractorsMiscellaneous,
        
        [XmlEnum("ContractorsSubcontractors")]
        ContractorsSubcontractors,
        
        [XmlEnum("DepartmentAndMailOrderCompleteDepartmentStores")]
        DepartmentAndMailOrderCompleteDepartmentStores,
        
        [XmlEnum("DepartmentAndMailOrderMailOrderFirms")]
        DepartmentAndMailOrderMailOrderFirms,
        
        [XmlEnum("DepartmentAndMailOrderMiscellaneous")]
        DepartmentAndMailOrderMiscellaneous,
        
        [XmlEnum("DepartmentAndMailOrderVarietyStores")]
        DepartmentAndMailOrderVarietyStores,
        
        [XmlEnum("EmploymentServices")]
        EmploymentServices,
        
        [XmlEnum("FarmAndGardenSuppliesChemicalAndFertilizer")]
        FarmAndGardenSuppliesChemicalAndFertilizer,
        
        [XmlEnum("FarmAndGardenSuppliesFeedAndSeed")]
        FarmAndGardenSuppliesFeedAndSeed,
        
        [XmlEnum("FarmAndGardenSuppliesMiscellaneous")]
        FarmAndGardenSuppliesMiscellaneous,
        
        [XmlEnum("FarmAndGardenSuppliesNurseryAndLandscaping")]
        FarmAndGardenSuppliesNurseryAndLandscaping,
        
        [XmlEnum("FinanceAutomobile")]
        FinanceAutomobile,
        
        [XmlEnum("FinanceBailBonds")]
        FinanceBailBonds,
        
        [XmlEnum("FinanceBrokerageAndInvestmentFirms")]
        FinanceBrokerageAndInvestmentFirms,
        
        [XmlEnum("FinanceBulkPurchase")]
        FinanceBulkPurchase,
        
        [XmlEnum("FinanceCreditUnions")]
        FinanceCreditUnions,
        
        [XmlEnum("FinanceEducation")]
        FinanceEducation,
        
        [XmlEnum("FinanceFactoringCompanies")]
        FinanceFactoringCompanies,
        
        [XmlEnum("FinanceMiscellaneous")]
        FinanceMiscellaneous,
        
        [XmlEnum("FinanceMortgageBrokers")]
        FinanceMortgageBrokers,
        
        [XmlEnum("FinanceMortgageCompanies")]
        FinanceMortgageCompanies,
        
        [XmlEnum("FinanceMortgageReporters")]
        FinanceMortgageReporters,
        
        [XmlEnum("FinancePersonalLoanCompanies")]
        FinancePersonalLoanCompanies,
        
        [XmlEnum("FinanceSales")]
        FinanceSales,
        
        [XmlEnum("FinanceSavingsAndLoanAssociations")]
        FinanceSavingsAndLoanAssociations,
        
        [XmlEnum("GovernmentCityAndCounty")]
        GovernmentCityAndCounty,
        
        [XmlEnum("GovernmentFederal")]
        GovernmentFederal,
        
        [XmlEnum("GovernmentMiscellaneous")]
        GovernmentMiscellaneous,
        
        [XmlEnum("GovernmentState")]
        GovernmentState,
        
        [XmlEnum("GroceryDairies")]
        GroceryDairies,
        
        [XmlEnum("GroceryMiscellaneous")]
        GroceryMiscellaneous,
        
        [XmlEnum("GroceryNeighborhood")]
        GroceryNeighborhood,
        
        [XmlEnum("GrocerySupermarkets")]
        GrocerySupermarkets,
        
        [XmlEnum("HomeFurnishingApplianceSalesAndService")]
        HomeFurnishingApplianceSalesAndService,
        
        [XmlEnum("HomeFurnishingCarpetAndFloorCoverings")]
        HomeFurnishingCarpetAndFloorCoverings,
        
        [XmlEnum("HomeFurnishingElectronicsTelevisionAndRadio")]
        HomeFurnishingElectronicsTelevisionAndRadio,
        
        [XmlEnum("HomeFurnishingFurniture")]
        HomeFurnishingFurniture,
        
        [XmlEnum("HomeFurnishingFurnitureRental")]
        HomeFurnishingFurnitureRental,
        
        [XmlEnum("HomeFurnishingInteriorDecoratorsDesign")]
        HomeFurnishingInteriorDecoratorsDesign,
        
        [XmlEnum("HomeFurnishingMiscellaneous")]
        HomeFurnishingMiscellaneous,
        
        [XmlEnum("HomeFurnishingMusicAndRecords")]
        HomeFurnishingMusicAndRecords,
        
        [XmlEnum("InsuranceGeneral")]
        InsuranceGeneral,
        
        [XmlEnum("InsuranceLife")]
        InsuranceLife,
        
        [XmlEnum("InsuranceMiscellaneous")]
        InsuranceMiscellaneous,
        
        [XmlEnum("JewelryAndCameraCameras")]
        JewelryAndCameraCameras,
        
        [XmlEnum("JewelryAndCameraComputerSalesAndServices")]
        JewelryAndCameraComputerSalesAndServices,
        
        [XmlEnum("JewelryAndCameraJewelry")]
        JewelryAndCameraJewelry,
        
        [XmlEnum("JewelryAndCameraMiscellaneous")]
        JewelryAndCameraMiscellaneous,
        
        [XmlEnum("JewelryAndCameraVideoTapeRentalAndSales")]
        JewelryAndCameraVideoTapeRentalAndSales,
        
        [XmlEnum("LumberAndHardwareAirConditioningPlumbingElectrical")]
        LumberAndHardwareAirConditioningPlumbingElectrical,
        
        [XmlEnum("LumberAndHardwareFixtureAndCabinet")]
        LumberAndHardwareFixtureAndCabinet,
        
        [XmlEnum("LumberAndHardwareLumberYards")]
        LumberAndHardwareLumberYards,
        
        [XmlEnum("LumberAndHardwareMiscellaneous")]
        LumberAndHardwareMiscellaneous,
        
        [XmlEnum("LumberAndHardwarePaintGlassAndPaper")]
        LumberAndHardwarePaintGlassAndPaper,
        
        [XmlEnum("LumberAndHardwareStores")]
        LumberAndHardwareStores,
        
        [XmlEnum("MedicalAndHealthAnimalHospitals")]
        MedicalAndHealthAnimalHospitals,
        
        [XmlEnum("MedicalAndHealthChiropractors")]
        MedicalAndHealthChiropractors,
        
        [XmlEnum("MedicalAndHealthDentists")]
        MedicalAndHealthDentists,
        
        [XmlEnum("MedicalAndHealthDoctorsAndClinics")]
        MedicalAndHealthDoctorsAndClinics,
        
        [XmlEnum("MedicalAndHealthFuneralHomesAndCemeteries")]
        MedicalAndHealthFuneralHomesAndCemeteries,
        
        [XmlEnum("MedicalAndHealthHospitals")]
        MedicalAndHealthHospitals,
        
        [XmlEnum("MedicalAndHealthMedicalEquipment")]
        MedicalAndHealthMedicalEquipment,
        
        [XmlEnum("MedicalAndHealthMiscellaneous")]
        MedicalAndHealthMiscellaneous,
        
        [XmlEnum("MedicalAndHealthOptometristsAndOpticalStores")]
        MedicalAndHealthOptometristsAndOpticalStores,
        
        [XmlEnum("MedicalAndHealthOsteopaths")]
        MedicalAndHealthOsteopaths,
        
        [XmlEnum("MedicalAndHealthPharmaciesAndDrugs")]
        MedicalAndHealthPharmaciesAndDrugs,
        
        [XmlEnum("MedicalAndHealthVeterinarians")]
        MedicalAndHealthVeterinarians,
        
        [XmlEnum("MiscellaneousAuthenticationProducts")]
        MiscellaneousAuthenticationProducts,
        
        [XmlEnum("MiscellaneousCreditBureaus")]
        MiscellaneousCreditBureaus,
        
        [XmlEnum("MiscellaneousCreditReportBrokers")]
        MiscellaneousCreditReportBrokers,
        
        [XmlEnum("MiscellaneousOther")]
        MiscellaneousOther,
        
        [XmlEnum("MiscellaneousRetail")]
        MiscellaneousRetail,
        
        [XmlEnum("MiscellaneousServices")]
        MiscellaneousServices,
        
        [XmlEnum("MiscellaneousWholesale")]
        MiscellaneousWholesale,
        
        [XmlEnum("OilAndNationalCreditCardsAirlineTravelCards")]
        OilAndNationalCreditCardsAirlineTravelCards,
        
        [XmlEnum("OilAndNationalCreditCardsCreditCardCompanies")]
        OilAndNationalCreditCardsCreditCardCompanies,
        
        [XmlEnum("OilAndNationalCreditCardsMiscellaneous")]
        OilAndNationalCreditCardsMiscellaneous,
        
        [XmlEnum("OilAndNationalCreditCardsOilCompanies")]
        OilAndNationalCreditCardsOilCompanies,
        
        [XmlEnum("PersonalServicesNotMedicalAccountants")]
        PersonalServicesNotMedicalAccountants,
        
        [XmlEnum("PersonalServicesNotMedicalBarbersBeautyShops")]
        PersonalServicesNotMedicalBarbersBeautyShops,
        
        [XmlEnum("PersonalServicesNotMedicalCountryClubsRestaurants")]
        PersonalServicesNotMedicalCountryClubsRestaurants,
        
        [XmlEnum("PersonalServicesNotMedicalDetectiveInvestigativeServices")]
        PersonalServicesNotMedicalDetectiveInvestigativeServices,
        
        [XmlEnum("PersonalServicesNotMedicalDryCleaningLaundry")]
        PersonalServicesNotMedicalDryCleaningLaundry,
        
        [XmlEnum("PersonalServicesNotMedicalEngineering")]
        PersonalServicesNotMedicalEngineering,
        
        [XmlEnum("PersonalServicesNotMedicalEquipmentLeasing")]
        PersonalServicesNotMedicalEquipmentLeasing,
        
        [XmlEnum("PersonalServicesNotMedicalFlorists")]
        PersonalServicesNotMedicalFlorists,
        
        [XmlEnum("PersonalServicesNotMedicalHealthAndFitnessClubs")]
        PersonalServicesNotMedicalHealthAndFitnessClubs,
        
        [XmlEnum("PersonalServicesNotMedicalLegal")]
        PersonalServicesNotMedicalLegal,
        
        [XmlEnum("PersonalServicesNotMedicalMiscellaneous")]
        PersonalServicesNotMedicalMiscellaneous,
        
        [XmlEnum("PersonalServicesNotMedicalPestControl")]
        PersonalServicesNotMedicalPestControl,
        
        [XmlEnum("PersonalServicesNotMedicalPhotographers")]
        PersonalServicesNotMedicalPhotographers,
        
        [XmlEnum("RealEstateAndPublicAccommodationApartments")]
        RealEstateAndPublicAccommodationApartments,
        
        [XmlEnum("RealEstateAndPublicAccommodationHotels")]
        RealEstateAndPublicAccommodationHotels,
        
        [XmlEnum("RealEstateAndPublicAccommodationMiscellaneous")]
        RealEstateAndPublicAccommodationMiscellaneous,
        
        [XmlEnum("RealEstateAndPublicAccommodationMobileHomes")]
        RealEstateAndPublicAccommodationMobileHomes,
        
        [XmlEnum("RealEstateAndPublicAccommodationMotels")]
        RealEstateAndPublicAccommodationMotels,
        
        [XmlEnum("RealEstateAndPublicAccommodationOfficeLeasing")]
        RealEstateAndPublicAccommodationOfficeLeasing,
        
        [XmlEnum("RealEstateAndPublicAccommodationPropertyManagement")]
        RealEstateAndPublicAccommodationPropertyManagement,
        
        [XmlEnum("RealEstateAndPublicAccommodationRealEstateSales")]
        RealEstateAndPublicAccommodationRealEstateSales,
        
        [XmlEnum("SportingGoodsAircraftSalesAndService")]
        SportingGoodsAircraftSalesAndService,
        
        [XmlEnum("SportingGoodsBoatAndMarinas")]
        SportingGoodsBoatAndMarinas,
        
        [XmlEnum("SportingGoodsMiscellaneous")]
        SportingGoodsMiscellaneous,
        
        [XmlEnum("SportingGoodsMotorcycleAndBicycle")]
        SportingGoodsMotorcycleAndBicycle,
        
        [XmlEnum("SportingGoodsStores")]
        SportingGoodsStores,
        
        [XmlEnum("UtilitiesAndFuelCableSatelliteCompanies")]
        UtilitiesAndFuelCableSatelliteCompanies,
        
        [XmlEnum("UtilitiesAndFuelCoalAndWoodDealers")]
        UtilitiesAndFuelCoalAndWoodDealers,
        
        [XmlEnum("UtilitiesAndFuelElectricalLightAndPower")]
        UtilitiesAndFuelElectricalLightAndPower,
        
        [XmlEnum("UtilitiesAndFuelFuelOilDealers")]
        UtilitiesAndFuelFuelOilDealers,
        
        [XmlEnum("UtilitiesAndFuelGarbageAndRubbishDisposals")]
        UtilitiesAndFuelGarbageAndRubbishDisposals,
        
        [XmlEnum("UtilitiesAndFuelGasNaturalAndBottled")]
        UtilitiesAndFuelGasNaturalAndBottled,
        
        [XmlEnum("UtilitiesAndFuelMiscellaneous")]
        UtilitiesAndFuelMiscellaneous,
        
        [XmlEnum("UtilitiesAndFuelTelephoneCompanies")]
        UtilitiesAndFuelTelephoneCompanies,
        
        [XmlEnum("UtilitiesAndFuelWaterCompanies")]
        UtilitiesAndFuelWaterCompanies,
        
        [XmlEnum("WholesaleAutomotiveSupplies")]
        WholesaleAutomotiveSupplies,
        
        [XmlEnum("WholesaleBuildingAndHardwareSupply")]
        WholesaleBuildingAndHardwareSupply,
        
        [XmlEnum("WholesaleClothingAndDryGoods")]
        WholesaleClothingAndDryGoods,
        
        [XmlEnum("WholesaleCreditCardProcessing")]
        WholesaleCreditCardProcessing,
        
        [XmlEnum("WholesaleDrugsAndChemicals")]
        WholesaleDrugsAndChemicals,
        
        [XmlEnum("WholesaleGroceries")]
        WholesaleGroceries,
        
        [XmlEnum("WholesaleHomeFurnishings")]
        WholesaleHomeFurnishings,
        
        [XmlEnum("WholesaleMachineryAndEquipment")]
        WholesaleMachineryAndEquipment,
        
        [XmlEnum("WholesaleMiscellaneous")]
        WholesaleMiscellaneous,
    }
}
