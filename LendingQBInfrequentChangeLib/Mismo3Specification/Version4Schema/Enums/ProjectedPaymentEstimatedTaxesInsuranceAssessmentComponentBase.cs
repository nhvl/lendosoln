namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ProjectedPaymentEstimatedTaxesInsuranceAssessmentComponentBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("CondominiumAssociationDues")]
        CondominiumAssociationDues,
        
        [XmlEnum("CondominiumAssociationSpecialAssessment")]
        CondominiumAssociationSpecialAssessment,
        
        [XmlEnum("CooperativeAssociationDues")]
        CooperativeAssociationDues,
        
        [XmlEnum("CooperativeAssociationSpecialAssessment")]
        CooperativeAssociationSpecialAssessment,
        
        [XmlEnum("GroundRent")]
        GroundRent,
        
        [XmlEnum("HomeownersAssociationDues")]
        HomeownersAssociationDues,
        
        [XmlEnum("HomeownersAssociationSpecialAssessment")]
        HomeownersAssociationSpecialAssessment,
        
        [XmlEnum("HomeownersInsurance")]
        HomeownersInsurance,
        
        [XmlEnum("LeaseholdPayment")]
        LeaseholdPayment,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PropertyTaxes")]
        PropertyTaxes,
    }
}
