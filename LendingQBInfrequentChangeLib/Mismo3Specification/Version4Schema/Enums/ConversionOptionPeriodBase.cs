namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ConversionOptionPeriodBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("OnDemand")]
        OnDemand,
        
        [XmlEnum("OnDemandAtInterestRateChangeDates")]
        OnDemandAtInterestRateChangeDates,
        
        [XmlEnum("OnDemandMonthly")]
        OnDemandMonthly,
        
        [XmlEnum("Scheduled")]
        Scheduled,
    }
}
