namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PropertyOwnerStatusBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Current")]
        Current,
        
        [XmlEnum("Proposed")]
        Proposed,
    }
}
