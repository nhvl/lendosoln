namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum AdjustmentRuleBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("First")]
        First,
        
        [XmlEnum("Subsequent")]
        Subsequent,
    }
}
