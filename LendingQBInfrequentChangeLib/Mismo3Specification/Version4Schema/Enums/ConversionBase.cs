namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ConversionBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("ToBiweeklyPaymentFrequency")]
        ToBiweeklyPaymentFrequency,
        
        [XmlEnum("ToFixedRate")]
        ToFixedRate,
        
        [XmlEnum("ToFullyAmortizingTerm")]
        ToFullyAmortizingTerm,
        
        [XmlEnum("ToLevelPayment")]
        ToLevelPayment,
        
        [XmlEnum("ToLowerInterestRate")]
        ToLowerInterestRate,
        
        [XmlEnum("ToMonthlyPaymentFrequency")]
        ToMonthlyPaymentFrequency,
    }
}
