namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum TitleProcessInsuranceBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Insurance")]
        Insurance,
        
        [XmlEnum("LimitedInsurance")]
        LimitedInsurance,
        
        [XmlEnum("NonInsurance")]
        NonInsurance,
    }
}
