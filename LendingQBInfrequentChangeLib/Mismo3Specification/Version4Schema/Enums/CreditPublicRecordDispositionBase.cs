namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum CreditPublicRecordDispositionBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Adjudicated")]
        Adjudicated,
        
        [XmlEnum("Appealed")]
        Appealed,
        
        [XmlEnum("Canceled")]
        Canceled,
        
        [XmlEnum("Completed")]
        Completed,
        
        [XmlEnum("Converted")]
        Converted,
        
        [XmlEnum("Discharged")]
        Discharged,
        
        [XmlEnum("Dismissed")]
        Dismissed,
        
        [XmlEnum("Distributed")]
        Distributed,
        
        [XmlEnum("Filed")]
        Filed,
        
        [XmlEnum("Granted")]
        Granted,
        
        [XmlEnum("InvoluntarilyDischarged")]
        InvoluntarilyDischarged,
        
        [XmlEnum("Nonadjudicated")]
        Nonadjudicated,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Paid")]
        Paid,
        
        [XmlEnum("PaidNotSatisfied")]
        PaidNotSatisfied,
        
        [XmlEnum("Pending")]
        Pending,
        
        [XmlEnum("RealEstateSold")]
        RealEstateSold,
        
        [XmlEnum("Released")]
        Released,
        
        [XmlEnum("Rescinded")]
        Rescinded,
        
        [XmlEnum("Satisfied")]
        Satisfied,
        
        [XmlEnum("Settled")]
        Settled,
        
        [XmlEnum("Unknown")]
        Unknown,
        
        [XmlEnum("Unreleased")]
        Unreleased,
        
        [XmlEnum("Unsatisfied")]
        Unsatisfied,
        
        [XmlEnum("Vacated")]
        Vacated,
        
        [XmlEnum("VoluntarilyDischarged")]
        VoluntarilyDischarged,
        
        [XmlEnum("Withdrawn")]
        Withdrawn,
    }
}
