namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ReversePaymentPlanBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("LineOfCredit")]
        LineOfCredit,
        
        [XmlEnum("ModifiedTenure")]
        ModifiedTenure,
        
        [XmlEnum("ModifiedTerm")]
        ModifiedTerm,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Tenure")]
        Tenure,
        
        [XmlEnum("Term")]
        Term,
    }
}
