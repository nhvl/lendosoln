namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum VerificationStatusBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("NotVerified")]
        NotVerified,
        
        [XmlEnum("ToBeVerified")]
        ToBeVerified,
        
        [XmlEnum("Verified")]
        Verified,
    }
}
