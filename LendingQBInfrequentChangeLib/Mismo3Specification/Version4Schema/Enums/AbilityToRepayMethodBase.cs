namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum AbilityToRepayMethodBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Exempt")]
        Exempt,
        
        [XmlEnum("General")]
        General,
        
        [XmlEnum("QualifiedMortgage")]
        QualifiedMortgage,
    }
}
