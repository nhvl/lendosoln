namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum EscrowPaymentFrequencyBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Annual")]
        Annual,
        
        [XmlEnum("Monthly")]
        Monthly,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Quarterly")]
        Quarterly,
        
        [XmlEnum("SemiAnnual")]
        SemiAnnual,
        
        [XmlEnum("Unequal")]
        Unequal,
    }
}
