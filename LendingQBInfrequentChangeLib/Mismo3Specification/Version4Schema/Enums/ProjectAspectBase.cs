namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ProjectAspectBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AmenityAndRecreationalFacilities")]
        AmenityAndRecreationalFacilities,
        
        [XmlEnum("AppealToMarket")]
        AppealToMarket,
        
        [XmlEnum("ConditionOfExterior")]
        ConditionOfExterior,
        
        [XmlEnum("ConditionOfInterior")]
        ConditionOfInterior,
        
        [XmlEnum("Density")]
        Density,
        
        [XmlEnum("GeneralAppearance")]
        GeneralAppearance,
        
        [XmlEnum("Location")]
        Location,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("QualityOfConstruction")]
        QualityOfConstruction,
        
        [XmlEnum("UnitMix")]
        UnitMix,
    }
}
