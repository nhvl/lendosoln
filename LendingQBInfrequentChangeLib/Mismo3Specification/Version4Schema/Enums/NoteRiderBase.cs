namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum NoteRiderBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("ARM")]
        ARM,
        
        [XmlEnum("Balloon")]
        Balloon,
        
        [XmlEnum("Buydown")]
        Buydown,
        
        [XmlEnum("Occupancy")]
        Occupancy,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Prepayment")]
        Prepayment,
        
        [XmlEnum("StepRate")]
        StepRate,
    }
}
