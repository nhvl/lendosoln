namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ServicingCommentBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Bankruptcy")]
        Bankruptcy,
        
        [XmlEnum("Collection")]
        Collection,
        
        [XmlEnum("CustomerService")]
        CustomerService,
        
        [XmlEnum("Foreclosure")]
        Foreclosure,
        
        [XmlEnum("LossMitigation")]
        LossMitigation,
        
        [XmlEnum("Other")]
        Other,
    }
}
