namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PrepaymentPenaltyBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Assumption")]
        Assumption,
        
        [XmlEnum("EarlyPayoff")]
        EarlyPayoff,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PrincipalCurtailment")]
        PrincipalCurtailment,
        
        [XmlEnum("Refinance")]
        Refinance,
    }
}
