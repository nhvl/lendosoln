namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum HeatingFuelBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Coal")]
        Coal,
        
        [XmlEnum("Electric")]
        Electric,
        
        [XmlEnum("Gas")]
        Gas,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Solar")]
        Solar,
        
        [XmlEnum("Wood")]
        Wood,
    }
}
