namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum FNMHomeImprovementProductBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("ActualActualBiweekly")]
        ActualActualBiweekly,
        
        [XmlEnum("ConstructionToPermanent")]
        ConstructionToPermanent,
        
        [XmlEnum("DailySimpleInterestCashConventional")]
        DailySimpleInterestCashConventional,
        
        [XmlEnum("DailySimpleInterestMBS")]
        DailySimpleInterestMBS,
        
        [XmlEnum("GovernmentTitleI")]
        GovernmentTitleI,
        
        [XmlEnum("Universal")]
        Universal,
    }
}
