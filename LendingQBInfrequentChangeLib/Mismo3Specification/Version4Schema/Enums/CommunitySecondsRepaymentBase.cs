namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum CommunitySecondsRepaymentBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("DeferredFullyForgiven")]
        DeferredFullyForgiven,
        
        [XmlEnum("DeferredNotFullyForgiven")]
        DeferredNotFullyForgiven,
        
        [XmlEnum("NonDeferred")]
        NonDeferred,
        
        [XmlEnum("Other")]
        Other,
    }
}
