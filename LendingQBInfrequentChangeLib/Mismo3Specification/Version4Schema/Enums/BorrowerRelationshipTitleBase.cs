namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum BorrowerRelationshipTitleBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AHusbandAndWife")]
        AHusbandAndWife,
        
        [XmlEnum("AMarriedCouple")]
        AMarriedCouple,
        
        [XmlEnum("AMarriedMan")]
        AMarriedMan,
        
        [XmlEnum("AMarriedPerson")]
        AMarriedPerson,
        
        [XmlEnum("AMarriedWoman")]
        AMarriedWoman,
        
        [XmlEnum("AnUnmarriedMan")]
        AnUnmarriedMan,
        
        [XmlEnum("AnUnmarriedPerson")]
        AnUnmarriedPerson,
        
        [XmlEnum("AnUnmarriedWoman")]
        AnUnmarriedWoman,
        
        [XmlEnum("ASameSexMarriedCouple")]
        ASameSexMarriedCouple,
        
        [XmlEnum("AsDomesticPartners")]
        AsDomesticPartners,
        
        [XmlEnum("ASingleMan")]
        ASingleMan,
        
        [XmlEnum("ASinglePerson")]
        ASinglePerson,
        
        [XmlEnum("ASingleWoman")]
        ASingleWoman,
        
        [XmlEnum("AWidow")]
        AWidow,
        
        [XmlEnum("AWidower")]
        AWidower,
        
        [XmlEnum("AWifeAndHusband")]
        AWifeAndHusband,
        
        [XmlEnum("HerHusband")]
        HerHusband,
        
        [XmlEnum("HisWife")]
        HisWife,
        
        [XmlEnum("JoinedInACivilUnion")]
        JoinedInACivilUnion,
        
        [XmlEnum("JoinedInACommonLawMarriage")]
        JoinedInACommonLawMarriage,
        
        [XmlEnum("NotApplicable")]
        NotApplicable,
        
        [XmlEnum("Other")]
        Other,
    }
}
