namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum CarStorageBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Carport")]
        Carport,
        
        [XmlEnum("Covered")]
        Covered,
        
        [XmlEnum("Driveway")]
        Driveway,
        
        [XmlEnum("Garage")]
        Garage,
        
        [XmlEnum("OffStreet")]
        OffStreet,
        
        [XmlEnum("Open")]
        Open,
        
        [XmlEnum("Other")]
        Other,
    }
}
