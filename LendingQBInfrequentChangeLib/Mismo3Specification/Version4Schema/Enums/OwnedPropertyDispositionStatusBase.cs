namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum OwnedPropertyDispositionStatusBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("PendingSale")]
        PendingSale,
        
        [XmlEnum("Retain")]
        Retain,
        
        [XmlEnum("Sold")]
        Sold,
    }
}
