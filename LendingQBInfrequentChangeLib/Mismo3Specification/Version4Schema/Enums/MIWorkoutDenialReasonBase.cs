namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum MIWorkoutDenialReasonBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("BorrowerCannotAfford")]
        BorrowerCannotAfford,
        
        [XmlEnum("BorrowerQualifiesForDifferentWorkoutOption")]
        BorrowerQualifiesForDifferentWorkoutOption,
        
        [XmlEnum("DelegatedAuthorityNotCompliant")]
        DelegatedAuthorityNotCompliant,
        
        [XmlEnum("HasAssetsAndRefusedToContribute")]
        HasAssetsAndRefusedToContribute,
        
        [XmlEnum("InsufficientContribution")]
        InsufficientContribution,
        
        [XmlEnum("MissingDocumentation")]
        MissingDocumentation,
        
        [XmlEnum("NoLegitimateHardship")]
        NoLegitimateHardship,
        
        [XmlEnum("NonArmsLengthTransaction")]
        NonArmsLengthTransaction,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("SecondLienHolderRefusedRelease")]
        SecondLienHolderRefusedRelease,
        
        [XmlEnum("SecondLienHolderWillNotResubordinate")]
        SecondLienHolderWillNotResubordinate,
        
        [XmlEnum("ServicerDelegated")]
        ServicerDelegated,
        
        [XmlEnum("SufficientIncomeToSupportDebt")]
        SufficientIncomeToSupportDebt,
        
        [XmlEnum("TornDownOrNotRepairable")]
        TornDownOrNotRepairable,
        
        [XmlEnum("UnacceptableOffer")]
        UnacceptableOffer,
    }
}
