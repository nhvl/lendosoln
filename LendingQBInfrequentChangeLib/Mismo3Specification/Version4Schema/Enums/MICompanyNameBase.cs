namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum MICompanyNameBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("ArchMI")]
        ArchMI,
        
        [XmlEnum("CMG")]
        CMG,
        
        [XmlEnum("Essent")]
        Essent,
        
        [XmlEnum("Genworth")]
        Genworth,
        
        [XmlEnum("MGIC")]
        MGIC,
        
        [XmlEnum("NationalMI")]
        NationalMI,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PMI")]
        PMI,
        
        [XmlEnum("Radian")]
        Radian,
        
        [XmlEnum("RMIC")]
        RMIC,
        
        [XmlEnum("Triad")]
        Triad,
        
        [XmlEnum("UGI")]
        UGI,
    }
}
