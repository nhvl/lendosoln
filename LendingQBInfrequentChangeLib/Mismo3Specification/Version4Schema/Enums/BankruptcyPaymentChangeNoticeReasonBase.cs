namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum BankruptcyPaymentChangeNoticeReasonBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("ARMAdjustment")]
        ARMAdjustment,
        
        [XmlEnum("CommunityDevelopmentDistrictFeeChange")]
        CommunityDevelopmentDistrictFeeChange,
        
        [XmlEnum("HomeownersAssociationFeeChange")]
        HomeownersAssociationFeeChange,
        
        [XmlEnum("InsuranceChange")]
        InsuranceChange,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PropertyTaxChange")]
        PropertyTaxChange,
    }
}
