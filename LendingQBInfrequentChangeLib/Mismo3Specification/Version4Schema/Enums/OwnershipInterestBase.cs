namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum OwnershipInterestBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("GreaterThanOrEqualTo25Percent")]
        GreaterThanOrEqualTo25Percent,
        
        [XmlEnum("LessThan25Percent")]
        LessThan25Percent,
        
        [XmlEnum("Other")]
        Other,
    }
}
