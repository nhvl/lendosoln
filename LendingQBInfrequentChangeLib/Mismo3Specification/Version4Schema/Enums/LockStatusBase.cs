namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum LockStatusBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("CanceledOrWithdrawn")]
        CanceledOrWithdrawn,
        
        [XmlEnum("Expired")]
        Expired,
        
        [XmlEnum("Locked")]
        Locked,
        
        [XmlEnum("ModificationRequested")]
        ModificationRequested,
        
        [XmlEnum("Modified")]
        Modified,
        
        [XmlEnum("None")]
        None,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Pending")]
        Pending,
        
        [XmlEnum("Rejected")]
        Rejected,
        
        [XmlEnum("Requested")]
        Requested,
    }
}
