namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum RentControlStatusBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Likely")]
        Likely,
        
        [XmlEnum("No")]
        No,
        
        [XmlEnum("Yes")]
        Yes,
    }
}
