namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ServicerBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("MasterServicer")]
        MasterServicer,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PriorServicer")]
        PriorServicer,
        
        [XmlEnum("Servicer")]
        Servicer,
        
        [XmlEnum("SpecialServicer")]
        SpecialServicer,
        
        [XmlEnum("Subservicer")]
        Subservicer,
    }
}
