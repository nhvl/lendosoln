namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum DisclosureReasonBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("24HourAdvancedPreview")]
        Item24HourAdvancedPreview,
        
        [XmlEnum("AdditionOfPrepaymentPenalty")]
        AdditionOfPrepaymentPenalty,
        
        [XmlEnum("APR")]
        APR,
        
        [XmlEnum("ClericalErrorCorrection")]
        ClericalErrorCorrection,
        
        [XmlEnum("DelayedSettlementDate")]
        DelayedSettlementDate,
        
        [XmlEnum("Eligibility")]
        Eligibility,
        
        [XmlEnum("Expiration")]
        Expiration,
        
        [XmlEnum("InterestRateDependentCharges")]
        InterestRateDependentCharges,
        
        [XmlEnum("LoanProduct")]
        LoanProduct,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("RevisionsRequestedByBorrower")]
        RevisionsRequestedByBorrower,
        
        [XmlEnum("SettlementCharges")]
        SettlementCharges,
        
        [XmlEnum("ToleranceCure")]
        ToleranceCure,
    }
}
