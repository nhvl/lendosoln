namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum CounselingSettingBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Classroom")]
        Classroom,
        
        [XmlEnum("Homestudy")]
        Homestudy,
        
        [XmlEnum("Other")]
        Other,
    }
}
