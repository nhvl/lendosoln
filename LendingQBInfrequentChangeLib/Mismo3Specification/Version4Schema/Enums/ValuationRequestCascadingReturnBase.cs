namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ValuationRequestCascadingReturnBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("All")]
        All,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Single")]
        Single,
    }
}
