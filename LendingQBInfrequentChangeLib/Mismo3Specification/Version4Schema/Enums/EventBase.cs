namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum EventBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AddedTaxCollectionStamps")]
        AddedTaxCollectionStamps,
        
        [XmlEnum("AppliedFinalTamperEvidentSignature")]
        AppliedFinalTamperEvidentSignature,
        
        [XmlEnum("AppliedTamperEvidentSignature")]
        AppliedTamperEvidentSignature,
        
        [XmlEnum("CorrectedData")]
        CorrectedData,
        
        [XmlEnum("CreatedBlankDocument")]
        CreatedBlankDocument,
        
        [XmlEnum("DocumentReceived")]
        DocumentReceived,
        
        [XmlEnum("DraftedData")]
        DraftedData,
        
        [XmlEnum("EnteredData")]
        EnteredData,
        
        [XmlEnum("ExportedDocument")]
        ExportedDocument,
        
        [XmlEnum("NotarizedDocument")]
        NotarizedDocument,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PaperedOutDocument")]
        PaperedOutDocument,
        
        [XmlEnum("PartiallyPopulatedDocument")]
        PartiallyPopulatedDocument,
        
        [XmlEnum("PopulatedDocument")]
        PopulatedDocument,
        
        [XmlEnum("RecordedDocument")]
        RecordedDocument,
        
        [XmlEnum("RerecordedDocument")]
        RerecordedDocument,
        
        [XmlEnum("SignedDocument")]
        SignedDocument,
        
        [XmlEnum("ValidatedDocument")]
        ValidatedDocument,
        
        [XmlEnum("VerifiedSignatures")]
        VerifiedSignatures,
        
        [XmlEnum("VoidedDocument")]
        VoidedDocument,
    }
}
