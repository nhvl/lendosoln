namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum StructureFeatureBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Age")]
        Age,
        
        [XmlEnum("Appeal")]
        Appeal,
        
        [XmlEnum("Design")]
        Design,
        
        [XmlEnum("GrossLivingArea")]
        GrossLivingArea,
        
        [XmlEnum("LotShapeAndTopography")]
        LotShapeAndTopography,
        
        [XmlEnum("LotSize")]
        LotSize,
        
        [XmlEnum("MaterialAndConstruction")]
        MaterialAndConstruction,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Overall")]
        Overall,
        
        [XmlEnum("Utilities")]
        Utilities,
        
        [XmlEnum("View")]
        View,
    }
}
