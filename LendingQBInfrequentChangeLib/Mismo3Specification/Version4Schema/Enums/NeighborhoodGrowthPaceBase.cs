namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum NeighborhoodGrowthPaceBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Declining")]
        Declining,
        
        [XmlEnum("FullyDeveloped")]
        FullyDeveloped,
        
        [XmlEnum("Rapid")]
        Rapid,
        
        [XmlEnum("Slow")]
        Slow,
        
        [XmlEnum("Stable")]
        Stable,
    }
}
