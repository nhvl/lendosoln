namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum FundsBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("BridgeLoan")]
        BridgeLoan,
        
        [XmlEnum("CashOnHand")]
        CashOnHand,
        
        [XmlEnum("CashOrOtherEquity")]
        CashOrOtherEquity,
        
        [XmlEnum("CheckingSavings")]
        CheckingSavings,
        
        [XmlEnum("Contribution")]
        Contribution,
        
        [XmlEnum("CreditCard")]
        CreditCard,
        
        [XmlEnum("DepositOnSalesContract")]
        DepositOnSalesContract,
        
        [XmlEnum("EquityOnPendingSale")]
        EquityOnPendingSale,
        
        [XmlEnum("EquityOnSoldProperty")]
        EquityOnSoldProperty,
        
        [XmlEnum("EquityOnSubjectProperty")]
        EquityOnSubjectProperty,
        
        [XmlEnum("ExcessDeposit")]
        ExcessDeposit,
        
        [XmlEnum("ForgivableSecuredLoan")]
        ForgivableSecuredLoan,
        
        [XmlEnum("GiftFunds")]
        GiftFunds,
        
        [XmlEnum("Grant")]
        Grant,
        
        [XmlEnum("HousingRelocation")]
        HousingRelocation,
        
        [XmlEnum("LifeInsuranceCashValue")]
        LifeInsuranceCashValue,
        
        [XmlEnum("LotEquity")]
        LotEquity,
        
        [XmlEnum("MortgageCreditCertificates")]
        MortgageCreditCertificates,
        
        [XmlEnum("MortgageRevenueBond")]
        MortgageRevenueBond,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("OtherEquity")]
        OtherEquity,
        
        [XmlEnum("PledgedCollateral")]
        PledgedCollateral,
        
        [XmlEnum("PremiumFunds")]
        PremiumFunds,
        
        [XmlEnum("RentWithOptionToPurchase")]
        RentWithOptionToPurchase,
        
        [XmlEnum("RetirementFunds")]
        RetirementFunds,
        
        [XmlEnum("SaleOfChattel")]
        SaleOfChattel,
        
        [XmlEnum("SalesPriceAdjustment")]
        SalesPriceAdjustment,
        
        [XmlEnum("SecondaryFinancing")]
        SecondaryFinancing,
        
        [XmlEnum("SecuredLoan")]
        SecuredLoan,
        
        [XmlEnum("StocksAndBonds")]
        StocksAndBonds,
        
        [XmlEnum("SweatEquity")]
        SweatEquity,
        
        [XmlEnum("TradeEquity")]
        TradeEquity,
        
        [XmlEnum("TrustFunds")]
        TrustFunds,
        
        [XmlEnum("UnsecuredBorrowedFunds")]
        UnsecuredBorrowedFunds,
    }
}
