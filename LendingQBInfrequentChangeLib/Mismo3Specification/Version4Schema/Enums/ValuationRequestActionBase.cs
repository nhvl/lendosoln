namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ValuationRequestActionBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Cancellation")]
        Cancellation,
        
        [XmlEnum("Change")]
        Change,
        
        [XmlEnum("Hold")]
        Hold,
        
        [XmlEnum("Original")]
        Original,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PriceQuote")]
        PriceQuote,
        
        [XmlEnum("Reissue")]
        Reissue,
        
        [XmlEnum("Resume")]
        Resume,
        
        [XmlEnum("StatusQuery")]
        StatusQuery,
        
        [XmlEnum("Update")]
        Update,
        
        [XmlEnum("Upgrade")]
        Upgrade,
    }
}
