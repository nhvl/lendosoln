namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PropertyPreservationStatusBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Approved")]
        Approved,
        
        [XmlEnum("Cancelled")]
        Cancelled,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Required")]
        Required,
    }
}
