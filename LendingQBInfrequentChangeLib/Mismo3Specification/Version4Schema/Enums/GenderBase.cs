namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum GenderBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Female")]
        Female,
        
        [XmlEnum("InformationNotProvidedUnknown")]
        InformationNotProvidedUnknown,
        
        [XmlEnum("Male")]
        Male,
        
        [XmlEnum("NotApplicable")]
        NotApplicable,
    }
}
