namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum LoanSecuredByBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("SubjectProperty")]
        SubjectProperty,
    }
}
