namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum FeeSummaryTotalFeesPaidByBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Broker")]
        Broker,
        
        [XmlEnum("Buyer")]
        Buyer,
        
        [XmlEnum("Investor")]
        Investor,
        
        [XmlEnum("Lender")]
        Lender,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Seller")]
        Seller,
    }
}
