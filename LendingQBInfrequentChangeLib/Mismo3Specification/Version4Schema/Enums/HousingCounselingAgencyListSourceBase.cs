namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum HousingCounselingAgencyListSourceBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("CFPB")]
        CFPB,
        
        [XmlEnum("HUD")]
        HUD,
        
        [XmlEnum("Other")]
        Other,
    }
}
