namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum CompensationBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Rebate")]
        Rebate,
        
        [XmlEnum("ServiceReleasePremium")]
        ServiceReleasePremium,
        
        [XmlEnum("YieldSpreadDifferential")]
        YieldSpreadDifferential,
    }
}
