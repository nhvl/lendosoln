namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum HELOCTeaserRateBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Adjustable")]
        Adjustable,
        
        [XmlEnum("Fixed")]
        Fixed,
    }
}
