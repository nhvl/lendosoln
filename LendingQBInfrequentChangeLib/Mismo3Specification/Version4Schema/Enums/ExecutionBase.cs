namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ExecutionBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Cash")]
        Cash,
        
        [XmlEnum("Guarantor")]
        Guarantor,
        
        [XmlEnum("Multilender")]
        Multilender,
        
        [XmlEnum("Other")]
        Other,
    }
}
