namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PeriodicLateCountBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("CurrentOneToTwelveMonths")]
        CurrentOneToTwelveMonths,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Previous25To36Months")]
        Previous25To36Months,
        
        [XmlEnum("PreviousThirteenTo24Months")]
        PreviousThirteenTo24Months,
    }
}
