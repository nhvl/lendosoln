namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum LocationBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AdjacentToPark")]
        AdjacentToPark,
        
        [XmlEnum("AdjacentToPowerLines")]
        AdjacentToPowerLines,
        
        [XmlEnum("BusyRoad")]
        BusyRoad,
        
        [XmlEnum("Commercial")]
        Commercial,
        
        [XmlEnum("GolfCourse")]
        GolfCourse,
        
        [XmlEnum("Industrial")]
        Industrial,
        
        [XmlEnum("Landfill")]
        Landfill,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PublicTransportation")]
        PublicTransportation,
        
        [XmlEnum("Residential")]
        Residential,
        
        [XmlEnum("WaterFront")]
        WaterFront,
    }
}
