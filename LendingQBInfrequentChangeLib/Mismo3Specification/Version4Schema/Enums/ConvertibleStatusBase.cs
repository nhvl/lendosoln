namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ConvertibleStatusBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Active")]
        Active,
        
        [XmlEnum("Exercised")]
        Exercised,
        
        [XmlEnum("Expired")]
        Expired,
    }
}
