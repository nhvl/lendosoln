namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum RefinanceImprovementsBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Made")]
        Made,
        
        [XmlEnum("ToBeMade")]
        ToBeMade,
        
        [XmlEnum("Unknown")]
        Unknown,
    }
}
