namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ACHBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Current")]
        Current,
        
        [XmlEnum("Pending")]
        Pending,
    }
}
