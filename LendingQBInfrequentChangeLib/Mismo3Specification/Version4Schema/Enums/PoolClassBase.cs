namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PoolClassBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("FNM")]
        FNM,
        
        [XmlEnum("FRE")]
        FRE,
        
        [XmlEnum("GNMAI")]
        GNMAI,
        
        [XmlEnum("GNMAII")]
        GNMAII,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Private")]
        Private,
    }
}
