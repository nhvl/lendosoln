namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum SaleBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("CourtOrderedNonForeclosureSale")]
        CourtOrderedNonForeclosureSale,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("ProbateSale")]
        ProbateSale,
        
        [XmlEnum("RelocationSale")]
        RelocationSale,
        
        [XmlEnum("REOPostForeclosureSale")]
        REOPostForeclosureSale,
        
        [XmlEnum("ShortSale")]
        ShortSale,
        
        [XmlEnum("TrusteeJudicialForeclosureSale")]
        TrusteeJudicialForeclosureSale,
        
        [XmlEnum("TrusteeNonJudicialForeclosureSale")]
        TrusteeNonJudicialForeclosureSale,
        
        [XmlEnum("TypicallyMotivated")]
        TypicallyMotivated,
    }
}
