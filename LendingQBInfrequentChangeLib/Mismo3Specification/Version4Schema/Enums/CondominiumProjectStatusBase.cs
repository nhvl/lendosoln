namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum CondominiumProjectStatusBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Established")]
        Established,
        
        [XmlEnum("New")]
        New,
    }
}
