namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum SalesConcessionProvidedByBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Builder")]
        Builder,
        
        [XmlEnum("MortgageLender")]
        MortgageLender,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("RealEstateBroker")]
        RealEstateBroker,
        
        [XmlEnum("Seller")]
        Seller,
    }
}
