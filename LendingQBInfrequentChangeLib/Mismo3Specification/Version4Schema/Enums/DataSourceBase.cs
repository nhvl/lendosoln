namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum DataSourceBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AssessmentAndTaxRecords")]
        AssessmentAndTaxRecords,
        
        [XmlEnum("CooperativeBoard")]
        CooperativeBoard,
        
        [XmlEnum("Developer")]
        Developer,
        
        [XmlEnum("EngagementLetter")]
        EngagementLetter,
        
        [XmlEnum("ExteriorInspectionOnly")]
        ExteriorInspectionOnly,
        
        [XmlEnum("HomeownersAssociation")]
        HomeownersAssociation,
        
        [XmlEnum("InteriorExteriorInspection")]
        InteriorExteriorInspection,
        
        [XmlEnum("InvestorGuidance")]
        InvestorGuidance,
        
        [XmlEnum("LoanFile")]
        LoanFile,
        
        [XmlEnum("ManagementAgency")]
        ManagementAgency,
        
        [XmlEnum("MultipleListingService")]
        MultipleListingService,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PreviousAppraisalFile")]
        PreviousAppraisalFile,
        
        [XmlEnum("PriorInspection")]
        PriorInspection,
        
        [XmlEnum("PropertyOwner")]
        PropertyOwner,
        
        [XmlEnum("PurchaseAgreement")]
        PurchaseAgreement,
        
        [XmlEnum("ThirdPartyAppraisalReport")]
        ThirdPartyAppraisalReport,
    }
}
