namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum BuildingPermitWorkBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Building")]
        Building,
        
        [XmlEnum("Demolition")]
        Demolition,
        
        [XmlEnum("Electrical")]
        Electrical,
        
        [XmlEnum("Mechanical")]
        Mechanical,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Plumbing")]
        Plumbing,
    }
}
