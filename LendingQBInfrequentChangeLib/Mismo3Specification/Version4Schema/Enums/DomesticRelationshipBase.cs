namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum DomesticRelationshipBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("CivilUnion")]
        CivilUnion,
        
        [XmlEnum("DomesticPartnership")]
        DomesticPartnership,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("RegisteredReciprocalBeneficiaryRelationship")]
        RegisteredReciprocalBeneficiaryRelationship,
    }
}
