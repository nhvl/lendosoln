namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ConsiderationBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AttorneysFee")]
        AttorneysFee,
        
        [XmlEnum("Judgment")]
        Judgment,
        
        [XmlEnum("Lien")]
        Lien,
        
        [XmlEnum("OriginalLoanAmount")]
        OriginalLoanAmount,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("SalePrice")]
        SalePrice,
    }
}
