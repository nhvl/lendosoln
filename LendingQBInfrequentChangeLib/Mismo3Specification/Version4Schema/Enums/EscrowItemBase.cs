namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum EscrowItemBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AssessmentTax")]
        AssessmentTax,
        
        [XmlEnum("CityBondTax")]
        CityBondTax,
        
        [XmlEnum("CityPropertyTax")]
        CityPropertyTax,
        
        [XmlEnum("CondominiumAssociationDues")]
        CondominiumAssociationDues,
        
        [XmlEnum("CondominiumAssociationSpecialAssessment")]
        CondominiumAssociationSpecialAssessment,
        
        [XmlEnum("ConstructionCompletionFunds")]
        ConstructionCompletionFunds,
        
        [XmlEnum("CooperativeAssociationDues")]
        CooperativeAssociationDues,
        
        [XmlEnum("CooperativeAssociationSpecialAssessment")]
        CooperativeAssociationSpecialAssessment,
        
        [XmlEnum("CountyBondTax")]
        CountyBondTax,
        
        [XmlEnum("CountyPropertyTax")]
        CountyPropertyTax,
        
        [XmlEnum("DistrictPropertyTax")]
        DistrictPropertyTax,
        
        [XmlEnum("EarthquakeInsurance")]
        EarthquakeInsurance,
        
        [XmlEnum("EnergyEfficientImprovementFunds")]
        EnergyEfficientImprovementFunds,
        
        [XmlEnum("FloodInsurance")]
        FloodInsurance,
        
        [XmlEnum("HailInsurancePremium")]
        HailInsurancePremium,
        
        [XmlEnum("HazardInsurance")]
        HazardInsurance,
        
        [XmlEnum("HomeownersAssociationDues")]
        HomeownersAssociationDues,
        
        [XmlEnum("HomeownersAssociationSpecialAssessment")]
        HomeownersAssociationSpecialAssessment,
        
        [XmlEnum("HomeownersInsurance")]
        HomeownersInsurance,
        
        [XmlEnum("MortgageInsurance")]
        MortgageInsurance,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("ParishTax")]
        ParishTax,
        
        [XmlEnum("PestInsurance")]
        PestInsurance,
        
        [XmlEnum("PropertyTax")]
        PropertyTax,
        
        [XmlEnum("RehabilitationFunds")]
        RehabilitationFunds,
        
        [XmlEnum("SchoolPropertyTax")]
        SchoolPropertyTax,
        
        [XmlEnum("StatePropertyTax")]
        StatePropertyTax,
        
        [XmlEnum("TownPropertyTax")]
        TownPropertyTax,
        
        [XmlEnum("TownshipPropertyTax")]
        TownshipPropertyTax,
        
        [XmlEnum("VillagePropertyTax")]
        VillagePropertyTax,
        
        [XmlEnum("VolcanoInsurance")]
        VolcanoInsurance,
        
        [XmlEnum("WindstormInsurance")]
        WindstormInsurance,
    }
}
