namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ContributionSourceBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Borrower")]
        Borrower,
        
        [XmlEnum("Investor")]
        Investor,
        
        [XmlEnum("Other")]
        Other,
    }
}
