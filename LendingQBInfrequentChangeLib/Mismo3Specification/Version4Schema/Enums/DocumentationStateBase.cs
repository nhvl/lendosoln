namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum DocumentationStateBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AsCollected")]
        AsCollected,
        
        [XmlEnum("AsRequested")]
        AsRequested,
        
        [XmlEnum("AsRequired")]
        AsRequired,
        
        [XmlEnum("Other")]
        Other,
    }
}
