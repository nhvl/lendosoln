namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum CurrentFirstMortgageHolderBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("FNM")]
        FNM,
        
        [XmlEnum("FRE")]
        FRE,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Unknown")]
        Unknown,
    }
}
