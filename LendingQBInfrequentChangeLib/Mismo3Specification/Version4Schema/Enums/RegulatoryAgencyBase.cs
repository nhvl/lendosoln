namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum RegulatoryAgencyBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("CFPB")]
        CFPB,
        
        [XmlEnum("FDIC")]
        FDIC,
        
        [XmlEnum("FRS")]
        FRS,
        
        [XmlEnum("HUD")]
        HUD,
        
        [XmlEnum("NationalCreditUnionAssociation")]
        NationalCreditUnionAssociation,
        
        [XmlEnum("OfficeOfTheComptrollerOfTheCurrency")]
        OfficeOfTheComptrollerOfTheCurrency,
        
        [XmlEnum("OfficeOfThriftSupervision")]
        OfficeOfThriftSupervision,
        
        [XmlEnum("Other")]
        Other,
    }
}
