namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PresumptionOfComplianceBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("RebuttablePresumption")]
        RebuttablePresumption,
        
        [XmlEnum("SafeHarbor")]
        SafeHarbor,
    }
}
