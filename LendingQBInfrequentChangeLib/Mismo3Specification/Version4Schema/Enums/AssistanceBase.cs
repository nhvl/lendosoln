namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum AssistanceBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PayForPerformance")]
        PayForPerformance,
        
        [XmlEnum("Relocation")]
        Relocation,
    }
}
