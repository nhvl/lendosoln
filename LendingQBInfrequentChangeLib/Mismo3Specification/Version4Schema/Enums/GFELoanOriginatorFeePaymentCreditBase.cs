namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum GFELoanOriginatorFeePaymentCreditBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AmountsPaidByOrInBehalfOfBorrower")]
        AmountsPaidByOrInBehalfOfBorrower,
        
        [XmlEnum("ChosenInterestRateCreditOrCharge")]
        ChosenInterestRateCreditOrCharge,
        
        [XmlEnum("Other")]
        Other,
    }
}
