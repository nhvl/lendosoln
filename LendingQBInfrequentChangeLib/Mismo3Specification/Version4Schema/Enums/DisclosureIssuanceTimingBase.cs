namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum DisclosureIssuanceTimingBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AtClosing")]
        AtClosing,
        
        [XmlEnum("PostClosing")]
        PostClosing,
        
        [XmlEnum("PreClosing")]
        PreClosing,
    }
}
