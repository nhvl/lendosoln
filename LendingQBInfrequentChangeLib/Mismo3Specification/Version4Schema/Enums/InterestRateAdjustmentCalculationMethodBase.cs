namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum InterestRateAdjustmentCalculationMethodBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AddFixedPercentageToCurrentInterestRate")]
        AddFixedPercentageToCurrentInterestRate,
        
        [XmlEnum("AddFixedPercentageToOriginalInterestRate")]
        AddFixedPercentageToOriginalInterestRate,
        
        [XmlEnum("AddImpliedMarginToValueOfFinancialIndex")]
        AddImpliedMarginToValueOfFinancialIndex,
        
        [XmlEnum("AddIndexChangeToOriginalInterestRate")]
        AddIndexChangeToOriginalInterestRate,
        
        [XmlEnum("AddMarginToValueOfFinancialIndex")]
        AddMarginToValueOfFinancialIndex,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Renegotiated")]
        Renegotiated,
    }
}
