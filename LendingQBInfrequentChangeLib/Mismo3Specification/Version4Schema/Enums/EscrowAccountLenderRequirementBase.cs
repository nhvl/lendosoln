namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum EscrowAccountLenderRequirementBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("EscrowsRequiredByLender")]
        EscrowsRequiredByLender,
        
        [XmlEnum("EscrowsWaivedByLender")]
        EscrowsWaivedByLender,
    }
}
