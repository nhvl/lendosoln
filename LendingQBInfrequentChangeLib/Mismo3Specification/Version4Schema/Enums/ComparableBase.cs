namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ComparableBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("ActiveListing")]
        ActiveListing,
        
        [XmlEnum("ClosedSale")]
        ClosedSale,
        
        [XmlEnum("CompetitiveOffering")]
        CompetitiveOffering,
        
        [XmlEnum("ExpiredListing")]
        ExpiredListing,
        
        [XmlEnum("Other")]
        Other,
    }
}
