namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum FloodRequestActionBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Cancellation")]
        Cancellation,
        
        [XmlEnum("Change")]
        Change,
        
        [XmlEnum("Dispute")]
        Dispute,
        
        [XmlEnum("Original")]
        Original,
        
        [XmlEnum("Reissue")]
        Reissue,
        
        [XmlEnum("StatusQuery")]
        StatusQuery,
        
        [XmlEnum("Transfer")]
        Transfer,
        
        [XmlEnum("Upgrade")]
        Upgrade,
    }
}
