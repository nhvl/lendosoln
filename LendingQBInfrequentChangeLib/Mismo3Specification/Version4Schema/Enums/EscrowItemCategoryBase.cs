namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum EscrowItemCategoryBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AssociationDues")]
        AssociationDues,
        
        [XmlEnum("Holdback")]
        Holdback,
        
        [XmlEnum("HomeownersInsurance")]
        HomeownersInsurance,
        
        [XmlEnum("MI")]
        MI,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PropertyTaxes")]
        PropertyTaxes,
    }
}
