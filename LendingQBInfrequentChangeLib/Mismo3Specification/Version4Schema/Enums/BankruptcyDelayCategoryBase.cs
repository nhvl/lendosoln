namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum BankruptcyDelayCategoryBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("ContestedOrLitigatedAction")]
        ContestedOrLitigatedAction,
        
        [XmlEnum("ImpactedPartyProcess")]
        ImpactedPartyProcess,
        
        [XmlEnum("LegalProcess")]
        LegalProcess,
        
        [XmlEnum("LegislativeChange")]
        LegislativeChange,
        
        [XmlEnum("Mediation")]
        Mediation,
        
        [XmlEnum("MilitaryIndulgence")]
        MilitaryIndulgence,
        
        [XmlEnum("MissingDocument")]
        MissingDocument,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Probate")]
        Probate,
        
        [XmlEnum("TitleIssue")]
        TitleIssue,
    }
}
