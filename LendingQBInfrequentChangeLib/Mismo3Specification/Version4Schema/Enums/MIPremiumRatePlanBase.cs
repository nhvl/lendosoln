namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum MIPremiumRatePlanBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("BackLoaded")]
        BackLoaded,
        
        [XmlEnum("Level")]
        Level,
        
        [XmlEnum("ModifiedFrontLoaded")]
        ModifiedFrontLoaded,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("StandardFrontLoaded")]
        StandardFrontLoaded,
    }
}
