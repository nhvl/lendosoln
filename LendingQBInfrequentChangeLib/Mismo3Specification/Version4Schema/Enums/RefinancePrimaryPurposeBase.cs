namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum RefinancePrimaryPurposeBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AssetAcquisition")]
        AssetAcquisition,
        
        [XmlEnum("CapitalizedInterestTaxesInsuranceOrFees")]
        CapitalizedInterestTaxesInsuranceOrFees,
        
        [XmlEnum("Cash")]
        Cash,
        
        [XmlEnum("Convenience")]
        Convenience,
        
        [XmlEnum("DebtConsolidation")]
        DebtConsolidation,
        
        [XmlEnum("Education")]
        Education,
        
        [XmlEnum("EquityBuyout")]
        EquityBuyout,
        
        [XmlEnum("HomeImprovement")]
        HomeImprovement,
        
        [XmlEnum("InterestRateReduction")]
        InterestRateReduction,
        
        [XmlEnum("Medical")]
        Medical,
        
        [XmlEnum("MortgageTermReduction")]
        MortgageTermReduction,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PayoffLeaseholdInterest")]
        PayoffLeaseholdInterest,
        
        [XmlEnum("PrimaryLienPayoff")]
        PrimaryLienPayoff,
        
        [XmlEnum("SecondaryLienPayoff")]
        SecondaryLienPayoff,
        
        [XmlEnum("SpecialPurpose")]
        SpecialPurpose,
        
        [XmlEnum("UnsecuredLienPayoff")]
        UnsecuredLienPayoff,
        
        [XmlEnum("Unspecified")]
        Unspecified,
    }
}
