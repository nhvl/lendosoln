namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ManufacturedHomeWidthBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("MultiWide")]
        MultiWide,
        
        [XmlEnum("SingleWide")]
        SingleWide,
    }
}
