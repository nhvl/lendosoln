namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum OptionalProductChangeBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Add")]
        Add,
        
        [XmlEnum("Cancel")]
        Cancel,
        
        [XmlEnum("Modify")]
        Modify,
        
        [XmlEnum("Other")]
        Other,
    }
}
