namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PreferredResponseFormatBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PCL")]
        PCL,
        
        [XmlEnum("PDF")]
        PDF,
        
        [XmlEnum("Text")]
        Text,
        
        [XmlEnum("XML")]
        XML,
    }
}
