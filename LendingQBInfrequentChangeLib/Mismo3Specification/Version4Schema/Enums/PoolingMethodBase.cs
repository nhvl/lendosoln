namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PoolingMethodBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("ConcurrentDate")]
        ConcurrentDate,
        
        [XmlEnum("InternalReserve")]
        InternalReserve,
        
        [XmlEnum("Other")]
        Other,
    }
}
