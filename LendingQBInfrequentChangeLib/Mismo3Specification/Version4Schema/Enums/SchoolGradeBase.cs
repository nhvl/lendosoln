namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum SchoolGradeBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Eighth")]
        Eighth,
        
        [XmlEnum("Eleventh")]
        Eleventh,
        
        [XmlEnum("Fifth")]
        Fifth,
        
        [XmlEnum("First")]
        First,
        
        [XmlEnum("Fourth")]
        Fourth,
        
        [XmlEnum("Kindergarten")]
        Kindergarten,
        
        [XmlEnum("Ninth")]
        Ninth,
        
        [XmlEnum("PreKindergarten")]
        PreKindergarten,
        
        [XmlEnum("Second")]
        Second,
        
        [XmlEnum("Seventh")]
        Seventh,
        
        [XmlEnum("Sixth")]
        Sixth,
        
        [XmlEnum("Tenth")]
        Tenth,
        
        [XmlEnum("Third")]
        Third,
        
        [XmlEnum("Twelfth")]
        Twelfth,
    }
}
