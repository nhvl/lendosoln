namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum BankruptcyActionBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AmendedPetitionFiled")]
        AmendedPetitionFiled,
        
        [XmlEnum("ComplaintToDetermineExtentAndValidityOfLien")]
        ComplaintToDetermineExtentAndValidityOfLien,
        
        [XmlEnum("ConsentOrderFiled")]
        ConsentOrderFiled,
        
        [XmlEnum("DebtDischargeGranted")]
        DebtDischargeGranted,
        
        [XmlEnum("DeclarationOfIntentToSurrenderOtherAssets")]
        DeclarationOfIntentToSurrenderOtherAssets,
        
        [XmlEnum("DeclarationOfIntentToSurrenderSubjectProperty")]
        DeclarationOfIntentToSurrenderSubjectProperty,
        
        [XmlEnum("MortgageDebtReaffirmed")]
        MortgageDebtReaffirmed,
        
        [XmlEnum("MotionForDischargeFiled")]
        MotionForDischargeFiled,
        
        [XmlEnum("MotionForDischargeHearingHeld")]
        MotionForDischargeHearingHeld,
        
        [XmlEnum("MotionForDismissalFiled")]
        MotionForDismissalFiled,
        
        [XmlEnum("MotionForReliefFiled")]
        MotionForReliefFiled,
        
        [XmlEnum("MotionForReliefGranted")]
        MotionForReliefGranted,
        
        [XmlEnum("MotionForReliefHearingHeld")]
        MotionForReliefHearingHeld,
        
        [XmlEnum("MotionToAvoidLien")]
        MotionToAvoidLien,
        
        [XmlEnum("MotionToConvertChapter")]
        MotionToConvertChapter,
        
        [XmlEnum("MotionToDetermineLienPriority")]
        MotionToDetermineLienPriority,
        
        [XmlEnum("MotionToSell")]
        MotionToSell,
        
        [XmlEnum("MotionToValueCollateral")]
        MotionToValueCollateral,
        
        [XmlEnum("NoticeOfMortgagePaymentChange")]
        NoticeOfMortgagePaymentChange,
        
        [XmlEnum("ObjectionToConfirmation")]
        ObjectionToConfirmation,
        
        [XmlEnum("ObjectionToDischarge")]
        ObjectionToDischarge,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PetitionFiled")]
        PetitionFiled,
        
        [XmlEnum("PlanObjection")]
        PlanObjection,
        
        [XmlEnum("PostPetitionPlanFiled")]
        PostPetitionPlanFiled,
        
        [XmlEnum("ProofOfClaimFiled")]
        ProofOfClaimFiled,
        
        [XmlEnum("ProofOfClaimReferred")]
        ProofOfClaimReferred,
        
        [XmlEnum("ServicerReferredNoticeToAttorney")]
        ServicerReferredNoticeToAttorney,
    }
}
