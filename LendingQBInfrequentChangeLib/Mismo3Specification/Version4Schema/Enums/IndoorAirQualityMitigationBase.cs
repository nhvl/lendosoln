namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum IndoorAirQualityMitigationBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("EnergyRecoveryVentilatorUnit")]
        EnergyRecoveryVentilatorUnit,
        
        [XmlEnum("IndoorAirPLUS")]
        IndoorAirPLUS,
        
        [XmlEnum("NonToxicPestControl")]
        NonToxicPestControl,
        
        [XmlEnum("Other")]
        Other,
    }
}
