namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum TILPaymentSummaryItemBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AtFirstAdjustment")]
        AtFirstAdjustment,
        
        [XmlEnum("AtSecondAdjustment")]
        AtSecondAdjustment,
        
        [XmlEnum("Introductory")]
        Introductory,
        
        [XmlEnum("MaximumEver")]
        MaximumEver,
        
        [XmlEnum("MaximumFirstFiveYears")]
        MaximumFirstFiveYears,
        
        [XmlEnum("Other")]
        Other,
    }
}
