namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum NeighborhoodDemandSupplyBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("InBalance")]
        InBalance,
        
        [XmlEnum("OverSupply")]
        OverSupply,
        
        [XmlEnum("Shortage")]
        Shortage,
    }
}
