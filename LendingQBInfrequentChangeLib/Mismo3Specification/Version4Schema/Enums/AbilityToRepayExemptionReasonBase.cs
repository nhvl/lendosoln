namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum AbilityToRepayExemptionReasonBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("CreditorOrganization")]
        CreditorOrganization,
        
        [XmlEnum("LoanProgram")]
        LoanProgram,
        
        [XmlEnum("NumberOfFinancedUnits")]
        NumberOfFinancedUnits,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PropertyUsage")]
        PropertyUsage,
    }
}
