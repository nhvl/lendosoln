namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PayoffActionBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("LenderOrderBothPayoffSubordinate")]
        LenderOrderBothPayoffSubordinate,
        
        [XmlEnum("LenderOrderPayoff")]
        LenderOrderPayoff,
        
        [XmlEnum("LenderOrderSubordinate")]
        LenderOrderSubordinate,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("TitleCompanyOrderBothPayoffSubordinate")]
        TitleCompanyOrderBothPayoffSubordinate,
        
        [XmlEnum("TitleCompanyOrderPayoff")]
        TitleCompanyOrderPayoff,
        
        [XmlEnum("TitleCompanyOrderSubordinate")]
        TitleCompanyOrderSubordinate,
    }
}
