namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum MERSRegistrationStatusBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Active")]
        Active,
        
        [XmlEnum("Inactive")]
        Inactive,
        
        [XmlEnum("Other")]
        Other,
    }
}
