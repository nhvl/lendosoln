namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum FIPSCountySubdivisionBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("CensusCountyDivision")]
        CensusCountyDivision,
        
        [XmlEnum("MinorCivilDivision")]
        MinorCivilDivision,
    }
}
