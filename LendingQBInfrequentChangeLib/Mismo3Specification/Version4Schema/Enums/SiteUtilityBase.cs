namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum SiteUtilityBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Electricity")]
        Electricity,
        
        [XmlEnum("Gas")]
        Gas,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("RenewableEnergyBiomass")]
        RenewableEnergyBiomass,
        
        [XmlEnum("RenewableEnergyHydropower")]
        RenewableEnergyHydropower,
        
        [XmlEnum("RenewableEnergyWind")]
        RenewableEnergyWind,
        
        [XmlEnum("SanitarySewer")]
        SanitarySewer,
        
        [XmlEnum("StormSewer")]
        StormSewer,
        
        [XmlEnum("Water")]
        Water,
    }
}
