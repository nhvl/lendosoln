namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ACHAccountBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Checking")]
        Checking,
        
        [XmlEnum("Savings")]
        Savings,
    }
}
