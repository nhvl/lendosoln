namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum StopCodeActionBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AcceptCertifiedFundsOnly")]
        AcceptCertifiedFundsOnly,
        
        [XmlEnum("DoNotApplyNormalPayments")]
        DoNotApplyNormalPayments,
        
        [XmlEnum("DoNotAssessLateCharge")]
        DoNotAssessLateCharge,
        
        [XmlEnum("DoNotDisburseFromEscrow")]
        DoNotDisburseFromEscrow,
        
        [XmlEnum("DoNotPerformEscrowAnalysis")]
        DoNotPerformEscrowAnalysis,
        
        [XmlEnum("DoNotSendDelinquentNotices")]
        DoNotSendDelinquentNotices,
        
        [XmlEnum("DoNotSolicitForOptionalProducts")]
        DoNotSolicitForOptionalProducts,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("SpecialHandling")]
        SpecialHandling,
    }
}
