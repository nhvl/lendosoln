namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum DownPaymentOptionBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("FivePercentOption")]
        FivePercentOption,
        
        [XmlEnum("FNM97Option")]
        FNM97Option,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("ThreeTwoOption")]
        ThreeTwoOption,
    }
}
