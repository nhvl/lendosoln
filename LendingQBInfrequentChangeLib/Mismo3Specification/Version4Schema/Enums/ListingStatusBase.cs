namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ListingStatusBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Active")]
        Active,
        
        [XmlEnum("Contract")]
        Contract,
        
        [XmlEnum("Expired")]
        Expired,
        
        [XmlEnum("Leased")]
        Leased,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Rented")]
        Rented,
        
        [XmlEnum("SettledSale")]
        SettledSale,
        
        [XmlEnum("Withdrawn")]
        Withdrawn,
    }
}
