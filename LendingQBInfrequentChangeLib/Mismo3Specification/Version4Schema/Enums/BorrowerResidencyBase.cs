namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum BorrowerResidencyBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Current")]
        Current,
        
        [XmlEnum("Prior")]
        Prior,
    }
}
