namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum IdentityDocumentIssuedByBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Country")]
        Country,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("StateProvince")]
        StateProvince,
    }
}
