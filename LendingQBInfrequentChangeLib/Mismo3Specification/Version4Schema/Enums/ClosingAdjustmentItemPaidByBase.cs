namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ClosingAdjustmentItemPaidByBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Broker")]
        Broker,
        
        [XmlEnum("Buyer")]
        Buyer,
        
        [XmlEnum("Correspondent")]
        Correspondent,
        
        [XmlEnum("Lender")]
        Lender,
        
        [XmlEnum("Seller")]
        Seller,
        
        [XmlEnum("ThirdParty")]
        ThirdParty,
    }
}
