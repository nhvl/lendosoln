namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum EndUserBillingInstructionBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("BillAdditionalEndUser")]
        BillAdditionalEndUser,
        
        [XmlEnum("BillReissueRecordingFeesToAdditionalEndUser")]
        BillReissueRecordingFeesToAdditionalEndUser,
        
        [XmlEnum("BillReissueRecordingFeesToOriginalRequestingParty")]
        BillReissueRecordingFeesToOriginalRequestingParty,
        
        [XmlEnum("BillRequestingParty")]
        BillRequestingParty,
        
        [XmlEnum("Other")]
        Other,
    }
}
