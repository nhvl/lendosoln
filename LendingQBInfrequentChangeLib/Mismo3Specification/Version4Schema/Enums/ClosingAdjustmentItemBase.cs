namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ClosingAdjustmentItemBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("FuelCosts")]
        FuelCosts,
        
        [XmlEnum("Gift")]
        Gift,
        
        [XmlEnum("Grant")]
        Grant,
        
        [XmlEnum("LenderCredit")]
        LenderCredit,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("ProceedsOfSubordinateLiens")]
        ProceedsOfSubordinateLiens,
        
        [XmlEnum("RebateCredit")]
        RebateCredit,
        
        [XmlEnum("RelocationFunds")]
        RelocationFunds,
        
        [XmlEnum("RentFromSubjectProperty")]
        RentFromSubjectProperty,
        
        [XmlEnum("RepairCompletionEscrowHoldback")]
        RepairCompletionEscrowHoldback,
        
        [XmlEnum("Repairs")]
        Repairs,
        
        [XmlEnum("SatisfactionOfSubordinateLien")]
        SatisfactionOfSubordinateLien,
        
        [XmlEnum("SellerCredit")]
        SellerCredit,
        
        [XmlEnum("SellersEscrowAssumption")]
        SellersEscrowAssumption,
        
        [XmlEnum("SellersMortgageInsuranceAssumption")]
        SellersMortgageInsuranceAssumption,
        
        [XmlEnum("SellersReserveAccountAssumption")]
        SellersReserveAccountAssumption,
        
        [XmlEnum("Services")]
        Services,
        
        [XmlEnum("SubordinateFinancingProceeds")]
        SubordinateFinancingProceeds,
        
        [XmlEnum("SweatEquity")]
        SweatEquity,
        
        [XmlEnum("TenantSecurityDeposit")]
        TenantSecurityDeposit,
        
        [XmlEnum("TitlePremiumAdjustment")]
        TitlePremiumAdjustment,
        
        [XmlEnum("TradeEquity")]
        TradeEquity,
        
        [XmlEnum("UnpaidUtilityEscrowHoldback")]
        UnpaidUtilityEscrowHoldback,
    }
}
