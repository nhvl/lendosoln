namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum CreditCommentSourceBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Borrower")]
        Borrower,
        
        [XmlEnum("CreditBureau")]
        CreditBureau,
        
        [XmlEnum("Equifax")]
        Equifax,
        
        [XmlEnum("Experian")]
        Experian,
        
        [XmlEnum("Lender")]
        Lender,
        
        [XmlEnum("RepositoryBureau")]
        RepositoryBureau,
        
        [XmlEnum("TransUnion")]
        TransUnion,
    }
}
