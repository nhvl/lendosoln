namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum RentIncludesUtilityBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Cable")]
        Cable,
        
        [XmlEnum("Electric")]
        Electric,
        
        [XmlEnum("Gas")]
        Gas,
        
        [XmlEnum("Oil")]
        Oil,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Sewer")]
        Sewer,
        
        [XmlEnum("Trash")]
        Trash,
        
        [XmlEnum("Water")]
        Water,
    }
}
