namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum SummaryAmountBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("SubtotalLiabilitiesForRentalPropertyBalance")]
        SubtotalLiabilitiesForRentalPropertyBalance,
        
        [XmlEnum("SubtotalLiabilitiesForRentalPropertyMonthlyPayment")]
        SubtotalLiabilitiesForRentalPropertyMonthlyPayment,
        
        [XmlEnum("SubtotalLiabilitiesMonthlyPayment")]
        SubtotalLiabilitiesMonthlyPayment,
        
        [XmlEnum("SubtotalLiabilitiesPaidByClosingNotIncludingSubjectPropertyLiensBalance")]
        SubtotalLiabilitiesPaidByClosingNotIncludingSubjectPropertyLiensBalance,
        
        [XmlEnum("SubtotalLiabilitiesPaidByClosingNotIncludingSubjectPropertyLiensMonthlyPayment")]
        SubtotalLiabilitiesPaidByClosingNotIncludingSubjectPropertyLiensMonthlyPayment,
        
        [XmlEnum("SubtotalLiquidAssetsNotIncludingGift")]
        SubtotalLiquidAssetsNotIncludingGift,
        
        [XmlEnum("SubtotalNonLiquidAssets")]
        SubtotalNonLiquidAssets,
        
        [XmlEnum("SubtotalOmittedLiabilitiesBalance")]
        SubtotalOmittedLiabilitiesBalance,
        
        [XmlEnum("SubtotalOmittedLiabilitiesMonthlyPayment")]
        SubtotalOmittedLiabilitiesMonthlyPayment,
        
        [XmlEnum("SubtotalResubordinatedLiabilitiesBalanceForSubjectProperty")]
        SubtotalResubordinatedLiabilitiesBalanceForSubjectProperty,
        
        [XmlEnum("SubtotalResubordinatedLiabilitiesMonthlyPaymentForSubjectProperty")]
        SubtotalResubordinatedLiabilitiesMonthlyPaymentForSubjectProperty,
        
        [XmlEnum("SubtotalSubjectPropertyLiensPaidByClosingBalance")]
        SubtotalSubjectPropertyLiensPaidByClosingBalance,
        
        [XmlEnum("SubtotalSubjectPropertyLiensPaidByClosingMonthlyPayment")]
        SubtotalSubjectPropertyLiensPaidByClosingMonthlyPayment,
        
        [XmlEnum("TotalLiabilitiesBalance")]
        TotalLiabilitiesBalance,
        
        [XmlEnum("TotalMonthlyIncomeNotIncludingNetRentalIncome")]
        TotalMonthlyIncomeNotIncludingNetRentalIncome,
        
        [XmlEnum("TotalPresentHousingExpense")]
        TotalPresentHousingExpense,
        
        [XmlEnum("UndrawnHELOC")]
        UndrawnHELOC,
    }
}
