namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum CreditErrorMessageSourceBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("CreditBureau")]
        CreditBureau,
        
        [XmlEnum("Equifax")]
        Equifax,
        
        [XmlEnum("Experian")]
        Experian,
        
        [XmlEnum("RepositoryBureau")]
        RepositoryBureau,
        
        [XmlEnum("TransUnion")]
        TransUnion,
    }
}
