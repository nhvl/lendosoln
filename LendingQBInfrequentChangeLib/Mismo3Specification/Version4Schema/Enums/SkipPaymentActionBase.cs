namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum SkipPaymentActionBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("CapitalizeInterest")]
        CapitalizeInterest,
        
        [XmlEnum("CreateSubordinateLoanComponent")]
        CreateSubordinateLoanComponent,
        
        [XmlEnum("ExtendTerm")]
        ExtendTerm,
        
        [XmlEnum("MakeupPayment")]
        MakeupPayment,
        
        [XmlEnum("Other")]
        Other,
    }
}
