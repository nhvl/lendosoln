namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum MISourceBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("FHA")]
        FHA,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PMI")]
        PMI,
        
        [XmlEnum("USDA")]
        USDA,
    }
}
