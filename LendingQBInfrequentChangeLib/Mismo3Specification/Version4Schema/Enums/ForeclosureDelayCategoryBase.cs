namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ForeclosureDelayCategoryBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("BankruptcyFiled")]
        BankruptcyFiled,
        
        [XmlEnum("ContestedOrLitigatedForeclosure")]
        ContestedOrLitigatedForeclosure,
        
        [XmlEnum("ImpactedPartyProcess")]
        ImpactedPartyProcess,
        
        [XmlEnum("LegalProcess")]
        LegalProcess,
        
        [XmlEnum("LegislativeChange")]
        LegislativeChange,
        
        [XmlEnum("Mediation")]
        Mediation,
        
        [XmlEnum("MilitaryIndulgence")]
        MilitaryIndulgence,
        
        [XmlEnum("MissingDocuments")]
        MissingDocuments,
        
        [XmlEnum("MonetaryTransactionPending")]
        MonetaryTransactionPending,
        
        [XmlEnum("Moratorium")]
        Moratorium,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Probate")]
        Probate,
        
        [XmlEnum("PropertyCondition")]
        PropertyCondition,
        
        [XmlEnum("PropertySeizure")]
        PropertySeizure,
        
        [XmlEnum("TitleIssue")]
        TitleIssue,
        
        [XmlEnum("WorkoutInReview")]
        WorkoutInReview,
    }
}
