namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum HMDAPurposeOfLoanBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("HomeImprovement")]
        HomeImprovement,
        
        [XmlEnum("HomePurchase")]
        HomePurchase,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Refinancing")]
        Refinancing,
    }
}
