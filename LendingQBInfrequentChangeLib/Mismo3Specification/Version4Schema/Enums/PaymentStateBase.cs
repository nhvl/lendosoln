namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PaymentStateBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Current")]
        Current,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Pending")]
        Pending,
        
        [XmlEnum("Previous")]
        Previous,
    }
}
