namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum UnitSaleRentalStatusBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("ForRent")]
        ForRent,
        
        [XmlEnum("ForSale")]
        ForSale,
        
        [XmlEnum("Rented")]
        Rented,
        
        [XmlEnum("Sold")]
        Sold,
    }
}
