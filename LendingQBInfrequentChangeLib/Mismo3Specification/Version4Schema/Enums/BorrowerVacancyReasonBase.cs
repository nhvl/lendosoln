namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum BorrowerVacancyReasonBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Abandoned")]
        Abandoned,
        
        [XmlEnum("CashForKeys")]
        CashForKeys,
        
        [XmlEnum("DeedInLieu")]
        DeedInLieu,
        
        [XmlEnum("Eviction")]
        Eviction,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Unknown")]
        Unknown,
    }
}
