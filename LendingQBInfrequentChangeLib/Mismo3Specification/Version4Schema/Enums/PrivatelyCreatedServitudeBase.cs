namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PrivatelyCreatedServitudeBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("DeedRestriction")]
        DeedRestriction,
        
        [XmlEnum("Easement")]
        Easement,
        
        [XmlEnum("License")]
        License,
        
        [XmlEnum("Other")]
        Other,
    }
}
