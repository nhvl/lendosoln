namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum CreditBusinessBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Advertising")]
        Advertising,
        
        [XmlEnum("Automotive")]
        Automotive,
        
        [XmlEnum("Banking")]
        Banking,
        
        [XmlEnum("Clothing")]
        Clothing,
        
        [XmlEnum("CollectionServices")]
        CollectionServices,
        
        [XmlEnum("Contractors")]
        Contractors,
        
        [XmlEnum("DepartmentAndMailOrder")]
        DepartmentAndMailOrder,
        
        [XmlEnum("Employment")]
        Employment,
        
        [XmlEnum("FarmAndGardenSupplies")]
        FarmAndGardenSupplies,
        
        [XmlEnum("Finance")]
        Finance,
        
        [XmlEnum("Government")]
        Government,
        
        [XmlEnum("Grocery")]
        Grocery,
        
        [XmlEnum("HomeFurnishing")]
        HomeFurnishing,
        
        [XmlEnum("Insurance")]
        Insurance,
        
        [XmlEnum("JewelryAndCamera")]
        JewelryAndCamera,
        
        [XmlEnum("LumberAndHardware")]
        LumberAndHardware,
        
        [XmlEnum("MedicalAndHealth")]
        MedicalAndHealth,
        
        [XmlEnum("Miscellaneous")]
        Miscellaneous,
        
        [XmlEnum("OilAndNationalCreditCards")]
        OilAndNationalCreditCards,
        
        [XmlEnum("PersonalServicesNotMedical")]
        PersonalServicesNotMedical,
        
        [XmlEnum("RealEstateAndPublicAccommodation")]
        RealEstateAndPublicAccommodation,
        
        [XmlEnum("SportingGoods")]
        SportingGoods,
        
        [XmlEnum("UtilitiesAndFuel")]
        UtilitiesAndFuel,
        
        [XmlEnum("Wholesale")]
        Wholesale,
    }
}
