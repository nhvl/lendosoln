namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PayoffApplicationSequenceBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("JustPayoff")]
        JustPayoff,
        
        [XmlEnum("NormalPaymentThenPayoff")]
        NormalPaymentThenPayoff,
    }
}
