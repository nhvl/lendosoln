namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum AbilityToRepayExemptionCreditorOrganizationBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("CommunityDevelopmentFinancialInstitution")]
        CommunityDevelopmentFinancialInstitution,
        
        [XmlEnum("CommunityHousingDevelopmentOrganization")]
        CommunityHousingDevelopmentOrganization,
        
        [XmlEnum("DownpaymentAssistanceProvider")]
        DownpaymentAssistanceProvider,
        
        [XmlEnum("NonProfitOrganization")]
        NonProfitOrganization,
        
        [XmlEnum("Other")]
        Other,
    }
}
