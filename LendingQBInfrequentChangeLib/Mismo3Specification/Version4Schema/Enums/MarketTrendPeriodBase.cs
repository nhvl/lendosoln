namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum MarketTrendPeriodBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("FutureEighteenMonths")]
        FutureEighteenMonths,
        
        [XmlEnum("FutureSixMonths")]
        FutureSixMonths,
        
        [XmlEnum("FutureThreeMonths")]
        FutureThreeMonths,
        
        [XmlEnum("FutureTwelveMonths")]
        FutureTwelveMonths,
        
        [XmlEnum("PriorEighteenMonths")]
        PriorEighteenMonths,
        
        [XmlEnum("PriorSixMonths")]
        PriorSixMonths,
        
        [XmlEnum("PriorThreeMonths")]
        PriorThreeMonths,
        
        [XmlEnum("PriorTwelveMonths")]
        PriorTwelveMonths,
    }
}
