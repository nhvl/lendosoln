namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum InsuranceClaimBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Hazard")]
        Hazard,
        
        [XmlEnum("MI")]
        MI,
        
        [XmlEnum("Other")]
        Other,
    }
}
