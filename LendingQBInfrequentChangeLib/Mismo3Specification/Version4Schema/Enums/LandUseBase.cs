namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum LandUseBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Agricultural")]
        Agricultural,
        
        [XmlEnum("Commercial")]
        Commercial,
        
        [XmlEnum("Income")]
        Income,
        
        [XmlEnum("Industrial")]
        Industrial,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PublicAndSemipublic")]
        PublicAndSemipublic,
        
        [XmlEnum("Recreational")]
        Recreational,
        
        [XmlEnum("Residential")]
        Residential,
        
        [XmlEnum("TransportationAndUtility")]
        TransportationAndUtility,
        
        [XmlEnum("Vacant")]
        Vacant,
    }
}
