namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum InvestorRemittanceBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("ActualInterestActualPrincipal")]
        ActualInterestActualPrincipal,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("ScheduledInterestActualPrincipal")]
        ScheduledInterestActualPrincipal,
        
        [XmlEnum("ScheduledInterestScheduledPrincipal")]
        ScheduledInterestScheduledPrincipal,
    }
}
