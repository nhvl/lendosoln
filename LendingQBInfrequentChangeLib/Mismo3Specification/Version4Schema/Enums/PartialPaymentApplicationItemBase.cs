namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PartialPaymentApplicationItemBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Escrow")]
        Escrow,
        
        [XmlEnum("Interest")]
        Interest,
        
        [XmlEnum("Principal")]
        Principal,
    }
}
