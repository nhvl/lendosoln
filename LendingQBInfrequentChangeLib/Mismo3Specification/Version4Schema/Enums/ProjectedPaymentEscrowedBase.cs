namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ProjectedPaymentEscrowedBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Escrowed")]
        Escrowed,
        
        [XmlEnum("NotEscrowed")]
        NotEscrowed,
        
        [XmlEnum("SomeEscrowed")]
        SomeEscrowed,
    }
}
