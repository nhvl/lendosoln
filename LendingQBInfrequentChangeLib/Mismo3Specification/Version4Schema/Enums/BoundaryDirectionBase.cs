namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum BoundaryDirectionBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("East")]
        East,
        
        [XmlEnum("North")]
        North,
        
        [XmlEnum("NorthEast")]
        NorthEast,
        
        [XmlEnum("NorthWest")]
        NorthWest,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("South")]
        South,
        
        [XmlEnum("SouthEast")]
        SouthEast,
        
        [XmlEnum("SouthWest")]
        SouthWest,
        
        [XmlEnum("West")]
        West,
    }
}
