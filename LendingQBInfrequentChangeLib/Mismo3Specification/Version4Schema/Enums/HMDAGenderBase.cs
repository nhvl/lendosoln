﻿namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum HMDAGenderBase
    {
        [XmlEnum("")]
        LeaveBlank,

        [XmlEnum(nameof(ApplicantHasSelectedBothMaleAndFemale))]
        ApplicantHasSelectedBothMaleAndFemale,

        [XmlEnum(nameof(Female))]
        Female,

        [XmlEnum(nameof(InformationNotProvidedUnknown))]
        InformationNotProvidedUnknown,

        [XmlEnum(nameof(Male))]
        Male,

        [XmlEnum(nameof(NotApplicable))]
        NotApplicable
    }
}
