namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum VendorRelationshipBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Independent")]
        Independent,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Staff")]
        Staff,
    }
}
