namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum LitigationStatusBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Filed")]
        Filed,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Resolved")]
        Resolved,
        
        [XmlEnum("Threatened")]
        Threatened,
    }
}
