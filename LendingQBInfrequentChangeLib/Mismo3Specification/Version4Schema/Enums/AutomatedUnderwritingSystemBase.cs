namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum AutomatedUnderwritingSystemBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Assetwise")]
        Assetwise,
        
        [XmlEnum("Capstone")]
        Capstone,
        
        [XmlEnum("Clues")]
        Clues,
        
        [XmlEnum("DecisionEngine")]
        DecisionEngine,
        
        [XmlEnum("DesktopUnderwriter")]
        DesktopUnderwriter,
        
        [XmlEnum("ECS")]
        ECS,
        
        [XmlEnum("FHAScorecard")]
        FHAScorecard,
        
        [XmlEnum("FirstMortgageCreditScore")]
        FirstMortgageCreditScore,
        
        [XmlEnum("GuaranteedUnderwritingSystem")]
        GuaranteedUnderwritingSystem,
        
        [XmlEnum("LoanProspector")]
        LoanProspector,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Strategyware")]
        Strategyware,
        
        [XmlEnum("Zippy")]
        Zippy,
    }
}
