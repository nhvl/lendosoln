namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum FREPurchaseEligibilityBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("FreddieMacEligible")]
        FreddieMacEligible,
        
        [XmlEnum("FreddieMacIneligible")]
        FreddieMacIneligible,
    }
}
