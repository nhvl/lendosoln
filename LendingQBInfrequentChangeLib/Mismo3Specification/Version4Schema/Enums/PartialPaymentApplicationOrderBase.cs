namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PartialPaymentApplicationOrderBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("First")]
        First,
        
        [XmlEnum("Second")]
        Second,
        
        [XmlEnum("Third")]
        Third,
    }
}
