namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ProjectManagementBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("CooperativeBoard")]
        CooperativeBoard,
        
        [XmlEnum("Developer")]
        Developer,
        
        [XmlEnum("HomeownersAssociation")]
        HomeownersAssociation,
        
        [XmlEnum("ManagementAgent")]
        ManagementAgent,
        
        [XmlEnum("Other")]
        Other,
    }
}
