namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum VAFundingFeeExemptionBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("CompletelyExempt")]
        CompletelyExempt,
        
        [XmlEnum("NotExempt")]
        NotExempt,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PartiallyExempt")]
        PartiallyExempt,
    }
}
