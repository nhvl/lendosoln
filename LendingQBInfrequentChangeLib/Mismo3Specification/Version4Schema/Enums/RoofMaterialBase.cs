namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum RoofMaterialBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Aluminum")]
        Aluminum,
        
        [XmlEnum("Asphalt")]
        Asphalt,
        
        [XmlEnum("Clay")]
        Clay,
        
        [XmlEnum("Composition")]
        Composition,
        
        [XmlEnum("Concrete")]
        Concrete,
        
        [XmlEnum("FireRetardant")]
        FireRetardant,
        
        [XmlEnum("Gravel")]
        Gravel,
        
        [XmlEnum("Metal")]
        Metal,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Roll")]
        Roll,
        
        [XmlEnum("Shake")]
        Shake,
        
        [XmlEnum("Shingle")]
        Shingle,
        
        [XmlEnum("Slate")]
        Slate,
        
        [XmlEnum("SpanishTile")]
        SpanishTile,
        
        [XmlEnum("Stone")]
        Stone,
        
        [XmlEnum("Synthetic")]
        Synthetic,
        
        [XmlEnum("Tar")]
        Tar,
        
        [XmlEnum("Tile")]
        Tile,
        
        [XmlEnum("Tin")]
        Tin,
        
        [XmlEnum("Wood")]
        Wood,
    }
}
