namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum UniqueDwellingBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("EarthShelterHome")]
        EarthShelterHome,
        
        [XmlEnum("GeodesicDome")]
        GeodesicDome,
        
        [XmlEnum("Houseboat")]
        Houseboat,
        
        [XmlEnum("LogHome")]
        LogHome,
        
        [XmlEnum("Other")]
        Other,
    }
}
