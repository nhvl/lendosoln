namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum HazardInsuranceNonStandardPolicyBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Application")]
        Application,
        
        [XmlEnum("Binder")]
        Binder,
        
        [XmlEnum("Bridge")]
        Bridge,
        
        [XmlEnum("Condo")]
        Condo,
        
        [XmlEnum("CondominiumPUDMasterPolicy")]
        CondominiumPUDMasterPolicy,
        
        [XmlEnum("FairPlan")]
        FairPlan,
        
        [XmlEnum("ForcePlaced")]
        ForcePlaced,
        
        [XmlEnum("Other")]
        Other,
    }
}
