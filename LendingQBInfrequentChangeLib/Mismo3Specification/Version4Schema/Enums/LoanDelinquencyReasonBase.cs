namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum LoanDelinquencyReasonBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AbandonmentOfProperty")]
        AbandonmentOfProperty,
        
        [XmlEnum("BusinessFailure")]
        BusinessFailure,
        
        [XmlEnum("CasualtyLoss")]
        CasualtyLoss,
        
        [XmlEnum("CurtailmentOfIncome")]
        CurtailmentOfIncome,
        
        [XmlEnum("DeathOfMortgagorFamilyMember")]
        DeathOfMortgagorFamilyMember,
        
        [XmlEnum("DeathOfPrincipalMortgagor")]
        DeathOfPrincipalMortgagor,
        
        [XmlEnum("Disaster")]
        Disaster,
        
        [XmlEnum("DistantEmploymentTransfer")]
        DistantEmploymentTransfer,
        
        [XmlEnum("EnergyEnvironmentCost")]
        EnergyEnvironmentCost,
        
        [XmlEnum("ExcessiveObligations")]
        ExcessiveObligations,
        
        [XmlEnum("Fraud")]
        Fraud,
        
        [XmlEnum("IllnessOfMortgagorFamilyMember")]
        IllnessOfMortgagorFamilyMember,
        
        [XmlEnum("IllnessOfPrincipalMortgagor")]
        IllnessOfPrincipalMortgagor,
        
        [XmlEnum("InabilityToRentProperty")]
        InabilityToRentProperty,
        
        [XmlEnum("InabilityToSellProperty")]
        InabilityToSellProperty,
        
        [XmlEnum("Incarceration")]
        Incarceration,
        
        [XmlEnum("MaritalDifficulties")]
        MaritalDifficulties,
        
        [XmlEnum("MilitaryService")]
        MilitaryService,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PaymentAdjustment")]
        PaymentAdjustment,
        
        [XmlEnum("PaymentDispute")]
        PaymentDispute,
        
        [XmlEnum("PropertyProblem")]
        PropertyProblem,
        
        [XmlEnum("SeparationOfBorrowersUnrelatedToMarriage")]
        SeparationOfBorrowersUnrelatedToMarriage,
        
        [XmlEnum("ServicingProblems")]
        ServicingProblems,
        
        [XmlEnum("ThirdPartyOccupantFiledBankruptcy")]
        ThirdPartyOccupantFiledBankruptcy,
        
        [XmlEnum("TransferOfOwnershipPending")]
        TransferOfOwnershipPending,
        
        [XmlEnum("UnableToContactBorrower")]
        UnableToContactBorrower,
        
        [XmlEnum("Underemployment")]
        Underemployment,
        
        [XmlEnum("Unemployment")]
        Unemployment,
    }
}
