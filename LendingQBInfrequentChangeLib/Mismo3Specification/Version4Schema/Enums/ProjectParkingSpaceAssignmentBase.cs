namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ProjectParkingSpaceAssignmentBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Assigned")]
        Assigned,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Owned")]
        Owned,
    }
}
