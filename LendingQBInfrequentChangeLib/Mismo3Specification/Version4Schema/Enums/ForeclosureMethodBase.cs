namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ForeclosureMethodBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Judicial")]
        Judicial,
        
        [XmlEnum("NonJudicial")]
        NonJudicial,
        
        [XmlEnum("Other")]
        Other,
    }
}
