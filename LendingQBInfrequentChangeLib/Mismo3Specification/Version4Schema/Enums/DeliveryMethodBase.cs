namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum DeliveryMethodBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("CommercialCarrier")]
        CommercialCarrier,
        
        [XmlEnum("ElectronicDelivery")]
        ElectronicDelivery,
        
        [XmlEnum("Email")]
        Email,
        
        [XmlEnum("Facsimile")]
        Facsimile,
        
        [XmlEnum("InPerson")]
        InPerson,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Telephone")]
        Telephone,
        
        [XmlEnum("TTY")]
        TTY,
        
        [XmlEnum("USPS")]
        USPS,
    }
}
