namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum JudgmentBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Consent")]
        Consent,
        
        [XmlEnum("Default")]
        Default,
        
        [XmlEnum("ForeclosureBySale")]
        ForeclosureBySale,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Renewal")]
        Renewal,
        
        [XmlEnum("StrictForeclosure")]
        StrictForeclosure,
        
        [XmlEnum("SummaryJudgment")]
        SummaryJudgment,
        
        [XmlEnum("WritOfExecution")]
        WritOfExecution,
    }
}
