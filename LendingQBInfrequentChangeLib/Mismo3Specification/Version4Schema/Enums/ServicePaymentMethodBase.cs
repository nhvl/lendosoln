namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ServicePaymentMethodBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("ACH")]
        ACH,
        
        [XmlEnum("CreditCard")]
        CreditCard,
        
        [XmlEnum("DebitCard")]
        DebitCard,
        
        [XmlEnum("OnAccount")]
        OnAccount,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Voucher")]
        Voucher,
        
        [XmlEnum("Wire")]
        Wire,
    }
}
