namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum FeePaymentProcessBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Closing")]
        Closing,
        
        [XmlEnum("Processing")]
        Processing,
    }
}
