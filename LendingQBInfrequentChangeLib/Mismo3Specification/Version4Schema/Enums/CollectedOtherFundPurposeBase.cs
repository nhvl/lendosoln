namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum CollectedOtherFundPurposeBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AdvancePITIPayment")]
        AdvancePITIPayment,
        
        [XmlEnum("EscrowFunds")]
        EscrowFunds,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PrincipalCurtailment")]
        PrincipalCurtailment,
    }
}
