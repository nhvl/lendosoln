namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum CreditLiabilityTermsSourceBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Calculated")]
        Calculated,
        
        [XmlEnum("Provided")]
        Provided,
    }
}
