namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum BorrowerResidencyBasisBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("LivingRentFree")]
        LivingRentFree,
        
        [XmlEnum("Own")]
        Own,
        
        [XmlEnum("Rent")]
        Rent,
        
        [XmlEnum("Unknown")]
        Unknown,
    }
}
