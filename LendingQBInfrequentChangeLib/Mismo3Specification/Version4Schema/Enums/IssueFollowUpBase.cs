namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum IssueFollowUpBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Conversation")]
        Conversation,
        
        [XmlEnum("ExtensionOfTimeRequest")]
        ExtensionOfTimeRequest,
        
        [XmlEnum("InterimActionUpdate")]
        InterimActionUpdate,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Research")]
        Research,
        
        [XmlEnum("WrittenInformationRequest")]
        WrittenInformationRequest,
    }
}
