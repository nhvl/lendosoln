namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum BorrowerClassificationBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Primary")]
        Primary,
        
        [XmlEnum("Secondary")]
        Secondary,
    }
}
