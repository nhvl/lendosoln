namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum AdvanceBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Appraisal")]
        Appraisal,
        
        [XmlEnum("Escrow")]
        Escrow,
        
        [XmlEnum("HomeownersAssociationDues")]
        HomeownersAssociationDues,
        
        [XmlEnum("Interest")]
        Interest,
        
        [XmlEnum("LegalFees")]
        LegalFees,
        
        [XmlEnum("MI")]
        MI,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Principal")]
        Principal,
        
        [XmlEnum("PropertyInspection")]
        PropertyInspection,
        
        [XmlEnum("PropertyInsurance")]
        PropertyInsurance,
        
        [XmlEnum("PropertyPreservation")]
        PropertyPreservation,
        
        [XmlEnum("PropertyTaxes")]
        PropertyTaxes,
        
        [XmlEnum("ReliefOverpaymentReimbursement")]
        ReliefOverpaymentReimbursement,
        
        [XmlEnum("ReverseMortgagePaymentPlanChange")]
        ReverseMortgagePaymentPlanChange,
    }
}
