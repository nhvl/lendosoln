namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum FHAAlimonyLiabilityTreatmentBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AdditionToDebt")]
        AdditionToDebt,
        
        [XmlEnum("ReductionToIncome")]
        ReductionToIncome,
    }
}
