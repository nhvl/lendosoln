namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum MIApplicationRequestBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("OriginalRequest")]
        OriginalRequest,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Resubmission")]
        Resubmission,
    }
}
