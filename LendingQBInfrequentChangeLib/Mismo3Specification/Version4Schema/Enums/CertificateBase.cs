namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum CertificateBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("OccupancyCertification")]
        OccupancyCertification,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("UsageCertification")]
        UsageCertification,
    }
}
