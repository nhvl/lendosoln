namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum TILPeriodDurationBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Biweekly")]
        Biweekly,
        
        [XmlEnum("Monthly")]
        Monthly,
        
        [XmlEnum("Yearly")]
        Yearly,
    }
}
