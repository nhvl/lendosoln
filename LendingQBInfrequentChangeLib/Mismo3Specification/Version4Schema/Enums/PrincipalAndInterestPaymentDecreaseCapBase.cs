namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PrincipalAndInterestPaymentDecreaseCapBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Mandatory")]
        Mandatory,
        
        [XmlEnum("Optional")]
        Optional,
    }
}
