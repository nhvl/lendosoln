namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum MarketInventoryMonthRangeBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("LastThreeMonths")]
        LastThreeMonths,
        
        [XmlEnum("PriorFourToSixMonths")]
        PriorFourToSixMonths,
        
        [XmlEnum("PriorSevenToTwelveMonths")]
        PriorSevenToTwelveMonths,
    }
}
