namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum LoanMarginCalculationMethodBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("BasedOnProduct")]
        BasedOnProduct,
        
        [XmlEnum("Constant")]
        Constant,
    }
}
