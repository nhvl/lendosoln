namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum CommunityLendingProductBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("HFAPreferred")]
        HFAPreferred,
        
        [XmlEnum("HFAPreferredRiskSharing")]
        HFAPreferredRiskSharing,
        
        [XmlEnum("HomeReady")]
        HomeReady,
        
        [XmlEnum("MyCommunityMortgage")]
        MyCommunityMortgage,
        
        [XmlEnum("Other")]
        Other,
    }
}
