namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PRIAResponseBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("FeeQuote")]
        FeeQuote,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Received")]
        Received,
        
        [XmlEnum("Recorded")]
        Recorded,
        
        [XmlEnum("Rejected")]
        Rejected,
    }
}
