namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum LoanDelinquencyEventBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("BorrowerEmailSent")]
        BorrowerEmailSent,
        
        [XmlEnum("BorrowerFaceToFaceInterviewConducted")]
        BorrowerFaceToFaceInterviewConducted,
        
        [XmlEnum("BorrowerFormLetterSent")]
        BorrowerFormLetterSent,
        
        [XmlEnum("BreachLetterExpires")]
        BreachLetterExpires,
        
        [XmlEnum("BreachLetterSent")]
        BreachLetterSent,
        
        [XmlEnum("BrokerPriceOpinionOrAppraisalObtained")]
        BrokerPriceOpinionOrAppraisalObtained,
        
        [XmlEnum("BrokerPriceOpinionOrdered")]
        BrokerPriceOpinionOrdered,
        
        [XmlEnum("ChapterElevenBankruptcyPlanConfirmed")]
        ChapterElevenBankruptcyPlanConfirmed,
        
        [XmlEnum("ChapterElevenCourtClearanceObtained")]
        ChapterElevenCourtClearanceObtained,
        
        [XmlEnum("ChapterElevenPropertySurrendered")]
        ChapterElevenPropertySurrendered,
        
        [XmlEnum("ChapterSevenBankruptcyAssetsPursued")]
        ChapterSevenBankruptcyAssetsPursued,
        
        [XmlEnum("ChapterSevenCourtClearanceObtained")]
        ChapterSevenCourtClearanceObtained,
        
        [XmlEnum("ChapterSevenPropertySurrendered")]
        ChapterSevenPropertySurrendered,
        
        [XmlEnum("ChapterThirteenBankruptcyPlanConfirmed")]
        ChapterThirteenBankruptcyPlanConfirmed,
        
        [XmlEnum("ChapterThirteenCourtClearanceObtained")]
        ChapterThirteenCourtClearanceObtained,
        
        [XmlEnum("ChapterThirteenPropertySurrendered")]
        ChapterThirteenPropertySurrendered,
        
        [XmlEnum("ChapterTwelveBankruptcyPlanConfirmed")]
        ChapterTwelveBankruptcyPlanConfirmed,
        
        [XmlEnum("ChapterTwelveCourtClearanceObtained")]
        ChapterTwelveCourtClearanceObtained,
        
        [XmlEnum("ChapterTwelvePropertySurrendered")]
        ChapterTwelvePropertySurrendered,
        
        [XmlEnum("DeedInLieu")]
        DeedInLieu,
        
        [XmlEnum("EligibleForHAMP")]
        EligibleForHAMP,
        
        [XmlEnum("FinalJudgmentRecorded")]
        FinalJudgmentRecorded,
        
        [XmlEnum("FirstLegalAction")]
        FirstLegalAction,
        
        [XmlEnum("FirstRightPartyContact")]
        FirstRightPartyContact,
        
        [XmlEnum("ForbearanceBegin")]
        ForbearanceBegin,
        
        [XmlEnum("ForbearanceEnd")]
        ForbearanceEnd,
        
        [XmlEnum("ForeclosureOnHold")]
        ForeclosureOnHold,
        
        [XmlEnum("ForeclosureRestarted")]
        ForeclosureRestarted,
        
        [XmlEnum("ForeclosureSaleCancelled")]
        ForeclosureSaleCancelled,
        
        [XmlEnum("ForeclosureSaleConfirmed")]
        ForeclosureSaleConfirmed,
        
        [XmlEnum("ForeclosureSaleContinued")]
        ForeclosureSaleContinued,
        
        [XmlEnum("ForeclosureSaleHeld")]
        ForeclosureSaleHeld,
        
        [XmlEnum("ForeclosureSaleRescinded")]
        ForeclosureSaleRescinded,
        
        [XmlEnum("ForeclosureSaleScheduled")]
        ForeclosureSaleScheduled,
        
        [XmlEnum("FullReinstatement")]
        FullReinstatement,
        
        [XmlEnum("HAMPBorrowerResponsePackageEvaluated")]
        HAMPBorrowerResponsePackageEvaluated,
        
        [XmlEnum("HAMPDeclinedByBorrower")]
        HAMPDeclinedByBorrower,
        
        [XmlEnum("HAMPInReview")]
        HAMPInReview,
        
        [XmlEnum("HAMPModificationAgreementReceived")]
        HAMPModificationAgreementReceived,
        
        [XmlEnum("HAMPModificationAgreementSent")]
        HAMPModificationAgreementSent,
        
        [XmlEnum("HAMPStipulationDocumentsReceived")]
        HAMPStipulationDocumentsReceived,
        
        [XmlEnum("HardshipAffidavitReceived")]
        HardshipAffidavitReceived,
        
        [XmlEnum("IneligibleForModification")]
        IneligibleForModification,
        
        [XmlEnum("IneligibleForShortSale")]
        IneligibleForShortSale,
        
        [XmlEnum("InvestorREONotificationSent")]
        InvestorREONotificationSent,
        
        [XmlEnum("LastOutboundCallAttemptToBorrower")]
        LastOutboundCallAttemptToBorrower,
        
        [XmlEnum("LastRightPartyContact")]
        LastRightPartyContact,
        
        [XmlEnum("LastSkipTraceAttempt")]
        LastSkipTraceAttempt,
        
        [XmlEnum("ModificationInReview")]
        ModificationInReview,
        
        [XmlEnum("ModificationNotificationDenied")]
        ModificationNotificationDenied,
        
        [XmlEnum("ModificationTrialBegin")]
        ModificationTrialBegin,
        
        [XmlEnum("ModificationTrialEnd")]
        ModificationTrialEnd,
        
        [XmlEnum("NoActivity")]
        NoActivity,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PartialReinstatement")]
        PartialReinstatement,
        
        [XmlEnum("PositiveSolicitationResponse")]
        PositiveSolicitationResponse,
        
        [XmlEnum("PromiseToPayBroken")]
        PromiseToPayBroken,
        
        [XmlEnum("PromiseToPayKept")]
        PromiseToPayKept,
        
        [XmlEnum("PromiseToPayMade")]
        PromiseToPayMade,
        
        [XmlEnum("PropertyIsListedForSale")]
        PropertyIsListedForSale,
        
        [XmlEnum("ReinitiateForeclosure")]
        ReinitiateForeclosure,
        
        [XmlEnum("REO")]
        REO,
        
        [XmlEnum("RepaymentBegin")]
        RepaymentBegin,
        
        [XmlEnum("RepaymentEnd")]
        RepaymentEnd,
        
        [XmlEnum("RepaymentPlanFirstPlanPaymentMade")]
        RepaymentPlanFirstPlanPaymentMade,
        
        [XmlEnum("RepaymentPlanInReview")]
        RepaymentPlanInReview,
        
        [XmlEnum("RightPartyContactNextContactScheduled")]
        RightPartyContactNextContactScheduled,
        
        [XmlEnum("RightPartyContactNoAction")]
        RightPartyContactNoAction,
        
        [XmlEnum("ServicerSinglePointOfContactAssigned")]
        ServicerSinglePointOfContactAssigned,
        
        [XmlEnum("ShortSaleAgreementSigned")]
        ShortSaleAgreementSigned,
        
        [XmlEnum("ShortSaleCompleted")]
        ShortSaleCompleted,
        
        [XmlEnum("ShortSaleContractReceived")]
        ShortSaleContractReceived,
        
        [XmlEnum("ShortSaleInReview")]
        ShortSaleInReview,
        
        [XmlEnum("SkipTraceInitiated")]
        SkipTraceInitiated,
        
        [XmlEnum("SkipTraceSuccessful")]
        SkipTraceSuccessful,
        
        [XmlEnum("SolicitationLetterSent")]
        SolicitationLetterSent,
        
        [XmlEnum("ThirdPartySale")]
        ThirdPartySale,
        
        [XmlEnum("TitleClaimResolutionPursed")]
        TitleClaimResolutionPursed,
        
        [XmlEnum("UnsuccessfulRightPartyContactPhoneContactAttempt")]
        UnsuccessfulRightPartyContactPhoneContactAttempt,
        
        [XmlEnum("UpdatedBorrowerFinancialsReceived")]
        UpdatedBorrowerFinancialsReceived,
        
        [XmlEnum("WorkoutEvaluationComplete")]
        WorkoutEvaluationComplete,
        
        [XmlEnum("WorkoutInReview")]
        WorkoutInReview,
        
        [XmlEnum("WorkoutPackageCompleted")]
        WorkoutPackageCompleted,
    }
}
