namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum DefinitionOfValueBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AsIsValue")]
        AsIsValue,
        
        [XmlEnum("AsRepaired")]
        AsRepaired,
        
        [XmlEnum("ForecastValue")]
        ForecastValue,
        
        [XmlEnum("MarketValue")]
        MarketValue,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("QuickSaleValue")]
        QuickSaleValue,
        
        [XmlEnum("RetrospectiveValue")]
        RetrospectiveValue,
    }
}
