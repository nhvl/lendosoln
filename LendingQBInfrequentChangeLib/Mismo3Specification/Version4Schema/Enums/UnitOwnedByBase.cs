namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum UnitOwnedByBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Developer")]
        Developer,
        
        [XmlEnum("Investor")]
        Investor,
        
        [XmlEnum("OwnerOccupier")]
        OwnerOccupier,
    }
}
