namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum LoanIdentifierBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AgencyCase")]
        AgencyCase,
        
        [XmlEnum("InvestorCommitment")]
        InvestorCommitment,
        
        [XmlEnum("InvestorContract")]
        InvestorContract,
        
        [XmlEnum("InvestorLoan")]
        InvestorLoan,
        
        [XmlEnum("InvestorWorkoutCase")]
        InvestorWorkoutCase,
        
        [XmlEnum("LenderCase")]
        LenderCase,
        
        [XmlEnum("LenderLoan")]
        LenderLoan,
        
        [XmlEnum("LoanPriceQuote")]
        LoanPriceQuote,
        
        [XmlEnum("MERS_MIN")]
        MERS_MIN,
        
        [XmlEnum("MIRateQuote")]
        MIRateQuote,
        
        [XmlEnum("NewServicerLoan")]
        NewServicerLoan,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PoolIssuerLoan")]
        PoolIssuerLoan,
        
        [XmlEnum("PriceResponse")]
        PriceResponse,
        
        [XmlEnum("SellerLoan")]
        SellerLoan,
        
        [XmlEnum("ServicerLoan")]
        ServicerLoan,
        
        [XmlEnum("ServicerWorkoutCase")]
        ServicerWorkoutCase,
        
        [XmlEnum("SubservicerLoan")]
        SubservicerLoan,
        
        [XmlEnum("UniversalLoan")]
        UniversalLoan,
        
        [XmlEnum("WholesaleLenderLoan")]
        WholesaleLenderLoan,
    }
}
