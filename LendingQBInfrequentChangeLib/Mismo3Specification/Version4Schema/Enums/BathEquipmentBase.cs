namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum BathEquipmentBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AutomaticVentSystem")]
        AutomaticVentSystem,
        
        [XmlEnum("DualSinks")]
        DualSinks,
        
        [XmlEnum("GardenTub")]
        GardenTub,
        
        [XmlEnum("HeatedFloors")]
        HeatedFloors,
        
        [XmlEnum("HeatLamps")]
        HeatLamps,
        
        [XmlEnum("LowFlowShowerheads")]
        LowFlowShowerheads,
        
        [XmlEnum("LowFlushToliet")]
        LowFlushToliet,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("RomanShower")]
        RomanShower,
        
        [XmlEnum("SingleSink")]
        SingleSink,
        
        [XmlEnum("SolidSurfaceManmadeCountertop")]
        SolidSurfaceManmadeCountertop,
        
        [XmlEnum("SolidSurfaceNaturalCountertop")]
        SolidSurfaceNaturalCountertop,
        
        [XmlEnum("StallShower")]
        StallShower,
        
        [XmlEnum("SteamShower")]
        SteamShower,
        
        [XmlEnum("TubShower")]
        TubShower,
        
        [XmlEnum("Vanity")]
        Vanity,
        
        [XmlEnum("WaterEffecientFaucet")]
        WaterEffecientFaucet,
    }
}
