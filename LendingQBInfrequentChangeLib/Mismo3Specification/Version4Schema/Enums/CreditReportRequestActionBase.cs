namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum CreditReportRequestActionBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("ForceNew")]
        ForceNew,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Reissue")]
        Reissue,
        
        [XmlEnum("Retrieve")]
        Retrieve,
        
        [XmlEnum("SecondaryUseNotification")]
        SecondaryUseNotification,
        
        [XmlEnum("StatusQuery")]
        StatusQuery,
        
        [XmlEnum("Submit")]
        Submit,
        
        [XmlEnum("Update")]
        Update,
        
        [XmlEnum("Upgrade")]
        Upgrade,
    }
}
