namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum CreditorServicingOfLoanStatementBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("CreditorIntendsToServiceLoan")]
        CreditorIntendsToServiceLoan,
        
        [XmlEnum("CreditorIntendsToTransferServicingOfLoan")]
        CreditorIntendsToTransferServicingOfLoan,
        
        [XmlEnum("CreditorMayAssignSellOrTransferServicingOfLoan")]
        CreditorMayAssignSellOrTransferServicingOfLoan,
        
        [XmlEnum("Other")]
        Other,
    }
}
