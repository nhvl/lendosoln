namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ProjectLegalStructureBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("CommonInterestApartment")]
        CommonInterestApartment,
        
        [XmlEnum("Condominium")]
        Condominium,
        
        [XmlEnum("Cooperative")]
        Cooperative,
        
        [XmlEnum("Unknown")]
        Unknown,
    }
}
