namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum AVMOutcomeBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("IndicatedValue")]
        IndicatedValue,
        
        [XmlEnum("NetValue")]
        NetValue,
        
        [XmlEnum("Other")]
        Other,
    }
}
