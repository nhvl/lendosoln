namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum HighCostJurisdictionBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("City")]
        City,
        
        [XmlEnum("County")]
        County,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("State")]
        State,
    }
}
