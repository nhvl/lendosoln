namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum IrrigationSystemSourceBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("CatchmentSystemFed")]
        CatchmentSystemFed,
        
        [XmlEnum("CisternFed")]
        CisternFed,
        
        [XmlEnum("CityWaterFed")]
        CityWaterFed,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("ReclaimedWater")]
        ReclaimedWater,
        
        [XmlEnum("WellFed")]
        WellFed,
    }
}
