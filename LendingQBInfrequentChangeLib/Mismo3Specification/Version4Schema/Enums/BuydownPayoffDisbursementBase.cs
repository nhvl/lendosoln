namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum BuydownPayoffDisbursementBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("ApplyToPrincipal")]
        ApplyToPrincipal,
        
        [XmlEnum("Unknown")]
        Unknown,
    }
}
