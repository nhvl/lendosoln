namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ManufacturedHomeConstructionQualityRatingBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Average")]
        Average,
        
        [XmlEnum("Excellent")]
        Excellent,
        
        [XmlEnum("Fair")]
        Fair,
        
        [XmlEnum("Poor")]
        Poor,
    }
}
