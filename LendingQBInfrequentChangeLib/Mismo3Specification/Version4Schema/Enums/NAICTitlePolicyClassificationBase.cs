namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum NAICTitlePolicyClassificationBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("NonResidential")]
        NonResidential,
        
        [XmlEnum("Residential")]
        Residential,
    }
}
