namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ContractVarianceCodeIssuerBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("FHA")]
        FHA,
        
        [XmlEnum("Investor")]
        Investor,
        
        [XmlEnum("MI")]
        MI,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Seller")]
        Seller,
        
        [XmlEnum("VA")]
        VA,
    }
}
