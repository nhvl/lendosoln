namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum TreasuryNPVLoanAmortizationBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AdjustableRate")]
        AdjustableRate,
        
        [XmlEnum("FixedRate")]
        FixedRate,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Step")]
        Step,
    }
}
