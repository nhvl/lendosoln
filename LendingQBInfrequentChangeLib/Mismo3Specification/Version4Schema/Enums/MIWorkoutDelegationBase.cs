namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum MIWorkoutDelegationBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("InvestorDelegated")]
        InvestorDelegated,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("ServicerDelegated")]
        ServicerDelegated,
    }
}
