namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ParcelIdentificationBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AssessorUnformattedIdentifier")]
        AssessorUnformattedIdentifier,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("ParcelIdentificationNumber")]
        ParcelIdentificationNumber,
        
        [XmlEnum("TaxMapIdentifier")]
        TaxMapIdentifier,
        
        [XmlEnum("TaxParcelIdentifier")]
        TaxParcelIdentifier,
        
        [XmlEnum("TorrensCertificateIdentifier")]
        TorrensCertificateIdentifier,
    }
}
