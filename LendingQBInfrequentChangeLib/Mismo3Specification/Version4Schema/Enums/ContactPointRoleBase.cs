namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ContactPointRoleBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Home")]
        Home,
        
        [XmlEnum("Mobile")]
        Mobile,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Work")]
        Work,
    }
}
