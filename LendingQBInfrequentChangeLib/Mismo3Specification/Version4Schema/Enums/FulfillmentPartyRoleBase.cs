namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum FulfillmentPartyRoleBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Notary")]
        Notary,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("TitleAgent")]
        TitleAgent,
    }
}
