namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ExpenseClaimItemInsurancePlacedBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Homeowner")]
        Homeowner,
        
        [XmlEnum("Lender")]
        Lender,
        
        [XmlEnum("Other")]
        Other,
    }
}
