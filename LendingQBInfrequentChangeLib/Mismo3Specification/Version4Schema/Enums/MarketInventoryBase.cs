namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum MarketInventoryBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AbsorptionRate")]
        AbsorptionRate,
        
        [XmlEnum("MedianListDaysOnMarket")]
        MedianListDaysOnMarket,
        
        [XmlEnum("MedianListPrice")]
        MedianListPrice,
        
        [XmlEnum("MedianSalesDaysOnMarket")]
        MedianSalesDaysOnMarket,
        
        [XmlEnum("MedianSalesPrice")]
        MedianSalesPrice,
        
        [XmlEnum("MedianSalesToListRatio")]
        MedianSalesToListRatio,
        
        [XmlEnum("Supply")]
        Supply,
        
        [XmlEnum("TotalListings")]
        TotalListings,
        
        [XmlEnum("TotalSales")]
        TotalSales,
    }
}
