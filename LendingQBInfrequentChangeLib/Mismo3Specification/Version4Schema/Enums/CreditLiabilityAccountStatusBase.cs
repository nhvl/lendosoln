namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum CreditLiabilityAccountStatusBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Closed")]
        Closed,
        
        [XmlEnum("Frozen")]
        Frozen,
        
        [XmlEnum("Open")]
        Open,
        
        [XmlEnum("Paid")]
        Paid,
        
        [XmlEnum("Refinanced")]
        Refinanced,
        
        [XmlEnum("Transferred")]
        Transferred,
    }
}
