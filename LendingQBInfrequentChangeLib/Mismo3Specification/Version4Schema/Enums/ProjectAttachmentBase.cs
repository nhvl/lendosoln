namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ProjectAttachmentBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Attached")]
        Attached,
        
        [XmlEnum("Detached")]
        Detached,
    }
}
