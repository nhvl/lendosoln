namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum InterestAccrualBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AverageDailyInterestAccrual")]
        AverageDailyInterestAccrual,
        
        [XmlEnum("DailyInterestAccrual")]
        DailyInterestAccrual,
    }
}
