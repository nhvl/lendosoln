namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum VALoanProcedureBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Automatic")]
        Automatic,
        
        [XmlEnum("AutomaticInterestRateReductionRefinanceLoan")]
        AutomaticInterestRateReductionRefinanceLoan,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("VAPriorApproval")]
        VAPriorApproval,
    }
}
