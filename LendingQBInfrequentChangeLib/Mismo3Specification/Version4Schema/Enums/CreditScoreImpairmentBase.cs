namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum CreditScoreImpairmentBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("ImmaterialDisputedAccount")]
        ImmaterialDisputedAccount,
        
        [XmlEnum("InsufficientCreditHistory")]
        InsufficientCreditHistory,
        
        [XmlEnum("NonPredictiveScore")]
        NonPredictiveScore,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("SignificantErrorsScore")]
        SignificantErrorsScore,
        
        [XmlEnum("Unscorable")]
        Unscorable,
    }
}
