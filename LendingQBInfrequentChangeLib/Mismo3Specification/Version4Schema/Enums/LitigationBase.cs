namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum LitigationBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Bankruptcy")]
        Bankruptcy,
        
        [XmlEnum("Contract")]
        Contract,
        
        [XmlEnum("Foreclosure")]
        Foreclosure,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Probate")]
        Probate,
        
        [XmlEnum("Tort")]
        Tort,
    }
}
