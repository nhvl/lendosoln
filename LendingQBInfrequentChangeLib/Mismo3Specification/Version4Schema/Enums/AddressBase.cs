namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum AddressBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("CorporateHeadquarters")]
        CorporateHeadquarters,
        
        [XmlEnum("Current")]
        Current,
        
        [XmlEnum("LegalEntityFormation")]
        LegalEntityFormation,
        
        [XmlEnum("Mailing")]
        Mailing,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Primary")]
        Primary,
        
        [XmlEnum("Prior")]
        Prior,
    }
}
