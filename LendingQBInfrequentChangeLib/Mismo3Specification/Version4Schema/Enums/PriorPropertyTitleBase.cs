namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PriorPropertyTitleBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("JointWithOtherThanSpouse")]
        JointWithOtherThanSpouse,
        
        [XmlEnum("JointWithSpouse")]
        JointWithSpouse,
        
        [XmlEnum("Sole")]
        Sole,
    }
}
