namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum RepresentedPartyBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Borrower")]
        Borrower,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("RealEstateAgent")]
        RealEstateAgent,
        
        [XmlEnum("Servicer")]
        Servicer,
    }
}
