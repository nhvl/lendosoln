namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum HardshipDurationBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("LongTerm")]
        LongTerm,
        
        [XmlEnum("MediumTerm")]
        MediumTerm,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Permanent")]
        Permanent,
        
        [XmlEnum("ShortTerm")]
        ShortTerm,
        
        [XmlEnum("Unknown")]
        Unknown,
    }
}
