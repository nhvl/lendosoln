namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum MIPolicyStatusBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("ActiveApplication")]
        ActiveApplication,
        
        [XmlEnum("ActiveCertificate")]
        ActiveCertificate,
        
        [XmlEnum("ActiveCommitment")]
        ActiveCommitment,
        
        [XmlEnum("InactiveApplication")]
        InactiveApplication,
        
        [XmlEnum("InactiveCertificate")]
        InactiveCertificate,
        
        [XmlEnum("InactiveCommitment")]
        InactiveCommitment,
        
        [XmlEnum("Other")]
        Other,
    }
}
