namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum QualifyingRateBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AboveNoteRate")]
        AboveNoteRate,
        
        [XmlEnum("AtNoteRate")]
        AtNoteRate,
        
        [XmlEnum("BelowNoteRate")]
        BelowNoteRate,
        
        [XmlEnum("BoughtDownRate")]
        BoughtDownRate,
        
        [XmlEnum("Other")]
        Other,
    }
}
