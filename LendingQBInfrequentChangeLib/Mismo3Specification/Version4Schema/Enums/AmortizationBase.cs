namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum AmortizationBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AdjustableRate")]
        AdjustableRate,
        
        [XmlEnum("Fixed")]
        Fixed,
        
        [XmlEnum("GEM")]
        GEM,
        
        [XmlEnum("GPM")]
        GPM,
        
        [XmlEnum("GraduatedPaymentARM")]
        GraduatedPaymentARM,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("RateImprovementMortgage")]
        RateImprovementMortgage,
        
        [XmlEnum("Step")]
        Step,
    }
}
