namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum MonetaryEventBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("BalloonPayment")]
        BalloonPayment,
        
        [XmlEnum("CurtailmentToFirstYearChargesSetAside")]
        CurtailmentToFirstYearChargesSetAside,
        
        [XmlEnum("CurtailmentToLineOfCredit")]
        CurtailmentToLineOfCredit,
        
        [XmlEnum("CurtailmentToRepairSetAside")]
        CurtailmentToRepairSetAside,
        
        [XmlEnum("CurtailmentToTaxAndInsuranceSetAside")]
        CurtailmentToTaxAndInsuranceSetAside,
        
        [XmlEnum("Draw")]
        Draw,
        
        [XmlEnum("FinalDraw")]
        FinalDraw,
        
        [XmlEnum("FinalDrawWithEarlyConversion")]
        FinalDrawWithEarlyConversion,
        
        [XmlEnum("FinalDrawWithPaydown")]
        FinalDrawWithPaydown,
        
        [XmlEnum("FinalDrawWithPaydownAndEarlyConversion")]
        FinalDrawWithPaydownAndEarlyConversion,
        
        [XmlEnum("FirstYearPropertyChargesFinal")]
        FirstYearPropertyChargesFinal,
        
        [XmlEnum("FirstYearPropertyChargesNotFinal")]
        FirstYearPropertyChargesNotFinal,
        
        [XmlEnum("LineOfCreditForAppraisal")]
        LineOfCreditForAppraisal,
        
        [XmlEnum("LineOfCreditToPreventLien")]
        LineOfCreditToPreventLien,
        
        [XmlEnum("NonBorrowerFundsRemitted")]
        NonBorrowerFundsRemitted,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Payment")]
        Payment,
        
        [XmlEnum("PrincipalCurtailment")]
        PrincipalCurtailment,
        
        [XmlEnum("RepairSetAsideFinal")]
        RepairSetAsideFinal,
        
        [XmlEnum("RepairSetAsideNotFinal")]
        RepairSetAsideNotFinal,
        
        [XmlEnum("TaxAndInsuranceSetAside")]
        TaxAndInsuranceSetAside,
        
        [XmlEnum("TrialPayment")]
        TrialPayment,
        
        [XmlEnum("UnscheduledDraw")]
        UnscheduledDraw,
    }
}
