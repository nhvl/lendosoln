namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum EmploymentStateBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Employed")]
        Employed,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Retired")]
        Retired,
        
        [XmlEnum("Unemployed")]
        Unemployed,
    }
}
