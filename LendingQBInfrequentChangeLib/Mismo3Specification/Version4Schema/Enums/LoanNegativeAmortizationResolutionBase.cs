namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum LoanNegativeAmortizationResolutionBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("NoMoreNegativeAmortizationAllowed")]
        NoMoreNegativeAmortizationAllowed,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PaymentRecastToFullyAmortizingTermRemainsTheSame")]
        PaymentRecastToFullyAmortizingTermRemainsTheSame,
        
        [XmlEnum("PayoffDifference")]
        PayoffDifference,
        
        [XmlEnum("TermExtendedAndPaymentRecastToFullyAmortizing")]
        TermExtendedAndPaymentRecastToFullyAmortizing,
        
        [XmlEnum("TermExtendedPaymentRemainsTheSame")]
        TermExtendedPaymentRemainsTheSame,
    }
}
