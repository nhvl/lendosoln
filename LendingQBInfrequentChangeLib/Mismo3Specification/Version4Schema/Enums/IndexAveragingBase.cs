namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum IndexAveragingBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AverageConsecutiveIndexValues")]
        AverageConsecutiveIndexValues,
        
        [XmlEnum("AveragePeriodicIndexRates")]
        AveragePeriodicIndexRates,
        
        [XmlEnum("AveragePreviousLoanIndexRates")]
        AveragePreviousLoanIndexRates,
    }
}
