namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum BuildingPermitLevelBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Final")]
        Final,
        
        [XmlEnum("Maintenance")]
        Maintenance,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Recurring")]
        Recurring,
        
        [XmlEnum("SubWork")]
        SubWork,
        
        [XmlEnum("Temporary")]
        Temporary,
        
        [XmlEnum("Transitional")]
        Transitional,
    }
}
