namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum FeeBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("203KConsultantFee")]
        Item203KConsultantFee,
        
        [XmlEnum("203KDiscountOnRepairs")]
        Item203KDiscountOnRepairs,
        
        [XmlEnum("203KInspectionFee")]
        Item203KInspectionFee,
        
        [XmlEnum("203KPermits")]
        Item203KPermits,
        
        [XmlEnum("203KSupplementalOriginationFee")]
        Item203KSupplementalOriginationFee,
        
        [XmlEnum("203KTitleUpdate")]
        Item203KTitleUpdate,
        
        [XmlEnum("ApplicationFee")]
        ApplicationFee,
        
        [XmlEnum("AppraisalDeskReviewFee")]
        AppraisalDeskReviewFee,
        
        [XmlEnum("AppraisalFee")]
        AppraisalFee,
        
        [XmlEnum("AppraisalFieldReviewFee")]
        AppraisalFieldReviewFee,
        
        [XmlEnum("AppraisalManagementCompanyFee")]
        AppraisalManagementCompanyFee,
        
        [XmlEnum("AsbestosInspectionFee")]
        AsbestosInspectionFee,
        
        [XmlEnum("AssignmentPreparationFee")]
        AssignmentPreparationFee,
        
        [XmlEnum("AssumptionFee")]
        AssumptionFee,
        
        [XmlEnum("AttorneyFee")]
        AttorneyFee,
        
        [XmlEnum("AutomatedUnderwritingFee")]
        AutomatedUnderwritingFee,
        
        [XmlEnum("AVMFee")]
        AVMFee,
        
        [XmlEnum("BankruptcyMonitoringFee")]
        BankruptcyMonitoringFee,
        
        [XmlEnum("BondFee")]
        BondFee,
        
        [XmlEnum("BondReviewFee")]
        BondReviewFee,
        
        [XmlEnum("CertificationFee")]
        CertificationFee,
        
        [XmlEnum("ChosenInterestRateCreditOrChargeTotal")]
        ChosenInterestRateCreditOrChargeTotal,
        
        [XmlEnum("CommitmentFee")]
        CommitmentFee,
        
        [XmlEnum("CondominiumAssociationDues")]
        CondominiumAssociationDues,
        
        [XmlEnum("CondominiumAssociationSpecialAssessment")]
        CondominiumAssociationSpecialAssessment,
        
        [XmlEnum("CooperativeAssociationDues")]
        CooperativeAssociationDues,
        
        [XmlEnum("CooperativeAssociationSpecialAssessment")]
        CooperativeAssociationSpecialAssessment,
        
        [XmlEnum("CopyOrFaxFee")]
        CopyOrFaxFee,
        
        [XmlEnum("CourierFee")]
        CourierFee,
        
        [XmlEnum("CreditDisabilityInsurancePremium")]
        CreditDisabilityInsurancePremium,
        
        [XmlEnum("CreditLifeInsurancePremium")]
        CreditLifeInsurancePremium,
        
        [XmlEnum("CreditPropertyInsurancePremium")]
        CreditPropertyInsurancePremium,
        
        [XmlEnum("CreditReportFee")]
        CreditReportFee,
        
        [XmlEnum("CreditUnemploymentInsurancePremium")]
        CreditUnemploymentInsurancePremium,
        
        [XmlEnum("DebtCancellationInsurancePremium")]
        DebtCancellationInsurancePremium,
        
        [XmlEnum("DebtSuspensionInsurancePremium")]
        DebtSuspensionInsurancePremium,
        
        [XmlEnum("DeedPreparationFee")]
        DeedPreparationFee,
        
        [XmlEnum("DisasterInspectionFee")]
        DisasterInspectionFee,
        
        [XmlEnum("DiscountOnRepairsFee")]
        DiscountOnRepairsFee,
        
        [XmlEnum("DocumentaryStampFee")]
        DocumentaryStampFee,
        
        [XmlEnum("DocumentPreparationFee")]
        DocumentPreparationFee,
        
        [XmlEnum("DownPaymentProtectionFee")]
        DownPaymentProtectionFee,
        
        [XmlEnum("DryWallInspectionFee")]
        DryWallInspectionFee,
        
        [XmlEnum("ElectricalInspectionFee")]
        ElectricalInspectionFee,
        
        [XmlEnum("ElectronicDocumentDeliveryFee")]
        ElectronicDocumentDeliveryFee,
        
        [XmlEnum("EnvironmentalInspectionFee")]
        EnvironmentalInspectionFee,
        
        [XmlEnum("EscrowHoldbackFee")]
        EscrowHoldbackFee,
        
        [XmlEnum("EscrowServiceFee")]
        EscrowServiceFee,
        
        [XmlEnum("EscrowWaiverFee")]
        EscrowWaiverFee,
        
        [XmlEnum("FilingFee")]
        FilingFee,
        
        [XmlEnum("FloodCertification")]
        FloodCertification,
        
        [XmlEnum("FoundationInspectionFee")]
        FoundationInspectionFee,
        
        [XmlEnum("HeatingCoolingInspectionFee")]
        HeatingCoolingInspectionFee,
        
        [XmlEnum("HighCostMortgageCounselingFee")]
        HighCostMortgageCounselingFee,
        
        [XmlEnum("HomeInspectionFee")]
        HomeInspectionFee,
        
        [XmlEnum("HomeownersAssociationDues")]
        HomeownersAssociationDues,
        
        [XmlEnum("HomeownersAssociationServiceFee")]
        HomeownersAssociationServiceFee,
        
        [XmlEnum("HomeownersAssociationSpecialAssessment")]
        HomeownersAssociationSpecialAssessment,
        
        [XmlEnum("HomeWarrantyFee")]
        HomeWarrantyFee,
        
        [XmlEnum("LeadInspectionFee")]
        LeadInspectionFee,
        
        [XmlEnum("LendersAttorneyFee")]
        LendersAttorneyFee,
        
        [XmlEnum("LoanDiscountPoints")]
        LoanDiscountPoints,
        
        [XmlEnum("LoanLevelPriceAdjustment")]
        LoanLevelPriceAdjustment,
        
        [XmlEnum("LoanOriginationFee")]
        LoanOriginationFee,
        
        [XmlEnum("LoanOriginatorCompensation")]
        LoanOriginatorCompensation,
        
        [XmlEnum("ManualUnderwritingFee")]
        ManualUnderwritingFee,
        
        [XmlEnum("ManufacturedHousingInspectionFee")]
        ManufacturedHousingInspectionFee,
        
        [XmlEnum("ManufacturedHousingProcessingFee")]
        ManufacturedHousingProcessingFee,
        
        [XmlEnum("MERSRegistrationFee")]
        MERSRegistrationFee,
        
        [XmlEnum("MIInitialPremium")]
        MIInitialPremium,
        
        [XmlEnum("MIUpfrontPremium")]
        MIUpfrontPremium,
        
        [XmlEnum("ModificationFee")]
        ModificationFee,
        
        [XmlEnum("MoldInspectionFee")]
        MoldInspectionFee,
        
        [XmlEnum("MortgageBrokerFee")]
        MortgageBrokerFee,
        
        [XmlEnum("MortgageSurchargeCountyOrParish")]
        MortgageSurchargeCountyOrParish,
        
        [XmlEnum("MortgageSurchargeMunicipal")]
        MortgageSurchargeMunicipal,
        
        [XmlEnum("MortgageSurchargeState")]
        MortgageSurchargeState,
        
        [XmlEnum("MortgageTaxCreditServiceFee")]
        MortgageTaxCreditServiceFee,
        
        [XmlEnum("MultipleLoansClosingFee")]
        MultipleLoansClosingFee,
        
        [XmlEnum("MunicipalLienCertificateFee")]
        MunicipalLienCertificateFee,
        
        [XmlEnum("NotaryFee")]
        NotaryFee,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("OurOriginationChargeTotal")]
        OurOriginationChargeTotal,
        
        [XmlEnum("PayoffRequestFee")]
        PayoffRequestFee,
        
        [XmlEnum("PestInspectionFee")]
        PestInspectionFee,
        
        [XmlEnum("PlumbingInspectionFee")]
        PlumbingInspectionFee,
        
        [XmlEnum("PowerOfAttorneyPreparationFee")]
        PowerOfAttorneyPreparationFee,
        
        [XmlEnum("PowerOfAttorneyRecordingFee")]
        PowerOfAttorneyRecordingFee,
        
        [XmlEnum("PreclosingVerificationControlFee")]
        PreclosingVerificationControlFee,
        
        [XmlEnum("ProcessingFee")]
        ProcessingFee,
        
        [XmlEnum("ProgramGuaranteeFee")]
        ProgramGuaranteeFee,
        
        [XmlEnum("PropertyInspectionWaiverFee")]
        PropertyInspectionWaiverFee,
        
        [XmlEnum("RadonInspectionFee")]
        RadonInspectionFee,
        
        [XmlEnum("RateLockFee")]
        RateLockFee,
        
        [XmlEnum("RealEstateCommissionBuyersBroker")]
        RealEstateCommissionBuyersBroker,
        
        [XmlEnum("RealEstateCommissionSellersBroker")]
        RealEstateCommissionSellersBroker,
        
        [XmlEnum("ReconveyanceFee")]
        ReconveyanceFee,
        
        [XmlEnum("ReconveyanceTrackingFee")]
        ReconveyanceTrackingFee,
        
        [XmlEnum("RecordingFeeForAssignment")]
        RecordingFeeForAssignment,
        
        [XmlEnum("RecordingFeeForDeed")]
        RecordingFeeForDeed,
        
        [XmlEnum("RecordingFeeForMortgage")]
        RecordingFeeForMortgage,
        
        [XmlEnum("RecordingFeeForMunicipalLienCertificate")]
        RecordingFeeForMunicipalLienCertificate,
        
        [XmlEnum("RecordingFeeForOtherDocument")]
        RecordingFeeForOtherDocument,
        
        [XmlEnum("RecordingFeeForRelease")]
        RecordingFeeForRelease,
        
        [XmlEnum("RecordingFeeForSubordination")]
        RecordingFeeForSubordination,
        
        [XmlEnum("RecordingFeeTotal")]
        RecordingFeeTotal,
        
        [XmlEnum("RecordingServiceFee")]
        RecordingServiceFee,
        
        [XmlEnum("RedrawFee")]
        RedrawFee,
        
        [XmlEnum("ReinspectionFee")]
        ReinspectionFee,
        
        [XmlEnum("RenovationConsultantFee")]
        RenovationConsultantFee,
        
        [XmlEnum("RepairsFee")]
        RepairsFee,
        
        [XmlEnum("RoofInspectionFee")]
        RoofInspectionFee,
        
        [XmlEnum("SepticInspectionFee")]
        SepticInspectionFee,
        
        [XmlEnum("SettlementFee")]
        SettlementFee,
        
        [XmlEnum("SigningAgentFee")]
        SigningAgentFee,
        
        [XmlEnum("SmokeDetectorInspectionFee")]
        SmokeDetectorInspectionFee,
        
        [XmlEnum("StateTitleInsuranceFee")]
        StateTitleInsuranceFee,
        
        [XmlEnum("StructuralInspectionFee")]
        StructuralInspectionFee,
        
        [XmlEnum("SubordinationFee")]
        SubordinationFee,
        
        [XmlEnum("SurveyFee")]
        SurveyFee,
        
        [XmlEnum("TaxServiceFee")]
        TaxServiceFee,
        
        [XmlEnum("TaxStampForCityDeed")]
        TaxStampForCityDeed,
        
        [XmlEnum("TaxStampForCityMortgage")]
        TaxStampForCityMortgage,
        
        [XmlEnum("TaxStampForCountyDeed")]
        TaxStampForCountyDeed,
        
        [XmlEnum("TaxStampForCountyMortgage")]
        TaxStampForCountyMortgage,
        
        [XmlEnum("TaxStampForStateDeed")]
        TaxStampForStateDeed,
        
        [XmlEnum("TaxStampForStateMortgage")]
        TaxStampForStateMortgage,
        
        [XmlEnum("TaxStatusResearchFee")]
        TaxStatusResearchFee,
        
        [XmlEnum("TemporaryBuydownAdministrationFee")]
        TemporaryBuydownAdministrationFee,
        
        [XmlEnum("TemporaryBuydownPoints")]
        TemporaryBuydownPoints,
        
        [XmlEnum("TitleAbstractFee")]
        TitleAbstractFee,
        
        [XmlEnum("TitleBorrowerClosingProtectionLetterFee")]
        TitleBorrowerClosingProtectionLetterFee,
        
        [XmlEnum("TitleCertificationFee")]
        TitleCertificationFee,
        
        [XmlEnum("TitleClosingCoordinationFee")]
        TitleClosingCoordinationFee,
        
        [XmlEnum("TitleClosingFee")]
        TitleClosingFee,
        
        [XmlEnum("TitleClosingProtectionLetterFee")]
        TitleClosingProtectionLetterFee,
        
        [XmlEnum("TitleDocumentPreparationFee")]
        TitleDocumentPreparationFee,
        
        [XmlEnum("TitleEndorsementFee")]
        TitleEndorsementFee,
        
        [XmlEnum("TitleExaminationFee")]
        TitleExaminationFee,
        
        [XmlEnum("TitleFinalPolicyShortFormFee")]
        TitleFinalPolicyShortFormFee,
        
        [XmlEnum("TitleInsuranceBinderFee")]
        TitleInsuranceBinderFee,
        
        [XmlEnum("TitleInsuranceFee")]
        TitleInsuranceFee,
        
        [XmlEnum("TitleLendersCoveragePremium")]
        TitleLendersCoveragePremium,
        
        [XmlEnum("TitleNotaryFee")]
        TitleNotaryFee,
        
        [XmlEnum("TitleOwnersCoveragePremium")]
        TitleOwnersCoveragePremium,
        
        [XmlEnum("TitleServicesFeeTotal")]
        TitleServicesFeeTotal,
        
        [XmlEnum("TitleServicesSalesTax")]
        TitleServicesSalesTax,
        
        [XmlEnum("TitleUnderwritingIssueResolutionFee")]
        TitleUnderwritingIssueResolutionFee,
        
        [XmlEnum("TransferTaxTotal")]
        TransferTaxTotal,
        
        [XmlEnum("UnderwritingFee")]
        UnderwritingFee,
        
        [XmlEnum("USDARuralDevelopmentGuaranteeFee")]
        USDARuralDevelopmentGuaranteeFee,
        
        [XmlEnum("VAFundingFee")]
        VAFundingFee,
        
        [XmlEnum("VerificationOfAssetsFee")]
        VerificationOfAssetsFee,
        
        [XmlEnum("VerificationOfEmploymentFee")]
        VerificationOfEmploymentFee,
        
        [XmlEnum("VerificationOfIncomeFee")]
        VerificationOfIncomeFee,
        
        [XmlEnum("VerificationOfResidencyStatusFee")]
        VerificationOfResidencyStatusFee,
        
        [XmlEnum("VerificationOfTaxpayerIdentificationFee")]
        VerificationOfTaxpayerIdentificationFee,
        
        [XmlEnum("VerificationOfTaxReturnFee")]
        VerificationOfTaxReturnFee,
        
        [XmlEnum("WaterTestingFee")]
        WaterTestingFee,
        
        [XmlEnum("WellInspectionFee")]
        WellInspectionFee,
        
        [XmlEnum("WireTransferFee")]
        WireTransferFee,
    }
}
