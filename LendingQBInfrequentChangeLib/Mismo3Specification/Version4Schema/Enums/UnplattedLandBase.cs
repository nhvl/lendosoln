namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum UnplattedLandBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("GovernmentSurvey")]
        GovernmentSurvey,
        
        [XmlEnum("LandGrant")]
        LandGrant,
        
        [XmlEnum("MetesAndBounds")]
        MetesAndBounds,
        
        [XmlEnum("NativeAmericanLand")]
        NativeAmericanLand,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Ranchero")]
        Ranchero,
    }
}
