namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum LitigationEndReasonBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Dismissal")]
        Dismissal,
        
        [XmlEnum("FinalJudgment")]
        FinalJudgment,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Settlement")]
        Settlement,
    }
}
