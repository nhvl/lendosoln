namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PoolStructureBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("InvestorDefinedMultipleLender")]
        InvestorDefinedMultipleLender,
        
        [XmlEnum("LenderInitiatedMultipleLender")]
        LenderInitiatedMultipleLender,
        
        [XmlEnum("MultipleIssuer")]
        MultipleIssuer,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("SingleIssuer")]
        SingleIssuer,
        
        [XmlEnum("SingleLender")]
        SingleLender,
    }
}
