namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum GFESectionIdentifierBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("A")]
        A,
        
        [XmlEnum("AB")]
        AB,
        
        [XmlEnum("B")]
        B,
        
        [XmlEnum("Eight")]
        Eight,
        
        [XmlEnum("Eleven")]
        Eleven,
        
        [XmlEnum("Five")]
        Five,
        
        [XmlEnum("Four")]
        Four,
        
        [XmlEnum("Nine")]
        Nine,
        
        [XmlEnum("One")]
        One,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Seven")]
        Seven,
        
        [XmlEnum("Six")]
        Six,
        
        [XmlEnum("Ten")]
        Ten,
        
        [XmlEnum("Three")]
        Three,
        
        [XmlEnum("Two")]
        Two,
    }
}
