namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ThermalRatedItemsBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Ceilings")]
        Ceilings,
        
        [XmlEnum("Floors")]
        Floors,
        
        [XmlEnum("Walls")]
        Walls,
    }
}
