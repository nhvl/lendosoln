namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ConversionScheduleBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Anytime")]
        Anytime,
        
        [XmlEnum("OnFirstAndSecondRateAdjustment")]
        OnFirstAndSecondRateAdjustment,
        
        [XmlEnum("OnFirstThroughFifthRateAdjustment")]
        OnFirstThroughFifthRateAdjustment,
        
        [XmlEnum("OnFirstThroughThirdRateAdjustment")]
        OnFirstThroughThirdRateAdjustment,
        
        [XmlEnum("OnSecondThroughTenthRateAdjustment")]
        OnSecondThroughTenthRateAdjustment,
        
        [XmlEnum("OnThirdThroughFifthRateAdjustment")]
        OnThirdThroughFifthRateAdjustment,
        
        [XmlEnum("Other")]
        Other,
    }
}
