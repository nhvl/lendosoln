namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum MIPremiumPeriodBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Renewal")]
        Renewal,
        
        [XmlEnum("Upfront")]
        Upfront,
    }
}
