namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum MonthlyRentCurrentRatingBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AsAgreed")]
        AsAgreed,
        
        [XmlEnum("ChargeOff")]
        ChargeOff,
        
        [XmlEnum("Collection")]
        Collection,
        
        [XmlEnum("Late30Days")]
        Late30Days,
        
        [XmlEnum("Late60Days")]
        Late60Days,
        
        [XmlEnum("Late90Days")]
        Late90Days,
        
        [XmlEnum("LateOver120Days")]
        LateOver120Days,
        
        [XmlEnum("NoDataAvailable")]
        NoDataAvailable,
        
        [XmlEnum("TooNew")]
        TooNew,
    }
}
