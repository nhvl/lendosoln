namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum VerificationMethodBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Verbal")]
        Verbal,
        
        [XmlEnum("Written")]
        Written,
    }
}
