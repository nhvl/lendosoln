namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum WorkoutProcessStatusBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Active")]
        Active,
        
        [XmlEnum("Ceased")]
        Ceased,
        
        [XmlEnum("Inactive")]
        Inactive,
    }
}
