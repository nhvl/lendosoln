namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum FoundationMaterialBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("BlockAndPier")]
        BlockAndPier,
        
        [XmlEnum("ConcreteRunners")]
        ConcreteRunners,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PouredConcrete")]
        PouredConcrete,
    }
}
