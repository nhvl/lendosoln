namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum IRSDocumentBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("IRS1040")]
        IRS1040,
        
        [XmlEnum("IRS1065")]
        IRS1065,
        
        [XmlEnum("IRS1098")]
        IRS1098,
        
        [XmlEnum("IRS1099")]
        IRS1099,
        
        [XmlEnum("IRS1120")]
        IRS1120,
        
        [XmlEnum("IRS1120H")]
        IRS1120H,
        
        [XmlEnum("IRS1120L")]
        IRS1120L,
        
        [XmlEnum("IRS1120S")]
        IRS1120S,
        
        [XmlEnum("IRS5498")]
        IRS5498,
        
        [XmlEnum("IRSW2")]
        IRSW2,
        
        [XmlEnum("Other")]
        Other,
    }
}
