namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ConstructionToPermanentClosingFeatureBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AutomaticConversion")]
        AutomaticConversion,
        
        [XmlEnum("ConvertibleARM")]
        ConvertibleARM,
        
        [XmlEnum("ModificationAgreement")]
        ModificationAgreement,
        
        [XmlEnum("NewNote")]
        NewNote,
        
        [XmlEnum("Other")]
        Other,
    }
}
