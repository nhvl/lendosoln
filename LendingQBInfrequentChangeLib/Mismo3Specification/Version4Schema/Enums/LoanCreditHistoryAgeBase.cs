namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum LoanCreditHistoryAgeBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("LessThan180Days")]
        LessThan180Days,
        
        [XmlEnum("LessThan270Days")]
        LessThan270Days,
        
        [XmlEnum("LessThan90Days")]
        LessThan90Days,
        
        [XmlEnum("Other")]
        Other,
    }
}
