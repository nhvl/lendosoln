namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum NeighborhoodHousingPredominantOccupancyBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Owner")]
        Owner,
        
        [XmlEnum("Tenant")]
        Tenant,
        
        [XmlEnum("Vacant")]
        Vacant,
    }
}
