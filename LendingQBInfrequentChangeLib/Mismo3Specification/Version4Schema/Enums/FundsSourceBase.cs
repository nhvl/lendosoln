namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum FundsSourceBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Borrower")]
        Borrower,
        
        [XmlEnum("Builder")]
        Builder,
        
        [XmlEnum("CommunityNonProfit")]
        CommunityNonProfit,
        
        [XmlEnum("Employer")]
        Employer,
        
        [XmlEnum("FederalAgency")]
        FederalAgency,
        
        [XmlEnum("Institutional")]
        Institutional,
        
        [XmlEnum("Lender")]
        Lender,
        
        [XmlEnum("LocalAgency")]
        LocalAgency,
        
        [XmlEnum("NonOriginatingFinancialInstitution")]
        NonOriginatingFinancialInstitution,
        
        [XmlEnum("NonParentRelative")]
        NonParentRelative,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Parent")]
        Parent,
        
        [XmlEnum("PropertySeller")]
        PropertySeller,
        
        [XmlEnum("Relative")]
        Relative,
        
        [XmlEnum("ReligiousNonProfit")]
        ReligiousNonProfit,
        
        [XmlEnum("RuralHousingService")]
        RuralHousingService,
        
        [XmlEnum("StateAgency")]
        StateAgency,
        
        [XmlEnum("Unknown")]
        Unknown,
        
        [XmlEnum("UnmarriedPartner")]
        UnmarriedPartner,
        
        [XmlEnum("UnrelatedFriend")]
        UnrelatedFriend,
    }
}
