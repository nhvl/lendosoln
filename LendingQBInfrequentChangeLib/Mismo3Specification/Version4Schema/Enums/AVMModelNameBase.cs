namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum AVMModelNameBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AutomatedPropertyService")]
        AutomatedPropertyService,
        
        [XmlEnum("AVMax")]
        AVMax,
        
        [XmlEnum("Casa")]
        Casa,
        
        [XmlEnum("CAValue")]
        CAValue,
        
        [XmlEnum("CollateralMarketValue")]
        CollateralMarketValue,
        
        [XmlEnum("CollateralValuationModel")]
        CollateralValuationModel,
        
        [XmlEnum("HomePriceAnalyzer")]
        HomePriceAnalyzer,
        
        [XmlEnum("HomeValueExplorer")]
        HomeValueExplorer,
        
        [XmlEnum("IntellirealAVM")]
        IntellirealAVM,
        
        [XmlEnum("IVal")]
        IVal,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Pass")]
        Pass,
        
        [XmlEnum("PowerBase6")]
        PowerBase6,
        
        [XmlEnum("RealAssessment")]
        RealAssessment,
        
        [XmlEnum("RealtorValuationModel")]
        RealtorValuationModel,
        
        [XmlEnum("Relar")]
        Relar,
        
        [XmlEnum("SiteXValue")]
        SiteXValue,
        
        [XmlEnum("ValueFinder")]
        ValueFinder,
        
        [XmlEnum("ValuePoint")]
        ValuePoint,
        
        [XmlEnum("ValuePoint4")]
        ValuePoint4,
        
        [XmlEnum("ValuePointPlus")]
        ValuePointPlus,
        
        [XmlEnum("ValueSure")]
        ValueSure,
        
        [XmlEnum("VeroIndexPlus")]
        VeroIndexPlus,
        
        [XmlEnum("VeroValue")]
        VeroValue,
        
        [XmlEnum("VeroValueAdvantage")]
        VeroValueAdvantage,
        
        [XmlEnum("VeroValuePreferred")]
        VeroValuePreferred,
    }
}
