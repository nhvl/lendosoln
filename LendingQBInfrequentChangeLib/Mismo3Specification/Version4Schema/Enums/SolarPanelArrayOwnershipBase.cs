namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum SolarPanelArrayOwnershipBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Leased")]
        Leased,
        
        [XmlEnum("Owned")]
        Owned,
    }
}
