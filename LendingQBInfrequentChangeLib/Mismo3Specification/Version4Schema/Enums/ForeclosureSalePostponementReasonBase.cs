namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ForeclosureSalePostponementReasonBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AttorneyProcess")]
        AttorneyProcess,
        
        [XmlEnum("CompromisedPropertyCondition")]
        CompromisedPropertyCondition,
        
        [XmlEnum("GovernmentSeizure")]
        GovernmentSeizure,
        
        [XmlEnum("Moratorium")]
        Moratorium,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("ServicerDelegation")]
        ServicerDelegation,
        
        [XmlEnum("ServicerProcess")]
        ServicerProcess,
        
        [XmlEnum("StatutoryJurisdictionalDelay")]
        StatutoryJurisdictionalDelay,
        
        [XmlEnum("TitleDefect")]
        TitleDefect,
    }
}
