namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum EscrowPremiumPaymentBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("CollectAtClosing")]
        CollectAtClosing,
        
        [XmlEnum("PaidOutsideOfClosing")]
        PaidOutsideOfClosing,
        
        [XmlEnum("Waived")]
        Waived,
    }
}
