namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ProjectBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("OtherPhase")]
        OtherPhase,
        
        [XmlEnum("SubjectPhase")]
        SubjectPhase,
    }
}
