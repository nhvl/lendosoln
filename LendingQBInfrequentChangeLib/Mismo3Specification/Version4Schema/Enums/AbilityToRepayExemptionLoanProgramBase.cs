namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum AbilityToRepayExemptionLoanProgramBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("EmergencyEconomicStabilizationAct")]
        EmergencyEconomicStabilizationAct,
        
        [XmlEnum("HELOC")]
        HELOC,
        
        [XmlEnum("HousingFinanceAgency")]
        HousingFinanceAgency,
        
        [XmlEnum("NonStandardToStandardRefinance")]
        NonStandardToStandardRefinance,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("ReverseMortgage")]
        ReverseMortgage,
        
        [XmlEnum("TemporaryLoan")]
        TemporaryLoan,
        
        [XmlEnum("TimeSharePlan")]
        TimeSharePlan,
    }
}
