namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum TitleOwnershipBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Combination")]
        Combination,
        
        [XmlEnum("Corporation")]
        Corporation,
        
        [XmlEnum("Estate")]
        Estate,
        
        [XmlEnum("GovernmentEntity")]
        GovernmentEntity,
        
        [XmlEnum("Guardianship")]
        Guardianship,
        
        [XmlEnum("Individual")]
        Individual,
        
        [XmlEnum("JointVenture")]
        JointVenture,
        
        [XmlEnum("LimitedLiabilityCompany")]
        LimitedLiabilityCompany,
        
        [XmlEnum("LimitedPartnership")]
        LimitedPartnership,
        
        [XmlEnum("NonProfitCorporation")]
        NonProfitCorporation,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Partnership")]
        Partnership,
        
        [XmlEnum("Trust")]
        Trust,
    }
}
