namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ConditionSatisfactionTimeframeBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PriorToApproval")]
        PriorToApproval,
        
        [XmlEnum("PriorToDocuments")]
        PriorToDocuments,
        
        [XmlEnum("PriorToFunding")]
        PriorToFunding,
        
        [XmlEnum("PriorToSigning")]
        PriorToSigning,
        
        [XmlEnum("UnderwriterToReview")]
        UnderwriterToReview,
    }
}
