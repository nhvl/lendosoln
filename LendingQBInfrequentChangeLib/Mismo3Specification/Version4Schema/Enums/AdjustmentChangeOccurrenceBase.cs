namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum AdjustmentChangeOccurrenceBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("ARM")]
        ARM,
        
        [XmlEnum("Conversion")]
        Conversion,
        
        [XmlEnum("GEM")]
        GEM,
        
        [XmlEnum("GPM")]
        GPM,
        
        [XmlEnum("LoanToValue")]
        LoanToValue,
        
        [XmlEnum("NonScheduled")]
        NonScheduled,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Step")]
        Step,
        
        [XmlEnum("TimelyPaymentReward")]
        TimelyPaymentReward,
    }
}
