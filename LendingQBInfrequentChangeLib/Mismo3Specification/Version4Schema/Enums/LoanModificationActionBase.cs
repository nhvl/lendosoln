namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum LoanModificationActionBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("BorrowerDelinquencyWorkout")]
        BorrowerDelinquencyWorkout,
        
        [XmlEnum("Capitalization")]
        Capitalization,
        
        [XmlEnum("ChangeOfPaymentFrequency")]
        ChangeOfPaymentFrequency,
        
        [XmlEnum("ConstructionToPermanentFinancing")]
        ConstructionToPermanentFinancing,
        
        [XmlEnum("HAMP")]
        HAMP,
        
        [XmlEnum("LenderPortfolioManagement")]
        LenderPortfolioManagement,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PrincipalForgiveness")]
        PrincipalForgiveness,
        
        [XmlEnum("Restructure")]
        Restructure,
        
        [XmlEnum("UnscheduledPaymentRecast")]
        UnscheduledPaymentRecast,
    }
}
