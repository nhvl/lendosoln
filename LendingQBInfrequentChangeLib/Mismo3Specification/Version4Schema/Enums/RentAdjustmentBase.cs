namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum RentAdjustmentBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Age")]
        Age,
        
        [XmlEnum("Appeal")]
        Appeal,
        
        [XmlEnum("Condition")]
        Condition,
        
        [XmlEnum("Design")]
        Design,
        
        [XmlEnum("GrossLivingArea")]
        GrossLivingArea,
        
        [XmlEnum("Location")]
        Location,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("OtherConcessions")]
        OtherConcessions,
        
        [XmlEnum("RentConcessions")]
        RentConcessions,
        
        [XmlEnum("View")]
        View,
    }
}
