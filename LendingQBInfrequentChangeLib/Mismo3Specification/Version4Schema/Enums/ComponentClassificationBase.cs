namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ComponentClassificationBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("PersonalProperty")]
        PersonalProperty,
        
        [XmlEnum("RealProperty")]
        RealProperty,
    }
}
