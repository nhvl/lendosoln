namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum BoundaryBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Alley")]
        Alley,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Street")]
        Street,
        
        [XmlEnum("Waterway")]
        Waterway,
    }
}
