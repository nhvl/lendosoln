namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum MICertificationStatusBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("LenderToObtain")]
        LenderToObtain,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("SellerOfLoanToObtain")]
        SellerOfLoanToObtain,
    }
}
