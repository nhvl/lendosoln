namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum SignatureFieldMarkBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("FullSignature")]
        FullSignature,
        
        [XmlEnum("Initials")]
        Initials,
    }
}
