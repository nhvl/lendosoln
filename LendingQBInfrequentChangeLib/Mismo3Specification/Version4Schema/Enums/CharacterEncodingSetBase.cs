namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum CharacterEncodingSetBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("ISO88591")]
        ISO88591,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("USASCII")]
        USASCII,
        
        [XmlEnum("UTF16")]
        UTF16,
        
        [XmlEnum("UTF8")]
        UTF8,
    }
}
