namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum EmploymentDocumentBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("ThirdPartyEmploymentStatement")]
        ThirdPartyEmploymentStatement,
        
        [XmlEnum("VerbalStatement")]
        VerbalStatement,
        
        [XmlEnum("VerificationOfEmployment")]
        VerificationOfEmployment,
    }
}
