namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum AssignmentParameterSourceBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AssignmentSpecific")]
        AssignmentSpecific,
        
        [XmlEnum("ClientSpecific")]
        ClientSpecific,
        
        [XmlEnum("MasterAgreement")]
        MasterAgreement,
        
        [XmlEnum("Other")]
        Other,
    }
}
