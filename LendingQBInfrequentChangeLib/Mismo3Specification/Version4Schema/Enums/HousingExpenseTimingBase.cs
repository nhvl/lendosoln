namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum HousingExpenseTimingBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Present")]
        Present,
        
        [XmlEnum("Proposed")]
        Proposed,
    }
}
