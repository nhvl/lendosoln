namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum LoanPricePercentFormatBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("0Base")]
        Item0Base,
        
        [XmlEnum("100Base")]
        Item100Base,
    }
}
