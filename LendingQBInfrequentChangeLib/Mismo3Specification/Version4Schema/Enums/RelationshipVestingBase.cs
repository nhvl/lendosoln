namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum RelationshipVestingBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("CommunityProperty")]
        CommunityProperty,
        
        [XmlEnum("Individual")]
        Individual,
        
        [XmlEnum("JointTenants")]
        JointTenants,
        
        [XmlEnum("JointTenantsWithRightOfSurvivorship")]
        JointTenantsWithRightOfSurvivorship,
        
        [XmlEnum("LeasedFee")]
        LeasedFee,
        
        [XmlEnum("LifeEstate")]
        LifeEstate,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("TenantsByTheEntirety")]
        TenantsByTheEntirety,
        
        [XmlEnum("TenantsInCommon")]
        TenantsInCommon,
        
        [XmlEnum("Unassigned")]
        Unassigned,
    }
}
