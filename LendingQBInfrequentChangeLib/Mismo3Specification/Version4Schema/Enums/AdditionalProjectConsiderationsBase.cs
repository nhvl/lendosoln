namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum AdditionalProjectConsiderationsBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("CondoHotel")]
        CondoHotel,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Timeshare")]
        Timeshare,
    }
}
