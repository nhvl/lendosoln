namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum NeighborhoodPropertyLocationBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Rural")]
        Rural,
        
        [XmlEnum("RuralUrban")]
        RuralUrban,
        
        [XmlEnum("Suburban")]
        Suburban,
        
        [XmlEnum("Urban")]
        Urban,
        
        [XmlEnum("UrbanSprawl")]
        UrbanSprawl,
    }
}
