namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum InvestorOwnershipBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Administered")]
        Administered,
        
        [XmlEnum("Participation")]
        Participation,
        
        [XmlEnum("Pending")]
        Pending,
        
        [XmlEnum("Whole")]
        Whole,
    }
}
