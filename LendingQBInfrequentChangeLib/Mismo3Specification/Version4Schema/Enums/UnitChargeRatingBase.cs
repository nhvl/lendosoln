namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum UnitChargeRatingBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("High")]
        High,
        
        [XmlEnum("Low")]
        Low,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Typical")]
        Typical,
    }
}
