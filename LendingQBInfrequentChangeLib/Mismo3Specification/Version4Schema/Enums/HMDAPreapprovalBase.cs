namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum HMDAPreapprovalBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("NotApplicable")]
        NotApplicable,
        
        [XmlEnum("PreapprovalWasNotRequested")]
        PreapprovalWasNotRequested,
        
        [XmlEnum("PreapprovalWasRequested")]
        PreapprovalWasRequested,
    }
}
