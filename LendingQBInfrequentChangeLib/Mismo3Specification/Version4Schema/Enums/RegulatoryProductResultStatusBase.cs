namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum RegulatoryProductResultStatusBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Clear")]
        Clear,
        
        [XmlEnum("Error")]
        Error,
        
        [XmlEnum("Match")]
        Match,
        
        [XmlEnum("Other")]
        Other,
    }
}
