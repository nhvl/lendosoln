namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum SecuritySystemBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("InteractiveAlarmSystem")]
        InteractiveAlarmSystem,
        
        [XmlEnum("LocalAlarmSystem")]
        LocalAlarmSystem,
        
        [XmlEnum("MonitoredAlarmSystem")]
        MonitoredAlarmSystem,
        
        [XmlEnum("Other")]
        Other,
    }
}
