namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum MIValidationRequestBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Initial")]
        Initial,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Recurring")]
        Recurring,
    }
}
