namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PerChangeInterestRateRoundingTimingBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("RoundRateAfterCheckingMinimumIndexRateMovement")]
        RoundRateAfterCheckingMinimumIndexRateMovement,
        
        [XmlEnum("RoundRateBeforeCheckingMinimumIndexRateMovement")]
        RoundRateBeforeCheckingMinimumIndexRateMovement,
    }
}
