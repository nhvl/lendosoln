namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum InsulationAreaBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Ceiling")]
        Ceiling,
        
        [XmlEnum("Floor")]
        Floor,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Roof")]
        Roof,
        
        [XmlEnum("Walls")]
        Walls,
    }
}
