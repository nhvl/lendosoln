namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum IndexSourceBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("FHLBEleventhDistrictMonthlyCostOfFundsIndex")]
        FHLBEleventhDistrictMonthlyCostOfFundsIndex,
        
        [XmlEnum("LIBOROneMonthWSJ25thDayOfMonth")]
        LIBOROneMonthWSJ25thDayOfMonth,
        
        [XmlEnum("LIBOROneMonthWSJ25thDayOfMonthOrNextBusinessDay")]
        LIBOROneMonthWSJ25thDayOfMonthOrNextBusinessDay,
        
        [XmlEnum("LIBOROneMonthWSJDaily")]
        LIBOROneMonthWSJDaily,
        
        [XmlEnum("LIBOROneMonthWSJFifteenthDayOfMonth")]
        LIBOROneMonthWSJFifteenthDayOfMonth,
        
        [XmlEnum("LIBOROneMonthWSJFifteenthDayOfMonthOrNextBusinessDay")]
        LIBOROneMonthWSJFifteenthDayOfMonthOrNextBusinessDay,
        
        [XmlEnum("LIBOROneYearWSJDaily")]
        LIBOROneYearWSJDaily,
        
        [XmlEnum("LIBORSixMonthWSJ25thDayOfMonth")]
        LIBORSixMonthWSJ25thDayOfMonth,
        
        [XmlEnum("LIBORSixMonthWSJ25thDayOfMonthOrNextBusinessDay")]
        LIBORSixMonthWSJ25thDayOfMonthOrNextBusinessDay,
        
        [XmlEnum("LIBORSixMonthWSJFifteenthDayOfMonth")]
        LIBORSixMonthWSJFifteenthDayOfMonth,
        
        [XmlEnum("LIBORSixMonthWSJFifteenthDayOfMonthOrNextBusinessDay")]
        LIBORSixMonthWSJFifteenthDayOfMonthOrNextBusinessDay,
        
        [XmlEnum("LIBORSixMonthWSJLastBusinessDayOfMonth")]
        LIBORSixMonthWSJLastBusinessDayOfMonth,
        
        [XmlEnum("MonthlyFiveYearTreasurySecuritiesConstantMaturityFRBH15")]
        MonthlyFiveYearTreasurySecuritiesConstantMaturityFRBH15,
        
        [XmlEnum("MonthlyOneYearTreasurySecuritiesConstantMaturityFRBH15")]
        MonthlyOneYearTreasurySecuritiesConstantMaturityFRBH15,
        
        [XmlEnum("MonthlyThreeYearTreasurySecuritiesConstantMaturityFRBH15")]
        MonthlyThreeYearTreasurySecuritiesConstantMaturityFRBH15,
        
        [XmlEnum("NationalAverageContractMortgageRate")]
        NationalAverageContractMortgageRate,
        
        [XmlEnum("NationalMonthlyMedianCostOfFundsIndexOTS")]
        NationalMonthlyMedianCostOfFundsIndexOTS,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PrimeRateWSJEffectiveDate")]
        PrimeRateWSJEffectiveDate,
        
        [XmlEnum("PrimeRateWSJPublicationDate")]
        PrimeRateWSJPublicationDate,
        
        [XmlEnum("SixMonthLIBOR_WSJDaily")]
        SixMonthLIBOR_WSJDaily,
        
        [XmlEnum("SixMonthLIBOR_WSJFirstBusinessDayOfTheMonth")]
        SixMonthLIBOR_WSJFirstBusinessDayOfTheMonth,
        
        [XmlEnum("SixMonthUSTBillMonthlyAuctionDiscountRateCalculated")]
        SixMonthUSTBillMonthlyAuctionDiscountRateCalculated,
        
        [XmlEnum("SixMonthUSTBillMonthlyAuctionInvestmentYieldCalculated")]
        SixMonthUSTBillMonthlyAuctionInvestmentYieldCalculated,
        
        [XmlEnum("SixMonthUSTBillWeeklyAuctionDiscountRateUST")]
        SixMonthUSTBillWeeklyAuctionDiscountRateUST,
        
        [XmlEnum("SixMonthUSTBillWeeklyAuctionInvestmentYieldUST")]
        SixMonthUSTBillWeeklyAuctionInvestmentYieldUST,
        
        [XmlEnum("WeeklyFiveYearTreasurySecuritiesConstantMaturityFRBH15")]
        WeeklyFiveYearTreasurySecuritiesConstantMaturityFRBH15,
        
        [XmlEnum("WeeklyOneYearTreasurySecuritiesConstantMaturityFRBH15")]
        WeeklyOneYearTreasurySecuritiesConstantMaturityFRBH15,
        
        [XmlEnum("WeeklySixMonthCertificateOfDepositSecondaryMarketFRBH15")]
        WeeklySixMonthCertificateOfDepositSecondaryMarketFRBH15,
        
        [XmlEnum("WeeklyTenYearTreasurySecuritiesConstantMaturityFRBH15")]
        WeeklyTenYearTreasurySecuritiesConstantMaturityFRBH15,
        
        [XmlEnum("WeeklyThreeYearTreasurySecuritiesConstantMaturityFRBH15")]
        WeeklyThreeYearTreasurySecuritiesConstantMaturityFRBH15,
    }
}
