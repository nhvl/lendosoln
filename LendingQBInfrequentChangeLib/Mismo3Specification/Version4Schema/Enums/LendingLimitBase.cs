namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum LendingLimitBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Calculated")]
        Calculated,
        
        [XmlEnum("County")]
        County,
        
        [XmlEnum("Investor")]
        Investor,
        
        [XmlEnum("Other")]
        Other,
    }
}
