namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum OwnershipTransferTransactionBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("CashSale")]
        CashSale,
        
        [XmlEnum("DeedTransferOnly")]
        DeedTransferOnly,
        
        [XmlEnum("Other")]
        Other,
    }
}
