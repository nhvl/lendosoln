namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum MIPremiumSequenceBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Fifth")]
        Fifth,
        
        [XmlEnum("First")]
        First,
        
        [XmlEnum("Fourth")]
        Fourth,
        
        [XmlEnum("Second")]
        Second,
        
        [XmlEnum("Third")]
        Third,
    }
}
