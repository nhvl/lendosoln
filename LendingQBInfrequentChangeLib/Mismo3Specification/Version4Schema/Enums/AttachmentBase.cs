namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum AttachmentBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Attached")]
        Attached,
        
        [XmlEnum("Detached")]
        Detached,
        
        [XmlEnum("SemiDetached")]
        SemiDetached,
    }
}
