namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum MeasurementUnitBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Centimeters")]
        Centimeters,
        
        [XmlEnum("Inches")]
        Inches,
        
        [XmlEnum("Pixels")]
        Pixels,
        
        [XmlEnum("Points")]
        Points,
    }
}
