namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum URLAStatusBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Final")]
        Final,
        
        [XmlEnum("Interim")]
        Interim,
    }
}
