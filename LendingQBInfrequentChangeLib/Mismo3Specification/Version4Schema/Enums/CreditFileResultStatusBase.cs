namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum CreditFileResultStatusBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("FileReturned")]
        FileReturned,
        
        [XmlEnum("NoFileReturnedBadAccessCode")]
        NoFileReturnedBadAccessCode,
        
        [XmlEnum("NoFileReturnedBorrowerIsAMinor")]
        NoFileReturnedBorrowerIsAMinor,
        
        [XmlEnum("NoFileReturnedCreditFreeze")]
        NoFileReturnedCreditFreeze,
        
        [XmlEnum("NoFileReturnedError")]
        NoFileReturnedError,
        
        [XmlEnum("NoFileReturnedFraudulentRequestData")]
        NoFileReturnedFraudulentRequestData,
        
        [XmlEnum("NoFileReturnedInadequateRequestData")]
        NoFileReturnedInadequateRequestData,
        
        [XmlEnum("NoFileReturnedNoHit")]
        NoFileReturnedNoHit,
        
        [XmlEnum("NoFileReturnedThinFile")]
        NoFileReturnedThinFile,
        
        [XmlEnum("Other")]
        Other,
    }
}
