namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ValuationUseBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Comparable")]
        Comparable,
        
        [XmlEnum("SubjectProperty")]
        SubjectProperty,
    }
}
