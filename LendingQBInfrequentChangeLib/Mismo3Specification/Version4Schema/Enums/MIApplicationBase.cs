namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum MIApplicationBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Delegated")]
        Delegated,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Standard")]
        Standard,
    }
}
