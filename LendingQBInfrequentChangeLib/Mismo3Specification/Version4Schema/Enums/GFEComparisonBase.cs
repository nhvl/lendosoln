namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum GFEComparisonBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("NotApplicable")]
        NotApplicable,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("SameLoanWithLowerInterestRate")]
        SameLoanWithLowerInterestRate,
        
        [XmlEnum("SameLoanWithLowerSettlementCharges")]
        SameLoanWithLowerSettlementCharges,
    }
}
