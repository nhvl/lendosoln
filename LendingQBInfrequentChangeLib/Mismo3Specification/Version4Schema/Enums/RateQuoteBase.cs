namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum RateQuoteBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Detail")]
        Detail,
        
        [XmlEnum("Estimated")]
        Estimated,
        
        [XmlEnum("Other")]
        Other,
    }
}
