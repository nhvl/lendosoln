namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum LiabilityBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("BorrowerEstimatedTotalMonthlyLiabilityPayment")]
        BorrowerEstimatedTotalMonthlyLiabilityPayment,
        
        [XmlEnum("CollectionsJudgmentsAndLiens")]
        CollectionsJudgmentsAndLiens,
        
        [XmlEnum("DeferredStudentLoan")]
        DeferredStudentLoan,
        
        [XmlEnum("DelinquentTaxes")]
        DelinquentTaxes,
        
        [XmlEnum("FirstPositionMortgageLien")]
        FirstPositionMortgageLien,
        
        [XmlEnum("Garnishments")]
        Garnishments,
        
        [XmlEnum("HELOC")]
        HELOC,
        
        [XmlEnum("HomeownersAssociationLien")]
        HomeownersAssociationLien,
        
        [XmlEnum("Installment")]
        Installment,
        
        [XmlEnum("LeasePayment")]
        LeasePayment,
        
        [XmlEnum("MonetaryJudgment")]
        MonetaryJudgment,
        
        [XmlEnum("MortgageLoan")]
        MortgageLoan,
        
        [XmlEnum("Open30DayChargeAccount")]
        Open30DayChargeAccount,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PersonalLoan")]
        PersonalLoan,
        
        [XmlEnum("Revolving")]
        Revolving,
        
        [XmlEnum("SecondPositionMortgageLien")]
        SecondPositionMortgageLien,
        
        [XmlEnum("Taxes")]
        Taxes,
        
        [XmlEnum("TaxLien")]
        TaxLien,
        
        [XmlEnum("ThirdPositionMortgageLien")]
        ThirdPositionMortgageLien,
        
        [XmlEnum("UnsecuredHomeImprovementLoanInstallment")]
        UnsecuredHomeImprovementLoanInstallment,
        
        [XmlEnum("UnsecuredHomeImprovementLoanRevolving")]
        UnsecuredHomeImprovementLoanRevolving,
    }
}
