namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ComparableAdjustmentBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Additions")]
        Additions,
        
        [XmlEnum("AdjustedMonthlyRent")]
        AdjustedMonthlyRent,
        
        [XmlEnum("Age")]
        Age,
        
        [XmlEnum("BasementArea")]
        BasementArea,
        
        [XmlEnum("BasementFinish")]
        BasementFinish,
        
        [XmlEnum("CarStorage")]
        CarStorage,
        
        [XmlEnum("CommonElements")]
        CommonElements,
        
        [XmlEnum("Condition")]
        Condition,
        
        [XmlEnum("DateOfSale")]
        DateOfSale,
        
        [XmlEnum("DesignAppeal")]
        DesignAppeal,
        
        [XmlEnum("DesignStyle")]
        DesignStyle,
        
        [XmlEnum("EffectiveAge")]
        EffectiveAge,
        
        [XmlEnum("EnergyEfficient")]
        EnergyEfficient,
        
        [XmlEnum("FencePool")]
        FencePool,
        
        [XmlEnum("FinancingConcessions")]
        FinancingConcessions,
        
        [XmlEnum("Fireplaces")]
        Fireplaces,
        
        [XmlEnum("FloorLocation")]
        FloorLocation,
        
        [XmlEnum("Forecasting")]
        Forecasting,
        
        [XmlEnum("FunctionalUtility")]
        FunctionalUtility,
        
        [XmlEnum("Furnishings")]
        Furnishings,
        
        [XmlEnum("GarageCarport")]
        GarageCarport,
        
        [XmlEnum("GrossBuildingArea")]
        GrossBuildingArea,
        
        [XmlEnum("GrossLivingArea")]
        GrossLivingArea,
        
        [XmlEnum("HeatingCooling")]
        HeatingCooling,
        
        [XmlEnum("InteriorAppeal")]
        InteriorAppeal,
        
        [XmlEnum("Location")]
        Location,
        
        [XmlEnum("MaintenanceFees")]
        MaintenanceFees,
        
        [XmlEnum("Make")]
        Make,
        
        [XmlEnum("MarketChange")]
        MarketChange,
        
        [XmlEnum("MonthlyFacilityFee")]
        MonthlyFacilityFee,
        
        [XmlEnum("NeighborhoodAppeal")]
        NeighborhoodAppeal,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Parking")]
        Parking,
        
        [XmlEnum("PorchDeck")]
        PorchDeck,
        
        [XmlEnum("ProjectAmenities")]
        ProjectAmenities,
        
        [XmlEnum("ProjectSecurity")]
        ProjectSecurity,
        
        [XmlEnum("ProjectSize")]
        ProjectSize,
        
        [XmlEnum("PropertyRights")]
        PropertyRights,
        
        [XmlEnum("Quality")]
        Quality,
        
        [XmlEnum("RecreationFacilities")]
        RecreationFacilities,
        
        [XmlEnum("RemodelBath")]
        RemodelBath,
        
        [XmlEnum("RemodelKitchen")]
        RemodelKitchen,
        
        [XmlEnum("RentConcessions")]
        RentConcessions,
        
        [XmlEnum("RoomsBelowGrade")]
        RoomsBelowGrade,
        
        [XmlEnum("SalesConcessions")]
        SalesConcessions,
        
        [XmlEnum("SecurityFeatures")]
        SecurityFeatures,
        
        [XmlEnum("SignificantFeatures")]
        SignificantFeatures,
        
        [XmlEnum("SiteAppeal")]
        SiteAppeal,
        
        [XmlEnum("SiteArea")]
        SiteArea,
        
        [XmlEnum("SiteValue")]
        SiteValue,
        
        [XmlEnum("TipOut")]
        TipOut,
        
        [XmlEnum("TotalBathroomCountAboveGrade")]
        TotalBathroomCountAboveGrade,
        
        [XmlEnum("TotalBedroomCountAboveGrade")]
        TotalBedroomCountAboveGrade,
        
        [XmlEnum("Utilities")]
        Utilities,
        
        [XmlEnum("View")]
        View,
    }
}
