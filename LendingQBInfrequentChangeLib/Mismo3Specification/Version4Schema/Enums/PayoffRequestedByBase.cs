namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PayoffRequestedByBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Lender")]
        Lender,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("ServiceProvider")]
        ServiceProvider,
    }
}
