namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ForeclosureDelayEndReasonBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AttorneyTransferReviewCompleted")]
        AttorneyTransferReviewCompleted,
        
        [XmlEnum("BankruptcyStayLifted")]
        BankruptcyStayLifted,
        
        [XmlEnum("BorrowerRequestedToExit")]
        BorrowerRequestedToExit,
        
        [XmlEnum("LegalProcessEnded")]
        LegalProcessEnded,
        
        [XmlEnum("LegislativeImpactAssessed")]
        LegislativeImpactAssessed,
        
        [XmlEnum("LoanPayoffReceived")]
        LoanPayoffReceived,
        
        [XmlEnum("MediationEnded")]
        MediationEnded,
        
        [XmlEnum("MilitaryIndulgenceEnded")]
        MilitaryIndulgenceEnded,
        
        [XmlEnum("MissingDocumentsReceived")]
        MissingDocumentsReceived,
        
        [XmlEnum("MoratoriumEnded")]
        MoratoriumEnded,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("ProbateEnded")]
        ProbateEnded,
        
        [XmlEnum("PropertyConditionResolved")]
        PropertyConditionResolved,
        
        [XmlEnum("PropertySeizureResolved")]
        PropertySeizureResolved,
        
        [XmlEnum("ReinstatementReceived")]
        ReinstatementReceived,
        
        [XmlEnum("ServicingTransferCompleted")]
        ServicingTransferCompleted,
        
        [XmlEnum("TitleIssueResolved")]
        TitleIssueResolved,
        
        [XmlEnum("WorkoutEnded")]
        WorkoutEnded,
    }
}
