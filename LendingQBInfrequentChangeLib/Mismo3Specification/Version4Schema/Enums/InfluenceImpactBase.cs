namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum InfluenceImpactBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Adverse")]
        Adverse,
        
        [XmlEnum("Beneficial")]
        Beneficial,
        
        [XmlEnum("Neutral")]
        Neutral,
    }
}
