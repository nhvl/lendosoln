namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum TrusteeBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Primary")]
        Primary,
        
        [XmlEnum("Secondary")]
        Secondary,
    }
}
