namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum HMDAManufacturedHomeLegalClassificationBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("NotApplicable")]
        NotApplicable,
        
        [XmlEnum("PersonalProperty")]
        PersonalProperty,
        
        [XmlEnum("RealProperty")]
        RealProperty,
    }
}
