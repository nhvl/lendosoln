namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ConstructionToPermanentClosingBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("OneClosing")]
        OneClosing,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("TwoClosing")]
        TwoClosing,
    }
}
