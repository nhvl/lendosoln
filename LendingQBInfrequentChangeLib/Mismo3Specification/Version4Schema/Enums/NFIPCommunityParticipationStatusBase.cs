namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum NFIPCommunityParticipationStatusBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Emergency")]
        Emergency,
        
        [XmlEnum("NonParticipating")]
        NonParticipating,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Probation")]
        Probation,
        
        [XmlEnum("Regular")]
        Regular,
        
        [XmlEnum("Suspended")]
        Suspended,
        
        [XmlEnum("Unknown")]
        Unknown,
    }
}
