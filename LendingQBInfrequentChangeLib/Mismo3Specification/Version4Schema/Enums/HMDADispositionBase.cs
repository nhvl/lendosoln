namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum HMDADispositionBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("ApplicationApprovedButNotAccepted")]
        ApplicationApprovedButNotAccepted,
        
        [XmlEnum("ApplicationDenied")]
        ApplicationDenied,
        
        [XmlEnum("ApplicationWithdrawn")]
        ApplicationWithdrawn,
        
        [XmlEnum("FileClosedForIncompleteness")]
        FileClosedForIncompleteness,
        
        [XmlEnum("LoanOriginated")]
        LoanOriginated,
        
        [XmlEnum("LoanPurchasedByYourInstitution")]
        LoanPurchasedByYourInstitution,
        
        [XmlEnum("PreapprovalRequestApprovedButNotAccepted")]
        PreapprovalRequestApprovedButNotAccepted,
        
        [XmlEnum("PreapprovalRequestDenied")]
        PreapprovalRequestDenied,
    }
}
