namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ApplicationTakenMethodBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Email")]
        Email,
        
        [XmlEnum("FaceToFace")]
        FaceToFace,
        
        [XmlEnum("Fax")]
        Fax,
        
        [XmlEnum("Internet")]
        Internet,
        
        [XmlEnum("Mail")]
        Mail,
        
        [XmlEnum("Telephone")]
        Telephone,
    }
}
