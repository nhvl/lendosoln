namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum FeePaymentResponsiblePartyBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Branch")]
        Branch,
        
        [XmlEnum("Broker")]
        Broker,
        
        [XmlEnum("Buyer")]
        Buyer,
        
        [XmlEnum("Lender")]
        Lender,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Seller")]
        Seller,
    }
}
