namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum HomeownerPastThreeYearsBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("No")]
        No,
        
        [XmlEnum("Unknown")]
        Unknown,
        
        [XmlEnum("Yes")]
        Yes,
    }
}
