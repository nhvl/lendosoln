namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PropertyZoningCategoryBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Agricultural")]
        Agricultural,
        
        [XmlEnum("Commercial")]
        Commercial,
        
        [XmlEnum("Industrial")]
        Industrial,
        
        [XmlEnum("NonResidentialGrandfatheredResidential")]
        NonResidentialGrandfatheredResidential,
        
        [XmlEnum("NoZoning")]
        NoZoning,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Residential")]
        Residential,
    }
}
