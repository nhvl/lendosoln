namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ConstructionLoanEstimatedInterestCalculationMethodBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("FullLoanCommitment")]
        FullLoanCommitment,
        
        [XmlEnum("HalfLoanCommitment")]
        HalfLoanCommitment,
    }
}
