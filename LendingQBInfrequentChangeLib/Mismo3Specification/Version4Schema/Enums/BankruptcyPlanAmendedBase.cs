namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum BankruptcyPlanAmendedBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("ChapterFifteen")]
        ChapterFifteen,
        
        [XmlEnum("PlanDateExtended")]
        PlanDateExtended,
        
        [XmlEnum("PlanDisbursementAmountIncreased")]
        PlanDisbursementAmountIncreased,
        
        [XmlEnum("PlanDisbursementAmountReduced")]
        PlanDisbursementAmountReduced,
        
        [XmlEnum("PlanLengthReduced")]
        PlanLengthReduced,
    }
}
