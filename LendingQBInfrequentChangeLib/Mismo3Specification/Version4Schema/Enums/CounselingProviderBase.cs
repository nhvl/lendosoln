namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum CounselingProviderBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AmericanHomeownerEducationAndCounselingInstituteApproved")]
        AmericanHomeownerEducationAndCounselingInstituteApproved,
        
        [XmlEnum("HUDApprovedCounselingAgency")]
        HUDApprovedCounselingAgency,
        
        [XmlEnum("LenderTrained")]
        LenderTrained,
        
        [XmlEnum("LocalHousingCounselingAgency")]
        LocalHousingCounselingAgency,
        
        [XmlEnum("MultiStateOrganization")]
        MultiStateOrganization,
        
        [XmlEnum("NationalIntermediary")]
        NationalIntermediary,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("RegionalIntermediary")]
        RegionalIntermediary,
        
        [XmlEnum("StateHousingFinanceAgency")]
        StateHousingFinanceAgency,
        
        [XmlEnum("SubGrantee")]
        SubGrantee,
        
        [XmlEnum("Unknown")]
        Unknown,
    }
}
