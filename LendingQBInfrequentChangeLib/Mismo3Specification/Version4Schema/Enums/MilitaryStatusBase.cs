namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum MilitaryStatusBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("ActiveDuty")]
        ActiveDuty,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("ReserveNationalGuardNeverActivated")]
        ReserveNationalGuardNeverActivated,
        
        [XmlEnum("Separated")]
        Separated,
        
        [XmlEnum("Veteran")]
        Veteran,
    }
}
