namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ActiveWorkoutDocumentStatusBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Complete")]
        Complete,
        
        [XmlEnum("Incomplete")]
        Incomplete,
        
        [XmlEnum("InReview")]
        InReview,
        
        [XmlEnum("NoResponse")]
        NoResponse,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Resubmission")]
        Resubmission,
    }
}
