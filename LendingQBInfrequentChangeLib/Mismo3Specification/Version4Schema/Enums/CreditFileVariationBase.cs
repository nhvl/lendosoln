namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum CreditFileVariationBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("DifferentAddress")]
        DifferentAddress,
        
        [XmlEnum("DifferentBirthDate")]
        DifferentBirthDate,
        
        [XmlEnum("DifferentGeneration")]
        DifferentGeneration,
        
        [XmlEnum("DifferentLastName")]
        DifferentLastName,
        
        [XmlEnum("DifferentName")]
        DifferentName,
        
        [XmlEnum("DifferentSSN")]
        DifferentSSN,
    }
}
