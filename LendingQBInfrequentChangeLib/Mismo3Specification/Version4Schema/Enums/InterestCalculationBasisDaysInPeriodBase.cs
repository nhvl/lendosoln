namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum InterestCalculationBasisDaysInPeriodBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("30Days")]
        Item30Days,
        
        [XmlEnum("DaysBetweenPayments")]
        DaysBetweenPayments,
        
        [XmlEnum("DaysInCalendarMonth")]
        DaysInCalendarMonth,
        
        [XmlEnum("Other")]
        Other,
    }
}
