namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum MIDurationBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Annual")]
        Annual,
        
        [XmlEnum("NotApplicable")]
        NotApplicable,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PeriodicMonthly")]
        PeriodicMonthly,
        
        [XmlEnum("SingleLifeOfLoan")]
        SingleLifeOfLoan,
        
        [XmlEnum("SingleSpecific")]
        SingleSpecific,
        
        [XmlEnum("SplitPremium")]
        SplitPremium,
    }
}
