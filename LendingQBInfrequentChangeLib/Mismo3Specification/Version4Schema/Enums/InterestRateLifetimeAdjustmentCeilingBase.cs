namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum InterestRateLifetimeAdjustmentCeilingBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AbsoluteRequiredCeiling")]
        AbsoluteRequiredCeiling,
        
        [XmlEnum("FactorAddedToTheGreaterOfTheNetCommitmentYieldOrTheOriginalNoteRate")]
        FactorAddedToTheGreaterOfTheNetCommitmentYieldOrTheOriginalNoteRate,
        
        [XmlEnum("FactorAddedToTheNetCommitmentYield")]
        FactorAddedToTheNetCommitmentYield,
        
        [XmlEnum("FactorAddedToTheOriginalNoteRate")]
        FactorAddedToTheOriginalNoteRate,
        
        [XmlEnum("NotApplicable")]
        NotApplicable,
        
        [XmlEnum("Other")]
        Other,
    }
}
