namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum VALoanProgramBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("VADirectHomeLoanProgramForNativeAmericans")]
        VADirectHomeLoanProgramForNativeAmericans,
        
        [XmlEnum("VAGuaranteedCondominiumUnit")]
        VAGuaranteedCondominiumUnit,
        
        [XmlEnum("VAGuaranteedFarmResidenceConstruction")]
        VAGuaranteedFarmResidenceConstruction,
        
        [XmlEnum("VAGuaranteedFarmResidenceEnergyEfficiencyImprovements")]
        VAGuaranteedFarmResidenceEnergyEfficiencyImprovements,
        
        [XmlEnum("VAGuaranteedFarmResidencePurchase")]
        VAGuaranteedFarmResidencePurchase,
        
        [XmlEnum("VAGuaranteedFarmResidenceRepairAlternationOrImprovement")]
        VAGuaranteedFarmResidenceRepairAlternationOrImprovement,
        
        [XmlEnum("VAGuaranteedHomePurchaseOrConstruction")]
        VAGuaranteedHomePurchaseOrConstruction,
        
        [XmlEnum("VAGuaranteedHomePurchaseWithEnergyEfficiencyImprovements")]
        VAGuaranteedHomePurchaseWithEnergyEfficiencyImprovements,
        
        [XmlEnum("VAGuaranteedHomeRefinanceWithEnergyEfficiencyImprovements")]
        VAGuaranteedHomeRefinanceWithEnergyEfficiencyImprovements,
        
        [XmlEnum("VAGuaranteedManufacturedHomeOrLotPurchaseOrRefinance")]
        VAGuaranteedManufacturedHomeOrLotPurchaseOrRefinance,
        
        [XmlEnum("VAGuaranteedRefinance")]
        VAGuaranteedRefinance,
        
        [XmlEnum("VAGuaranteedRefinanceLiensSecuredByVADwelling")]
        VAGuaranteedRefinanceLiensSecuredByVADwelling,
    }
}
