namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum MISubPrimeProgramBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AMinus")]
        AMinus,
        
        [XmlEnum("BPaper")]
        BPaper,
        
        [XmlEnum("CPaper")]
        CPaper,
        
        [XmlEnum("Other")]
        Other,
    }
}
