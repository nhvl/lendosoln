namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum DisclosureDeliveryMethodBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("CourierDeliveryService")]
        CourierDeliveryService,
        
        [XmlEnum("ElectronicDelivery")]
        ElectronicDelivery,
        
        [XmlEnum("InPerson")]
        InPerson,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("USPSFirstClassMail")]
        USPSFirstClassMail,
    }
}
