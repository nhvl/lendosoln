namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum EscrowAbsenceReasonBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("BorrowerDeclined")]
        BorrowerDeclined,
        
        [XmlEnum("LenderDoesNotOffer")]
        LenderDoesNotOffer,
        
        [XmlEnum("LenderDoesNotRequire")]
        LenderDoesNotRequire,
        
        [XmlEnum("Other")]
        Other,
    }
}
