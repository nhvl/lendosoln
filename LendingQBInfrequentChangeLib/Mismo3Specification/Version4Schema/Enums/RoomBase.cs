namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum RoomBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Bedroom")]
        Bedroom,
        
        [XmlEnum("BonusRoom")]
        BonusRoom,
        
        [XmlEnum("Den")]
        Den,
        
        [XmlEnum("DiningRoom")]
        DiningRoom,
        
        [XmlEnum("EnsuiteFullBath")]
        EnsuiteFullBath,
        
        [XmlEnum("ExerciseRoom")]
        ExerciseRoom,
        
        [XmlEnum("FamilyRoom")]
        FamilyRoom,
        
        [XmlEnum("Foyer")]
        Foyer,
        
        [XmlEnum("FullBathroom")]
        FullBathroom,
        
        [XmlEnum("GameRoom")]
        GameRoom,
        
        [XmlEnum("GreatRoom")]
        GreatRoom,
        
        [XmlEnum("HalfBath")]
        HalfBath,
        
        [XmlEnum("Kitchen")]
        Kitchen,
        
        [XmlEnum("Laundry")]
        Laundry,
        
        [XmlEnum("Library")]
        Library,
        
        [XmlEnum("LivingRoom")]
        LivingRoom,
        
        [XmlEnum("MasterBedroom")]
        MasterBedroom,
        
        [XmlEnum("MediaRoom")]
        MediaRoom,
        
        [XmlEnum("Office")]
        Office,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("RecreationRoom")]
        RecreationRoom,
        
        [XmlEnum("UtilityRoom")]
        UtilityRoom,
        
        [XmlEnum("Workshop")]
        Workshop,
    }
}
