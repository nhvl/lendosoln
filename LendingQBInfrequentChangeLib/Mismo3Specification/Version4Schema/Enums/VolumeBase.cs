namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum VolumeBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Deed")]
        Deed,
        
        [XmlEnum("Maps")]
        Maps,
        
        [XmlEnum("Mortgage")]
        Mortgage,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Plat")]
        Plat,
    }
}
