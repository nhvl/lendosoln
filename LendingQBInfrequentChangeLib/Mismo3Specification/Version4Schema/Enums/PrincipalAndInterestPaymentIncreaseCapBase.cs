namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PrincipalAndInterestPaymentIncreaseCapBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Mandatory")]
        Mandatory,
        
        [XmlEnum("Optional")]
        Optional,
    }
}
