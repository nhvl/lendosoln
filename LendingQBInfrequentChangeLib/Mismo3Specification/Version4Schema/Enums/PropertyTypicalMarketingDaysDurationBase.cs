namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PropertyTypicalMarketingDaysDurationBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("30To120Days")]
        Item30To120Days,
        
        [XmlEnum("Over120Days")]
        Over120Days,
        
        [XmlEnum("Under30Days")]
        Under30Days,
    }
}
