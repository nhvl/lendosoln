namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum LoanProductStatusBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Available")]
        Available,
        
        [XmlEnum("Eligible")]
        Eligible,
        
        [XmlEnum("Ineligible")]
        Ineligible,
        
        [XmlEnum("Offered")]
        Offered,
        
        [XmlEnum("Requested")]
        Requested,
        
        [XmlEnum("Selected")]
        Selected,
    }
}
