namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum BankruptcyDispositionBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Confirmed")]
        Confirmed,
        
        [XmlEnum("Discharged")]
        Discharged,
        
        [XmlEnum("Dismissed")]
        Dismissed,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Pending")]
        Pending,
    }
}
