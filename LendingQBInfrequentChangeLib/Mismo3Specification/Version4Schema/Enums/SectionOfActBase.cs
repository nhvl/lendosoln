namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum SectionOfActBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("184")]
        Item184,
        
        [XmlEnum("184A")]
        Item184A,
        
        [XmlEnum("201S")]
        Item201S,
        
        [XmlEnum("201SD")]
        Item201SD,
        
        [XmlEnum("201U")]
        Item201U,
        
        [XmlEnum("201UD")]
        Item201UD,
        
        [XmlEnum("203B")]
        Item203B,
        
        [XmlEnum("203B2")]
        Item203B2,
        
        [XmlEnum("203B241")]
        Item203B241,
        
        [XmlEnum("203B251")]
        Item203B251,
        
        [XmlEnum("203H")]
        Item203H,
        
        [XmlEnum("203I")]
        Item203I,
        
        [XmlEnum("203K")]
        Item203K,
        
        [XmlEnum("203K241")]
        Item203K241,
        
        [XmlEnum("203K251")]
        Item203K251,
        
        [XmlEnum("204A")]
        Item204A,
        
        [XmlEnum("204GFHAGoodNeighborNextDoor")]
        Item204GFHAGoodNeighborNextDoor,
        
        [XmlEnum("204GSingleFamilyPropertyDisposition")]
        Item204GSingleFamilyPropertyDisposition,
        
        [XmlEnum("213")]
        Item213,
        
        [XmlEnum("220")]
        Item220,
        
        [XmlEnum("221")]
        Item221,
        
        [XmlEnum("221D2")]
        Item221D2,
        
        [XmlEnum("221D2251")]
        Item221D2251,
        
        [XmlEnum("222")]
        Item222,
        
        [XmlEnum("223E")]
        Item223E,
        
        [XmlEnum("233")]
        Item233,
        
        [XmlEnum("234C")]
        Item234C,
        
        [XmlEnum("234C251")]
        Item234C251,
        
        [XmlEnum("235")]
        Item235,
        
        [XmlEnum("237")]
        Item237,
        
        [XmlEnum("240")]
        Item240,
        
        [XmlEnum("245")]
        Item245,
        
        [XmlEnum("245A")]
        Item245A,
        
        [XmlEnum("247")]
        Item247,
        
        [XmlEnum("248")]
        Item248,
        
        [XmlEnum("251")]
        Item251,
        
        [XmlEnum("255")]
        Item255,
        
        [XmlEnum("256")]
        Item256,
        
        [XmlEnum("257")]
        Item257,
        
        [XmlEnum("26101")]
        Item26101,
        
        [XmlEnum("26102")]
        Item26102,
        
        [XmlEnum("26201")]
        Item26201,
        
        [XmlEnum("26202")]
        Item26202,
        
        [XmlEnum("27001")]
        Item27001,
        
        [XmlEnum("27002")]
        Item27002,
        
        [XmlEnum("27003")]
        Item27003,
        
        [XmlEnum("3710")]
        Item3710,
        
        [XmlEnum("502USDARuralDevelopmentGuaranteedHousingLoan")]
        Item502USDARuralDevelopmentGuaranteedHousingLoan,
        
        [XmlEnum("513")]
        Item513,
        
        [XmlEnum("729")]
        Item729,
        
        [XmlEnum("8Y")]
        Item8Y,
        
        [XmlEnum("FHATitleIInsuranceForManufacturedHomes")]
        FHATitleIInsuranceForManufacturedHomes,
        
        [XmlEnum("FHATitleIInsuranceForPropertyImprovementLoans")]
        FHATitleIInsuranceForPropertyImprovementLoans,
        
        [XmlEnum("Other")]
        Other,
    }
}
