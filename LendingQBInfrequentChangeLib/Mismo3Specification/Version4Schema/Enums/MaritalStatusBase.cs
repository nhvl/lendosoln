namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum MaritalStatusBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Divorced")]
        Divorced,
        
        [XmlEnum("Married")]
        Married,
        
        [XmlEnum("NotProvided")]
        NotProvided,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Separated")]
        Separated,
        
        [XmlEnum("Unknown")]
        Unknown,
        
        [XmlEnum("Unmarried")]
        Unmarried,
    }
}
