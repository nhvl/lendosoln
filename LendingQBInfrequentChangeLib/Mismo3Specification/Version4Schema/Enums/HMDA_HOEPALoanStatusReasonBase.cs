namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum HMDA_HOEPALoanStatusReasonBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("HOEPALoanBecauseOfAPR")]
        HOEPALoanBecauseOfAPR,
        
        [XmlEnum("HOEPALoanBecauseOfBothAPRAndPointsAndFees")]
        HOEPALoanBecauseOfBothAPRAndPointsAndFees,
        
        [XmlEnum("HOEPALoanBecauseOfPointsAndFees")]
        HOEPALoanBecauseOfPointsAndFees,
        
        [XmlEnum("Other")]
        Other,
    }
}
