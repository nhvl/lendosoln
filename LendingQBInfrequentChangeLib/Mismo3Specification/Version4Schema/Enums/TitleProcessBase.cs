namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum TitleProcessBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("DeedReport")]
        DeedReport,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("TitleSearch")]
        TitleSearch,
    }
}
