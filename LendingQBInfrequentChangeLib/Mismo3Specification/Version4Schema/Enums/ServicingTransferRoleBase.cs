namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ServicingTransferRoleBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("ServicingTransferee")]
        ServicingTransferee,
        
        [XmlEnum("ServicingTransferor")]
        ServicingTransferor,
    }
}
