namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum CreditScoreCategoryVersionBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("FICO4")]
        FICO4,
        
        [XmlEnum("FICO8")]
        FICO8,
        
        [XmlEnum("FICO9")]
        FICO9,
        
        [XmlEnum("FICO98")]
        FICO98,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Vantage2")]
        Vantage2,
        
        [XmlEnum("Vantage3")]
        Vantage3,
    }
}
