namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ConstructionPhaseInterestPaymentBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("InterestPaidAtEndOfConstruction")]
        InterestPaidAtEndOfConstruction,
        
        [XmlEnum("InterestPaidPeriodically")]
        InterestPaidPeriodically,
        
        [XmlEnum("Other")]
        Other,
    }
}
