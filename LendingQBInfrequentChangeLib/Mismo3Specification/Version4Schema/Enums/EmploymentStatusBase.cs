namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum EmploymentStatusBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Current")]
        Current,
        
        [XmlEnum("Previous")]
        Previous,
    }
}
