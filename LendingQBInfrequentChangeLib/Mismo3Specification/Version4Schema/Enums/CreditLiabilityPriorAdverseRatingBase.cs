namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum CreditLiabilityPriorAdverseRatingBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AsAgreed")]
        AsAgreed,
        
        [XmlEnum("BankruptcyOrWageEarnerPlan")]
        BankruptcyOrWageEarnerPlan,
        
        [XmlEnum("ChargeOff")]
        ChargeOff,
        
        [XmlEnum("Collection")]
        Collection,
        
        [XmlEnum("CollectionOrChargeOff")]
        CollectionOrChargeOff,
        
        [XmlEnum("Foreclosure")]
        Foreclosure,
        
        [XmlEnum("ForeclosureOrRepossession")]
        ForeclosureOrRepossession,
        
        [XmlEnum("Late30Days")]
        Late30Days,
        
        [XmlEnum("Late60Days")]
        Late60Days,
        
        [XmlEnum("Late90Days")]
        Late90Days,
        
        [XmlEnum("LateOver120Days")]
        LateOver120Days,
        
        [XmlEnum("NoDataAvailable")]
        NoDataAvailable,
        
        [XmlEnum("Repossession")]
        Repossession,
        
        [XmlEnum("TooNew")]
        TooNew,
        
        [XmlEnum("WageEarnerPlan")]
        WageEarnerPlan,
    }
}
