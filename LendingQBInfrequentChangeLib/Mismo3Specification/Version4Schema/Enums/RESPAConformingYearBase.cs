namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum RESPAConformingYearBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("January2010")]
        January2010,
        
        [XmlEnum("October2015")]
        October2015,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Pre2010")]
        Pre2010,
    }
}
