namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum RestrictionBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Age")]
        Age,
        
        [XmlEnum("AnimalPet")]
        AnimalPet,
        
        [XmlEnum("Architectural")]
        Architectural,
        
        [XmlEnum("CommonArea")]
        CommonArea,
        
        [XmlEnum("HistoricPreservation")]
        HistoricPreservation,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PropertyUse")]
        PropertyUse,
        
        [XmlEnum("Racial")]
        Racial,
        
        [XmlEnum("Rental")]
        Rental,
        
        [XmlEnum("Resale")]
        Resale,
        
        [XmlEnum("Unenforceable")]
        Unenforceable,
    }
}
