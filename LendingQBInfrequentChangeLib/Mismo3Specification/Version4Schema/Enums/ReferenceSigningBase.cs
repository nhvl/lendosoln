namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ReferenceSigningBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("NotSigned")]
        NotSigned,
        
        [XmlEnum("ReferenceAndContentSignedDirectly")]
        ReferenceAndContentSignedDirectly,
        
        [XmlEnum("ReferenceAndContentSignedIndirectly")]
        ReferenceAndContentSignedIndirectly,
        
        [XmlEnum("ReferenceCreatedFromSignedContent")]
        ReferenceCreatedFromSignedContent,
        
        [XmlEnum("ReferenceSignedDirectly")]
        ReferenceSignedDirectly,
    }
}
