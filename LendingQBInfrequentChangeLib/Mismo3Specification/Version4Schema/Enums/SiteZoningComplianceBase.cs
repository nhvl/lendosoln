namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum SiteZoningComplianceBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Illegal")]
        Illegal,
        
        [XmlEnum("Legal")]
        Legal,
        
        [XmlEnum("LegalNonConforming")]
        LegalNonConforming,
        
        [XmlEnum("NoZoning")]
        NoZoning,
        
        [XmlEnum("Undetermined")]
        Undetermined,
        
        [XmlEnum("Unknown")]
        Unknown,
    }
}
