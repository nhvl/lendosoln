namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum OffSiteImprovementBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Alley")]
        Alley,
        
        [XmlEnum("CurbGutter")]
        CurbGutter,
        
        [XmlEnum("Gated")]
        Gated,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Sidewalk")]
        Sidewalk,
        
        [XmlEnum("Street")]
        Street,
        
        [XmlEnum("StreetAccess")]
        StreetAccess,
        
        [XmlEnum("StreetLighting")]
        StreetLighting,
        
        [XmlEnum("StreetMaintenance")]
        StreetMaintenance,
        
        [XmlEnum("StreetSurface")]
        StreetSurface,
    }
}
