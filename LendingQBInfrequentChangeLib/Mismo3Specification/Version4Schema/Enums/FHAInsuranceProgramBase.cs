namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum FHAInsuranceProgramBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("FHAUpfront")]
        FHAUpfront,
        
        [XmlEnum("RBP")]
        RBP,
        
        [XmlEnum("Section530")]
        Section530,
    }
}
