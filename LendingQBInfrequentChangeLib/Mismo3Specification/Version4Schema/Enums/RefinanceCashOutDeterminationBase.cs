namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum RefinanceCashOutDeterminationBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("CashOut")]
        CashOut,
        
        [XmlEnum("LimitedCashOut")]
        LimitedCashOut,
        
        [XmlEnum("NoCashOut")]
        NoCashOut,
        
        [XmlEnum("Unknown")]
        Unknown,
    }
}
