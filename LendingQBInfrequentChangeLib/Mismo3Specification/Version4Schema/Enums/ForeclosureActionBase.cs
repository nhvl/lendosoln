namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ForeclosureActionBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("BreachNoticeSent")]
        BreachNoticeSent,
        
        [XmlEnum("EvictionApproved")]
        EvictionApproved,
        
        [XmlEnum("EvictionCompleted")]
        EvictionCompleted,
        
        [XmlEnum("EvictionStarted")]
        EvictionStarted,
        
        [XmlEnum("FirstLegalAction")]
        FirstLegalAction,
        
        [XmlEnum("ForeclosureCompleted")]
        ForeclosureCompleted,
        
        [XmlEnum("ForeclosureSaleCancelled")]
        ForeclosureSaleCancelled,
        
        [XmlEnum("ForeclosureSaleConfirmed")]
        ForeclosureSaleConfirmed,
        
        [XmlEnum("ForeclosureSaleHeld")]
        ForeclosureSaleHeld,
        
        [XmlEnum("ForeclosureSalePostponed")]
        ForeclosureSalePostponed,
        
        [XmlEnum("ForeclosureSalePublicationCommenced")]
        ForeclosureSalePublicationCommenced,
        
        [XmlEnum("ForeclosureSaleRescinded")]
        ForeclosureSaleRescinded,
        
        [XmlEnum("ForeclosureSaleScheduled")]
        ForeclosureSaleScheduled,
        
        [XmlEnum("ForeclosureTerminated")]
        ForeclosureTerminated,
        
        [XmlEnum("FormalNoticeFiled")]
        FormalNoticeFiled,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("RedemptionPeriodEnded")]
        RedemptionPeriodEnded,
        
        [XmlEnum("RedemptionPeriodStarted")]
        RedemptionPeriodStarted,
    }
}
