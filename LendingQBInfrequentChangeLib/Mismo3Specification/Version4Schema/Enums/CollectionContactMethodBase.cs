namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum CollectionContactMethodBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Email")]
        Email,
        
        [XmlEnum("FormLetter")]
        FormLetter,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PhoneCall")]
        PhoneCall,
    }
}
