namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum AssetDocumentBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("BankStatement")]
        BankStatement,
        
        [XmlEnum("FinancialStatement")]
        FinancialStatement,
        
        [XmlEnum("InvestmentAccountStatement")]
        InvestmentAccountStatement,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Receipt")]
        Receipt,
        
        [XmlEnum("RelocationBuyoutAgreement")]
        RelocationBuyoutAgreement,
        
        [XmlEnum("RetirementAccountStatement")]
        RetirementAccountStatement,
        
        [XmlEnum("SettlementStatement")]
        SettlementStatement,
        
        [XmlEnum("VerbalStatement")]
        VerbalStatement,
        
        [XmlEnum("VerificationOfDeposit")]
        VerificationOfDeposit,
    }
}
