namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ForeclosureSaleRescindedReasonBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("ExcessivePropertyDamage")]
        ExcessivePropertyDamage,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("ProceduralError")]
        ProceduralError,
    }
}
