namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum SolicitationMethodBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Email")]
        Email,
        
        [XmlEnum("Letter")]
        Letter,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PhoneCall")]
        PhoneCall,
    }
}
