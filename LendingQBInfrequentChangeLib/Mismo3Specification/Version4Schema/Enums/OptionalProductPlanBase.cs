namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum OptionalProductPlanBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AccidentalDeathAndDismembermentInsurance")]
        AccidentalDeathAndDismembermentInsurance,
        
        [XmlEnum("AccidentAndHealthInsurance")]
        AccidentAndHealthInsurance,
        
        [XmlEnum("CreditInsurance")]
        CreditInsurance,
        
        [XmlEnum("DebtCancellationInsurance")]
        DebtCancellationInsurance,
        
        [XmlEnum("DisabilityInsurance")]
        DisabilityInsurance,
        
        [XmlEnum("FloodInsurance")]
        FloodInsurance,
        
        [XmlEnum("HazardInsurance")]
        HazardInsurance,
        
        [XmlEnum("LifeInsurance")]
        LifeInsurance,
        
        [XmlEnum("Other")]
        Other,
    }
}
