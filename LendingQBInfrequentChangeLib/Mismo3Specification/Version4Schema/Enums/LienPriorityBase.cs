namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum LienPriorityBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("FirstLien")]
        FirstLien,
        
        [XmlEnum("FourthLien")]
        FourthLien,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("SecondLien")]
        SecondLien,
        
        [XmlEnum("ThirdLien")]
        ThirdLien,
    }
}
