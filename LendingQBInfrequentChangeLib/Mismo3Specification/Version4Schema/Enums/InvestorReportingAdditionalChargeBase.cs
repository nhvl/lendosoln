namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum InvestorReportingAdditionalChargeBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("FeesCollected")]
        FeesCollected,
        
        [XmlEnum("LatePaymentCharge")]
        LatePaymentCharge,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PrepaymentPenalty")]
        PrepaymentPenalty,
        
        [XmlEnum("RealizedLossDueToForeclosedREOPropertyLiquidation")]
        RealizedLossDueToForeclosedREOPropertyLiquidation,
        
        [XmlEnum("RealizedLossDueToLoanModification")]
        RealizedLossDueToLoanModification,
        
        [XmlEnum("SkipPaymentCharge")]
        SkipPaymentCharge,
    }
}
