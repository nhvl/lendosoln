namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum GovernmentRefinanceBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("CashOutVA")]
        CashOutVA,
        
        [XmlEnum("FullDocumentation")]
        FullDocumentation,
        
        [XmlEnum("InterestRateReductionRefinanceLoan")]
        InterestRateReductionRefinanceLoan,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("StreamlineWithAppraisal")]
        StreamlineWithAppraisal,
        
        [XmlEnum("StreamlineWithoutAppraisal")]
        StreamlineWithoutAppraisal,
    }
}
