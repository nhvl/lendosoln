namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ExpenseClaimSubmissionReasonBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PriorApproval")]
        PriorApproval,
        
        [XmlEnum("Reimbursement")]
        Reimbursement,
    }
}
