namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PaymentPatternRatingCodeBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Equifax")]
        Equifax,
        
        [XmlEnum("Experian")]
        Experian,
        
        [XmlEnum("Other")]
        Other,
    }
}
