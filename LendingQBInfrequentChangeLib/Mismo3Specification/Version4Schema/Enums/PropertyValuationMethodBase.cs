namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PropertyValuationMethodBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AutomatedValuationModel")]
        AutomatedValuationModel,
        
        [XmlEnum("BrokerPriceOpinion")]
        BrokerPriceOpinion,
        
        [XmlEnum("DesktopAppraisal")]
        DesktopAppraisal,
        
        [XmlEnum("DriveBy")]
        DriveBy,
        
        [XmlEnum("FullAppraisal")]
        FullAppraisal,
        
        [XmlEnum("None")]
        None,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PriorAppraisalUsed")]
        PriorAppraisalUsed,
        
        [XmlEnum("TaxValuation")]
        TaxValuation,
    }
}
