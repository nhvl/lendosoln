namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum RecordingJurisdictionBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("County")]
        County,
        
        [XmlEnum("Other")]
        Other,
    }
}
