namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum EscrowHoldbackBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("ConstructionMortgage")]
        ConstructionMortgage,
        
        [XmlEnum("EnergyEfficientMortgage")]
        EnergyEfficientMortgage,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("RehabilitationMortgage")]
        RehabilitationMortgage,
    }
}
