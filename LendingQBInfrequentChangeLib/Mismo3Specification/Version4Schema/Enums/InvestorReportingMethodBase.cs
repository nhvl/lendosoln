namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum InvestorReportingMethodBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("ExceptionReporting")]
        ExceptionReporting,
        
        [XmlEnum("FullReporting")]
        FullReporting,
    }
}
