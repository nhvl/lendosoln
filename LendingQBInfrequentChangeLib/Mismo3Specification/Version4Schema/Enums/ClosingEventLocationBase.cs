namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ClosingEventLocationBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("ClosingAgentBranch")]
        ClosingAgentBranch,
        
        [XmlEnum("LenderBranch")]
        LenderBranch,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("SignerBusiness")]
        SignerBusiness,
        
        [XmlEnum("SignerCurrentResidence")]
        SignerCurrentResidence,
        
        [XmlEnum("SubjectProperty")]
        SubjectProperty,
    }
}
