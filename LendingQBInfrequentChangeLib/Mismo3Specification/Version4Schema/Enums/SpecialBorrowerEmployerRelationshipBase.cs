namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum SpecialBorrowerEmployerRelationshipBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("EmployedByRelative")]
        EmployedByRelative,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PropertySeller")]
        PropertySeller,
        
        [XmlEnum("RealEstateBroker")]
        RealEstateBroker,
    }
}
