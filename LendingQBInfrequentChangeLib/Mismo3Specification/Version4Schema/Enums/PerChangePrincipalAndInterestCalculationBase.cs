namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PerChangePrincipalAndInterestCalculationBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AddDollarAmountToPreviousPrincipalAndInterest")]
        AddDollarAmountToPreviousPrincipalAndInterest,
        
        [XmlEnum("AddPercentBasedOnOriginalPrincipalAndInterest")]
        AddPercentBasedOnOriginalPrincipalAndInterest,
        
        [XmlEnum("AddPercentBasedOnPreviousPrincipalAndInterest")]
        AddPercentBasedOnPreviousPrincipalAndInterest,
        
        [XmlEnum("BasedOnInterestRateUsingAThrowawayRate")]
        BasedOnInterestRateUsingAThrowawayRate,
        
        [XmlEnum("BasedOnNewInterestRate")]
        BasedOnNewInterestRate,
        
        [XmlEnum("FHA245Formula")]
        FHA245Formula,
        
        [XmlEnum("Other")]
        Other,
    }
}
