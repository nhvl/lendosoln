namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum HELOCDailyPeriodicInterestRateCalculationBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("360")]
        Item360,
        
        [XmlEnum("365")]
        Item365,
    }
}
