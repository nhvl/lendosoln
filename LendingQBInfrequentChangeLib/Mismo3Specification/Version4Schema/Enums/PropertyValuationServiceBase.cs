namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PropertyValuationServiceBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("LimitedSummaryReport")]
        LimitedSummaryReport,
        
        [XmlEnum("NonUSPAPAutomatedValuationModel")]
        NonUSPAPAutomatedValuationModel,
        
        [XmlEnum("NonUSPAPPriceOpinion")]
        NonUSPAPPriceOpinion,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("RestrictedUseReport")]
        RestrictedUseReport,
        
        [XmlEnum("SelfContainedReport")]
        SelfContainedReport,
    }
}
