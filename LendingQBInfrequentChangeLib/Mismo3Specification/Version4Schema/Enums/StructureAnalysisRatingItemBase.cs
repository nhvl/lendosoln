namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum StructureAnalysisRatingItemBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AdequacyOfClosetsAndStorage")]
        AdequacyOfClosetsAndStorage,
        
        [XmlEnum("AdequacyOfInsulation")]
        AdequacyOfInsulation,
        
        [XmlEnum("AppealAndMarketability")]
        AppealAndMarketability,
        
        [XmlEnum("Condition")]
        Condition,
        
        [XmlEnum("ConstructionQuality")]
        ConstructionQuality,
        
        [XmlEnum("Electrical")]
        Electrical,
        
        [XmlEnum("ExteriorAppeal")]
        ExteriorAppeal,
        
        [XmlEnum("FunctionalUtility")]
        FunctionalUtility,
        
        [XmlEnum("InteriorAppeal")]
        InteriorAppeal,
        
        [XmlEnum("KitchenEquipment")]
        KitchenEquipment,
        
        [XmlEnum("LocationWithinProjectOrView")]
        LocationWithinProjectOrView,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("OverallLivability")]
        OverallLivability,
        
        [XmlEnum("Plumbing")]
        Plumbing,
        
        [XmlEnum("RoomSizeAndLayout")]
        RoomSizeAndLayout,
        
        [XmlEnum("Skirting")]
        Skirting,
        
        [XmlEnum("TrimAndFinish")]
        TrimAndFinish,
    }
}
