namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum CreditResponseAlertMessageBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("EquifaxSAFESCAN")]
        EquifaxSAFESCAN,
        
        [XmlEnum("EquifaxSSNVerified")]
        EquifaxSSNVerified,
        
        [XmlEnum("ExperianFACSPlus")]
        ExperianFACSPlus,
        
        [XmlEnum("ExperianFraudShield")]
        ExperianFraudShield,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("TransUnionHAWKAlert")]
        TransUnionHAWKAlert,
        
        [XmlEnum("TransUnionHighRiskFraudAlert")]
        TransUnionHighRiskFraudAlert,
        
        [XmlEnum("TransUnionIdentifierMismatchAlert")]
        TransUnionIdentifierMismatchAlert,
        
        [XmlEnum("TransUnionTransAlert")]
        TransUnionTransAlert,
    }
}
