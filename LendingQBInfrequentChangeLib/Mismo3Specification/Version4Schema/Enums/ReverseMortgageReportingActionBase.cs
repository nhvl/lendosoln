namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ReverseMortgageReportingActionBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("CalledDue")]
        CalledDue,
        
        [XmlEnum("DefaultCondition")]
        DefaultCondition,
        
        [XmlEnum("DisbursementOrPaymentResumed")]
        DisbursementOrPaymentResumed,
        
        [XmlEnum("DisbursementOrPaymentSuspended")]
        DisbursementOrPaymentSuspended,
        
        [XmlEnum("ForeclosureLiquidatedHeldForSale")]
        ForeclosureLiquidatedHeldForSale,
        
        [XmlEnum("ForeclosureLiquidatedPendingConveyance")]
        ForeclosureLiquidatedPendingConveyance,
        
        [XmlEnum("ForeclosureLiquidatedThirdPartySale")]
        ForeclosureLiquidatedThirdPartySale,
        
        [XmlEnum("LegalActionInitiated")]
        LegalActionInitiated,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Payoff")]
        Payoff,
        
        [XmlEnum("ReferredForDeedInLieu")]
        ReferredForDeedInLieu,
        
        [XmlEnum("ReferredForForeclosure")]
        ReferredForForeclosure,
        
        [XmlEnum("Repurchase")]
        Repurchase,
        
        [XmlEnum("UnscheduledPaymentMade")]
        UnscheduledPaymentMade,
    }
}
