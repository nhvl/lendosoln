namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum HMDAPostalAddressUnknownBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("CityName")]
        CityName,
        
        [XmlEnum("EntireAddress")]
        EntireAddress,
        
        [XmlEnum("None")]
        None,
        
        [XmlEnum("PostalCode")]
        PostalCode,
        
        [XmlEnum("StreetAddress")]
        StreetAddress,
        
        [XmlEnum("StreetAddressAndCityName")]
        StreetAddressAndCityName,
        
        [XmlEnum("StreetAddressAndPostalCode")]
        StreetAddressAndPostalCode,
    }
}
