namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum MIWorkoutDecisionBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Approved")]
        Approved,
        
        [XmlEnum("BorrowerDeclined")]
        BorrowerDeclined,
        
        [XmlEnum("Denied")]
        Denied,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("UnderReview")]
        UnderReview,
    }
}
