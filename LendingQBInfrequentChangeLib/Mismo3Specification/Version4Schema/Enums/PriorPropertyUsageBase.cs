namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PriorPropertyUsageBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Investment")]
        Investment,
        
        [XmlEnum("PrimaryResidence")]
        PrimaryResidence,
        
        [XmlEnum("SecondHome")]
        SecondHome,
    }
}
