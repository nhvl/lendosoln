namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum InterestRateLifetimeAdjustmentFloorBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AbsoluteMinimumNetInterestRate")]
        AbsoluteMinimumNetInterestRate,
        
        [XmlEnum("FactorAddedToOriginalNoteRate")]
        FactorAddedToOriginalNoteRate,
        
        [XmlEnum("InitialNoteRate")]
        InitialNoteRate,
        
        [XmlEnum("NotApplicable")]
        NotApplicable,
        
        [XmlEnum("Other")]
        Other,
    }
}
