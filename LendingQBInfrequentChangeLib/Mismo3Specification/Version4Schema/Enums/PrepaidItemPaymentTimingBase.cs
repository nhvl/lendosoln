namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PrepaidItemPaymentTimingBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AtClosing")]
        AtClosing,
        
        [XmlEnum("BeforeClosing")]
        BeforeClosing,
        
        [XmlEnum("FirstPayment")]
        FirstPayment,
        
        [XmlEnum("Other")]
        Other,
    }
}
