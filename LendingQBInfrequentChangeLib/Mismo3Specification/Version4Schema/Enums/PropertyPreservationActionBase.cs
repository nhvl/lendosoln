namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PropertyPreservationActionBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Secured")]
        Secured,
        
        [XmlEnum("Unknown")]
        Unknown,
        
        [XmlEnum("Winterized")]
        Winterized,
    }
}
