namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum EmploymentClassificationBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Primary")]
        Primary,
        
        [XmlEnum("Secondary")]
        Secondary,
    }
}
