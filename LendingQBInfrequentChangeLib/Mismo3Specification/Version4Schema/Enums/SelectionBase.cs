namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum SelectionBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Comparison")]
        Comparison,
        
        [XmlEnum("Proposed")]
        Proposed,
        
        [XmlEnum("Requested")]
        Requested,
        
        [XmlEnum("Selected")]
        Selected,
    }
}
