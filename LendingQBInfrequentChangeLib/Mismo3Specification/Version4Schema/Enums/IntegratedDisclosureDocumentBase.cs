namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum IntegratedDisclosureDocumentBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("ClosingDisclosure")]
        ClosingDisclosure,
        
        [XmlEnum("LoanEstimate")]
        LoanEstimate,
        
        [XmlEnum("Other")]
        Other,
    }
}
