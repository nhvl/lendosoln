namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PaymentBillingMethodBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("BillOnReceipt")]
        BillOnReceipt,
        
        [XmlEnum("Coupons")]
        Coupons,
        
        [XmlEnum("NoBilling")]
        NoBilling,
        
        [XmlEnum("Statement")]
        Statement,
        
        [XmlEnum("StatementAndCoupon")]
        StatementAndCoupon,
    }
}
