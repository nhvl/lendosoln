namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum RecordingEndorsementBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AdditionalRecordingJurisdiction")]
        AdditionalRecordingJurisdiction,
        
        [XmlEnum("CorrectiveEndorsement")]
        CorrectiveEndorsement,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PreviouslyRecordedReference")]
        PreviouslyRecordedReference,
        
        [XmlEnum("PrimaryRecordingJurisdiction")]
        PrimaryRecordingJurisdiction,
    }
}
