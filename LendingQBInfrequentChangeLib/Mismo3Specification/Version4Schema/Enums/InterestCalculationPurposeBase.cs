namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum InterestCalculationPurposeBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Draw")]
        Draw,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Payoff")]
        Payoff,
        
        [XmlEnum("ServicerAdvance")]
        ServicerAdvance,
        
        [XmlEnum("Standard")]
        Standard,
    }
}
