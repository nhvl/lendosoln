namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum EscrowPaidByBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Buyer")]
        Buyer,
        
        [XmlEnum("LenderPremium")]
        LenderPremium,
        
        [XmlEnum("Seller")]
        Seller,
        
        [XmlEnum("ThirdParty")]
        ThirdParty,
    }
}
