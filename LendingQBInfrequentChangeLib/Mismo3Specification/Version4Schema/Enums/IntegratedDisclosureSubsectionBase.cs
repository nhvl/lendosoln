namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum IntegratedDisclosureSubsectionBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Adjustments")]
        Adjustments,
        
        [XmlEnum("AdjustmentsForItemsPaidBySellerInAdvance")]
        AdjustmentsForItemsPaidBySellerInAdvance,
        
        [XmlEnum("AdjustmentsForItemsUnpaidBySeller")]
        AdjustmentsForItemsUnpaidBySeller,
        
        [XmlEnum("ClosingCostsSubtotal")]
        ClosingCostsSubtotal,
        
        [XmlEnum("LenderCredits")]
        LenderCredits,
        
        [XmlEnum("LoanCostsSubtotal")]
        LoanCostsSubtotal,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("OtherCostsSubtotal")]
        OtherCostsSubtotal,
        
        [XmlEnum("OtherCredits")]
        OtherCredits,
        
        [XmlEnum("TotalClosingCostsSellerOnly")]
        TotalClosingCostsSellerOnly,
    }
}
