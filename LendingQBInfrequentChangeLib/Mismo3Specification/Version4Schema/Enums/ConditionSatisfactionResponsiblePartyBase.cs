namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ConditionSatisfactionResponsiblePartyBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Attorney")]
        Attorney,
        
        [XmlEnum("Broker")]
        Broker,
        
        [XmlEnum("Closer")]
        Closer,
        
        [XmlEnum("ClosingAgent")]
        ClosingAgent,
        
        [XmlEnum("EscrowCompany")]
        EscrowCompany,
        
        [XmlEnum("LoanOfficer")]
        LoanOfficer,
        
        [XmlEnum("Originator")]
        Originator,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Processor")]
        Processor,
        
        [XmlEnum("SettlementAgent")]
        SettlementAgent,
        
        [XmlEnum("TitleCompany")]
        TitleCompany,
    }
}
