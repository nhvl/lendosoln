namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum InterestCalculationBasisBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AverageBalance")]
        AverageBalance,
        
        [XmlEnum("DailyLoanBalance")]
        DailyLoanBalance,
        
        [XmlEnum("EndOfPeriod")]
        EndOfPeriod,
        
        [XmlEnum("MaximumBalance")]
        MaximumBalance,
        
        [XmlEnum("Other")]
        Other,
    }
}
