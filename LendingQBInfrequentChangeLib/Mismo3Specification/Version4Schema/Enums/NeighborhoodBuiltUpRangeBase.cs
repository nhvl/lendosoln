namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum NeighborhoodBuiltUpRangeBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Over75Percent")]
        Over75Percent,
        
        [XmlEnum("TwentyFiveToSeventyFivePercent")]
        TwentyFiveToSeventyFivePercent,
        
        [XmlEnum("Under25Percent")]
        Under25Percent,
    }
}
