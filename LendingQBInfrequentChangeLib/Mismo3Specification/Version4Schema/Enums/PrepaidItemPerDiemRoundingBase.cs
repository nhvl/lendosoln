namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PrepaidItemPerDiemRoundingBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Down")]
        Down,
        
        [XmlEnum("None")]
        None,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Up")]
        Up,
    }
}
