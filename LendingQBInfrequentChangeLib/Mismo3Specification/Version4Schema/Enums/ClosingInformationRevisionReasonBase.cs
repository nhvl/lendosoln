namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ClosingInformationRevisionReasonBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("ChangedBorrowerInformation")]
        ChangedBorrowerInformation,
        
        [XmlEnum("ChangedFeeAmount")]
        ChangedFeeAmount,
        
        [XmlEnum("ChangedItemPlacement")]
        ChangedItemPlacement,
        
        [XmlEnum("Other")]
        Other,
    }
}
