namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum CarStorageAttachmentBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Attached")]
        Attached,
        
        [XmlEnum("BuiltIn")]
        BuiltIn,
        
        [XmlEnum("Detached")]
        Detached,
    }
}
