namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum BuydownFundingBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Financed")]
        Financed,
        
        [XmlEnum("IncreaseInInterestRate")]
        IncreaseInInterestRate,
        
        [XmlEnum("LumpSumCash")]
        LumpSumCash,
        
        [XmlEnum("Other")]
        Other,
    }
}
