namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum HMDAEthnicityBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("HispanicOrLatino")]
        HispanicOrLatino,
        
        [XmlEnum("InformationNotProvidedByApplicantInMailInternetOrTelephoneApplication")]
        InformationNotProvidedByApplicantInMailInternetOrTelephoneApplication,
        
        [XmlEnum("NotApplicable")]
        NotApplicable,
        
        [XmlEnum("NotHispanicOrLatino")]
        NotHispanicOrLatino,
    }
}
