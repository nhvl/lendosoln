namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum EncumbranceBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("DeedRestriction")]
        DeedRestriction,
        
        [XmlEnum("FirstMortgageLien")]
        FirstMortgageLien,
        
        [XmlEnum("GroundLease")]
        GroundLease,
        
        [XmlEnum("MechanicsLien")]
        MechanicsLien,
        
        [XmlEnum("None")]
        None,
        
        [XmlEnum("OccupancyRestriction")]
        OccupancyRestriction,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("ResaleRestriction")]
        ResaleRestriction,
        
        [XmlEnum("SecondMortgageLien")]
        SecondMortgageLien,
        
        [XmlEnum("TaxLien")]
        TaxLien,
        
        [XmlEnum("TransferFee")]
        TransferFee,
    }
}
