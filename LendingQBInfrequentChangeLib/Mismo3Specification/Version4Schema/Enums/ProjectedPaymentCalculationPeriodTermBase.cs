namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ProjectedPaymentCalculationPeriodTermBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("FinalPayment")]
        FinalPayment,
        
        [XmlEnum("Monthly")]
        Monthly,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Yearly")]
        Yearly,
    }
}
