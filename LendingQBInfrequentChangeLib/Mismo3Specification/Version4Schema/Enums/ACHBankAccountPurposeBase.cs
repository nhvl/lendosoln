namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ACHBankAccountPurposeBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PrincipalAndInterest")]
        PrincipalAndInterest,
        
        [XmlEnum("Settlement")]
        Settlement,
        
        [XmlEnum("TaxesAndInsurance")]
        TaxesAndInsurance,
    }
}
