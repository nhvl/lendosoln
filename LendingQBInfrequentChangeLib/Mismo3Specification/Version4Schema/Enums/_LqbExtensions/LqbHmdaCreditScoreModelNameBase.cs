﻿namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates a HMDA-recognized credit scoring model.
    /// </summary>
    public enum LqbHmdaCreditScoreModelNameBase
    {
        /// <summary>
        /// Indicates no model data is saved.
        /// </summary>
        [XmlEnum("")]
        LeaveBlank,

        /// <summary>
        /// Indicates the Equifax Beacon 5.0 model.
        /// </summary>
        [XmlEnum("EquifaxBeacon5")]
        EquifaxBeacon5,

        /// <summary>
        /// Indicates the Experian Fair Isaac model.
        /// </summary>
        [XmlEnum("ExperianFairIsaac")]
        ExperianFairIsaac,

        /// <summary>
        /// Indicates the FICO Risk Score Classic '04 model.
        /// </summary>
        [XmlEnum("FicoRiskScoreClassic04")]
        FicoRiskScoreClassic04,

        /// <summary>
        /// Indicates the FICO Risk Score Classic '98 model.
        /// </summary>
        [XmlEnum("FicoRiskScoreClassic98")]
        FicoRiskScoreClassic98,

        /// <summary>
        /// Indicates the VantageScore 2.0 model.
        /// </summary>
        [XmlEnum("VantageScore2")]
        VantageScore2,

        /// <summary>
        /// Indicates the VantageScore 3.0 model.
        /// </summary>
        [XmlEnum("VantageScore3")]
        VantageScore3,

        /// <summary>
        /// Indicates multiple credit scoring models.
        /// </summary>
        [XmlEnum("MoreThanOneScoringModel")]
        MoreThanOneScoringModel,

        /// <summary>
        /// Indicates a credit scoring model that is not otherwise captured.
        /// </summary>
        [XmlEnum("Other")]
        Other,

        /// <summary>
        /// Indicates that a credit scoring model is not applicable.
        /// </summary>
        [XmlEnum("NotApplicable")]
        NotApplicable,

        /// <summary>
        /// Indicates a co-applicant datapoint when there is no co-applicant on the file.
        /// </summary>
        [XmlEnum("NoCoApplicant")]
        NoCoApplicant
    }
}
