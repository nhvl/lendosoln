﻿namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates the owner of the lot for a construction loan.
    /// </summary>
    public enum LqbLotOwnerBase
    {
        /// <summary>
        /// No value.
        /// </summary>
        [XmlEnum("")]
        Blank = 0,

        /// <summary>
        /// The lot is owned by the borrower.
        /// </summary>
        [XmlEnum("Borrower")]
        Borrower = 1,

        /// <summary>
        /// The lot is owned by the builder.
        /// </summary>
        [XmlEnum("Builder")]
        Builder = 2,

        /// <summary>
        /// Another entity owns the lot.
        /// </summary>
        [XmlEnum("Other")]
        Other = 3
    }
}