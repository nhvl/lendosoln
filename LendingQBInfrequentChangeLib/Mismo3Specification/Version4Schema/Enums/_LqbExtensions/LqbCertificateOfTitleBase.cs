namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum LqbCertificateOfTitleBase
    {
        /// <summary>
        /// A catchall value to account for a blank field in LQB.
        /// </summary>
        [XmlEnum("")]
        Blank = 0,

        [XmlEnum("HomeShallBeCoveredByCertificateOfTitle")]
        HomeShallBeCoveredByCertificateOfTitle = 1,

        [XmlEnum("TitleCertificateShallBeEliminated")]
        TitleCertificateShallBeEliminated = 2,

        [XmlEnum("TitleCertificateHasBeenEliminated")]
        TitleCertificateHasBeenEliminated = 3,

        [XmlEnum("ManufacturersCertificateShallBeEliminated")]
        ManufacturersCertificateShallBeEliminated = 4,

        [XmlEnum("ManufacturersCertificateHasBeenEliminated")]
        ManufacturersCertificateHasBeenEliminated = 5,

        [XmlEnum("HomeIsNotCoveredOrCertIsAttached")]
        HomeIsNotCoveredOrCertIsAttached = 6,

        [XmlEnum("HomeIsNotCoveredOrNotAbleToProduceCert")]
        HomeIsNotCoveredOrNotAbleToProduceCert = 7,
    }
}
