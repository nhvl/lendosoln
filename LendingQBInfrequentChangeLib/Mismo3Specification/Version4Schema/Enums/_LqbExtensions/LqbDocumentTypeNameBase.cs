﻿namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// LQB Document types.
    /// </summary>
    public enum LqbDocumentTypeNameBase
    {
        [XmlEnum("")]
        Blank,

        [XmlEnum("Full")]
        Full,

        [XmlEnum("Alt")]
        Alt,

        [XmlEnum("Light")]
        Light,

        [XmlEnum("SIVA")]
        SIVA,

        [XmlEnum("VISA")]
        VISA,

        [XmlEnum("SISA")]
        SISA,

        [XmlEnum("NIVA")]
        NIVA,

        [XmlEnum("NINA")]
        NINA,

        [XmlEnum("NISA")]
        NISA,

        [XmlEnum("NINANE")]
        NINANE,

        [XmlEnum("NIVANE")]
        NIVANE,

        [XmlEnum("VINA")]
        VINA,

        [XmlEnum("Streamline")]
        Streamline,

        [XmlEnum("_12MoPersonalBankStatements")]
        _12MoPersonalBankStatements,

        [XmlEnum("_24MoPersonalBankStatements")]
        _24MoPersonalBankStatements,

        [XmlEnum("_12MoBusinessBankStatements")]
        _12MoBusinessBankStatements,

        [XmlEnum("_24MoBusinessBankStatements")]
        _24MoBusinessBankStatements,

        [XmlEnum("OtherBankStatements")]
        OtherBankStatements,

        [XmlEnum("_1YrTaxReturns")]
        _1YrTaxReturns,

        [XmlEnum("AssetUtilization")]
        AssetUtilization,

        [XmlEnum("DebtServiceCoverage")]
        DebtServiceCoverage,

        [XmlEnum("NoIncome")]
        NoIncome,

        [XmlEnum("Voe")]
        Voe,
    }
}
