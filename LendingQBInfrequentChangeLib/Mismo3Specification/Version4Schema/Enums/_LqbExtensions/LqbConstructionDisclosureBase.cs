﻿namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the construction and permanent financing will be disclosed separately or together.
    /// </summary>
    public enum LqbConstructionDisclosureBase
    {
        /// <summary>
        /// No value.
        /// </summary>
        [XmlEnum("")]
        Blank = 0,

        /// <summary>
        /// The construction and permament financing will be disclosed together.
        /// </summary>
        [XmlEnum("Combined")]
        Combined = 1,

        /// <summary>
        /// The construction and permanent financing will be disclosed separately.
        /// </summary>
        [XmlEnum("Separate")]
        Separate = 2
    }
}