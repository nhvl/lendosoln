﻿namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether a piece of HMDA data was collected based on
    /// visual observation of the applicant or derived from their surname.
    /// </summary>
    public enum LqbHmdaCollectedBasedOnVisualObservationOrSurnameBase
    {
        /// <summary>
        /// Indicates that data was collected based on visual observation
        /// of the applicant or derived from their surname.
        /// </summary>
        [XmlEnum("CollectedBasedOnVisualObservationOrSurname")]
        CollectedBasedOnVisualObservationOrSurname,

        /// <summary>
        /// Indicates that data was not collected based on visual observation
        /// of the applicant or derived from their surname.
        /// </summary>
        [XmlEnum("NotCollectedBasedOnVisualObservationOrSurname")]
        NotCollectedBasedOnVisualObservationOrSurname,

        /// <summary>
        /// Indicates that the value is not applicable to the application.
        /// </summary>
        [XmlEnum("NotApplicable")]
        NotApplicable,

        /// <summary>
        /// Indicates a co-applicant datapoint when there is no co-applicant on the file.
        /// </summary>
        [XmlEnum("NoCoApplicant")]
        NoCoApplicant,

        /// <summary>
        /// A catchall value to account for a blank field in LQB.
        /// </summary>
        [XmlEnum("")]
        Blank

    }
}
