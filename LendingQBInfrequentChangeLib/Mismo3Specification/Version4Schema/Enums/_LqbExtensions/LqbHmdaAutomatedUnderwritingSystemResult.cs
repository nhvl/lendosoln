﻿namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates the AUS result determined for a loan.
    /// </summary>
    public enum LqbHmdaAutomatedUnderwritingSystemResult
    {
        /// <summary>
        /// A result of Approve/Eligible.
        /// </summary>
        [XmlEnum("ApproveEligible")]
        ApproveEligible,

        /// <summary>
        /// A result of Approve/Ineligible.
        /// </summary>
        [XmlEnum("ApproveIneligible")]
        ApproveIneligible,

        /// <summary>
        /// A result of Refer/Eligible.
        /// </summary>
        [XmlEnum("ReferEligible")]
        ReferEligible,

        /// <summary>
        /// A result of Refer/Ineligible.
        /// </summary>
        [XmlEnum("ReferIneligible")]
        ReferIneligible,

        /// <summary>
        /// A result of Refer/Caution.
        /// </summary>
        [XmlEnum("Refer/Caution")]
        ReferCaution,

        /// <summary>
        /// The specifics of the loan are out of the AUS's scope.
        /// </summary>
        [XmlEnum("OutOfScope")]
        OutOfScope,

        /// <summary>
        /// The AUS ran into an error.
        /// </summary>
        [XmlEnum("Error")]
        Error,

        /// <summary>
        /// A result of Accept.
        /// </summary>
        [XmlEnum("Accept")]
        Accept,

        /// <summary>
        /// A result of Caution.
        /// </summary>
        [XmlEnum("Caution")]
        Caution,

        /// <summary>
        /// A result of Ineligible.
        /// </summary>
        [XmlEnum("Ineligible")]
        Ineligible,

        /// <summary>
        /// A result of Incomplete.
        /// </summary>
        [XmlEnum("Incomplete")]
        Incomplete,

        /// <summary>
        /// A result of Invalid.
        /// </summary>
        [XmlEnum("Invalid")]
        Invalid,

        /// <summary>
        /// A result of Refer.
        /// </summary>
        [XmlEnum("Refer")]
        Refer,

        /// <summary>
        /// A result of Eligible.
        /// </summary>
        [XmlEnum("Eligible")]
        Eligible,

        /// <summary>
        /// The AUS was not able to determine a result.
        /// </summary>
        [XmlEnum("UnableToDetermine")]
        UnableToDetermine,

        /// <summary>
        /// The AUS returned another result not captured here.
        /// </summary>
        [XmlEnum("Other")]
        Other,

        /// <summary>
        /// The use of an AUS was not applicable to this loan.
        /// </summary>
        [XmlEnum("NotApplicable")]
        NotApplicable,

        /// <summary>
        /// A catchall value to account for a blank field in LQB.
        /// </summary>
        [XmlEnum("")]
        Blank
    }
}
