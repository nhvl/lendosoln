namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum CashToCloseCalculationMethodBase
    {
        /// <summary>
        /// A catchall value to account for a blank field in LQB.
        /// </summary>
        [XmlEnum("")]
        Blank,

        [XmlEnum("Standard")]
        Standard,

        [XmlEnum("Alternative")]
        Alternative
    }
}
