﻿namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates the interest accrual type for a construction loan in the construction phase.
    /// </summary>
    public enum LqbConstructionPhaseInterestAccrualBase
    {
        /// <summary>
        /// No value.
        /// </summary>
        [XmlEnum("")]
        Blank,

        /// <summary>
        /// Monthly interest accrual where a 360-day year is used for calculations.
        /// </summary>
        [XmlEnum("Monthly 360/360")]
        Monthly360_360,

        /// <summary>
        /// Daily interest accrual where the daily interest is calculated using a 360-day year,
        /// but is actually charged using a 365-day year.
        /// </summary>
        [XmlEnum("Daily 365/360")]
        Daily365_360,

        /// <summary>
        /// Daily interest accrual where a 365-day year is used for calculations.
        /// </summary>
        [XmlEnum("Daily 365/365")]
        Daily365_365
    }
}