﻿namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates the purpose of a construction loan.
    /// </summary>
    public enum LqbConstructionPurposeBase
    {
        /// <summary>
        /// No value.
        /// </summary>
        [XmlEnum("")]
        Blank,

        /// <summary>
        /// Construction only.
        /// </summary>
        [XmlEnum("Construction")]
        Construction,

        /// <summary>
        /// Construction and purchasing the lot.
        /// </summary>
        [XmlEnum("ConstructionAndLotPurchase")]
        ConstructionAndLotPurchase,

        /// <summary>
        /// Construction and refinancing the lot.
        /// </summary>
        [XmlEnum("ConstructionAndLotRefinance")]
        ConstructionAndLotRefinance
    }
}