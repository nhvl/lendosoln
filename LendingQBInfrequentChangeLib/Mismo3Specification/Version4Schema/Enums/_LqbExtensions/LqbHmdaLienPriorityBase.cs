﻿namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates the lien priority for the loan.
    /// </summary>
    public enum LqbLienPriorityBase
    {
        /// <summary>
        /// The loan is secured by a first lien.
        /// </summary>
        [XmlEnum("FirstLien")]
        FirstLien,

        /// <summary>
        /// The loan is secured by a subordinate lien.
        /// </summary>
        [XmlEnum("SubordinateLien")]
        SubordinateLien,

        /// <summary>
        /// The loan is not secured by a lien.
        /// </summary>
        [XmlEnum("NotSecuredByLien")]
        NotSecuredByLien,

        /// <summary>
        /// Lien status is not applicable.
        /// </summary>
        [XmlEnum("NotApplicable")]
        NotApplicable,

        /// <summary>
        /// A catchall value to account for a blank field in LQB.
        /// </summary>
        [XmlEnum("")]
        Blank
    }
}
