﻿namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Contains all values from MISMO 3.4 HMDARaceBase and HMDARaceDesignationBase
    /// to support the LendingQB extension datapoints containing LAR Race data.
    /// </summary>
    public enum LqbHmdaRaceAndDesignationBase
    {
        /// <summary>
        /// Indicates Native American race.
        /// </summary>
        [XmlEnum("AmericanIndianOrAlaskaNative")]
        AmericanIndianOrAlaskaNative,

        /// <summary>
        /// Indicates Asian race.
        /// </summary>
        [XmlEnum("Asian")]
        Asian,

        /// <summary>
        /// Indicates Black or African American race.
        /// </summary>
        [XmlEnum("BlackOrAfricanAmerican")]
        BlackOrAfricanAmerican,

        /// <summary>
        /// Indicates race information was not provided.
        /// </summary>
        [XmlEnum("InformationNotProvidedByApplicantInMailInternetOrTelephoneApplication")]
        InformationNotProvidedByApplicantInMailInternetOrTelephoneApplication,

        /// <summary>
        /// Indicates Pacific Islander race.
        /// </summary>
        [XmlEnum("NativeHawaiianOrOtherPacificIslander")]
        NativeHawaiianOrOtherPacificIslander,

        /// <summary>
        /// Indicates race information is not applicable.
        /// </summary>
        [XmlEnum("NotApplicable")]
        NotApplicable,

        /// <summary>
        /// Indicates White race.
        /// </summary>
        [XmlEnum("White")]
        White,

        /// <summary>
        /// Indicates an Asian Indian race designation.
        /// </summary>
        [XmlEnum("AsianIndian")]
        AsianIndian,

        /// <summary>
        /// Indicates a Chinese race designation.
        /// </summary>
        [XmlEnum("Chinese")]
        Chinese,

        /// <summary>
        /// Indicates a Filipino race designation.
        /// </summary>
        [XmlEnum("Filipino")]
        Filipino,

        /// <summary>
        /// Indicates a Guamanian or Chamorro race designation.
        /// </summary>
        [XmlEnum("GuamanianOrChamorro")]
        GuamanianOrChamorro,

        /// <summary>
        /// Indicates a Japanese race designation.
        /// </summary>
        [XmlEnum("Japanese")]
        Japanese,

        /// <summary>
        /// Indicates a Korean race designation.
        /// </summary>
        [XmlEnum("Korean")]
        Korean,

        /// <summary>
        /// Indicates a Native Hawaiian race designation.
        /// </summary>
        [XmlEnum("NativeHawaiian")]
        NativeHawaiian,

        /// <summary>
        /// Indicates another race designation.
        /// </summary>
        [XmlEnum("Other")]
        Other,

        /// <summary>
        /// Indicates a Samoan race designation.
        /// </summary>
        [XmlEnum("Samoan")]
        Samoan,

        /// <summary>
        /// Indicates a Vietnamese race designation.
        /// </summary>
        [XmlEnum("Vietnamese")]
        Vietnamese,

        /// <summary>
        /// Indicates a co-applicant datapoint when there is no co-applicant on the file.
        /// </summary>
        [XmlEnum("NoCoApplicant")]
        NoCoApplicant,

        /// <summary>
        /// Indicates that an "other" value for an Asian race is specified.
        /// </summary>
        [XmlEnum("OtherAsian")]
        OtherAsian,

        /// <summary>
        /// Indicates that an "other" value for a Pacific Islander race is specified.
        /// </summary>
        [XmlEnum("OtherPacificIslander")]
        OtherPacificIslander,
        
        /// <summary>
        /// A catchall value to account for a blank field in LQB.
        /// </summary>
        [XmlEnum("")]
        Blank
    }
}
