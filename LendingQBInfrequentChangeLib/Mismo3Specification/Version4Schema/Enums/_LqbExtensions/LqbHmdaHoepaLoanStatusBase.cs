﻿namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates the HOEPA status of the loan.
    /// </summary>
    public enum LqbHmdaHoepaLoanStatusBase
    {
        /// <summary>
        /// Indicates the loan is high cost.
        /// </summary>
        [XmlEnum("HighCost")]
        HighCost,

        /// <summary>
        /// Indicates the loan is not high cost.
        /// </summary>
        [XmlEnum("NotHighCost")]
        NotHighCost,

        /// <summary>
        /// Indicates the HOEPA status is not applicable to the loan.
        /// </summary>
        [XmlEnum("NotApplicable")]
        NotApplicable,

        /// <summary>
        /// A catchall value to account for a blank field in LQB.
        /// </summary>
        [XmlEnum("")]
        Blank
    }
}
