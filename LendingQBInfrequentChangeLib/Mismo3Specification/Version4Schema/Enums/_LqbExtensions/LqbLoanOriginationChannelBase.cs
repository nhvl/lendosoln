namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates the loan origination channel.
    /// </summary>
    public enum LqbLoanOriginationChannelBase
    {
        /// <summary>
        /// A catchall value to account for a blank field in LQB.
        /// </summary>
        [XmlEnum("")]
        Blank,

        [XmlEnum("BrokeredOut")]
        BrokeredOut,

        [XmlEnum("Correspondent")]
        Correspondent,

        [XmlEnum("Internet")]
        Internet,

        [XmlEnum("Purchased")]
        Purchased,

        [XmlEnum("Retail")]
        Retail,

        [XmlEnum("Servicing")]
        Servicing,

        [XmlEnum("Wholesale")]
        Wholesale
    }
}
