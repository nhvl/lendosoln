namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ProjectAnalysisAdequacyOfManagementBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Adequate")]
        Adequate,
        
        [XmlEnum("Inadequate")]
        Inadequate,
        
        [XmlEnum("Unknown")]
        Unknown,
    }
}
