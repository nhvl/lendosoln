namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum CostAnalysisBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Replacement")]
        Replacement,
        
        [XmlEnum("Reproduction")]
        Reproduction,
    }
}
