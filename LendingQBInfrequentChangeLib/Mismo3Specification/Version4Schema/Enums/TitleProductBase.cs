namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum TitleProductBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("ClosingProtectionLetter")]
        ClosingProtectionLetter,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("TitleCommitment")]
        TitleCommitment,
        
        [XmlEnum("TitlePolicy")]
        TitlePolicy,
    }
}
