namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum SCRAReliefStatusBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("NeverReceived")]
        NeverReceived,
        
        [XmlEnum("OneOrMoreBorrowersCurrentlyReceive")]
        OneOrMoreBorrowersCurrentlyReceive,
        
        [XmlEnum("OneOrMoreBorrowersPreviouslyReceived")]
        OneOrMoreBorrowersPreviouslyReceived,
        
        [XmlEnum("Other")]
        Other,
    }
}
