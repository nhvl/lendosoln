namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum IdentityDocumentBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("BankStatement")]
        BankStatement,
        
        [XmlEnum("DriversLicense")]
        DriversLicense,
        
        [XmlEnum("EmployeeIdentification")]
        EmployeeIdentification,
        
        [XmlEnum("MilitaryIdentification")]
        MilitaryIdentification,
        
        [XmlEnum("NationalIdentification")]
        NationalIdentification,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Passport")]
        Passport,
        
        [XmlEnum("PrivateIdentification")]
        PrivateIdentification,
        
        [XmlEnum("StateIdentification")]
        StateIdentification,
        
        [XmlEnum("TaxpayerIdentification")]
        TaxpayerIdentification,
        
        [XmlEnum("Visa")]
        Visa,
    }
}
