namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum RealEstateAgentBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Listing")]
        Listing,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Selling")]
        Selling,
    }
}
