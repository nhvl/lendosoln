namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum BankruptcyStatusBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Active")]
        Active,
        
        [XmlEnum("Closed")]
        Closed,
        
        [XmlEnum("Other")]
        Other,
    }
}
