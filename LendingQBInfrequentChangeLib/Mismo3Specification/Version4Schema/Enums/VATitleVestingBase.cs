namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum VATitleVestingBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("JointTwoOrMoreVeterans")]
        JointTwoOrMoreVeterans,
        
        [XmlEnum("JointVeteranAndNonVeteran")]
        JointVeteranAndNonVeteran,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Veteran")]
        Veteran,
        
        [XmlEnum("VeteranAndSpouse")]
        VeteranAndSpouse,
    }
}
