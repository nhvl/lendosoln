namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum CompensationPaidToBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Borrower")]
        Borrower,
        
        [XmlEnum("Broker")]
        Broker,
        
        [XmlEnum("Lender")]
        Lender,
        
        [XmlEnum("LoanOriginator")]
        LoanOriginator,
        
        [XmlEnum("Other")]
        Other,
    }
}
