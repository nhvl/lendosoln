namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum OfferingHistoryBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Cancelled")]
        Cancelled,
        
        [XmlEnum("ContingentSale")]
        ContingentSale,
        
        [XmlEnum("ContractOffMarket")]
        ContractOffMarket,
        
        [XmlEnum("ContractOnMarket")]
        ContractOnMarket,
        
        [XmlEnum("OfferPending")]
        OfferPending,
        
        [XmlEnum("OnHold")]
        OnHold,
        
        [XmlEnum("OriginalOffering")]
        OriginalOffering,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PriceChange")]
        PriceChange,
        
        [XmlEnum("Sold")]
        Sold,
        
        [XmlEnum("TemporaryHold")]
        TemporaryHold,
    }
}
