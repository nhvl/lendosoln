namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum NamedInsuredBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Lender")]
        Lender,
        
        [XmlEnum("Owner")]
        Owner,
    }
}
