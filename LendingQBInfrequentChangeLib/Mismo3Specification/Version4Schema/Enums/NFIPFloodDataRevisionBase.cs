namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum NFIPFloodDataRevisionBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("LOMA")]
        LOMA,
        
        [XmlEnum("LOMR")]
        LOMR,
    }
}
