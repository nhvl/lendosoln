namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum LoanOriginatorBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Broker")]
        Broker,
        
        [XmlEnum("Correspondent")]
        Correspondent,
        
        [XmlEnum("Lender")]
        Lender,
        
        [XmlEnum("Other")]
        Other,
    }
}
