namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PRIARequestBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("FeeQuote")]
        FeeQuote,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("RecordCorrectedDocuments")]
        RecordCorrectedDocuments,
        
        [XmlEnum("RecordDocuments")]
        RecordDocuments,
        
        [XmlEnum("StatusUpdate")]
        StatusUpdate,
    }
}
