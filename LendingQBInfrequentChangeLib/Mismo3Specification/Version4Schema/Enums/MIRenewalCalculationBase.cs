namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum MIRenewalCalculationBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AverageAnnualScheduledUnpaidPrincipalBalance")]
        AverageAnnualScheduledUnpaidPrincipalBalance,
        
        [XmlEnum("Constant")]
        Constant,
        
        [XmlEnum("Declining")]
        Declining,
        
        [XmlEnum("NoRenewals")]
        NoRenewals,
        
        [XmlEnum("Other")]
        Other,
    }
}
