namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum EscrowMonthlyPaymentRoundingBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Down")]
        Down,
        
        [XmlEnum("None")]
        None,
        
        [XmlEnum("Up")]
        Up,
    }
}
