namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum RecordingTransactionIdentifierBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("CourtCase")]
        CourtCase,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Recorder")]
        Recorder,
        
        [XmlEnum("TitleEscrow")]
        TitleEscrow,
    }
}
