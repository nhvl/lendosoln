namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum IssueResolutionBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("ConfidentialProprietaryOrPrivileged")]
        ConfidentialProprietaryOrPrivileged,
        
        [XmlEnum("Duplicative")]
        Duplicative,
        
        [XmlEnum("ErrorCorrectionMade")]
        ErrorCorrectionMade,
        
        [XmlEnum("Irrelevant")]
        Irrelevant,
        
        [XmlEnum("NoErrorOccurred")]
        NoErrorOccurred,
        
        [XmlEnum("NotServicedLoan")]
        NotServicedLoan,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Overbroad")]
        Overbroad,
        
        [XmlEnum("ReferredToRegulatoryAgency")]
        ReferredToRegulatoryAgency,
        
        [XmlEnum("RequestedInformationProvided")]
        RequestedInformationProvided,
        
        [XmlEnum("UnauthorizedParty")]
        UnauthorizedParty,
        
        [XmlEnum("Untimely")]
        Untimely,
    }
}
