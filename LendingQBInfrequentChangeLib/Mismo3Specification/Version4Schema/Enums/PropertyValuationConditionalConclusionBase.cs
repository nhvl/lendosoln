namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PropertyValuationConditionalConclusionBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AsIs")]
        AsIs,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("SubjectToCompletionPerPlans")]
        SubjectToCompletionPerPlans,
        
        [XmlEnum("SubjectToRepairsAndConditions")]
        SubjectToRepairsAndConditions,
        
        [XmlEnum("SubjectToRepairsProvingUnnecessary")]
        SubjectToRepairsProvingUnnecessary,
    }
}
