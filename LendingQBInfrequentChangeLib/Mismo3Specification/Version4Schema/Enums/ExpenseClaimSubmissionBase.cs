namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ExpenseClaimSubmissionBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Final")]
        Final,
        
        [XmlEnum("Initial")]
        Initial,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Supplemental")]
        Supplemental,
    }
}
