namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum InterestCalculationBasisDaysInYearCountBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("360")]
        Item360,
        
        [XmlEnum("365")]
        Item365,
        
        [XmlEnum("36525")]
        Item36525,
        
        [XmlEnum("365Or366")]
        Item365Or366,
        
        [XmlEnum("366")]
        Item366,
    }
}
