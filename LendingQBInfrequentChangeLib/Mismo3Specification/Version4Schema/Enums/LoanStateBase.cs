namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum LoanStateBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AtBankruptcyFiling")]
        AtBankruptcyFiling,
        
        [XmlEnum("AtClosing")]
        AtClosing,
        
        [XmlEnum("AtConversion")]
        AtConversion,
        
        [XmlEnum("AtEstimate")]
        AtEstimate,
        
        [XmlEnum("AtModification")]
        AtModification,
        
        [XmlEnum("AtRelief")]
        AtRelief,
        
        [XmlEnum("AtReset")]
        AtReset,
        
        [XmlEnum("AtTransfer")]
        AtTransfer,
        
        [XmlEnum("AtTrial")]
        AtTrial,
        
        [XmlEnum("AtUnderwriting")]
        AtUnderwriting,
        
        [XmlEnum("Current")]
        Current,
    }
}
