namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum FoundationBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Basement")]
        Basement,
        
        [XmlEnum("Crawlspace")]
        Crawlspace,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PartialBasement")]
        PartialBasement,
        
        [XmlEnum("Slab")]
        Slab,
    }
}
