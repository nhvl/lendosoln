namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum GFEAggregationBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("ChosenInterestRateCreditOrCharge")]
        ChosenInterestRateCreditOrCharge,
        
        [XmlEnum("CombinedOurOriginationAndInterestRateCreditOrCharge")]
        CombinedOurOriginationAndInterestRateCreditOrCharge,
        
        [XmlEnum("GovernmentRecordingCharges")]
        GovernmentRecordingCharges,
        
        [XmlEnum("HomeownersInsurance")]
        HomeownersInsurance,
        
        [XmlEnum("None")]
        None,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("OurOriginationCharge")]
        OurOriginationCharge,
        
        [XmlEnum("OwnersTitleInsurance")]
        OwnersTitleInsurance,
        
        [XmlEnum("RequiredServicesLenderSelected")]
        RequiredServicesLenderSelected,
        
        [XmlEnum("RequiredServicesYouCanShopFor")]
        RequiredServicesYouCanShopFor,
        
        [XmlEnum("TitleServices")]
        TitleServices,
        
        [XmlEnum("TransferTaxes")]
        TransferTaxes,
    }
}
