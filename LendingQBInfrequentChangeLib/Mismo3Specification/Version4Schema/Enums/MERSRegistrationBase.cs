namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum MERSRegistrationBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("MERS_ERegistry")]
        MERS_ERegistry,
        
        [XmlEnum("MERSSystem")]
        MERSSystem,
        
        [XmlEnum("Other")]
        Other,
    }
}
