namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum TransferOfServicingDisclosureBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Investor")]
        Investor,
        
        [XmlEnum("Lender")]
        Lender,
        
        [XmlEnum("New")]
        New,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Present")]
        Present,
    }
}
