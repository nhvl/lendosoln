namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ProrationItemBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("BoroughPropertyTax")]
        BoroughPropertyTax,
        
        [XmlEnum("CityPropertyTax")]
        CityPropertyTax,
        
        [XmlEnum("CondominiumAssociationDues")]
        CondominiumAssociationDues,
        
        [XmlEnum("CondominiumAssociationSpecialAssessment")]
        CondominiumAssociationSpecialAssessment,
        
        [XmlEnum("CooperativeAssociationDues")]
        CooperativeAssociationDues,
        
        [XmlEnum("CooperativeAssociationSpecialAssessment")]
        CooperativeAssociationSpecialAssessment,
        
        [XmlEnum("CountyPropertyTax")]
        CountyPropertyTax,
        
        [XmlEnum("DistrictPropertyTax")]
        DistrictPropertyTax,
        
        [XmlEnum("EarthquakeInsurancePremium")]
        EarthquakeInsurancePremium,
        
        [XmlEnum("FloodInsurancePremium")]
        FloodInsurancePremium,
        
        [XmlEnum("GroundRent")]
        GroundRent,
        
        [XmlEnum("HailInsurancePremium")]
        HailInsurancePremium,
        
        [XmlEnum("HazardInsurancePremium")]
        HazardInsurancePremium,
        
        [XmlEnum("HomeownersAssociationDues")]
        HomeownersAssociationDues,
        
        [XmlEnum("HomeownersAssociationSpecialAssessment")]
        HomeownersAssociationSpecialAssessment,
        
        [XmlEnum("HomeownersInsurancePremium")]
        HomeownersInsurancePremium,
        
        [XmlEnum("InterestOnLoanAssumption")]
        InterestOnLoanAssumption,
        
        [XmlEnum("MortgageInsurancePremium")]
        MortgageInsurancePremium,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PastDuePropertyTax")]
        PastDuePropertyTax,
        
        [XmlEnum("RentFromSubjectProperty")]
        RentFromSubjectProperty,
        
        [XmlEnum("SellersReserveAccountAssumption")]
        SellersReserveAccountAssumption,
        
        [XmlEnum("StatePropertyTax")]
        StatePropertyTax,
        
        [XmlEnum("TownPropertyTax")]
        TownPropertyTax,
        
        [XmlEnum("Utilities")]
        Utilities,
        
        [XmlEnum("VolcanoInsurancePremium")]
        VolcanoInsurancePremium,
        
        [XmlEnum("WindAndStormInsurancePremium")]
        WindAndStormInsurancePremium,
    }
}
