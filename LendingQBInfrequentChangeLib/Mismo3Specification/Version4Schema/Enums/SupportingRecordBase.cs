namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum SupportingRecordBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("CallRecording")]
        CallRecording,
        
        [XmlEnum("ConversationSummary")]
        ConversationSummary,
        
        [XmlEnum("CreditReportingHistory")]
        CreditReportingHistory,
        
        [XmlEnum("Document")]
        Document,
        
        [XmlEnum("ElectronicCommunication")]
        ElectronicCommunication,
        
        [XmlEnum("ElectronicImage")]
        ElectronicImage,
        
        [XmlEnum("Facsimile")]
        Facsimile,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("ServicingFile")]
        ServicingFile,
        
        [XmlEnum("ServicingFileBankruptcyNotes")]
        ServicingFileBankruptcyNotes,
        
        [XmlEnum("ServicingFileBorrowerSuppliedInformation")]
        ServicingFileBorrowerSuppliedInformation,
        
        [XmlEnum("ServicingFileCollectionNotes")]
        ServicingFileCollectionNotes,
        
        [XmlEnum("ServicingFileEscrowAnalysisInformation")]
        ServicingFileEscrowAnalysisInformation,
        
        [XmlEnum("ServicingFileForeclosureNotes")]
        ServicingFileForeclosureNotes,
        
        [XmlEnum("ServicingFileGeneralCustomerServiceNotes")]
        ServicingFileGeneralCustomerServiceNotes,
        
        [XmlEnum("ServicingFileHomeownersInsuranceInformation")]
        ServicingFileHomeownersInsuranceInformation,
        
        [XmlEnum("ServicingFileLoanModificationInvestorCriteria")]
        ServicingFileLoanModificationInvestorCriteria,
        
        [XmlEnum("ServicingFileLossMitigationNotes")]
        ServicingFileLossMitigationNotes,
        
        [XmlEnum("ServicingFileMortgageInsuranceInformation")]
        ServicingFileMortgageInsuranceInformation,
        
        [XmlEnum("ServicingFileOther")]
        ServicingFileOther,
        
        [XmlEnum("ServicingFilePaymentHistory")]
        ServicingFilePaymentHistory,
        
        [XmlEnum("ServicingFileRealEstateTaxInformation")]
        ServicingFileRealEstateTaxInformation,
        
        [XmlEnum("ServicingFileServicerStructuredData")]
        ServicingFileServicerStructuredData,
        
        [XmlEnum("ServicingFileThirdPartyStructuredData")]
        ServicingFileThirdPartyStructuredData,
        
        [XmlEnum("VoiceMailRecording")]
        VoiceMailRecording,
    }
}
