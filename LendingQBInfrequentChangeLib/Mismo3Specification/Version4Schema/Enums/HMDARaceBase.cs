namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum HMDARaceBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AmericanIndianOrAlaskaNative")]
        AmericanIndianOrAlaskaNative,
        
        [XmlEnum("Asian")]
        Asian,
        
        [XmlEnum("BlackOrAfricanAmerican")]
        BlackOrAfricanAmerican,
        
        [XmlEnum("InformationNotProvidedByApplicantInMailInternetOrTelephoneApplication")]
        InformationNotProvidedByApplicantInMailInternetOrTelephoneApplication,
        
        [XmlEnum("NativeHawaiianOrOtherPacificIslander")]
        NativeHawaiianOrOtherPacificIslander,
        
        [XmlEnum("NotApplicable")]
        NotApplicable,
        
        [XmlEnum("White")]
        White,
    }
}
