namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ConstructionMethodBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Manufactured")]
        Manufactured,
        
        [XmlEnum("MobileHome")]
        MobileHome,
        
        [XmlEnum("Modular")]
        Modular,
        
        [XmlEnum("OnFrameModular")]
        OnFrameModular,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("SiteBuilt")]
        SiteBuilt,
    }
}
