namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ConstructionStatusBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Existing")]
        Existing,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Planned")]
        Planned,
        
        [XmlEnum("Proposed")]
        Proposed,
        
        [XmlEnum("SubjectToAlteration")]
        SubjectToAlteration,
        
        [XmlEnum("SubjectToAlterationImprovementRepairAndRehabilitation")]
        SubjectToAlterationImprovementRepairAndRehabilitation,
        
        [XmlEnum("SubstantiallyRehabilitated")]
        SubstantiallyRehabilitated,
        
        [XmlEnum("UnderConstruction")]
        UnderConstruction,
    }
}
