namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum CovenantBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Architectural")]
        Architectural,
        
        [XmlEnum("CommunityUse")]
        CommunityUse,
        
        [XmlEnum("Land")]
        Land,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Utility")]
        Utility,
    }
}
