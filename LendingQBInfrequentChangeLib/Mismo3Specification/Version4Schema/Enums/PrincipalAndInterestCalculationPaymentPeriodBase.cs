namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PrincipalAndInterestCalculationPaymentPeriodBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("30DayPaymentPeriod")]
        Item30DayPaymentPeriod,
        
        [XmlEnum("NumberOfDaysBetweenPayments")]
        NumberOfDaysBetweenPayments,
        
        [XmlEnum("NumberOfDaysInCalendarMonth")]
        NumberOfDaysInCalendarMonth,
        
        [XmlEnum("Other")]
        Other,
    }
}
