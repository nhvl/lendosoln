namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum HMDAApplicationSubmissionBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("NotApplicable")]
        NotApplicable,
        
        [XmlEnum("NotSubmittedDirectly")]
        NotSubmittedDirectly,
        
        [XmlEnum("SubmittedDirectly")]
        SubmittedDirectly,
    }
}
