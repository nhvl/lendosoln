namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum AppraiserDesignationBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AccreditedAgriculturalConsultant")]
        AccreditedAgriculturalConsultant,
        
        [XmlEnum("AccreditedFarmManager")]
        AccreditedFarmManager,
        
        [XmlEnum("AccreditedMember")]
        AccreditedMember,
        
        [XmlEnum("AccreditedRuralAppraiser")]
        AccreditedRuralAppraiser,
        
        [XmlEnum("AccreditedSeniorAppraiser")]
        AccreditedSeniorAppraiser,
        
        [XmlEnum("AssociateOfTheRoyalInstitutionOfCharteredSurveyors")]
        AssociateOfTheRoyalInstitutionOfCharteredSurveyors,
        
        [XmlEnum("DesignatedMember")]
        DesignatedMember,
        
        [XmlEnum("DesignatedMemberAgricultural")]
        DesignatedMemberAgricultural,
        
        [XmlEnum("DesignatedMemberCounselor")]
        DesignatedMemberCounselor,
        
        [XmlEnum("DesignatedMemberSenior")]
        DesignatedMemberSenior,
        
        [XmlEnum("FellowOfTheRoyalInstitutionOfCharteredSurveyors")]
        FellowOfTheRoyalInstitutionOfCharteredSurveyors,
        
        [XmlEnum("GeneralAccreditedAppraiser")]
        GeneralAccreditedAppraiser,
        
        [XmlEnum("MemberAppraisalInstitute")]
        MemberAppraisalInstitute,
        
        [XmlEnum("MemberOfTheRoyalInstitutionOfCharteredSurveyors")]
        MemberOfTheRoyalInstitutionOfCharteredSurveyors,
        
        [XmlEnum("RealPropertyReviewAppraiser")]
        RealPropertyReviewAppraiser,
        
        [XmlEnum("ResidentialAccreditedAppraiser")]
        ResidentialAccreditedAppraiser,
        
        [XmlEnum("SeniorRealPropertyAppraiser")]
        SeniorRealPropertyAppraiser,
        
        [XmlEnum("SeniorResidentialAppraiser")]
        SeniorResidentialAppraiser,
    }
}
