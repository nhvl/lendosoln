namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum IncomeReportedBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("PostTax")]
        PostTax,
        
        [XmlEnum("PreTax")]
        PreTax,
    }
}
