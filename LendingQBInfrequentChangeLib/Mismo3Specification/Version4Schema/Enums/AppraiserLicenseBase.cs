namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum AppraiserLicenseBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("CertifiedGeneral")]
        CertifiedGeneral,
        
        [XmlEnum("CertifiedResidential")]
        CertifiedResidential,
        
        [XmlEnum("LicensedResidentialAppraiser")]
        LicensedResidentialAppraiser,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("RegisteredTraineeApprentice")]
        RegisteredTraineeApprentice,
    }
}
