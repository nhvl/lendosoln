namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum EventParticipantBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Initiator")]
        Initiator,
        
        [XmlEnum("Investigator")]
        Investigator,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PrimaryPointOfContact")]
        PrimaryPointOfContact,
        
        [XmlEnum("Recipient")]
        Recipient,
        
        [XmlEnum("Resolver")]
        Resolver,
        
        [XmlEnum("Responder")]
        Responder,
    }
}
