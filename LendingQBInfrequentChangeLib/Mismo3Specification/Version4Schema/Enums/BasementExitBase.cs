namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum BasementExitBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("InteriorStairs")]
        InteriorStairs,
        
        [XmlEnum("Walkout")]
        Walkout,
        
        [XmlEnum("Walkup")]
        Walkup,
    }
}
