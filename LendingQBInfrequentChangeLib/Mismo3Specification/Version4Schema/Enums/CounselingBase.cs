namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum CounselingBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Counseling")]
        Counseling,
        
        [XmlEnum("Education")]
        Education,
    }
}
