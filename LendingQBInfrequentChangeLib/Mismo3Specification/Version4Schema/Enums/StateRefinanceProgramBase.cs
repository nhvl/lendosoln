namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum StateRefinanceProgramBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("ConsolidationExtensionAndModificationAgreement")]
        ConsolidationExtensionAndModificationAgreement,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("TexasHomeEquity")]
        TexasHomeEquity,
    }
}
