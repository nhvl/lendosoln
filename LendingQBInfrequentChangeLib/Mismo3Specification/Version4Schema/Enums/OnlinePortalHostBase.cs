namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum OnlinePortalHostBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Agency")]
        Agency,
        
        [XmlEnum("CFPB")]
        CFPB,
        
        [XmlEnum("HomeownersProtectionOffice")]
        HomeownersProtectionOffice,
        
        [XmlEnum("LoanInvestor")]
        LoanInvestor,
        
        [XmlEnum("LoanOriginator")]
        LoanOriginator,
        
        [XmlEnum("LoanServicer")]
        LoanServicer,
        
        [XmlEnum("OCC")]
        OCC,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("OwnerAssignee")]
        OwnerAssignee,
        
        [XmlEnum("ServiceProvider")]
        ServiceProvider,
    }
}
