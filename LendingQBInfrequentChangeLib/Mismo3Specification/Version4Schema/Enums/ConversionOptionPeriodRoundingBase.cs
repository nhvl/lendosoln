namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ConversionOptionPeriodRoundingBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Down")]
        Down,
        
        [XmlEnum("Nearest")]
        Nearest,
        
        [XmlEnum("NoRounding")]
        NoRounding,
        
        [XmlEnum("Up")]
        Up,
    }
}
