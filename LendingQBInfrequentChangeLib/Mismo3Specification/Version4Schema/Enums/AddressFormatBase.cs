namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum AddressFormatBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("HighwayContract")]
        HighwayContract,
        
        [XmlEnum("Individual")]
        Individual,
        
        [XmlEnum("International")]
        International,
        
        [XmlEnum("Military")]
        Military,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PostOfficeBox")]
        PostOfficeBox,
        
        [XmlEnum("RuralRoute")]
        RuralRoute,
    }
}
