namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum AcceptableSigningMethodBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Any")]
        Any,
        
        [XmlEnum("Electronic")]
        Electronic,
        
        [XmlEnum("None")]
        None,
        
        [XmlEnum("Wet")]
        Wet,
    }
}
