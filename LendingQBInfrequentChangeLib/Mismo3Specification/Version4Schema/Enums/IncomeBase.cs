namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum IncomeBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AccessoryUnitIincome")]
        AccessoryUnitIincome,
        
        [XmlEnum("Alimony")]
        Alimony,
        
        [XmlEnum("AutomobileAllowance")]
        AutomobileAllowance,
        
        [XmlEnum("Base")]
        Base,
        
        [XmlEnum("BoarderIncome")]
        BoarderIncome,
        
        [XmlEnum("Bonus")]
        Bonus,
        
        [XmlEnum("BorrowerEstimatedTotalMonthlyIncome")]
        BorrowerEstimatedTotalMonthlyIncome,
        
        [XmlEnum("CapitalGains")]
        CapitalGains,
        
        [XmlEnum("ChildSupport")]
        ChildSupport,
        
        [XmlEnum("Commissions")]
        Commissions,
        
        [XmlEnum("ContractBasis")]
        ContractBasis,
        
        [XmlEnum("DefinedContributionPlan")]
        DefinedContributionPlan,
        
        [XmlEnum("Disability")]
        Disability,
        
        [XmlEnum("DividendsInterest")]
        DividendsInterest,
        
        [XmlEnum("EmploymentRelatedAccount")]
        EmploymentRelatedAccount,
        
        [XmlEnum("FosterCare")]
        FosterCare,
        
        [XmlEnum("HousingAllowance")]
        HousingAllowance,
        
        [XmlEnum("HousingChoiceVoucherProgram")]
        HousingChoiceVoucherProgram,
        
        [XmlEnum("MilitaryBasePay")]
        MilitaryBasePay,
        
        [XmlEnum("MilitaryClothesAllowance")]
        MilitaryClothesAllowance,
        
        [XmlEnum("MilitaryCombatPay")]
        MilitaryCombatPay,
        
        [XmlEnum("MilitaryFlightPay")]
        MilitaryFlightPay,
        
        [XmlEnum("MilitaryHazardPay")]
        MilitaryHazardPay,
        
        [XmlEnum("MilitaryOverseasPay")]
        MilitaryOverseasPay,
        
        [XmlEnum("MilitaryPropPay")]
        MilitaryPropPay,
        
        [XmlEnum("MilitaryQuartersAllowance")]
        MilitaryQuartersAllowance,
        
        [XmlEnum("MilitaryRationsAllowance")]
        MilitaryRationsAllowance,
        
        [XmlEnum("MilitaryVariableHousingAllowance")]
        MilitaryVariableHousingAllowance,
        
        [XmlEnum("MiscellaneousIncome")]
        MiscellaneousIncome,
        
        [XmlEnum("MortgageCreditCertificate")]
        MortgageCreditCertificate,
        
        [XmlEnum("MortgageDifferential")]
        MortgageDifferential,
        
        [XmlEnum("NetRentalIncome")]
        NetRentalIncome,
        
        [XmlEnum("NonBorrowerContribution")]
        NonBorrowerContribution,
        
        [XmlEnum("NonBorrowerHouseholdIncome")]
        NonBorrowerHouseholdIncome,
        
        [XmlEnum("NotesReceivableInstallment")]
        NotesReceivableInstallment,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Overtime")]
        Overtime,
        
        [XmlEnum("Pension")]
        Pension,
        
        [XmlEnum("ProposedGrossRentForSubjectProperty")]
        ProposedGrossRentForSubjectProperty,
        
        [XmlEnum("PublicAssistance")]
        PublicAssistance,
        
        [XmlEnum("RealEstateOwnedGrossRentalIncome")]
        RealEstateOwnedGrossRentalIncome,
        
        [XmlEnum("Royalties")]
        Royalties,
        
        [XmlEnum("SelfEmploymentIncome")]
        SelfEmploymentIncome,
        
        [XmlEnum("SelfEmploymentLoss")]
        SelfEmploymentLoss,
        
        [XmlEnum("SeparateMaintenance")]
        SeparateMaintenance,
        
        [XmlEnum("SocialSecurity")]
        SocialSecurity,
        
        [XmlEnum("SubjectPropertyNetCashFlow")]
        SubjectPropertyNetCashFlow,
        
        [XmlEnum("TemporaryLeave")]
        TemporaryLeave,
        
        [XmlEnum("TipIncome")]
        TipIncome,
        
        [XmlEnum("TrailingCoBorrowerIncome")]
        TrailingCoBorrowerIncome,
        
        [XmlEnum("Trust")]
        Trust,
        
        [XmlEnum("Unemployment")]
        Unemployment,
        
        [XmlEnum("VABenefitsNonEducational")]
        VABenefitsNonEducational,
        
        [XmlEnum("WorkersCompensation")]
        WorkersCompensation,
    }
}
