namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum BuydownCalculationBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("DecliningLoanBalance")]
        DecliningLoanBalance,
        
        [XmlEnum("OriginalLoanAmount")]
        OriginalLoanAmount,
    }
}
