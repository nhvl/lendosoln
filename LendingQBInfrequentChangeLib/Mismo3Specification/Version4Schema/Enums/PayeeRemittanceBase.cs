namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PayeeRemittanceBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Check")]
        Check,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Wire")]
        Wire,
    }
}
