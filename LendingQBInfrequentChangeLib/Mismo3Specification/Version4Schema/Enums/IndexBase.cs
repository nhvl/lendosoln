namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum IndexBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("BankPrimeLoan")]
        BankPrimeLoan,
        
        [XmlEnum("CertificateOfDepositIndex")]
        CertificateOfDepositIndex,
        
        [XmlEnum("ConstantMaturityTreasury")]
        ConstantMaturityTreasury,
        
        [XmlEnum("CostOfSavingsIndex")]
        CostOfSavingsIndex,
        
        [XmlEnum("EleventhDistrictCostOfFundsIndex")]
        EleventhDistrictCostOfFundsIndex,
        
        [XmlEnum("LIBOR")]
        LIBOR,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("TreasuryBill")]
        TreasuryBill,
        
        [XmlEnum("TwelveMonthTreasuryAverage")]
        TwelveMonthTreasuryAverage,
    }
}
