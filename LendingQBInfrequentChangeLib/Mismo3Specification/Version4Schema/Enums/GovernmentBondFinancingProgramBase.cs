namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum GovernmentBondFinancingProgramBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("BuilderBond")]
        BuilderBond,
        
        [XmlEnum("ConsolidatedBond")]
        ConsolidatedBond,
        
        [XmlEnum("FinalBond")]
        FinalBond,
        
        [XmlEnum("Other")]
        Other,
    }
}
