namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum AppraisalReviewCategoryAccuracyBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Accurate")]
        Accurate,
        
        [XmlEnum("NotAccurate")]
        NotAccurate,
        
        [XmlEnum("NotApplicable")]
        NotApplicable,
        
        [XmlEnum("NotDeveloped")]
        NotDeveloped,
    }
}
