namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PaymentFrequencyBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Annual")]
        Annual,
        
        [XmlEnum("AtMaturity")]
        AtMaturity,
        
        [XmlEnum("Biweekly")]
        Biweekly,
        
        [XmlEnum("Monthly")]
        Monthly,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Quarterly")]
        Quarterly,
        
        [XmlEnum("Semiannual")]
        Semiannual,
        
        [XmlEnum("Semimonthly")]
        Semimonthly,
        
        [XmlEnum("Weekly")]
        Weekly,
    }
}
