namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum QualifiedMortgageTemporaryGSEBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("FannieMae")]
        FannieMae,
        
        [XmlEnum("FreddieMac")]
        FreddieMac,
    }
}
