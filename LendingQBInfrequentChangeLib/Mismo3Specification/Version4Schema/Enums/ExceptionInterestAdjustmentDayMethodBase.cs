namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ExceptionInterestAdjustmentDayMethodBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("ActualLastPaidInstallmentDueDate")]
        ActualLastPaidInstallmentDueDate,
        
        [XmlEnum("FirstOfMonth")]
        FirstOfMonth,
        
        [XmlEnum("ScheduledLastPaidInstallmentDueDate")]
        ScheduledLastPaidInstallmentDueDate,
    }
}
