namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PropertyInspectionPurposeBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("BuildingCodeCompliance")]
        BuildingCodeCompliance,
        
        [XmlEnum("BuildingComponent")]
        BuildingComponent,
        
        [XmlEnum("ComplianceInspection")]
        ComplianceInspection,
        
        [XmlEnum("EnergyEfficiency")]
        EnergyEfficiency,
        
        [XmlEnum("EnvironmentalHazard")]
        EnvironmentalHazard,
        
        [XmlEnum("HomeInspection")]
        HomeInspection,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Pest")]
        Pest,
        
        [XmlEnum("PropertyPreservation")]
        PropertyPreservation,
        
        [XmlEnum("Valuation")]
        Valuation,
    }
}
