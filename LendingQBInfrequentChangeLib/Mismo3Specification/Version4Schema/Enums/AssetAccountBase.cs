namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum AssetAccountBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("BorrowerManagedAccount")]
        BorrowerManagedAccount,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("RelatedPartyManagedAccount")]
        RelatedPartyManagedAccount,
    }
}
