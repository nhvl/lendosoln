namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum BuydownBaseDateBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("FirstPaymentDate")]
        FirstPaymentDate,
        
        [XmlEnum("LastPaymentDate")]
        LastPaymentDate,
        
        [XmlEnum("NoteDate")]
        NoteDate,
        
        [XmlEnum("Other")]
        Other,
    }
}
