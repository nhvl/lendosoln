namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum IndexCalculationMethodBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AverageTwoIndexRates")]
        AverageTwoIndexRates,
        
        [XmlEnum("CurrentDaysIndexRate")]
        CurrentDaysIndexRate,
        
        [XmlEnum("HighestOfTwoIndexRates")]
        HighestOfTwoIndexRates,
        
        [XmlEnum("LowestOfTwoIndexRates")]
        LowestOfTwoIndexRates,
        
        [XmlEnum("SingleIndexRate")]
        SingleIndexRate,
    }
}
