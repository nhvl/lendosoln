namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum CreditRequestBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Individual")]
        Individual,
        
        [XmlEnum("Joint")]
        Joint,
    }
}
