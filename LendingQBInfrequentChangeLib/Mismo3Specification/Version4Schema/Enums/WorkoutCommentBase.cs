namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum WorkoutCommentBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("DeedInLieu")]
        DeedInLieu,
        
        [XmlEnum("General")]
        General,
        
        [XmlEnum("MIDecision")]
        MIDecision,
        
        [XmlEnum("Other")]
        Other,
    }
}
