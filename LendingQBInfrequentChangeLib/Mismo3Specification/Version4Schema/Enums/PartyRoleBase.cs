namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PartyRoleBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Appraiser")]
        Appraiser,
        
        [XmlEnum("AppraiserSupervisor")]
        AppraiserSupervisor,
        
        [XmlEnum("AssignFrom")]
        AssignFrom,
        
        [XmlEnum("AssignTo")]
        AssignTo,
        
        [XmlEnum("Attorney")]
        Attorney,
        
        [XmlEnum("AttorneyInFact")]
        AttorneyInFact,
        
        [XmlEnum("AuthorizedRepresentative")]
        AuthorizedRepresentative,
        
        [XmlEnum("AuthorizedThirdParty")]
        AuthorizedThirdParty,
        
        [XmlEnum("BankruptcyFiler")]
        BankruptcyFiler,
        
        [XmlEnum("BankruptcyTrustee")]
        BankruptcyTrustee,
        
        [XmlEnum("BeneficialInterestParty")]
        BeneficialInterestParty,
        
        [XmlEnum("BillToParty")]
        BillToParty,
        
        [XmlEnum("Borrower")]
        Borrower,
        
        [XmlEnum("Builder")]
        Builder,
        
        [XmlEnum("Client")]
        Client,
        
        [XmlEnum("ClosingAgent")]
        ClosingAgent,
        
        [XmlEnum("Conservator")]
        Conservator,
        
        [XmlEnum("ConsumerReportingAgency")]
        ConsumerReportingAgency,
        
        [XmlEnum("CooperativeCompany")]
        CooperativeCompany,
        
        [XmlEnum("CorrespondentLender")]
        CorrespondentLender,
        
        [XmlEnum("Cosigner")]
        Cosigner,
        
        [XmlEnum("CreditCounselingAgent")]
        CreditCounselingAgent,
        
        [XmlEnum("CreditEnhancementRiskHolder")]
        CreditEnhancementRiskHolder,
        
        [XmlEnum("CustodianNotePayTo")]
        CustodianNotePayTo,
        
        [XmlEnum("Defendant")]
        Defendant,
        
        [XmlEnum("DeliverRescissionTo")]
        DeliverRescissionTo,
        
        [XmlEnum("DesignatedContact")]
        DesignatedContact,
        
        [XmlEnum("DocumentCustodian")]
        DocumentCustodian,
        
        [XmlEnum("ENoteController")]
        ENoteController,
        
        [XmlEnum("ENoteControllerTransferee")]
        ENoteControllerTransferee,
        
        [XmlEnum("ENoteCustodian")]
        ENoteCustodian,
        
        [XmlEnum("ENoteCustodianTransferee")]
        ENoteCustodianTransferee,
        
        [XmlEnum("ENoteDelegateeForTransfers")]
        ENoteDelegateeForTransfers,
        
        [XmlEnum("ENoteRegisteringParty")]
        ENoteRegisteringParty,
        
        [XmlEnum("ENoteServicer")]
        ENoteServicer,
        
        [XmlEnum("ENoteServicerTransferee")]
        ENoteServicerTransferee,
        
        [XmlEnum("ENoteTransferInitiator")]
        ENoteTransferInitiator,
        
        [XmlEnum("Executor")]
        Executor,
        
        [XmlEnum("FHASponsor")]
        FHASponsor,
        
        [XmlEnum("FloodCertificateProvider")]
        FloodCertificateProvider,
        
        [XmlEnum("FulfillmentParty")]
        FulfillmentParty,
        
        [XmlEnum("GiftDonor")]
        GiftDonor,
        
        [XmlEnum("Grantee")]
        Grantee,
        
        [XmlEnum("Grantor")]
        Grantor,
        
        [XmlEnum("HazardInsuranceAgent")]
        HazardInsuranceAgent,
        
        [XmlEnum("HazardInsuranceCompany")]
        HazardInsuranceCompany,
        
        [XmlEnum("HomeownersAssociation")]
        HomeownersAssociation,
        
        [XmlEnum("HousingCounselingAgency")]
        HousingCounselingAgency,
        
        [XmlEnum("HousingCounselingAgent")]
        HousingCounselingAgent,
        
        [XmlEnum("HUD1SettlementAgent")]
        HUD1SettlementAgent,
        
        [XmlEnum("Interviewer")]
        Interviewer,
        
        [XmlEnum("InterviewerEmployer")]
        InterviewerEmployer,
        
        [XmlEnum("Investor")]
        Investor,
        
        [XmlEnum("IRSTaxFormThirdParty")]
        IRSTaxFormThirdParty,
        
        [XmlEnum("LawFirm")]
        LawFirm,
        
        [XmlEnum("Lender")]
        Lender,
        
        [XmlEnum("LenderBranch")]
        LenderBranch,
        
        [XmlEnum("LienHolder")]
        LienHolder,
        
        [XmlEnum("LoanCloser")]
        LoanCloser,
        
        [XmlEnum("LoanDeliveryFilePreparer")]
        LoanDeliveryFilePreparer,
        
        [XmlEnum("LoanFunder")]
        LoanFunder,
        
        [XmlEnum("LoanOfficer")]
        LoanOfficer,
        
        [XmlEnum("LoanOriginationCompany")]
        LoanOriginationCompany,
        
        [XmlEnum("LoanOriginator")]
        LoanOriginator,
        
        [XmlEnum("LoanProcessor")]
        LoanProcessor,
        
        [XmlEnum("LoanSeller")]
        LoanSeller,
        
        [XmlEnum("LoanUnderwriter")]
        LoanUnderwriter,
        
        [XmlEnum("LossPayee")]
        LossPayee,
        
        [XmlEnum("ManagementCompany")]
        ManagementCompany,
        
        [XmlEnum("MICompany")]
        MICompany,
        
        [XmlEnum("MortgageBroker")]
        MortgageBroker,
        
        [XmlEnum("NonTitleNonSpouseOwnershipInterest")]
        NonTitleNonSpouseOwnershipInterest,
        
        [XmlEnum("NonTitleSpouse")]
        NonTitleSpouse,
        
        [XmlEnum("Notary")]
        Notary,
        
        [XmlEnum("NotePayTo")]
        NotePayTo,
        
        [XmlEnum("NotePayToRecipient")]
        NotePayToRecipient,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Payee")]
        Payee,
        
        [XmlEnum("Plaintiff")]
        Plaintiff,
        
        [XmlEnum("PoolInsurer")]
        PoolInsurer,
        
        [XmlEnum("PoolIssuer")]
        PoolIssuer,
        
        [XmlEnum("PoolIssuerTransferee")]
        PoolIssuerTransferee,
        
        [XmlEnum("PreparedBy")]
        PreparedBy,
        
        [XmlEnum("ProjectDeveloper")]
        ProjectDeveloper,
        
        [XmlEnum("ProjectManagementAgent")]
        ProjectManagementAgent,
        
        [XmlEnum("PropertyAccessContact")]
        PropertyAccessContact,
        
        [XmlEnum("PropertyJurisdictionalAuthority")]
        PropertyJurisdictionalAuthority,
        
        [XmlEnum("PropertyOwner")]
        PropertyOwner,
        
        [XmlEnum("PropertyPreservationAgent")]
        PropertyPreservationAgent,
        
        [XmlEnum("PropertyPurchaser")]
        PropertyPurchaser,
        
        [XmlEnum("PropertySeller")]
        PropertySeller,
        
        [XmlEnum("RealEstateAgent")]
        RealEstateAgent,
        
        [XmlEnum("ReceivingParty")]
        ReceivingParty,
        
        [XmlEnum("RegistryOperator")]
        RegistryOperator,
        
        [XmlEnum("RegulatoryAgency")]
        RegulatoryAgency,
        
        [XmlEnum("RequestingParty")]
        RequestingParty,
        
        [XmlEnum("RespondingParty")]
        RespondingParty,
        
        [XmlEnum("RespondToParty")]
        RespondToParty,
        
        [XmlEnum("ReturnTo")]
        ReturnTo,
        
        [XmlEnum("ReviewAppraiser")]
        ReviewAppraiser,
        
        [XmlEnum("SecurityIssuer")]
        SecurityIssuer,
        
        [XmlEnum("ServiceBureau")]
        ServiceBureau,
        
        [XmlEnum("ServiceProvider")]
        ServiceProvider,
        
        [XmlEnum("Servicer")]
        Servicer,
        
        [XmlEnum("ServicerPaymentCollection")]
        ServicerPaymentCollection,
        
        [XmlEnum("Settlor")]
        Settlor,
        
        [XmlEnum("Spouse")]
        Spouse,
        
        [XmlEnum("SubmittingParty")]
        SubmittingParty,
        
        [XmlEnum("TaxableParty")]
        TaxableParty,
        
        [XmlEnum("TaxAssessor")]
        TaxAssessor,
        
        [XmlEnum("TaxCollector")]
        TaxCollector,
        
        [XmlEnum("Taxpayer")]
        Taxpayer,
        
        [XmlEnum("TaxServiceProvider")]
        TaxServiceProvider,
        
        [XmlEnum("TaxServicer")]
        TaxServicer,
        
        [XmlEnum("ThirdPartyInvestor")]
        ThirdPartyInvestor,
        
        [XmlEnum("ThirdPartyOriginator")]
        ThirdPartyOriginator,
        
        [XmlEnum("TitleCompany")]
        TitleCompany,
        
        [XmlEnum("TitleHolder")]
        TitleHolder,
        
        [XmlEnum("TitleUnderwriter")]
        TitleUnderwriter,
        
        [XmlEnum("Trust")]
        Trust,
        
        [XmlEnum("TrustBeneficiary")]
        TrustBeneficiary,
        
        [XmlEnum("Trustee")]
        Trustee,
        
        [XmlEnum("Unspecified")]
        Unspecified,
        
        [XmlEnum("WarehouseLender")]
        WarehouseLender,
        
        [XmlEnum("Witness")]
        Witness,
    }
}
