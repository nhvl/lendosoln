namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum NativeAmericanLandsBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("FeeSimple")]
        FeeSimple,
        
        [XmlEnum("HawaiianHomeLands")]
        HawaiianHomeLands,
        
        [XmlEnum("IndividualTrustLand")]
        IndividualTrustLand,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("TribalTrustLand")]
        TribalTrustLand,
    }
}
