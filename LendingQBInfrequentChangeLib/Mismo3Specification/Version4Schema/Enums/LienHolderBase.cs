namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum LienHolderBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("FNM")]
        FNM,
        
        [XmlEnum("FRE")]
        FRE,
        
        [XmlEnum("GovernmentAgency")]
        GovernmentAgency,
        
        [XmlEnum("Lender")]
        Lender,
        
        [XmlEnum("NonProfitOrganization")]
        NonProfitOrganization,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PropertySeller")]
        PropertySeller,
        
        [XmlEnum("StateOrLocalHousingFinanceAgency")]
        StateOrLocalHousingFinanceAgency,
        
        [XmlEnum("Unknown")]
        Unknown,
    }
}
