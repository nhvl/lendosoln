namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum AssignmentConditionBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("CertifiedAppraiser")]
        CertifiedAppraiser,
        
        [XmlEnum("CertifiedGeneral")]
        CertifiedGeneral,
        
        [XmlEnum("FHARoster")]
        FHARoster,
        
        [XmlEnum("NoTrainee")]
        NoTrainee,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PreApprovedByClient")]
        PreApprovedByClient,
        
        [XmlEnum("ProfessionalDesignation")]
        ProfessionalDesignation,
        
        [XmlEnum("VAPanel")]
        VAPanel,
    }
}
