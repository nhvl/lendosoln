namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum QualityRatingBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Average")]
        Average,
        
        [XmlEnum("Excellent")]
        Excellent,
        
        [XmlEnum("Fair")]
        Fair,
        
        [XmlEnum("Good")]
        Good,
        
        [XmlEnum("Poor")]
        Poor,
    }
}
