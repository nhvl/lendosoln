namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum DismissalBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("WithoutPrejudice")]
        WithoutPrejudice,
        
        [XmlEnum("WithPrejudice")]
        WithPrejudice,
    }
}
