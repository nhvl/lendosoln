namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ArchitecturalDesignConfigurationBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Duplex")]
        Duplex,
        
        [XmlEnum("Fourplex")]
        Fourplex,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("RowHouse")]
        RowHouse,
        
        [XmlEnum("Single")]
        Single,
        
        [XmlEnum("Triplex")]
        Triplex,
    }
}
