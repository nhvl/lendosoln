namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum NeighborhoodLandUseChangeStatusBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("InProcess")]
        InProcess,
        
        [XmlEnum("Likely")]
        Likely,
        
        [XmlEnum("NotLikely")]
        NotLikely,
    }
}
