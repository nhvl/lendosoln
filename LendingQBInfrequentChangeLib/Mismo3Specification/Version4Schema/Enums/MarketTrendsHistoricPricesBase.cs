namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum MarketTrendsHistoricPricesBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Decreasing")]
        Decreasing,
        
        [XmlEnum("Increasing")]
        Increasing,
        
        [XmlEnum("Stable")]
        Stable,
    }
}
