namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PropertyEstateBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("FeeSimple")]
        FeeSimple,
        
        [XmlEnum("Fractional")]
        Fractional,
        
        [XmlEnum("Leasehold")]
        Leasehold,
        
        [XmlEnum("Other")]
        Other,
    }
}
