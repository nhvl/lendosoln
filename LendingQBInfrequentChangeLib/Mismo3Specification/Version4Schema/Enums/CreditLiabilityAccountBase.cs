namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum CreditLiabilityAccountBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("CreditLine")]
        CreditLine,
        
        [XmlEnum("Installment")]
        Installment,
        
        [XmlEnum("Mortgage")]
        Mortgage,
        
        [XmlEnum("Open")]
        Open,
        
        [XmlEnum("Revolving")]
        Revolving,
        
        [XmlEnum("Unknown")]
        Unknown,
    }
}
