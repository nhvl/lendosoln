namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum StopCodeConditionBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("BadCheckHoldingNSF")]
        BadCheckHoldingNSF,
        
        [XmlEnum("LoanInBankruptcy")]
        LoanInBankruptcy,
        
        [XmlEnum("LoanInForeclosure")]
        LoanInForeclosure,
        
        [XmlEnum("LoanPaidInFull")]
        LoanPaidInFull,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PayoffPending")]
        PayoffPending,
        
        [XmlEnum("ProcessingException")]
        ProcessingException,
    }
}
