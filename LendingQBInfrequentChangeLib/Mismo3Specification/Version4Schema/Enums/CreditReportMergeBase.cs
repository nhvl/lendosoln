namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum CreditReportMergeBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Blend")]
        Blend,
        
        [XmlEnum("ListAndStack")]
        ListAndStack,
        
        [XmlEnum("PickAndChoose")]
        PickAndChoose,
    }
}
