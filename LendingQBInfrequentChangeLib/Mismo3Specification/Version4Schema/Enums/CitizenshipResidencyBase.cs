namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum CitizenshipResidencyBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("NonPermanentResidentAlien")]
        NonPermanentResidentAlien,
        
        [XmlEnum("NonResidentAlien")]
        NonResidentAlien,
        
        [XmlEnum("PermanentResidentAlien")]
        PermanentResidentAlien,
        
        [XmlEnum("Unknown")]
        Unknown,
        
        [XmlEnum("USCitizen")]
        USCitizen,
    }
}
