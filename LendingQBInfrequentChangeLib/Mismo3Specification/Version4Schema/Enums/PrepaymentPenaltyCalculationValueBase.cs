namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PrepaymentPenaltyCalculationValueBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("FixedAmount")]
        FixedAmount,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PercentageOfPrincipalBalance")]
        PercentageOfPrincipalBalance,
        
        [XmlEnum("QualifyingAmountOfCurtailment")]
        QualifyingAmountOfCurtailment,
        
        [XmlEnum("SpecifiedMonthsOfInterest")]
        SpecifiedMonthsOfInterest,
        
        [XmlEnum("YieldMaintenance")]
        YieldMaintenance,
    }
}
