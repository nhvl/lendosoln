namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum AssistanceRecipientBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Borrower")]
        Borrower,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PrivateInvestor")]
        PrivateInvestor,
        
        [XmlEnum("Servicer")]
        Servicer,
    }
}
