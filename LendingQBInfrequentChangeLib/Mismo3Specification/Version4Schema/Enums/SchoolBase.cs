namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum SchoolBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("ElementarySchool")]
        ElementarySchool,
        
        [XmlEnum("HighSchool")]
        HighSchool,
        
        [XmlEnum("JuniorHighSchool")]
        JuniorHighSchool,
        
        [XmlEnum("MiddleSchool")]
        MiddleSchool,
        
        [XmlEnum("Other")]
        Other,
    }
}
