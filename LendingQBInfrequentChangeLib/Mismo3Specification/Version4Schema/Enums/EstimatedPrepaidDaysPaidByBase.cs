namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum EstimatedPrepaidDaysPaidByBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Buyer")]
        Buyer,
        
        [XmlEnum("Lender")]
        Lender,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Seller")]
        Seller,
    }
}
