namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum FloorCoveringBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Carpet")]
        Carpet,
        
        [XmlEnum("CeramicTile")]
        CeramicTile,
        
        [XmlEnum("Hardwood")]
        Hardwood,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Vinyl")]
        Vinyl,
    }
}
