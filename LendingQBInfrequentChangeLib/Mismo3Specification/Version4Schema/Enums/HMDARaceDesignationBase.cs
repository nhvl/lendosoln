namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum HMDARaceDesignationBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AsianIndian")]
        AsianIndian,
        
        [XmlEnum("Chinese")]
        Chinese,
        
        [XmlEnum("Filipino")]
        Filipino,
        
        [XmlEnum("GuamanianOrChamorro")]
        GuamanianOrChamorro,
        
        [XmlEnum("Japanese")]
        Japanese,
        
        [XmlEnum("Korean")]
        Korean,
        
        [XmlEnum("NativeHawaiian")]
        NativeHawaiian,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Samoan")]
        Samoan,
        
        [XmlEnum("Vietnamese")]
        Vietnamese,
    }
}
