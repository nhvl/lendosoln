namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum SignatoryRoleBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Authorized")]
        Authorized,
        
        [XmlEnum("NotAuthorized")]
        NotAuthorized,
        
        [XmlEnum("Required")]
        Required,
    }
}
