namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum DeedInLieuTransitionDurationBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("LongTerm")]
        LongTerm,
        
        [XmlEnum("None")]
        None,
        
        [XmlEnum("ShortTerm")]
        ShortTerm,
    }
}
