namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum IntegratedDisclosureSubsectionPaymentTimingBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AtClosing")]
        AtClosing,
        
        [XmlEnum("BeforeClosing")]
        BeforeClosing,
    }
}
