namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum OutgoingCallMethodBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Dialer")]
        Dialer,
        
        [XmlEnum("Manual")]
        Manual,
    }
}
