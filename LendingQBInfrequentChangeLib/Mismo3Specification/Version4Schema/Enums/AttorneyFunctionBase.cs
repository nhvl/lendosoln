namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum AttorneyFunctionBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Bankruptcy")]
        Bankruptcy,
        
        [XmlEnum("Foreclosure")]
        Foreclosure,
        
        [XmlEnum("Other")]
        Other,
    }
}
