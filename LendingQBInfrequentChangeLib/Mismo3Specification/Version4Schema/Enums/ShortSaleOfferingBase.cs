namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ShortSaleOfferingBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Preapproved")]
        Preapproved,
        
        [XmlEnum("Requested")]
        Requested,
    }
}
