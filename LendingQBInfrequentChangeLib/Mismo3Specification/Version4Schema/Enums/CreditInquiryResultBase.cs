namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum CreditInquiryResultBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AccountClosed")]
        AccountClosed,
        
        [XmlEnum("ApplicationPending")]
        ApplicationPending,
        
        [XmlEnum("Declined")]
        Declined,
        
        [XmlEnum("DidNotInquire")]
        DidNotInquire,
        
        [XmlEnum("NoOpenAccount")]
        NoOpenAccount,
        
        [XmlEnum("NotALender")]
        NotALender,
        
        [XmlEnum("OpenAccountNumberNotIssued")]
        OpenAccountNumberNotIssued,
        
        [XmlEnum("OpenDiscovered")]
        OpenDiscovered,
        
        [XmlEnum("OpenPrimaryAccount")]
        OpenPrimaryAccount,
        
        [XmlEnum("ReportingAgencyInquiry")]
        ReportingAgencyInquiry,
        
        [XmlEnum("Unknown")]
        Unknown,
    }
}
