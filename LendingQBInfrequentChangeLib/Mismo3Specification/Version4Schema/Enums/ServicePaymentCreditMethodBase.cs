namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ServicePaymentCreditMethodBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AmericanExpress")]
        AmericanExpress,
        
        [XmlEnum("DinersClub")]
        DinersClub,
        
        [XmlEnum("Discover")]
        Discover,
        
        [XmlEnum("MasterCard")]
        MasterCard,
        
        [XmlEnum("MasterCardDebit")]
        MasterCardDebit,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Visa")]
        Visa,
        
        [XmlEnum("VisaDebit")]
        VisaDebit,
    }
}
