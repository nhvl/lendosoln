namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum FeePaidToBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Broker")]
        Broker,
        
        [XmlEnum("BrokerAffiliate")]
        BrokerAffiliate,
        
        [XmlEnum("Investor")]
        Investor,
        
        [XmlEnum("Lender")]
        Lender,
        
        [XmlEnum("LenderAffiliate")]
        LenderAffiliate,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("ThirdPartyProvider")]
        ThirdPartyProvider,
    }
}
