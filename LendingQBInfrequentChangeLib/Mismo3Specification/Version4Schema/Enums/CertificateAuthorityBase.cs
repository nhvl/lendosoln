namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum CertificateAuthorityBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Private")]
        Private,
        
        [XmlEnum("PublicFederal")]
        PublicFederal,
        
        [XmlEnum("PublicLocalCounty")]
        PublicLocalCounty,
        
        [XmlEnum("PublicLocalMunicipal")]
        PublicLocalMunicipal,
        
        [XmlEnum("PublicState")]
        PublicState,
    }
}
