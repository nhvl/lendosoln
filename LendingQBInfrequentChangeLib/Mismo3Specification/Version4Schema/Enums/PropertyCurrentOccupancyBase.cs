namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PropertyCurrentOccupancyBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Abandoned")]
        Abandoned,
        
        [XmlEnum("AdverseOccupied")]
        AdverseOccupied,
        
        [XmlEnum("OccupiedByUnknown")]
        OccupiedByUnknown,
        
        [XmlEnum("OwnerOccupied")]
        OwnerOccupied,
        
        [XmlEnum("PartiallyVacant")]
        PartiallyVacant,
        
        [XmlEnum("TenantOccupied")]
        TenantOccupied,
        
        [XmlEnum("Unknown")]
        Unknown,
        
        [XmlEnum("Vacant")]
        Vacant,
    }
}
