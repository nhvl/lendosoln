namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum EscrowItemPaymentTimingBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AtClosing")]
        AtClosing,
        
        [XmlEnum("BeforeClosing")]
        BeforeClosing,
        
        [XmlEnum("Other")]
        Other,
    }
}
