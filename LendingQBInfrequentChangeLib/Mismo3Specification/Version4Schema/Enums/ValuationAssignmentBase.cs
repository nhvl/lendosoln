namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ValuationAssignmentBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("EmployeeRelocation")]
        EmployeeRelocation,
        
        [XmlEnum("Foreclosure")]
        Foreclosure,
        
        [XmlEnum("JuniorLien")]
        JuniorLien,
        
        [XmlEnum("LossMitigation")]
        LossMitigation,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PortfolioEvaluation")]
        PortfolioEvaluation,
        
        [XmlEnum("Purchase")]
        Purchase,
        
        [XmlEnum("QualityControl")]
        QualityControl,
        
        [XmlEnum("Refinance")]
        Refinance,
        
        [XmlEnum("REO")]
        REO,
        
        [XmlEnum("Review")]
        Review,
        
        [XmlEnum("ShortSale")]
        ShortSale,
        
        [XmlEnum("Validation")]
        Validation,
    }
}
