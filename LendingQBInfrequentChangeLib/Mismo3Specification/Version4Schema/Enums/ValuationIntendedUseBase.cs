namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ValuationIntendedUseBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("EstatePlanning")]
        EstatePlanning,
        
        [XmlEnum("Litigation")]
        Litigation,
        
        [XmlEnum("MIRemoval")]
        MIRemoval,
        
        [XmlEnum("MortgageLossMitigation")]
        MortgageLossMitigation,
        
        [XmlEnum("MortgageOrigination")]
        MortgageOrigination,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PrePurchaseConsultationAssistance")]
        PrePurchaseConsultationAssistance,
        
        [XmlEnum("PrivateSale")]
        PrivateSale,
        
        [XmlEnum("PropertyOwnerAssistanceConsultation")]
        PropertyOwnerAssistanceConsultation,
        
        [XmlEnum("SecondaryMarketDueDiligence")]
        SecondaryMarketDueDiligence,
        
        [XmlEnum("TaxAppeal")]
        TaxAppeal,
        
        [XmlEnum("TaxAssessment")]
        TaxAssessment,
    }
}
