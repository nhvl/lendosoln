namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum BorrowerCharacteristicBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("LivingTrust")]
        LivingTrust,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("SeasonedWorker")]
        SeasonedWorker,
        
        [XmlEnum("SellerEmployee")]
        SellerEmployee,
        
        [XmlEnum("TrailingBorrower")]
        TrailingBorrower,
    }
}
