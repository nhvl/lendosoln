namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum TrialPlanUnsuccessfulReasonBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("BorrowerWithdrew")]
        BorrowerWithdrew,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("TrialPaymentDefault")]
        TrialPaymentDefault,
    }
}
