namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ValuationIntendedUserBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("DueDiligenceCompany")]
        DueDiligenceCompany,
        
        [XmlEnum("Investor")]
        Investor,
        
        [XmlEnum("Lender")]
        Lender,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Regulator")]
        Regulator,
    }
}
