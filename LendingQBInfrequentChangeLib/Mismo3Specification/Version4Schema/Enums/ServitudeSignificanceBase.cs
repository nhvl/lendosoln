namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ServitudeSignificanceBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Affirmative")]
        Affirmative,
        
        [XmlEnum("Negative")]
        Negative,
        
        [XmlEnum("Neutral")]
        Neutral,
    }
}
