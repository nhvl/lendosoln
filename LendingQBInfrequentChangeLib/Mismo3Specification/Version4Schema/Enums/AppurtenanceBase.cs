namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum AppurtenanceBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("BoatSlip")]
        BoatSlip,
        
        [XmlEnum("GarageSpace")]
        GarageSpace,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("ParkingSpace")]
        ParkingSpace,
        
        [XmlEnum("StorageUnit")]
        StorageUnit,
    }
}
