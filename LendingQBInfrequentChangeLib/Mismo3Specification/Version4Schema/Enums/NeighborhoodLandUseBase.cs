namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum NeighborhoodLandUseBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Apartment")]
        Apartment,
        
        [XmlEnum("Commercial")]
        Commercial,
        
        [XmlEnum("Condominium")]
        Condominium,
        
        [XmlEnum("Cooperative")]
        Cooperative,
        
        [XmlEnum("Industrial")]
        Industrial,
        
        [XmlEnum("ManufacturedHome")]
        ManufacturedHome,
        
        [XmlEnum("MultiFamily")]
        MultiFamily,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("SingleFamily")]
        SingleFamily,
        
        [XmlEnum("TwoToFourFamily")]
        TwoToFourFamily,
        
        [XmlEnum("Vacant")]
        Vacant,
    }
}
