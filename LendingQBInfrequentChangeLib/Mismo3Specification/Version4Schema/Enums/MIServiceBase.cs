namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum MIServiceBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("ContractUnderwrite")]
        ContractUnderwrite,
        
        [XmlEnum("ContractUnderwriteWithMI")]
        ContractUnderwriteWithMI,
        
        [XmlEnum("EligibilityOnly")]
        EligibilityOnly,
        
        [XmlEnum("FullUnderwrite")]
        FullUnderwrite,
        
        [XmlEnum("MIApplication")]
        MIApplication,
        
        [XmlEnum("NotApplicable")]
        NotApplicable,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PostCloseDocuments")]
        PostCloseDocuments,
        
        [XmlEnum("PreQualification")]
        PreQualification,
        
        [XmlEnum("RateQuote")]
        RateQuote,
        
        [XmlEnum("RateQuoteAndEligibility")]
        RateQuoteAndEligibility,
        
        [XmlEnum("StatusQuery")]
        StatusQuery,
    }
}
