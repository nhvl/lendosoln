namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PayeeBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Appraiser")]
        Appraiser,
        
        [XmlEnum("Attorney")]
        Attorney,
        
        [XmlEnum("CityTaxAuthority")]
        CityTaxAuthority,
        
        [XmlEnum("CountyTaxAuthority")]
        CountyTaxAuthority,
        
        [XmlEnum("DocumentCustodian")]
        DocumentCustodian,
        
        [XmlEnum("HazardInsuranceAgent")]
        HazardInsuranceAgent,
        
        [XmlEnum("HazardInsuranceCompany")]
        HazardInsuranceCompany,
        
        [XmlEnum("MortgageInsuranceCompany")]
        MortgageInsuranceCompany,
        
        [XmlEnum("OptionalProductProvider")]
        OptionalProductProvider,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("OtherTaxAuthority")]
        OtherTaxAuthority,
        
        [XmlEnum("PoolCertificateHolder")]
        PoolCertificateHolder,
        
        [XmlEnum("PropertyInspectionService")]
        PropertyInspectionService,
        
        [XmlEnum("PropertyManagementCompany")]
        PropertyManagementCompany,
        
        [XmlEnum("RealEstateBroker")]
        RealEstateBroker,
        
        [XmlEnum("TaxServiceProvider")]
        TaxServiceProvider,
    }
}
