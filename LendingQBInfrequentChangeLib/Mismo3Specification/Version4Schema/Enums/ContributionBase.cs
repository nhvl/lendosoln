namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ContributionBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("HazardClaim")]
        HazardClaim,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Repair")]
        Repair,
    }
}
