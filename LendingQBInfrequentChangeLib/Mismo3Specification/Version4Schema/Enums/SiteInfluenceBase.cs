namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum SiteInfluenceBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Easements")]
        Easements,
        
        [XmlEnum("Encroachments")]
        Encroachments,
        
        [XmlEnum("EnvironmentalConditions")]
        EnvironmentalConditions,
        
        [XmlEnum("LandUses")]
        LandUses,
        
        [XmlEnum("Location")]
        Location,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("View")]
        View,
        
        [XmlEnum("WaterRights")]
        WaterRights,
    }
}
