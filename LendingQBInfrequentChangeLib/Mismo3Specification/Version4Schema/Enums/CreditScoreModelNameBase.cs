namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum CreditScoreModelNameBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Beacon09MortgageIndustryOption")]
        Beacon09MortgageIndustryOption,
        
        [XmlEnum("EquifaxBankruptcyNavigatorIndex02781")]
        EquifaxBankruptcyNavigatorIndex02781,
        
        [XmlEnum("EquifaxBankruptcyNavigatorIndex02782")]
        EquifaxBankruptcyNavigatorIndex02782,
        
        [XmlEnum("EquifaxBankruptcyNavigatorIndex02783")]
        EquifaxBankruptcyNavigatorIndex02783,
        
        [XmlEnum("EquifaxBankruptcyNavigatorIndex02784")]
        EquifaxBankruptcyNavigatorIndex02784,
        
        [XmlEnum("EquifaxBeacon")]
        EquifaxBeacon,
        
        [XmlEnum("EquifaxBeacon5.0")]
        EquifaxBeacon50,
        
        [XmlEnum("EquifaxBeacon5.0Auto")]
        EquifaxBeacon50Auto,
        
        [XmlEnum("EquifaxBeacon5.0BankCard")]
        EquifaxBeacon50BankCard,
        
        [XmlEnum("EquifaxBeacon5.0Installment")]
        EquifaxBeacon50Installment,
        
        [XmlEnum("EquifaxBeacon5.0PersonalFinance")]
        EquifaxBeacon50PersonalFinance,
        
        [XmlEnum("EquifaxBeaconAuto")]
        EquifaxBeaconAuto,
        
        [XmlEnum("EquifaxBeaconBankcard")]
        EquifaxBeaconBankcard,
        
        [XmlEnum("EquifaxBeaconInstallment")]
        EquifaxBeaconInstallment,
        
        [XmlEnum("EquifaxBeaconPersonalFinance")]
        EquifaxBeaconPersonalFinance,
        
        [XmlEnum("EquifaxDAS")]
        EquifaxDAS,
        
        [XmlEnum("EquifaxEnhancedBeacon")]
        EquifaxEnhancedBeacon,
        
        [XmlEnum("EquifaxEnhancedDAS")]
        EquifaxEnhancedDAS,
        
        [XmlEnum("EquifaxMarketMax")]
        EquifaxMarketMax,
        
        [XmlEnum("EquifaxMortgageScore")]
        EquifaxMortgageScore,
        
        [XmlEnum("EquifaxPinnacle")]
        EquifaxPinnacle,
        
        [XmlEnum("EquifaxPinnacle2.0")]
        EquifaxPinnacle20,
        
        [XmlEnum("EquifaxVantageScore")]
        EquifaxVantageScore,
        
        [XmlEnum("EquifaxVantageScore3.0")]
        EquifaxVantageScore30,
        
        [XmlEnum("ExperianFairIsaac")]
        ExperianFairIsaac,
        
        [XmlEnum("ExperianFairIsaacAdvanced")]
        ExperianFairIsaacAdvanced,
        
        [XmlEnum("ExperianFairIsaacAdvanced2.0")]
        ExperianFairIsaacAdvanced20,
        
        [XmlEnum("ExperianFairIsaacAuto")]
        ExperianFairIsaacAuto,
        
        [XmlEnum("ExperianFairIsaacBankcard")]
        ExperianFairIsaacBankcard,
        
        [XmlEnum("ExperianFairIsaacInstallment")]
        ExperianFairIsaacInstallment,
        
        [XmlEnum("ExperianFairIsaacPersonalFinance")]
        ExperianFairIsaacPersonalFinance,
        
        [XmlEnum("ExperianFICOClassicV3")]
        ExperianFICOClassicV3,
        
        [XmlEnum("ExperianMDSBankruptcyII")]
        ExperianMDSBankruptcyII,
        
        [XmlEnum("ExperianNewNationalEquivalency")]
        ExperianNewNationalEquivalency,
        
        [XmlEnum("ExperianNewNationalRisk")]
        ExperianNewNationalRisk,
        
        [XmlEnum("ExperianOldNationalRisk")]
        ExperianOldNationalRisk,
        
        [XmlEnum("ExperianScorexPLUS")]
        ExperianScorexPLUS,
        
        [XmlEnum("ExperianVantageScore")]
        ExperianVantageScore,
        
        [XmlEnum("ExperianVantageScore3.0")]
        ExperianVantageScore30,
        
        [XmlEnum("FICOExpansionScore")]
        FICOExpansionScore,
        
        [XmlEnum("FICORiskScoreClassic04")]
        FICORiskScoreClassic04,
        
        [XmlEnum("FICORiskScoreClassic98")]
        FICORiskScoreClassic98,
        
        [XmlEnum("FICORiskScoreClassicAuto98")]
        FICORiskScoreClassicAuto98,
        
        [XmlEnum("FICORiskScoreClassicBankcard98")]
        FICORiskScoreClassicBankcard98,
        
        [XmlEnum("FICORiskScoreClassicInstallmentLoan98")]
        FICORiskScoreClassicInstallmentLoan98,
        
        [XmlEnum("FICORiskScoreClassicPersonalFinance98")]
        FICORiskScoreClassicPersonalFinance98,
        
        [XmlEnum("FICORiskScoreNextGen00")]
        FICORiskScoreNextGen00,
        
        [XmlEnum("FICORiskScoreNextGen03")]
        FICORiskScoreNextGen03,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("TransUnionDelphi")]
        TransUnionDelphi,
        
        [XmlEnum("TransUnionEmpirica")]
        TransUnionEmpirica,
        
        [XmlEnum("TransUnionEmpiricaAuto")]
        TransUnionEmpiricaAuto,
        
        [XmlEnum("TransUnionEmpiricaBankcard")]
        TransUnionEmpiricaBankcard,
        
        [XmlEnum("TransUnionEmpiricaInstallment")]
        TransUnionEmpiricaInstallment,
        
        [XmlEnum("TransUnionEmpiricaPersonalFinance")]
        TransUnionEmpiricaPersonalFinance,
        
        [XmlEnum("TransUnionNewDelphi")]
        TransUnionNewDelphi,
        
        [XmlEnum("TransUnionPrecision")]
        TransUnionPrecision,
        
        [XmlEnum("TransUnionPrecision03")]
        TransUnionPrecision03,
        
        [XmlEnum("TransUnionVantageScore")]
        TransUnionVantageScore,
        
        [XmlEnum("TransUnionVantageScore3.0")]
        TransUnionVantageScore30,
    }
}
