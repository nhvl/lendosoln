namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum EnvironmentalConditionBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Asbestos")]
        Asbestos,
        
        [XmlEnum("EPAFinalSuperfundSiteProximate")]
        EPAFinalSuperfundSiteProximate,
        
        [XmlEnum("FormerDrugLab")]
        FormerDrugLab,
        
        [XmlEnum("LandfillProximate")]
        LandfillProximate,
        
        [XmlEnum("LeadBasedPaint")]
        LeadBasedPaint,
        
        [XmlEnum("LeakingUndergroundPetroleumStorageTank")]
        LeakingUndergroundPetroleumStorageTank,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("VaporIntrusion")]
        VaporIntrusion,
        
        [XmlEnum("WellWaterContamination")]
        WellWaterContamination,
    }
}
