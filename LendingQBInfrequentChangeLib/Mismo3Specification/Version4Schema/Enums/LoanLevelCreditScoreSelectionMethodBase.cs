namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum LoanLevelCreditScoreSelectionMethodBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AverageThenAverage")]
        AverageThenAverage,
        
        [XmlEnum("MiddleOrLowerThenAverage")]
        MiddleOrLowerThenAverage,
        
        [XmlEnum("MiddleOrLowerThenLowest")]
        MiddleOrLowerThenLowest,
        
        [XmlEnum("Other")]
        Other,
    }
}
