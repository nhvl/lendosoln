namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum LoanModificationBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AmortizationMethod")]
        AmortizationMethod,
        
        [XmlEnum("InterestRate")]
        InterestRate,
        
        [XmlEnum("MaturityDate")]
        MaturityDate,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PaymentAmount")]
        PaymentAmount,
        
        [XmlEnum("PaymentFrequency")]
        PaymentFrequency,
        
        [XmlEnum("PrincipalAmount")]
        PrincipalAmount,
    }
}
