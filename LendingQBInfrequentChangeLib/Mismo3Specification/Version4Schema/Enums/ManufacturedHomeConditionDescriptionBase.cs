namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ManufacturedHomeConditionDescriptionBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("New")]
        New,
        
        [XmlEnum("Used")]
        Used,
    }
}
