namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum WorkoutBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("DeedInLieu")]
        DeedInLieu,
        
        [XmlEnum("Modification")]
        Modification,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PaymentForbearance")]
        PaymentForbearance,
        
        [XmlEnum("PreclaimedAdvance")]
        PreclaimedAdvance,
        
        [XmlEnum("PrincipalForbearance")]
        PrincipalForbearance,
        
        [XmlEnum("Refinance")]
        Refinance,
        
        [XmlEnum("RepaymentPlan")]
        RepaymentPlan,
        
        [XmlEnum("ShortSale")]
        ShortSale,
    }
}
