namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum QualifiedMortgageBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("BalloonPayment")]
        BalloonPayment,
        
        [XmlEnum("NotApplicable")]
        NotApplicable,
        
        [XmlEnum("NotAQualifiedMortgage")]
        NotAQualifiedMortgage,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("SmallCreditor")]
        SmallCreditor,
        
        [XmlEnum("SmallCreditorRural")]
        SmallCreditorRural,
        
        [XmlEnum("Standard")]
        Standard,
        
        [XmlEnum("Temporary")]
        Temporary,
        
        [XmlEnum("TemporaryAgencyGSE")]
        TemporaryAgencyGSE,
        
        [XmlEnum("TemporarySmallCreditorBalloon")]
        TemporarySmallCreditorBalloon,
    }
}
