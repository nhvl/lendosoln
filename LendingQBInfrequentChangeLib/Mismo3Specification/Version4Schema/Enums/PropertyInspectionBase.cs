namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PropertyInspectionBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("ExteriorAndInterior")]
        ExteriorAndInterior,
        
        [XmlEnum("ExteriorOnly")]
        ExteriorOnly,
        
        [XmlEnum("None")]
        None,
        
        [XmlEnum("Other")]
        Other,
    }
}
