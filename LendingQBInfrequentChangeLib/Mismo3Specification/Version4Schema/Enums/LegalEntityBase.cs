namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum LegalEntityBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Corporation")]
        Corporation,
        
        [XmlEnum("CorporationSole")]
        CorporationSole,
        
        [XmlEnum("Estate")]
        Estate,
        
        [XmlEnum("GovernmentEntity")]
        GovernmentEntity,
        
        [XmlEnum("JointVenture")]
        JointVenture,
        
        [XmlEnum("LimitedLiabilityCompany")]
        LimitedLiabilityCompany,
        
        [XmlEnum("LimitedPartnership")]
        LimitedPartnership,
        
        [XmlEnum("NonProfitCorporation")]
        NonProfitCorporation,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Partnership")]
        Partnership,
        
        [XmlEnum("SoleProprietorship")]
        SoleProprietorship,
        
        [XmlEnum("Trust")]
        Trust,
    }
}
