namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum HMDAReasonForDenialBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("CreditApplicationIncomplete")]
        CreditApplicationIncomplete,
        
        [XmlEnum("InsufficientCash")]
        InsufficientCash,
        
        [XmlEnum("InsufficientCollateralValue")]
        InsufficientCollateralValue,
        
        [XmlEnum("InsufficientCreditHistory")]
        InsufficientCreditHistory,
        
        [XmlEnum("InsufficientEmploymentHistory")]
        InsufficientEmploymentHistory,
        
        [XmlEnum("InsufficientIncome")]
        InsufficientIncome,
        
        [XmlEnum("MortgageInsuranceDenied")]
        MortgageInsuranceDenied,
        
        [XmlEnum("NotApplicable")]
        NotApplicable,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("UnverifiableInformation")]
        UnverifiableInformation,
    }
}
