namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum TransferRequestBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AllLoans")]
        AllLoans,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("SpecificLoans")]
        SpecificLoans,
    }
}
