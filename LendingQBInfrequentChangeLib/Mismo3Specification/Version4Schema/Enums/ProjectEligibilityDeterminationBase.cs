namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ProjectEligibilityDeterminationBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("FullReview")]
        FullReview,
        
        [XmlEnum("LimitedReview")]
        LimitedReview,
        
        [XmlEnum("ProjectEligibilityReviewService")]
        ProjectEligibilityReviewService,
    }
}
