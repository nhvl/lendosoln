namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum BuildingPermitUsageStandardBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Commercial")]
        Commercial,
        
        [XmlEnum("Hazardous")]
        Hazardous,
        
        [XmlEnum("Industrial")]
        Industrial,
        
        [XmlEnum("MultiFamily")]
        MultiFamily,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("SingleFamily")]
        SingleFamily,
    }
}
