namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum RelatedLoanInvestorBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("FNM")]
        FNM,
        
        [XmlEnum("FRE")]
        FRE,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Seller")]
        Seller,
        
        [XmlEnum("Unknown")]
        Unknown,
    }
}
