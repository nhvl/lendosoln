namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum SignatureBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Digital")]
        Digital,
        
        [XmlEnum("Image")]
        Image,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Text")]
        Text,
        
        [XmlEnum("Wet")]
        Wet,
    }
}
