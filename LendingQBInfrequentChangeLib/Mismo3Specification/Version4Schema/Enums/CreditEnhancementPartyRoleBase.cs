namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum CreditEnhancementPartyRoleBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Investor")]
        Investor,
        
        [XmlEnum("Lender")]
        Lender,
        
        [XmlEnum("LoanSeller")]
        LoanSeller,
        
        [XmlEnum("MICompany")]
        MICompany,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Servicer")]
        Servicer,
        
        [XmlEnum("Shared")]
        Shared,
        
        [XmlEnum("Unknown")]
        Unknown,
    }
}
