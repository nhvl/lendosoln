namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum IndexLookbackBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("FirstMondayOfTheMonth")]
        FirstMondayOfTheMonth,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("SecondTuesdayOfTheMonth")]
        SecondTuesdayOfTheMonth,
        
        [XmlEnum("SpecificDayOfTheMonth")]
        SpecificDayOfTheMonth,
        
        [XmlEnum("ThirdFridayOfTheMonth")]
        ThirdFridayOfTheMonth,
    }
}
