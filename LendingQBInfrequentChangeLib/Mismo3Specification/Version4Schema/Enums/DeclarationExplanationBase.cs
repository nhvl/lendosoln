namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum DeclarationExplanationBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AlimonyChildSupport")]
        AlimonyChildSupport,
        
        [XmlEnum("BorrowedDownPayment")]
        BorrowedDownPayment,
        
        [XmlEnum("CoMakerEndorserOnNote")]
        CoMakerEndorserOnNote,
        
        [XmlEnum("DeclaredBankruptcyPastSevenYears")]
        DeclaredBankruptcyPastSevenYears,
        
        [XmlEnum("DelinquencyOrDefault")]
        DelinquencyOrDefault,
        
        [XmlEnum("DirectIndirectForeclosedPropertyPastSevenYears")]
        DirectIndirectForeclosedPropertyPastSevenYears,
        
        [XmlEnum("ObligatedOnLoanForeclosedOrDeedInLieuOfJudgment")]
        ObligatedOnLoanForeclosedOrDeedInLieuOfJudgment,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("OutstandingJudgments")]
        OutstandingJudgments,
        
        [XmlEnum("PartyToLawsuit")]
        PartyToLawsuit,
    }
}
