namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum LoanUnderwritingSubmitterBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Broker")]
        Broker,
        
        [XmlEnum("Correspondent")]
        Correspondent,
        
        [XmlEnum("LenderOtherThanSeller")]
        LenderOtherThanSeller,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Seller")]
        Seller,
    }
}
