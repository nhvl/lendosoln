namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PrepaymentPenaltyPrincipalBalanceBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("CurrentPrincipalBalance")]
        CurrentPrincipalBalance,
        
        [XmlEnum("ExcessPrincipal")]
        ExcessPrincipal,
        
        [XmlEnum("OriginalPrincipalBalance")]
        OriginalPrincipalBalance,
        
        [XmlEnum("Other")]
        Other,
    }
}
