namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum LoanPriceLineItemBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AlternatePropertyValuation")]
        AlternatePropertyValuation,
        
        [XmlEnum("BalloonLoan")]
        BalloonLoan,
        
        [XmlEnum("CashOutRefinance")]
        CashOutRefinance,
        
        [XmlEnum("HighCreditRisk")]
        HighCreditRisk,
        
        [XmlEnum("HighLTVLoan")]
        HighLTVLoan,
        
        [XmlEnum("InsufficientFloodInsurance")]
        InsufficientFloodInsurance,
        
        [XmlEnum("InterestRateDifferential")]
        InterestRateDifferential,
        
        [XmlEnum("InvestmentProperty")]
        InvestmentProperty,
        
        [XmlEnum("LoanWithSubordinateFinancing")]
        LoanWithSubordinateFinancing,
        
        [XmlEnum("LockPeriod")]
        LockPeriod,
        
        [XmlEnum("MarginDifferential")]
        MarginDifferential,
        
        [XmlEnum("ModifiableLoan")]
        ModifiableLoan,
        
        [XmlEnum("MultipleDwellingUnitProperty")]
        MultipleDwellingUnitProperty,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("ReducedDocumentation")]
        ReducedDocumentation,
        
        [XmlEnum("SeasonedLoan")]
        SeasonedLoan,
        
        [XmlEnum("SecondLien")]
        SecondLien,
        
        [XmlEnum("ServiceReleasePremium")]
        ServiceReleasePremium,
        
        [XmlEnum("StreamlinedOrigination")]
        StreamlinedOrigination,
        
        [XmlEnum("TexasEquityRefinance")]
        TexasEquityRefinance,
        
        [XmlEnum("VeryHighLTVLoan")]
        VeryHighLTVLoan,
    }
}
