namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum InsulationMaterialBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Cellulose")]
        Cellulose,
        
        [XmlEnum("FiberglassBatt")]
        FiberglassBatt,
        
        [XmlEnum("FiberglassBlownIn")]
        FiberglassBlownIn,
        
        [XmlEnum("Foam")]
        Foam,
        
        [XmlEnum("MineralWool")]
        MineralWool,
        
        [XmlEnum("Other")]
        Other,
    }
}
