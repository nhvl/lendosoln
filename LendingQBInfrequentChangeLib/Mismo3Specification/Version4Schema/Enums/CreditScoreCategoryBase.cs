namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum CreditScoreCategoryBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Bankruptcy")]
        Bankruptcy,
        
        [XmlEnum("CreditRepository")]
        CreditRepository,
        
        [XmlEnum("FICO")]
        FICO,
        
        [XmlEnum("FICONextGen")]
        FICONextGen,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("ThinFile")]
        ThinFile,
        
        [XmlEnum("Vantage")]
        Vantage,
    }
}
