namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum FoundationFeatureBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("SumpPump")]
        SumpPump,
    }
}
