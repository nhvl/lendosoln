namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum VAAppraisalBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("HUDConversion")]
        HUDConversion,
        
        [XmlEnum("LenderAppraisal")]
        LenderAppraisal,
        
        [XmlEnum("ManufacturedHome")]
        ManufacturedHome,
        
        [XmlEnum("MasterCertificateOfReasonableValueCase")]
        MasterCertificateOfReasonableValueCase,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PropertyManagementCase")]
        PropertyManagementCase,
        
        [XmlEnum("SingleProperty")]
        SingleProperty,
    }
}
