namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum IntegratedDisclosureSubsectionPaidByBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Buyer")]
        Buyer,
        
        [XmlEnum("Seller")]
        Seller,
        
        [XmlEnum("ThirdParty")]
        ThirdParty,
    }
}
