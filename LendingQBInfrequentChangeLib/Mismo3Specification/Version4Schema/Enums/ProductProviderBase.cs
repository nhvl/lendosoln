namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ProductProviderBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Investor")]
        Investor,
        
        [XmlEnum("Lender")]
        Lender,
        
        [XmlEnum("Other")]
        Other,
    }
}
