namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum BuildingPermitStatusBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Applied")]
        Applied,
        
        [XmlEnum("Cancelled")]
        Cancelled,
        
        [XmlEnum("Completed")]
        Completed,
        
        [XmlEnum("Expired")]
        Expired,
        
        [XmlEnum("Issued")]
        Issued,
        
        [XmlEnum("OnHold")]
        OnHold,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Unknown")]
        Unknown,
        
        [XmlEnum("Voided")]
        Voided,
    }
}
