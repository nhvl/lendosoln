namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum LicenseAuthorityLevelBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Private")]
        Private,
        
        [XmlEnum("PublicFederal")]
        PublicFederal,
        
        [XmlEnum("PublicLocal")]
        PublicLocal,
        
        [XmlEnum("PublicState")]
        PublicState,
    }
}
