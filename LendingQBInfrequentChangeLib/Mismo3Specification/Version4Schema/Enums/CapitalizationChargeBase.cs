namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum CapitalizationChargeBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AttorneyFees")]
        AttorneyFees,
        
        [XmlEnum("DelinquentHomeownersFees")]
        DelinquentHomeownersFees,
        
        [XmlEnum("DelinquentInterest")]
        DelinquentInterest,
        
        [XmlEnum("EscrowShortages")]
        EscrowShortages,
        
        [XmlEnum("LateCharge")]
        LateCharge,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("UnreportedInterest")]
        UnreportedInterest,
    }
}
