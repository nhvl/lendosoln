namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum RegulatoryProductSourceBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("OFAC")]
        OFAC,
        
        [XmlEnum("Other")]
        Other,
    }
}
