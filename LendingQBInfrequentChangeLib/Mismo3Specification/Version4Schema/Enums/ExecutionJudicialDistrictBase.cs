namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ExecutionJudicialDistrictBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Borough")]
        Borough,
        
        [XmlEnum("City")]
        City,
        
        [XmlEnum("County")]
        County,
        
        [XmlEnum("District")]
        District,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Parish")]
        Parish,
        
        [XmlEnum("Town")]
        Town,
    }
}
