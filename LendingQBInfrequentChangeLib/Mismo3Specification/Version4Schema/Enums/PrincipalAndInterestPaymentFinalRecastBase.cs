namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PrincipalAndInterestPaymentFinalRecastBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("None")]
        None,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("RecastOnceOnFinalPaymentChangeDate")]
        RecastOnceOnFinalPaymentChangeDate,
        
        [XmlEnum("RecastOnFinalPaymentChangeDateAndEveryMonthThereafter")]
        RecastOnFinalPaymentChangeDateAndEveryMonthThereafter,
    }
}
