namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ArrearageComponentBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("HomeownerAssociationFees")]
        HomeownerAssociationFees,
        
        [XmlEnum("HomeownersInsurance")]
        HomeownersInsurance,
        
        [XmlEnum("LateFees")]
        LateFees,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PrincipalAndInterest")]
        PrincipalAndInterest,
        
        [XmlEnum("Taxes")]
        Taxes,
    }
}
