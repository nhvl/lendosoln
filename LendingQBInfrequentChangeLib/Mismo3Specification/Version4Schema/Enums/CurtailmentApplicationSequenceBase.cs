namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum CurtailmentApplicationSequenceBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("CurtailmentFirst")]
        CurtailmentFirst,
        
        [XmlEnum("CurtailmentLast")]
        CurtailmentLast,
    }
}
