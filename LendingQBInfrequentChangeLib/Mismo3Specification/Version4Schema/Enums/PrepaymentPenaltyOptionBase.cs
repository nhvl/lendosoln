namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PrepaymentPenaltyOptionBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Hard")]
        Hard,
        
        [XmlEnum("Soft")]
        Soft,
    }
}
