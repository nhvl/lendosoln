namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum LandTrustBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("CommunityLandTrust")]
        CommunityLandTrust,
        
        [XmlEnum("IllinoisLandTrust")]
        IllinoisLandTrust,
        
        [XmlEnum("LandTrust")]
        LandTrust,
        
        [XmlEnum("Other")]
        Other,
    }
}
