namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum NotaryCertificateSignerIdentificationBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("PersonallyKnown")]
        PersonallyKnown,
        
        [XmlEnum("ProvidedIdentification")]
        ProvidedIdentification,
    }
}
