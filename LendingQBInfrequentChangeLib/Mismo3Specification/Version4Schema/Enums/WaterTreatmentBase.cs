namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum WaterTreatmentBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Purifier")]
        Purifier,
        
        [XmlEnum("Softener")]
        Softener,
    }
}
