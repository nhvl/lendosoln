namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ObjectEncodingBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Base64")]
        Base64,
        
        [XmlEnum("DeflateBase64")]
        DeflateBase64,
        
        [XmlEnum("EscapedXML")]
        EscapedXML,
        
        [XmlEnum("GzipBase64")]
        GzipBase64,
        
        [XmlEnum("None")]
        None,
        
        [XmlEnum("Other")]
        Other,
    }
}
