namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ManufacturedHomeSectionBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("SectionFour")]
        SectionFour,
        
        [XmlEnum("SectionOne")]
        SectionOne,
        
        [XmlEnum("SectionThree")]
        SectionThree,
        
        [XmlEnum("SectionTwo")]
        SectionTwo,
    }
}
