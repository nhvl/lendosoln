namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum NameDocumentsDrawnInBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Broker")]
        Broker,
        
        [XmlEnum("Investor")]
        Investor,
        
        [XmlEnum("Lender")]
        Lender,
        
        [XmlEnum("Other")]
        Other,
    }
}
