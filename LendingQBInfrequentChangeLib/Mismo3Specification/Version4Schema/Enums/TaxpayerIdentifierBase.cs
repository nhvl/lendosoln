namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum TaxpayerIdentifierBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("EmployerIdentificationNumber")]
        EmployerIdentificationNumber,
        
        [XmlEnum("IndividualTaxpayerIdentificationNumber")]
        IndividualTaxpayerIdentificationNumber,
        
        [XmlEnum("PreparerTaxpayerIdentificationNumber")]
        PreparerTaxpayerIdentificationNumber,
        
        [XmlEnum("SocialSecurityNumber")]
        SocialSecurityNumber,
        
        [XmlEnum("TaxpayerIdentificationNumberForPendingUSAdoptions")]
        TaxpayerIdentificationNumberForPendingUSAdoptions,
    }
}
