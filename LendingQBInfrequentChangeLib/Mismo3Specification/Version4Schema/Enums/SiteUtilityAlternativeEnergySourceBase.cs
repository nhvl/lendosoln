namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum SiteUtilityAlternativeEnergySourceBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Biomass")]
        Biomass,
        
        [XmlEnum("Ethanol")]
        Ethanol,
        
        [XmlEnum("Geothermal")]
        Geothermal,
        
        [XmlEnum("Hydrogen")]
        Hydrogen,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Propane")]
        Propane,
        
        [XmlEnum("RecycledOils")]
        RecycledOils,
        
        [XmlEnum("Solar")]
        Solar,
        
        [XmlEnum("Wind")]
        Wind,
    }
}
