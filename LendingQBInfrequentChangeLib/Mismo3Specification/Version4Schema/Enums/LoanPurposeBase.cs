namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum LoanPurposeBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("MortgageModification")]
        MortgageModification,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Purchase")]
        Purchase,
        
        [XmlEnum("Refinance")]
        Refinance,
        
        [XmlEnum("Unknown")]
        Unknown,
    }
}
