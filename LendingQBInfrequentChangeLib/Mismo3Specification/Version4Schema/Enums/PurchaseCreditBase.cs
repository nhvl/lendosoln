namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PurchaseCreditBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("BuydownFund")]
        BuydownFund,
        
        [XmlEnum("CommitmentOriginationFee")]
        CommitmentOriginationFee,
        
        [XmlEnum("EarnestMoney")]
        EarnestMoney,
        
        [XmlEnum("EmployerAssistedHousing")]
        EmployerAssistedHousing,
        
        [XmlEnum("FederalAgencyFundingFeeRefund")]
        FederalAgencyFundingFeeRefund,
        
        [XmlEnum("GiftOfEquity")]
        GiftOfEquity,
        
        [XmlEnum("LeasePurchaseFund")]
        LeasePurchaseFund,
        
        [XmlEnum("MIPremiumRefund")]
        MIPremiumRefund,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("RelocationFunds")]
        RelocationFunds,
        
        [XmlEnum("SweatEquity")]
        SweatEquity,
        
        [XmlEnum("TradeEquity")]
        TradeEquity,
        
        [XmlEnum("TradeEquityFromPropertySwap")]
        TradeEquityFromPropertySwap,
    }
}
