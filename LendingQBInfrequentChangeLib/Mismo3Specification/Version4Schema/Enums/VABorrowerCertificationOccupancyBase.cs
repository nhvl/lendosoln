namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum VABorrowerCertificationOccupancyBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("A")]
        A,
        
        [XmlEnum("B")]
        B,
        
        [XmlEnum("C")]
        C,
        
        [XmlEnum("D")]
        D,
    }
}
