namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum DealSetExpenseBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Shipping")]
        Shipping,
        
        [XmlEnum("Special")]
        Special,
        
        [XmlEnum("Travel")]
        Travel,
    }
}
