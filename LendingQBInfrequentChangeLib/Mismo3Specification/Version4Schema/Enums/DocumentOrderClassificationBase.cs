namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum DocumentOrderClassificationBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Final")]
        Final,
        
        [XmlEnum("Preliminary")]
        Preliminary,
    }
}
