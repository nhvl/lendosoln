namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PrimaryMIAbsenceReasonBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("CharterParticipation")]
        CharterParticipation,
        
        [XmlEnum("DelinquencyDisposition")]
        DelinquencyDisposition,
        
        [XmlEnum("InvestorPurchasedMortgageInsurance")]
        InvestorPurchasedMortgageInsurance,
        
        [XmlEnum("MICanceledBasedOnCurrentLTV")]
        MICanceledBasedOnCurrentLTV,
        
        [XmlEnum("NoMIBasedOnOriginalLTV")]
        NoMIBasedOnOriginalLTV,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PledgedAsset")]
        PledgedAsset,
        
        [XmlEnum("PoolCoverage")]
        PoolCoverage,
        
        [XmlEnum("Repurchase")]
        Repurchase,
        
        [XmlEnum("SelfInsured")]
        SelfInsured,
    }
}
