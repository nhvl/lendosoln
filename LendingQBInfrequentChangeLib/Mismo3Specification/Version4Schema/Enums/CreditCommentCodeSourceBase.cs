namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum CreditCommentCodeSourceBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("EquifaxBankruptcy")]
        EquifaxBankruptcy,
        
        [XmlEnum("EquifaxCollectionStatus")]
        EquifaxCollectionStatus,
        
        [XmlEnum("EquifaxLegalStatus")]
        EquifaxLegalStatus,
        
        [XmlEnum("EquifaxNarrative")]
        EquifaxNarrative,
        
        [XmlEnum("ExperianAccountPurpose")]
        ExperianAccountPurpose,
        
        [XmlEnum("ExperianAccountStatus")]
        ExperianAccountStatus,
        
        [XmlEnum("ExperianLegalStatus")]
        ExperianLegalStatus,
        
        [XmlEnum("ExperianSpecialComment")]
        ExperianSpecialComment,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("TransUnionLoanType")]
        TransUnionLoanType,
        
        [XmlEnum("TransUnionPublicRecordType")]
        TransUnionPublicRecordType,
        
        [XmlEnum("TransUnionRemarks")]
        TransUnionRemarks,
    }
}
