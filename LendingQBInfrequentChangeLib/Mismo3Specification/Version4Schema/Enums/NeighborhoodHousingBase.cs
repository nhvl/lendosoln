namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum NeighborhoodHousingBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Condominium")]
        Condominium,
        
        [XmlEnum("Cooperative")]
        Cooperative,
        
        [XmlEnum("ManufacturedHome")]
        ManufacturedHome,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("SingleFamily")]
        SingleFamily,
        
        [XmlEnum("TwoToFourFamily")]
        TwoToFourFamily,
    }
}
