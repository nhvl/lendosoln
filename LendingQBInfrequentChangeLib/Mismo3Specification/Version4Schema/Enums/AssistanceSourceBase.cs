namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum AssistanceSourceBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Investor")]
        Investor,
        
        [XmlEnum("Other")]
        Other,
    }
}
