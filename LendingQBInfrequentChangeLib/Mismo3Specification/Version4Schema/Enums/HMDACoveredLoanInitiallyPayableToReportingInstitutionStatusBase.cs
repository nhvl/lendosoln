namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum HMDACoveredLoanInitiallyPayableToReportingInstitutionStatusBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("InitiallyPayable")]
        InitiallyPayable,
        
        [XmlEnum("NotApplicable")]
        NotApplicable,
        
        [XmlEnum("NotInitiallyPayable")]
        NotInitiallyPayable,
    }
}
