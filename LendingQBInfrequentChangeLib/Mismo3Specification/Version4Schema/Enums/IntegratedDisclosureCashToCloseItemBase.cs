namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum IntegratedDisclosureCashToCloseItemBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AdjustmentsAndOtherCredits")]
        AdjustmentsAndOtherCredits,
        
        [XmlEnum("CashToCloseTotal")]
        CashToCloseTotal,
        
        [XmlEnum("ClosingCostsFinanced")]
        ClosingCostsFinanced,
        
        [XmlEnum("ClosingCostsPaidBeforeClosing")]
        ClosingCostsPaidBeforeClosing,
        
        [XmlEnum("Deposit")]
        Deposit,
        
        [XmlEnum("DownPayment")]
        DownPayment,
        
        [XmlEnum("FundsForBorrower")]
        FundsForBorrower,
        
        [XmlEnum("FundsFromBorrower")]
        FundsFromBorrower,
        
        [XmlEnum("LenderCredits")]
        LenderCredits,
        
        [XmlEnum("LoanAmount")]
        LoanAmount,
        
        [XmlEnum("LoanCostsAndOtherCostsTotal")]
        LoanCostsAndOtherCostsTotal,
        
        [XmlEnum("SellerCredits")]
        SellerCredits,
        
        [XmlEnum("TotalClosingCosts")]
        TotalClosingCosts,
        
        [XmlEnum("TotalPayoffsAndPayments")]
        TotalPayoffsAndPayments,
    }
}
