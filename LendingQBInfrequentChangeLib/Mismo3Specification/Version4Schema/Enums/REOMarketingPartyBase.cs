namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum REOMarketingPartyBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Investor")]
        Investor,
        
        [XmlEnum("Lender")]
        Lender,
        
        [XmlEnum("MortgageInsuranceCompany")]
        MortgageInsuranceCompany,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Seller")]
        Seller,
        
        [XmlEnum("Servicer")]
        Servicer,
        
        [XmlEnum("Unknown")]
        Unknown,
    }
}
