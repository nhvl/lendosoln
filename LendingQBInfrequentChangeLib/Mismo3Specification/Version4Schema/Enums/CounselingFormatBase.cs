namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum CounselingFormatBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("FaceToFace")]
        FaceToFace,
        
        [XmlEnum("Internet")]
        Internet,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Telephone")]
        Telephone,
    }
}
