namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum SectionClassificationBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("100")]
        Item100,
        
        [XmlEnum("1000")]
        Item1000,
        
        [XmlEnum("1100")]
        Item1100,
        
        [XmlEnum("1200")]
        Item1200,
        
        [XmlEnum("1300")]
        Item1300,
        
        [XmlEnum("200")]
        Item200,
        
        [XmlEnum("300")]
        Item300,
        
        [XmlEnum("400")]
        Item400,
        
        [XmlEnum("500")]
        Item500,
        
        [XmlEnum("600")]
        Item600,
        
        [XmlEnum("700")]
        Item700,
        
        [XmlEnum("800")]
        Item800,
        
        [XmlEnum("900")]
        Item900,
    }
}
