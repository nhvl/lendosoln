namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum MICoveragePlanBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("LossLimitCap")]
        LossLimitCap,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Pool")]
        Pool,
        
        [XmlEnum("RiskSharing")]
        RiskSharing,
        
        [XmlEnum("SecondLayer")]
        SecondLayer,
        
        [XmlEnum("StandardPrimary")]
        StandardPrimary,
    }
}
