namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum DeliveryReceivedStatusBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Accepted")]
        Accepted,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PresumedReceived")]
        PresumedReceived,
        
        [XmlEnum("Rejected")]
        Rejected,
    }
}
