namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum CertificateStatusBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Approved")]
        Approved,
        
        [XmlEnum("Cancelled")]
        Cancelled,
        
        [XmlEnum("Denied")]
        Denied,
        
        [XmlEnum("Expired")]
        Expired,
        
        [XmlEnum("InApplication")]
        InApplication,
        
        [XmlEnum("Other")]
        Other,
    }
}
