namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum UnitOfMeasureBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Acres")]
        Acres,
        
        [XmlEnum("Hectares")]
        Hectares,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("SquareFeet")]
        SquareFeet,
        
        [XmlEnum("SquareMeters")]
        SquareMeters,
    }
}
