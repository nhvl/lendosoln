namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum MortgageScoreBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("FraudFilterScore")]
        FraudFilterScore,
        
        [XmlEnum("GE_IQScore")]
        GE_IQScore,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PMIAuraAQIScore")]
        PMIAuraAQIScore,
        
        [XmlEnum("UGIAccuscore")]
        UGIAccuscore,
    }
}
