namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum InvestorReportingActionBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Active")]
        Active,
        
        [XmlEnum("AssignedToFHAOrVA")]
        AssignedToFHAOrVA,
        
        [XmlEnum("DeedInLieuLiquidated")]
        DeedInLieuLiquidated,
        
        [XmlEnum("ExerciseSkipPay")]
        ExerciseSkipPay,
        
        [XmlEnum("ForeclosureLiquidatedHeldForSale")]
        ForeclosureLiquidatedHeldForSale,
        
        [XmlEnum("ForeclosureLiquidatedPendingConveyance")]
        ForeclosureLiquidatedPendingConveyance,
        
        [XmlEnum("ForeclosureLiquidatedThirdPartySale")]
        ForeclosureLiquidatedThirdPartySale,
        
        [XmlEnum("ForeclosureSaleUnsuccessful")]
        ForeclosureSaleUnsuccessful,
        
        [XmlEnum("Inactive")]
        Inactive,
        
        [XmlEnum("LiquidatedHeldForSale")]
        LiquidatedHeldForSale,
        
        [XmlEnum("MICancellationAutomaticTermination")]
        MICancellationAutomaticTermination,
        
        [XmlEnum("MICancellationBasedOnCurrentPropertyValue")]
        MICancellationBasedOnCurrentPropertyValue,
        
        [XmlEnum("MICancellationBasedOnMandatoryTermination")]
        MICancellationBasedOnMandatoryTermination,
        
        [XmlEnum("MICancellationBasedOnOriginalPropertyValue")]
        MICancellationBasedOnOriginalPropertyValue,
        
        [XmlEnum("MIRescindedByInsurer")]
        MIRescindedByInsurer,
        
        [XmlEnum("NoServicerActionTaken")]
        NoServicerActionTaken,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PaidInFullAtMaturity")]
        PaidInFullAtMaturity,
        
        [XmlEnum("PaidInFullPriorToMaturity")]
        PaidInFullPriorToMaturity,
        
        [XmlEnum("Payoff")]
        Payoff,
        
        [XmlEnum("PayoffShortSale")]
        PayoffShortSale,
        
        [XmlEnum("PrincipalBalanceCorrection")]
        PrincipalBalanceCorrection,
        
        [XmlEnum("Repurchase")]
        Repurchase,
        
        [XmlEnum("RepurchaseDueToARMConversionToFixedRate")]
        RepurchaseDueToARMConversionToFixedRate,
        
        [XmlEnum("RepurchaseDueToModifiedARM")]
        RepurchaseDueToModifiedARM,
        
        [XmlEnum("Supplemental")]
        Supplemental,
        
        [XmlEnum("TransferToREO")]
        TransferToREO,
    }
}
