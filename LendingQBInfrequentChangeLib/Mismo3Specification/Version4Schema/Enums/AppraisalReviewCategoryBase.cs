namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum AppraisalReviewCategoryBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("ComparableRentals")]
        ComparableRentals,
        
        [XmlEnum("ComparablesSimilar")]
        ComparablesSimilar,
        
        [XmlEnum("Contract")]
        Contract,
        
        [XmlEnum("CostAndIncome")]
        CostAndIncome,
        
        [XmlEnum("DataAnalysis")]
        DataAnalysis,
        
        [XmlEnum("Improvements")]
        Improvements,
        
        [XmlEnum("MarketRent")]
        MarketRent,
        
        [XmlEnum("Neighborhood")]
        Neighborhood,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("RentSchedule")]
        RentSchedule,
        
        [XmlEnum("Site")]
        Site,
        
        [XmlEnum("Subject")]
        Subject,
        
        [XmlEnum("TransferHistory")]
        TransferHistory,
        
        [XmlEnum("ValueIndicators")]
        ValueIndicators,
    }
}
