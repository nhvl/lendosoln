namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PrincipalBalanceCalculationMethodBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("CurrentPrincipalBalance")]
        CurrentPrincipalBalance,
        
        [XmlEnum("ProjectedPrincipalBalance")]
        ProjectedPrincipalBalance,
    }
}
