namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum MIPremiumRefundableBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("NotRefundable")]
        NotRefundable,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Refundable")]
        Refundable,
        
        [XmlEnum("RefundableWithLimits")]
        RefundableWithLimits,
    }
}
