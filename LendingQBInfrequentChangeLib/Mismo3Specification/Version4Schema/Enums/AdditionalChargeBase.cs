namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum AdditionalChargeBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("LatePayment")]
        LatePayment,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PrepaymentPenalty")]
        PrepaymentPenalty,
        
        [XmlEnum("RealizedLossDueToForeclosedREOPropertyLiquidation")]
        RealizedLossDueToForeclosedREOPropertyLiquidation,
        
        [XmlEnum("RealizedLossDueToLoanModification")]
        RealizedLossDueToLoanModification,
        
        [XmlEnum("SkipPayment")]
        SkipPayment,
    }
}
