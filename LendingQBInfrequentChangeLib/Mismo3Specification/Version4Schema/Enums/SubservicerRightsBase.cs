namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum SubservicerRightsBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AllLoans")]
        AllLoans,
        
        [XmlEnum("OneLoan")]
        OneLoan,
        
        [XmlEnum("OneRemittance")]
        OneRemittance,
        
        [XmlEnum("OneSecurity")]
        OneSecurity,
        
        [XmlEnum("Other")]
        Other,
    }
}
