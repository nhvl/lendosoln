namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum CreditReportPriceBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Net")]
        Net,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Tax")]
        Tax,
        
        [XmlEnum("Total")]
        Total,
    }
}
