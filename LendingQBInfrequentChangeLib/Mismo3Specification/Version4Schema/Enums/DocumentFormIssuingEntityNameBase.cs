namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum DocumentFormIssuingEntityNameBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("CFPB")]
        CFPB,
        
        [XmlEnum("FEMA")]
        FEMA,
        
        [XmlEnum("FHA")]
        FHA,
        
        [XmlEnum("FNM")]
        FNM,
        
        [XmlEnum("FNM_FRE")]
        FNM_FRE,
        
        [XmlEnum("FRE")]
        FRE,
        
        [XmlEnum("HUD")]
        HUD,
        
        [XmlEnum("IRS")]
        IRS,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("VA")]
        VA,
    }
}
