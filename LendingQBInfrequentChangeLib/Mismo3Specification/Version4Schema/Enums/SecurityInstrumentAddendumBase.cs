namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum SecurityInstrumentAddendumBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("FixedRateOption")]
        FixedRateOption,
        
        [XmlEnum("Other")]
        Other,
    }
}
