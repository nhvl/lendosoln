namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum FeePercentBasisBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("BaseLoanAmount")]
        BaseLoanAmount,
        
        [XmlEnum("OriginalLoanAmount")]
        OriginalLoanAmount,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PropertyAppraisedValueAmount")]
        PropertyAppraisedValueAmount,
        
        [XmlEnum("PurchasePriceAmount")]
        PurchasePriceAmount,
    }
}
