namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum DocumentRecordationProcessingBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("DeliveryRequired")]
        DeliveryRequired,
        
        [XmlEnum("NoProcessingRequired")]
        NoProcessingRequired,
        
        [XmlEnum("RecordationRequired")]
        RecordationRequired,
    }
}
