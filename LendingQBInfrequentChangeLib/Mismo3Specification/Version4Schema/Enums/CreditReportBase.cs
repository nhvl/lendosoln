namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum CreditReportBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Error")]
        Error,
        
        [XmlEnum("LoanQuality")]
        LoanQuality,
        
        [XmlEnum("LoanQualityCompare")]
        LoanQualityCompare,
        
        [XmlEnum("Merge")]
        Merge,
        
        [XmlEnum("MergePlus")]
        MergePlus,
        
        [XmlEnum("MortgageOnly")]
        MortgageOnly,
        
        [XmlEnum("NonTraditional")]
        NonTraditional,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("RiskScoresOnly")]
        RiskScoresOnly,
        
        [XmlEnum("RMCR")]
        RMCR,
        
        [XmlEnum("RMCRForeign")]
        RMCRForeign,
        
        [XmlEnum("Status")]
        Status,
    }
}
