namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum AssetBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Annuity")]
        Annuity,
        
        [XmlEnum("Automobile")]
        Automobile,
        
        [XmlEnum("Boat")]
        Boat,
        
        [XmlEnum("Bond")]
        Bond,
        
        [XmlEnum("BorrowerEstimatedTotalAssets")]
        BorrowerEstimatedTotalAssets,
        
        [XmlEnum("BorrowerPrimaryHome")]
        BorrowerPrimaryHome,
        
        [XmlEnum("BridgeLoanNotDeposited")]
        BridgeLoanNotDeposited,
        
        [XmlEnum("CashOnHand")]
        CashOnHand,
        
        [XmlEnum("CertificateOfDepositTimeDeposit")]
        CertificateOfDepositTimeDeposit,
        
        [XmlEnum("CheckingAccount")]
        CheckingAccount,
        
        [XmlEnum("EarnestMoneyCashDepositTowardPurchase")]
        EarnestMoneyCashDepositTowardPurchase,
        
        [XmlEnum("EmployerAssistance")]
        EmployerAssistance,
        
        [XmlEnum("GiftOfCash")]
        GiftOfCash,
        
        [XmlEnum("GiftOfPropertyEquity")]
        GiftOfPropertyEquity,
        
        [XmlEnum("GiftsTotal")]
        GiftsTotal,
        
        [XmlEnum("Grant")]
        Grant,
        
        [XmlEnum("IndividualDevelopmentAccount")]
        IndividualDevelopmentAccount,
        
        [XmlEnum("LifeInsurance")]
        LifeInsurance,
        
        [XmlEnum("MoneyMarketFund")]
        MoneyMarketFund,
        
        [XmlEnum("MutualFund")]
        MutualFund,
        
        [XmlEnum("NetWorthOfBusinessOwned")]
        NetWorthOfBusinessOwned,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PendingNetSaleProceedsFromRealEstateAssets")]
        PendingNetSaleProceedsFromRealEstateAssets,
        
        [XmlEnum("ProceedsFromSaleOfNonRealEstateAsset")]
        ProceedsFromSaleOfNonRealEstateAsset,
        
        [XmlEnum("ProceedsFromSecuredLoan")]
        ProceedsFromSecuredLoan,
        
        [XmlEnum("ProceedsFromUnsecuredLoan")]
        ProceedsFromUnsecuredLoan,
        
        [XmlEnum("RealEstateOwned")]
        RealEstateOwned,
        
        [XmlEnum("RecreationalVehicle")]
        RecreationalVehicle,
        
        [XmlEnum("RelocationMoney")]
        RelocationMoney,
        
        [XmlEnum("RetirementFund")]
        RetirementFund,
        
        [XmlEnum("SaleOtherAssets")]
        SaleOtherAssets,
        
        [XmlEnum("SavingsAccount")]
        SavingsAccount,
        
        [XmlEnum("SavingsBond")]
        SavingsBond,
        
        [XmlEnum("SeverancePackage")]
        SeverancePackage,
        
        [XmlEnum("Stock")]
        Stock,
        
        [XmlEnum("StockOptions")]
        StockOptions,
        
        [XmlEnum("TrustAccount")]
        TrustAccount,
    }
}
