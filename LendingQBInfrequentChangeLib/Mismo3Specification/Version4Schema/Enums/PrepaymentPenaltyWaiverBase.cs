namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PrepaymentPenaltyWaiverBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("HardshipSale")]
        HardshipSale,
        
        [XmlEnum("None")]
        None,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("ThirdPartySale")]
        ThirdPartySale,
    }
}
