namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum CreditResponseAlertMessageCategoryBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("CreditFileSuppressed")]
        CreditFileSuppressed,
        
        [XmlEnum("DeathClaim")]
        DeathClaim,
        
        [XmlEnum("DemographicsVerification")]
        DemographicsVerification,
        
        [XmlEnum("FACTAActiveDuty")]
        FACTAActiveDuty,
        
        [XmlEnum("FACTAAddressDiscrepancy")]
        FACTAAddressDiscrepancy,
        
        [XmlEnum("FACTAFraudVictimExtended")]
        FACTAFraudVictimExtended,
        
        [XmlEnum("FACTAFraudVictimInitial")]
        FACTAFraudVictimInitial,
        
        [XmlEnum("FACTARiskScoreValue")]
        FACTARiskScoreValue,
        
        [XmlEnum("FraudVictim")]
        FraudVictim,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("RecentlyAddedAuthorizedUserAlert")]
        RecentlyAddedAuthorizedUserAlert,
        
        [XmlEnum("SSNVerification")]
        SSNVerification,
    }
}
