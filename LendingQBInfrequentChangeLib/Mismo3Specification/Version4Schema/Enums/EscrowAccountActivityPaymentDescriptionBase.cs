namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum EscrowAccountActivityPaymentDescriptionBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Assessment")]
        Assessment,
        
        [XmlEnum("CityPropertyTax")]
        CityPropertyTax,
        
        [XmlEnum("CountyPropertyTax")]
        CountyPropertyTax,
        
        [XmlEnum("EarthquakeInsurance")]
        EarthquakeInsurance,
        
        [XmlEnum("FloodInsurance")]
        FloodInsurance,
        
        [XmlEnum("HazardInsurance")]
        HazardInsurance,
        
        [XmlEnum("MortgageInsurance")]
        MortgageInsurance,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("SchoolPropertyTax")]
        SchoolPropertyTax,
        
        [XmlEnum("TownPropertyTax")]
        TownPropertyTax,
        
        [XmlEnum("USDAAnnualFee")]
        USDAAnnualFee,
        
        [XmlEnum("VillagePropertyTax")]
        VillagePropertyTax,
        
        [XmlEnum("WindstormInsurance")]
        WindstormInsurance,
    }
}
