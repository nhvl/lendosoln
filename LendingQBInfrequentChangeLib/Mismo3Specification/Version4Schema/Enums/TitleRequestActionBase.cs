namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum TitleRequestActionBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Cancellation")]
        Cancellation,
        
        [XmlEnum("Change")]
        Change,
        
        [XmlEnum("ERemittance")]
        ERemittance,
        
        [XmlEnum("GetDocument")]
        GetDocument,
        
        [XmlEnum("Original")]
        Original,
        
        [XmlEnum("PriceQuote")]
        PriceQuote,
        
        [XmlEnum("StatusQuery")]
        StatusQuery,
        
        [XmlEnum("Update")]
        Update,
        
        [XmlEnum("ValidateAgent")]
        ValidateAgent,
    }
}
