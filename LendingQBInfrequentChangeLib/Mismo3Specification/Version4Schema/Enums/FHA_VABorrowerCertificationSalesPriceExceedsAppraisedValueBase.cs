namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum FHA_VABorrowerCertificationSalesPriceExceedsAppraisedValueBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("A")]
        A,
        
        [XmlEnum("B")]
        B,
    }
}
