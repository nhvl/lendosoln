namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum IntentToOccupyBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("No")]
        No,
        
        [XmlEnum("Unknown")]
        Unknown,
        
        [XmlEnum("Yes")]
        Yes,
    }
}
