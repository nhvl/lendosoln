namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ExteriorFeatureBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Doors")]
        Doors,
        
        [XmlEnum("GuttersAndDownspouts")]
        GuttersAndDownspouts,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Skirting")]
        Skirting,
    }
}
