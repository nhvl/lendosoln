namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PropertyDispositionStatusBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Retain")]
        Retain,
        
        [XmlEnum("Sell")]
        Sell,
        
        [XmlEnum("Undecided")]
        Undecided,
        
        [XmlEnum("Unknown")]
        Unknown,
        
        [XmlEnum("Vacate")]
        Vacate,
    }
}
