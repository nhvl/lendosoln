namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum DisclosureIssuanceBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Initial")]
        Initial,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Revised")]
        Revised,
    }
}
