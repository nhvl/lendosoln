namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum HMDAEthnicityOriginBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Cuban")]
        Cuban,
        
        [XmlEnum("Mexican")]
        Mexican,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PuertoRican")]
        PuertoRican,
    }
}
