namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum GovernmentLoanApplicationBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("HomeLoanGuaranty")]
        HomeLoanGuaranty,
        
        [XmlEnum("NationalHousingAct")]
        NationalHousingAct,
    }
}
