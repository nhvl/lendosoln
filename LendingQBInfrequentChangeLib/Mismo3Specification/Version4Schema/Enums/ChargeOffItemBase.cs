namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ChargeOffItemBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AdditionalLien")]
        AdditionalLien,
        
        [XmlEnum("Bankruptcy")]
        Bankruptcy,
        
        [XmlEnum("Fraud")]
        Fraud,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("UncollectedInterest")]
        UncollectedInterest,
        
        [XmlEnum("UncollectedPrincipal")]
        UncollectedPrincipal,
        
        [XmlEnum("UnrecoverableSpecialHazard")]
        UnrecoverableSpecialHazard,
    }
}
