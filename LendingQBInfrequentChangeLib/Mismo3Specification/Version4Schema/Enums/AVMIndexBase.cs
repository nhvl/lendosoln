namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum AVMIndexBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("CoreLogic")]
        CoreLogic,
        
        [XmlEnum("FederalHousingFinanceAdministration")]
        FederalHousingFinanceAdministration,
        
        [XmlEnum("LenderProcessingServices")]
        LenderProcessingServices,
        
        [XmlEnum("NationalAssociationRealtorsPendingHomeSale")]
        NationalAssociationRealtorsPendingHomeSale,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("StandardsPoorCaseShiller")]
        StandardsPoorCaseShiller,
    }
}
