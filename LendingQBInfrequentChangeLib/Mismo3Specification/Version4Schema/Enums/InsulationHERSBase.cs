namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum InsulationHERSBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Best")]
        Best,
        
        [XmlEnum("Lowest")]
        Lowest,
        
        [XmlEnum("Moderate")]
        Moderate,
    }
}
