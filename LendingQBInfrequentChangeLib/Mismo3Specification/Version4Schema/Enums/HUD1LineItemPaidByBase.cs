namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum HUD1LineItemPaidByBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Buyer")]
        Buyer,
        
        [XmlEnum("LenderPremium")]
        LenderPremium,
        
        [XmlEnum("Seller")]
        Seller,
    }
}
