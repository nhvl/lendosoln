namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum OffSiteImprovementOwnershipBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Private")]
        Private,
        
        [XmlEnum("Public")]
        Public,
        
        [XmlEnum("Unknown")]
        Unknown,
    }
}
