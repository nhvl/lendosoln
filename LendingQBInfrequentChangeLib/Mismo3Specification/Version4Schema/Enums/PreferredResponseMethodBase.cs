namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PreferredResponseMethodBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Fax")]
        Fax,
        
        [XmlEnum("File")]
        File,
        
        [XmlEnum("FTP")]
        FTP,
        
        [XmlEnum("HTTP")]
        HTTP,
        
        [XmlEnum("HTTPS")]
        HTTPS,
        
        [XmlEnum("Mail")]
        Mail,
        
        [XmlEnum("MessageQueue")]
        MessageQueue,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("SMTP")]
        SMTP,
        
        [XmlEnum("VAN")]
        VAN,
    }
}
