namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum LandscapeLightingPowerSourceBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Solar")]
        Solar,
        
        [XmlEnum("TraditionalElectric")]
        TraditionalElectric,
    }
}
