namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum InterestCalculationBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Compound")]
        Compound,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("RuleOf78s")]
        RuleOf78s,
        
        [XmlEnum("Simple")]
        Simple,
    }
}
