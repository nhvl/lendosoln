namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum AssumabilityBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AssumableAfterFirstRateChangeDate")]
        AssumableAfterFirstRateChangeDate,
        
        [XmlEnum("AssumableForLifeOfLoan")]
        AssumableForLifeOfLoan,
        
        [XmlEnum("Other")]
        Other,
    }
}
