namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum UnitRentRateBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Market")]
        Market,
        
        [XmlEnum("Regulated")]
        Regulated,
    }
}
