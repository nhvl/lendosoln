namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum CreditPublicRecordBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Annulment")]
        Annulment,
        
        [XmlEnum("Attachment")]
        Attachment,
        
        [XmlEnum("BankruptcyChapter11")]
        BankruptcyChapter11,
        
        [XmlEnum("BankruptcyChapter12")]
        BankruptcyChapter12,
        
        [XmlEnum("BankruptcyChapter13")]
        BankruptcyChapter13,
        
        [XmlEnum("BankruptcyChapter7")]
        BankruptcyChapter7,
        
        [XmlEnum("BankruptcyChapter7Involuntary")]
        BankruptcyChapter7Involuntary,
        
        [XmlEnum("BankruptcyChapter7Voluntary")]
        BankruptcyChapter7Voluntary,
        
        [XmlEnum("BankruptcyTypeUnknown")]
        BankruptcyTypeUnknown,
        
        [XmlEnum("ChildSupport")]
        ChildSupport,
        
        [XmlEnum("Collection")]
        Collection,
        
        [XmlEnum("CustodyAgreement")]
        CustodyAgreement,
        
        [XmlEnum("DivorceDecree")]
        DivorceDecree,
        
        [XmlEnum("FamilySupport")]
        FamilySupport,
        
        [XmlEnum("FictitiousName")]
        FictitiousName,
        
        [XmlEnum("FinancialCounseling")]
        FinancialCounseling,
        
        [XmlEnum("FinancingStatement")]
        FinancingStatement,
        
        [XmlEnum("ForcibleDetainer")]
        ForcibleDetainer,
        
        [XmlEnum("Foreclosure")]
        Foreclosure,
        
        [XmlEnum("Garnishment")]
        Garnishment,
        
        [XmlEnum("Judgment")]
        Judgment,
        
        [XmlEnum("LawSuit")]
        LawSuit,
        
        [XmlEnum("Lien")]
        Lien,
        
        [XmlEnum("MechanicsLien")]
        MechanicsLien,
        
        [XmlEnum("MedicalLien")]
        MedicalLien,
        
        [XmlEnum("NonResponsibility")]
        NonResponsibility,
        
        [XmlEnum("NoticeOfDefault")]
        NoticeOfDefault,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PublicSale")]
        PublicSale,
        
        [XmlEnum("RealEstateRecording")]
        RealEstateRecording,
        
        [XmlEnum("Repossession")]
        Repossession,
        
        [XmlEnum("SpouseSupport")]
        SpouseSupport,
        
        [XmlEnum("SupportDebt")]
        SupportDebt,
        
        [XmlEnum("TaxLienCity")]
        TaxLienCity,
        
        [XmlEnum("TaxLienCounty")]
        TaxLienCounty,
        
        [XmlEnum("TaxLienFederal")]
        TaxLienFederal,
        
        [XmlEnum("TaxLienOther")]
        TaxLienOther,
        
        [XmlEnum("TaxLienState")]
        TaxLienState,
        
        [XmlEnum("Trusteeship")]
        Trusteeship,
        
        [XmlEnum("Unknown")]
        Unknown,
        
        [XmlEnum("UnlawfulDetainer")]
        UnlawfulDetainer,
    }
}
