namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum EnergyEfficiencyAuthorityLevelBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("County")]
        County,
        
        [XmlEnum("Federal")]
        Federal,
        
        [XmlEnum("Municipality")]
        Municipality,
        
        [XmlEnum("Private")]
        Private,
        
        [XmlEnum("State")]
        State,
    }
}
