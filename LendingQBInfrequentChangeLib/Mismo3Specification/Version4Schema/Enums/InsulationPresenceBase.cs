namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum InsulationPresenceBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Absent")]
        Absent,
        
        [XmlEnum("Exists")]
        Exists,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Unknown")]
        Unknown,
    }
}
