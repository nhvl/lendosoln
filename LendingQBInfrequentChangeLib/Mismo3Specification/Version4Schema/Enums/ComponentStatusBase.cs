namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ComponentStatusBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("NotUpdated")]
        NotUpdated,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Remodeled")]
        Remodeled,
        
        [XmlEnum("Renovated")]
        Renovated,
        
        [XmlEnum("Replaced")]
        Replaced,
        
        [XmlEnum("Updated")]
        Updated,
    }
}
