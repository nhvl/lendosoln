namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum EscrowPremiumRatePercentBasisBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("BorrowerRequestedLoanAmount")]
        BorrowerRequestedLoanAmount,
        
        [XmlEnum("NoteAmount")]
        NoteAmount,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PropertyValuationAmount")]
        PropertyValuationAmount,
        
        [XmlEnum("PurchasePriceAmount")]
        PurchasePriceAmount,
        
        [XmlEnum("TotalLoanAmount")]
        TotalLoanAmount,
    }
}
