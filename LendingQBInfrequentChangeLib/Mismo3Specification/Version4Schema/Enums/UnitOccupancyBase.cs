namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum UnitOccupancyBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("OwnerOccupied")]
        OwnerOccupied,
        
        [XmlEnum("Tenant")]
        Tenant,
        
        [XmlEnum("Unknown")]
        Unknown,
        
        [XmlEnum("Vacant")]
        Vacant,
    }
}
