namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum MIWorkoutApprovalConditionBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AdvanceFundsPartial")]
        AdvanceFundsPartial,
        
        [XmlEnum("BorrowerContribution")]
        BorrowerContribution,
        
        [XmlEnum("DelinquencyCapitalization")]
        DelinquencyCapitalization,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("WithRightToPursueDeficiencyJudgement")]
        WithRightToPursueDeficiencyJudgement,
    }
}
