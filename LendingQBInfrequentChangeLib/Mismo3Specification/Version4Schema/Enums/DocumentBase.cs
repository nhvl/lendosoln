namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum DocumentBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("203KBorrowerAcknowledgment")]
        Item203KBorrowerAcknowledgment,
        
        [XmlEnum("203KBorrowerIdentityOfInterestCertification")]
        Item203KBorrowerIdentityOfInterestCertification,
        
        [XmlEnum("203KConsultantIdentityOfInterestStatement")]
        Item203KConsultantIdentityOfInterestStatement,
        
        [XmlEnum("203KConsultantReport")]
        Item203KConsultantReport,
        
        [XmlEnum("203KCostEstimates")]
        Item203KCostEstimates,
        
        [XmlEnum("203KDrawRequest")]
        Item203KDrawRequest,
        
        [XmlEnum("203KHomeownerAcknowledgment")]
        Item203KHomeownerAcknowledgment,
        
        [XmlEnum("203KInitialDrawRequest")]
        Item203KInitialDrawRequest,
        
        [XmlEnum("203KMaximumMortgageWorksheet")]
        Item203KMaximumMortgageWorksheet,
        
        [XmlEnum("203KRehabilitationAgreement")]
        Item203KRehabilitationAgreement,
        
        [XmlEnum("AbstractNoticeAgreement")]
        AbstractNoticeAgreement,
        
        [XmlEnum("AbstractOfJudgment")]
        AbstractOfJudgment,
        
        [XmlEnum("ACHDebitAuthorization")]
        ACHDebitAuthorization,
        
        [XmlEnum("AcknowledgmentOfNoticeOfRightToCancel")]
        AcknowledgmentOfNoticeOfRightToCancel,
        
        [XmlEnum("AffidavitOfDeath")]
        AffidavitOfDeath,
        
        [XmlEnum("AffidavitOfLostNote")]
        AffidavitOfLostNote,
        
        [XmlEnum("AffiliatedBusinessArrangementDisclosure")]
        AffiliatedBusinessArrangementDisclosure,
        
        [XmlEnum("AirportNoisePollutionAgreement")]
        AirportNoisePollutionAgreement,
        
        [XmlEnum("AmendatoryClause")]
        AmendatoryClause,
        
        [XmlEnum("AmortizationSchedule")]
        AmortizationSchedule,
        
        [XmlEnum("ApplicationDisclosure")]
        ApplicationDisclosure,
        
        [XmlEnum("ApplicationDisclosureAdditionalCreditAndDebt")]
        ApplicationDisclosureAdditionalCreditAndDebt,
        
        [XmlEnum("AppraisalRecertification")]
        AppraisalRecertification,
        
        [XmlEnum("AppraisalReport")]
        AppraisalReport,
        
        [XmlEnum("AppraisalReportDesktop")]
        AppraisalReportDesktop,
        
        [XmlEnum("AppraisalReportExteriorOnly")]
        AppraisalReportExteriorOnly,
        
        [XmlEnum("AppraisalReportInteriorExterior")]
        AppraisalReportInteriorExterior,
        
        [XmlEnum("AppraisalReview")]
        AppraisalReview,
        
        [XmlEnum("AppraisalReviewExteriorOnlyInspectionWithValueOpinion")]
        AppraisalReviewExteriorOnlyInspectionWithValueOpinion,
        
        [XmlEnum("AppraisalReviewExteriorOnlyWithoutValueOpinion")]
        AppraisalReviewExteriorOnlyWithoutValueOpinion,
        
        [XmlEnum("AppraisalReviewInteriorExteriorInspectionWithValueOpinion")]
        AppraisalReviewInteriorExteriorInspectionWithValueOpinion,
        
        [XmlEnum("AppraisalReviewInteriorExteriorWithoutValueOpinion")]
        AppraisalReviewInteriorExteriorWithoutValueOpinion,
        
        [XmlEnum("AppraisalReviewNoInspectionWithValueOpinion")]
        AppraisalReviewNoInspectionWithValueOpinion,
        
        [XmlEnum("AppraisalReviewWithoutValueOpinion")]
        AppraisalReviewWithoutValueOpinion,
        
        [XmlEnum("AppraisalReviewRequest")]
        AppraisalReviewRequest,
        
        [XmlEnum("ApprovalLetter")]
        ApprovalLetter,
        
        [XmlEnum("ArticlesOfIncorporation")]
        ArticlesOfIncorporation,
        
        [XmlEnum("Assignment")]
        Assignment,
        
        [XmlEnum("AssignmentAssignmentOfDeedOfTrust")]
        AssignmentAssignmentOfDeedOfTrust,
        
        [XmlEnum("AssignmentAssignmentOfMortgage")]
        AssignmentAssignmentOfMortgage,
        
        [XmlEnum("AssignmentAssignmentOfRents")]
        AssignmentAssignmentOfRents,
        
        [XmlEnum("AssignmentAssignmentOfTrade")]
        AssignmentAssignmentOfTrade,
        
        [XmlEnum("AssignmentBlanketAssignment")]
        AssignmentBlanketAssignment,
        
        [XmlEnum("AssignmentCooperativeAssignmentOfProprietaryLease")]
        AssignmentCooperativeAssignmentOfProprietaryLease,
        
        [XmlEnum("AssumptionAgreement")]
        AssumptionAgreement,
        
        [XmlEnum("AssuranceOfCompletion")]
        AssuranceOfCompletion,
        
        [XmlEnum("AttestationOfDamageToProperty")]
        AttestationOfDamageToProperty,
        
        [XmlEnum("AttorneyInFactAffidavit")]
        AttorneyInFactAffidavit,
        
        [XmlEnum("AttorneysOpinionLetter")]
        AttorneysOpinionLetter,
        
        [XmlEnum("AutomatedUnderwritingFeedback")]
        AutomatedUnderwritingFeedback,
        
        [XmlEnum("AutomatedValuationReport")]
        AutomatedValuationReport,
        
        [XmlEnum("AutomatedValuationReportWithInspection")]
        AutomatedValuationReportWithInspection,
        
        [XmlEnum("AVMFeedback")]
        AVMFeedback,
        
        [XmlEnum("BaileeLetter")]
        BaileeLetter,
        
        [XmlEnum("BalanceTransferAuthorizationNotice")]
        BalanceTransferAuthorizationNotice,
        
        [XmlEnum("BalloonRefinanceDisclosure")]
        BalloonRefinanceDisclosure,
        
        [XmlEnum("BankDepositSlip")]
        BankDepositSlip,
        
        [XmlEnum("BankruptcyDischargeNotice")]
        BankruptcyDischargeNotice,
        
        [XmlEnum("BankStatement")]
        BankStatement,
        
        [XmlEnum("BankStatement401K")]
        BankStatement401K,
        
        [XmlEnum("BankStatementCheckingAccount")]
        BankStatementCheckingAccount,
        
        [XmlEnum("BankStatementMutualFundAccount")]
        BankStatementMutualFundAccount,
        
        [XmlEnum("BankStatementSavingAccount")]
        BankStatementSavingAccount,
        
        [XmlEnum("BankStatementStockAccount")]
        BankStatementStockAccount,
        
        [XmlEnum("BenefitPlanDistributionStatement")]
        BenefitPlanDistributionStatement,
        
        [XmlEnum("BenefitPlanDistributionStatement401K")]
        BenefitPlanDistributionStatement401K,
        
        [XmlEnum("BenefitPlanDistributionStatementAnnuity")]
        BenefitPlanDistributionStatementAnnuity,
        
        [XmlEnum("BenefitPlanDistributionStatementPension")]
        BenefitPlanDistributionStatementPension,
        
        [XmlEnum("BenefitPlanDistributionStatementTrust")]
        BenefitPlanDistributionStatementTrust,
        
        [XmlEnum("Bid")]
        Bid,
        
        [XmlEnum("BirthCertificate")]
        BirthCertificate,
        
        [XmlEnum("BondCertificate")]
        BondCertificate,
        
        [XmlEnum("BorrowerAcknowledgmentOfPropertyCondition")]
        BorrowerAcknowledgmentOfPropertyCondition,
        
        [XmlEnum("BorrowerCorrespondence")]
        BorrowerCorrespondence,
        
        [XmlEnum("BorrowerCorrespondenceGapInEmployment")]
        BorrowerCorrespondenceGapInEmployment,
        
        [XmlEnum("BorrowerCorrespondenceLetterOfExplanation")]
        BorrowerCorrespondenceLetterOfExplanation,
        
        [XmlEnum("BorrowerCorrespondenceLetterOfIntent")]
        BorrowerCorrespondenceLetterOfIntent,
        
        [XmlEnum("BorrowerCorrespondenceQualifiedWrittenRequest")]
        BorrowerCorrespondenceQualifiedWrittenRequest,
        
        [XmlEnum("BorrowerCorrespondenceTrailingSpouse")]
        BorrowerCorrespondenceTrailingSpouse,
        
        [XmlEnum("BorrowerLienAffidavit")]
        BorrowerLienAffidavit,
        
        [XmlEnum("BorrowersCertification")]
        BorrowersCertification,
        
        [XmlEnum("BorrowersContractWithRespectToHotelAndTransientUseOfProperty")]
        BorrowersContractWithRespectToHotelAndTransientUseOfProperty,
        
        [XmlEnum("BreachNotice")]
        BreachNotice,
        
        [XmlEnum("BrokerDisclosureStatement")]
        BrokerDisclosureStatement,
        
        [XmlEnum("BrokerPriceOpinion")]
        BrokerPriceOpinion,
        
        [XmlEnum("BrokerPriceOpinionDesktop")]
        BrokerPriceOpinionDesktop,
        
        [XmlEnum("BrokerPriceOpinionExteriorInspection")]
        BrokerPriceOpinionExteriorInspection,
        
        [XmlEnum("BuildersCertification")]
        BuildersCertification,
        
        [XmlEnum("BuildersCertificationBuilderCertificationOfPlansAndSpecifications")]
        BuildersCertificationBuilderCertificationOfPlansAndSpecifications,
        
        [XmlEnum("BuildersCertificationBuildersCertificate")]
        BuildersCertificationBuildersCertificate,
        
        [XmlEnum("BuildersCertificationPropertyInspection")]
        BuildersCertificationPropertyInspection,
        
        [XmlEnum("BuildersCertificationTermiteTreatment")]
        BuildersCertificationTermiteTreatment,
        
        [XmlEnum("BuildingPermit")]
        BuildingPermit,
        
        [XmlEnum("BusinessLicense")]
        BusinessLicense,
        
        [XmlEnum("BuydownAgreement")]
        BuydownAgreement,
        
        [XmlEnum("BuyingYourHomeSettlementCostsAndHelpfulInformation")]
        BuyingYourHomeSettlementCostsAndHelpfulInformation,
        
        [XmlEnum("CAIVRSAuthorization")]
        CAIVRSAuthorization,
        
        [XmlEnum("CancellationOfListing")]
        CancellationOfListing,
        
        [XmlEnum("Check")]
        Check,
        
        [XmlEnum("Checklist")]
        Checklist,
        
        [XmlEnum("ChildSupportVerification")]
        ChildSupportVerification,
        
        [XmlEnum("CloseLineOfCreditRequest")]
        CloseLineOfCreditRequest,
        
        [XmlEnum("ClosingDisclosure")]
        ClosingDisclosure,
        
        [XmlEnum("ClosingDisclosureAlternateForm")]
        ClosingDisclosureAlternateForm,
        
        [XmlEnum("ClosingDisclosureBorrowerOnly")]
        ClosingDisclosureBorrowerOnly,
        
        [XmlEnum("ClosingDisclosureModelForm")]
        ClosingDisclosureModelForm,
        
        [XmlEnum("ClosingDisclosureSellerOnly")]
        ClosingDisclosureSellerOnly,
        
        [XmlEnum("ClosingInstructions")]
        ClosingInstructions,
        
        [XmlEnum("CollectionRegister")]
        CollectionRegister,
        
        [XmlEnum("ComparativeIncomeAnalysis")]
        ComparativeIncomeAnalysis,
        
        [XmlEnum("ComplianceAgreement")]
        ComplianceAgreement,
        
        [XmlEnum("ComplianceInspectionReport")]
        ComplianceInspectionReport,
        
        [XmlEnum("ConditionalCommitment")]
        ConditionalCommitment,
        
        [XmlEnum("CondominiumOccupancyCertificate")]
        CondominiumOccupancyCertificate,
        
        [XmlEnum("ConservatorAndGuardianshipAgreement")]
        ConservatorAndGuardianshipAgreement,
        
        [XmlEnum("ConstructionCostBreakdown")]
        ConstructionCostBreakdown,
        
        [XmlEnum("ConsumerHandbookOnARM")]
        ConsumerHandbookOnARM,
        
        [XmlEnum("ConversionOptionNotice")]
        ConversionOptionNotice,
        
        [XmlEnum("ConveyanceDeed")]
        ConveyanceDeed,
        
        [XmlEnum("ConveyanceDeedBargainAndSaleDeed")]
        ConveyanceDeedBargainAndSaleDeed,
        
        [XmlEnum("ConveyanceDeedQuitClaimDeed")]
        ConveyanceDeedQuitClaimDeed,
        
        [XmlEnum("ConveyanceDeedWarrantyDeed")]
        ConveyanceDeedWarrantyDeed,
        
        [XmlEnum("ConveyanceTaxForm")]
        ConveyanceTaxForm,
        
        [XmlEnum("CooperativeBylaws")]
        CooperativeBylaws,
        
        [XmlEnum("CooperativeOperatingBudget")]
        CooperativeOperatingBudget,
        
        [XmlEnum("CooperativeProprietaryLease")]
        CooperativeProprietaryLease,
        
        [XmlEnum("CooperativeRecognitionAgreement")]
        CooperativeRecognitionAgreement,
        
        [XmlEnum("CooperativeStockCertificate")]
        CooperativeStockCertificate,
        
        [XmlEnum("CooperativeStockPower")]
        CooperativeStockPower,
        
        [XmlEnum("CosignerNotice")]
        CosignerNotice,
        
        [XmlEnum("CouncilOfAmericanBuildingOfficialsCertification")]
        CouncilOfAmericanBuildingOfficialsCertification,
        
        [XmlEnum("CounselingCertification")]
        CounselingCertification,
        
        [XmlEnum("CounselingCertificationHomeownership")]
        CounselingCertificationHomeownership,
        
        [XmlEnum("CounselingCertificationHomeRetention")]
        CounselingCertificationHomeRetention,
        
        [XmlEnum("CounselingChecklistForMilitaryHomebuyers")]
        CounselingChecklistForMilitaryHomebuyers,
        
        [XmlEnum("CreditCardAuthorization")]
        CreditCardAuthorization,
        
        [XmlEnum("CreditInsuranceAgreement")]
        CreditInsuranceAgreement,
        
        [XmlEnum("CreditReport")]
        CreditReport,
        
        [XmlEnum("CreditReportingAdjustment")]
        CreditReportingAdjustment,
        
        [XmlEnum("CustomerIdentificationVerification")]
        CustomerIdentificationVerification,
        
        [XmlEnum("DeathCertificate")]
        DeathCertificate,
        
        [XmlEnum("DivorceDecree")]
        DivorceDecree,
        
        [XmlEnum("DocumentDeliveryCertification")]
        DocumentDeliveryCertification,
        
        [XmlEnum("DocumentDeliveryCertificationAppraisalReport")]
        DocumentDeliveryCertificationAppraisalReport,
        
        [XmlEnum("DocumentSequence")]
        DocumentSequence,
        
        [XmlEnum("DriversLicense")]
        DriversLicense,
        
        [XmlEnum("EarlyStartLetter")]
        EarlyStartLetter,
        
        [XmlEnum("ElectronicFundsTransfer")]
        ElectronicFundsTransfer,
        
        [XmlEnum("ElevationCertificate")]
        ElevationCertificate,
        
        [XmlEnum("EmployeeIdentification")]
        EmployeeIdentification,
        
        [XmlEnum("EnergyEfficientMortgageWorksheet")]
        EnergyEfficientMortgageWorksheet,
        
        [XmlEnum("EqualCreditOpportunityActDisclosure")]
        EqualCreditOpportunityActDisclosure,
        
        [XmlEnum("EscrowAgreement")]
        EscrowAgreement,
        
        [XmlEnum("EscrowAnalysisAnnualDisclosureStatement")]
        EscrowAnalysisAnnualDisclosureStatement,
        
        [XmlEnum("EscrowForCompletionAgreement")]
        EscrowForCompletionAgreement,
        
        [XmlEnum("EscrowForCompletionLetter")]
        EscrowForCompletionLetter,
        
        [XmlEnum("EscrowWaiver")]
        EscrowWaiver,
        
        [XmlEnum("EstimateOfClosingCostsPaidToThirdParty")]
        EstimateOfClosingCostsPaidToThirdParty,
        
        [XmlEnum("EstoppelAgreement")]
        EstoppelAgreement,
        
        [XmlEnum("EvaluationReport")]
        EvaluationReport,
        
        [XmlEnum("EvaluationReportDesktop")]
        EvaluationReportDesktop,
        
        [XmlEnum("EvaluationReportExteriorInspection")]
        EvaluationReportExteriorInspection,
        
        [XmlEnum("FACTACreditScoreDisclosure")]
        FACTACreditScoreDisclosure,
        
        [XmlEnum("FairLendingNotice")]
        FairLendingNotice,
        
        [XmlEnum("FederalApplicationInsuranceDisclosure")]
        FederalApplicationInsuranceDisclosure,
        
        [XmlEnum("FederalSaleOfInsuranceDisclosure")]
        FederalSaleOfInsuranceDisclosure,
        
        [XmlEnum("FHA_MIPremiumNettingAuthorization")]
        FHA_MIPremiumNettingAuthorization,
        
        [XmlEnum("FHAFiveDayWaiver")]
        FHAFiveDayWaiver,
        
        [XmlEnum("FHALimitedDenialOfParticipationGeneralServicesAdministrationChecklist")]
        FHALimitedDenialOfParticipationGeneralServicesAdministrationChecklist,
        
        [XmlEnum("FHAMortgageCreditAnalysisWorksheet")]
        FHAMortgageCreditAnalysisWorksheet,
        
        [XmlEnum("FHAReferralChecklist")]
        FHAReferralChecklist,
        
        [XmlEnum("FHARefinanceMaximumMortgageWorksheet")]
        FHARefinanceMaximumMortgageWorksheet,
        
        [XmlEnum("FHARefinanceOfBorrowersInNegativeEquityPositionsBorrowerCertification")]
        FHARefinanceOfBorrowersInNegativeEquityPositionsBorrowerCertification,
        
        [XmlEnum("FinancialStatement")]
        FinancialStatement,
        
        [XmlEnum("FinancialStatementBalanceSheet")]
        FinancialStatementBalanceSheet,
        
        [XmlEnum("FinancialStatementCashFlow")]
        FinancialStatementCashFlow,
        
        [XmlEnum("FinancialStatementFinancialPosition")]
        FinancialStatementFinancialPosition,
        
        [XmlEnum("FinancialStatementIncome")]
        FinancialStatementIncome,
        
        [XmlEnum("FloodHazardNotice")]
        FloodHazardNotice,
        
        [XmlEnum("FloodInsuranceAgreement")]
        FloodInsuranceAgreement,
        
        [XmlEnum("ForbearancePlanAgreement")]
        ForbearancePlanAgreement,
        
        [XmlEnum("ForcePlacedInsuranceNotice")]
        ForcePlacedInsuranceNotice,
        
        [XmlEnum("ForcePlacedInsuranceNoticeCancellation")]
        ForcePlacedInsuranceNoticeCancellation,
        
        [XmlEnum("ForcePlacedInsuranceNoticeInitial")]
        ForcePlacedInsuranceNoticeInitial,
        
        [XmlEnum("ForcePlacedInsuranceNoticeInsufficientDocumentation")]
        ForcePlacedInsuranceNoticeInsufficientDocumentation,
        
        [XmlEnum("ForcePlacedInsuranceNoticePlacement")]
        ForcePlacedInsuranceNoticePlacement,
        
        [XmlEnum("ForcePlacedInsuranceNoticeRenewal")]
        ForcePlacedInsuranceNoticeRenewal,
        
        [XmlEnum("ForcePlacedInsuranceNoticeSecondAndFinal")]
        ForcePlacedInsuranceNoticeSecondAndFinal,
        
        [XmlEnum("ForYourProtectionHomeInspection")]
        ForYourProtectionHomeInspection,
        
        [XmlEnum("FREOwnedStreamlineRefinanceChecklist")]
        FREOwnedStreamlineRefinanceChecklist,
        
        [XmlEnum("FundingTransmittal")]
        FundingTransmittal,
        
        [XmlEnum("GeneralLoanAcknowledgment")]
        GeneralLoanAcknowledgment,
        
        [XmlEnum("GFE")]
        GFE,
        
        [XmlEnum("GiftLetter")]
        GiftLetter,
        
        [XmlEnum("GroupSavingsAgreement")]
        GroupSavingsAgreement,
        
        [XmlEnum("HECMAntiChurningDisclosure")]
        HECMAntiChurningDisclosure,
        
        [XmlEnum("HECMCalculationEvidence")]
        HECMCalculationEvidence,
        
        [XmlEnum("HECMChoiceOfInsuranceOptionsNotice")]
        HECMChoiceOfInsuranceOptionsNotice,
        
        [XmlEnum("HECMClosingCostsAndLiensSchedule")]
        HECMClosingCostsAndLiensSchedule,
        
        [XmlEnum("HECMCounselingWaiverQualification")]
        HECMCounselingWaiverQualification,
        
        [XmlEnum("HECMExtension")]
        HECMExtension,
        
        [XmlEnum("HECMFaceToFaceCertification")]
        HECMFaceToFaceCertification,
        
        [XmlEnum("HECMFinancialAssessmentWorksheet")]
        HECMFinancialAssessmentWorksheet,
        
        [XmlEnum("HECMLoanSubmissionSchedule")]
        HECMLoanSubmissionSchedule,
        
        [XmlEnum("HECMNoticeToBorrower")]
        HECMNoticeToBorrower,
        
        [XmlEnum("HECMPaymentPlan")]
        HECMPaymentPlan,
        
        [XmlEnum("HERSHomeEnergyReport")]
        HERSHomeEnergyReport,
        
        [XmlEnum("HighCostWorksheet")]
        HighCostWorksheet,
        
        [XmlEnum("HoldHarmlessAgreement")]
        HoldHarmlessAgreement,
        
        [XmlEnum("HomeBuyerEducationCertification")]
        HomeBuyerEducationCertification,
        
        [XmlEnum("HomeEquityLineFreezeLetter")]
        HomeEquityLineFreezeLetter,
        
        [XmlEnum("HomeownersAssociationCertification")]
        HomeownersAssociationCertification,
        
        [XmlEnum("HousingCounselingAgenciesList")]
        HousingCounselingAgenciesList,
        
        [XmlEnum("IdentityAffidavit")]
        IdentityAffidavit,
        
        [XmlEnum("IdentityAffidavitIdentityTheft")]
        IdentityAffidavitIdentityTheft,
        
        [XmlEnum("IdentityAffidavitName")]
        IdentityAffidavitName,
        
        [XmlEnum("IdentityAffidavitSignature")]
        IdentityAffidavitSignature,
        
        [XmlEnum("IdentityTheftDisclosure")]
        IdentityTheftDisclosure,
        
        [XmlEnum("IncompleteApplicationNotice")]
        IncompleteApplicationNotice,
        
        [XmlEnum("IndividualDevelopmentAccountStatement")]
        IndividualDevelopmentAccountStatement,
        
        [XmlEnum("InitialAdjustableRateMortgageDisclosure")]
        InitialAdjustableRateMortgageDisclosure,
        
        [XmlEnum("InitialEscrowAccountDisclosure")]
        InitialEscrowAccountDisclosure,
        
        [XmlEnum("IntegratedDisclosure")]
        IntegratedDisclosure,
        
        [XmlEnum("InterestRateAndDiscountDisclosureStatement")]
        InterestRateAndDiscountDisclosureStatement,
        
        [XmlEnum("Invoice")]
        Invoice,
        
        [XmlEnum("InvoiceUtilityBill")]
        InvoiceUtilityBill,
        
        [XmlEnum("IRS1040")]
        IRS1040,
        
        [XmlEnum("IRS1041")]
        IRS1041,
        
        [XmlEnum("IRS1065")]
        IRS1065,
        
        [XmlEnum("IRS1098")]
        IRS1098,
        
        [XmlEnum("IRS1099")]
        IRS1099,
        
        [XmlEnum("IRS1099A")]
        IRS1099A,
        
        [XmlEnum("IRS1099B")]
        IRS1099B,
        
        [XmlEnum("IRS1099C")]
        IRS1099C,
        
        [XmlEnum("IRS1099CAP")]
        IRS1099CAP,
        
        [XmlEnum("IRS1099DIV")]
        IRS1099DIV,
        
        [XmlEnum("IRS1099G")]
        IRS1099G,
        
        [XmlEnum("IRS1099H")]
        IRS1099H,
        
        [XmlEnum("IRS1099INT")]
        IRS1099INT,
        
        [XmlEnum("IRS1099LTC")]
        IRS1099LTC,
        
        [XmlEnum("IRS1099MISC")]
        IRS1099MISC,
        
        [XmlEnum("IRS1099OID")]
        IRS1099OID,
        
        [XmlEnum("IRS1099PATR")]
        IRS1099PATR,
        
        [XmlEnum("IRS1099Q")]
        IRS1099Q,
        
        [XmlEnum("IRS1099R")]
        IRS1099R,
        
        [XmlEnum("IRS1099S")]
        IRS1099S,
        
        [XmlEnum("IRS1099SA")]
        IRS1099SA,
        
        [XmlEnum("IRS1120")]
        IRS1120,
        
        [XmlEnum("IRS1120S")]
        IRS1120S,
        
        [XmlEnum("IRSTaxTranscript")]
        IRSTaxTranscript,
        
        [XmlEnum("IRSW2")]
        IRSW2,
        
        [XmlEnum("IRSW8")]
        IRSW8,
        
        [XmlEnum("IRSW9")]
        IRSW9,
        
        [XmlEnum("ItemizationOfAmountFinanced")]
        ItemizationOfAmountFinanced,
        
        [XmlEnum("Judgment")]
        Judgment,
        
        [XmlEnum("LandLeaseholdAgreement")]
        LandLeaseholdAgreement,
        
        [XmlEnum("LastWillAndTestament")]
        LastWillAndTestament,
        
        [XmlEnum("LeadHazardInformation")]
        LeadHazardInformation,
        
        [XmlEnum("LegalDescription")]
        LegalDescription,
        
        [XmlEnum("LenderCorrespondence")]
        LenderCorrespondence,
        
        [XmlEnum("LenderLateEndorsementRequestCertificationLetter")]
        LenderLateEndorsementRequestCertificationLetter,
        
        [XmlEnum("LifeInsurancePolicy")]
        LifeInsurancePolicy,
        
        [XmlEnum("LineItemBudget")]
        LineItemBudget,
        
        [XmlEnum("LoanApplication")]
        LoanApplication,
        
        [XmlEnum("LoanApplicationFNM1009")]
        LoanApplicationFNM1009,
        
        [XmlEnum("LoanApplicationURLA")]
        LoanApplicationURLA,
        
        [XmlEnum("LoanClosingNotice")]
        LoanClosingNotice,
        
        [XmlEnum("LoanEstimate")]
        LoanEstimate,
        
        [XmlEnum("LoanModificationAffidavit")]
        LoanModificationAffidavit,
        
        [XmlEnum("LoanModificationAgreement")]
        LoanModificationAgreement,
        
        [XmlEnum("LoanModificationDenialNotice")]
        LoanModificationDenialNotice,
        
        [XmlEnum("LoanModificationRequest")]
        LoanModificationRequest,
        
        [XmlEnum("LoanModificationTrialPaymentPeriodAgreement")]
        LoanModificationTrialPaymentPeriodAgreement,
        
        [XmlEnum("LoanPayoffRequest")]
        LoanPayoffRequest,
        
        [XmlEnum("LoanStatement")]
        LoanStatement,
        
        [XmlEnum("LoanSubmissionSummary")]
        LoanSubmissionSummary,
        
        [XmlEnum("ManufacturedHousingCertificateOfTitle")]
        ManufacturedHousingCertificateOfTitle,
        
        [XmlEnum("MaritalStatusAffidavit")]
        MaritalStatusAffidavit,
        
        [XmlEnum("MarriageCertificate")]
        MarriageCertificate,
        
        [XmlEnum("MIApplication")]
        MIApplication,
        
        [XmlEnum("MICertificate")]
        MICertificate,
        
        [XmlEnum("MIConditionalCommitment")]
        MIConditionalCommitment,
        
        [XmlEnum("MIDeclination")]
        MIDeclination,
        
        [XmlEnum("MIDisclosure")]
        MIDisclosure,
        
        [XmlEnum("MilitaryDischargePapers")]
        MilitaryDischargePapers,
        
        [XmlEnum("MilitaryIdentification")]
        MilitaryIdentification,
        
        [XmlEnum("MIModification")]
        MIModification,
        
        [XmlEnum("MIRateQuote")]
        MIRateQuote,
        
        [XmlEnum("MISuspension")]
        MISuspension,
        
        [XmlEnum("MonthlySummaryOfAssistancePayments")]
        MonthlySummaryOfAssistancePayments,
        
        [XmlEnum("MortgageesCertificationAndApplicationForAssistanceOrInterestReductionPayments")]
        MortgageesCertificationAndApplicationForAssistanceOrInterestReductionPayments,
        
        [XmlEnum("NameAffidavit")]
        NameAffidavit,
        
        [XmlEnum("NationalIdentification")]
        NationalIdentification,
        
        [XmlEnum("NationalIdentificationCedulaDeIdentidad")]
        NationalIdentificationCedulaDeIdentidad,
        
        [XmlEnum("NationalIdentificationSocialSecurityCard")]
        NationalIdentificationSocialSecurityCard,
        
        [XmlEnum("NearestLivingRelativeInformation")]
        NearestLivingRelativeInformation,
        
        [XmlEnum("NearestLivingRelativeInformationAlternativeContact")]
        NearestLivingRelativeInformationAlternativeContact,
        
        [XmlEnum("NearestLivingRelativeInformationVAQuestionnaire")]
        NearestLivingRelativeInformationVAQuestionnaire,
        
        [XmlEnum("NonBorrowingSpouseCertificationLetter")]
        NonBorrowingSpouseCertificationLetter,
        
        [XmlEnum("NonBorrowingSpouseCertificationLetterEligible")]
        NonBorrowingSpouseCertificationLetterEligible,
        
        [XmlEnum("NonBorrowingSpouseCertificationLetterIneligible")]
        NonBorrowingSpouseCertificationLetterIneligible,
        
        [XmlEnum("NonDiplomatVerification")]
        NonDiplomatVerification,
        
        [XmlEnum("NonRefundabilityNotice")]
        NonRefundabilityNotice,
        
        [XmlEnum("Note")]
        Note,
        
        [XmlEnum("NoteConsolidated")]
        NoteConsolidated,
        
        [XmlEnum("NoteHECMLoanAgreement")]
        NoteHECMLoanAgreement,
        
        [XmlEnum("NoteNewMoney")]
        NoteNewMoney,
        
        [XmlEnum("NoteAddendum")]
        NoteAddendum,
        
        [XmlEnum("NoteAddendumAffordableMeritRate")]
        NoteAddendumAffordableMeritRate,
        
        [XmlEnum("NoteAddendumArbitration")]
        NoteAddendumArbitration,
        
        [XmlEnum("NoteAddendumARM")]
        NoteAddendumARM,
        
        [XmlEnum("NoteAddendumBalloon")]
        NoteAddendumBalloon,
        
        [XmlEnum("NoteAddendumConstruction")]
        NoteAddendumConstruction,
        
        [XmlEnum("NoteAddendumFixedRateOption")]
        NoteAddendumFixedRateOption,
        
        [XmlEnum("NoteAddendumGEM")]
        NoteAddendumGEM,
        
        [XmlEnum("NoteAddendumGPM")]
        NoteAddendumGPM,
        
        [XmlEnum("NoteAddendumInterestOnly")]
        NoteAddendumInterestOnly,
        
        [XmlEnum("NoteAddendumInterVivosRevocableTrust")]
        NoteAddendumInterVivosRevocableTrust,
        
        [XmlEnum("NoteAddendumOccupancyDeclaration")]
        NoteAddendumOccupancyDeclaration,
        
        [XmlEnum("NoteAddendumPrepayment")]
        NoteAddendumPrepayment,
        
        [XmlEnum("NoteAddendumRateImprovement")]
        NoteAddendumRateImprovement,
        
        [XmlEnum("NoteAllonge")]
        NoteAllonge,
        
        [XmlEnum("NoteAndSecurityInstrumentModification")]
        NoteAndSecurityInstrumentModification,
        
        [XmlEnum("NoteModification")]
        NoteModification,
        
        [XmlEnum("NoteRider")]
        NoteRider,
        
        [XmlEnum("NoteRiderARM")]
        NoteRiderARM,
        
        [XmlEnum("NoteRiderBalloon")]
        NoteRiderBalloon,
        
        [XmlEnum("NoteRiderBuydown")]
        NoteRiderBuydown,
        
        [XmlEnum("NoteRiderHECMRepair")]
        NoteRiderHECMRepair,
        
        [XmlEnum("NoteRiderOccupancy")]
        NoteRiderOccupancy,
        
        [XmlEnum("NoteRiderPrepayment")]
        NoteRiderPrepayment,
        
        [XmlEnum("NoteRiderStepRate")]
        NoteRiderStepRate,
        
        [XmlEnum("NoticeOfActionTaken")]
        NoticeOfActionTaken,
        
        [XmlEnum("NoticeOfCompletion")]
        NoticeOfCompletion,
        
        [XmlEnum("NoticeOfDefault")]
        NoticeOfDefault,
        
        [XmlEnum("NoticeOfRightToCancel")]
        NoticeOfRightToCancel,
        
        [XmlEnum("NoticeOfRightToCancelAddSecurityInterest")]
        NoticeOfRightToCancelAddSecurityInterest,
        
        [XmlEnum("NoticeOfRightToCancelCreditIncrease")]
        NoticeOfRightToCancelCreditIncrease,
        
        [XmlEnum("NoticeOfRightToCancelIncreaseSecurity")]
        NoticeOfRightToCancelIncreaseSecurity,
        
        [XmlEnum("NoticeOfRightToCancelOpenAccount")]
        NoticeOfRightToCancelOpenAccount,
        
        [XmlEnum("NoticeOfRightToCopyOfAppraisalReport")]
        NoticeOfRightToCopyOfAppraisalReport,
        
        [XmlEnum("NoticeToHomebuyer")]
        NoticeToHomebuyer,
        
        [XmlEnum("NoticeToLender")]
        NoticeToLender,
        
        [XmlEnum("OccupancyAgreement")]
        OccupancyAgreement,
        
        [XmlEnum("OccupancyCertification")]
        OccupancyCertification,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PartialReleaseOfSecurityInstrument")]
        PartialReleaseOfSecurityInstrument,
        
        [XmlEnum("PartnershipAgreement")]
        PartnershipAgreement,
        
        [XmlEnum("Passport")]
        Passport,
        
        [XmlEnum("PaymentHistory")]
        PaymentHistory,
        
        [XmlEnum("PaymentLetter")]
        PaymentLetter,
        
        [XmlEnum("PayoffStatement")]
        PayoffStatement,
        
        [XmlEnum("PayStub")]
        PayStub,
        
        [XmlEnum("PermanentResidentIdentification")]
        PermanentResidentIdentification,
        
        [XmlEnum("PersonalPropertyAppraisalReport")]
        PersonalPropertyAppraisalReport,
        
        [XmlEnum("PowerOfAttorney")]
        PowerOfAttorney,
        
        [XmlEnum("PowerOfAttorneyDurable")]
        PowerOfAttorneyDurable,
        
        [XmlEnum("PowerOfAttorneyLimited")]
        PowerOfAttorneyLimited,
        
        [XmlEnum("PowerOfAttorneyManufacturedHousing")]
        PowerOfAttorneyManufacturedHousing,
        
        [XmlEnum("PrepaymentChargeOptionNotice")]
        PrepaymentChargeOptionNotice,
        
        [XmlEnum("PrequalificationLetter")]
        PrequalificationLetter,
        
        [XmlEnum("PrimaryMIAnnualDisclosure")]
        PrimaryMIAnnualDisclosure,
        
        [XmlEnum("PrivacyDisclosure")]
        PrivacyDisclosure,
        
        [XmlEnum("PrivateIdentification")]
        PrivateIdentification,
        
        [XmlEnum("ProductComparisonDisclosure")]
        ProductComparisonDisclosure,
        
        [XmlEnum("ProofOfHUDWaiverCAIVRSClearance")]
        ProofOfHUDWaiverCAIVRSClearance,
        
        [XmlEnum("ProofOfMonetaryInvestment")]
        ProofOfMonetaryInvestment,
        
        [XmlEnum("ProofOfResidenceInDeclaredMajorDisasterArea")]
        ProofOfResidenceInDeclaredMajorDisasterArea,
        
        [XmlEnum("ProofOfTaxDeferralOrExemption")]
        ProofOfTaxDeferralOrExemption,
        
        [XmlEnum("PropertyInspectionReport")]
        PropertyInspectionReport,
        
        [XmlEnum("PropertyInspectionReportCarpet")]
        PropertyInspectionReportCarpet,
        
        [XmlEnum("PropertyInspectionReportEnvironmental")]
        PropertyInspectionReportEnvironmental,
        
        [XmlEnum("PropertyInspectionReportFootings")]
        PropertyInspectionReportFootings,
        
        [XmlEnum("PropertyInspectionReportFraming")]
        PropertyInspectionReportFraming,
        
        [XmlEnum("PropertyInspectionReportHeating")]
        PropertyInspectionReportHeating,
        
        [XmlEnum("PropertyInspectionReportInsulation")]
        PropertyInspectionReportInsulation,
        
        [XmlEnum("PropertyInspectionReportPest")]
        PropertyInspectionReportPest,
        
        [XmlEnum("PropertyInspectionReportPlumbing")]
        PropertyInspectionReportPlumbing,
        
        [XmlEnum("PropertyInspectionReportRoof")]
        PropertyInspectionReportRoof,
        
        [XmlEnum("PropertyInspectionReportSeptic")]
        PropertyInspectionReportSeptic,
        
        [XmlEnum("PropertyInspectionReportSoil")]
        PropertyInspectionReportSoil,
        
        [XmlEnum("PropertyInspectionReportSoilTreatment")]
        PropertyInspectionReportSoilTreatment,
        
        [XmlEnum("PropertyInspectionReportStructuralEngineering")]
        PropertyInspectionReportStructuralEngineering,
        
        [XmlEnum("PropertyInspectionReportSubterraneanTermite")]
        PropertyInspectionReportSubterraneanTermite,
        
        [XmlEnum("PropertyInspectionReportTermite")]
        PropertyInspectionReportTermite,
        
        [XmlEnum("PropertyInspectionReportWaterHealth")]
        PropertyInspectionReportWaterHealth,
        
        [XmlEnum("PropertyInspectionReportWell")]
        PropertyInspectionReportWell,
        
        [XmlEnum("PropertyInsuranceBinder")]
        PropertyInsuranceBinder,
        
        [XmlEnum("PropertyInsuranceBinderEarthquake")]
        PropertyInsuranceBinderEarthquake,
        
        [XmlEnum("PropertyInsuranceBinderFireAndExtendedCoverage")]
        PropertyInsuranceBinderFireAndExtendedCoverage,
        
        [XmlEnum("PropertyInsuranceBinderFlood")]
        PropertyInsuranceBinderFlood,
        
        [XmlEnum("PropertyInsuranceBinderHazard")]
        PropertyInsuranceBinderHazard,
        
        [XmlEnum("PropertyInsuranceBinderHomeowners")]
        PropertyInsuranceBinderHomeowners,
        
        [XmlEnum("PropertyInsuranceBinderHurricane")]
        PropertyInsuranceBinderHurricane,
        
        [XmlEnum("PropertyInsuranceBinderInsectInfestation")]
        PropertyInsuranceBinderInsectInfestation,
        
        [XmlEnum("PropertyInsuranceBinderLeasehold")]
        PropertyInsuranceBinderLeasehold,
        
        [XmlEnum("PropertyInsuranceBinderMineSubsidence")]
        PropertyInsuranceBinderMineSubsidence,
        
        [XmlEnum("PropertyInsuranceBinderPersonalProperty")]
        PropertyInsuranceBinderPersonalProperty,
        
        [XmlEnum("PropertyInsuranceBinderStorm")]
        PropertyInsuranceBinderStorm,
        
        [XmlEnum("PropertyInsuranceBinderTornado")]
        PropertyInsuranceBinderTornado,
        
        [XmlEnum("PropertyInsuranceBinderVolcano")]
        PropertyInsuranceBinderVolcano,
        
        [XmlEnum("PropertyInsuranceBinderWind")]
        PropertyInsuranceBinderWind,
        
        [XmlEnum("PropertyInsuranceDeclarations")]
        PropertyInsuranceDeclarations,
        
        [XmlEnum("PropertyInsuranceDeclarationsEarthquake")]
        PropertyInsuranceDeclarationsEarthquake,
        
        [XmlEnum("PropertyInsuranceDeclarationsFireAndExtendedCoverage")]
        PropertyInsuranceDeclarationsFireAndExtendedCoverage,
        
        [XmlEnum("PropertyInsuranceDeclarationsFlood")]
        PropertyInsuranceDeclarationsFlood,
        
        [XmlEnum("PropertyInsuranceDeclarationsHazard")]
        PropertyInsuranceDeclarationsHazard,
        
        [XmlEnum("PropertyInsuranceDeclarationsHomeowners")]
        PropertyInsuranceDeclarationsHomeowners,
        
        [XmlEnum("PropertyInsuranceDeclarationsHurricane")]
        PropertyInsuranceDeclarationsHurricane,
        
        [XmlEnum("PropertyInsuranceDeclarationsInsectInfestation")]
        PropertyInsuranceDeclarationsInsectInfestation,
        
        [XmlEnum("PropertyInsuranceDeclarationsLeasehold")]
        PropertyInsuranceDeclarationsLeasehold,
        
        [XmlEnum("PropertyInsuranceDeclarationsMineSubsidence")]
        PropertyInsuranceDeclarationsMineSubsidence,
        
        [XmlEnum("PropertyInsuranceDeclarationsPersonalProperty")]
        PropertyInsuranceDeclarationsPersonalProperty,
        
        [XmlEnum("PropertyInsuranceDeclarationsStorm")]
        PropertyInsuranceDeclarationsStorm,
        
        [XmlEnum("PropertyInsuranceDeclarationsTornado")]
        PropertyInsuranceDeclarationsTornado,
        
        [XmlEnum("PropertyInsuranceDeclarationsVolcano")]
        PropertyInsuranceDeclarationsVolcano,
        
        [XmlEnum("PropertyInsuranceDeclarationsWind")]
        PropertyInsuranceDeclarationsWind,
        
        [XmlEnum("PropertyInsurancePolicy")]
        PropertyInsurancePolicy,
        
        [XmlEnum("PropertyInsurancePolicyEarthquake")]
        PropertyInsurancePolicyEarthquake,
        
        [XmlEnum("PropertyInsurancePolicyFireAndExtendedCoverage")]
        PropertyInsurancePolicyFireAndExtendedCoverage,
        
        [XmlEnum("PropertyInsurancePolicyFlood")]
        PropertyInsurancePolicyFlood,
        
        [XmlEnum("PropertyInsurancePolicyHazard")]
        PropertyInsurancePolicyHazard,
        
        [XmlEnum("PropertyInsurancePolicyHomeowners")]
        PropertyInsurancePolicyHomeowners,
        
        [XmlEnum("PropertyInsurancePolicyHurricane")]
        PropertyInsurancePolicyHurricane,
        
        [XmlEnum("PropertyInsurancePolicyInsectInfestation")]
        PropertyInsurancePolicyInsectInfestation,
        
        [XmlEnum("PropertyInsurancePolicyLeasehold")]
        PropertyInsurancePolicyLeasehold,
        
        [XmlEnum("PropertyInsurancePolicyMineSubsidence")]
        PropertyInsurancePolicyMineSubsidence,
        
        [XmlEnum("PropertyInsurancePolicyPersonalProperty")]
        PropertyInsurancePolicyPersonalProperty,
        
        [XmlEnum("PropertyInsurancePolicyStorm")]
        PropertyInsurancePolicyStorm,
        
        [XmlEnum("PropertyInsurancePolicyTornado")]
        PropertyInsurancePolicyTornado,
        
        [XmlEnum("PropertyInsurancePolicyVolcano")]
        PropertyInsurancePolicyVolcano,
        
        [XmlEnum("PropertyInsurancePolicyWind")]
        PropertyInsurancePolicyWind,
        
        [XmlEnum("PropertyInsuranceRequirementDisclosure")]
        PropertyInsuranceRequirementDisclosure,
        
        [XmlEnum("PurchaseAgreement")]
        PurchaseAgreement,
        
        [XmlEnum("RateChangeNotice")]
        RateChangeNotice,
        
        [XmlEnum("RateLockAgreement")]
        RateLockAgreement,
        
        [XmlEnum("RateLockAgreementBusinessToBusiness")]
        RateLockAgreementBusinessToBusiness,
        
        [XmlEnum("RateLockAgreementBusinessToConsumer")]
        RateLockAgreementBusinessToConsumer,
        
        [XmlEnum("ReaffirmationAgreement")]
        ReaffirmationAgreement,
        
        [XmlEnum("Receipt")]
        Receipt,
        
        [XmlEnum("RecertificationOfFamilyIncomeAndComposition")]
        RecertificationOfFamilyIncomeAndComposition,
        
        [XmlEnum("RecertificationOfFamilyIncomeStatisticalReport")]
        RecertificationOfFamilyIncomeStatisticalReport,
        
        [XmlEnum("Reconveyance")]
        Reconveyance,
        
        [XmlEnum("RefinanceCostEstimateWorksheet")]
        RefinanceCostEstimateWorksheet,
        
        [XmlEnum("RefinanceCostEstimateWorksheetHECM")]
        RefinanceCostEstimateWorksheetHECM,
        
        [XmlEnum("RelocationBenefitsPackage")]
        RelocationBenefitsPackage,
        
        [XmlEnum("RelocationBuyoutAgreement")]
        RelocationBuyoutAgreement,
        
        [XmlEnum("RemittanceDeliveryReceipt")]
        RemittanceDeliveryReceipt,
        
        [XmlEnum("RentalAgreement")]
        RentalAgreement,
        
        [XmlEnum("RentalIncomeAnalysisStatement")]
        RentalIncomeAnalysisStatement,
        
        [XmlEnum("RepaymentPlanAgreement")]
        RepaymentPlanAgreement,
        
        [XmlEnum("RequestForCopyOfTaxReturn")]
        RequestForCopyOfTaxReturn,
        
        [XmlEnum("RequestForCopyOfTaxReturnIRS4506")]
        RequestForCopyOfTaxReturnIRS4506,
        
        [XmlEnum("RequestForCopyOfTaxReturnIRS4506T")]
        RequestForCopyOfTaxReturnIRS4506T,
        
        [XmlEnum("RequestForCopyOfTaxReturnIRS4506TEZ")]
        RequestForCopyOfTaxReturnIRS4506TEZ,
        
        [XmlEnum("RequestForNoticeOfDefault")]
        RequestForNoticeOfDefault,
        
        [XmlEnum("ResidualIncomeAnalysisWorksheet")]
        ResidualIncomeAnalysisWorksheet,
        
        [XmlEnum("ResolutionOfConsumerComplaint")]
        ResolutionOfConsumerComplaint,
        
        [XmlEnum("RetirementAccountStatement")]
        RetirementAccountStatement,
        
        [XmlEnum("RiskBasedPricingNotice")]
        RiskBasedPricingNotice,
        
        [XmlEnum("RiskBasedPricingNoticeAccountReviewNotice")]
        RiskBasedPricingNoticeAccountReviewNotice,
        
        [XmlEnum("RiskBasedPricingNoticeCreditScoreDisclosureException")]
        RiskBasedPricingNoticeCreditScoreDisclosureException,
        
        [XmlEnum("RiskBasedPricingNoticeCreditScoreDisclosureNoResidentialSecured")]
        RiskBasedPricingNoticeCreditScoreDisclosureNoResidentialSecured,
        
        [XmlEnum("RiskBasedPricingNoticeCreditScoreDisclosureNoScore")]
        RiskBasedPricingNoticeCreditScoreDisclosureNoScore,
        
        [XmlEnum("RiskBasedPricingNoticeGeneralNotice")]
        RiskBasedPricingNoticeGeneralNotice,
        
        [XmlEnum("RoadMaintenanceAgreement")]
        RoadMaintenanceAgreement,
        
        [XmlEnum("SatisfactionOfJudgment")]
        SatisfactionOfJudgment,
        
        [XmlEnum("SatisfactionOfSecurityInstrument")]
        SatisfactionOfSecurityInstrument,
        
        [XmlEnum("SatisfactionOfSecurityInstrumentLienRelease")]
        SatisfactionOfSecurityInstrumentLienRelease,
        
        [XmlEnum("SatisfactionOfSecurityInstrumentSatisfactionOfDeedOfTrust")]
        SatisfactionOfSecurityInstrumentSatisfactionOfDeedOfTrust,
        
        [XmlEnum("SatisfactionOfSecurityInstrumentSatisfactionOfMortgage")]
        SatisfactionOfSecurityInstrumentSatisfactionOfMortgage,
        
        [XmlEnum("SCRANoticeDisclosure")]
        SCRANoticeDisclosure,
        
        [XmlEnum("Section32DisclosureNotice")]
        Section32DisclosureNotice,
        
        [XmlEnum("SecurityInstrument")]
        SecurityInstrument,
        
        [XmlEnum("SecurityInstrumentDeedOfTrust")]
        SecurityInstrumentDeedOfTrust,
        
        [XmlEnum("SecurityInstrumentMortgage")]
        SecurityInstrumentMortgage,
        
        [XmlEnum("SecurityInstrumentAddendum")]
        SecurityInstrumentAddendum,
        
        [XmlEnum("SecurityInstrumentAddendumFixedRateOption")]
        SecurityInstrumentAddendumFixedRateOption,
        
        [XmlEnum("SecurityInstrumentModification")]
        SecurityInstrumentModification,
        
        [XmlEnum("SecurityInstrumentModificationConsolidationAgreement")]
        SecurityInstrumentModificationConsolidationAgreement,
        
        [XmlEnum("SecurityInstrumentModificationConsolidationExtensionModificationAgreement")]
        SecurityInstrumentModificationConsolidationExtensionModificationAgreement,
        
        [XmlEnum("SecurityInstrumentModificationModificationAgreement")]
        SecurityInstrumentModificationModificationAgreement,
        
        [XmlEnum("SecurityInstrumentRider")]
        SecurityInstrumentRider,
        
        [XmlEnum("SecurityInstrumentRiderAffordableMeritRate")]
        SecurityInstrumentRiderAffordableMeritRate,
        
        [XmlEnum("SecurityInstrumentRiderARM")]
        SecurityInstrumentRiderARM,
        
        [XmlEnum("SecurityInstrumentRiderBalloon")]
        SecurityInstrumentRiderBalloon,
        
        [XmlEnum("SecurityInstrumentRiderBeneficiary")]
        SecurityInstrumentRiderBeneficiary,
        
        [XmlEnum("SecurityInstrumentRiderBiweekly")]
        SecurityInstrumentRiderBiweekly,
        
        [XmlEnum("SecurityInstrumentRiderCondominium")]
        SecurityInstrumentRiderCondominium,
        
        [XmlEnum("SecurityInstrumentRiderConstruction")]
        SecurityInstrumentRiderConstruction,
        
        [XmlEnum("SecurityInstrumentRiderGEM")]
        SecurityInstrumentRiderGEM,
        
        [XmlEnum("SecurityInstrumentRiderGPM")]
        SecurityInstrumentRiderGPM,
        
        [XmlEnum("SecurityInstrumentRiderHomesteadExemption")]
        SecurityInstrumentRiderHomesteadExemption,
        
        [XmlEnum("SecurityInstrumentRiderInterestOnly")]
        SecurityInstrumentRiderInterestOnly,
        
        [XmlEnum("SecurityInstrumentRiderInterVivosRevocableTrust")]
        SecurityInstrumentRiderInterVivosRevocableTrust,
        
        [XmlEnum("SecurityInstrumentRiderInvestor")]
        SecurityInstrumentRiderInvestor,
        
        [XmlEnum("SecurityInstrumentRiderLandTrust")]
        SecurityInstrumentRiderLandTrust,
        
        [XmlEnum("SecurityInstrumentRiderLeasehold")]
        SecurityInstrumentRiderLeasehold,
        
        [XmlEnum("SecurityInstrumentRiderManufacturedHousing")]
        SecurityInstrumentRiderManufacturedHousing,
        
        [XmlEnum("SecurityInstrumentRiderMERSRegistry")]
        SecurityInstrumentRiderMERSRegistry,
        
        [XmlEnum("SecurityInstrumentRiderNonOwner")]
        SecurityInstrumentRiderNonOwner,
        
        [XmlEnum("SecurityInstrumentRiderOneToFourFamily")]
        SecurityInstrumentRiderOneToFourFamily,
        
        [XmlEnum("SecurityInstrumentRiderOwnerOccupancy")]
        SecurityInstrumentRiderOwnerOccupancy,
        
        [XmlEnum("SecurityInstrumentRiderPrepayment")]
        SecurityInstrumentRiderPrepayment,
        
        [XmlEnum("SecurityInstrumentRiderPUD")]
        SecurityInstrumentRiderPUD,
        
        [XmlEnum("SecurityInstrumentRiderRateImprovement")]
        SecurityInstrumentRiderRateImprovement,
        
        [XmlEnum("SecurityInstrumentRiderRehabilitation")]
        SecurityInstrumentRiderRehabilitation,
        
        [XmlEnum("SecurityInstrumentRiderRenewalAndExtension")]
        SecurityInstrumentRiderRenewalAndExtension,
        
        [XmlEnum("SecurityInstrumentRiderSecondHome")]
        SecurityInstrumentRiderSecondHome,
        
        [XmlEnum("SecurityInstrumentRiderSecondLien")]
        SecurityInstrumentRiderSecondLien,
        
        [XmlEnum("SecurityInstrumentRiderTaxExemptFinancing")]
        SecurityInstrumentRiderTaxExemptFinancing,
        
        [XmlEnum("SecurityInstrumentRiderVA")]
        SecurityInstrumentRiderVA,
        
        [XmlEnum("SecurityInstrumentRiderVeteransLandBoard")]
        SecurityInstrumentRiderVeteransLandBoard,
        
        [XmlEnum("SecurityInstrumentRiderWaiverOfBorrowersRights")]
        SecurityInstrumentRiderWaiverOfBorrowersRights,
        
        [XmlEnum("SecurityInstrumentRiderWaiverOfDowerRights")]
        SecurityInstrumentRiderWaiverOfDowerRights,
        
        [XmlEnum("SellerLeadHazardDisclosureAndAcknowledgment")]
        SellerLeadHazardDisclosureAndAcknowledgment,
        
        [XmlEnum("ServicingDisclosureStatement")]
        ServicingDisclosureStatement,
        
        [XmlEnum("ServicingTransferStatement")]
        ServicingTransferStatement,
        
        [XmlEnum("SettlementStatement")]
        SettlementStatement,
        
        [XmlEnum("SettlementStatementHUD1")]
        SettlementStatementHUD1,
        
        [XmlEnum("SettlementStatementHUD1A")]
        SettlementStatementHUD1A,
        
        [XmlEnum("ShortSaleAgreement")]
        ShortSaleAgreement,
        
        [XmlEnum("ShortSaleProcessChecklist")]
        ShortSaleProcessChecklist,
        
        [XmlEnum("SocialSecurityAwardLetter")]
        SocialSecurityAwardLetter,
        
        [XmlEnum("SolicitationLetter")]
        SolicitationLetter,
        
        [XmlEnum("StandardFloodHazardDetermination")]
        StandardFloodHazardDetermination,
        
        [XmlEnum("StateIdentification")]
        StateIdentification,
        
        [XmlEnum("StatementOfBorrowerBenefit")]
        StatementOfBorrowerBenefit,
        
        [XmlEnum("StockCertificate")]
        StockCertificate,
        
        [XmlEnum("SubordinationAgreement")]
        SubordinationAgreement,
        
        [XmlEnum("SubsidyAgreement")]
        SubsidyAgreement,
        
        [XmlEnum("SubstitutionOfTrustee")]
        SubstitutionOfTrustee,
        
        [XmlEnum("Survey")]
        Survey,
        
        [XmlEnum("SurveyAffidavit")]
        SurveyAffidavit,
        
        [XmlEnum("TaxAssessmentValueIndication")]
        TaxAssessmentValueIndication,
        
        [XmlEnum("TaxAuthorization")]
        TaxAuthorization,
        
        [XmlEnum("TaxCertificate")]
        TaxCertificate,
        
        [XmlEnum("TaxLien")]
        TaxLien,
        
        [XmlEnum("TaxLienFederal")]
        TaxLienFederal,
        
        [XmlEnum("TaxLienLocal")]
        TaxLienLocal,
        
        [XmlEnum("TaxLienState")]
        TaxLienState,
        
        [XmlEnum("TaxLienRelease")]
        TaxLienRelease,
        
        [XmlEnum("TaxLienReleaseFederal")]
        TaxLienReleaseFederal,
        
        [XmlEnum("TaxLienReleaseLocal")]
        TaxLienReleaseLocal,
        
        [XmlEnum("TaxLienReleaseState")]
        TaxLienReleaseState,
        
        [XmlEnum("TaxpayerIdentification")]
        TaxpayerIdentification,
        
        [XmlEnum("TaxpayerIdentificationNumeroDeIdentificacionTributaria")]
        TaxpayerIdentificationNumeroDeIdentificacionTributaria,
        
        [XmlEnum("TaxReturn")]
        TaxReturn,
        
        [XmlEnum("TenYearWarranty")]
        TenYearWarranty,
        
        [XmlEnum("ThirdPartyEmploymentStatement")]
        ThirdPartyEmploymentStatement,
        
        [XmlEnum("TILDisclosure")]
        TILDisclosure,
        
        [XmlEnum("TitleAbstract")]
        TitleAbstract,
        
        [XmlEnum("TitleCommitment")]
        TitleCommitment,
        
        [XmlEnum("TitleInsuranceEndorsement")]
        TitleInsuranceEndorsement,
        
        [XmlEnum("TitleInsurancePolicy")]
        TitleInsurancePolicy,
        
        [XmlEnum("TrialLoanModificationAgreement")]
        TrialLoanModificationAgreement,
        
        [XmlEnum("TrialPeriodPlanNotice")]
        TrialPeriodPlanNotice,
        
        [XmlEnum("TrustAgreement")]
        TrustAgreement,
        
        [XmlEnum("UCC1Statement")]
        UCC1Statement,
        
        [XmlEnum("UCC3Statement")]
        UCC3Statement,
        
        [XmlEnum("UnderwritingConditions")]
        UnderwritingConditions,
        
        [XmlEnum("UnderwritingConditionsMIApplication")]
        UnderwritingConditionsMIApplication,
        
        [XmlEnum("UnderwritingTransmittal")]
        UnderwritingTransmittal,
        
        [XmlEnum("UnusedEscrowFundsDistributionAuthorization")]
        UnusedEscrowFundsDistributionAuthorization,
        
        [XmlEnum("UtilityBill")]
        UtilityBill,
        
        [XmlEnum("VAAcknowledgmentOfNoInspection")]
        VAAcknowledgmentOfNoInspection,
        
        [XmlEnum("VACertificateOfEligibility")]
        VACertificateOfEligibility,
        
        [XmlEnum("VACertificateOfReasonableValue")]
        VACertificateOfReasonableValue,
        
        [XmlEnum("VACollectionPolicyNotice")]
        VACollectionPolicyNotice,
        
        [XmlEnum("VAForeclosureBidLetter")]
        VAForeclosureBidLetter,
        
        [XmlEnum("VAFundingFeeNotice")]
        VAFundingFeeNotice,
        
        [XmlEnum("VAInterestRateReductionRefinancingLoanWorksheet")]
        VAInterestRateReductionRefinancingLoanWorksheet,
        
        [XmlEnum("VALoanAnalysis")]
        VALoanAnalysis,
        
        [XmlEnum("VALoanSummarySheet")]
        VALoanSummarySheet,
        
        [XmlEnum("ValuationUpdate")]
        ValuationUpdate,
        
        [XmlEnum("VARateReductionRefinanceInformationAndAgreement")]
        VARateReductionRefinanceInformationAndAgreement,
        
        [XmlEnum("VAReportAndCertificationOfLoanDisbursement")]
        VAReportAndCertificationOfLoanDisbursement,
        
        [XmlEnum("VARequestForCertificationOfEligibilityForHomeLoanBenefit")]
        VARequestForCertificationOfEligibilityForHomeLoanBenefit,
        
        [XmlEnum("VAVerificationOfBenefitRelatedIndebtedness")]
        VAVerificationOfBenefitRelatedIndebtedness,
        
        [XmlEnum("VerificationOfAddress")]
        VerificationOfAddress,
        
        [XmlEnum("VerificationOfCredit")]
        VerificationOfCredit,
        
        [XmlEnum("VerificationOfDependentCare")]
        VerificationOfDependentCare,
        
        [XmlEnum("VerificationOfDeposit")]
        VerificationOfDeposit,
        
        [XmlEnum("VerificationOfEmployment")]
        VerificationOfEmployment,
        
        [XmlEnum("VerificationOfMERSRegistry")]
        VerificationOfMERSRegistry,
        
        [XmlEnum("VerificationOfMortgageOrRent")]
        VerificationOfMortgageOrRent,
        
        [XmlEnum("VerificationOfSecurities")]
        VerificationOfSecurities,
        
        [XmlEnum("VerificationOfSSN")]
        VerificationOfSSN,
        
        [XmlEnum("Visa")]
        Visa,
        
        [XmlEnum("VolunteerEscrowPrepaymentDesignation")]
        VolunteerEscrowPrepaymentDesignation,
        
        [XmlEnum("VoterRegistration")]
        VoterRegistration,
        
        [XmlEnum("WarrantyOfCompletionOfConstruction")]
        WarrantyOfCompletionOfConstruction,
        
        [XmlEnum("WireInstructions")]
        WireInstructions,
        
        [XmlEnum("WireTransferAuthorization")]
        WireTransferAuthorization,
        
        [XmlEnum("WireTransferConfirmation")]
        WireTransferConfirmation,
        
        [XmlEnum("Worksheet")]
        Worksheet,
    }
}
