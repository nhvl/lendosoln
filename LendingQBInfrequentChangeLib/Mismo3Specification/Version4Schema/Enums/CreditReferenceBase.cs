namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum CreditReferenceBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("MortgageCreditOnly")]
        MortgageCreditOnly,
        
        [XmlEnum("NonCreditPayment")]
        NonCreditPayment,
        
        [XmlEnum("NonTraditional")]
        NonTraditional,
        
        [XmlEnum("Other")]
        Other,
    }
}
