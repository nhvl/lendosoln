namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum OfferContingencyBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Appraisal")]
        Appraisal,
        
        [XmlEnum("BuyerFinancingApproval")]
        BuyerFinancingApproval,
        
        [XmlEnum("EnvironmentalRemediation")]
        EnvironmentalRemediation,
        
        [XmlEnum("HomeInspection")]
        HomeInspection,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("SaleOfBuyersExistingHome")]
        SaleOfBuyersExistingHome,
        
        [XmlEnum("SubjectToRepairs")]
        SubjectToRepairs,
        
        [XmlEnum("ThirdPartyApproval")]
        ThirdPartyApproval,
    }
}
