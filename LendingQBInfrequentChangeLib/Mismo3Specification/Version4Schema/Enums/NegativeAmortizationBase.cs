namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum NegativeAmortizationBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("PotentialNegativeAmortization")]
        PotentialNegativeAmortization,
        
        [XmlEnum("ScheduledNegativeAmortization")]
        ScheduledNegativeAmortization,
    }
}
