namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum IntegratedDisclosureCashToCloseItemPaymentBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("FromBorrower")]
        FromBorrower,
        
        [XmlEnum("ToBorrower")]
        ToBorrower,
    }
}
