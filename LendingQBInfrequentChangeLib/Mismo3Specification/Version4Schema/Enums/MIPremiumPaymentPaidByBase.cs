namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum MIPremiumPaymentPaidByBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Broker")]
        Broker,
        
        [XmlEnum("Buyer")]
        Buyer,
        
        [XmlEnum("Correspondent")]
        Correspondent,
        
        [XmlEnum("Lender")]
        Lender,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Seller")]
        Seller,
        
        [XmlEnum("ThirdParty")]
        ThirdParty,
    }
}
