namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum UnparsedLegalDescriptionBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Long")]
        Long,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Short")]
        Short,
    }
}
