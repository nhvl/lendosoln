namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PurchaseCreditSourceBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("BorrowerPaidOutsideClosing")]
        BorrowerPaidOutsideClosing,
        
        [XmlEnum("BuilderDeveloper")]
        BuilderDeveloper,
        
        [XmlEnum("Employer")]
        Employer,
        
        [XmlEnum("FederalAgency")]
        FederalAgency,
        
        [XmlEnum("Lender")]
        Lender,
        
        [XmlEnum("LocalAgency")]
        LocalAgency,
        
        [XmlEnum("NonParentRelative")]
        NonParentRelative,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Parent")]
        Parent,
        
        [XmlEnum("PropertySeller")]
        PropertySeller,
        
        [XmlEnum("RealEstateAgent")]
        RealEstateAgent,
        
        [XmlEnum("StateAgency")]
        StateAgency,
        
        [XmlEnum("UnrelatedFriend")]
        UnrelatedFriend,
    }
}
