namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum HMDAPurchaserBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AffiliateInstitution")]
        AffiliateInstitution,
        
        [XmlEnum("CommercialOrSavingsBank")]
        CommercialOrSavingsBank,
        
        [XmlEnum("FannieMae")]
        FannieMae,
        
        [XmlEnum("FarmerMac")]
        FarmerMac,
        
        [XmlEnum("FinancialInstitution")]
        FinancialInstitution,
        
        [XmlEnum("FreddieMac")]
        FreddieMac,
        
        [XmlEnum("GinnieMae")]
        GinnieMae,
        
        [XmlEnum("MortgageCompany")]
        MortgageCompany,
        
        [XmlEnum("NotApplicable")]
        NotApplicable,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PrivateSecuritization")]
        PrivateSecuritization,
    }
}
