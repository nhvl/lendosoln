namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum AnalysisComponentBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Feature")]
        Feature,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Room")]
        Room,
        
        [XmlEnum("StructuralElement")]
        StructuralElement,
    }
}
