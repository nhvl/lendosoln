namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PaymentBillingStatementFrequencyBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Annual")]
        Annual,
        
        [XmlEnum("Monthly")]
        Monthly,
        
        [XmlEnum("Quarterly")]
        Quarterly,
        
        [XmlEnum("Semiannual")]
        Semiannual,
    }
}
