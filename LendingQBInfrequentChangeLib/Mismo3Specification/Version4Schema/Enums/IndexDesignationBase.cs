namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum IndexDesignationBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Alternate")]
        Alternate,
        
        [XmlEnum("Primary")]
        Primary,
    }
}
