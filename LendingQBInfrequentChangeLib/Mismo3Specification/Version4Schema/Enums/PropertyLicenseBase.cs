namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PropertyLicenseBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Rental")]
        Rental,
        
        [XmlEnum("RightToUse")]
        RightToUse,
    }
}
