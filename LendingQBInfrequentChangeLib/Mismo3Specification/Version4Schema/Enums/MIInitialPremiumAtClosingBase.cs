namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum MIInitialPremiumAtClosingBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Deferred")]
        Deferred,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Prepaid")]
        Prepaid,
    }
}
