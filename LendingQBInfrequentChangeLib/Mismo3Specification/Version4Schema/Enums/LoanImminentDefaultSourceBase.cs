namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum LoanImminentDefaultSourceBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("InvestorModel")]
        InvestorModel,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("ServicerEvaluation")]
        ServicerEvaluation,
    }
}
