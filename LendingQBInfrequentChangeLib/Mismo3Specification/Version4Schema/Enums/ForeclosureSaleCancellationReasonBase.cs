namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ForeclosureSaleCancellationReasonBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("ExcessiveDelays")]
        ExcessiveDelays,
        
        [XmlEnum("FilingErrors")]
        FilingErrors,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Payoff")]
        Payoff,
        
        [XmlEnum("Workout")]
        Workout,
    }
}
