namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum MIPremiumPaymentBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Escrowed")]
        Escrowed,
        
        [XmlEnum("Financed")]
        Financed,
        
        [XmlEnum("Other")]
        Other,
    }
}
