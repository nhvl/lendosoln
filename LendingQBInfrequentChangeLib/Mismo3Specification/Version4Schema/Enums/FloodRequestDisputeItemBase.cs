namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum FloodRequestDisputeItemBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("CBRAStatus")]
        CBRAStatus,
        
        [XmlEnum("CommunityNumber")]
        CommunityNumber,
        
        [XmlEnum("CommunityStatus")]
        CommunityStatus,
        
        [XmlEnum("MapDate")]
        MapDate,
        
        [XmlEnum("MapPanel")]
        MapPanel,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Zone")]
        Zone,
    }
}
