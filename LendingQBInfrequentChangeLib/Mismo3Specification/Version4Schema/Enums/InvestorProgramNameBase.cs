namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum InvestorProgramNameBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Calpers")]
        Calpers,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PaymentPower")]
        PaymentPower,
        
        [XmlEnum("PrimeRatePlus")]
        PrimeRatePlus,
        
        [XmlEnum("SettleAmerica")]
        SettleAmerica,
    }
}
