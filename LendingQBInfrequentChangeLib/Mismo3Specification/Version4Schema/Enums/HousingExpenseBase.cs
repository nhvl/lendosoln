namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum HousingExpenseBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Cable")]
        Cable,
        
        [XmlEnum("Electricity")]
        Electricity,
        
        [XmlEnum("EscrowShortage")]
        EscrowShortage,
        
        [XmlEnum("FirstMortgagePITI")]
        FirstMortgagePITI,
        
        [XmlEnum("FirstMortgagePrincipalAndInterest")]
        FirstMortgagePrincipalAndInterest,
        
        [XmlEnum("FloodInsurance")]
        FloodInsurance,
        
        [XmlEnum("GroundRent")]
        GroundRent,
        
        [XmlEnum("Heating")]
        Heating,
        
        [XmlEnum("HomeownersAssociationDuesAndCondominiumFees")]
        HomeownersAssociationDuesAndCondominiumFees,
        
        [XmlEnum("HomeownersInsurance")]
        HomeownersInsurance,
        
        [XmlEnum("LeaseholdPayments")]
        LeaseholdPayments,
        
        [XmlEnum("MaintenanceAndMiscellaneous")]
        MaintenanceAndMiscellaneous,
        
        [XmlEnum("MIPremium")]
        MIPremium,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("OtherMortgageLoanPrincipalAndInterest")]
        OtherMortgageLoanPrincipalAndInterest,
        
        [XmlEnum("OtherMortgageLoanPrincipalInterestTaxesAndInsurance")]
        OtherMortgageLoanPrincipalInterestTaxesAndInsurance,
        
        [XmlEnum("RealEstateTax")]
        RealEstateTax,
        
        [XmlEnum("Rent")]
        Rent,
        
        [XmlEnum("SupplementalPropertyInsurance")]
        SupplementalPropertyInsurance,
        
        [XmlEnum("Telephone")]
        Telephone,
        
        [XmlEnum("Utilities")]
        Utilities,
    }
}
