namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PropertyCurrentUsageBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Investment")]
        Investment,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PrimaryResidence")]
        PrimaryResidence,
        
        [XmlEnum("SecondHome")]
        SecondHome,
    }
}
