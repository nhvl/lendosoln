namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ReasonForMIPolicyCancellationBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("ClaimDenial")]
        ClaimDenial,
        
        [XmlEnum("ClaimPaid")]
        ClaimPaid,
        
        [XmlEnum("CoverageNoLongerRequired")]
        CoverageNoLongerRequired,
        
        [XmlEnum("LoanPaidInFull")]
        LoanPaidInFull,
        
        [XmlEnum("LoanRefinanced")]
        LoanRefinanced,
        
        [XmlEnum("NonPaymentOfPremium")]
        NonPaymentOfPremium,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Rescinded")]
        Rescinded,
    }
}
