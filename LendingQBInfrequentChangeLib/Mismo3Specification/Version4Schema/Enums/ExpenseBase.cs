namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum ExpenseBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Alimony")]
        Alimony,
        
        [XmlEnum("CarMaintenance")]
        CarMaintenance,
        
        [XmlEnum("CharitableContributions")]
        CharitableContributions,
        
        [XmlEnum("ChildCare")]
        ChildCare,
        
        [XmlEnum("ChildSupport")]
        ChildSupport,
        
        [XmlEnum("Clothing")]
        Clothing,
        
        [XmlEnum("DryCleaning")]
        DryCleaning,
        
        [XmlEnum("Entertainment")]
        Entertainment,
        
        [XmlEnum("GroceryToiletry")]
        GroceryToiletry,
        
        [XmlEnum("HealthInsurance")]
        HealthInsurance,
        
        [XmlEnum("JobRelatedExpenses")]
        JobRelatedExpenses,
        
        [XmlEnum("Medical")]
        Medical,
        
        [XmlEnum("MiscellaneousLivingExpenses")]
        MiscellaneousLivingExpenses,
        
        [XmlEnum("NetRentalExpense")]
        NetRentalExpense,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PayrollInsuranceDeduction")]
        PayrollInsuranceDeduction,
        
        [XmlEnum("PayrollMiscellaneousDeductions")]
        PayrollMiscellaneousDeductions,
        
        [XmlEnum("PayrollProfitSharingDeduction")]
        PayrollProfitSharingDeduction,
        
        [XmlEnum("PayrollRetirementDeduction")]
        PayrollRetirementDeduction,
        
        [XmlEnum("PayrollTaxDeduction")]
        PayrollTaxDeduction,
        
        [XmlEnum("SeparateMaintenanceExpense")]
        SeparateMaintenanceExpense,
        
        [XmlEnum("UnionDues")]
        UnionDues,
    }
}
