namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum PropertyPartialInterestBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("AirRights")]
        AirRights,
        
        [XmlEnum("Easement")]
        Easement,
        
        [XmlEnum("GroundLease")]
        GroundLease,
        
        [XmlEnum("HuntingRights")]
        HuntingRights,
        
        [XmlEnum("MineralRights")]
        MineralRights,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("WaterRights")]
        WaterRights,
    }
}
