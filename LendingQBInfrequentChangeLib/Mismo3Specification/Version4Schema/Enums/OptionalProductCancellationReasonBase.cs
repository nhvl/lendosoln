namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum OptionalProductCancellationReasonBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Bankruptcy")]
        Bankruptcy,
        
        [XmlEnum("Foreclosure")]
        Foreclosure,
        
        [XmlEnum("NonPaymentOfPremium")]
        NonPaymentOfPremium,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PolicyOrProductExpiration")]
        PolicyOrProductExpiration,
    }
}
