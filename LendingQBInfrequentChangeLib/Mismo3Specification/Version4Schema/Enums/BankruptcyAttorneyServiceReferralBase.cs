namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum BankruptcyAttorneyServiceReferralBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("CompleteProcessReferral")]
        CompleteProcessReferral,
        
        [XmlEnum("DebtBifurcation")]
        DebtBifurcation,
        
        [XmlEnum("MotionForRelief")]
        MotionForRelief,
        
        [XmlEnum("MotionToValue")]
        MotionToValue,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("PlanObjection")]
        PlanObjection,
        
        [XmlEnum("PlanReview")]
        PlanReview,
        
        [XmlEnum("ProofOfClaim")]
        ProofOfClaim,
    }
}
