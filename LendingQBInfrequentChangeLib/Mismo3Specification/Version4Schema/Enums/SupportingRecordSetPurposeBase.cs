namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum SupportingRecordSetPurposeBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Acknowledgement")]
        Acknowledgement,
        
        [XmlEnum("FollowUpRequest")]
        FollowUpRequest,
        
        [XmlEnum("FollowUpResponse")]
        FollowUpResponse,
        
        [XmlEnum("InitiatorCorrespondence")]
        InitiatorCorrespondence,
        
        [XmlEnum("Other")]
        Other,
        
        [XmlEnum("Research")]
        Research,
        
        [XmlEnum("Resolution")]
        Resolution,
    }
}
