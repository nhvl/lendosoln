namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public enum MIDecisionBase
    {
        [XmlEnum("")]
        Blank,
    
        [XmlEnum("Approved")]
        Approved,
        
        [XmlEnum("ApprovedAfterReevaluation")]
        ApprovedAfterReevaluation,
        
        [XmlEnum("Canceled")]
        Canceled,
        
        [XmlEnum("ConditionedApproval")]
        ConditionedApproval,
        
        [XmlEnum("Declined")]
        Declined,
        
        [XmlEnum("Suspended")]
        Suspended,
    }
}
