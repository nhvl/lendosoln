﻿namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A data type of Amount SHOULD identify a number value that is an amount of money and MAY have a CurrencyURI attribute. It SHALL NOT contain any punctuation other than the decimal point or sign values. The decimal point is ALWAYS the US nationalization character (.) in the designated character encoding (UTF-8 assumed when not stated)
    /// It SHOULD be able to contain at least 18 digits.
    /// EXAMPLE: An Unpaid Principle Balance Amount of $100,000.12 would be expressed as "100000.12".
    /// </summary>
    public partial class MISMOAmount : MISMOData
    {
        /// <summary>
        /// Gets or sets the currency the value represents.
        /// </summary>
        [XmlAttribute(AttributeName = "CurrencyURI", DataType = "anyURI")]
        public string CurrencyUri { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the currency URI has a value to serialize.
        /// </summary>
        [XmlIgnore]
        public bool CurrencyUriSpecified
        {
            get { return !string.IsNullOrEmpty(this.CurrencyUri); }
            set { }
        }
    }
}
