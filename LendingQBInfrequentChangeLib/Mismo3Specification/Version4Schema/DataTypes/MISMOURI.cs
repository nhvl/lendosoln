﻿namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A data type of URI SHOULD identify a data point that represents a Uniform Resource Identifier that conforms to both the generic syntax of RFC 2396 as amended by RFC 2732 and the specific syntax of the scheme identified by the first component. Note that a URI that is in URL format does not necessarily provide access to a resource at that location.
    /// </summary>
    /// <example>
    /// A Registered Domain Name URI for My Company could be expressed in the http scheme as "http://MyCompany.com".
    /// </example>
    public partial class MISMOURI : MISMOData
    {
        /// <summary>
        /// Gets or sets the URI.
        /// </summary>
        [XmlText(DataType = "anyURI")]
        public override string Value { get; set; }
    }
}
