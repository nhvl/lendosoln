﻿namespace Mismo3Specification.Version4Schema
{
    /// <summary>
    /// A data type of Xpath SHOULD identify a data point that is a string that complies with a version of the W3C Xpath Language to identify content in an XML document instance.
    /// </summary>
    /// <example>
    /// A Data Item Change Xpath to change the Data Version could be expressed as "//MESSAGE/ABOUT_VERSIONS/ABOUT_VERSION[AboutVersionIdentifier[@IdentifierOwnerURI='http://my.org'] = '3.2']/DataVersionIdentifier"."
    /// </example>
    public partial class MISMOXPath : MISMOData
    {
    }
}
