﻿namespace Mismo3Specification.Version4Schema
{
    /// <summary>
    /// A data type of Date SHOULD identify a data point that represents numerically a specific year, month and year, or day month and year of the Gregorian Calendar.
    /// </summary>
    /// <remarks>
    /// Full dates MUST be represented in the extended format CCYY-MM-DD.
    /// CCYY is a four-digit year, "0001" through "9999".
    /// MM is a two-digit month of that year, "01" through "12".
    /// DD is a two-digit day of that month, "01" through "31".
    /// Padded zeros must be used.
    /// - is the required separator between CCYY-MM-DD.
    /// If the day or month and day are dropped, the preceding separator is also dropped.
    /// An optional trailing time zone indicator is permitted using either Z (for Coordinated Universal Time UTC) or an offset from UTC using +hh:mm or -hh:mm as in Datetime.
    /// Note that negative or extended (more than 4 digit) years are not permitted.
    /// </remarks>
    /// <example>A Next Escrow Review Date that discloses the year and month would be expressed as "2010-06".</example>
    public partial class MISMODate : MISMOData
    {
    }
}