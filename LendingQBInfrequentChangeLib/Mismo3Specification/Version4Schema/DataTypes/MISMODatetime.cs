﻿namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A data type of date/time SHOULD identify a data point that represents numerically a specific day month and year of the Gregorian Calendar plus a time of day expressed in hours, minutes and seconds as related to Coordinated Universal Time (UTC) and MAY have an Ignore Time Segment Indicator attribute.
    /// </summary>
    /// <remarks>
    /// Datetimes MUST be represented in the extended format CCYY-MM-DDThh:mm:ssZ
    /// CCYY is a four-digit year, "0000" through "9999"
    /// MM is a two-digit month of that year, "01" through "12"
    /// DD is a two-digit day of that month, "01" through "31"
    /// - is the required separator between CCYY-MM-DD
    /// T is the required separator between the date and the time groupings
    /// hh is a two-digit hour of that day, "00" through "23"
    /// mm is a two-digit minute of that hour, "00" through "59"
    /// ss is a two-digit second of that minute, "00" through "59"
    /// : is the required separator between hh:mm:ss
    /// Z is a required time zone indicator using either Z (for Coordinated Universal Time UTC) or an offset from UTC using +hh:mm or -hh:mm
    /// Padded leading zeros must be used throughout.
    /// An optional fractional seconds (decimal point and up to 9 digits) is permitted between the seconds and the time zone.
    /// Note that negative or extended (more than 4 digit) years are not permitted.
    /// </remarks>
    /// <example>An Event Datetime would be expressed as "2010-06-29T14;24:13.124-05:00".</example>
    public partial class MISMODatetime : MISMOData
    {
        /// <summary>
        /// Gets or sets a value indicating whether the Ignore Time Segment indicator is set.
        /// </summary>
        /// <remarks>
        /// Set to true if an accurate time segment is not provided.
        /// </remarks>
        [XmlIgnore]
        public bool IgnoreTimeSegmentIndicator { get; set; }

        /// <summary>
        /// Gets or sets the Ignore Time Segment Indicator member as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the ignore time segment indicator as a string for serialization.</value>
        [XmlAttribute(AttributeName = "IgnoreTimeSegmentIndicator")]
        public string IgnoreTimeSegmentIndicatorStringified
        {
            get { return Mismo3Utilities.ConvertBoolToString(this.IgnoreTimeSegmentIndicator); }
            set { }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the Ignore Time Segment Indicator has a value to serialize.
        /// </summary>
        [XmlIgnore]
        public bool IgnoreTimeSegmentIndicatorStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.IgnoreTimeSegmentIndicatorStringified); }
            set { }
        }
    }
}
