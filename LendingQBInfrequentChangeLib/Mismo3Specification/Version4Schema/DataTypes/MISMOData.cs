﻿namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// An abstract MISMO-serializable class from which to derive MISMO data containers. Provides a sensitive indicator and a value.
    /// </summary>
    public abstract class MISMOData
    {
        /// <summary>
        /// Gets or sets the data value.
        /// </summary>
        /// <value>Gets the data value as a string for serialization.</value>
        [XmlText(Type = typeof(string))]
        public virtual string Value { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the information in this element is sensitive.
        /// </summary>
        /// <value>A boolean indicating whether the information in this data point is sensitive.</value>
        [XmlIgnore]
        public bool? IsSensitive { get; set; }

        /// <summary>
        /// Gets or sets the Sensitive Information indicator as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the sensitive indicator property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SensitiveIndicator")]
        public string SensitiveIndicator
        {
            get { return IsSensitive.HasValue ? Mismo3Utilities.ConvertBoolToString(this.IsSensitive.Value) : string.Empty; }
            set { }
        }

        /// <summary>
        /// Gets a value indicating whether the Sensitive Information indicator has a value to serialize.
        /// </summary>
        /// <remarks>
        /// The indicator only serializes if the value is set to true.
        /// </remarks>
        [XmlIgnore]
        public bool SensitiveIndicatorSpecified
        {
            get { return !string.IsNullOrEmpty(this.SensitiveIndicator) && this.IsSensitive.Value; }
        }

        /// <summary>
        /// Gets or sets the type of reason a piece of data was not supplied.
        /// </summary>
        /// <remarks>
        /// We do not use MISMOEnum in this case because MISMOEnum is a complex type, which cannot
        /// be serialized to an XML attribute.
        /// </remarks>
        [XmlIgnore]
        public DataNotSuppliedReasonBase? DataNotSuppliedReasonType { get; set; }

        /// <summary>
        /// Gets or sets the string value of the reason a piece of data was not supplied
        /// </summary>
        [XmlAttribute(AttributeName = "DataNotSuppliedReasonType")]
        public string DataNotSuppliedReasonTypeStringified
        {
            get { return this.DataNotSuppliedReasonType?.ToString() ?? string.Empty; }
            set { }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the DataNotSuppliedReasonType attribute has a value to serialize.
        /// </summary>
        [XmlIgnore]
        public bool DataNotSuppliedReasonTypeStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.DataNotSuppliedReasonTypeStringified); }
            set { }
        }

        /// <summary>
        /// Gets or sets an additional description as to why a piece of data was not supplied.
        /// </summary>
        [XmlAttribute(AttributeName = "DataNotSuppliedReasonTypeAdditionalDescription")]
        public string DataNotSuppliedReasonTypeAdditionalDescription { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the additional description has a value to serialize.
        /// </summary>
        [XmlIgnore]
        public bool DataNotSuppliedReasonTypeAdditionalDescriptionSpecified
        {
            get { return !string.IsNullOrEmpty(this.DataNotSuppliedReasonTypeAdditionalDescription); }
            set { }
        }

        /// <summary>
        /// Gets or sets a value indicating the reason a datapoint was not supplied, if the reason type is Other.
        /// </summary>
        [XmlAttribute(AttributeName = "DataNotSuppliedReasonTypeOtherDescription")]
        public string DataNotSuppliedReasonTypeOtherDescription { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the other description has a value to serialize.
        /// </summary>
        [XmlIgnore]
        public bool DataNotSuppliedReasonTypeOtherDescriptionSpecified
        {
            get { return !string.IsNullOrEmpty(this.DataNotSuppliedReasonTypeOtherDescription); }
            set { }
        }
    }
}
