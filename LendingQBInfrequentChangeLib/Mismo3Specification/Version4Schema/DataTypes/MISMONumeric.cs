﻿namespace Mismo3Specification.Version4Schema
{
    /// <summary>
    /// A data type of Numeric SHOULD identify a number value that is in the subset of real numbers expressible by decimal numerals. It MAY contain a single decimal point as punctuation. The decimal point is ALWAYS the US nationalization character (.) in the designated character encoding (UTF-8 assumed when not stated.)
    /// It SHOULD be able to contain at least 18 digits.
    /// </summary>
    /// <example>
    /// A Field Height Number for a document field expressed in inches that is 3/8 inch high would be expressed as ".375".
    /// </example>
    public partial class MISMONumeric : MISMOData
    {
    }
}
