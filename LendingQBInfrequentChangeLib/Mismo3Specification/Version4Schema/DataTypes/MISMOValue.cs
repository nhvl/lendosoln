﻿namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A data type of Value SHOULD identify a data point that is a String generated as the result of applying an algorithm to other data and MAY have AlgorithmURI and lang attributes.
    /// </summary>
    /// <example>
    /// A Delivery Point Bar Code Value could be expressed as "20500999901".
    /// </example>
    public partial class MISMOValue : MISMOData
    {
        /// <summary>
        /// A value that identifies the algorithm used to calculate digest.
        /// </summary>
        [XmlAttribute(AttributeName = "AlgorithmURI", DataType = "anyURI")]
        public string AlgorithmUri;

        /// <summary>
        /// Indicates whether the Algorithm URI has a value to serialize.
        /// </summary>
        [XmlIgnore]
        public bool AlgorithmUriSpecified
        {
            get
            {
                return !string.IsNullOrEmpty(this.AlgorithmUri);
            }
        }
        /// <summary>
        /// The ISO 639-1 two character code value representing the language of the text of the element data point.
        /// </summary>
        /// <example>For example en for English es for spanish.  If no ISO639-1 value exists for the language use the ISO639-2 three character  value.  For example Philippine languages phi.</example> 
        [XmlAttribute(AttributeName = "lang", DataType = "language")]
        public string IsoLanguageCode;

        /// <summary>
        /// Indicates whether the language code has a value to serialize.
        /// </summary>
        [XmlIgnore]
        public bool IsoLanguageCodeSpecified
        {
            get
            {
                return !string.IsNullOrEmpty(this.IsoLanguageCode);
            }
        }
    }
}
