﻿namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A data type of Identifier SHOULD identify a data point that provides a unique value for reference purposes and MAY have IdentifierOwnerURI and IdentifierEffectiveDate attributes.
    /// </summary>
    /// <example>
    /// A Loan Identifier could be expressed as "0034567891-A".
    /// </example>
    public partial class MISMOIdentifier : MISMOData
    {
        /// <summary>
        /// The date on which the value represented by the MISMOIdentifier is effective as determined by the issuer or owner as specified by the IdentifierOwnerURI.
        /// </summary>
        [XmlAttribute(AttributeName = "IdentifierEffectiveDate")]
        public string IdentifierEffectiveDate { get; set; }

        /// <summary>
        /// Gets a value indicating whether the identifier effective date has a value to serialize.
        /// </summary>
        [XmlIgnore]
        public bool IdentifierEffectiveDateSpecified
        {
            get
            {
                return !string.IsNullOrEmpty(this.IdentifierEffectiveDate);
            }
        }

        /// <summary>
        /// Identifies the owner or publisher of the identifier associated with a MISMO term with the class
        /// word of "Identifier" by means or a URI. This is an XML attribute.
        /// </summary>
        [XmlAttribute(AttributeName = "IdentifierOwnerURI", DataType = "anyURI")]
        public string IdentifierOwnerUri { get; set; }

        /// <summary>
        /// Gets a value indicating whether the identifier owner URI has a value to serialize.
        /// </summary>
        [XmlIgnore]
        public bool IdentifierOwnerUriSpecified
        {
            get
            {
                return !string.IsNullOrEmpty(this.IdentifierOwnerUri);
            }
        }
    }
}
