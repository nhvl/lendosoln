﻿namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A data type of Object URL SHOULD identify an Object URL that is a MISMO URL that MAY have a ReferenceSigningType attribute.
    /// </summary>
    public partial class MISMOObjectURL : MISMOURL
    {
        /// <summary>
        /// Gets or sets the Reference Signing Type.
        /// </summary>
        /// <remarks>
        /// This attribute describes how the content of a REFERENCE or ObjectURL element is signed using XML digital signatures.
        /// This instructs systems about which transformation, if any, needs to be applied to the REFERENCE or ObjectURL element
        /// before validating existing SYSTEM_SIGNATURES or applying new ones and what transformations are prohibited.
        /// </remarks>
        [XmlIgnore]
        public ReferenceSigningBase? ReferenceSigningType { get; set; }

        /// <summary>
        /// Gets or sets the Reference Signing Type member as a string for serialization.
        /// </summary>
        /// <value>Gets the enumerated value as a string for serialization.</value>
        [XmlAttribute(AttributeName = "ReferenceSigningType")]
        public string ReferenceSigningTypeStringified
        {
            get { return this.ReferenceSigningType?.ToString() ?? string.Empty; }
            set { }
        }

        /// <summary>
        /// Indicates whether the Reference Signing Type has a value to serialize.
        /// </summary>
        [XmlIgnore]
        public bool ReferenceSigningTypeStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.ReferenceSigningTypeStringified); }
        }
    }
}
