﻿namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A data type of Code SHOULD identify a data point that is a look-up value for an entry in a list of values and explanations and MAY have CodeOwnerURI and CodeEffectiveDate attributes.
    /// EXAMPLE: A State Code would be expressed as "AL".
    /// </summary>
    public partial class MISMOCode : MISMOData
    {
        /// <summary>
        /// Gets or sets the code effective date.
        /// </summary>
        /// <remarks>
        /// This is the date on which the value represented by the MISMOCode is effective as determined
        /// by the issuer or owner as specified by the CodeOwnerURI.
        /// </remarks>
        [XmlAttribute(AttributeName = "CodeEffectiveDate")]
        public string CodeEffectiveDate { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the code effective date has a value to serialize.
        /// </summary>
        [XmlIgnore]
        public bool CodeEffectiveDateSpecified
        {
            get { return !string.IsNullOrEmpty(this.CodeEffectiveDate); }
            set { }
        }

        /// <summary>
        /// Gets or sets a value indicating the code owner URI.
        /// </summary>
        /// <remarks>
        /// This property represents the owner or publisher of the code associated with a MISMO term
        /// with the class word of "Code" by means of a URI.
        /// </remarks>
        [XmlAttribute(AttributeName = "CodeOwnerURI", DataType = "anyURI")]
        public string CodeOwnerUri { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the code owner URI has a value to serialize.
        /// </summary>
        [XmlIgnore]
        public bool CodeOwnerUriSpecified
        {
            get { return !string.IsNullOrEmpty(this.CodeOwnerUri); }
            set { }
        }
    }
}
