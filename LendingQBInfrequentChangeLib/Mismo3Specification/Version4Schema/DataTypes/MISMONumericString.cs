﻿namespace Mismo3Specification.Version4Schema
{
    /// <summary>
    /// A data type of NumericString SHOULD identify a series of digits that are not number values. It MUST NOT contain any punctuation.
    /// </summary>
    /// <example>
    /// A Taxpayer Identifier Value for a party could be expressed as "011223333".
    /// </example>
    public partial class MISMONumericString : MISMOData
    {
    }
}
