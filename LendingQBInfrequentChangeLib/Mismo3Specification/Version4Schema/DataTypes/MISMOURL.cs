﻿namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A data type of URL SHOULD identify a data point that represents a Uniform Resource Identifier (URI) that is a Uniform Resource Location that conforms to both the generic syntax of RFC 2396 as amended by RFC 2732 and the specific syntax of the scheme identified by the first component. Note that a URL provides (or provided) access to a resource at that location via that scheme's mechanism.
    /// </summary>
    /// <example>
    /// An School District URL for the Our Town District 1 schools could be expressed as "http://www.schools.ourtown.org".
    /// </example>
    [XmlInclude(typeof(MISMOObjectURL))]
    public partial class MISMOURL : MISMOData
    {
        /// <summary>
        /// Gets or sets the URL.
        /// </summary>
        [XmlText(DataType = "anyURI", Type = typeof(string))]
        public override string Value { get; set; }
    }
}
