﻿namespace Mismo3Specification.Version4Schema
{
    using System;
    using System.Xml.Serialization;
    using static Mismo3Utilities;

    /// <summary>
    /// Provides standard methods for MISMO-serializable enumerated types to use. Includes a SensitiveIndicator and text Value for the enumerated type.
    /// </summary>
    /// <typeparam name="TEnum">
    /// An enum type for the extending class to specify. 
    /// Note: TEnum is only able to be constrained to type struct at compile time, but Value will throw an exception at runtime if TEnum is not an Enum type.
    ///   See http://codeblog.jonskeet.uk/2009/09/10/generic-constraints-for-enums-and-delegates/ for more information.
    /// </typeparam>
    public class MISMOEnum<TEnum> : MISMOData where TEnum : struct
    {
        /// <summary>
        /// Gets or sets the enumerated value of this MISMOEnum.
        /// </summary>
        [XmlIgnore]
        public TEnum EnumValue { get; set; }

        /// <summary>
        /// Gets a value indicating whether a MISMO enum is set to Other.
        /// </summary>
        /// <returns>A boolean indicating whether the enum is set to Other.</returns>
        public bool IsSetToOther
        {
            get
            {
                var other = "Other";
                return other.Equals(this.EnumValue.ToString(), StringComparison.OrdinalIgnoreCase)
                    || other.Equals(GetXmlEnumName(this.EnumValue as Enum), StringComparison.OrdinalIgnoreCase);
            }
        }

        /// <summary>
        /// Gets the enumerated value as a string for serialization.
        /// </summary>
        /// <value>The enumerated value as a string for serialization.</value>
        [XmlText(Type = typeof(string))]
        public override string Value
        {
            get
            {
                if (!typeof(TEnum).IsEnum)
                {
                    throw new InvalidOperationException(this.GetType() + ": Type of TEnum must be an Enum type. Type was " + typeof(TEnum));
                }

                return Mismo3Utilities.GetXmlEnumName(this.EnumValue as Enum);
            }
            set
            {
                // Use a temp variable, properties cannot be used as out parameters.
                TEnum enumValue;
                Enum.TryParse(value, out enumValue);
                this.EnumValue = enumValue;
            }
        }

        /// <summary>
        /// Gets the display label text attribute, a value used by document vendors, presumably for more display-friendly descriptions of enums.
        /// </summary>
        /// <value>A string description for the enum.</value>
        [XmlAttribute]
        public string DisplayLabelText { get; set; }
    }
}
