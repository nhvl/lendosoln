﻿namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A data type of Date SHOULD identify a data point that represents numerically a specific year, of the Gregorian Calendar.
    /// </summary>
    /// <remarks>
    /// Years MUST be represented in the extended format CCYY
    /// CCYY is a four-digit year, "0001" through "9999"
    /// Padded zeros must be used.
    /// An optional trailing time zone indicator is permitted using either Z (for Coordinated Universal Time UTC) or an offset from UTC using +hh:mm or -hh:mm as in Datetime
    /// Note that negative or extended (more than 4 digit) years are not permitted.
    /// </remarks>
    /// <example>
    /// The Escrow Account Activity Disbursement Year would be expressed as "2010".
    /// </example>
    public partial class MISMOYear : MISMOData
    {
        /// <summary>
        /// Gets or sets the year value.
        /// </summary>
        [XmlText(DataType = "gYear")]
        public override string Value { get; set; }
    }
}
