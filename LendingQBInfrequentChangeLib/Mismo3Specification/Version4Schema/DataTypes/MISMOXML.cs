﻿namespace Mismo3Specification.Version4Schema
{
    /// <summary>
    /// A data type of XML SHOULD identify a data point that is an XML compatible byte stream in the encoding of the EncodingType that when decoded is a resource of the MediaType using characters from the CharacterEncodingSetType. If a binary EncodingType is not used (e.g. "None" or "EscapedXML") then the CharacterEncodingSetType must be the same as that used for the XML instance document. The content MAY be mixed text and XML.
    /// </summary>
    /// <example> 
    /// Embedded Content XML for a print stream (.prn) in Base64 encoding could be expressed as "VGhlb2RvcmUgQWRhbXMNCkdyZWdvcnkgQWx2b3JkDQpMZWVuZGVydCBCaWpuYWd0ZQwNCg==".
    /// </example>
    public partial class MISMOXML : MISMOData
    {
    }
}
