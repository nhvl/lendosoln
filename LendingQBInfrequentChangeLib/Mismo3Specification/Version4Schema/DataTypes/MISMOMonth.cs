﻿namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A data type of Month SHOULD identify a data point that represents numerically a specific month of the year of the Gregorian Calendar.
    /// </summary>
    /// <remarks>
    /// Months MUST be represented in the extended format --MM
    /// MM is a two-digit month of the year, "01" through "12"
    /// Padded zeros must be used.
    /// -- is the required prefix for the absent negative and year
    /// An optional trailing time zone indicator is permitted using either Z (for Coordinated Universal Time UTC) or an offset from UTC using +hh:mm or -hh:mm as in Datetime
    /// </remarks>
    /// <example>
    /// The Escrow Account Activity Disbursement Month of the 10th month of the year would be expressed as "--10".
    /// </example>
    public partial class MISMOMonth : MISMOData
    {
        /// <summary>
        /// The month value.
        /// </summary>
        [XmlText(DataType = "gMonth")]
        public override string Value { get; set; }
    }
}
