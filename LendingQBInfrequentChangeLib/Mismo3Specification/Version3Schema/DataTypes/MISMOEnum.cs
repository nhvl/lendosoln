﻿namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Xml.Serialization;

    /// <summary>
    /// Provides standard methods for MISMO-serializable enumerated types to use. Includes a SensitiveIndicator and text Value for the enumerated type.
    /// </summary>
    /// <typeparam name="TEnum">
    /// An enum type for the extending class to specify. 
    /// Note: TEnum is only able to be constrained to type struct at compile time, but Value will throw an exception at runtime if TEnum is not an Enum type.
    ///   See http://codeblog.jonskeet.uk/2009/09/10/generic-constraints-for-enums-and-delegates/ for more information.
    /// </typeparam>
    public class MISMOEnum<TEnum> : MISMOData where TEnum : struct
    {
        /// <summary>
        /// Gets or sets the enumerated value of this MISMOEnum.
        /// </summary>
        [XmlIgnore]
        public TEnum enumValue;

        /// <summary>
        /// Gets the enumerated value as a string for serialization.
        /// </summary>
        /// <value>The enumerated value as a string for serialization.</value>
        [XmlText(Type = typeof(string))]
        public override string Value
        {
            get
            {
                if (!typeof(TEnum).IsEnum)
                {
                    throw new InvalidOperationException(this.GetType() + ": Type of TEnum must be an Enum type. Type was " + typeof(TEnum));
                }

                return Mismo3Utilities.GetXmlEnumName(enumValue as Enum);
            }
            set
            {
                Enum.TryParse<TEnum>(value, out enumValue);
            }
        }

        /// <summary>
        /// Gets the display label text attribute, a value used by document vendors, presumably for more display-friendly descriptions of enums.
        /// </summary>
        /// <value>A string description for the enum.</value>
        [XmlAttribute]
        public string DisplayLabelText { get; set; }

        /// <summary>
        /// Downcasts a MISMO 3.4 enum object to a MISMO 3.3 enum object.
        /// </summary>
        /// <param name="version4Enum">A MISMO 3.4 enum object.</param>
        public static implicit operator MISMOEnum<TEnum>(Version4Schema.MISMOEnum<TEnum> version4Enum)
        {
            if (version4Enum == null)
            {
                return null;
            }

            var version3Enum = new MISMOEnum<TEnum>();
            version3Enum.enumValue = version4Enum.EnumValue;
            version3Enum.DisplayLabelText = version4Enum.DisplayLabelText;
            version3Enum.IsSensitive = version4Enum.IsSensitive ?? false;
            version3Enum.DisplaySensitive = version4Enum.IsSensitive != null;

            return version3Enum;
        }
    }
}
