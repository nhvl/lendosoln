﻿namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A data type of String SHOULD identify a data point that is either a text value consisting of a word, phrase, sentence, paragraph, or formatted (TAB, CR, LF) character content depending on its purpose or a numeric value with a maximum of 16383 characters and MAY have the lang attribute. There is no list of valid values provided.
    /// EXAMPLE: A disqualifying reason text for excluding a product from those offered to a borrower could be expressed as "Does not meet income to indebtedness guidelines for down payment support.".
    /// </summary>
    public partial class MISMOString : MISMOData
    {
        /// <summary>
        /// The ISO 639-1 two character code value representing the language of the text of the element data point.
        /// </summary>
        //// For example en for English es for spanish.  If no ISO639-1 value exists for the language use the ISO639-2 three character  value.  For example Philippine languages phi.
        [XmlAttribute(AttributeName = "lang", DataType = "language")]
        public string Lang;

        /// <summary>
        /// Indicates whether the language can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the language can be serialized.</returns>
        public bool ShouldSerializeLang()
        {
            return !string.IsNullOrEmpty(Lang);
        }
    }
}
