﻿namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A data type of Indicator SHOULD identify a data point that represents a boolean value.
    /// The value choices SHOULD be "true" or "false" but MAY be "1" or "0".
    /// </summary>
    /// <example>
    /// A Basement Finished Indicator for a basement that qualifies would be expressed as "true".
    /// </example>
    public partial class MISMOIndicator : MISMOData
    {
        /// <summary>
        /// The boolean value of this indicator.
        /// </summary>
        [XmlIgnore]
        public bool booleanValue;

        /// <summary>
        /// Gets or sets the boolean value as a string for serialization.
        /// </summary>
        /// <value>Gets the boolean value as a string for serialization.</value>
        [XmlText(Type = typeof(string))]
        public override string Value
        {
            get { return Mismo3Utilities.ConvertBoolToString(booleanValue); }
            set { }
        }
    }
}
