﻿namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A data type of Value SHOULD identify a data point that is a String generated as the result of applying an algorithm to other data and MAY have AlgorithmURI and lang attributes.
    /// </summary>
    /// <example>
    /// A Delivery Point Bar Code Value could be expressed as "20500999901".
    /// </example>
    public partial class MISMOValue : MISMOData
    {
        /// <summary>
        /// A value that identifies the algorithm used to calculate digest.
        /// </summary>
        [XmlAttribute(DataType = "anyURI")]
        public string AlgorithmURI;

        /// <summary>
        /// Indicates whether the Algorithm URI can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the Algorithm URI can be serialized.</returns>
        public bool ShouldSerializeAlgorithmURI()
        {
            return !string.IsNullOrEmpty(AlgorithmURI);
        }
        /// <summary>
        /// The ISO 639-1 two character code value representing the language of the text of the element data point.
        /// </summary>
        /// <example>For example en for English es for spanish.  If no ISO639-1 value exists for the language use the ISO639-2 three character  value.  For example Philippine languages phi.</example> 
        [XmlAttribute(AttributeName = "lang", DataType = "language")]
        public string Lang;

        /// <summary>
        /// Indicates whether the language can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the language can be serialized.</returns>
        public bool ShouldSerializeLang()
        {
            return !string.IsNullOrEmpty(Lang);
        }
    }
}
