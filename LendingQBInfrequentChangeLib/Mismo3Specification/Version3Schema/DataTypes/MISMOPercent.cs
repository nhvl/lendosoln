﻿namespace Mismo3Specification.Version3Schema
{
    /// <summary>
    /// A data type of Percent SHOULD identify a number value that is in the subset of real numbers expressible by decimal numerals and represents a value that is scaled upward by 100 so that 1/100 is represented by 1. It MAY contain a single decimal point as punctuation. The decimal point is ALWAYS the US nationalization character (.) in the designated character encoding (UTF-8 assumed when not stated.)
    /// It SHOULD be able to contain at least 18 digits.
    /// </summary>
    /// <example>
    /// A Tax Rate Percent for a rate of .055 would be expressed as "5.5".
    /// </example>
    public partial class MISMOPercent : MISMOData
    {
    }
}
