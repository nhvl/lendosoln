﻿namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A data type of Identifier SHOULD identify a data point that provides a unique value for reference purposes and MAY have IdentifierOwnerURI and IdentifierEffectiveDate attributes.
    /// </summary>
    /// <example>
    /// A Loan Identifier could be expressed as "0034567891-A".
    /// </example>
    public partial class MISMOIdentifier : MISMOData
    {
        /// <summary>
        /// The date on which the value represented by the MISMOIdentifier is effective as determined by the issuer or owner as specified by the IdentifierOwnerURI.
        /// </summary>
        [XmlAttribute]
        public string IdentifierEffectiveDate;

        /// <summary>
        /// Indicates whether the identifier effective date can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the identifier effective date can be serialized.</returns>
        public bool ShouldSerializeIdentifierEffectiveDate()
        {
            return !string.IsNullOrEmpty(IdentifierEffectiveDate);
        }

        /// <summary>
        /// Identifies the owner or publisher of the identifier associated with a MISMO term with the class word of "Identifier" by means or a URI. This is an XML attribute.
        /// </summary>
        [XmlAttribute(DataType = "anyURI")]
        public string IdentifierOwnerURI;

        /// <summary>
        /// Indicates whether the identifier owner URI can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the identifier effective date can be serialized.</returns>
        public bool ShouldSerializeIdentifierOwnerURI()
        {
            return !string.IsNullOrEmpty(IdentifierOwnerURI);
        }
    }
}
