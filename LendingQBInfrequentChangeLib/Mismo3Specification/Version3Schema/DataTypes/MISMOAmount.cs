﻿namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A data type of Amount SHOULD identify a number value that is an amount of money and MAY have a CurrencyURI attribute. It SHALL NOT contain any punctuation other than the decimal point or sign values. The decimal point is ALWAYS the US nationalization character (.) in the designated character encoding (UTF-8 assumed when not stated)
    /// It SHOULD be able to contain at least 18 digits.
    /// EXAMPLE: An Unpaid Principle Balance Amount of $100,000.12 would be expressed as "100000.12".
    /// </summary>
    public partial class MISMOAmount : MISMOData
    {
        /// <summary>
        /// The currency the value represents.
        /// </summary>
        [XmlAttribute(DataType = "anyURI")]
        public string CurrencyURI;

        /// <summary>
        /// Indicates whether the currency URI can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the currency URI can be serialized.</returns>
        public bool ShouldSerializeCurrencyURI()
        {
            return !string.IsNullOrEmpty(CurrencyURI);
        }
    }
}
