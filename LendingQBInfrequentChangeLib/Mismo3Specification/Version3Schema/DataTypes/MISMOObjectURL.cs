﻿namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A data type of Object URL SHOULD identify an Object URL that is a MISMO URL that MAY have a ReferenceSigningType attribute.
    /// </summary>
    public partial class MISMOObjectURL : MISMOURL
    {
        /// <summary>
        /// This attribute describes how the content of a REFERENCE or ObjectURL element is signed using XML digital signatures. This instructs systems about which transformation, if any, needs to be applied to the REFERENCE or ObjectURL element before validating existing SYSTEM_SIGNATURES or applying new ones and what transformations are prohibited.
        /// </summary>
        [XmlIgnore]
        private ReferenceSigningBase referenceSigningType;

        /// <summary>
        /// Gets or sets the Reference Signing Type.
        /// </summary>
        /// <value>A selected value from the Reference Signing enumeration.</value>
        [XmlIgnore]
        public ReferenceSigningBase ReferenceSigningType
        {
            get { return referenceSigningType; }
            set { this.referenceSigningType = value; this.ReferenceSigningTypeSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the Reference Signing Type has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool ReferenceSigningTypeSpecified = false;

        /// <summary>
        /// Gets or sets the Reference Signing Type member as a string for serialization.
        /// </summary>
        /// <value>Gets the enumerated value as a string for serialization.</value>
        [XmlAttribute(AttributeName = "ReferenceSigningType")]
        public string ReferenceSigningTypeSerialized
        {
            get { return referenceSigningType.ToString("G"); }
            set { }
        }

        /// <summary>
        /// Indicates whether the Reference Signing Type can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the Reference Signing Type can be serialized.</returns>
        public bool ShouldSerializeReferenceSigningTypeSerialized()
        {
            return ReferenceSigningTypeSpecified;
        }
    }
}
