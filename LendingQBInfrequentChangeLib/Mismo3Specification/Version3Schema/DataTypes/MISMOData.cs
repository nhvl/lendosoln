﻿namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// An abstract MISMO-serializable class from which to derive MISMO data containers. Provides a sensitive indicator and a value.
    /// </summary>
    public abstract class MISMOData
    {
        /// <summary>
        /// Gets or sets the data value.
        /// </summary>
        /// <value>Gets the data value as a string for serialization.</value>
        [XmlText(Type = typeof(string))]
        public virtual string Value
        {
            get; set;
        }

        /// <summary>
        /// Specifies whether the SensitiveIndicator should be serialized in the XML output.
        /// </summary>
        [XmlIgnore]
        public bool DisplaySensitive = true;

        /// <summary>
        /// Gets or sets a value indicating whether the information in this element is sensitive.
        /// </summary>
        /// <value>A boolean indicating whether the information in this data point is sensitive.</value>
        [XmlIgnore]
        public bool IsSensitive { get; set; }

        /// <summary>
        /// Gets or sets the Sensitive Information indicator as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the sensitive indicator property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SensitiveIndicator")]
        public string SensitiveIndicator
        {
            get { return Mismo3Utilities.ConvertBoolToString(IsSensitive); }
            set { }
        }

        /// <summary>
        /// Indicates whether the Sensitive Information indicator can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        [XmlIgnore]
        public bool SensitiveIndicatorSpecified
        {
            get { return !string.IsNullOrEmpty(this.SensitiveIndicator) && this.DisplaySensitive; }
        }
    }
}
