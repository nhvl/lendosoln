﻿namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A data type of Day SHOULD identify a data point that represents numerically a specific day of the month of the Gregorian Calendar.
    /// </summary>
    /// <remarks>
    /// Days MUST be represented in the extended format ---DD
    /// DD is a two-digit day of the month, "01" through "31"
    /// Padded zeros must be used.
    /// --- is the required prefix for the absent negative, year, and month
    /// An optional trailing time zone indicator is permitted using either Z (for Coordinated Universal Time UTC) or an offset from UTC using +hh:mm or -hh:mm as in Datetime
    /// </remarks>
    /// <example>
    /// The Investor Remittance Day of the 9th of every month would be expressed as "---09"."
    /// </example>
    public partial class MISMODay : MISMOData
    {
        /// <summary>
        /// The day value.
        /// </summary>
        [XmlText(DataType = "gDay")]
        public override string Value
        {
            get; set;
        }
    }
}
