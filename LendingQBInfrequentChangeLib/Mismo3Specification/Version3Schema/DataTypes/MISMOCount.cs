﻿namespace Mismo3Specification.Version3Schema
{
    /// <summary>
    /// A data type of Count SHOULD identify a non-negative number value that is a whole number with a maximum value of 2147483647. It SHALL NOT contain any punctuation.
    /// EXAMPLE: A Total Bathroom Count for a dwelling unit with one full bathroom and one partial bathroom would be expressed as "2".
    /// </summary>
    public partial class MISMOCount : MISMOData
    {
    }
}
