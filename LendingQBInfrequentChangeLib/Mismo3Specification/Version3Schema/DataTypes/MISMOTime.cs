﻿namespace Mismo3Specification.Version3Schema
{
    /// <summary>
    /// A data type of Time SHOULD identify a data point that represents numerically a time of day expressed in hours, minutes and seconds.
    /// </summary>
    /// <remarks>
    /// Times MUST be represented in the hh:mm:ss format
    /// hh is a two-digit hour of that day, "00" through "23"
    /// mm is a two-digit minute of that hour, "00" through "59"
    /// ss is a two-digit second of that minute, "00" through "59"
    /// : is the required separator between hh:mm:ss
    /// Padded leading zeros must be used throughout.
    /// An optional time zone indicator may be appended using either Z (for Coordinated Universal Time UTC) or an offset from UTC using +hh:mm or -hh:mm
    /// An optional fractional seconds (decimal point and up to 9 digits) is permitted after the seconds but before any time zone.
    /// </remarks>
    /// <example>
    /// A Requested Closing Time of noon EST could be expressed as "12:00:00-05:00".
    /// </example>
    public partial class MISMOTime : MISMOData
    {
    }
}
