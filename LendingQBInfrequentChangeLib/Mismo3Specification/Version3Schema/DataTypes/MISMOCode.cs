﻿namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A data type of Code SHOULD identify a data point that is a look-up value for an entry in a list of values and explanations and MAY have CodeOwnerURI and CodeEffectiveDate attributes.
    /// EXAMPLE: A State Code would be expressed as "AL".
    /// </summary>
    public partial class MISMOCode : MISMOData
    {
        /// <summary>
        /// The date on which the value represented by the MISMOCode is effective as determined by the issuer or owner as specified by the CodeOwnerURI.
        /// </summary>
        [XmlAttribute]
        public string CodeEffectiveDate;

        /// <summary>
        /// Indicates whether the code effective date can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the code effective date can be serialized.</returns>
        public bool ShouldSerializeCodeEffectiveDate()
        {
            return !string.IsNullOrEmpty(CodeEffectiveDate);
        }

        /// <summary>
        /// Identifies the owner or publisher of the code associated with a MISMO term with the class word of "Code" by means of a URI. This is an XML attribute.
        /// </summary>
        [XmlAttribute(DataType = "anyURI")]
        public string CodeOwnerURI;

        /// <summary>
        /// Indicates whether the code owner URI can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the code owner URI can be serialized.</returns>
        public bool ShouldSerializeCodeOwnerURI()
        {
            return !string.IsNullOrEmpty(CodeOwnerURI);
        }
    }
}
