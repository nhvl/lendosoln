﻿namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A data type of Sequence Number SHOULD identify a number value that is a position in a series of positive whole numbers with a maximum value of 2147483647. It SHALL NOT contain any punctuation.
    /// </summary>
    /// <example>
    /// A Sequence Number for the second occurrence of a VIEW container would be expressed as "2".
    /// </example>
    public partial class MISMOSequenceNumber : MISMOData
    {
        /// <summary>
        /// Gets or sets an integer value representing the sequence number.
        /// </summary>
        [XmlIgnore]
        public int intValue;

        /// <summary>
        /// Gets or sets the integer value as a string for serialization.
        /// </summary>
        /// <value>Gets the integer value as a string for serialization.</value>
        [XmlText(Type = typeof(string))]
        public override string Value
        {
            get { return intValue.ToString(); }
            set { }
        }
    }
}
