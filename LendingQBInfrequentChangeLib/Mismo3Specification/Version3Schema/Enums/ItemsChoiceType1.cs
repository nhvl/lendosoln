namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum ItemsChoiceType1
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("CharacterEncodingSetType")]
        CharacterEncodingSetType,

        [XmlEnum("CharacterEncodingSetTypeOtherDescription")]
        CharacterEncodingSetTypeOtherDescription,

        [XmlEnum("EXTENSION")]
        EXTENSION,

        [XmlEnum("EmbeddedContentXML")]
        EmbeddedContentXML,

        [XmlEnum("MIMETypeIdentifier")]
        MIMETypeIdentifier,

        [XmlEnum("MIMETypeVersionIdentifier")]
        MIMETypeVersionIdentifier,

        [XmlEnum("ObjectCreatedDatetime")]
        ObjectCreatedDatetime,

        [XmlEnum("ObjectDescription")]
        ObjectDescription,

        [XmlEnum("ObjectEncodingType")]
        ObjectEncodingType,

        [XmlEnum("ObjectEncodingTypeOtherDescription")]
        ObjectEncodingTypeOtherDescription,

        [XmlEnum("ObjectName")]
        ObjectName,

        [XmlEnum("ObjectURL")]
        ObjectURL,

        [XmlEnum("OriginalCreatorDigestValue")]
        OriginalCreatorDigestValue,

        [XmlEnum("REFERENCE")]
        REFERENCE,

        [XmlEnum("UnencodedObjectByteCount")]
        UnencodedObjectByteCount,

    }
}
