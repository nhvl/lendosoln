namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// The weekly average yield on United States Treasury securities adjusted to constant maturity of three years, as made available by the Federal Reserve Board.
    /// </summary>
    public enum IndexSourceBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// The monthly weighted average cost of savings, borrowings, and advances of members of the Federal Home Loan Bank of San Francisco (the Bank), as made available by the Bank.
		/// </summary>
        [XmlEnum("FHLBEleventhDistrictMonthlyCostOfFundsIndex")]
        FHLBEleventhDistrictMonthlyCostOfFundsIndex,

		/// <summary>
		/// The average of interbank offered rates for one month U.S. dollar-denominated deposits in the London market (LIBOR), as published in the Wall Street Journal on the 25th day of each month.
		/// </summary>
        [XmlEnum("LIBOROneMonthWSJ25thDayOfMonth")]
        LIBOROneMonthWSJ25thDayOfMonth,

		/// <summary>
		/// The average of interbank offered rates for one month U.S. dollar-denominated deposits in the London market (LIBOR), as published in the Wall Street Journal on the 25th day of each month (or next business day after the 25th day of the month if the 25th falls on a holiday or weekend).
		/// </summary>
        [XmlEnum("LIBOROneMonthWSJ25thDayOfMonthOrNextBusinessDay")]
        LIBOROneMonthWSJ25thDayOfMonthOrNextBusinessDay,

		/// <summary>
		/// The average of interbank offered rates for one month U.S. dollar-denominated deposits in the London market (LIBOR), as published in the Wall Street Journal.
		/// </summary>
        [XmlEnum("LIBOROneMonthWSJDaily")]
        LIBOROneMonthWSJDaily,

		/// <summary>
		/// The average of interbank offered rates for one month U.S. dollar-denominated deposits in the London market (LIBOR), as published in the Wall Street Journal on the 15th day of each month.
		/// </summary>
        [XmlEnum("LIBOROneMonthWSJFifteenthDayOfMonth")]
        LIBOROneMonthWSJFifteenthDayOfMonth,

		/// <summary>
		/// The average of interbank offered rates for one month U.S. dollar-denominated deposits in the London market (LIBOR), as published in the Wall Street Journal on the 15th day of each month (or next business day after the 15th day of the month if the 15th falls on a holiday or weekend).
		/// </summary>
        [XmlEnum("LIBOROneMonthWSJFifteenthDayOfMonthOrNextBusinessDay")]
        LIBOROneMonthWSJFifteenthDayOfMonthOrNextBusinessDay,

		/// <summary>
		/// The average of interbank offered rates for one year U.S. dollar-denominated deposits in the London market (LIBOR), as published in the Wall Street Journal.
		/// </summary>
        [XmlEnum("LIBOROneYearWSJDaily")]
        LIBOROneYearWSJDaily,

		/// <summary>
		/// The average of interbank offered rates for six month U.S. dollar-denominated deposits in the London market (LIBOR), as published in the Wall Street Journal on the 25th day of each month.
		/// </summary>
        [XmlEnum("LIBORSixMonthWSJ25thDayOfMonth")]
        LIBORSixMonthWSJ25thDayOfMonth,

		/// <summary>
		/// The average of interbank offered rates for six month U.S. dollar-denominated deposits in the London market (LIBOR), as published in the Wall Street Journal on the 25th day of each month (or next business day after the 25th day of the month if the 25th falls on a holiday or weekend).
		/// </summary>
        [XmlEnum("LIBORSixMonthWSJ25thDayOfMonthOrNextBusinessDay")]
        LIBORSixMonthWSJ25thDayOfMonthOrNextBusinessDay,

		/// <summary>
		/// The average of interbank offered rates for six month U.S. dollar-denominated deposits in the London market (LIBOR), as published in the Wall Street Journal on the 15th day of each month.
		/// </summary>
        [XmlEnum("LIBORSixMonthWSJFifteenthDayOfMonth")]
        LIBORSixMonthWSJFifteenthDayOfMonth,

		/// <summary>
		/// The average of interbank offered rates for six month U.S. dollar-denominated deposits in the London market (LIBOR), as published in the Wall Street Journal on the 15th day of each month (or next business day after the 15th day of the month if the 15th falls on a holiday or weekend).
		/// </summary>
        [XmlEnum("LIBORSixMonthWSJFifteenthDayOfMonthOrNextBusinessDay")]
        LIBORSixMonthWSJFifteenthDayOfMonthOrNextBusinessDay,

		/// <summary>
		/// The average of interbank offered rates for six month U.S. dollar-denominated deposits in the London market (LIBOR), as published in the Wall Street Journal on the last business day of the month.
		/// </summary>
        [XmlEnum("LIBORSixMonthWSJLastBusinessDayOfMonth")]
        LIBORSixMonthWSJLastBusinessDayOfMonth,

		/// <summary>
		/// The monthly average yield on United States Treasury securities adjusted to constant maturity of five years, as made available by the Federal Reserve Board.
		/// </summary>
        [XmlEnum("MonthlyFiveYearTreasurySecuritiesConstantMaturityFRBH15")]
        MonthlyFiveYearTreasurySecuritiesConstantMaturityFRBH15,

		/// <summary>
		/// The monthly average yield on United States Treasury securities adjusted to constant maturity of one year, as made available by the Federal Reserve Board.
		/// </summary>
        [XmlEnum("MonthlyOneYearTreasurySecuritiesConstantMaturityFRBH15")]
        MonthlyOneYearTreasurySecuritiesConstantMaturityFRBH15,

		/// <summary>
		/// The monthly average yield on United States Treasury securities adjusted to constant maturity of three years, as made available by the Federal Reserve Board.
		/// </summary>
        [XmlEnum("MonthlyThreeYearTreasurySecuritiesConstantMaturityFRBH15")]
        MonthlyThreeYearTreasurySecuritiesConstantMaturityFRBH15,

		/// <summary>
		/// The National Average Contract Mortgage Rate, as derived from the Federal Housing Finance Board monthly interest rate survey (MIRS).
		/// </summary>
        [XmlEnum("NationalAverageContractMortgageRate")]
        NationalAverageContractMortgageRate,

        [XmlEnum("NationalMonthlyMedianCostOfFundsIndexOTS")]
        NationalMonthlyMedianCostOfFundsIndexOTS,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// The Prime Rate (effective as of the published Effective Date) as published in the Money Rates section of the Wall Street Journal.
		/// </summary>
        [XmlEnum("PrimeRateWSJEffectiveDate")]
        PrimeRateWSJEffectiveDate,

		/// <summary>
		/// The Prime Rate as published in the Money Rates section of the Wall Street Journal.
		/// </summary>
        [XmlEnum("PrimeRateWSJPublicationDate")]
        PrimeRateWSJPublicationDate,

		/// <summary>
		/// The average of the interbank offered rates for six months U.S. dollar-denominated deposits in the London market (LIBOR), as published in the Wall Street Journal.
		/// </summary>
        [XmlEnum("SixMonthLIBOR_WSJDaily")]
        SixMonthLIBOR_WSJDaily,

		/// <summary>
		/// The average of the interbank offered rates for six months U.S. dollar-denominated deposits in the London market (LIBOR), as published in the Wall Street Journal. The most recent index figure available as of the first business day of the month immediately preceding the month in which the Change Date occurs is the Current Index.
		/// </summary>
        [XmlEnum("SixMonthLIBOR_WSJFirstBusinessDayOfTheMonth")]
        SixMonthLIBOR_WSJFirstBusinessDayOfTheMonth,

		/// <summary>
		/// The twelve (12) month average of the monthly auction average (discount rate) on United States Treasury bills with a maturity of six months [or twenty-six weeks], as made available by the Federal Reserve Board
		/// </summary>
        [XmlEnum("SixMonthUSTBillMonthlyAuctionDiscountRateCalculated")]
        SixMonthUSTBillMonthlyAuctionDiscountRateCalculated,

		/// <summary>
		/// The monthly auction average (investment yield) on United States Treasury bills with a maturity of six months [or twenty-six weeks], as made available by the Federal Reserve Board.
		/// </summary>
        [XmlEnum("SixMonthUSTBillMonthlyAuctionInvestmentYieldCalculated")]
        SixMonthUSTBillMonthlyAuctionInvestmentYieldCalculated,

		/// <summary>
		/// The weekly auction average (discount rate) on United States Treasury bills with a maturity of six months [or twenty-six weeks], as made available by the U.S. Treasury after the sale of these securities.
		/// </summary>
        [XmlEnum("SixMonthUSTBillWeeklyAuctionDiscountRateUST")]
        SixMonthUSTBillWeeklyAuctionDiscountRateUST,

		/// <summary>
		/// The weekly auction average (investment yield) on United States Treasury bills with a maturity of six months [or twenty-six weeks], as made available by the U.S. Treasury after the sale of these securities.
		/// </summary>
        [XmlEnum("SixMonthUSTBillWeeklyAuctionInvestmentYieldUST")]
        SixMonthUSTBillWeeklyAuctionInvestmentYieldUST,

		/// <summary>
		/// The weekly average yield on United States Treasury securities adjusted to constant maturity of five years, as made available by the Federal Reserve Board.
		/// </summary>
        [XmlEnum("WeeklyFiveYearTreasurySecuritiesConstantMaturityFRBH15")]
        WeeklyFiveYearTreasurySecuritiesConstantMaturityFRBH15,

		/// <summary>
		/// The weekly average yield on United States Treasury securities adjusted to constant maturity of one year, as made available by the Federal Reserve Board.
		/// </summary>
        [XmlEnum("WeeklyOneYearTreasurySecuritiesConstantMaturityFRBH15")]
        WeeklyOneYearTreasurySecuritiesConstantMaturityFRBH15,

		/// <summary>
		/// The weekly average yield of the secondary market interest rates on six-month negotiable certificates of deposit, as made by the Federal Reserve Board.
		/// </summary>
        [XmlEnum("WeeklySixMonthCertificateOfDepositSecondaryMarketFRBH15")]
        WeeklySixMonthCertificateOfDepositSecondaryMarketFRBH15,

		/// <summary>
		/// The weekly average yield on United States Treasury securities adjusted to constant maturity of ten years, as made available by the Federal Reserve Board.
		/// </summary>
        [XmlEnum("WeeklyTenYearTreasurySecuritiesConstantMaturityFRBH15")]
        WeeklyTenYearTreasurySecuritiesConstantMaturityFRBH15,

		/// <summary>
		/// The weekly average yield on United States Treasury securities adjusted to constant maturity of three years, as made available by the Federal Reserve Board.
		/// </summary>
        [XmlEnum("WeeklyThreeYearTreasurySecuritiesConstantMaturityFRBH15")]
        WeeklyThreeYearTreasurySecuritiesConstantMaturityFRBH15,

    }
}
