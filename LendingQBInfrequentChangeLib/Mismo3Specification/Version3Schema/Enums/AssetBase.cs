namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Fiduciary relationship where legal title to a property is transferred to a trustee with the intention the property be administered by the trustee for benefit of the beneficiary, who holds equitable title to such property.
    /// </summary>
    public enum AssetBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// A fixed sum of money paid to someone each year typically for the rest of their life.
		/// </summary>
        [XmlEnum("Annuity")]
        Annuity,

		/// <summary>
		/// Car or similar personal transportation vehicle disclosed as borrowers asset. Usually linked to make or model and year. Typical valuation method is market (blue book).
		/// </summary>
        [XmlEnum("Automobile")]
        Automobile,

		/// <summary>
		/// A vessel used for travel on water.
		/// </summary>
        [XmlEnum("Boat")]
        Boat,

		/// <summary>
		/// An interest-bearing certificate of debt with maturity date. An obligation of issuing entity. Typical valuation is face value (e.g., $1,000 denomination) & market (e.g., value of 4% bond when yields are 6%). 
		/// </summary>
        [XmlEnum("Bond")]
        Bond,

		/// <summary>
		/// The asset amount used by the lender to qualify the borrower for the loan.
		/// </summary>
        [XmlEnum("BorrowerEstimatedTotalAssets")]
        BorrowerEstimatedTotalAssets,

		/// <summary>
		/// The owners estimate of the primary residence.
		/// </summary>
        [XmlEnum("BorrowerPrimaryHome")]
        BorrowerPrimaryHome,

		/// <summary>
		/// Proceeds of a loan (secured by borrowers present house) to provide homebuyer with funds for down payment & closing costs on a new home before selling present house, which have not yet been deposited to borrowers accounts or included with other assets disclosed. Collected on the URLA in Section VI (Assets - Other Assets).
		/// </summary>
        [XmlEnum("BridgeLoanNotDeposited")]
        BridgeLoanNotDeposited,

		/// <summary>
		/// Currency physically possessed by borrower, not deposited in any financial institution. 
		/// </summary>
        [XmlEnum("CashOnHand")]
        CashOnHand,

		/// <summary>
		/// Certificates issued by financial institutions with a stated term or interest rate, and with a set maturity date. The bank pays the holder in due course at maturity. Early withdrawal subject to penalty. Considered liquid.
		/// </summary>
        [XmlEnum("CertificateOfDepositTimeDeposit")]
        CertificateOfDepositTimeDeposit,

		/// <summary>
		/// Transaction account with a financial institution. Accounts purpose is to effect transfer of owners funds to others by various methods (e.g., check, EFT, ACH, draft). Technical name is demand deposit account (DDA).
		/// </summary>
        [XmlEnum("CheckingAccount")]
        CheckingAccount,

		/// <summary>
		/// Deposit made to bind the conditions of a real estate sale. Deposited in escrow account controlled by others, so funds cannot be included in disclosure in any other account. Collected on the URLA in Section VI (Assets - Cash Deposit).
		/// </summary>
        [XmlEnum("EarnestMoneyCashDepositTowardPurchase")]
        EarnestMoneyCashDepositTowardPurchase,

		/// <summary>
		/// Money given as gift (no repayment expected) to Borrowers to assist in home purchase, usually from relatives or close friend, where gift funds have not yet been deposited or included with other assets. Collected on the URLA in Section VI (Assets - Other Assets).
		/// </summary>
        [XmlEnum("GiftsNotDeposited")]
        GiftsNotDeposited,

		/// <summary>
		/// The total amount given as a gift either deposited or not, (no repayment expected) to borrowers usually from relatives or close friends.
		/// </summary>
        [XmlEnum("GiftsTotal")]
        GiftsTotal,

		/// <summary>
		/// Money scheduled to be given as grant (no repayment expected) to Borrowers to assist in home purchase, usually from local organization or charity, where grant funds have not yet been deposited to borrowers accounts or included with other assets. Collected
		/// </summary>
        [XmlEnum("GrantsNotDeposited")]
        GrantsNotDeposited,

		/// <summary>
		/// A life insurance policy with a value or investment component. Details of amounts for each policy or Accumulated investment cash value of life insurance policy, net of any loans against that amount. Detail of amounts for each policy.
		/// </summary>
        [XmlEnum("LifeInsurance")]
        LifeInsurance,

		/// <summary>
		/// Mutual fund that allows individuals to participate in managed investments in short-term debt securities, such as certificates of deposit and Treasury bills.
		/// </summary>
        [XmlEnum("MoneyMarketFund")]
        MoneyMarketFund,

		/// <summary>
		/// Financial entity that invests its shareholders funds in stocks and bonds of other entities with the objective of obtaining a return on its shareholders investment.
		/// </summary>
        [XmlEnum("MutualFund")]
        MutualFund,

		/// <summary>
		/// For a business owned by borrower, the amount resulting from the calculation Assets minus liabilities. Collected on the URLA in Section VI (Assets - Net Worth of Business).
		/// </summary>
        [XmlEnum("NetWorthOfBusinessOwned")]
        NetWorthOfBusinessOwned,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Estimated net cash proceeds (sales price - obligations) from anticipated sale of borrowers real estate assets.
		/// </summary>
        [XmlEnum("PendingNetSaleProceedsFromRealEstateAssets")]
        PendingNetSaleProceedsFromRealEstateAssets,

		/// <summary>
		/// Market value of real estate owned by the borrower. Not the borrowers primary home.
		/// </summary>
        [XmlEnum("RealEstateOwned")]
        RealEstateOwned,

		/// <summary>
		/// A motor vehicle or trailer equipped with living space and amenities used for recreational purposes.
		/// </summary>
        [XmlEnum("RecreationalVehicle")]
        RecreationalVehicle,

		/// <summary>
		/// The funds that are being contributed by an outside party to be added to the closing funds. The Asset value is necessary for multiple borrowers, who each may have their own credit.
		/// </summary>
        [XmlEnum("RelocationMoney")]
        RelocationMoney,

		/// <summary>
		/// 401Ks, IRAs, etc. controlled by borrower (vested) valued at market.
		/// </summary>
        [XmlEnum("RetirementFund")]
        RetirementFund,

		/// <summary>
		/// The amount that will be applied to closing funds from the sale of assets not otherwise identified.
		/// </summary>
        [XmlEnum("SaleOtherAssets")]
        SaleOtherAssets,

		/// <summary>
		/// Interest-paying account with a financial institution whose main purpose is to earn interest on owners funds held therein. This is NOT a demand deposit account.
		/// </summary>
        [XmlEnum("SavingsAccount")]
        SavingsAccount,

		/// <summary>
		/// A nontransferable registered bond issued by the U.S. government.
		/// </summary>
        [XmlEnum("SavingsBond")]
        SavingsBond,

		/// <summary>
		/// Proceeds from a loan secured by borrowers assets, which have not yet been deposited to borrowers accounts or included with other assets disclosed. Considered liquid. Collected on the URLA in Section VI (Assets - Other Assets).
		/// </summary>
        [XmlEnum("SecuredBorrowedFundsNotDeposited")]
        SecuredBorrowedFundsNotDeposited,

		/// <summary>
		/// A bundle of pay and benefits offered to an employee upon separation from employment.
		/// </summary>
        [XmlEnum("SeverancePackage")]
        SeverancePackage,

		/// <summary>
		/// Financial instrument evidencing an ownership interest in entity that issued the stock. Typical valuation method is Market.
		/// </summary>
        [XmlEnum("Stock")]
        Stock,

		/// <summary>
		/// Fiduciary relationship where legal title to a property is transferred to a trustee with the intention the property be administered by the trustee for benefit of the beneficiary, who holds equitable title to such property.
		/// </summary>
        [XmlEnum("TrustAccount")]
        TrustAccount,
    }
}
