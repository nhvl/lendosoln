namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum LienHolderBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("FNM")]
        FNM,

        [XmlEnum("FRE")]
        FRE,

        [XmlEnum("GovernmentAgency")]
        GovernmentAgency,

        [XmlEnum("Lender")]
        Lender,

        [XmlEnum("NonProfitOrganization")]
        NonProfitOrganization,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("PropertySeller")]
        PropertySeller,

        [XmlEnum("StateOrLocalHousingFinanceAgency")]
        StateOrLocalHousingFinanceAgency,

        [XmlEnum("Unknown")]
        Unknown,

    }
}
