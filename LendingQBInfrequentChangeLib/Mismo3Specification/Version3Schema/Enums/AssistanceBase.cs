namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Assistance provided for relocation of the borrower to encourage a completed workout.
    /// </summary>
    public enum AssistanceBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Assistance to encourage continued loan performance after a completed workout.
		/// </summary>
        [XmlEnum("PayForPerformance")]
        PayForPerformance,

		/// <summary>
		/// Assistance provided for relocation of the borrower to encourage a completed workout.
		/// </summary>
        [XmlEnum("Relocation")]
        Relocation,
    }
}
