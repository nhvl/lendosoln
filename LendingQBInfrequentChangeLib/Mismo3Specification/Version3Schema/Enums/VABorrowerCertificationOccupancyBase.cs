namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// While my spouse was on active military duty and unable to occupy the property securing this loan, I previously occupied the property that is securing this loan as my home. (for interest rate reduction loans)
    /// </summary>
    public enum VABorrowerCertificationOccupancyBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// I now actually occupy the above described property as my home or intend to move into and occupy said property as my home within a reasonable period of time or intend to reoccupy it after the completion of major alterations, repairs or improvements
		/// </summary>
        [XmlEnum("A")]
        A,

		/// <summary>
		/// My spouse is on active military duty and in his or her absence, I occupy or intend to occupy the property securing this loan as my home
		/// </summary>
        [XmlEnum("B")]
        B,

		/// <summary>
		/// I previously occupied the property securing this loan as my home. (for interest rate reductions.)
		/// </summary>
        [XmlEnum("C")]
        C,

		/// <summary>
		/// While my spouse was on active military duty and unable to occupy the property securing this loan, I previously occupied the property that is securing this loan as my home. (for interest rate reduction loans)
		/// </summary>
        [XmlEnum("D")]
        D,

    }
}
