namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum InterestRateAdjustmentCalculationMethodBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("AddFixedPercentageToCurrentInterestRate")]
        AddFixedPercentageToCurrentInterestRate,

        [XmlEnum("AddFixedPercentageToOriginalInterestRate")]
        AddFixedPercentageToOriginalInterestRate,

        [XmlEnum("AddImpliedMarginToValueOfFinancialIndex")]
        AddImpliedMarginToValueOfFinancialIndex,

        [XmlEnum("AddIndexChangeToOriginalInterestRate")]
        AddIndexChangeToOriginalInterestRate,

        [XmlEnum("AddMarginToValueOfFinancialIndex")]
        AddMarginToValueOfFinancialIndex,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Renegotiated")]
        Renegotiated,

    }
}
