namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Change in property taxes
    /// </summary>
    public enum BankruptcyPaymentChangeNoticeReasonBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Adjustment due to the rate chnage of a loan with amortization type of ARM.
		/// </summary>
        [XmlEnum("ARMAdjustment")]
        ARMAdjustment,

		/// <summary>
		/// Change in Community Development District fees.
		/// </summary>
        [XmlEnum("CommunityDevelopmentDistrictFeeChange")]
        CommunityDevelopmentDistrictFeeChange,

		/// <summary>
		/// Change in homeowners association fees.
		/// </summary>
        [XmlEnum("HomeownersAssociationFeeChange")]
        HomeownersAssociationFeeChange,

		/// <summary>
		/// Change in Insurance premiums.
		/// </summary>
        [XmlEnum("InsuranceChange")]
        InsuranceChange,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Change in property taxes
		/// </summary>
        [XmlEnum("PropertyTaxChange")]
        PropertyTaxChange,
    }
}
