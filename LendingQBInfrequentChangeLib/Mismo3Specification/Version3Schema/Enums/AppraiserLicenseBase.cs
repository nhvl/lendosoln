namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Aappraisal work completed by this level of license must be accompanied the signature of a valid licensed supervisory appraiser.
    /// </summary>
    public enum AppraiserLicenseBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// May appraise all types of real property (subject to to the COMPETENCY RULE) in USPAP.
		/// </summary>
        [XmlEnum("CertifiedGeneral")]
        CertifiedGeneral,

		/// <summary>
		/// May appraise 1-4 family residential properties without regard to value or complexity, and all other properties with transaction values up to $250,000 (subject to the COMPETENCY RULE in USPAP and applicable state laws).
		/// </summary>
        [XmlEnum("CertifiedResidential")]
        CertifiedResidential,

		/// <summary>
		/// May appraise non-complex 1-4 unit residential properties with transaction values up to $1million and all other properties with transaction values up to $250,000 (subject to the COMPETENCY RULE is USPAP and applicable state laws).
		/// </summary>
        [XmlEnum("LicensedResidentialAppraiser")]
        LicensedResidentialAppraiser,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Aappraisal work completed by this level of license must be accompanied the signature of a valid licensed supervisory appraiser.
		/// </summary>
        [XmlEnum("RegisteredTraineeApprentice")]
        RegisteredTraineeApprentice,
    }
}
