namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum VAAppraisalBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("HUDConversion")]
        HUDConversion,

        [XmlEnum("LenderAppraisal")]
        LenderAppraisal,

        [XmlEnum("ManufacturedHome")]
        ManufacturedHome,

        [XmlEnum("MasterCertificateOfReasonableValueCase")]
        MasterCertificateOfReasonableValueCase,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("PropertyManagementCase")]
        PropertyManagementCase,

        [XmlEnum("SingleProperty")]
        SingleProperty,

    }
}
