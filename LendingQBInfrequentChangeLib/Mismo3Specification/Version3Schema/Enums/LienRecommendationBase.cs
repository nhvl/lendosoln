namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Reinstate the first lien.
    /// </summary>
    public enum LienRecommendationBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Buyout the first lien.
		/// </summary>
        [XmlEnum("BuyOut")]
        BuyOut,

		/// <summary>
		/// Chargeoff the subject loan, subordinate lien, sometimes referred to as a walkaway.
		/// </summary>
        [XmlEnum("ChargeOffSubordinate")]
        ChargeOffSubordinate,

        [XmlEnum("NoRecommendation")]
        NoRecommendation,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Reinstate the first lien.
		/// </summary>
        [XmlEnum("Reinstate")]
        Reinstate,

    }
}
