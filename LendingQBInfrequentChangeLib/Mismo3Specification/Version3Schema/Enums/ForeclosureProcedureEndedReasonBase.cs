namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// The property is now owned by the investor due to an unsuccessful sale at the foreclosure auction.
    /// </summary>
    public enum ForeclosureProcedureEndedReasonBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// The borrower has filed for bankruptcy under Chapter 7, 11, 12 or 13 of the Federal Bankruptcy Act.
		/// </summary>
        [XmlEnum("Bankruptcy")]
        Bankruptcy,

		/// <summary>
		/// It is not in the best interest of the investor to pursue collection efforts or legal actions against the borrower because of a reduced value for the property, a low outstanding mortgage balance, or the presence of certain environmental hazards on the property.
		/// </summary>
        [XmlEnum("ChargedOff")]
        ChargedOff,

		/// <summary>
		/// It is in the best interest of the investor to pursue a loss mitigation effort, such as a short sale, deed-in-lieu, modification, or repayment plan.
		/// </summary>
        [XmlEnum("LossMitigation")]
        LossMitigation,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// The loan has been paid in full.
		/// </summary>
        [XmlEnum("PaidInFull")]
        PaidInFull,

		/// <summary>
		/// The borrower has paid the outstanding mortgage balance and paid the costs incurred in the foreclosure process during the redemption period.
		/// </summary>
        [XmlEnum("Redeemed")]
        Redeemed,

		/// <summary>
		/// The borrower has brought the loan payments current and paid all fees, expenses and late charges.
		/// </summary>
        [XmlEnum("Reinstated")]
        Reinstated,

		/// <summary>
		/// The foreclosure procedure was started in error.
		/// </summary>
        [XmlEnum("StartedInError")]
        StartedInError,

		/// <summary>
		/// A successful third-party bidder was awarded the property at the foreclosure sale.
		/// </summary>
        [XmlEnum("ThirdPartySale")]
        ThirdPartySale,

		/// <summary>
		/// The property is now owned by the investor due to an unsuccessful sale at the foreclosure auction.
		/// </summary>
        [XmlEnum("UnsuccessfulForeclosureSale")]
        UnsuccessfulForeclosureSale,

    }
}
