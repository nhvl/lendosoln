namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Bankruptcy is related to the personal assets of the borrower.
    /// </summary>
    public enum BankruptcyBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Bankruptcy is related to business in which the borrower is a principal.
		/// </summary>
        [XmlEnum("Business")]
        Business,

		/// <summary>
		/// Bankruptcy is related to the personal assets of the borrower.
		/// </summary>
        [XmlEnum("Personal")]
        Personal,
    }
}
