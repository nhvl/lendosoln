namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Public land held in trust by the State of Hawaii, Department of Hawaiian Home Lands for native Hawaiians.
    /// </summary>
    public enum NativeAmericanLandsBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Land allotted to eligible Native American Indian not living on a reservation or to a member of a tribe without reservation lands.
		/// </summary>
        [XmlEnum("AllottedTribalLand")]
        AllottedTribalLand,

		/// <summary>
		/// Public land held in trust by the State of Hawaii, Department of Hawaiian Home Lands for native Hawaiians.
		/// </summary>
        [XmlEnum("HawaiianHomeLands")]
        HawaiianHomeLands,

        [XmlEnum("Other")]
        Other,

    }
}
