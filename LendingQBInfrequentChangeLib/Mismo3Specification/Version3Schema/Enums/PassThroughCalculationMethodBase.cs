namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// The pass through rate is determined by subtracting the Minimum Service Fee Rate and the Guaranty Fee Rate from the Note Rate after the new interest rate is determined. Service Fee Rate and Guaranty Fee Rate are defined by the investor contract. Other factors may apply.
    /// </summary>
    public enum PassThroughCalculationMethodBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// The pass through rate is determined by adding the index to the contract margin (Investor required margin). Investor required margin is defined by investor contract. Other factors may apply.
		/// </summary>
        [XmlEnum("BottomUp")]
        BottomUp,

		/// <summary>
		/// The pass through rate is determined by subtracting the Minimum Service Fee Rate and the Guaranty Fee Rate from the Note Rate after the new interest rate is determined. Service Fee Rate and Guaranty Fee Rate are defined by the investor contract. Other factors may apply.
		/// </summary>
        [XmlEnum("TopDown")]
        TopDown,

    }
}
