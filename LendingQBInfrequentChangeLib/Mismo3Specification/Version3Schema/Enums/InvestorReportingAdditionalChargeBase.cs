namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Assessed at the time a borrower chooses to exercise the Skip Payment option.
    /// </summary>
    public enum InvestorReportingAdditionalChargeBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Fees collected from a responsible party during the current period.
		/// </summary>
        [XmlEnum("FeesCollected")]
        FeesCollected,

		/// <summary>
		/// Assessed as a penalty for failure to pay a regular installment by the due date plus any allowed grace period.
		/// </summary>
        [XmlEnum("LatePaymentCharge")]
        LatePaymentCharge,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("PrepaymentPenalty")]
        PrepaymentPenalty,

		/// <summary>
		/// Net economic loss on a foreclosure and/or REO liquidation as it pertains to the trust that backs a security.
		/// </summary>
        [XmlEnum("RealizedLossDueToForeclosedREOPropertyLiquidation")]
        RealizedLossDueToForeclosedREOPropertyLiquidation,

		/// <summary>
		/// Temporary or ongoing loss attributable to short-term modification of terms of a loan not being liquidated from the trust that backs a security.
		/// </summary>
        [XmlEnum("RealizedLossDueToLoanModification")]
        RealizedLossDueToLoanModification,

		/// <summary>
		/// Assessed at the time a borrower chooses to exercise the Skip Payment option.
		/// </summary>
        [XmlEnum("SkipPaymentCharge")]
        SkipPaymentCharge,

    }
}
