namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum ListingStatusBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Active")]
        Active,

        [XmlEnum("Contract")]
        Contract,

        [XmlEnum("Expired")]
        Expired,

        [XmlEnum("Leased")]
        Leased,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Rented")]
        Rented,

        [XmlEnum("SettledSale")]
        SettledSale,

        [XmlEnum("Withdrawn")]
        Withdrawn,

    }
}
