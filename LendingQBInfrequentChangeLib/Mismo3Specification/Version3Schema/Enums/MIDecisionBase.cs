namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Approved subject to conditions.
    /// </summary>
    public enum MIDecisionBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Approved")]
        Approved,

        [XmlEnum("ApprovedAfterReevaluation")]
        ApprovedAfterReevaluation,

        [XmlEnum("Canceled")]
        Canceled,

		/// <summary>
		/// Approved subject to conditions.
		/// </summary>
        [XmlEnum("ConditionedApproval")]
        ConditionedApproval,

        [XmlEnum("Declined")]
        Declined,

        [XmlEnum("Suspended")]
        Suspended,

    }
}
