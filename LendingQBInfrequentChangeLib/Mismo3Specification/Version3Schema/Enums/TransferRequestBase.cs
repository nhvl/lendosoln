namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A servicing transfer request for  a specifically targeted loan or loans held by a servicing trading partner.
    /// </summary>
    public enum TransferRequestBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// A request for a servicing transfer on all loans held as assets by a servicing trading partner.
		/// </summary>
        [XmlEnum("AllLoans")]
        AllLoans,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// A servicing transfer request for  a specifically targeted loan or loans held by a servicing trading partner.
		/// </summary>
        [XmlEnum("SpecificLoans")]
        SpecificLoans,

    }
}
