namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum AnalysisComponentBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Feature")]
        Feature,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Room")]
        Room,

        [XmlEnum("StructuralElement")]
        StructuralElement,
    }
}
