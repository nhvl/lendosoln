namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum FHAInsuranceProgramBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("FHAUpfront")]
        FHAUpfront,

        [XmlEnum("RBP")]
        RBP,

        [XmlEnum("Section530")]
        Section530,

    }
}
