namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// The act of unlawfully keeping goods or property. For example, a tenant wrongfully remaining in a residence after a valid eviction notice is guilty of an unlawful detainer.
    /// </summary>
    public enum CreditPublicRecordBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// A legal decree that states that a marriage was never valid.
		/// </summary>
        [XmlEnum("Annulment")]
        Annulment,

		/// <summary>
		/// The preliminary legal seizure of property to force compliance with a decision which may be obtained in a pending suit.
		/// </summary>
        [XmlEnum("Attachment")]
        Attachment,

		/// <summary>
		/// Bankruptcy where business assets are kept and creditors are paid according to an approved plan.
		/// </summary>
        [XmlEnum("BankruptcyChapter11")]
        BankruptcyChapter11,

		/// <summary>
		/// Bankruptcy where a family with regular income keeps their assets and pays creditors according to an approved plan. Usually applies to family farms.
		/// </summary>
        [XmlEnum("BankruptcyChapter12")]
        BankruptcyChapter12,

		/// <summary>
		/// Bankruptcy where an individual with regular income keeps assets and pays creditors according to an approved plan.
		/// </summary>
        [XmlEnum("BankruptcyChapter13")]
        BankruptcyChapter13,

		/// <summary>
		/// Bankruptcy where assets are liquidated and the proceeds are distributed to the creditors.
		/// </summary>
        [XmlEnum("BankruptcyChapter7")]
        BankruptcyChapter7,

		/// <summary>
		/// Bankruptcy petitioned by the creditors, where assets are liquidated and the proceeds are distributed to the creditors.
		/// </summary>
        [XmlEnum("BankruptcyChapter7Involuntary")]
        BankruptcyChapter7Involuntary,

		/// <summary>
		/// Bankruptcy petitioned by the debtor, where assets are liquidated and the proceeds are distributed to the creditors.
		/// </summary>
        [XmlEnum("BankruptcyChapter7Voluntary")]
        BankruptcyChapter7Voluntary,

		/// <summary>
		/// A bankruptcy case but the type is not known.
		/// </summary>
        [XmlEnum("BankruptcyTypeUnknown")]
        BankruptcyTypeUnknown,

		/// <summary>
		/// A legal notice specifying payment schedule for support of children  involved in a marital separation or divorce.
		/// </summary>
        [XmlEnum("ChildSupport")]
        ChildSupport,

		/// <summary>
		/// A debt has not been paid according to terms and has been turned over to an agency or attorney to collect the debt from the borrower.
		/// </summary>
        [XmlEnum("Collection")]
        Collection,

		/// <summary>
		/// An agreement has been made or approved by a court regarding custody of children of a separating or divorcing couple.
		/// </summary>
        [XmlEnum("CustodyAgreement")]
        CustodyAgreement,

		/// <summary>
		/// A legal decree that specifies the terms related to the dissolution of a marriage.
		/// </summary>
        [XmlEnum("DivorceDecree")]
        DivorceDecree,

		/// <summary>
		/// A legal notice specifying payment schedule for support of a family  involved in a marital separation or divorce.
		/// </summary>
        [XmlEnum("FamilySupport")]
        FamilySupport,

		/// <summary>
		/// A legal notice announcing the namge change of a business or entity.
		/// </summary>
        [XmlEnum("FictitiousName")]
        FictitiousName,

		/// <summary>
		/// Process here debtors make payments to creditors according to a plan arranged by a financial counseling agency.
		/// </summary>
        [XmlEnum("FinancialCounseling")]
        FinancialCounseling,

		/// <summary>
		/// Also called a UCC-1 from its form number, a statement that contains information about a security interest in collateral used to secure a debt and that is filed to provide notice to other creditors of the security interest.
		/// </summary>
        [XmlEnum("FinancingStatement")]
        FinancingStatement,

		/// <summary>
		/// Act of keeping goods or property by force. For example, when a tenant remains in a property after an eviction notice.
		/// </summary>
        [XmlEnum("ForcibleDetainer")]
        ForcibleDetainer,

		/// <summary>
		/// Process where a lender forces the sale of property to pay off a mortgage.
		/// </summary>
        [XmlEnum("Foreclosure")]
        Foreclosure,

		/// <summary>
		/// Court ordered method of debt collection in which a portion of a persons salary is deducted and paid to a collector.
		/// </summary>
        [XmlEnum("Garnishment")]
        Garnishment,

		/// <summary>
		/// A final court ruling resolving the key questions in a lawsuit and determining the rights and obligations of the opposing parties.
		/// </summary>
        [XmlEnum("Judgment")]
        Judgment,

		/// <summary>
		/// A legal action based on a complaint that the defendant failed to perform a legal duty, resulting in harm to the plaintiff.
		/// </summary>
        [XmlEnum("LawSuit")]
        LawSuit,

		/// <summary>
		/// A legal document filed with a court that can force the sale of property if a debt is not paid.
		/// </summary>
        [XmlEnum("Lien")]
        Lien,

		/// <summary>
		/// A legal claim placed on real estate by someone who is owed money for labor, services or supplies contributed to the property for purposes of improving it.
		/// </summary>
        [XmlEnum("MechanicsLien")]
        MechanicsLien,

		/// <summary>
		/// A legal claim placed on a specific item of property if a medical related debt is not paid.
		/// </summary>
        [XmlEnum("MedicalLien")]
        MedicalLien,

		/// <summary>
		/// A legal notice file by a land owner to claim they are not responsible for construction, alteration or repair of buildings or other improvements located on their land.
		/// </summary>
        [XmlEnum("NonResponsibility")]
        NonResponsibility,

		/// <summary>
		/// A legal document filed by a lender that a property owner has missed several payments on their loan.
		/// </summary>
        [XmlEnum("NoticeOfDefault")]
        NoticeOfDefault,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Public sale is the term used for the sale of lien property after public notice stating the date, time and location of the sale.
		/// </summary>
        [XmlEnum("PublicSale")]
        PublicSale,

		/// <summary>
		/// A notice of the sale or transfer of real estate property.
		/// </summary>
        [XmlEnum("RealEstateRecording")]
        RealEstateRecording,

		/// <summary>
		/// The act of recovery of goods or chattels for non-payment or property that has been wrongfully detained.
		/// </summary>
        [XmlEnum("Repossession")]
        Repossession,

		/// <summary>
		/// A legal notice specifying payment schedule for support of a spouse  involved in a marital separation or divorce.
		/// </summary>
        [XmlEnum("SpouseSupport")]
        SpouseSupport,

		/// <summary>
		/// A legal notice specifying payment schedule for support of children or family involved in a marital separation or divorce.
		/// </summary>
        [XmlEnum("SupportDebt")]
        SupportDebt,

		/// <summary>
		/// A notice filed by a city government for the collection of taxes.
		/// </summary>
        [XmlEnum("TaxLienCity")]
        TaxLienCity,

		/// <summary>
		/// A notice filed by a count government for the collection of taxes.
		/// </summary>
        [XmlEnum("TaxLienCounty")]
        TaxLienCounty,

		/// <summary>
		/// A notice filed by the federal government for the collection of taxes.
		/// </summary>
        [XmlEnum("TaxLienFederal")]
        TaxLienFederal,

		/// <summary>
		/// A notice filed by an unspecified agency for the collection of taxes.
		/// </summary>
        [XmlEnum("TaxLienOther")]
        TaxLienOther,

		/// <summary>
		/// A notice filed by a state government for the collection of taxes.
		/// </summary>
        [XmlEnum("TaxLienState")]
        TaxLienState,

		/// <summary>
		/// A trusteeship is the appointment of a person who holds title to property for benefit of another.
		/// </summary>
        [XmlEnum("Trusteeship")]
        Trusteeship,

        [XmlEnum("Unknown")]
        Unknown,

		/// <summary>
		/// The act of unlawfully keeping goods or property. For example, a tenant wrongfully remaining in a residence after a valid eviction notice is guilty of an unlawful detainer.
		/// </summary>
        [XmlEnum("UnlawfulDetainer")]
        UnlawfulDetainer,

    }
}
