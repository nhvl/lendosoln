namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A transaction with a Title Company or Escrow Agent
    /// </summary>
    public enum RecordingTransactionIdentifierBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// A transaction in the Clerk of Circuit Court office
		/// </summary>
        [XmlEnum("CourtCase")]
        CourtCase,

		/// <summary>
		/// A transaction with an agency not included in the enumerated list
		/// </summary>
        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// A transaction in the office of the County Recorder. 
		/// </summary>
        [XmlEnum("Recorder")]
        Recorder,

		/// <summary>
		/// A transaction with a Title Company or Escrow Agent
		/// </summary>
        [XmlEnum("TitleEscrow")]
        TitleEscrow,

    }
}
