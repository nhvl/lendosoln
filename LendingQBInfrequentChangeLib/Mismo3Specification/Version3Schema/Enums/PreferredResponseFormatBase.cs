namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// XML format.
    /// </summary>
    public enum PreferredResponseFormatBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Enter description in Response Format Other.
		/// </summary>
        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// A report formated with graphic PCL (Print Control Language) commands.
		/// </summary>
        [XmlEnum("PCL")]
        PCL,

		/// <summary>
		/// A formatted text report in the Adobe PDF (Portable Document Format).
		/// </summary>
        [XmlEnum("PDF")]
        PDF,

		/// <summary>
		/// A formatted text report.
		/// </summary>
        [XmlEnum("Text")]
        Text,

		/// <summary>
		/// XML format.
		/// </summary>
        [XmlEnum("XML")]
        XML,

    }
}
