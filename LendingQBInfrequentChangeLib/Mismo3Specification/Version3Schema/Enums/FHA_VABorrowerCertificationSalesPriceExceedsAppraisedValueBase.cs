namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// I was not aware of this valuation when I signed my contract but have elected to complete the transaction at the contract purchase price or cost. I have paid or will pay in cash from my own resources at or prior to loan closing a sum equal to the difference between the contract purchase price or cost and the VA or HUD FHA established value. I do not and will not have outstanding after loan closing any unpaid contractual obligation on account of such cash payment.
    /// </summary>
    public enum FHA_VABorrowerCertificationSalesPriceExceedsAppraisedValueBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// I was aware of this valuation when I signed my contract and I have paid or will pay in cash from my own resources at or prior to loan closing a sum equal to the difference, between the contract purchase price or cost and the VA or HUD FHA established value. I do not and will not have outstanding after loan closing any unpaid contractual obligation on account of such cash payment.
		/// </summary>
        [XmlEnum("A")]
        A,

		/// <summary>
		/// I was not aware of this valuation when I signed my contract but have elected to complete the transaction at the contract purchase price or cost. I have paid or will pay in cash from my own resources at or prior to loan closing a sum equal to the difference between the contract purchase price or cost and the VA or HUD FHA established value. I do not and will not have outstanding after loan closing any unpaid contractual obligation on account of such cash payment.
		/// </summary>
        [XmlEnum("B")]
        B,

    }
}
