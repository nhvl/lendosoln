namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// The case has been closed by the bankruptcy court.
    /// </summary>
    public enum BankruptcyStatusBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// The bankruptcy is ongoing.
		/// </summary>
        [XmlEnum("Active")]
        Active,

		/// <summary>
		/// The case has been closed by the bankruptcy court.
		/// </summary>
        [XmlEnum("Closed")]
        Closed,

        [XmlEnum("Other")]
        Other,
    }
}
