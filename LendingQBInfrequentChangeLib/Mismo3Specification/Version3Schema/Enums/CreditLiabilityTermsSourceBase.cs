namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// The monthly payment terms amount was provided by the credit liability holder.
    /// </summary>
    public enum CreditLiabilityTermsSourceBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// The monthly payment terms amount was calculated by the credit bureau.
		/// </summary>
        [XmlEnum("Calculated")]
        Calculated,

		/// <summary>
		/// The monthly payment terms amount was provided by the credit liability holder.
		/// </summary>
        [XmlEnum("Provided")]
        Provided,

    }
}
