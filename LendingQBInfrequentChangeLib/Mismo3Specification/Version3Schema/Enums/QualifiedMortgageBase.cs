namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A loan made subject to Regulation Z temporary qualified mortgage standards for a balloon payment loan made by a small creditor on a subject property that is not rural or underserved.
    /// </summary>
    public enum QualifiedMortgageBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// A loan made subject to Regulation Z Small Creditor Qualified Mortgage standards.
		/// </summary>
        [XmlEnum("SmallCreditor")]
        SmallCreditor,

		/// <summary>
		/// A loan made subject to Regulation Z Small Creditor Qualified Mortgage standards.
		/// </summary>
        [XmlEnum("SmallCreditorRural")]
        SmallCreditorRural,

		/// <summary>
		/// A loan made subject to Regulation Z standard Qualified Mortgage standards.
		/// </summary>
        [XmlEnum("Standard")]
        Standard,

		/// <summary>
		/// A loan made subject to Regulation Z temporary Qualified Mortgage standards under FHA, USDA, VA, or GSE guidelines.
		/// </summary>
        [XmlEnum("TemporaryAgencyGSE")]
        TemporaryAgencyGSE,

		/// <summary>
		/// A loan made subject to Regulation Z temporary qualified mortgage standards for a balloon payment loan made by a small creditor on a subject property that is not rural or underserved.
		/// </summary>
        [XmlEnum("TemporarySmallCreditorBalloon")]
        TemporarySmallCreditorBalloon,

    }
}
