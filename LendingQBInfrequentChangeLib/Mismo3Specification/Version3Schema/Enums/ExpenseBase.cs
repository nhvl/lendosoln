namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Union membership dues borrower incurs as a condition of remaining employment through union (e.g., a closed shop). Collected on the URLA in Section VI (Liabilities - Job Related Expenses.)
    /// </summary>
    public enum ExpenseBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Alimony (also called maintenance or spousal support) is financial support to a spouse from the other spouse after marital separation or from the ex-spouse upon divorce.
		/// </summary>
        [XmlEnum("Alimony")]
        Alimony,

		/// <summary>
		/// Cost of average monthly maintenance for automobiles.
		/// </summary>
        [XmlEnum("CarMaintenance")]
        CarMaintenance,

		/// <summary>
		/// Amounts for contributions made to non profit organizations, charities or private foundations.
		/// </summary>
        [XmlEnum("CharitableContributions")]
        CharitableContributions,

		/// <summary>
		/// Periodic costs of providing care for the borrowers children.
		/// </summary>
        [XmlEnum("ChildCare")]
        ChildCare,

		/// <summary>
		/// Child support (or child maintenance) is an ongoing, periodic payment made by a parent for the financial benefit of a child following the end of a marriage or other relationship.
		/// </summary>
        [XmlEnum("ChildSupport")]
        ChildSupport,

		/// <summary>
		/// Cost of clothing.
		/// </summary>
        [XmlEnum("Clothing")]
        Clothing,

        [XmlEnum("DryCleaning")]
        DryCleaning,

		/// <summary>
		/// Cost of amusement and leisure activities.
		/// </summary>
        [XmlEnum("Entertainment")]
        Entertainment,

		/// <summary>
		/// Cost of groceries and personal hygiene products, such as soap and cosmetics.
		/// </summary>
        [XmlEnum("GroceryToiletry")]
        GroceryToiletry,

		/// <summary>
		/// Costs of health insurance that is not provided by employer and is paid by borrower and not deducted from paycheck.
		/// </summary>
        [XmlEnum("HealthInsurance")]
        HealthInsurance,

		/// <summary>
		/// Ongoing obligations incurred by the borrower which are prerequisite to retaining employment (e.g., professional associations, special uniforms or tools, etc.)
		/// </summary>
        [XmlEnum("JobRelatedExpenses")]
        JobRelatedExpenses,

		/// <summary>
		/// Cost of out of pocket medical expenses not covered by insurance.
		/// </summary>
        [XmlEnum("Medical")]
        Medical,

		/// <summary>
		/// Total amount of living expenses not otherwise reported separately.
		/// </summary>
        [XmlEnum("MiscellaneousLivingExpenses")]
        MiscellaneousLivingExpenses,

		/// <summary>
		/// The amount by which the sum of the mortgage payments, insurance, maintenance, taxes, and miscellaneous expenses exceeds the gross rental income.
		/// </summary>
        [XmlEnum("NetRentalExpense")]
        NetRentalExpense,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// The amount deducted from wages to pay insurance obligations.
		/// </summary>
        [XmlEnum("PayrollInsuranceDeduction")]
        PayrollInsuranceDeduction,

		/// <summary>
		/// The amount deducted from wages to pay all obligations not separately reported.
		/// </summary>
        [XmlEnum("PayrollMiscellaneousDeductions")]
        PayrollMiscellaneousDeductions,

		/// <summary>
		/// The amount deducted from wages to pay profit sharing obligations.
		/// </summary>
        [XmlEnum("PayrollProfitSharingDeduction")]
        PayrollProfitSharingDeduction,

		/// <summary>
		/// The amount deducted from wages to fund a retirement account.
		/// </summary>
        [XmlEnum("PayrollRetirementDeduction")]
        PayrollRetirementDeduction,

		/// <summary>
		/// The amount deducted from wages to pay tax obligations.
		/// </summary>
        [XmlEnum("PayrollTaxDeduction")]
        PayrollTaxDeduction,

		/// <summary>
		/// Periodic amount paid under terms of separation agreement. Collected on the URLA in Section VI (Liabilities - Alimony.)
		/// </summary>
        [XmlEnum("SeparateMaintenanceExpense")]
        SeparateMaintenanceExpense,

		/// <summary>
		/// Union membership dues borrower incurs as a condition of remaining employment through union (e.g., a closed shop). Collected on the URLA in Section VI (Liabilities - Job Related Expenses.)
		/// </summary>
        [XmlEnum("UnionDues")]
        UnionDues,

    }
}
