namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Private Pool
    /// </summary>
    public enum PoolClassBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Fannie Mae Pool
		/// </summary>
        [XmlEnum("FNM")]
        FNM,

		/// <summary>
		/// Freddie Mac Pool
		/// </summary>
        [XmlEnum("FRE")]
        FRE,

		/// <summary>
		/// Ginnie Mae I Pool
		/// </summary>
        [XmlEnum("GNMAI")]
        GNMAI,

		/// <summary>
		/// Ginnie Mae II Pool
		/// </summary>
        [XmlEnum("GNMAII")]
        GNMAII,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Private Pool
		/// </summary>
        [XmlEnum("Private")]
        Private,

    }
}
