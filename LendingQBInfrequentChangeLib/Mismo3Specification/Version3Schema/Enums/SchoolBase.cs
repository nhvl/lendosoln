namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Grades 6-8 or 7-9 Age level from: 10 to: 14
    /// </summary>
    public enum SchoolBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Grades PK or K-6 on average, but state and local practice may vary.  Age level from: 6 to: 14

		/// </summary>
        [XmlEnum("ElementarySchool")]
        ElementarySchool,

		/// <summary>
		/// Grades 9-12 or 10-12 Age level from: 15 to: 18
		/// </summary>
        [XmlEnum("HighSchool")]
        HighSchool,

		/// <summary>
		/// Grades 7-8, 7-9 or 8-9 Age level from: 13 to: 15
		/// </summary>
        [XmlEnum("JuniorHighSchool")]
        JuniorHighSchool,

		/// <summary>
		/// Grades 6-8 or 7-9 Age level from: 10 to: 14
		/// </summary>
        [XmlEnum("MiddleSchool")]
        MiddleSchool,

        [XmlEnum("Other")]
        Other,

    }
}
