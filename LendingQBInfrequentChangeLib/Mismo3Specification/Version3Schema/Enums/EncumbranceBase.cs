namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// There are no encumbrances on the property.
    /// </summary>
    public enum EncumbranceBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("DeedRestriction")]
        DeedRestriction,

        [XmlEnum("FirstMortgageLien")]
        FirstMortgageLien,

        [XmlEnum("GroundLease")]
        GroundLease,

        [XmlEnum("MechanicsLien")]
        MechanicsLien,

		/// <summary>
		/// There are no encumbrances on the property.
		/// </summary>
        [XmlEnum("None")]
        None,

        [XmlEnum("OccupancyRestriction")]
        OccupancyRestriction,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("ResaleRestriction")]
        ResaleRestriction,

        [XmlEnum("SecondMortgageLien")]
        SecondMortgageLien,

        [XmlEnum("TaxLien")]
        TaxLien,

        [XmlEnum("TransferFee")]
        TransferFee,

    }
}
