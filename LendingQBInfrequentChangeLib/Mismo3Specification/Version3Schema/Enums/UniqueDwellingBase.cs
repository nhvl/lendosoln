namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A dwelling made from logs that have not been milled in to conventional lumber.
    /// </summary>
    public enum UniqueDwellingBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// A dwelling built with mounds of dirt (earth) covering one or more exterior walls and/or roof.
		/// </summary>
        [XmlEnum("EarthShelterHome")]
        EarthShelterHome,

		/// <summary>
		/// A spherical structure designed as a dwelling unit.
		/// </summary>
        [XmlEnum("GeodesicDome")]
        GeodesicDome,

		/// <summary>
		/// A boat that has been designed or modified to be used primarily as a dwelling unit.
		/// </summary>
        [XmlEnum("Houseboat")]
        Houseboat,

		/// <summary>
		/// A dwelling made from logs that have not been milled in to conventional lumber.
		/// </summary>
        [XmlEnum("LogHome")]
        LogHome,

        [XmlEnum("Other")]
        Other,

    }
}
