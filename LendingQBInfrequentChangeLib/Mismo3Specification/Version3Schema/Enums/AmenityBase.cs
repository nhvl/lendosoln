namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum AmenityBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Balcony")]
        Balcony,

        [XmlEnum("Deck")]
        Deck,

        [XmlEnum("Fence")]
        Fence,

        [XmlEnum("Fireplace")]
        Fireplace,

        [XmlEnum("Intercom")]
        Intercom,

        [XmlEnum("JettedTub")]
        JettedTub,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Patio")]
        Patio,

        [XmlEnum("Pool")]
        Pool,

        [XmlEnum("Porch")]
        Porch,

        [XmlEnum("SecuritySystem")]
        SecuritySystem,

        [XmlEnum("Spa")]
        Spa,

        [XmlEnum("WoodStove")]
        WoodStove,
    }
}
