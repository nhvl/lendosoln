namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Documentation was provided to review the eligibility of the borrower again.
    /// </summary>
    public enum ActiveWorkoutDocumentStatusBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Complete")]
        Complete,

        [XmlEnum("Incomplete")]
        Incomplete,

        [XmlEnum("InReview")]
        InReview,

		/// <summary>
		/// There was no response from the borrower in regards to the borrower response package that was sent.
		/// </summary>
        [XmlEnum("NoResponse")]
        NoResponse,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Documentation was provided to review the eligibility of the borrower again.
		/// </summary>
        [XmlEnum("Resubmission")]
        Resubmission,
    }
}
