namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum GFEComparisonBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("NotApplicable")]
        NotApplicable,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("SameLoanWithLowerInterestRate")]
        SameLoanWithLowerInterestRate,

        [XmlEnum("SameLoanWithLowerSettlementCharges")]
        SameLoanWithLowerSettlementCharges,

    }
}
