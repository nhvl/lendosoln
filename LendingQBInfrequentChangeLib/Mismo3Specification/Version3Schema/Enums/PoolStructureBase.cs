namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates a pool originated by a single party to back a single issuance of securities.
    /// </summary>
    public enum PoolStructureBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("InvestorDefinedMultipleLender")]
        InvestorDefinedMultipleLender,

        [XmlEnum("LenderInitiatedMultipleLender")]
        LenderInitiatedMultipleLender,

		/// <summary>
		/// Indicates a pool originated by multiple parties to back a single issuance of securities.
		/// </summary>
        [XmlEnum("MultipleIssuer")]
        MultipleIssuer,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Indicates a pool originated by a single party to back a single issuance of securities.
		/// </summary>
        [XmlEnum("SingleIssuer")]
        SingleIssuer,

        [XmlEnum("SingleLender")]
        SingleLender,
    }
}
