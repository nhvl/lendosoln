namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// The borrower makes up the skipped payment with an unscheduled payment.
    /// </summary>
    public enum SkipPaymentActionBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Interest from the skipped payment is capitalized into the UPB.
		/// </summary>
        [XmlEnum("CapitalizeInterest")]
        CapitalizeInterest,

		/// <summary>
		/// Principal and interest from the skipped payment is used to create a subordinate loan component.
		/// </summary>
        [XmlEnum("CreateSubordinateLoanComponent")]
        CreateSubordinateLoanComponent,

		/// <summary>
		/// The mortgage term is extended to amortize the skipped payment.
		/// </summary>
        [XmlEnum("ExtendTerm")]
        ExtendTerm,

		/// <summary>
		/// The borrower makes up the skipped payment with an unscheduled payment.
		/// </summary>
        [XmlEnum("MakeupPayment")]
        MakeupPayment,

        [XmlEnum("Other")]
        Other,

    }
}
