namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// The primary index to use.
    /// </summary>
    public enum IndexDesignationBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// The alternate (second) index to use when index averaging is required.
		/// </summary>
        [XmlEnum("Alternate")]
        Alternate,

		/// <summary>
		/// The primary index to use.
		/// </summary>
        [XmlEnum("Primary")]
        Primary,

    }
}
