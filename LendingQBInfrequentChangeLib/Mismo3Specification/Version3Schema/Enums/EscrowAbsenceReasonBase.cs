namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum EscrowAbsenceReasonBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("BorrowerDeclined")]
        BorrowerDeclined,

        [XmlEnum("LenderDoesNotOffer")]
        LenderDoesNotOffer,

        [XmlEnum("LenderDoesNotRequire")]
        LenderDoesNotRequire,

        [XmlEnum("Other")]
        Other,

    }
}
