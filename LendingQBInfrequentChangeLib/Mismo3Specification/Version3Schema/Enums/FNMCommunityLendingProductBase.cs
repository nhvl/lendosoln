namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum FNMCommunityLendingProductBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("CommunityHomeBuyersProgram")]
        CommunityHomeBuyersProgram,

        [XmlEnum("Fannie32")]
        Fannie32,

        [XmlEnum("Fannie97")]
        Fannie97,

        [XmlEnum("MyCommunityMortgage")]
        MyCommunityMortgage,

        [XmlEnum("Other")]
        Other,

    }
}
