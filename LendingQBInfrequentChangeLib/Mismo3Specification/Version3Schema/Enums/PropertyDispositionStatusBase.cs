namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum PropertyDispositionStatusBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Retain")]
        Retain,

        [XmlEnum("Sell")]
        Sell,

        [XmlEnum("Undecided")]
        Undecided,

        [XmlEnum("Unknown")]
        Unknown,

        [XmlEnum("Vacate")]
        Vacate,

    }
}
