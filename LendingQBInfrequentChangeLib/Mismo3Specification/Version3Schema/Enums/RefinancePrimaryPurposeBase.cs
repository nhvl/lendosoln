namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// To shorten the maturity period of the note
    /// </summary>
    public enum RefinancePrimaryPurposeBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("AssetAcquisition")]
        AssetAcquisition,

        [XmlEnum("CapitalizedInterestTaxesInsuranceOrFees")]
        CapitalizedInterestTaxesInsuranceOrFees,

        [XmlEnum("Cash")]
        Cash,

        [XmlEnum("Convenience")]
        Convenience,

        [XmlEnum("DebtConsolidation")]
        DebtConsolidation,

        [XmlEnum("Education")]
        Education,

        [XmlEnum("EquityBuyout")]
        EquityBuyout,

        [XmlEnum("HomeImprovement")]
        HomeImprovement,

		/// <summary>
		/// To decrease the Note Rate Percent
		/// </summary>
        [XmlEnum("InterestRateReduction")]
        InterestRateReduction,

        [XmlEnum("Medical")]
        Medical,

		/// <summary>
		/// To shorten the maturity period of the note
		/// </summary>
        [XmlEnum("MortgageTermReduction")]
        MortgageTermReduction,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("PayoffLeaseholdInterest")]
        PayoffLeaseholdInterest,

        [XmlEnum("PrimaryLienPayoff")]
        PrimaryLienPayoff,

        [XmlEnum("SecondaryLienPayoff")]
        SecondaryLienPayoff,

        [XmlEnum("SpecialPurpose")]
        SpecialPurpose,

        [XmlEnum("UnsecuredLienPayoff")]
        UnsecuredLienPayoff,

        [XmlEnum("Unspecified")]
        Unspecified,

    }
}
