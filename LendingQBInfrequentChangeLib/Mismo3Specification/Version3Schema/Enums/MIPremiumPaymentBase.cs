namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Rolled into mortgage.
    /// </summary>
    public enum MIPremiumPaymentBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Funds are collected monthly and places in an escrow account to pay the anual priemium when due.
		/// </summary>
        [XmlEnum("Escrowed")]
        Escrowed,

		/// <summary>
		/// Rolled into mortgage.
		/// </summary>
        [XmlEnum("Financed")]
        Financed,

        [XmlEnum("Other")]
        Other,

    }
}
