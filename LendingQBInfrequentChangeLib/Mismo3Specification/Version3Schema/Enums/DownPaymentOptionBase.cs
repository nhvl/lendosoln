namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum DownPaymentOptionBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("FivePercentOption")]
        FivePercentOption,

        [XmlEnum("FNM97Option")]
        FNM97Option,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("ThreeTwoOption")]
        ThreeTwoOption,

    }
}
