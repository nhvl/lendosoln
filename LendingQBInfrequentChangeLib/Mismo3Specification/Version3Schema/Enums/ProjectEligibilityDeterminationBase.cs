namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Fannie Mae Project Eligibility Review Service, aka PERS
    /// </summary>
    public enum ProjectEligibilityDeterminationBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("FullReview")]
        FullReview,

        [XmlEnum("LimitedReview")]
        LimitedReview,

		/// <summary>
		/// Fannie Mae Project Eligibility Review Service, aka PERS
		/// </summary>
        [XmlEnum("ProjectEligibilityReviewService")]
        ProjectEligibilityReviewService,

    }
}
