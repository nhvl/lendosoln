namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum MarketInventoryMonthRangeBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("LastThreeMonths")]
        LastThreeMonths,

        [XmlEnum("PriorFourToSixMonths")]
        PriorFourToSixMonths,

        [XmlEnum("PriorSevenToTwelveMonths")]
        PriorSevenToTwelveMonths,

    }
}
