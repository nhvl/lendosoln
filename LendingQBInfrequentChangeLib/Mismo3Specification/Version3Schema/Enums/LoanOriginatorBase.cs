namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum LoanOriginatorBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Broker")]
        Broker,

        [XmlEnum("Correspondent")]
        Correspondent,

        [XmlEnum("Lender")]
        Lender,

        [XmlEnum("Other")]
        Other,

    }
}
