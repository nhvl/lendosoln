namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum GFECreditOrChargeForChosenInterestRateBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("BorrowerCharge")]
        BorrowerCharge,

        [XmlEnum("BorrowerCredit")]
        BorrowerCredit,

        [XmlEnum("CreditOrChargeIncludedInOriginationCharge")]
        CreditOrChargeIncludedInOriginationCharge,

        [XmlEnum("Other")]
        Other,

    }
}
