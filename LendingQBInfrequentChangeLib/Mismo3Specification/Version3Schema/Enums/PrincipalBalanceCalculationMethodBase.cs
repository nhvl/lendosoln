namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Project the actual principal balance forward to the change date, normal method of calculation.
    /// </summary>
    public enum PrincipalBalanceCalculationMethodBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Use the current principal balance at the current payment due date.
		/// </summary>
        [XmlEnum("CurrentPrincipalBalance")]
        CurrentPrincipalBalance,

		/// <summary>
		/// Project the actual principal balance forward to the change date, normal method of calculation.
		/// </summary>
        [XmlEnum("ProjectedPrincipalBalance")]
        ProjectedPrincipalBalance,

    }
}
