namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// The recording endorsement from the jurisdiction where the document is being recorded.  If the document needs to be recorded in more than one jurisdiction, this would be the used by the first jurisdiction to record the document.
    /// </summary>
    public enum RecordingEndorsementBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// The recording endorsement from any additional jurisdictions where the document is recorded.  Used only if a single document must be recorded in multiple jurisdictions and if the document submitted for recording already contains a completed PrimaryRecordingJurisdiction element.
		/// </summary>
        [XmlEnum("AdditionalRecordingJurisdiction")]
        AdditionalRecordingJurisdiction,

		/// <summary>
		/// An additional recording endorsement from the PrimaryRecordingJurisdicition.  Used when an error in a document is discovered after it has been recorded.  The document is corrected in accordance with jurisdictional requirements and resubmitted to the recording jurisdiction an additional time.
		/// </summary>
        [XmlEnum("CorrectiveEndorsement")]
        CorrectiveEndorsement,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// The recording endorsement information from a document that is already in the public record and is referenced in the body of a new document.  E.g.: a â€˜Satisfaction of Mortgageâ€™ would reference the recording endorsement information from the original mortgage in the body of the Satisfaction.
		/// </summary>
        [XmlEnum("PreviouslyRecordedReference")]
        PreviouslyRecordedReference,

		/// <summary>
		/// The recording endorsement from the jurisdiction where the document is being recorded.  If the document needs to be recorded in more than one jurisdiction, this would be the used by the first jurisdiction to record the document.
		/// </summary>
        [XmlEnum("PrimaryRecordingJurisdiction")]
        PrimaryRecordingJurisdiction,

    }
}
