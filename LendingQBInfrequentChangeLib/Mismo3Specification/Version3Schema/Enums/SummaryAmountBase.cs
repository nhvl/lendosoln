namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// The amount of the unused HELOC remaining.
    /// </summary>
    public enum SummaryAmountBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("SubtotalLiabilitiesForRentalPropertyBalance")]
        SubtotalLiabilitiesForRentalPropertyBalance,

        [XmlEnum("SubtotalLiabilitiesForRentalPropertyMonthlyPayment")]
        SubtotalLiabilitiesForRentalPropertyMonthlyPayment,

        [XmlEnum("SubtotalLiabilitiesMonthlyPayment")]
        SubtotalLiabilitiesMonthlyPayment,

        [XmlEnum("SubtotalLiabilitiesPaidByClosingNotIncludingSubjectPropertyLiensBalance")]
        SubtotalLiabilitiesPaidByClosingNotIncludingSubjectPropertyLiensBalance,

        [XmlEnum("SubtotalLiabilitiesPaidByClosingNotIncludingSubjectPropertyLiensMonthlyPayment")]
        SubtotalLiabilitiesPaidByClosingNotIncludingSubjectPropertyLiensMonthlyPayment,

        [XmlEnum("SubtotalLiquidAssetsNotIncludingGift")]
        SubtotalLiquidAssetsNotIncludingGift,

        [XmlEnum("SubtotalNonLiquidAssets")]
        SubtotalNonLiquidAssets,

        [XmlEnum("SubtotalOmittedLiabilitiesBalance")]
        SubtotalOmittedLiabilitiesBalance,

        [XmlEnum("SubtotalOmittedLiabilitiesMonthlyPayment")]
        SubtotalOmittedLiabilitiesMonthlyPayment,

        [XmlEnum("SubtotalResubordinatedLiabilitiesBalanceForSubjectProperty")]
        SubtotalResubordinatedLiabilitiesBalanceForSubjectProperty,

        [XmlEnum("SubtotalResubordinatedLiabilitiesMonthlyPaymentForSubjectProperty")]
        SubtotalResubordinatedLiabilitiesMonthlyPaymentForSubjectProperty,

        [XmlEnum("SubtotalSubjectPropertyLiensPaidByClosingBalance")]
        SubtotalSubjectPropertyLiensPaidByClosingBalance,

        [XmlEnum("SubtotalSubjectPropertyLiensPaidByClosingMonthlyPayment")]
        SubtotalSubjectPropertyLiensPaidByClosingMonthlyPayment,

        [XmlEnum("TotalLiabilitiesBalance")]
        TotalLiabilitiesBalance,

        [XmlEnum("TotalMonthlyIncomeNotIncludingNetRentalIncome")]
        TotalMonthlyIncomeNotIncludingNetRentalIncome,

        [XmlEnum("TotalPresentHousingExpense")]
        TotalPresentHousingExpense,

		/// <summary>
		/// The amount of the unused HELOC remaining.
		/// </summary>
        [XmlEnum("UndrawnHELOC")]
        UndrawnHELOC,

    }
}
