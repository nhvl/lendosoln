namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// This value indicates that a SYSTEM_SIGNATURE is applied to the REFERENCE or ObjectURL element; the resource it points to is not signed. The REFERENCE or ObjectURL element MUST NOT change or be resolved, the resource SHOULD remain available, but its trustworthiness is outside the scope of the SYSTEM_SIGNATURES.
    /// </summary>
    public enum ReferenceSigningBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// This value indicates that none of the SYSTEM_SIGNATURES are applied to the REFERENCE or ObjectURL element and the resource it points to is not signed. The REFERENCE or ObjectURL element MAY change or be resolved, the resource MAY remain available, and its trustworthiness is outside the scope of the SYSTEM_SIGNATURES.
		/// </summary>
        [XmlEnum("NotSigned")]
        NotSigned,

		/// <summary>
		/// This value indicates that a SYSTEM_SIGNATURE is applied to the REFERENCE or ObjectURL element; the resource it points to has also been signed. The REFERENCE or ObjectURL element MUST NOT change or be resolved, the resource MUST remain available, and its trustworthiness can be determined from the Signature.
		/// </summary>
        [XmlEnum("ReferenceAndContentSignedDirectly")]
        ReferenceAndContentSignedDirectly,

		/// <summary>
		/// This value indicates that a SYSTEM_SIGNATURE is applied to the REFERENCE element or the ObjectURL and its sibling elements after computing and saving the OriginalCreatorDigestValue; the resource it points to is not signed. The REFERENCE or ObjectURL element MUST NOT change or be resolved, the resource SHOULD remain available, and its trustworthiness can be determined from the OriginalCreatorDigestValue.
		/// </summary>
        [XmlEnum("ReferenceAndContentSignedIndirectly")]
        ReferenceAndContentSignedIndirectly,

		/// <summary>
		/// This value indicates that the resource pointed to by the REFERENCE or ObjectURL element was originally embedded in the current XML instance document that has SYSTEM_SIGNATURES applied. Prior to validating a SYSTEM_SIGNATURE or applying a new one, the reference MUST be resolved to maintain or determine its trustworthiness.
		/// </summary>
        [XmlEnum("ReferenceCreatedFromSignedContent")]
        ReferenceCreatedFromSignedContent,

		/// <summary>
		/// This value indicates that a SYSTEM_SIGNATURE is applied to the REFERENCE or ObjectURL element; the resource it points to is not signed. The REFERENCE or ObjectURL element MUST NOT change or be resolved, the resource SHOULD remain available, but its trustworthiness is outside the scope of the SYSTEM_SIGNATURES.
		/// </summary>
        [XmlEnum("ReferenceSignedDirectly")]
        ReferenceSignedDirectly,

    }
}
