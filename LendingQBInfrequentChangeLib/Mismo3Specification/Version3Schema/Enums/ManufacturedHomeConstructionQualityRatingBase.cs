namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum ManufacturedHomeConstructionQualityRatingBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Average")]
        Average,

        [XmlEnum("Excellent")]
        Excellent,

        [XmlEnum("Fair")]
        Fair,

        [XmlEnum("Poor")]
        Poor,

    }
}
