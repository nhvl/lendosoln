namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A match was found with a name in the product database.
    /// </summary>
    public enum RegulatoryProductResultStatusBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// No match found with any names in product database.
		/// </summary>
        [XmlEnum("Clear")]
        Clear,

		/// <summary>
		/// Product result not available for reason listed in Regulatory Product Result Text.
		/// </summary>
        [XmlEnum("Error")]
        Error,

		/// <summary>
		/// A match was found with a name in the product database.
		/// </summary>
        [XmlEnum("Match")]
        Match,

        [XmlEnum("Other")]
        Other,

    }
}
