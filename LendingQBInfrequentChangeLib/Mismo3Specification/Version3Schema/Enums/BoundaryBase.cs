namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum BoundaryBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Alley")]
        Alley,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Street")]
        Street,

        [XmlEnum("Waterway")]
        Waterway,
    }
}
