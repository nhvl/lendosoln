namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum NeighborhoodTypicalMarketingMonthsDurationBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("OverSixMonths")]
        OverSixMonths,

        [XmlEnum("ThreeToSixMonths")]
        ThreeToSixMonths,

        [XmlEnum("UnderThreeMonths")]
        UnderThreeMonths,

    }
}
