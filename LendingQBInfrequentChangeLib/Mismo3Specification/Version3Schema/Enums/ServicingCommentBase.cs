namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum ServicingCommentBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("CancelWorkout")]
        CancelWorkout,

        [XmlEnum("Collection")]
        Collection,

        [XmlEnum("CustomerService")]
        CustomerService,

        [XmlEnum("Default")]
        Default,

        [XmlEnum("General")]
        General,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Workout")]
        Workout,

    }
}
