namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum LoanDefaultLossPartyBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Investor")]
        Investor,

        [XmlEnum("Lender")]
        Lender,

        [XmlEnum("LoanSeller")]
        LoanSeller,

        [XmlEnum("MortgageInsuranceCompany")]
        MortgageInsuranceCompany,

        [XmlEnum("Servicer")]
        Servicer,

        [XmlEnum("Shared")]
        Shared,

    }
}
