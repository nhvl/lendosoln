namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Interest capitalized for a payment posted to the servicer's system but not yet reported during the scheduled investor reporting cycle.
    /// </summary>
    public enum CapitalizationChargeBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("AttorneyFees")]
        AttorneyFees,

        [XmlEnum("DelinquentHomeownersFees")]
        DelinquentHomeownersFees,

        [XmlEnum("DelinquentInterest")]
        DelinquentInterest,

        [XmlEnum("EscrowShortages")]
        EscrowShortages,

        [XmlEnum("LateCharge")]
        LateCharge,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Interest capitalized for a payment posted to the servicer's system but not yet reported during the scheduled investor reporting cycle.
		/// </summary>
        [XmlEnum("UnreportedInterest")]
        UnreportedInterest,

    }
}
