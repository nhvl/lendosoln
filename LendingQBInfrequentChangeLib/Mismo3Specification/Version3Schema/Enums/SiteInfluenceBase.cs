namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum SiteInfluenceBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Easements")]
        Easements,

        [XmlEnum("Encroachments")]
        Encroachments,

        [XmlEnum("EnvironmentalConditions")]
        EnvironmentalConditions,

        [XmlEnum("LandUses")]
        LandUses,

        [XmlEnum("Location")]
        Location,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("View")]
        View,

        [XmlEnum("WaterRights")]
        WaterRights,

    }
}
