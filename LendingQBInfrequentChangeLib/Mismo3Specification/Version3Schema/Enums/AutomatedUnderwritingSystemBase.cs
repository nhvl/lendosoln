namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Chase
    /// </summary>
    public enum AutomatedUnderwritingSystemBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// GMAC / RFC
		/// </summary>
        [XmlEnum("Assetwise")]
        Assetwise,

        [XmlEnum("Capstone")]
        Capstone,

		/// <summary>
		/// Bank of America
		/// </summary>
        [XmlEnum("Clues")]
        Clues,

		/// <summary>
		/// Fannie Mae
		/// </summary>
        [XmlEnum("DesktopUnderwriter")]
        DesktopUnderwriter,

		/// <summary>
		/// Wells Fargo
		/// </summary>
        [XmlEnum("ECS")]
        ECS,

		/// <summary>
		/// The loan scoring tool used by lenders to manage workflow and expedite the endorsement process for FHA loans. The FHA TOTAL Scorecard evaluates the overall creditworthiness of the applicants and determines an associated risk level of a loan's eligibility for insurance by FHA. 
		/// </summary>
        [XmlEnum("FHAScorecard")]
        FHAScorecard,

		/// <summary>
		///  Freddie Mac
		/// </summary>
        [XmlEnum("LoanProspector")]
        LoanProspector,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Strategyware")]
        Strategyware,

		/// <summary>
		/// Chase
		/// </summary>
        [XmlEnum("Zippy")]
        Zippy,
    }
}
