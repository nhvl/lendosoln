namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum PropertyPartialInterestBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("AirRights")]
        AirRights,

        [XmlEnum("Easement")]
        Easement,

        [XmlEnum("GroundLease")]
        GroundLease,

        [XmlEnum("HuntingRights")]
        HuntingRights,

        [XmlEnum("MineralRights")]
        MineralRights,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("WaterRights")]
        WaterRights,

    }
}
