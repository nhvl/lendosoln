namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Based on the data submitted, the loan appears to meet eligibility requirements for the evaluated workout.
    /// </summary>
    public enum WorkoutQualificationStatusBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// All of the information that has been provided has passed qualification but one or  more pieces of information are missing.
		/// </summary>
        [XmlEnum("ConditionallyQualified")]
        ConditionallyQualified,

		/// <summary>
		/// Based on the data submitted, the loan does not meet eligibility requirements for the evaluated workout.
		/// </summary>
        [XmlEnum("NotQualified")]
        NotQualified,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// PreQualified
		/// </summary>
        [XmlEnum("PreQualified")]
        PreQualified,

		/// <summary>
		/// Based on the data submitted, the loan appears to meet eligibility requirements for the evaluated workout.
		/// </summary>
        [XmlEnum("Qualified")]
        Qualified,

    }
}
