namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum MIPremiumCalculationBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("AverageAnnualScheduledUnpaidPrincipalBalance")]
        AverageAnnualScheduledUnpaidPrincipalBalance,

        [XmlEnum("BaseLoanAmount")]
        BaseLoanAmount,

        [XmlEnum("Constant")]
        Constant,

        [XmlEnum("Declining")]
        Declining,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("PercentOfCurrentBalance")]
        PercentOfCurrentBalance,

        [XmlEnum("PercentOfOriginalBalance")]
        PercentOfOriginalBalance,

    }
}
