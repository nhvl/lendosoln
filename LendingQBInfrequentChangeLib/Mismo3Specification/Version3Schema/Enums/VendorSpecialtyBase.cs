namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum VendorSpecialtyBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Agricultural")]
        Agricultural,

        [XmlEnum("Bilingual")]
        Bilingual,

        [XmlEnum("ComplexProperties")]
        ComplexProperties,

        [XmlEnum("HistoricProperties")]
        HistoricProperties,

        [XmlEnum("LitigationExperience")]
        LitigationExperience,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("VacantLand")]
        VacantLand,

    }
}
