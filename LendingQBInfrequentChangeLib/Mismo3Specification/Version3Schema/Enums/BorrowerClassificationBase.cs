namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Also know as co-borrower
    /// </summary>
    public enum BorrowerClassificationBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Primary")]
        Primary,

		/// <summary>
		/// Also know as co-borrower
		/// </summary>
        [XmlEnum("Secondary")]
        Secondary,
    }
}
