namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// The property has been condemned by the local housing authority.
    /// </summary>
    public enum DelinquencyReportingPropertyOverallConditionBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// The property has been condemned by the local housing authority.
		/// </summary>
        [XmlEnum("Condemned")]
        Condemned,

        [XmlEnum("Excellent")]
        Excellent,

        [XmlEnum("Fair")]
        Fair,

        [XmlEnum("Good")]
        Good,

        [XmlEnum("NoPropertyInspection")]
        NoPropertyInspection,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Poor")]
        Poor,

        [XmlEnum("PropertyInaccessible")]
        PropertyInaccessible,

        [XmlEnum("RemovedOrDestroyed")]
        RemovedOrDestroyed,

        [XmlEnum("Unknown")]
        Unknown,

    }
}
