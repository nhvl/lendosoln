namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// </summary>
    public enum SecuritySystemBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// An alarm system that is connected to a central commerical monitoring station and allows two way communication between the property and the monitoring station.
		/// </summary>
        [XmlEnum("InteractiveAlarmSystem")]
        InteractiveAlarmSystem,

		/// <summary>
		/// An alarm system that is not connected to a central commerical monitoring station.
		/// </summary>
        [XmlEnum("LocalAlarmSystem")]
        LocalAlarmSystem,

		/// <summary>
		/// An alarm system that is connected to a central commerical monitoring station.
		/// </summary>
        [XmlEnum("MonitoredAlarmSystem")]
        MonitoredAlarmSystem,

		/// <summary>
		/// 
		/// </summary>
        [XmlEnum("Other")]
        Other,

    }
}
