namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum TypicalApartmentVacancyTrendBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Decreasing")]
        Decreasing,

        [XmlEnum("Increasing")]
        Increasing,

        [XmlEnum("Stable")]
        Stable,

    }
}
