namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Based on calculations per MI company's servicing guidelines the workout is denied due to the offer being unacceptable.
    /// </summary>
    public enum MIWorkoutDenialReasonBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Based on calculations per MI Company's servicing guidelines the borrower is not able to afford the restructured payment for the workout.
		/// </summary>
        [XmlEnum("BorrowerCannotAfford")]
        BorrowerCannotAfford,

		/// <summary>
		/// The workout option is denied because the borrower qualifies for another preferred workout option.
		/// </summary>
        [XmlEnum("BorrowerQualifiesForDifferentWorkoutOption")]
        BorrowerQualifiesForDifferentWorkoutOption,

		/// <summary>
		/// Finalized transaction did not meet delegated guidelines.
		/// </summary>
        [XmlEnum("DelegatedAuthorityNotCompliant")]
        DelegatedAuthorityNotCompliant,

		/// <summary>
		/// Borrower has assets that can be contributed toward the workout but has refused to do so.
		/// </summary>
        [XmlEnum("HasAssetsAndRefusedToContribute")]
        HasAssetsAndRefusedToContribute,

		/// <summary>
		/// Borrower's agreed contribution amount did not meet contribution requirement.
		/// </summary>
        [XmlEnum("InsufficientContribution")]
        InsufficientContribution,

		/// <summary>
		/// Documentation required by the MI in order to provide a decision on a workout request was not provided.
		/// </summary>
        [XmlEnum("MissingDocumentation")]
        MissingDocumentation,

		/// <summary>
		/// Based on calculations per MI company's servicing guidelines the hardship being experienced by the borrower is not a legitmate reason for a workout.
		/// </summary>
        [XmlEnum("NoLegitimateHardship")]
        NoLegitimateHardship,

		/// <summary>
		/// The short sale request was denied due to the transaction not being arms length.
		/// </summary>
        [XmlEnum("NonArmsLengthTransaction")]
        NonArmsLengthTransaction,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Second lien holder will not accept settlement to release the second lien from the title.
		/// </summary>
        [XmlEnum("SecondLienHolderRefusedRelease")]
        SecondLienHolderRefusedRelease,

		/// <summary>
		/// The second lien holder will not resubordinate their lien in order for the first lien to refinance.
		/// </summary>
        [XmlEnum("SecondLienHolderWillNotResubordinate")]
        SecondLienHolderWillNotResubordinate,

		/// <summary>
		/// The MI company workout decision is delegated to the servicer.
		/// </summary>
        [XmlEnum("ServicerDelegated")]
        ServicerDelegated,

		/// <summary>
		/// Based on calculations borrower has sufficient income to support and/or pay off the debt.
		/// </summary>
        [XmlEnum("SufficientIncomeToSupportDebt")]
        SufficientIncomeToSupportDebt,

		/// <summary>
		/// Based on calculations per MI company's servicing guidelines the workout is denied due to the subject property being torn down or is unable to be repaired.
		/// </summary>
        [XmlEnum("TornDownOrNotRepairable")]
        TornDownOrNotRepairable,

		/// <summary>
		/// Based on calculations per MI company's servicing guidelines the workout is denied due to the offer being unacceptable.
		/// </summary>
        [XmlEnum("UnacceptableOffer")]
        UnacceptableOffer,

    }
}
