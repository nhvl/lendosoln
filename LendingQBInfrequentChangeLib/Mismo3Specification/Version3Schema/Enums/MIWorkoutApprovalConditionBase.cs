namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// The workout is approved with the MI company retaining the right to pursue a deficiency judgement.
    /// </summary>
    public enum MIWorkoutApprovalConditionBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// The MI company will advance partial funds to mitigate the potential risk of a future claim.
		/// </summary>
        [XmlEnum("AdvanceFundsPartial")]
        AdvanceFundsPartial,

		/// <summary>
		/// The workout is approved with a contribution needed from the borrower.
		/// </summary>
        [XmlEnum("BorrowerContribution")]
        BorrowerContribution,

		/// <summary>
		/// The past due amounts will be capitalized as part of the workout.
		/// </summary>
        [XmlEnum("DelinquencyCapitalization")]
        DelinquencyCapitalization,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// The workout is approved with the MI company retaining the right to pursue a deficiency judgement.
		/// </summary>
        [XmlEnum("WithRightToPursueDeficiencyJudgement")]
        WithRightToPursueDeficiencyJudgement,

    }
}
