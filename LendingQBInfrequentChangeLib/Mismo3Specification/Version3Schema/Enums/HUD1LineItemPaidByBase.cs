namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum HUD1LineItemPaidByBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Buyer")]
        Buyer,

        [XmlEnum("LenderPremium")]
        LenderPremium,

        [XmlEnum("Seller")]
        Seller,

    }
}
