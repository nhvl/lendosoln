namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Statement is sent to Borrower according to Billing Statement Frequency and Borrower is supplied with coupons.
    /// </summary>
    public enum PaymentBillingMethodBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Statement sent based on transactions.
		/// </summary>
        [XmlEnum("BillOnReceipt")]
        BillOnReceipt,

		/// <summary>
		/// Borrower is supplied with coupons
		/// </summary>
        [XmlEnum("Coupons")]
        Coupons,

        [XmlEnum("NoBilling")]
        NoBilling,

		/// <summary>
		/// Statement is sent to Borrower according to Billing Statement Frequency.
		/// </summary>
        [XmlEnum("Statement")]
        Statement,

		/// <summary>
		/// Statement is sent to Borrower according to Billing Statement Frequency and Borrower is supplied with coupons.
		/// </summary>
        [XmlEnum("StatementAndCoupon")]
        StatementAndCoupon,

    }
}
