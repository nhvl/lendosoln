namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum BuildingPermitLevelBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Final")]
        Final,

        [XmlEnum("Maintenance")]
        Maintenance,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Recurring")]
        Recurring,

        [XmlEnum("SubWork")]
        SubWork,

        [XmlEnum("Temporary")]
        Temporary,

        [XmlEnum("Transitional")]
        Transitional,
    }
}
