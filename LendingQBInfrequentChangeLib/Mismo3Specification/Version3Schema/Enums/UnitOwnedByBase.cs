namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum UnitOwnedByBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Developer")]
        Developer,

        [XmlEnum("Investor")]
        Investor,

        [XmlEnum("OwnerOccupier")]
        OwnerOccupier,

    }
}
