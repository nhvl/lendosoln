namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// The loan is in a seniority class due to traunching of a pool of loans.
    /// </summary>
    public enum CreditEnhancementBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Cash amount earmarked for repurchase.
		/// </summary>
        [XmlEnum("CashAsset")]
        CashAsset,

        [XmlEnum("CollateralTaxAssessmentValue")]
        CollateralTaxAssessmentValue,

        [XmlEnum("CommercialCollateral")]
        CommercialCollateral,

        [XmlEnum("Excluded")]
        Excluded,

		/// <summary>
		/// Servicer obligation to make investor whole for any losses incurred.
		/// </summary>
        [XmlEnum("Indemnification")]
        Indemnification,

		/// <summary>
		/// Credit amount earmarked for repurchase.
		/// </summary>
        [XmlEnum("LetterOfCredit")]
        LetterOfCredit,

        [XmlEnum("LimitedRecourse")]
        LimitedRecourse,

		/// <summary>
		/// Servicer agrees to share loss with the investor.
		/// </summary>
        [XmlEnum("LossParticipation")]
        LossParticipation,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// The loan belongs to a pool  covered by an insurance policy.
		/// </summary>
        [XmlEnum("PoolInsurance")]
        PoolInsurance,

        [XmlEnum("Recourse")]
        Recourse,

		/// <summary>
		/// MI policy that covers losses that exceed those covered by the primary credit enhancement.
		/// </summary>
        [XmlEnum("SecondTierMI")]
        SecondTierMI,

		/// <summary>
		/// The loan is in a seniority class due to traunching of a pool of loans.
		/// </summary>
        [XmlEnum("SubordinationAgreement")]
        SubordinationAgreement,

    }
}
