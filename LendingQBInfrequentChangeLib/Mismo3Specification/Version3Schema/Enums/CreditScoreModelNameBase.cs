namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Version 3.0 of the score model developed jointly by the three credit data repositories - Equifax, Experian and Trans Union.
    /// </summary>
    public enum CreditScoreModelNameBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Beacon09MortgageIndustryOption")]
        Beacon09MortgageIndustryOption,

		/// <summary>
		/// The Equifax score model, Bankruptcy Navigator Index 3.0, predicts the likelihood of bankruptcy within the next 24 months. Consumer files with bankruptcies are not scored. Score Scale is 1 - 600.
		/// </summary>
        [XmlEnum("EquifaxBankruptcyNavigatorIndex02781")]
        EquifaxBankruptcyNavigatorIndex02781,

		/// <summary>
		/// The Equifax score model, Bankruptcy Navigator Index 3.0, predicts the likelihood of bankruptcy within the next 24 months. Scores files with previous bankruptcies. Score Scale is 1 - 600.
		/// </summary>
        [XmlEnum("EquifaxBankruptcyNavigatorIndex02782")]
        EquifaxBankruptcyNavigatorIndex02782,

		/// <summary>
		/// The Equifax score model, Bankruptcy Navigator Index 3.0, predicts the likelihood of bankruptcy within the next 24 months. Consumer files with bankruptcies are not scored. Score Scale is 1 - 200.
		/// </summary>
        [XmlEnum("EquifaxBankruptcyNavigatorIndex02783")]
        EquifaxBankruptcyNavigatorIndex02783,

		/// <summary>
		/// The Equifax score model, Bankruptcy Navigator Index 3.0, predicts the likelihood of bankruptcy within the next 24 months. Scores files with previous bankruptcies.  Score Scale is 1 - 300.
		/// </summary>
        [XmlEnum("EquifaxBankruptcyNavigatorIndex02784")]
        EquifaxBankruptcyNavigatorIndex02784,

		/// <summary>
		/// Fair Isaac risk model forecasting delinquency within 24 months.
		/// </summary>
        [XmlEnum("EquifaxBeacon")]
        EquifaxBeacon,

		/// <summary>
		/// Fair Isaac risk model forecasting delinquency within 24 months. Updated in 2003.
		/// </summary>
        [XmlEnum("EquifaxBeacon5.0")]
        EquifaxBeacon50,

		/// <summary>
		/// Fair Isaac risk model for auto finance loans. Updated in 2003.
		/// </summary>
        [XmlEnum("EquifaxBeacon5.0Auto")]
        EquifaxBeacon50Auto,

		/// <summary>
		/// Fair Isaac risk model for bank credit cards. Updated in 2003.
		/// </summary>
        [XmlEnum("EquifaxBeacon5.0BankCard")]
        EquifaxBeacon50BankCard,

		/// <summary>
		/// Fair Isaac risk model for installment loans. Updated in 2003.
		/// </summary>
        [XmlEnum("EquifaxBeacon5.0Installment")]
        EquifaxBeacon50Installment,

		/// <summary>
		/// Fair Isaac risk model for personal finance loans.
		/// </summary>
        [XmlEnum("EquifaxBeacon5.0PersonalFinance")]
        EquifaxBeacon50PersonalFinance,

		/// <summary>
		/// Fair Isaac risk model for auto finance loans.
		/// </summary>
        [XmlEnum("EquifaxBeaconAuto")]
        EquifaxBeaconAuto,

		/// <summary>
		/// Fair Isaac risk model for bank credit cards.
		/// </summary>
        [XmlEnum("EquifaxBeaconBankcard")]
        EquifaxBeaconBankcard,

		/// <summary>
		/// Fair Isaac risk model for installment loans.
		/// </summary>
        [XmlEnum("EquifaxBeaconInstallment")]
        EquifaxBeaconInstallment,

		/// <summary>
		/// Fair Isaac risk model for personal finance loans.
		/// </summary>
        [XmlEnum("EquifaxBeaconPersonalFinance")]
        EquifaxBeaconPersonalFinance,

		/// <summary>
		/// Delinquency Alert System is a MDS risk model that forecasts risk of consumer filing bankruptcy.
		/// </summary>
        [XmlEnum("EquifaxDAS")]
        EquifaxDAS,

		/// <summary>
		/// An updated Fair Isaac risk model forecasting the risk of delinquency within 24 months.
		/// </summary>
        [XmlEnum("EquifaxEnhancedBeacon")]
        EquifaxEnhancedBeacon,

		/// <summary>
		/// Enhanced Delinquency Alert System is a MDS risk model that forecasts risk of consumer filing bankruptcy.
		/// </summary>
        [XmlEnum("EquifaxEnhancedDAS")]
        EquifaxEnhancedDAS,

		/// <summary>
		/// Equifax predictive score for no-hit or thin files
		/// </summary>
        [XmlEnum("EquifaxMarketMax")]
        EquifaxMarketMax,

		/// <summary>
		/// A custom risk score model from Equifax specifically for assessing mortgage loan risk.
		/// </summary>
        [XmlEnum("EquifaxMortgageScore")]
        EquifaxMortgageScore,

		/// <summary>
		/// The Fair Isaac Next Gen risk model that utilizes expanded segmentation to provide greater predictive capability.
		/// </summary>
        [XmlEnum("EquifaxPinnacle")]
        EquifaxPinnacle,

		/// <summary>
		/// The Fair Isaac Next Gen 2 risk model that utilizes expanded segmentation to provide greater predictive capability. Updated in 2003.
		/// </summary>
        [XmlEnum("EquifaxPinnacle2.0")]
        EquifaxPinnacle20,

		/// <summary>
		/// A score model developed jointly by the three credit data repositories - Equifax, Experian and Trans Union.
		/// </summary>
        [XmlEnum("EquifaxVantageScore")]
        EquifaxVantageScore,

		/// <summary>
		/// Version 3.0 of the score model developed jointly by the three credit data repositories - Equifax, Experian and Trans Union.
		/// </summary>
        [XmlEnum("EquifaxVantageScore3.0")]
        EquifaxVantageScore30,

		/// <summary>
		/// Fair Isaac risk model forecasting risk of delinquency within 24 months.
		/// </summary>
        [XmlEnum("ExperianFairIsaac")]
        ExperianFairIsaac,

		/// <summary>
		/// The Fair Isaac Next Gen risk model that utilizes expanded segmentation to provide greater predictive capability.
		/// </summary>
        [XmlEnum("ExperianFairIsaacAdvanced")]
        ExperianFairIsaacAdvanced,

		/// <summary>
		/// The Fair Isaac Next Gen 2 risk model that utilizes expanded segmentation to provide greater predictive capability. Updated in 2003.
		/// </summary>
        [XmlEnum("ExperianFairIsaacAdvanced2.0")]
        ExperianFairIsaacAdvanced20,

		/// <summary>
		/// Fair Isaac risk model for auto finance.
		/// </summary>
        [XmlEnum("ExperianFairIsaacAuto")]
        ExperianFairIsaacAuto,

		/// <summary>
		/// Fair Isaac risk model for bank credit cards.
		/// </summary>
        [XmlEnum("ExperianFairIsaacBankcard")]
        ExperianFairIsaacBankcard,

		/// <summary>
		/// Fair Isaac risk model for installment loans.
		/// </summary>
        [XmlEnum("ExperianFairIsaacInstallment")]
        ExperianFairIsaacInstallment,

		/// <summary>
		/// Fair Isaac risk model for personal finance loans.
		/// </summary>
        [XmlEnum("ExperianFairIsaacPersonalFinance")]
        ExperianFairIsaacPersonalFinance,

		/// <summary>
		/// Fair Isaac risk model forecasting risk of delinquency within 24 months. Updated in 2004.
		/// </summary>
        [XmlEnum("ExperianFICOClassicV3")]
        ExperianFICOClassicV3,

		/// <summary>
		/// MDS risk model that forecasts risk of consumer filing bankruptcy.
		/// </summary>
        [XmlEnum("ExperianMDSBankruptcyII")]
        ExperianMDSBankruptcyII,

		/// <summary>
		/// Experian option for the National Risk Model that produces score ranges similar to Fair Isaac models.
		/// </summary>
        [XmlEnum("ExperianNewNationalEquivalency")]
        ExperianNewNationalEquivalency,

		/// <summary>
		/// New version of Experians National Risk Model.
		/// </summary>
        [XmlEnum("ExperianNewNationalRisk")]
        ExperianNewNationalRisk,

		/// <summary>
		/// Original version of Experians National Risk Model.
		/// </summary>
        [XmlEnum("ExperianOldNationalRisk")]
        ExperianOldNationalRisk,

		/// <summary>
		/// An Experian risk model based on their own credit data.
		/// </summary>
        [XmlEnum("ExperianScorexPLUS")]
        ExperianScorexPLUS,

		/// <summary>
		/// A score model developed jointly by the three credit data repositories - Equifax, Experian and Trans Union.
		/// </summary>
        [XmlEnum("ExperianVantageScore")]
        ExperianVantageScore,

		/// <summary>
		/// Version 3.0 of the score model developed jointly by the three credit data repositories - Equifax, Experian and Trans Union.
		/// </summary>
        [XmlEnum("ExperianVantageScore3.0")]
        ExperianVantageScore30,

		/// <summary>
		/// Fair Isaac risk model based on non-traditional credit data, targeted to predict risk for consumers with non-existent or thin credit histories.
		/// </summary>
        [XmlEnum("FICOExpansionScore")]
        FICOExpansionScore,

		/// <summary>
		/// Fair Isaac risk model forecasting risk of delinquency within 24 months. Updated in 2004.
		/// </summary>
        [XmlEnum("FICORiskScoreClassic04")]
        FICORiskScoreClassic04,

		/// <summary>
		/// Fair Isaac risk model forecasting risk of delinquency within 24 months. New Trans Union product name for Empirica.
		/// </summary>
        [XmlEnum("FICORiskScoreClassic98")]
        FICORiskScoreClassic98,

		/// <summary>
		/// Fair Isaac risk model for auto finance loans. New Trans Union product name for Empirica Auto.
		/// </summary>
        [XmlEnum("FICORiskScoreClassicAuto98")]
        FICORiskScoreClassicAuto98,

		/// <summary>
		/// Fair Isaac risk model for bank credit cards. New Trans Union product name for Empirica Bankcard.
		/// </summary>
        [XmlEnum("FICORiskScoreClassicBankcard98")]
        FICORiskScoreClassicBankcard98,

		/// <summary>
		/// Fair Isaac risk model for installment loans. New Trans Union product name for Empirica Installment.
		/// </summary>
        [XmlEnum("FICORiskScoreClassicInstallmentLoan98")]
        FICORiskScoreClassicInstallmentLoan98,

		/// <summary>
		/// Fair Isaac risk model for personal finance loans. New Trans Union product name for Empirica Personal Finance.
		/// </summary>
        [XmlEnum("FICORiskScoreClassicPersonalFinance98")]
        FICORiskScoreClassicPersonalFinance98,

		/// <summary>
		/// The Fair Isaac Next Gen risk model that utilizes expanded segmentation to provide greater predictive capability. New Trans Union product name for Precision.
		/// </summary>
        [XmlEnum("FICORiskScoreNextGen00")]
        FICORiskScoreNextGen00,

		/// <summary>
		/// The Fair Isaac Next Gen 2 risk model that utilizes expanded segmentation to provide greater predictive capability. Updated in 2003. New Trans Union product name for Precision 03.
		/// </summary>
        [XmlEnum("FICORiskScoreNextGen03")]
        FICORiskScoreNextGen03,

		/// <summary>
		/// Used when Score Model Name is not in the enumerated list.
		/// </summary>
        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// MDS risk model that forecasts risk of consumer filing bankruptcy.
		/// </summary>
        [XmlEnum("TransUnionDelphi")]
        TransUnionDelphi,

		/// <summary>
		/// Fair Isaac risk model forecasting risk of delinquency within 24 months. Trans Union has renamed this risk model to FICO Risk Score Classic 98.
		/// </summary>
        [XmlEnum("TransUnionEmpirica")]
        TransUnionEmpirica,

		/// <summary>
		/// Fair Isaac risk model for auto finance loans. Trans Union has renamed this risk model to FICO Risk Score Classic Auto 98.
		/// </summary>
        [XmlEnum("TransUnionEmpiricaAuto")]
        TransUnionEmpiricaAuto,

		/// <summary>
		/// Fair Isaac risk model for bank credit cards. Trans Union has renamed this risk model to FICO Risk Score Classic Bankcard 98.
		/// </summary>
        [XmlEnum("TransUnionEmpiricaBankcard")]
        TransUnionEmpiricaBankcard,

		/// <summary>
		/// Fair Isaac risk model for installment loans. Trans Union has renamed this risk model to FICO Risk Score Classic Installment 98.
		/// </summary>
        [XmlEnum("TransUnionEmpiricaInstallment")]
        TransUnionEmpiricaInstallment,

		/// <summary>
		/// Fair Isaac risk model for personal finance loans. Trans Union has renamed this risk model to FICO Risk Score Classic Personal Finance 98.
		/// </summary>
        [XmlEnum("TransUnionEmpiricaPersonalFinance")]
        TransUnionEmpiricaPersonalFinance,

		/// <summary>
		/// Updated MDS risk model that forecasts risk of consumer filing bankruptcy.
		/// </summary>
        [XmlEnum("TransUnionNewDelphi")]
        TransUnionNewDelphi,

		/// <summary>
		/// The Fair Isaac Next Gen risk model that utilizes expanded segmentation to provide greater predictive capability. Trans Union has renamed this risk model to FICO Risk Score Next Gen 00.
		/// </summary>
        [XmlEnum("TransUnionPrecision")]
        TransUnionPrecision,

		/// <summary>
		/// The Fair Isaac Next Gen 2 risk model that utilizes expanded segmentation to provide greater predictive capability. Updated in 2003. Trans Union has renamed this risk model to FICO Risk Score Next Gen 03.
		/// </summary>
        [XmlEnum("TransUnionPrecision03")]
        TransUnionPrecision03,

		/// <summary>
		/// A score model developed jointly by the three credit data repositories - Equifax, Experian and Trans Union.
		/// </summary>
        [XmlEnum("TransUnionVantageScore")]
        TransUnionVantageScore,

		/// <summary>
		/// Version 3.0 of the score model developed jointly by the three credit data repositories - Equifax, Experian and Trans Union.
		/// </summary>
        [XmlEnum("TransUnionVantageScore3.0")]
        TransUnionVantageScore30,

    }
}
