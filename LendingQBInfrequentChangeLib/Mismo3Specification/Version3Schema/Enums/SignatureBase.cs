namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Used for non-electronic signatures.
    /// </summary>
    public enum SignatureBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Denotes a digital signature. The signer uses a digital certificate to apply a PKI-based signature to the document. The certificate maybe a personal certificate of the signer, a system certificate, or a dynamically generated certificate created for that transaction. After signing a signature appearance is generated and place on the signature field in the view. 
		/// </summary>
        [XmlEnum("Digital")]
        Digital,

		/// <summary>
		/// Denotes an image signature. A graphic image of the signers signature  is captured and placed in signature field.
		/// </summary>
        [XmlEnum("Image")]
        Image,

		/// <summary>
		/// Denotes that the signature type is other than the ones listed above. When this option is selected, a description of the signature type must be provided in the SignatureTypeOtherDescription attribute.
		/// </summary>
        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Denotes a text signature. The signer types his/her name (signature) in the signature field or clicks a button onscreen indicating acceptance or agreement.
		/// </summary>
        [XmlEnum("Text")]
        Text,

		/// <summary>
		/// Used for non-electronic signatures.
		/// </summary>
        [XmlEnum("Wet")]
        Wet,

    }
}
