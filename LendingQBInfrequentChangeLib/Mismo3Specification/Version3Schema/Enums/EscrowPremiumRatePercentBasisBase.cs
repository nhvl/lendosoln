namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum EscrowPremiumRatePercentBasisBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("BorrowerRequestedLoanAmount")]
        BorrowerRequestedLoanAmount,

        [XmlEnum("NoteAmount")]
        NoteAmount,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("PropertyValuationAmount")]
        PropertyValuationAmount,

        [XmlEnum("PurchasePriceAmount")]
        PurchasePriceAmount,

        [XmlEnum("TotalLoanAmount")]
        TotalLoanAmount,

    }
}
