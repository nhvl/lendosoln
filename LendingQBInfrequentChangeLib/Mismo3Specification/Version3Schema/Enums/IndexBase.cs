namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Often referred to as MTA
    /// </summary>
    public enum IndexBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Often referred to as Prime Rate
		/// </summary>
        [XmlEnum("BankPrimeLoan")]
        BankPrimeLoan,

		/// <summary>
		/// Often referred to as CODI
		/// </summary>
        [XmlEnum("CertificateOfDepositIndex")]
        CertificateOfDepositIndex,

		/// <summary>
		/// Often referred to as CMT
		/// </summary>
        [XmlEnum("ConstantMaturityTreasury")]
        ConstantMaturityTreasury,

		/// <summary>
		/// Often referred to as COSI
		/// </summary>
        [XmlEnum("CostOfSavingsIndex")]
        CostOfSavingsIndex,

		/// <summary>
		/// Often referred to as COFI
		/// </summary>
        [XmlEnum("EleventhDistrictCostOfFundsIndex")]
        EleventhDistrictCostOfFundsIndex,

		/// <summary>
		/// London Interbank Offered Rate
		/// </summary>
        [XmlEnum("LIBOR")]
        LIBOR,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Often referred to as T-Bill
		/// </summary>
        [XmlEnum("TreasuryBill")]
        TreasuryBill,

		/// <summary>
		/// Often referred to as MTA
		/// </summary>
        [XmlEnum("TwelveMonthTreasuryAverage")]
        TwelveMonthTreasuryAverage,

    }
}
