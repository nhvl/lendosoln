namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum AntiSteeringComparisonBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("LowestInterestRate")]
        LowestInterestRate,

        [XmlEnum("LowestInterestRateWithoutCertainFeatures")]
        LowestInterestRateWithoutCertainFeatures,

        [XmlEnum("LowestTotalAmountForOriginationPointsOrFeesAndDiscountPoints")]
        LowestTotalAmountForOriginationPointsOrFeesAndDiscountPoints,

        [XmlEnum("NotApplicable")]
        NotApplicable,

        [XmlEnum("Other")]
        Other,
    }
}
