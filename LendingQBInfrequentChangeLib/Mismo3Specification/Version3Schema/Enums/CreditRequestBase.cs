namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A combined credit report on two married persons
    /// </summary>
    public enum CreditRequestBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Credit Report on a single individual
		/// </summary>
        [XmlEnum("Individual")]
        Individual,

		/// <summary>
		/// A combined credit report on two married persons
		/// </summary>
        [XmlEnum("Joint")]
        Joint,

    }
}
