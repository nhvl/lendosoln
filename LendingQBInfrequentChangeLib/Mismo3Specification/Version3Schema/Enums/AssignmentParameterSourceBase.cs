namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum AssignmentParameterSourceBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("AssignmentSpecific")]
        AssignmentSpecific,

        [XmlEnum("ClientSpecific")]
        ClientSpecific,

        [XmlEnum("MasterAgreement")]
        MasterAgreement,

        [XmlEnum("Other")]
        Other,
    }
}
