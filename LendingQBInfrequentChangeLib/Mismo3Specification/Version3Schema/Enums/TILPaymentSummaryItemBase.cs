namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// The maximum interest rate and payment summary information during the first five years after consummation and earliest date on which that rate may apply.
    /// </summary>
    public enum TILPaymentSummaryItemBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// The interest rate and payment summary information at the time the first payment increase is scheduled to occur.
		/// </summary>
        [XmlEnum("AtFirstAdjustment")]
        AtFirstAdjustment,

		/// <summary>
		/// The interest rate and payment summary information at the time the second payment increase is scheduled to occur.
		/// </summary>
        [XmlEnum("AtSecondAdjustment")]
        AtSecondAdjustment,

		/// <summary>
		/// The interest rate and payment summary information at consummation and for the period of time until the first interest rate adjustment may occur.
		/// </summary>
        [XmlEnum("Introductory")]
        Introductory,

		/// <summary>
		/// The maximum interest rate and payment summary information that may apply during the life of the loan and earliest date on which that rate may apply.
		/// </summary>
        [XmlEnum("MaximumEver")]
        MaximumEver,

		/// <summary>
		/// The maximum interest rate and payment summary information during the first five years after consummation and earliest date on which that rate may apply.
		/// </summary>
        [XmlEnum("MaximumFirstFiveYears")]
        MaximumFirstFiveYears,

        [XmlEnum("Other")]
        Other,

    }
}
