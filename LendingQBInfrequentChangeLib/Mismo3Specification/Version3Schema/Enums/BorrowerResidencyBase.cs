namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// The borrowers prior residence
    /// </summary>
    public enum BorrowerResidencyBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// The borrowers current residence
		/// </summary>
        [XmlEnum("Current")]
        Current,

		/// <summary>
		/// The borrowers prior residence
		/// </summary>
        [XmlEnum("Prior")]
        Prior,
    }
}
