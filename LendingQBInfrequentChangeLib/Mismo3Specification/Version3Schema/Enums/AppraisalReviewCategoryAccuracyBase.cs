namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum AppraisalReviewCategoryAccuracyBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Accurate")]
        Accurate,

        [XmlEnum("NotAccurate")]
        NotAccurate,

        [XmlEnum("NotApplicable")]
        NotApplicable,

        [XmlEnum("NotDeveloped")]
        NotDeveloped,
    }
}
