namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum MIPremiumSequenceBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Fifth")]
        Fifth,

        [XmlEnum("First")]
        First,

        [XmlEnum("Fourth")]
        Fourth,

        [XmlEnum("Second")]
        Second,

        [XmlEnum("Third")]
        Third,

    }
}
