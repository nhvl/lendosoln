namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Lender-placed insurance, also known as force-placed insurance; is an insurance policy placed by a bank or mortgage servicer on a home when the homeownersâ€™ own property insurance may have lapsed or where the bank deems the homeownersâ€™ insurance insufficient.
    /// </summary>
    public enum ExpenseClaimItemInsurancePlacedBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Homeowner has secured the insurance.
		/// </summary>
        [XmlEnum("Homeowner")]
        Homeowner,

		/// <summary>
		/// Lender-placed insurance, also known as force-placed insurance; is an insurance policy placed by a bank or mortgage servicer on a home when the homeownersâ€™ own property insurance may have lapsed or where the bank deems the homeownersâ€™ insurance insufficient.
		/// </summary>
        [XmlEnum("Lender")]
        Lender,

        [XmlEnum("Other")]
        Other,

    }
}
