namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum LicenseAuthorityLevelBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Private")]
        Private,

        [XmlEnum("PublicFederal")]
        PublicFederal,

        [XmlEnum("PublicLocal")]
        PublicLocal,

        [XmlEnum("PublicState")]
        PublicState,

    }
}
