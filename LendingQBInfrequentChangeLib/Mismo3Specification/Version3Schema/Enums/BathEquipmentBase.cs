namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum BathEquipmentBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("AutomaticVentSystem")]
        AutomaticVentSystem,

        [XmlEnum("DualSinks")]
        DualSinks,

        [XmlEnum("GardenTub")]
        GardenTub,

        [XmlEnum("HeatedFloors")]
        HeatedFloors,

        [XmlEnum("HeatLamps")]
        HeatLamps,

        [XmlEnum("LowFlowShowerheads")]
        LowFlowShowerheads,

        [XmlEnum("LowFlushToliet")]
        LowFlushToliet,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("RomanShower")]
        RomanShower,

        [XmlEnum("SingleSink")]
        SingleSink,

        [XmlEnum("SolidSurfaceManmadeCountertop")]
        SolidSurfaceManmadeCountertop,

        [XmlEnum("SolidSurfaceNaturalCountertop")]
        SolidSurfaceNaturalCountertop,

        [XmlEnum("StallShower")]
        StallShower,

        [XmlEnum("SteamShower")]
        SteamShower,

        [XmlEnum("TubShower")]
        TubShower,

        [XmlEnum("Vanity")]
        Vanity,

        [XmlEnum("WaterEffecientFawcet")]
        WaterEffecientFawcet,
    }
}
