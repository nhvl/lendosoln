namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum DisasterBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Chemical")]
        Chemical,

        [XmlEnum("DamFailure")]
        DamFailure,

        [XmlEnum("Earthquake")]
        Earthquake,

        [XmlEnum("Fire")]
        Fire,

        [XmlEnum("Flood")]
        Flood,

        [XmlEnum("HazardousMaterial")]
        HazardousMaterial,

        [XmlEnum("Heat")]
        Heat,

        [XmlEnum("Hurricane")]
        Hurricane,

        [XmlEnum("Landslide")]
        Landslide,

        [XmlEnum("Nuclear")]
        Nuclear,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Terrorism")]
        Terrorism,

        [XmlEnum("Thunderstorm")]
        Thunderstorm,

        [XmlEnum("Tornado")]
        Tornado,

        [XmlEnum("Tsunami")]
        Tsunami,

        [XmlEnum("Volcano")]
        Volcano,

        [XmlEnum("Wildfire")]
        Wildfire,

        [XmlEnum("WinterStorm")]
        WinterStorm,

    }
}
