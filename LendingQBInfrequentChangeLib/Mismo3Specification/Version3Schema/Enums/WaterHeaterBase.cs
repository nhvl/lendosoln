namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A system that generates hot directly without the use of a storage tank.
    /// </summary>
    public enum WaterHeaterBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Identifies that a system utilizing energy derived from the sun which requires special equipment including the tank utilized.
		/// </summary>
        [XmlEnum("Solar")]
        Solar,

		/// <summary>
		/// A system that offers a ready reservior of hot water.
		/// </summary>
        [XmlEnum("StandardTank")]
        StandardTank,

		/// <summary>
		/// A system that generates hot directly without the use of a storage tank.
		/// </summary>
        [XmlEnum("Tankless")]
        Tankless,

    }
}
