namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum IntegratedDisclosureDocumentBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("ClosingDisclosure")]
        ClosingDisclosure,

        [XmlEnum("LoanEstimate")]
        LoanEstimate,

        [XmlEnum("Other")]
        Other,

    }
}
