namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum StopCodeActionBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("AcceptCertifiedFundsOnly")]
        AcceptCertifiedFundsOnly,

        [XmlEnum("DoNotApplyNormalPayments")]
        DoNotApplyNormalPayments,

        [XmlEnum("DoNotAssessLateCharge")]
        DoNotAssessLateCharge,

        [XmlEnum("DoNotDisburseFromEscrow")]
        DoNotDisburseFromEscrow,

        [XmlEnum("DoNotPerformEscrowAnalysis")]
        DoNotPerformEscrowAnalysis,

        [XmlEnum("DoNotSendDelinquentNotices")]
        DoNotSendDelinquentNotices,

        [XmlEnum("DoNotSolicitForOptionalProducts")]
        DoNotSolicitForOptionalProducts,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("SpecialHandling")]
        SpecialHandling,

    }
}
