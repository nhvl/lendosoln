namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A house, individual condominium unit, mobile home permanently affixed to real estate, or other dwelling unit intended principally for the occupancy of one to four families.
    /// </summary>
    public enum NAICTitlePolicyClassificationBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Multifamily structures intended for the use of 5 or more families, undeveloped lots, or real estate intended principally for business, commercial, industrial, religious, educational or agricultural purposes even if some portion of the real estate is used for residential purposes.
		/// </summary>
        [XmlEnum("NonResidential")]
        NonResidential,

		/// <summary>
		/// A house, individual condominium unit, mobile home permanently affixed to real estate, or other dwelling unit intended principally for the occupancy of one to four families.
		/// </summary>
        [XmlEnum("Residential")]
        Residential,

    }
}
