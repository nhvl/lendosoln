namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Used to verify accounts and balance amounts at depository institutions.
    /// </summary>
    public enum AssetDocumentBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Evidences transactions recorded and account balances for a depository account of an individual or business during a specified period.
		/// </summary>
        [XmlEnum("BankStatement")]
        BankStatement,

		/// <summary>
		/// Evidences financial position of an individual or business for a specified period.
		/// </summary>
        [XmlEnum("FinancialStatement")]
        FinancialStatement,

		/// <summary>
		/// Evidences transactions recorded and account balances for an unrestricted individual or business investment account during a specified period.
		/// </summary>
        [XmlEnum("InvestmentAccountStatement")]
        InvestmentAccountStatement,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		///  Evidences the payment in exchange for a good or service.
		/// </summary>
        [XmlEnum("Receipt")]
        Receipt,

		/// <summary>
		/// Evidences the terms between a relocation company/employer and an employee to purchase a home based on an agreed upon value, or takes responsibility for outstanding mortgages.
		/// </summary>
        [XmlEnum("RelocationBuyoutAgreement")]
        RelocationBuyoutAgreement,

		/// <summary>
		/// Evidences transactions recorded and account balances for a restricted individual investment account during a specified period.
		/// </summary>
        [XmlEnum("RetirementAccountStatement")]
        RetirementAccountStatement,

		/// <summary>
		/// Evidences the funds exchanged in a real property sale transaction. HUD-1 is one example.
		/// </summary>
        [XmlEnum("SettlementStatement")]
        SettlementStatement,

		/// <summary>
		/// When verbal verification is allowed. This may be a written statement by the lender that information was collected verbally. Might include the name of the person making the contact, the name of the entity contacted, the name and title of the individual at the entity who provided the information, date of the contact and information that was collected.
		/// </summary>
        [XmlEnum("VerbalStatement")]
        VerbalStatement,

		/// <summary>
		/// Used to verify accounts and balance amounts at depository institutions.
		/// </summary>
        [XmlEnum("VerificationOfDeposit")]
        VerificationOfDeposit,
    }
}
