namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum QualifyingRateBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("AboveNoteRate")]
        AboveNoteRate,

        [XmlEnum("AtNoteRate")]
        AtNoteRate,

        [XmlEnum("BelowNoteRate")]
        BelowNoteRate,

        [XmlEnum("BoughtDownRate")]
        BoughtDownRate,

        [XmlEnum("Other")]
        Other,

    }
}
