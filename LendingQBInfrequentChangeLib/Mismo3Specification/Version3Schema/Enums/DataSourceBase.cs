namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// The legally binding agreement between the buyer and seller also known as Sales Contract.
    /// </summary>
    public enum DataSourceBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("AssessmentAndTaxRecords")]
        AssessmentAndTaxRecords,

        [XmlEnum("CooperativeBoard")]
        CooperativeBoard,

        [XmlEnum("Developer")]
        Developer,

		/// <summary>
		/// The assignment-level contract to an appraiser from a client providing details of the assignment and expectations. 
		/// </summary>
        [XmlEnum("EngagementLetter")]
        EngagementLetter,

        [XmlEnum("ExteriorInspectionOnly")]
        ExteriorInspectionOnly,

        [XmlEnum("HomeownersAssociation")]
        HomeownersAssociation,

        [XmlEnum("InteriorExteriorInspection")]
        InteriorExteriorInspection,

		/// <summary>
		/// Instructions from a published or provided guideline of a secondary market investor.
		/// </summary>
        [XmlEnum("InvestorGuidance")]
        InvestorGuidance,

		/// <summary>
		/// The collective information on a specific mortgage loan that includes both documents and data.  
		/// </summary>
        [XmlEnum("LoanFile")]
        LoanFile,

        [XmlEnum("ManagementAgency")]
        ManagementAgency,

        [XmlEnum("MultipleListingService")]
        MultipleListingService,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("PreviousAppraisalFile")]
        PreviousAppraisalFile,

        [XmlEnum("PriorInspection")]
        PriorInspection,

        [XmlEnum("PropertyOwner")]
        PropertyOwner,

		/// <summary>
		/// The legally binding agreement between the buyer and seller also known as Sales Contract.
		/// </summary>
        [XmlEnum("PurchaseAgreement")]
        PurchaseAgreement,

        [XmlEnum("ThirdPartyAppraisalReport")]
        ThirdPartyAppraisalReport,

    }
}
