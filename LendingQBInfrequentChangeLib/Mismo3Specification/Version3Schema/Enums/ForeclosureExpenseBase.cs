namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Legal fees other than attorney fees.
    /// </summary>
    public enum ForeclosureExpenseBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("AttorneyFees")]
        AttorneyFees,

        [XmlEnum("Eviction")]
        Eviction,

		/// <summary>
		/// Legal fees other than attorney fees.
		/// </summary>
        [XmlEnum("LegalFees")]
        LegalFees,

        [XmlEnum("Other")]
        Other,

    }
}
