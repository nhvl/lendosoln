namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum PrepaymentPenaltyPeriodBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Biweekly")]
        Biweekly,

        [XmlEnum("Day")]
        Day,

        [XmlEnum("Month")]
        Month,

        [XmlEnum("Quarter")]
        Quarter,

        [XmlEnum("Semimonthly")]
        Semimonthly,

        [XmlEnum("Week")]
        Week,

        [XmlEnum("Year")]
        Year,

    }
}
