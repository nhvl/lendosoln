namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A snapshot of the loan data as of the "Loan State Date".
    /// </summary>
    public enum LoanStateBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// A snapshot of the loan data at the time a borrower files for bankruptcy.
		/// </summary>
        [XmlEnum("AtBankruptcyFiling")]
        AtBankruptcyFiling,

		/// <summary>
		/// A snapshot of the loan data at the completion of the closing process.  This is sometimes referred to as "original".
		/// </summary>
        [XmlEnum("AtClosing")]
        AtClosing,

		/// <summary>
		/// For loans with a conversion option, a snapshot of the loan data at the time the conversion features become effective (e.g., biweekly to monthly payments; adjustable to fixed rate amortization).
		/// </summary>
        [XmlEnum("AtConversion")]
        AtConversion,

		/// <summary>
		/// A snapshot of the loan data at the point in time when a loan estimate is disclosed.
		/// </summary>
        [XmlEnum("AtEstimate")]
        AtEstimate,

		/// <summary>
		/// For loans which undergo term modifications not originally specified in the note, a snapshot of the loan data at the time the new note terms become effective.
		/// </summary>
        [XmlEnum("AtModification")]
        AtModification,

		/// <summary>
		/// For loans subject to payment relief, a snapshot of the loan data at the time the relief is initiated. 
		/// </summary>
        [XmlEnum("AtRelief")]
        AtRelief,

		/// <summary>
		/// For balloon mortgages with a reset feature, a snapshot of the loan data on the balloon maturity date at the time the borrower exercises the reset option to modify and extend the balloon note.
		/// </summary>
        [XmlEnum("AtReset")]
        AtReset,

		/// <summary>
		/// A snapshot of the loan data as of the effective date of the servicing transfer. 
		/// </summary>
        [XmlEnum("AtTransfer")]
        AtTransfer,

		/// <summary>
		/// A snapshot of the loan data at the initiation of a trial period for a workout modification.
		/// </summary>
        [XmlEnum("AtTrial")]
        AtTrial,

		/// <summary>
		/// A snapshot of the loan data at the point at which the underwriting recommendation is made.
		/// </summary>
        [XmlEnum("AtUnderwriting")]
        AtUnderwriting,

		/// <summary>
		/// A snapshot of the loan data as of the "Loan State Date".
		/// </summary>
        [XmlEnum("Current")]
        Current,

    }
}
