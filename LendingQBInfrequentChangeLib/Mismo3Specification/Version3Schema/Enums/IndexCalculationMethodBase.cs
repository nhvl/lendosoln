namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Use the index value rate in the ARM Index History.
    /// </summary>
    public enum IndexCalculationMethodBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Use the average value of two index rates.
		/// </summary>
        [XmlEnum("AverageTwoIndexRates")]
        AverageTwoIndexRates,

		/// <summary>
		/// Use the index value that is effective on the date the ARM Change is performed.
		/// </summary>
        [XmlEnum("CurrentDaysIndexRate")]
        CurrentDaysIndexRate,

		/// <summary>
		/// Use the higher value of two index rates.
		/// </summary>
        [XmlEnum("HighestOfTwoIndexRates")]
        HighestOfTwoIndexRates,

		/// <summary>
		/// Use the lower value of two index rates.
		/// </summary>
        [XmlEnum("LowestOfTwoIndexRates")]
        LowestOfTwoIndexRates,

		/// <summary>
		/// Use the index value rate in the ARM Index History.
		/// </summary>
        [XmlEnum("SingleIndexRate")]
        SingleIndexRate,

    }
}
