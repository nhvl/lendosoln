namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum BorrowerCharacteristicBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("LivingTrust")]
        LivingTrust,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("SeasonedWorker")]
        SeasonedWorker,

        [XmlEnum("SellerEmployee")]
        SellerEmployee,

        [XmlEnum("TrailingBorrower")]
        TrailingBorrower,
    }
}
