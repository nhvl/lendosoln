namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Reasonable travel expenses incurred and paid to retrieve the servicing records and related items for a group of loans as comply with the expense policy.
    /// </summary>
    public enum DealSetExpenseBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Reasonable actual out-of-pocket expenses to ship the servicing records for a group of loans.
		/// </summary>
        [XmlEnum("Shipping")]
        Shipping,

		/// <summary>
		/// A summary of special expenses include tax penalties, Borrower escrow payments, escrow discrepancies, tax penalties, missing borrower escrow payments, escrow discrepancies.
		/// </summary>
        [XmlEnum("Special")]
        Special,

		/// <summary>
		/// Reasonable travel expenses incurred and paid to retrieve the servicing records and related items for a group of loans as comply with the expense policy.
		/// </summary>
        [XmlEnum("Travel")]
        Travel,

    }
}
