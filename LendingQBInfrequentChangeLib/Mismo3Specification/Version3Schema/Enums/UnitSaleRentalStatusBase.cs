namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum UnitSaleRentalStatusBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("ForRent")]
        ForRent,

        [XmlEnum("ForSale")]
        ForSale,

        [XmlEnum("Rented")]
        Rented,

        [XmlEnum("Sold")]
        Sold,

    }
}
