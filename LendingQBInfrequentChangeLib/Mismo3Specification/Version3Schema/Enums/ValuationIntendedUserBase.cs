namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum ValuationIntendedUserBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("DueDiligenceCompany")]
        DueDiligenceCompany,

        [XmlEnum("Investor")]
        Investor,

        [XmlEnum("Lender")]
        Lender,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Regulator")]
        Regulator,

    }
}
