namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Fee is being paid to a third party provider.
    /// </summary>
    public enum FeePaidToBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Fee is being paid to a third party provider who is an affiliate of the Lender or Broker.
		/// </summary>
        [XmlEnum("AffiliateProvider")]
        AffiliateProvider,

        [XmlEnum("Broker")]
        Broker,

        [XmlEnum("Investor")]
        Investor,

        [XmlEnum("Lender")]
        Lender,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Fee is being paid to a third party provider.
		/// </summary>
        [XmlEnum("ThirdPartyProvider")]
        ThirdPartyProvider,

    }
}
