namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// The subject loan will be used for construction financing and then be converted to permanent financing.
    /// </summary>
    public enum ConstructionLoanBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// A short term, interim loan for financing the cost of construction. The lender advances funds to the builder at periodic intervals as work progresses.
		/// </summary>
        [XmlEnum("ConstructionOnly")]
        ConstructionOnly,

		/// <summary>
		/// The subject loan will be used for construction financing and then be converted to permanent financing.
		/// </summary>
        [XmlEnum("ConstructionToPermanent")]
        ConstructionToPermanent,

    }
}
