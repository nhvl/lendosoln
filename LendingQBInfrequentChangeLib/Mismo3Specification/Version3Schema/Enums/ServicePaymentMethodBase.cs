namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A wire transfer transaction
    /// </summary>
    public enum ServicePaymentMethodBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// An Automated Clearing House transaction
		/// </summary>
        [XmlEnum("ACH")]
        ACH,

		/// <summary>
		/// A credit card is to be charged for the transaction
		/// </summary>
        [XmlEnum("CreditCard")]
        CreditCard,

		/// <summary>
		/// A debit card is to be debited for the transaction
		/// </summary>
        [XmlEnum("DebitCard")]
        DebitCard,

		/// <summary>
		/// A prepaid account is to be charged for the transaction
		/// </summary>
        [XmlEnum("OnAccount")]
        OnAccount,

		/// <summary>
		/// A form of payment not included in the enumerated list is to be used for the transaction
		/// </summary>
        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// A payment voucher is to be used for the transaction
		/// </summary>
        [XmlEnum("Voucher")]
        Voucher,

		/// <summary>
		/// A wire transfer transaction
		/// </summary>
        [XmlEnum("Wire")]
        Wire,

    }
}
