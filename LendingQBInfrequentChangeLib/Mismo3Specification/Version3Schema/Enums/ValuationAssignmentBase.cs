namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum ValuationAssignmentBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("EmployeeRelocation")]
        EmployeeRelocation,

        [XmlEnum("Foreclosure")]
        Foreclosure,

        [XmlEnum("JuniorLien")]
        JuniorLien,

        [XmlEnum("LossMitigation")]
        LossMitigation,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("PortfolioEvaluation")]
        PortfolioEvaluation,

        [XmlEnum("Purchase")]
        Purchase,

        [XmlEnum("QualityControl")]
        QualityControl,

        [XmlEnum("Refinance")]
        Refinance,

        [XmlEnum("REO")]
        REO,

        [XmlEnum("Review")]
        Review,

        [XmlEnum("ShortSale")]
        ShortSale,

        [XmlEnum("Validation")]
        Validation,

    }
}
