namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Pool maturity period is measured in months.
    /// </summary>
    public enum PoolMaturityPeriodBase
    {
        /// <summary>
        /// A catchall value to account for a blank field in LQB.
        /// </summary>
        [XmlEnum("")]
        Blank,

        /// <summary>
        /// Pool maturity period is measured in months.
        /// </summary>
        [XmlEnum("Month")]
        Month,
    }
}
