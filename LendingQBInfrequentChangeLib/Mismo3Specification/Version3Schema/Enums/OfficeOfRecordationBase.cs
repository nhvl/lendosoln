namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum OfficeOfRecordationBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("ClerkOfCourts")]
        ClerkOfCourts,

        [XmlEnum("CountyClerk")]
        CountyClerk,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("RegisterOfDeeds")]
        RegisterOfDeeds,

    }
}
