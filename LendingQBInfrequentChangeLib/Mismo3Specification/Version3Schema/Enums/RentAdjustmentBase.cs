namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum RentAdjustmentBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Age")]
        Age,

        [XmlEnum("Appeal")]
        Appeal,

        [XmlEnum("Condition")]
        Condition,

        [XmlEnum("Design")]
        Design,

        [XmlEnum("GrossLivingArea")]
        GrossLivingArea,

        [XmlEnum("Location")]
        Location,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("OtherConcessions")]
        OtherConcessions,

        [XmlEnum("RentConcessions")]
        RentConcessions,

        [XmlEnum("View")]
        View,

    }
}
