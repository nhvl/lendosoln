namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// The billing instruction is not one of the enumerated values and is listed in the End User Billing Instruction Type Other Description value.
    /// </summary>
    public enum EndUserBillingInstructionBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// The party identified as the Additional End User will be billed for all costs related to the Additional End User reporting.
		/// </summary>
        [XmlEnum("BillAdditionalEndUser")]
        BillAdditionalEndUser,

		/// <summary>
		/// The party identified as the Additional End User will be billed only for any Reissue Recording Fees that may be charged by the credit repository bureaus.  Other fees related to the Additional End User reporting, if any, will be billed to the party that requested the credit report.
		/// </summary>
        [XmlEnum("BillReissueRecordingFeesToAdditionalEndUser")]
        BillReissueRecordingFeesToAdditionalEndUser,

		/// <summary>
		/// The party identified as the Requesting Party on the original credit request will be billed only for any Reissue Recording Fees that may be charged by the credit repository bureaus. Other fees related to the Additional End User reporting, if any, will be billed to the party that requested the reissued credit report.
		/// </summary>
        [XmlEnum("BillReissueRecordingFeesToOriginalRequestingParty")]
        BillReissueRecordingFeesToOriginalRequestingParty,

		/// <summary>
		/// The party that requested the credit report will be billed for all costs related to the Additional End User reporting.  This is the default value assumed if this attribute is not present.
		/// </summary>
        [XmlEnum("BillRequestingParty")]
        BillRequestingParty,

		/// <summary>
		/// The billing instruction is not one of the enumerated values and is listed in the End User Billing Instruction Type Other Description value.
		/// </summary>
        [XmlEnum("Other")]
        Other,

    }
}
