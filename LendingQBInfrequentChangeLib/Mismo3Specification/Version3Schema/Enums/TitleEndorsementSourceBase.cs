namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum TitleEndorsementSourceBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("ALTA")]
        ALTA,

        [XmlEnum("CLTA")]
        CLTA,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("TLTA")]
        TLTA,

    }
}
