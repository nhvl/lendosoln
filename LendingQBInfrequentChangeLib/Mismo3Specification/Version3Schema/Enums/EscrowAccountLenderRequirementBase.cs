namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates the escrow account requirement has been waived by the lender.
    /// </summary>
    public enum EscrowAccountLenderRequirementBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Indicates an escrow account is required by the lender.
		/// </summary>
        [XmlEnum("EscrowsRequiredByLender")]
        EscrowsRequiredByLender,

		/// <summary>
		/// Indicates the escrow account requirement has been waived by the lender.
		/// </summary>
        [XmlEnum("EscrowsWaivedByLender")]
        EscrowsWaivedByLender,

    }
}
