namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum ProjectAnalysisCompetitiveProjectComparisonBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Average")]
        Average,

        [XmlEnum("High")]
        High,

        [XmlEnum("Low")]
        Low,

    }
}
