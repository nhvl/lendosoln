namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Statement that, if the extension of credit exceeds the fair market value of the property, the interest on the portion of the credit extension that is greater than the fair market value of the property is not tax deductible for Federal income tax purposes and a statement that the consumer should consult a tax adviser for further information.
    /// </summary>
    public enum LoanConsiderationDisclosureStatementBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Statement of information pertaining to borrower's right to a copy of the written property appraisal or valuation.
		/// </summary>
        [XmlEnum("Appraisal")]
        Appraisal,

		/// <summary>
		/// Statement of whether a subsequent purchaser of the property may be permitted to assume the remaining loan obligation on its original terms.
		/// </summary>
        [XmlEnum("Assumption")]
        Assumption,

		/// <summary>
		/// Statement referring the consumer to the loan contract for additional information about loan terms.
		/// </summary>
        [XmlEnum("ContractDetails")]
        ContractDetails,

		/// <summary>
		/// Statement relating to whether the legal obligation of the loan permits the creditor to demand early repayment of the loan.
		/// </summary>
        [XmlEnum("DemandFeature")]
        DemandFeature,

		/// <summary>
		/// Statement on whether the loan will have an escrow account, and details about the payments made using escrow account funds and those the consumer must make directly.
		/// </summary>
        [XmlEnum("EscrowAccountCurrent")]
        EscrowAccountCurrent,

		/// <summary>
		/// Statement providing information about future requirements for property costs and the escrow account.
		/// </summary>
        [XmlEnum("EscrowAccountFuture")]
        EscrowAccountFuture,

		/// <summary>
		/// Statement of whether homeowners insurance is required as a condition of the loan transaction.
		/// </summary>
        [XmlEnum("HomeownersInsurance")]
        HomeownersInsurance,

		/// <summary>
		/// Statement detailing any dollar or percentage charge that may be imposed before maturity due to a late payment.
		/// </summary>
        [XmlEnum("LatePayment")]
        LatePayment,

		/// <summary>
		/// Statement related to the liability of the borrower for any unpaid loan balance after foreclosure.
		/// </summary>
        [XmlEnum("LiabilityAfterForeclosure")]
        LiabilityAfterForeclosure,

		/// <summary>
		/// Statement clarifying that the consumer does not have to accept the loan because the consumer has received the integrated disclosure document or signed a loan application.
		/// </summary>
        [XmlEnum("LoanAcceptance")]
        LoanAcceptance,

		/// <summary>
		/// Statement notifying the consumer that the transaction may or will result in negative amortization.
		/// </summary>
        [XmlEnum("NegativeAmortization")]
        NegativeAmortization,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Statement on the creditor's policy regarding the acceptance of partial payments, and if partial payments are accepted, how such payments will be applied to the mortgage and whether such payments will be placed in escrow.
		/// </summary>
        [XmlEnum("PartialPayment")]
        PartialPayment,

		/// <summary>
		/// Statement of the factors in the borrower's ability to refinance the loan.
		/// </summary>
        [XmlEnum("Refinance")]
        Refinance,

		/// <summary>
		/// Statement that a security interest has been taken in the property that secures the transaction or in property not purchased as part of  the transaction by item or type.
		/// </summary>
        [XmlEnum("SecurityInterest")]
        SecurityInterest,

		/// <summary>
		/// Statement of the creditor's intention to retain or transfer servicing of the loan.
		/// </summary>
        [XmlEnum("Servicing")]
        Servicing,

		/// <summary>
		/// Statement that, if the extension of credit exceeds the fair market value of the property, the interest on the portion of the credit extension that is greater than the fair market value of the property is not tax deductible for Federal income tax purposes and a statement that the consumer should consult a tax adviser for further information.
		/// </summary>
        [XmlEnum("TaxDeductions")]
        TaxDeductions,

    }
}
