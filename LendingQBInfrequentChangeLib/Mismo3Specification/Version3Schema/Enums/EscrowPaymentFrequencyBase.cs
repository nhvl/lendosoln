namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum EscrowPaymentFrequencyBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Annual")]
        Annual,

        [XmlEnum("Monthly")]
        Monthly,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Quarterly")]
        Quarterly,

        [XmlEnum("SemiAnnual")]
        SemiAnnual,

        [XmlEnum("Unequal")]
        Unequal,

    }
}
