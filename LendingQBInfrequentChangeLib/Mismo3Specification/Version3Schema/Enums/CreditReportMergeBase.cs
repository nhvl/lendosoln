namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A record is picked from a list of duplicates.
    /// </summary>
    public enum CreditReportMergeBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// A record is formed by blending data from a group of duplicates.
		/// </summary>
        [XmlEnum("Blend")]
        Blend,

		/// <summary>
		/// A Merged data record is followed by the individual duplicate Repository records.
		/// </summary>
        [XmlEnum("ListAndStack")]
        ListAndStack,

		/// <summary>
		/// A record is picked from a list of duplicates.
		/// </summary>
        [XmlEnum("PickAndChoose")]
        PickAndChoose,

    }
}
