namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum CompensationBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Rebate")]
        Rebate,

        [XmlEnum("ServiceReleasePremium")]
        ServiceReleasePremium,

        [XmlEnum("YieldSpreadDifferential")]
        YieldSpreadDifferential,

    }
}
