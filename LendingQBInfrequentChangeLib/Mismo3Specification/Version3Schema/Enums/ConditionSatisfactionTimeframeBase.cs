namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum ConditionSatisfactionTimeframeBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("PriorToApproval")]
        PriorToApproval,

        [XmlEnum("PriorToDocuments")]
        PriorToDocuments,

        [XmlEnum("PriorToFunding")]
        PriorToFunding,

        [XmlEnum("PriorToSigning")]
        PriorToSigning,

        [XmlEnum("UnderwriterToReview")]
        UnderwriterToReview,

    }
}
