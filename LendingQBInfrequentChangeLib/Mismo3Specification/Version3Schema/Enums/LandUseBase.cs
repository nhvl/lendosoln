namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum LandUseBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Agricultural")]
        Agricultural,

        [XmlEnum("Commercial")]
        Commercial,

        [XmlEnum("Income")]
        Income,

        [XmlEnum("Industrial")]
        Industrial,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("PublicAndSemipublic")]
        PublicAndSemipublic,

        [XmlEnum("Recreational")]
        Recreational,

        [XmlEnum("Residential")]
        Residential,

        [XmlEnum("TransportationAndUtility")]
        TransportationAndUtility,

        [XmlEnum("Vacant")]
        Vacant,

    }
}
