namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Application was taken over the telephone.
    /// </summary>
    public enum ApplicationTakenMethodBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Application was taken in a face to face interview.
		/// </summary>
        [XmlEnum("FaceToFace")]
        FaceToFace,

        [XmlEnum("Fax")]
        Fax,

		/// <summary>
		/// Application taken via the internet.
		/// </summary>
        [XmlEnum("Internet")]
        Internet,

		/// <summary>
		/// Application was taken by mail
		/// </summary>
        [XmlEnum("Mail")]
        Mail,

		/// <summary>
		/// Application was taken over the telephone.
		/// </summary>
        [XmlEnum("Telephone")]
        Telephone,
    }
}
