namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum InterestCalculationBasisDaysInPeriodBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("30Days")]
        Item30Days,

        [XmlEnum("DaysBetweenPayments")]
        DaysBetweenPayments,

        [XmlEnum("DaysInCalendarMonth")]
        DaysInCalendarMonth,

        [XmlEnum("Other")]
        Other,

    }
}
