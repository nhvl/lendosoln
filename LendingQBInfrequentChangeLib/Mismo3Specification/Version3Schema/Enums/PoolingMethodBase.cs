namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Payments are remitted to investors 45 days after due date.
    /// </summary>
    public enum PoolingMethodBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Payments are remitted to investors 15 days after the loan scheduled due date.
		/// </summary>
        [XmlEnum("ConcurrentDate")]
        ConcurrentDate,

		/// <summary>
		/// Payments are remitted to investors 45 days after due date.
		/// </summary>
        [XmlEnum("InternalReserve")]
        InternalReserve,

        [XmlEnum("Other")]
        Other,

    }
}
