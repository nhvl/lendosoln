namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// No closing agent was found matching search criteria provided.
    /// </summary>
    public enum TitleAgentValidationBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// The closing agent is active and authorized to perform the transaction.
		/// </summary>
        [XmlEnum("Allowed")]
        Allowed,

		/// <summary>
		/// The closing agent is not authorized to perform the transaction.
		/// </summary>
        [XmlEnum("NotAllowed")]
        NotAllowed,

		/// <summary>
		/// No closing agent was found matching search criteria provided.
		/// </summary>
        [XmlEnum("NotFound")]
        NotFound,

        [XmlEnum("Other")]
        Other,

    }
}
