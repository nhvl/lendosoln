namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum LoanOriginationChannelBase
    {
        /// <summary>
        /// A catchall value to account for a blank field in LQB.
        /// </summary>
        [XmlEnum("")]
        Blank,

        [XmlEnum("BrokeredOut")]
        BrokeredOut,

        [XmlEnum("Correspondent")]
        Correspondent,

        [XmlEnum("Internet")]
        Internet,

        [XmlEnum("Purchased")]
        Purchased,

        [XmlEnum("Retail")]
        Retail,

        [XmlEnum("Servicing")]
        Servicing,

        [XmlEnum("Wholesale")]
        Wholesale

    }
}
