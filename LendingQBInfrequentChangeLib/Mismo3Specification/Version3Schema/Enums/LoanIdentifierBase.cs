namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A tracking number assigned by the wholesale lender to identify the loan for pipeline management.
    /// </summary>
    public enum LoanIdentifierBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// An identifier assigned by a government agency (for example FHA Case Number, VA Case Number). The identifier is used by the agency to identify a loan.
		/// </summary>
        [XmlEnum("AgencyCase")]
        AgencyCase,

		/// <summary>
		/// The unique identifier of the commitment that states the terms under which a loan seller and an investor agree to exchange loans for funds, securities, or other assets.
		/// </summary>
        [XmlEnum("InvestorCommitment")]
        InvestorCommitment,

		/// <summary>
		/// A unique identifier for a group of loans identified as part of a cash pool or a security pool.
		/// </summary>
        [XmlEnum("InvestorContract")]
        InvestorContract,

		/// <summary>
		/// Account number assigned by the investor used for tracking on the investors systems.
		/// </summary>
        [XmlEnum("InvestorLoan")]
        InvestorLoan,

		/// <summary>
		/// A unique identifier of a workout assigned by the investor associated with a loan that has been evaluated for a workout.
		/// </summary>
        [XmlEnum("InvestorWorkoutCase")]
        InvestorWorkoutCase,

		/// <summary>
		/// Number used by the Lender to identify a loan. 
		/// </summary>
        [XmlEnum("LenderCase")]
        LenderCase,

		/// <summary>
		/// The identifier assigned by the originating Lender to be referenced as the Loan ID/Number on all settlement documents, notes, riders, etc.
		/// </summary>
        [XmlEnum("LenderLoan")]
        LenderLoan,

		/// <summary>
		/// A unique identifier for a loan price quote, assigned by the party that makes the price quote for tracking purposes.
		/// </summary>
        [XmlEnum("LoanPriceQuote")]
        LoanPriceQuote,

		/// <summary>
		/// Number used by MERS to identify loans. Referred to as the MIN, Mortgage Identification Number.
		/// </summary>
        [XmlEnum("MERS_MIN")]
        MERS_MIN,

		/// <summary>
		/// A unique identifier for a MI rate quote, assigned by the party that makes the rate quote, used to track or refer to the rate quote.
		/// </summary>
        [XmlEnum("MIRateQuote")]
        MIRateQuote,

		/// <summary>
		/// Account number assigned by the servicer used for tracking on the servicer systems. For servicing transfer purposes the servicer would be the Transferee.
		/// </summary>
        [XmlEnum("NewServicerLoan")]
        NewServicerLoan,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// A unique identifier assigned by the Pool Issuer
		/// </summary>
        [XmlEnum("PoolIssuerLoan")]
        PoolIssuerLoan,

		/// <summary>
		/// A unique identifier for a price response, assigned by the responder, used to track or refer to the price response.
		/// </summary>
        [XmlEnum("PriceResponse")]
        PriceResponse,

		/// <summary>
		/// A unique identifier assigned by the seller to the loan.
		/// </summary>
        [XmlEnum("SellerLoan")]
        SellerLoan,

		/// <summary>
		/// A unique identifier assigned by the servicer to identify the loan. For servicing transfer purposes the servicer would be the Transferor.
		/// </summary>
        [XmlEnum("ServicerLoan")]
        ServicerLoan,

		/// <summary>
		/// A unique identifier of a workout assigned by the servicer associated with a loan that has been evaluated for a workout.
		/// </summary>
        [XmlEnum("ServicerWorkoutCase")]
        ServicerWorkoutCase,

        [XmlEnum("SubservicerLoan")]
        SubservicerLoan,

		/// <summary>
		/// A tracking number assigned by the wholesale lender to identify the loan for pipeline management.
		/// </summary>
        [XmlEnum("WholesaleLenderLoan")]
        WholesaleLenderLoan,

    }
}
