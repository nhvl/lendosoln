namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates that the signing party provided document based identification to the notary
    /// </summary>
    public enum NotaryCertificateSignerIdentificationBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Indicates that the signing party was personally known by the notary
		/// </summary>
        [XmlEnum("PersonallyKnown")]
        PersonallyKnown,

		/// <summary>
		/// Indicates that the signing party provided document based identification to the notary
		/// </summary>
        [XmlEnum("ProvidedIdentification")]
        ProvidedIdentification,

    }
}
