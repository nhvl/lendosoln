namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum MilitaryBranchBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("AirForce")]
        AirForce,

        [XmlEnum("AirNationalGuard")]
        AirNationalGuard,

        [XmlEnum("Army")]
        Army,

        [XmlEnum("ArmyNationalGuard")]
        ArmyNationalGuard,

        [XmlEnum("ArmyReserves")]
        ArmyReserves,

        [XmlEnum("CoastGuard")]
        CoastGuard,

        [XmlEnum("Marines")]
        Marines,

        [XmlEnum("MarinesReserves")]
        MarinesReserves,

        [XmlEnum("Navy")]
        Navy,

        [XmlEnum("NavyReserves")]
        NavyReserves,

        [XmlEnum("Other")]
        Other,

    }
}
