namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Specify in Reason for Cancellation Other Description element
    /// </summary>
    public enum ReasonForMIPolicyCancellationBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("ClaimDenial")]
        ClaimDenial,

        [XmlEnum("ClaimPaid")]
        ClaimPaid,

		/// <summary>
		/// The LTV of the loan has dropped below the required 80% and no longer requires MI coverage.
		/// </summary>
        [XmlEnum("CoverageNoLongerRequired")]
        CoverageNoLongerRequired,

		/// <summary>
		/// The Coverage is being cancelled due to the loan being paid off and no longer requires MI coverage.
		/// </summary>
        [XmlEnum("LoanPaidInFull")]
        LoanPaidInFull,

		/// <summary>
		/// The loan is being refinanced and no longer requires MI coverage of this specific policy.
		/// </summary>
        [XmlEnum("LoanRefinanced")]
        LoanRefinanced,

        [XmlEnum("NonPaymentOfPremium")]
        NonPaymentOfPremium,

		/// <summary>
		/// Specify in Reason for Cancellation Other Description element
		/// </summary>
        [XmlEnum("Other")]
        Other,

        [XmlEnum("Rescinded")]
        Rescinded,

    }
}
