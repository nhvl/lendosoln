namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum MilitaryStatusBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("ActiveDuty")]
        ActiveDuty,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("ReserveNationalGuardNotSubjectToActivation")]
        ReserveNationalGuardNotSubjectToActivation,

        [XmlEnum("Separated")]
        Separated,

    }
}
