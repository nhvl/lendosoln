namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum ForeclosureSaleRescindedReasonBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("ExcessivePropertyDamage")]
        ExcessivePropertyDamage,

        [XmlEnum("MutualAgreementWithBorrower")]
        MutualAgreementWithBorrower,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("ProceduralError")]
        ProceduralError,

        [XmlEnum("Redemption")]
        Redemption,

        [XmlEnum("Unknown")]
        Unknown,

    }
}
