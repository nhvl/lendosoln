namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum CounselingFormatBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("BorrowerEducationNotRequired")]
        BorrowerEducationNotRequired,

        [XmlEnum("Classroom")]
        Classroom,

        [XmlEnum("HomeStudy")]
        HomeStudy,

        [XmlEnum("Individual")]
        Individual,

        [XmlEnum("Other")]
        Other,

    }
}
