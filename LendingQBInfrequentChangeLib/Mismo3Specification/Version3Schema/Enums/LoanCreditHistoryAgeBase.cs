namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum LoanCreditHistoryAgeBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("LessThan180Days")]
        LessThan180Days,

        [XmlEnum("LessThan270Days")]
        LessThan270Days,

        [XmlEnum("LessThan90Days")]
        LessThan90Days,

        [XmlEnum("Other")]
        Other,

    }
}
