namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum LocationBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("AdjacentToPark")]
        AdjacentToPark,

        [XmlEnum("AdjacentToPowerLines")]
        AdjacentToPowerLines,

        [XmlEnum("BusyRoad")]
        BusyRoad,

        [XmlEnum("Commercial")]
        Commercial,

        [XmlEnum("GolfCourse")]
        GolfCourse,

        [XmlEnum("Industrial")]
        Industrial,

        [XmlEnum("Landfill")]
        Landfill,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("PublicTransportation")]
        PublicTransportation,

        [XmlEnum("Residential")]
        Residential,

        [XmlEnum("WaterFront")]
        WaterFront,

    }
}
