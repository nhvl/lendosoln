namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum PropertyPhotoBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("AreaOfConcern")]
        AreaOfConcern,

        [XmlEnum("CompletedRepair")]
        CompletedRepair,

        [XmlEnum("Exterior")]
        Exterior,

        [XmlEnum("Interior")]
        Interior,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("StreetView")]
        StreetView,

    }
}
