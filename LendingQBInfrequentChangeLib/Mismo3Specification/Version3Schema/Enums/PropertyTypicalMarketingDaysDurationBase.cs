namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum PropertyTypicalMarketingDaysDurationBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("30To120Days")]
        Item30To120Days,

        [XmlEnum("Over120Days")]
        Over120Days,

        [XmlEnum("Under30Days")]
        Under30Days,

    }
}
