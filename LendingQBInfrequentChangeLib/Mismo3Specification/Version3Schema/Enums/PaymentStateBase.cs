namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum PaymentStateBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Current")]
        Current,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Pending")]
        Pending,

        [XmlEnum("Previous")]
        Previous,

    }
}
