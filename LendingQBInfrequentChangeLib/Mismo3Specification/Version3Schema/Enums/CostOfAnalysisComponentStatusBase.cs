namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Cost refers to plans to improve or work in progress at time of analysis.
    /// </summary>
    public enum CostOfAnalysisComponentStatusBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Cost refers to items complete at time of analysis.
		/// </summary>
        [XmlEnum("Completed")]
        Completed,

		/// <summary>
		/// Cost refers to plans to improve or work in progress at time of analysis.
		/// </summary>
        [XmlEnum("Proposed")]
        Proposed,

    }
}
