namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// The MI company workout decision is delegated to the servicer.
    /// </summary>
    public enum MIWorkoutDelegationBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// The MI company workout decision is delegated to the investor.
		/// </summary>
        [XmlEnum("InvestorDelegated")]
        InvestorDelegated,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// The MI company workout decision is delegated to the servicer.
		/// </summary>
        [XmlEnum("ServicerDelegated")]
        ServicerDelegated,

    }
}
