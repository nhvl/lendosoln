namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Freddie Mac
    /// </summary>
    public enum CurrentFirstMortgageHolderBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Fannie Mae
		/// </summary>
        [XmlEnum("FNM")]
        FNM,

		/// <summary>
		/// Freddie Mac
		/// </summary>
        [XmlEnum("FRE")]
        FRE,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Unknown")]
        Unknown,

    }
}
