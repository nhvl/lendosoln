namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum EnvironmentalConditionBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Asbestos")]
        Asbestos,

        [XmlEnum("EPAFinalSuperfundSiteProximate")]
        EPAFinalSuperfundSiteProximate,

        [XmlEnum("FormerDrugLab")]
        FormerDrugLab,

        [XmlEnum("LandfillProximate")]
        LandfillProximate,

        [XmlEnum("LeadBasedPaint")]
        LeadBasedPaint,

        [XmlEnum("LeakingUndergroundPetroleumStorageTank")]
        LeakingUndergroundPetroleumStorageTank,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("VaporIntrusion")]
        VaporIntrusion,

        [XmlEnum("WellWaterContamination")]
        WellWaterContamination,

    }
}
