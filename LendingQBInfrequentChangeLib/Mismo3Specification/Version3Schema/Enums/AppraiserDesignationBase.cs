namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// SRA - Appraisal Insitute   URI urn:www.org:appraisalinstitute:SRA URL http://www.appraisalinstitute.org/
    /// </summary>
    public enum AppraiserDesignationBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// AAC- American Society of Farm Managers and Rural Appraisers   URI urn:www:org:asfmra:AAC  URL http://www.asfmra.org
		/// </summary>
        [XmlEnum("AccreditedAgriculturalConsultant")]
        AccreditedAgriculturalConsultant,

		/// <summary>
		/// AFM - American Society of Farm Managers and Rural Appraisers URI urn:www:org:asfmra:AFM URL http://www.asfmra.org
		/// </summary>
        [XmlEnum("AccreditedFarmManager")]
        AccreditedFarmManager,

		/// <summary>
		/// AM - American Society of Appraisers URI urn:www:org:appraisers:AM URL http://www.appraisers.org
		/// </summary>
        [XmlEnum("AccreditedMember")]
        AccreditedMember,

		/// <summary>
		/// ARA- American Society of Farm Managers and Rural Appraisers URI urn:www:org:asfmra:ARA URL http://www.asfmra.org
		/// </summary>
        [XmlEnum("AccreditedRuralAppraiser")]
        AccreditedRuralAppraiser,

		/// <summary>
		/// ASA - American Society of Appraisers URI urn:www:org:appraisers:ASA URL http://www.appraisers.org
		/// </summary>
        [XmlEnum("AccreditedSeniorAppraiser")]
        AccreditedSeniorAppraiser,

		/// <summary>
		/// ARICS - Royal Institution of Chartered Surveyors URI urn:wwwl:org:RICS:ARICS   URL http://www.rics.org
		/// </summary>
        [XmlEnum("AssociateOfTheRoyalInstitutionOfCharteredSurveyors")]
        AssociateOfTheRoyalInstitutionOfCharteredSurveyors,

		/// <summary>
		/// IFA - National Association of Independent Fee Appraisers URI urn:www:com:naifa:IFA  URL http://www.naifa.com
		/// </summary>
        [XmlEnum("DesignatedMember")]
        DesignatedMember,

		/// <summary>
		/// IFAA - National Association of Independent Fee Appraisers URI urn:www:com:naifa:IFAA URL http://www.naifa.com
		/// </summary>
        [XmlEnum("DesignatedMemberAgricultural")]
        DesignatedMemberAgricultural,

		/// <summary>
		/// IFAC- National Association of Independent Fee Appraisers  URI urn:www:com:naifa:IFAC URL http://www.naifa.com
		/// </summary>
        [XmlEnum("DesignatedMemberCounselor")]
        DesignatedMemberCounselor,

		/// <summary>
		/// IFAS - National Association of Independent Fee Appraisers URI urn:www:com:naifa:IFAS URL http://www.naifa.com
		/// </summary>
        [XmlEnum("DesignatedMemberSenior")]
        DesignatedMemberSenior,

		/// <summary>
		/// FRICS - Royal Institution of Chartered Surveyors URI urn:wwwl:org:RICS:FRICS  URL http://www.rics.org
		/// </summary>
        [XmlEnum("FellowOfTheRoyalInstitutionOfCharteredSurveyors")]
        FellowOfTheRoyalInstitutionOfCharteredSurveyors,

		/// <summary>
		/// GAA - National Assocation of Realtors URI urn:www:org:realtor:GAA URL http://www.realtor.org
		/// </summary>
        [XmlEnum("GeneralAccreditedAppraiser")]
        GeneralAccreditedAppraiser,

		/// <summary>
		/// MAI - Appraisal Insitute  URI urn:www.org:appraisalinstitute:MAI URL http://www.appraisalinstitute.org/ 
		/// </summary>
        [XmlEnum("MemberAppraisalInstitute")]
        MemberAppraisalInstitute,

		/// <summary>
		/// MRICS - Royal Institution of Chartered Surveyors  URI urn:wwwl:org:RICS:MRICS  URL http://www.rics.org
		/// </summary>
        [XmlEnum("MemberOfTheRoyalInstitutionOfCharteredSurveyors")]
        MemberOfTheRoyalInstitutionOfCharteredSurveyors,

		/// <summary>
		/// RPRP- American Society of Farm Managers and Rural Appraisers  URI urn:www:org:asfmra:RPRP URL http://www.asfmra.org
		/// </summary>
        [XmlEnum("RealPropertyReviewAppraiser")]
        RealPropertyReviewAppraiser,

		/// <summary>
		/// RAA - - National Association of Independent Fee Appraisers URI urn:www:org:realtor:RAA URL http://www.realtor.org
		/// </summary>
        [XmlEnum("ResidentialAccreditedAppraiser")]
        ResidentialAccreditedAppraiser,

		/// <summary>
		/// SRPA - Appraisal Institute  URI urn:www.org:appraisalinstitute:SRPA URL http://www.appraisalinstitute.org/ 
		/// </summary>
        [XmlEnum("SeniorRealPropertyAppraiser")]
        SeniorRealPropertyAppraiser,

		/// <summary>
		/// SRA - Appraisal Insitute   URI urn:www.org:appraisalinstitute:SRA URL http://www.appraisalinstitute.org/ 
		/// </summary>
        [XmlEnum("SeniorResidentialAppraiser")]
        SeniorResidentialAppraiser,
    }
}
