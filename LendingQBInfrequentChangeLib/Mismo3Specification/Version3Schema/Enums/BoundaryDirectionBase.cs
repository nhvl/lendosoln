namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum BoundaryDirectionBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("East")]
        East,

        [XmlEnum("North")]
        North,

        [XmlEnum("NorthEast")]
        NorthEast,

        [XmlEnum("NorthWest")]
        NorthWest,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("South")]
        South,

        [XmlEnum("SouthEast")]
        SouthEast,

        [XmlEnum("SouthWest")]
        SouthWest,

        [XmlEnum("West")]
        West,
    }
}
