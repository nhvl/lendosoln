namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Fixed periodic payment/rate changes without subsidy or negative amortization.
    /// </summary>
    public enum AmortizationBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// A mortgage that allows the lender to adjust the interest rate in accordance with a specified index periodically.
		/// </summary>
        [XmlEnum("AdjustableRate")]
        AdjustableRate,

		/// <summary>
		/// A mortgage in which the interest rate and payments remain the same for the life of the loan.
		/// </summary>
        [XmlEnum("Fixed")]
        Fixed,

		/// <summary>
		/// GEM (Growing Equity Mortgage) -  A graduated payment mortgage in which increases in a borrowers mortgage payments are used to accelerate reduction of principal on the mortgage. Due to increased payment, the borrower acquires equity more rapidly and retires the debt earlier.
		/// </summary>
        [XmlEnum("GEM")]
        GEM,

		/// <summary>
		/// GPM - (Graduated Payment Mortgage) A type of flexible payment mortgage where the payments increase for a specified period of time and then level off. Usually involves negative amortization.
		/// </summary>
        [XmlEnum("GPM")]
        GPM,

		/// <summary>
		/// Periodic payments/rate changes with additional specified principal and interest changes as documented in the Security Instruments.
		/// </summary>
        [XmlEnum("GraduatedPaymentARM")]
        GraduatedPaymentARM,

		/// <summary>
		/// Used to describe the amortization type if it is other than those specified in the enumerated list of Loan Amortization Types.
		/// </summary>
        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// A type of flexible mortgage where the interest rate may decrease based on payment history.
		/// </summary>
        [XmlEnum("RateImprovementMortgage")]
        RateImprovementMortgage,

		/// <summary>
		/// Fixed periodic payment/rate changes without subsidy or negative amortization.
		/// </summary>
        [XmlEnum("Step")]
        Step,
    }
}
