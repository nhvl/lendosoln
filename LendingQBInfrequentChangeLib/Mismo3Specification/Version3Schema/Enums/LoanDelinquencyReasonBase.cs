namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// The principal income of the borrowers has been reduced because of a loss of job.
    /// </summary>
    public enum LoanDelinquencyReasonBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// The borrower has abandoned the property for reasons that are not known (the Servicer has been unable to locate the borrower).
		/// </summary>
        [XmlEnum("AbandonmentOfProperty")]
        AbandonmentOfProperty,

		/// <summary>
		/// The failure the business of a self-employed borrower , or the inability of the business to generate sufficient profit, has reduced the ability of the borrower to meet his or her financial obligations.
		/// </summary>
        [XmlEnum("BusinessFailure")]
        BusinessFailure,

		/// <summary>
		/// The borrower has experienced a sudden, unexpected property as a result of an accident, fire, storm, theft, earthquake, etc.
		/// </summary>
        [XmlEnum("CasualtyLoss")]
        CasualtyLoss,

		/// <summary>
		/// The income of the borrower has been reduced, because of a garnishment of wages, a change to a lower paying job, reduced commissions or overtime pay, loss of a part-time job, etc.
		/// </summary>
        [XmlEnum("CurtailmentOfIncome")]
        CurtailmentOfIncome,

		/// <summary>
		/// The principal borrower has incurred extraordinary expenses because of the death of a family member or because of taking on sole responsibility for repayment of the mortgage as a result of the death of a co-borrower.
		/// </summary>
        [XmlEnum("DeathOfMortgagorFamilyMember")]
        DeathOfMortgagorFamilyMember,

		/// <summary>
		/// The principal borrower has died.
		/// </summary>
        [XmlEnum("DeathOfPrincipalMortgagor")]
        DeathOfPrincipalMortgagor,

		/// <summary>
		/// Problems associated with a natural disaster.
		/// </summary>
        [XmlEnum("Disaster")]
        Disaster,

		/// <summary>
		/// The principal borrower has been transferred or relocated to a distant job location. The additional expenses for moving and housing in the new location have affected his or her ability to pay those expensed and the mortgage debt.
		/// </summary>
        [XmlEnum("DistantEmploymentTransfer")]
        DistantEmploymentTransfer,

		/// <summary>
		/// The borrower has incurred excessive energy related costs, or costs associated with the removal of environmental hazards, in, on, or near the property.
		/// </summary>
        [XmlEnum("EnergyEnvironmentCost")]
        EnergyEnvironmentCost,

		/// <summary>
		/// The borrower has incurred excessive debts (either in a single instance or built up over time) that prevent him or her from making payments on both those debts and the mortgage debt.
		/// </summary>
        [XmlEnum("ExcessiveObligations")]
        ExcessiveObligations,

		/// <summary>
		/// The delinquency is attributable to a legal dispute arising from a fraudulent or illegal action that occurred in connection with the origination of the mortgage (or later).
		/// </summary>
        [XmlEnum("Fraud")]
        Fraud,

		/// <summary>
		/// The principal borrower has incurred extraordinary expenses because of the illness of a family member or because of taking on sole responsibility for repayment of the mortgage as a result of the illness of aco-borrower.
		/// </summary>
        [XmlEnum("IllnessOfMortgagorFamilyMember")]
        IllnessOfMortgagorFamilyMember,

		/// <summary>
		/// A prolonged illness keeps the principal borrower from working and generating income.
		/// </summary>
        [XmlEnum("IllnessOfPrincipalMortgagor")]
        IllnessOfPrincipalMortgagor,

		/// <summary>
		/// The borrower needs rental income to make the mortgage payments and is having difficulty in finding a tenant for a one-family investment property or for one or more of the units in a one- to four-family property.
		/// </summary>
        [XmlEnum("InabilityToRentProperty")]
        InabilityToRentProperty,

		/// <summary>
		/// The borrower is having difficulty selling the property.
		/// </summary>
        [XmlEnum("InabilityToSellProperty")]
        InabilityToSellProperty,

		/// <summary>
		/// The principal borrower has been imprisoned (regardless of whether he or she is still incarcerated).
		/// </summary>
        [XmlEnum("Incarceration")]
        Incarceration,

		/// <summary>
		/// Problems associated with separation or divorce - such as a dispute over ownership of the property, a decision not to make payments until the divorce settlement is final, a reduction in the income available to repay the mortgage debt, etc. - are affecting the ability of the borrowers to make mortgage payments.
		/// </summary>
        [XmlEnum("MaritalDifficulties")]
        MaritalDifficulties,

		/// <summary>
		/// The principal borrower has been called to active duty, but his or her military pay is not sufficient to continue to pay the existing mortgage debt.
		/// </summary>
        [XmlEnum("MilitaryService")]
        MilitaryService,

		/// <summary>
		/// The delinquency is attributable to reasons that are not otherwise included in the enumerated list.
		/// </summary>
        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// An increase in the scheduled payment of a graduated payment or adjustable-rate mortgage; increased monthly escrow accruals needed to pay higher taxes, insurance premiums, or special assessments; or the need to make up an escrow shortage over the next year, is affecting the ability to make the new payment by the borrowers.
		/// </summary>
        [XmlEnum("PaymentAdjustment")]
        PaymentAdjustment,

		/// <summary>
		/// The borrower refuses to make payment(s) because of a disagreement with the Servicer about the amount of the mortgage payment, the acceptance of a partial payment, or the application of previous payments, until the dispute is resolved.
		/// </summary>
        [XmlEnum("PaymentDispute")]
        PaymentDispute,

		/// <summary>
		/// The condition of the property (substandard construction, expensive and extensive repairs needed, subsidence or sinkholes on property, impaired rights on ingress and egress, etc.) or the dissatisfaction with the property or the neighborhood is affecting the willingness to pay the mortgage by the borrowers.
		/// </summary>
        [XmlEnum("PropertyProblem")]
        PropertyProblem,

		/// <summary>
		/// Problems associated with non-marital separation.
		/// </summary>
        [XmlEnum("SeparationOfBorrowersUnrelatedToMarriage")]
        SeparationOfBorrowersUnrelatedToMarriage,

		/// <summary>
		/// There is a dispute by the Borrower in regards to how their loan has been serviced, normally centered around escrow advances, application of payments or interest rate changes. This would be used if the Servicer is having operational issues that are preventing them from effectively servicing the loan (loan set up problems, lock box delays, transfer of servicing, etc.)
		/// </summary>
        [XmlEnum("ServicingProblems")]
        ServicingProblems,

		/// <summary>
		/// The individual filing bankruptcy is not the borrower but the action may impact the borrower's financial responsibility (or debt collection efforts) for the subject property.
		/// </summary>
        [XmlEnum("ThirdPartyOccupantFiledBankruptcy")]
        ThirdPartyOccupantFiledBankruptcy,

		/// <summary>
		/// The borrower has agreed to sell the property and has decided not to make any additional payments.
		/// </summary>
        [XmlEnum("TransferOfOwnershipPending")]
        TransferOfOwnershipPending,

		/// <summary>
		/// The reason for the delinquency cannot be determined because the borrower cannot be located or has not responded to inquiries.
		/// </summary>
        [XmlEnum("UnableToContactBorrower")]
        UnableToContactBorrower,

		/// <summary>
		/// Problems associated with an employment situation that is insufficient to meet financial obligations.
		/// </summary>
        [XmlEnum("Underemployment")]
        Underemployment,

		/// <summary>
		/// The principal income of the borrowers has been reduced because of a loss of job.
		/// </summary>
        [XmlEnum("Unemployment")]
        Unemployment,

    }
}
