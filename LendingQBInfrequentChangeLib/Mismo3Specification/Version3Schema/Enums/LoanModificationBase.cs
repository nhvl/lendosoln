namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// The remaining principal balance due on the note.
    /// </summary>
    public enum LoanModificationBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// The amortization method documented on the note is to be modified (e.g., adjustable rate)
		/// </summary>
        [XmlEnum("AmortizationMethod")]
        AmortizationMethod,

		/// <summary>
		/// The interest rate documented on the note is to be modified
		/// </summary>
        [XmlEnum("InterestRate")]
        InterestRate,

		/// <summary>
		/// The date the loan is due, as documented on the note is to be modified
		/// </summary>
        [XmlEnum("MaturityDate")]
        MaturityDate,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// The principal and interest payment as documented on the note is to be modified
		/// </summary>
        [XmlEnum("PaymentAmount")]
        PaymentAmount,

		/// <summary>
		/// The time period between payment due dates as documented on the note is to be modified
		/// </summary>
        [XmlEnum("PaymentFrequency")]
        PaymentFrequency,

		/// <summary>
		/// The remaining principal balance due on the note.
		/// </summary>
        [XmlEnum("PrincipalAmount")]
        PrincipalAmount,

    }
}
