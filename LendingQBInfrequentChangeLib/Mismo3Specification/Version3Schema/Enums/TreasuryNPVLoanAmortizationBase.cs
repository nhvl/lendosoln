namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Fixed periodic payment/rate changes without subsidy or negative amortization.
    /// </summary>
    public enum TreasuryNPVLoanAmortizationBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// A mortgage that allows the lender to adjust the interest rate in accordance with a specified index periodically.
		/// </summary>
        [XmlEnum("AdjustableRate")]
        AdjustableRate,

		/// <summary>
		/// A mortgage in which the interest rate and payments remain the same for the life of the loan.
		/// </summary>
        [XmlEnum("FixedRate")]
        FixedRate,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Fixed periodic payment/rate changes without subsidy or negative amortization.
		/// </summary>
        [XmlEnum("Step")]
        Step,

    }
}
