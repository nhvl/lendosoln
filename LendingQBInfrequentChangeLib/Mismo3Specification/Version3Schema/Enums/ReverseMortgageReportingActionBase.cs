namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// An unscheduled payment to the borrower has been made that increased the loan balance, reducing the net line of credit or set aside balances.
    /// </summary>
    public enum ReverseMortgageReportingActionBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// The loan has been called due for a reason such as death, non occcupancy, failure to pay taxes and insurance, etc.
		/// </summary>
        [XmlEnum("CalledDue")]
        CalledDue,

		/// <summary>
		/// The loan has a condition which could require that the loan be made due and payable. Examples include non occupancy, failure to pay taxes and insurance, etc.
		/// </summary>
        [XmlEnum("DefaultCondition")]
        DefaultCondition,

		/// <summary>
		/// Reactivate the scheduled payments for a loan that currently had scheduled payments suspended to the borrower.
		/// </summary>
        [XmlEnum("DisbursementOrPaymentResumed")]
        DisbursementOrPaymentResumed,

		/// <summary>
		/// Suspend scheduled payments for a loan that currently has scheduled payments disbursed to the borrower.
		/// </summary>
        [XmlEnum("DisbursementOrPaymentSuspended")]
        DisbursementOrPaymentSuspended,

		/// <summary>
		/// The loan has been foreclosed. The payments will be suspended and stop the accrual of MIP, interest and service fees, as of the effective date of the action code.
		/// </summary>
        [XmlEnum("ForeclosureLiquidatedHeldForSale")]
        ForeclosureLiquidatedHeldForSale,

		/// <summary>
		/// The loan has been foreclosed and it will be liquidated as soon as the transfer of property has occurred. This will suspend payments and stop the accrual of MIP, interest and service fees, as of the effective date of the action code.
		/// </summary>
        [XmlEnum("ForeclosureLiquidatedPendingConveyance")]
        ForeclosureLiquidatedPendingConveyance,

		/// <summary>
		/// The loan has been foreclosed and a third party has acquired the property. This will suspend payments and stop the accrual of MIP, interest and service fees, as of the effective date of the action code.
		/// </summary>
        [XmlEnum("ForeclosureLiquidatedThirdPartySale")]
        ForeclosureLiquidatedThirdPartySale,

		/// <summary>
		/// The borrower has filed bankruptcy or has instituted some other type of litigation that will prevent or delay the liquidation of the loan.
		/// </summary>
        [XmlEnum("LegalActionInitiated")]
        LegalActionInitiated,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// The mortgage has been paid off.
		/// </summary>
        [XmlEnum("Payoff")]
        Payoff,

		/// <summary>
		/// The loan has been referred for deed-in-lieu, for reasons such as death, non occupancy, failure to pay taxes and insurance, etc.
		/// </summary>
        [XmlEnum("ReferredForDeedInLieu")]
        ReferredForDeedInLieu,

		/// <summary>
		/// The loan has been referred for foreclosure, for reasons such as death, non occupancy, failure to pay taxes and insurance, etc.
		/// </summary>
        [XmlEnum("ReferredForForeclosure")]
        ReferredForForeclosure,

		/// <summary>
		/// A servicer repurchase due to the fact that the loan does not meet or no longer meets the contract parameters (a quality control issue).
		/// </summary>
        [XmlEnum("Repurchase")]
        Repurchase,

		/// <summary>
		/// An unscheduled payment to the borrower has been made that increased the loan balance, reducing the net line of credit or set aside balances.
		/// </summary>
        [XmlEnum("UnscheduledPaymentMade")]
        UnscheduledPaymentMade,

    }
}
