namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum SolicitationMethodBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Email")]
        Email,

        [XmlEnum("Letter")]
        Letter,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("PhoneCall")]
        PhoneCall,

    }
}
