namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum ConversionBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("ToBiweeklyPaymentFrequency")]
        ToBiweeklyPaymentFrequency,

        [XmlEnum("ToFixedRate")]
        ToFixedRate,

        [XmlEnum("ToFullyAmortizingTerm")]
        ToFullyAmortizingTerm,

        [XmlEnum("ToLevelPayment")]
        ToLevelPayment,

        [XmlEnum("ToLowerInterestRate")]
        ToLowerInterestRate,

        [XmlEnum("ToMonthlyPaymentFrequency")]
        ToMonthlyPaymentFrequency,

    }
}
