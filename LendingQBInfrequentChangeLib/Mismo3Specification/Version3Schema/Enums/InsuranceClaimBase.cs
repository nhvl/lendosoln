namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Includes all types of hazard insurance.
    /// </summary>
    public enum InsuranceClaimBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Includes all types of hazard insurance.
		/// </summary>
        [XmlEnum("Hazard")]
        Hazard,

        [XmlEnum("MI")]
        MI,

        [XmlEnum("Other")]
        Other,

    }
}
