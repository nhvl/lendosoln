namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum PropertyPreservationStatusBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Approved")]
        Approved,

        [XmlEnum("Cancelled")]
        Cancelled,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Required")]
        Required,

    }
}
