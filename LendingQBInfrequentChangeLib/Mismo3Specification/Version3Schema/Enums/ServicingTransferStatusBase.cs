namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// The servicing rights are being retained by the seller.
    /// </summary>
    public enum ServicingTransferStatusBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// The servicing rights are being transferred to another party.
		/// </summary>
        [XmlEnum("Released")]
        Released,

		/// <summary>
		/// The servicing rights are being retained by the seller.
		/// </summary>
        [XmlEnum("Retained")]
        Retained,

    }
}
