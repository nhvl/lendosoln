namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum NewImprovementBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Additional")]
        Additional,

        [XmlEnum("Dwelling")]
        Dwelling,

        [XmlEnum("EnergyEfficientItems")]
        EnergyEfficientItems,

        [XmlEnum("Garage")]
        Garage,

        [XmlEnum("LevelFive")]
        LevelFive,

        [XmlEnum("LevelFour")]
        LevelFour,

        [XmlEnum("LevelOne")]
        LevelOne,

        [XmlEnum("LevelThree")]
        LevelThree,

        [XmlEnum("LevelTwo")]
        LevelTwo,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("PorchesPatios")]
        PorchesPatios,

        [XmlEnum("SectionFour")]
        SectionFour,

        [XmlEnum("SectionOne")]
        SectionOne,

        [XmlEnum("SectionThree")]
        SectionThree,

        [XmlEnum("SectionTwo")]
        SectionTwo,

    }
}
