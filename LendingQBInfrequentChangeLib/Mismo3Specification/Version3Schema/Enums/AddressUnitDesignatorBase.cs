namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum AddressUnitDesignatorBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Apartment")]
        Apartment,

        [XmlEnum("Basement")]
        Basement,

        [XmlEnum("Building")]
        Building,

        [XmlEnum("Condo")]
        Condo,

        [XmlEnum("Department")]
        Department,

        [XmlEnum("Floor")]
        Floor,

        [XmlEnum("Front")]
        Front,

        [XmlEnum("Hanger")]
        Hanger,

        [XmlEnum("Key")]
        Key,

        [XmlEnum("Lobby")]
        Lobby,

        [XmlEnum("Lot")]
        Lot,

        [XmlEnum("Lower")]
        Lower,

        [XmlEnum("Office")]
        Office,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Penthouse")]
        Penthouse,

        [XmlEnum("Pier")]
        Pier,

        [XmlEnum("Rear")]
        Rear,

        [XmlEnum("Room")]
        Room,

        [XmlEnum("Side")]
        Side,

        [XmlEnum("Space")]
        Space,

        [XmlEnum("Stop")]
        Stop,

        [XmlEnum("Suite")]
        Suite,

        [XmlEnum("Trailer")]
        Trailer,

        [XmlEnum("Unit")]
        Unit,

        [XmlEnum("Upper")]
        Upper,
    }
}
