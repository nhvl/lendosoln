namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum CreditPublicRecordPaymentFrequencyBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Biweekly")]
        Biweekly,

        [XmlEnum("LumpSum")]
        LumpSum,

        [XmlEnum("Monthly")]
        Monthly,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Unknown")]
        Unknown,

        [XmlEnum("Weekly")]
        Weekly,

        [XmlEnum("Yearly")]
        Yearly,

    }
}
