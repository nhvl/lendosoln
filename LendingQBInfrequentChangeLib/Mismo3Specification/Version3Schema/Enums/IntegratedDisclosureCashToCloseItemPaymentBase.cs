namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum IntegratedDisclosureCashToCloseItemPaymentBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("FromBorrower")]
        FromBorrower,

        [XmlEnum("ToBorrower")]
        ToBorrower,

    }
}
