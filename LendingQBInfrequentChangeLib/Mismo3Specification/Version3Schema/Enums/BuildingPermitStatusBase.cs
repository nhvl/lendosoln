namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// The permit is no longer active because it  was voided.
    /// </summary>
    public enum BuildingPermitStatusBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// The permit has been applied for, but has not yet been issued.
		/// </summary>
        [XmlEnum("Applied")]
        Applied,

		/// <summary>
		/// The permit is no longer active because it was cancelled.
		/// </summary>
        [XmlEnum("Cancelled")]
        Cancelled,

		/// <summary>
		/// All work on the permit has been completed, and all inspections have passed.
		/// </summary>
        [XmlEnum("Completed")]
        Completed,

		/// <summary>
		/// The permit is no longer active because its timeframe has expired.
		/// </summary>
        [XmlEnum("Expired")]
        Expired,

		/// <summary>
		/// The permit has been issued, and work on it is ongoing.
		/// </summary>
        [XmlEnum("Issued")]
        Issued,

		/// <summary>
		/// The permit has been issued, but has subsequently been put on hold, and no work can be done on the permit until one or more issues have been resolved.
		/// </summary>
        [XmlEnum("OnHold")]
        OnHold,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Unknown")]
        Unknown,

		/// <summary>
		/// The permit is no longer active because it  was voided.
		/// </summary>
        [XmlEnum("Voided")]
        Voided,
    }
}
