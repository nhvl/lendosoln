namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum RentIncludesUtilityBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Cable")]
        Cable,

        [XmlEnum("Electric")]
        Electric,

        [XmlEnum("Gas")]
        Gas,

        [XmlEnum("Oil")]
        Oil,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Sewer")]
        Sewer,

        [XmlEnum("Trash")]
        Trash,

        [XmlEnum("Water")]
        Water,

    }
}
