namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Conversion option has expired.
    /// </summary>
    public enum ConvertibleStatusBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Conversion option currently active.
		/// </summary>
        [XmlEnum("Active")]
        Active,

		/// <summary>
		/// Mortgagor exercised option to convert to fixed rate.
		/// </summary>
        [XmlEnum("Exercised")]
        Exercised,

		/// <summary>
		/// Conversion option has expired.
		/// </summary>
        [XmlEnum("Expired")]
        Expired,

    }
}
