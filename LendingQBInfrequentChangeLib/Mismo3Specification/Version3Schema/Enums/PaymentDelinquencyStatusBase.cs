namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum PaymentDelinquencyStatusBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("MoreThan120Days")]
        MoreThan120Days,

        [XmlEnum("MoreThan30Days")]
        MoreThan30Days,

        [XmlEnum("MoreThan60Days")]
        MoreThan60Days,

        [XmlEnum("MoreThan90Days")]
        MoreThan90Days,

        [XmlEnum("NotDelinquent")]
        NotDelinquent,

    }
}
