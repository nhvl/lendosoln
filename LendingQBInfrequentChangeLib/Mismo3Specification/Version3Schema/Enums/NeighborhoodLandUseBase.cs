namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum NeighborhoodLandUseBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Apartment")]
        Apartment,

        [XmlEnum("Commercial")]
        Commercial,

        [XmlEnum("Condominium")]
        Condominium,

        [XmlEnum("Cooperative")]
        Cooperative,

        [XmlEnum("Industrial")]
        Industrial,

        [XmlEnum("ManufacturedHome")]
        ManufacturedHome,

        [XmlEnum("MultiFamily")]
        MultiFamily,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("SingleFamily")]
        SingleFamily,

        [XmlEnum("TwoToFourFamily")]
        TwoToFourFamily,

        [XmlEnum("Vacant")]
        Vacant,

    }
}
