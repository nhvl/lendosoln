namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum IntegratedDisclosureSubsectionBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Adjustments")]
        Adjustments,

        [XmlEnum("AdjustmentsForItemsPaidBySellerInAdvance")]
        AdjustmentsForItemsPaidBySellerInAdvance,

        [XmlEnum("AdjustmentsForItemsUnpaidBySeller")]
        AdjustmentsForItemsUnpaidBySeller,

        [XmlEnum("ClosingCostsSubtotal")]
        ClosingCostsSubtotal,

        [XmlEnum("LenderCredits")]
        LenderCredits,

        [XmlEnum("LoanCostsSubtotal")]
        LoanCostsSubtotal,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("OtherCostsSubtotal")]
        OtherCostsSubtotal,

        [XmlEnum("OtherCredits")]
        OtherCredits,

    }
}
