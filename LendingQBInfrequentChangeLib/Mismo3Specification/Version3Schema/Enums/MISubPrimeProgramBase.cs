namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum MISubPrimeProgramBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("AMinus")]
        AMinus,

        [XmlEnum("BPaper")]
        BPaper,

        [XmlEnum("CPaper")]
        CPaper,

        [XmlEnum("Other")]
        Other,

    }
}
