namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// An estate or interest in real property held by virtue of a lease.
    /// </summary>
    public enum PropertyEstateBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// The greatest possible interest a person can have in real estate, including the right to dispose of the property or pass it on to their heirs.
		/// </summary>
        [XmlEnum("FeeSimple")]
        FeeSimple,

		/// <summary>
		/// An shared interest among multiple parties in the single title or deed associated with a property. Fractional ownership generally restricts the owners usage of the property.
		/// </summary>
        [XmlEnum("Fractional")]
        Fractional,

		/// <summary>
		/// An estate or interest in real property held by virtue of a lease.
		/// </summary>
        [XmlEnum("Leasehold")]
        Leasehold,

        [XmlEnum("Other")]
        Other,

    }
}
