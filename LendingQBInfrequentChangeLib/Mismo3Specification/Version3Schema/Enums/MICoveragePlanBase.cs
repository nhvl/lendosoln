namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum MICoveragePlanBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("LossLimitCap")]
        LossLimitCap,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Pool")]
        Pool,

        [XmlEnum("RiskSharing")]
        RiskSharing,

        [XmlEnum("SecondLayer")]
        SecondLayer,

        [XmlEnum("StandardPrimary")]
        StandardPrimary,

    }
}
