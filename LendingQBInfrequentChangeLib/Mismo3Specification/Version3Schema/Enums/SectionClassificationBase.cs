namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Lender Required Paid In Advance
    /// </summary>
    public enum SectionClassificationBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Gross Amount Due From Borrower
		/// </summary>
        [XmlEnum("100")]
        Item100,

		/// <summary>
		/// Reserves Deposited With Lender
		/// </summary>
        [XmlEnum("1000")]
        Item1000,

		/// <summary>
		/// Title Charges
		/// </summary>
        [XmlEnum("1100")]
        Item1100,

		/// <summary>
		/// Recording And Transfer Charges
		/// </summary>
        [XmlEnum("1200")]
        Item1200,

		/// <summary>
		/// Additional Settlement Charges
		/// </summary>
        [XmlEnum("1300")]
        Item1300,

		/// <summary>
		/// Amounts Paid By Or In Behalf Of Borrower
		/// </summary>
        [XmlEnum("200")]
        Item200,

		/// <summary>
		/// Cash At Settlement From To Borrower
		/// </summary>
        [XmlEnum("300")]
        Item300,

		/// <summary>
		/// Gross Amount Due To Seller
		/// </summary>
        [XmlEnum("400")]
        Item400,

		/// <summary>
		/// Reductions In Amount Due Seller
		/// </summary>
        [XmlEnum("500")]
        Item500,

		/// <summary>
		/// Cash At Settlement To From Seller
		/// </summary>
        [XmlEnum("600")]
        Item600,

		/// <summary>
		/// Division Of Commissions
		/// </summary>
        [XmlEnum("700")]
        Item700,

		/// <summary>
		/// Loan Fees
		/// </summary>
        [XmlEnum("800")]
        Item800,

		/// <summary>
		/// Lender Required Paid In Advance
		/// </summary>
        [XmlEnum("900")]
        Item900,

    }
}
