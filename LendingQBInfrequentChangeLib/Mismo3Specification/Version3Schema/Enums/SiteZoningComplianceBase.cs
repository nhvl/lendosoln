namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum SiteZoningComplianceBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Illegal")]
        Illegal,

        [XmlEnum("Legal")]
        Legal,

        [XmlEnum("LegalNonConforming")]
        LegalNonConforming,

        [XmlEnum("NoZoning")]
        NoZoning,

        [XmlEnum("Undetermined")]
        Undetermined,

        [XmlEnum("Unknown")]
        Unknown,

    }
}
