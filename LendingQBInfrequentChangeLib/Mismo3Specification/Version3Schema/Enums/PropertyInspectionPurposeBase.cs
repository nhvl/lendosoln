namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Conducted as a part of the preparation of a property valuation assignment by a licensed or certified real estate appraiser.
    /// </summary>
    public enum PropertyInspectionPurposeBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Conducted by a local building official from the municipality or county or other government agency for purposes of enforcing building codes and statutes.
		/// </summary>
        [XmlEnum("BuildingCodeCompliance")]
        BuildingCodeCompliance,

		/// <summary>
		/// An inspection conducted on a single system by a contractor, tradesman or professional inspector, licensed where required.
		/// </summary>
        [XmlEnum("BuildingComponent")]
        BuildingComponent,

		/// <summary>
		/// An inspection conducted to meet specific business requirements of mortgage insurers, hazard insurers, investors or servicers.
		/// </summary>
        [XmlEnum("ComplianceInspection")]
        ComplianceInspection,

		/// <summary>
		/// An inspection conducted to confirm and rate the energy effeciency of a structure.
		/// </summary>
        [XmlEnum("EnergyEffeciency")]
        EnergyEffeciency,

		/// <summary>
		/// An inspection conducted for an environmental hazard such as vapor intrusion, asbestos, radon, mold, etc.
		/// </summary>
        [XmlEnum("EnvironmentalHazard")]
        EnvironmentalHazard,

		/// <summary>
		/// Conducted by a professional property inspector, licensed where required, and at least three systems are inspected.
		/// </summary>
        [XmlEnum("HomeInspection")]
        HomeInspection,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Conducted a professional pest control technician, licensed where required, and includes wood-destroying pests.
		/// </summary>
        [XmlEnum("Pest")]
        Pest,

		/// <summary>
		/// Conducted as a part of securing or managing a property by a property manager or contractor on behalf of a lender, servicer or investor.
		/// </summary>
        [XmlEnum("PropertyPreservation")]
        PropertyPreservation,

		/// <summary>
		/// Conducted as a part of the preparation of a property valuation assignment by a licensed or certified real estate appraiser. 
		/// </summary>
        [XmlEnum("Valuation")]
        Valuation,

    }
}
