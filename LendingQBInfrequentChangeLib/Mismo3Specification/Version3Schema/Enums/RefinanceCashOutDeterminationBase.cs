namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum RefinanceCashOutDeterminationBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("CashOut")]
        CashOut,

        [XmlEnum("LimitedCashOut")]
        LimitedCashOut,

        [XmlEnum("NoCashOut")]
        NoCashOut,

        [XmlEnum("Unknown")]
        Unknown,

    }
}
