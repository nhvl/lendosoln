namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates the property is legally habitable.
    /// </summary>
    public enum CertificateBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Indicates the property is legally habitable.
		/// </summary>
        [XmlEnum("OccupancyCertification")]
        OccupancyCertification,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("UsageCertification")]
        UsageCertification,
    }
}
