namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Modification of E_LoanFeaturesGsePropertyType for GsePropertyType field in MISMO 2.6.
    /// DO NOT USE EXCEPT IN PROPRIETARY EXTENSIONS.
    /// </summary>
    public enum GsePropertyBase
    {
        [XmlEnum("")]
        Blank,

        [XmlEnum("SingleFamily")]
        SingleFamily,

        [XmlEnum("Condominium")]
        Condominium,

        [XmlEnum("Townhouse")]
        Townhouse,

        [XmlEnum("Cooperative")]
        Cooperative,

        [XmlEnum("TwoToFourUnitProperty")]
        TwoToFourUnitProperty,

        [XmlEnum("MultifamilyMoreThanFourUnits")]
        MultifamilyMoreThanFourUnits,

        [XmlEnum("ManufacturedMobileHome")]
        ManufacturedMobileHome,

        [XmlEnum("CommercialNonResidential")]
        CommercialNonResidential,

        [XmlEnum("MixedUseResidential")]
        MixedUseResidential,

        [XmlEnum("Farm")]
        Farm,

        [XmlEnum("HomeAndBusinessCombined")]
        HomeAndBusinessCombined,

        [XmlEnum("Land")]
        Land
    }
}
