namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum LegalEntityBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Corporation")]
        Corporation,

        [XmlEnum("CorporationSole")]
        CorporationSole,

        [XmlEnum("Estate")]
        Estate,

        [XmlEnum("GovernmentEntity")]
        GovernmentEntity,

        [XmlEnum("JointVenture")]
        JointVenture,

        [XmlEnum("LimitedLiabilityCompany")]
        LimitedLiabilityCompany,

        [XmlEnum("LimitedPartnership")]
        LimitedPartnership,

        [XmlEnum("NonProfitCorporation")]
        NonProfitCorporation,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Partnership")]
        Partnership,

        [XmlEnum("SoleProprietorship")]
        SoleProprietorship,

        [XmlEnum("Trust")]
        Trust,

    }
}
