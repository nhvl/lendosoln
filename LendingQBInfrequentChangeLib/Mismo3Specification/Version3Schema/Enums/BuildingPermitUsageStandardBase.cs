namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum BuildingPermitUsageStandardBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Commercial")]
        Commercial,

        [XmlEnum("Hazardous")]
        Hazardous,

        [XmlEnum("Industrial")]
        Industrial,

        [XmlEnum("MultiFamily")]
        MultiFamily,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("SingleFamily")]
        SingleFamily,
    }
}
