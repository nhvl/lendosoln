namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// The transaction meets Regulation Z Qualified Mortgage Safe Harbor threshold.
    /// </summary>
    public enum PresumptionOfComplianceBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Presumed compliance that creditor has followed or met certain requirements related to the borrower's ability to repay.
		/// </summary>
        [XmlEnum("RebuttablePresumption")]
        RebuttablePresumption,

		/// <summary>
		/// The transaction meets Regulation Z Qualified Mortgage Safe Harbor threshold.
		/// </summary>
        [XmlEnum("SafeHarbor")]
        SafeHarbor,

    }
}
