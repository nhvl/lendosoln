namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// American Land Title Association
    /// </summary>
    public enum TitleAssociationBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// American Land Title Association
		/// </summary>
        [XmlEnum("ALTA")]
        ALTA,

        [XmlEnum("CLTA")]
        CLTA,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("TLTA")]
        TLTA,

    }
}
