namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum HazardInsuranceCoverageBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Earthquake")]
        Earthquake,

        [XmlEnum("FireAndExtendedCoverage")]
        FireAndExtendedCoverage,

        [XmlEnum("Flood")]
        Flood,

        [XmlEnum("Hazard")]
        Hazard,

        [XmlEnum("Homeowners")]
        Homeowners,

        [XmlEnum("Hurricane")]
        Hurricane,

        [XmlEnum("InsectInfestation")]
        InsectInfestation,

        [XmlEnum("Leasehold")]
        Leasehold,

        [XmlEnum("MineSubsidence")]
        MineSubsidence,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("PersonalProperty")]
        PersonalProperty,

        [XmlEnum("Storm")]
        Storm,

        [XmlEnum("Tornado")]
        Tornado,

        [XmlEnum("Volcano")]
        Volcano,

        [XmlEnum("Wind")]
        Wind,

    }
}
