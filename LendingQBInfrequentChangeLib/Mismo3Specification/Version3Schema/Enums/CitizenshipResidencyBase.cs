namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum CitizenshipResidencyBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("NonPermanentResidentAlien")]
        NonPermanentResidentAlien,

        [XmlEnum("NonResidentAlien")]
        NonResidentAlien,

        [XmlEnum("PermanentResidentAlien")]
        PermanentResidentAlien,

        [XmlEnum("Unknown")]
        Unknown,

        [XmlEnum("USCitizen")]
        USCitizen,

    }
}
