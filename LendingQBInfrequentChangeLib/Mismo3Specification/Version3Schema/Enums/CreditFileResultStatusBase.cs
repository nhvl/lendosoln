namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Data value will be found in Credit File Result Status Type Other Description.
    /// </summary>
    public enum CreditFileResultStatusBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// The requested credit file was returned by the credit repository bureau.
		/// </summary>
        [XmlEnum("FileReturned")]
        FileReturned,

		/// <summary>
		/// The requested credit file was not returned because the correct access code was not present in the request file. A valid access code from the borrower is needed to access files that are under a credit freeze.
		/// </summary>
        [XmlEnum("NoFileReturnedBadAccessCode")]
        NoFileReturnedBadAccessCode,

		/// <summary>
		/// The requested credit file was not delivered because the borrower is a minor.
		/// </summary>
        [XmlEnum("NoFileReturnedBorrowerIsAMinor")]
        NoFileReturnedBorrowerIsAMinor,

		/// <summary>
		/// The requested credit file was not delivered because the borrower requested that it not be delivered.
		/// </summary>
        [XmlEnum("NoFileReturnedCreditFreeze")]
        NoFileReturnedCreditFreeze,

		/// <summary>
		/// Because of errors, there was no credit file returned.
		/// </summary>
        [XmlEnum("NoFileReturnedError")]
        NoFileReturnedError,

		/// <summary>
		/// The requested credit file was not returned because known fraudulent data was submitted in the credit request file, such as an address or SSN.
		/// </summary>
        [XmlEnum("NoFileReturnedFraudulentRequestData")]
        NoFileReturnedFraudulentRequestData,

		/// <summary>
		/// The requested credit file was not returned because there was insufficient borrower demographics information present in the request to correctly link with a borrower credit file. Some state laws set minimum requirements for demographics information needed to access a credit file.
		/// </summary>
        [XmlEnum("NoFileReturnedInadequateRequestData")]
        NoFileReturnedInadequateRequestData,

		/// <summary>
		/// There was no credit file that matched the borrower data in the credit request.
		/// </summary>
        [XmlEnum("NoFileReturnedNoHit")]
        NoFileReturnedNoHit,

		/// <summary>
		/// The requested credit file was not returned because there was insufficient credit data and/or history available.
		/// </summary>
        [XmlEnum("NoFileReturnedThinFile")]
        NoFileReturnedThinFile,

		/// <summary>
		/// Data value will be found in Credit File Result Status Type Other Description.
		/// </summary>
        [XmlEnum("Other")]
        Other,

    }
}
