namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum RefinanceImprovementsBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Made")]
        Made,

        [XmlEnum("ToBeMade")]
        ToBeMade,

        [XmlEnum("Unknown")]
        Unknown,

    }
}
