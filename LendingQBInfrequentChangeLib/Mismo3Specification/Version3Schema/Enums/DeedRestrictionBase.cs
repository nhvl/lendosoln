namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum DeedRestrictionBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("ModeratelyPricedDwellingUnit")]
        ModeratelyPricedDwellingUnit,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Servitude")]
        Servitude,

    }
}
