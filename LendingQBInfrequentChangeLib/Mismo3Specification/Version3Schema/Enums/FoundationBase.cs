namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum FoundationBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Basement")]
        Basement,

        [XmlEnum("Crawlspace")]
        Crawlspace,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("PartialBasement")]
        PartialBasement,

        [XmlEnum("Slab")]
        Slab,

    }
}
