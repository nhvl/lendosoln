namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Agent ID specified was found but the Title Underwriter identified is not recognized as a valid Title Underwriter or is not valid for that agent in the trading partner's system.
    /// </summary>
    public enum TitleAgentValidationReasonBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Agent ID specified was found but the address specified was not matching the trading partner's system.
		/// </summary>
        [XmlEnum("AgentAddressInvalid")]
        AgentAddressInvalid,

		/// <summary>
		/// The format of the agent's ID was incorrectly formatted based on agreed format between trading partners.
		/// </summary>
        [XmlEnum("AgentIdentifierFormatInvalid")]
        AgentIdentifierFormatInvalid,

		/// <summary>
		/// Agent ID specified was found but the agent is not recognized as a valid agent for that jurisdiction.
		/// </summary>
        [XmlEnum("AgentJurisdictionInvalid")]
        AgentJurisdictionInvalid,

		/// <summary>
		/// Agent ID specified was found but the loan amount exceeds the maximum amount the agent is authorized to issue policy.
		/// </summary>
        [XmlEnum("AmountExceedsAuthorizedLimit")]
        AmountExceedsAuthorizedLimit,

		/// <summary>
		/// Agent ID specified was found but is currently a cancelled agent for the underwriter.
		/// </summary>
        [XmlEnum("Cancelled")]
        Cancelled,

		/// <summary>
		/// The closing date specified was not valid for the transaction specified.
		/// </summary>
        [XmlEnum("ClosingDateInvalid")]
        ClosingDateInvalid,

		/// <summary>
		/// Agent ID specified was found but is currently not active agent in the trading partner's system.
		/// </summary>
        [XmlEnum("NotActive")]
        NotActive,

		/// <summary>
		/// Place the validation reason type information in TitleAgentValidationReasonTypeOtherDescription this would be used to pass trading partner specific reason codes.
		/// </summary>
        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Agent ID specified was found but the Title Underwriter identified is not recognized as a valid Title Underwriter or is not valid for that agent in the trading partner's system.
		/// </summary>
        [XmlEnum("TitleUnderwriterInvalid")]
        TitleUnderwriterInvalid,

    }
}
