namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum PropertyZoningCategoryBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Agricultural")]
        Agricultural,

        [XmlEnum("Commercial")]
        Commercial,

        [XmlEnum("Industrial")]
        Industrial,

        [XmlEnum("NonResidentialGrandfatheredResidential")]
        NonResidentialGrandfatheredResidential,

        [XmlEnum("NoZoning")]
        NoZoning,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Residential")]
        Residential,

    }
}
