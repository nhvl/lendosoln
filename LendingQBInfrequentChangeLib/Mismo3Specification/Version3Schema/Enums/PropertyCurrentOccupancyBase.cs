namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// The property consists of a 2 to 4 family and the entire dwelling is not vacant.
    /// </summary>
    public enum PropertyCurrentOccupancyBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Abandoned")]
        Abandoned,

		/// <summary>
		/// Occupancy of a property by a trespasser to the exclusion of the owner or lawful occupier.
		/// </summary>
        [XmlEnum("AdverseOccupied")]
        AdverseOccupied,

		/// <summary>
		/// Property is known to be occupied but the identity of the occupant is unknown.
		/// </summary>
        [XmlEnum("OccupiedByUnknown")]
        OccupiedByUnknown,

        [XmlEnum("OwnerOccupied")]
        OwnerOccupied,

		/// <summary>
		/// The property consists of a 2 to 4 family and the entire dwelling is not vacant.
		/// </summary>
        [XmlEnum("PartiallyVacant")]
        PartiallyVacant,

        [XmlEnum("TenantOccupied")]
        TenantOccupied,

        [XmlEnum("Unknown")]
        Unknown,

        [XmlEnum("Vacant")]
        Vacant,

    }
}
