namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum PrimaryMIAbsenceReasonBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("CharterParticipation")]
        CharterParticipation,

        [XmlEnum("DelinquencyDisposition")]
        DelinquencyDisposition,

        [XmlEnum("InvestorPurchasedMortgageInsurance")]
        InvestorPurchasedMortgageInsurance,

        [XmlEnum("MICanceledBasedOnCurrentLTV")]
        MICanceledBasedOnCurrentLTV,

        [XmlEnum("NoMIBasedOnOriginalLTV")]
        NoMIBasedOnOriginalLTV,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("PledgedAsset")]
        PledgedAsset,

        [XmlEnum("PoolCoverage")]
        PoolCoverage,

        [XmlEnum("Repurchase")]
        Repurchase,

        [XmlEnum("SelfInsured")]
        SelfInsured,

    }
}
