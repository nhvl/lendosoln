namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Construction of the improvements has begun but is not complete.
    /// </summary>
    public enum ConstructionStatusBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// The structure is newly completed and has never been occupied as a residence.
		/// </summary>
        [XmlEnum("Complete")]
        Complete,

		/// <summary>
		/// The improvements are 100%  complete.
		/// </summary>
        [XmlEnum("Existing")]
        Existing,

		/// <summary>
		/// Construction of the improvements is not complete and not actively under construction.
		/// </summary>
        [XmlEnum("Incomplete")]
        Incomplete,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Construction of the improvements has not started and description is based on plans and specifications and/or substantially similar documentation.
		/// </summary>
        [XmlEnum("Proposed")]
        Proposed,

        [XmlEnum("SubjectToAlteration")]
        SubjectToAlteration,

        [XmlEnum("SubjectToAlterationImprovementRepairAndRehabilitation")]
        SubjectToAlterationImprovementRepairAndRehabilitation,

        [XmlEnum("SubstantiallyRehabilitated")]
        SubstantiallyRehabilitated,

		/// <summary>
		/// Construction of the improvements has begun but is not complete.
		/// </summary>
        [XmlEnum("UnderConstruction")]
        UnderConstruction,

    }
}
