namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Landscaping components design to not interfere with environment.
    /// </summary>
    public enum LandscapingBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Refers to the built environment including paved areas like streets & sidewalks, structures, walls, street amenities, pools and fountains.
		/// </summary>
        [XmlEnum("HardScape")]
        HardScape,

		/// <summary>
		/// A landscape that is unaffected by human activity. A natural landscape is intact when all living and nonliving elements are free to move and change.
		/// </summary>
        [XmlEnum("Natural")]
        Natural,

		/// <summary>
		/// 
		/// </summary>
        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Refers to horticultural expert or professionally designed and executed plantings specific to the site.
		/// </summary>
        [XmlEnum("ProfessionalPlantings")]
        ProfessionalPlantings,

		/// <summary>
		/// Landscaping and gardening that reduces or eliminates the need for supplemental water from irrigation.
		/// </summary>
        [XmlEnum("XeriScape")]
        XeriScape,

		/// <summary>
		/// Landscaping components design to not interfere with environment.
		/// </summary>
        [XmlEnum("ZeroImpact")]
        ZeroImpact,

    }
}
