namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum LoanRepaymentBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("ConstantPrincipal")]
        ConstantPrincipal,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("PrincipalPaymentOption")]
        PrincipalPaymentOption,

        [XmlEnum("ScheduledAmortization")]
        ScheduledAmortization,

    }
}
