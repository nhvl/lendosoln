namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum ClosingAgentBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Attorney")]
        Attorney,

        [XmlEnum("EscrowCompany")]
        EscrowCompany,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("TitleCompany")]
        TitleCompany,

    }
}
