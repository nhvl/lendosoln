namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum ConstructionPhaseInterestPaymentMethodBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("AddToPermanentLoanAmount")]
        AddToPermanentLoanAmount,

        [XmlEnum("BilledToBorrower")]
        BilledToBorrower,

        [XmlEnum("Other")]
        Other,

    }
}
