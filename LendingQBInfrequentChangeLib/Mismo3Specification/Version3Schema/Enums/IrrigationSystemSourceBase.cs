namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A system that utilizes water from an underground water source.
    /// </summary>
    public enum IrrigationSystemSourceBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// A system that collects rainwater, commonly using an open barrel, that is not attached to the structure or land.
		/// </summary>
        [XmlEnum("CatchmentSystemFed")]
        CatchmentSystemFed,

		/// <summary>
		/// A system that harvests rainwater collected in a closed tank attached to the structure or the land.
		/// </summary>
        [XmlEnum("CisternFed")]
        CisternFed,

		/// <summary>
		/// A system that utilizes water provided from a public utility.
		/// </summary>
        [XmlEnum("CityWaterFed")]
        CityWaterFed,

		/// <summary>
		/// 
		/// </summary>
        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// A system that utilizes water recycled from other uses.
		/// </summary>
        [XmlEnum("ReclaimedWater")]
        ReclaimedWater,

		/// <summary>
		/// A system that utilizes water from an underground water source.
		/// </summary>
        [XmlEnum("WellFed")]
        WellFed,

    }
}
