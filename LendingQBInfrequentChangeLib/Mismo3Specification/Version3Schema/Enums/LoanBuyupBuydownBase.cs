namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum LoanBuyupBuydownBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Buydown")]
        Buydown,

        [XmlEnum("Buyup")]
        Buyup,

        [XmlEnum("BuyupBuydownDoesNotApply")]
        BuyupBuydownDoesNotApply,

        [XmlEnum("Other")]
        Other,

    }
}
