namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum ConstructionToPermanentClosingBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("OneClosing")]
        OneClosing,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("TwoClosing")]
        TwoClosing,

    }
}
