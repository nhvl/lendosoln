namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Also referenced as Method A, one-half of the loan commitment amount is used to calculate the amount of estimated interest.
    /// </summary>
    public enum ConstructionLoanEstimatedInterestCalculationMethodBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Also referenced as Method B, the entire amount of the loan commitment is used to calculate the amount of estimated interest.
		/// </summary>
        [XmlEnum("FullLoanCommitment")]
        FullLoanCommitment,

		/// <summary>
		/// Also referenced as Method A, one-half of the loan commitment amount is used to calculate the amount of estimated interest.
		/// </summary>
        [XmlEnum("HalfLoanCommitment")]
        HalfLoanCommitment,

    }
}
