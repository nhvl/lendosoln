namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// This means that the income entered is to be reduced by the amount of alimony.
    /// </summary>
    public enum FHAAlimonyLiabilityTreatmentBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// This means that the alimony is included in the aggregate of obligations.
		/// </summary>
        [XmlEnum("AdditionToDebt")]
        AdditionToDebt,

		/// <summary>
		/// This means that the income entered is to be reduced by the amount of alimony.
		/// </summary>
        [XmlEnum("ReductionToIncome")]
        ReductionToIncome,

    }
}
