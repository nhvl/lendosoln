namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum VALoanProcedureBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Automatic")]
        Automatic,

        [XmlEnum("AutomaticInterestRateReductionRefinanceLoan")]
        AutomaticInterestRateReductionRefinanceLoan,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("VAPriorApproval")]
        VAPriorApproval,

    }
}
