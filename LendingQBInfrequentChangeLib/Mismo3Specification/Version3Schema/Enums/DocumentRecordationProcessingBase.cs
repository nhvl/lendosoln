namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// The document needs to be sent to the recorder and will recorded in the land records.
    /// </summary>
    public enum DocumentRecordationProcessingBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// The document needs to be sent to the recorder but is not to be recorded in the land records.
		/// </summary>
        [XmlEnum("DeliveryRequired")]
        DeliveryRequired,

		/// <summary>
		/// The document does not get sent to the recorder at all.
		/// </summary>
        [XmlEnum("NoProcessingRequired")]
        NoProcessingRequired,

		/// <summary>
		/// The document needs to be sent to the recorder and will recorded in the land records.
		/// </summary>
        [XmlEnum("RecordationRequired")]
        RecordationRequired,

    }
}
