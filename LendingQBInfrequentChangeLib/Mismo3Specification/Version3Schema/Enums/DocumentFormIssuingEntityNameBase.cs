namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Federal Emergency Management Agency
    /// </summary>
    public enum DocumentFormIssuingEntityNameBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Consumer Financial Protection Bureau
		/// </summary>
        [XmlEnum("CFPB")]
        CFPB,

		/// <summary>
		/// Federal Emergency Management Agency
		/// </summary>
        [XmlEnum("FEMA")]
        FEMA,

        [XmlEnum("FHA")]
        FHA,

        [XmlEnum("FNM")]
        FNM,

        [XmlEnum("FNM_FRE")]
        FNM_FRE,

        [XmlEnum("FRE")]
        FRE,

        [XmlEnum("HUD")]
        HUD,

        [XmlEnum("IRS")]
        IRS,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("VA")]
        VA,

    }
}
