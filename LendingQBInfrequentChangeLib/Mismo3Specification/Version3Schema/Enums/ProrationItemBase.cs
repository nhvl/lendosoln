namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Periodic amount paid for coverage against losses on the insured subject property and sometimes insured contents of personal property, and other improvements (fences, porches, patios, driveways, landscaping, accessory buildings, in ground pools, etc.) associated with windstorms, tornadoes and hurricanes affecting the subject property.
    /// </summary>
    public enum ProrationItemBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Property tax levied by the borough in which the subject property is located.
		/// </summary>
        [XmlEnum("BoroughPropertyTax")]
        BoroughPropertyTax,

		/// <summary>
		/// Property tax levied by the city in which the subject property is located.
		/// </summary>
        [XmlEnum("CityPropertyTax")]
        CityPropertyTax,

		/// <summary>
		/// Fee payable to condominium association in which the subject property is located, which is intended to pay electricity bills for street lights, landscaping, and maintenance and repairs to community facilities like clubhouses, pools, and exercise rooms and may also cover insurance on community assets, the salaries of condominium association employees and/or third party management fees.
		/// </summary>
        [XmlEnum("CondominiumAssociationDues")]
        CondominiumAssociationDues,

		/// <summary>
		/// Fee payable to condominium association in which the subject property is located for capital improvements, utility service upgrades, etc. that are assessed in addition to the regularly occurring condominium dues.
		/// </summary>
        [XmlEnum("CondominiumAssociationSpecialAssessment")]
        CondominiumAssociationSpecialAssessment,

		/// <summary>
		/// Fee payable to cooperative in which the subject property is located, which is intended to pay common utilities, landscaping, and maintenance and repairs to cooperative facilities and may also cover insurance on cooperative assets, the salaries of cooperative employees and/or third party management fees.
		/// </summary>
        [XmlEnum("CooperativeAssociationDues")]
        CooperativeAssociationDues,

		/// <summary>
		/// Fee payable to cooperative association or corporation in which the subject property is located for capital improvements, utility service upgrades, etc. that are assessed in addition to the regularly occurring dues, fees or debt service.
		/// </summary>
        [XmlEnum("CooperativeAssociationSpecialAssessment")]
        CooperativeAssociationSpecialAssessment,

		/// <summary>
		/// Property tax levied by the county in which the subject property is located.
		/// </summary>
        [XmlEnum("CountyPropertyTax")]
        CountyPropertyTax,

		/// <summary>
		/// Property tax levied by the district in which the subject property is located.
		/// </summary>
        [XmlEnum("DistrictPropertyTax")]
        DistrictPropertyTax,

		/// <summary>
		/// Fee payable to insurer to provide property insurance against loss from earthquake, land tremors, landslide, mudflow, or other earth movement whether the damage is caused by an earthquake. 
		/// </summary>
        [XmlEnum("EarthquakeInsurancePremium")]
        EarthquakeInsurancePremium,

		/// <summary>
		/// Fee paid for flood insurance coverage on the subject property and may include coverage for contents.
		/// </summary>
        [XmlEnum("FloodInsurancePremium")]
        FloodInsurancePremium,

		/// <summary>
		/// A sum paid for land lease by the owner of a building to the owner of the land on which it is located.
		/// </summary>
        [XmlEnum("GroundRent")]
        GroundRent,

		/// <summary>
		/// Cost of coverage for hail damage insurance, which typically covers the cost of rebuilding the subject property if losses are caused by hail storms.
		/// </summary>
        [XmlEnum("HailInsurancePremium")]
        HailInsurancePremium,

		/// <summary>
		/// Fee paid for coverage on the subject property for losses or damage due to fire or other named perils.
		/// </summary>
        [XmlEnum("HazardInsurancePremium")]
        HazardInsurancePremium,

		/// <summary>
		/// Fee payable to homeowners or neighborhood association within which the subject property is located, which is intended to pay electricity bills for street lights, landscaping, and maintenance and repairs to community facilities like clubhouses, pools, and exercise rooms and may also cover insurance on community assets, the salaries of HOA employees or third party management fees.
		/// </summary>
        [XmlEnum("HomeownersAssociationDues")]
        HomeownersAssociationDues,

		/// <summary>
		/// Fee payable to homeowners or neighborhood association in which the subject property is located for capital improvements, utility service upgrades, etc., that are assessed in addition to the regularly occurring association dues.
		/// </summary>
        [XmlEnum("HomeownersAssociationSpecialAssessment")]
        HomeownersAssociationSpecialAssessment,

		/// <summary>
		/// Periodic amount paid for coverage against hazards, theft and other losses on the insured subject property and sometimes insured contents of personal property, and other improvements (fences, porches, patios, driveways, landscaping, accessory buildings, in ground pools, etc.) May or may not include riders to insure against losses caused by windstorms, hurricanes, etc.
		/// </summary>
        [XmlEnum("HomeownersInsurancePremium")]
        HomeownersInsurancePremium,

        [XmlEnum("InterestOnLoanAssumption")]
        InterestOnLoanAssumption,

		/// <summary>
		/// Fee for Insurance or guaranty to protect lender against the non-payment of, or default on, an individual mortgage.
		/// </summary>
        [XmlEnum("MortgageInsurancePremium")]
        MortgageInsurancePremium,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Amount of real estate taxes on the subject property that are past due as of the closing date.
		/// </summary>
        [XmlEnum("PastDuePropertyTax")]
        PastDuePropertyTax,

		/// <summary>
		/// Periodic amount paid by a tenant for occupancy of a property.
		/// </summary>
        [XmlEnum("RentFromSubjectProperty")]
        RentFromSubjectProperty,

		/// <summary>
		/// Property tax levied by the state in which the subject property is located.
		/// </summary>
        [XmlEnum("StatePropertyTax")]
        StatePropertyTax,

		/// <summary>
		/// Property tax levied by the town in which the subject property is located.
		/// </summary>
        [XmlEnum("TownPropertyTax")]
        TownPropertyTax,

		/// <summary>
		/// Cost of services to provide power, heat, water, waste removal and/or sewer to the subject property.
		/// </summary>
        [XmlEnum("Utilities")]
        Utilities,

		/// <summary>
		/// Periodic amount paid for coverage against losses on the insured subject property and sometimes insured contents of personal property, and other improvements (fences, porches, patios, driveways, landscaping, accessory buildings, in ground pools, etc.) associated with volcano eruptions near the subject property.
		/// </summary>
        [XmlEnum("VolcanoInsurancePremium")]
        VolcanoInsurancePremium,

		/// <summary>
		/// Periodic amount paid for coverage against losses on the insured subject property and sometimes insured contents of personal property, and other improvements (fences, porches, patios, driveways, landscaping, accessory buildings, in ground pools, etc.) associated with windstorms, tornadoes and hurricanes affecting the subject property.
		/// </summary>
        [XmlEnum("WindAndStormInsurancePremium")]
        WindAndStormInsurancePremium,

    }
}
