namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A condominium project that is not 100% complete, or subject to additional phasing, or  less than 90% of the total number of units have conveyed to unit owners, or the unit owners do not control the home owners association.
    /// </summary>
    public enum CondominiumProjectStatusBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// A condominium project that is 100% complete, and not subject to additional phasing, and at least 90% of the total number of units have been conveyed to unit owners, and the unit owners control the home owners association.
		/// </summary>
        [XmlEnum("Established")]
        Established,

		/// <summary>
		/// A condominium project that is not 100% complete, or subject to additional phasing, or  less than 90% of the total number of units have conveyed to unit owners, or the unit owners do not control the home owners association.
		/// </summary>
        [XmlEnum("New")]
        New,

    }
}
