namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum InterestCalculationBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Compound")]
        Compound,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("RuleOf78s")]
        RuleOf78s,

        [XmlEnum("Simple")]
        Simple,

    }
}
