namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum ArchitecturalDesignCategoryBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Bungalow")]
        Bungalow,

        [XmlEnum("CapeCod")]
        CapeCod,

        [XmlEnum("Colonial")]
        Colonial,

        [XmlEnum("Contemporary")]
        Contemporary,

        [XmlEnum("Cottage")]
        Cottage,

        [XmlEnum("Craftsman")]
        Craftsman,

        [XmlEnum("English")]
        English,

        [XmlEnum("French")]
        French,

        [XmlEnum("Georgian")]
        Georgian,

        [XmlEnum("LowCountry")]
        LowCountry,

        [XmlEnum("Mediterranean")]
        Mediterranean,

        [XmlEnum("Modern")]
        Modern,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Plantation")]
        Plantation,

        [XmlEnum("Ranch")]
        Ranch,

        [XmlEnum("Shotgun")]
        Shotgun,

        [XmlEnum("Spanish")]
        Spanish,

        [XmlEnum("Traditional")]
        Traditional,

        [XmlEnum("Tudor")]
        Tudor,

        [XmlEnum("Victorian")]
        Victorian,
    }
}
