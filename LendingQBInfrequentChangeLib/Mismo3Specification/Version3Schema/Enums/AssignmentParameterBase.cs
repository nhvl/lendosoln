namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum AssignmentParameterBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("BirdsEyeAerialMap")]
        BirdsEyeAerialMap,

        [XmlEnum("CurrentLicence")]
        CurrentLicence,

        [XmlEnum("DetailedConditionComments")]
        DetailedConditionComments,

        [XmlEnum("ErrorsAndOmissionsDeclaration")]
        ErrorsAndOmissionsDeclaration,

        [XmlEnum("ExteriorPhotoFront")]
        ExteriorPhotoFront,

        [XmlEnum("ExteriorPhotoRear")]
        ExteriorPhotoRear,

        [XmlEnum("InteriorInspection")]
        InteriorInspection,

        [XmlEnum("InteriorPhotoBath")]
        InteriorPhotoBath,

        [XmlEnum("InteriorPhotoBedroom")]
        InteriorPhotoBedroom,

        [XmlEnum("InteriorPhotoKitchen")]
        InteriorPhotoKitchen,

        [XmlEnum("InteriorPhotoLivingArea")]
        InteriorPhotoLivingArea,

        [XmlEnum("LocationMap")]
        LocationMap,

        [XmlEnum("NoInvoice")]
        NoInvoice,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("PhotoAdverseCondition")]
        PhotoAdverseCondition,

        [XmlEnum("PhotoCarbonMonoxideDetector")]
        PhotoCarbonMonoxideDetector,

        [XmlEnum("PhotoFunctionalObsolescence")]
        PhotoFunctionalObsolescence,

        [XmlEnum("PhotoHeatingVentalationAirConditioning")]
        PhotoHeatingVentalationAirConditioning,

        [XmlEnum("PlatMap")]
        PlatMap,

        [XmlEnum("ProofOfElectricity")]
        ProofOfElectricity,

        [XmlEnum("ProofOfRunningWater")]
        ProofOfRunningWater,

        [XmlEnum("RenovationRestorationPhoto")]
        RenovationRestorationPhoto,

        [XmlEnum("SafteyLatchWindowPhoto")]
        SafteyLatchWindowPhoto,

        [XmlEnum("TaxAssementRecord")]
        TaxAssementRecord,
    }
}
