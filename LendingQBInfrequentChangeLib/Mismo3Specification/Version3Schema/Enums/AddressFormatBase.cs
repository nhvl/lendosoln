namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Delivery of mail to a rural free area.
    /// </summary>
    public enum AddressFormatBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// A route for carrying mail over the highway between designated points. Given on a contract to a private carrier and often requiring, in rural areas, delivery to home mailboxes.
		/// </summary>
        [XmlEnum("HighwayContract")]
        HighwayContract,

		/// <summary>
		/// Delivery of mail to a single destination defined by USPS. Commonly referred to as the street address.
		/// </summary>
        [XmlEnum("Individual")]
        Individual,

		/// <summary>
		/// Delivery of mail to a foreign destination that is not part of the USPS delivery network.
		/// </summary>
        [XmlEnum("International")]
        International,

		/// <summary>
		/// Delivery of mail to a military location (APO/FPO/DPO).
		/// </summary>
        [XmlEnum("Military")]
        Military,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Delivery of mail to a secured location within a postal facility.
		/// </summary>
        [XmlEnum("PostOfficeBox")]
        PostOfficeBox,

		/// <summary>
		/// Delivery of mail to a rural free area.
		/// </summary>
        [XmlEnum("RuralRoute")]
        RuralRoute,
    }
}
