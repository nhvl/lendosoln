namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum AssumabilityBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("AssumableAfterFirstRateChangeDate")]
        AssumableAfterFirstRateChangeDate,

        [XmlEnum("AssumableForLifeOfLoan")]
        AssumableForLifeOfLoan,

        [XmlEnum("Other")]
        Other,
    }
}
