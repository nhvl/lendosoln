namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum NeighborhoodPropertyLocationBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Rural")]
        Rural,

        [XmlEnum("RuralUrban")]
        RuralUrban,

        [XmlEnum("Suburban")]
        Suburban,

        [XmlEnum("Urban")]
        Urban,

        [XmlEnum("UrbanSprawl")]
        UrbanSprawl,

    }
}
