namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// This term must be associated with an AVM that allows for end user to make adjustments.
    /// </summary>
    public enum AVMOutcomeBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// The value conclusion derived by the model after underlying valuation engine has pulled property record data and performed analysis.  High and Low of the range would be common data fields that go along with indicated value.
		/// </summary>
        [XmlEnum("IndicatedValue")]
        IndicatedValue,

		/// <summary>
		/// This term must be associated with an AVM that allows for end user to make adjustments.
		/// </summary>
        [XmlEnum("NetValue")]
        NetValue,

        [XmlEnum("Other")]
        Other,
    }
}
