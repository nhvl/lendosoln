namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum DesktopUnderwriterRecommendationBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("ApproveEligible")]
        ApproveEligible,

        [XmlEnum("ApproveIneligible")]
        ApproveIneligible,

        [XmlEnum("Error")]
        Error,

        [XmlEnum("ExpandedApproval1Eligible")]
        ExpandedApproval1Eligible,

        [XmlEnum("ExpandedApproval1Ineligible")]
        ExpandedApproval1Ineligible,

        [XmlEnum("ExpandedApproval2Eligible")]
        ExpandedApproval2Eligible,

        [XmlEnum("ExpandedApproval2Ineligible")]
        ExpandedApproval2Ineligible,

        [XmlEnum("ExpandedApproval3Eligible")]
        ExpandedApproval3Eligible,

        [XmlEnum("ExpandedApproval3Ineligible")]
        ExpandedApproval3Ineligible,

        [XmlEnum("OutOfScope")]
        OutOfScope,

        [XmlEnum("ReferEligible")]
        ReferEligible,

        [XmlEnum("ReferIneligible")]
        ReferIneligible,

        [XmlEnum("ReferWithCaution4")]
        ReferWithCaution4,

        [XmlEnum("Unknown")]
        Unknown,

    }
}
