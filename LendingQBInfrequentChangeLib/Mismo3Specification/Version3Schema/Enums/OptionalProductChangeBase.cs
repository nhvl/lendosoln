namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum OptionalProductChangeBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Add")]
        Add,

        [XmlEnum("Cancel")]
        Cancel,

        [XmlEnum("Modify")]
        Modify,

        [XmlEnum("Other")]
        Other,

    }
}
