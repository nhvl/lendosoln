namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Individual
    /// </summary>
    public enum PriorPropertyTitleBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Use this answer for any property owned with any other person including spouse.
		/// </summary>
        [XmlEnum("JointWithOtherThanSpouse")]
        JointWithOtherThanSpouse,

		/// <summary>
		/// Use this answer for any property owned with a spouse.
		/// </summary>
        [XmlEnum("JointWithSpouse")]
        JointWithSpouse,

		/// <summary>
		/// Individual
		/// </summary>
        [XmlEnum("Sole")]
        Sole,

    }
}
