namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Optional product that highlights mismatched addresses, invalid postal codes, SSN mismatches, etc. Trans Union has renamed this product to Trans Union ID Mismatch Alert.
    /// </summary>
    public enum CreditResponseAlertMessageBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Optional product that provides alert messages for possibly fraudulent SSN, addresses and other data.
		/// </summary>
        [XmlEnum("EquifaxSAFESCAN")]
        EquifaxSAFESCAN,

		/// <summary>
		/// Optional product that validates borrowers SSN data.
		/// </summary>
        [XmlEnum("EquifaxSSNVerified")]
        EquifaxSSNVerified,

		/// <summary>
		/// Optional product that generates alert messages for possibly fraudulent SSNs, addresses and other data.
		/// </summary>
        [XmlEnum("ExperianFACSPlus")]
        ExperianFACSPlus,

		/// <summary>
		/// Optional product that generates alert messages for possibly fraudulent SSNs, addresses and other data. This product was formerly called Experian FACS Plus.
		/// </summary>
        [XmlEnum("ExperianFraudShield")]
        ExperianFraudShield,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Optional product that matches addresses, SSNs, and phone numbers against national fraud database. Trans Union has renamed this product to Trans Union High Risk Fraud Alert.
		/// </summary>
        [XmlEnum("TransUnionHAWKAlert")]
        TransUnionHAWKAlert,

		/// <summary>
		/// New name for the Trans Union HAWK Alert product, which matches addresses, SSNs and phone numbers against national fraud database.
		/// </summary>
        [XmlEnum("TransUnionHighRiskFraudAlert")]
        TransUnionHighRiskFraudAlert,

		/// <summary>
		/// New name for the Trans Union Trans Alert product, which highlights mismatched addresses, invalid postal codes, SSN mismatches, etc.
		/// </summary>
        [XmlEnum("TransUnionIdentifierMismatchAlert")]
        TransUnionIdentifierMismatchAlert,

		/// <summary>
		/// Optional product that highlights mismatched addresses, invalid postal codes, SSN mismatches, etc. Trans Union has renamed this product to Trans Union ID Mismatch Alert.
		/// </summary>
        [XmlEnum("TransUnionTransAlert")]
        TransUnionTransAlert,

    }
}
