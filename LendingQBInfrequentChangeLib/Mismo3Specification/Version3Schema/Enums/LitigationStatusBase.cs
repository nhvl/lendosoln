namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// The borrower is a party to  threatened litigation.
    /// </summary>
    public enum LitigationStatusBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Litigation proceedings have been filed with the court of competent jurisdiction.
		/// </summary>
        [XmlEnum("Filed")]
        Filed,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// The borrower is a party to  resolved litigation.
		/// </summary>
        [XmlEnum("Resolved")]
        Resolved,

		/// <summary>
		/// The borrower is a party to  threatened litigation.
		/// </summary>
        [XmlEnum("Threatened")]
        Threatened,

    }
}
