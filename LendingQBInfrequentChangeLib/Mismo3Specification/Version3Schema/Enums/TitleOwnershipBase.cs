namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Ownership will be held by a trust.
    /// </summary>
    public enum TitleOwnershipBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Ownership will be held by a combination of Individuals, Corporations, Partnerships and Trusts.
		/// </summary>
        [XmlEnum("Combination")]
        Combination,

		/// <summary>
		/// Ownership will be held by a corporation.
		/// </summary>
        [XmlEnum("Corporation")]
        Corporation,

        [XmlEnum("Estate")]
        Estate,

        [XmlEnum("GovernmentEntity")]
        GovernmentEntity,

        [XmlEnum("Guardianship")]
        Guardianship,

		/// <summary>
		/// Ownership will be held by one individual.
		/// </summary>
        [XmlEnum("Individual")]
        Individual,

        [XmlEnum("JointVenture")]
        JointVenture,

        [XmlEnum("LimitedLiabilityCompany")]
        LimitedLiabilityCompany,

        [XmlEnum("LimitedPartnership")]
        LimitedPartnership,

        [XmlEnum("NonProfitCorporation")]
        NonProfitCorporation,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Ownership will be held by a partnership.
		/// </summary>
        [XmlEnum("Partnership")]
        Partnership,

		/// <summary>
		/// Ownership will be held by a trust.
		/// </summary>
        [XmlEnum("Trust")]
        Trust,

    }
}
