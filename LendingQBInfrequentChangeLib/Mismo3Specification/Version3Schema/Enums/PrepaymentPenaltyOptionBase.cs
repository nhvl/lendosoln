namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Borrower may sell property without penalty. However, refinance will incur penalty.
    /// </summary>
    public enum PrepaymentPenaltyOptionBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Standard prepay penalty applies in all situations.
		/// </summary>
        [XmlEnum("Hard")]
        Hard,

		/// <summary>
		/// Borrower may sell property without penalty. However, refinance will incur penalty.
		/// </summary>
        [XmlEnum("Soft")]
        Soft,

    }
}
