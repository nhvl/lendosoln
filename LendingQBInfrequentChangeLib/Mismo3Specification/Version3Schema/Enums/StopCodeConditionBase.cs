namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum StopCodeConditionBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("BadCheckHoldingNSF")]
        BadCheckHoldingNSF,

        [XmlEnum("LoanInBankruptcy")]
        LoanInBankruptcy,

        [XmlEnum("LoanInForeclosure")]
        LoanInForeclosure,

        [XmlEnum("LoanPaidInFull")]
        LoanPaidInFull,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("PayoffPending")]
        PayoffPending,

        [XmlEnum("ProcessingException")]
        ProcessingException,

    }
}
