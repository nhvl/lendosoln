namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum UnitChargeUtilityBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Cable")]
        Cable,

        [XmlEnum("Cooling")]
        Cooling,

        [XmlEnum("Electricity")]
        Electricity,

        [XmlEnum("Garbage")]
        Garbage,

        [XmlEnum("Gas")]
        Gas,

        [XmlEnum("Heating")]
        Heating,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Sewer")]
        Sewer,

        [XmlEnum("Water")]
        Water,

    }
}
