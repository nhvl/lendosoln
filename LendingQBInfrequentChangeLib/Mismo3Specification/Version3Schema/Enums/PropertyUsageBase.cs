namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A property occupied by the owner for a portion of the year and is not the primary residence.
    /// </summary>
    public enum PropertyUsageBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// A home owned for the purpose of generating income by renting the property.
		/// </summary>
        [XmlEnum("Investment")]
        Investment,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Residence that the owner physically occupies and uses as his or her principal residence.
		/// </summary>
        [XmlEnum("PrimaryResidence")]
        PrimaryResidence,

		/// <summary>
		/// A property occupied by the owner for a portion of the year and is not the primary residence.
		/// </summary>
        [XmlEnum("SecondHome")]
        SecondHome,

    }
}
