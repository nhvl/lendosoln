namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Limited modifications to meet market expectations but no structural modifications.
    /// </summary>
    public enum ComponentStatusBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Little or no material change observed.
		/// </summary>
        [XmlEnum("NotUpdated")]
        NotUpdated,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Modifications and improvements with minor structural changes with limited impact on utility and appeal.
		/// </summary>
        [XmlEnum("Remodeled")]
        Remodeled,

		/// <summary>
		/// Significant finish and/or structural changes have been made that increase utility and appeal through complete replacement and/or expansion.
		/// </summary>
        [XmlEnum("Renovated")]
        Renovated,

		/// <summary>
		/// A single component which as been replaced.
		/// </summary>
        [XmlEnum("Replaced")]
        Replaced,

		/// <summary>
		/// Limited modifications to meet market expectations but no structural modifications.
		/// </summary>
        [XmlEnum("Updated")]
        Updated,

    }
}
