namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// The note amount.
    /// </summary>
    public enum PrepaymentPenaltyPrincipalBalanceBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// The unpaid principal balance at the time the prepayment penalty is triggered.
		/// </summary>
        [XmlEnum("CurrentPrincipalBalance")]
        CurrentPrincipalBalance,

		/// <summary>
		/// The difference between the scheduled amortized principal balance and the principal balance after curtailment.
		/// </summary>
        [XmlEnum("ExcessPrincipal")]
        ExcessPrincipal,

		/// <summary>
		/// The note amount.
		/// </summary>
        [XmlEnum("OriginalPrincipalBalance")]
        OriginalPrincipalBalance,

        [XmlEnum("Other")]
        Other,

    }
}
