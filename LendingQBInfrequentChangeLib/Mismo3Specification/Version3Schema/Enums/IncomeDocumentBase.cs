namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// This could include items such as an award letter from Social Security, Pension Statement or IRS 1099
    /// </summary>
    public enum IncomeDocumentBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Confirms the wage income and taxes withheld for a person.
		/// </summary>
        [XmlEnum("IRSW2")]
        IRSW2,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Describes income for a particular pay period.
		/// </summary>
        [XmlEnum("PayStub")]
        PayStub,

		/// <summary>
		/// Evaluates the rental potential of a property and establishes its value.
		/// </summary>
        [XmlEnum("RentalIncomeAnalysisStatement")]
        RentalIncomeAnalysisStatement,

		/// <summary>
		/// Definition: Evidences transactions recorded and account balances for a restricted individual investment account during a specified period.
		/// </summary>
        [XmlEnum("RetirementAccountStatement")]
        RetirementAccountStatement,

		/// <summary>
		/// Provides information used to calculate income or other taxes.
		/// </summary>
        [XmlEnum("TaxReturn")]
        TaxReturn,

		/// <summary>
		/// Used when a third party employment verification vendor provides verification of employment.
		/// </summary>
        [XmlEnum("ThirdPartyEmploymentStatement")]
        ThirdPartyEmploymentStatement,

		/// <summary>
		///  When verbal verification is allowed. This may be a written statement by the lender that information was collected verbally. Might include the name of the person making the contact, the name of the entity contacted, the name and title of the individual at the entity who provided the information, date of the contact and information that was collected.
		/// </summary>
        [XmlEnum("VerbalStatement")]
        VerbalStatement,

		/// <summary>
		/// This could include items such as an award letter from Social Security, Pension Statement or IRS 1099
		/// </summary>
        [XmlEnum("VerificationOfIncome")]
        VerificationOfIncome,

    }
}
