namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Wage and Tax Statement
    /// </summary>
    public enum IRSDocumentBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// U.S. Individual Income Tax Return
		/// </summary>
        [XmlEnum("IRS1040")]
        IRS1040,

		/// <summary>
		/// U.S. Return of Partnership Income
		/// </summary>
        [XmlEnum("IRS1065")]
        IRS1065,

		/// <summary>
		/// Mortgage Interest Statement
		/// </summary>
        [XmlEnum("IRS1098")]
        IRS1098,

		/// <summary>
		/// Information Return (AKA All Forms)
		/// </summary>
        [XmlEnum("IRS1099")]
        IRS1099,

		/// <summary>
		/// U.S. Corporation Income Tax Return
		/// </summary>
        [XmlEnum("IRS1120")]
        IRS1120,

		/// <summary>
		/// U.S. Income Tax Return for Homeowners Associations
		/// </summary>
        [XmlEnum("IRS1120H")]
        IRS1120H,

		/// <summary>
		/// U.S. Life Insurance Company Income Tax Return
		/// </summary>
        [XmlEnum("IRS1120L")]
        IRS1120L,

		/// <summary>
		/// U.S. Income Tax Return for an S Corporation
		/// </summary>
        [XmlEnum("IRS1120S")]
        IRS1120S,

		/// <summary>
		/// IRA Contribution Information
		/// </summary>
        [XmlEnum("IRS5498")]
        IRS5498,

		/// <summary>
		/// Wage and Tax Statement
		/// </summary>
        [XmlEnum("IRSW2")]
        IRSW2,

        [XmlEnum("Other")]
        Other,

    }
}
