namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum PrivatelyCreatedServitudeBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("DeedRestriction")]
        DeedRestriction,

        [XmlEnum("Easement")]
        Easement,

        [XmlEnum("License")]
        License,

        [XmlEnum("Other")]
        Other,

    }
}
