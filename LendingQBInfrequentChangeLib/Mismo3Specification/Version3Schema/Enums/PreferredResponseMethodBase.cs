namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Supply a mailbox ID
    /// </summary>
    public enum PreferredResponseMethodBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Supply a telephone number
		/// </summary>
        [XmlEnum("Fax")]
        Fax,

		/// <summary>
		/// Supply a path and file name
		/// </summary>
        [XmlEnum("File")]
        File,

		/// <summary>
		/// Supply a URL
		/// </summary>
        [XmlEnum("FTP")]
        FTP,

		/// <summary>
		/// Supply a URL
		/// </summary>
        [XmlEnum("HTTP")]
        HTTP,

		/// <summary>
		/// Supply a secure URL.
		/// </summary>
        [XmlEnum("HTTPS")]
        HTTPS,

		/// <summary>
		/// Supply a mailing address
		/// </summary>
        [XmlEnum("Mail")]
        Mail,

		/// <summary>
		/// Supply a queue name
		/// </summary>
        [XmlEnum("MessageQueue")]
        MessageQueue,

		/// <summary>
		/// Specify in Delivery Method Other element
		/// </summary>
        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Supply an email address.
		/// </summary>
        [XmlEnum("SMTP")]
        SMTP,

		/// <summary>
		/// Supply a mailbox ID
		/// </summary>
        [XmlEnum("VAN")]
        VAN,

    }
}
