namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum HeatingFuelBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Coal")]
        Coal,

        [XmlEnum("Electric")]
        Electric,

        [XmlEnum("Gas")]
        Gas,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Solar")]
        Solar,

        [XmlEnum("Wood")]
        Wood,

    }
}
