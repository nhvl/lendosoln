namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum InterestRateLifetimeAdjustmentCeilingBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("AbsoluteRequiredCeiling")]
        AbsoluteRequiredCeiling,

        [XmlEnum("FactorAddedToTheGreaterOfTheNetCommitmentYieldOrTheOriginalNoteRate")]
        FactorAddedToTheGreaterOfTheNetCommitmentYieldOrTheOriginalNoteRate,

        [XmlEnum("FactorAddedToTheNetCommitmentYield")]
        FactorAddedToTheNetCommitmentYield,

        [XmlEnum("FactorAddedToTheOriginalNoteRate")]
        FactorAddedToTheOriginalNoteRate,

        [XmlEnum("NotApplicable")]
        NotApplicable,

        [XmlEnum("Other")]
        Other,

    }
}
