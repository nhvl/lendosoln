namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// The quote amount is estimated because not enough data has been sent in order to guarantee the quoted price.
    /// </summary>
    public enum RateQuoteBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Complete data set required by the provider in order to provide a rate quote.
		/// </summary>
        [XmlEnum("Detail")]
        Detail,

		/// <summary>
		/// The quote amount is estimated because not enough data has been sent in order to guarantee the quoted price.
		/// </summary>
        [XmlEnum("Estimated")]
        Estimated,

        [XmlEnum("Other")]
        Other,

    }
}
