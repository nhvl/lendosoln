namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum LockStatusBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("CanceledOrWithdrawn")]
        CanceledOrWithdrawn,

        [XmlEnum("Expired")]
        Expired,

        [XmlEnum("Locked")]
        Locked,

        [XmlEnum("ModificationRequested")]
        ModificationRequested,

        [XmlEnum("Modified")]
        Modified,

        [XmlEnum("None")]
        None,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Pending")]
        Pending,

        [XmlEnum("Rejected")]
        Rejected,

        [XmlEnum("Requested")]
        Requested,

    }
}
