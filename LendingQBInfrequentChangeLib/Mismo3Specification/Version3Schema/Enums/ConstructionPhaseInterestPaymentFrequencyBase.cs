namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum ConstructionPhaseInterestPaymentFrequencyBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Biweekly")]
        Biweekly,

        [XmlEnum("Monthly")]
        Monthly,

        [XmlEnum("Other")]
        Other,

    }
}
