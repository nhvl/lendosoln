namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum HardshipDurationBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("LongTerm")]
        LongTerm,

        [XmlEnum("MediumTerm")]
        MediumTerm,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Permanent")]
        Permanent,

        [XmlEnum("ShortTerm")]
        ShortTerm,

        [XmlEnum("Unknown")]
        Unknown,

    }
}
