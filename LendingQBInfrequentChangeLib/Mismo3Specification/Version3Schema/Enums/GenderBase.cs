namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Used for loans where the borrower is an institution, corporation, or partnership.
    /// </summary>
    public enum GenderBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Female")]
        Female,

		/// <summary>
		/// Information was not provided by borrower in a mail, telephone, or internet application. Category as defined by the Home Mortgage Disclosure Act.
		/// </summary>
        [XmlEnum("InformationNotProvidedUnknown")]
        InformationNotProvidedUnknown,

        [XmlEnum("Male")]
        Male,

		/// <summary>
		/// Used for loans where the borrower is an institution, corporation, or partnership.
		/// </summary>
        [XmlEnum("NotApplicable")]
        NotApplicable,

    }
}
