namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum AtticFeatureBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("DropStair")]
        DropStair,

        [XmlEnum("Finished")]
        Finished,

        [XmlEnum("Floor")]
        Floor,

        [XmlEnum("Heated")]
        Heated,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Scuttle")]
        Scuttle,

        [XmlEnum("Stairs")]
        Stairs,
    }
}
