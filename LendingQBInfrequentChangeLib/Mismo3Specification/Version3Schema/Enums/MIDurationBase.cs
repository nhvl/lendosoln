namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum MIDurationBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Annual")]
        Annual,

        [XmlEnum("NotApplicable")]
        NotApplicable,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("PeriodicMonthly")]
        PeriodicMonthly,

        [XmlEnum("SingleLifeOfLoan")]
        SingleLifeOfLoan,

        [XmlEnum("SingleSpecific")]
        SingleSpecific,

        [XmlEnum("SplitPremium")]
        SplitPremium,

    }
}
