namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum LoanProductStatusBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Available")]
        Available,

        [XmlEnum("Eligible")]
        Eligible,

        [XmlEnum("Ineligible")]
        Ineligible,

        [XmlEnum("Offered")]
        Offered,

        [XmlEnum("Requested")]
        Requested,

        [XmlEnum("Selected")]
        Selected,

    }
}
