namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Information on the loan reported by the servicer after liquidation.
    /// </summary>
    public enum InvestorReportingActionBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// The loan is considered current.
		/// </summary>
        [XmlEnum("Active")]
        Active,

		/// <summary>
		/// The loan as been assigned to the FHA or VA.
		/// </summary>
        [XmlEnum("AssignedToFHAOrVA")]
        AssignedToFHAOrVA,

        [XmlEnum("DeedInLieuLiquidated")]
        DeedInLieuLiquidated,

		/// <summary>
		/// A change in loan status due to a borrower request to exercise the skip payment feature.
		/// </summary>
        [XmlEnum("ExerciseSkipPay")]
        ExerciseSkipPay,

		/// <summary>
		/// A change in loan status due to the foreclosure sale of the borrowers property to pay off a defaulted loan. The property is currently held for sale.
		/// </summary>
        [XmlEnum("ForeclosureLiquidatedHeldForSale")]
        ForeclosureLiquidatedHeldForSale,

		/// <summary>
		/// A change in loan status due to the foreclosure sale of the borrowers property to pay off a defaulted loan. The property is awaiting conveyance to purchaser.
		/// </summary>
        [XmlEnum("ForeclosureLiquidatedPendingConveyance")]
        ForeclosureLiquidatedPendingConveyance,

		/// <summary>
		/// A change in loan status due to the foreclosure sale of the borrowers property to pay off a defaulted loan. Property has been sold and conveyed to someone other than the mortgagee or mortgagor.
		/// </summary>
        [XmlEnum("ForeclosureLiquidatedThirdPartySale")]
        ForeclosureLiquidatedThirdPartySale,

        [XmlEnum("ForeclosureSaleUnsuccessful")]
        ForeclosureSaleUnsuccessful,

        [XmlEnum("Inactive")]
        Inactive,

        [XmlEnum("LiquidatedHeldForSale")]
        LiquidatedHeldForSale,

		/// <summary>
		/// A change in loan status due to automatic mortgage insurance cancellation provisions.
		/// </summary>
        [XmlEnum("MICancellationAutomaticTermination")]
        MICancellationAutomaticTermination,

		/// <summary>
		/// A change in loan status due to mortgage insurance cancellation based on current property value.
		/// </summary>
        [XmlEnum("MICancellationBasedOnCurrentPropertyValue")]
        MICancellationBasedOnCurrentPropertyValue,

		/// <summary>
		/// A change in loan status due to mandatory mortgage insurance cancellation.
		/// </summary>
        [XmlEnum("MICancellationBasedOnMandatoryTermination")]
        MICancellationBasedOnMandatoryTermination,

		/// <summary>
		/// A change in loan status due to mortgage insurance cancellation based on original property value.
		/// </summary>
        [XmlEnum("MICancellationBasedOnOriginalPropertyValue")]
        MICancellationBasedOnOriginalPropertyValue,

		/// <summary>
		/// Mortgage insurance is rescinded or canceled by the insurer.
		/// </summary>
        [XmlEnum("MIRescindedByInsurer")]
        MIRescindedByInsurer,

		/// <summary>
		/// Current or delinquent without servicer action
		/// </summary>
        [XmlEnum("NoServicerActionTaken")]
        NoServicerActionTaken,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// The loan is paid in full at the loans maturity date where the last payment has been received.
		/// </summary>
        [XmlEnum("PaidInFullAtMaturity")]
        PaidInFullAtMaturity,

		/// <summary>
		/// The loan is paid in full and the action date is prior to the loans maturity date.
		/// </summary>
        [XmlEnum("PaidInFullPriorToMaturity")]
        PaidInFullPriorToMaturity,

		/// <summary>
		/// All outstanding balances on the loan have been paid in full.
		/// </summary>
        [XmlEnum("Payoff")]
        Payoff,

		/// <summary>
		/// The sale of the mortgaged premises for less than the total amount necessary to satisfy the outstanding debt.
		/// </summary>
        [XmlEnum("PayoffShortSale")]
        PayoffShortSale,

        [XmlEnum("PrincipalBalanceCorrection")]
        PrincipalBalanceCorrection,

		/// <summary>
		/// A repurchase due to the fact that the loan does not meet or no longer meets the contract parameters (a quality control issue).
		/// </summary>
        [XmlEnum("Repurchase")]
        Repurchase,

		/// <summary>
		/// A repurchase due to the fact that the loan characteristics are being modified so that the loan no longer meets the ARM contract parameters. Loan is often repurchased by the Investor after the modification under a different contract.
		/// </summary>
        [XmlEnum("RepurchaseDueToARMConversionToFixedRate")]
        RepurchaseDueToARMConversionToFixedRate,

		/// <summary>
		/// A repurchase due to the fact that the loan characteristics are being modified so that the loan no longer meets the current ARM contract parameters and is being modified into a new ARM loan.
		/// </summary>
        [XmlEnum("RepurchaseDueToModifiedARM")]
        RepurchaseDueToModifiedARM,

		/// <summary>
		/// Information on the loan reported by the servicer after liquidation.
		/// </summary>
        [XmlEnum("Supplemental")]
        Supplemental,

        [XmlEnum("TransferToREO")]
        TransferToREO,

    }
}
