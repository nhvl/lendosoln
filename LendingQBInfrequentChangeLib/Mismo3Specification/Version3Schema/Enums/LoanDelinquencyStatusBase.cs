namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// There is a workout being pursued on the loan that will help bring the loan current or liquidate the loan to avoid foreclosure.
    /// </summary>
    public enum LoanDelinquencyStatusBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// An FHA-insured mortgage that qualifies for HUD special assignment procedures is in the process of being assigned to HUD.
		/// </summary>
        [XmlEnum("AssignmentPursued")]
        AssignmentPursued,

		/// <summary>
		/// The Servicer is working with the borrower to sell the property by permitting the purchaser to pay the delinquent installments and assume the outstanding debt.
		/// </summary>
        [XmlEnum("Assumption")]
        Assumption,

		/// <summary>
		/// The borrower has filed for bankruptcy under Chapter 11 of the Federal Bankruptcy Act.
		/// </summary>
        [XmlEnum("ChapterElevenBankruptcy")]
        ChapterElevenBankruptcy,

		/// <summary>
		/// The borrower has filed for bankruptcy under Chapter 7 of the Federal Bankruptcy Act.
		/// </summary>
        [XmlEnum("ChapterSevenBankruptcy")]
        ChapterSevenBankruptcy,

		/// <summary>
		/// The borrower has filed for bankruptcy under Chapter 13 of the Federal Bankruptcy Act.
		/// </summary>
        [XmlEnum("ChapterThirteenBankruptcy")]
        ChapterThirteenBankruptcy,

		/// <summary>
		/// The borrower has filed for bankruptcy under Chapter 12 of the Federal Bankruptcy Act. A Chapter 12 bankruptcy is a bankruptcy action that involves the reorganization of a family farming business.
		/// </summary>
        [XmlEnum("ChapterTwelveBankruptcy")]
        ChapterTwelveBankruptcy,

		/// <summary>
		/// The investor has agreed not to pursue collection efforts or legal actions against the borrower because of a reduced value for the property, a low outstanding mortgage balance, or the presence of environmental hazards on the property.
		/// </summary>
        [XmlEnum("ChargeOffPursued")]
        ChargeOffPursued,

		/// <summary>
		/// Borrower has contested the  legal action of the servicer or there is litigation pending or filed that adversely affects the property. For example, a borrower or other defendant interposes an answer to an action, or files a countersuit.
		/// </summary>
        [XmlEnum("ContestedForeclosure")]
        ContestedForeclosure,

		/// <summary>
		/// The loan has been made current and is no longer delinquent.
		/// </summary>
        [XmlEnum("Current")]
        Current,

		/// <summary>
		/// Considered delinquent based on investor guidelines, but the servicer has not taken legal action.
		/// </summary>
        [XmlEnum("DelinquentNoAction")]
        DelinquentNoAction,

		/// <summary>
		/// The Department of Justice or a state/local agency has decided to seize (or has seized) a property under the forfeiture provision of the Controlled Substances Act.
		/// </summary>
        [XmlEnum("DrugSeizure")]
        DrugSeizure,

		/// <summary>
		/// The Servicer has authorized a temporary suspension of payments or a repayment plan that calls for periodic payments of less than the normal monthly payment, or periodic payments at different intervals, to give the borrower additional time to bring the mortgage current.
		/// </summary>
        [XmlEnum("Forbearance")]
        Forbearance,

		/// <summary>
		/// The Servicer has referred the case to an attorney to take legal action to acquire the property through a foreclosure sale.
		/// </summary>
        [XmlEnum("Foreclosure")]
        Foreclosure,

		/// <summary>
		/// An action by a local agency to seize or initiate forfeiture proceedings against a property.
		/// </summary>
        [XmlEnum("GovernmentSeizure")]
        GovernmentSeizure,

		/// <summary>
		/// The mortgage loan has been referred to mediation.
		/// </summary>
        [XmlEnum("MediationReferral")]
        MediationReferral,

		/// <summary>
		/// The Servicer has granted a delinquent service member forbearance or a stay in foreclosure proceedings under the provisions of the Soldiers and Sailors Civil Relief Act.
		/// </summary>
        [XmlEnum("MilitaryIndulgence")]
        MilitaryIndulgence,

		/// <summary>
		/// The Servicer is working with the borrower to renegotiate the terms of the mortgage.
		/// </summary>
        [XmlEnum("Modification")]
        Modification,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// The Servicer plans to pursue a preforeclosure sale with a payoff of less than the full amount of indebtedness.
		/// </summary>
        [XmlEnum("PreforeclosureSalePlanned")]
        PreforeclosureSalePlanned,

		/// <summary>
		/// The Servicer cannot pursue (or complete) foreclosure action because proceedings required to verify a will of a deceased mortgagor are in process.
		/// </summary>
        [XmlEnum("Probate")]
        Probate,

		/// <summary>
		/// The Servicer is pursuing a new loan in which the existing first mortgage is paid off with the proceeds of the new mortgage.
		/// </summary>
        [XmlEnum("Refinance")]
        Refinance,

		/// <summary>
		/// A written or verbal agreement between the Servicer and the borrower that gives the borrower a defined period of time to reinstate the mortgage by making payments in excess of the monthly payment.
		/// </summary>
        [XmlEnum("RepaymentPlan")]
        RepaymentPlan,

		/// <summary>
		/// The Servicer is evaluating the pros and cons of pursuing foreclosure or recommending that the debt be charged off on a second mortgage.
		/// </summary>
        [XmlEnum("SecondLienConsiderations")]
        SecondLienConsiderations,

		/// <summary>
		/// The Servicer is working with the borrower to pursue a short sale.
		/// </summary>
        [XmlEnum("ShortSalePlanning")]
        ShortSalePlanning,

		/// <summary>
		/// The borrower has been approved to participate in a modification that requires a trial period plan and is current on their trial payments.
		/// </summary>
        [XmlEnum("TrialModificationActive")]
        TrialModificationActive,

		/// <summary>
		/// The investor has agreed to make a cash contribution to reduce the outstanding indebtedness of a VA-guaranteed mortgage, for which the Department of Veterans Affairs failed to establish an "upset price" bid for the foreclosure sale, in order to get the VA to reconsider its decision. Servicer has completed processing the Buydown.
		/// </summary>
        [XmlEnum("VeteransAffairsBuydown")]
        VeteransAffairsBuydown,

		/// <summary>
		/// The Department of Veterans Affairs refused to establish an "upset price" to be bid at the foreclosure sale of a VA-guaranteed mortgage that the Servicer had referred for foreclosure. Servicer is pursuing a buydown, charge-off or other alternative.
		/// </summary>
        [XmlEnum("VeteransAffairsNoBids")]
        VeteransAffairsNoBids,

		/// <summary>
		/// The Department of Veterans Affairs has requested information about a VA-guaranteed mortgage the Servicer had referred for foreclosure in order to decide whether to accept an assignment and refund the mortgage.
		/// </summary>
        [XmlEnum("VeteransAffairsRefundPursued")]
        VeteransAffairsRefundPursued,

		/// <summary>
		/// There is a workout being pursued on the loan that will help bring the loan current or liquidate the loan to avoid foreclosure.
		/// </summary>
        [XmlEnum("Workout")]
        Workout,

    }
}
