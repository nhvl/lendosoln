namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Amount paid or payable to borrower from Workers Compensation Insurance program, usually for injuries that prevent borrower from working. Collected on the URLA in Section V (Monthly Income).
    /// </summary>
    public enum IncomeBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Alimony (also called maintenance or spousal support) is financial support to a spouse from the other spouse after marital separation or from the ex-spouse upon divorce.
		/// </summary>
        [XmlEnum("Alimony")]
        Alimony,

        [XmlEnum("AutomobileExpenseAccount")]
        AutomobileExpenseAccount,

		/// <summary>
		/// For salaried positions, the monthly amount of salary (annual salary/12). For hourly positions, the income that would arise from 52 weeks of work at the usual and customary number of non-overtime hours per week /12.
		/// </summary>
        [XmlEnum("Base")]
        Base,

        [XmlEnum("BoarderIncome")]
        BoarderIncome,

		/// <summary>
		/// Income derived from optional compensation paid out by employer, linked to employers overall performance and/or individual performance. Collected on the URLA in Section V (Monthly Income - Bonuses).
		/// </summary>
        [XmlEnum("Bonus")]
        Bonus,

		/// <summary>
		/// The borrower monthly income amount used by the lender to qualify the borrower for the loan.
		/// </summary>
        [XmlEnum("BorrowerEstimatedTotalMonthlyIncome")]
        BorrowerEstimatedTotalMonthlyIncome,

		/// <summary>
		/// Child support (or child maintenance) is an ongoing, periodic payment made by a parent for the financial benefit of a child following the end of a marriage or other relationship.
		/// </summary>
        [XmlEnum("ChildSupport")]
        ChildSupport,

		/// <summary>
		/// Borrower income that is based upon a percentage of the goods or services the borrower sells (e.g., gross Realtor commission = 6%) Collected on the URLA in Section V (Monthly Income - Commissions).
		/// </summary>
        [XmlEnum("Commissions")]
        Commissions,

		/// <summary>
		/// Income received on a contract or per job basis. Recipients are classified by employers as independent contractors and not considered employees.
		/// </summary>
        [XmlEnum("ContractBasis")]
        ContractBasis,

		/// <summary>
		/// A plan which provides each participant an individual account for benefits based solely on the amount contributed to the participant's account as well as any income, expenses, gains and losses. Typical examples are Individual Retirement Account (IRA) and 401k.
		/// </summary>
        [XmlEnum("DefinedContributionPlan")]
        DefinedContributionPlan,

		/// <summary>
		/// Supplementary income from disability coverage for a condition that prevents a person from working.
		/// </summary>
        [XmlEnum("Disability")]
        Disability,

		/// <summary>
		/// Funds paid to owners of stocks or bonds by issuer of stock or bond. Collected on the URLA in Section V (Monthly Income - Dividends / Interest).
		/// </summary>
        [XmlEnum("DividendsInterest")]
        DividendsInterest,

        [XmlEnum("FosterCare")]
        FosterCare,

		/// <summary>
		/// Compensation for basic living expenses for certain employment situations.
		/// </summary>
        [XmlEnum("HousingAllowance")]
        HousingAllowance,

        [XmlEnum("MilitaryBasePay")]
        MilitaryBasePay,

        [XmlEnum("MilitaryClothesAllowance")]
        MilitaryClothesAllowance,

        [XmlEnum("MilitaryCombatPay")]
        MilitaryCombatPay,

        [XmlEnum("MilitaryFlightPay")]
        MilitaryFlightPay,

        [XmlEnum("MilitaryHazardPay")]
        MilitaryHazardPay,

        [XmlEnum("MilitaryOverseasPay")]
        MilitaryOverseasPay,

		/// <summary>
		/// Extra income paid to military people that have professional degrees, such as military doctors, dentists, lawyers, etc
		/// </summary>
        [XmlEnum("MilitaryPropPay")]
        MilitaryPropPay,

        [XmlEnum("MilitaryQuartersAllowance")]
        MilitaryQuartersAllowance,

        [XmlEnum("MilitaryRationsAllowance")]
        MilitaryRationsAllowance,

        [XmlEnum("MilitaryVariableHousingAllowance")]
        MilitaryVariableHousingAllowance,

		/// <summary>
		/// Total amount of income not otherwise reported separately.
		/// </summary>
        [XmlEnum("MiscellaneousIncome")]
        MiscellaneousIncome,

		/// <summary>
		/// An annual tax credit granted by government jurisdictions to help first-time homebuyers offset a portion of the interest on a new mortgage. Mortgage lenders often view the estimated amount of the credit as additional monthly income to help the potential borrower qualify for the loan.
		/// </summary>
        [XmlEnum("MortgageCreditCertificate")]
        MortgageCreditCertificate,

		/// <summary>
		/// Income used when an employer subsidizes and employees mortgage payments by paying all or part of the interest differential between the employees present and proposed mortgage payments.
		/// </summary>
        [XmlEnum("MortgageDifferential")]
        MortgageDifferential,

        [XmlEnum("NetRentalIncome")]
        NetRentalIncome,

		/// <summary>
		/// Income from a party who occupies the home but is not a borrower on the loan. The party has consented to have their income included in a review for a modification. May be used as part of a HAMP modification.
		/// </summary>
        [XmlEnum("NonBorrowerContribution")]
        NonBorrowerContribution,

        [XmlEnum("NotesReceivableInstallment")]
        NotesReceivableInstallment,

		/// <summary>
		/// Used in conjunction with a textual description of other types of income. Collected on the URLA in Section V (Monthly Income - Other).
		/// </summary>
        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Additional hourly compensation paid for work beyond the usual and customary work period. Collected on the URLA in Section V (Monthly Income - Overtime).
		/// </summary>
        [XmlEnum("Overtime")]
        Overtime,

		/// <summary>
		/// Periodic amount paid by employer to persons who have retired from employer after attaining necessary term of service and other attributes. Collected on the URLA in Section V (Monthly Income).
		/// </summary>
        [XmlEnum("Pension")]
        Pension,

		/// <summary>
		/// The anticipated rent on the subject property if it is an investment property or if a 2-4 property where other units are rented.
		/// </summary>
        [XmlEnum("ProposedGrossRentForSubjectProperty")]
        ProposedGrossRentForSubjectProperty,

		/// <summary>
		/// Amount paid to borrower by federal, state or local public assistance program
		/// </summary>
        [XmlEnum("PublicAssistance")]
        PublicAssistance,

		/// <summary>
		/// Periodic gross rents (before expenses) arising from a piece of real estate owned (or to be owned) by borrower.
		/// </summary>
        [XmlEnum("RealEstateOwnedGrossRentalIncome")]
        RealEstateOwnedGrossRentalIncome,

		/// <summary>
		/// In the US, monthly amount paid to recipients of OASI (Old Age and Survivors Insurance) commonly known as Social Security. Collected on the URLA in Section V (Monthly Income).
		/// </summary>
        [XmlEnum("SocialSecurity")]
        SocialSecurity,

        [XmlEnum("SubjectPropertyNetCashFlow")]
        SubjectPropertyNetCashFlow,

        [XmlEnum("TrailingCoBorrowerIncome")]
        TrailingCoBorrowerIncome,

        [XmlEnum("Trust")]
        Trust,

		/// <summary>
		/// Amount paid or payable to borrower from state or Federal unemployment insurance program.
		/// </summary>
        [XmlEnum("Unemployment")]
        Unemployment,

		/// <summary>
		/// Dollar amount of VA Benefits excluding education benefits available to a veteran borrower. Collected on the URLA in Section V (Monthly Income).
		/// </summary>
        [XmlEnum("VABenefitsNonEducational")]
        VABenefitsNonEducational,

		/// <summary>
		/// Amount paid or payable to borrower from Workers Compensation Insurance program, usually for injuries that prevent borrower from working. Collected on the URLA in Section V (Monthly Income).
		/// </summary>
        [XmlEnum("WorkersCompensation")]
        WorkersCompensation,

    }
}
