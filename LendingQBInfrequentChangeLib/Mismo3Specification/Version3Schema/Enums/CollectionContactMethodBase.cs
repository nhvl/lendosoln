namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum CollectionContactMethodBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Email")]
        Email,

        [XmlEnum("FormLetter")]
        FormLetter,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("PhoneCall")]
        PhoneCall,

    }
}
