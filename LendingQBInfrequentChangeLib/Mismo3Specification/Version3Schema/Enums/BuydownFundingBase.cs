namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Buydown funds paid with cash or cash equivalent at closing.
    /// </summary>
    public enum BuydownFundingBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Buydown funds added to the note amount. 
		/// </summary>
        [XmlEnum("Financed")]
        Financed,

		/// <summary>
		/// Buydown funded through increase to the note rate.
		/// </summary>
        [XmlEnum("IncreaseInInterestRate")]
        IncreaseInInterestRate,

		/// <summary>
		/// Buydown funds paid with cash or cash equivalent at closing.
		/// </summary>
        [XmlEnum("LumpSumCash")]
        LumpSumCash,

        [XmlEnum("Other")]
        Other,
    }
}
