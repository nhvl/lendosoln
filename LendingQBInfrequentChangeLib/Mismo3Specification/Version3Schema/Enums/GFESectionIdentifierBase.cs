namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum GFESectionIdentifierBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("A")]
        A,

        [XmlEnum("AB")]
        AB,

        [XmlEnum("B")]
        B,

        [XmlEnum("Eight")]
        Eight,

        [XmlEnum("Eleven")]
        Eleven,

        [XmlEnum("Five")]
        Five,

        [XmlEnum("Four")]
        Four,

        [XmlEnum("Nine")]
        Nine,

        [XmlEnum("One")]
        One,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Seven")]
        Seven,

        [XmlEnum("Six")]
        Six,

        [XmlEnum("Ten")]
        Ten,

        [XmlEnum("Three")]
        Three,

        [XmlEnum("Two")]
        Two,

    }
}
