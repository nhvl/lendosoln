﻿namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    public enum HMDARaceDesignationBase
    {
        [XmlEnum(nameof(NativeHawaiian))]
        NativeHawaiian,

        [XmlEnum(nameof(GuamanianOrChamorro))]
        GuamanianOrChamorro,

        [XmlEnum(nameof(Samoan))]
        Samoan,

        [XmlEnum(nameof(AsianIndian))]
        AsianIndian,

        [XmlEnum(nameof(Chinese))]
        Chinese,

        [XmlEnum(nameof(Filipino))]
        Filipino,

        [XmlEnum(nameof(Japanese))]
        Japanese,

        [XmlEnum(nameof(Korean))]
        Korean,

        [XmlEnum(nameof(Vietnamese))]
        Vietnamese,

        [XmlEnum(nameof(OtherAsian))]
        OtherAsian,
        
        [XmlEnum(nameof(OtherPacificIslander))]
        OtherPacificIslander,
    }
}
