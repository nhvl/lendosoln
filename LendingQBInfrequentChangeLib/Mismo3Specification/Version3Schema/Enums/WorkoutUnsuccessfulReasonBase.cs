namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// The borrower has not been considered for the workout, because the non-income data is incomplete to allow a disposition of the request.
    /// </summary>
    public enum WorkoutUnsuccessfulReasonBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// The borrower is not eligible for the workout considered because the loan is in bankruptcy, and the bankruptcy court declined the workout
		/// </summary>
        [XmlEnum("BankruptcyCourtDeclined")]
        BankruptcyCourtDeclined,

		/// <summary>
		/// The borrower has not been considered for the workout, because the borrower failed to respond to the servicer's solicitation
		/// </summary>
        [XmlEnum("BorrowerFailedToRespondToSolicitation")]
        BorrowerFailedToRespondToSolicitation,

		/// <summary>
		/// The borrower is not sufficiently delinquent to be eligible for the workout considered, and it has been determined that the borrower's default is not imminent.
		/// </summary>
        [XmlEnum("DefaultNotImminent")]
        DefaultNotImminent,

		/// <summary>
		/// The borrower is not eligible for the workout considered, because the amount of principal forbearance that would be required to make the loan eligible exceeds the maximum forbearance permitted for the workout based on trading partner guidelines.
		/// </summary>
        [XmlEnum("ExcessiveForbearance")]
        ExcessiveForbearance,

		/// <summary>
		/// The borrower withdrew from a workout program in order to participate in a workout program within a designated federally declared disaster area.
		/// </summary>
        [XmlEnum("FederallyDeclaredDisasterArea")]
        FederallyDeclaredDisasterArea,

		/// <summary>
		/// The borrower has not been considered for the workout, because the income data is insufficient to render a disposition of the request.
		/// </summary>
        [XmlEnum("IncomeDocumentationNotProvided")]
        IncomeDocumentationNotProvided,

		/// <summary>
		/// The borrower has not been considered for the workout, because the servicer verified the borrower misrepresented income data.
		/// </summary>
        [XmlEnum("IncomeMisrepresented")]
        IncomeMisrepresented,

		/// <summary>
		/// The borrower is not eligible for the workout considered for a reason other than the property eligibility. 
		/// </summary>
        [XmlEnum("IneligibleBorrower")]
        IneligibleBorrower,

		/// <summary>
		/// The borrower is not eligible for the workout considered, because the mortgage loan program was not eligible according to trading partner guidelines.
		/// </summary>
        [XmlEnum("IneligibleMortgage")]
        IneligibleMortgage,

		/// <summary>
		/// The borrower is not eligible for the workout considered because the property is ineligible based on trading partner guidelines. 
		/// </summary>
        [XmlEnum("IneligibleProperty")]
        IneligibleProperty,

		/// <summary>
		/// The borrower is not eligible for the workout considered because the loan's investor will not allow this type of workout or is not participating in the workout program.
		/// </summary>
        [XmlEnum("InvestorGuarantorNotParticipating")]
        InvestorGuarantorNotParticipating,

		/// <summary>
		/// The borrower no longer needs to be considered for a workout, because the loan has been paid in full or brought current.
		/// </summary>
        [XmlEnum("LoanPaidOffOrReinstated")]
        LoanPaidOffOrReinstated,

		/// <summary>
		/// The borrower is not eligible for the workout considered because the value of the proposed modified loan is less than the net present value of the loan without modification, within a certain tolerance.
		/// </summary>
        [XmlEnum("NegativeNPV")]
        NegativeNPV,

		/// <summary>
		/// The borrower did not accept an offered workout.
		/// </summary>
        [XmlEnum("OfferNotAcceptedByBorrower")]
        OfferNotAcceptedByBorrower,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// The borrower is not eligible for the workout considered, because the borrower had a prior permanent HAMP modification.
		/// </summary>
        [XmlEnum("PreviousOfficialHAMPModificationLastTwelveMonths")]
        PreviousOfficialHAMPModificationLastTwelveMonths,

		/// <summary>
		/// The borrower was not eligible for the workout considered, because the workout requires that the borrower occupy the property.
		/// </summary>
        [XmlEnum("PropertyNotOwnerOccupied")]
        PropertyNotOwnerOccupied,

		/// <summary>
		/// The borrower withdrew the request for workout consideration before disposition.
		/// </summary>
        [XmlEnum("RequestWithdrawnByBorrower")]
        RequestWithdrawnByBorrower,

		/// <summary>
		/// The borrower is not eligible for permanent modification, because the provisional (trial) period payments were not made on a timely basis.
		/// </summary>
        [XmlEnum("TrialPlanDefault")]
        TrialPlanDefault,

		/// <summary>
		/// The borrower withdrew from a workout in order to participate in an unemployment forbearance program.
		/// </summary>
        [XmlEnum("UnemploymentForbearanceProgram")]
        UnemploymentForbearanceProgram,

		/// <summary>
		/// The borrower has not been considered for the workout, because the non-income data is incomplete to allow a disposition of the request.
		/// </summary>
        [XmlEnum("WorkoutRequestIncomplete")]
        WorkoutRequestIncomplete,

    }
}
