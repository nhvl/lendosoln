namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum MIPolicyStatusBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("ActiveApplication")]
        ActiveApplication,

        [XmlEnum("ActiveCertificate")]
        ActiveCertificate,

        [XmlEnum("ActiveCommitment")]
        ActiveCommitment,

        [XmlEnum("InactiveApplication")]
        InactiveApplication,

        [XmlEnum("InactiveCertificate")]
        InactiveCertificate,

        [XmlEnum("InactiveCommitment")]
        InactiveCommitment,

        [XmlEnum("Other")]
        Other,

    }
}
