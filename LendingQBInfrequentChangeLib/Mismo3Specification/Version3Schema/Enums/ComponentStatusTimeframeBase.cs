namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum ComponentStatusTimeframeBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("ElevenToFifteenYears")]
        ElevenToFifteenYears,

        [XmlEnum("LessThanOneYear")]
        LessThanOneYear,

        [XmlEnum("OneToFiveYears")]
        OneToFiveYears,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("SixToTenYears")]
        SixToTenYears,

        [XmlEnum("Unknown")]
        Unknown,

    }
}
