namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// The response is related to an action not listed in the enumerated list.
    /// </summary>
    public enum PRIAResponseBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// The response is related to a request of a fee quote.
		/// </summary>
        [XmlEnum("FeeQuote")]
        FeeQuote,

		/// <summary>
		/// The response is related to an action not listed in the enumerated list.
		/// </summary>
        [XmlEnum("Other")]
        Other,

        [XmlEnum("Received")]
        Received,

        [XmlEnum("Recorded")]
        Recorded,

        [XmlEnum("Rejected")]
        Rejected,

    }
}
