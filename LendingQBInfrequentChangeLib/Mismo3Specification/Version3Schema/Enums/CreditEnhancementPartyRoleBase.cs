namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum CreditEnhancementPartyRoleBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Investor")]
        Investor,

        [XmlEnum("Lender")]
        Lender,

        [XmlEnum("LoanSeller")]
        LoanSeller,

        [XmlEnum("MICompany")]
        MICompany,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Servicer")]
        Servicer,

        [XmlEnum("Shared")]
        Shared,

        [XmlEnum("Unknown")]
        Unknown,

    }
}
