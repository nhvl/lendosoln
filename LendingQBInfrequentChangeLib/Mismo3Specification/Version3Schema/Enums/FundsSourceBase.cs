namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Party having a non-familial relationship to the Borrower
    /// </summary>
    public enum FundsSourceBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Party who incurred the obligation for the mortgage loan.
		/// </summary>
        [XmlEnum("Borrower")]
        Borrower,

		/// <summary>
		/// Party who constructed the property serving as collateral for the mortgage loan.
		/// </summary>
        [XmlEnum("Builder")]
        Builder,

		/// <summary>
		/// A nonprofit organization with no religious affiliation
		/// </summary>
        [XmlEnum("CommunityNonProfit")]
        CommunityNonProfit,

		/// <summary>
		/// Party who provides income to the Borrower in exchange for work.
		/// </summary>
        [XmlEnum("Employer")]
        Employer,

		/// <summary>
		/// A department of the national government responsible for the oversight and administration of a specific function.
		/// </summary>
        [XmlEnum("FederalAgency")]
        FederalAgency,

        [XmlEnum("Institutional")]
        Institutional,

		/// <summary>
		/// Party funding the mortgage loan.
		/// </summary>
        [XmlEnum("Lender")]
        Lender,

		/// <summary>
		/// An administrative authority over areas that are smaller than a state, for example, counties, cities, or townships.
		/// </summary>
        [XmlEnum("LocalAgency")]
        LocalAgency,

        [XmlEnum("NonOriginatingFinancialInstitution")]
        NonOriginatingFinancialInstitution,

		/// <summary>
		/// An individual related to the borrower that is not the borrower's legal parent or guardian.
		/// </summary>
        [XmlEnum("NonParentRelative")]
        NonParentRelative,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Borrower's mother or father
		/// </summary>
        [XmlEnum("Parent")]
        Parent,

		/// <summary>
		/// Party from whom the borrower purchased the property serving as collateral for the mortgage loan.
		/// </summary>
        [XmlEnum("PropertySeller")]
        PropertySeller,

		/// <summary>
		/// An individual related to the borrower.
		/// </summary>
        [XmlEnum("Relative")]
        Relative,

		/// <summary>
		/// A nonprofit organization affiliated with a religion.
		/// </summary>
        [XmlEnum("ReligiousNonProfit")]
        ReligiousNonProfit,

        [XmlEnum("RuralHousingService")]
        RuralHousingService,

		/// <summary>
		/// An administrative authority one of the fifty states making up the United States.
		/// </summary>
        [XmlEnum("StateAgency")]
        StateAgency,

		/// <summary>
		/// Party providing the funding is not known.
		/// </summary>
        [XmlEnum("Unknown")]
        Unknown,

		/// <summary>
		/// Party having a non-familial relationship to the Borrower
		/// </summary>
        [XmlEnum("UnrelatedFriend")]
        UnrelatedFriend,

    }
}
