namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum ProjectedPaymentCalculationPeriodTermBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Monthly")]
        Monthly,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Yearly")]
        Yearly,

    }
}
