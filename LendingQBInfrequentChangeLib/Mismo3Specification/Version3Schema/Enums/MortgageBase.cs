namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A loan originated in accordance with the guidelines of the Department of Veterans Affairsâ€™  Loan Guaranty Home Loan Program, which guarantees loans made by private lenders, such as banks, savings & loans, or mortgage companies to eligible veterans for the purchase of a home which must be for their own personal occupancy.
    /// </summary>
    public enum MortgageBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Mortgage financing which is not insured or guaranteed by a government agency.
		/// </summary>
        [XmlEnum("Conventional")]
        Conventional,

		/// <summary>
		/// A loan originated in accordance with the guidelines of HUD's Federal Housing Administration, a federal agency that provides mortgage insurance on single-family, multifamily, manufactured homes and hospital loans made by FHA-approved lenders throughout the United States and its territories.
		/// </summary>
        [XmlEnum("FHA")]
        FHA,

		/// <summary>
		/// A loan originated in accordance with the guidelines of an agency governed by a municipal jurisdiction.
		/// </summary>
        [XmlEnum("LocalAgency")]
        LocalAgency,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// A loan originated in accordance with the guidelines of HUD's Office of Native American Programs, which provides safe and affordable housing for lower-income Native American, Alaska Native and Native Hawaiian families.
		/// </summary>
        [XmlEnum("PublicAndIndianHousing")]
        PublicAndIndianHousing,

		/// <summary>
		/// A loan originated in accordance with the guidelines of an agency governed by a state jurisdiction.
		/// </summary>
        [XmlEnum("StateAgency")]
        StateAgency,

		/// <summary>
		/// A  loan originated in accordance with the guidelines of the USDA Rural Development Agency's Housing and Community Facilities Program, which provides funding for single family homes in rural communities.
		/// </summary>
        [XmlEnum("USDARuralDevelopment")]
        USDARuralDevelopment,

		/// <summary>
		/// A loan originated in accordance with the guidelines of the Department of Veterans Affairsâ€™  Loan Guaranty Home Loan Program, which guarantees loans made by private lenders, such as banks, savings & loans, or mortgage companies to eligible veterans for the purchase of a home which must be for their own personal occupancy.  
		/// </summary>
        [XmlEnum("VA")]
        VA,

    }
}
