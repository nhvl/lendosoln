namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// The interest calculation will be used for the regular payment.
    /// </summary>
    public enum InterestCalculationPurposeBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// The interest calculation will be used for a draw.
		/// </summary>
        [XmlEnum("Draw")]
        Draw,

		/// <summary>
		/// The interest calculation will be used for other purpose.
		/// </summary>
        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// The interest calculation will be used for a payoff.
		/// </summary>
        [XmlEnum("Payoff")]
        Payoff,

		/// <summary>
		/// The interest calculation will be used for a servicer advance.
		/// </summary>
        [XmlEnum("ServicerAdvance")]
        ServicerAdvance,

		/// <summary>
		/// The interest calculation will be used for the regular payment.
		/// </summary>
        [XmlEnum("Standard")]
        Standard,

    }
}
