namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Single, divorced, or widowed
    /// </summary>
    public enum MaritalStatusBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Married")]
        Married,

        [XmlEnum("NotProvided")]
        NotProvided,

        [XmlEnum("Separated")]
        Separated,

        [XmlEnum("Unknown")]
        Unknown,

		/// <summary>
		/// Single, divorced, or widowed
		/// </summary>
        [XmlEnum("Unmarried")]
        Unmarried,

    }
}
