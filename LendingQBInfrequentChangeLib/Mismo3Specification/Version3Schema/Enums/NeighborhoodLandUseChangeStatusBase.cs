namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum NeighborhoodLandUseChangeStatusBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("InProcess")]
        InProcess,

        [XmlEnum("Likely")]
        Likely,

        [XmlEnum("NotLikely")]
        NotLikely,

    }
}
