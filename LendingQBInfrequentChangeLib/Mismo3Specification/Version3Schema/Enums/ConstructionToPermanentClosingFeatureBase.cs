namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum ConstructionToPermanentClosingFeatureBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("AutomaticConversion")]
        AutomaticConversion,

        [XmlEnum("ConvertibleARM")]
        ConvertibleARM,

        [XmlEnum("ModificationAgreement")]
        ModificationAgreement,

        [XmlEnum("NewNote")]
        NewNote,

        [XmlEnum("Other")]
        Other,

    }
}
