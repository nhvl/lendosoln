namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// The servicer remits the scheduled interest (less servicing fees) and the scheduled principal, regardless of actual collections from the borrower plus any unscheduled principal collected from the borrower that is due to the investor.
    /// </summary>
    public enum InvestorRemittanceBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// The service remits the actual interest net of servicing fees and actual principal collected from the borrower that is due to the investor.
		/// </summary>
        [XmlEnum("ActualInterestActualPrincipal")]
        ActualInterestActualPrincipal,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// The service remits the scheduled interest (less servicing fees), regardless of actual collections from the borrower plus actual principal collected from borrowers that is due to the investor.
		/// </summary>
        [XmlEnum("ScheduledInterestActualPrincipal")]
        ScheduledInterestActualPrincipal,

		/// <summary>
		/// The servicer remits the scheduled interest (less servicing fees) and the scheduled principal, regardless of actual collections from the borrower plus any unscheduled principal collected from the borrower that is due to the investor.
		/// </summary>
        [XmlEnum("ScheduledInterestScheduledPrincipal")]
        ScheduledInterestScheduledPrincipal,

    }
}
