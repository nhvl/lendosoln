namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Account is closed and has been transferred to another account or creditor.
    /// </summary>
    public enum CreditLiabilityAccountStatusBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Account is closed and credit is no longer available for use.
		/// </summary>
        [XmlEnum("Closed")]
        Closed,

		/// <summary>
		/// Legal proceedings have been initated to sell property to settle an unpaid balance.
		/// </summary>
        [XmlEnum("Frozen")]
        Frozen,

		/// <summary>
		/// Account is open
		/// </summary>
        [XmlEnum("Open")]
        Open,

		/// <summary>
		/// Account is closed and fully paid.
		/// </summary>
        [XmlEnum("Paid")]
        Paid,

		/// <summary>
		/// Account closed and debt is being paid under another account.
		/// </summary>
        [XmlEnum("Refinanced")]
        Refinanced,

		/// <summary>
		/// Account is closed and has been transferred to another account or creditor.
		/// </summary>
        [XmlEnum("Transferred")]
        Transferred,

    }
}
