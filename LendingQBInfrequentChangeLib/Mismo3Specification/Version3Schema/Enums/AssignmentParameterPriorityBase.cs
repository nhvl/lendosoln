namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Apply in order, master assignment parameters then client-specific parameters.
    /// </summary>
    public enum AssignmentParameterPriorityBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Apply only client parameter.
		/// </summary>
        [XmlEnum("ClientSpecificOnly")]
        ClientSpecificOnly,

		/// <summary>
		/// Apply all.
		/// </summary>
        [XmlEnum("Combined")]
        Combined,

		/// <summary>
		/// Apply only one.
		/// </summary>
        [XmlEnum("MutuallyExclusive")]
        MutuallyExclusive,

		/// <summary>
		/// One time instruction that trumps existing parameters.
		/// </summary>
        [XmlEnum("Override")]
        Override,

		/// <summary>
		/// Apply in order, master assignment parameters then client-specific parameters.
		/// </summary>
        [XmlEnum("SequentiallyApplied")]
        SequentiallyApplied,
    }
}
