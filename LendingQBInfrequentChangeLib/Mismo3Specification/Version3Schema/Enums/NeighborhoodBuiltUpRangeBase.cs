namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum NeighborhoodBuiltUpRangeBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Over75Percent")]
        Over75Percent,

        [XmlEnum("TwentyFiveToSeventyFivePercent")]
        TwentyFiveToSeventyFivePercent,

        [XmlEnum("Under25Percent")]
        Under25Percent,

    }
}
