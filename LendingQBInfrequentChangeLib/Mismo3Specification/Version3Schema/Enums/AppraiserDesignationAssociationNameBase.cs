namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum AppraiserDesignationAssociationNameBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("AmericanSocietyOfAppraisers")]
        AmericanSocietyOfAppraisers,

        [XmlEnum("AmericanSocietyOfFarmManagersAndRuralAppraisers")]
        AmericanSocietyOfFarmManagersAndRuralAppraisers,

        [XmlEnum("AppraisalInstitute")]
        AppraisalInstitute,

        [XmlEnum("NationalAssociationIndependentFeeAppraisers")]
        NationalAssociationIndependentFeeAppraisers,

        [XmlEnum("NationalAssociationOfRealtors")]
        NationalAssociationOfRealtors,

        [XmlEnum("RoyalInstituteOfCharteredSurveyors")]
        RoyalInstituteOfCharteredSurveyors,
    }
}
