namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// The VA will give a percentage of exemption for Veterans that do have a disability, but the disability is not service related.
    /// </summary>
    public enum VAFundingFeeExemptionBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("CompletelyExempt")]
        CompletelyExempt,

        [XmlEnum("NotExempt")]
        NotExempt,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// The VA will give a percentage of exemption for Veterans that do have a disability, but the disability is not service related.
		/// </summary>
        [XmlEnum("PartiallyExempt")]
        PartiallyExempt,

    }
}
