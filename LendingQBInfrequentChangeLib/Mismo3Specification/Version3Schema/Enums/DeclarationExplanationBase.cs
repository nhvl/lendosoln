namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum DeclarationExplanationBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("AlimonyChildSupport")]
        AlimonyChildSupport,

        [XmlEnum("BorrowedDownPayment")]
        BorrowedDownPayment,

        [XmlEnum("CoMakerEndorserOnNote")]
        CoMakerEndorserOnNote,

        [XmlEnum("DeclaredBankruptcyPastSevenYears")]
        DeclaredBankruptcyPastSevenYears,

        [XmlEnum("DelinquencyOrDefault")]
        DelinquencyOrDefault,

        [XmlEnum("DirectIndirectForeclosedPropertyPastSevenYears")]
        DirectIndirectForeclosedPropertyPastSevenYears,

        [XmlEnum("ObligatedOnLoanForeclosedOrDeedInLieuOfJudgment")]
        ObligatedOnLoanForeclosedOrDeedInLieuOfJudgment,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("OutstandingJudgments")]
        OutstandingJudgments,

        [XmlEnum("PartyToLawsuit")]
        PartyToLawsuit,

    }
}
