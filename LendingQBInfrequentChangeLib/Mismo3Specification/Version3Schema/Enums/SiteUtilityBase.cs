namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum SiteUtilityBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Electricity")]
        Electricity,

        [XmlEnum("Gas")]
        Gas,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("SanitarySewer")]
        SanitarySewer,

        [XmlEnum("StormSewer")]
        StormSewer,

        [XmlEnum("Water")]
        Water,

    }
}
