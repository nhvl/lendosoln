namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Preparation taken to ready the property for cold weather.
    /// </summary>
    public enum PropertyPreservationActionBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Preparation taken to prevent intrusion into the property.
		/// </summary>
        [XmlEnum("Secured")]
        Secured,

		/// <summary>
		/// Actions to preserve the property are not known.
		/// </summary>
        [XmlEnum("Unknown")]
        Unknown,

		/// <summary>
		/// Preparation taken to ready the property for cold weather.
		/// </summary>
        [XmlEnum("Winterized")]
        Winterized,

    }
}
