namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum AbilityToRepayExemptionCreditorOrganizationBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("CommunityDevelopmentFinancialInstitution")]
        CommunityDevelopmentFinancialInstitution,

        [XmlEnum("CommunityHousingDevelopmentOrganization")]
        CommunityHousingDevelopmentOrganization,

        [XmlEnum("DownpaymentAssistanceProvider")]
        DownpaymentAssistanceProvider,

        [XmlEnum("NonProfitOrganization")]
        NonProfitOrganization,

        [XmlEnum("Other")]
        Other,
    }
}
