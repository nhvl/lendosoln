namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum BuydownBaseDateBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("FirstPaymentDate")]
        FirstPaymentDate,

        [XmlEnum("LastPaymentDate")]
        LastPaymentDate,

        [XmlEnum("NoteDate")]
        NoteDate,

        [XmlEnum("Other")]
        Other,
    }
}
