namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum FloodRequestActionBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Cancellation")]
        Cancellation,

        [XmlEnum("Change")]
        Change,

        [XmlEnum("Dispute")]
        Dispute,

        [XmlEnum("Original")]
        Original,

        [XmlEnum("Reissue")]
        Reissue,

        [XmlEnum("StatusQuery")]
        StatusQuery,

        [XmlEnum("Transfer")]
        Transfer,

        [XmlEnum("Upgrade")]
        Upgrade,

    }
}
