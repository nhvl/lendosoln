namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Code List Found In: Trans Union 4.0 User Guide - Chapter 3 SH01 Segment - Suppression Indicator Codes.  Data Source: SH01 Segment / Suppression Indicator Codes.
    /// </summary>
    public enum CreditResponseAlertMessageCodeSourceBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Code List Found In: Equifax STS V5/V6 Attachment 1 - Fraud Victim/Alert Indicators.  Data Source: Header Segment (FULL Record) / Fraud Victim/Alert Indicator Code.
		/// </summary>
        [XmlEnum("EquifaxFraudVictimAlert")]
        EquifaxFraudVictimAlert,

		/// <summary>
		/// Code List Found In: Equifax STS V5/V6 Guide / Attachment 1 - Hit/No-Hit Designator Codes.  Data Source: Header Segment (FULL Record) / Hit/No-Hit Designator Code.
		/// </summary>
        [XmlEnum("EquifaxHitNoHitDesignator")]
        EquifaxHitNoHitDesignator,

		/// <summary>
		/// Code List Found In: Equifax STS V5/V6 Guide / OFAC Alert Notes (4).  Data Source: Segment 59 (CD Record) / Match Codes.
		/// </summary>
        [XmlEnum("EquifaxOFAC")]
        EquifaxOFAC,

		/// <summary>
		/// Code List Found In: Equifax STS V5/V6 Attachment 7 - SAFESCAN Codes.  Data Source: Header Segment (FULL Record) / SAFESCAN Codes.
		/// </summary>
        [XmlEnum("EquifaxSafescan")]
        EquifaxSafescan,

		/// <summary>
		/// Code List Found In: Experian File One Appendix F - Fraud Services Indicators.  Data Source: 382 Segment - C4 Sub-segment / Indicator 1, 2, 3, 4.
		/// </summary>
        [XmlEnum("ExperianFraudServicesIndicator")]
        ExperianFraudServicesIndicator,

		/// <summary>
		/// Code List Found In: Experian File One Appendix O - Message Codes.  Data Source: 361 Segment / Message Code.
		/// </summary>
        [XmlEnum("ExperianMessage")]
        ExperianMessage,

		/// <summary>
		/// Code List Found In: Experian File One Appendix P - Statement Type Codes.  Data Source: 365 Segment / Type Code.
		/// </summary>
        [XmlEnum("ExperianStatementType")]
        ExperianStatementType,

		/// <summary>
		/// Code List is from another source that is identified in Alert Message Code Source Type Other Description.
		/// </summary>
        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Code List Found In: Trans Union 4.0 User Guide - Chapter 6 Add-on Services - Authorized User Alert - Code 07014 - Fixed Format Response Note 1 (Authorized User Alert Search Status codes).  Data Source: AO01 Add-on Status Segment / Search Status Code.
		/// </summary>
        [XmlEnum("TransUnionAuthorizedUser")]
        TransUnionAuthorizedUser,

		/// <summary>
		/// Code List Found In: Trans Union 4.0 User Guide - Chapter 3 SH01 Segment - File Hit Codes.  Data Source: SH01 Segment / File Hit Codes.
		/// </summary>
        [XmlEnum("TransUnionFileHit")]
        TransUnionFileHit,

		/// <summary>
		/// Code List Found In: Trans Union 4.0 User Guide - Chapter 3 CS01 Segment - Content Type Codes.  Data Source: CS01 Segment / Content Type Codes.
		/// </summary>
        [XmlEnum("TransUnionFraudVictimAlert")]
        TransUnionFraudVictimAlert,

		/// <summary>
		/// Code List Found In: Trans Union  4.0 User Guide - Chapter 6 Add-on Services - High Risk Fraud Alert Service - Code 06500.  Data Source: QH01, MC01, DC01, YI01 or MT01 Segments - Message Code.
		/// </summary>
        [XmlEnum("TransUnionHighRiskFraudAlert")]
        TransUnionHighRiskFraudAlert,

		/// <summary>
		/// Code List Found In: Trans Union 4.0 User Guide - Chapter 6 Add-on Services - ID Mismatch Alert Service - Code 06400.  Data Source: TA01 segment - Message Type Code.
		/// </summary>
        [XmlEnum("TransUnionIdentifierMismatchAlert")]
        TransUnionIdentifierMismatchAlert,

		/// <summary>
		/// Code List Found In: Trans Union 4.0 User Guide - Chapter 6 Add-on Services - OFAC Name Screen - Code 06800 - Fixed Format Response Note 1 (OFAC Name Screen Search Status).  Data Source: OFAC Name Screen 06800 - Search Status Codes returned in segment AO01.
		/// </summary>
        [XmlEnum("TransUnionOFAC")]
        TransUnionOFAC,

		/// <summary>
		/// Code List Found In: Trans Union 4.0 User Guide - Chapter 3 SH01 Segment - Suppression Indicator Codes.  Data Source: SH01 Segment / Suppression Indicator Codes.
		/// </summary>
        [XmlEnum("TransUnionSuppressionIndicator")]
        TransUnionSuppressionIndicator,

    }
}
