namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// The volume referenced contains plats
    /// </summary>
    public enum VolumeBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// The volume referenced contains deeds
		/// </summary>
        [XmlEnum("Deed")]
        Deed,

		/// <summary>
		/// The volume referenced contains various maps
		/// </summary>
        [XmlEnum("Maps")]
        Maps,

		/// <summary>
		/// The volume referenced contains mortgages
		/// </summary>
        [XmlEnum("Mortgage")]
        Mortgage,

		/// <summary>
		/// The volume referenced contains something other than the enumerated list
		/// </summary>
        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// The volume referenced contains plats
		/// </summary>
        [XmlEnum("Plat")]
        Plat,

    }
}
