namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Uses other rating code system.
    /// </summary>
    public enum CreditRatingCodeBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Uses Equifax coding systems for rating manner of payment.
		/// </summary>
        [XmlEnum("Equifax")]
        Equifax,

		/// <summary>
		/// Uses Experian coding system for rating manner of payment.
		/// </summary>
        [XmlEnum("Experian")]
        Experian,

		/// <summary>
		/// Uses other rating code system.
		/// </summary>
        [XmlEnum("Other")]
        Other,

    }
}
