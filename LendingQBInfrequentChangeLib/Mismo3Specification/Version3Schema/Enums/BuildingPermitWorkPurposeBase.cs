namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Work that involves replacing a non-fully working feature on the property.
    /// </summary>
    public enum BuildingPermitWorkPurposeBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Some level of improvement or alteration to an existing structure that adds square footage, accessory feature or amenity.
		/// </summary>
        [XmlEnum("Addition")]
        Addition,

		/// <summary>
		/// Work that primarily involves the deconstruction of existing structure or features on a property.
		/// </summary>
        [XmlEnum("Demolition")]
        Demolition,

		/// <summary>
		/// Work that primarily involves adding new structures to a property.
		/// </summary>
        [XmlEnum("NewConstruction")]
        NewConstruction,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Some level of signficant alteration to improve or update an existing structure.
		/// </summary>
        [XmlEnum("Remodel")]
        Remodel,

		/// <summary>
		/// Work that primarily involves repairing a feature on a property.
		/// </summary>
        [XmlEnum("Repair")]
        Repair,

		/// <summary>
		/// Work that involves replacing a non-fully working feature on the property.
		/// </summary>
        [XmlEnum("Replace")]
        Replace,

        [XmlEnum("Unknown")]
        Unknown,
    }
}
