namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Loan is brought current.
    /// </summary>
    public enum ForeclosureCaseDismissedReasonBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("CourtOrdered")]
        CourtOrdered,

        [XmlEnum("LossMitigationCompleted")]
        LossMitigationCompleted,

        [XmlEnum("LossMitigationInReview")]
        LossMitigationInReview,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Loan is brought current.
		/// </summary>
        [XmlEnum("Reinstatement")]
        Reinstatement,

        [XmlEnum("Unknown")]
        Unknown,

    }
}
