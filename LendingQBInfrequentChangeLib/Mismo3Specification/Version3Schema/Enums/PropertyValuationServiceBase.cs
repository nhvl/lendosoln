namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum PropertyValuationServiceBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("LimitedSummaryReport")]
        LimitedSummaryReport,

        [XmlEnum("NonUSPAPAutomatedValuationModel")]
        NonUSPAPAutomatedValuationModel,

        [XmlEnum("NonUSPAPPriceOpinion")]
        NonUSPAPPriceOpinion,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("RestrictedUseReport")]
        RestrictedUseReport,

        [XmlEnum("SelfContainedReport")]
        SelfContainedReport,

    }
}
