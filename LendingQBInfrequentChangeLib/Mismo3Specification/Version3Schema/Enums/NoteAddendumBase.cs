namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum NoteAddendumBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("AffordableMeritRate")]
        AffordableMeritRate,

        [XmlEnum("Arbitration")]
        Arbitration,

        [XmlEnum("Balloon")]
        Balloon,

        [XmlEnum("Construction")]
        Construction,

        [XmlEnum("FixedRateOption")]
        FixedRateOption,

        [XmlEnum("GEM")]
        GEM,

        [XmlEnum("GPM")]
        GPM,

        [XmlEnum("InterestOnly")]
        InterestOnly,

        [XmlEnum("InterVivosRevocableTrust")]
        InterVivosRevocableTrust,

        [XmlEnum("OccupancyDeclaration")]
        OccupancyDeclaration,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Prepayment")]
        Prepayment,

        [XmlEnum("RateImprovement")]
        RateImprovement,

    }
}
