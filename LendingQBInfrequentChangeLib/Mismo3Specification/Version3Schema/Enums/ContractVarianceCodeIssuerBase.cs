namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Department of Veteran Affairs
    /// </summary>
    public enum ContractVarianceCodeIssuerBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Federal Housing Administration 
		/// </summary>
        [XmlEnum("FHA")]
        FHA,

        [XmlEnum("Investor")]
        Investor,

        [XmlEnum("MI")]
        MI,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Seller")]
        Seller,

		/// <summary>
		/// Department of Veteran Affairs
		/// </summary>
        [XmlEnum("VA")]
        VA,

    }
}
