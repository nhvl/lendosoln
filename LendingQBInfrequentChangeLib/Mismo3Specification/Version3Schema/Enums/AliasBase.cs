namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// The party is now known by this name
    /// </summary>
    public enum AliasBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// The party is also known by this name
		/// </summary>
        [XmlEnum("AlsoKnownAs")]
        AlsoKnownAs,

		/// <summary>
		/// The party was previously known by this name
		/// </summary>
        [XmlEnum("FormerlyKnownAs")]
        FormerlyKnownAs,

		/// <summary>
		/// The party is now known by this name
		/// </summary>
        [XmlEnum("NowKnownAs")]
        NowKnownAs,

        [XmlEnum("Other")]
        Other,
    }
}
