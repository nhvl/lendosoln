namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Original LTV on loan greater than 97 percent.
    /// </summary>
    public enum LoanPriceLineItemBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Property Value not established using appraisal standard of the investor.
		/// </summary>
        [XmlEnum("AlternatePropertyValuation")]
        AlternatePropertyValuation,

		/// <summary>
		/// Loan is a Balloon.
		/// </summary>
        [XmlEnum("BalloonLoan")]
        BalloonLoan,

		/// <summary>
		/// Cash back to borrower exceeded allowable level to be considered no-cash back.
		/// </summary>
        [XmlEnum("CashOutRefinance")]
        CashOutRefinance,

		/// <summary>
		/// Loan has high risk of default primarily due to risky combination of LTV and borrower creditworthiness.
		/// </summary>
        [XmlEnum("HighCreditRisk")]
        HighCreditRisk,

		/// <summary>
		/// Original LTV on loan up to 97percent.
		/// </summary>
        [XmlEnum("HighLTVLoan")]
        HighLTVLoan,

		/// <summary>
		/// Property does not have sufficient flood insurance coverage.
		/// </summary>
        [XmlEnum("InsufficientFloodInsurance")]
        InsufficientFloodInsurance,

		/// <summary>
		/// A fee or adjustment based on the difference between the interest rate of the loan and the required yield of the lender or investor.
		/// </summary>
        [XmlEnum("InterestRateDifferential")]
        InterestRateDifferential,

		/// <summary>
		/// Property use is non-owner-occupied
		/// </summary>
        [XmlEnum("InvestmentProperty")]
        InvestmentProperty,

		/// <summary>
		/// Transaction includes certain secondary financing structures.
		/// </summary>
        [XmlEnum("LoanWithSubordinateFinancing")]
        LoanWithSubordinateFinancing,

		/// <summary>
		/// Charge based on the duration of a price lock.
		/// </summary>
        [XmlEnum("LockPeriod")]
        LockPeriod,

		/// <summary>
		/// A fee or adjustment based on the difference between the margin rate of the loan and the standard margin for the loan product.
		/// </summary>
        [XmlEnum("MarginDifferential")]
        MarginDifferential,

		/// <summary>
		/// Modifiable Loan in lieu of a refinance.
		/// </summary>
        [XmlEnum("ModifiableLoan")]
        ModifiableLoan,

		/// <summary>
		/// Collateral has more than one dwelling unit.
		/// </summary>
        [XmlEnum("MultipleDwellingUnitProperty")]
        MultipleDwellingUnitProperty,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Income and/or asset documentation varied from investor standard.
		/// </summary>
        [XmlEnum("ReducedDocumentation")]
        ReducedDocumentation,

		/// <summary>
		/// Loan originated more than 12 months prior to investor funding.
		/// </summary>
        [XmlEnum("SeasonedLoan")]
        SeasonedLoan,

		/// <summary>
		/// Loan has Lien Priority Type of Second.
		/// </summary>
        [XmlEnum("SecondLien")]
        SecondLien,

		/// <summary>
		/// The premium paid for releasing loan servicing.
		/// </summary>
        [XmlEnum("ServiceReleasePremium")]
        ServiceReleasePremium,

		/// <summary>
		/// Origination process streamlined from Investor standard.
		/// </summary>
        [XmlEnum("StreamlinedOrigination")]
        StreamlinedOrigination,

		/// <summary>
		/// Loan originated as Texas equity refinance.
		/// </summary>
        [XmlEnum("TexasEquityRefinance")]
        TexasEquityRefinance,

		/// <summary>
		/// Original LTV on loan greater than 97 percent.
		/// </summary>
        [XmlEnum("VeryHighLTVLoan")]
        VeryHighLTVLoan,

    }
}
