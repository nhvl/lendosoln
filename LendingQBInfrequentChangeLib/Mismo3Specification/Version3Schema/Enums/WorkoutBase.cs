namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A sale of property in which the lien holder accepts less than the debt or obligation against the property.
    /// </summary>
    public enum WorkoutBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// The investor has authorized the Servicer to accept a voluntary conveyance of the property instead of initiating foreclosure proceedings. The Servicer has received the executed Deed from the borrower.
		/// </summary>
        [XmlEnum("DeedInLieu")]
        DeedInLieu,

        [XmlEnum("Modification")]
        Modification,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// The servicer has authorized a temporary suspension of payments or a repayment plan that calls for periodic payments of less than the normal monthly payment, or periodic payments at different intervals, to give the borrower additional time to bring the mortgage current.
		/// </summary>
        [XmlEnum("PaymentForbearance")]
        PaymentForbearance,

		/// <summary>
		/// Borrower pays back MI company
		/// </summary>
        [XmlEnum("PreclaimedAdvance")]
        PreclaimedAdvance,

		/// <summary>
		/// The servicer has authorized a agreement that calls for a certain amount of principal to become non-interest bearing UPB and due in the form of a balloon payment at the end of the loan.
		/// </summary>
        [XmlEnum("PrincipalForbearance")]
        PrincipalForbearance,

		/// <summary>
		/// The servicer is pursuing a new loan in which the existing mortgage is paid off with the proceeds of the new mortgage.
		/// </summary>
        [XmlEnum("Refinance")]
        Refinance,

		/// <summary>
		/// The servicer is pursuing a written or verbal agreement between the servicer and the borrower that gives the borrower a defined period of time to reinstate the mortgage by making payments in excess of the regularly scheduled payment.
		/// </summary>
        [XmlEnum("RepaymentPlan")]
        RepaymentPlan,

		/// <summary>
		/// A sale of property in which the lien holder accepts less than the debt or obligation against the property.
		/// </summary>
        [XmlEnum("ShortSale")]
        ShortSale,

    }
}
