namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum ComparableBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("ActiveListing")]
        ActiveListing,

        [XmlEnum("ClosedSale")]
        ClosedSale,

        [XmlEnum("CompetitiveOffering")]
        CompetitiveOffering,

        [XmlEnum("ExpiredListing")]
        ExpiredListing,

        [XmlEnum("Other")]
        Other,

    }
}
