namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum PartialPaymentApplicationMethodBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("ApplyPartialPayment")]
        ApplyPartialPayment,

        [XmlEnum("HoldUntilCompleteAmount")]
        HoldUntilCompleteAmount,

        [XmlEnum("Other")]
        Other,

    }
}
