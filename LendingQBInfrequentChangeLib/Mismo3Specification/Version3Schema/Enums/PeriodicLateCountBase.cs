namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// The next previous 12 months - months 13 to 24.
    /// </summary>
    public enum PeriodicLateCountBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// The most recent 12 months - months 1 to 12.
		/// </summary>
        [XmlEnum("CurrentOneToTwelveMonths")]
        CurrentOneToTwelveMonths,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// The next previous 12 months - months 25 to 36.
		/// </summary>
        [XmlEnum("Previous25To36Months")]
        Previous25To36Months,

		/// <summary>
		/// The next previous 12 months - months 13 to 24.
		/// </summary>
        [XmlEnum("PreviousThirteenTo24Months")]
        PreviousThirteenTo24Months,

    }
}
