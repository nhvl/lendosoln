namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum MarketTrendPeriodBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("FutureEighteenMonths")]
        FutureEighteenMonths,

        [XmlEnum("FutureSixMonths")]
        FutureSixMonths,

        [XmlEnum("FutureThreeMonths")]
        FutureThreeMonths,

        [XmlEnum("FutureTwelveMonths")]
        FutureTwelveMonths,

        [XmlEnum("PriorEighteenMonths")]
        PriorEighteenMonths,

        [XmlEnum("PriorSixMonths")]
        PriorSixMonths,

        [XmlEnum("PriorThreeMonths")]
        PriorThreeMonths,

        [XmlEnum("PriorTwelveMonths")]
        PriorTwelveMonths,

    }
}
