namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum MIPremiumSourceBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Borrower")]
        Borrower,

        [XmlEnum("BorrowerAndLender")]
        BorrowerAndLender,

        [XmlEnum("Lender")]
        Lender,

        [XmlEnum("Other")]
        Other,

    }
}
