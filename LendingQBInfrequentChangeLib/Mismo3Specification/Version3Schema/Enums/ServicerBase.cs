namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum ServicerBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("MasterServicer")]
        MasterServicer,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("PriorServicer")]
        PriorServicer,

        [XmlEnum("Servicer")]
        Servicer,

        [XmlEnum("SpecialServicer")]
        SpecialServicer,

        [XmlEnum("Subservicer")]
        Subservicer,

    }
}
