namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum LoanDefaultedReportingFrequencyBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Annually")]
        Annually,

        [XmlEnum("Biweekly")]
        Biweekly,

        [XmlEnum("Daily")]
        Daily,

        [XmlEnum("Monthly")]
        Monthly,

        [XmlEnum("Quarterly")]
        Quarterly,

        [XmlEnum("Semiannually")]
        Semiannually,

        [XmlEnum("Semimonthly")]
        Semimonthly,

        [XmlEnum("Weekly")]
        Weekly,

    }
}
