namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum ProjectedPaymentEscrowedBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Escrowed")]
        Escrowed,

        [XmlEnum("NotEscrowed")]
        NotEscrowed,

        [XmlEnum("SomeEscrowed")]
        SomeEscrowed,

    }
}
