namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// EscapedXML encoding MUST follow W3C Canonical XMLVersion 1.0 as documented in section 2.3 for producing a text node.
    /// </summary>
    public enum ObjectEncodingBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Base64 encoding MUST follow IETF RFC 4648 as documented in sections 3 and 4.
		/// </summary>
        [XmlEnum("Base64")]
        Base64,

        [XmlEnum("DeflateBase64")]
        DeflateBase64,

		/// <summary>
		/// EscapedXML encoding MUST follow W3C Canonical XMLVersion 1.0 as documented in section 2.3 for producing a text node.
		/// </summary>
        [XmlEnum("EscapedXML")]
        EscapedXML,

        [XmlEnum("GzipBase64")]
        GzipBase64,

        [XmlEnum("None")]
        None,

        [XmlEnum("Other")]
        Other,

    }
}
