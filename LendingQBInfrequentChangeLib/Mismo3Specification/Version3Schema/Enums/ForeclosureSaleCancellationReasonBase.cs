namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum ForeclosureSaleCancellationReasonBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("ExcessiveDelays")]
        ExcessiveDelays,

        [XmlEnum("FilingErrors")]
        FilingErrors,

        [XmlEnum("LoanModification")]
        LoanModification,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Refinance")]
        Refinance,

        [XmlEnum("ShortSale")]
        ShortSale,

    }
}
