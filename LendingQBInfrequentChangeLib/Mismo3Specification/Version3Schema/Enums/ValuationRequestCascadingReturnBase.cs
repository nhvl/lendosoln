namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// If this value is selected, then the product returned as part of the cascade will be returned in the response.
    /// </summary>
    public enum ValuationRequestCascadingReturnBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// If this value is selected, then all products returned as part of the cascade will be returned in the response.
		/// </summary>
        [XmlEnum("All")]
        All,

		/// <summary>
		/// Used to describe an "Other" enumerated value that is not included in the enumerated value list.
		/// </summary>
        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// If this value is selected, then the product returned as part of the cascade will be returned in the response.
		/// </summary>
        [XmlEnum("Single")]
        Single,

    }
}
