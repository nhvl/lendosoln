namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum ArrearageComponentBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("HomeownerAssociationFees")]
        HomeownerAssociationFees,

        [XmlEnum("HomeownersInsurance")]
        HomeownersInsurance,

        [XmlEnum("LateFees")]
        LateFees,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("PrincipalAndInterest")]
        PrincipalAndInterest,

        [XmlEnum("Taxes")]
        Taxes,
    }
}
