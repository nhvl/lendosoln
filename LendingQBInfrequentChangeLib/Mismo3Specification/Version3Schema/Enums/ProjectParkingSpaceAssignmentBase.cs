namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum ProjectParkingSpaceAssignmentBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Assigned")]
        Assigned,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Owned")]
        Owned,

    }
}
