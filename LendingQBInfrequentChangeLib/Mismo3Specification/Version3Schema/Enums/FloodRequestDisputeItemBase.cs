namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum FloodRequestDisputeItemBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("CBRAStatus")]
        CBRAStatus,

        [XmlEnum("CommunityNumber")]
        CommunityNumber,

        [XmlEnum("CommunityStatus")]
        CommunityStatus,

        [XmlEnum("MapDate")]
        MapDate,

        [XmlEnum("MapPanel")]
        MapPanel,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Zone")]
        Zone,

    }
}
