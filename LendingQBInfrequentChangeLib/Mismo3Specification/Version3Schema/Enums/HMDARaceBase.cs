namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Category as defined by the Home Mortgage Disclosure Act.
    /// </summary>
    public enum HMDARaceBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Category as defined by the Home Mortgage Disclosure Act.
		/// </summary>
        [XmlEnum("AmericanIndianOrAlaskaNative")]
        AmericanIndianOrAlaskaNative,

		/// <summary>
		/// Category as defined by the Home Mortgage Disclosure Act.
		/// </summary>
        [XmlEnum("Asian")]
        Asian,

		/// <summary>
		/// Category as defined by the Home Mortgage Disclosure Act.
		/// </summary>
        [XmlEnum("BlackOrAfricanAmerican")]
        BlackOrAfricanAmerican,

		/// <summary>
		/// Category as defined by the Home Mortgage Disclosure Act.
		/// </summary>
        [XmlEnum("InformationNotProvidedByApplicantInMailInternetOrTelephoneApplication")]
        InformationNotProvidedByApplicantInMailInternetOrTelephoneApplication,

		/// <summary>
		/// Category as defined by the Home Mortgage Disclosure Act.
		/// </summary>
        [XmlEnum("NativeHawaiianOrOtherPacificIslander")]
        NativeHawaiianOrOtherPacificIslander,

		/// <summary>
		/// Category as defined by the Home Mortgage Disclosure Act. Used for loans where the borrower is an institution, corporation, or partnership.
		/// </summary>
        [XmlEnum("NotApplicable")]
        NotApplicable,

		/// <summary>
		/// Category as defined by the Home Mortgage Disclosure Act.
		/// </summary>
        [XmlEnum("White")]
        White,
    }
}
