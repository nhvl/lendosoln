namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// National Mortgage Insurance
    /// </summary>
    public enum MICompanyNameBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("CMG")]
        CMG,

        [XmlEnum("Essent")]
        Essent,

        [XmlEnum("Genworth")]
        Genworth,

        [XmlEnum("MGIC")]
        MGIC,

		/// <summary>
		/// National Mortgage Insurance
		/// </summary>
        [XmlEnum("NationalMI")]
        NationalMI,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("PMI")]
        PMI,

        [XmlEnum("Radian")]
        Radian,

        [XmlEnum("RMIC")]
        RMIC,

        [XmlEnum("Triad")]
        Triad,

        [XmlEnum("UGI")]
        UGI,

    }
}
