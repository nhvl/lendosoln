namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Denotes that the contact is required to sign.
    /// </summary>
    public enum SignatoryRoleBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Denotes that the contact is one the persons authorized to sign. 
		/// </summary>
        [XmlEnum("Authorized")]
        Authorized,

		/// <summary>
		/// Denotes that the contact is not authorized to sign.
		/// </summary>
        [XmlEnum("NotAuthorized")]
        NotAuthorized,

		/// <summary>
		/// Denotes that the contact is required to sign.
		/// </summary>
        [XmlEnum("Required")]
        Required,

    }
}
