namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// One in a row of identical houses or having common wall. Attached to another unit via common wall (e.g., a brownstone.)
    /// </summary>
    public enum ProjectDesignBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("GardenProject")]
        GardenProject,

        [XmlEnum("HighriseProject")]
        HighriseProject,

        [XmlEnum("MidriseProject")]
        MidriseProject,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// One in a row of identical houses or having common wall. Attached to another unit via common wall (e.g., a brownstone.)
		/// </summary>
        [XmlEnum("TownhouseRowhouse")]
        TownhouseRowhouse,

    }
}
