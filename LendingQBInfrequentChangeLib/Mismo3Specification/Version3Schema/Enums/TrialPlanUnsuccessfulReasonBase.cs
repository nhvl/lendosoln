namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// The trail payment agreement was not fulfilled.
    /// </summary>
    public enum TrialPlanUnsuccessfulReasonBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// The borrower voluntarily dropped out of the trial plan.
		/// </summary>
        [XmlEnum("BorrowerWithdrew")]
        BorrowerWithdrew,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// The trail payment agreement was not fulfilled.
		/// </summary>
        [XmlEnum("TrialPaymentDefault")]
        TrialPaymentDefault,

    }
}
