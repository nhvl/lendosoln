namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum CarStorageAttachmentBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Attached")]
        Attached,

        [XmlEnum("BuiltIn")]
        BuiltIn,

        [XmlEnum("Detached")]
        Detached,

    }
}
