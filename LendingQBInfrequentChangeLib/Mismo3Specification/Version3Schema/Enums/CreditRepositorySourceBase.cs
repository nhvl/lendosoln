namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum CreditRepositorySourceBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Equifax")]
        Equifax,

        [XmlEnum("Experian")]
        Experian,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("TransUnion")]
        TransUnion,

        [XmlEnum("Unspecified")]
        Unspecified,

    }
}
