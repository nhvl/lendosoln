namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum IdentityDocumentIssuedByBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Country")]
        Country,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("StateProvince")]
        StateProvince,

    }
}
