namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum PrepaidItemPerDiemCalculationMethodBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("360")]
        Item360,

        [XmlEnum("365")]
        Item365,

        [XmlEnum("365Or366")]
        Item365Or366,

        [XmlEnum("Other")]
        Other,

    }
}
