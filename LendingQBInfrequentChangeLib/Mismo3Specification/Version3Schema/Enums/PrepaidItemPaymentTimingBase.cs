namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum PrepaidItemPaymentTimingBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("AtClosing")]
        AtClosing,

        [XmlEnum("BeforeClosing")]
        BeforeClosing,

        [XmlEnum("FirstPayment")]
        FirstPayment,

        [XmlEnum("Other")]
        Other,

    }
}
