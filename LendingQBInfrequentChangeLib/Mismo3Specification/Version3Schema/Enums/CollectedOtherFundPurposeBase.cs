namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Provided to reduce principal in excess of scheduled payment amount
    /// </summary>
    public enum CollectedOtherFundPurposeBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// PITI payment(s) provided prior to the scheduled due date.
		/// </summary>
        [XmlEnum("AdvancePITIPayment")]
        AdvancePITIPayment,

		/// <summary>
		/// Provided to fund the escrow account at closing 
		/// </summary>
        [XmlEnum("EscrowFunds")]
        EscrowFunds,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Provided to reduce principal in excess of scheduled payment amount
		/// </summary>
        [XmlEnum("PrincipalCurtailment")]
        PrincipalCurtailment,

    }
}
