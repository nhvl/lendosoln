namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Property taxes in aggregate levied by all authorities in which the subject property is located.
    /// </summary>
    public enum ProjectedPaymentEstimatedTaxesInsuranceAssessmentComponentBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Fee payable to condominium association in which the subject property is located, which is intended to pay electricity bills for street lights, landscaping, and insurance, maintenance and repairs to community facilities like clubhouses, pools, and exercise rooms and may also cover the salaries of condominium association employees and/or third party management fees.
		/// </summary>
        [XmlEnum("CondominiumAssociationDues")]
        CondominiumAssociationDues,

		/// <summary>
		/// Fee payable to condominium association in which the subject property is located for capital improvements, repairs, utility service upgrades, etc. that are assessed in addition to the regularly occurring condominium dues.
		/// </summary>
        [XmlEnum("CondominiumAssociationSpecialAssessment")]
        CondominiumAssociationSpecialAssessment,

		/// <summary>
		/// Fee payable to cooperative in which the subject property is located, which is intended to pay common utilities, landscaping, and insurance, maintenance and repairs to cooperative facilities and may also cover the salaries of cooperative employees and/or third party management fees.
		/// </summary>
        [XmlEnum("CooperativeAssociationDues")]
        CooperativeAssociationDues,

		/// <summary>
		/// Fee payable to cooperative association by the unit owners which is in addition to the regularly occurring maintenance fees for a specific project or outstanding debt that was not part of the annual budget/assessment. The special assessment is against all unit owners and requires them to pay their fractional interest of the money being requested. The payment of the special assessment is divided by each unit owner's interest. The amount may be requested immediately from each unit owner or may be broken into installments depending on how they have decided to handle it.
		/// </summary>
        [XmlEnum("CooperativeAssociationSpecialAssessment")]
        CooperativeAssociationSpecialAssessment,

		/// <summary>
		/// A sum paid for land lease by the owner of a building to the owner of the land on which it is located for state, territory, tribal or other land not pursuant to a leasehold.
		/// </summary>
        [XmlEnum("GroundRent")]
        GroundRent,

		/// <summary>
		/// Fee payable by homeowners to homeowners or neighborhood association within which the subject property is located, which is intended to pay for the daily operation of the association and may include but is not limited to items such as electricity bills for street lights, landscaping, and maintenance and repairs to community facilities like clubhouses, pools, and exercise rooms and may also cover insurance on community assets, the salaries of HOA employees or third party management fees.
		/// </summary>
        [XmlEnum("HomeownersAssociationDues")]
        HomeownersAssociationDues,

		/// <summary>
		/// Fee payable by homeowners to homeowners or neighborhood association in which the subject property is located for items such as capital improvements, utility service upgrades, etc., that may be assessed in addition to the regularly occurring association dues.
		/// </summary>
        [XmlEnum("HomeownersAssociationSpecialAssessment")]
        HomeownersAssociationSpecialAssessment,

		/// <summary>
		/// Periodic amount paid for coverage against losses or damage to the insured subject property from hazards such as fire, theft or other named perils. May also cover personal property and/or include riders providing supplemental coverage, e.g. windstorm, hurricanes, or other named causes.
		/// </summary>
        [XmlEnum("HomeownersInsurance")]
        HomeownersInsurance,

		/// <summary>
		/// An amount of periodic payment due pursuant to a leasehold agreement.
		/// </summary>
        [XmlEnum("LeaseholdPayment")]
        LeaseholdPayment,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Property taxes in aggregate levied by all authorities in which the subject property is located.
		/// </summary>
        [XmlEnum("PropertyTaxes")]
        PropertyTaxes,

    }
}
