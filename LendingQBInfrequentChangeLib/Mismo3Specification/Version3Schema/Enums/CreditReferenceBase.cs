namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum CreditReferenceBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("MortgageCreditOnly")]
        MortgageCreditOnly,

        [XmlEnum("NonCreditPayment")]
        NonCreditPayment,

        [XmlEnum("NonTraditional")]
        NonTraditional,

        [XmlEnum("Other")]
        Other,

    }
}
