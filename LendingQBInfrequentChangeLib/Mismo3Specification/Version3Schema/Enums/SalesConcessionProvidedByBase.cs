namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum SalesConcessionProvidedByBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Builder")]
        Builder,

        [XmlEnum("MortgageLender")]
        MortgageLender,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("RealEstateBroker")]
        RealEstateBroker,

        [XmlEnum("Seller")]
        Seller,

    }
}
