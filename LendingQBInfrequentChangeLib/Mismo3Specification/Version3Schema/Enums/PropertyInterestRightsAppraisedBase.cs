namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum PropertyInterestRightsAppraisedBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Easement")]
        Easement,

        [XmlEnum("Encroachment")]
        Encroachment,

        [XmlEnum("FeeSimpleEstate")]
        FeeSimpleEstate,

        [XmlEnum("LeaseFeeEstate")]
        LeaseFeeEstate,

        [XmlEnum("LeaseholdEstate")]
        LeaseholdEstate,

        [XmlEnum("License")]
        License,

        [XmlEnum("LifeEstate")]
        LifeEstate,

        [XmlEnum("Other")]
        Other,

    }
}
