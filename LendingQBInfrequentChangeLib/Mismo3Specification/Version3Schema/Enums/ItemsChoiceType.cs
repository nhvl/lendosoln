namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum ItemsChoiceType
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("ContactPointFaxExtensionValue")]
        ContactPointFaxExtensionValue,

        [XmlEnum("ContactPointFaxValue")]
        ContactPointFaxValue,

        [XmlEnum("ContactPointTelephoneExtensionValue")]
        ContactPointTelephoneExtensionValue,

        [XmlEnum("ContactPointTelephoneValue")]
        ContactPointTelephoneValue,

    }
}
