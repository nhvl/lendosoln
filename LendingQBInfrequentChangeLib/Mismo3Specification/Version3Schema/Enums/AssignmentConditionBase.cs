namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum AssignmentConditionBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Agriculture")]
        Agriculture,

        [XmlEnum("CertifiedAppraiser")]
        CertifiedAppraiser,

        [XmlEnum("CertifiedGeneral")]
        CertifiedGeneral,

        [XmlEnum("ComplexProperties")]
        ComplexProperties,

        [XmlEnum("FHARoster")]
        FHARoster,

        [XmlEnum("HistoricProperties")]
        HistoricProperties,

        [XmlEnum("NoTrainee")]
        NoTrainee,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("PreApprovedByClient")]
        PreApprovedByClient,

        [XmlEnum("ProfessionalDesignation")]
        ProfessionalDesignation,

        [XmlEnum("VAPanel")]
        VAPanel,
    }
}
