namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum IntegratedDisclosureSectionBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("DueFromBorrowerAtClosing")]
        DueFromBorrowerAtClosing,

        [XmlEnum("DueFromSellerAtClosing")]
        DueFromSellerAtClosing,

        [XmlEnum("DueToSellerAtClosing")]
        DueToSellerAtClosing,

        [XmlEnum("InitialEscrowPaymentAtClosing")]
        InitialEscrowPaymentAtClosing,

        [XmlEnum("OriginationCharges")]
        OriginationCharges,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("OtherCosts")]
        OtherCosts,

        [XmlEnum("PaidAlreadyByOrOnBehalfOfBorrowerAtClosing")]
        PaidAlreadyByOrOnBehalfOfBorrowerAtClosing,

        [XmlEnum("PayoffsAndPayments")]
        PayoffsAndPayments,

        [XmlEnum("Prepaids")]
        Prepaids,

        [XmlEnum("ServicesBorrowerDidNotShopFor")]
        ServicesBorrowerDidNotShopFor,

        [XmlEnum("ServicesBorrowerDidShopFor")]
        ServicesBorrowerDidShopFor,

        [XmlEnum("ServicesYouCannotShopFor")]
        ServicesYouCannotShopFor,

        [XmlEnum("ServicesYouCanShopFor")]
        ServicesYouCanShopFor,

        [XmlEnum("TaxesAndOtherGovernmentFees")]
        TaxesAndOtherGovernmentFees,

        [XmlEnum("TotalClosingCosts")]
        TotalClosingCosts,

        [XmlEnum("TotalLoanCosts")]
        TotalLoanCosts,

        [XmlEnum("TotalOtherCosts")]
        TotalOtherCosts,

    }
}
