namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// The applicable Point Type applies to the contacts work.
    /// </summary>
    public enum ContactPointRoleBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// The applicable Point Type applies to the contacts home.
		/// </summary>
        [XmlEnum("Home")]
        Home,

		/// <summary>
		/// The applicable Point Type applies to the contacts mobile (phone/fax/email).
		/// </summary>
        [XmlEnum("Mobile")]
        Mobile,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// The applicable Point Type applies to the contacts work.
		/// </summary>
        [XmlEnum("Work")]
        Work,

    }
}
