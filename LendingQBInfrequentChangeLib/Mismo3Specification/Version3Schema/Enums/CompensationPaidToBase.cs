namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum CompensationPaidToBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Borrower")]
        Borrower,

        [XmlEnum("Broker")]
        Broker,

        [XmlEnum("Lender")]
        Lender,

        [XmlEnum("LoanOriginator")]
        LoanOriginator,

        [XmlEnum("Other")]
        Other,

    }
}
