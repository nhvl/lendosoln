namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// The organization that sells the loan to the investor.
    /// </summary>
    public enum REOMarketingPartyBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Investor")]
        Investor,

        [XmlEnum("Lender")]
        Lender,

        [XmlEnum("MortgageInsuranceCompany")]
        MortgageInsuranceCompany,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// The organization that sells the loan to the investor.
		/// </summary>
        [XmlEnum("Seller")]
        Seller,

        [XmlEnum("Servicer")]
        Servicer,

        [XmlEnum("Unknown")]
        Unknown,

    }
}
