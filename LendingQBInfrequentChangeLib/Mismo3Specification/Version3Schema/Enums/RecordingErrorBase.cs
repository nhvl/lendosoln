namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates an error with the electronic signature
    /// </summary>
    public enum RecordingErrorBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Indicates an error processing against a business rule
		/// </summary>
        [XmlEnum("BusinessRule")]
        BusinessRule,

		/// <summary>
		/// Indicates an error with the image attachment
		/// </summary>
        [XmlEnum("Image")]
        Image,

		/// <summary>
		/// Indicates an error not included in the enumerated list
		/// </summary>
        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Indicates an error when validating against the published schema.
		/// </summary>
        [XmlEnum("SchemaValidation")]
        SchemaValidation,

		/// <summary>
		/// Indicates an error with the electronic signature
		/// </summary>
        [XmlEnum("Signature")]
        Signature,

    }
}
