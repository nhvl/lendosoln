namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Government mortgage programs, as understood by Ginnie Mae in the PDD (<see cref="GinnieMaePddExporter"/ >).
    /// </summary>
    public enum MortgageProgramBase
    {
        [XmlEnum("")]
        Blank,

        [XmlEnum("FHASingleFamily")]
        FHASingleFamily,

        [XmlEnum("FHATitleI")]
        FHATitleI,

        [XmlEnum("PIH")]
        PIH,

        [XmlEnum("SingleFamilyRHS")]
        SingleFamilyRHS,

        [XmlEnum("VAGuaranteedInsured")]
        VAGuaranteedInsured,

        [XmlEnum("VAVendee")]
        VAVendee
    }
}
