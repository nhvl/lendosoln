namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Convertible at 3rd, 4th, and 5th Interest Rate Change Dates
    /// </summary>
    public enum ConversionScheduleBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Convertible Anytime Throughout Life of Loan
		/// </summary>
        [XmlEnum("Anytime")]
        Anytime,

		/// <summary>
		/// Convertible at 1st and 2nd Interest Rate Change Dates
		/// </summary>
        [XmlEnum("OnFirstAndSecondRateAdjustment")]
        OnFirstAndSecondRateAdjustment,

		/// <summary>
		/// Convertible from 1st through 5th Interest Rate Change Dates
		/// </summary>
        [XmlEnum("OnFirstThroughFifthRateAdjustment")]
        OnFirstThroughFifthRateAdjustment,

		/// <summary>
		/// Convertible at 1st, 2nd, and 3rd Interest Rate Change Dates
		/// </summary>
        [XmlEnum("OnFirstThroughThirdRateAdjustment")]
        OnFirstThroughThirdRateAdjustment,

		/// <summary>
		/// Convertible from 2nd through 10th Interest Rate Change Dates
		/// </summary>
        [XmlEnum("OnSecondThroughTenthRateAdjustment")]
        OnSecondThroughTenthRateAdjustment,

		/// <summary>
		/// Convertible at 3rd, 4th, and 5th Interest Rate Change Dates
		/// </summary>
        [XmlEnum("OnThirdThroughFifthRateAdjustment")]
        OnThirdThroughFifthRateAdjustment,

        [XmlEnum("Other")]
        Other,

    }
}
