namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum ContributionBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("HazardClaim")]
        HazardClaim,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Repair")]
        Repair,

    }
}
