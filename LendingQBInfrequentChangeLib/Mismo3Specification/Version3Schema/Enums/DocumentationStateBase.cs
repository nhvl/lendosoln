namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// As required of the borrow
    /// </summary>
    public enum DocumentationStateBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// As collected from the borrower
		/// </summary>
        [XmlEnum("AsCollected")]
        AsCollected,

		/// <summary>
		/// As requested by the borrower
		/// </summary>
        [XmlEnum("AsRequested")]
        AsRequested,

		/// <summary>
		/// As required of the borrow
		/// </summary>
        [XmlEnum("AsRequired")]
        AsRequired,

        [XmlEnum("Other")]
        Other,

    }
}
