namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum ProjectAnalysisAdequacyOfManagementBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Adequate")]
        Adequate,

        [XmlEnum("Inadequate")]
        Inadequate,

        [XmlEnum("Unknown")]
        Unknown,

    }
}
