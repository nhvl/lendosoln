namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Usually involves what the IRS calls a 1031 exchange of equity the buyer and seller hold in one or more other properties, in lieu of some portion of the cash purchase price
    /// </summary>
    public enum PurchaseCreditBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Funds held for the purpose of subsidizing some part of the periodic Principle and Interest in a specific pattern for a specific time period.
		/// </summary>
        [XmlEnum("BuydownFund")]
        BuydownFund,

        [XmlEnum("CommitmentOriginationFee")]
        CommitmentOriginationFee,

		/// <summary>
		/// A deposit made as a show of good faith by buyer to bind the conditions of a sale of real estate.
		/// </summary>
        [XmlEnum("EarnestMoney")]
        EarnestMoney,

		/// <summary>
		/// Funds provided to borrower by employer to offset some of the initial or ongoing costs of borrowers home.
		/// </summary>
        [XmlEnum("EmployerAssistedHousing")]
        EmployerAssistedHousing,

		/// <summary>
		/// Anticipated refund of Rural Development or VA Funding Fees enabled by the subject transaction.
		/// </summary>
        [XmlEnum("FederalAgencyFundingFeeRefund")]
        FederalAgencyFundingFeeRefund,

		/// <summary>
		/// Without consideration or promise of repayment, owner of property credits buyer with a specified dollar amount of equity. This is most often seen in inter-generation transfers in families, where parents will credit children with some portion of equity not conveyed in cash
		/// </summary>
        [XmlEnum("GiftOfEquity")]
        GiftOfEquity,

		/// <summary>
		/// Under a lease/purchase arrangement, owner of real estate agrees to credit some portion of rent towards down payment. This is the accumulated amount of such credits reflected at closing.
		/// </summary>
        [XmlEnum("LeasePurchaseFund")]
        LeasePurchaseFund,

		/// <summary>
		///  Anticipated refund of unearned prepaid mortgage insurance premium (either PMI or FHA) enabled by the subject transaction. 
		/// </summary>
        [XmlEnum("MIPremiumRefund")]
        MIPremiumRefund,

		/// <summary>
		/// Purchase credits of any type not otherwise listed; typically linked to a FREE TEXT description field.
		/// </summary>
        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Funds provided to relocating borrower (usually by employer) to offset some of the initial or ongoing costs of borrowers home at the new location.
		/// </summary>
        [XmlEnum("RelocationFunds")]
        RelocationFunds,

		/// <summary>
		/// Buyer/borrower agrees with owner (who is usually also builder/re-habber) that work done by Buyer/borrower to help complete the property will be credited as having specific value and will be done in lieu of some portion of the cash purchase price.
		/// </summary>
        [XmlEnum("SweatEquity")]
        SweatEquity,

		/// <summary>
		/// Usually involves what the IRS calls a 1031 exchange of equity the buyer and seller hold in one or more other properties, in lieu of some portion of the cash purchase price
		/// </summary>
        [XmlEnum("TradeEquity")]
        TradeEquity,

    }
}
