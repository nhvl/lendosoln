namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Valuation for the purpose of establishing an appropriate opinion of value for property tax assessment purposes.
    /// </summary>
    public enum ValuationIntendedUseBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// A valuation analysis to provide information to the property owner,  trustee or advisor, which may include an opinion of value and or consultation.
		/// </summary>
        [XmlEnum("EstatePlanning")]
        EstatePlanning,

		/// <summary>
		/// Professional consultation for property dispute resolution  including expert witness testimony, eminent domain cases, divorce proceedings and general litigation support.
		/// </summary>
        [XmlEnum("Litigation")]
        Litigation,

		/// <summary>
		/// Valuation for recomputing a loan to value ratio.
		/// </summary>
        [XmlEnum("MIRemoval")]
        MIRemoval,

        [XmlEnum("MortgageLossMitigation")]
        MortgageLossMitigation,

        [XmlEnum("MortgageOrigination")]
        MortgageOrigination,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Professional services provided directly to the potential buyer of a property.
		/// </summary>
        [XmlEnum("PrePurchaseConsultationAssistance")]
        PrePurchaseConsultationAssistance,

        [XmlEnum("PrivateSale")]
        PrivateSale,

		/// <summary>
		/// Professional services provided directly to the property owner.
		/// </summary>
        [XmlEnum("PropertyOwnerAssistanceConsultation")]
        PropertyOwnerAssistanceConsultation,

        [XmlEnum("SecondaryMarketDueDiligence")]
        SecondaryMarketDueDiligence,

		/// <summary>
		/// Valuation for the purpose of establishing an appropriate opinion of value for tax value disputes.
		/// </summary>
        [XmlEnum("TaxAppeal")]
        TaxAppeal,

		/// <summary>
		/// Valuation for the purpose of establishing an appropriate opinion of value for property tax assessment purposes.
		/// </summary>
        [XmlEnum("TaxAssessment")]
        TaxAssessment,

    }
}
