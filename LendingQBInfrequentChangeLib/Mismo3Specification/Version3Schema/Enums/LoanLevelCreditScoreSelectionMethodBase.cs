namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Lowest value of the middle or lower score per set of borrower scores.
    /// </summary>
    public enum LoanLevelCreditScoreSelectionMethodBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// The mean value of the average of each borrower.
		/// </summary>
        [XmlEnum("AverageThenAverage")]
        AverageThenAverage,

		/// <summary>
		/// Mean value of the middle or lower score per set of borrower scores.
		/// </summary>
        [XmlEnum("MiddleOrLowerThenAverage")]
        MiddleOrLowerThenAverage,

		/// <summary>
		/// Lowest value of the middle or lower score per set of borrower scores.
		/// </summary>
        [XmlEnum("MiddleOrLowerThenLowest")]
        MiddleOrLowerThenLowest,

        [XmlEnum("Other")]
        Other,

    }
}
