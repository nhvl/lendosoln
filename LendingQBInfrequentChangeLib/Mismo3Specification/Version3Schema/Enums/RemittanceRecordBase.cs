namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Taxes required to be paid.  Requirement and amount is based upon location jurisdiction.
    /// </summary>
    public enum RemittanceRecordBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Fee charged in some states by the underwriter to provide Closing Protection Letter coverage (LA, MO, NE, NJ, NC, OH, PA, UT).
		/// </summary>
        [XmlEnum("ClosingProtectionLetterFee")]
        ClosingProtectionLetterFee,

		/// <summary>
		/// Non-standard, additional coverage to the title policy for which fees may be charged.
		/// </summary>
        [XmlEnum("Endorsement")]
        Endorsement,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Dollar amount charged for the issuance of a policy.  Requirement and amount is based upon location jurisdiction.
		/// </summary>
        [XmlEnum("PolicyFee")]
        PolicyFee,

		/// <summary>
		/// Total premium dollar amount due to the title underwriter.
		/// </summary>
        [XmlEnum("PolicyPremium")]
        PolicyPremium,

		/// <summary>
		/// Taxes required to be paid.  Requirement and amount is based upon location jurisdiction.
		/// </summary>
        [XmlEnum("Tax")]
        Tax,

    }
}
