namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum HMDAPreapprovalBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("NotApplicable")]
        NotApplicable,

        [XmlEnum("PreapprovalWasNotRequested")]
        PreapprovalWasNotRequested,

        [XmlEnum("PreapprovalWasRequested")]
        PreapprovalWasRequested,

    }
}
