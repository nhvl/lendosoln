namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum UnitOccupancyBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("OwnerOccupied")]
        OwnerOccupied,

        [XmlEnum("Tenant")]
        Tenant,

        [XmlEnum("Unknown")]
        Unknown,

        [XmlEnum("Vacant")]
        Vacant,

    }
}
