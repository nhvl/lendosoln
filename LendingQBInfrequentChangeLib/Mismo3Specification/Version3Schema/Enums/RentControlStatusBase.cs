namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Rent Control is likely in the future.
    /// </summary>
    public enum RentControlStatusBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Rent Control is likely in the future.
		/// </summary>
        [XmlEnum("Likely")]
        Likely,

        [XmlEnum("No")]
        No,

        [XmlEnum("Yes")]
        Yes,

    }
}
