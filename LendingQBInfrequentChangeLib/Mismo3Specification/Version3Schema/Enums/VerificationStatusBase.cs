namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum VerificationStatusBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("NotVerified")]
        NotVerified,

        [XmlEnum("ToBeVerified")]
        ToBeVerified,

        [XmlEnum("Verified")]
        Verified,

    }
}
