namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum SchoolGradeBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Eighth")]
        Eighth,

        [XmlEnum("Eleventh")]
        Eleventh,

        [XmlEnum("Fifth")]
        Fifth,

        [XmlEnum("First")]
        First,

        [XmlEnum("Fourth")]
        Fourth,

        [XmlEnum("Kindergarten")]
        Kindergarten,

        [XmlEnum("Ninth")]
        Ninth,

        [XmlEnum("PreKindergarten")]
        PreKindergarten,

        [XmlEnum("Second")]
        Second,

        [XmlEnum("Seventh")]
        Seventh,

        [XmlEnum("Sixth")]
        Sixth,

        [XmlEnum("Tenth")]
        Tenth,

        [XmlEnum("Third")]
        Third,

        [XmlEnum("Twelfth")]
        Twelfth,

    }
}
