namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// The consideration given is the sale price of the subject property
    /// </summary>
    public enum ConsiderationBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// The consideration given is the attorney fee of the party
		/// </summary>
        [XmlEnum("AttorneysFee")]
        AttorneysFee,

		/// <summary>
		/// The consideration given is a judgment in favor of the party
		/// </summary>
        [XmlEnum("Judgment")]
        Judgment,

		/// <summary>
		/// The consideration given is a lien in favor of the party
		/// </summary>
        [XmlEnum("Lien")]
        Lien,

        [XmlEnum("OriginalLoanAmount")]
        OriginalLoanAmount,

		/// <summary>
		/// The consideration given is something other than the enumerated list
		/// </summary>
        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// The consideration given is the sale price of the subject property
		/// </summary>
        [XmlEnum("SalePrice")]
        SalePrice,

    }
}
