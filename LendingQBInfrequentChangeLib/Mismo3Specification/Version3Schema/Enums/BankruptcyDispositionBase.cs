namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A bankruptcy action is currently waiting on a matter resolution by the court.
    /// </summary>
    public enum BankruptcyDispositionBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// The bankruptcy plan has been accepted by affected parties.
		/// </summary>
        [XmlEnum("Confirmed")]
        Confirmed,

		/// <summary>
		/// The unsecured debt is no longer pursuable.
		/// </summary>
        [XmlEnum("Discharged")]
        Discharged,

		/// <summary>
		/// The bankruptcy was not completed. Collection activities can resume.
		/// </summary>
        [XmlEnum("Dismissed")]
        Dismissed,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// A bankruptcy action is currently waiting on a matter resolution by the court.
		/// </summary>
        [XmlEnum("Pending")]
        Pending,
    }
}
