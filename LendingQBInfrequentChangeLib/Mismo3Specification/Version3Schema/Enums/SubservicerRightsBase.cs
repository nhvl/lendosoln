namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Servicing a loan or group of loans for one remittance cycle.
    /// </summary>
    public enum SubservicerRightsBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("AllLoans")]
        AllLoans,

        [XmlEnum("OneLoan")]
        OneLoan,

		/// <summary>
		/// Servicing a loan or group of loans for one remittance cycle.
		/// </summary>
        [XmlEnum("OneRemittance")]
        OneRemittance,

        [XmlEnum("OneSecurity")]
        OneSecurity,

        [XmlEnum("Other")]
        Other,

    }
}
