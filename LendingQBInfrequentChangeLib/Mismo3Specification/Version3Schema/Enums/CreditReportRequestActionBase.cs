namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Upgrade an existing report (specify Credit Report Identifier) to a type listed in Credit Report Type, or specify additional repository bureaus to be pulled.
    /// </summary>
    public enum CreditReportRequestActionBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Add as a new request and force new credit data to be requested from the repository bureaus.
		/// </summary>
        [XmlEnum("ForceNew")]
        ForceNew,

		/// <summary>
		/// Credit Report Request Action Type is not in the enumerated list. Specify the action in the Credit Report Request Action Type Other Description.
		/// </summary>
        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Resend existing report (specify Credit Report Identifier). Reissue is used to request the re-delivery of a response file that had been delivered previously. Some credit vendors may charge for a Reissue, and in some cases, a Reissue may also trigger re-pulling of credit data if the on file credit data is older than a specified number of days.
		/// </summary>
        [XmlEnum("Reissue")]
        Reissue,

		/// <summary>
		/// Request initial delivery of an updated, upgraded, or completed report (specify Credit Report Identifier).
		/// </summary>
        [XmlEnum("Retrieve")]
        Retrieve,

		/// <summary>
		/// This option notifies the Credit Bureau that the previously issued credit response (specify Credit Report Identifier) has been reviewed by additional users (specify in KEY element). A response transaction does not need to be returned by the Credit Bureau.
		/// </summary>
        [XmlEnum("SecondaryUseNotification")]
        SecondaryUseNotification,

		/// <summary>
		/// Used to check on the status of an existing request transaction.
		/// </summary>
        [XmlEnum("StatusQuery")]
        StatusQuery,

		/// <summary>
		/// Submit or add as a new request.  If the Credit Bureau determines that the report data already exists on their system and was recently pulled, they may return this previously pulled data instead of requesting new data from the repository bureau.
		/// </summary>
        [XmlEnum("Submit")]
        Submit,

		/// <summary>
		/// Update data for an existing report (specify Credit Report Identifier).
		/// </summary>
        [XmlEnum("Update")]
        Update,

		/// <summary>
		/// Upgrade an existing report (specify Credit Report Identifier) to a type listed in Credit Report Type, or specify additional repository bureaus to be pulled.
		/// </summary>
        [XmlEnum("Upgrade")]
        Upgrade,

    }
}
