namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum CounselingBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("DefaultCounseling")]
        DefaultCounseling,

        [XmlEnum("FirstTimeHomebuyerCounseling")]
        FirstTimeHomebuyerCounseling,

        [XmlEnum("ForeclosureCounseling")]
        ForeclosureCounseling,

        [XmlEnum("HighCostMortgageCounseling")]
        HighCostMortgageCounseling,

        [XmlEnum("Other")]
        Other,

    }
}
