namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum RegulatoryAgencyBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("NationalCreditUnionAssociation")]
        NationalCreditUnionAssociation,

        [XmlEnum("OfficeOfTheComptrollerOfTheCurrency")]
        OfficeOfTheComptrollerOfTheCurrency,

        [XmlEnum("OfficeOfThriftSupervision")]
        OfficeOfThriftSupervision,

        [XmlEnum("Other")]
        Other,

    }
}
