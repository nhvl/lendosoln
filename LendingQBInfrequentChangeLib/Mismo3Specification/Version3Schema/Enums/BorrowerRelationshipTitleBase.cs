namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A couple legally joined by a common law marriage.
    /// </summary>
    public enum BorrowerRelationshipTitleBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("AHusbandAndWife")]
        AHusbandAndWife,

		/// <summary>
		/// A couple legally joined by marriage.
		/// </summary>
        [XmlEnum("AMarriedCouple")]
        AMarriedCouple,

        [XmlEnum("AMarriedMan")]
        AMarriedMan,

        [XmlEnum("AMarriedPerson")]
        AMarriedPerson,

        [XmlEnum("AMarriedWoman")]
        AMarriedWoman,

        [XmlEnum("AnUnmarriedMan")]
        AnUnmarriedMan,

        [XmlEnum("AnUnmarriedPerson")]
        AnUnmarriedPerson,

        [XmlEnum("AnUnmarriedWoman")]
        AnUnmarriedWoman,

		/// <summary>
		/// A couple legally joined by marriage or civil union in which both people are of the same gender.
		/// </summary>
        [XmlEnum("ASameSexMarriedCouple")]
        ASameSexMarriedCouple,

		/// <summary>
		/// A legal or interpersonal relationship between two individuals who live together and share a common domestic life but are neither joined by marriage nor a civil union.
		/// </summary>
        [XmlEnum("AsDomesticPartners")]
        AsDomesticPartners,

        [XmlEnum("ASingleMan")]
        ASingleMan,

        [XmlEnum("ASinglePerson")]
        ASinglePerson,

        [XmlEnum("ASingleWoman")]
        ASingleWoman,

        [XmlEnum("AWidow")]
        AWidow,

        [XmlEnum("AWidower")]
        AWidower,

        [XmlEnum("AWifeAndHusband")]
        AWifeAndHusband,

        [XmlEnum("HerHusband")]
        HerHusband,

        [XmlEnum("HisWife")]
        HisWife,

		/// <summary>
		/// A couple legally joined by civil union.
		/// </summary>
        [XmlEnum("JoinedInACivilUnion")]
        JoinedInACivilUnion,

		/// <summary>
		/// A couple legally joined by a common law marriage.
		/// </summary>
        [XmlEnum("JoinedInACommonLawMarriage")]
        JoinedInACommonLawMarriage,

        [XmlEnum("NotApplicable")]
        NotApplicable,

        [XmlEnum("Other")]
        Other,
    }
}
