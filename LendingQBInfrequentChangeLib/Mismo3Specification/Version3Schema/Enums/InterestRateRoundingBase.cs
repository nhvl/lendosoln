namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum InterestRateRoundingBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Down")]
        Down,

        [XmlEnum("Nearest")]
        Nearest,

        [XmlEnum("NoRounding")]
        NoRounding,

        [XmlEnum("Up")]
        Up,

    }
}
