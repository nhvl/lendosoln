namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum VATitleVestingBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("JointTwoOrMoreVeterans")]
        JointTwoOrMoreVeterans,

        [XmlEnum("JointVeteranAndNonVeteran")]
        JointVeteranAndNonVeteran,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Veteran")]
        Veteran,

        [XmlEnum("VeteranAndSpouse")]
        VeteranAndSpouse,

    }
}
