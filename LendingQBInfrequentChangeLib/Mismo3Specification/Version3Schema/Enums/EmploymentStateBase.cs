namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum EmploymentStateBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Employed")]
        Employed,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Retired")]
        Retired,

        [XmlEnum("Unemployed")]
        Unemployed,

    }
}
