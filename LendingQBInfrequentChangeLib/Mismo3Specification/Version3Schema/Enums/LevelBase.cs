namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum LevelBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Attic")]
        Attic,

        [XmlEnum("Basement")]
        Basement,

        [XmlEnum("LevelFive")]
        LevelFive,

        [XmlEnum("LevelFour")]
        LevelFour,

        [XmlEnum("LevelOne")]
        LevelOne,

        [XmlEnum("LevelThree")]
        LevelThree,

        [XmlEnum("LevelTwo")]
        LevelTwo,

        [XmlEnum("Other")]
        Other,

    }
}
