namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates the Texas refinance code of Texas Home Equity.
    /// </summary>
    public enum StateRefinanceProgramBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Indicates the New York refinance code of Consolidation Extension And Modification Agreement (CEMA).
		/// </summary>
        [XmlEnum("ConsolidationExtensionAndModificationAgreement")]
        ConsolidationExtensionAndModificationAgreement,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Indicates the Texas refinance code of Texas Home Equity. 
		/// </summary>
        [XmlEnum("TexasHomeEquity")]
        TexasHomeEquity,

    }
}
