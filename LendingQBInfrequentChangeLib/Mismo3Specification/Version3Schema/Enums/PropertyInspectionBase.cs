namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum PropertyInspectionBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("ExteriorAndInterior")]
        ExteriorAndInterior,

        [XmlEnum("ExteriorOnly")]
        ExteriorOnly,

        [XmlEnum("None")]
        None,

        [XmlEnum("Other")]
        Other,

    }
}
