namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Total price including tax
    /// </summary>
    public enum CreditReportPriceBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Total price not including tax
		/// </summary>
        [XmlEnum("Net")]
        Net,

		/// <summary>
		/// Other price type - List description in Price Type Other
		/// </summary>
        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Amount of local or state tax, if any
		/// </summary>
        [XmlEnum("Tax")]
        Tax,

		/// <summary>
		/// Total price including tax
		/// </summary>
        [XmlEnum("Total")]
        Total,

    }
}
