namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum FeeSummaryTotalFeesPaidToBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Broker")]
        Broker,

        [XmlEnum("Investor")]
        Investor,

        [XmlEnum("Lender")]
        Lender,

        [XmlEnum("Other")]
        Other,

    }
}
