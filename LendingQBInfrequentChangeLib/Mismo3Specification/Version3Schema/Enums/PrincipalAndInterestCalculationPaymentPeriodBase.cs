namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum PrincipalAndInterestCalculationPaymentPeriodBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("30DayPaymentPeriod")]
        Item30DayPaymentPeriod,

        [XmlEnum("NumberOfDaysBetweenPayments")]
        NumberOfDaysBetweenPayments,

        [XmlEnum("NumberOfDaysInCalendarMonth")]
        NumberOfDaysInCalendarMonth,

        [XmlEnum("Other")]
        Other,

    }
}
