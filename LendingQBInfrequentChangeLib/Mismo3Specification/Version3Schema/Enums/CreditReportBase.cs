namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// RMCR with additional foreign investigation.
    /// </summary>
    public enum CreditReportBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// This report type lists errors that caused the request transaction to be rejected
		/// </summary>
        [XmlEnum("Error")]
        Error,

		/// <summary>
		/// Credit report type used for performing a credit history search for undisclosed liabilities.
		/// </summary>
        [XmlEnum("LoanQuality")]
        LoanQuality,

		/// <summary>
		/// Credit report type used for performing a credit history search and comparing the updated data against the original report.
		/// </summary>
        [XmlEnum("LoanQualityCompare")]
        LoanQualityCompare,

		/// <summary>
		/// A merge report (also called merged infile) includes data on file from the selected credit data repositories.
		/// </summary>
        [XmlEnum("Merge")]
        Merge,

		/// <summary>
		/// An enhanced version of the Merge product. Contact credit bureau for details.
		/// </summary>
        [XmlEnum("MergePlus")]
        MergePlus,

		/// <summary>
		/// Provides a credit report showing only mortgage liabilities. The report may or may not include credit scores.
		/// </summary>
        [XmlEnum("MortgageOnly")]
        MortgageOnly,

		/// <summary>
		/// Provides credit evaluation for borrowers with minimal credit history.
		/// </summary>
        [XmlEnum("NonTraditional")]
        NonTraditional,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Provides credit risk scores only.
		/// </summary>
        [XmlEnum("RiskScoresOnly")]
        RiskScoresOnly,

		/// <summary>
		/// Residential Mortgage Credit Report is an enhanced credit product that contains verified information and/or additional investigation.
		/// </summary>
        [XmlEnum("RMCR")]
        RMCR,

		/// <summary>
		/// RMCR with additional foreign investigation.
		/// </summary>
        [XmlEnum("RMCRForeign")]
        RMCRForeign,

        [XmlEnum("Status")]
        Status,

    }
}
