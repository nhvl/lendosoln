namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum SpecialBorrowerEmployerRelationshipBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("EmployedByRelative")]
        EmployedByRelative,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("PropertySeller")]
        PropertySeller,

        [XmlEnum("RealEstateBroker")]
        RealEstateBroker,

    }
}
