namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// The lending limit amount that has been specified by the investor.
    /// </summary>
    public enum LendingLimitBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// The lending limit amount that has been calculated based on the guidelines for the mortgage type.
		/// </summary>
        [XmlEnum("Calculated")]
        Calculated,

		/// <summary>
		/// The lending limit amount based on the number of units and county location of the subject property.
		/// </summary>
        [XmlEnum("County")]
        County,

		/// <summary>
		/// The lending limit amount that has been specified by the investor.
		/// </summary>
        [XmlEnum("Investor")]
        Investor,

        [XmlEnum("Other")]
        Other,

    }
}
