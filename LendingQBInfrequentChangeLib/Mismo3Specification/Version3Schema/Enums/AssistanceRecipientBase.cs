namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum AssistanceRecipientBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Borrower")]
        Borrower,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("PrivateInvestor")]
        PrivateInvestor,

        [XmlEnum("Servicer")]
        Servicer,
    }
}
