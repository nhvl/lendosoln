namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum InvestorReportingAdditionalChargeAssessedToPartyBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Borrower")]
        Borrower,

        [XmlEnum("Investor")]
        Investor,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Servicer")]
        Servicer,

    }
}
