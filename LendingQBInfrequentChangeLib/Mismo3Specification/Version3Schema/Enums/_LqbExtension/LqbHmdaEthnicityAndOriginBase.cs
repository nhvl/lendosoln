﻿namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Contains all values from MISMO 3.4 HMDAEthnicityBase and HMDAEthnicityOriginBase
    /// to support the LendingQB extension datapoints containing LAR Ethnicity data.
    /// </summary>
    public enum LqbHmdaEthnicityAndOriginBase
    {
        /// <summary>
        /// Indicates Hispanic or Latino.
        /// </summary>
        [XmlEnum("HispanicOrLatino")]
        HispanicOrLatino,

        /// <summary>
        /// Indicates that ethnicity information was not provided.
        /// </summary>
        [XmlEnum("InformationNotProvidedByApplicantInMailInternetOrTelephoneApplication")]
        InformationNotProvidedByApplicantInMailInternetOrTelephoneApplication,

        /// <summary>
        /// Indicates that ethnicity information is not applicable.
        /// </summary>
        [XmlEnum("NotApplicable")]
        NotApplicable,

        /// <summary>
        /// Indicates not Hispanic or Latino.
        /// </summary>
        [XmlEnum("NotHispanicOrLatino")]
        NotHispanicOrLatino,

        /// <summary>
        /// Indicates Cuban ethnicity origin.
        /// </summary>
        [XmlEnum("Cuban")]
        Cuban,

        /// <summary>
        /// Indicates Mexican ethnicity origin.
        /// </summary>
        [XmlEnum("Mexican")]
        Mexican,

        /// <summary>
        /// Indicates Puerto Rican ethnicity origin.
        /// </summary>
        [XmlEnum("PuertoRican")]
        PuertoRican,

        /// <summary>
        /// Indicates other ethnicity origin.
        /// </summary>
        [XmlEnum("Other")]
        Other,

        /// <summary>
        /// Indicates a co-applicant datapoint when there is no co-applicant on the file.
        /// </summary>
        [XmlEnum("NoCoApplicant")]
        NoCoApplicant
    }
}
