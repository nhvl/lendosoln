﻿namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Specifies the relied-on credit score mode.
    /// </summary>
    public enum LqbReliedOnCreditScoreModeBase
    {
        /// <summary>
        /// Indicates an applicant's individual score.
        /// </summary>
        [XmlEnum("ApplicantsIndividualScore")]
        ApplicantsIndividualScore,

        /// <summary>
        /// Indicates a single score for the whole application.
        /// </summary>
        [XmlEnum("SingleScore")]
        SingleScore
    }
}
