﻿namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates the amortization type of a construction loan.
    /// </summary>
    public enum LqbConstructionLoanAmortizationBase
    {
        /// <summary>
        /// No value.
        /// </summary>
        [XmlEnum("")]
        Blank,

        /// <summary>
        /// A fixed rate loan.
        /// </summary>
        [XmlEnum("Fixed")]
        Fixed,

        /// <summary>
        /// An adjustable rate loan.
        /// </summary>
        [XmlEnum("AdjustableRate")]
        AdjustableRate
    }
}
