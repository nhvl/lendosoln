﻿namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates when the escrow account analysis is started.
    /// </summary>
    public enum LqbCdEscrowFirstYearAnalysisStartFromBase
    {
        /// <summary>
        /// Not applicable.
        /// </summary>
        [XmlEnum("")]
        Blank,

        /// <summary>
        /// One year from first payment date.
        /// </summary>
        [XmlEnum("FirstPayment")]
        FirstPayment,

        /// <summary>
        /// One year from consummation.
        /// </summary>
        [XmlEnum("Consummation")]
        Consummation
    }
}
