﻿using System.Xml.Serialization;

namespace Mismo3Specification.Version3Schema
{
    /// <summary>
    /// Indicates the TRID target regulation.
    /// </summary>
    public enum LqbTridTargetRegulationVersionBase
    {
        /// <summary>
        /// Indicates no data.
        /// </summary>
        [XmlEnum("")]
        Blank,

        /// <summary>
        /// Indicates the 2015 TRID regulations.
        /// </summary>
        [XmlEnum("TRID2015")]
        TRID2015,

        /// <summary>
        /// Indicates the 2017 TRID regulations.
        /// </summary>
        [XmlEnum("TRID2017")]
        TRID2017
    }
}
