﻿namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates the legal classification of a manufactured home.
    /// </summary>
    public enum LqbManufacturedHomeSecuredPropertyBase
    {
        /// <summary>
        /// A blank value.
        /// </summary>
        [XmlEnum("")]
        LeaveBlank,

        /// <summary>
        /// Indicates the legal classification includes the land.
        /// </summary>
        [XmlEnum("ManufacturedHomeAndLand")]
        ManufacturedHomeAndLand,

        /// <summary>
        /// Indicates the legal classification does not include the land.
        /// </summary>
        [XmlEnum("ManufacturedHomeAndNotLand")]
        ManufacturedHomeAndNotLand,

        /// <summary>
        /// Not applicable.
        /// </summary>
        [XmlEnum("NotApplicable")]
        NotApplicable
    }
}
