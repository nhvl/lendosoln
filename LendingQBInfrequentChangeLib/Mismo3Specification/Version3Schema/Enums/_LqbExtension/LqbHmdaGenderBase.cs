﻿namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates the gender of an applicant.
    /// </summary>
    public enum LqbHmdaGenderBase
    {
        [XmlEnum("")]
        LeaveBlank,

        [XmlEnum(nameof(ApplicantHasSelectedBothMaleAndFemale))]
        ApplicantHasSelectedBothMaleAndFemale,

        [XmlEnum(nameof(Female))]
        Female,

        [XmlEnum(nameof(InformationNotProvidedUnknown))]
        InformationNotProvidedUnknown,

        [XmlEnum(nameof(Male))]
        Male,

        [XmlEnum(nameof(NotApplicable))]
        NotApplicable,

        [XmlEnum(nameof(NoCoApplicant))]
        NoCoApplicant
    }
}
