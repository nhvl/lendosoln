﻿namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates an AUS used to underwrite the loan.
    /// </summary>
    public enum LqbHmdaAutomatedUnderwritingSystemBase
    {
        /// <summary>
        /// Desktop Underwriter.
        /// </summary>
        [XmlEnum("DesktopUnderwriter")]
        DesktopUnderwriter,

        /// <summary>
        /// Loan Prospector or Loan Product Advisor.
        /// </summary>
        [XmlEnum("LoanProspector")]
        LoanProspector,

        /// <summary>
        /// FHA TOTAL Scorecard.
        /// </summary>
        [XmlEnum("TotalScorecard")]
        TotalScorecard,

        /// <summary>
        /// Guaranteed Underwriting System.
        /// </summary>
        [XmlEnum("GuaranteedUnderwritingSystem")]
        GuaranteedUnderwritingSystem,

        /// <summary>
        /// Another AUS not captured here.
        /// </summary>
        [XmlEnum("Other")]
        Other,

        /// <summary>
        /// Not applicable.
        /// </summary>
        [XmlEnum("NotApplicable")]
        NotApplicable
    }
}
