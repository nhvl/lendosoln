﻿namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Specifies the property interest type for land containing a manufactured home.
    /// </summary>
    public enum LqbManufacturedHomeLandPropertyInterestBase
    {
        /// <summary>
        /// A blank value.
        /// </summary>
        [XmlEnum("")]
        LeaveBlank,

        /// <summary>
        /// The applicant directly owns the land.
        /// </summary>
        [XmlEnum("DirectOwnership")]
        DirectOwnership,

        /// <summary>
        /// The applicant indirectly owns the land.
        /// </summary>
        [XmlEnum("IndirectOwnership")]
        IndirectOwnership,

        /// <summary>
        /// The applicant pays to lease the land.
        /// </summary>
        [XmlEnum("PaidLeasehold")]
        PaidLeasehold,

        /// <summary>
        /// The applicant is leasing the land without payment.
        /// </summary>
        [XmlEnum("UnpaidLeasehold")]
        UnpaidLeasehold,

        /// <summary>
        /// Not applicable.
        /// </summary>
        [XmlEnum("NotApplicable")]
        NotApplicable
    }
}
