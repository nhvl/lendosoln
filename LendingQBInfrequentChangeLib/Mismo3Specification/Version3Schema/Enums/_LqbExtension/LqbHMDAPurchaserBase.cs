﻿namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    public enum LqbHmdaPurchaserBase
    {
        /// <summary>
        /// An affiliate of the loan originator.
        /// </summary>
        [XmlEnum("AffiliateInstitution")]
        AffiliateInstitution,

        /// <summary>
        /// A commercial bank, savings bank, or savings association.
        /// </summary>
        [XmlEnum("CommercialOrSavingsBank")]
        CommercialOrSavingsBank,

        /// <summary>
        /// Fannie Mae.
        /// </summary>
        [XmlEnum("FannieMae")]
        FannieMae,

        /// <summary>
        /// Farmer Mac.
        /// </summary>
        [XmlEnum("FarmerMac")]
        FarmerMac,

        /// <summary>
        /// A life insurance company, credit union, mortgage bank, or finance company.
        /// </summary>
        [XmlEnum("FinancialInstitution")]
        FinancialInstitution,

        /// <summary>
        /// Freddie Mac.
        /// </summary>
        [XmlEnum("FreddieMac")]
        FreddieMac,

        /// <summary>
        /// Ginnie Mae.
        /// </summary>
        [XmlEnum("GinnieMae")]
        GinnieMae,

        /// <summary>
        /// A non-depository institution that purchases covered loans and typically
        /// originates such loans.
        /// </summary>
        [XmlEnum("MortgageCompany")]
        MortgageCompany,

        /// <summary>
        /// Covered loan was not originated or was originated or purchased but was
        /// not sold to a secondary market entity in the applicable calendar year.
        /// </summary>
        [XmlEnum("NotApplicable")]
        NotApplicable,

        /// <summary>
        /// Any option not otherwise captured.
        /// </summary>
        [XmlEnum("Other")]
        Other,

        /// <summary>
        /// Private securitization by purchasers other than Ginnie Mae,
        /// Fannie Mae, Freddie Mac, or Farmer Mac.
        /// </summary>
        [XmlEnum("PrivateSecuritization")]
        PrivateSecuritization,

        /// <summary>
        /// A credit union, mortgage company, or finance company.
        /// </summary>
        [XmlEnum("CreditUnionMortgageCompanyFinanceCompany")]
        CreditUnionMortgageCompanyFinanceCompany,

        /// <summary>
        /// A life insurance credit union.
        /// </summary>
        [XmlEnum("LifeInsuranceCreditUnion")]
        LifeInsuranceCreditUnion,

        /// <summary>
        /// A life insurance company.
        /// </summary>
        [XmlEnum("LifeInsuranceCompany")]
        LifeInsuranceCompany
    }
}
