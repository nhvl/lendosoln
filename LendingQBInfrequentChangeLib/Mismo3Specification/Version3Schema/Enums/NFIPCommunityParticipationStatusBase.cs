namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// In the Flood Response DTD this enumerated value is Non-Participating. This is marked for deprecation for 3.x and will be changed to Non Participating at that time.
    /// </summary>
    public enum NFIPCommunityParticipationStatusBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Emergency")]
        Emergency,

		/// <summary>
		/// In the Flood Response DTD this enumerated value is Non-Participating. This is marked for deprecation for 3.x and will be changed to Non Participating at that time.
		/// </summary>
        [XmlEnum("NonParticipating")]
        NonParticipating,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Probation")]
        Probation,

        [XmlEnum("Regular")]
        Regular,

        [XmlEnum("Suspended")]
        Suspended,

        [XmlEnum("Unknown")]
        Unknown,

    }
}
