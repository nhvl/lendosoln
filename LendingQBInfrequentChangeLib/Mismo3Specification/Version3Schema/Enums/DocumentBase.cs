namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Describes the procedure for performing an analysis or making a determination and captures results of the work.
    /// </summary>
    public enum DocumentBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Describes the work that needs to be done to bring the house to FHA standards.  AKA: 203K Specification of Repairs.
		/// </summary>
        [XmlEnum("203KConsultantReport")]
        Item203KConsultantReport,

		/// <summary>
		/// Confirms the final bid amount for the project and the time frame for completion.  A contract between the homeowner and the contractor.  AKA: 203K Contractor Acknowledgement.
		/// </summary>
        [XmlEnum("203KHomeownerAcknowledgment")]
        Item203KHomeownerAcknowledgment,

		/// <summary>
		/// Provides information required to release funds based on work in place. Must be signed by contractor, borrower and inspector.
		/// </summary>
        [XmlEnum("203KInitialDrawRequest")]
        Item203KInitialDrawRequest,

		/// <summary>
		/// Determines Maximum amount for 203K Mortgage based on the loan amount; closing costs; sales price (if purchase) and work to be performed. Provided to the Lender.  AKA: MMW.
		/// </summary>
        [XmlEnum("203KMaximumMortgageWorksheet")]
        Item203KMaximumMortgageWorksheet,

		/// <summary>
		/// Establishes the terms and conditions of the 203K loan program works and describes the guidelines that will be followed in administering funds.
		/// </summary>
        [XmlEnum("203KRehabilitationAgreement")]
        Item203KRehabilitationAgreement,

		/// <summary>
		/// Establishes that the lender will not be responsible for replacing the abstract. 
		/// </summary>
        [XmlEnum("AbstractNoticeAgreement")]
        AbstractNoticeAgreement,

		/// <summary>
		/// Derived from a Judgment; contains only the details of a court action that relate to fulfilling the court order.
		/// </summary>
        [XmlEnum("AbstractOfJudgment")]
        AbstractOfJudgment,

		/// <summary>
		/// Establishes a direct debit from a borrowers account for loan or escrow payment. AKA: ACH.
		/// </summary>
        [XmlEnum("ACHDebitAuthorization")]
        ACHDebitAuthorization,

		/// <summary>
		/// Confirms that a borrower has received and understands the Notice Of Right To Cancel document.  Sometimes contained with the Notice Of Right To Cancel document.
		/// </summary>
        [XmlEnum("AcknowledgmentOfNoticeOfRightToCancel")]
        AcknowledgmentOfNoticeOfRightToCancel,

		/// <summary>
		/// Establishes the fact of death but does not contain personally identifiable information.
		/// </summary>
        [XmlEnum("AffidavitOfDeath")]
        AffidavitOfDeath,

		/// <summary>
		/// Informs the borrower of any Lender-related businesses (i.e., subsidiaries or affiliates) and borrower options related to using these providers. This document is a part of HUD Regulation X (RESPA/Reg X).
		/// </summary>
        [XmlEnum("AffiliatedBusinessArrangementDisclosure")]
        AffiliatedBusinessArrangementDisclosure,

		/// <summary>
		/// Confirms that a borrower is aware of presence of airport and possible noise situations. Provided by Borrower.
		/// </summary>
        [XmlEnum("AirportNoisePollutionAgreement")]
        AirportNoisePollutionAgreement,

		/// <summary>
		/// Adds HUD required conditions to the Purchase Agreement. Escape clause allowing a borrower to exit transaction if it does not meet value.  These do not have HUD number but are required in Purchase Agreement.  AKA: Real Estate Certification.
		/// </summary>
        [XmlEnum("AmendatoryClause")]
        AmendatoryClause,

		/// <summary>
		/// Describes interest and principal payments to a borrower from a lender, or lender to borrower, and their effect on the balance of the loan.  AKA: Home Equity Conversion Mortgage (HECM) Amortization Schedule.
		/// </summary>
        [XmlEnum("AmortizationSchedule")]
        AmortizationSchedule,

		/// <summary>
		/// A Recertification of Value is performed to confirm whether or not the conditions of a prior appraisal have been met. It does not change the effective date of the value opinion. It is often used for the final inspection or completion certificate for a newly constructed home when the original appraisal was done using plans or specs or when the house was partially complete. It is also used when the original appraisal was subject to repairs or alterations to be made to the house. Confirms that the appraised value of a home has not decreased. The use of the document type includes the Fannie Mae 1004D and Freddie Mac 442.
		/// </summary>
        [XmlEnum("AppraisalRecertification")]
        AppraisalRecertification,

		/// <summary>
		/// A report produced by an appropriately qualified, competent and independent appraiser in accordance with USPAP and in which the scope of work includes the development of a value conclusion.
		/// </summary>
        [XmlEnum("AppraisalReport")]
        AppraisalReport,

		/// <summary>
		/// Documents the results of a independent evaluation of an appraisal report and is usually conducted from a checklist and is usually focused on completeness and consistency.
		/// </summary>
        [XmlEnum("AppraisalReview")]
        AppraisalReview,

		/// <summary>
		/// Requests an internal review and evaluation of an appraisal. 
		/// </summary>
        [XmlEnum("AppraisalReviewRequest")]
        AppraisalReviewRequest,

		/// <summary>
		/// Describes the loan approval status and any associated conditions to the borrower. AKA: Notice of Loan Approval, NOLA, Commitment Letter, Conditional Approval, Conditional Commitment.
		/// </summary>
        [XmlEnum("ApprovalLetter")]
        ApprovalLetter,

		/// <summary>
		/// Establishes the purpose and structure of a corporation, also percentages of ownership.
		/// </summary>
        [XmlEnum("ArticlesOfIncorporation")]
        ArticlesOfIncorporation,

		/// <summary>
		/// Transfers the beneficial interest in a instrument from one party to another.
		/// </summary>
        [XmlEnum("Assignment")]
        Assignment,

        [XmlEnum("Assignment:AssignmentOfDeedOfTrust")]
        AssignmentAssignmentOfDeedOfTrust,

        [XmlEnum("Assignment:AssignmentOfMortgage")]
        AssignmentAssignmentOfMortgage,

        [XmlEnum("Assignment:AssignmentOfRents")]
        AssignmentAssignmentOfRents,

        [XmlEnum("Assignment:AssignmentOfTrade")]
        AssignmentAssignmentOfTrade,

        [XmlEnum("Assignment:BlanketAssignment")]
        AssignmentBlanketAssignment,

        [XmlEnum("Assignment:CooperativeAssignmentOfProprietaryLease")]
        AssignmentCooperativeAssignmentOfProprietaryLease,

		/// <summary>
		/// Establishes that the buyer will assume responsibility for the obligations of a mortgage from the builder or original owner.
		/// </summary>
        [XmlEnum("AssumptionAgreement")]
        AssumptionAgreement,

		/// <summary>
		/// Describes escrowed repairs and certifies that escrowed repairs will be completed.  Provided by Lender.  AKA: Mortgagee's Assurance of Completion.
		/// </summary>
        [XmlEnum("AssuranceOfCompletion")]
        AssuranceOfCompletion,

		/// <summary>
		/// Confirms that a person is under a Power of Attorney and warrants specifics facts about the person such as: POA has not been revoked, principle has not died, no divorce action has been commenced if spouse of the principle, a guardian or conservator has not been appointed for the Principle.
		/// </summary>
        [XmlEnum("AttorneyInFactAffidavit")]
        AttorneyInFactAffidavit,

		/// <summary>
		/// Addresses trust or other property matters and confirms the legality of the information that is being provided to the lender.  This could consist of beneficiary information, assets used as collateral, trustee and revocable nature of trusts. In Some States, Title insurance is provided via an Attorney's Opinion Letter.  AKA: Title Opinion, Opinion of Abstract.
		/// </summary>
        [XmlEnum("AttorneysOpinionLetter")]
        AttorneysOpinionLetter,

		/// <summary>
		/// Confirms results from Desk Top Underwriter, Loan Prospector or other underwriting system, including the loan decision and conditions or stipulations of the approval. AKA: DU Findings, LP Feedback, Feedback Certificate, Findings, Feedback, Findings Report and Underwriting Analysis, AUS Findings.
		/// </summary>
        [XmlEnum("AutomatedUnderwritingFeedback")]
        AutomatedUnderwritingFeedback,

		/// <summary>
		/// Confirms results from an automated property valuation system. AKA: AVM, Automated Value Model.
		/// </summary>
        [XmlEnum("AVMFeedback")]
        AVMFeedback,

		/// <summary>
		/// Confirms that a company will return to the funding lender the collateral package, the funded value, or mortgage-backed security value of a property.
		/// </summary>
        [XmlEnum("BaileeLetter")]
        BaileeLetter,

		/// <summary>
		/// Informs borrower of what will happen when lender is instructed to pay off debt on behalf of a borrower directly from a Home Equity account
		/// </summary>
        [XmlEnum("BalanceTransferAuthorizationNotice")]
        BalanceTransferAuthorizationNotice,

		/// <summary>
		/// Informs the borrower that rate and payment amount may change when the refinance option is chosen for balloon loans and describes how the interest rate and payments are determined. AKA: Convertible Balloon Disclosure.
		/// </summary>
        [XmlEnum("BalloonRefinanceDisclosure")]
        BalloonRefinanceDisclosure,

		/// <summary>
		/// Provides written evidence of a deposit to a financial institution.
		/// </summary>
        [XmlEnum("BankDepositSlip")]
        BankDepositSlip,

		/// <summary>
		/// Confirms that a person or company is legally free and clear to repay certain debts.  Provided by the court.
		/// </summary>
        [XmlEnum("BankruptcyDischargeNotice")]
        BankruptcyDischargeNotice,

		/// <summary>
		/// Describes transactions recorded and account balances of the accounts of a customer during a specified period. AKA: Account Statement.
		/// </summary>
        [XmlEnum("BankStatement")]
        BankStatement,

        [XmlEnum("BankStatement:401K")]
        BankStatement401K,

        [XmlEnum("BankStatement:CheckingAccount")]
        BankStatementCheckingAccount,

        [XmlEnum("BankStatement:MutualFundAccount")]
        BankStatementMutualFundAccount,

        [XmlEnum("BankStatement:SavingAccount")]
        BankStatementSavingAccount,

        [XmlEnum("BankStatement:StockAccount")]
        BankStatementStockAccount,

		/// <summary>
		/// Outlines the details for distribution of funds from a benefit plan.
		/// </summary>
        [XmlEnum("BenefitPlanDistributionStatement")]
        BenefitPlanDistributionStatement,

        [XmlEnum("BenefitPlanDistributionStatement:401K")]
        BenefitPlanDistributionStatement401K,

        [XmlEnum("BenefitPlanDistributionStatement:Annuity")]
        BenefitPlanDistributionStatementAnnuity,

        [XmlEnum("BenefitPlanDistributionStatement:Pension")]
        BenefitPlanDistributionStatementPension,

        [XmlEnum("BenefitPlanDistributionStatement:Trust")]
        BenefitPlanDistributionStatementTrust,

		/// <summary>
		/// Describes required home repairs or improvements and provides an estimation of cost to complete. AKA: Estimate to Complete Work, Repair Estimate, Alteration Improvement and Repairs.
		/// </summary>
        [XmlEnum("Bid")]
        Bid,

		/// <summary>
		/// Confirms details about the birth of an individual.  An official record provided by a government entity such as a state, county, city or borough.
		/// </summary>
        [XmlEnum("BirthCertificate")]
        BirthCertificate,

		/// <summary>
		/// Confirms the ownership of a bond.
		/// </summary>
        [XmlEnum("BondCertificate")]
        BondCertificate,

		/// <summary>
		/// Confirms that the borrowers understand the condition of the property.
		/// </summary>
        [XmlEnum("BorrowerAcknowledgmentOfPropertyCondition")]
        BorrowerAcknowledgmentOfPropertyCondition,

		/// <summary>
		/// Describes items related to the application including borrower credit, income, employment or assets. Typically provided by borrower. Includes items provided by third parties in support of the application of the borrower. Documents provided by the lender to be signed by the borrower are not considered Borrower Correspondence.
		/// </summary>
        [XmlEnum("BorrowerCorrespondence")]
        BorrowerCorrespondence,

        [XmlEnum("BorrowerCorrespondence:GapInEmployment")]
        BorrowerCorrespondenceGapInEmployment,

        [XmlEnum("BorrowerCorrespondence:LetterOfExplanation")]
        BorrowerCorrespondenceLetterOfExplanation,

        [XmlEnum("BorrowerCorrespondence:LetterOfIntent")]
        BorrowerCorrespondenceLetterOfIntent,

		/// <summary>
		/// Under Section 6 of the Real Estate Settlement Procedures Act, a written request to the loan servicer which identifies the borrower by name and account, includes a statement of reasons why the borrower believes the mortgage account is in error, and which should include the words qualified written request. The servicer has legal deadlines to respond to a qualified written request.
		/// </summary>
        [XmlEnum("BorrowerCorrespondence:QualifiedWrittenRequest")]
        BorrowerCorrespondenceQualifiedWrittenRequest,

        [XmlEnum("BorrowerCorrespondence:TrailingSpouse")]
        BorrowerCorrespondenceTrailingSpouse,

		/// <summary>
		/// Verifies that there are no outstanding liens or mortgages that would not allow for the appropriate lien position of a new loan. AKA: Borrowers Affidavit.
		/// </summary>
        [XmlEnum("BorrowerLienAffidavit")]
        BorrowerLienAffidavit,

		/// <summary>
		/// Authorizes lender to request personal information on the borrower's behalf. AKA: Borrowers Authorization, General Certification and Authorization.
		/// </summary>
        [XmlEnum("BorrowersCertification")]
        BorrowersCertification,

		/// <summary>
		/// Certification that the borrower will not use a multiple dwelling property for hotel or transient housing. AKA: Hotel Transient, HUD 92561.
		/// </summary>
        [XmlEnum("BorrowersContractWithRespectToHotelAndTransientUseOfProperty")]
        BorrowersContractWithRespectToHotelAndTransientUseOfProperty,

		/// <summary>
		/// Describes sources and amounts of compensation a brokered mortgage transaction.  Provided by Broker to Borrower.
		/// </summary>
        [XmlEnum("BrokerDisclosureStatement")]
        BrokerDisclosureStatement,

		/// <summary>
		/// A report produced by a real estate sales person or broker which provides an estimation of the subject property's most probable selling price and a listing of comparable sales. The report typically combines information from a drive-by exterior inspection and various external data sources, such as MLS, but not an interior inspection. AKA: BPO.
		/// </summary>
        [XmlEnum("BrokerPriceOpinion")]
        BrokerPriceOpinion,

		/// <summary>
		/// Confirms that the property construction is completed to requirements. Provided by builder.  AKA: Termite Treatment Builders Certificate (99A).
		/// </summary>
        [XmlEnum("BuildersCertification")]
        BuildersCertification,

        [XmlEnum("BuildersCertification:BuilderCertificationOfPlansAndSpecifications")]
        BuildersCertificationBuilderCertificationOfPlansAndSpecifications,

        [XmlEnum("BuildersCertification:BuildersCertificate")]
        BuildersCertificationBuildersCertificate,

        [XmlEnum("BuildersCertification:PropertyInspection")]
        BuildersCertificationPropertyInspection,

        [XmlEnum("BuildersCertification:TermiteTreatment")]
        BuildersCertificationTermiteTreatment,

		/// <summary>
		/// Authorizes the builder to build on the property. Provided by local municipal authority.
		/// </summary>
        [XmlEnum("BuildingPermit")]
        BuildingPermit,

		/// <summary>
		/// Authorizes the legal operation of a business in an area issued by the state or municipality.
		/// </summary>
        [XmlEnum("BusinessLicense")]
        BusinessLicense,

		/// <summary>
		/// Establishes a rate variant, or temporary adjustment, of interest rate on the loan.  Used to reduce, or step down, an interest rate.
		/// </summary>
        [XmlEnum("BuydownAgreement")]
        BuydownAgreement,

		/// <summary>
		/// Describes the settlement process, charges, rights and remedies available under RESPA, as well as explanation of settlement services and costs. AKA: Settlement Cost Booklet.
		/// </summary>
        [XmlEnum("BuyingYourHomeSettlementCostsAndHelpfulInformation")]
        BuyingYourHomeSettlementCostsAndHelpfulInformation,

		/// <summary>
		/// Confirms results from the HUD Credit Alert Interactive Voice Response System, which indicates whether the individual is in default on direct or guaranteed federal loans or are otherwise ineligible for an FHA loan.  Result is transcribed or captured as a screen-print from the HUD website. AKA: CAIVRS.
		/// </summary>
        [XmlEnum("CAIVRSAuthorization")]
        CAIVRSAuthorization,

		/// <summary>
		/// Confirms that a property is no longer listed with a real estate agent.
		/// </summary>
        [XmlEnum("CancellationOfListing")]
        CancellationOfListing,

		/// <summary>
		/// Represents money payable or paid (if cancelled). AKA: Checks, Cancelled Checks.
		/// </summary>
        [XmlEnum("Check")]
        Check,

		/// <summary>
		/// Describes the list of action items, steps, or elements required to be consulted or completed to accomplish a task.
		/// </summary>
        [XmlEnum("Checklist")]
        Checklist,

		/// <summary>
		/// Describes the legal obligation of a parent to pay money toward the care and maintenance of his/her children.
		/// </summary>
        [XmlEnum("ChildSupportVerification")]
        ChildSupportVerification,

		/// <summary>
		/// Requests that a line of credit be closed after receiving a payoff under a separate cover. AKA: account Closure Letter.
		/// </summary>
        [XmlEnum("CloseLineOfCreditRequest")]
        CloseLineOfCreditRequest,

		/// <summary>
		/// The CFPB Closing Disclosure document. Provides disclosures that help consumers in understanding all of the costs of the transaction.
		/// </summary>
        [XmlEnum("ClosingDisclosure")]
        ClosingDisclosure,

		/// <summary>
		/// Describes the conditions that must be met in order to complete the closing. Provided by Lender to Closing Agent. AKA: Lender Closing Instructions, Settlement Agent Instructions.
		/// </summary>
        [XmlEnum("ClosingInstructions")]
        ClosingInstructions,

		/// <summary>
		/// A document the collection officer uses to record all collections related to the Single Family Property Disposition Program (Section 204(g)). The document provides the initial point of an audit trail regarding all collections. Form HUD-235 (5/89).
		/// </summary>
        [XmlEnum("CollectionRegister")]
        CollectionRegister,

		/// <summary>
		/// The lender uses a comparative income analysis document to compare the performance of a self-employed borrower's business over a period of years to determine its viability. Fannie Mae Form 1088 July 96.
		/// </summary>
        [XmlEnum("ComparativeIncomeAnalysis")]
        ComparativeIncomeAnalysis,

		/// <summary>
		/// Establishes that the borrower will accommodate changes to loan documentation that result from errors and omissions, to ensure that the documents conform to industry standards. AKA: Errors and Omissions, E & O.
		/// </summary>
        [XmlEnum("ComplianceAgreement")]
        ComplianceAgreement,

		/// <summary>
		/// Confirms that property construction, repairs or improvements related to the loan (but not to the appraisal per se) have been completed acceptably. Provided by an appraiser or inspector. AKA: HUD 92051, Final Inspection.
		/// </summary>
        [XmlEnum("ComplianceInspectionReport")]
        ComplianceInspectionReport,

		/// <summary>
		/// Establishes the property value and describes repair conditions.  Provided by Underwriter. This is only related to appraisal and property.
		/// </summary>
        [XmlEnum("ConditionalCommitment")]
        ConditionalCommitment,

		/// <summary>
		/// Confirms the percentage of a Condominium (Condo) Association or phase is currently occupied.
		/// </summary>
        [XmlEnum("CondominiumOccupancyCertificate")]
        CondominiumOccupancyCertificate,

		/// <summary>
		/// Authorizes a person to sign documents for a borrower.
		/// </summary>
        [XmlEnum("ConservatorAndGuardianshipAgreement")]
        ConservatorAndGuardianshipAgreement,

		/// <summary>
		/// Describes in detail the construction costs to build a new structure.
		/// </summary>
        [XmlEnum("ConstructionCostBreakdown")]
        ConstructionCostBreakdown,

		/// <summary>
		/// Describes Adjustable Rate Mortgages and reviews their benefits and risks. AKA: CHARM.
		/// </summary>
        [XmlEnum("ConsumerHandbookOnARM")]
        ConsumerHandbookOnARM,

		/// <summary>
		/// A document that notifies the borrower of upcoming conversion options and scheduled interest rate changes..
		/// </summary>
        [XmlEnum("ConversionOptionNotice")]
        ConversionOptionNotice,

		/// <summary>
		/// Transfers title to a property from a seller (grantor) to a recipient (grantee); provides evidence of the legal right of ownership.
		/// </summary>
        [XmlEnum("ConveyanceDeed")]
        ConveyanceDeed,

        [XmlEnum("ConveyanceDeed:BargainAndSaleDeed")]
        ConveyanceDeedBargainAndSaleDeed,

        [XmlEnum("ConveyanceDeed:QuitClaimDeed")]
        ConveyanceDeedQuitClaimDeed,

        [XmlEnum("ConveyanceDeed:WarrantyDeed")]
        ConveyanceDeedWarrantyDeed,

		/// <summary>
		/// Contains data about the property sale and conveyance tax calculations. Accompanies a Conveyance Deed at the time of recording. AKA: Deed Tax Form; Conveyance Tax Return.
		/// </summary>
        [XmlEnum("ConveyanceTaxForm")]
        ConveyanceTaxForm,

		/// <summary>
		/// Establishes the rules for governing and operating the Cooperative (Coop) Housing Corporation.
		/// </summary>
        [XmlEnum("CooperativeBylaws")]
        CooperativeBylaws,

		/// <summary>
		/// Describes the planned revenues and expenditures of the Cooperative (Coop) for the current fiscal year.
		/// </summary>
        [XmlEnum("CooperativeOperatingBudget")]
        CooperativeOperatingBudget,

		/// <summary>
		/// Establishes the rights and duties of the Cooperative (Coop) and member shareholder under which the shareholder is allowed to use a certain unit in the premises.
		/// </summary>
        [XmlEnum("CooperativeProprietaryLease")]
        CooperativeProprietaryLease,

		/// <summary>
		/// Establishes the responsibility of the Cooperative (Coop)  to notify the lender in the event of borrower default.
		/// </summary>
        [XmlEnum("CooperativeRecognitionAgreement")]
        CooperativeRecognitionAgreement,

		/// <summary>
		/// Confirms an individual's ownership interest in the Cooperative. AKA: Membership certificate/Perpetual Use & Equity Contract/Certificate of Ownership/Ownership Contract, Coop Stock Certificate.    
		/// </summary>
        [XmlEnum("CooperativeStockCertificate")]
        CooperativeStockCertificate,

		/// <summary>
		/// Authorizes the lender to assign the borrowers stock certificate in case of default. AKA: Coop Stock Power.
		/// </summary>
        [XmlEnum("CooperativeStockPower")]
        CooperativeStockPower,

		/// <summary>
		/// Informs a cosigner of a loan that they will be obligated to pay the debt if there is a default by  the main  borrowers on the loan.
		/// </summary>
        [XmlEnum("CosignerNotice")]
        CosignerNotice,

		/// <summary>
		/// Confirms that the property was constructed to CABO (Council of American Building Officials) standards. Provided by builder. AKA: CABO Certification
		/// </summary>
        [XmlEnum("CouncilOfAmericanBuildingOfficialsCertification")]
        CouncilOfAmericanBuildingOfficialsCertification,

		/// <summary>
		/// Confirms that the borrower has completed counseling related to their mortgage and property obligations
		/// </summary>
        [XmlEnum("CounselingCertification")]
        CounselingCertification,

		/// <summary>
		/// Informs the borrower of certain Veterans' Affairs items of interest regarding the loan and financing eligibility. AKA: VA 26-0592.
		/// </summary>
        [XmlEnum("CounselingChecklistForMilitaryHomebuyers")]
        CounselingChecklistForMilitaryHomebuyers,

		/// <summary>
		/// Authorizes a company to use a credit card belonging to a client  for mortgage related charges.  Generally these charges consist of credit report and appraisal fee.
		/// </summary>
        [XmlEnum("CreditCardAuthorization")]
        CreditCardAuthorization,

		/// <summary>
		/// Establishes the rates and charges incurred for credit insurance. AKA: Credit Insurance Terms, Credit Life Insurance Disclosure.
		/// </summary>
        [XmlEnum("CreditInsuranceAgreement")]
        CreditInsuranceAgreement,

		/// <summary>
		/// Describes the borrowers credit history. Provided by a credit reporting bureau, or a service which consolidates reports from multiple sources into a single report (Tri-merge). AKA: Tri-Merge, Residential Mortgage Credit Report (RMCR).
		/// </summary>
        [XmlEnum("CreditReport")]
        CreditReport,

		/// <summary>
		/// Verifies the identity of any person seeking to open an account; maintains records of the information used to verify the person's identity; and  determines whether the person appears on any lists of known or suspected terrorists. AKA: U.S. Patriot Act, Important Applicant Information.
		/// </summary>
        [XmlEnum("CustomerIdentificationVerification")]
        CustomerIdentificationVerification,

		/// <summary>
		/// Confirms the date, location and cause of the death of a person.  Issued by a government official.
		/// </summary>
        [XmlEnum("DeathCertificate")]
        DeathCertificate,

		/// <summary>
		/// Establishes the terms of the divorce.  Provided by a court or legal authority. AKA: Final Judgment (divorce), Abridgment of Judgment (divorce).
		/// </summary>
        [XmlEnum("DivorceDecree")]
        DivorceDecree,

		/// <summary>
		/// A document that expresses the sequence in which documents occur within a document set. The investor often dictates the document sequence to simplify document processing. A document set sometimes includes the DocumentSequence document itself. AKA: Documentation Order, Stacking Sheet.
		/// </summary>
        [XmlEnum("DocumentSequence")]
        DocumentSequence,

		/// <summary>
		/// A document which authorizes a person to operate a motor vehicle on a public roadway. Evidences a person's identity and driver's license identifier.
		/// </summary>
        [XmlEnum("DriversLicense")]
        DriversLicense,

		/// <summary>
		/// Establishes a repeatable direct deposit of funds into an account of a borrower. Used by servicing. AKA: EFT, Automatic Funds Transfer.
		/// </summary>
        [XmlEnum("ElectronicFundsTransfer")]
        ElectronicFundsTransfer,

		/// <summary>
		/// Certifies elevation information necessary to ensure compliance with community floodplain management ordinances, to determine the proper insurance premium rate, and to support a request for a Letter of Map Amendment (LOMA) or Letter of Map Revision based on fill (LOMR-F).
		/// </summary>
        [XmlEnum("ElevationCertificate")]
        ElevationCertificate,

		/// <summary>
		/// A document issued by an employer which evidences a person's identity and employee identifier.
		/// </summary>
        [XmlEnum("EmployeeIdentification")]
        EmployeeIdentification,

		/// <summary>
		/// Determines loan amount for Energy Efficient Mortgage loan (loan to finance repairs that increase energy efficiency of the home). Required by HUD.  Provided by Underwriter.
		/// </summary>
        [XmlEnum("EnergyEfficientMortgageWorksheet")]
        EnergyEfficientMortgageWorksheet,

		/// <summary>
		/// Informs the borrower that all consumers are entitled to an equal chance to obtain credit. AKA: Reg B, ECOA.
		/// </summary>
        [XmlEnum("EqualCreditOpportunityActDisclosure")]
        EqualCreditOpportunityActDisclosure,

		/// <summary>
		/// Establishes escrow accounts for taxes and insurance. Agreement between borrower and lender.
		/// </summary>
        [XmlEnum("EscrowAgreement")]
        EscrowAgreement,

		/// <summary>
		/// Establishes work to be completed on a property and the terms under which the funds for the work will be released. AKA: Holdback, Work Escrow Agreement.
		/// </summary>
        [XmlEnum("EscrowForCompletionAgreement")]
        EscrowForCompletionAgreement,

		/// <summary>
		/// Confirms work on the property has been completed and any funds held in escrow can be released. AKA: Work Escrow Mortgagee Assurance of Completion (2300).
		/// </summary>
        [XmlEnum("EscrowForCompletionLetter")]
        EscrowForCompletionLetter,

		/// <summary>
		/// Confirms the lender agrees to waive collecting a monthly payment for taxes and insurance (escrow or impound account). Informs the applicant of their responsibilities to pay tax/insurance bills when due, whether an escrow waiver fee will be imposed, and information regarding lender placement of insurance. AKA: Notice Regarding Payments For Homeowners Insurance And Property Taxes, Escrow Account Notice.
		/// </summary>
        [XmlEnum("EscrowWaiver")]
        EscrowWaiver,

		/// <summary>
		/// Informs the borrower of estimated loan closing costs.
		/// </summary>
        [XmlEnum("EstimateOfClosingCostsPaidToThirdParty")]
        EstimateOfClosingCostsPaidToThirdParty,

		/// <summary>
		/// Establishes that certain statements of fact are correct as of the date of the statement and can be relied upon by a third party (including a prospective lender or purchaser).
		/// </summary>
        [XmlEnum("EstoppelAgreement")]
        EstoppelAgreement,

		/// <summary>
		/// Informs the borrower of their credit score, range of possible scores, key factors affecting score, reporting agencies used, contacts for obtaining a report copy, and includes a statement that terms offered are based on consumer report information.
		/// </summary>
        [XmlEnum("FACTACreditScoreDisclosure")]
        FACTACreditScoreDisclosure,

		/// <summary>
		/// Informs the borrower that it is illegal to discriminate in lending practices. This is different that the Equal Credit Opportunity Act. AKA: The Housing Financial Discrimination Act of 1977 Fair Lending Notice.
		/// </summary>
        [XmlEnum("FairLendingNotice")]
        FairLendingNotice,

		/// <summary>
		/// Informs the borrower that the lender is offering an insurance product or annuity in connection with an extension of credit. AKA: AntiCoercion Disclosure.
		/// </summary>
        [XmlEnum("FederalApplicationInsuranceDisclosure")]
        FederalApplicationInsuranceDisclosure,

		/// <summary>
		/// Informs the borrower that the lender is selling credit insurance as part of the loan transaction.
		/// </summary>
        [XmlEnum("FederalSaleOfInsuranceDisclosure")]
        FederalSaleOfInsuranceDisclosure,

		/// <summary>
		/// Determines the appropriate amount for an FHA streamlined refinance loan.
		/// </summary>
        [XmlEnum("FHA_MIPremiumNettingAuthorization")]
        FHA_MIPremiumNettingAuthorization,

		/// <summary>
		/// Confirms that the borrower waives the requirement to receive the Notice to Homebuyer five days before closing.
		/// </summary>
        [XmlEnum("FHAFiveDayWaiver")]
        FHAFiveDayWaiver,

		/// <summary>
		/// Confirms that a borrower is not recorded on the LDP/GSA (Limited Denial of Participation/General Services Administration) lists, and therefore is eligible for an FHA loan.
		/// </summary>
        [XmlEnum("FHALimitedDenialOfParticipationGeneralServicesAdministrationChecklist")]
        FHALimitedDenialOfParticipationGeneralServicesAdministrationChecklist,

		/// <summary>
		/// Determines the appropriate amount for an FHA loan. AKA: MCAW.
		/// </summary>
        [XmlEnum("FHAMortgageCreditAnalysisWorksheet")]
        FHAMortgageCreditAnalysisWorksheet,

		/// <summary>
		/// Confirms that all requirements have been met prior to FHA foreclosure. Provided to FHA.
		/// </summary>
        [XmlEnum("FHAReferralChecklist")]
        FHAReferralChecklist,

		/// <summary>
		/// Determines loan amount for refinance loan, included with or attached to the MCAW.  Required by HUD, provided by Underwriter.
		/// </summary>
        [XmlEnum("FHARefinanceMaximumMortgageWorksheet")]
        FHARefinanceMaximumMortgageWorksheet,

		/// <summary>
		/// Describes financial performance of a business or individual.
		/// </summary>
        [XmlEnum("FinancialStatement")]
        FinancialStatement,

        [XmlEnum("FinancialStatement:BalanceSheet")]
        FinancialStatementBalanceSheet,

        [XmlEnum("FinancialStatement:CashFlow")]
        FinancialStatementCashFlow,

        [XmlEnum("FinancialStatement:FinancialPosition")]
        FinancialStatementFinancialPosition,

        [XmlEnum("FinancialStatement:Income")]
        FinancialStatementIncome,

		/// <summary>
		/// Informs the borrower that flood insurance may be required by the lender. AKA: Special Flood Hazard Notice, Flood Hazard Notification.
		/// </summary>
        [XmlEnum("FloodHazardNotice")]
        FloodHazardNotice,

		/// <summary>
		/// Establishes that a borrower will maintain flood insurance in the amount prescribed by the lender for the duration of the loan term.
		/// </summary>
        [XmlEnum("FloodInsuranceAgreement")]
        FloodInsuranceAgreement,

		/// <summary>
		/// Confirms that a borrower has received the information that the borrower should consider a home inspection.
		/// </summary>
        [XmlEnum("ForYourProtectionHomeInspection")]
        ForYourProtectionHomeInspection,

		/// <summary>
		/// Confirms that a loan meets Freddie Mac streamlined refinance criteria. Provided to Freddie Mac.
		/// </summary>
        [XmlEnum("FREOwnedStreamlineRefinanceChecklist")]
        FREOwnedStreamlineRefinanceChecklist,

		/// <summary>
		/// Confirms final closing information and numbers on a brokered or third party closing. AKA: Funding Worksheet.
		/// </summary>
        [XmlEnum("FundingTransmittal")]
        FundingTransmittal,

		/// <summary>
		/// Confirms receipt of all the disclosures in the Disclosure Booklet,  HUD Guide to Settlement Costs. Also used to document the float/lock rate requests and mortgage insurance requests and provide the Appraisal Notice to the borrower. AKA: GLAD.
		/// </summary>
        [XmlEnum("GeneralLoanAcknowledgment")]
        GeneralLoanAcknowledgment,

		/// <summary>
		/// Describes the approximate costs borrower will pay at or before settlement, based on common practice in the locality. The mortgage banker or mortgage broker, if any, must deliver or mail the GFE to the applicant within three business days after the application is received. This document is a part of HUD Regulation X (RESPA/Reg X). AKA: Good Faith Estimate.
		/// </summary>
        [XmlEnum("GFE")]
        GFE,

		/// <summary>
		/// Confirms that the borrower is receiving all or part of the down payment as a gift.  Provided by gift donor.
		/// </summary>
        [XmlEnum("GiftLetter")]
        GiftLetter,

		/// <summary>
		/// Establishes participation and terms in a Group Savings Plan.
		/// </summary>
        [XmlEnum("GroupSavingsAgreement")]
        GroupSavingsAgreement,

		/// <summary>
		/// Informs the borrower that the costs associated with the new HECM loan will not exceed the loan amount.
		/// </summary>
        [XmlEnum("HECMAntiChurningDisclosure")]
        HECMAntiChurningDisclosure,

		/// <summary>
		/// Evidence, often a screen print, of calculation for HECM principal limit and monthly payment amount.
		/// </summary>
        [XmlEnum("HECMCalculationEvidence")]
        HECMCalculationEvidence,

		/// <summary>
		/// Confirms that a borrower will choose an FHA Insured HECM loan. Provided by the Lender.
		/// </summary>
        [XmlEnum("HECMChoiceOfInsuranceOptionsNotice")]
        HECMChoiceOfInsuranceOptionsNotice,

		/// <summary>
		/// Informs the borrower of right to waive HECM counseling requirements if the original loan is originated within five years of the preceding loan.
		/// </summary>
        [XmlEnum("HECMCounselingWaiverQualification")]
        HECMCounselingWaiverQualification,

		/// <summary>
		/// Establishes additional time to sell a property associated with a Home Equity Conversion Mortgage.
		/// </summary>
        [XmlEnum("HECMExtension")]
        HECMExtension,

		/// <summary>
		/// Confirms that a borrower has met with a lending representative, in person, at some point during the HECM loan origination process.
		/// </summary>
        [XmlEnum("HECMFaceToFaceCertification")]
        HECMFaceToFaceCertification,

		/// <summary>
		/// Describes final loan information and figures in a HECM transaction.
		/// </summary>
        [XmlEnum("HECMLoanSubmissionSchedule")]
        HECMLoanSubmissionSchedule,

		/// <summary>
		/// Informs the borrower of the steps to take if payment is not received from the HECM lender.
		/// </summary>
        [XmlEnum("HECMNoticeToBorrower")]
        HECMNoticeToBorrower,

		/// <summary>
		/// Describes the charges associated with the HECM loan and the payments made to the borrower.
		/// </summary>
        [XmlEnum("HECMPaymentPlan")]
        HECMPaymentPlan,

		/// <summary>
		/// Determines whether the loan is high cost or predatory based on state or national statutes. AKA: Anti-Predatory Worksheet.
		/// </summary>
        [XmlEnum("HighCostWorksheet")]
        HighCostWorksheet,

		/// <summary>
		/// Agrees to hold harmless and indemnify (assumes the legal liability of) the lender from any future liability on items defined in the notice. AKA: Indemnity Notice.
		/// </summary>
        [XmlEnum("HoldHarmlessAgreement")]
        HoldHarmlessAgreement,

		/// <summary>
		/// Confirms that the borrower has completed a homebuyer education program. The programs are usually in association with first time homebuyer and affordable housing programs. 
		/// </summary>
        [XmlEnum("HomeBuyerEducationCertification")]
        HomeBuyerEducationCertification,

		/// <summary>
		/// Authorizes lender to freeze a Home Equity Line of Credit in anticipation of a pay off.
		/// </summary>
        [XmlEnum("HomeEquityLineFreezeLetter")]
        HomeEquityLineFreezeLetter,

		/// <summary>
		/// Confirms the property meets HUD requirements. Provided by the Condominium Association. AKA: Condominium Association Certification, HOA Certification, PUD Certification.
		/// </summary>
        [XmlEnum("HomeownersAssociationCertification")]
        HomeownersAssociationCertification,

		/// <summary>
		/// A written list of homeownership counseling organizations that provide relevant services in the loan applicant's location, obtained: 1) using a tool developed and maintained by the CFPB on its website, or 2) using data made available by the CFPB or HUD.
		/// </summary>
        [XmlEnum("HousingCounselingAgenciesList")]
        HousingCounselingAgenciesList,

		/// <summary>
		/// Confirms the correct borrower name to be used in executing the documents.
		/// </summary>
        [XmlEnum("IdentityAffidavit")]
        IdentityAffidavit,

        [XmlEnum("IdentityAffidavit:IdentityTheft")]
        IdentityAffidavitIdentityTheft,

        [XmlEnum("IdentityAffidavit:Name")]
        IdentityAffidavitName,

        [XmlEnum("IdentityAffidavit:Signature")]
        IdentityAffidavitSignature,

		/// <summary>
		/// Informs the borrower about identify theft and how to protect their credit.
		/// </summary>
        [XmlEnum("IdentityTheftDisclosure")]
        IdentityTheftDisclosure,

		/// <summary>
		/// Informs the borrower that the application as submitted is incomplete and establishes a time period in which information must be received. AKA: 10 Day Letter, Request For Additional Information.
		/// </summary>
        [XmlEnum("IncompleteApplicationNotice")]
        IncompleteApplicationNotice,

		/// <summary>
		/// Describes the funds available through a program that matches funds of the borrower deposited for the sole purpose of purchasing a residence.
		/// </summary>
        [XmlEnum("IndividualDevelopmentAccountStatement")]
        IndividualDevelopmentAccountStatement,

		/// <summary>
		/// Estimates the amount and activity in the escrow account based on anticipated future payments. AKA: Escrow Analysis Statement, Impound.
		/// </summary>
        [XmlEnum("InitialEscrowAccountDisclosure")]
        InitialEscrowAccountDisclosure,

		/// <summary>
		/// CFPB mortgage disclosure required under the Truth in Lending Act and the Real Estate Settlement Procedures Act for most closed-end consumer credit transactions secured by real property. This enumerated value is provisionally supported and must not be used until the regulation is final. 
		/// </summary>
        [XmlEnum("IntegratedDisclosure")]
        IntegratedDisclosure,

		/// <summary>
		/// Discloses the interest rate and discount points to the borrower and are an agreement between the borrower and the lender. The Veteran's Affairs does not govern the rate or points on the loan. AKA: Interest Rate and Discount Disclosure.
		/// </summary>
        [XmlEnum("InterestRateAndDiscountDisclosureStatement")]
        InterestRateAndDiscountDisclosureStatement,

		/// <summary>
		/// Provides bill which itemizes nature and cost of transactions.  AKA: Bill.
		/// </summary>
        [XmlEnum("Invoice")]
        Invoice,

        [XmlEnum("Invoice:UtilityBill")]
        InvoiceUtilityBill,

		/// <summary>
		/// U.S. Individual Income Tax Return.
		/// </summary>
        [XmlEnum("IRS1040")]
        IRS1040,

		/// <summary>
		/// U.S. Income Tax Return for Estates and Trusts.
		/// </summary>
        [XmlEnum("IRS1041")]
        IRS1041,

		/// <summary>
		/// U.S. Return of Partnership Income.
		/// </summary>
        [XmlEnum("IRS1065")]
        IRS1065,

		/// <summary>
		/// Confirms the amount of interest a person has paid on a mortgage. AKA: Mortgage Interest Statement.
		/// </summary>
        [XmlEnum("IRS1098")]
        IRS1098,

		/// <summary>
		/// Reports a person's non-salary income. This is a generic document type used for any IRS 1099 document when the specific IRS 1099 document type is unknown or unnecessary
		/// </summary>
        [XmlEnum("IRS1099")]
        IRS1099,

		/// <summary>
		/// Acquisition or Abandonment of Secured Property
		/// </summary>
        [XmlEnum("IRS1099A")]
        IRS1099A,

		/// <summary>
		/// Proceeds From Broker and Barter Exchange Transactions
		/// </summary>
        [XmlEnum("IRS1099B")]
        IRS1099B,

		/// <summary>
		/// Cancellation of Debt
		/// </summary>
        [XmlEnum("IRS1099C")]
        IRS1099C,

		/// <summary>
		/// Changes in Corporate Control and Capital Structure
		/// </summary>
        [XmlEnum("IRS1099CAP")]
        IRS1099CAP,

		/// <summary>
		/// Dividends and Distributions
		/// </summary>
        [XmlEnum("IRS1099DIV")]
        IRS1099DIV,

		/// <summary>
		/// Certain Government Payments
		/// </summary>
        [XmlEnum("IRS1099G")]
        IRS1099G,

		/// <summary>
		/// Health Coverage Tax Credit (HCTC) Advance Payments
		/// </summary>
        [XmlEnum("IRS1099H")]
        IRS1099H,

		/// <summary>
		/// Interest Income
		/// </summary>
        [XmlEnum("IRS1099INT")]
        IRS1099INT,

		/// <summary>
		/// Long-Term Care and Accelerated Death Benefits
		/// </summary>
        [XmlEnum("IRS1099LTC")]
        IRS1099LTC,

		/// <summary>
		/// Miscellaneous Income
		/// </summary>
        [XmlEnum("IRS1099MISC")]
        IRS1099MISC,

		/// <summary>
		/// Original Issue Discount
		/// </summary>
        [XmlEnum("IRS1099OID")]
        IRS1099OID,

		/// <summary>
		/// Taxable Distributions Received From Cooperatives
		/// </summary>
        [XmlEnum("IRS1099PATR")]
        IRS1099PATR,

		/// <summary>
		/// Payments From Qualified Education Programs (Under Sections 529 and 530)
		/// </summary>
        [XmlEnum("IRS1099Q")]
        IRS1099Q,

		/// <summary>
		/// Distributions From Pensions, Annuities, Retirement or Profit-Sharing Plans, IRAs, Insurance Contracts, etc.
		/// </summary>
        [XmlEnum("IRS1099R")]
        IRS1099R,

		/// <summary>
		/// Proceeds From Real Estate Transactions
		/// </summary>
        [XmlEnum("IRS1099S")]
        IRS1099S,

		/// <summary>
		/// Distributions From an HSA, Archer MSA, or Medicare Advantage MSA
		/// </summary>
        [XmlEnum("IRS1099SA")]
        IRS1099SA,

		/// <summary>
		/// U.S. Corporation Income Tax Return.
		/// </summary>
        [XmlEnum("IRS1120")]
        IRS1120,

		/// <summary>
		/// U.S. Income Tax Return for an S Corporation.
		/// </summary>
        [XmlEnum("IRS1120S")]
        IRS1120S,

		/// <summary>
		/// Confirms a person's wage income and taxes withheld. AKA: Wage and Tax Statement.
		/// </summary>
        [XmlEnum("IRSW2")]
        IRSW2,

		/// <summary>
		/// Confirms the individual is a foreign tax resident. AKA: IRS Certificate of Foreign Status.
		/// </summary>
        [XmlEnum("IRSW8")]
        IRSW8,

		/// <summary>
		/// Provides the borrowers tax identification number and certifies truthfulness of data provided.  Provided by borrower via IRS W9 form. AKA: Request for Tax ID and Certification, Tax Payer ID.
		/// </summary>
        [XmlEnum("IRSW9")]
        IRSW9,

		/// <summary>
		/// Describes the finance charges associated with the mortgage loan. May be included in the TIL Disclosure document.
		/// </summary>
        [XmlEnum("ItemizationOfAmountFinanced")]
        ItemizationOfAmountFinanced,

		/// <summary>
		/// Evidences an obligation determined by a decree of a court. AKA: Judgment Letter.
		/// </summary>
        [XmlEnum("Judgment")]
        Judgment,

		/// <summary>
		/// Establishes the interest in land of a person who owns the lease granted by the property owner.
		/// </summary>
        [XmlEnum("LandLeaseholdAgreement")]
        LandLeaseholdAgreement,

		/// <summary>
		/// Establishes the rights of others over an individual's (the Testator's) property after the individual's death. AKA: Certificate of Appointment, Letters of Testamentary.
		/// </summary>
        [XmlEnum("LastWillAndTestament")]
        LastWillAndTestament,

		/// <summary>
		/// A document titled â€œProtect Your Family from Lead in Your Homeâ€ that informs people about how to reduce exposure to lead hazards in their home. Sellers or their agents must provide the buyers with this pamphlet for houses built prior to 1978. EPA747-K-99-001 June 2003.
		/// </summary>
        [XmlEnum("LeadHazardInformation")]
        LeadHazardInformation,

		/// <summary>
		/// Describes the physical location of the property as defined in the country records.  This is primarily a component of other documents (i.e. security instruments, title policies, commitments), but is received separately on some occasions. AKA: Exhibit A.
		/// </summary>
        [XmlEnum("LegalDescription")]
        LegalDescription,

		/// <summary>
		/// Informs the borrower or a third party, information from, or about, the lender. This is other then a federal, state, or local requirements or regulations.  AKA: Cover Letter, Welcome Letter.
		/// </summary>
        [XmlEnum("LenderCorrespondence")]
        LenderCorrespondence,

		/// <summary>
		/// Specifies agreement between a company and an individual to pay a pre-determined amount of money to a beneficiary if the insured dies during the policy term.
		/// </summary>
        [XmlEnum("LifeInsurancePolicy")]
        LifeInsurancePolicy,

		/// <summary>
		/// Describes in detail the budget for the construction of a home.
		/// </summary>
        [XmlEnum("LineItemBudget")]
        LineItemBudget,

		/// <summary>
		/// Provides information about the employment, income, assets, debts and other financial information of the prospective borrowers. Includes information about the purpose of the home loan and about the property securing the home loan. Provided by Borrower with assistance from the lender.  AKA: Uniform Residential Loan Application, 1003, Credit Application, 65.
		/// </summary>
        [XmlEnum("LoanApplication")]
        LoanApplication,

		/// <summary>
		/// Residential Loan Application for Reverse Mortgages
		/// </summary>
        [XmlEnum("LoanApplication:FNM1009")]
        LoanApplicationFNM1009,

		/// <summary>
		/// Uniform Residential Loan Application
		/// </summary>
        [XmlEnum("LoanApplication:URLA")]
        LoanApplicationURLA,

		/// <summary>
		/// Informs the borrower of total finance charges and interest rates associated with the loan.
		/// </summary>
        [XmlEnum("LoanClosingNotice")]
        LoanClosingNotice,

		/// <summary>
		/// The CFPB Loan Estimate document. Provides disclosures that help consumers in understanding the key features, costs, and risks of the mortgage for which they are applying. The Loan Estimate replaces the GFE document and the initial TILDisclosure document.
		/// </summary>
        [XmlEnum("LoanEstimate")]
        LoanEstimate,

		/// <summary>
		/// Provides Lender with information to determine the amount required to pay off an account and the outstanding principle balance as of a specific date.
		/// </summary>
        [XmlEnum("LoanPayoffRequest")]
        LoanPayoffRequest,

		/// <summary>
		/// Describes transactions recorded and account balances of a customers accounts during a specified period.
		/// </summary>
        [XmlEnum("LoanStatement")]
        LoanStatement,

		/// <summary>
		/// Summarizes key loan origination information including; pricing, product, contact and payment information; does not include credit decision. AKA: Standard Submission Summary.
		/// </summary>
        [XmlEnum("LoanSubmissionSummary")]
        LoanSubmissionSummary,

		/// <summary>
		/// Proves the ownership of the manufactured home and shows any liens on the property. Lists the manufacturer, serial numbers, and date of manufacture.
		/// </summary>
        [XmlEnum("ManufacturedHousingCertificateOfTitle")]
        ManufacturedHousingCertificateOfTitle,

		/// <summary>
		/// Confirms that a marriage has occurred.
		/// </summary>
        [XmlEnum("MarriageCertificate")]
        MarriageCertificate,

		/// <summary>
		/// The mortgage insurance application provides information about a prospective borrower, the loan program, AU decision, MI premium plan, etc.  The MI Application is generally sent by the lender along with the corresponding GSE form 1003/65 and/or GSE 1008/1077.
		/// </summary>
        [XmlEnum("MIApplication")]
        MIApplication,

		/// <summary>
		/// Confirms existence of the insurance policy protecting the lender against loss from the borrower's failure to make payments on the mortgage loan. AKA: Mortgage Insurance PMI, MIP, MI.
		/// </summary>
        [XmlEnum("MICertificate")]
        MICertificate,

		/// <summary>
		/// Establishes a commitment to issue mortgage insurance pending specified conditions.
		/// </summary>
        [XmlEnum("MIConditionalCommitment")]
        MIConditionalCommitment,

		/// <summary>
		/// Notice issued to the lender to indicate a mortgage insurance underwriting decision of declined.  
		/// </summary>
        [XmlEnum("MIDeclination")]
        MIDeclination,

		/// <summary>
		/// Explains terms, requirements, or costs of mortgage insurance. AKA: Comparison Of Lender Paid Mortgage Insurance With Borrower Paid Mortgage Insurance.
		/// </summary>
        [XmlEnum("MIDisclosure")]
        MIDisclosure,

		/// <summary>
		/// Confirms that a person served in the military and describes their veterans status. AKA: DD Form 214, DD214.
		/// </summary>
        [XmlEnum("MilitaryDischargePapers")]
        MilitaryDischargePapers,

		/// <summary>
		/// A document issued by the US Department of Defense which evidences a person's identity and military identifier.
		/// </summary>
        [XmlEnum("MilitaryIdentification")]
        MilitaryIdentification,

		/// <summary>
		/// Changes one or more of the terms of the Mortgage Insurance Certificate after closing.
		/// </summary>
        [XmlEnum("MIModification")]
        MIModification,

		/// <summary>
		/// Notice issued to the lender to indicate a mortgage insurance underwriting decision of suspend.
		/// </summary>
        [XmlEnum("MISuspension")]
        MISuspension,

		/// <summary>
		/// The Monthly Summary of Assistance Payments document is due under HUD Sections 235(b), 235(i), or 235(j), or of Interest Reduction Payments due under Section 236. HUD-300 (04/2003).
		/// </summary>
        [XmlEnum("MonthlySummaryOfAssistancePayments")]
        MonthlySummaryOfAssistancePayments,

		/// <summary>
		/// Mortgagee's Certification and Application for Assistance or Interest Reduction Payments due under HUD Sections 235(b), 235(j), or 235(i). HUD-93102 (04/2003).
		/// </summary>
        [XmlEnum("MortgageesCertificationAndApplicationForAssistanceOrInterestReductionPayments")]
        MortgageesCertificationAndApplicationForAssistanceOrInterestReductionPayments,

        [XmlEnum("NameAffidavit")]
        NameAffidavit,

		/// <summary>
		/// A document which assigns a national identifier to a party for a government purpose like taxation or benefits. In the US this is an IRS or Social Security Administration document such as a Social Security Card. Evidences a party's identity and National Identifier Value.
		/// </summary>
        [XmlEnum("NationalIdentification")]
        NationalIdentification,

		/// <summary>
		/// A national identity document used in many countries in Central and South America. Evidences a person's identity and national Identifier. 
		/// </summary>
        [XmlEnum("NationalIdentification:CedulaDeIdentidad")]
        NationalIdentificationCedulaDeIdentidad,

		/// <summary>
		/// A national identity document used in the US. Evidences a person's identity and Social Security Number.
		/// </summary>
        [XmlEnum("NationalIdentification:SocialSecurityCard")]
        NationalIdentificationSocialSecurityCard,

		/// <summary>
		/// Identifies the nearest living relative of the borrower.
		/// </summary>
        [XmlEnum("NearestLivingRelativeInformation")]
        NearestLivingRelativeInformation,

		/// <summary>
		/// Identifies whom to contact, including contact point values, when the borrower(s) are unavailable. Signed by the borrower(s).
		/// </summary>
        [XmlEnum("NearestLivingRelativeInformation:AlternativeContact")]
        NearestLivingRelativeInformationAlternativeContact,

        [XmlEnum("NearestLivingRelativeInformation:VAQuestionnaire")]
        NearestLivingRelativeInformationVAQuestionnaire,

		/// <summary>
		/// Confirms that a person is not a diplomat, in the case where the person is a non-resident alien.
		/// </summary>
        [XmlEnum("NonDiplomatVerification")]
        NonDiplomatVerification,

		/// <summary>
		/// Informs the borrower of appraisal and credit report fees. A state disclosure. AKA: Agreement Concerning Non Refundability of Advance Fee.
		/// </summary>
        [XmlEnum("NonRefundabilityNotice")]
        NonRefundabilityNotice,

		/// <summary>
		/// Establishes a legal obligation of the borrower to repay the debt.  AKA: Promissory Note, Mortgage Note, Loan Agreement.
		/// </summary>
        [XmlEnum("Note")]
        Note,

        [XmlEnum("Note:Consolidated")]
        NoteConsolidated,

        [XmlEnum("Note:HECMLoanAgreement")]
        NoteHECMLoanAgreement,

        [XmlEnum("Note:NewMoney")]
        NoteNewMoney,

		/// <summary>
		/// Adds the specific terms of the ARM to the promissory note.
		/// </summary>
        [XmlEnum("NoteAddendum")]
        NoteAddendum,

        [XmlEnum("NoteAddendum:AffordableMeritRate")]
        NoteAddendumAffordableMeritRate,

        [XmlEnum("NoteAddendum:Arbitration")]
        NoteAddendumArbitration,

        [XmlEnum("NoteAddendum:ARM")]
        NoteAddendumARM,

        [XmlEnum("NoteAddendum:Balloon")]
        NoteAddendumBalloon,

        [XmlEnum("NoteAddendum:Construction")]
        NoteAddendumConstruction,

        [XmlEnum("NoteAddendum:FixedRateOption")]
        NoteAddendumFixedRateOption,

		/// <summary>
		/// Growing Equity
		/// </summary>
        [XmlEnum("NoteAddendum:GEM")]
        NoteAddendumGEM,

		/// <summary>
		/// Graduated Payment
		/// </summary>
        [XmlEnum("NoteAddendum:GPM")]
        NoteAddendumGPM,

        [XmlEnum("NoteAddendum:InterestOnly")]
        NoteAddendumInterestOnly,

        [XmlEnum("NoteAddendum:InterVivosRevocableTrust")]
        NoteAddendumInterVivosRevocableTrust,

        [XmlEnum("NoteAddendum:OccupancyDeclaration")]
        NoteAddendumOccupancyDeclaration,

        [XmlEnum("NoteAddendum:Prepayment")]
        NoteAddendumPrepayment,

        [XmlEnum("NoteAddendum:RateImprovement")]
        NoteAddendumRateImprovement,

		/// <summary>
		/// Adds additional page(s) to the Note for endorsements.
		/// </summary>
        [XmlEnum("NoteAllonge")]
        NoteAllonge,

		/// <summary>
		/// Changes one or more of the terms of a loan in the note and in the security instrument of the mortgagor.
		/// </summary>
        [XmlEnum("NoteAndSecurityInstrumentModification")]
        NoteAndSecurityInstrumentModification,

		/// <summary>
		/// Changes one or more of the terms of the loan in the promissory note.
		/// </summary>
        [XmlEnum("NoteModification")]
        NoteModification,

		/// <summary>
		/// Adds additional terms to the promissory note.
		/// </summary>
        [XmlEnum("NoteRider")]
        NoteRider,

        [XmlEnum("NoteRider:ARM")]
        NoteRiderARM,

        [XmlEnum("NoteRider:Balloon")]
        NoteRiderBalloon,

        [XmlEnum("NoteRider:Buydown")]
        NoteRiderBuydown,

		/// <summary>
		/// Establishes that the lender will reserve funds from the initial principal limit to be used to repair the property, and that the borrower will complete the repairs.
		/// </summary>
        [XmlEnum("NoteRider:HECMRepair")]
        NoteRiderHECMRepair,

        [XmlEnum("NoteRider:Occupancy")]
        NoteRiderOccupancy,

        [XmlEnum("NoteRider:Prepayment")]
        NoteRiderPrepayment,

        [XmlEnum("NoteRider:StepRate")]
        NoteRiderStepRate,

		/// <summary>
		/// Informs the borrower of denial or other material change in the loan application. AKA: Denial Letter, Statement of Credit Denial, Termination, or Change, Notice of Adverse Action, Notice of Counter Offer, Credit Denial Letter, Withdrawal Letter, Request for More Information.
		/// </summary>
        [XmlEnum("NoticeOfActionTaken")]
        NoticeOfActionTaken,

		/// <summary>
		/// Changes (updates) a previous appraisal that was prepared subject to completion of repairs or incomplete items. AKA: FNMA 442.
		/// </summary>
        [XmlEnum("NoticeOfCompletion")]
        NoticeOfCompletion,

		/// <summary>
		/// Informs the borrower of their three-day right to cancel the loan application. Required by TILA/Regulation Z.  AKA: Right of Rescission, Cancellation Notice.
		/// </summary>
        [XmlEnum("NoticeOfRightToCancel")]
        NoticeOfRightToCancel,

        [XmlEnum("NoticeOfRightToCancel:AddSecurityInterest")]
        NoticeOfRightToCancelAddSecurityInterest,

        [XmlEnum("NoticeOfRightToCancel:CreditIncrease")]
        NoticeOfRightToCancelCreditIncrease,

        [XmlEnum("NoticeOfRightToCancel:IncreaseSecurity")]
        NoticeOfRightToCancelIncreaseSecurity,

        [XmlEnum("NoticeOfRightToCancel:OpenAccount")]
        NoticeOfRightToCancelOpenAccount,

		/// <summary>
		/// Informs the borrower of their right to a copy of their appraisal report.
		/// </summary>
        [XmlEnum("NoticeOfRightToCopyOfAppraisalReport")]
        NoticeOfRightToCopyOfAppraisalReport,

		/// <summary>
		/// Informs the borrower of the repair conditions listed on the Notice to Lender form, outlining any issues or comments on the appraisal. Provided by the lender to the borrower. AKA: Homebuyer Summary.
		/// </summary>
        [XmlEnum("NoticeToHomebuyer")]
        NoticeToHomebuyer,

		/// <summary>
		/// Describes repair conditions on the subject property. Provided by Appraiser to the lender. Provided as an addendum to Appraisal. AKA: VC sheet, VC condition, valuation condition sheet.
		/// </summary>
        [XmlEnum("NoticeToLender")]
        NoticeToLender,

		/// <summary>
		/// Establishes the borrower intends to occupy the property.
		/// </summary>
        [XmlEnum("OccupancyAgreement")]
        OccupancyAgreement,

		/// <summary>
		/// Confirms that a property is fit for occupancy.  Provided by local municipal authority. AKA: CO.
		/// </summary>
        [XmlEnum("OccupancyCertification")]
        OccupancyCertification,

		/// <summary>
		/// To be used with TypeOtherDescription data point to describe a document not already on this list.
		/// </summary>
        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Releases a specific parcel of real property from an existing security instrument.
		/// </summary>
        [XmlEnum("PartialReleaseOfSecurityInstrument")]
        PartialReleaseOfSecurityInstrument,

		/// <summary>
		/// Establishes the terms of a partnership, including the rights and responsibilities of the partners.
		/// </summary>
        [XmlEnum("PartnershipAgreement")]
        PartnershipAgreement,

		/// <summary>
		/// A document issued by a national government which certifies a person's identity, nationality, and passport identifier.
		/// </summary>
        [XmlEnum("Passport")]
        Passport,

		/// <summary>
		/// Describes history of payments on a loan. AKA: Loan History.
		/// </summary>
        [XmlEnum("PaymentHistory")]
        PaymentHistory,

		/// <summary>
		/// Describes the terms of a loan and loan payments, often emphasizing timing and amount of the first payment. AKA: Loan Payment Information Sheet, First Payment Letter.
		/// </summary>
        [XmlEnum("PaymentLetter")]
        PaymentLetter,

		/// <summary>
		/// Confirms the remaining principle balance of a loan that is being paid off or refinanced, based on a defined close or pay date. AKA: Demand, Demand Statement.
		/// </summary>
        [XmlEnum("PayoffStatement")]
        PayoffStatement,

		/// <summary>
		/// Describes income for a particular pay period.
		/// </summary>
        [XmlEnum("PayStub")]
        PayStub,

		/// <summary>
		/// Confirms a person's permanent legal residency. AKA: Green Card, Temporary Permanent Residence ID, Residence Alien Card, Alien Registration.
		/// </summary>
        [XmlEnum("PermanentResidentIdentification")]
        PermanentResidentIdentification,

		/// <summary>
		/// Describes personal property and provides and estimation of value.
		/// </summary>
        [XmlEnum("PersonalPropertyAppraisalReport")]
        PersonalPropertyAppraisalReport,

		/// <summary>
		/// Authorizes one person to act as agent or attorney on behalf of another person.  AKA: POA.
		/// </summary>
        [XmlEnum("PowerOfAttorney")]
        PowerOfAttorney,

        [XmlEnum("PowerOfAttorney:Durable")]
        PowerOfAttorneyDurable,

        [XmlEnum("PowerOfAttorney:Limited")]
        PowerOfAttorneyLimited,

        [XmlEnum("PowerOfAttorney:ManufacturedHousing")]
        PowerOfAttorneyManufacturedHousing,

		/// <summary>
		/// Informs the borrower of "premium pricing option" (i.e., option of prepayment charge in lieu of  higher interest rate).
		/// </summary>
        [XmlEnum("PrepaymentChargeOptionNotice")]
        PrepaymentChargeOptionNotice,

		/// <summary>
		/// Informs a perspective borrowers that they will qualify for a loan based on the income and credit information the borrower has provided.
		/// </summary>
        [XmlEnum("PrequalificationLetter")]
        PrequalificationLetter,

		/// <summary>
		/// Informs the borrower of privacy rights; describes what information will be collected and shared. AKA: Privacy Policy.
		/// </summary>
        [XmlEnum("PrivacyDisclosure")]
        PrivacyDisclosure,

        [XmlEnum("PrivateIdentification")]
        PrivateIdentification,

		/// <summary>
		/// A disclosure required under California Code of Regulations Title 10 Chapter 3 Subchapter 6 Article 3 Section 1436 Part (d). DocumentFormIssuingEntityNameTypeOtherDescription is CA.
		/// </summary>
        [XmlEnum("ProductComparisonDisclosure")]
        ProductComparisonDisclosure,

		/// <summary>
		/// Describes the condition of the subject property, or any of a variety of building systems or interior or exterior elements of the structure, or aspects of the surrounding property (such as water or soil).
		/// </summary>
        [XmlEnum("PropertyInspectionReport")]
        PropertyInspectionReport,

        [XmlEnum("PropertyInspectionReport:Carpet")]
        PropertyInspectionReportCarpet,

        [XmlEnum("PropertyInspectionReport:Environmental")]
        PropertyInspectionReportEnvironmental,

        [XmlEnum("PropertyInspectionReport:Footings")]
        PropertyInspectionReportFootings,

        [XmlEnum("PropertyInspectionReport:Framing")]
        PropertyInspectionReportFraming,

        [XmlEnum("PropertyInspectionReport:Heating")]
        PropertyInspectionReportHeating,

        [XmlEnum("PropertyInspectionReport:Insulation")]
        PropertyInspectionReportInsulation,

        [XmlEnum("PropertyInspectionReport:Pest")]
        PropertyInspectionReportPest,

        [XmlEnum("PropertyInspectionReport:Plumbing")]
        PropertyInspectionReportPlumbing,

        [XmlEnum("PropertyInspectionReport:Roof")]
        PropertyInspectionReportRoof,

        [XmlEnum("PropertyInspectionReport:Septic")]
        PropertyInspectionReportSeptic,

        [XmlEnum("PropertyInspectionReport:Soil")]
        PropertyInspectionReportSoil,

        [XmlEnum("PropertyInspectionReport:SoilTreatment")]
        PropertyInspectionReportSoilTreatment,

        [XmlEnum("PropertyInspectionReport:StructuralEngineering")]
        PropertyInspectionReportStructuralEngineering,

        [XmlEnum("PropertyInspectionReport:SubterraneanTermite")]
        PropertyInspectionReportSubterraneanTermite,

        [XmlEnum("PropertyInspectionReport:Termite")]
        PropertyInspectionReportTermite,

        [XmlEnum("PropertyInspectionReport:WaterHealth")]
        PropertyInspectionReportWaterHealth,

        [XmlEnum("PropertyInspectionReport:Well")]
        PropertyInspectionReportWell,

		/// <summary>
		/// Establishes a commitment to issue property insurance pending payment.
		/// </summary>
        [XmlEnum("PropertyInsuranceBinder")]
        PropertyInsuranceBinder,

        [XmlEnum("PropertyInsuranceBinder:Earthquake")]
        PropertyInsuranceBinderEarthquake,

        [XmlEnum("PropertyInsuranceBinder:FireAndExtendedCoverage")]
        PropertyInsuranceBinderFireAndExtendedCoverage,

        [XmlEnum("PropertyInsuranceBinder:Flood")]
        PropertyInsuranceBinderFlood,

        [XmlEnum("PropertyInsuranceBinder:Hazard")]
        PropertyInsuranceBinderHazard,

        [XmlEnum("PropertyInsuranceBinder:Homeowners")]
        PropertyInsuranceBinderHomeowners,

        [XmlEnum("PropertyInsuranceBinder:Hurricane")]
        PropertyInsuranceBinderHurricane,

        [XmlEnum("PropertyInsuranceBinder:InsectInfestation")]
        PropertyInsuranceBinderInsectInfestation,

        [XmlEnum("PropertyInsuranceBinder:Leasehold")]
        PropertyInsuranceBinderLeasehold,

        [XmlEnum("PropertyInsuranceBinder:MineSubsidence")]
        PropertyInsuranceBinderMineSubsidence,

        [XmlEnum("PropertyInsuranceBinder:PersonalProperty")]
        PropertyInsuranceBinderPersonalProperty,

        [XmlEnum("PropertyInsuranceBinder:Storm")]
        PropertyInsuranceBinderStorm,

        [XmlEnum("PropertyInsuranceBinder:Tornado")]
        PropertyInsuranceBinderTornado,

        [XmlEnum("PropertyInsuranceBinder:Volcano")]
        PropertyInsuranceBinderVolcano,

        [XmlEnum("PropertyInsuranceBinder:Wind")]
        PropertyInsuranceBinderWind,

		/// <summary>
		/// Describes the information regarding the risk covered by the insurance policy, including the name of the insurance company, its address, name of the policy holder, starting and ending dates of coverage, the actual coverage given in the contract, including locations and amounts.
		/// </summary>
        [XmlEnum("PropertyInsuranceDeclarations")]
        PropertyInsuranceDeclarations,

        [XmlEnum("PropertyInsuranceDeclarations:Earthquake")]
        PropertyInsuranceDeclarationsEarthquake,

        [XmlEnum("PropertyInsuranceDeclarations:FireAndExtendedCoverage")]
        PropertyInsuranceDeclarationsFireAndExtendedCoverage,

        [XmlEnum("PropertyInsuranceDeclarations:Flood")]
        PropertyInsuranceDeclarationsFlood,

        [XmlEnum("PropertyInsuranceDeclarations:Hazard")]
        PropertyInsuranceDeclarationsHazard,

        [XmlEnum("PropertyInsuranceDeclarations:Homeowners")]
        PropertyInsuranceDeclarationsHomeowners,

        [XmlEnum("PropertyInsuranceDeclarations:Hurricane")]
        PropertyInsuranceDeclarationsHurricane,

        [XmlEnum("PropertyInsuranceDeclarations:InsectInfestation")]
        PropertyInsuranceDeclarationsInsectInfestation,

        [XmlEnum("PropertyInsuranceDeclarations:Leasehold")]
        PropertyInsuranceDeclarationsLeasehold,

        [XmlEnum("PropertyInsuranceDeclarations:MineSubsidence")]
        PropertyInsuranceDeclarationsMineSubsidence,

        [XmlEnum("PropertyInsuranceDeclarations:PersonalProperty")]
        PropertyInsuranceDeclarationsPersonalProperty,

        [XmlEnum("PropertyInsuranceDeclarations:Storm")]
        PropertyInsuranceDeclarationsStorm,

        [XmlEnum("PropertyInsuranceDeclarations:Tornado")]
        PropertyInsuranceDeclarationsTornado,

        [XmlEnum("PropertyInsuranceDeclarations:Volcano")]
        PropertyInsuranceDeclarationsVolcano,

        [XmlEnum("PropertyInsuranceDeclarations:Wind")]
        PropertyInsuranceDeclarationsWind,

		/// <summary>
		/// Establishes the terms and conditions of the contract for insurance covering the subject property.
		/// </summary>
        [XmlEnum("PropertyInsurancePolicy")]
        PropertyInsurancePolicy,

        [XmlEnum("PropertyInsurancePolicy:Earthquake")]
        PropertyInsurancePolicyEarthquake,

        [XmlEnum("PropertyInsurancePolicy:FireAndExtendedCoverage")]
        PropertyInsurancePolicyFireAndExtendedCoverage,

        [XmlEnum("PropertyInsurancePolicy:Flood")]
        PropertyInsurancePolicyFlood,

        [XmlEnum("PropertyInsurancePolicy:Hazard")]
        PropertyInsurancePolicyHazard,

        [XmlEnum("PropertyInsurancePolicy:Homeowners")]
        PropertyInsurancePolicyHomeowners,

        [XmlEnum("PropertyInsurancePolicy:Hurricane")]
        PropertyInsurancePolicyHurricane,

        [XmlEnum("PropertyInsurancePolicy:InsectInfestation")]
        PropertyInsurancePolicyInsectInfestation,

        [XmlEnum("PropertyInsurancePolicy:Leasehold")]
        PropertyInsurancePolicyLeasehold,

        [XmlEnum("PropertyInsurancePolicy:MineSubsidence")]
        PropertyInsurancePolicyMineSubsidence,

        [XmlEnum("PropertyInsurancePolicy:PersonalProperty")]
        PropertyInsurancePolicyPersonalProperty,

        [XmlEnum("PropertyInsurancePolicy:Storm")]
        PropertyInsurancePolicyStorm,

        [XmlEnum("PropertyInsurancePolicy:Tornado")]
        PropertyInsurancePolicyTornado,

        [XmlEnum("PropertyInsurancePolicy:Volcano")]
        PropertyInsurancePolicyVolcano,

        [XmlEnum("PropertyInsurancePolicy:Wind")]
        PropertyInsurancePolicyWind,

		/// <summary>
		/// Explains lenders right to approve a borrower's choice of insurance company and the required specifics of the insurance such as deductible, premium and term. AKA: Agreement to Provide Insurance.
		/// </summary>
        [XmlEnum("PropertyInsuranceRequirementDisclosure")]
        PropertyInsuranceRequirementDisclosure,

		/// <summary>
		/// Establishes the terms and conditions for purchase of a property. AKA: Sale Agreement, Contract for Sale, Sales Contract, Purchase Contract.
		/// </summary>
        [XmlEnum("PurchaseAgreement")]
        PurchaseAgreement,

		/// <summary>
		/// A document that notifies the borrower that the interest rate on the ARM loan has changed.
		/// </summary>
        [XmlEnum("RateChangeNotice")]
        RateChangeNotice,

		/// <summary>
		/// Establishes that the parties to a loan transaction have fixed (locked) specific proposed terms of the loan for a specified period. Terms may include such items as interest rate, yield, term, points, fees, expiration and product type.
		/// </summary>
        [XmlEnum("RateLockAgreement")]
        RateLockAgreement,

		/// <summary>
		/// Establishes that the borrower will reinstitute a mortgage debt after bankruptcy proceedings are completed.
		/// </summary>
        [XmlEnum("ReaffirmationAgreement")]
        ReaffirmationAgreement,

		/// <summary>
		/// Confirms the payment or exchange of a good or service.
		/// </summary>
        [XmlEnum("Receipt")]
        Receipt,

		/// <summary>
		/// The Recertification of Family Income and Composition document is used in HUD Section 235(b) loans. It is sent by lenders to individual borrowers to obtain the information necessary to determine family income and composition. Mortgagees maintain copies for audit purposes. HUD-93101 (4/91).
		/// </summary>
        [XmlEnum("RecertificationOfFamilyIncomeAndComposition")]
        RecertificationOfFamilyIncomeAndComposition,

		/// <summary>
		/// Recertification of Family Income and Composition Statistical Report. HUD-93101-A (12/91). Used in HUD Section 235 loans.
		/// </summary>
        [XmlEnum("RecertificationOfFamilyIncomeStatisticalReport")]
        RecertificationOfFamilyIncomeStatisticalReport,

		/// <summary>
		/// Conveys a trustee's interest back to the property owner when a deed of trust is paid in full. In some jurisdictions both a Satisfaction of Deed of Trust and a Reconveyance. AKA: Reconveyance Deed.
		/// </summary>
        [XmlEnum("Reconveyance")]
        Reconveyance,

		/// <summary>
		/// Describes terms and conditions of the relocation package.
		/// </summary>
        [XmlEnum("RelocationBenefitsPackage")]
        RelocationBenefitsPackage,

		/// <summary>
		/// Establishes the terms between a relocation company and an employee to purchase a home based on the appraised value. It describes what happens if a home does not sell within a certain period (usually 60 or 90 days), and the circumstances in which a relocation company will  buy a home.
		/// </summary>
        [XmlEnum("RelocationBuyoutAgreement")]
        RelocationBuyoutAgreement,

		/// <summary>
		/// A document provided to the remitter for cash received related to the Single Family Property Disposition Program (Section 204(g)) that shows the remittance amount, case number and the date that the collection took place. HUD-235.1 (4/89).
		/// </summary>
        [XmlEnum("RemittanceDeliveryReceipt")]
        RemittanceDeliveryReceipt,

		/// <summary>
		/// Establishes the rights and duties of the landlord and tenant in a rental transaction. At a minimum identifies the parties, the property, the term of the rental, and the amount of rent for the term. AKA: Lease Agreement.
		/// </summary>
        [XmlEnum("RentalAgreement")]
        RentalAgreement,

		/// <summary>
		/// Evaluates the rental potential of a property and establishes its value. AKA: Operating Income Statement, FHLMC 998, FNMA 216, One to Four Family Investment Property and Two to Four Owner Occupied Property.
		/// </summary>
        [XmlEnum("RentalIncomeAnalysisStatement")]
        RentalIncomeAnalysisStatement,

		/// <summary>
		/// Authorizes taxing authority (such as IRS) to provide certified copy of borrowers tax return
		/// </summary>
        [XmlEnum("RequestForCopyOfTaxReturn")]
        RequestForCopyOfTaxReturn,

		/// <summary>
		/// Request for Copy of Tax Return
		/// </summary>
        [XmlEnum("RequestForCopyOfTaxReturn:IRS4506")]
        RequestForCopyOfTaxReturnIRS4506,

		/// <summary>
		/// Request for Transcript of Tax Return
		/// </summary>
        [XmlEnum("RequestForCopyOfTaxReturn:IRS4506T")]
        RequestForCopyOfTaxReturnIRS4506T,

		/// <summary>
		/// Short Form Request for Individual Tax Return Transcript. IRS Form 4506T-EZ Rev. January 2012.
		/// </summary>
        [XmlEnum("RequestForCopyOfTaxReturn:IRS4506TEZ")]
        RequestForCopyOfTaxReturnIRS4506TEZ,

		/// <summary>
		/// Establishes a requirement for a senior lien holder to inform a junior lien holder in the event of a default on the common property.  Required to be recorded. AKA: Notice of Default.
		/// </summary>
        [XmlEnum("RequestForNoticeOfDefault")]
        RequestForNoticeOfDefault,

		/// <summary>
		/// Evidences transactions recorded and account balances for a restricted individual investment account during a specified period.
		/// </summary>
        [XmlEnum("RetirementAccountStatement")]
        RetirementAccountStatement,

		/// <summary>
		/// Informs the borrower that a consumer report is used in connection with providing credit with materially less favorable terms than the most favorable terms available to a substantial proportion of customers.
		/// </summary>
        [XmlEnum("RiskBasedPricingNotice")]
        RiskBasedPricingNotice,

		/// <summary>
		/// AKA: H-2 model form.
		/// </summary>
        [XmlEnum("RiskBasedPricingNotice:AccountReviewNotice")]
        RiskBasedPricingNoticeAccountReviewNotice,

		/// <summary>
		/// AKA: H-3 model form.
		/// </summary>
        [XmlEnum("RiskBasedPricingNotice:CreditScoreDisclosureException")]
        RiskBasedPricingNoticeCreditScoreDisclosureException,

		/// <summary>
		/// AKA: H-4 model form.
		/// </summary>
        [XmlEnum("RiskBasedPricingNotice:CreditScoreDisclosureNoResidentialSecured")]
        RiskBasedPricingNoticeCreditScoreDisclosureNoResidentialSecured,

		/// <summary>
		/// AKA: H-5 model form.
		/// </summary>
        [XmlEnum("RiskBasedPricingNotice:CreditScoreDisclosureNoScore")]
        RiskBasedPricingNoticeCreditScoreDisclosureNoScore,

		/// <summary>
		/// AKA: H-1 model form.
		/// </summary>
        [XmlEnum("RiskBasedPricingNotice:GeneralNotice")]
        RiskBasedPricingNoticeGeneralNotice,

		/// <summary>
		/// Establishes the parties and their respective responsibilities to jointly maintain a road. AKA: Private Road Maintenance Agreement, Street Road Maintenance Agreement.
		/// </summary>
        [XmlEnum("RoadMaintenanceAgreement")]
        RoadMaintenanceAgreement,

		/// <summary>
		/// Confirms that a judgment filed against a property has been released. Provided by a legal authority. AKA: Acknowledgement of Satisfaction of Judgment.
		/// </summary>
        [XmlEnum("SatisfactionOfJudgment")]
        SatisfactionOfJudgment,

		/// <summary>
		/// Confirms the security interest has been paid off and the lien on the property is released. Required to be recorded. AKA: Reconveyance, Real Estate Lien Release, Satisfaction of Deed of Trust Certificate And Affidavit Of Satisfaction, Certificate Of Discharge, Certificate Of Satisfaction, Deed Of Reconveyance, Deed Of Release, Discharge Of Deed To Secure Debt, Discharge Of Mortgage, Full Deed Of Release, Full Reconveyance, Lost Mortgage Satisfaction, Release Of Lien, Release Of Mortgage, Request For Cancellation, Request For Full [X] / Partial [ ] Release Of Deed Of Trust And Release, Satisfaction, Satisfaction Of Mortgage, Satisfaction Piece, Satisfaction, Cancellation Or Discharge Of Mortgage, Trust Deed Release.
		/// </summary>
        [XmlEnum("SatisfactionOfSecurityInstrument")]
        SatisfactionOfSecurityInstrument,

        [XmlEnum("SatisfactionOfSecurityInstrument:LienRelease")]
        SatisfactionOfSecurityInstrumentLienRelease,

        [XmlEnum("SatisfactionOfSecurityInstrument:SatisfactionOfDeedOfTrust")]
        SatisfactionOfSecurityInstrumentSatisfactionOfDeedOfTrust,

        [XmlEnum("SatisfactionOfSecurityInstrument:SatisfactionOfMortgage")]
        SatisfactionOfSecurityInstrumentSatisfactionOfMortgage,

		/// <summary>
		/// Informs the borrower that a loan is considered a high cost loan.  Required by Regulation Z. AKA: 2016a.
		/// </summary>
        [XmlEnum("Section32DisclosureNotice")]
        Section32DisclosureNotice,

		/// <summary>
		/// Establishes the lender's security interest in the real property. AKA: Security Trust Deed.
		/// </summary>
        [XmlEnum("SecurityInstrument")]
        SecurityInstrument,

        [XmlEnum("SecurityInstrument:DeedOfTrust")]
        SecurityInstrumentDeedOfTrust,

        [XmlEnum("SecurityInstrument:Mortgage")]
        SecurityInstrumentMortgage,

		/// <summary>
		/// Adds additional terms to the security instrument.
		/// </summary>
        [XmlEnum("SecurityInstrumentAddendum")]
        SecurityInstrumentAddendum,

        [XmlEnum("SecurityInstrumentAddendum:FixedRateOption")]
        SecurityInstrumentAddendumFixedRateOption,

		/// <summary>
		/// Changes one or more of the terms of a loan of the mortgagor in the security instrument.
		/// </summary>
        [XmlEnum("SecurityInstrumentModification")]
        SecurityInstrumentModification,

        [XmlEnum("SecurityInstrumentModification:ConsolidationAgreement")]
        SecurityInstrumentModificationConsolidationAgreement,

		/// <summary>
		/// Used for refinance loans in state of NY. AKA: CEMA.
		/// </summary>
        [XmlEnum("SecurityInstrumentModification:ConsolidationExtensionModificationAgreement")]
        SecurityInstrumentModificationConsolidationExtensionModificationAgreement,

        [XmlEnum("SecurityInstrumentModification:ModificationAgreement")]
        SecurityInstrumentModificationModificationAgreement,

		/// <summary>
		/// Adds additional provisions to the security instrument.
		/// </summary>
        [XmlEnum("SecurityInstrumentRider")]
        SecurityInstrumentRider,

        [XmlEnum("SecurityInstrumentRider:AffordableMeritRate")]
        SecurityInstrumentRiderAffordableMeritRate,

        [XmlEnum("SecurityInstrumentRider:ARM")]
        SecurityInstrumentRiderARM,

        [XmlEnum("SecurityInstrumentRider:Balloon")]
        SecurityInstrumentRiderBalloon,

        [XmlEnum("SecurityInstrumentRider:Beneficiary")]
        SecurityInstrumentRiderBeneficiary,

        [XmlEnum("SecurityInstrumentRider:Biweekly")]
        SecurityInstrumentRiderBiweekly,

        [XmlEnum("SecurityInstrumentRider:Condominium")]
        SecurityInstrumentRiderCondominium,

        [XmlEnum("SecurityInstrumentRider:Construction")]
        SecurityInstrumentRiderConstruction,

		/// <summary>
		/// Growing Equity
		/// </summary>
        [XmlEnum("SecurityInstrumentRider:GEM")]
        SecurityInstrumentRiderGEM,

		/// <summary>
		/// Graduated Payment
		/// </summary>
        [XmlEnum("SecurityInstrumentRider:GPM")]
        SecurityInstrumentRiderGPM,

        [XmlEnum("SecurityInstrumentRider:HomesteadExemption")]
        SecurityInstrumentRiderHomesteadExemption,

        [XmlEnum("SecurityInstrumentRider:InterestOnly")]
        SecurityInstrumentRiderInterestOnly,

        [XmlEnum("SecurityInstrumentRider:InterVivosRevocableTrust")]
        SecurityInstrumentRiderInterVivosRevocableTrust,

        [XmlEnum("SecurityInstrumentRider:Investor")]
        SecurityInstrumentRiderInvestor,

        [XmlEnum("SecurityInstrumentRider:LandTrust")]
        SecurityInstrumentRiderLandTrust,

        [XmlEnum("SecurityInstrumentRider:Leasehold")]
        SecurityInstrumentRiderLeasehold,

        [XmlEnum("SecurityInstrumentRider:ManufacturedHousing")]
        SecurityInstrumentRiderManufacturedHousing,

        [XmlEnum("SecurityInstrumentRider:NonOwner")]
        SecurityInstrumentRiderNonOwner,

        [XmlEnum("SecurityInstrumentRider:OneToFourFamily")]
        SecurityInstrumentRiderOneToFourFamily,

        [XmlEnum("SecurityInstrumentRider:OwnerOccupancy")]
        SecurityInstrumentRiderOwnerOccupancy,

        [XmlEnum("SecurityInstrumentRider:Prepayment")]
        SecurityInstrumentRiderPrepayment,

        [XmlEnum("SecurityInstrumentRider:PUD")]
        SecurityInstrumentRiderPUD,

        [XmlEnum("SecurityInstrumentRider:RateImprovement")]
        SecurityInstrumentRiderRateImprovement,

		/// <summary>
		/// Rehabilitation Loan Rider
		/// </summary>
        [XmlEnum("SecurityInstrumentRider:Rehabilitation")]
        SecurityInstrumentRiderRehabilitation,

        [XmlEnum("SecurityInstrumentRider:RenewalAndExtension")]
        SecurityInstrumentRiderRenewalAndExtension,

        [XmlEnum("SecurityInstrumentRider:SecondHome")]
        SecurityInstrumentRiderSecondHome,

        [XmlEnum("SecurityInstrumentRider:SecondLien")]
        SecurityInstrumentRiderSecondLien,

        [XmlEnum("SecurityInstrumentRider:TaxExemptFinancing")]
        SecurityInstrumentRiderTaxExemptFinancing,

        [XmlEnum("SecurityInstrumentRider:VA")]
        SecurityInstrumentRiderVA,

        [XmlEnum("SecurityInstrumentRider:VeteransLandBoard")]
        SecurityInstrumentRiderVeteransLandBoard,

        [XmlEnum("SecurityInstrumentRider:WaiverOfBorrowersRights")]
        SecurityInstrumentRiderWaiverOfBorrowersRights,

        [XmlEnum("SecurityInstrumentRider:WaiverOfDowerRights")]
        SecurityInstrumentRiderWaiverOfDowerRights,

		/// <summary>
		/// A document which discloses to buyers known lead-based paint and lead-based paint hazards in the housing and any available reports on lead in the housing.
		/// </summary>
        [XmlEnum("SellerLeadHazardDisclosureAndAcknowledgment")]
        SellerLeadHazardDisclosureAndAcknowledgment,

		/// <summary>
		/// Informs the borrower that the right to collect their mortgage loan payments may be transferred, and they have certain rights under federal law. Required by RESPA / Regulation X. AKA: Notice to Mortgage Loan Applicants.
		/// </summary>
        [XmlEnum("ServicingDisclosureStatement")]
        ServicingDisclosureStatement,

		/// <summary>
		/// Informs the borrower that the servicing of their loan is being transferred. Required by RESPA / Regulation X. AKA: Notice of Transfer of Servicing, Hello/Goodbye Letter.
		/// </summary>
        [XmlEnum("ServicingTransferStatement")]
        ServicingTransferStatement,

		/// <summary>
		/// Describes the funds that are payable at closing. Items that appear on the statement include real estate commissions, loan fees, points, and initial escrow amounts.  Required by RESPA/Regulation X.  AKA: Closing Statement.
		/// </summary>
        [XmlEnum("SettlementStatement")]
        SettlementStatement,

		/// <summary>
		/// Settlement Statement (HUD-1)
		/// </summary>
        [XmlEnum("SettlementStatement:HUD1")]
        SettlementStatementHUD1,

		/// <summary>
		/// Settlement Statement (HUD-1A) - Optional Form for Transactions without Sellers.
		/// </summary>
        [XmlEnum("SettlementStatement:HUD1A")]
        SettlementStatementHUD1A,

		/// <summary>
		/// Confirms that Social Security benefits are payable and the amount received each month.  Provided by the Social Security Administration. AKA: Social Security Benefit Letter, Social Security Decision Notice.
		/// </summary>
        [XmlEnum("SocialSecurityAwardLetter")]
        SocialSecurityAwardLetter,

		/// <summary>
		/// A document that servicers must send to delinquent borrowers which explains alternatives to foreclosure. AKA Delinquent Notice. Example: Guide Exhibit 1131, Borrower Solicitation Letter - 31 Days Delinquent
		/// </summary>
        [XmlEnum("SolicitationLetter")]
        SolicitationLetter,

		/// <summary>
		/// Confirms whether the property is located in a flood zone and therefore whether it requires flood insurance.  Provided by FEMA. AKA: Flood Cert, FEMA Standard Flood Hazard Determination.
		/// </summary>
        [XmlEnum("StandardFloodHazardDetermination")]
        StandardFloodHazardDetermination,

		/// <summary>
		/// A document issued by a state to identify a resident in lieu of a driver's license. Evidences a person's identity and state identifier.
		/// </summary>
        [XmlEnum("StateIdentification")]
        StateIdentification,

		/// <summary>
		/// Describes how the borrower is benefiting from a loan transaction. AKA: Tangible Benefit Letter.
		/// </summary>
        [XmlEnum("StatementOfBorrowerBenefit")]
        StatementOfBorrowerBenefit,

		/// <summary>
		/// Confirms legal ownership of stock in a corporation.
		/// </summary>
        [XmlEnum("StockCertificate")]
        StockCertificate,

		/// <summary>
		/// Establishes that a prior lien is made inferior to an otherwise junior lien.  An Agreement between two creditors (lien holders) of a particular borrower.
		/// </summary>
        [XmlEnum("SubordinationAgreement")]
        SubordinationAgreement,

		/// <summary>
		/// Establishes that the borrower will set up a subsidy account to supplement the mortgage payment.
		/// </summary>
        [XmlEnum("SubsidyAgreement")]
        SubsidyAgreement,

		/// <summary>
		/// Names a successor trustee. AKA: Appointment of Successor Trustee.
		/// </summary>
        [XmlEnum("SubstitutionOfTrustee")]
        SubstitutionOfTrustee,

		/// <summary>
		/// Describes encroachments on mortgaged property. AKA: Improvement Location Certificate, Mortgage Survey.
		/// </summary>
        [XmlEnum("Survey")]
        Survey,

		/// <summary>
		/// Confirms that no encroachments have been built since the preceding survey. Provided by the borrower.
		/// </summary>
        [XmlEnum("SurveyAffidavit")]
        SurveyAffidavit,

		/// <summary>
		/// Authorizes the tax collector to send the tax bill to the respective mortgagee, servicing or processing organization. AKA: Tax Escrow Account Designation, Initial Tax Authorization Notice.
		/// </summary>
        [XmlEnum("TaxAuthorization")]
        TaxAuthorization,

		/// <summary>
		/// Describes taxes charged by state or local governments. AKA: Tax Information Sheet, Tax Bill, Property Tax Statement.
		/// </summary>
        [XmlEnum("TaxCertificate")]
        TaxCertificate,

		/// <summary>
		/// Evidences a tax lien against a property, person, or entity.
		/// </summary>
        [XmlEnum("TaxLien")]
        TaxLien,

        [XmlEnum("TaxLien:Federal")]
        TaxLienFederal,

        [XmlEnum("TaxLien:Local")]
        TaxLienLocal,

        [XmlEnum("TaxLien:State")]
        TaxLienState,

		/// <summary>
		/// Releases a tax lien against a property, person, or entity. AKA: Satisfaction of Taxes.
		/// </summary>
        [XmlEnum("TaxLienRelease")]
        TaxLienRelease,

        [XmlEnum("TaxLienRelease:Federal")]
        TaxLienReleaseFederal,

        [XmlEnum("TaxLienRelease:Local")]
        TaxLienReleaseLocal,

        [XmlEnum("TaxLienRelease:State")]
        TaxLienReleaseState,

		/// <summary>
		/// A document which assigns a taxpayer identifier to a party. Evidences a party's identity and taxpayer identifier value. 
		/// </summary>
        [XmlEnum("TaxpayerIdentification")]
        TaxpayerIdentification,

		/// <summary>
		/// In Bolivia, Colombia, El Salvador and Guatemala the taxpayer identifier is called NÃºmero de IdentificaciÃ³n Tributaria, or NIT. Evidences a person's identity and taxpayer identifier.
		/// </summary>
        [XmlEnum("TaxpayerIdentification:NumeroDeIdentificacionTributaria")]
        TaxpayerIdentificationNumeroDeIdentificacionTributaria,

		/// <summary>
		/// Reports information used to calculate income and income taxes. This is a generic document type used for any IRS tax return document when the specific IRS tax return document type is unknown or unnecessary. Valid AKA: Partnership Return, Corporate Return, Individual Return, S-Corporation, Schedule K1, Schedule D.
		/// </summary>
        [XmlEnum("TaxReturn")]
        TaxReturn,

		/// <summary>
		/// Confirms existence of the insurance policy that covers the property against defects in workmanship. Provided by Insurance company. AKA: 2-10 Warranty.
		/// </summary>
        [XmlEnum("TenYearWarranty")]
        TenYearWarranty,

		/// <summary>
		/// Used when a third party employment verification vendor provides verification of employment.
		/// </summary>
        [XmlEnum("ThirdPartyEmploymentStatement")]
        ThirdPartyEmploymentStatement,

		/// <summary>
		/// Informs the borrower of the annual percentage rate, or APR, of the borrower's loan as well as other facets of the mortgage program.  Required by TILA / Reg Z. AKA: TIL, TILD.
		/// </summary>
        [XmlEnum("TILDisclosure")]
        TILDisclosure,

		/// <summary>
		/// Describes the result of a title search of public records.  This is the source document that produces the conditions in a title commitment.  Provided by Title Insurance company. AKA: Title Search.
		/// </summary>
        [XmlEnum("TitleAbstract")]
        TitleAbstract,

		/// <summary>
		/// Establishes a commitment to issue Title insurance subject to payment or other specified conditions.  Provided by the Title Insurance company. AKA: Preliminary Title Report, Prelim.
		/// </summary>
        [XmlEnum("TitleCommitment")]
        TitleCommitment,

		/// <summary>
		/// Changes one or more of the terms of the Title Insurance Policy.
		/// </summary>
        [XmlEnum("TitleInsuranceEndorsement")]
        TitleInsuranceEndorsement,

		/// <summary>
		/// Establishes that the insurer agrees to pay the insured (a purchaser or mortgagee with an interest in the property) a specific amount for any loss caused by defects of title to the real estate. AKA: ALTA, CLTA, TLTA, Title Certification, Attorney's Title Opinion Letter.
		/// </summary>
        [XmlEnum("TitleInsurancePolicy")]
        TitleInsurancePolicy,

		/// <summary>
		/// A document that describes the terms of the trial modification and the payment due dates. Examples: Fannie Mae Form 195, Fannie Mae/Freddie Mac Form 3156 3/09 (rev. 3/09).
		/// </summary>
        [XmlEnum("TrialPeriodPlanNotice")]
        TrialPeriodPlanNotice,

		/// <summary>
		/// Establishes the terms of a Trust, including the rights and responsibilities of the trustee and the trustor. AKA: Revocable Trust, Irrevocable Trust, Living Trust, Testamentary Trust, Inter Vivos Trust.
		/// </summary>
        [XmlEnum("TrustAgreement")]
        TrustAgreement,

		/// <summary>
		/// Describes property taken as collateral from a borrower.  Required to be filed in public records to record security interest in the property. AKA: Uniform Commercial Code Financing Statement, Notice of Security Interest.
		/// </summary>
        [XmlEnum("UCC1Statement")]
        UCC1Statement,

		/// <summary>
		/// Changes the UCC-1 by amending or terminating the security interest. AKA: Uniform Commercial Code Financing Statement Amendment.
		/// </summary>
        [XmlEnum("UCC3Statement")]
        UCC3Statement,

		/// <summary>
		/// Describes key information utilized in the comprehensive risk assessment of the mortgage, and the final underwriting decision.  Used for manually-processed applications for single-family conventional first and second mortgages. AKA: Uniform Underwriting and Transmittal Summary, Transmittal Summary, FNMA 1008.
		/// </summary>
        [XmlEnum("UnderwritingTransmittal")]
        UnderwritingTransmittal,

		/// <summary>
		/// Acknowledges the borrower's awareness that the property was not inspected and will not qualify for government assistance in the correction of structural defects. AKA: Acknowledgement of No Inspection (VA).
		/// </summary>
        [XmlEnum("VAAcknowledgmentOfNoInspection")]
        VAAcknowledgmentOfNoInspection,

		/// <summary>
		/// Confirms a person's veteran status and prior home loan usage. AKA: 26-1880, Certificate of Eligibility.
		/// </summary>
        [XmlEnum("VACertificateOfEligibility")]
        VACertificateOfEligibility,

		/// <summary>
		/// Confirms the maximum value and loan amount for a VA mortgage. AKA: CRM, Master Certificate of Reasonable Value / MCRV.
		/// </summary>
        [XmlEnum("VACertificateOfReasonableValue")]
        VACertificateOfReasonableValue,

		/// <summary>
		/// Informs the borrower of the actions that can be taken by the VA if a borrower defaults on a loan.
		/// </summary>
        [XmlEnum("VACollectionPolicyNotice")]
        VACollectionPolicyNotice,

		/// <summary>
		/// Describes the procedure for purchasing a foreclosed VA property.
		/// </summary>
        [XmlEnum("VAForeclosureBidLetter")]
        VAForeclosureBidLetter,

		/// <summary>
		/// Informs the borrower of VA funding fee costs.
		/// </summary>
        [XmlEnum("VAFundingFeeNotice")]
        VAFundingFeeNotice,

		/// <summary>
		/// Determines amount current VA homeowners can reduce their interest rate with no out of pocket expenses and minimal documentation. AKA: IRRRL.
		/// </summary>
        [XmlEnum("VAInterestRateReductionRefinancingLoanWorksheet")]
        VAInterestRateReductionRefinancingLoanWorksheet,

		/// <summary>
		/// Determines whether a veteran qualifies for a VA loan and whether a lenders adheres to VA standards.
		/// </summary>
        [XmlEnum("VALoanAnalysis")]
        VALoanAnalysis,

		/// <summary>
		/// VA Form 26-0286. Describes key information utilized in the comprehensive risk assessment of the Veterans's mortgage application, and the final underwriting decision. This is similar but is not the Underwriting Transmittal.
		/// </summary>
        [XmlEnum("VALoanSummarySheet")]
        VALoanSummarySheet,

		/// <summary>
		/// Details an update or correction to an appraisal report. AKA: Appraisal Update.
		/// </summary>
        [XmlEnum("ValuationUpdate")]
        ValuationUpdate,

		/// <summary>
		/// Acknowledges the borrower is aware of the terms of the existing loan and the terms of the new loan, whether the payment will be increasing or decreasing and how long it takes to recoup all closing costs. AKA: VA Old vs. New Loan Statement.
		/// </summary>
        [XmlEnum("VARateReductionRefinanceInformationAndAgreement")]
        VARateReductionRefinanceInformationAndAgreement,

		/// <summary>
		/// Describes details of a VA closed loan.
		/// </summary>
        [XmlEnum("VAReportAndCertificationOfLoanDisbursement")]
        VAReportAndCertificationOfLoanDisbursement,

		/// <summary>
		/// Provides VA with information to make determination of a borrower's VA eligibility. AKA: Request for Certificate of Eligibility.
		/// </summary>
        [XmlEnum("VARequestForCertificationOfEligibilityForHomeLoanBenefit")]
        VARequestForCertificationOfEligibilityForHomeLoanBenefit,

		/// <summary>
		/// Confirms whether a veteran has the ability to incur debt on a loan.
		/// </summary>
        [XmlEnum("VAVerificationOfBenefitRelatedIndebtedness")]
        VAVerificationOfBenefitRelatedIndebtedness,

		/// <summary>
		/// Confirms borrower credit information. AKA: VOC.
		/// </summary>
        [XmlEnum("VerificationOfCredit")]
        VerificationOfCredit,

		/// <summary>
		/// Confirms dependent care expenses paid by borrower. AKA: VODC.
		/// </summary>
        [XmlEnum("VerificationOfDependentCare")]
        VerificationOfDependentCare,

		/// <summary>
		/// Confirms borrowers deposit information.  AKA VOD, Verification of Asset.
		/// </summary>
        [XmlEnum("VerificationOfDeposit")]
        VerificationOfDeposit,

		/// <summary>
		/// Confirms borrowers employment information. AKA: VOE.
		/// </summary>
        [XmlEnum("VerificationOfEmployment")]
        VerificationOfEmployment,

		/// <summary>
		/// Confirms mortgage or rent expenses paid by borrower. AKA: VOM,  Verification of Rent, VOR.
		/// </summary>
        [XmlEnum("VerificationOfMortgageOrRent")]
        VerificationOfMortgageOrRent,

		/// <summary>
		/// Confirms information about securities owned by borrower. AKA: VOS.
		/// </summary>
        [XmlEnum("VerificationOfSecurities")]
        VerificationOfSecurities,

		/// <summary>
		/// Confirms the borrowers social security number. AKA: SSA-89.
		/// </summary>
        [XmlEnum("VerificationOfSSN")]
        VerificationOfSSN,

		/// <summary>
		/// A document that authorizes a person to enter the territory for which it was issued. A visa may be a document, but more commonly it is a stamp endorsed in the person's passport. Evidences a person's identity and visa identifier..
		/// </summary>
        [XmlEnum("Visa")]
        Visa,

		/// <summary>
		/// Authorizes lender to increase borrower's escrow accounts. AKA: VEPD.
		/// </summary>
        [XmlEnum("VolunteerEscrowPrepaymentDesignation")]
        VolunteerEscrowPrepaymentDesignation,

		/// <summary>
		/// Describes the procedure for wiring funds to escrow account. Provided by closing agent.
		/// </summary>
        [XmlEnum("WireInstructions")]
        WireInstructions,

		/// <summary>
		/// Authorizes a wire transfer of funds. AKA: Wire Set Up Authorization, Seller Wire/Change Authorization.
		/// </summary>
        [XmlEnum("WireTransferAuthorization")]
        WireTransferAuthorization,

		/// <summary>
		/// Confirms that the recipient is in receipt of funds sent via bank wire process.
		/// </summary>
        [XmlEnum("WireTransferConfirmation")]
        WireTransferConfirmation,

		/// <summary>
		/// Describes the procedure for performing an analysis or making a determination and captures results of the work.
		/// </summary>
        [XmlEnum("Worksheet")]
        Worksheet,

    }
}
