namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// The case has been removed from consideration by the court.
    /// </summary>
    public enum CreditPublicRecordDispositionBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Case has been settled by a court.
		/// </summary>
        [XmlEnum("Adjudicated")]
        Adjudicated,

		/// <summary>
		/// Application has been made to have a case reviewed by a higher court.
		/// </summary>
        [XmlEnum("Appealed")]
        Appealed,

		/// <summary>
		/// Case has been revoked or annulled.
		/// </summary>
        [XmlEnum("Canceled")]
        Canceled,

		/// <summary>
		/// Terms of a payment plan or agreement have been fulfilled.
		/// </summary>
        [XmlEnum("Completed")]
        Completed,

		/// <summary>
		/// Bankruptcy has been changed from one type to another, or property has been exchanged for another type of property.
		/// </summary>
        [XmlEnum("Converted")]
        Converted,

		/// <summary>
		/// Contract has been canceled or subject has been released from debt.
		/// </summary>
        [XmlEnum("Discharged")]
        Discharged,

		/// <summary>
		/// Case has been removed from consideration.
		/// </summary>
        [XmlEnum("Dismissed")]
        Dismissed,

		/// <summary>
		/// Proceeds from a forced sale have been paid to creditors.
		/// </summary>
        [XmlEnum("Distributed")]
        Distributed,

		/// <summary>
		/// A case, action or application have been initiated.
		/// </summary>
        [XmlEnum("Filed")]
        Filed,

		/// <summary>
		/// A request has been agreed to.
		/// </summary>
        [XmlEnum("Granted")]
        Granted,

		/// <summary>
		/// Contract or debt has been canceled involuntarily.
		/// </summary>
        [XmlEnum("InvoluntarilyDischarged")]
        InvoluntarilyDischarged,

		/// <summary>
		/// Case has not been settled or determined by a court.
		/// </summary>
        [XmlEnum("Nonadjudicated")]
        Nonadjudicated,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Case has been discharged or settled by transfer of money or goods.
		/// </summary>
        [XmlEnum("Paid")]
        Paid,

		/// <summary>
		/// Debt or claim has been paid, but satisfaction has not been filed with the court.
		/// </summary>
        [XmlEnum("PaidNotSatisfied")]
        PaidNotSatisfied,

		/// <summary>
		/// Case is awaiting a decision or settlement.
		/// </summary>
        [XmlEnum("Pending")]
        Pending,

		/// <summary>
		/// Case was settled by selling of real estate property.
		/// </summary>
        [XmlEnum("RealEstateSold")]
        RealEstateSold,

		/// <summary>
		/// Contract or debt has been canceled.
		/// </summary>
        [XmlEnum("Released")]
        Released,

		/// <summary>
		/// Case has been revoked or annulled.
		/// </summary>
        [XmlEnum("Rescinded")]
        Rescinded,

		/// <summary>
		/// Declaration by a party in whose favor a judgment was rendered, that he has been paid and satisfied.
		/// </summary>
        [XmlEnum("Satisfied")]
        Satisfied,

		/// <summary>
		/// Parties to a lawsuit have resolved their differences without a trial.
		/// </summary>
        [XmlEnum("Settled")]
        Settled,

        [XmlEnum("Unknown")]
        Unknown,

		/// <summary>
		/// Contract or debt has not been canceled.
		/// </summary>
        [XmlEnum("Unreleased")]
        Unreleased,

		/// <summary>
		/// Satisfaction notice has not been filed by party in whose favor a judgment was rendered.
		/// </summary>
        [XmlEnum("Unsatisfied")]
        Unsatisfied,

		/// <summary>
		/// The case has been declared void, set aside.
		/// </summary>
        [XmlEnum("Vacated")]
        Vacated,

		/// <summary>
		/// Contract or debt has been canceled voluntarily.
		/// </summary>
        [XmlEnum("VoluntarilyDischarged")]
        VoluntarilyDischarged,

		/// <summary>
		/// The case has been removed from consideration by the court.
		/// </summary>
        [XmlEnum("Withdrawn")]
        Withdrawn,

    }
}
