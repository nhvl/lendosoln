namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum SalesConcessionBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Automobile")]
        Automobile,

        [XmlEnum("ClosingCosts")]
        ClosingCosts,

        [XmlEnum("Downpayment")]
        Downpayment,

        [XmlEnum("InteriorDecorationAllowance")]
        InteriorDecorationAllowance,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Prepaids")]
        Prepaids,

        [XmlEnum("Repairs")]
        Repairs,

        [XmlEnum("ThirdPartyFinancingDiscounts")]
        ThirdPartyFinancingDiscounts,

        [XmlEnum("Unknown")]
        Unknown,

    }
}
