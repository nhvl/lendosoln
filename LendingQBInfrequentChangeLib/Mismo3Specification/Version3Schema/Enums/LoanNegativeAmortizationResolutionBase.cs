namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Extend the term as needed to fully amortize the loan up to the maximum allowed stated term on the note.
    /// </summary>
    public enum LoanNegativeAmortizationResolutionBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("NoMoreNegativeAmortizationAllowed")]
        NoMoreNegativeAmortizationAllowed,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Recalculate the principal and interest payment to fully amortize the loan over the remaining term.
		/// </summary>
        [XmlEnum("PaymentRecastToFullyAmortizingTermRemainsTheSame")]
        PaymentRecastToFullyAmortizingTermRemainsTheSame,

		/// <summary>
		/// Borrower pays the amount by which the loan has negatively amortized.
		/// </summary>
        [XmlEnum("PayoffDifference")]
        PayoffDifference,

		/// <summary>
		/// Modify the term of the mortgage and recalculate the fully amortizing mortgage over the newly extended remaining term.
		/// </summary>
        [XmlEnum("TermExtendedAndPaymentRecastToFullyAmortizing")]
        TermExtendedAndPaymentRecastToFullyAmortizing,

		/// <summary>
		/// Extend the term as needed to fully amortize the loan up to the maximum allowed stated term on the note.
		/// </summary>
        [XmlEnum("TermExtendedPaymentRemainsTheSame")]
        TermExtendedPaymentRemainsTheSame,

    }
}
