namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum RentalFeatureBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Age")]
        Age,

        [XmlEnum("Condition")]
        Condition,

        [XmlEnum("Lease")]
        Lease,

        [XmlEnum("Location")]
        Location,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("UtilitiesIncluded")]
        UtilitiesIncluded,

    }
}
