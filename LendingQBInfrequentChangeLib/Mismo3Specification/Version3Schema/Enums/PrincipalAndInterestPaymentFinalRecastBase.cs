namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum PrincipalAndInterestPaymentFinalRecastBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("None")]
        None,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("RecastOnceOnFinalPaymentChangeDate")]
        RecastOnceOnFinalPaymentChangeDate,

        [XmlEnum("RecastOnFinalPaymentChangeDateAndEveryMonthThereafter")]
        RecastOnFinalPaymentChangeDateAndEveryMonthThereafter,

    }
}
