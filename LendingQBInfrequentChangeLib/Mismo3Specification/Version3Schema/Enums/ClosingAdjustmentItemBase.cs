namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Amount of funds held by lender or servicer to be disbursed to pay unpaid utilities on the subject property that have not been billed as of the closing date.
    /// </summary>
    public enum ClosingAdjustmentItemBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// The value of any fuel stored on the subject property that may be transferred to a subsequent owner.
		/// </summary>
        [XmlEnum("FuelCosts")]
        FuelCosts,

		/// <summary>
		/// Funds or equity provided as a personal gift to the borrower at the time of closing that does not require repayment.
		/// </summary>
        [XmlEnum("Gift")]
        Gift,

		/// <summary>
		/// Amount paid on behalf of the borrower toward down payment and/or closing costs by a religious organization, municipality, non-profit organization (excluding credit unions) or public agency that does not require repayment.
		/// </summary>
        [XmlEnum("Grant")]
        Grant,

		/// <summary>
		/// A lump sum amount credited from the lender, correspondent lender or mortgage broker to the borrower toward closing costs.
		/// </summary>
        [XmlEnum("LenderCredit")]
        LenderCredit,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Dollar amount of proceeds from a subordinate lien secured by the subject property that are being applied to the subject transaction (may be less than the full note loan amount).
		/// </summary>
        [XmlEnum("ProceedsOfSubordinateLiens")]
        ProceedsOfSubordinateLiens,

		/// <summary>
		/// A premium in the form of cash, services or merchandise to the consumer to reduce the cost otherwise borne by the consumer.
		/// </summary>
        [XmlEnum("RebateCredit")]
        RebateCredit,

		/// <summary>
		/// Funds provided to relocating borrower (usually by employer) to offset some of the initial or ongoing costs of borrower's home at the new location.
		/// </summary>
        [XmlEnum("RelocationFunds")]
        RelocationFunds,

		/// <summary>
		/// Periodic amount paid by a tenant for occupancy of a property.
		/// </summary>
        [XmlEnum("RentFromSubjectProperty")]
        RentFromSubjectProperty,

		/// <summary>
		/// Amount of funds held by lender or servicer to be disbursed when repairs, construction or other improvements are completed on the subject property.
		/// </summary>
        [XmlEnum("RepairCompletionEscrowHoldback")]
        RepairCompletionEscrowHoldback,

		/// <summary>
		/// Repairs made to the subject property and agreed to between the buyer and the seller in a real estate transaction.
		/// </summary>
        [XmlEnum("Repairs")]
        Repairs,

		/// <summary>
		/// Payment to subordinate lien holder by or on behalf of the borrower or seller in connection with the subordinate mortgage payoff.
		/// </summary>
        [XmlEnum("SatisfactionOfSubordinateLien")]
        SatisfactionOfSubordinateLien,

		/// <summary>
		/// A lump sum amount from the property seller to the borrower toward closing costs or where the property seller is making an allowance to the consumer for items to purchase separately. 
		/// </summary>
        [XmlEnum("SellerCredit")]
        SellerCredit,

		/// <summary>
		/// Any transferred escrow balance from the seller to the borrower.
		/// </summary>
        [XmlEnum("SellersEscrowAssumption")]
        SellersEscrowAssumption,

		/// <summary>
		/// The value of the current mortgage insurance policy being transferred from property seller to borrower.
		/// </summary>
        [XmlEnum("SellersMortgageInsuranceAssumption")]
        SellersMortgageInsuranceAssumption,

		/// <summary>
		/// Charge or reimbursement for services provided or work completed at the location of the subject property.
		/// </summary>
        [XmlEnum("Services")]
        Services,

		/// <summary>
		/// The net loan funds from a lien secured against a property that is in a subordinate lien position. 
		/// </summary>
        [XmlEnum("SubordinateFinancingProceeds")]
        SubordinateFinancingProceeds,

		/// <summary>
		/// The value of borrower's labor and/or cost of materials contributed by the borrower in construction or renovation to subject property in lieu of some portion of the cash purchase price of the subject property.
		/// </summary>
        [XmlEnum("SweatEquity")]
        SweatEquity,

		/// <summary>
		/// Amount of security deposit paid by a tenant to a property seller that is transferrable to the borrower purchasing the subject property.
		/// </summary>
        [XmlEnum("TenantSecurityDeposit")]
        TenantSecurityDeposit,

		/// <summary>
		/// Amount of value exchanged when property seller and buyer are trading equity in other real property in lieu of some portion of the cash purchase price of the subject property. IRS 1031 Tax Exchange is one type.
		/// </summary>
        [XmlEnum("TradeEquity")]
        TradeEquity,

		/// <summary>
		/// Amount of funds held by lender or servicer to be disbursed to pay unpaid utilities on the subject property that have not been billed as of the closing date.
		/// </summary>
        [XmlEnum("UnpaidUtilityEscrowHoldback")]
        UnpaidUtilityEscrowHoldback,

    }
}
