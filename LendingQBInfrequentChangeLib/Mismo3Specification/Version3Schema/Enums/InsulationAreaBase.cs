namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum InsulationAreaBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Ceiling")]
        Ceiling,

        [XmlEnum("Floor")]
        Floor,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Roof")]
        Roof,

        [XmlEnum("Walls")]
        Walls,

    }
}
