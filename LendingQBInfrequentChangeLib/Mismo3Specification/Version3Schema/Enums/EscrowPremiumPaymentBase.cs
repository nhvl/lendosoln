namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum EscrowPremiumPaymentBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("CollectAtClosing")]
        CollectAtClosing,

        [XmlEnum("PaidOutsideOfClosing")]
        PaidOutsideOfClosing,

        [XmlEnum("Waived")]
        Waived,

    }
}
