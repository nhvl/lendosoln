namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Update a data item value.
    /// </summary>
    public enum DataItemChangeBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Add a new data item value.
		/// </summary>
        [XmlEnum("Add")]
        Add,

		/// <summary>
		/// Delete a data item value.
		/// </summary>
        [XmlEnum("Delete")]
        Delete,

		/// <summary>
		/// Update a data item value.
		/// </summary>
        [XmlEnum("Update")]
        Update,

    }
}
