namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// The document requires an original handwritten signature.
    /// </summary>
    public enum AcceptableSigningMethodBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// The document requires either electronic or wet signatures.
		/// </summary>
        [XmlEnum("Any")]
        Any,

		/// <summary>
		/// The document requires electronic signatures.
		/// </summary>
        [XmlEnum("Electronic")]
        Electronic,

		/// <summary>
		/// The document does not require signatures. 
		/// </summary>
        [XmlEnum("None")]
        None,

		/// <summary>
		/// The document requires an original handwritten signature.
		/// </summary>
        [XmlEnum("Wet")]
        Wet,
    }
}
