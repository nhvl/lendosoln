namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum CertificateStatusBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Approved")]
        Approved,

        [XmlEnum("Cancelled")]
        Cancelled,

        [XmlEnum("Denied")]
        Denied,

        [XmlEnum("Expired")]
        Expired,

        [XmlEnum("InApplication")]
        InApplication,

        [XmlEnum("Other")]
        Other,

    }
}
