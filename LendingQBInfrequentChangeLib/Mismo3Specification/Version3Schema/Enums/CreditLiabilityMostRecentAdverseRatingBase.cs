namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Past due debt on an account is being paid according to terms established by a court or agreement with the creditor.
    /// </summary>
    public enum CreditLiabilityMostRecentAdverseRatingBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Account is being paid on time and according to terms of the credit agreement.
		/// </summary>
        [XmlEnum("AsAgreed")]
        AsAgreed,

		/// <summary>
		/// The exact type could not be determined.
		/// </summary>
        [XmlEnum("BankruptcyOrWageEarnerPlan")]
        BankruptcyOrWageEarnerPlan,

		/// <summary>
		/// The account was not paid on a timely basis and the debt is being written off as uncollectible by the creditor.
		/// </summary>
        [XmlEnum("ChargeOff")]
        ChargeOff,

		/// <summary>
		/// Account has not been paid on a timely basis and is been turned over to an attorney or agency for collection.
		/// </summary>
        [XmlEnum("Collection")]
        Collection,

		/// <summary>
		/// The exact type could not be determined.
		/// </summary>
        [XmlEnum("CollectionOrChargeOff")]
        CollectionOrChargeOff,

		/// <summary>
		/// Legal proceedings have been initiated to sell property to settle debt on an unpaid balance.
		/// </summary>
        [XmlEnum("Foreclosure")]
        Foreclosure,

		/// <summary>
		/// The exact type could not be determined.
		/// </summary>
        [XmlEnum("ForeclosureOrRepossession")]
        ForeclosureOrRepossession,

		/// <summary>
		/// Account is between 30 and 59 days past due.
		/// </summary>
        [XmlEnum("Late30Days")]
        Late30Days,

		/// <summary>
		/// Account is between 60 and 89 days past due.
		/// </summary>
        [XmlEnum("Late60Days")]
        Late60Days,

		/// <summary>
		/// Account is betwen 90 and 119 days past due.
		/// </summary>
        [XmlEnum("Late90Days")]
        Late90Days,

		/// <summary>
		/// Account is 120 days or more past due.
		/// </summary>
        [XmlEnum("LateOver120Days")]
        LateOver120Days,

		/// <summary>
		/// Status of the account is not known.
		/// </summary>
        [XmlEnum("NoDataAvailable")]
        NoDataAvailable,

		/// <summary>
		/// Account has not been paid on a timely basis and proceedings have been initiated to repossess collateral property to partially of fully satisfy the debt.
		/// </summary>
        [XmlEnum("Repossession")]
        Repossession,

		/// <summary>
		/// Account has been recently established and is too new to rate payment history.
		/// </summary>
        [XmlEnum("TooNew")]
        TooNew,

		/// <summary>
		/// Past due debt on an account is being paid according to terms established by a court or agreement with the creditor.
		/// </summary>
        [XmlEnum("WageEarnerPlan")]
        WageEarnerPlan,

    }
}
