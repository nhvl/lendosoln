namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum RoofMaterialBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Aluminum")]
        Aluminum,

        [XmlEnum("Asphalt")]
        Asphalt,

        [XmlEnum("Clay")]
        Clay,

        [XmlEnum("Composition")]
        Composition,

        [XmlEnum("Concrete")]
        Concrete,

        [XmlEnum("FireRetardant")]
        FireRetardant,

        [XmlEnum("Gravel")]
        Gravel,

        [XmlEnum("Metal")]
        Metal,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Roll")]
        Roll,

        [XmlEnum("Shake")]
        Shake,

        [XmlEnum("Shingle")]
        Shingle,

        [XmlEnum("Slate")]
        Slate,

        [XmlEnum("SpanishTile")]
        SpanishTile,

        [XmlEnum("Stone")]
        Stone,

        [XmlEnum("Synthetic")]
        Synthetic,

        [XmlEnum("Tar")]
        Tar,

        [XmlEnum("Tile")]
        Tile,

        [XmlEnum("Tin")]
        Tin,

        [XmlEnum("Wood")]
        Wood,

    }
}
