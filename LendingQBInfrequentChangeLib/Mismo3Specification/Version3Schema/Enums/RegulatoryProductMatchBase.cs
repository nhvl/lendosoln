namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum RegulatoryProductMatchBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Address")]
        Address,

        [XmlEnum("City")]
        City,

        [XmlEnum("FirstName")]
        FirstName,

        [XmlEnum("LastName")]
        LastName,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("PostalCode")]
        PostalCode,

        [XmlEnum("State")]
        State,

    }
}
