namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A loss suffered by a mortgaged property that is classified as a special hazard in the governing documents.
    /// </summary>
    public enum ChargeOffItemBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// A loss related to a second lien.
		/// </summary>
        [XmlEnum("AdditionalLien")]
        AdditionalLien,

		/// <summary>
		/// A loss resulting from a deficient valuation or a debt service reduction in a bankruptcy.
		/// </summary>
        [XmlEnum("Bankruptcy")]
        Bankruptcy,

		/// <summary>
		/// A loss resulting from intentional misstatement, misrepresentation, or omission by an applicant or other interested parties.
		/// </summary>
        [XmlEnum("Fraud")]
        Fraud,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// A loss from uncollected interest.
		/// </summary>
        [XmlEnum("UncollectedInterest")]
        UncollectedInterest,

		/// <summary>
		/// A loss from uncollected principal.
		/// </summary>
        [XmlEnum("UncollectedPrincipal")]
        UncollectedPrincipal,

		/// <summary>
		/// A loss suffered by a mortgaged property that is classified as a special hazard in the governing documents.
		/// </summary>
        [XmlEnum("UnrecoverableSpecialHazard")]
        UnrecoverableSpecialHazard,

    }
}
