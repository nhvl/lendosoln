namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum RestrictionBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Age")]
        Age,

        [XmlEnum("AnimalPet")]
        AnimalPet,

        [XmlEnum("Architectural")]
        Architectural,

        [XmlEnum("CommonArea")]
        CommonArea,

        [XmlEnum("HistoricPreservation")]
        HistoricPreservation,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("PropertyUse")]
        PropertyUse,

        [XmlEnum("Racial")]
        Racial,

        [XmlEnum("Rental")]
        Rental,

        [XmlEnum("Resale")]
        Resale,

        [XmlEnum("Unenforceable")]
        Unenforceable,

    }
}
