namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// The bankruptcy plan was amended due to the reduction of the plan length.
    /// </summary>
    public enum BankruptcyPlanAmendedBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// A debtor has filed for bankruptcy in another country and has filed for Chapter 15 in the US Bankruptcy Court for protection under US law.
		/// </summary>
        [XmlEnum("ChapterFifteen")]
        ChapterFifteen,

		/// <summary>
		/// The bankruptcy plan was amended due to the extension of the plan date.
		/// </summary>
        [XmlEnum("PlanDateExtended")]
        PlanDateExtended,

		/// <summary>
		/// The bankruptcy plan was amended due to an increase in the plan disbursement amount.
		/// </summary>
        [XmlEnum("PlanDisbursementAmountIncreased")]
        PlanDisbursementAmountIncreased,

		/// <summary>
		/// The bankruptcy plan was amended due to a reduction in the plan disbursement amount.
		/// </summary>
        [XmlEnum("PlanDisbursementAmountReduced")]
        PlanDisbursementAmountReduced,

		/// <summary>
		/// The bankruptcy plan was amended due to the reduction of the plan length.
		/// </summary>
        [XmlEnum("PlanLengthReduced")]
        PlanLengthReduced,
    }
}
