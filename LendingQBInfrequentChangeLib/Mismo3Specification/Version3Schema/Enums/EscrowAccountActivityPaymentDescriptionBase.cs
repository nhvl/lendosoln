namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum EscrowAccountActivityPaymentDescriptionBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Assessment")]
        Assessment,

        [XmlEnum("CityPropertyTax")]
        CityPropertyTax,

        [XmlEnum("CountyPropertyTax")]
        CountyPropertyTax,

        [XmlEnum("EarthquakeInsurance")]
        EarthquakeInsurance,

        [XmlEnum("FloodInsurance")]
        FloodInsurance,

        [XmlEnum("HazardInsurance")]
        HazardInsurance,

        [XmlEnum("MortgageInsurance")]
        MortgageInsurance,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("SchoolPropertyTax")]
        SchoolPropertyTax,

        [XmlEnum("TownPropertyTax")]
        TownPropertyTax,

        [XmlEnum("USDAAnnualFee")]
        USDAAnnualFee,

        [XmlEnum("VillagePropertyTax")]
        VillagePropertyTax,

        [XmlEnum("WindstormInsurance")]
        WindstormInsurance,

    }
}
