namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum NeighborhoodInfluenceBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("AdequacyOfShopping")]
        AdequacyOfShopping,

        [XmlEnum("AdequacyOfUtilities")]
        AdequacyOfUtilities,

        [XmlEnum("AppealToMarket")]
        AppealToMarket,

        [XmlEnum("Broadband")]
        Broadband,

        [XmlEnum("ConvenienceToEmployment")]
        ConvenienceToEmployment,

        [XmlEnum("ConvenienceToFreewayAccess")]
        ConvenienceToFreewayAccess,

        [XmlEnum("ConvenienceToGrammarSchool")]
        ConvenienceToGrammarSchool,

        [XmlEnum("ConvenienceToPublicTransportation")]
        ConvenienceToPublicTransportation,

        [XmlEnum("ConvenienceToShopping")]
        ConvenienceToShopping,

        [XmlEnum("EmploymentConditions")]
        EmploymentConditions,

        [XmlEnum("EmploymentOpportunities")]
        EmploymentOpportunities,

        [XmlEnum("EmploymentStability")]
        EmploymentStability,

        [XmlEnum("GeneralAppearanceOfProperties")]
        GeneralAppearanceOfProperties,

        [XmlEnum("HousingSupply")]
        HousingSupply,

        [XmlEnum("MarketConditions")]
        MarketConditions,

        [XmlEnum("MarketingTime")]
        MarketingTime,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("PoliceAndFireProtection")]
        PoliceAndFireProtection,

        [XmlEnum("PropertyCompatibility")]
        PropertyCompatibility,

        [XmlEnum("PropertyValues")]
        PropertyValues,

        [XmlEnum("ProtectionFromDetrimentalConditions")]
        ProtectionFromDetrimentalConditions,

        [XmlEnum("RecreationalFacilities")]
        RecreationalFacilities,

    }
}
