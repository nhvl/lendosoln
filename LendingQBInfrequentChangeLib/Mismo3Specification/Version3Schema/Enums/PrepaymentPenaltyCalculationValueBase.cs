namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A prepayment penalty that allows the lender to attain the same yield as if the borrower had made all scheduled mortgage payments until maturity.
    /// </summary>
    public enum PrepaymentPenaltyCalculationValueBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("FixedAmount")]
        FixedAmount,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("PercentageOfPrincipalBalance")]
        PercentageOfPrincipalBalance,

        [XmlEnum("QualifyingAmountOfCurtailment")]
        QualifyingAmountOfCurtailment,

        [XmlEnum("SpecifiedMonthsOfInterest")]
        SpecifiedMonthsOfInterest,

		/// <summary>
		/// A prepayment penalty that allows the lender to attain the same yield as if the borrower had made all scheduled mortgage payments until maturity.
		/// </summary>
        [XmlEnum("YieldMaintenance")]
        YieldMaintenance,

    }
}
