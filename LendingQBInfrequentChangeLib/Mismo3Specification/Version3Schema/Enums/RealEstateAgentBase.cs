namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// The agent that sold the property and represents the Buyer (may also be known as the Buyers Agent).
    /// </summary>
    public enum RealEstateAgentBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// The agent that listed the property and represents the Seller.
		/// </summary>
        [XmlEnum("Listing")]
        Listing,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// The agent that sold the property and represents the Buyer (may also be known as the Buyers Agent).
		/// </summary>
        [XmlEnum("Selling")]
        Selling,

    }
}
