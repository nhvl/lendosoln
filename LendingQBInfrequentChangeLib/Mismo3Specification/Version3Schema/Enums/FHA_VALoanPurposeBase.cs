namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum FHA_VALoanPurposeBase
    {
        /// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
        Blank,

        [XmlEnum("PurchaseExistingHomePreviouslyOccupied")]
        PurchaseExistingHomePreviouslyOccupied,

        [XmlEnum("FinanceImprovementsToExistingProperty")]
        FinanceImprovementsToExistingProperty,

        [XmlEnum("Refinance")]
        Refinance,

        [XmlEnum("PurchaseNewCondominiumUnit")]
        PurchaseNewCondominiumUnit,

        [XmlEnum("PurchaseExistingCondominiumUnit")]
        PurchaseExistingCondominiumUnit,

        [XmlEnum("PurchaseExistingHomeNotPreviouslyOccupied")]
        PurchaseExistingHomeNotPreviouslyOccupied,

        [XmlEnum("ConstructHome")]
        ConstructHome,

        [XmlEnum("FinanceCooperativePurchase")]
        FinanceCooperativePurchase,

        [XmlEnum("PurchasePermanentlySitedManufacturedHome")]
        PurchasePermanentlySitedManufacturedHome,

        [XmlEnum("PurchasePermanentlySitedManufacturedHomeAndLot")]
        PurchasePermanentlySitedManufacturedHomeAndLot,

        [XmlEnum("RefinancePermanentlySitedManufacturedHomeToBuyLot")]
        RefinancePermanentlySitedManufacturedHomeToBuyLot,

        [XmlEnum("RefinancePermanentlySitedManufacturedHomeLotLoan")]
        RefinancePermanentlySitedManufacturedHomeLotLoan
    }
}
