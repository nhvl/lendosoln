namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum ExecutionJudicialDistrictBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Borough")]
        Borough,

        [XmlEnum("City")]
        City,

        [XmlEnum("County")]
        County,

        [XmlEnum("District")]
        District,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Parish")]
        Parish,

        [XmlEnum("Town")]
        Town,

    }
}
