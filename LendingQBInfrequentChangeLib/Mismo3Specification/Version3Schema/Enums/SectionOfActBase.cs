namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Title II Section I-FHA Property Improvement Loan Insurance. Insures loans to finance improvements, alterations, and repairs of individual homes, apartment buildings, and nonresidential structures, as well as new construction of nonresidential buildings.
    /// </summary>
    public enum SectionOfActBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Section 184-Public and Indian Housing Loan Guarantees for Indian Housing guarantees mortgage loans to Indian families, IHAs, and Indian tribes that could not otherwise acquire housing financing because of the unique legal status of Indian lands. The loans guaranteed under the program are used to construct, acquire, refinance, or rehabilitate single family housing located on trust land or land located in an Indian or Alaska Native area.
		/// </summary>
        [XmlEnum("184")]
        Item184,

		/// <summary>
		/// Section 184-Public and Indian Housing Loan Guarantees for Indian Housing guarantees mortgage loans to Indian families, IHAs, and Indian tribes that could not otherwise acquire housing financing because of the unique legal status of Indian lands. The loans guaranteed under the program are used to construct, acquire, refinance, or rehabilitate single family housing located on trust land or land located in an Indian or Alaska Native area.
		/// </summary>
        [XmlEnum("184A")]
        Item184A,

		/// <summary>
		/// Referred to as 201(s) - Secured Title I - Direct Loan
		/// </summary>
        [XmlEnum("201S")]
        Item201S,

		/// <summary>
		/// Referred to as 201(s)(d) - Secured Title II - Dealer Loan
		/// </summary>
        [XmlEnum("201SD")]
        Item201SD,

		/// <summary>
		/// Referred to as 201(u) - Unsecured Title I - Direct Loan
		/// </summary>
        [XmlEnum("201U")]
        Item201U,

		/// <summary>
		/// Referred to as 201(u)(d) - Unsecured Title II - Dealer Loan
		/// </summary>
        [XmlEnum("201UD")]
        Item201UD,

		/// <summary>
		/// Section 203(b)-Federal mortgage insurance to finance homeownership and the construction and financing of housing.  
		/// </summary>
        [XmlEnum("203B")]
        Item203B,

		/// <summary>
		/// Referred to as 203(b)2. Purchase, Refinances - Veteran Status
		/// </summary>
        [XmlEnum("203B2")]
        Item203B2,

		/// <summary>
		/// Referred to as 203(b)/241. Purchase, Refinances - Native American
		/// </summary>
        [XmlEnum("203B241")]
        Item203B241,

		/// <summary>
		/// Referred to as 203(b)/251. Condominium/ARM
		/// </summary>
        [XmlEnum("203B251")]
        Item203B251,

		/// <summary>
		/// Section 203(h)-FHA Mortgage Insurance for Disaster Victims. Federal mortgage insurance for victims of a major disaster who have lost their homes and are in the process of rebuilding or buying another home.
		/// </summary>
        [XmlEnum("203H")]
        Item203H,

		/// <summary>
		/// Referred to as 203(I). Mortgage Insurance for Outlying Areas
		/// </summary>
        [XmlEnum("203I")]
        Item203I,

		/// <summary>
		/// Section 203(k)-FHA Rehabilitation Loan Insurance. Insures loans to finance the rehabilitation or purchase and rehabilitation of one- to four-family properties.
		/// </summary>
        [XmlEnum("203K")]
        Item203K,

		/// <summary>
		/// Referred to as 203(k)/241. A first mortgage that covers the costs of rehabilitation and purchase or refinance of an eligible property -Native American
		/// </summary>
        [XmlEnum("203K241")]
        Item203K241,

		/// <summary>
		/// Referred to as 203(k)/251. Purchase, Rehab/ARM
		/// </summary>
        [XmlEnum("203K251")]
        Item203K251,

		/// <summary>
		/// Section 204(a)-Helps homeowners with FHA-insured loans to effectively work with lenders to find creative solutions to avoid foreclosure.
		/// </summary>
        [XmlEnum("204A")]
        Item204A,

		/// <summary>
		/// Section 204(g)-FHA Good Neighbor Next Door Program. Provides law enforcement officers, teachers, firefighters, and emergency medical technicians with the opportunity to purchase homes located in revitalization areas at significant discount.
		/// </summary>
        [XmlEnum("204GFHAGoodNeighborNextDoor")]
        Item204GFHAGoodNeighborNextDoor,

		/// <summary>
		/// Section 204(g)-FHA Single Family Property Disposition Program. Disposes of one- to four-family FHA-owned properties in a manner targeted to expanding homeownership opportunities.
		/// </summary>
        [XmlEnum("204GSingleFamilyPropertyDisposition")]
        Item204GSingleFamilyPropertyDisposition,

		/// <summary>
		/// Cooperative Mortgages (individual share loans only) 
        /// (Inactive code. Deactivated on 10/26/09.)
		/// </summary>
        [XmlEnum("213")]
        Item213,

		/// <summary>
		/// 220: Mortgages in Urban Renewal Areas (individual share loans only) 
        /// (Inactive code. Deactivated on 10/26/09.)
		/// </summary>
        [XmlEnum("220")]
        Item220,

		/// <summary>
		/// Referred to as 221 - Low Cost and Moderate Income Mortgages 
        /// (Inactive code. Deactivated on 10/26/09.)
		/// </summary>
        [XmlEnum("221")]
        Item221,

		/// <summary>
		///  Referred to as 221(d)(2). Low and Moderate Income/ARM 
        /// (Inactive code. Deactivated on 10/26/09.)
		/// </summary>
        [XmlEnum("221D2")]
        Item221D2,

		/// <summary>
		/// Referred to as 221(d)(2)/251. Low and Moderate Income/ARM 
        /// (Inactive code. Deactivated on 10/26/09.)
		/// </summary>
        [XmlEnum("221D2251")]
        Item221D2251,

		/// <summary>
		/// Defense Service Members Mortgage
		/// </summary>
        [XmlEnum("222")]
        Item222,

		/// <summary>
		/// Section 223(e) FHA Mortgage Insurance for older, declining areas.  Federal insurance on mortgage loans to finance the purchase, rehabilitation, or construction of housing in older, declining, but still viable urban areas where conditions are such that normal requirements for mortgage insurance cannot be met.
		/// </summary>
        [XmlEnum("223E")]
        Item223E,

		/// <summary>
		/// Experimental Housing Units (individual mortgages only)
		/// </summary>
        [XmlEnum("233")]
        Item233,

		/// <summary>
		/// Section 234(c)-FHA Mortgage Insurance for Condominium Units. Federal mortgage insurance to finance the purchase of individual housing units in proposed or existing condominiums.
		/// </summary>
        [XmlEnum("234C")]
        Item234C,

		/// <summary>
		/// Referred to as 234(c)/251. Condominium/ARM
		/// </summary>
        [XmlEnum("234C251")]
        Item234C251,

		/// <summary>
		/// Homeownership Assistance for Lower Income Families
		/// </summary>
        [XmlEnum("235")]
        Item235,

		/// <summary>
		/// Special Credit Risks
		/// </summary>
        [XmlEnum("237")]
        Item237,

		/// <summary>
		/// Fee Simple Title from Lessors
		/// </summary>
        [XmlEnum("240")]
        Item240,

		/// <summary>
		/// Graduated Payment Mortgages (GPM) and Growing Equity Mortgages (GEM)
		/// </summary>
        [XmlEnum("245")]
        Item245,

		/// <summary>
		/// Section 245(a)-FHA Graduated Payment Mortgages (GPM) and Growing Equity Mortgages (GEM).  Enables a household with a limited income that is expected to rise to buy a home sooner by making mortgage payments that start small and increase gradually over time.  The GPM program offers five different plans varying in length of time and rate of increase of nominal interest rate.
		/// </summary>
        [XmlEnum("245A")]
        Item245A,

		/// <summary>
		/// Section 247-FHA Mortgages on Hawaiian Home Lands. Provides opportunities to low- and moderate-income Native Hawaiians to purchase a home on Hawaiian home lands, where, because a mortgage is taken on a homestead lease granted by the Department of Hawaiian Homelands, home financing may be difficult to find.
		/// </summary>
        [XmlEnum("247")]
        Item247,

		/// <summary>
		/// Section 248-FHA Insured Mortgages on Indian lands. Provides opportunities for low- and moderate-income Native Americans to purchase a home in their communities on Indianland, where, because of the complex title issues involved, financing may be difficult to find.
		/// </summary>
        [XmlEnum("248")]
        Item248,

		/// <summary>
		/// Section 251-FHA Adjustable Rate Mortgages.  Under this HUD-insured mortgage, the interest rate and monthly payment may change during the life of the loan.
		/// </summary>
        [XmlEnum("251")]
        Item251,

		/// <summary>
		/// Section 255-FHA Home Equity Conversion Mortgage (HECM) Program. Allows borrowers, who are at least 62 years of age, to convert the equity in their homes into a monthly stream of income or a line of credit.
		/// </summary>
        [XmlEnum("255")]
        Item255,

		/// <summary>
		/// Home Repair Services Grants For Older And Disabled Homeowners. 
        /// (Inactive code. Deactivated on 10/26/09.)
		/// </summary>
        [XmlEnum("256")]
        Item256,

		/// <summary>
		/// HOPE for Homeowners 
        /// (Inactive code. Deactivated on 10/26/09.)
		/// </summary>
        [XmlEnum("257")]
        Item257,

		/// <summary>
		/// Federal Housing Administration (FHA) Graduated Payment Mortgage (GPM) Plan I, Section of the Act 203(b)
		/// </summary>
        [XmlEnum("26101")]
        Item26101,

		/// <summary>
		/// Federal Housing Administration (FHA) Graduated Payment Mortgage (GPM) Plan II, Section of the Act 203(b)
		/// </summary>
        [XmlEnum("26102")]
        Item26102,

		/// <summary>
		/// Federal Housing Administration (FHA) Graduated Payment Mortgage (GPM) Plan I, Section of the Act 234(c)
		/// </summary>
        [XmlEnum("26201")]
        Item26201,

		/// <summary>
		/// Federal Housing Administration (FHA) Graduated Payment Mortgage (GPM) Plan II, Section of the Act 234(c)
		/// </summary>
        [XmlEnum("26202")]
        Item26202,

		/// <summary>
		/// Federal Housing Administration (FHA) Graduated Payment Mortgage (GPM) Plan I, Section of the Act 203(b)
		/// </summary>
        [XmlEnum("27001")]
        Item27001,

		/// <summary>
		/// Federal Housing Administration (FHA) Graduated Payment Mortgage (GPM) Plan II, Section of the Act 203(b)
		/// </summary>
        [XmlEnum("27002")]
        Item27002,

		/// <summary>
		/// Federal Housing Administration (FHA) Graduated Payment Mortgage (GPM) Plan III, Section of the Act 203(b)
		/// </summary>
        [XmlEnum("27003")]
        Item27003,

		/// <summary>
		/// Department of Veterans Affairs (VA) Fixed Rate Mortgages (Formerly 1810)
		/// </summary>
        [XmlEnum("3710")]
        Item3710,

		/// <summary>
		/// Section 502-USDA Rural Development Direct Housing Loan. Loans to low-income individuals or households purchase homes in rural areas. Funds can be used to build, repair, renovate or relocate a home, or to purchase and prepare sites, including providing water and sewage facilities. 
		/// </summary>
        [XmlEnum("502USDARuralDevelopmentGuaranteedHousingLoan")]
        Item502USDARuralDevelopmentGuaranteedHousingLoan,

		/// <summary>
		/// FHA Energy Efficient Mortgage Insurance.  Insures an FHA mortgage that exceeds the normal maximum loan limits if the mortgage includes an amount for the purchase of energy efficient improvements made or to be made to the property.
		/// </summary>
        [XmlEnum("513")]
        Item513,

		/// <summary>
		/// Government Adjustable Rate Mortgages From Lenders With Housing and Urban Development (HUD) Direct Endorsement Authority, Section of the Act 203(b)/251 
        /// (Inactive code. Deactivated on 10/26/09.)
		/// </summary>
        [XmlEnum("729")]
        Item729,

		/// <summary>
		/// Section 8(y)- Public and Indian Housing Homeownership Voucher Assistance Program Program enabling public housing agencies to provide monthly assistance in an amount that would otherwise have been provided as tenant-based voucher assistance to qualified Section 8 Housing Choice Voucher families who are purchasing homes.
		/// </summary>
        [XmlEnum("8Y")]
        Item8Y,

		/// <summary>
		/// Title II Section I-FHA Manufactured Home Loans Insurance. Insures loans to finance the purchase of manufactured homes or lots.
		/// </summary>
        [XmlEnum("FHATitleIInsuranceForManufacturedHomes")]
        FHATitleIInsuranceForManufacturedHomes,

		/// <summary>
		/// Title II Section I-FHA Property Improvement Loan Insurance. Insures loans to finance improvements, alterations, and repairs of individual homes, apartment buildings, and nonresidential structures, as well as new construction of nonresidential buildings.
		/// </summary>
        [XmlEnum("FHATitleIInsuranceForPropertyImprovementLoans")]
        FHATitleIInsuranceForPropertyImprovementLoans,

        [XmlEnum("Other")]
        Other,

    }
}
