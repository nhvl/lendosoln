namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Individual is no longer associated with this public record.
    /// </summary>
    public enum CreditPublicRecordAccountOwnershipBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// This individual is solely responsible for payment of any legal obligations on this public record.
		/// </summary>
        [XmlEnum("Individual")]
        Individual,

		/// <summary>
		/// Multiple individuals are responsible for payment of any legal obligations on this public record.
		/// </summary>
        [XmlEnum("Joint")]
        Joint,

		/// <summary>
		/// Individual is no longer associated with this public record.
		/// </summary>
        [XmlEnum("Terminated")]
        Terminated,

    }
}
