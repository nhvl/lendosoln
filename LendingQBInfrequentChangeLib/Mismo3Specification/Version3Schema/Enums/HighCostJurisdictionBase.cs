namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum HighCostJurisdictionBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("City")]
        City,

        [XmlEnum("County")]
        County,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("State")]
        State,

    }
}
