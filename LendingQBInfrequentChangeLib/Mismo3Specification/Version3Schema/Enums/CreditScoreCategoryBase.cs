namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Credit score obtained using the Vantage credit evaluation model.  A score model developed jointly by the three credit data repositories - Equifax, Experian and Trans Union.
    /// </summary>
    public enum CreditScoreCategoryBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// A risk index representing the likelihood of an individual to file for bankruptcy in the near future.
		/// </summary>
        [XmlEnum("Bankruptcy")]
        Bankruptcy,

		/// <summary>
		/// A score model jointly developed by the credit repository bureaus using the same algorithms to evaluate potential credit performance and calculate risk of delinquency.
		/// </summary>
        [XmlEnum("CreditRepository")]
        CreditRepository,

		/// <summary>
		/// A credit score model developed by Fair Isaac Corporation, representing the financial credit worthiness of an individual.
		/// </summary>
        [XmlEnum("FICO")]
        FICO,

		/// <summary>
		/// A credit score model developed by Fair Isaac Corporation as part of their "Next Gen" scoring algorithm, representing the financial credit worthiness of an individual.
		/// </summary>
        [XmlEnum("FICONextGen")]
        FICONextGen,

		/// <summary>
		/// Use this enumeration for any credit score category that is not one of the pre-defined category types.  The actual category type should be stored in the new Credit Score Category Type Other Description.
		/// </summary>
        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// A credit score model that uses non-traditional data such as utility bill payment history and residential rental history to determine the credit worthiness of an an individual. This score model is normally used for individuals who to not have sufficient credit history.
		/// </summary>
        [XmlEnum("ThinFile")]
        ThinFile,

		/// <summary>
		/// Credit score obtained using the Vantage credit evaluation model.  A score model developed jointly by the three credit data repositories - Equifax, Experian and Trans Union.
		/// </summary>
        [XmlEnum("Vantage")]
        Vantage,

    }
}
