namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Equivalent of J on Loan Estimate and Closing Disclosure
    /// </summary>
    public enum IntegratedDisclosureCashToCloseItemBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("AdjustmentsAndOtherCredits")]
        AdjustmentsAndOtherCredits,

        [XmlEnum("CashToCloseTotal")]
        CashToCloseTotal,

        [XmlEnum("ClosingCostsFinanced")]
        ClosingCostsFinanced,

        [XmlEnum("ClosingCostsPaidBeforeClosing")]
        ClosingCostsPaidBeforeClosing,

        [XmlEnum("Deposit")]
        Deposit,

        [XmlEnum("DownPayment")]
        DownPayment,

        [XmlEnum("FundsForBorrower")]
        FundsForBorrower,

        [XmlEnum("FundsFromBorrower")]
        FundsFromBorrower,

        [XmlEnum("LenderCredits")]
        LenderCredits,

        [XmlEnum("LoanAmount")]
        LoanAmount,

        [XmlEnum("LoanCostsAndOtherCostsTotal")]
        LoanCostsAndOtherCostsTotal,

        [XmlEnum("SellerCredits")]
        SellerCredits,

		/// <summary>
		/// Equivalent of J on Loan Estimate and Closing Disclosure
		/// </summary>
        [XmlEnum("TotalClosingCosts")]
        TotalClosingCosts,

        [XmlEnum("TotalPayoffsAndPayments")]
        TotalPayoffsAndPayments,

    }
}
