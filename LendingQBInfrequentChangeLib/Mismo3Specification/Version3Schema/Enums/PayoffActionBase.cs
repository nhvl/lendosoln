namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum PayoffActionBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("LenderOrderBothPayoffSubordinate")]
        LenderOrderBothPayoffSubordinate,

        [XmlEnum("LenderOrderPayoff")]
        LenderOrderPayoff,

        [XmlEnum("LenderOrderSubordinate")]
        LenderOrderSubordinate,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("TitleCompanyOrderBothPayoffSubordinate")]
        TitleCompanyOrderBothPayoffSubordinate,

        [XmlEnum("TitleCompanyOrderPayoff")]
        TitleCompanyOrderPayoff,

        [XmlEnum("TitleCompanyOrderSubordinate")]
        TitleCompanyOrderSubordinate,

    }
}
