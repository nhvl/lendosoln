namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A sale that is not under distress and aligns with the defintion of Market Value.
    /// </summary>
    public enum SaleBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// A sale in which a judge has ordered the sale of the property as a dispute resolution. Usually two people own it, one wants to sell, the other does not. The one wanting to sell always wins if the other refuses to buy them out. This could be a divorce or it could be an inherited property with multiple siblings. 
		/// </summary>
        [XmlEnum("CourtOrderedNonForeclosureSale")]
        CourtOrderedNonForeclosureSale,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Property sold as a result of disposition of assets of a deceased person. Also termed Estate Sale in some areas of the US.
		/// </summary>
        [XmlEnum("ProbateSale")]
        ProbateSale,

		/// <summary>
		/// A property being sold as a part of an relocation program paid by an employer to assist an employee in liquidating a home.
		/// </summary>
        [XmlEnum("RelocationSale")]
        RelocationSale,

		/// <summary>
		/// The sale of a property from inventory that has been foreclosed and taken possession of by the lien holder. 
		/// </summary>
        [XmlEnum("REOPostForeclosureSale")]
        REOPostForeclosureSale,

		/// <summary>
		/// A sale of property in which the lien holder accepts less than the debt or obligation against the property.
		/// </summary>
        [XmlEnum("ShortSale")]
        ShortSale,

		/// <summary>
		/// A judicial foreclosure process using a trustee.  A Trustee Sale is a public auction that is open to all bidders and the property is usually awarded to the highest bidder who meets all the criteria set by the Trustee.  A trustee is an entity or company chosen to administer the assets  of the beneficiary and facilitate the foreclosure process. Once underway, a judicial foreclosure can only be stopped by payment in full.
		/// </summary>
        [XmlEnum("TrusteeJudicialForeclosureSale")]
        TrusteeJudicialForeclosureSale,

		/// <summary>
		/// A non-judicial foreclosure process. In a trustee sale, the lender files a Notice of Default and a notice is posted on the property indicating a trustee sale will be held at least 90 days later, generally but not always at the county courthouse steps.  Unlike a foreclosure where the homeowners lone recourse is to redeem the full amount of the loan, an owner can stop a trustee sale simply by getting current on his or her mortgage payments plus any interest and fees that have been added along the way. A homeowner can do this at any time up to the posted time of the trustee sale and the entire process comes to a halt.
		/// </summary>
        [XmlEnum("TrusteeNonJudicialForeclosureSale")]
        TrusteeNonJudicialForeclosureSale,

		/// <summary>
		/// A sale that is not under distress and aligns with the defintion of Market Value.
		/// </summary>
        [XmlEnum("TypicallyMotivated")]
        TypicallyMotivated,

    }
}
