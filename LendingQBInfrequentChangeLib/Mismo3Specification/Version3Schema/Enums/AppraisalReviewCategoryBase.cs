namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum AppraisalReviewCategoryBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("ComparableRentals")]
        ComparableRentals,

        [XmlEnum("ComparablesSimilar")]
        ComparablesSimilar,

        [XmlEnum("Contract")]
        Contract,

        [XmlEnum("CostAndIncome")]
        CostAndIncome,

        [XmlEnum("DataAnalysis")]
        DataAnalysis,

        [XmlEnum("Improvements")]
        Improvements,

        [XmlEnum("MarketRent")]
        MarketRent,

        [XmlEnum("Neighborhood")]
        Neighborhood,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("RentSchedule")]
        RentSchedule,

        [XmlEnum("Site")]
        Site,

        [XmlEnum("Subject")]
        Subject,

        [XmlEnum("TransferHistory")]
        TransferHistory,

        [XmlEnum("ValueIndicators")]
        ValueIndicators,
    }
}
