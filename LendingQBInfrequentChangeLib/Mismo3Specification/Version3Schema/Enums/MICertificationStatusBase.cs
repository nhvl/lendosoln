namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum MICertificationStatusBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("LenderToObtain")]
        LenderToObtain,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("SellerOfLoanToObtain")]
        SellerOfLoanToObtain,

    }
}
