namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    ///  A stamp in the passport issued by an American Embassy or Consulate abroad, allowing the holder to request entry to the United States in a certain visa status within the time period specified on the visa. A visa may allow one, two or multiple entries before the expiration date of the visa.
    /// </summary>
    public enum IdentityDocumentBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Evidences transactions recorded and account balances for a depository account of an individual or business during a specified period.
		/// </summary>
        [XmlEnum("BankStatement")]
        BankStatement,

		/// <summary>
		/// A document which authorizes a person to operate a motor vehicle on a public roadway. Evidences a person's identity and driver's license identifier.
		/// </summary>
        [XmlEnum("DriversLicense")]
        DriversLicense,

		/// <summary>
		/// A document issued by an employer which evidences a person's identity and employee identifier.
		/// </summary>
        [XmlEnum("EmployeeIdentification")]
        EmployeeIdentification,

		/// <summary>
		/// A document issued by the US Department of Defense which evidences a person's identity and military identifier.
		/// </summary>
        [XmlEnum("MilitaryIdentification")]
        MilitaryIdentification,

		/// <summary>
		/// A document which assigns a national identifier to a party for a government purpose like taxation or benefits. In the US this is an IRS or Social Security Administration document such as a Social Security Card. Evidences a party's identity and National Identifier Value.
		/// </summary>
        [XmlEnum("NationalIdentification")]
        NationalIdentification,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// A document issued by a national government which certifies a person's identity, nationality, and passport identifier.
		/// </summary>
        [XmlEnum("Passport")]
        Passport,

		/// <summary>
		/// A document issued by a private organization other than an employer which associates a person with the organization, such as a credit union membership. Evidences a person's identity and private identifier.
		/// </summary>
        [XmlEnum("PrivateIdentification")]
        PrivateIdentification,

		/// <summary>
		/// A unique number assigned by a state to identify a resident. Often used in lieu of a driver's license number.
		/// </summary>
        [XmlEnum("StateIdentification")]
        StateIdentification,

		/// <summary>
		/// A document which assigns a taxpayer identifier to a party. Evidences a party's identity and taxpayer identifier value. 
		/// </summary>
        [XmlEnum("TaxpayerIdentification")]
        TaxpayerIdentification,

		/// <summary>
		///  A stamp in the passport issued by an American Embassy or Consulate abroad, allowing the holder to request entry to the United States in a certain visa status within the time period specified on the visa. A visa may allow one, two or multiple entries before the expiration date of the visa.
		/// </summary>
        [XmlEnum("Visa")]
        Visa,

    }
}
