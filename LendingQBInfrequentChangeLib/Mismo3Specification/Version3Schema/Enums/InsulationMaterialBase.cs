namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A type of fibrous content made of slag rock or other similar composite used in wall, roof or other cavities to insulate, draught proof and reduce noise.
    /// </summary>
    public enum InsulationMaterialBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// A plant fiber used in wall and roof cavities to insulate, draught proof and reduce noise.
		/// </summary>
        [XmlEnum("Cellulose")]
        Cellulose,

		/// <summary>
		/// A type of insulation that comes in pre-cut panels and is used to insulate floors, walls, and ceilings. This type of insulation is generally made of fiberglass.
		/// </summary>
        [XmlEnum("FiberglassBatt")]
        FiberglassBatt,

		/// <summary>
		/// An insulation type of fiberglass  (a material made up of very fine fibers of glass). Fiberglass is resistant to heat and fire.
		/// </summary>
        [XmlEnum("FiberglassBlownIn")]
        FiberglassBlownIn,

		/// <summary>
		/// An alternative to traditional building insulation such as fiberglass. A two-component mixture composed of isocyanate and polyol resin comes together at the tip of a gun, and forms an expanding foam that is sprayed onto roof tiles, concrete slabs, into wall cavities, or through holes drilled in into a cavity of a finished wall.
		/// </summary>
        [XmlEnum("Foam")]
        Foam,

		/// <summary>
		/// A type of fibrous content made of slag rock or other similar composite used in wall, roof or other cavities to insulate, draught proof and reduce noise.
		/// </summary>
        [XmlEnum("MineralWool")]
        MineralWool,

        [XmlEnum("Other")]
        Other,

    }
}
