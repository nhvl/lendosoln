namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum MIPremiumRefundableBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("NotRefundable")]
        NotRefundable,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Refundable")]
        Refundable,

        [XmlEnum("RefundableWithLimits")]
        RefundableWithLimits,

    }
}
