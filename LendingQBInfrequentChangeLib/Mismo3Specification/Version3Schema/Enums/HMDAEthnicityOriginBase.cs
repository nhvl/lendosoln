﻿namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    public enum HMDAEthnicityOriginBase
    {
        [XmlEnum("")]
        LeaveBlank,

        [XmlEnum(nameof(Cuban))]
        Cuban,

        [XmlEnum(nameof(Mexican))]
        Mexican,

        [XmlEnum(nameof(Other))]
        Other,

        [XmlEnum(nameof(PuertoRican))]
        PuertoRican
    }
}
