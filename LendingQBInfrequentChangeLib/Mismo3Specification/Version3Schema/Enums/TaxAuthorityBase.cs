namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum TaxAuthorityBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("AnnualCityBondAuthorityTax")]
        AnnualCityBondAuthorityTax,

        [XmlEnum("AnnualCountyBondAuthorityTax")]
        AnnualCountyBondAuthorityTax,

        [XmlEnum("AssessmentDistrictTax")]
        AssessmentDistrictTax,

        [XmlEnum("BondAuthorityTax")]
        BondAuthorityTax,

        [XmlEnum("BoroughTax")]
        BoroughTax,

        [XmlEnum("CentralAppraisalTaxingAuthorityTax")]
        CentralAppraisalTaxingAuthorityTax,

        [XmlEnum("CentralCollectionTaxingAuthorityTax")]
        CentralCollectionTaxingAuthorityTax,

        [XmlEnum("CityAndSchoolTax")]
        CityAndSchoolTax,

        [XmlEnum("CityTax")]
        CityTax,

        [XmlEnum("CombinationCollectionTax")]
        CombinationCollectionTax,

        [XmlEnum("CondoAssessmentTax")]
        CondoAssessmentTax,

        [XmlEnum("ConservancyTax")]
        ConservancyTax,

        [XmlEnum("CountyCollectedByOtherTaxingAuthorityTax")]
        CountyCollectedByOtherTaxingAuthorityTax,

        [XmlEnum("CountyTax")]
        CountyTax,

        [XmlEnum("DrainageTax")]
        DrainageTax,

        [XmlEnum("FireOrPoliceTax")]
        FireOrPoliceTax,

        [XmlEnum("GroundRentTax")]
        GroundRentTax,

        [XmlEnum("HomeownersAssociationTax")]
        HomeownersAssociationTax,

        [XmlEnum("HospitalsTax")]
        HospitalsTax,

        [XmlEnum("ImprovementTax")]
        ImprovementTax,

        [XmlEnum("IrrigationTax")]
        IrrigationTax,

        [XmlEnum("JuniorCollegesTax")]
        JuniorCollegesTax,

        [XmlEnum("LightingTax")]
        LightingTax,

        [XmlEnum("MobileHomeAuthorityTax")]
        MobileHomeAuthorityTax,

        [XmlEnum("MunicipalityTax")]
        MunicipalityTax,

        [XmlEnum("MunicipalServicesTax")]
        MunicipalServicesTax,

        [XmlEnum("OccupancyTax")]
        OccupancyTax,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("ParishTax")]
        ParishTax,

        [XmlEnum("PavingTax")]
        PavingTax,

        [XmlEnum("PUDTax")]
        PUDTax,

        [XmlEnum("ReclamationDistrictTax")]
        ReclamationDistrictTax,

        [XmlEnum("RoadBondTax")]
        RoadBondTax,

        [XmlEnum("RoadsOrBridgesTax")]
        RoadsOrBridgesTax,

        [XmlEnum("SanitationTax")]
        SanitationTax,

        [XmlEnum("SchoolDistrictTax")]
        SchoolDistrictTax,

        [XmlEnum("SemiannualCityBondAuthorityTax")]
        SemiannualCityBondAuthorityTax,

        [XmlEnum("SemiannualCountyBondAuthorityTax")]
        SemiannualCountyBondAuthorityTax,

        [XmlEnum("SpecialApplicationsTax")]
        SpecialApplicationsTax,

        [XmlEnum("SpecialAssessment")]
        SpecialAssessment,

        [XmlEnum("SpecialDistrictTax")]
        SpecialDistrictTax,

        [XmlEnum("StateAndCountyTax")]
        StateAndCountyTax,

        [XmlEnum("StateTaxingAuthorityTax")]
        StateTaxingAuthorityTax,

        [XmlEnum("StormTax")]
        StormTax,

        [XmlEnum("SupplementalTax")]
        SupplementalTax,

        [XmlEnum("TaxBillFee")]
        TaxBillFee,

        [XmlEnum("TownshipAndCountyTax")]
        TownshipAndCountyTax,

        [XmlEnum("TownshipTax")]
        TownshipTax,

        [XmlEnum("TownTax")]
        TownTax,

        [XmlEnum("UnsecuredCountyTaxes")]
        UnsecuredCountyTaxes,

        [XmlEnum("UtilityDistrictTax")]
        UtilityDistrictTax,

        [XmlEnum("VillageTax")]
        VillageTax,

        [XmlEnum("WaterControlTax")]
        WaterControlTax,

        [XmlEnum("WaterOrSewerDistrictTax")]
        WaterOrSewerDistrictTax,

    }
}
