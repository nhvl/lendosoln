namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A relative other than a parent.
    /// </summary>
    public enum PurchaseCreditSourceBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("BorrowerPaidOutsideClosing")]
        BorrowerPaidOutsideClosing,

        [XmlEnum("BuilderDeveloper")]
        BuilderDeveloper,

        [XmlEnum("Employer")]
        Employer,

        [XmlEnum("FederalAgency")]
        FederalAgency,

        [XmlEnum("Lender")]
        Lender,

        [XmlEnum("LocalAgency")]
        LocalAgency,

		/// <summary>
		/// A relative other than a parent.
		/// </summary>
        [XmlEnum("NonParentRelative")]
        NonParentRelative,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Parent")]
        Parent,

        [XmlEnum("PropertySeller")]
        PropertySeller,

        [XmlEnum("RealEstateAgent")]
        RealEstateAgent,

        [XmlEnum("StateAgency")]
        StateAgency,

        [XmlEnum("UnrelatedFriend")]
        UnrelatedFriend,

    }
}
