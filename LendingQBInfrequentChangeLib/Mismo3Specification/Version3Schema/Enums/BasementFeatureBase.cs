namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum BasementFeatureBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("FloorDrain")]
        FloorDrain,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("OutsideEntry")]
        OutsideEntry,

        [XmlEnum("SumpPump")]
        SumpPump,
    }
}
