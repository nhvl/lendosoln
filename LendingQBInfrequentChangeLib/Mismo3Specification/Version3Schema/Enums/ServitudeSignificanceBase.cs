namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Minimal or no impact.
    /// </summary>
    public enum ServitudeSignificanceBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Benefit runs with the land requires the encumbered to do something or refrain from doing something.
		/// </summary>
        [XmlEnum("Affirmative")]
        Affirmative,

		/// <summary>
		/// Burden runs with the land and requires the encumbered to do something or refrain from doing something. 
		/// </summary>
        [XmlEnum("Negative")]
        Negative,

		/// <summary>
		/// Minimal or no impact.
		/// </summary>
        [XmlEnum("Neutral")]
        Neutral,

    }
}
