namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates that transaction is exempt from Regulation Z ability-to-repay requirements because the occupancy type for the property is exempt from requirements.
    /// </summary>
    public enum AbilityToRepayExemptionReasonBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Indicates that transaction is exempt from Regulation Z ability-to-repay requirements because the lender making the loan is exempt by nature of the organization.
		/// </summary>
        [XmlEnum("CreditorOrganization")]
        CreditorOrganization,

		/// <summary>
		/// Indicates that transaction is exempt from Regulation Z ability-to-repay requirements because the loan program is exempt from requirements.
		/// </summary>
        [XmlEnum("LoanProgram")]
        LoanProgram,

		/// <summary>
		/// Indicates that transaction is exempt from Regulation Z ability-to-repay requirements because the number of financed units exceeds the regulation requirements.
		/// </summary>
        [XmlEnum("NumberOfFinancedUnits")]
        NumberOfFinancedUnits,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Indicates that transaction is exempt from Regulation Z ability-to-repay requirements because the occupancy type for the property is exempt from requirements.
		/// </summary>
        [XmlEnum("PropertyUsage")]
        PropertyUsage,
    }
}
