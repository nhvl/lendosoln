namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum PropertyValuationConditionalConclusionBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("AsIs")]
        AsIs,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("SubjectToCompletionPerPlans")]
        SubjectToCompletionPerPlans,

        [XmlEnum("SubjectToRepairsAndConditions")]
        SubjectToRepairsAndConditions,

        [XmlEnum("SubjectToRepairsProvingUnnecessary")]
        SubjectToRepairsProvingUnnecessary,

    }
}
