namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A primary governmental and/or administrative subdivision of a county, such as a township, precinct, or magisterial district. MCDs exist in 28 states and the District of Columbia. In 20 states, all or many MCDs are general-purpose governmental units: Connecticut, Illinois, Indiana, Kansas, Maine, Massachusetts, Michigan, Minnesota, Missouri, Nebraska, New Hampshire, New Jersey, New York, North Dakota, Ohio, Pennsylvania, Rhode Island, South Dakota, Vermont, and Wisconsin. Most of these MCDs are legally designated as towns or townships.
    /// </summary>
    public enum FIPSCountySubdivisionBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// A subdivision of a county that is a relatively permanent statistical area established cooperatively by the Census Bureau and state and local government authorities. Used for presenting decennial census statistics in those states that do not have well-defined and stable minor civil divisions that serve as local governments.
		/// </summary>
        [XmlEnum("CensusCountyDivision")]
        CensusCountyDivision,

		/// <summary>
		/// A primary governmental and/or administrative subdivision of a county, such as a township, precinct, or magisterial district. MCDs exist in 28 states and the District of Columbia. In 20 states, all or many MCDs are general-purpose governmental units: Connecticut, Illinois, Indiana, Kansas, Maine, Massachusetts, Michigan, Minnesota, Missouri, Nebraska, New Hampshire, New Jersey, New York, North Dakota, Ohio, Pennsylvania, Rhode Island, South Dakota, Vermont, and Wisconsin. Most of these MCDs are legally designated as towns or townships.
		/// </summary>
        [XmlEnum("MinorCivilDivision")]
        MinorCivilDivision,

    }
}
