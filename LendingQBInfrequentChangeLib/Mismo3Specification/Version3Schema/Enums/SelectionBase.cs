namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Loan paramters that will be processed or has been processed. Assumed value if SelectionType is not present
    /// </summary>
    public enum SelectionBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// One of possibly many loan paramter sets used to complate GFE Page 3
		/// </summary>
        [XmlEnum("Comparison")]
        Comparison,

		/// <summary>
		/// Loan products that are proposed which may meet current borrower needs.
		/// </summary>
        [XmlEnum("Proposed")]
        Proposed,

		/// <summary>
		/// Borrower or lender selected paramters for searching for a loan
		/// </summary>
        [XmlEnum("Requested")]
        Requested,

		/// <summary>
		/// Loan paramters that will be processed or has been processed. Assumed value if SelectionType is not present
		/// </summary>
        [XmlEnum("Selected")]
        Selected,

    }
}
