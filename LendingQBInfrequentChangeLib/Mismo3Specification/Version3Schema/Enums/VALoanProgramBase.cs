namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Section 3710(a)(5). VA Guaranteed Refinance Liens Secured by VA Dwelling
    /// </summary>
    public enum VALoanProgramBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Section 3711. VA Direct Home Loans for Native American Veterans Living on Trust Lands
		/// </summary>
        [XmlEnum("VADirectHomeLoanProgramForNativeAmericans")]
        VADirectHomeLoanProgramForNativeAmericans,

		/// <summary>
		/// Section 3710(a)(6). VA Guaranteed Condomium Unit
		/// </summary>
        [XmlEnum("VAGuaranteedCondominiumUnit")]
        VAGuaranteedCondominiumUnit,

		/// <summary>
		/// Section 3710(a)(3). VA Guaranteed Farm Residence Construction
		/// </summary>
        [XmlEnum("VAGuaranteedFarmResidenceConstruction")]
        VAGuaranteedFarmResidenceConstruction,

		/// <summary>
		/// Section 3710(a)(7). VA Guaranteed Farm Residence Energy Efficiency Improvements
		/// </summary>
        [XmlEnum("VAGuaranteedFarmResidenceEnergyEfficiencyImprovements")]
        VAGuaranteedFarmResidenceEnergyEfficiencyImprovements,

		/// <summary>
		/// Section 3710(a)(2). VA Guaranteed Farm Residence Purchase
		/// </summary>
        [XmlEnum("VAGuaranteedFarmResidencePurchase")]
        VAGuaranteedFarmResidencePurchase,

		/// <summary>
		/// Section 3710(a)(4). VA Guaranteed Farm Residence Repair, Alternation or Improvement
		/// </summary>
        [XmlEnum("VAGuaranteedFarmResidenceRepairAlternationOrImprovement")]
        VAGuaranteedFarmResidenceRepairAlternationOrImprovement,

		/// <summary>
		/// Section 3710(a)(1). VA Guaranteed Single Family Home Purchase Or Construction
		/// </summary>
        [XmlEnum("VAGuaranteedHomePurchaseOrConstruction")]
        VAGuaranteedHomePurchaseOrConstruction,

		/// <summary>
		/// Section 3710(10).VA Guaranteed Home Purchase with Energy Efficiency Improvements
		/// </summary>
        [XmlEnum("VAGuaranteedHomePurchaseWithEnergyEfficiencyImprovements")]
        VAGuaranteedHomePurchaseWithEnergyEfficiencyImprovements,

		/// <summary>
		/// Section 3710(11). VA Guarantee Home Refinance with Energy Efficiency Improvements
		/// </summary>
        [XmlEnum("VAGuaranteedHomeRefinanceWithEnergyEfficiencyImprovements")]
        VAGuaranteedHomeRefinanceWithEnergyEfficiencyImprovements,

		/// <summary>
		/// Section 3712-VA Guaranteed Manufactured Home Or Lot Purchase Or Refinance
		/// </summary>
        [XmlEnum("VAGuaranteedManufacturedHomeOrLotPurchaseOrRefinance")]
        VAGuaranteedManufacturedHomeOrLotPurchaseOrRefinance,

		/// <summary>
		/// Section 3710(a)(8). VA Guaranteed Refinance
		/// </summary>
        [XmlEnum("VAGuaranteedRefinance")]
        VAGuaranteedRefinance,

		/// <summary>
		/// Section 3710(a)(5). VA Guaranteed Refinance Liens Secured by VA Dwelling
		/// </summary>
        [XmlEnum("VAGuaranteedRefinanceLiensSecuredByVADwelling")]
        VAGuaranteedRefinanceLiensSecuredByVADwelling,

    }
}
