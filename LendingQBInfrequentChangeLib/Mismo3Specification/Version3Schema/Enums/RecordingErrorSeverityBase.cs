namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Request completed - fix error in future requests
    /// </summary>
    public enum RecordingErrorSeverityBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		///  Request cannot be completed without corrective action
		/// </summary>
        [XmlEnum("Fatal")]
        Fatal,

		/// <summary>
		/// Request completed - follow-up recommended on this transaction
		/// </summary>
        [XmlEnum("NonFatal")]
        NonFatal,

		/// <summary>
		/// A severity indicator not included in the enumerated list
		/// </summary>
        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Request completed - fix error in future requests
		/// </summary>
        [XmlEnum("Warning")]
        Warning,

    }
}
