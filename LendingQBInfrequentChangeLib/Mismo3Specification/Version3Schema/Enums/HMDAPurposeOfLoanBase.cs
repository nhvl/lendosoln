namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum HMDAPurposeOfLoanBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("HomeImprovement")]
        HomeImprovement,

        [XmlEnum("HomePurchase")]
        HomePurchase,

        [XmlEnum("Refinancing")]
        Refinancing,

    }
}
