namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum SecurityInstrumentRiderBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("AffordableMeritRate")]
        AffordableMeritRate,

        [XmlEnum("ARM")]
        ARM,

        [XmlEnum("Balloon")]
        Balloon,

        [XmlEnum("Beneficiary")]
        Beneficiary,

        [XmlEnum("Biweekly")]
        Biweekly,

        [XmlEnum("Condominium")]
        Condominium,

        [XmlEnum("Construction")]
        Construction,

        [XmlEnum("GEM")]
        GEM,

        [XmlEnum("GPM")]
        GPM,

        [XmlEnum("HomesteadExemption")]
        HomesteadExemption,

        [XmlEnum("IllinoisLandTrust")]
        IllinoisLandTrust,

        [XmlEnum("InterestOnly")]
        InterestOnly,

        [XmlEnum("InterVivosRevocableTrust")]
        InterVivosRevocableTrust,

        [XmlEnum("Investor")]
        Investor,

        [XmlEnum("Leasehold")]
        Leasehold,

        [XmlEnum("ManufacturedHousing")]
        ManufacturedHousing,

        [XmlEnum("NonOwnerOccupancy")]
        NonOwnerOccupancy,

        [XmlEnum("OneToFourFamily")]
        OneToFourFamily,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("OwnerOccupancy")]
        OwnerOccupancy,

        [XmlEnum("Prepayment")]
        Prepayment,

        [XmlEnum("PUD")]
        PUD,

        [XmlEnum("RateImprovement")]
        RateImprovement,

        [XmlEnum("Rehabilitation")]
        Rehabilitation,

        [XmlEnum("RenewalAndExtension")]
        RenewalAndExtension,

        [XmlEnum("SecondHome")]
        SecondHome,

        [XmlEnum("SecondLien")]
        SecondLien,

        [XmlEnum("TaxExemptFinancing")]
        TaxExemptFinancing,

        [XmlEnum("VA")]
        VA,

        [XmlEnum("VeteransLandBoard")]
        VeteransLandBoard,

        [XmlEnum("WaiverOfBorrowersRights")]
        WaiverOfBorrowersRights,

        [XmlEnum("WaiverOfDowerRights")]
        WaiverOfDowerRights,

    }
}
