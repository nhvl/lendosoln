namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Bankruptcy where a family with regular income keeps their assets and pays creditors according to an approved plan. Usually applies to family farms.
    /// </summary>
    public enum BankruptcyChapterBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Bankruptcy type where business assets are kept and creditors are paid according to an approved plan.
		/// </summary>
        [XmlEnum("ChapterEleven")]
        ChapterEleven,

		/// <summary>
		/// A debtor has filed for bankruptcy in another country and has filed for Chapter 15 in the US Bankruptcy Court for protection under US law.
		/// </summary>
        [XmlEnum("ChapterFifteen")]
        ChapterFifteen,

		/// <summary>
		/// Bankruptcy where assets are liquidated and the proceeds are distributed to the creditors.
		/// </summary>
        [XmlEnum("ChapterSeven")]
        ChapterSeven,

		/// <summary>
		/// Bankruptcy where an individual with regular income keeps assets and pays creditors according to an approved plan.
		/// </summary>
        [XmlEnum("ChapterThirteen")]
        ChapterThirteen,

		/// <summary>
		/// Bankruptcy where a family with regular income keeps their assets and pays creditors according to an approved plan. Usually applies to family farms.
		/// </summary>
        [XmlEnum("ChapterTwelve")]
        ChapterTwelve,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Unknown")]
        Unknown,
    }
}
