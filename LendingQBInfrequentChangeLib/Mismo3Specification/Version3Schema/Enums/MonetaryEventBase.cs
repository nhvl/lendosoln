namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Unscheduled funds advanced to a borrower from the net line of credit for a reverse mortgage.
    /// </summary>
    public enum MonetaryEventBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// A scheduled payment on a mortgage that is larger than other periodic payments but where the loan obligation is not fully satisfied.
		/// </summary>
        [XmlEnum("BalloonPayment")]
        BalloonPayment,

		/// <summary>
		/// A payment which reduces the outstanding amount of a reverse mortgage set aside account for first year property charges.
		/// </summary>
        [XmlEnum("CurtailmentToFirstYearChargesSetAside")]
        CurtailmentToFirstYearChargesSetAside,

		/// <summary>
		/// A payment of additional principal not scheduled as part of the line of credit of the borrower.
		/// </summary>
        [XmlEnum("CurtailmentToLineOfCredit")]
        CurtailmentToLineOfCredit,

		/// <summary>
		/// A payment which reduces the outstanding amount of a reverse mortgage set aside account for repairs.
		/// </summary>
        [XmlEnum("CurtailmentToRepairSetAside")]
        CurtailmentToRepairSetAside,

		/// <summary>
		/// A payment which reduces the outstanding amount of a reverse mortgage set aside account for tax and insurance payments.
		/// </summary>
        [XmlEnum("CurtailmentToTaxAndInsuranceSetAside")]
        CurtailmentToTaxAndInsuranceSetAside,

		/// <summary>
		/// A regular draw from the line of credit of the borrower.
		/// </summary>
        [XmlEnum("Draw")]
        Draw,

		/// <summary>
		/// Last draw during the construction phase of a construction to permanent loan.
		/// </summary>
        [XmlEnum("FinalDraw")]
        FinalDraw,

		/// <summary>
		/// Last draw during the construction phase of a construction to permanent loan converting to a permanent loan earlier than the original scheduled conversion date.
		/// </summary>
        [XmlEnum("FinalDrawWithEarlyConversion")]
        FinalDrawWithEarlyConversion,

		/// <summary>
		/// Last draw during the construction phase of a construction to permanent loan with a principal paydown to the permanent loan.
		/// </summary>
        [XmlEnum("FinalDrawWithPaydown")]
        FinalDrawWithPaydown,

		/// <summary>
		/// Last draw during the construction phase of a construction to permanent loan converting to a permanent loan earlier than the original scheduled conversion date with a principal paydown to the permanent loan.
		/// </summary>
        [XmlEnum("FinalDrawWithPaydownAndEarlyConversion")]
        FinalDrawWithPaydownAndEarlyConversion,

		/// <summary>
		/// The final advancement of funds from that portion of a reverse mortgage of a borrower set aside account for first year property charges.
		/// </summary>
        [XmlEnum("FirstYearPropertyChargesFinal")]
        FirstYearPropertyChargesFinal,

		/// <summary>
		/// Funds advanced from the portion of the reverse mortgage of a borrower set aside account for first year property charges.
		/// </summary>
        [XmlEnum("FirstYearPropertyChargesNotFinal")]
        FirstYearPropertyChargesNotFinal,

		/// <summary>
		/// Funds advanced from the line of credit of the borrowers that is associated with a reverse mortgage set aside for appraisal fees.
		/// </summary>
        [XmlEnum("LineOfCreditForAppraisal")]
        LineOfCreditForAppraisal,

		/// <summary>
		/// Funds advanced from the line of credit of the borrowers that is associated with a reverse mortgage assigned to prevent a lien against the property.
		/// </summary>
        [XmlEnum("LineOfCreditToPreventLien")]
        LineOfCreditToPreventLien,

		/// <summary>
		/// Funds from a source other than the borrower. Typically this would be associated with a repurchase or a foreclosure liquidation.
		/// </summary>
        [XmlEnum("NonBorrowerFundsRemitted")]
        NonBorrowerFundsRemitted,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Principal and or interest payment.
		/// </summary>
        [XmlEnum("Payment")]
        Payment,

		/// <summary>
		/// A payment of additional principal not scheduled as part of a normal monthly installment.
		/// </summary>
        [XmlEnum("PrincipalCurtailment")]
        PrincipalCurtailment,

		/// <summary>
		/// The final advancement of funds to the  borrower from that portion of a reverse mortgage set aside account for repairs.
		/// </summary>
        [XmlEnum("RepairSetAsideFinal")]
        RepairSetAsideFinal,

		/// <summary>
		/// Funds advanced to a borrower from the portion of a reverse mortgage set aside account for repairs.
		/// </summary>
        [XmlEnum("RepairSetAsideNotFinal")]
        RepairSetAsideNotFinal,

		/// <summary>
		/// Funds advanced from the portion of a reverse mortgage of a borrower,set aside account for tax and insurance payments.
		/// </summary>
        [XmlEnum("TaxAndInsuranceSetAside")]
        TaxAndInsuranceSetAside,

		/// <summary>
		/// A payment assocaited with a workout trail period.
		/// </summary>
        [XmlEnum("TrialPayment")]
        TrialPayment,

		/// <summary>
		/// Unscheduled funds advanced to a borrower from the net line of credit for a reverse mortgage.
		/// </summary>
        [XmlEnum("UnscheduledDraw")]
        UnscheduledDraw,

    }
}
