namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum RoomFeatureBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("BathroomFloors")]
        BathroomFloors,

        [XmlEnum("BathroomWainscot")]
        BathroomWainscot,

        [XmlEnum("Doors")]
        Doors,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("TrimAndFinish")]
        TrimAndFinish,

    }
}
