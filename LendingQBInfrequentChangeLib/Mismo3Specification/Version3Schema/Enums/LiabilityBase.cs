namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Lien recorded to secure a third mortgage when a first and second mortgage liens already exist.
    /// </summary>
    public enum LiabilityBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// The borrower monthly liability amount used by the lender to qualify the borrower for the loan.
		/// </summary>
        [XmlEnum("BorrowerEstimatedTotalMonthlyLiabilityPayment")]
        BorrowerEstimatedTotalMonthlyLiabilityPayment,

		/// <summary>
		/// Lien upon the property of a debtor resulting from a decree of the court.
		/// </summary>
        [XmlEnum("CollectionsJudgmentsAndLiens")]
        CollectionsJudgmentsAndLiens,

		/// <summary>
		/// A student loan whose repayment is deferred.
		/// </summary>
        [XmlEnum("DeferredStudentLoan")]
        DeferredStudentLoan,

		/// <summary>
		/// Amount of local, state or federal taxes that are past due and not paid
		/// </summary>
        [XmlEnum("DelinquentTaxes")]
        DelinquentTaxes,

		/// <summary>
		/// The amount of any mortgage loan secured in first lien position.
		/// </summary>
        [XmlEnum("FirstPositionMortgageLien")]
        FirstPositionMortgageLien,

		/// <summary>
		/// A notice to an employer or other asset holder that monies, wages, or property of a debtor must be applied to a specific debt or creditor.
		/// </summary>
        [XmlEnum("Garnishments")]
        Garnishments,

		/// <summary>
		/// A mortgage loan, which is usually in a subordinate position, that allows the borrower to obtain multiple advances of the loan proceeds at his or her own discretion, up to an amount that represents a specified percentage of the borrowers equity in a proper
		/// </summary>
        [XmlEnum("HELOC")]
        HELOC,

        [XmlEnum("HomeownersAssociationLien")]
        HomeownersAssociationLien,

		/// <summary>
		/// Borrowed money repaid in several successive payments, usually at regular intervals, for a specific amount and a specified term.
		/// </summary>
        [XmlEnum("Installment")]
        Installment,

		/// <summary>
		/// Regular payment amount specified by long-term contract for the use of an asset.
		/// </summary>
        [XmlEnum("LeasePayment")]
        LeasePayment,

		/// <summary>
		/// A closed ended lien secured against a property that may be in a first or subordinate lien position. Collected on the URLA in Section VI (Liabilities).
		/// </summary>
        [XmlEnum("MortgageLoan")]
        MortgageLoan,

		/// <summary>
		/// An account where the borrower can charge debt as needed. The full balance is normally repaid on a monthly basis.
		/// </summary>
        [XmlEnum("Open30DayChargeAccount")]
        Open30DayChargeAccount,

		/// <summary>
		/// Used in conjunction with a textual description of other types of liabilities.
		/// </summary>
        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// A loan granted for personal or household use. Loans may be unsecured or secured by the item purchased.
		/// </summary>
        [XmlEnum("PersonalLoan")]
        PersonalLoan,

		/// <summary>
		/// Open lines of credit, which are subject to variable payments and interest in accordance with the balance.
		/// </summary>
        [XmlEnum("Revolving")]
        Revolving,

		/// <summary>
		/// Lien recorded to secure a second mortgage when a first mortgage lien already exists.
		/// </summary>
        [XmlEnum("SecondPositionMortgageLien")]
        SecondPositionMortgageLien,

		/// <summary>
		/// Taxes levied on borrowers income by local, state, or US Government.
		/// </summary>
        [XmlEnum("Taxes")]
        Taxes,

        [XmlEnum("TaxLien")]
        TaxLien,

		/// <summary>
		/// Lien recorded to secure a third mortgage when a first and second mortgage liens already exist.
		/// </summary>
        [XmlEnum("ThirdPositionMortgageLien")]
        ThirdPositionMortgageLien,

        [XmlEnum("UnsecuredHomeImprovementLoanInstallment")]
        UnsecuredHomeImprovementLoanInstallment,

        [XmlEnum("UnsecuredHomeImprovementLoanRevolving")]
        UnsecuredHomeImprovementLoanRevolving,

    }
}
