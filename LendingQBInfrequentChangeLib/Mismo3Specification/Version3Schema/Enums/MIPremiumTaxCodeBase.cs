namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Combined amount for all taxes.
    /// </summary>
    public enum MIPremiumTaxCodeBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Combined amount for all taxes.
		/// </summary>
        [XmlEnum("AllTaxes")]
        AllTaxes,

        [XmlEnum("County")]
        County,

        [XmlEnum("Municipal")]
        Municipal,

        [XmlEnum("State")]
        State,

    }
}
