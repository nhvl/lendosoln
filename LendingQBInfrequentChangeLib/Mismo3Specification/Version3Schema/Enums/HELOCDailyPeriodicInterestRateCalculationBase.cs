namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Ignores Leap Year.
    /// </summary>
    public enum HELOCDailyPeriodicInterestRateCalculationBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Assumes each month has 30 days.
		/// </summary>
        [XmlEnum("360")]
        Item360,

		/// <summary>
		/// Ignores Leap Year.
		/// </summary>
        [XmlEnum("365")]
        Item365,

    }
}
