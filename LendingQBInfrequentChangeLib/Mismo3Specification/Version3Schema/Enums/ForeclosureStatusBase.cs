namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Currently within the state's foreclosure law redemption period.
    /// </summary>
    public enum ForeclosureStatusBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Breach letter has been sent to the borrower.
		/// </summary>
        [XmlEnum("BreachNoticeSent")]
        BreachNoticeSent,

        [XmlEnum("DeedInLieuCompleted")]
        DeedInLieuCompleted,

        [XmlEnum("DeedInLieuStarted")]
        DeedInLieuStarted,

		/// <summary>
		/// Foreclosure sale has occurred and borrower's redemption period (if applicable) has expired and eviction proceedings are underway.
		/// </summary>
        [XmlEnum("EvictionInProcess")]
        EvictionInProcess,

        [XmlEnum("FirstLegalActionToCommenceForeclosure")]
        FirstLegalActionToCommenceForeclosure,

        [XmlEnum("ForeclosureCompleted")]
        ForeclosureCompleted,

        [XmlEnum("ForeclosureDismissed")]
        ForeclosureDismissed,

        [XmlEnum("ForeclosurePublicationCommenced")]
        ForeclosurePublicationCommenced,

        [XmlEnum("ForeclosureSaleConfirmed")]
        ForeclosureSaleConfirmed,

        [XmlEnum("ForeclosureSaleContinued")]
        ForeclosureSaleContinued,

        [XmlEnum("ForeclosureSaleHeld")]
        ForeclosureSaleHeld,

		/// <summary>
		/// Foreclosure sale has been scheduled.
		/// </summary>
        [XmlEnum("ForeclosureSaleScheduled")]
        ForeclosureSaleScheduled,

		/// <summary>
		/// Foreclosure legal action has been initiated by the servicer.
		/// </summary>
        [XmlEnum("FormalNoticeFiled")]
        FormalNoticeFiled,

		/// <summary>
		/// Currently within the state's foreclosure law redemption period.
		/// </summary>
        [XmlEnum("InRedemptionPeriod")]
        InRedemptionPeriod,

    }
}
