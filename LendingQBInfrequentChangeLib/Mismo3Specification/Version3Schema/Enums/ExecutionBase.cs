namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum ExecutionBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Cash")]
        Cash,

        [XmlEnum("Guarantor")]
        Guarantor,

        [XmlEnum("Multilender")]
        Multilender,

        [XmlEnum("Other")]
        Other,

    }
}
