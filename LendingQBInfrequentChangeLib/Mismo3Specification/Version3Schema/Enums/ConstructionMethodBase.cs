namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Describes construction process, indicating that most elements are created at the homes permanent site. May include some prefabricated components.
    /// </summary>
    public enum ConstructionMethodBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// A factory built dwelling built in compliance with the Federal Manufactured Home Construction and Safety Standards in effect at the time the home was manufactured as evidenced by the HUD label. (HUD Code Home)
		/// </summary>
        [XmlEnum("Manufactured")]
        Manufactured,

		/// <summary>
		/// Dwelling unit constructed on a base frame which features wheels and axles to be used in transporting home from place to place, does not meet HUD code.
		/// </summary>
        [XmlEnum("MobileHome")]
        MobileHome,

		/// <summary>
		/// A factory built dwelling not on a permanent chassis.
		/// </summary>
        [XmlEnum("Modular")]
        Modular,

		/// <summary>
		/// A factory built dwelling on a permanent chassis which does not have a HUD label.
		/// </summary>
        [XmlEnum("OnFrameModular")]
        OnFrameModular,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Describes construction process, indicating that most elements are created at the homes permanent site. May include some prefabricated components.
		/// </summary>
        [XmlEnum("SiteBuilt")]
        SiteBuilt,

    }
}
