namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Basic services associated with developed areas that include provisions for electricity, telephone, gas, water, and garbage collection.
    /// </summary>
    public enum HousingExpenseBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Cable")]
        Cable,

        [XmlEnum("Electricity")]
        Electricity,

        [XmlEnum("EscrowShortage")]
        EscrowShortage,

		/// <summary>
		/// Amount of periodic principal and interest payment on a first mortgage on real estate plus annual pro-rata share of property taxes and hazard insurance on the property. Principal refers to the monthly payment that reduces the remaining balance of the mortgage.
		/// </summary>
        [XmlEnum("FirstMortgagePITI")]
        FirstMortgagePITI,

		/// <summary>
		/// Amount of periodic principal and interest payment on a first mortgage on real estate. Could be present or proposed housing expense. Principal refers to the monthly payment that reduces the remaining balance of the mortgage. Interest is the fee charged for the use of money.
		/// </summary>
        [XmlEnum("FirstMortgagePrincipalAndInterest")]
        FirstMortgagePrincipalAndInterest,

        [XmlEnum("FloodInsurance")]
        FloodInsurance,

        [XmlEnum("GroundRent")]
        GroundRent,

		/// <summary>
		/// Insurance coverage that provides compensation to insured in case of property damage or loss. (e.g., renters, homeowners, fire, earthquake, flood, etc.)
		/// </summary>
        [XmlEnum("HazardInsurance")]
        HazardInsurance,

        [XmlEnum("Heating")]
        Heating,

		/// <summary>
		/// Association to pay the cost of maintaining areas owned in common by all homeowners within a planned unit development.
		/// </summary>
        [XmlEnum("HomeownersAssociationDuesAndCondominiumFees")]
        HomeownersAssociationDuesAndCondominiumFees,

		/// <summary>
		/// When land under improvements is leased rather than owned, this is the periodic rent. A form of other housing expense.
		/// </summary>
        [XmlEnum("LeaseholdPayments")]
        LeaseholdPayments,

		/// <summary>
		/// Ongoing expenses incurred by borrower to maintain real estate owned.
		/// </summary>
        [XmlEnum("MaintenanceAndMiscellaneous")]
        MaintenanceAndMiscellaneous,

		/// <summary>
		/// Insurance which protects mortgage lenders against loss in the event of default by the borrower.
		/// </summary>
        [XmlEnum("MI")]
        MI,

		/// <summary>
		/// Other expenses related to housing which are not included in the listed values.
		/// </summary>
        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Amount of periodic principal and interest payment on a subordinate mortgage on real estate. Could be present or proposed housing expense. Principal refers to the monthly payment that reduces the remaining balance of the mortgage. Interest is the fee charged for the use of money.
		/// </summary>
        [XmlEnum("OtherMortgageLoanPrincipalAndInterest")]
        OtherMortgageLoanPrincipalAndInterest,

		/// <summary>
		/// Amount of periodic principal and interest payment on a subordinate mortgage on real estate plus annual pro-rata share of property taxes and hazard insurance on the property. Principal refers to the monthly payment that reduces the remaining balance of the mortgage. Interest is the fee charged for the use of money.
		/// </summary>
        [XmlEnum("OtherMortgageLoanPrincipalInterestTaxesAndInsurance")]
        OtherMortgageLoanPrincipalInterestTaxesAndInsurance,

		/// <summary>
		/// Local government taxes levied on the ownership of real property. Also know as Real Estate Property Tax.
		/// </summary>
        [XmlEnum("RealEstateTax")]
        RealEstateTax,

		/// <summary>
		/// Periodic amount a tenant pays to a landlord for occupancy of a property.
		/// </summary>
        [XmlEnum("Rent")]
        Rent,

        [XmlEnum("Telephone")]
        Telephone,

		/// <summary>
		/// Basic services associated with developed areas that include provisions for electricity, telephone, gas, water, and garbage collection.
		/// </summary>
        [XmlEnum("Utilities")]
        Utilities,

    }
}
