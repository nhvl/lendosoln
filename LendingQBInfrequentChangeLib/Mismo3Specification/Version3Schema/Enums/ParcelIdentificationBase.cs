namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum ParcelIdentificationBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("AssessorUnformattedIdentifier")]
        AssessorUnformattedIdentifier,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("ParcelIdentificationNumber")]
        ParcelIdentificationNumber,

        [XmlEnum("TaxMapIdentifier")]
        TaxMapIdentifier,

        [XmlEnum("TaxParcelIdentifier")]
        TaxParcelIdentifier,

        [XmlEnum("TorrensCertificateIdentifier")]
        TorrensCertificateIdentifier,

    }
}
