namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum InspectionRequestActionBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Cancellation")]
        Cancellation,

        [XmlEnum("Change")]
        Change,

        [XmlEnum("Hold")]
        Hold,

        [XmlEnum("Original")]
        Original,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("PriceQuote")]
        PriceQuote,

        [XmlEnum("Reissue")]
        Reissue,

        [XmlEnum("Resume")]
        Resume,

        [XmlEnum("StatusQuery")]
        StatusQuery,

        [XmlEnum("Update")]
        Update,

        [XmlEnum("Upgrade")]
        Upgrade,

    }
}
