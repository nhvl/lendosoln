namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum CharacterEncodingSetBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("ISO88591")]
        ISO88591,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("USASCII")]
        USASCII,

        [XmlEnum("UTF16")]
        UTF16,

        [XmlEnum("UTF8")]
        UTF8,

    }
}
