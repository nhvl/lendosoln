namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum FloorCoveringBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Carpet")]
        Carpet,

        [XmlEnum("CeramicTile")]
        CeramicTile,

        [XmlEnum("Hardwood")]
        Hardwood,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Vinyl")]
        Vinyl,

    }
}
