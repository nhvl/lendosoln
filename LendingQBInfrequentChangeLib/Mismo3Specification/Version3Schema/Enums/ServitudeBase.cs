namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Relates to specific restrictions related to ownership or use.
    /// </summary>
    public enum ServitudeBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Relates to conditions of ownership or use.
		/// </summary>
        [XmlEnum("Condition")]
        Condition,

		/// <summary>
		/// An enforceable promise related to act or refrain from acting, typically running with the land and affecting subsequent owners.
		/// </summary>
        [XmlEnum("Covenant")]
        Covenant,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("PrivatelyCreated")]
        PrivatelyCreated,

		/// <summary>
		/// Relates to specific restrictions related to ownership or use.
		/// </summary>
        [XmlEnum("Restriction")]
        Restriction,

    }
}
