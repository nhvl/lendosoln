namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Implies w-2.
    /// </summary>
    public enum VendorRelationshipBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Implies 1099 relationship.
		/// </summary>
        [XmlEnum("Independent")]
        Independent,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Implies w-2.
		/// </summary>
        [XmlEnum("Staff")]
        Staff,

    }
}
