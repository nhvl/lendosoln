namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum InterestCalculationBasisBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("AverageBalance")]
        AverageBalance,

        [XmlEnum("DailyLoanBalance")]
        DailyLoanBalance,

        [XmlEnum("EndOfPeriod")]
        EndOfPeriod,

        [XmlEnum("MaximumBalance")]
        MaximumBalance,

        [XmlEnum("Other")]
        Other,

    }
}
