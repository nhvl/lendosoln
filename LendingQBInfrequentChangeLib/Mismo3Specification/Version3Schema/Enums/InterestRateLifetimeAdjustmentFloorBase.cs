namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum InterestRateLifetimeAdjustmentFloorBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("AbsoluteMinimumNetInterestRate")]
        AbsoluteMinimumNetInterestRate,

        [XmlEnum("FactorAddedToOriginalNoteRate")]
        FactorAddedToOriginalNoteRate,

        [XmlEnum("InitialNoteRate")]
        InitialNoteRate,

        [XmlEnum("NotApplicable")]
        NotApplicable,

        [XmlEnum("Other")]
        Other,

    }
}
