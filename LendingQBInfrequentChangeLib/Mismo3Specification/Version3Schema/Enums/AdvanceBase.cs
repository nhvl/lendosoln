namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Funds returned to the borrower based on back dated relief payment amount.
    /// </summary>
    public enum AdvanceBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Appraisal")]
        Appraisal,

		/// <summary>
		/// All escrowed amounts that were advanced.
		/// </summary>
        [XmlEnum("Escrow")]
        Escrow,

        [XmlEnum("HomeownersAssociationDues")]
        HomeownersAssociationDues,

        [XmlEnum("Interest")]
        Interest,

        [XmlEnum("LegalFees")]
        LegalFees,

        [XmlEnum("MI")]
        MI,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Principal")]
        Principal,

        [XmlEnum("PropertyInspection")]
        PropertyInspection,

		/// <summary>
		/// Insurance on the dwelling associated with the property. Includes hazard, flood and others.
		/// </summary>
        [XmlEnum("PropertyInsurance")]
        PropertyInsurance,

        [XmlEnum("PropertyPreservation")]
        PropertyPreservation,

        [XmlEnum("PropertyTaxes")]
        PropertyTaxes,

		/// <summary>
		/// Funds returned to the borrower based on back dated relief payment amount.
		/// </summary>
        [XmlEnum("ReliefOverpaymentReimbursement")]
        ReliefOverpaymentReimbursement,

        [XmlEnum("ReverseMortgagePaymentPlanChange")]
        ReverseMortgagePaymentPlanChange,
    }
}
