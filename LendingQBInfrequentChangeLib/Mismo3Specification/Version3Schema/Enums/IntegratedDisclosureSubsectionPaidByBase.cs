namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum IntegratedDisclosureSubsectionPaidByBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Buyer")]
        Buyer,

        [XmlEnum("Seller")]
        Seller,

        [XmlEnum("ThirdParty")]
        ThirdParty,

    }
}
