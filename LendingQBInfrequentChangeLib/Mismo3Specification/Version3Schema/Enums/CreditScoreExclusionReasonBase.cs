namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum CreditScoreExclusionReasonBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("InvalidScoreRequest")]
        InvalidScoreRequest,

        [XmlEnum("NotScoredCreditDataNotAvailable")]
        NotScoredCreditDataNotAvailable,

        [XmlEnum("NotScoredFileCannotBeScored")]
        NotScoredFileCannotBeScored,

        [XmlEnum("NotScoredFileIsUnderReview")]
        NotScoredFileIsUnderReview,

        [XmlEnum("NotScoredFileTooLong")]
        NotScoredFileTooLong,

        [XmlEnum("NotScoredInsufficientCredit")]
        NotScoredInsufficientCredit,

        [XmlEnum("NotScoredNoQualifyingAccount")]
        NotScoredNoQualifyingAccount,

        [XmlEnum("NotScoredNoRecentAccountInformation")]
        NotScoredNoRecentAccountInformation,

        [XmlEnum("NotScoredRequirementsNotMet")]
        NotScoredRequirementsNotMet,

        [XmlEnum("NotScoredSubjectDeceased")]
        NotScoredSubjectDeceased,

        [XmlEnum("ScoringNotAvailable")]
        ScoringNotAvailable,

        [XmlEnum("UnauthorizedScoreRequest")]
        UnauthorizedScoreRequest,

    }
}
