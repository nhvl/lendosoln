namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum TrustClassificationBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("IllinoisLandTrust")]
        IllinoisLandTrust,

        [XmlEnum("LivingTrust")]
        LivingTrust,

        [XmlEnum("Other")]
        Other,

    }
}
