namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// The message text provides additional information related to the SSN issue date, state of issue, or borrower's age at time of issue.
    /// </summary>
    public enum CreditResponseAlertMessageCategoryBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// The file exists, but is suppressed due to borrower's request, or borrower is a minor, or credit freeze is in effect in compliance with any state law.
		/// </summary>
        [XmlEnum("CreditFileSuppressed")]
        CreditFileSuppressed,

		/// <summary>
		/// Death benefits have been filed through Social Security Administration or other entity, or death certificate is on file.
		/// </summary>
        [XmlEnum("DeathClaim")]
        DeathClaim,

		/// <summary>
		/// Message identifies potentially fraudulent, missing, or erroneous information in the credit request. (i.e. Equifax SafeScan, Experian FACT Plus, Trans Union Hawk Alert).
		/// </summary>
        [XmlEnum("DemographicsVerification")]
        DemographicsVerification,

		/// <summary>
		/// The borrower is on active military duty. There may be FACT Act compliance requirements related to this alert message.
		/// </summary>
        [XmlEnum("FACTAActiveDuty")]
        FACTAActiveDuty,

		/// <summary>
		/// There are significant differences between the address submitted in the credit request and the address in the credit file. There may be FACT Act compliance requirements related to this alert message.
		/// </summary>
        [XmlEnum("FACTAAddressDiscrepancy")]
        FACTAAddressDiscrepancy,

		/// <summary>
		/// The borrower has been a fraud victim. Extended alerts are maintained by the credit repository bureaus for seven years.
		/// </summary>
        [XmlEnum("FACTAFraudVictimExtended")]
        FACTAFraudVictimExtended,

		/// <summary>
		/// The borrower has been a fraud victim. Initial alerts are maintained by the credit repository bureaus for 90 days.
		/// </summary>
        [XmlEnum("FACTAFraudVictimInitial")]
        FACTAFraudVictimInitial,

		/// <summary>
		/// The borrower's Credit Risk Score Value was negatively affected by the presence of credit inquiry records on their credit report. There may be FACT Act compliance requirements related to this alert message.
		/// </summary>
        [XmlEnum("FACTARiskScoreValue")]
        FACTARiskScoreValue,

		/// <summary>
		/// Borrower has been a fraud victim and a police report has been filed.
		/// </summary>
        [XmlEnum("FraudVictim")]
        FraudVictim,

		/// <summary>
		/// The message category will be listed in the Credit File Alert Message Category Type Other Description data value.
		/// </summary>
        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// This alert is triggered when a borrower was recently added as an Authorized User on another users account and sometimes used as a scheme to raise credit scores.
        /// Repository Sources:
        /// Experian: Fraud Services Indicators 28, 29 and 30.
        /// Trans Union: Authorized User Alert (Product Code 07014).
		/// </summary>
        [XmlEnum("RecentlyAddedAuthorizedUserAlert")]
        RecentlyAddedAuthorizedUserAlert,

		/// <summary>
		/// The message text provides additional information related to the SSN issue date, state of issue, or borrower's age at time of issue.
		/// </summary>
        [XmlEnum("SSNVerification")]
        SSNVerification,

    }
}
