namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum KitchenEquipmentBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Compactor")]
        Compactor,

        [XmlEnum("Cooktop")]
        Cooktop,

        [XmlEnum("Dishwasher")]
        Dishwasher,

        [XmlEnum("Disposal")]
        Disposal,

        [XmlEnum("FanHood")]
        FanHood,

        [XmlEnum("Microwave")]
        Microwave,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("RangeOven")]
        RangeOven,

        [XmlEnum("Refrigerator")]
        Refrigerator,

        [XmlEnum("SolidSurfaceManmadeCountertop")]
        SolidSurfaceManmadeCountertop,

        [XmlEnum("SolidSurfaceNaturalCountertop")]
        SolidSurfaceNaturalCountertop,

        [XmlEnum("WasherDryer")]
        WasherDryer,

        [XmlEnum("WaterEfficientFaucet")]
        WaterEfficientFaucet,

    }
}
