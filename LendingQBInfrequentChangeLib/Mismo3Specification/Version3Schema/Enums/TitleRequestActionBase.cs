namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates that this is a request for an agent validation transaction on an order.
    /// </summary>
    public enum TitleRequestActionBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Indicates that this order is to be cancelled.
		/// </summary>
        [XmlEnum("Cancellation")]
        Cancellation,

		/// <summary>
		/// Indicates a change to a previously placed order.
		/// </summary>
        [XmlEnum("Change")]
        Change,

		/// <summary>
		/// Indicates that there is a request for an Agent to perform eRemittance to the underwriter.
		/// </summary>
        [XmlEnum("ERemittance")]
        ERemittance,

		/// <summary>
		/// Indicates that there is a request for a document and/or data to be delivered.
		/// </summary>
        [XmlEnum("GetDocument")]
        GetDocument,

		/// <summary>
		/// Indicates that this is the original order.
		/// </summary>
        [XmlEnum("Original")]
        Original,

		/// <summary>
		/// Indicates that this request is merely for a price for a particular title policy.
		/// </summary>
        [XmlEnum("PriceQuote")]
        PriceQuote,

		/// <summary>
		/// Indicates that this is a request for status on a previously placed title order.
		/// </summary>
        [XmlEnum("StatusQuery")]
        StatusQuery,

		/// <summary>
		/// Indicates that this request will update a previously placed title order.
		/// </summary>
        [XmlEnum("Update")]
        Update,

		/// <summary>
		/// Indicates that this is a request for an agent validation transaction on an order.
		/// </summary>
        [XmlEnum("ValidateAgent")]
        ValidateAgent,

    }
}
