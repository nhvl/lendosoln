namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// At any point in the supply chain of loans, information about the potential of actual income from the sale of the loan collateral asset or the servicing rights to the loan is placed in the container with this attribute.
    /// </summary>
    public enum LoanPriceQuoteBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// At any point in the supply chain of loans, information about the costs of buying the loan collateral asset or the servicing rights to the loan is placed in the container with this attribute.
		/// </summary>
        [XmlEnum("BuySide")]
        BuySide,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// At any point in the supply chain of loans, information about the potential of actual income from the sale of the loan collateral asset or the servicing rights to the loan is placed in the container with this attribute.
		/// </summary>
        [XmlEnum("SellSide")]
        SellSide,

    }
}
