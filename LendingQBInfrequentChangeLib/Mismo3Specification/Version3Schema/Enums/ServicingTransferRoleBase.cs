namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum ServicingTransferRoleBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("ServicingTransferee")]
        ServicingTransferee,

        [XmlEnum("ServicingTransferor")]
        ServicingTransferor,

    }
}
