namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// The error message was sent by Trans Union.
    /// </summary>
    public enum CreditErrorMessageSourceBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// The error message was sent by the credit bureau that prepares the credit report.
		/// </summary>
        [XmlEnum("CreditBureau")]
        CreditBureau,

		/// <summary>
		/// The error message was sent by Equifax.
		/// </summary>
        [XmlEnum("Equifax")]
        Equifax,

		/// <summary>
		/// The error message was sent by Experian.
		/// </summary>
        [XmlEnum("Experian")]
        Experian,

		/// <summary>
		/// The error message was sent by agency that stores and maintains the credit data files - not specified - could be Equifax, Experian, or Trans Union.
		/// </summary>
        [XmlEnum("RepositoryBureau")]
        RepositoryBureau,

		/// <summary>
		/// The error message was sent by Trans Union.
		/// </summary>
        [XmlEnum("TransUnion")]
        TransUnion,

    }
}
