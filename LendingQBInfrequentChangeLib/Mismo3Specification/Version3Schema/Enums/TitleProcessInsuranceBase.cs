namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum TitleProcessInsuranceBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Insurance")]
        Insurance,

        [XmlEnum("LimitedInsurance")]
        LimitedInsurance,

        [XmlEnum("NonInsurance")]
        NonInsurance,

    }
}
