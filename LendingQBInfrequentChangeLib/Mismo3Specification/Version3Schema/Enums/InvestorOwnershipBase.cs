namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Investor owns 100 percent interest in the loan.
    /// </summary>
    public enum InvestorOwnershipBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// The investor has a management interest in the loan but not ownership.
		/// </summary>
        [XmlEnum("Administered")]
        Administered,

		/// <summary>
		/// The investor owns something less than 100 percent interest in the loan.
		/// </summary>
        [XmlEnum("Participation")]
        Participation,

        [XmlEnum("Pending")]
        Pending,

		/// <summary>
		/// Investor owns 100 percent interest in the loan.
		/// </summary>
        [XmlEnum("Whole")]
        Whole,

    }
}
