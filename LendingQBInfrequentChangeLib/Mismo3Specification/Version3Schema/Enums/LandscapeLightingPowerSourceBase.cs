namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Identifies that a system utilizes power purchased from a utility company.
    /// </summary>
    public enum LandscapeLightingPowerSourceBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// 
		/// </summary>
        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Identifies that a system utilizes energy derived from the sun.
		/// </summary>
        [XmlEnum("Solar")]
        Solar,

		/// <summary>
		/// Identifies that a system utilizes power purchased from a utility company.
		/// </summary>
        [XmlEnum("TraditionalElectric")]
        TraditionalElectric,

    }
}
