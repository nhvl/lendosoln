namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// The account type is not available.
    /// </summary>
    public enum CreditLiabilityAccountBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// An account where the borrower can make loan withdrawals by check or other method up to a preset limit. Minimum monthly repayment amounts are normally a percentage of the total unpaid balance similar to a Revolving account.
		/// </summary>
        [XmlEnum("CreditLine")]
        CreditLine,

		/// <summary>
		/// Debt to be paid at regular times over a specified period. An auto loan is typically an Installment loan.
		/// </summary>
        [XmlEnum("Installment")]
        Installment,

		/// <summary>
		/// A real estate debt where repayment is normally a fixed number of payments of a fixed amount similar to Installment loans.
		/// </summary>
        [XmlEnum("Mortgage")]
        Mortgage,

		/// <summary>
		/// An account where the borrower can charge debt as needed. The full balance is normally repaid on a monthly basis. American Express cards and gasoline credit cards are examples of Open accounts.
		/// </summary>
        [XmlEnum("Open")]
        Open,

		/// <summary>
		/// Debt owed on an account that the borrower can repeatedly use and pay back without having to reapply every time credit is used. Credit cards are the most common type of Revolving account.
		/// </summary>
        [XmlEnum("Revolving")]
        Revolving,

		/// <summary>
		/// The account type is not available.
		/// </summary>
        [XmlEnum("Unknown")]
        Unknown,

    }
}
