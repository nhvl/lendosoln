namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum FeePaymentResponsiblePartyBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Branch")]
        Branch,

        [XmlEnum("Broker")]
        Broker,

        [XmlEnum("Buyer")]
        Buyer,

        [XmlEnum("Lender")]
        Lender,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Seller")]
        Seller,

    }
}
