namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// The comment text indicates information being reported that may warrant additional investigation or consideration.
    /// </summary>
    public enum CreditCommentBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Additional text comments provided by the credit bureau or repository bureau. See Credit Comment Source Type attribute for the exact source. Equifax Narrative codes are listed in Attachment 6 of the Equifax System-to-System V5/V6 Manuals. Experian Comment Codes are listed in Appendix B of the Experian Technical Manual. Trans Union Remarks Codes are listed in Appendix C of the TU Release 4 User Guide.
		/// </summary>
        [XmlEnum("BureauRemarks")]
        BureauRemarks,

		/// <summary>
		/// Comments from the borrower regarding their credit report data.
		/// </summary>
        [XmlEnum("ConsumerStatement")]
        ConsumerStatement,

		/// <summary>
		/// Request for Update of credit information are normally accompanied by special instruction comments.
		/// </summary>
        [XmlEnum("Instruction")]
        Instruction,

		/// <summary>
		/// See Credit Comment Type Other Description.
		/// </summary>
        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Additional text regarding public record data. See Credit Comment Source Type attribute for the exact source.
		/// </summary>
        [XmlEnum("PublicRecordText")]
        PublicRecordText,

		/// <summary>
		/// The comment text matches the status code listed in the specification for the entity listed in the Credit Comment Source Type attribute. Experian Status Codes are listed in Appendix N of the Experian Technical Manual.
		/// </summary>
        [XmlEnum("StatusCode")]
        StatusCode,

		/// <summary>
		/// The comment text indicates information being reported that may warrant additional investigation or consideration. 
		/// </summary>
        [XmlEnum("Warning")]
        Warning,

    }
}
