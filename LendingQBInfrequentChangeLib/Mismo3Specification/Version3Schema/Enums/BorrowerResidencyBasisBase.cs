namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Basis on which borrower lived at indicated address is not known.
    /// </summary>
    public enum BorrowerResidencyBasisBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Borrower lives/lived at indicated address without paying rent (e.g., parents home).
		/// </summary>
        [XmlEnum("LivingRentFree")]
        LivingRentFree,

		/// <summary>
		/// Borrower lives/lived at indicated address as an owner.
		/// </summary>
        [XmlEnum("Own")]
        Own,

		/// <summary>
		/// Borrower lives/lived in rented dwelling at indicated address.
		/// </summary>
        [XmlEnum("Rent")]
        Rent,

		/// <summary>
		/// Basis on which borrower lived at indicated address is not known.
		/// </summary>
        [XmlEnum("Unknown")]
        Unknown,
    }
}
