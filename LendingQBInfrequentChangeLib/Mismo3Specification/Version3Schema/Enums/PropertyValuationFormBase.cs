namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// ERC2010
    /// </summary>
    public enum PropertyValuationFormBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// FNM 1004D FRE 442
		/// </summary>
        [XmlEnum("AppraisalUpdateAndOrCompletionReport")]
        AppraisalUpdateAndOrCompletionReport,

		/// <summary>
		/// HUD-92051 (07/1987)
		/// </summary>
        [XmlEnum("ComplianceInspectionReport")]
        ComplianceInspectionReport,

		/// <summary>
		/// HUD-92800.5B (09/2004)
		/// </summary>
        [XmlEnum("ConditionalCommitmentDirectEndorsementStatementOfAppraisedValue")]
        ConditionalCommitmentDirectEndorsementStatementOfAppraisedValue,

		/// <summary>
		/// FNM 1004B
		/// </summary>
        [XmlEnum("DefinitionsStatementOfLimitingConditionsAndAppraisersCertification")]
        DefinitionsStatementOfLimitingConditionsAndAppraisersCertification,

		/// <summary>
		/// FNM 2075
		/// </summary>
        [XmlEnum("DesktopUnderwriterPropertyInspectionReport")]
        DesktopUnderwriterPropertyInspectionReport,

		/// <summary>
		/// ERC2001
		/// </summary>
        [XmlEnum("EmployeeRelocationCouncil2001")]
        EmployeeRelocationCouncil2001,

		/// <summary>
		/// FNM 1075 FRE 466
		/// </summary>
        [XmlEnum("ExteriorOnlyInspectionIndividualCondominiumUnitAppraisalReport")]
        ExteriorOnlyInspectionIndividualCondominiumUnitAppraisalReport,

		/// <summary>
		/// FNM 2095
		/// </summary>
        [XmlEnum("ExteriorOnlyInspectionIndividualCooperativeInterestAppraisalReport")]
        ExteriorOnlyInspectionIndividualCooperativeInterestAppraisalReport,

		/// <summary>
		/// FNM 2055 FRE2055
		/// </summary>
        [XmlEnum("ExteriorOnlyInspectionResidentialAppraisalReport")]
        ExteriorOnlyInspectionResidentialAppraisalReport,

		/// <summary>
		/// HUD-92564-CN (06/2006)
		/// </summary>
        [XmlEnum("ForYourProtectionGetAHomeInspection")]
        ForYourProtectionGetAHomeInspection,

        [XmlEnum("GeneralAddendum")]
        GeneralAddendum,

		/// <summary>
		/// HUD-92900-A (09/2010)
		/// </summary>
        [XmlEnum("HUD_VAAddendumToURLA")]
        HUD_VAAddendumToURLA,

		/// <summary>
		/// HUD-92900-B (12/2004)
		/// </summary>
        [XmlEnum("ImportantNoticeToHomebuyers")]
        ImportantNoticeToHomebuyers,

		/// <summary>
		/// FNM 1073, FRE 465
		/// </summary>
        [XmlEnum("IndividualCondominiumUnitAppraisalReport")]
        IndividualCondominiumUnitAppraisalReport,

		/// <summary>
		/// FNM 2090
		/// </summary>
        [XmlEnum("IndividualCooperativeInterestAppraisalReport")]
        IndividualCooperativeInterestAppraisalReport,

		/// <summary>
		/// FRE 2070
		/// </summary>
        [XmlEnum("LoanProspectorConditionAndMarketability")]
        LoanProspectorConditionAndMarketability,

		/// <summary>
		/// FNM 1004C FRE 70B
		/// </summary>
        [XmlEnum("ManufacturedHomeAppraisalReport")]
        ManufacturedHomeAppraisalReport,

		/// <summary>
		/// VA Form 26-8712
		/// </summary>
        [XmlEnum("ManufacturedHomeAppraisalReportVA")]
        ManufacturedHomeAppraisalReportVA,

		/// <summary>
		/// FNM 1004MC
		/// </summary>
        [XmlEnum("MarketConditionsAddendumToTheAppraisalReport")]
        MarketConditionsAddendumToTheAppraisalReport,

		/// <summary>
		/// HUD-92300 (02/1991)
		/// </summary>
        [XmlEnum("MortgageesAssuranceOfCompletion")]
        MortgageesAssuranceOfCompletion,

		/// <summary>
		/// NPMA-99-B (08/2008)
		/// </summary>
        [XmlEnum("NewConstructionSubterraneanTermiteSoilTreatmentRecord")]
        NewConstructionSubterraneanTermiteSoilTreatmentRecord,

		/// <summary>
		/// FNM 2000 FRE1032
		/// </summary>
        [XmlEnum("OneUnitResidentialAppraisalFieldReviewReport")]
        OneUnitResidentialAppraisalFieldReviewReport,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// VA Form 26-1805
		/// </summary>
        [XmlEnum("RequestForDeterminationOfReasonableValue")]
        RequestForDeterminationOfReasonableValue,

		/// <summary>
		/// FNM 1025, FRE 72
		/// </summary>
        [XmlEnum("SmallResidentialIncomePropertyAppraisalReport")]
        SmallResidentialIncomePropertyAppraisalReport,

		/// <summary>
		/// NPMA-99-A (08/2008)
		/// </summary>
        [XmlEnum("SubterraneanTermiteSoilTreatmentBuildersGuarantee")]
        SubterraneanTermiteSoilTreatmentBuildersGuarantee,

		/// <summary>
		/// FNM 2000A, FRE 1072
		/// </summary>
        [XmlEnum("TwoToFourUnitResidentialAppraisalFieldReviewReport")]
        TwoToFourUnitResidentialAppraisalFieldReviewReport,

		/// <summary>
		/// FNM 1004, FRE 70
		/// </summary>
        [XmlEnum("UniformResidentialAppraisalReport")]
        UniformResidentialAppraisalReport,

		/// <summary>
		/// ERC2010
		/// </summary>
        [XmlEnum("WorldwideEmployeeRelocationCouncilSummaryAppraisalReport")]
        WorldwideEmployeeRelocationCouncilSummaryAppraisalReport,

    }
}
