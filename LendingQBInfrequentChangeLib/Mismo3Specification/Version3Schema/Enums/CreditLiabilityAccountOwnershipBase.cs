namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Account ownership type has not been designated.
    /// </summary>
    public enum CreditLiabilityAccountOwnershipBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// This individual is an authorized user of this account; another individual has contractual responsibility.
		/// </summary>
        [XmlEnum("AuthorizedUser")]
        AuthorizedUser,

		/// <summary>
		/// This individual has guaranteed this account and assumes responsibility should the maker default.
		/// </summary>
        [XmlEnum("Comaker")]
        Comaker,

		/// <summary>
		/// The individual associated with the account has been reported as deceased or death benefit claims have been reported as filed under thename of the individual.
		/// </summary>
        [XmlEnum("Deceased")]
        Deceased,

		/// <summary>
		/// This individual has contractual responsibility for the account and is primarily responsible for its payment.
		/// </summary>
        [XmlEnum("Individual")]
        Individual,

		/// <summary>
		/// Individuals using this account have contractual responsibility to repay all debts on this account.
		/// </summary>
        [XmlEnum("JointContractualLiability")]
        JointContractualLiability,

		/// <summary>
		/// Individual is an authorized user on an account with another. Others associated with account may or may not have contractual responsibility.
		/// </summary>
        [XmlEnum("JointParticipating")]
        JointParticipating,

		/// <summary>
		/// This individual is responsible for the account, which may be guaranteed by a co-maker.
		/// </summary>
        [XmlEnum("Maker")]
        Maker,

		/// <summary>
		/// This individual has signed an application for the purpose of securing credit for another individual, other than spouse.
		/// </summary>
        [XmlEnum("OnBehalfOf")]
        OnBehalfOf,

		/// <summary>
		/// No longer associated with account.
		/// </summary>
        [XmlEnum("Terminated")]
        Terminated,

		/// <summary>
		/// Account ownership type has not been designated.
		/// </summary>
        [XmlEnum("Undesignated")]
        Undesignated,

    }
}
