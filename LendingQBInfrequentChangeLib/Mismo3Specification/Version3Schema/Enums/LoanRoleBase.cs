namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// The loan that is the object of the transaction, upon which the receiving business partner will take some action.
    /// </summary>
    public enum LoanRoleBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// A loan linked to this transaction via past relationship to the borrower and mortgage type. Does not have a direct bearing on the Related Loan or Subject Loan.
		/// </summary>
        [XmlEnum("HistoricalLoan")]
        HistoricalLoan,

		/// <summary>
		/// A loan linked to the subject loan by virtue of being collateralized by the same property.  Examples include the loan being refinanced, or a HELOC or other subordinate lien collateralized by the same property as the subject loan.
		/// </summary>
        [XmlEnum("RelatedLoan")]
        RelatedLoan,

		/// <summary>
		/// The loan that is the object of the transaction, upon which the receiving business partner will take some action.
		/// </summary>
        [XmlEnum("SubjectLoan")]
        SubjectLoan,

    }
}
