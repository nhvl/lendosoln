namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Residential dwellings that consist of pairs of houses built side by side sharing a common party wall. Sometimes called a duplex.
    /// </summary>
    public enum AttachmentBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Residential dwelling unit that has common wall or other direct physical connection with another residential dwelling unit.
		/// </summary>
        [XmlEnum("Attached")]
        Attached,

		/// <summary>
		/// Residential dwelling unit that has no common wall or other direct physical connection with another residential dwelling unit.
		/// </summary>
        [XmlEnum("Detached")]
        Detached,

		/// <summary>
		/// Residential dwellings that consist of pairs of houses built side by side sharing a common party wall. Sometimes called a duplex.
		/// </summary>
        [XmlEnum("SemiDetached")]
        SemiDetached,
    }
}
