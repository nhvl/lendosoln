namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Used to verify employment with a specific employer.
    /// </summary>
    public enum EmploymentDocumentBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Used when a third party employment verification vendor provides verification of employment.
		/// </summary>
        [XmlEnum("ThirdPartyEmploymentStatement")]
        ThirdPartyEmploymentStatement,

		/// <summary>
		/// When verbal verification is allowed. This may be a written statement by the lender that information was collected verbally. Might include the name of the person making the contact, the name of the entity contacted, the name and title of the individual at the entity who provided the information, date of the contact and information that was collected.
		/// </summary>
        [XmlEnum("VerbalStatement")]
        VerbalStatement,

		/// <summary>
		/// Used to verify employment with a specific employer.
		/// </summary>
        [XmlEnum("VerificationOfEmployment")]
        VerificationOfEmployment,

    }
}
