namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum UnitChargeRatingBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("High")]
        High,

        [XmlEnum("Low")]
        Low,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Typical")]
        Typical,

    }
}
