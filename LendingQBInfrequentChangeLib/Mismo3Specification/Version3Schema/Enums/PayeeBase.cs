namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum PayeeBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Appraiser")]
        Appraiser,

        [XmlEnum("Attorney")]
        Attorney,

        [XmlEnum("CityTaxAuthority")]
        CityTaxAuthority,

        [XmlEnum("CountyTaxAuthority")]
        CountyTaxAuthority,

        [XmlEnum("DocumentCustodian")]
        DocumentCustodian,

        [XmlEnum("HazardInsuranceAgent")]
        HazardInsuranceAgent,

        [XmlEnum("HazardInsuranceCompany")]
        HazardInsuranceCompany,

        [XmlEnum("MortgageInsuranceCompany")]
        MortgageInsuranceCompany,

        [XmlEnum("OptionalProductProvider")]
        OptionalProductProvider,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("OtherTaxAuthority")]
        OtherTaxAuthority,

        [XmlEnum("PoolCertificateHolder")]
        PoolCertificateHolder,

        [XmlEnum("PropertyInspectionService")]
        PropertyInspectionService,

        [XmlEnum("PropertyManagementCompany")]
        PropertyManagementCompany,

        [XmlEnum("RealEstateBroker")]
        RealEstateBroker,

        [XmlEnum("TaxServiceProvider")]
        TaxServiceProvider,

    }
}
