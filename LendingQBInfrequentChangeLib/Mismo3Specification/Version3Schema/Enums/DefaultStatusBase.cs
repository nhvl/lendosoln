namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum DefaultStatusBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Bankruptcy")]
        Bankruptcy,

        [XmlEnum("DefaultImminent")]
        DefaultImminent,

        [XmlEnum("Forbearance")]
        Forbearance,

        [XmlEnum("Foreclosure")]
        Foreclosure,

        [XmlEnum("InDefault")]
        InDefault,

        [XmlEnum("Other")]
        Other,

    }
}
