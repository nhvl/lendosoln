namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Category as defined by the Home Mortgage Disclosure Act.
    /// </summary>
    public enum HMDAEthnicityBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Category as defined by the Home Mortgage Disclosure Act.
		/// </summary>
        [XmlEnum("HispanicOrLatino")]
        HispanicOrLatino,

		/// <summary>
		/// Category as defined by the Home Mortgage Disclosure Act.
		/// </summary>
        [XmlEnum("InformationNotProvidedByApplicantInMailInternetOrTelephoneApplication")]
        InformationNotProvidedByApplicantInMailInternetOrTelephoneApplication,

		/// <summary>
		/// Category as defined by the Home Mortgage Disclosure Act. Used for loans where the borrower is an institution, corporation, or partnership.
		/// </summary>
        [XmlEnum("NotApplicable")]
        NotApplicable,

		/// <summary>
		/// Category as defined by the Home Mortgage Disclosure Act.
		/// </summary>
        [XmlEnum("NotHispanicOrLatino")]
        NotHispanicOrLatino,

    }
}
