namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum GFELoanOriginatorFeePaymentCreditBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("AmountsPaidByOrInBehalfOfBorrower")]
        AmountsPaidByOrInBehalfOfBorrower,

        [XmlEnum("ChosenInterestRateCreditOrCharge")]
        ChosenInterestRateCreditOrCharge,

        [XmlEnum("Other")]
        Other,

    }
}
