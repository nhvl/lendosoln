namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Payment twice a month
    /// </summary>
    public enum PaymentFrequencyBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Annual")]
        Annual,

		/// <summary>
		/// Payment one time at maturity
		/// </summary>
        [XmlEnum("AtMaturity")]
        AtMaturity,

        [XmlEnum("Biweekly")]
        Biweekly,

        [XmlEnum("Monthly")]
        Monthly,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Quarterly")]
        Quarterly,

		/// <summary>
		/// Payment every six months
		/// </summary>
        [XmlEnum("Semiannual")]
        Semiannual,

		/// <summary>
		/// Payment twice a month
		/// </summary>
        [XmlEnum("Semimonthly")]
        Semimonthly,

        [XmlEnum("Weekly")]
        Weekly,

    }
}
