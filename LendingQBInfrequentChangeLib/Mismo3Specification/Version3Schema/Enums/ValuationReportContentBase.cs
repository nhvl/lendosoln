namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum ValuationReportContentBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("AppraisalForm")]
        AppraisalForm,

        [XmlEnum("AppraisalLicense")]
        AppraisalLicense,

        [XmlEnum("Certification")]
        Certification,

        [XmlEnum("CommentAddendum")]
        CommentAddendum,

        [XmlEnum("CoverPage")]
        CoverPage,

        [XmlEnum("Exhibit")]
        Exhibit,

        [XmlEnum("ExtraListings")]
        ExtraListings,

        [XmlEnum("ExtraRentals")]
        ExtraRentals,

        [XmlEnum("ExtraSales")]
        ExtraSales,

        [XmlEnum("FloodMap")]
        FloodMap,

        [XmlEnum("Invoice")]
        Invoice,

        [XmlEnum("ListingPhotos")]
        ListingPhotos,

        [XmlEnum("LocationMap")]
        LocationMap,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("PlatMap")]
        PlatMap,

        [XmlEnum("RentalPhotos")]
        RentalPhotos,

        [XmlEnum("SalePhotos")]
        SalePhotos,

        [XmlEnum("SalesContract")]
        SalesContract,

        [XmlEnum("Sketch")]
        Sketch,

        [XmlEnum("SubjectPhotos")]
        SubjectPhotos,

    }
}
