namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum UnitOfMeasureBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Acres")]
        Acres,

        [XmlEnum("Hectares")]
        Hectares,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("SquareFeet")]
        SquareFeet,

        [XmlEnum("SquareMeters")]
        SquareMeters,

    }
}
