namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum FREPurchaseEligibilityBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("FreddieMacEligible")]
        FreddieMacEligible,

        [XmlEnum("FreddieMacIneligible")]
        FreddieMacIneligible,

    }
}
