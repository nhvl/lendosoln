namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Denotes that the signature field is for an initials mark.
    /// </summary>
    public enum SignatureFieldMarkBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Denotes that the signature field is a for a full signature
		/// </summary>
        [XmlEnum("FullSignature")]
        FullSignature,

		/// <summary>
		/// Denotes that the signature field is for an initials mark.
		/// </summary>
        [XmlEnum("Initials")]
        Initials,

    }
}
