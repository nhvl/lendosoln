namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Used when making revisions or changes to an existing MI application.
    /// </summary>
    public enum MIApplicationRequestBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("OriginalRequest")]
        OriginalRequest,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Used when making revisions or changes to an existing MI application.
		/// </summary>
        [XmlEnum("Resubmission")]
        Resubmission,

    }
}
