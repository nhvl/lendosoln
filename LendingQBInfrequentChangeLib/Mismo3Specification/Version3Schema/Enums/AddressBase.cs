namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates that this address applies to a previous point in time.*
    /// </summary>
    public enum AddressBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// The address of legal formation for the legal entity, as specified in ISO standard 17442:2012 (E).*
		/// </summary>
        [XmlEnum("CorporateHeadquarters")]
        CorporateHeadquarters,

        [XmlEnum("Current")]
        Current,

		/// <summary>
		/// The address of legal entity headquarters, as specified in ISO standard 17442:2012 (E).*
		/// </summary>
        [XmlEnum("LegalEntityFormation")]
        LegalEntityFormation,

        [XmlEnum("Mailing")]
        Mailing,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Primary")]
        Primary,

		/// <summary>
		/// Indicates that this address applies to a previous point in time.*
		/// </summary>
        [XmlEnum("Prior")]
        Prior,
    }
}
