namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A detailed breakdown of the various types of units within the project.
    /// </summary>
    public enum ProjectAspectBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("AmenityAndRecreationalFacilities")]
        AmenityAndRecreationalFacilities,

		/// <summary>
		/// subjective judgment of enhanced or adverse marketability factors.
		/// </summary>
        [XmlEnum("AppealToMarket")]
        AppealToMarket,

		/// <summary>
		/// refers to the outside of walls and roofs of the project buildings.
		/// </summary>
        [XmlEnum("ConditionOfExterior")]
        ConditionOfExterior,

		/// <summary>
		/// refers to the inside of walls, ceilings, floors of the project buildings.
		/// </summary>
        [XmlEnum("ConditionOfInterior")]
        ConditionOfInterior,

		/// <summary>
		/// e.g. dwellings per acre.
		/// </summary>
        [XmlEnum("Density")]
        Density,

		/// <summary>
		/// condition overall project including common areas (vs. Condition Of Exterior).
		/// </summary>
        [XmlEnum("GeneralAppearance")]
        GeneralAppearance,

		/// <summary>
		/// the time/distance relationships or linkages between a project and all other possible origins and destinations of people going to or coming from the project.
		/// </summary>
        [XmlEnum("Location")]
        Location,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("QualityOfConstruction")]
        QualityOfConstruction,

		/// <summary>
		/// A detailed breakdown of the various types of units within the project.
		/// </summary>
        [XmlEnum("UnitMix")]
        UnitMix,

    }
}
