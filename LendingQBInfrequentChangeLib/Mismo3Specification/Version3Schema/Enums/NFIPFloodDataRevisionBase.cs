namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Letter of Map Revision
    /// </summary>
    public enum NFIPFloodDataRevisionBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Letter of Map Amendment
		/// </summary>
        [XmlEnum("LOMA")]
        LOMA,

		/// <summary>
		/// Letter of Map Revision
		/// </summary>
        [XmlEnum("LOMR")]
        LOMR,

    }
}
