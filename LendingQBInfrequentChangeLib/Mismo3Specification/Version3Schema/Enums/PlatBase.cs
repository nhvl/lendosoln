namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum PlatBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("CertifiedSurveyMap")]
        CertifiedSurveyMap,

        [XmlEnum("Condominium")]
        Condominium,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("PlannedUnitDevelopment")]
        PlannedUnitDevelopment,

        [XmlEnum("Subdivision")]
        Subdivision,

        [XmlEnum("Timeshare")]
        Timeshare,

    }
}
