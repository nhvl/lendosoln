namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Used to specify monies needed at closing.
    /// </summary>
    public enum MIPremiumPeriodBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Renewal")]
        Renewal,

		/// <summary>
		/// Used to specify monies needed at closing.
		/// </summary>
        [XmlEnum("Upfront")]
        Upfront,

    }
}
