namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A foreclosure alternative program is under review or in progress which created a processing delay.
    /// </summary>
    public enum ForeclosureDelayCategoryBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// A borrower has filed for bankruptcy and has been given an automatic stay.
		/// </summary>
        [XmlEnum("BankruptcyFiled")]
        BankruptcyFiled,

		/// <summary>
		/// A borrower has acquired an attorney and litigation proceedings have begun.
		/// </summary>
        [XmlEnum("ContestedOrLitigatedForeclosure")]
        ContestedOrLitigatedForeclosure,

		/// <summary>
		/// A change, request or issue by an investor, servicer or guarantor created a processing delay.
		/// </summary>
        [XmlEnum("ImpactedPartyProcess")]
        ImpactedPartyProcess,

		/// <summary>
		/// A legal action has been initiated that created a processing delay.
		/// </summary>
        [XmlEnum("LegalProcess")]
        LegalProcess,

		/// <summary>
		/// Federal or State legislative action created a processing delay.
		/// </summary>
        [XmlEnum("LegislativeChange")]
        LegislativeChange,

		/// <summary>
		/// Pending mediation created a processing delay.
		/// </summary>
        [XmlEnum("Mediation")]
        Mediation,

		/// <summary>
		/// The military status of a borrower created a processing delay.
		/// </summary>
        [XmlEnum("MilitaryIndulgence")]
        MilitaryIndulgence,

		/// <summary>
		/// A missing document created a processing delay.
		/// </summary>
        [XmlEnum("MissingDocuments")]
        MissingDocuments,

		/// <summary>
		/// A monetary transaction has been registered but not yet confirmed creating a processing delay.
		/// </summary>
        [XmlEnum("MonetaryTransactionPending")]
        MonetaryTransactionPending,

		/// <summary>
		/// A government, investor or servicer moratorium created a processing delay.
		/// </summary>
        [XmlEnum("Moratorium")]
        Moratorium,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// A borrower is deceased and the estate probate process created a processing delay.
		/// </summary>
        [XmlEnum("Probate")]
        Probate,

		/// <summary>
		/// An issue with the property condition created a processing delay.
		/// </summary>
        [XmlEnum("PropertyCondition")]
        PropertyCondition,

		/// <summary>
		/// The seizure of property by the Federal Government created a processing delay.
		/// </summary>
        [XmlEnum("PropertySeizure")]
        PropertySeizure,

		/// <summary>
		/// Issues with the title or chain of title to the property created a processing delay.
		/// </summary>
        [XmlEnum("TitleIssue")]
        TitleIssue,

		/// <summary>
		/// A foreclosure alternative program is under review or in progress which created a processing delay.
		/// </summary>
        [XmlEnum("WorkoutInReview")]
        WorkoutInReview,

    }
}
