namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum RelatedLoanInvestorBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("FNM")]
        FNM,

        [XmlEnum("FRE")]
        FRE,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Seller")]
        Seller,

        [XmlEnum("Unknown")]
        Unknown,

    }
}
