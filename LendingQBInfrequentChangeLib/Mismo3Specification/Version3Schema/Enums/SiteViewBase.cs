namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum SiteViewBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("CityStreet")]
        CityStreet,

        [XmlEnum("GolfCourse")]
        GolfCourse,

        [XmlEnum("Industrial")]
        Industrial,

        [XmlEnum("LimitedSight")]
        LimitedSight,

        [XmlEnum("MountainView")]
        MountainView,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Park")]
        Park,

        [XmlEnum("Pastoral")]
        Pastoral,

        [XmlEnum("PowerLines")]
        PowerLines,

        [XmlEnum("Residential")]
        Residential,

        [XmlEnum("Skyline")]
        Skyline,

        [XmlEnum("Traffic")]
        Traffic,

        [XmlEnum("WaterView")]
        WaterView,

        [XmlEnum("Woods")]
        Woods,

    }
}
