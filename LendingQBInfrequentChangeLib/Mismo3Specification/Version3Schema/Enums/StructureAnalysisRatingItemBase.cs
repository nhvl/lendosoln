namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum StructureAnalysisRatingItemBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("AdequacyOfClosetsAndStorage")]
        AdequacyOfClosetsAndStorage,

        [XmlEnum("AdequacyOfInsulation")]
        AdequacyOfInsulation,

        [XmlEnum("AppealAndMarketability")]
        AppealAndMarketability,

        [XmlEnum("Condition")]
        Condition,

        [XmlEnum("ConstructionQuality")]
        ConstructionQuality,

        [XmlEnum("Electrical")]
        Electrical,

        [XmlEnum("ExteriorAppeal")]
        ExteriorAppeal,

        [XmlEnum("FunctionalUtility")]
        FunctionalUtility,

        [XmlEnum("InteriorAppeal")]
        InteriorAppeal,

        [XmlEnum("KitchenEquipment")]
        KitchenEquipment,

        [XmlEnum("LocationWithinProjectOrView")]
        LocationWithinProjectOrView,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("OverallLivability")]
        OverallLivability,

        [XmlEnum("Plumbing")]
        Plumbing,

        [XmlEnum("RoomSizeAndLayout")]
        RoomSizeAndLayout,

        [XmlEnum("Skirting")]
        Skirting,

        [XmlEnum("TrimAndFinish")]
        TrimAndFinish,

    }
}
