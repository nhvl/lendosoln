namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum FNMHomeImprovementProductBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("ActualActualBiweekly")]
        ActualActualBiweekly,

        [XmlEnum("ConstructionToPermanent")]
        ConstructionToPermanent,

        [XmlEnum("DailySimpleInterestCashConventional")]
        DailySimpleInterestCashConventional,

        [XmlEnum("DailySimpleInterestMBS")]
        DailySimpleInterestMBS,

        [XmlEnum("GovernmentTitleI")]
        GovernmentTitleI,

        [XmlEnum("Universal")]
        Universal,

    }
}
