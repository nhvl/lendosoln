namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum MIRenewalCalculationBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("AverageAnnualScheduledUnpaidPrincipalBalance")]
        AverageAnnualScheduledUnpaidPrincipalBalance,

        [XmlEnum("Constant")]
        Constant,

        [XmlEnum("Declining")]
        Declining,

        [XmlEnum("NoRenewals")]
        NoRenewals,

        [XmlEnum("Other")]
        Other,

    }
}
