namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Loan program designed to qualify immigrant families for home loans.
    /// </summary>
    public enum InvestorProgramNameBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// California Public Employees Retirement System
		/// </summary>
        [XmlEnum("Calpers")]
        Calpers,

		/// <summary>
		/// Other Investor Program name.
		/// </summary>
        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Loan program where qualified borrowers can skip up to 2 mortgage payments a year and 10 payments over the life of the loan.
		/// </summary>
        [XmlEnum("PaymentPower")]
        PaymentPower,

		/// <summary>
		/// Loan program from Countrywide that relates to construction lending with ARM pricing.
		/// </summary>
        [XmlEnum("PrimeRatePlus")]
        PrimeRatePlus,

		/// <summary>
		/// Loan program designed to qualify immigrant families for home loans.
		/// </summary>
        [XmlEnum("SettleAmerica")]
        SettleAmerica,

    }
}
