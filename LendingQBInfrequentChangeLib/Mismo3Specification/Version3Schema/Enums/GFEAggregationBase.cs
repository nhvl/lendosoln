namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum GFEAggregationBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("ChosenInterestRateCreditOrCharge")]
        ChosenInterestRateCreditOrCharge,

        [XmlEnum("CombinedOurOriginationAndInterestRateCreditOrCharge")]
        CombinedOurOriginationAndInterestRateCreditOrCharge,

        [XmlEnum("GovernmentRecordingCharges")]
        GovernmentRecordingCharges,

        [XmlEnum("HomeownersInsurance")]
        HomeownersInsurance,

        [XmlEnum("None")]
        None,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("OurOriginationCharge")]
        OurOriginationCharge,

        [XmlEnum("OwnersTitleInsurance")]
        OwnersTitleInsurance,

        [XmlEnum("RequiredServicesLenderSelected")]
        RequiredServicesLenderSelected,

        [XmlEnum("RequiredServicesYouCanShopFor")]
        RequiredServicesYouCanShopFor,

        [XmlEnum("TitleServices")]
        TitleServices,

        [XmlEnum("TransferTaxes")]
        TransferTaxes,

    }
}
