namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Also referred to as Credit Life Insurance
    /// </summary>
    public enum OptionalProductPlanBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("AccidentalDeathAndDismembermentInsurance")]
        AccidentalDeathAndDismembermentInsurance,

        [XmlEnum("AccidentAndHealthInsurance")]
        AccidentAndHealthInsurance,

		/// <summary>
		/// Also referred to as Mortgage Protection Insurance
		/// </summary>
        [XmlEnum("CreditInsurance")]
        CreditInsurance,

        [XmlEnum("DebtCancellationInsurance")]
        DebtCancellationInsurance,

		/// <summary>
		/// Also referred to as Credit Disability Insurance
		/// </summary>
        [XmlEnum("DisabilityInsurance")]
        DisabilityInsurance,

        [XmlEnum("FloodInsurance")]
        FloodInsurance,

        [XmlEnum("HazardInsurance")]
        HazardInsurance,

		/// <summary>
		/// Also referred to as Credit Life Insurance
		/// </summary>
        [XmlEnum("LifeInsurance")]
        LifeInsurance,

        [XmlEnum("Other")]
        Other,

    }
}
