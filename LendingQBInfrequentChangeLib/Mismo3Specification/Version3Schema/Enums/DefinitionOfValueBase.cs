namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// The anticipated selling price of a property as of a defined time-period or date.
    /// </summary>
    public enum DefinitionOfValueBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("AsIsValue")]
        AsIsValue,

        [XmlEnum("AsRepaired")]
        AsRepaired,

		/// <summary>
		/// The anticipated selling price of a property as of a defined time-period or date.
		/// </summary>
        [XmlEnum("ForecastValue")]
        ForecastValue,

        [XmlEnum("MarketValue")]
        MarketValue,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("QuickSaleValue")]
        QuickSaleValue,

        [XmlEnum("RetrospectiveValue")]
        RetrospectiveValue,

    }
}
