namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// First premium is at a higher rate than subsequent renewals.
    /// </summary>
    public enum MIPremiumRatePlanBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("BackLoaded")]
        BackLoaded,

		/// <summary>
		/// First premium is at same rate as subsequent renewals.
		/// </summary>
        [XmlEnum("Level")]
        Level,

		/// <summary>
		/// MI premium collected from borrower.
		/// </summary>
        [XmlEnum("ModifiedFrontLoaded")]
        ModifiedFrontLoaded,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// First premium is at a higher rate than subsequent renewals.
		/// </summary>
        [XmlEnum("StandardFrontLoaded")]
        StandardFrontLoaded,

    }
}
