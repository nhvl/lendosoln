namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum OfferContingencyBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Appraisal")]
        Appraisal,

        [XmlEnum("BuyerFinancingApproval")]
        BuyerFinancingApproval,

        [XmlEnum("EnvironmentalRemediation")]
        EnvironmentalRemediation,

        [XmlEnum("HomeInspection")]
        HomeInspection,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("SaleOfBuyersExistingHome")]
        SaleOfBuyersExistingHome,

        [XmlEnum("SubjectToRepairs")]
        SubjectToRepairs,

        [XmlEnum("ThirdPartyApproval")]
        ThirdPartyApproval,

    }
}
