namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// The servicer is in the process of evaluating the borrower for the workout.
    /// </summary>
    public enum WorkoutStatusBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// The borrower accepted the offered workout.
		/// </summary>
        [XmlEnum("AcceptedByBorrower")]
        AcceptedByBorrower,

		/// <summary>
		/// The servicer evaluated the borrower for a workout and determined that the borrower is qualified for the workout.
		/// </summary>
        [XmlEnum("Approved")]
        Approved,

        [XmlEnum("Closed")]
        Closed,

		/// <summary>
		/// The workout has been successfully completed.
		/// </summary>
        [XmlEnum("Completed")]
        Completed,

		/// <summary>
		/// The servicer offered the eligible workout to the borrower, but the borrower declined.
		/// </summary>
        [XmlEnum("DeclinedByBorrower")]
        DeclinedByBorrower,

		/// <summary>
		/// The servicer evaluated the borrower for the workout and determined that the borrower is not qualified for the workout.
		/// </summary>
        [XmlEnum("Denied")]
        Denied,

		/// <summary>
		/// The servicer determined that the borrower is eligible but not yet approved for the workout.
		/// </summary>
        [XmlEnum("Eligible")]
        Eligible,

        [XmlEnum("Failed")]
        Failed,

        [XmlEnum("Initiated")]
        Initiated,

		/// <summary>
		/// The borrower made one or more trial payments, but the loan has not been converted to a permanent modification.
		/// </summary>
        [XmlEnum("InTrial")]
        InTrial,

		/// <summary>
		/// The servicer has offered the eligible workout to the borrower.
		/// </summary>
        [XmlEnum("OfferedToBorrower")]
        OfferedToBorrower,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// The servicer is in the process of evaluating the borrower for the workout.
		/// </summary>
        [XmlEnum("UnderReview")]
        UnderReview,

        [XmlEnum("Unknown")]
        Unknown,

    }
}
