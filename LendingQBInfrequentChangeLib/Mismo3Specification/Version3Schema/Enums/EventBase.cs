namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Denotes that the document no longer has legal force or effect. A document must not contain more than one audit entries with this action type.
    /// </summary>
    public enum EventBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("AddedTaxCollectionStamps")]
        AddedTaxCollectionStamps,

        [XmlEnum("CorrectedData")]
        CorrectedData,

		/// <summary>
		/// Denotes that the document boiler plate has been created. The view is unpopulated and there are no data parts. A document must contain exactly one audit entry with this action type.
		/// </summary>
        [XmlEnum("CreatedBlankDocument")]
        CreatedBlankDocument,

        [XmlEnum("DocumentReceived")]
        DocumentReceived,

		/// <summary>
		/// Denotes that draft data has been added to the document or that a previous set of draft data has been modified. A document may contain zero or more audit entries with this action type.
		/// </summary>
        [XmlEnum("DraftedData")]
        DraftedData,

        [XmlEnum("EnteredData")]
        EnteredData,

		/// <summary>
		/// Denotes that the document has been exported to a different electronic format, but the document is still in effect.
		/// </summary>
        [XmlEnum("ExportedDocument")]
        ExportedDocument,

        [XmlEnum("NotarizedDocument")]
        NotarizedDocument,

		/// <summary>
		/// Denotes that the action type is other than the ones listed above. When this option is selected, a description of the action type must be provided in the ActionTypeOtherDescription attribute.
		/// </summary>
        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Denotes that the document has been converted to paper form and that the electronic version no longer has legal force or effect. A document must not contain more than one audit entries with this action type.
		/// </summary>
        [XmlEnum("PaperedOutDocument")]
        PaperedOutDocument,

        [XmlEnum("PartiallyPopulatedDocument")]
        PartiallyPopulatedDocument,

		/// <summary>
		/// Denotes that the document has been populated with data. The data appears in the original view and may also be present in one or more data parts. A document must not contain more than one audit entries with this action type.
		/// </summary>
        [XmlEnum("PopulatedDocument")]
        PopulatedDocument,

		/// <summary>
		/// Denotes that the document has been recorded.
		/// </summary>
        [XmlEnum("RecordedDocument")]
        RecordedDocument,

        [XmlEnum("RerecordedDocument")]
        RerecordedDocument,

		/// <summary>
		/// Denotes that a principal party, a witness or a notary has signed the document. When this value is specified, additional information about the action must be provided in the SIGNED_ACTION_DETAIL element. A document may contain zero or more audit entries with this action type.
		/// </summary>
        [XmlEnum("SignedDocument")]
        SignedDocument,

		/// <summary>
		/// Denotes that a system or process has validated the provided data parts against the corresponding readable data in the presentation view. A document may contain zero or more audit entries with this action type.
		/// </summary>
        [XmlEnum("ValidatedDocument")]
        ValidatedDocument,

		/// <summary>
		/// Denotes that a system or process has verified that all required signatures for all stakeholder parties, notaries and witnesses have been executed. The document is fully executed.
		/// </summary>
        [XmlEnum("VerifiedSignatures")]
        VerifiedSignatures,

		/// <summary>
		/// Denotes that the document no longer has legal force or effect. A document must not contain more than one audit entries with this action type.
		/// </summary>
        [XmlEnum("VoidedDocument")]
        VoidedDocument,

    }
}
