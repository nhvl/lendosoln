namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum ConditionSatisfactionResponsiblePartyBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Attorney")]
        Attorney,

        [XmlEnum("Broker")]
        Broker,

        [XmlEnum("Closer")]
        Closer,

        [XmlEnum("ClosingAgent")]
        ClosingAgent,

        [XmlEnum("EscrowCompany")]
        EscrowCompany,

        [XmlEnum("LoanOfficer")]
        LoanOfficer,

        [XmlEnum("Originator")]
        Originator,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Processor")]
        Processor,

        [XmlEnum("SettlementAgent")]
        SettlementAgent,

        [XmlEnum("TitleCompany")]
        TitleCompany,

    }
}
