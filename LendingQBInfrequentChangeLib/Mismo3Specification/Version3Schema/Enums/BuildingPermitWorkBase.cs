namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum BuildingPermitWorkBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Building")]
        Building,

        [XmlEnum("Demolition")]
        Demolition,

        [XmlEnum("Electrical")]
        Electrical,

        [XmlEnum("Mechanical")]
        Mechanical,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Plumbing")]
        Plumbing,
    }
}
