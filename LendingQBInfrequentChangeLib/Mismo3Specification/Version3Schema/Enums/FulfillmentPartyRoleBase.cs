namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum FulfillmentPartyRoleBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Notary")]
        Notary,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("TitleAgent")]
        TitleAgent,

    }
}
