namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum HazardInsuranceNonStandardPolicyBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Application")]
        Application,

        [XmlEnum("Binder")]
        Binder,

        [XmlEnum("Bridge")]
        Bridge,

        [XmlEnum("Condo")]
        Condo,

        [XmlEnum("CondominiumPUDMasterPolicy")]
        CondominiumPUDMasterPolicy,

        [XmlEnum("FairPlan")]
        FairPlan,

        [XmlEnum("ForcePlaced")]
        ForcePlaced,

        [XmlEnum("Other")]
        Other,

    }
}
