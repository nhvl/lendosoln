namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// The loan is being funded with money advanced by the lender at the closing table.
    /// </summary>
    public enum LoanClosingStatusBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// The loan has already been closed and is now being purchased by an investor.
		/// </summary>
        [XmlEnum("Closed")]
        Closed,

		/// <summary>
		/// The loan is being funded with money advanced by the lender at the closing table.
		/// </summary>
        [XmlEnum("TableFunded")]
        TableFunded,

    }
}
