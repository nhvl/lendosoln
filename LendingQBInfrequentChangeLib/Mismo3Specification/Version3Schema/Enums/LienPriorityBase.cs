namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A mortgage that gives the mortgagee a security right over all other mortgages of the mortgaged property.
    /// </summary>
    public enum LienPriorityBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// A mortgage that gives the mortgagee a security right over all other mortgages of the mortgaged property.
		/// </summary>
        [XmlEnum("FirstLien")]
        FirstLien,

        [XmlEnum("FourthLien")]
        FourthLien,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("SecondLien")]
        SecondLien,

        [XmlEnum("ThirdLien")]
        ThirdLien,

    }
}
