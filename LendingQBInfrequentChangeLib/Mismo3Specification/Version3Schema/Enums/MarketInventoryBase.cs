namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum MarketInventoryBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("AbsorptionRate")]
        AbsorptionRate,

        [XmlEnum("MedianListDaysOnMarket")]
        MedianListDaysOnMarket,

        [XmlEnum("MedianListPrice")]
        MedianListPrice,

        [XmlEnum("MedianSalesDaysOnMarket")]
        MedianSalesDaysOnMarket,

        [XmlEnum("MedianSalesPrice")]
        MedianSalesPrice,

        [XmlEnum("MedianSalesToListRatio")]
        MedianSalesToListRatio,

        [XmlEnum("Supply")]
        Supply,

        [XmlEnum("TotalListings")]
        TotalListings,

        [XmlEnum("TotalSales")]
        TotalSales,

    }
}
