namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Escrow account for rehabilitation repair or upgrade funds that are set aside as part of a Section 203(k) rehabilitation loan.
    /// </summary>
    public enum EscrowHoldbackBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Escrow account for construction completion that are set aside as part of a construction or construction-to-perm loan.
		/// </summary>
        [XmlEnum("ConstructionMortgage")]
        ConstructionMortgage,

		/// <summary>
		/// Escrow account for energy efficiency improvement funds that are set aside as part of an Energy Efficient Mortgage (EEM) program that are part of Section 203(b), 203(k), 234(c) and 203(h) loans.
		/// </summary>
        [XmlEnum("EnergyEfficientMortgage")]
        EnergyEfficientMortgage,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Escrow account for rehabilitation repair or upgrade funds that are set aside as part of a Section 203(k) rehabilitation loan.
		/// </summary>
        [XmlEnum("RehabilitationMortgage")]
        RehabilitationMortgage,

    }
}
