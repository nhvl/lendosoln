namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum PrepaymentPenaltyBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Assumption")]
        Assumption,

        [XmlEnum("EarlyPayoff")]
        EarlyPayoff,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("PrincipalCurtailment")]
        PrincipalCurtailment,

        [XmlEnum("Refinance")]
        Refinance,

    }
}
