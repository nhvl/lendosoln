namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum HeatingSystemBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("ForcedWarmAir")]
        ForcedWarmAir,

        [XmlEnum("HotWaterBaseboard")]
        HotWaterBaseboard,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Radiant")]
        Radiant,

    }
}
