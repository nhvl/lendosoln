namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Escrow account for rehabilitation repair or upgrade funds that are set aside as part of a Section 203(k) rehabilitation loan.
    /// </summary>
    public enum EscrowItemBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("AssessmentTax")]
        AssessmentTax,

        [XmlEnum("CityBondTax")]
        CityBondTax,

        [XmlEnum("CityPropertyTax")]
        CityPropertyTax,

        [XmlEnum("CondominiumAssociationDues")]
        CondominiumAssociationDues,

        [XmlEnum("CondominiumAssociationSpecialAssessment")]
        CondominiumAssociationSpecialAssessment,

		/// <summary>
		/// Escrow account for construction completion that are set aside as part of a construction or construction-to-perm loan.
		/// </summary>
        [XmlEnum("ConstructionCompletionFunds")]
        ConstructionCompletionFunds,

        [XmlEnum("CooperativeAssociationDues")]
        CooperativeAssociationDues,

        [XmlEnum("CooperativeAssociationSpecialAssessment")]
        CooperativeAssociationSpecialAssessment,

        [XmlEnum("CountyBondTax")]
        CountyBondTax,

        [XmlEnum("CountyPropertyTax")]
        CountyPropertyTax,

        [XmlEnum("DistrictPropertyTax")]
        DistrictPropertyTax,

        [XmlEnum("EarthquakeInsurance")]
        EarthquakeInsurance,

		/// <summary>
		/// Escrow account for energy efficiency improvement funds that are set aside as part of an Energy Efficient Mortgage (EEM) program that are part of Section 203(b), 203(k), 234(c) and 203(h) loans.
		/// </summary>
        [XmlEnum("EnergyEfficientImprovementFunds")]
        EnergyEfficientImprovementFunds,

        [XmlEnum("FloodInsurance")]
        FloodInsurance,

        [XmlEnum("HailInsurancePremium")]
        HailInsurancePremium,

        [XmlEnum("HazardInsurance")]
        HazardInsurance,

        [XmlEnum("HomeownersAssociationDues")]
        HomeownersAssociationDues,

        [XmlEnum("HomeownersAssociationSpecialAssessment")]
        HomeownersAssociationSpecialAssessment,

        [XmlEnum("HomeownersInsurance")]
        HomeownersInsurance,

        [XmlEnum("MortgageInsurance")]
        MortgageInsurance,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("ParishTax")]
        ParishTax,

        [XmlEnum("PestInsurance")]
        PestInsurance,

        [XmlEnum("PropertyTax")]
        PropertyTax,

		/// <summary>
		/// Escrow account for rehabilitation repair or upgrade funds that are set aside as part of a Section 203(k) rehabilitation loan.
		/// </summary>
        [XmlEnum("RehabilitationFunds")]
        RehabilitationFunds,

        [XmlEnum("SchoolPropertyTax")]
        SchoolPropertyTax,

        [XmlEnum("StatePropertyTax")]
        StatePropertyTax,

        [XmlEnum("TownPropertyTax")]
        TownPropertyTax,

        [XmlEnum("TownshipPropertyTax")]
        TownshipPropertyTax,

        [XmlEnum("VillagePropertyTax")]
        VillagePropertyTax,

        [XmlEnum("VolcanoInsurance")]
        VolcanoInsurance,

        [XmlEnum("WindstormInsurance")]
        WindstormInsurance,

    }
}
