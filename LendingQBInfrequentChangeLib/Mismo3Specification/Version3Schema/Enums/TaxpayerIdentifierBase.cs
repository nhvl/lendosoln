namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum TaxpayerIdentifierBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("EmployerIdentificationNumber")]
        EmployerIdentificationNumber,

        [XmlEnum("IndividualTaxpayerIdentificationNumber")]
        IndividualTaxpayerIdentificationNumber,

        [XmlEnum("PreparerTaxpayerIdentificationNumber")]
        PreparerTaxpayerIdentificationNumber,

        [XmlEnum("SocialSecurityNumber")]
        SocialSecurityNumber,

        [XmlEnum("TaxpayerIdentificationNumberForPendingUSAdoptions")]
        TaxpayerIdentificationNumberForPendingUSAdoptions,

    }
}
