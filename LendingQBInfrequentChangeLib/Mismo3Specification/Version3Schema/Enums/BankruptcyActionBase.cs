namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// The bankruptcy was referred to the servicer attorney.
    /// </summary>
    public enum BankruptcyActionBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// An amendment to the petition has been filed with the court.
		/// </summary>
        [XmlEnum("AmendedPetitionFiled")]
        AmendedPetitionFiled,

		/// <summary>
		/// A litigation, or adversary action filed in the bankruptcy court, and associated to the borrower's bankruptcy case, requesting a judicial review and opinion on the validity of a Â contested claim and/or lien against the subject collateral.
		/// </summary>
        [XmlEnum("ComplaintToDetermineExtentAndValidityOfLien")]
        ComplaintToDetermineExtentAndValidityOfLien,

		/// <summary>
		/// Bankruptcy consent order filed with court.
		/// </summary>
        [XmlEnum("ConsentOrderFiled")]
        ConsentOrderFiled,

		/// <summary>
		/// Debt discharge granted by the court.
		/// </summary>
        [XmlEnum("DebtDischargeGranted")]
        DebtDischargeGranted,

		/// <summary>
		/// The borrower intends to surrender other assets as part of the bankruptcy proceeding.
		/// </summary>
        [XmlEnum("DeclarationOfIntentToSurrenderOtherAssets")]
        DeclarationOfIntentToSurrenderOtherAssets,

		/// <summary>
		/// The borrower intends to surrender the subject property as part of the bankruptcy proceeding.
		/// </summary>
        [XmlEnum("DeclarationOfIntentToSurrenderSubjectProperty")]
        DeclarationOfIntentToSurrenderSubjectProperty,

		/// <summary>
		/// Borrower agreed to pay the mortgage debt in accordance with the bankruptcy reaffirmation agreement. 
		/// </summary>
        [XmlEnum("MortgageDebtReaffirmed")]
        MortgageDebtReaffirmed,

		/// <summary>
		/// Bankruptcy motion for discharge filed with court.
		/// </summary>
        [XmlEnum("MotionForDischargeFiled")]
        MotionForDischargeFiled,

		/// <summary>
		/// The court has held a hearing on the motion for discharge.
		/// </summary>
        [XmlEnum("MotionForDischargeHearingHeld")]
        MotionForDischargeHearingHeld,

		/// <summary>
		/// Bankruptcy motion for dismissal filed with court.
		/// </summary>
        [XmlEnum("MotionForDismissalFiled")]
        MotionForDismissalFiled,

		/// <summary>
		/// Bankruptcy motion for relief filed with court.
		/// </summary>
        [XmlEnum("MotionForReliefFiled")]
        MotionForReliefFiled,

		/// <summary>
		/// The court granted relief from the bankruptcy stay.
		/// </summary>
        [XmlEnum("MotionForReliefGranted")]
        MotionForReliefGranted,

		/// <summary>
		/// The court has held a hearing on the motion for relief.
		/// </summary>
        [XmlEnum("MotionForReliefHearingHeld")]
        MotionForReliefHearingHeld,

		/// <summary>
		/// An action contesting the validity, and stripping the secured status, of a lienholder's claim in order to divest a lien from the subject collateral.
		/// </summary>
        [XmlEnum("MotionToAvoidLien")]
        MotionToAvoidLien,

		/// <summary>
		/// An action seeking to change the bankruptcy from one chapter to another.
		/// </summary>
        [XmlEnum("MotionToConvertChapter")]
        MotionToConvertChapter,

		/// <summary>
		/// An action seeking to determine a creditor's position and therefore payment priority based upon predefined classes and positions in the bankruptcy code.
		/// </summary>
        [XmlEnum("MotionToDetermineLienPriority")]
        MotionToDetermineLienPriority,

		/// <summary>
		/// An action seeking to sell an asset of the borrower's bankruptcy estate and distribute the sale proceeds, on a pro rata basis, to creditors that are party to the bankruptcy estate.
		/// </summary>
        [XmlEnum("MotionToSell")]
        MotionToSell,

		/// <summary>
		/// An action seeking to determine the value of a contested bankruptcy estate asset, usually through an independent appraisal.
		/// </summary>
        [XmlEnum("MotionToValueCollateral")]
        MotionToValueCollateral,

		/// <summary>
		/// Notice of a change in the installment payment amount filed with the bankruptcy court.
		/// </summary>
        [XmlEnum("NoticeOfMortgagePaymentChange")]
        NoticeOfMortgagePaymentChange,

		/// <summary>
		/// An action by an interested party contesting one or more element(s) of the borrower's bankruptcy plan.
		/// </summary>
        [XmlEnum("ObjectionToConfirmation")]
        ObjectionToConfirmation,

		/// <summary>
		/// An action contesting the borrower's declared intention to discharge all or part of a debt.
		/// </summary>
        [XmlEnum("ObjectionToDischarge")]
        ObjectionToDischarge,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Bankruptcy petition filed with court.
		/// </summary>
        [XmlEnum("PetitionFiled")]
        PetitionFiled,

		/// <summary>
		/// An interested party has filed an objection to the bankruptcy plan.
		/// </summary>
        [XmlEnum("PlanObjection")]
        PlanObjection,

		/// <summary>
		/// The bankruptcy plan confirmation of attorney or stipulation agreements was filed.
		/// </summary>
        [XmlEnum("PostPetitionPlanFiled")]
        PostPetitionPlanFiled,

		/// <summary>
		/// Proof of claim was filed with the court by the creditors attorney.
		/// </summary>
        [XmlEnum("ProofOfClaimFiled")]
        ProofOfClaimFiled,

		/// <summary>
		/// Proof of claim was referred to the creditor attorney by servicer.
		/// </summary>
        [XmlEnum("ProofOfClaimReferred")]
        ProofOfClaimReferred,

		/// <summary>
		/// The bankruptcy was referred to the servicer attorney.
		/// </summary>
        [XmlEnum("ServicerReferredNoticeToAttorney")]
        ServicerReferredNoticeToAttorney,
    }
}
