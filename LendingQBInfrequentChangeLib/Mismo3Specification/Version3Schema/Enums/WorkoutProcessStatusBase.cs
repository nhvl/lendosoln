namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// The workout is not progressing according to policy guidelines but may become active again.
    /// </summary>
    public enum WorkoutProcessStatusBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// The workout is progressing according to policy guidelines.
		/// </summary>
        [XmlEnum("Active")]
        Active,

		/// <summary>
		/// The workout has stopped progressing and will not be reactivated.
		/// </summary>
        [XmlEnum("Ceased")]
        Ceased,

		/// <summary>
		/// The workout is not progressing according to policy guidelines but may become active again.
		/// </summary>
        [XmlEnum("Inactive")]
        Inactive,

    }
}
