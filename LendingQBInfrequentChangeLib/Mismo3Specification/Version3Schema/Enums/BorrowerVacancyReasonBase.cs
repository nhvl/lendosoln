namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Reason for vacancy is not known.
    /// </summary>
    public enum BorrowerVacancyReasonBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Borrower has abandoned the property.
		/// </summary>
        [XmlEnum("Abandoned")]
        Abandoned,

		/// <summary>
		/// Borrower agreed to receive cash in exchange for surrendering the keys and vacating the property.
		/// </summary>
        [XmlEnum("CashForKeys")]
        CashForKeys,

		/// <summary>
		/// Borrower has executed a deed in lieu and has vacated the property.
		/// </summary>
        [XmlEnum("DeedInLieu")]
        DeedInLieu,

		/// <summary>
		/// Borrower has been evicted.
		/// </summary>
        [XmlEnum("Eviction")]
        Eviction,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Reason for vacancy is not known.
		/// </summary>
        [XmlEnum("Unknown")]
        Unknown,
    }
}
