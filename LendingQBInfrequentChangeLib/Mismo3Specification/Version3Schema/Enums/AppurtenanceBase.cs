namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum AppurtenanceBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("BoatSlip")]
        BoatSlip,

        [XmlEnum("GarageSpace")]
        GarageSpace,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("ParkingSpace")]
        ParkingSpace,

        [XmlEnum("StorageUnit")]
        StorageUnit,
    }
}
