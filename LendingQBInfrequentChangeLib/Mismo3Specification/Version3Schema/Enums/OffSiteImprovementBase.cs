namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum OffSiteImprovementBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Alley")]
        Alley,

        [XmlEnum("CurbGutter")]
        CurbGutter,

        [XmlEnum("Gated")]
        Gated,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Sidewalk")]
        Sidewalk,

        [XmlEnum("Street")]
        Street,

        [XmlEnum("StreetAccess")]
        StreetAccess,

        [XmlEnum("StreetLighting")]
        StreetLighting,

        [XmlEnum("StreetMaintenance")]
        StreetMaintenance,

        [XmlEnum("StreetSurface")]
        StreetSurface,

    }
}
