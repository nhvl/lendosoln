namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// 1/72 of an inch.
    /// </summary>
    public enum MeasurementUnitBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Centimeters")]
        Centimeters,

        [XmlEnum("Inches")]
        Inches,

        [XmlEnum("Pixels")]
        Pixels,

		/// <summary>
		/// 1/72 of an inch.
		/// </summary>
        [XmlEnum("Points")]
        Points,

    }
}
