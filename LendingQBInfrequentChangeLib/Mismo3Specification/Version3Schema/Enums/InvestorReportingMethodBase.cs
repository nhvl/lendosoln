namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// The servicer must report on all servicing activity,
    /// </summary>
    public enum InvestorReportingMethodBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// The servicer only reports activity other than the scheduled payment.
		/// </summary>
        [XmlEnum("ExceptionReporting")]
        ExceptionReporting,

		/// <summary>
		/// The servicer must report on all servicing activity,
		/// </summary>
        [XmlEnum("FullReporting")]
        FullReporting,

    }
}
