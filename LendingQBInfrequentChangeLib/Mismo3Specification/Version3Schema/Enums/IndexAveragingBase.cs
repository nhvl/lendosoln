namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Average consecutive index rates from the loans change file from previous scheduled IR change dates after getting the first value from Index History.
    /// </summary>
    public enum IndexAveragingBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Average consecutive Index values from actual Index history.
		/// </summary>
        [XmlEnum("AverageConsecutiveIndexValues")]
        AverageConsecutiveIndexValues,

		/// <summary>
		/// Average periodic index rates from the Index History regardless of changes made to the loans interest rate.
		/// </summary>
        [XmlEnum("AveragePeriodicIndexRates")]
        AveragePeriodicIndexRates,

		/// <summary>
		/// Average consecutive index rates from the loans change file from previous scheduled IR change dates after getting the first value from Index History.
		/// </summary>
        [XmlEnum("AveragePreviousLoanIndexRates")]
        AveragePreviousLoanIndexRates,

    }
}
