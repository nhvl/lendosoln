namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Provided by Core Logic
    /// </summary>
    public enum AVMModelNameBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Provided by Fannie Mae, commonly known as APS.
		/// </summary>
        [XmlEnum("AutomatedPropertyService")]
        AutomatedPropertyService,

		/// <summary>
		/// Provided by R.J. Peters Assoc, Inc.
		/// </summary>
        [XmlEnum("AVMax")]
        AVMax,

		/// <summary>
		/// Provided by Fiserve Case Shiller
		/// </summary>
        [XmlEnum("Casa")]
        Casa,

		/// <summary>
		/// Provided by Collateral Analytics
		/// </summary>
        [XmlEnum("CAValue")]
        CAValue,

		/// <summary>
		/// Provided by MDA Lending Solutions
		/// </summary>
        [XmlEnum("CollateralMarketValue")]
        CollateralMarketValue,

		/// <summary>
		/// Provided by Freddie Mac
		/// </summary>
        [XmlEnum("CollateralValuationModel")]
        CollateralValuationModel,

		/// <summary>
		/// Provided by Core Logic
		/// </summary>
        [XmlEnum("HomePriceAnalyzer")]
        HomePriceAnalyzer,

		/// <summary>
		/// Provided by Freddie Mac, commonly known as HVE.
		/// </summary>
        [XmlEnum("HomeValueExplorer")]
        HomeValueExplorer,

		/// <summary>
		/// Provided by IntelliReal
		/// </summary>
        [XmlEnum("IntellirealAVM")]
        IntellirealAVM,

		/// <summary>
		///  Provided by Real Info, Inc.
		/// </summary>
        [XmlEnum("IVal")]
        IVal,

		/// <summary>
		/// Other AVM model.
		/// </summary>
        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Provided by Core Logic
		/// </summary>
        [XmlEnum("Pass")]
        Pass,

		/// <summary>
		/// Provided by Core Logic
		/// </summary>
        [XmlEnum("PowerBase6")]
        PowerBase6,

		/// <summary>
		/// Provided by Real Info, Inc.
		/// </summary>
        [XmlEnum("RealAssessment")]
        RealAssessment,

		/// <summary>
		/// Realtor Property Resources
		/// </summary>
        [XmlEnum("RealtorValuationModel")]
        RealtorValuationModel,

		/// <summary>
		/// Provided by CDR 
		/// </summary>
        [XmlEnum("Relar")]
        Relar,

		/// <summary>
		/// LPS Real Estate Data Solutions
		/// </summary>
        [XmlEnum("SiteXValue")]
        SiteXValue,

		/// <summary>
		/// Provided by LandSafe.
		/// </summary>
        [XmlEnum("ValueFinder")]
        ValueFinder,

		/// <summary>
		/// Provided by First American.
		/// </summary>
        [XmlEnum("ValuePoint")]
        ValuePoint,

		/// <summary>
		/// Provided by Core Logic
		/// </summary>
        [XmlEnum("ValuePoint4")]
        ValuePoint4,

		/// <summary>
		/// Provided by First American.
		/// </summary>
        [XmlEnum("ValuePointPlus")]
        ValuePointPlus,

		/// <summary>
		/// Provided by LPS Valuation Solutions
		/// </summary>
        [XmlEnum("ValueSure")]
        ValueSure,

		/// <summary>
		/// Provided by VeroVALUE.
		/// </summary>
        [XmlEnum("VeroIndexPlus")]
        VeroIndexPlus,

		/// <summary>
		/// Provided by VeroVALUE.
		/// </summary>
        [XmlEnum("VeroValue")]
        VeroValue,

		/// <summary>
		///  Provided by Veros Real Estate Solutions
		/// </summary>
        [XmlEnum("VeroValueAdvantage")]
        VeroValueAdvantage,

		/// <summary>
		/// Provided by Core Logic
		/// </summary>
        [XmlEnum("VeroValuePreferred")]
        VeroValuePreferred,
    }
}
