namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Object x is a trustee for trust y.
    /// </summary>
    public enum ArcroleVerbPhrase
    {
        /// <summary>
        /// Object x is associated with object y, e.g. liability x is associated with borrower y.
        /// </summary>
        [XmlEnum("IsAssociatedWith")]
        IsAssociatedWith,

        /// <summary>
        /// Object x is an attorney for object y, e.g. role x is attorney in fact (power of attorney) for borrower y.
        /// </summary>
        [XmlEnum("IsAttorneyInFactFor")]
        IsAttorneyInFactFor,

        /// <summary>
        /// Object x shares sufficient assets and liabilities with object y, e.g. borrower a and b are on the same application.
        /// </summary>
        [XmlEnum("SharesSufficientAssetsAndLiabilitiesWith")]
        SharesSufficientAssetsAndLiabilitiesWith,

        /// <summary>
        /// Object x is employed by object y.
        /// </summary>
        [XmlEnum("IsEmployedBy")]
        IsEmployedBy,

        /// <summary>
        /// Object x is supervised by object y.
        /// </summary>
        [XmlEnum("IsSupervisedBy")]
        IsSupervisedBy,

        /// <summary>
        /// Object x is a settlor for trust y.
        /// </summary>
        [XmlEnum("IsSettlorFor")]
        IsSettlorFor,

        /// <summary>
        /// Object x is a trustee for trust y.
        /// </summary>
        [XmlEnum("IsTrusteeFor")]
        IsTrusteeFor
    }
}
