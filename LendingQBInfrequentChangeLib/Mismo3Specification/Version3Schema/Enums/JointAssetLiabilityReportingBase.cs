namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Assets and liabilities are sufficiently separate and are being reported as such for the co-borrowers.
    /// </summary>
    public enum JointAssetLiabilityReportingBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// The income or assets of a person other than the borrower (including the spouse of the borrower ) will be used as a basis for loan qualification.
		/// </summary>
        [XmlEnum("Jointly")]
        Jointly,

		/// <summary>
		/// Assets and liabilities are sufficiently separate and are being reported as such for the co-borrowers.
		/// </summary>
        [XmlEnum("NotJointly")]
        NotJointly,

    }
}
