namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum GovernmentBondFinancingProgramBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("BuilderBond")]
        BuilderBond,

        [XmlEnum("ConsolidatedBond")]
        ConsolidatedBond,

        [XmlEnum("FinalBond")]
        FinalBond,

        [XmlEnum("Other")]
        Other,

    }
}
