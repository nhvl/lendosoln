namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// The principal and interest payment is based on an interest rate used solely for the calculation and not for accruing interest.
    /// </summary>
    public enum PerChangePrincipalAndInterestCalculationBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("AddDollarAmountToPreviousPrincipalAndInterest")]
        AddDollarAmountToPreviousPrincipalAndInterest,

        [XmlEnum("AddPercentBasedOnOriginalPrincipalAndInterest")]
        AddPercentBasedOnOriginalPrincipalAndInterest,

        [XmlEnum("AddPercentBasedOnPreviousPrincipalAndInterest")]
        AddPercentBasedOnPreviousPrincipalAndInterest,

		/// <summary>
		/// The principal and interest payment is based on an interest rate used solely for the calculation and not for accruing interest.
		/// </summary>
        [XmlEnum("BasedOnInterestRateUsingAThrowawayRate")]
        BasedOnInterestRateUsingAThrowawayRate,

        [XmlEnum("BasedOnNewInterestRate")]
        BasedOnNewInterestRate,

        [XmlEnum("FHA245Formula")]
        FHA245Formula,

        [XmlEnum("Other")]
        Other,

    }
}
