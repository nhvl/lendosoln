namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum CounselingProviderBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("AmericanHomeownerEducationInstituteApprovedCounseling")]
        AmericanHomeownerEducationInstituteApprovedCounseling,

        [XmlEnum("GovernmentAgency")]
        GovernmentAgency,

        [XmlEnum("HUDApprovedCounselingAgency")]
        HUDApprovedCounselingAgency,

        [XmlEnum("LenderTrainedCounseling")]
        LenderTrainedCounseling,

        [XmlEnum("NoBorrowerCounseling")]
        NoBorrowerCounseling,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("ThirdPartyCounseling")]
        ThirdPartyCounseling,

        [XmlEnum("Unknown")]
        Unknown,

    }
}
