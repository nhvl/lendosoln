namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// The inquiry result is unknown.
    /// </summary>
    public enum CreditInquiryResultBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// The account has been closed.
		/// </summary>
        [XmlEnum("AccountClosed")]
        AccountClosed,

		/// <summary>
		/// Application for credit has been made - no decision made.
		/// </summary>
        [XmlEnum("ApplicationPending")]
        ApplicationPending,

		/// <summary>
		/// The application for credit has been declined.
		/// </summary>
        [XmlEnum("Declined")]
        Declined,

		/// <summary>
		/// No credit inquiry was made by the listed party.
		/// </summary>
        [XmlEnum("DidNotInquire")]
        DidNotInquire,

		/// <summary>
		/// There is no account open with the listed party.
		/// </summary>
        [XmlEnum("NoOpenAccount")]
        NoOpenAccount,

		/// <summary>
		/// The listed party is not a lender.
		/// </summary>
        [XmlEnum("NotALender")]
        NotALender,

		/// <summary>
		/// An account has been opened, but an account number has not been issued.
		/// </summary>
        [XmlEnum("OpenAccountNumberNotIssued")]
        OpenAccountNumberNotIssued,

		/// <summary>
		/// An account has been opened with the listed party.
		/// </summary>
        [XmlEnum("OpenDiscovered")]
        OpenDiscovered,

		/// <summary>
		/// An account has been opened.
		/// </summary>
        [XmlEnum("OpenPrimaryAccount")]
        OpenPrimaryAccount,

		/// <summary>
		/// The listed inquiry was posted by a credit reporting agency.
		/// </summary>
        [XmlEnum("ReportingAgencyInquiry")]
        ReportingAgencyInquiry,

		/// <summary>
		/// The inquiry result is unknown.
		/// </summary>
        [XmlEnum("Unknown")]
        Unknown,

    }
}
