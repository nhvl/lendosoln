namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum InsulationPresenceBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Absent")]
        Absent,

        [XmlEnum("Exists")]
        Exists,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Unknown")]
        Unknown,

    }
}
