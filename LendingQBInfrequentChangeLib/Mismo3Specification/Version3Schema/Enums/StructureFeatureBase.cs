namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum StructureFeatureBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Age")]
        Age,

        [XmlEnum("Appeal")]
        Appeal,

        [XmlEnum("Design")]
        Design,

        [XmlEnum("GrossLivingArea")]
        GrossLivingArea,

        [XmlEnum("LotShapeAndTopography")]
        LotShapeAndTopography,

        [XmlEnum("LotSize")]
        LotSize,

        [XmlEnum("MaterialAndConstruction")]
        MaterialAndConstruction,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Overall")]
        Overall,

        [XmlEnum("Utilities")]
        Utilities,

        [XmlEnum("View")]
        View,

    }
}
