namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum AVMIndexBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("CoreLogic")]
        CoreLogic,

        [XmlEnum("FederalHousingFinanceAdministration")]
        FederalHousingFinanceAdministration,

        [XmlEnum("LenderProcessingServices")]
        LenderProcessingServices,

        [XmlEnum("NationalAssociationRealtorsPendingHomeSale")]
        NationalAssociationRealtorsPendingHomeSale,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("StandardsPoorCaseShiller")]
        StandardsPoorCaseShiller,
    }
}
