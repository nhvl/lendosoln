namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum CreditScoreImpairmentBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("ImmaterialDisputedAccount")]
        ImmaterialDisputedAccount,

        [XmlEnum("InsufficientCreditHistory")]
        InsufficientCreditHistory,

        [XmlEnum("NonPredictiveScore")]
        NonPredictiveScore,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("SignificantErrorsScore")]
        SignificantErrorsScore,

        [XmlEnum("Unscorable")]
        Unscorable,

    }
}
