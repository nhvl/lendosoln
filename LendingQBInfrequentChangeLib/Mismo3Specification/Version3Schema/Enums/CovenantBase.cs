namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum CovenantBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Architectural")]
        Architectural,

        [XmlEnum("CommunityUse")]
        CommunityUse,

        [XmlEnum("Land")]
        Land,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Utility")]
        Utility,

    }
}
