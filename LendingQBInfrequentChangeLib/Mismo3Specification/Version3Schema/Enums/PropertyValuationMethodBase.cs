namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// This appraisal valuation is commonly used for refinancing, home equity loans, and home equity lines of credit.Â  The appraiser determines the value of the property through public records, tax assessments and comparable sales history.
    /// </summary>
    public enum PropertyValuationMethodBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("AutomatedValuationModel")]
        AutomatedValuationModel,

        [XmlEnum("BrokerPriceOpinion")]
        BrokerPriceOpinion,

		/// <summary>
		/// This appraisal valuation is commonly used for refinancing, home equity loans, and home equity lines of credit.Â  The appraiser determines the value of the property through public records, tax assessments and comparable sales history.
		/// </summary>
        [XmlEnum("DesktopAppraisal")]
        DesktopAppraisal,

        [XmlEnum("DriveBy")]
        DriveBy,

        [XmlEnum("FullAppraisal")]
        FullAppraisal,

        [XmlEnum("None")]
        None,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("PriorAppraisalUsed")]
        PriorAppraisalUsed,

        [XmlEnum("TaxValuation")]
        TaxValuation,

    }
}
