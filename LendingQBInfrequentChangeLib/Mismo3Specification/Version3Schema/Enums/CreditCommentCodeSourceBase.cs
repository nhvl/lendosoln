namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Code List Found In: Trans Union 4.0 User Guide - Appendix C. Remarks Codes
    /// Data Source: TR01 Segment / Remarks Code
    /// </summary>
    public enum CreditCommentCodeSourceBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Code List Found In: Equifax STS V5/V6 Guide / Attachment 2 - Equifax Bankruptcy Intent/Disposition Codes.
        /// Data Source: Segment 13 (BP Record) / Current Intent Disposition Code.
		/// </summary>
        [XmlEnum("EquifaxBankruptcy")]
        EquifaxBankruptcy,

		/// <summary>
		/// Code List Found In: Equifax STS V5/V6 Guide / Attachment 3 - Equifax Collection Status Codes
        /// Data Source: Segment 14 (CO Record) / Collection Status Code
		/// </summary>
        [XmlEnum("EquifaxCollectionStatus")]
        EquifaxCollectionStatus,

		/// <summary>
		/// Code List Found In: Equifax STS V5/V6 Guide / Attachment 17 - Equifax Legal Item Status Codes
        /// Data Source: Segment 16 (LI Record) / Status Code
		/// </summary>
        [XmlEnum("EquifaxLegalStatus")]
        EquifaxLegalStatus,

		/// <summary>
		/// Code List Found In: Equifax STS V5/V6 Guide / Attachment 6 - Equifax Narrative Codes
        /// Data Source: Segment 13 (BP Record) / Narrative Code #1, #2
        /// Data Source: Segment 14 (CO Record) / Narrative Code #1, #2
        /// Data Source: Segment 16 (LI Record) / Narrative Code #1, #2
        /// Data Source: Segment 20 (TL Record) / Narrative Code #1, #2
        /// Data Source: Segment 23 (TC/PT Record) / Narrative Code #1, #2
		/// </summary>
        [XmlEnum("EquifaxNarrative")]
        EquifaxNarrative,

		/// <summary>
		/// Code List Found In: Experian File One Appendix G - Account Purpose Type Codes
        /// Data Source: 357 Segment / Type Code
        /// Data Source: 357 Segment - F3 Sub-segment / Enhanced Type Code
        /// Data Source: 359 Segment / Type Code
		/// </summary>
        [XmlEnum("ExperianAccountPurpose")]
        ExperianAccountPurpose,

		/// <summary>
		/// Code List Found In: Experian File One Appendix L - Account Condition and Payment Status Codes
        /// Data Source: 357 Segment / Status Code
        /// Data Source: 357 Segment - F3 Sub-segment / Account Condition Code
        /// Data Source: 357 Segment - F3 Sub-segment / Enhanced Payment Status
		/// </summary>
        [XmlEnum("ExperianAccountStatus")]
        ExperianAccountStatus,

		/// <summary>
		/// Code List Found In: Experian File One Appendix C - Legal Status Codes and Designators
        /// Data Source: 350 Segment / Status Code
		/// </summary>
        [XmlEnum("ExperianLegalStatus")]
        ExperianLegalStatus,

		/// <summary>
		/// Code List Found In: Experian File One Appendix B - Special Comment Codes
        /// Data Source: 357 Segment / Special Comment Code
        /// Data Source: 357 Segment - F3 Sub-segment / Enhanced Special Comment Code
		/// </summary>
        [XmlEnum("ExperianSpecialComment")]
        ExperianSpecialComment,

		/// <summary>
		/// Code List is from another source that is identified in Credit Comment Code Source Type Other Description.
		/// </summary>
        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Code List Found In: Trans Union 4.0 User Guide - Appendix C. Loan Type Codes
        /// Data Source: IN01 Segment / Loan Type Code
        /// Data Source: TR01 Segment / Loan Type Code
		/// </summary>
        [XmlEnum("TransUnionLoanType")]
        TransUnionLoanType,

		/// <summary>
		/// Code List Found In: Trans Union 4.0 User Guide - Appendix D. Public Record Type Codes
        /// Data Source: PR01 Segment / Public Record Type
		/// </summary>
        [XmlEnum("TransUnionPublicRecordType")]
        TransUnionPublicRecordType,

		/// <summary>
		/// Code List Found In: Trans Union 4.0 User Guide - Appendix C. Remarks Codes
        /// Data Source: TR01 Segment / Remarks Code
		/// </summary>
        [XmlEnum("TransUnionRemarks")]
        TransUnionRemarks,

    }
}
