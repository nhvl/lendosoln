namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// The party selling the loan in the subject transaction.
    /// </summary>
    public enum LoanUnderwritingSubmitterBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// A party who, for a commission, matches borrowers and lenders. A mortgage broker takes applications and sometimes processes loans, but generally does not use its own funds for closing.
		/// </summary>
        [XmlEnum("Broker")]
        Broker,

		/// <summary>
		/// A specialized type of mortgage banker whose function is limited to the origination of mortgage loans which are sold to other mortgage bankers or investment bankers under a specific commitment.
		/// </summary>
        [XmlEnum("Correspondent")]
        Correspondent,

		/// <summary>
		/// The party that invests in or originates mortgage loans. The lender is generally whosever name the loan is closed in.
		/// </summary>
        [XmlEnum("LenderOtherThanSeller")]
        LenderOtherThanSeller,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// The party selling the loan in the subject transaction.
		/// </summary>
        [XmlEnum("Seller")]
        Seller,

    }
}
