namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum FeePercentBasisBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("BaseLoanAmount")]
        BaseLoanAmount,

        [XmlEnum("OriginalLoanAmount")]
        OriginalLoanAmount,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("PropertyAppraisedValueAmount")]
        PropertyAppraisedValueAmount,

        [XmlEnum("PurchasePriceAmount")]
        PurchasePriceAmount,

    }
}
