namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A modification action initiated when a borrower makes a large curtailment.
    /// </summary>
    public enum LoanModificationActionBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// A modification action that is initiated by the borrower.
		/// </summary>
        [XmlEnum("BorrowerDelinquencyWorkout")]
        BorrowerDelinquencyWorkout,

		/// <summary>
		/// A modification action that incorporates past due amounts and/or fees into the unpaid principal balance.
		/// </summary>
        [XmlEnum("Capitalization")]
        Capitalization,

        [XmlEnum("ChangeOfPaymentFrequency")]
        ChangeOfPaymentFrequency,

        [XmlEnum("ConstructionToPermanentFinancing")]
        ConstructionToPermanentFinancing,

		/// <summary>
		/// A modification action initiated under the Home Affordable Modification Program
		/// </summary>
        [XmlEnum("HAMP")]
        HAMP,

		/// <summary>
		/// A modification action initiated by the lender holding the loan in order to optimize the portfolio's performance.
		/// </summary>
        [XmlEnum("LenderPortfolioManagement")]
        LenderPortfolioManagement,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// A modification action that writes off a portion of the unpaid principal balance.
		/// </summary>
        [XmlEnum("PrincipalForgiveness")]
        PrincipalForgiveness,

		/// <summary>
		/// A modification action that completely changes the nature and features of the original loan.
		/// </summary>
        [XmlEnum("Restructure")]
        Restructure,

		/// <summary>
		/// A modification action initiated when a borrower makes a large curtailment.
		/// </summary>
        [XmlEnum("UnscheduledPaymentRecast")]
        UnscheduledPaymentRecast,

    }
}
