namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum UnplattedLandBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("GovernmentSurvey")]
        GovernmentSurvey,

        [XmlEnum("LandGrant")]
        LandGrant,

        [XmlEnum("MetesAndBounds")]
        MetesAndBounds,

        [XmlEnum("NativeAmericanLand")]
        NativeAmericanLand,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Ranchero")]
        Ranchero,

    }
}
