namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// The request is for a status update on a previously submitted request
    /// </summary>
    public enum PRIARequestBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// The request is for the county to provide a fee quote based on information contained in the request
		/// </summary>
        [XmlEnum("FeeQuote")]
        FeeQuote,

		/// <summary>
		/// The request is for an action not listed in the enumerated list
		/// </summary>
        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// The request is that documents be recorded
		/// </summary>
        [XmlEnum("RecordDocuments")]
        RecordDocuments,

		/// <summary>
		/// The request is for a status update on a previously submitted request
		/// </summary>
        [XmlEnum("StatusUpdate")]
        StatusUpdate,

    }
}
