namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum ProjectManagementBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("CooperativeBoard")]
        CooperativeBoard,

        [XmlEnum("Developer")]
        Developer,

        [XmlEnum("HomeownersAssociation")]
        HomeownersAssociation,

        [XmlEnum("ManagementAgent")]
        ManagementAgent,

        [XmlEnum("Other")]
        Other,

    }
}
