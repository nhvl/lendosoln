namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum ArchitecturalDesignConfigurationBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Duplex")]
        Duplex,

        [XmlEnum("Fourplex")]
        Fourplex,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("RowHouse")]
        RowHouse,

        [XmlEnum("Single")]
        Single,

        [XmlEnum("Triplex")]
        Triplex,
    }
}
