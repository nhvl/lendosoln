namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum MortgageScoreBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("FraudFilterScore")]
        FraudFilterScore,

        [XmlEnum("GE_IQScore")]
        GE_IQScore,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("PMIAuraAQIScore")]
        PMIAuraAQIScore,

        [XmlEnum("UGIAccuscore")]
        UGIAccuscore,

    }
}
