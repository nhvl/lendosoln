namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum RelationshipVestingBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("CommunityProperty")]
        CommunityProperty,

        [XmlEnum("Individual")]
        Individual,

        [XmlEnum("JointTenants")]
        JointTenants,

        [XmlEnum("JointTenantsWithRightOfSurvivorship")]
        JointTenantsWithRightOfSurvivorship,

        [XmlEnum("LeasedFee")]
        LeasedFee,

        [XmlEnum("LifeEstate")]
        LifeEstate,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("TenantsByTheEntirety")]
        TenantsByTheEntirety,

        [XmlEnum("TenantsInCommon")]
        TenantsInCommon,

        [XmlEnum("Unassigned")]
        Unassigned,

    }
}
