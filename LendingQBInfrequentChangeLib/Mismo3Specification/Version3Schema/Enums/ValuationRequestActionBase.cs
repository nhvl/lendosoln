namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Modification request to the original order with a higher and/or more expensive level of product or service.
    /// </summary>
    public enum ValuationRequestActionBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Request to permenantly cease work of an order in process.
		/// </summary>
        [XmlEnum("Cancellation")]
        Cancellation,

		/// <summary>
		/// Modification of the original data that was submitted in the original service request.
		/// </summary>
        [XmlEnum("Change")]
        Change,

		/// <summary>
		/// Request to temporarilty cease work of an order in process.
		/// </summary>
        [XmlEnum("Hold")]
        Hold,

		/// <summary>
		/// Initial service request.
		/// </summary>
        [XmlEnum("Original")]
        Original,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Request for cost estimate.
		/// </summary>
        [XmlEnum("PriceQuote")]
        PriceQuote,

		/// <summary>
		/// Additional report copy.
		/// </summary>
        [XmlEnum("Reissue")]
        Reissue,

		/// <summary>
		/// Request to complete order on hold.
		/// </summary>
        [XmlEnum("Resume")]
        Resume,

		/// <summary>
		/// Request for status of an order in process.
		/// </summary>
        [XmlEnum("StatusQuery")]
        StatusQuery,

		/// <summary>
		/// An update to the original order with additional data that was not sent in the original service request. (i.e. If Legal Description was not sent previously, but is needed, then an Update would be used and that data would be passed).
		/// </summary>
        [XmlEnum("Update")]
        Update,

		/// <summary>
		/// Modification request to the original order with a higher and/or more expensive level of product or service.
		/// </summary>
        [XmlEnum("Upgrade")]
        Upgrade,

    }
}
