namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum CreditFileVariationBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("DifferentAddress")]
        DifferentAddress,

        [XmlEnum("DifferentBirthDate")]
        DifferentBirthDate,

        [XmlEnum("DifferentGeneration")]
        DifferentGeneration,

        [XmlEnum("DifferentLastName")]
        DifferentLastName,

        [XmlEnum("DifferentName")]
        DifferentName,

        [XmlEnum("DifferentSSN")]
        DifferentSSN,

    }
}
