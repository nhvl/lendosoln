namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum MISourceBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("FHA")]
        FHA,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("PMI")]
        PMI,

        [XmlEnum("USDA")]
        USDA,

    }
}
