namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Assumes year based on 366 days.
    /// </summary>
    public enum InterestCalculationBasisDaysInYearCountBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Assumes each month has 30 days.
		/// </summary>
        [XmlEnum("360")]
        Item360,

		/// <summary>
		/// Ignores leap years.
		/// </summary>
        [XmlEnum("365")]
        Item365,

		/// <summary>
		/// Splits leap day into 4 different years.
		/// </summary>
        [XmlEnum("365.25")]
        Item36525,

		/// <summary>
		/// Actual days in the year.
		/// </summary>
        [XmlEnum("365Or366")]
        Item365Or366,

		/// <summary>
		/// Assumes year based on 366 days.
		/// </summary>
        [XmlEnum("366")]
        Item366,

    }
}
