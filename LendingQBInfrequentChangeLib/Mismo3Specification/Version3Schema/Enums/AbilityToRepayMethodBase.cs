namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A loan made subject to Regulation Z Qualified Mortgage standards.
    /// </summary>
    public enum AbilityToRepayMethodBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// A loan that is exempt from Regulation Z Ability-to-Repay requirements.
		/// </summary>
        [XmlEnum("Exempt")]
        Exempt,

		/// <summary>
		/// A loan made subject to Regulation Z general Ability-to-Repay standards.
		/// </summary>
        [XmlEnum("General")]
        General,

		/// <summary>
		/// A loan made subject to Regulation Z Qualified Mortgage standards.
		/// </summary>
        [XmlEnum("QualifiedMortgage")]
        QualifiedMortgage,
    }
}
