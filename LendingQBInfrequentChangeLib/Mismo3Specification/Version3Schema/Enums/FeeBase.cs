namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Fee for wiring funds in connection with the loan.
    /// </summary>
    public enum FeeBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("203KArchitecturalAndEngineeringFee")]
        Item203KArchitecturalAndEngineeringFee,

        [XmlEnum("203KConsultantFee")]
        Item203KConsultantFee,

        [XmlEnum("203KDiscountOnRepairs")]
        Item203KDiscountOnRepairs,

        [XmlEnum("203KInspectionFee")]
        Item203KInspectionFee,

        [XmlEnum("203KPermits")]
        Item203KPermits,

        [XmlEnum("203KSupplementalOriginationFee")]
        Item203KSupplementalOriginationFee,

        [XmlEnum("203KTitleUpdate")]
        Item203KTitleUpdate,

        [XmlEnum("AmortizationFee")]
        AmortizationFee,

		/// <summary>
		/// Fee paid to broker or lender to receive loan application for mortgage financing
		/// </summary>
        [XmlEnum("ApplicationFee")]
        ApplicationFee,

		/// <summary>
		/// Fee paid for a professional review of an appraisal, performed in an office setting without visual inspection of the actual premises of the subject property or the comparable properties cited in the appraisal report.
		/// </summary>
        [XmlEnum("AppraisalDeskReviewFee")]
        AppraisalDeskReviewFee,

		/// <summary>
		/// Fee paid to an appraiser for preparation of an independent, impartial, and objective opinion of value of the subject property, expressed in numerical and descriptive terms.
		/// </summary>
        [XmlEnum("AppraisalFee")]
        AppraisalFee,

		/// <summary>
		/// Fee charged for performing an appraisal review in the field - actually inspecting the property and comparable properties.
		/// </summary>
        [XmlEnum("AppraisalFieldReviewFee")]
        AppraisalFieldReviewFee,

		/// <summary>
		/// Fee paid to an external third party for coordinating assignment of qualified appraisers and supervising appraisers' work in completing opinion(s) of value. 
		/// </summary>
        [XmlEnum("AppraisalManagementCompanyFee")]
        AppraisalManagementCompanyFee,

		/// <summary>
		/// Fee for an inspection and/or testing for the presence of asbestos within a building.
		/// </summary>
        [XmlEnum("AsbestosInspectionFee")]
        AsbestosInspectionFee,

        [XmlEnum("AssignmentFee")]
        AssignmentFee,

		/// <summary>
		/// Fee paid for processing, approving and facilitating the assumption of an existing mortgage secured by the subject property to new borrower(s).
		/// </summary>
        [XmlEnum("AssumptionFee")]
        AssumptionFee,

        [XmlEnum("AttorneyFee")]
        AttorneyFee,

		/// <summary>
		/// Fee for use of an automated underwriting tool or system that assists the mortgage broker, correspondent lender, lender or lender's agent in making a decision to approve, deny or counteroffer a loan application submitted by the borrower(s).
		/// </summary>
        [XmlEnum("AutomatedUnderwritingFee")]
        AutomatedUnderwritingFee,

		/// <summary>
		/// Fee paid for preparation of an automated estimate of value of the subject property, expressed in numerical and possibly descriptive terms.
		/// </summary>
        [XmlEnum("AVMFee")]
        AVMFee,

		/// <summary>
		/// Monitoring or Proof of Claim Fee charged as a result of a bankruptcy filing by the Borrower.
		/// </summary>
        [XmlEnum("BankruptcyMonitoringFee")]
        BankruptcyMonitoringFee,

		/// <summary>
		/// Fee paid to a state or local housing agency to participate in a special lending program.
		/// </summary>
        [XmlEnum("BondFee")]
        BondFee,

        [XmlEnum("BondReviewFee")]
        BondReviewFee,

		/// <summary>
		/// Fee charged to prepare and submit to the Home Owner's, Condominium, or Co-Op Association Certification for the property securing the loan.
		/// </summary>
        [XmlEnum("CertificationFee")]
        CertificationFee,

        [XmlEnum("ChosenInterestRateCreditOrChargeTotal")]
        ChosenInterestRateCreditOrChargeTotal,

        [XmlEnum("CLOAccessFee")]
        CLOAccessFee,

        [XmlEnum("CommitmentFee")]
        CommitmentFee,

		/// <summary>
		/// Fee payable to condominium association in which the subject property is located, which is intended to pay electricity bills for street lights, landscaping, and insurance, maintenance and repairs to community facilities like clubhouses, pools, and exercise rooms and may also cover the salaries of condominium association employees and/or third party management fees.
		/// </summary>
        [XmlEnum("CondominiumAssociationDues")]
        CondominiumAssociationDues,

		/// <summary>
		/// Fee payable to condominium association in which the subject property is located for capital improvements, repairs, utility service upgrades, etc. that are assessed in addition to the regularly occurring condominium dues.
		/// </summary>
        [XmlEnum("CondominiumAssociationSpecialAssessment")]
        CondominiumAssociationSpecialAssessment,

		/// <summary>
		/// Fee payable to cooperative in which the subject property is located, which is intended to pay common utilities, landscaping, and insurance, maintenance and repairs to cooperative facilities and may also cover the salaries of cooperative employees and/or third party management fees.
		/// </summary>
        [XmlEnum("CooperativeAssociationDues")]
        CooperativeAssociationDues,

		/// <summary>
		/// Fee payable to cooperative association by the unit owners which is in addition to the regularly occurring maintenance fees for a specific project or outstanding debt that was not part of the annual budget/assessment. The special assessment is against all unit owners and requires them to pay their fractional interest of the money being requested. The payment of the special assessment is divided by each unit owner's interest. The amount may be requested immediately from each unit owner or may be broken into installments depending on how they have decided to handle it. 
		/// </summary>
        [XmlEnum("CooperativeAssociationSpecialAssessment")]
        CooperativeAssociationSpecialAssessment,

		/// <summary>
		/// Fee for copies of documents and/or costs of faxes sent and/or received in conjunction with the loan application, loan processing or loan closing.
		/// </summary>
        [XmlEnum("CopyOrFaxFee")]
        CopyOrFaxFee,

		/// <summary>
		/// Fee for delivery of loan application, loan processing and/or loan closing documents.
		/// </summary>
        [XmlEnum("CourierFee")]
        CourierFee,

		/// <summary>
		/// Premium for insurance in which the lender is the beneficiary and proceeds payoff the unpaid principal balance in the event of the borrower's permanent disability and/or pay recurring mortgage payment during borrower's temporary disability.
		/// </summary>
        [XmlEnum("CreditDisabilityInsurancePremium")]
        CreditDisabilityInsurancePremium,

		/// <summary>
		/// Premium for insurance in which the lender is the beneficiary and proceeds payoff the unpaid principal balance in the event of the borrower's death.
		/// </summary>
        [XmlEnum("CreditLifeInsurancePremium")]
        CreditLifeInsurancePremium,

		/// <summary>
		/// Fee payable for insurance that provides coverage from actions such as fire or theft of any personal property used to secure the loan.
		/// </summary>
        [XmlEnum("CreditPropertyInsurancePremium")]
        CreditPropertyInsurancePremium,

		/// <summary>
		/// Fee for the credit report(s) used in assessing credit risk.
		/// </summary>
        [XmlEnum("CreditReportFee")]
        CreditReportFee,

		/// <summary>
		/// Premium for insurance in which the lender is the beneficiary and proceeds pay recurring mortgage payment during borrower's temporary unemployment.
		/// </summary>
        [XmlEnum("CreditUnemploymentInsurancePremium")]
        CreditUnemploymentInsurancePremium,

		/// <summary>
		/// Fee payable for insurance that provides for cancellation of some or all of the mortgage debt upon certain events, such as death or disability of the borrower(s). 
		/// </summary>
        [XmlEnum("DebtCancellationInsurancePremium")]
        DebtCancellationInsurancePremium,

		/// <summary>
		/// A fee charged for creation, review and generation of the documents necessary to prepare the deed for execution at closing.
		/// </summary>
        [XmlEnum("DeedPreparationFee")]
        DeedPreparationFee,

		/// <summary>
		/// Fee for a report that documents the condition of the property after a localized disaster. 
		/// </summary>
        [XmlEnum("DisasterInspectionFee")]
        DisasterInspectionFee,

        [XmlEnum("DocumentaryStampFee")]
        DocumentaryStampFee,

		/// <summary>
		/// Fee required by the Lender, title company and/or settlement agent for preparation of closing documents.
		/// </summary>
        [XmlEnum("DocumentPreparationFee")]
        DocumentPreparationFee,

		/// <summary>
		/// A fee that pertain to the inspection of the drywall within a building.
		/// </summary>
        [XmlEnum("DryWallInspectionFee")]
        DryWallInspectionFee,

		/// <summary>
		/// A fee that pertain to the inspection of the electrical system within a building.
		/// </summary>
        [XmlEnum("ElectricalInspectionFee")]
        ElectricalInspectionFee,

		/// <summary>
		/// Fee for transmitting or receiving documents in conjunction with loan application, loan processing and/or loan closing by electronic means.
		/// </summary>
        [XmlEnum("ElectronicDocumentDeliveryFee")]
        ElectronicDocumentDeliveryFee,

		/// <summary>
		/// A fee that pertain to the inspection of the Environmental aspects within a building.
		/// </summary>
        [XmlEnum("EnvironmentalInspectionFee")]
        EnvironmentalInspectionFee,

		/// <summary>
		/// Fee to set up and service the escrow account for the loan.
		/// </summary>
        [XmlEnum("EscrowServiceFee")]
        EscrowServiceFee,

		/// <summary>
		/// Fee imposed for waiving the requirement for an escrow account to be established which would, had it been established, been held in reserve to pay periodic payments for homeowners insurance, property taxes and other recurring costs.
		/// </summary>
        [XmlEnum("EscrowWaiverFee")]
        EscrowWaiverFee,

		/// <summary>
		/// Fee paid when submitting a document to the clerk of a court for the court's immediate consideration and for storage in the court's files.
		/// </summary>
        [XmlEnum("FilingFee")]
        FilingFee,

		/// <summary>
		/// Fee paid to obtain a certificate that verifies the flood zone, or special flood hazard area, status of a structure on a given property.
		/// </summary>
        [XmlEnum("FloodCertification")]
        FloodCertification,

		/// <summary>
		/// A fee that pertain to the inspection of the foundation of a building.
		/// </summary>
        [XmlEnum("FoundationInspectionFee")]
        FoundationInspectionFee,

        [XmlEnum("GeneralCounselFee")]
        GeneralCounselFee,

		/// <summary>
		/// A fee charged for an inspection of the adequacy and safety of the heating and cooling systems for the subject property.
		/// </summary>
        [XmlEnum("HeatingCoolingInspectionFee")]
        HeatingCoolingInspectionFee,

		/// <summary>
		/// A fee charged by a counselor or counseling organization for providing counseling regarding high-cost mortgages.
		/// </summary>
        [XmlEnum("HighCostMortgageCounselingFee")]
        HighCostMortgageCounselingFee,

		/// <summary>
		/// Fee for inspection of subject property by professional (other than an appraiser) for an examination of the condition of a home. Items inspected may include but are not limited to electrical, plumbing, sewage disposal, HVAC systems, roof, foundation, permanently installed appliances, drainage and grading, exterior finishes, decks, porches, attics, basements, insulation, smoke and fire detectors, and ventilation.
		/// </summary>
        [XmlEnum("HomeInspectionFee")]
        HomeInspectionFee,

		/// <summary>
		/// Fee payable by homeowners to homeowners or neighborhood association within which the subject property is located, which is intended to pay for the daily operation of the association and may include but is not limited to items such as electricity bills for street lights, landscaping, and maintenance and repairs to community facilities like clubhouses, pools, and exercise rooms and may also cover insurance on community assets, the salaries of HOA employees or third party management fees.
		/// </summary>
        [XmlEnum("HomeownersAssociationDues")]
        HomeownersAssociationDues,

		/// <summary>
		/// Fee payable by homeowners to homeowners or neighborhood association in which the subject property is located for items such as capital improvements, utility service upgrades, etc., that may be assessed in addition to the regularly occurring association dues.
		/// </summary>
        [XmlEnum("HomeownersAssociationSpecialAssessment")]
        HomeownersAssociationSpecialAssessment,

		/// <summary>
		///  Fee for issuance of a warranty covering major systems within the subject property, usually (but not always) including the structure, electrical, plumbing, sewage disposal, HVAC systems, roof, foundation, permanently installed appliances, drainage and grading, exterior finishes, decks, porches, attics, basements, insulation, smoke and fire detectors, and ventilation systems.
		/// </summary>
        [XmlEnum("HomeWarrantyFee")]
        HomeWarrantyFee,

		/// <summary>
		/// Identifies that the fee type being requested is for testing of the existence of Lead.
		/// </summary>
        [XmlEnum("LeadInspectionFee")]
        LeadInspectionFee,

		/// <summary>
		/// Fee for services provided by attorney or law firm representing or performing services on behalf of the lender.
		/// </summary>
        [XmlEnum("LendersAttorneyFee")]
        LendersAttorneyFee,

		/// <summary>
		/// A form of pre-paid interest, equaling a percentage of the loan amount, paid to reduce the interest rate on a mortgage for the life of the loan.
		/// </summary>
        [XmlEnum("LoanDiscountPoints")]
        LoanDiscountPoints,

		/// <summary>
		/// Loan-level pricing adjustments and/or delivery fees charged by mortgage investor or security issuer that is passed onto the borrower, expressed as a percent of loan amount or flat dollar amount.
		/// </summary>
        [XmlEnum("LoanLevelPriceAdjustment")]
        LoanLevelPriceAdjustment,

		/// <summary>
		/// Fee charged by the lender or broker as compensation for providing origination services associated with the subject loan.
		/// </summary>
        [XmlEnum("LoanOriginationFee")]
        LoanOriginationFee,

		/// <summary>
		/// The amount of any compensation of a loan originator paid by the creditor, the borrower, the property seller or any interested party to the loan.
		/// </summary>
        [XmlEnum("LoanOriginatorCompensation")]
        LoanOriginatorCompensation,

		/// <summary>
		/// Fee paid to mortgage broker, correspondent lender, lender or lender's agent to manually review loan processing documents, appraisal and disclosures and make a decision to approve the loan application.
		/// </summary>
        [XmlEnum("ManualUnderwritingFee")]
        ManualUnderwritingFee,

		/// <summary>
		/// A fee charged for an inspection of a Manufactured Home.
		/// </summary>
        [XmlEnum("ManufacturedHousingInspectionFee")]
        ManufacturedHousingInspectionFee,

		/// <summary>
		/// Fee charged for registering/transferring the loan onto the Mortgage Electronic Registration System.
		/// </summary>
        [XmlEnum("MERSRegistrationFee")]
        MERSRegistrationFee,

		/// <summary>
		/// Amount of the first premium paid to MI. For monthly plans, one months premium; for all other plans, Initial Premium Rate times Base Loan Amount.
		/// </summary>
        [XmlEnum("MIInitialPremium")]
        MIInitialPremium,

		/// <summary>
		/// The dollar amount of MI premium (PMI or FHA) charged to the borrower at time of closing. One time payment made at closing beyond the monthly or annual premiums.
		/// </summary>
        [XmlEnum("MIUpfrontPremium")]
        MIUpfrontPremium,

        [XmlEnum("ModificationFee")]
        ModificationFee,

		/// <summary>
		/// Identifies that the fee type being requested is for testing for the existence of mold. This Can be both Airborne and surface and will need to identify the number of samples to be taken for each.
		/// </summary>
        [XmlEnum("MoldInspectionFee")]
        MoldInspectionFee,

        [XmlEnum("MortgageBrokerFee")]
        MortgageBrokerFee,

		/// <summary>
		/// Special assessment, surcharge or fee charged by the county or parish in which the subject property is located in conjunction with a mortgage loan, which is levied in addition to regularly assessed property and school taxes.
		/// </summary>
        [XmlEnum("MortgageSurchargeCountyOrParish")]
        MortgageSurchargeCountyOrParish,

		/// <summary>
		/// Special assessment, surcharge or fee charged by the municipality in which the subject property is located in conjunction with a mortgage loan, which is levied in addition to regularly assessed property and school taxes.
		/// </summary>
        [XmlEnum("MortgageSurchargeMunicipal")]
        MortgageSurchargeMunicipal,

		/// <summary>
		/// Special assessment, surcharge or fee charged by the state in which the subject property is located in conjunction with a mortgage loan, which is levied in addition to regularly assessed property and school taxes.
		/// </summary>
        [XmlEnum("MortgageSurchargeState")]
        MortgageSurchargeState,

		/// <summary>
		/// Fee charged by a municipality to release a lien or injunction after payment of fines for property non-compliance have been completed.
		/// </summary>
        [XmlEnum("MunicipalLienCertificateFee")]
        MunicipalLienCertificateFee,

        [XmlEnum("NewLoanAdministrationFee")]
        NewLoanAdministrationFee,

        [XmlEnum("NotaryFee")]
        NotaryFee,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("OurOriginationChargeTotal")]
        OurOriginationChargeTotal,

		/// <summary>
		/// In refinances, fee paid to prior lender to prepare and provide a bookkeeping statement of how much is owed on the loan being refinanced.
		/// </summary>
        [XmlEnum("PayoffRequestFee")]
        PayoffRequestFee,

		/// <summary>
		/// Cost for inspection, testing and preparation of a report describing all infestations, past infestations and visible damage caused by termites, rodents, squirrels and other pests resulting from an inspection of the interior and exterior of the subject property structures by a qualified pest inspector.
		/// </summary>
        [XmlEnum("PestInspectionFee")]
        PestInspectionFee,

		/// <summary>
		/// A fee charged to conduct an inspection of the plumbing system within a building.
		/// </summary>
        [XmlEnum("PlumbingInspectionFee")]
        PlumbingInspectionFee,

		/// <summary>
		/// A fee charged to create and  review documents necessary to execute a Power of Attorney.
		/// </summary>
        [XmlEnum("PowerOfAttorneyPreparationFee")]
        PowerOfAttorneyPreparationFee,

		/// <summary>
		/// Fee assessed by a government authority to record and index a Power of Attorney as required under State or local law. Recording fees are assessed based on the type of document to be recorded or its physical characteristics, such as the number of pages. Unlike transfer taxes, recording fees are not based on the sales price of the property or loan amount.
		/// </summary>
        [XmlEnum("PowerOfAttorneyRecordingFee")]
        PowerOfAttorneyRecordingFee,

		/// <summary>
		/// Fee to reimburse lender or mortgage broker for performing certain quality control or verification activities and/or using automated fraud detection tools, which may include identity verification, Social Security number or Individual Tax Identification Number validation, employment and/or income validation, Federal Income Tax return validation, etc.
		/// </summary>
        [XmlEnum("PreclosingVerificationControlFee")]
        PreclosingVerificationControlFee,

		/// <summary>
		/// Fee charged by lender, mortgage broker or correspondent lender for processing of loan application.
		/// </summary>
        [XmlEnum("ProcessingFee")]
        ProcessingFee,

		/// <summary>
		/// Fee charged for waiving the requirement of a physical inspection of the subject property and preparation of an appraisal.
		/// </summary>
        [XmlEnum("PropertyInspectionWaiverFee")]
        PropertyInspectionWaiverFee,

		/// <summary>
		/// Fee paid to a firm or individual who verifies the status of unpaid property taxes on the subject property and/or provides continuous monitoring of the tax status on behalf of the lender of record.
		/// </summary>
        [XmlEnum("PropertyTaxStatusResearchFee")]
        PropertyTaxStatusResearchFee,

		/// <summary>
		/// Identifies that the fee type being requested is for testing the Level of Radon emissions in a residence.
		/// </summary>
        [XmlEnum("RadonInspectionFee")]
        RadonInspectionFee,

		/// <summary>
		/// Fee to establish that the lender and borrower have fixed (locked), for a specific period, certain terms of the loan to be closed at a later date.
		/// </summary>
        [XmlEnum("RateLockFee")]
        RateLockFee,

		/// <summary>
		/// Fee payable to licensed real estate broker representing the buyers' interests for professional activities related to the purchase of the subject property.
		/// </summary>
        [XmlEnum("RealEstateCommissionBuyersBroker")]
        RealEstateCommissionBuyersBroker,

		/// <summary>
		/// Fee payable to licensed real estate broker representing the sellers' interests for professional activities related to the listing and/or sale of the subject property.  
		/// </summary>
        [XmlEnum("RealEstateCommissionSellersBroker")]
        RealEstateCommissionSellersBroker,

		/// <summary>
		/// Fees imposed for preparing and providing documents when a loan is paid in full, whether or not the loan is prepaid, such as a loan payoff statement, a reconveyance document, or another document releasing the creditor's security interest in the dwelling that secures the loan.
		/// </summary>
        [XmlEnum("ReconveyanceFee")]
        ReconveyanceFee,

		/// <summary>
		/// Fee assessed by a government authority to record and index an assignment as required under State or local law. Recording fees are assessed based on the type of document to be recorded or its physical characteristics, such as the number of pages. Unlike transfer taxes, recording fees are not based on the sales price of the property or loan amount.
		/// </summary>
        [XmlEnum("RecordingFeeForAssignment")]
        RecordingFeeForAssignment,

		/// <summary>
		/// Fee for recording trust deed, deed of trust, or other security instrument with the appropriate government entity based on the nature of the physical document (typically number of pages) and not based on the sales price or value of the property or loan amount being extended to the borrower.
		/// </summary>
        [XmlEnum("RecordingFeeForDeed")]
        RecordingFeeForDeed,

		/// <summary>
		/// Fee for recording mortgage with the appropriate government entity based on the nature of the physical document (typically number of pages) and not based on the sales price or value of the property or loan amount being extended to the borrower.
		/// </summary>
        [XmlEnum("RecordingFeeForMortgage")]
        RecordingFeeForMortgage,

		/// <summary>
		/// Fee assessed by a government authority to record and index a municipal lien certificate as required under State or local law. Recording fees are assessed based on the type of document to be recorded or its physical characteristics, such as the number of pages. Unlike transfer taxes, recording fees are not based on the sales price of the property or loan amount.
		/// </summary>
        [XmlEnum("RecordingFeeForMunicipalLienCertificate")]
        RecordingFeeForMunicipalLienCertificate,

		/// <summary>
		/// Fee assessed by a government authority to record and index a release as required under State or local law. Recording fees are assessed based on the type of document to be recorded or its physical characteristics, such as the number of pages. Unlike transfer taxes, recording fees are not based on the sales price of the property or loan amount.
		/// </summary>
        [XmlEnum("RecordingFeeForRelease")]
        RecordingFeeForRelease,

		/// <summary>
		/// Fee assessed by a government authority to record and index Subordination Documents as required under State or local law. Recording fees are assessed based on the type of document to be recorded or its physical characteristics, such as the number of pages. Unlike transfer taxes, recording fees are not based on the sales price of the property or loan amount.
		/// </summary>
        [XmlEnum("RecordingFeeForSubordination")]
        RecordingFeeForSubordination,

		/// <summary>
		/// Fee total of all aggregated deed and mortgage taxes and government fees.
		/// </summary>
        [XmlEnum("RecordingFeeTotal")]
        RecordingFeeTotal,

        [XmlEnum("RedrawFee")]
        RedrawFee,

		/// <summary>
		/// Fee for reinspection of the subject property after completion of construction, repairs or improvements.
		/// </summary>
        [XmlEnum("ReinspectionFee")]
        ReinspectionFee,

		/// <summary>
		/// Fee for repairs to subject property by a contractor/professional. Items repaired may include but are not limited to  electrical, plumbing, sewage disposal, HVAC systems, roof, foundation, permanently installed appliances, drainage and grading, exterior finishes, decks, porches, attics, basements, insulation, smoke and fire detectors, ventilation, flooring, walls, and doors.
		/// </summary>
        [XmlEnum("RepairsFee")]
        RepairsFee,

		/// <summary>
		/// Identifies that the fee type being requested is for an detailed inspection of the roof.
		/// </summary>
        [XmlEnum("RoofInspectionFee")]
        RoofInspectionFee,

		/// <summary>
		/// Identifies that the fee type being requested is for testing the operation of an installed septic system.
		/// </summary>
        [XmlEnum("SepticInspectionFee")]
        SepticInspectionFee,

		/// <summary>
		/// Fee paid for settlement service provider to conduct the loan settlement and/or real estate closing transaction.
		/// </summary>
        [XmlEnum("SettlementFee")]
        SettlementFee,

		/// <summary>
		/// Fee charged for the services of a signing agent who, as an accommodation of the borrower, closes the loan at a specific time or at a location requested by the borrower.
		/// </summary>
        [XmlEnum("SigningAgentFee")]
        SigningAgentFee,

		/// <summary>
		/// Identifies that the fee type being requested is for inspection of Smoke Detectors.
		/// </summary>
        [XmlEnum("SmokeDetectorInspectionFee")]
        SmokeDetectorInspectionFee,

		/// <summary>
		/// A fee charged in certain states in lieu of a tax on the premium for Title Insurance. This fee may be a fix amount or may be based on premium amount.
		/// </summary>
        [XmlEnum("StateTitleInsuranceFee")]
        StateTitleInsuranceFee,

		/// <summary>
		/// A fee charged  to perform an inspection and generate a report as to the structural integrity of a building. 
		/// </summary>
        [XmlEnum("StructuralInspectionFee")]
        StructuralInspectionFee,

		/// <summary>
		/// Fee paid to a holder of a lien secured by the subject property to subordinate its lien position to the new first lien.
		/// </summary>
        [XmlEnum("SubordinationFee")]
        SubordinationFee,

		/// <summary>
		/// Fee paid to a professional surveyor to perform an inspection of the physical boundaries of the land pertaining to the subject property and to prepare a drawing and description of the boundaries, improvements (such as buildings, fences, pools, etc.) and any encroachments and/or easements on the land. 
		/// </summary>
        [XmlEnum("SurveyFee")]
        SurveyFee,

        [XmlEnum("TaxRelatedServiceFee")]
        TaxRelatedServiceFee,

		/// <summary>
		/// Fee for a tax stamp to be affixed to a taxable lien to indicate that the appropriate tax has been paid to the city for the deed.
		/// </summary>
        [XmlEnum("TaxStampForCityDeed")]
        TaxStampForCityDeed,

		/// <summary>
		/// Fee for a tax stamp to be affixed to a taxable lien to indicate that the appropriate tax has been paid to the city for the mortgage.
		/// </summary>
        [XmlEnum("TaxStampForCityMortgage")]
        TaxStampForCityMortgage,

		/// <summary>
		/// Fee for a tax stamp to be affixed to a taxable lien to indicate that the appropriate tax has been paid to the county for the deed.
		/// </summary>
        [XmlEnum("TaxStampForCountyDeed")]
        TaxStampForCountyDeed,

		/// <summary>
		/// Fee for a tax stamp to be affixed to a taxable lien to indicate that the appropriate tax has been paid to the county for the mortgage.
		/// </summary>
        [XmlEnum("TaxStampForCountyMortgage")]
        TaxStampForCountyMortgage,

		/// <summary>
		/// Fee for a tax stamp to be affixed to a taxable lien to indicate that the appropriate tax has been paid to the state for the deed.
		/// </summary>
        [XmlEnum("TaxStampForStateDeed")]
        TaxStampForStateDeed,

		/// <summary>
		/// Fee for a tax stamp to be affixed to a taxable lien to indicate that the appropriate tax has been paid to the state for the mortgage.
		/// </summary>
        [XmlEnum("TaxStampForStateMortgage")]
        TaxStampForStateMortgage,

		/// <summary>
		/// Fee paid to lender or lender's agent to administer a temporary buydown.
		/// </summary>
        [XmlEnum("TemporaryBuydownAdministrationFee")]
        TemporaryBuydownAdministrationFee,

		/// <summary>
		/// A form of pre-paid interest, equaling a percentage of the loan amount, paid to temporarily reduce the interest rate on a mortgage for a specific period of the initial term of the loan.
		/// </summary>
        [XmlEnum("TemporaryBuydownPoints")]
        TemporaryBuydownPoints,

		/// <summary>
		/// A fee charged to the research, create and certify a title abstract; a condensed history, taken from public records or documents, of the ownership of a piece of land.
		/// </summary>
        [XmlEnum("TitleCertificationFee")]
        TitleCertificationFee,

		/// <summary>
		/// Fee paid to title company for conducting the loan closing on the subject transaction.
		/// </summary>
        [XmlEnum("TitleClosingFee")]
        TitleClosingFee,

		/// <summary>
		/// Fee charged for a statement or document issued by title insurance underwriters that sets forth the responsibility for negligence of an underwriter, fraud and errors in closings performed by the agents of the underwriter and approved attorneys.
		/// </summary>
        [XmlEnum("TitleClosingProtectionLetterFee")]
        TitleClosingProtectionLetterFee,

		/// <summary>
		/// Fee paid to title company for preparation of documents for the loan closing, deed transfer and other documents needed to perfect the mortgage lien and complete ownership transfer of the subject property.
		/// </summary>
        [XmlEnum("TitleDocumentPreparationFee")]
        TitleDocumentPreparationFee,

		/// <summary>
		/// Fee paid to title company for preparation of any necessary endorsements to the title policy/policies issued pertaining to the subject transaction.
		/// </summary>
        [XmlEnum("TitleEndorsementFee")]
        TitleEndorsementFee,

		/// <summary>
		/// Fee paid to title company for conducting title search and preparing title abstract on subject property.
		/// </summary>
        [XmlEnum("TitleExaminationFee")]
        TitleExaminationFee,

		/// <summary>
		/// Identifies that the fee being referenced is related to the completion and issuance of the American Land Title Association (ALTA) Short Form Residential, a condensed version of the standard policy.
		/// </summary>
        [XmlEnum("TitleFinalPolicyShortFormFee")]
        TitleFinalPolicyShortFormFee,

		/// <summary>
		/// Fee paid to title company for preparation of the title binder (title commitment).
		/// </summary>
        [XmlEnum("TitleInsuranceBinderFee")]
        TitleInsuranceBinderFee,

        [XmlEnum("TitleInsuranceFee")]
        TitleInsuranceFee,

		/// <summary>
		/// Fee paid to title company for issuance of lender's title insurance policy that indemnifies the lender in the event that clear ownership of property is challenged by the discovery of faults in the title.
		/// </summary>
        [XmlEnum("TitleLendersCoveragePremium")]
        TitleLendersCoveragePremium,

		/// <summary>
		/// Fee paid to the title company for the performance of notary services.
		/// </summary>
        [XmlEnum("TitleNotaryFee")]
        TitleNotaryFee,

		/// <summary>
		/// Fee paid to title company for issuance of owner's title insurance policy that indemnifies the owner of real estate in the event that his or her clear ownership of property is challenged by the discovery of faults in the title. (This coverage is optional.)
		/// </summary>
        [XmlEnum("TitleOwnersCoveragePremium")]
        TitleOwnersCoveragePremium,

        [XmlEnum("TitleServicesFeeTotal")]
        TitleServicesFeeTotal,

		/// <summary>
		/// The amount of any taxes as required by county or state on the fee for Title Services.
		/// </summary>
        [XmlEnum("TitleServicesSalesTax")]
        TitleServicesSalesTax,

		/// <summary>
		/// Fee paid to title company for research and resolution of title issues.
		/// </summary>
        [XmlEnum("TitleUnderwritingIssueResolutionFee")]
        TitleUnderwritingIssueResolutionFee,

		/// <summary>
		/// Fee to jurisdictional government entity based on the sales price, value of the property or loan amount being extended to the borrower.
		/// </summary>
        [XmlEnum("TransferTaxTotal")]
        TransferTaxTotal,

        [XmlEnum("UnderwritingFee")]
        UnderwritingFee,

		/// <summary>
		/// An upfront financing fee the borrower pays at closing for USDA Rural Development loans.
		/// </summary>
        [XmlEnum("USDARuralDevelopmentGuaranteeFee")]
        USDARuralDevelopmentGuaranteeFee,

        [XmlEnum("VAFundingFee")]
        VAFundingFee,

		/// <summary>
		/// Fee paid for verification of assets disclosed by the borrower on the loan application.
		/// </summary>
        [XmlEnum("VerificationOfAssetsFee")]
        VerificationOfAssetsFee,

		/// <summary>
		/// Fee paid for verification of employment disclosed by the borrower on the loan application.
		/// </summary>
        [XmlEnum("VerificationOfEmploymentFee")]
        VerificationOfEmploymentFee,

		/// <summary>
		/// Fee paid for verification of income disclosed by the borrower on the loan application.
		/// </summary>
        [XmlEnum("VerificationOfIncomeFee")]
        VerificationOfIncomeFee,

		/// <summary>
		/// Fee paid for verification of residency status disclosed by the borrower on the loan application.
		/// </summary>
        [XmlEnum("VerificationOfResidencyStatusFee")]
        VerificationOfResidencyStatusFee,

		/// <summary>
		/// Fee paid for verification of Social Security number or Individual Tax Identification number disclosed by the borrower on the loan application.
		/// </summary>
        [XmlEnum("VerificationOfTaxpayerIdentificationFee")]
        VerificationOfTaxpayerIdentificationFee,

		/// <summary>
		/// Fee paid for verification of financial information reported by the borrower on the tax returns provided in conjunction with the loan application.
		/// </summary>
        [XmlEnum("VerificationOfTaxReturnFee")]
        VerificationOfTaxReturnFee,

		/// <summary>
		/// Identifies that the fee type being requested is for testing the Water Quality.
		/// </summary>
        [XmlEnum("WaterTestingFee")]
        WaterTestingFee,

		/// <summary>
		/// Identifies that the fee type being requested is for a well inspection.
		/// </summary>
        [XmlEnum("WellInspectionFee")]
        WellInspectionFee,

		/// <summary>
		/// Fee for wiring funds in connection with the loan.
		/// </summary>
        [XmlEnum("WireTransferFee")]
        WireTransferFee,

    }
}
