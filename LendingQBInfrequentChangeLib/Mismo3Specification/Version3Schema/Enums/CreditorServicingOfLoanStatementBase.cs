namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum CreditorServicingOfLoanStatementBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("CreditorIntendsToServiceLoan")]
        CreditorIntendsToServiceLoan,

        [XmlEnum("CreditorIntendsToTransferServicingOfLoan")]
        CreditorIntendsToTransferServicingOfLoan,

        [XmlEnum("CreditorMayAssignSellOrTransferServicingOfLoan")]
        CreditorMayAssignSellOrTransferServicingOfLoan,

        [XmlEnum("Other")]
        Other,

    }
}
