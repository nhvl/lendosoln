namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Oils, such as cooking oil, that have been recycled for use as a home energy source.
    /// </summary>
    public enum SiteUtilityAlternativeEnergySourceBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// A fuel that is developed from organic materials, a renewable and  sustainable source of energy used to create electricity or other forms of power.
		/// </summary>
        [XmlEnum("Biomass")]
        Biomass,

		/// <summary>
		/// Ethanol fuel is ethanol (ethyl alcohol), the same type of alcohol found in alcoholic beverages. It is most often used as a motor fuel, mainly as a biofuel additive for gasoline.
		/// </summary>
        [XmlEnum("Ethanol")]
        Ethanol,

		/// <summary>
		/// The direct use of geothermal energy for heating applications.
		/// </summary>
        [XmlEnum("Geothermal")]
        Geothermal,

		/// <summary>
		/// Hydrogen fuel is a zero-emission fuel which uses electrochemical cells, or  combustion in internal engines, to power vehicles and electric devices. AKA:Atomic element number one.
		/// </summary>
        [XmlEnum("Hydrogen")]
        Hydrogen,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// A colorless and orderless gas used for cooking and heating.
		/// </summary>
        [XmlEnum("Propane")]
        Propane,

		/// <summary>
		/// Oils, such as cooking oil, that have been recycled for use as a home energy source.
		/// </summary>
        [XmlEnum("RecycledOils")]
        RecycledOils,

        [XmlEnum("Solar")]
        Solar,

        [XmlEnum("Wind")]
        Wind,

    }
}
