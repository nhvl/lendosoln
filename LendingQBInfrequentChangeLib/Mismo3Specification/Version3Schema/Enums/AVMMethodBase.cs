namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum AVMMethodBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Assisted")]
        Assisted,

        [XmlEnum("HedonicModel")]
        HedonicModel,

        [XmlEnum("Hybrid")]
        Hybrid,

        [XmlEnum("Interactive")]
        Interactive,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("RepeatSalesModel")]
        RepeatSalesModel,

        [XmlEnum("Unknown")]
        Unknown,
    }
}
