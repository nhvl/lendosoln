namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum IndexLookbackBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("FirstMondayOfTheMonth")]
        FirstMondayOfTheMonth,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("SecondTuesdayOfTheMonth")]
        SecondTuesdayOfTheMonth,

        [XmlEnum("SpecificDayOfTheMonth")]
        SpecificDayOfTheMonth,

        [XmlEnum("ThirdFridayOfTheMonth")]
        ThirdFridayOfTheMonth,

    }
}
