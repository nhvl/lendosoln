namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum EscrowItemPaymentTimingBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("AtClosing")]
        AtClosing,

        [XmlEnum("BeforeClosing")]
        BeforeClosing,

        [XmlEnum("Other")]
        Other,

    }
}
