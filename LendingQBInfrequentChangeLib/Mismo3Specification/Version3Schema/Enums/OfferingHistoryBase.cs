namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates the offering has been temporarily placed on hold such as if the seller is on vacation and does not want the property shown.
    /// </summary>
    public enum OfferingHistoryBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Cancelled")]
        Cancelled,

		/// <summary>
		/// A status indicating a sale whose excecution is contractually contigent on another transaction.
		/// </summary>
        [XmlEnum("ContingentSale")]
        ContingentSale,

        [XmlEnum("ContractOffMarket")]
        ContractOffMarket,

        [XmlEnum("ContractOnMarket")]
        ContractOnMarket,

		/// <summary>
		/// Indicates an offer for purchase is pending on the property.
		/// </summary>
        [XmlEnum("OfferPending")]
        OfferPending,

        [XmlEnum("OnHold")]
        OnHold,

		/// <summary>
		/// Used to indicate the original offering for sale of the property.
		/// </summary>
        [XmlEnum("OriginalOffering")]
        OriginalOffering,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Indicates a price change event on an offering for sale.
		/// </summary>
        [XmlEnum("PriceChange")]
        PriceChange,

		/// <summary>
		/// Indicates that the offering for sale has ended in a sale, but, may not yet be a settled sale.
		/// </summary>
        [XmlEnum("Sold")]
        Sold,

		/// <summary>
		/// Indicates the offering has been temporarily placed on hold such as if the seller is on vacation and does not want the property shown.
		/// </summary>
        [XmlEnum("TemporaryHold")]
        TemporaryHold,

    }
}
