namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// The negative amortization is scheduled to end at a specific point in time.
    /// </summary>
    public enum NegativeAmortizationRecastBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// The negative amortization ends on first occurrence of predetermined percent or a specific point in time.
		/// </summary>
        [XmlEnum("BothScheduledAndMaximumPercent")]
        BothScheduledAndMaximumPercent,

		/// <summary>
		/// The negative amortization ends when it reaches a predetermined percent.
		/// </summary>
        [XmlEnum("MaximumPercent")]
        MaximumPercent,

		/// <summary>
		/// The negative amortization is scheduled to end at a specific point in time.
		/// </summary>
        [XmlEnum("Scheduled")]
        Scheduled,

    }
}
