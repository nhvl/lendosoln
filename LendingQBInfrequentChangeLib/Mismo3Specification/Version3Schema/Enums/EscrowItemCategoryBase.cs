namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Property taxes in aggregate levied by all authorities in which the subject property is located
    /// </summary>
    public enum EscrowItemCategoryBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Fee payable by homeowners to the respective association within which the subject property is located, which is intended to pay for the daily operation of the association and may include but is not limited to items such as electricity bills for street lights, landscaping, and maintenance and repairs to community facilities like clubhouses, pools, and exercise rooms and may also cover insurance on community assets, the salaries of HOA employees or third party management fees and any capital or special assessments in aggregate.
		/// </summary>
        [XmlEnum("AssociationDues")]
        AssociationDues,

		/// <summary>
		/// Funds set aside after closing for moneys held back until a specified work item has been completed.
		/// </summary>
        [XmlEnum("Holdback")]
        Holdback,

		/// <summary>
		/// Insurance against loss of or damage to the property, or against liability arising out of the ownership or use of the property.
		/// </summary>
        [XmlEnum("HomeownersInsurance")]
        HomeownersInsurance,

		/// <summary>
		/// Fee for Insurance or guaranty to protect lender against the non-payment of, or default on, an individual mortgage.
		/// </summary>
        [XmlEnum("MI")]
        MI,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Property taxes in aggregate levied by all authorities in which the subject property is located
		/// </summary>
        [XmlEnum("PropertyTaxes")]
        PropertyTaxes,

    }
}
