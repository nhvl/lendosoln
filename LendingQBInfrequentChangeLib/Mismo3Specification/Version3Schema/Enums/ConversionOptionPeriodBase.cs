namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// The conversion option is available to the borrower on a regularly scheduled basis.
    /// </summary>
    public enum ConversionOptionPeriodBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// The borrower must request the conversion.
		/// </summary>
        [XmlEnum("OnDemand")]
        OnDemand,

        [XmlEnum("OnDemandAtInterestRateChangeDates")]
        OnDemandAtInterestRateChangeDates,

        [XmlEnum("OnDemandMonthly")]
        OnDemandMonthly,

		/// <summary>
		/// The conversion option is available to the borrower on a regularly scheduled basis.
		/// </summary>
        [XmlEnum("Scheduled")]
        Scheduled,

    }
}
