namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Loan program with a term of 12 months or less; which may include a Bridge Loan, Contruction Only, or Construction to Permanent loan with a construction term of 12 months or less and an extension term of less than 12 months.
    /// </summary>
    public enum AbilityToRepayExemptionLoanProgramBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("EmergencyEconomicStabilizationAct")]
        EmergencyEconomicStabilizationAct,

        [XmlEnum("HELOC")]
        HELOC,

        [XmlEnum("HousingFinanceAgency")]
        HousingFinanceAgency,

        [XmlEnum("NonStandardToStandardRefinance")]
        NonStandardToStandardRefinance,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("ReverseMortgage")]
        ReverseMortgage,

		/// <summary>
		/// Loan program with a term of 12 months or less; which may include a Bridge Loan, Contruction Only, or Construction to Permanent loan with a construction term of 12 months or less and an extension term of less than 12 months.
		/// </summary>
        [XmlEnum("TemporaryLoan")]
        TemporaryLoan,

        [XmlEnum("TimeSharePlan")]
        TimeSharePlan,
    }
}
