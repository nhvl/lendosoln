namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Request the attorney file a statement of what is owed and the documentation supporting that claim.
    /// </summary>
    public enum BankruptcyAttorneyServiceReferralBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Request the attorney complete all processes through plan confirmation.
		/// </summary>
        [XmlEnum("CompleteProcessReferral")]
        CompleteProcessReferral,

		/// <summary>
		/// Bifurcation is a debt manipulation tool that allows the bankruptcy filer to keep assets pledged as collateral on loans and pay a smaller percentage of the associated debts. Bifurcation can result in a change to the rate and or principal balance.
		/// </summary>
        [XmlEnum("DebtBifurcation")]
        DebtBifurcation,

		/// <summary>
		/// Request the attorney file a motion to lift the automatic stay to resume normal business activities on the loan.
		/// </summary>
        [XmlEnum("MotionForRelief")]
        MotionForRelief,

		/// <summary>
		/// Request the attorney to establish the market value of the property.
		/// </summary>
        [XmlEnum("MotionToValue")]
        MotionToValue,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Request the attorney to object to the lienholder's treatment in the plan
		/// </summary>
        [XmlEnum("PlanObjection")]
        PlanObjection,

		/// <summary>
		/// Request the attorney review the lienholder treatment in the plan.
		/// </summary>
        [XmlEnum("PlanReview")]
        PlanReview,

		/// <summary>
		/// Request the attorney file a statement of what is owed and the documentation supporting that claim.
		/// </summary>
        [XmlEnum("ProofOfClaim")]
        ProofOfClaim,
    }
}
