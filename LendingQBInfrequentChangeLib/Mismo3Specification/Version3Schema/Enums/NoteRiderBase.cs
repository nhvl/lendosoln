namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum NoteRiderBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("ARM")]
        ARM,

        [XmlEnum("Balloon")]
        Balloon,

        [XmlEnum("Buydown")]
        Buydown,

        [XmlEnum("Occupancy")]
        Occupancy,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Prepayment")]
        Prepayment,

        [XmlEnum("StepRate")]
        StepRate,

    }
}
