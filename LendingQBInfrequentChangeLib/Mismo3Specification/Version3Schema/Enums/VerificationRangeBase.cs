namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum VerificationRangeBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("MostRecentDays")]
        MostRecentDays,

        [XmlEnum("MostRecentMonths")]
        MostRecentMonths,

        [XmlEnum("MostRecentYear")]
        MostRecentYear,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("PaymentPeriod")]
        PaymentPeriod,

        [XmlEnum("StatementPeriod")]
        StatementPeriod,

    }
}
