namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum AdditionalChargeBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("LatePayment")]
        LatePayment,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("PrepaymentPenalty")]
        PrepaymentPenalty,

        [XmlEnum("RealizedLossDueToForeclosedREOPropertyLiquidation")]
        RealizedLossDueToForeclosedREOPropertyLiquidation,

        [XmlEnum("RealizedLossDueToLoanModification")]
        RealizedLossDueToLoanModification,

        [XmlEnum("SkipPayment")]
        SkipPayment,
    }
}
