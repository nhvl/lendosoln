namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Loan Purpose has not been reported or is not known.
    /// </summary>
    public enum LoanPurposeBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Terms of the mortgage are modified from the original terms agreed to by the lender and borrower
		/// </summary>
        [XmlEnum("MortgageModification")]
        MortgageModification,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// A loan made in association with the original purchase of a piece of property.
		/// </summary>
        [XmlEnum("Purchase")]
        Purchase,

		/// <summary>
		/// The repayment of a debt from proceeds of a new loan using the same property as security  or a mortgage secured by a property previously owned free and clear by the Borrower.
		/// </summary>
        [XmlEnum("Refinance")]
        Refinance,

		/// <summary>
		/// Loan Purpose has not been reported or is not known.
		/// </summary>
        [XmlEnum("Unknown")]
        Unknown,

    }
}
