namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Evaluation by the servicer was used to predict default.
    /// </summary>
    public enum LoanImminentDefaultSourceBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// An investor financial model was used to evaluate existing information to predict default.
		/// </summary>
        [XmlEnum("InvestorModel")]
        InvestorModel,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Evaluation by the servicer was used to predict default.
		/// </summary>
        [XmlEnum("ServicerEvaluation")]
        ServicerEvaluation,

    }
}
