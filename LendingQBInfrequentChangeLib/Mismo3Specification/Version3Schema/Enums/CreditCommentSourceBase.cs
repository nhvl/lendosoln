namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Trans Union - the credit data repository provided the comment and/or code.
    /// </summary>
    public enum CreditCommentSourceBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// The comment was from the borrower or coborrower.
		/// </summary>
        [XmlEnum("Borrower")]
        Borrower,

		/// <summary>
		/// The comment text was from the credit bureau that prepared the credit report.
		/// </summary>
        [XmlEnum("CreditBureau")]
        CreditBureau,

		/// <summary>
		/// Equifax - the credit data repository provided the comment and/or code.
		/// </summary>
        [XmlEnum("Equifax")]
        Equifax,

		/// <summary>
		/// Experian - the credit data repository provided the comment and/or code.
		/// </summary>
        [XmlEnum("Experian")]
        Experian,

		/// <summary>
		/// The lender provided the comment and/or code.
		/// </summary>
        [XmlEnum("Lender")]
        Lender,

		/// <summary>
		/// One of the repository bureaus - not specified - provided the comment and/or code.
		/// </summary>
        [XmlEnum("RepositoryBureau")]
        RepositoryBureau,

		/// <summary>
		/// Trans Union - the credit data repository provided the comment and/or code.
		/// </summary>
        [XmlEnum("TransUnion")]
        TransUnion,

    }
}
