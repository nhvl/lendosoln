namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum MICertificateBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("BothPrimaryAndPool")]
        BothPrimaryAndPool,

        [XmlEnum("NoCoverage")]
        NoCoverage,

        [XmlEnum("Pool")]
        Pool,

        [XmlEnum("Primary")]
        Primary,

    }
}
