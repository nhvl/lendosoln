namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Sale of property to be closed at or before closing of the subject property.
    /// </summary>
    public enum OwnedPropertyDispositionStatusBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("PendingSale")]
        PendingSale,

        [XmlEnum("RetainForPrimaryOrSecondaryResidence")]
        RetainForPrimaryOrSecondaryResidence,

        [XmlEnum("RetainForRental")]
        RetainForRental,

		/// <summary>
		/// Sale of property to be closed at or before closing of the subject property.
		/// </summary>
        [XmlEnum("Sold")]
        Sold,

    }
}
