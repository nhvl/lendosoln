namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum FoundationMaterialBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("BlockAndPier")]
        BlockAndPier,

        [XmlEnum("ConcreteRunners")]
        ConcreteRunners,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("PouredConcrete")]
        PouredConcrete,

    }
}
