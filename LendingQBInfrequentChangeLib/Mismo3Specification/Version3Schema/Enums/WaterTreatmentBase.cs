namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A system that removes biological contaminants or chemicals from the water that may exist on faucets or as a whole system purifier.
    /// </summary>
    public enum WaterTreatmentBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// A system that removes biological contaminants or chemicals from the water that may exist on faucets or as a whole system purifier.
		/// </summary>
        [XmlEnum("Purifier")]
        Purifier,

		/// <summary>
		/// A system that removes biological contaminants or chemicals from the water that may exist on faucets or as a whole system purifier.
		/// </summary>
        [XmlEnum("Softener")]
        Softener,

    }
}
