namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A project in which a corporation or business trust holds title to the property and issues shares of stock as evidence of ownership in the corporation or business trust. The corporation or business trust grants occupancy rights to the shareholder tenants through proprietary leases.
    /// </summary>
    public enum ProjectLegalStructureBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Any project or building that is owned by several owners as tenants in common or by a home owners association in which individuals have an undivided interest in a residential apartment building and land, and are the rights of exclusive occupancy of a specific apartment in the building.
		/// </summary>
        [XmlEnum("CommonInterestApartment")]
        CommonInterestApartment,

		/// <summary>
		/// A project that is legally formed as a condominium under the Condominium-Enabling Legislation of the state in which the project is located.
		/// </summary>
        [XmlEnum("Condominium")]
        Condominium,

		/// <summary>
		/// A project in which a corporation or business trust holds title to the property and issues shares of stock as evidence of ownership in the corporation or business trust. The corporation or business trust grants occupancy rights to the shareholder tenants through proprietary leases. 
		/// </summary>
        [XmlEnum("Cooperative")]
        Cooperative,

        [XmlEnum("Unknown")]
        Unknown,

    }
}
