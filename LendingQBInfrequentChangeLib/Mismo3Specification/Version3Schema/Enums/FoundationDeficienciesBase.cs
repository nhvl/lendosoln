namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum FoundationDeficienciesBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Dampness")]
        Dampness,

        [XmlEnum("Infestation")]
        Infestation,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Settlement")]
        Settlement,

    }
}
