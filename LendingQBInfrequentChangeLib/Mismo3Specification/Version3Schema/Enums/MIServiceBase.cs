namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// To check the status of a previously placed order.
    /// </summary>
    public enum MIServiceBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Indicates that the loan that is being submitted for mortgage insurance is also being contract underwritten by the Mortgage Insurance Company.
		/// </summary>
        [XmlEnum("ContractUnderwriteWithMI")]
        ContractUnderwriteWithMI,

		/// <summary>
		/// A process where underwritng rules are checked for the requested insurance product and a decision is returned, but the product is not priced, nor is any insurance implied. Usually done as a precusor to ordering insurance.
		/// </summary>
        [XmlEnum("EligibilityOnly")]
        EligibilityOnly,

		/// <summary>
		/// A process where all underwriting functions are performed by the receiver of the data. No underwritiing functions are delegated to the lender. This usually includes the receiving of both data and documents.
		/// </summary>
        [XmlEnum("FullUnderwrite")]
        FullUnderwrite,

		/// <summary>
		/// A request for Mortgage Insurance. 
		/// </summary>
        [XmlEnum("MIApplication")]
        MIApplication,

        [XmlEnum("NotApplicable")]
        NotApplicable,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// A process where a rate is returned for the requested insurance product, but no underwriting rules are checked, nor is any insurance implied. Usually done as a precusor to ordering insurance.
		/// </summary>
        [XmlEnum("RateQuote")]
        RateQuote,

		/// <summary>
		/// A process where the requested insurance proudct is priced and an underwriting decision is given, but no insurance is implied. Usually done as a precusor to ordering insurance.
		/// </summary>
        [XmlEnum("RateQuoteAndEligibility")]
        RateQuoteAndEligibility,

		/// <summary>
		/// To check the status of a previously placed order. 
		/// </summary>
        [XmlEnum("StatusQuery")]
        StatusQuery,

    }
}
