namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum NeighborhoodGrowthPaceBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Declining")]
        Declining,

        [XmlEnum("FullyDeveloped")]
        FullyDeveloped,

        [XmlEnum("Rapid")]
        Rapid,

        [XmlEnum("Slow")]
        Slow,

        [XmlEnum("Stable")]
        Stable,

    }
}
