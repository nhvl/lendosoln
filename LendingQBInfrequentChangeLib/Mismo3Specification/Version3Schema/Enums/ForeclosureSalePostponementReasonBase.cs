namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// The foreclosure sale cannot proceed forward due to a title defect on the subject property.
    /// </summary>
    public enum ForeclosureSalePostponementReasonBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// An attorney has failed or delayed meeting its responsibilities in processing or handling the foreclosure.
		/// </summary>
        [XmlEnum("AttorneyProcess")]
        AttorneyProcess,

		/// <summary>
		/// Properties that are in poor condition due to fire, floods, mold, environmental hazards (such as asbestos, lead paint, septic tank problems, hazardous materials, etc), and other conditions, which pose a risk of ownership. This does not include personal neglect on the part of the borrower.
		/// </summary>
        [XmlEnum("CompromisedPropertyCondition")]
        CompromisedPropertyCondition,

		/// <summary>
		/// A government or law forcement agency has seized the property.
		/// </summary>
        [XmlEnum("GovernmentSeizure")]
        GovernmentSeizure,

		/// <summary>
		/// An investor authorized delay in proceeding forward with the foreclosure. A moratorium is usually for some defined period of time. Reasons for a moratorium could include natural disaster, holiday period, etc.
		/// </summary>
        [XmlEnum("Moratorium")]
        Moratorium,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Delegated to servicer for additional retention or workout processing in consideration of investor guidelines.
		/// </summary>
        [XmlEnum("ServicerDelegation")]
        ServicerDelegation,

		/// <summary>
		/// A Servicer has failed or delayed meeting its responsibilities in processing or handling the foreclosure.
		/// </summary>
        [XmlEnum("ServicerProcess")]
        ServicerProcess,

		/// <summary>
		/// A state/jurisdictional action delaying or halting the foreclosure process.
		/// </summary>
        [XmlEnum("StatutoryJurisdictionalDelay")]
        StatutoryJurisdictionalDelay,

		/// <summary>
		/// The foreclosure sale cannot proceed forward due to a title defect on the subject property.
		/// </summary>
        [XmlEnum("TitleDefect")]
        TitleDefect,

    }
}
