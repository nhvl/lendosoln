namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// identifies multiunit structure or property in which persons hold fee simple title to individual units and an undivided interest in common interest.
    /// </summary>
    public enum NeighborhoodHousingBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// identifies multiunit structure or property in which persons hold fee simple title to individual units and an undivided interest in common interest.
		/// </summary>
        [XmlEnum("Condominium")]
        Condominium,

        [XmlEnum("Cooperative")]
        Cooperative,

        [XmlEnum("ManufacturedHome")]
        ManufacturedHome,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("SingleFamily")]
        SingleFamily,

        [XmlEnum("TwoToFourFamily")]
        TwoToFourFamily,

    }
}
