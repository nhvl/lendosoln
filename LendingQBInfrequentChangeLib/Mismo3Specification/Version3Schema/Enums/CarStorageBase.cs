namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum CarStorageBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Carport")]
        Carport,

        [XmlEnum("Covered")]
        Covered,

        [XmlEnum("Driveway")]
        Driveway,

        [XmlEnum("Garage")]
        Garage,

        [XmlEnum("OffStreet")]
        OffStreet,

        [XmlEnum("Open")]
        Open,

        [XmlEnum("Other")]
        Other,

    }
}
