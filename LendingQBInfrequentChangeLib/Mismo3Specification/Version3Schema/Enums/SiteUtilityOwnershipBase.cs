namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum SiteUtilityOwnershipBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("NonPublic")]
        NonPublic,

        [XmlEnum("Public")]
        Public,

        [XmlEnum("Unknown")]
        Unknown,

    }
}
