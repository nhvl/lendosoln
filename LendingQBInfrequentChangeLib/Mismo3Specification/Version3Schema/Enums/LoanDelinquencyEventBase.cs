namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// The completed Borrower's Response Package has been received by the servicer.
    /// </summary>
    public enum LoanDelinquencyEventBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("BorrowerEmailSent")]
        BorrowerEmailSent,

        [XmlEnum("BorrowerFaceToFaceInterviewConducted")]
        BorrowerFaceToFaceInterviewConducted,

        [XmlEnum("BorrowerFormLetterSent")]
        BorrowerFormLetterSent,

        [XmlEnum("BreachLetterExpires")]
        BreachLetterExpires,

		/// <summary>
		/// The breach or acceleration letter has been sent and the mortgage loan has not yet been referred to an attorney (or trustee) for foreclosure proceedings.
		/// </summary>
        [XmlEnum("BreachLetterSent")]
        BreachLetterSent,

        [XmlEnum("BrokerPriceOpinionOrAppraisalObtained")]
        BrokerPriceOpinionOrAppraisalObtained,

        [XmlEnum("BrokerPriceOpinionOrdered")]
        BrokerPriceOpinionOrdered,

		/// <summary>
		/// The Creditors agreed to the restructuring plan.
		/// </summary>
        [XmlEnum("ChapterElevenBankruptcyPlanConfirmed")]
        ChapterElevenBankruptcyPlanConfirmed,

		/// <summary>
		/// Legal clearance is under a Chapter 11 bankruptcy. The Loan Delinquency Event Date reported is the date that relief was granted, the date the case was dismissed or some other provision as provided under the bankruptcy plan.
		/// </summary>
        [XmlEnum("ChapterElevenCourtClearanceObtained")]
        ChapterElevenCourtClearanceObtained,

        [XmlEnum("ChapterElevenPropertySurrendered")]
        ChapterElevenPropertySurrendered,

		/// <summary>
		/// Notification that the Chapter 7 Bankruptcy will remain in effect longer than expected due to the fact that the trustee has found additional assets.
		/// </summary>
        [XmlEnum("ChapterSevenBankruptcyAssetsPursued")]
        ChapterSevenBankruptcyAssetsPursued,

		/// <summary>
		/// Legal clearance is under a Chapter 7 bankruptcy. The Loan Delinquency Event Date reported is the date that relief was granted, the date the case was dismissed or the later of the Discharge of Debtor or the filing of the Trustee No Asset report/ Abandonment.
		/// </summary>
        [XmlEnum("ChapterSevenCourtClearanceObtained")]
        ChapterSevenCourtClearanceObtained,

        [XmlEnum("ChapterSevenPropertySurrendered")]
        ChapterSevenPropertySurrendered,

		/// <summary>
		/// The Bankruptcy Court entered confirmation of the bankruptcy of the Debtors Chapter 13 plan.
		/// </summary>
        [XmlEnum("ChapterThirteenBankruptcyPlanConfirmed")]
        ChapterThirteenBankruptcyPlanConfirmed,

		/// <summary>
		/// Legal clearance is under a Chapter 13 bankruptcy. The Loan Delinquency Event Date reported is the date that relief was granted or the date the case was dismissed.
		/// </summary>
        [XmlEnum("ChapterThirteenCourtClearanceObtained")]
        ChapterThirteenCourtClearanceObtained,

        [XmlEnum("ChapterThirteenPropertySurrendered")]
        ChapterThirteenPropertySurrendered,

		/// <summary>
		/// The Bankruptcy Court entered confirmation of the bankruptcy of the Debtors Chapter 12 plan.
		/// </summary>
        [XmlEnum("ChapterTwelveBankruptcyPlanConfirmed")]
        ChapterTwelveBankruptcyPlanConfirmed,

		/// <summary>
		/// Legal clearance is under a Chapter 12 bankruptcy. The Loan Delinquency Event Date reported is the date that relief was granted or the date the case was dismissed.
		/// </summary>
        [XmlEnum("ChapterTwelveCourtClearanceObtained")]
        ChapterTwelveCourtClearanceObtained,

        [XmlEnum("ChapterTwelvePropertySurrendered")]
        ChapterTwelvePropertySurrendered,

		/// <summary>
		/// The investor has authorized the Servicer to accept a voluntary conveyance of the property instead of initiating foreclosure proceedings. The Servicer has received the executed Deed from the borrower.
		/// </summary>
        [XmlEnum("DeedInLieu")]
        DeedInLieu,

        [XmlEnum("EligibleForHAMP")]
        EligibleForHAMP,

		/// <summary>
		/// The mortgage is in foreclosure and the final judgment or decree has been recorded in the appropriate venue.
		/// </summary>
        [XmlEnum("FinalJudgmentRecorded")]
        FinalJudgmentRecorded,

		/// <summary>
		/// The first legal step in processing a foreclosure was taken (i.e. complaint/petition filing, recording notice of default, posting publication of notice of sale, etc).
		/// </summary>
        [XmlEnum("FirstLegalAction")]
        FirstLegalAction,

		/// <summary>
		/// First contact was made by the Servicer or the agent of the Servicer in the delinquency period with the Borrower, co-Borrower, a representative of the estate or anyone that is legally responsible for representing any of these entities.
		/// </summary>
        [XmlEnum("FirstRightPartyContact")]
        FirstRightPartyContact,

        [XmlEnum("ForbearanceBegin")]
        ForbearanceBegin,

        [XmlEnum("ForbearanceEnd")]
        ForbearanceEnd,

		/// <summary>
		/// Foreclosure activity is placed on hold. 
		/// </summary>
        [XmlEnum("ForeclosureOnHold")]
        ForeclosureOnHold,

		/// <summary>
		/// The foreclosure process is restarted after a delay has occurred. Delays such as Bankruptcy, Contested/ Litigated Action, Repayment Plan, Forbearance, Drug Seizure, HUD Assignment/ VA Refunding review, canceled Workout, etc. could suspend the foreclosure process.
		/// </summary>
        [XmlEnum("ForeclosureRestarted")]
        ForeclosureRestarted,

		/// <summary>
		/// The foreclosure sale that was scheduled has been cancelled.
		/// </summary>
        [XmlEnum("ForeclosureSaleCancelled")]
        ForeclosureSaleCancelled,

        [XmlEnum("ForeclosureSaleConfirmed")]
        ForeclosureSaleConfirmed,

		/// <summary>
		/// The foreclosure sale that was on hold is now being  continued (or scheduled).
		/// </summary>
        [XmlEnum("ForeclosureSaleContinued")]
        ForeclosureSaleContinued,

        [XmlEnum("ForeclosureSaleHeld")]
        ForeclosureSaleHeld,

		/// <summary>
		/// The foreclosure sale has been rescinded. 
		/// </summary>
        [XmlEnum("ForeclosureSaleRescinded")]
        ForeclosureSaleRescinded,

		/// <summary>
		/// A foreclosure sale has been scheduled by the court or sheriff. In a statutory foreclosure state, the Loan Delinquency Event Date reported is the earliest date after the publication has expired on which the sale could occur. If the foreclosure sale is rescheduled, the Loan Delinquency Event Date reported is the updated foreclosure sale date.
		/// </summary>
        [XmlEnum("ForeclosureSaleScheduled")]
        ForeclosureSaleScheduled,

		/// <summary>
		/// The borrower has restored the delinquent mortgage to current status by paying the total amount delinquent, including advances, accrued interest, legal costs and other expenses.
		/// </summary>
        [XmlEnum("FullReinstatement")]
        FullReinstatement,

		/// <summary>
		/// The servicer has evaluated the borrower response package eligibilty for HAMP.
		/// </summary>
        [XmlEnum("HAMPBorrowerResponsePackageEvaluated")]
        HAMPBorrowerResponsePackageEvaluated,

		/// <summary>
		/// Borrower declines to accept a HAMP workout
		/// </summary>
        [XmlEnum("HAMPDeclinedByBorrower")]
        HAMPDeclinedByBorrower,

        [XmlEnum("HAMPInReview")]
        HAMPInReview,

        [XmlEnum("HAMPModificationAgreementReceived")]
        HAMPModificationAgreementReceived,

        [XmlEnum("HAMPModificationAgreementSent")]
        HAMPModificationAgreementSent,

		/// <summary>
		/// Servicer received HAMP Stipulation Documents.
		/// </summary>
        [XmlEnum("HAMPStipulationDocumentsReceived")]
        HAMPStipulationDocumentsReceived,

		/// <summary>
		/// The servicer receives the Hardship Affidavit from the borrower for any relevant workout.
		/// </summary>
        [XmlEnum("HardshipAffidavitReceived")]
        HardshipAffidavitReceived,

        [XmlEnum("IneligibleForModification")]
        IneligibleForModification,

        [XmlEnum("IneligibleForShortSale")]
        IneligibleForShortSale,

		/// <summary>
		/// Notification to the investor that the property is now REO.
		/// </summary>
        [XmlEnum("InvestorREONotificationSent")]
        InvestorREONotificationSent,

		/// <summary>
		/// The latest outbound call attempted to the borrower or right party contact . 
		/// </summary>
        [XmlEnum("LastOutboundCallAttemptToBorrower")]
        LastOutboundCallAttemptToBorrower,

		/// <summary>
		/// Last contact was made by the Servicer or the agent of the Servicer in the delinquency period with the Borrower, co-Borrower, a representative of the estate or anyone that is legally responsible for representing any of these entities.
		/// </summary>
        [XmlEnum("LastRightPartyContact")]
        LastRightPartyContact,

		/// <summary>
		/// The latest attempt to locate the borrower.
		/// </summary>
        [XmlEnum("LastSkipTraceAttempt")]
        LastSkipTraceAttempt,

        [XmlEnum("ModificationInReview")]
        ModificationInReview,

		/// <summary>
		/// The denial letter was sent by the servicer on behalf of the investor.
		/// </summary>
        [XmlEnum("ModificationNotificationDenied")]
        ModificationNotificationDenied,

        [XmlEnum("ModificationTrialBegin")]
        ModificationTrialBegin,

        [XmlEnum("ModificationTrialEnd")]
        ModificationTrialEnd,

		/// <summary>
		/// Servicer has no delinquency activity to report.
		/// </summary>
        [XmlEnum("NoActivity")]
        NoActivity,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// The borrower pays an amount less than the total amount delinquent, including advances, accrued interest, legal costs and other expenses and agrees to a repayment plan to bring the account current within a defined period of time.
		/// </summary>
        [XmlEnum("PartialReinstatement")]
        PartialReinstatement,

        [XmlEnum("PositiveSolicitationResponse")]
        PositiveSolicitationResponse,

		/// <summary>
		/// This event occurs when the Borrower does not keep their commitment to make payment promised by a specified date.
		/// </summary>
        [XmlEnum("PromiseToPayBroken")]
        PromiseToPayBroken,

		/// <summary>
		/// This event occurs when the Borrower kept their commitment and made a payment that they promised to make by a specified date.
		/// </summary>
        [XmlEnum("PromiseToPayKept")]
        PromiseToPayKept,

		/// <summary>
		/// This event occurs when the Borrower verbally promises to make a payment by a specified date.
		/// </summary>
        [XmlEnum("PromiseToPayMade")]
        PromiseToPayMade,

		/// <summary>
		/// Notification from property inspections, the Borrower or some other source that the property securing the Mortgage was listed for sale.
		/// </summary>
        [XmlEnum("PropertyIsListedForSale")]
        PropertyIsListedForSale,

		/// <summary>
		/// Reinitiating all foreclosure activities due to a failure in the current foreclosure process.
		/// </summary>
        [XmlEnum("ReinitiateForeclosure")]
        ReinitiateForeclosure,

		/// <summary>
		/// Real estate owned loan after foreclosure sale has been completed. Title has reverted to the beneficiary.
		/// </summary>
        [XmlEnum("REO")]
        REO,

		/// <summary>
		/// The initial payment on a repayment plan was received.
		/// </summary>
        [XmlEnum("RepaymentBegin")]
        RepaymentBegin,

		/// <summary>
		/// The final payment on a repayment plan was received.
		/// </summary>
        [XmlEnum("RepaymentEnd")]
        RepaymentEnd,

		/// <summary>
		/// The first payment negotiated under a Repayment Plan is received by the Servicer.
		/// </summary>
        [XmlEnum("RepaymentPlanFirstPlanPaymentMade")]
        RepaymentPlanFirstPlanPaymentMade,

        [XmlEnum("RepaymentPlanInReview")]
        RepaymentPlanInReview,

        [XmlEnum("RightPartyContactNextContactScheduled")]
        RightPartyContactNextContactScheduled,

		/// <summary>
		/// The right party contact was contacted and there was no action agreed upon.
		/// </summary>
        [XmlEnum("RightPartyContactNoAction")]
        RightPartyContactNoAction,

		/// <summary>
		/// Servicer assigns loan to a single point of contact (SPOC) for the borrower.
		/// </summary>
        [XmlEnum("ServicerSinglePointOfContactAssigned")]
        ServicerSinglePointOfContactAssigned,

		/// <summary>
		/// The fully executed short sale contract for the property was signed by all responsible parties.
		/// </summary>
        [XmlEnum("ShortSaleAgreementSigned")]
        ShortSaleAgreementSigned,

		/// <summary>
		/// A short sale has been completed.
		/// </summary>
        [XmlEnum("ShortSaleCompleted")]
        ShortSaleCompleted,

		/// <summary>
		/// A short sale contract or offer has been received by the servicer.
		/// </summary>
        [XmlEnum("ShortSaleContractReceived")]
        ShortSaleContractReceived,

        [XmlEnum("ShortSaleInReview")]
        ShortSaleInReview,

        [XmlEnum("SkipTraceInitiated")]
        SkipTraceInitiated,

        [XmlEnum("SkipTraceSuccessful")]
        SkipTraceSuccessful,

        [XmlEnum("SolicitationLetterSent")]
        SolicitationLetterSent,

		/// <summary>
		/// The property sold to a 3rd Party Purchaser at the foreclosure sale and did not revert to the Servicer/ Investor.
		/// </summary>
        [XmlEnum("ThirdPartySale")]
        ThirdPartySale,

		/// <summary>
		/// A title issue has been identified and is pending resolution.
		/// </summary>
        [XmlEnum("TitleClaimResolutionPursed")]
        TitleClaimResolutionPursed,

        [XmlEnum("UnsuccessfulRightPartyContactPhoneContactAttempt")]
        UnsuccessfulRightPartyContactPhoneContactAttempt,

		/// <summary>
		/// Received borrower income, assets, liabilities and expenses information either verbally or written.
		/// </summary>
        [XmlEnum("UpdatedBorrowerFinancialsReceived")]
        UpdatedBorrowerFinancialsReceived,

		/// <summary>
		/// The servicer has evaluatedÂ the borrower for a workout.
		/// </summary>
        [XmlEnum("WorkoutEvaluationComplete")]
        WorkoutEvaluationComplete,

		/// <summary>
		/// The borrower is being evaluated for a workout which is an alternative to foreclosure.
		/// </summary>
        [XmlEnum("WorkoutInReview")]
        WorkoutInReview,

		/// <summary>
		/// The completed Borrower's Response Package has been received by the servicer.
		/// </summary>
        [XmlEnum("WorkoutPackageCompleted")]
        WorkoutPackageCompleted,

    }
}
