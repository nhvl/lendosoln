namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum ExteriorFeatureBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Doors")]
        Doors,

        [XmlEnum("GuttersAndDownspouts")]
        GuttersAndDownspouts,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Skirting")]
        Skirting,

    }
}
