namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum CertificateAuthorityBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Private")]
        Private,

        [XmlEnum("PublicFederal")]
        PublicFederal,

        [XmlEnum("PublicLocalCounty")]
        PublicLocalCounty,

        [XmlEnum("PublicLocalMunicipal")]
        PublicLocalMunicipal,

        [XmlEnum("PublicState")]
        PublicState,

    }
}
