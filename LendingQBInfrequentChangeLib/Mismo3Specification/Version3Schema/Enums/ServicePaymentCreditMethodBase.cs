namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum ServicePaymentCreditMethodBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("AmericanExpress")]
        AmericanExpress,

        [XmlEnum("DinersClub")]
        DinersClub,

        [XmlEnum("Discover")]
        Discover,

        [XmlEnum("MasterCard")]
        MasterCard,

        [XmlEnum("MasterCardDebit")]
        MasterCardDebit,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Visa")]
        Visa,

        [XmlEnum("VisaDebit")]
        VisaDebit,

    }
}
