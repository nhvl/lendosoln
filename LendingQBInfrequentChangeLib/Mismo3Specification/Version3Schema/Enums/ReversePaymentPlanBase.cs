namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Receive equal monthly proceeds for a fixed period of time. The borrower must occupy the property to receive the monthly proceeds.
    /// </summary>
    public enum ReversePaymentPlanBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// The entire loan amount is available as a line of credit as long as the borrower occupies the property. Interest is only paid on the funds withdrawn for use.
		/// </summary>
        [XmlEnum("LineOfCredit")]
        LineOfCredit,

		/// <summary>
		/// Receive equal monthly proceeds for as long as the borrower occupies the property and has the option to draw against a line of credit.
		/// </summary>
        [XmlEnum("ModifiedTenure")]
        ModifiedTenure,

		/// <summary>
		/// Receive equal monthly proceeds for a fixed period of time and has the option to draw against a line of credit. The borrower must occupy the property to receive the monthly proceeds.
		/// </summary>
        [XmlEnum("ModifiedTerm")]
        ModifiedTerm,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// Receive equal monthly proceeds for as long as the borrower occupies the property.
		/// </summary>
        [XmlEnum("Tenure")]
        Tenure,

		/// <summary>
		/// Receive equal monthly proceeds for a fixed period of time. The borrower must occupy the property to receive the monthly proceeds.
		/// </summary>
        [XmlEnum("Term")]
        Term,

    }
}
