namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// The servicer has received a request for a workout and is evaluating the request.
    /// </summary>
    public enum MIWorkoutDecisionBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// The request for a workout has been approved.
		/// </summary>
        [XmlEnum("Approved")]
        Approved,

		/// <summary>
		/// The borrower has declined the workout offer.
		/// </summary>
        [XmlEnum("BorrowerDeclined")]
        BorrowerDeclined,

		/// <summary>
		/// The request for a workout has been denied.
		/// </summary>
        [XmlEnum("Denied")]
        Denied,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// The servicer has received a request for a workout and is evaluating the request.
		/// </summary>
        [XmlEnum("UnderReview")]
        UnderReview,

    }
}
