namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Late charge is a percentage of principal and interest amount.
    /// </summary>
    public enum LateChargeBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("FlatDollarAmount")]
        FlatDollarAmount,

        [XmlEnum("NoLateCharges")]
        NoLateCharges,

		/// <summary>
		/// Late charge is a percentage of the delinquent interest on any overdue payment.
		/// </summary>
        [XmlEnum("PercentageOfDelinquentInterest")]
        PercentageOfDelinquentInterest,

		/// <summary>
		/// Late change is a percentage of the total payment less any buydown subsidy or optional insurance portion of that payment.
		/// </summary>
        [XmlEnum("PercentageOfNetPayment")]
        PercentageOfNetPayment,

		/// <summary>
		/// Late charge is a percentage of the principal balance.
		/// </summary>
        [XmlEnum("PercentageOfPrincipalBalance")]
        PercentageOfPrincipalBalance,

		/// <summary>
		/// Late charge is a percentage of the total payment.
		/// </summary>
        [XmlEnum("PercentageOfTotalPayment")]
        PercentageOfTotalPayment,

		/// <summary>
		/// Late charge is a percentage of principal and interest amount.
		/// </summary>
        [XmlEnum("PercentOfPrincipalAndInterest")]
        PercentOfPrincipalAndInterest,

    }
}
