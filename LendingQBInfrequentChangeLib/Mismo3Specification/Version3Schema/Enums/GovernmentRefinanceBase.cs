namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum GovernmentRefinanceBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("FullDocumentation")]
        FullDocumentation,

        [XmlEnum("InterestRateReductionRefinanceLoan")]
        InterestRateReductionRefinanceLoan,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("StreamlineWithAppraisal")]
        StreamlineWithAppraisal,

        [XmlEnum("StreamlineWithoutAppraisal")]
        StreamlineWithoutAppraisal,

    }
}
