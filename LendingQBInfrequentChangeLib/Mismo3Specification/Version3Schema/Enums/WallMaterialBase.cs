namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum WallMaterialBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Aluminum")]
        Aluminum,

        [XmlEnum("Block")]
        Block,

        [XmlEnum("Brick")]
        Brick,

        [XmlEnum("CementBoard")]
        CementBoard,

        [XmlEnum("Concrete")]
        Concrete,

        [XmlEnum("Flagstone")]
        Flagstone,

        [XmlEnum("Frame")]
        Frame,

        [XmlEnum("Glass")]
        Glass,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Steel")]
        Steel,

        [XmlEnum("Stone")]
        Stone,

        [XmlEnum("Stucco")]
        Stucco,

        [XmlEnum("Synthetic")]
        Synthetic,

        [XmlEnum("Veneer")]
        Veneer,

        [XmlEnum("Vinyl")]
        Vinyl,

        [XmlEnum("Wood")]
        Wood,

    }
}
