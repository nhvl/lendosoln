namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A change in interest rate related to the successive history of timely payments
    /// </summary>
    public enum AdjustmentChangeOccurrenceBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// A change in the mortgages interest rate or PI Payment related to an Adjustable Rate Mortgage contract.
		/// </summary>
        [XmlEnum("ARM")]
        ARM,

		/// <summary>
		/// A change in interest rate related to the conversion of a mortgage to a fixed rate loan.
		/// </summary>
        [XmlEnum("Conversion")]
        Conversion,

		/// <summary>
		/// Referred to as Growing Equity Mortgage. A change to the PI payment as defined in the original mortgage note.
		/// </summary>
        [XmlEnum("GEM")]
        GEM,

		/// <summary>
		/// Referred to as Graduated Payment Mortgage. A change to the PI payment as defined in the original mortgage note.
		/// </summary>
        [XmlEnum("GPM")]
        GPM,

		/// <summary>
		/// A change in interest rate related to a comparison of the loan amount to the property value as a measure of equity in the property.
		/// </summary>
        [XmlEnum("LoanToValue")]
        LoanToValue,

		/// <summary>
		/// A change in the mortgages interest rate or PI Payment not scheduled based upon the terms of the mortgage note.
		/// </summary>
        [XmlEnum("NonScheduled")]
        NonScheduled,

        [XmlEnum("Other")]
        Other,

		/// <summary>
		/// A change to the PI payment based on a fixed periodic payment/rate change.
		/// </summary>
        [XmlEnum("Step")]
        Step,

		/// <summary>
		/// A change in interest rate related to the successive history of timely payments
		/// </summary>
        [XmlEnum("TimelyPaymentReward")]
        TimelyPaymentReward,
    }
}
