namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum MERSRegistrationBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("MERS_ERegistry")]
        MERS_ERegistry,

        [XmlEnum("MERSSystem")]
        MERSSystem,

        [XmlEnum("Other")]
        Other,

    }
}
