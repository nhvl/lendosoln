namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Office of Foreign Asset Control
    /// </summary>
    public enum RegulatoryProductSourceBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

		/// <summary>
		/// Office of Foreign Asset Control
		/// </summary>
        [XmlEnum("OFAC")]
        OFAC,

        [XmlEnum("Other")]
        Other,

    }
}
