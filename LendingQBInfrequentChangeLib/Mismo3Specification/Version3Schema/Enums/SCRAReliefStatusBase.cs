namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A catchall value to account for a blank field in LQB.
    /// </summary>
    public enum SCRAReliefStatusBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("NeverReceived")]
        NeverReceived,

        [XmlEnum("OneOrMoreBorrowersCurrentlyReceive")]
        OneOrMoreBorrowersCurrentlyReceive,

        [XmlEnum("OneOrMoreBorrowersPreviouslyReceived")]
        OneOrMoreBorrowersPreviouslyReceived,

        [XmlEnum("Other")]
        Other,

    }
}
