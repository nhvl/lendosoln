namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// The party selected by the beneficiary of the Deed of Trust or the settlor/grantor/trustor/ of a trust to oversee and manage the assets held in trust for the benefit of the named beneficiaries. An individual or a financial institution can serve as trustee.
    /// </summary>
    public enum PartyRoleBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("Appraiser")]
        Appraiser,

        [XmlEnum("AppraiserSupervisor")]
        AppraiserSupervisor,

        [XmlEnum("AssignFrom")]
        AssignFrom,

        [XmlEnum("AssignTo")]
        AssignTo,

		/// <summary>
		/// A person admitted to practice law in at least one jurisdiction and authorized to perform criminal and civil legal functions on behalf of clients. 
		/// </summary>
        [XmlEnum("Attorney")]
        Attorney,

		/// <summary>
		/// A person who is authorized to act as agent or attorney on behalf of another person.
		/// </summary>
        [XmlEnum("AttorneyInFact")]
        AttorneyInFact,

        [XmlEnum("AuthorizedRepresentative")]
        AuthorizedRepresentative,

		/// <summary>
		/// A individual or legal entity that has been authorized by the borrower to speak on their behalf in matters concerning the loan.
		/// </summary>
        [XmlEnum("AuthorizedThirdParty")]
        AuthorizedThirdParty,

		/// <summary>
		/// The party who initiates a bankruptcy filling.
		/// </summary>
        [XmlEnum("BankruptcyFiler")]
        BankruptcyFiler,

		/// <summary>
		/// The individual or legal entity that is in charge of administering the bankruptcy case.
		/// </summary>
        [XmlEnum("BankruptcyTrustee")]
        BankruptcyTrustee,

		/// <summary>
		/// Person or organization who has the beneficial interest in the deed of trust.
		/// </summary>
        [XmlEnum("BeneficialInterestParty")]
        BeneficialInterestParty,

        [XmlEnum("BillToParty")]
        BillToParty,

        [XmlEnum("Borrower")]
        Borrower,

        [XmlEnum("Builder")]
        Builder,

        [XmlEnum("Client")]
        Client,

        [XmlEnum("ClosingAgent")]
        ClosingAgent,

		/// <summary>
		/// The person or entity who is appointed to manage the financial affairs of someone who is not capable of managing their own affairs, like for a child or mentally incompetent person.
		/// </summary>
        [XmlEnum("Conservator")]
        Conservator,

		/// <summary>
		/// Cooperative Company is the legal entity of the housing cooperative. 
		/// </summary>
        [XmlEnum("CooperativeCompany")]
        CooperativeCompany,

        [XmlEnum("CorrespondentLender")]
        CorrespondentLender,

        [XmlEnum("Cosigner")]
        Cosigner,

		/// <summary>
		/// The individual or legal entity that provides credit counseling.
		/// </summary>
        [XmlEnum("CreditCounselingAgent")]
        CreditCounselingAgent,

		/// <summary>
		/// In the provision of credit enhancement, secondary to primary MI, the holder of the loss in accordance with the credit enhancement type. 
		/// </summary>
        [XmlEnum("CreditEnhancementRiskHolder")]
        CreditEnhancementRiskHolder,

        [XmlEnum("CustodianNotePayTo")]
        CustodianNotePayTo,

        [XmlEnum("DeliverRescissionTo")]
        DeliverRescissionTo,

        [XmlEnum("DocumentCustodian")]
        DocumentCustodian,

		/// <summary>
		/// Controller of the enote in the e mortgage registry.
		/// </summary>
        [XmlEnum("ENoteController")]
        ENoteController,

		/// <summary>
		/// The party to whom control is being transferred.  
		/// </summary>
        [XmlEnum("ENoteControllerTransferee")]
        ENoteControllerTransferee,

		/// <summary>
		/// A member of the eRegistry that maintains the Authoritative Copy of the eNote either as Controller or as a custodian on behalf of the Controller. 
		/// </summary>
        [XmlEnum("ENoteCustodian")]
        ENoteCustodian,

		/// <summary>
		/// The party to whom custody  is being transferred.
		/// </summary>
        [XmlEnum("ENoteCustodianTransferee")]
        ENoteCustodianTransferee,

		/// <summary>
		/// A member of the eRegistry that is authorized by the Controller to perform certain transfer  transactions on behalf of the Controller.
		/// </summary>
        [XmlEnum("ENoteDelegateeForTransfers")]
        ENoteDelegateeForTransfers,

		/// <summary>
		/// The party initiating the registration transaction.   
		/// </summary>
        [XmlEnum("ENoteRegisteringParty")]
        ENoteRegisteringParty,

		/// <summary>
		/// A member of the eRegistry that is authorized by the Controller to perform certain MERS eRegistry transactions on the Controller's behalf. The Servicer on an eNote record can submit Change Data and Change Status transactions.
		/// </summary>
        [XmlEnum("ENoteServicer")]
        ENoteServicer,

		/// <summary>
		/// The party to whom servicing  is being transferred.
		/// </summary>
        [XmlEnum("ENoteServicerTransferee")]
        ENoteServicerTransferee,

		/// <summary>
		/// The party initiating the transfer transaction.
		/// </summary>
        [XmlEnum("ENoteTransferInitiator")]
        ENoteTransferInitiator,

		/// <summary>
		/// The person or entity named in a will who has the responsibility of carrying out the terms of the will (that is, collecting the will maker's assets, paying the debts, and distributing the remaining assets to the beneficiaries)
		/// </summary>
        [XmlEnum("Executor")]
        Executor,

        [XmlEnum("FHASponsor")]
        FHASponsor,

        [XmlEnum("FloodCertificateProvider")]
        FloodCertificateProvider,

        [XmlEnum("FulfillmentParty")]
        FulfillmentParty,

		/// <summary>
		/// The party who has donated gift(s) that can be used by the loan applicant for down payment or other expenses.
		/// </summary>
        [XmlEnum("GiftDonor")]
        GiftDonor,

		/// <summary>
		/// The party to whom a transfer or conveyance of property is made. The buyer is commonly known as the grantee.
		/// </summary>
        [XmlEnum("Grantee")]
        Grantee,

		/// <summary>
		/// The party who conveys or transfers ownership of property. The seller is commonly known as the grantor.
		/// </summary>
        [XmlEnum("Grantor")]
        Grantor,

        [XmlEnum("HazardInsuranceAgent")]
        HazardInsuranceAgent,

        [XmlEnum("HazardInsuranceCompany")]
        HazardInsuranceCompany,

		/// <summary>
		/// A legal entity that manages the common areas of a planned unit development, cooperative or condominium.
		/// </summary>
        [XmlEnum("HomeownersAssociation")]
        HomeownersAssociation,

		/// <summary>
		/// The party specified as the Settlement Agent on the HUD-1. 
		/// </summary>
        [XmlEnum("HUD1SettlementAgent")]
        HUD1SettlementAgent,

        [XmlEnum("Interviewer")]
        Interviewer,

        [XmlEnum("InterviewerEmployer")]
        InterviewerEmployer,

        [XmlEnum("Investor")]
        Investor,

        [XmlEnum("IRSTaxFormThirdParty")]
        IRSTaxFormThirdParty,

        [XmlEnum("LawFirm")]
        LawFirm,

        [XmlEnum("Lender")]
        Lender,

        [XmlEnum("LenderBranch")]
        LenderBranch,

        [XmlEnum("LienHolder")]
        LienHolder,

        [XmlEnum("LoanDeliveryFilePreparer")]
        LoanDeliveryFilePreparer,

        [XmlEnum("LoanOfficer")]
        LoanOfficer,

        [XmlEnum("LoanOriginationCompany")]
        LoanOriginationCompany,

        [XmlEnum("LoanOriginator")]
        LoanOriginator,

		/// <summary>
		/// The party who  performs clerical or support duties at the direction of and subject to the supervision and instruction of the loan officers
		/// </summary>
        [XmlEnum("LoanProcessor")]
        LoanProcessor,

        [XmlEnum("LoanSeller")]
        LoanSeller,

		/// <summary>
		/// The party who decides whether to approve or reject the loan application based on a comprehensive review of the loan application and the applicants' credit report. 
		/// </summary>
        [XmlEnum("LoanUnderwriter")]
        LoanUnderwriter,

        [XmlEnum("LossPayee")]
        LossPayee,

        [XmlEnum("ManagementCompany")]
        ManagementCompany,

        [XmlEnum("MICompany")]
        MICompany,

        [XmlEnum("MortgageBroker")]
        MortgageBroker,

		/// <summary>
		/// Individual associated with the borrower, usually the spouse, who is not obligated on the loan nor are they a title holder on the subject property. Nevertheless they have certain rights with regard to the loan and disposition of the property.
		/// </summary>
        [XmlEnum("NonTitleSpouse")]
        NonTitleSpouse,

        [XmlEnum("Notary")]
        Notary,

        [XmlEnum("NotePayTo")]
        NotePayTo,

        [XmlEnum("NotePayToRecipient")]
        NotePayToRecipient,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Payee")]
        Payee,

		/// <summary>
		/// The legal entity that provides the insurance for the pool.
		/// </summary>
        [XmlEnum("PoolInsurer")]
        PoolInsurer,

		/// <summary>
		/// Identifies the role of the organization that created the pool.
		/// </summary>
        [XmlEnum("PoolIssuer")]
        PoolIssuer,

		/// <summary>
		/// Identifies the role of the organization to whom the pool was transferred
		/// </summary>
        [XmlEnum("PoolIssuerTransferee")]
        PoolIssuerTransferee,

        [XmlEnum("PreparedBy")]
        PreparedBy,

        [XmlEnum("ProjectDeveloper")]
        ProjectDeveloper,

        [XmlEnum("ProjectManagementAgent")]
        ProjectManagementAgent,

		/// <summary>
		/// The individual or legal entity that will allow access to the property.
		/// </summary>
        [XmlEnum("PropertyAccessContact")]
        PropertyAccessContact,

		/// <summary>
		/// The property owner of public record.
		/// </summary>
        [XmlEnum("PropertyOwner")]
        PropertyOwner,

		/// <summary>
		/// The individual or legal entity that will purchase or has purchased the property, and is not a borrower on the subject loan.
		/// </summary>
        [XmlEnum("PropertyPurchaser")]
        PropertyPurchaser,

        [XmlEnum("PropertySeller")]
        PropertySeller,

        [XmlEnum("RealEstateAgent")]
        RealEstateAgent,

        [XmlEnum("ReceivingParty")]
        ReceivingParty,

        [XmlEnum("RegistryOperator")]
        RegistryOperator,

        [XmlEnum("RegulatoryAgency")]
        RegulatoryAgency,

        [XmlEnum("RequestingParty")]
        RequestingParty,

        [XmlEnum("RespondingParty")]
        RespondingParty,

        [XmlEnum("RespondToParty")]
        RespondToParty,

        [XmlEnum("ReturnTo")]
        ReturnTo,

        [XmlEnum("ReviewAppraiser")]
        ReviewAppraiser,

		/// <summary>
		/// The entity that creates and issues an asset backed security for sale on the open financial market.
		/// </summary>
        [XmlEnum("SecurityIssuer")]
        SecurityIssuer,

        [XmlEnum("ServiceBureau")]
        ServiceBureau,

        [XmlEnum("ServiceProvider")]
        ServiceProvider,

        [XmlEnum("Servicer")]
        Servicer,

        [XmlEnum("ServicerPaymentCollection")]
        ServicerPaymentCollection,

		/// <summary>
		/// The party who establishes the trust and transfers the original assets, such as the subject property, into the trust for the trustee to hold and manage on behalf of the trust. Also referred to as the trustor or grantor of the trust.
		/// </summary>
        [XmlEnum("Settlor")]
        Settlor,

        [XmlEnum("Spouse")]
        Spouse,

        [XmlEnum("SubmittingParty")]
        SubmittingParty,

        [XmlEnum("TaxableParty")]
        TaxableParty,

        [XmlEnum("Taxpayer")]
        Taxpayer,

		/// <summary>
		/// Aggregator for all taxing authorities associated with the property.
		/// </summary>
        [XmlEnum("TaxServiceProvider")]
        TaxServiceProvider,

        [XmlEnum("TaxServicer")]
        TaxServicer,

        [XmlEnum("ThirdPartyInvestor")]
        ThirdPartyInvestor,

        [XmlEnum("ThirdPartyOriginator")]
        ThirdPartyOriginator,

		/// <summary>
		/// The legal entity which is responsible for issuing the title insurance policy on behalf of the title underwriter.  This entity may be an individual title agent, a corporate title agency, or a direct operation of a title underwriter.
		/// </summary>
        [XmlEnum("TitleCompany")]
        TitleCompany,

        [XmlEnum("TitleHolder")]
        TitleHolder,

		/// <summary>
		/// The legal entity which is the insurance underwriting company assuming coverage risk under the title insurance policy.
		/// </summary>
        [XmlEnum("TitleUnderwriter")]
        TitleUnderwriter,

        [XmlEnum("Trust")]
        Trust,

        [XmlEnum("TrustBeneficiary")]
        TrustBeneficiary,

		/// <summary>
		/// The party selected by the beneficiary of the Deed of Trust or the settlor/grantor/trustor/ of a trust to oversee and manage the assets held in trust for the benefit of the named beneficiaries. An individual or a financial institution can serve as trustee.
		/// </summary>
        [XmlEnum("Trustee")]
        Trustee,

        [XmlEnum("WarehouseLender")]
        WarehouseLender,

        [XmlEnum("Witness")]
        Witness,

    }
}
