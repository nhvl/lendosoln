namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Not full market
    /// </summary>
    public enum FundsBase
    {
		/// <summary>
		/// A catchall value to account for a blank field in LQB.
		/// </summary>
        [XmlEnum("")]
		Blank,

        [XmlEnum("BridgeLoan")]
        BridgeLoan,

        [XmlEnum("CashOnHand")]
        CashOnHand,

        [XmlEnum("CashOrOtherEquity")]
        CashOrOtherEquity,

        [XmlEnum("CheckingSavings")]
        CheckingSavings,

        [XmlEnum("Contribution")]
        Contribution,

        [XmlEnum("CreditCard")]
        CreditCard,

		/// <summary>
		/// Amount of money deposited with the property seller or property seller's agent at the time the sales contract is accepted to perfect the sales contract.
		/// </summary>
        [XmlEnum("DepositOnSalesContract")]
        DepositOnSalesContract,

        [XmlEnum("EquityOnPendingSale")]
        EquityOnPendingSale,

        [XmlEnum("EquityOnSoldProperty")]
        EquityOnSoldProperty,

        [XmlEnum("EquityOnSubjectProperty")]
        EquityOnSubjectProperty,

		/// <summary>
		/// Amount of any excess deposit retained by the seller at the time of real estate closing. Includes (a) amounts held by seller's real estate broker or other third party (who is not the settlement agent) which exceed the fee or commission owed to that party if the party will provide the excess deposit directly to the seller rather than through the closing agent, and (b) If the deposit or any portion has been distributed to the seller prior to closing, the amount not distributed to seller.
		/// </summary>
        [XmlEnum("ExcessDeposit")]
        ExcessDeposit,

        [XmlEnum("ForgivableSecuredLoan")]
        ForgivableSecuredLoan,

        [XmlEnum("GiftFunds")]
        GiftFunds,

        [XmlEnum("Grant")]
        Grant,

        [XmlEnum("HousingRelocation")]
        HousingRelocation,

        [XmlEnum("LifeInsuranceCashValue")]
        LifeInsuranceCashValue,

        [XmlEnum("LotEquity")]
        LotEquity,

        [XmlEnum("MortgageCreditCertificates")]
        MortgageCreditCertificates,

        [XmlEnum("MortgageRevenueBond")]
        MortgageRevenueBond,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("OtherEquity")]
        OtherEquity,

        [XmlEnum("PledgedCollateral")]
        PledgedCollateral,

        [XmlEnum("PremiumFunds")]
        PremiumFunds,

        [XmlEnum("RentWithOptionToPurchase")]
        RentWithOptionToPurchase,

        [XmlEnum("RetirementFunds")]
        RetirementFunds,

        [XmlEnum("SaleOfChattel")]
        SaleOfChattel,

		/// <summary>
		/// Not full market
		/// </summary>
        [XmlEnum("SalesPriceAdjustment")]
        SalesPriceAdjustment,

        [XmlEnum("SecondaryFinancing")]
        SecondaryFinancing,

        [XmlEnum("SecuredLoan")]
        SecuredLoan,

        [XmlEnum("StocksAndBonds")]
        StocksAndBonds,

        [XmlEnum("SweatEquity")]
        SweatEquity,

        [XmlEnum("TradeEquity")]
        TradeEquity,

        [XmlEnum("TrustFunds")]
        TrustFunds,

        [XmlEnum("UnsecuredBorrowedFunds")]
        UnsecuredBorrowedFunds,

    }
}
