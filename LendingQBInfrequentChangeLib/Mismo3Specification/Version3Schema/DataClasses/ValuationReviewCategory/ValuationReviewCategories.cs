namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class VALUATION_REVIEW_CATEGORIES
    {
        /// <summary>
        /// Gets a value indicating whether the VALUATION_REVIEW_CATEGORIES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ValuationReviewCategorySpecified;
            }
        }

        /// <summary>
        /// A collection of valuation review categories.
        /// </summary>
        [XmlElement("VALUATION_REVIEW_CATEGORY", Order = 0)]
		public List<VALUATION_REVIEW_CATEGORY> ValuationReviewCategory = new List<VALUATION_REVIEW_CATEGORY>();

        /// <summary>
        /// Gets or sets a value indicating whether the ValuationReviewCategory element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ValuationReviewCategory element has been assigned a value.</value>
        [XmlIgnore]
        public bool ValuationReviewCategorySpecified
        {
            get { return this.ValuationReviewCategory != null && this.ValuationReviewCategory.Count(v => v != null && v.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public VALUATION_REVIEW_CATEGORIES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
