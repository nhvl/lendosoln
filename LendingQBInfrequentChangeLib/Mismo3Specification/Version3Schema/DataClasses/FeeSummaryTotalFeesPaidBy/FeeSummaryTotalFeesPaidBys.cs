namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class FEE_SUMMARY_TOTAL_FEES_PAID_BYS
    {
        /// <summary>
        /// Gets a value indicating whether the FEE_SUMMARY_TOTAL_FEES_PAID_BYS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.FeeSummaryTotalFeesPaidBySpecified;
            }
        }

        /// <summary>
        /// A collection of fee summary total fees paid by.
        /// </summary>
        [XmlElement("FEE_SUMMARY_TOTAL_FEES_PAID_BY", Order = 0)]
        public List<FEE_SUMMARY_TOTAL_FEES_PAID_BY> FeeSummaryTotalFeesPaidBy = new List<FEE_SUMMARY_TOTAL_FEES_PAID_BY>();

        /// <summary>
        /// Gets or sets a value indicating whether the FeeSummaryTotalFeesPaidBy element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeeSummaryTotalFeesPaidBy element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeeSummaryTotalFeesPaidBySpecified
        {
            get { return this.FeeSummaryTotalFeesPaidBy != null && this.FeeSummaryTotalFeesPaidBy.Count(f => f != null && f.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public FEE_SUMMARY_TOTAL_FEES_PAID_BYS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
