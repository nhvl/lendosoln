namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class FEE_SUMMARY_TOTAL_FEES_PAID_BY
    {
        /// <summary>
        /// Gets a value indicating whether the FEE_SUMMARY_TOTAL_FEES_PAID_BY container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.FeeSummaryTotalFeesPaidByAmountSpecified
                    || this.FeeSummaryTotalFeesPaidByTypeOtherDescriptionSpecified
                    || this.FeeSummaryTotalFeesPaidByTypeSpecified;
            }
        }

        /// <summary>
        /// The total amount of fees paid by a party in the transaction.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount FeeSummaryTotalFeesPaidByAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the FeeSummaryTotalFeesPaidByAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeeSummaryTotalFeesPaidByAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeeSummaryTotalFeesPaidByAmountSpecified
        {
            get { return FeeSummaryTotalFeesPaidByAmount != null; }
            set { }
        }

        /// <summary>
        /// A party to the transaction by whom a fee or fees have been designated paid.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<FeeSummaryTotalFeesPaidByBase> FeeSummaryTotalFeesPaidByType;

        /// <summary>
        /// Gets or sets a value indicating whether the FeeSummaryTotalFeesPaidByType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeeSummaryTotalFeesPaidByType element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeeSummaryTotalFeesPaidByTypeSpecified
        {
            get { return this.FeeSummaryTotalFeesPaidByType != null && this.FeeSummaryTotalFeesPaidByType.enumValue != FeeSummaryTotalFeesPaidByBase.Blank; }
            set { }
        }

        /// <summary>
        /// A description of the party who fees were paid by when Other or Third Party are selected in Fee Summary Total Fees Paid By Type.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString FeeSummaryTotalFeesPaidByTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the FeeSummaryTotalFeesPaidByTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeeSummaryTotalFeesPaidByTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeeSummaryTotalFeesPaidByTypeOtherDescriptionSpecified
        {
            get { return FeeSummaryTotalFeesPaidByTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public FEE_SUMMARY_TOTAL_FEES_PAID_BY_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
