namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class LEGAL_ENTITY
    {
        /// <summary>
        /// Gets a value indicating whether the LEGAL_ENTITY container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AliasesSpecified
                    || this.ContactsSpecified
                    || this.ExtensionSpecified
                    || this.LegalEntityDetailSpecified;
            }
        }

        /// <summary>
        /// Aliases related to the legal entity.
        /// </summary>
        [XmlElement("ALIASES", Order = 0)]
        public ALIASES Aliases;

        /// <summary>
        /// Gets or sets a value indicating whether the Aliases element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Aliases element has been assigned a value.</value>
        [XmlIgnore]
        public bool AliasesSpecified
        {
            get { return this.Aliases != null && this.Aliases.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Contacts of the legal entity.
        /// </summary>
        [XmlElement("CONTACTS", Order = 1)]
        public CONTACTS Contacts;

        /// <summary>
        /// Gets or sets a value indicating whether the Contacts element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Contacts element has been assigned a value.</value>
        [XmlIgnore]
        public bool ContactsSpecified
        {
            get { return this.Contacts != null && this.Contacts.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details about the legal entity.
        /// </summary>
        [XmlElement("LEGAL_ENTITY_DETAIL", Order = 2)]
        public LEGAL_ENTITY_DETAIL LegalEntityDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the LegalEntityDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LegalEntityDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool LegalEntityDetailSpecified
        {
            get { return this.LegalEntityDetail != null && this.LegalEntityDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public LEGAL_ENTITY_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
