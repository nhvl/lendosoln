namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class LEGAL_ENTITY_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the LEGAL_ENTITY_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.EntityHomePageURLSpecified
                    || this.ExtensionSpecified
                    || this.FullNameSpecified
                    || this.GlobalLegalEntityIdentifierSpecified
                    || this.LegalEntityLicensingTypeDescriptionSpecified
                    || this.LegalEntityOrganizedUnderTheLawsOfJurisdictionNameSpecified
                    || this.LegalEntitySuccessorClauseTextDescriptionSpecified
                    || this.LegalEntityTypeOtherDescriptionSpecified
                    || this.LegalEntityTypeSpecified
                    || this.MERSOrganizationIdentifierSpecified
                    || this.RegisteredDomainNameURISpecified
                    || this.RequiredSignatoryCountSpecified;
            }
        }

        /// <summary>
        /// One of the many Internet resource locations that the legal entity entity has chosen to be its home page.  The EntityHomePageURL should be in URL format.  When one browses to that location one expected to find a real resource page.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOURL EntityHomePageURL;

        /// <summary>
        /// Gets or sets a value indicating whether the EntityHomePageURL element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EntityHomePageURL element has been assigned a value.</value>
        [XmlIgnore]
        public bool EntityHomePageURLSpecified
        {
            get { return EntityHomePageURL != null; }
            set { }
        }

        /// <summary>
        /// The unparsed name of either an individual or a legal entity.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString FullName;

        /// <summary>
        /// Gets or sets a value indicating whether the FullName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FullName element has been assigned a value.</value>
        [XmlIgnore]
        public bool FullNameSpecified
        {
            get { return FullName != null; }
            set { }
        }

        /// <summary>
        /// The ISO 17442 formatted 20 character string that uniquely identifies a legal entity.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOIdentifier GlobalLegalEntityIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the GlobalLegalEntityIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GlobalLegalEntityIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool GlobalLegalEntityIdentifierSpecified
        {
            get { return GlobalLegalEntityIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to indicate the manner in which the organization is authorized to make or broker loans, e.g., Mortgage Banker, Mortgage Broker, Licensed Lender.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString LegalEntityLicensingTypeDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the LegalEntityLicensingTypeDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LegalEntityLicensingTypeDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool LegalEntityLicensingTypeDescriptionSpecified
        {
            get { return LegalEntityLicensingTypeDescription != null; }
            set { }
        }

        /// <summary>
        /// The name and type of jurisdiction under which the party as an entity is organized and under whose authority the party as an entity operates.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString LegalEntityOrganizedUnderTheLawsOfJurisdictionName;

        /// <summary>
        /// Gets or sets a value indicating whether the LegalEntityOrganizedUnderTheLawsOfJurisdictionName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LegalEntityOrganizedUnderTheLawsOfJurisdictionName element has been assigned a value.</value>
        [XmlIgnore]
        public bool LegalEntityOrganizedUnderTheLawsOfJurisdictionNameSpecified
        {
            get { return LegalEntityOrganizedUnderTheLawsOfJurisdictionName != null; }
            set { }
        }

        /// <summary>
        /// The description used as the Successor Clause for the legal entity, i. e. Its successors and/or assigns.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString LegalEntitySuccessorClauseTextDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the LegalEntitySuccessorClauseTextDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LegalEntitySuccessorClauseTextDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool LegalEntitySuccessorClauseTextDescriptionSpecified
        {
            get { return LegalEntitySuccessorClauseTextDescription != null; }
            set { }
        }

        /// <summary>
        /// The description of the entity type of the party or organization.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOEnum<LegalEntityBase> LegalEntityType;

        /// <summary>
        /// Gets or sets a value indicating whether the LegalEntityType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LegalEntityType element has been assigned a value.</value>
        [XmlIgnore]
        public bool LegalEntityTypeSpecified
        {
            get { return this.LegalEntityType != null && this.LegalEntityType.enumValue != LegalEntityBase.Blank; }
            set { }
        }

        /// <summary>
        /// The description of the Legal Entity Type when Other is selected as the option from the enumerated list.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOString LegalEntityTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the LegalEntityTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LegalEntityTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool LegalEntityTypeOtherDescriptionSpecified
        {
            get { return LegalEntityTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The identification number assigned by MERS to the organization.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOIdentifier MERSOrganizationIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the MERSOrganizationIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MERSOrganizationIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool MERSOrganizationIdentifierSpecified
        {
            get { return MERSOrganizationIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A domain name is an identification label that defines a realm of administrative autonomy, authority, or control in the Internet, based on the Domain Name System (DNS). The Internet Corporation for Assigned Names and Numbers (ICANN) has overall responsibility for managing the DNS. Domain names are often seen in analogy to real estate in that (1) domain names are foundations on which a website (like a house or commercial building) can be built and (2) the highest "quality" domain names, like sought-after real estate, tend to carry significant value, usually due to their online brand-building potential, use in advertising, search engine optimization, and many other criteria. Because Domain names are registered it provides authoritative identification of the legal entity. the URI can be in URN or URL format.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOURI RegisteredDomainNameURI;

        /// <summary>
        /// Gets or sets a value indicating whether the RegisteredDomainNameURI element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RegisteredDomainNameURI element has been assigned a value.</value>
        [XmlIgnore]
        public bool RegisteredDomainNameURISpecified
        {
            get { return RegisteredDomainNameURI != null; }
            set { }
        }

        /// <summary>
        /// The number of authorized representatives required to sign documents on behalf of the legal entity. To be used in conjunction with Signatory Role Type. If the rule varies per document, this information must be provided under DOCUMENT/DEAL_SETS.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOCount RequiredSignatoryCount;

        /// <summary>
        /// Gets or sets a value indicating whether the RequiredSignatoryCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RequiredSignatoryCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool RequiredSignatoryCountSpecified
        {
            get { return RequiredSignatoryCount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 11)]
        public LEGAL_ENTITY_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
