namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class NOTE_RIDERS
    {
        /// <summary>
        /// Gets a value indicating whether the NOTE_RIDERS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.NoteRiderSpecified;
            }
        }

        /// <summary>
        /// A collection of note riders.
        /// </summary>
        [XmlElement("NOTE_RIDER", Order = 0)]
		public List<NOTE_RIDER> NoteRider = new List<NOTE_RIDER>();

        /// <summary>
        /// Gets or sets a value indicating whether the NoteRider element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NoteRider element has been assigned a value.</value>
        [XmlIgnore]
        public bool NoteRiderSpecified
        {
            get { return this.NoteRider != null && this.NoteRider.Count(n => n != null && n.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public NOTE_RIDERS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
