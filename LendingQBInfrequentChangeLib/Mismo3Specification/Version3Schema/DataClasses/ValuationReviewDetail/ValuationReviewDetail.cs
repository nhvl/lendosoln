namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class VALUATION_REVIEW_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the VALUATION_REVIEW_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AppraisalIdentifierSpecified
                    || this.AppraisalReviewDisagreementDescriptionSpecified
                    || this.AppraisalReviewExtraordinaryAssumptionsDescriptionSpecified
                    || this.AppraisalReviewImprovementsCompletedIndicatorSpecified
                    || this.AppraisalReviewMarketValueChangeCommentDescriptionSpecified
                    || this.AppraisalReviewOriginalAppraisalEffectiveDateSpecified
                    || this.AppraisalReviewOriginalAppraisedValueAccurateIndicatorSpecified
                    || this.AppraisalReviewOriginalAppraisedValueAmountSpecified
                    || this.AppraisalReviewValueConclusionCommentDescriptionSpecified
                    || this.ExtensionSpecified
                    || this.PropertyMarketValueDecreasedIndicatorSpecified;
            }
        }

        /// <summary>
        /// A unique identifier assigned by a party for all appraisal data delivered to the party for this loan. The party assigning the identifier can be provided using the IdentifierOwnerURI associated with a MISMO identifier.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIdentifier AppraisalIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the AppraisalIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AppraisalIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool AppraisalIdentifierSpecified
        {
            get { return AppraisalIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe or comment on the reasons for disagreement with the original opinion of market value in the appraisal report under review.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString AppraisalReviewDisagreementDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the AppraisalReviewDisagreementDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AppraisalReviewDisagreementDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool AppraisalReviewDisagreementDescriptionSpecified
        {
            get { return AppraisalReviewDisagreementDescription != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe or comment on the extraordinary assumptions used in the appraisal review process.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString AppraisalReviewExtraordinaryAssumptionsDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the AppraisalReviewExtraordinaryAssumptionsDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AppraisalReviewExtraordinaryAssumptionsDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool AppraisalReviewExtraordinaryAssumptionsDescriptionSpecified
        {
            get { return AppraisalReviewExtraordinaryAssumptionsDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether the improvements specified in the original appraisal have been completed.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOIndicator AppraisalReviewImprovementsCompletedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the AppraisalReviewImprovementsCompletedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AppraisalReviewImprovementsCompletedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool AppraisalReviewImprovementsCompletedIndicatorSpecified
        {
            get { return AppraisalReviewImprovementsCompletedIndicator != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to comment and describe the changes in the market value of the property between the original appraisal and the review of the appraisal.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString AppraisalReviewMarketValueChangeCommentDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the AppraisalReviewMarketValueChangeCommentDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AppraisalReviewMarketValueChangeCommentDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool AppraisalReviewMarketValueChangeCommentDescriptionSpecified
        {
            get { return AppraisalReviewMarketValueChangeCommentDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the original appraisal effective date and distinguishes it from all other effective dates during an appraisal review or appraisal update.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMODate AppraisalReviewOriginalAppraisalEffectiveDate;

        /// <summary>
        /// Gets or sets a value indicating whether the AppraisalReviewOriginalAppraisalEffectiveDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AppraisalReviewOriginalAppraisalEffectiveDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool AppraisalReviewOriginalAppraisalEffectiveDateSpecified
        {
            get { return AppraisalReviewOriginalAppraisalEffectiveDate != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether the original appraised value is accurate.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOIndicator AppraisalReviewOriginalAppraisedValueAccurateIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the AppraisalReviewOriginalAppraisedValueAccurateIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AppraisalReviewOriginalAppraisedValueAccurateIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool AppraisalReviewOriginalAppraisedValueAccurateIndicatorSpecified
        {
            get { return AppraisalReviewOriginalAppraisedValueAccurateIndicator != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of the original property valuation. This field distinguishes it from a new valuation amount determined during an appraisal review or appraisal update.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOAmount AppraisalReviewOriginalAppraisedValueAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the AppraisalReviewOriginalAppraisedValueAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AppraisalReviewOriginalAppraisedValueAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool AppraisalReviewOriginalAppraisedValueAmountSpecified
        {
            get { return AppraisalReviewOriginalAppraisedValueAmount != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe or summarize the reasons why the new data supports a different market value than was reported in the original appraisal report.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOString AppraisalReviewValueConclusionCommentDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the AppraisalReviewValueConclusionCommentDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AppraisalReviewValueConclusionCommentDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool AppraisalReviewValueConclusionCommentDescriptionSpecified
        {
            get { return AppraisalReviewValueConclusionCommentDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether the market value of a property has decreased. Generally used when updating a previous appraisal on the same property.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOIndicator PropertyMarketValueDecreasedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyMarketValueDecreasedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyMarketValueDecreasedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyMarketValueDecreasedIndicatorSpecified
        {
            get { return PropertyMarketValueDecreasedIndicator != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 10)]
        public VALUATION_REVIEW_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
