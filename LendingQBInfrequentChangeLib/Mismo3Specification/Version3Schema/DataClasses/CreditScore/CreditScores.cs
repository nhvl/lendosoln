namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CREDIT_SCORES
    {
        /// <summary>
        /// Gets a value indicating whether the CREDIT_SCORES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditScoreSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of credit scores.
        /// </summary>
        [XmlElement("CREDIT_SCORE", Order = 0)]
		public List<CREDIT_SCORE> CreditScore = new List<CREDIT_SCORE>();

        /// <summary>
        /// Gets or sets a value indicating whether the CreditScore element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditScore element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditScoreSpecified
        {
            get { return this.CreditScore != null && this.CreditScore.Count(c => c != null && c.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public CREDIT_SCORES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
