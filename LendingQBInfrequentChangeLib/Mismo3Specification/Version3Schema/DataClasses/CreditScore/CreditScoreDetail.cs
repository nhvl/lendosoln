namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CREDIT_SCORE_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the CREDIT_SCORE_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditReportIdentifierSpecified
                    || this.CreditReportTypeOtherDescriptionSpecified
                    || this.CreditReportTypeSpecified
                    || this.CreditRepositorySingleSourceIndicatorSpecified
                    || this.CreditRepositorySourceIndicatorSpecified
                    || this.CreditRepositorySourceTypeOtherDescriptionSpecified
                    || this.CreditRepositorySourceTypeSpecified
                    || this.CreditScoreCategoryTypeOtherDescriptionSpecified
                    || this.CreditScoreCategoryTypeSpecified
                    || this.CreditScoreDateSpecified
                    || this.CreditScoreExclusionReasonTypeSpecified
                    || this.CreditScoreFACTAInquiriesIndicatorSpecified
                    || this.CreditScoreImpairmentTypeOtherDescriptionSpecified
                    || this.CreditScoreImpairmentTypeSpecified
                    || this.CreditScoreModelNameTypeOtherDescriptionSpecified
                    || this.CreditScoreModelNameTypeSpecified
                    || this.CreditScoreRankPercentileDescriptionSpecified
                    || this.CreditScoreRankPercentileValueSpecified
                    || this.CreditScoreValueSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A reference number assigned by the credit bureau to a specific credit report. This report number is also referenced when a Reissue, Upgrade, or Status Query of an existing report is requested.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIdentifier CreditReportIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditReportIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditReportIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditReportIdentifierSpecified
        {
            get { return CreditReportIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The type of credit report that was requested or produced. Most common types are Merged report (data from credit data repositories is merged), and RMCR (Residential Mortgage Credit Report - contains verified liabilities, employment, etc.).
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<CreditReportBase> CreditReportType;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditReportType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditReportType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditReportTypeSpecified
        {
            get { return this.CreditReportType != null && this.CreditReportType.enumValue != CreditReportBase.Blank; }
            set { }
        }

        /// <summary>
        /// When Credit Report Type is set to Other, enter its description in this element.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString CreditReportTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditReportTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditReportTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditReportTypeOtherDescriptionSpecified
        {
            get { return CreditReportTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// If true, indicates the credit report was  based on the input from a single credit repository. Also know as a single in-file credit report.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOIndicator CreditRepositorySingleSourceIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditRepositorySingleSourceIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditRepositorySingleSourceIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditRepositorySingleSourceIndicatorSpecified
        {
            get { return CreditRepositorySingleSourceIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether a Credit Repository Source is available. If a Credit Repository Source is available then the source can be provided in Credit Repository Source Type.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOIndicator CreditRepositorySourceIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditRepositorySourceIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditRepositorySourceIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditRepositorySourceIndicatorSpecified
        {
            get { return CreditRepositorySourceIndicator != null; }
            set { }
        }

        /// <summary>
        /// This element describes the source of the credit file, Equifax, Experian, Trans Union or Unspecified if the specific sources are not specified.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOEnum<CreditRepositorySourceBase> CreditRepositorySourceType;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditRepositorySourceType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditRepositorySourceType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditRepositorySourceTypeSpecified
        {
            get { return this.CreditRepositorySourceType != null && this.CreditRepositorySourceType.enumValue != CreditRepositorySourceBase.Blank; }
            set { }
        }

        /// <summary>
        /// When Credit Repository Source Type is set to Other, enter its value in this data element.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOString CreditRepositorySourceTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditRepositorySourceTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditRepositorySourceTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditRepositorySourceTypeOtherDescriptionSpecified
        {
            get { return CreditRepositorySourceTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// An enumerated list that describes the general type or purpose of a credit score model. This allows LOS systems to extract scores by a more general category, without having to know the specific score model names.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOEnum<CreditScoreCategoryBase> CreditScoreCategoryType;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditScoreCategoryType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditScoreCategoryType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditScoreCategoryTypeSpecified
        {
            get { return this.CreditScoreCategoryType != null && this.CreditScoreCategoryType.enumValue != CreditScoreCategoryBase.Blank; }
            set { }
        }

        /// <summary>
        /// When Credit Score Category Type is set to "Other", enter its value in this data element.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOString CreditScoreCategoryTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditScoreCategoryTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditScoreCategoryTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditScoreCategoryTypeOtherDescriptionSpecified
        {
            get { return CreditScoreCategoryTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Date that the referenced credit score was produced.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMODate CreditScoreDate;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditScoreDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditScoreDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditScoreDateSpecified
        {
            get { return CreditScoreDate != null; }
            set { }
        }

        /// <summary>
        /// An enumerated list of reasons why a credit score could not be generated.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOEnum<CreditScoreExclusionReasonBase> CreditScoreExclusionReasonType;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditScoreExclusionReasonType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditScoreExclusionReasonType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditScoreExclusionReasonTypeSpecified
        {
            get { return this.CreditScoreExclusionReasonType != null && this.CreditScoreExclusionReasonType.enumValue != CreditScoreExclusionReasonBase.Blank; }
            set { }
        }

        /// <summary>
        /// This indicator is set to True when the Credit Risk Score Value of the borrower  was negatively affected by the presence of credit inquiry records on their credit report. There may be FACT Act compliance requirements related to this alert message.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOIndicator CreditScoreFACTAInquiriesIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditScoreFACTAInquiriesIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditScoreFACTAInquiriesIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditScoreFACTAInquiriesIndicatorSpecified
        {
            get { return CreditScoreFACTAInquiriesIndicator != null; }
            set { }
        }

        /// <summary>
        /// Identifies a characteristic of the Credit Score that impairs its effectiveness as an indicator of credit risk.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOEnum<CreditScoreImpairmentBase> CreditScoreImpairmentType;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditScoreImpairmentType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditScoreImpairmentType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditScoreImpairmentTypeSpecified
        {
            get { return this.CreditScoreImpairmentType != null && this.CreditScoreImpairmentType.enumValue != CreditScoreImpairmentBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Credit Score Impairment Type.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOString CreditScoreImpairmentTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditScoreImpairmentTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditScoreImpairmentTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditScoreImpairmentTypeOtherDescriptionSpecified
        {
            get { return CreditScoreImpairmentTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Identifies the score algorithm model name used to produce the referenced credit risk score.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOEnum<CreditScoreModelNameBase> CreditScoreModelNameType;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditScoreModelNameType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditScoreModelNameType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditScoreModelNameTypeSpecified
        {
            get { return this.CreditScoreModelNameType != null && this.CreditScoreModelNameType.enumValue != CreditScoreModelNameBase.Blank; }
            set { }
        }

        /// <summary>
        /// When the Credit Score Model Name Type is set to Other, this element holds the description.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOString CreditScoreModelNameTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditScoreModelNameTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditScoreModelNameTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditScoreModelNameTypeOtherDescriptionSpecified
        {
            get { return CreditScoreModelNameTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A detailed description of how the credit score compares to the scores of other consumers.
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMOString CreditScoreRankPercentileDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditScoreRankPercentileDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditScoreRankPercentileDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditScoreRankPercentileDescriptionSpecified
        {
            get { return CreditScoreRankPercentileDescription != null; }
            set { }
        }

        /// <summary>
        /// Value of a credit score rank percentile.
        /// </summary>
        [XmlElement(Order = 17)]
        public MISMOValue CreditScoreRankPercentileValue;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditScoreRankPercentileValue element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditScoreRankPercentileValue element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditScoreRankPercentileValueSpecified
        {
            get { return CreditScoreRankPercentileValue != null; }
            set { }
        }

        /// <summary>
        /// Value of the credit score.
        /// </summary>
        [XmlElement(Order = 18)]
        public MISMOValue CreditScoreValue;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditScoreValue element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditScoreValue element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditScoreValueSpecified
        {
            get { return CreditScoreValue != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 19)]
        public CREDIT_SCORE_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
