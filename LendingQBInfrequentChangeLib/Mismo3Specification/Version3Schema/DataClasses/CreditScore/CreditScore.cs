namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class CREDIT_SCORE
    {
        /// <summary>
        /// Gets a value indicating whether the CREDIT_SCORE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditScoreDetailSpecified
                    || this.CreditScoreFactorsSpecified
                    || this.CreditScoreProviderSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// Details on a credit score.
        /// </summary>
        [XmlElement("CREDIT_SCORE_DETAIL", Order = 0)]
        public CREDIT_SCORE_DETAIL CreditScoreDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditScoreDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditScoreDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditScoreDetailSpecified
        {
            get { return this.CreditScoreDetail != null && this.CreditScoreDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Factors in a credit score.
        /// </summary>
        [XmlElement("CREDIT_SCORE_FACTORS", Order = 1)]
        public CREDIT_SCORE_FACTORS CreditScoreFactors;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditScoreFactors element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditScoreFactors element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditScoreFactorsSpecified
        {
            get { return this.CreditScoreFactors != null && this.CreditScoreFactors.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A provider of credit scores.
        /// </summary>
        [XmlElement("CREDIT_SCORE_PROVIDER", Order = 2)]
        public CREDIT_SCORE_PROVIDER CreditScoreProvider;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditScoreProvider element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditScoreProvider element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditScoreProviderSpecified
        {
            get { return this.CreditScoreProvider != null && this.CreditScoreProvider.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public CREDIT_SCORE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
