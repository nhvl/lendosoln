namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class LOAN_STATE
    {
        /// <summary>
        /// Gets a value indicating whether the LOAN_STATE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.LoanStateDateSpecified
                    || this.LoanStateDatetimeSpecified
                    || this.LoanStateTypeSpecified;
            }
        }

        /// <summary>
        /// Specifies the date for the "Loan State Type".
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMODate LoanStateDate;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanStateDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanStateDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanStateDateSpecified
        {
            get { return LoanStateDate != null; }
            set { }
        }

        /// <summary>
        /// Specifies the date and time of the designated Loan State Type.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMODatetime LoanStateDatetime;

        /// <summary>
        /// Gets or sets a value indicating whether the Loan State date/time element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Loan State date/time element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanStateDatetimeSpecified
        {
            get { return LoanStateDatetime != null; }
            set { }
        }

        /// <summary>
        /// Identifies the state in time for the information associated with this occurrence of LOAN.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<LoanStateBase> LoanStateType;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanStateType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanStateType element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanStateTypeSpecified
        {
            get { return this.LoanStateType != null && this.LoanStateType.enumValue != LoanStateBase.Blank; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public LOAN_STATE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
