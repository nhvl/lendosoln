namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class IDENTITY_DOCUMENTATIONS
    {
        /// <summary>
        /// Gets a value indicating whether the IDENTITY_DOCUMENTATIONS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.IdentityDocumentationSpecified;
            }
        }

        /// <summary>
        /// Container for identity documentations.
        /// </summary>
        [XmlElement("IDENTITY_DOCUMENTATION", Order = 0)]
		public List<IDENTITY_DOCUMENTATION> IdentityDocumentation = new List<IDENTITY_DOCUMENTATION>();

        /// <summary>
        /// Gets or sets a value indicating whether the IdentityDocumentation element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IdentityDocumentation element has been assigned a value.</value>
        [XmlIgnore]
        public bool IdentityDocumentationSpecified
        {
            get { return this.IdentityDocumentation != null && this.IdentityDocumentation.Count(i => i != null && i.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public IDENTITY_DOCUMENTATIONS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
