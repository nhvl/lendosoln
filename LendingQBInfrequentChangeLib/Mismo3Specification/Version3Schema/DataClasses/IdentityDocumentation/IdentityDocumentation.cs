namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class IDENTITY_DOCUMENTATION
    {
        /// <summary>
        /// Gets a value indicating whether the IDENTITY_DOCUMENTATION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.IdentityDocumentExpirationDateSpecified
                    || this.IdentityDocumentIdentifierSpecified
                    || this.IdentityDocumentIssuedByCountryNameSpecified
                    || this.IdentityDocumentIssuedByNameSpecified
                    || this.IdentityDocumentIssuedByStateCodeSpecified
                    || this.IdentityDocumentIssuedByStateProvinceNameSpecified
                    || this.IdentityDocumentIssuedByTypeOtherDescriptionSpecified
                    || this.IdentityDocumentIssuedByTypeSpecified
                    || this.IdentityDocumentIssuedDateSpecified
                    || this.IdentityDocumentTypeOtherDescriptionSpecified
                    || this.IdentityDocumentTypeSpecified;
            }
        }

        /// <summary>
        /// The expiration date of the document used for identity verification of the individual.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMODate IdentityDocumentExpirationDate;

        /// <summary>
        /// Gets or sets a value indicating whether the IdentityDocumentExpirationDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IdentityDocumentExpirationDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool IdentityDocumentExpirationDateSpecified
        {
            get { return IdentityDocumentExpirationDate != null; }
            set { }
        }

        /// <summary>
        /// On a particular document or in a particular resource each individual is assigned a unique value. For example a drivers license number or passport number on documents. Another example is the SSN or Taxpayer Identifier with in those external resource systems.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOIdentifier IdentityDocumentIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the IdentityDocumentIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IdentityDocumentIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool IdentityDocumentIdentifierSpecified
        {
            get { return IdentityDocumentIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Country which issued the identification.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString IdentityDocumentIssuedByCountryName;

        /// <summary>
        /// Gets or sets a value indicating whether the IdentityDocumentIssuedByCountryName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IdentityDocumentIssuedByCountryName element has been assigned a value.</value>
        [XmlIgnore]
        public bool IdentityDocumentIssuedByCountryNameSpecified
        {
            get { return IdentityDocumentIssuedByCountryName != null; }
            set { }
        }

        /// <summary>
        /// Agency which issued the identification document.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString IdentityDocumentIssuedByName;

        /// <summary>
        /// Gets or sets a value indicating whether the IdentityDocumentIssuedByName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IdentityDocumentIssuedByName element has been assigned a value.</value>
        [XmlIgnore]
        public bool IdentityDocumentIssuedByNameSpecified
        {
            get { return IdentityDocumentIssuedByName != null; }
            set { }
        }

        /// <summary>
        /// The two-character representation of the US state, US Territory, Canadian Province, Military APO FPO, or Territory in which the identity document was issued.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOCode IdentityDocumentIssuedByStateCode;

        /// <summary>
        /// Gets or sets a value indicating whether the IdentityDocumentIssuedByStateCode element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IdentityDocumentIssuedByStateCode element has been assigned a value.</value>
        [XmlIgnore]
        public bool IdentityDocumentIssuedByStateCodeSpecified
        {
            get { return IdentityDocumentIssuedByStateCode != null; }
            set { }
        }

        /// <summary>
        /// State or province which issued the identification.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString IdentityDocumentIssuedByStateProvinceName;

        /// <summary>
        /// Gets or sets a value indicating whether the IdentityDocumentIssuedByStateProvinceName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IdentityDocumentIssuedByStateProvinceName element has been assigned a value.</value>
        [XmlIgnore]
        public bool IdentityDocumentIssuedByStateProvinceNameSpecified
        {
            get { return IdentityDocumentIssuedByStateProvinceName != null; }
            set { }
        }

        /// <summary>
        /// The type of entity that issued the document used for identity verification for this individual.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOEnum<IdentityDocumentIssuedByBase> IdentityDocumentIssuedByType;

        /// <summary>
        /// Gets or sets a value indicating whether the IdentityDocumentIssuedByType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IdentityDocumentIssuedByType element has been assigned a value.</value>
        [XmlIgnore]
        public bool IdentityDocumentIssuedByTypeSpecified
        {
            get { return this.IdentityDocumentIssuedByType != null && this.IdentityDocumentIssuedByType.enumValue != IdentityDocumentIssuedByBase.Blank; }
            set { }
        }

        /// <summary>
        /// Expiration date of the document used for identity verification of the individual.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOString IdentityDocumentIssuedByTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the IdentityDocumentIssuedByTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IdentityDocumentIssuedByTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool IdentityDocumentIssuedByTypeOtherDescriptionSpecified
        {
            get { return IdentityDocumentIssuedByTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// For document the date when the document was issued to the individual. For an external resource the date the individual was added to the data store. 
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMODate IdentityDocumentIssuedDate;

        /// <summary>
        /// Gets or sets a value indicating whether the IdentityDocumentIssuedDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IdentityDocumentIssuedDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool IdentityDocumentIssuedDateSpecified
        {
            get { return IdentityDocumentIssuedDate != null; }
            set { }
        }

        /// <summary>
        /// The kind of document that was examined to verify individual identity. A driver's license is an example of an identity document.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOEnum<IdentityDocumentBase> IdentityDocumentType;

        /// <summary>
        /// Gets or sets a value indicating whether the IdentityDocumentType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IdentityDocumentType element has been assigned a value.</value>
        [XmlIgnore]
        public bool IdentityDocumentTypeSpecified
        {
            get { return this.IdentityDocumentType != null && this.IdentityDocumentType.enumValue != IdentityDocumentBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Identity Verification Source Type.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOString IdentityDocumentTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the IdentityDocumentTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IdentityDocumentTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool IdentityDocumentTypeOtherDescriptionSpecified
        {
            get { return IdentityDocumentTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 11)]
        public IDENTITY_DOCUMENTATION_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
