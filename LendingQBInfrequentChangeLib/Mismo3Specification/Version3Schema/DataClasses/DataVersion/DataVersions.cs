namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class DATA_VERSIONS
    {
        /// <summary>
        /// Gets a value indicating whether the DATA_VERSIONS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DataVersionSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of data versions.
        /// </summary>
        [XmlElement("DATA_VERSION", Order = 0)]
		public List<DATA_VERSION> DataVersion = new List<DATA_VERSION>();

        /// <summary>
        /// Gets or sets a value indicating whether the DataVersion element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DataVersion element has been assigned a value.</value>
        [XmlIgnore]
        public bool DataVersionSpecified
        {
            get { return this.DataVersion != null && this.DataVersion.Count(d => d != null && d.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public DATA_VERSIONS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
