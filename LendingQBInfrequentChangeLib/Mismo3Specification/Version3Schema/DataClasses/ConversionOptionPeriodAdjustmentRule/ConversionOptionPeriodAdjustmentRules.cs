namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CONVERSION_OPTION_PERIOD_ADJUSTMENT_RULES
    {
        /// <summary>
        /// Gets a value indicating whether the CONVERSION_OPTION_PERIOD_ADJUSTMENT_RULES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ConversionOptionPeriodAdjustmentRuleSpecified
                    || this.ExtensionSpecified;
            }
        }


        /// <summary>
        /// A collection of conversion option period adjustment rules.
        /// </summary>
        [XmlElement("CONVERSION_OPTION_PERIOD_ADJUSTMENT_RULE", Order = 0)]
		public List<CONVERSION_OPTION_PERIOD_ADJUSTMENT_RULE> ConversionOptionPeriodAdjustmentRule = new List<CONVERSION_OPTION_PERIOD_ADJUSTMENT_RULE>();

        /// <summary>
        /// Gets or sets a value indicating whether the ConversionOptionPeriodAdjustmentRule element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConversionOptionPeriodAdjustmentRule element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConversionOptionPeriodAdjustmentRuleSpecified
        {
            get { return this.ConversionOptionPeriodAdjustmentRule != null && this.ConversionOptionPeriodAdjustmentRule.Count(c => c != null && c.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public CONVERSION_OPTION_PERIOD_ADJUSTMENT_RULES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
