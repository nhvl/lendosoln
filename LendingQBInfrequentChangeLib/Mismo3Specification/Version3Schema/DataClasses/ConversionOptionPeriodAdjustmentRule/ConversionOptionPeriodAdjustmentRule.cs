namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class CONVERSION_OPTION_PERIOD_ADJUSTMENT_RULE
    {
        /// <summary>
        /// Gets a value indicating whether the CONVERSION_OPTION_PERIOD_ADJUSTMENT_RULE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ConversionOptionPeriodAdjustmentEffectiveDateSpecified
                    || this.ConversionOptionPeriodDemandNotificationFrequencyCountSpecified
                    || this.ConversionOptionPeriodExpirationDateSpecified
                    || this.ConversionOptionPeriodFeeAmountSpecified
                    || this.ConversionOptionPeriodFeeBalanceCalculationTypeSpecified
                    || this.ConversionOptionPeriodFeePercentSpecified
                    || this.ConversionOptionPeriodReplyDaysCountSpecified
                    || this.ConversionOptionPeriodRoundingPercentSpecified
                    || this.ConversionOptionPeriodRoundingTypeSpecified
                    || this.ConversionOptionPeriodTypeSpecified
                    || this.ConversionRateCalculationMethodDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }


        /// <summary>
        /// The payment due date when the convertible factors become applicable, also known as the conversion period start date.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMODate ConversionOptionPeriodAdjustmentEffectiveDate;

        /// <summary>
        /// Gets or sets a value indicating whether the ConversionOptionPeriodAdjustmentEffectiveDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConversionOptionPeriodAdjustmentEffectiveDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConversionOptionPeriodAdjustmentEffectiveDateSpecified
        {
            get { return ConversionOptionPeriodAdjustmentEffectiveDate != null; }
            set { }
        }

        /// <summary>
        /// The number of payments between demand conversion option notifications to be mailed as a courtesy to the mortgagor.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOCount ConversionOptionPeriodDemandNotificationFrequencyCount;

        /// <summary>
        /// Gets or sets a value indicating whether the ConversionOptionPeriodDemandNotificationFrequencyCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConversionOptionPeriodDemandNotificationFrequencyCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConversionOptionPeriodDemandNotificationFrequencyCountSpecified
        {
            get { return ConversionOptionPeriodDemandNotificationFrequencyCount != null; }
            set { }
        }

        /// <summary>
        /// The payment due date when the convertible factors expire.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMODate ConversionOptionPeriodExpirationDate;

        /// <summary>
        /// Gets or sets a value indicating whether the ConversionOptionPeriodExpirationDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConversionOptionPeriodExpirationDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConversionOptionPeriodExpirationDateSpecified
        {
            get { return ConversionOptionPeriodExpirationDate != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount that the mortgagor must pay to exercise the conversion option.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOAmount ConversionOptionPeriodFeeAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ConversionOptionPeriodFeeAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConversionOptionPeriodFeeAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConversionOptionPeriodFeeAmountSpecified
        {
            get { return ConversionOptionPeriodFeeAmount != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether the conversion fee is calculated as a percentage of the original principal balance or the principal balance at the time of conversion.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<ConversionOptionPeriodFeeBalanceCalculationBase> ConversionOptionPeriodFeeBalanceCalculationType;

        /// <summary>
        /// Gets or sets a value indicating whether the ConversionOptionPeriodFeeBalanceCalculationType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConversionOptionPeriodFeeBalanceCalculationType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConversionOptionPeriodFeeBalanceCalculationTypeSpecified
        {
            get { return this.ConversionOptionPeriodFeeBalanceCalculationType != null && this.ConversionOptionPeriodFeeBalanceCalculationType.enumValue != ConversionOptionPeriodFeeBalanceCalculationBase.Blank; }
            set { }
        }

        /// <summary>
        /// The percentage of the loans principal balance (either original or at conversion) that the mortgagor must pay to exercise the option to convert.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOPercent ConversionOptionPeriodFeePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the ConversionOptionPeriodFeePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConversionOptionPeriodFeePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConversionOptionPeriodFeePercentSpecified
        {
            get { return ConversionOptionPeriodFeePercent != null; }
            set { }
        }

        /// <summary>
        /// The number of days after the notice is generated that the mortgagor must reply to exercise the option to convert.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOCount ConversionOptionPeriodReplyDaysCount;

        /// <summary>
        /// Gets or sets a value indicating whether the ConversionOptionPeriodReplyDaysCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConversionOptionPeriodReplyDaysCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConversionOptionPeriodReplyDaysCountSpecified
        {
            get { return ConversionOptionPeriodReplyDaysCount != null; }
            set { }
        }

        /// <summary>
        /// The percentage to which the interest rate is rounded when calculating a new interest rate for the option to convert an ARM loan to a fixed rate loan. This field is used in conjunction with Conversion Option Period Rounding Type which indicates how rounding should occur.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOPercent ConversionOptionPeriodRoundingPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the ConversionOptionPeriodRoundingPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConversionOptionPeriodRoundingPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConversionOptionPeriodRoundingPercentSpecified
        {
            get { return ConversionOptionPeriodRoundingPercent != null; }
            set { }
        }

        /// <summary>
        /// Indicates how the Conversion Option Margin Rate Percent is rounded when calculating a new interest rate for the option to convert an ARM loan to a fixed-rate loan. The interest rate can be rounded Up, Down or to the Nearest Rounding Factor. This field is used in conjunction with Conversion Option Period Rounding Percent which indicates the percentage to which the rounding occurs.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOEnum<ConversionOptionPeriodRoundingBase> ConversionOptionPeriodRoundingType;

        /// <summary>
        /// Gets or sets a value indicating whether the ConversionOptionPeriodRoundingType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConversionOptionPeriodRoundingType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConversionOptionPeriodRoundingTypeSpecified
        {
            get { return this.ConversionOptionPeriodRoundingType != null && this.ConversionOptionPeriodRoundingType.enumValue != ConversionOptionPeriodRoundingBase.Blank; }
            set { }
        }

        /// <summary>
        /// Indicates the type of conversion option associated with the loan.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOEnum<ConversionOptionPeriodBase> ConversionOptionPeriodType;

        /// <summary>
        /// Gets or sets a value indicating whether the ConversionOptionPeriodType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConversionOptionPeriodType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConversionOptionPeriodTypeSpecified
        {
            get { return this.ConversionOptionPeriodType != null && this.ConversionOptionPeriodType.enumValue != ConversionOptionPeriodBase.Blank; }
            set { }
        }

        /// <summary>
        /// Describes how interest rate changes are to be calculated when calculating a new interest rate for the option to convert an ARM loan to a fixed rate loan.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOString ConversionRateCalculationMethodDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ConversionRateCalculationMethodDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConversionRateCalculationMethodDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConversionRateCalculationMethodDescriptionSpecified
        {
            get { return ConversionRateCalculationMethodDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 11)]
        public CONVERSION_OPTION_PERIOD_ADJUSTMENT_RULE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
