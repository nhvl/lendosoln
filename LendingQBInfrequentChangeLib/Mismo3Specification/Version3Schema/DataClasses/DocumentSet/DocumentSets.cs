namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class DOCUMENT_SETS
    {
        /// <summary>
        /// Gets a value indicating whether the DOCUMENT_SETS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DocumentSetSpecified
                    || this.ExtensionSpecified
                    || this.ForeignObjectsSpecified;
            }
        }

        /// <summary>
        /// A collection of document sets.
        /// </summary>
        [XmlElement("DOCUMENT_SET", Order = 0)]
		public List<DOCUMENT_SET> DocumentSet = new List<DOCUMENT_SET>();

        /// <summary>
        /// Gets or sets a value indicating whether the DocumentSet element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DocumentSet element has been assigned a value.</value>
        [XmlIgnore]
        public bool DocumentSetSpecified
        {
            get { return this.DocumentSet != null && this.DocumentSet.Count(d => d != null && d.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// Foreign objects related to a document set.
        /// </summary>
        [XmlElement("FOREIGN_OBJECTS", Order = 1)]
        public FOREIGN_OBJECTS ForeignObjects;

        /// <summary>
        /// Gets or sets a value indicating whether the ForeignObjects element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ForeignObjects element has been assigned a value.</value>
        [XmlIgnore]
        public bool ForeignObjectsSpecified
        {
            get { return this.ForeignObjects != null && this.ForeignObjects.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public DOCUMENT_SETS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
