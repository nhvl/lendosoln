namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class DOCUMENT_SET
    {
        /// <summary>
        /// Gets a value indicating whether the DOCUMENT_SET container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DocumentSetIdentifiersSpecified
                    || this.DocumentSetUsagesSpecified
                    || this.DocumentsSpecified
                    || this.ExtensionSpecified
                    || this.ForeignObjectsSpecified;
            }
        }

        /// <summary>
        /// Identifiers for the document set.
        /// </summary>
        [XmlElement("DOCUMENT_SET_IDENTIFIERS", Order = 0)]
        public DOCUMENT_SET_IDENTIFIERS DocumentSetIdentifiers;

        /// <summary>
        /// Gets or sets a value indicating whether the DocumentSetIdentifiers element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DocumentSetIdentifiers element has been assigned a value.</value>
        [XmlIgnore]
        public bool DocumentSetIdentifiersSpecified
        {
            get { return this.DocumentSetIdentifiers != null && this.DocumentSetIdentifiers.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Usages for the document set.
        /// </summary>
        [XmlElement("DOCUMENT_SET_USAGES", Order = 1)]
        public DOCUMENT_SET_USAGES DocumentSetUsages;

        /// <summary>
        /// Gets or sets a value indicating whether the DocumentSetUsages element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DocumentSetUsages element has been assigned a value.</value>
        [XmlIgnore]
        public bool DocumentSetUsagesSpecified
        {
            get { return this.DocumentSetUsages != null && this.DocumentSetUsages.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Documents in the set.
        /// </summary>
        [XmlElement("DOCUMENTS", Order = 2)]
        public DOCUMENTS Documents;

        /// <summary>
        /// Gets or sets a value indicating whether the Documents element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Documents element has been assigned a value.</value>
        [XmlIgnore]
        public bool DocumentsSpecified
        {
            get { return this.Documents != null && this.Documents.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Foreign objects related to a document set.
        /// </summary>
        [XmlElement("FOREIGN_OBJECTS", Order = 3)]
        public FOREIGN_OBJECTS ForeignObjects;

        /// <summary>
        /// Gets or sets a value indicating whether the ForeignObjects element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ForeignObjects element has been assigned a value.</value>
        [XmlIgnore]
        public bool ForeignObjectsSpecified
        {
            get { return this.ForeignObjects != null && this.ForeignObjects.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public DOCUMENT_SET_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
