namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class TRANSFORM_PAGES
    {
        /// <summary>
        /// Gets a value indicating whether the TRANSFORM_PAGES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.TransformPageSpecified;
            }
        }

        /// <summary>
        /// A collection of transform pages.
        /// </summary>
        [XmlElement("TRANSFORM_PAGE", Order = 0)]
		public List<TRANSFORM_PAGE> TransformPage = new List<TRANSFORM_PAGE>();

        /// <summary>
        /// Gets or sets a value indicating whether the TransformPage element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TransformPage element has been assigned a value.</value>
        [XmlIgnore]
        public bool TransformPageSpecified
        {
            get { return this.TransformPage != null && this.TransformPage.Count(t => t != null && t.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public TRANSFORM_PAGES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
