namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class EMPLOYERS
    {
        /// <summary>
        /// Gets a value indicating whether the EMPLOYERS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.EmployerSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of employers.
        /// </summary>
        [XmlElement("EMPLOYER", Order = 0)]
		public List<EMPLOYER> Employer = new List<EMPLOYER>();

        /// <summary>
        /// Gets or sets a value indicating whether the Employer element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Employer element has been assigned a value.</value>
        [XmlIgnore]
        public bool EmployerSpecified
        {
            get { return this.Employer != null && this.Employer.Count(e => e != null && e.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public EMPLOYERS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
