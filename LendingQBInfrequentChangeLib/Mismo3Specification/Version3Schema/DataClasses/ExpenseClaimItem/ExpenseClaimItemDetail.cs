namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class EXPENSE_CLAIM_ITEM_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the EXPENSE_CLAIM_ITEM_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExpenseClaimCategoryIdentifierSpecified
                    || this.ExpenseClaimClassificationIdentifierSpecified
                    || this.ExpenseClaimItemCancellationDateSpecified
                    || this.ExpenseClaimItemCountSpecified
                    || this.ExpenseClaimItemEscrowedIndicatorSpecified
                    || this.ExpenseClaimItemFromDateSpecified
                    || this.ExpenseClaimItemIdentifierSpecified
                    || this.ExpenseClaimItemInsurancePlacedTypeOtherDescriptionSpecified
                    || this.ExpenseClaimItemInsurancePlacedTypeSpecified
                    || this.ExpenseClaimItemToDateSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// Specifies the broad category into which individual Expense Claim Types may be grouped. The party assigning the identifier should be provided using the IdentifierOwnerURI.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIdentifier ExpenseClaimCategoryIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the ExpenseClaimCategoryIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ExpenseClaimCategoryIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExpenseClaimCategoryIdentifierSpecified
        {
            get { return ExpenseClaimCategoryIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Identifies a user-defined classification to which the expense claim line item belongs.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOIdentifier ExpenseClaimClassificationIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the ExpenseClaimClassificationIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ExpenseClaimClassificationIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExpenseClaimClassificationIdentifierSpecified
        {
            get { return ExpenseClaimClassificationIdentifier != null; }
            set { }
        }

        /// <summary>
        /// For maintenance, service, or activities performed over a period of time, the date that it was cancelled.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMODate ExpenseClaimItemCancellationDate;

        /// <summary>
        /// Gets or sets a value indicating whether the ExpenseClaimItemCancellationDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ExpenseClaimItemCancellationDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExpenseClaimItemCancellationDateSpecified
        {
            get { return ExpenseClaimItemCancellationDate != null; }
            set { }
        }

        /// <summary>
        /// The number of times a service or activity was performed or delivered over a range of time.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOCount ExpenseClaimItemCount;

        /// <summary>
        /// Gets or sets a value indicating whether the ExpenseClaimItemCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ExpenseClaimItemCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExpenseClaimItemCountSpecified
        {
            get { return ExpenseClaimItemCount != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that the associated claim expense item is escrowed.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOIndicator ExpenseClaimItemEscrowedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ExpenseClaimItemEscrowedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ExpenseClaimItemEscrowedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExpenseClaimItemEscrowedIndicatorSpecified
        {
            get { return ExpenseClaimItemEscrowedIndicator != null; }
            set { }
        }

        /// <summary>
        /// For maintenance, service, or activities performed over a period of time, the date that it began or will begin.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMODate ExpenseClaimItemFromDate;

        /// <summary>
        /// Gets or sets a value indicating whether the ExpenseClaimItemFromDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ExpenseClaimItemFromDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExpenseClaimItemFromDateSpecified
        {
            get { return ExpenseClaimItemFromDate != null; }
            set { }
        }

        /// <summary>
        /// Specifies the type of maintenance, service or activity that was performed and is being claimed as an expense. The party assigning the identifier should be provided using the IdentifierOwnerURI.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOIdentifier ExpenseClaimItemIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the ExpenseClaimItemIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ExpenseClaimItemIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExpenseClaimItemIdentifierSpecified
        {
            get { return ExpenseClaimItemIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Specifies the party who placed the insurance associated with the expense claim.  
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOEnum<ExpenseClaimItemInsurancePlacedBase> ExpenseClaimItemInsurancePlacedType;

        /// <summary>
        /// Gets or sets a value indicating whether the ExpenseClaimItemInsurancePlacedType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ExpenseClaimItemInsurancePlacedType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExpenseClaimItemInsurancePlacedTypeSpecified
        {
            get { return this.ExpenseClaimItemInsurancePlacedType != null && this.ExpenseClaimItemInsurancePlacedType.enumValue != ExpenseClaimItemInsurancePlacedBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected as the Expense Claim Item Insurance Placed Type.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOString ExpenseClaimItemInsurancePlacedTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ExpenseClaimItemInsurancePlacedTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ExpenseClaimItemInsurancePlacedTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExpenseClaimItemInsurancePlacedTypeOtherDescriptionSpecified
        {
            get { return ExpenseClaimItemInsurancePlacedTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// For maintenance, service, or activities performed over a period of time, the date that it ended or will end.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMODate ExpenseClaimItemToDate;

        /// <summary>
        /// Gets or sets a value indicating whether the ExpenseClaimItemToDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ExpenseClaimItemToDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExpenseClaimItemToDateSpecified
        {
            get { return ExpenseClaimItemToDate != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 10)]
        public EXPENSE_CLAIM_ITEM_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
