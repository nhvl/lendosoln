namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class EXPENSE_CLAIM_ITEMS
    {
        /// <summary>
        /// Gets a value indicating whether the EXPENSE_CLAIM_ITEMS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExpenseClaimItemSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of expense claim items.
        /// </summary>
        [XmlElement("EXPENSE_CLAIM_ITEM", Order = 0)]
		public List<EXPENSE_CLAIM_ITEM> ExpenseClaimItem = new List<EXPENSE_CLAIM_ITEM>();

        /// <summary>
        /// Gets or sets a value indicating whether the ExpenseClaimItem element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ExpenseClaimItem element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExpenseClaimItemSpecified
        {
            get { return this.ExpenseClaimItem != null && this.ExpenseClaimItem.Count(e => e != null && e.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public EXPENSE_CLAIM_ITEMS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
