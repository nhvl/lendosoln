namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class EXPENSE_CLAIM_ITEM
    {
        /// <summary>
        /// Gets a value indicating whether the EXPENSE_CLAIM_ITEM container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExpenseClaimItemDetailSpecified
                    || this.ExpenseClaimItemOccurrencesSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// Details about an expense claim item.
        /// </summary>
        [XmlElement("EXPENSE_CLAIM_ITEM_DETAIL", Order = 0)]
        public EXPENSE_CLAIM_ITEM_DETAIL ExpenseClaimItemDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the ExpenseClaimItemDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ExpenseClaimItemDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExpenseClaimItemDetailSpecified
        {
            get { return this.ExpenseClaimItemDetail != null && this.ExpenseClaimItemDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Occurrences of an expense claim item.
        /// </summary>
        [XmlElement("EXPENSE_CLAIM_ITEM_OCCURRENCES", Order = 1)]
        public EXPENSE_CLAIM_ITEM_OCCURRENCES ExpenseClaimItemOccurrences;

        /// <summary>
        /// Gets or sets a value indicating whether the ExpenseClaimItemOccurrences element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ExpenseClaimItemOccurrences element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExpenseClaimItemOccurrencesSpecified
        {
            get { return this.ExpenseClaimItemOccurrences != null && this.ExpenseClaimItemOccurrences.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public EXPENSE_CLAIM_ITEM_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
