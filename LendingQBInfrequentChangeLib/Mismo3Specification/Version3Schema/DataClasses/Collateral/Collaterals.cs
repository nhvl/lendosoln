namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class COLLATERALS
    {
        /// <summary>
        /// Gets a value indicating whether the COLLATERALS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CollateralSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of collaterals.
        /// </summary>
        [XmlElement("COLLATERAL", Order = 0)]
		public List<COLLATERAL> Collateral = new List<COLLATERAL>();

        /// <summary>
        /// Gets or sets a value indicating whether the Collateral element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Collateral element has been assigned a value.</value>
        [XmlIgnore]
        public bool CollateralSpecified
        {
            get { return this.Collateral != null && this.Collateral.Count(c => c != null && c.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public COLLATERALS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
