namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class COLLATERAL_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.LienPriorityExceptionTypeOtherDescriptionSpecified
                    || this.LienPriorityExceptionTypeSpecified;
            }
        }

        /// <summary>
        /// Describes the lien priority for a piece of collateral where multiple pieces of collateral secure a single loan. 
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<LienPriorityBase> LienPriorityExceptionType;

        /// <summary>
        /// Gets or sets a value indicating whether the LienPriorityExceptionType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LienPriorityExceptionType element has been assigned a value.</value>
        [XmlIgnore]
        public bool LienPriorityExceptionTypeSpecified
        {
            get { return this.LienPriorityExceptionType != null && this.LienPriorityExceptionType.enumValue != LienPriorityBase.Blank; }
            set { }
        }

        /// <summary>
        /// Specifies the lien priority when other is selected for Lien Priority Exception Type.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString LienPriorityExceptionTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the LienPriorityExceptionTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LienPriorityExceptionTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool LienPriorityExceptionTypeOtherDescriptionSpecified
        {
            get { return LienPriorityExceptionTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public COLLATERAL_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
