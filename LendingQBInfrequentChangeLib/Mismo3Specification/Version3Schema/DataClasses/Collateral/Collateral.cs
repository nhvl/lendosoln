namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class COLLATERAL
    {
        /// <summary>
        /// Gets a value indicating whether the COLLATERAL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                if (Mismo3Utilities.ExceedsThreshold(1, new List<bool> { this.PledgedAssetSpecified, this.SubjectPropertySpecified }))
                {
                    throw new System.Xml.Schema.XmlSchemaValidationException(
                        Mismo3Utilities.GetXSDChoiceExceptionMessage(
                        "COLLATERAL", 
                        new List<string> { "PLEDGED_ASSET", "SUBJECT_PROPERTY" }));
                }

                return this.CollateralDetailSpecified
                    || this.ExtensionSpecified
                    || this.PledgedAssetSpecified
                    || this.SubjectPropertySpecified;
            }
        }

        /// <summary>
        /// A pledged asset collateral.
        /// </summary>
        [XmlElement("PLEDGED_ASSET", Order = 0)]
        public ASSET PledgedAsset;

        /// <summary>
        /// Gets or sets a value indicating whether the PledgedAsset element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PledgedAsset element has been assigned a value.</value>
        [XmlIgnore]
        public bool PledgedAssetSpecified
        {
            get { return this.PledgedAsset != null && this.PledgedAsset.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details about a property collateral.
        /// </summary>
        [XmlElement("SUBJECT_PROPERTY", Order = 1)]
        public PROPERTY SubjectProperty;

        /// <summary>
        /// Gets or sets a value indicating whether the SubjectProperty element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SubjectProperty element has been assigned a value.</value>
        [XmlIgnore]
        public bool SubjectPropertySpecified
        {
            get { return this.SubjectProperty != null && this.SubjectProperty.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details about a collateral.
        /// </summary>
        [XmlElement("COLLATERAL_DETAIL", Order = 2)]
        public COLLATERAL_DETAIL CollateralDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the CollateralDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CollateralDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool CollateralDetailSpecified
        {
            get { return this.CollateralDetail != null && this.CollateralDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public COLLATERAL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// Gets or sets the $$xlink$$ label.
        /// </summary>
        [XmlAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified, Namespace = Mismo3Constants.XlinkNamespace)]
        public string label;

        /// <summary>
        /// Indicates whether the label can be serialized.
        /// </summary>
        /// <returns>A boolean indicating whether the label can be serialized.</returns>
        public bool ShouldSerializelabel()
        {
            return !string.IsNullOrEmpty(label);
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
