namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets a value indicating whether the DocumentCertifications element has a value to serialize.
    /// </summary>
    /// <returns>A boolean indicating whether the DocumentCertifications element has been assigned a value.</returns>
    public partial class PDD_DOCUMENT_SPECIFIC_DATA_SET_EXTENSION
    {
        /// <summary>
        /// Gets a value indicating whether the PDD_DOCUMENT_SPECIFIC_DATA_SET_EXTENSION should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get { return this.DocumentCertificationsSpecified; }
        }

        /// <summary>
        /// Holds document certifications.
        /// </summary>
        [XmlElement("DOCUMENT_CERTIFICATIONS", Order = 0)]
        public DOCUMENT_CERTIFICATIONS DocumentCertifications;

        /// <summary>
        /// Gets a value indicating whether the DocumentCertifications element has a value to serialize.
        /// </summary>
        /// <returns>A boolean indicating whether the DocumentCertifications element has been assigned a value.</returns>
        [XmlIgnore]
        public bool DocumentCertificationsSpecified
        {
            get { return DocumentCertifications != null && DocumentCertifications.ShouldSerialize; }
        }
    }
}
