namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the ACHS element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the ACHS element has been assigned a value.</value>
    public partial class PDD_ACH_EXTENSION
    {
        /// <summary>
        /// Indicates whether this element should be serialized.
        /// </summary>
        /// <returns>A boolean value indicating whether this element should be serialized.</returns>
        public bool ShouldSerialize()
        {
            return ACHSSpecified;
        }

        /// <summary>
        /// A container for a list of <see cref="ACH"/> elements.
        /// </summary>
        [XmlElement("ACHS", Order = 0)]
        public ACHS ACHS;

        /// <summary>
        /// Gets or sets a value indicating whether the ACHS element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ACHS element has been assigned a value.</value>
        [XmlIgnore]
        public bool ACHSSpecified
        {
            get { return ACHS != null && ACHS.ShouldSerialize(); }
        }

    }
}
