namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the MortgageProgramType enum has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the MortgageProgramType enum has been assigned a value other than Blank.</value>
    public partial class PDD_TERMS_OF_LOAN_EXTENSION
    {
        /// <summary>
        /// Gets a value indicating whether the PDD_TERMS_OF_LOAN_EXTENSION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return MortgageProgramTypeSpecified;
            }
        }

        /// <summary>
        /// Represents the type of mortgage program, as it applies for the Ginnie Mae PDD.
        /// </summary>
        [XmlElement("MortgageProgramType", Order = 0)]
        public MISMOEnum<MortgageProgramBase> MortgageProgramType;

        /// <summary>
        /// Gets or sets a value indicating whether the MortgageProgramType enum has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MortgageProgramType enum has been assigned a value other than Blank.</value>
        [XmlIgnore]
        public bool MortgageProgramTypeSpecified
        {
            get
            {
                return MortgageProgramType != null && MortgageProgramType.enumValue != MortgageProgramBase.Blank;
            }
        }
    }
}
