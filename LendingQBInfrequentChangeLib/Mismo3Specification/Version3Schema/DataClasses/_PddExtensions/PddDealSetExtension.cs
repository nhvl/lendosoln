namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets a value indicating whether the DocumentSpecificDataSet has a value to serialize.
    /// </summary>
    /// <returns>A boolean indicating whether the DocumentSpecificDataSet element has been assigned a value.</returns>
    public partial class PDD_DEAL_SET_EXTENSION
    {
        /// <summary>
        /// Gets a value indicating whether the PDD_DEAL_SET_EXTENSION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get { return this.SecuritiesSpecified || this.DocumentSpecificDataSetSpecified; }
        }

        /// <summary>
        /// A list of securities in this extension.
        /// </summary>
        [XmlElement("SECURITIES", Order = 0)]
        public SECURITIES Securities;

        /// <summary>
        /// Gets a value indicating whether the Securities has a value to serialize.
        /// </summary>
        /// <returns>A boolean indicating whether the Securities element has been assigned a value.</returns>
        [XmlIgnore]
        public bool SecuritiesSpecified
        {
            get { return Securities != null; }
        }

        /// <summary>
        /// A document-specific data set associated with the deal set.
        /// </summary>
        [XmlElement("DOCUMENT_SPECIFIC_DATA_SETS", Order = 1)]
        public DOCUMENT_SPECIFIC_DATA_SETS DocumentSpecificDataSets;

        /// <summary>
        /// Gets a value indicating whether the DocumentSpecificDataSet has a value to serialize.
        /// </summary>
        /// <returns>A boolean indicating whether the DocumentSpecificDataSet element has been assigned a value.</returns>
        [XmlIgnore]
        public bool DocumentSpecificDataSetSpecified
        {
            get { return DocumentSpecificDataSets != null && DocumentSpecificDataSets.ShouldSerialize; }
        }
    }
}
