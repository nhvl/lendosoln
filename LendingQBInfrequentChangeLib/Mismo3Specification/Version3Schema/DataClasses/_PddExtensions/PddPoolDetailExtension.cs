namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the GovernmentBondFinancingProgramName element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the GovernmentBondFinancingProgramName element has been assigned a value.</value>
    public partial class PDD_POOL_DETAIL_EXTENSION
    {
        /// <summary>
        /// Gets a value indicating whether the PDD_POOL_DETAIL_EXTENSION container should be serialized.
        /// </summary>
        /// <returns>A value indicating whether the container should be serialized.</returns>
        public bool ShouldSerialize()
        {
            return this.PoolMaturityPeriodCountSpecified && PoolMaturityPeriodTypeSpecified;
        }

        /// <summary>
        /// Maturity period count for a pool.
        /// </summary>
        [XmlElement("PoolMaturityPeriodCount", Order = 0)]
        public MISMOCount PoolMaturityPeriodCount;

        /// <summary>
        /// Gets or sets a value indicating whether the PoolMaturityPeriodCount element has a value to serialize.
        /// </summary>
        /// <returns>A boolean indicating whether the PoolMaturityPeriodCount element has been assigned a value.</returns>
        [XmlIgnore]
        public bool PoolMaturityPeriodCountSpecified
        {
            get { return PoolMaturityPeriodCount != null; }
        }

        /// <summary>
        /// The type of the maturity period count for the pool (PDD expects Month).
        /// </summary>
        [XmlElement("PoolMaturityPeriodType", Order = 1)]
        public MISMOEnum<PoolMaturityPeriodBase> PoolMaturityPeriodType;

        /// <summary>
        /// Gets or sets a value indicating whether the PoolMaturityPeriodType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PoolMaturityPeriodType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PoolMaturityPeriodTypeSpecified
        {
            get { return PoolMaturityPeriodType != null && PoolMaturityPeriodType.enumValue != PoolMaturityPeriodBase.Blank; }
        }

        /// <summary>
        /// The name of the program for a pool using government bond financing.
        /// </summary>
        [XmlElement("GovernmentBondFinancingProgramName", Order = 2)]
        public MISMOString GovernmentBondFinancingProgramName;

        /// <summary>
        /// Gets or sets a value indicating whether the GovernmentBondFinancingProgramName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GovernmentBondFinancingProgramName element has been assigned a value.</value>
        [XmlIgnore]
        public bool GovernmentBondFinancingProgramNameSpecified
        {
            get { return GovernmentBondFinancingProgramName != null && !string.IsNullOrEmpty(GovernmentBondFinancingProgramName.Value); }
        }
    }
}
