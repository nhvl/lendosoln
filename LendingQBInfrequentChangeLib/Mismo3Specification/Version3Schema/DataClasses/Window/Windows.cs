namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class WINDOWS
    {
        /// <summary>
        /// Gets a value indicating whether the WINDOWS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.WindowSpecified;
            }
        }

        /// <summary>
        /// A collection of windows.
        /// </summary>
        [XmlElement("WINDOW", Order = 0)]
		public List<WINDOW> Window = new List<WINDOW>();

        /// <summary>
        /// Gets or sets a value indicating whether the Window element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Window element has been assigned a value.</value>
        [XmlIgnore]
        public bool WindowSpecified
        {
            get { return this.Window != null && this.Window.Count(w => w != null && w.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public WINDOWS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
