namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class SALES_CONTINGENCIES
    {
        /// <summary>
        /// Gets a value indicating whether the SALES_CONTINGENCIES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.SalesContingencySpecified;
            }
        }

        /// <summary>
        /// A collection of sales contingencies.
        /// </summary>
        [XmlElement("SALES_CONTINGENCY", Order = 0)]
		public List<SALES_CONTINGENCY> SalesContingency = new List<SALES_CONTINGENCY>();

        /// <summary>
        /// Gets or sets a value indicating whether the SalesContingency element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SalesContingency element has been assigned a value.</value>
        [XmlIgnore]
        public bool SalesContingencySpecified
        {
            get { return this.SalesContingency != null && this.SalesContingency.Count(s => s != null && s.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public SALES_CONTINGENCIES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
