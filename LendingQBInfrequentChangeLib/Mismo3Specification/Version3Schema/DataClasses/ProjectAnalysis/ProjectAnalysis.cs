namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PROJECT_ANALYSIS
    {
        /// <summary>
        /// Gets a value indicating whether the PROJECT_ANALYSIS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CharacteristicsAffectMarketabilityDescriptionSpecified
                    || this.ExtensionSpecified
                    || this.OwnersWithinProjectMoreThanTwoMonthsDelinquentCountSpecified
                    || this.ProjectAnalysisAdditionalFacilitiesFeeAmountSpecified
                    || this.ProjectAnalysisAdditionalFacilitiesFeeDescriptionSpecified
                    || this.ProjectAnalysisAdditionalFacilitiesFeeIndicatorSpecified
                    || this.ProjectAnalysisAdequacyOfBudgetTypeSpecified
                    || this.ProjectAnalysisAdequacyOfManagementTypeSpecified
                    || this.ProjectAnalysisBudgetAnalysisCommentDescriptionSpecified
                    || this.ProjectAnalysisBudgetAnalyzedIndicatorSpecified
                    || this.ProjectAnalysisCharacteristicsAffectMarketabilityIndicatorSpecified
                    || this.ProjectAnalysisCompetitiveProjectComparisonDescriptionSpecified
                    || this.ProjectAnalysisCompetitiveProjectComparisonTypeSpecified
                    || this.ProjectAnalysisGroundRentAmountSpecified
                    || this.ProjectAnalysisGroundRentDescriptionSpecified
                    || this.ProjectAnalysisGroundRentIndicatorSpecified
                    || this.ProjectAnalysisSpecialCharacteristicsDescriptionSpecified
                    || this.ProjectAnalysisSpecialCharacteristicsIndicatorSpecified
                    || this.ProjectConditionAndQualityDescriptionSpecified;
            }
        }

        /// <summary>
        /// A free-form text field describing the characteristics that affect marketability.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString CharacteristicsAffectMarketabilityDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the CharacteristicsAffectMarketabilityDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CharacteristicsAffectMarketabilityDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool CharacteristicsAffectMarketabilityDescriptionSpecified
        {
            get { return CharacteristicsAffectMarketabilityDescription != null; }
            set { }
        }

        /// <summary>
        /// The count of owners within project (i.e. of a cooperative) that are more than two (2) months delinquent in payment.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOCount OwnersWithinProjectMoreThanTwoMonthsDelinquentCount;

        /// <summary>
        /// Gets or sets a value indicating whether the OwnersWithinProjectMoreThanTwoMonthsDelinquentCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OwnersWithinProjectMoreThanTwoMonthsDelinquentCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool OwnersWithinProjectMoreThanTwoMonthsDelinquentCountSpecified
        {
            get { return OwnersWithinProjectMoreThanTwoMonthsDelinquentCount != null; }
            set { }
        }

        /// <summary>
        /// Identifies any other facility related charges that are not included in standard project fee (e.g. HOA fees).
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOAmount ProjectAnalysisAdditionalFacilitiesFeeAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectAnalysisAdditionalFacilitiesFeeAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectAnalysisAdditionalFacilitiesFeeAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectAnalysisAdditionalFacilitiesFeeAmountSpecified
        {
            get { return ProjectAnalysisAdditionalFacilitiesFeeAmount != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field describing the conditions, extent, and terms of the additional fee not included in the standard project fee (e.g. HOA fee).
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString ProjectAnalysisAdditionalFacilitiesFeeDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectAnalysisAdditionalFacilitiesFeeDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectAnalysisAdditionalFacilitiesFeeDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectAnalysisAdditionalFacilitiesFeeDescriptionSpecified
        {
            get { return ProjectAnalysisAdditionalFacilitiesFeeDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the project does impose an additional fee beyond the standard project fee.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOIndicator ProjectAnalysisAdditionalFacilitiesFeeIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectAnalysisAdditionalFacilitiesFeeIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectAnalysisAdditionalFacilitiesFeeIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectAnalysisAdditionalFacilitiesFeeIndicatorSpecified
        {
            get { return ProjectAnalysisAdditionalFacilitiesFeeIndicator != null; }
            set { }
        }

        /// <summary>
        /// Identifies the assessment, by the reviewer, if the operating budget of the project is adequate for its immediate as well as long-term needs.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOEnum<ProjectAnalysisAdequacyOfBudgetBase> ProjectAnalysisAdequacyOfBudgetType;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectAnalysisAdequacyOfBudgetType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectAnalysisAdequacyOfBudgetType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectAnalysisAdequacyOfBudgetTypeSpecified
        {
            get { return this.ProjectAnalysisAdequacyOfBudgetType != null && this.ProjectAnalysisAdequacyOfBudgetType.enumValue != ProjectAnalysisAdequacyOfBudgetBase.Blank; }
            set { }
        }

        /// <summary>
        /// Identifies the assessment by the reviewer if the management of the project as well as its rules and regulations are adequate to protect its operations and future value.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOEnum<ProjectAnalysisAdequacyOfManagementBase> ProjectAnalysisAdequacyOfManagementType;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectAnalysisAdequacyOfManagementType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectAnalysisAdequacyOfManagementType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectAnalysisAdequacyOfManagementTypeSpecified
        {
            get { return this.ProjectAnalysisAdequacyOfManagementType != null && this.ProjectAnalysisAdequacyOfManagementType.enumValue != ProjectAnalysisAdequacyOfManagementBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field describing the results of analyzing the project budget.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOString ProjectAnalysisBudgetAnalysisCommentDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectAnalysisBudgetAnalysisCommentDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectAnalysisBudgetAnalysisCommentDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectAnalysisBudgetAnalysisCommentDescriptionSpecified
        {
            get { return ProjectAnalysisBudgetAnalysisCommentDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the appraiser analyzed the budget of the project.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOIndicator ProjectAnalysisBudgetAnalyzedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectAnalysisBudgetAnalyzedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectAnalysisBudgetAnalyzedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectAnalysisBudgetAnalyzedIndicatorSpecified
        {
            get { return ProjectAnalysisBudgetAnalyzedIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the characteristics described in the Project Analysis affect marketability.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOIndicator ProjectAnalysisCharacteristicsAffectMarketabilityIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectAnalysisCharacteristicsAffectMarketabilityIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectAnalysisCharacteristicsAffectMarketabilityIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectAnalysisCharacteristicsAffectMarketabilityIndicatorSpecified
        {
            get { return ProjectAnalysisCharacteristicsAffectMarketabilityIndicator != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field describing the differences, relative values, and effect of competitive projects.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOString ProjectAnalysisCompetitiveProjectComparisonDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectAnalysisCompetitiveProjectComparisonDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectAnalysisCompetitiveProjectComparisonDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectAnalysisCompetitiveProjectComparisonDescriptionSpecified
        {
            get { return ProjectAnalysisCompetitiveProjectComparisonDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the type of competition the project faces.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOEnum<ProjectAnalysisCompetitiveProjectComparisonBase> ProjectAnalysisCompetitiveProjectComparisonType;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectAnalysisCompetitiveProjectComparisonType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectAnalysisCompetitiveProjectComparisonType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectAnalysisCompetitiveProjectComparisonTypeSpecified
        {
            get { return this.ProjectAnalysisCompetitiveProjectComparisonType != null && this.ProjectAnalysisCompetitiveProjectComparisonType.enumValue != ProjectAnalysisCompetitiveProjectComparisonBase.Blank; }
            set { }
        }

        /// <summary>
        /// Identifies the annual ground rent amount for the project unit.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOAmount ProjectAnalysisGroundRentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectAnalysisGroundRentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectAnalysisGroundRentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectAnalysisGroundRentAmountSpecified
        {
            get { return ProjectAnalysisGroundRentAmount != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field describing the ground rent.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOString ProjectAnalysisGroundRentDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectAnalysisGroundRentDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectAnalysisGroundRentDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectAnalysisGroundRentDescriptionSpecified
        {
            get { return ProjectAnalysisGroundRentDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the project is subject to ground rents.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOIndicator ProjectAnalysisGroundRentIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectAnalysisGroundRentIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectAnalysisGroundRentIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectAnalysisGroundRentIndicatorSpecified
        {
            get { return ProjectAnalysisGroundRentIndicator != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field describing any special, atypical, or unusual characteristics of the project.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOString ProjectAnalysisSpecialCharacteristicsDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectAnalysisSpecialCharacteristicsDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectAnalysisSpecialCharacteristicsDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectAnalysisSpecialCharacteristicsDescriptionSpecified
        {
            get { return ProjectAnalysisSpecialCharacteristicsDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the project has special, atypical, or unusual characteristics.
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMOIndicator ProjectAnalysisSpecialCharacteristicsIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectAnalysisSpecialCharacteristicsIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectAnalysisSpecialCharacteristicsIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectAnalysisSpecialCharacteristicsIndicatorSpecified
        {
            get { return ProjectAnalysisSpecialCharacteristicsIndicator != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the condition of the project.
        /// </summary>
        [XmlElement(Order = 17)]
        public MISMOString ProjectConditionAndQualityDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectConditionAndQualityDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectConditionAndQualityDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectConditionAndQualityDescriptionSpecified
        {
            get { return ProjectConditionAndQualityDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 18)]
        public PROJECT_ANALYSIS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
