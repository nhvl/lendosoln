namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class MANUFACTURED_HOME_IDENTIFICATION
    {
        /// <summary>
        /// Gets a value indicating whether the MANUFACTURED_HOME_IDENTIFICATION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ManufacturedHomeBuiltStateNameSpecified
                    || this.ManufacturedHomeCostDataCommentDescriptionSpecified
                    || this.ManufacturedHomeRegionIdentifierSpecified
                    || this.NADAManufacturedHousingGuideBlackPageIdentifierSpecified
                    || this.NADAManufacturedHousingGuideConversionChartPageIdentifierSpecified
                    || this.NADAManufacturedHousingGuideEditionMonthSpecified
                    || this.NADAManufacturedHousingGuideEditionYearSpecified
                    || this.NADAManufacturedHousingGuideGreyPageIdentifierSpecified
                    || this.NADAManufacturedHousingGuideWhitePageIdentifierSpecified
                    || this.NADAManufacturedHousingGuideYellowPageIdentifierSpecified;
            }
        }

        /// <summary>
        /// Specifies the state in which the Manufactured Home was manufactured.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString ManufacturedHomeBuiltStateName;

        /// <summary>
        /// Gets or sets a value indicating whether the ManufacturedHomeBuiltStateName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ManufacturedHomeBuiltStateName element has been assigned a value.</value>
        [XmlIgnore]
        public bool ManufacturedHomeBuiltStateNameSpecified
        {
            get { return ManufacturedHomeBuiltStateName != null; }
            set { }
        }

        /// <summary>
        /// Free-form field used to comment or describe the reproduction or replacement cost data.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString ManufacturedHomeCostDataCommentDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ManufacturedHomeCostDataCommentDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ManufacturedHomeCostDataCommentDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ManufacturedHomeCostDataCommentDescriptionSpecified
        {
            get { return ManufacturedHomeCostDataCommentDescription != null; }
            set { }
        }

        /// <summary>
        /// Identifies the region in which the Manufactured Home was manufactured.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOIdentifier ManufacturedHomeRegionIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the ManufacturedHomeRegionIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ManufacturedHomeRegionIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool ManufacturedHomeRegionIdentifierSpecified
        {
            get { return ManufacturedHomeRegionIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Identifies the page number of the "Black" page found in the Manufactured Housing Guide of the National Automobile Dealers Association.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOIdentifier NADAManufacturedHousingGuideBlackPageIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the NADAManufacturedHousingGuideBlackPageIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NADAManufacturedHousingGuideBlackPageIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool NADAManufacturedHousingGuideBlackPageIdentifierSpecified
        {
            get { return NADAManufacturedHousingGuideBlackPageIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Identifies the page number of a particular conversion chart found in the Manufactured Housing Guide of the National Automobile Dealers Association.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOIdentifier NADAManufacturedHousingGuideConversionChartPageIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the NADAManufacturedHousingGuideConversionChartPageIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NADAManufacturedHousingGuideConversionChartPageIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool NADAManufacturedHousingGuideConversionChartPageIdentifierSpecified
        {
            get { return NADAManufacturedHousingGuideConversionChartPageIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Specifies the publication month of the Manufactured Housing Guide of the National Automobile Dealers Association.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOMonth NADAManufacturedHousingGuideEditionMonth;

        /// <summary>
        /// Gets or sets a value indicating whether the NADAManufacturedHousingGuideEditionMonth element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NADAManufacturedHousingGuideEditionMonth element has been assigned a value.</value>
        [XmlIgnore]
        public bool NADAManufacturedHousingGuideEditionMonthSpecified
        {
            get { return NADAManufacturedHousingGuideEditionMonth != null; }
            set { }
        }

        /// <summary>
        /// Specifies the publication year of the Manufactured Housing Guide of the National Automobile Dealers Association.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOYear NADAManufacturedHousingGuideEditionYear;

        /// <summary>
        /// Gets or sets a value indicating whether the NADAManufacturedHousingGuideEditionYear element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NADAManufacturedHousingGuideEditionYear element has been assigned a value.</value>
        [XmlIgnore]
        public bool NADAManufacturedHousingGuideEditionYearSpecified
        {
            get { return NADAManufacturedHousingGuideEditionYear != null; }
            set { }
        }

        /// <summary>
        /// Identifies the page number of the "Grey" page found in the Manufactured Housing Guide of the National Automobile Dealers Association.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOIdentifier NADAManufacturedHousingGuideGreyPageIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the NADAManufacturedHousingGuideGreyPageIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NADAManufacturedHousingGuideGreyPageIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool NADAManufacturedHousingGuideGreyPageIdentifierSpecified
        {
            get { return NADAManufacturedHousingGuideGreyPageIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Identifies the page number of the "White" page found in the Manufactured Housing Guide of the National Automobile Dealers Association.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOIdentifier NADAManufacturedHousingGuideWhitePageIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the NADAManufacturedHousingGuideWhitePageIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NADAManufacturedHousingGuideWhitePageIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool NADAManufacturedHousingGuideWhitePageIdentifierSpecified
        {
            get { return NADAManufacturedHousingGuideWhitePageIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Identifies the page number of the "Yellow" page found in the Manufactured Housing Guide of the National Automobile Dealers Association.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOIdentifier NADAManufacturedHousingGuideYellowPageIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the NADAManufacturedHousingGuideYellowPageIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NADAManufacturedHousingGuideYellowPageIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool NADAManufacturedHousingGuideYellowPageIdentifierSpecified
        {
            get { return NADAManufacturedHousingGuideYellowPageIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 10)]
        public MANUFACTURED_HOME_IDENTIFICATION_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
