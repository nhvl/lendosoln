namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class LOAN_STATUSES
    {
        /// <summary>
        /// Gets a value indicating whether the LOAN_STATUSES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.LoanStatusSpecified;
            }
        }

        /// <summary>
        /// A collection of loan statuses.
        /// </summary>
        [XmlElement("LOAN_STATUS", Order = 0)]
        public List<LOAN_STATUS> LoanStatus = new List<LOAN_STATUS>();

        /// <summary>
        /// Gets or sets a value indicating whether the LoanStatus element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanStatus element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanStatusSpecified
        {
            get { return this.LoanStatus != null && this.LoanStatus.Count(l => l != null && l.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public LOAN_STATUSES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
