namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class NEGATIVE_AMORTIZATION_RULE
    {
        /// <summary>
        /// Gets a value indicating whether the NEGATIVE_AMORTIZATION_RULE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.LoanNegativeAmortizationResolutionTypeOtherDescriptionSpecified
                    || this.LoanNegativeAmortizationResolutionTypeSpecified
                    || this.MaximumPrincipalBalanceAmountSpecified
                    || this.NegativeAmortizationLimitAmountSpecified
                    || this.NegativeAmortizationLimitMonthsCountSpecified
                    || this.NegativeAmortizationLimitPercentSpecified
                    || this.NegativeAmortizationMaximumLoanBalanceAmountSpecified
                    || this.NegativeAmortizationMaximumPITIAmountSpecified
                    || this.NegativeAmortizationRecastTypeSpecified
                    || this.NegativeAmortizationTypeSpecified;
            }
        }

        /// <summary>
        /// Denotes the method of resolving a loan that has reached the negative amortization limit percent.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<LoanNegativeAmortizationResolutionBase> LoanNegativeAmortizationResolutionType;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanNegativeAmortizationResolutionType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanNegativeAmortizationResolutionType element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanNegativeAmortizationResolutionTypeSpecified
        {
            get { return this.LoanNegativeAmortizationResolutionType != null && this.LoanNegativeAmortizationResolutionType.enumValue != LoanNegativeAmortizationResolutionBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Loan Negative Amortization Resolution Type.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString LoanNegativeAmortizationResolutionTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanNegativeAmortizationResolutionTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanNegativeAmortizationResolutionTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanNegativeAmortizationResolutionTypeOtherDescriptionSpecified
        {
            get { return LoanNegativeAmortizationResolutionTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The stated maximum principal balance allowed if negative amortization occurs.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOAmount MaximumPrincipalBalanceAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the MaximumPrincipalBalanceAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MaximumPrincipalBalanceAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool MaximumPrincipalBalanceAmountSpecified
        {
            get { return MaximumPrincipalBalanceAmount != null; }
            set { }
        }

        /// <summary>
        /// Specifies the maximum dollar amount of negative amortization that is allowed before it is required to recalculate the fully amortizing payment based on the new loan balance.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOAmount NegativeAmortizationLimitAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the NegativeAmortizationLimitAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NegativeAmortizationLimitAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool NegativeAmortizationLimitAmountSpecified
        {
            get { return NegativeAmortizationLimitAmount != null; }
            set { }
        }

        /// <summary>
        /// The limit (in months) during which negative amortization is allowed and after which the loan is re-amortized.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOCount NegativeAmortizationLimitMonthsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the NegativeAmortizationLimitMonthsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NegativeAmortizationLimitMonthsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool NegativeAmortizationLimitMonthsCountSpecified
        {
            get { return NegativeAmortizationLimitMonthsCount != null; }
            set { }
        }

        /// <summary>
        /// The maximum percentage of the original principal balance allowed for negative amortization.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOPercent NegativeAmortizationLimitPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the NegativeAmortizationLimitPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NegativeAmortizationLimitPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool NegativeAmortizationLimitPercentSpecified
        {
            get { return NegativeAmortizationLimitPercent != null; }
            set { }
        }

        /// <summary>
        /// Then maximum loan balance with accrued increase from negative amortization. (The initial loan amount plus any balance increase that may accrue from the negative amortization provision).
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOAmount NegativeAmortizationMaximumLoanBalanceAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the NegativeAmortizationMaximumLoanBalanceAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NegativeAmortizationMaximumLoanBalanceAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool NegativeAmortizationMaximumLoanBalanceAmountSpecified
        {
            get { return NegativeAmortizationMaximumLoanBalanceAmount != null; }
            set { }
        }

        /// <summary>
        /// The PITI used to qualify the borrower for negative amortizing loan, calculated using the Negative Amortization Maximum Loan Balance.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOAmount NegativeAmortizationMaximumPITIAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the NegativeAmortizationMaximumPITIAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NegativeAmortizationMaximumPITIAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool NegativeAmortizationMaximumPITIAmountSpecified
        {
            get { return NegativeAmortizationMaximumPITIAmount != null; }
            set { }
        }

        /// <summary>
        /// Indicates the condition that will cause a mortgage to be recalculated as positively amortizing without any future negative amortization.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOEnum<NegativeAmortizationRecastBase> NegativeAmortizationRecastType;

        /// <summary>
        /// Gets or sets a value indicating whether the NegativeAmortizationRecastType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NegativeAmortizationRecastType element has been assigned a value.</value>
        [XmlIgnore]
        public bool NegativeAmortizationRecastTypeSpecified
        {
            get { return this.NegativeAmortizationRecastType != null && this.NegativeAmortizationRecastType.enumValue != NegativeAmortizationRecastBase.Blank; }
            set { }
        }

        /// <summary>
        /// Specifies whether negative amortization is scheduled or potential on the loan. Negative amortization is unpaid interest added to the mortgage principal. This applies to loans where the principal balance increases because the payments do not cover the interest.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOEnum<NegativeAmortizationBase> NegativeAmortizationType;

        /// <summary>
        /// Gets or sets a value indicating whether the NegativeAmortizationType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NegativeAmortizationType element has been assigned a value.</value>
        [XmlIgnore]
        public bool NegativeAmortizationTypeSpecified
        {
            get { return this.NegativeAmortizationType != null && this.NegativeAmortizationType.enumValue != NegativeAmortizationBase.Blank; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 10)]
        public NEGATIVE_AMORTIZATION_RULE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
