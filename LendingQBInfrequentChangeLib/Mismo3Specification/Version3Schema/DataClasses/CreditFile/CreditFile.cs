namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class CREDIT_FILE
    {
        /// <summary>
        /// Gets a value indicating whether the CREDIT_FILE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditCommentsSpecified
                    || this.CreditErrorMessagesSpecified
                    || this.CreditFileDetailSpecified
                    || this.CreditFileOwningBureauSpecified
                    || this.CreditFileVariationsSpecified
                    || this.ExtensionSpecified
                    || this.PartySpecified;
            }
        }

        /// <summary>
        /// Comments on the credit file.
        /// </summary>
        [XmlElement("CREDIT_COMMENTS", Order = 0)]
        public CREDIT_COMMENTS CreditComments;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditComments element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditComments element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditCommentsSpecified
        {
            get { return this.CreditComments != null && this.CreditComments.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Error messages in the credit file.
        /// </summary>
        [XmlElement("CREDIT_ERROR_MESSAGES", Order = 1)]
        public CREDIT_ERROR_MESSAGES CreditErrorMessages;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditErrorMessages element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditErrorMessages element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditErrorMessagesSpecified
        {
            get { return this.CreditErrorMessages != null && this.CreditErrorMessages.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details on the credit file.
        /// </summary>
        [XmlElement("CREDIT_FILE_DETAIL", Order = 2)]
        public CREDIT_FILE_DETAIL CreditFileDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditFileDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditFileDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditFileDetailSpecified
        {
            get { return this.CreditFileDetail != null && this.CreditFileDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The bureau who has ownership of the credit file.
        /// </summary>
        [XmlElement("CREDIT_FILE_OWNING_BUREAU", Order = 3)]
        public CREDIT_FILE_OWNING_BUREAU CreditFileOwningBureau;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditFileOwningBureau element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditFileOwningBureau element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditFileOwningBureauSpecified
        {
            get { return this.CreditFileOwningBureau != null && this.CreditFileOwningBureau.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Variations in the credit file.
        /// </summary>
        [XmlElement("CREDIT_FILE_VARIATIONS", Order = 4)]
        public CREDIT_FILE_VARIATIONS CreditFileVariations;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditFileVariations element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditFileVariations element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditFileVariationsSpecified
        {
            get { return this.CreditFileVariations != null && this.CreditFileVariations.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A party associated with the credit file.
        /// </summary>
        [XmlElement("PARTY", Order = 5)]
        public PARTY Party;

        /// <summary>
        /// Gets or sets a value indicating whether the Party element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Party element has been assigned a value.</value>
        [XmlIgnore]
        public bool PartySpecified
        {
            get { return this.Party != null && this.Party.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 6)]
        public CREDIT_FILE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
