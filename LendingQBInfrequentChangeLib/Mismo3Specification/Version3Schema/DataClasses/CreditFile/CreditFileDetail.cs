namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CREDIT_FILE_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the CREDIT_FILE_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditFileInfileDateSpecified
                    || this.CreditFileResultStatusTypeOtherDescriptionSpecified
                    || this.CreditFileResultStatusTypeSpecified
                    || this.CreditRepositorySourceTypeOtherDescriptionSpecified
                    || this.CreditRepositorySourceTypeSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// The In file Date is the date that the credit data repository first established this credit data file on this borrower.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMODate CreditFileInfileDate;

        /// <summary>
        /// Gets or sets a value indicating whether the Credit File In file Date element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Credit File In file Date element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditFileInfileDateSpecified
        {
            get { return CreditFileInfileDate != null; }
            set { }
        }

        /// <summary>
        /// Describes the resulting status of the request for a credit file from a credit repository bureau. The full reason for a Credit File not being returned will be reported in the Credit Error Message data element.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<CreditFileResultStatusBase> CreditFileResultStatusType;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditFileResultStatusType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditFileResultStatusType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditFileResultStatusTypeSpecified
        {
            get { return this.CreditFileResultStatusType != null && this.CreditFileResultStatusType.enumValue != CreditFileResultStatusBase.Blank; }
            set { }
        }

        /// <summary>
        /// When Credit File Result Status Type is set to Other, enter its value in this data element.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString CreditFileResultStatusTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditFileResultStatusTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditFileResultStatusTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditFileResultStatusTypeOtherDescriptionSpecified
        {
            get { return CreditFileResultStatusTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// This element describes the source of the credit file, Equifax, Experian, Trans Union or Unspecified if the specific sources are not specified.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOEnum<CreditRepositorySourceBase> CreditRepositorySourceType;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditRepositorySourceType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditRepositorySourceType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditRepositorySourceTypeSpecified
        {
            get { return this.CreditRepositorySourceType != null && this.CreditRepositorySourceType.enumValue != CreditRepositorySourceBase.Blank; }
            set { }
        }

        /// <summary>
        /// When Credit Repository Source Type is set to Other, enter its value in this data element.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString CreditRepositorySourceTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditRepositorySourceTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditRepositorySourceTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditRepositorySourceTypeOtherDescriptionSpecified
        {
            get { return CreditRepositorySourceTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 5)]
        public CREDIT_FILE_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
