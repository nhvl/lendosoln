namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CREDIT_FILES
    {
        /// <summary>
        /// Gets a value indicating whether the CREDIT_FILES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditFileSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of credit files.
        /// </summary>
        [XmlElement("CREDIT_FILE", Order = 0)]
		public List<CREDIT_FILE> CreditFile = new List<CREDIT_FILE>();

        /// <summary>
        /// Gets or sets a value indicating whether the CreditFile element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditFile element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditFileSpecified
        {
            get { return this.CreditFile != null && this.CreditFile.Count(c => c != null && c.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public CREDIT_FILES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
