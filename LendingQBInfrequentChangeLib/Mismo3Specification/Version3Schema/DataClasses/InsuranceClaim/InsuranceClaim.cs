namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class INSURANCE_CLAIM
    {
        /// <summary>
        /// Gets a value indicating whether the INSURANCE_CLAIM container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ActualInsuranceClaimAmountSpecified
                    || this.ActualInsuranceProceedsAmountSpecified
                    || this.EstimatedInsuranceClaimAmountSpecified
                    || this.EstimatedInsuranceProceedsAmountSpecified
                    || this.ExtensionSpecified
                    || this.InsuranceClaimEligibilityDateSpecified
                    || this.InsuranceClaimFiledDateSpecified
                    || this.InsuranceClaimPaidDateSpecified
                    || this.InsuranceClaimPendingIndicatorSpecified
                    || this.InsuranceClaimTypeOtherDescriptionSpecified
                    || this.InsuranceClaimTypeSpecified
                    || this.InsurancePartialClaimIndicatorSpecified;
            }
        }

        /// <summary>
        /// The actual dollar amount of the insurance claim.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount ActualInsuranceClaimAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ActualInsuranceClaimAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ActualInsuranceClaimAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ActualInsuranceClaimAmountSpecified
        {
            get { return ActualInsuranceClaimAmount != null; }
            set { }
        }

        /// <summary>
        /// The actual dollar amount of the insurance proceeds.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOAmount ActualInsuranceProceedsAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ActualInsuranceProceedsAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ActualInsuranceProceedsAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ActualInsuranceProceedsAmountSpecified
        {
            get { return ActualInsuranceProceedsAmount != null; }
            set { }
        }

        /// <summary>
        /// The estimated dollar amount of the insurance claim that will be filed.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOAmount EstimatedInsuranceClaimAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the EstimatedInsuranceClaimAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EstimatedInsuranceClaimAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool EstimatedInsuranceClaimAmountSpecified
        {
            get { return EstimatedInsuranceClaimAmount != null; }
            set { }
        }

        /// <summary>
        /// The estimated dollar amount of the insurance proceeds.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOAmount EstimatedInsuranceProceedsAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the EstimatedInsuranceProceedsAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EstimatedInsuranceProceedsAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool EstimatedInsuranceProceedsAmountSpecified
        {
            get { return EstimatedInsuranceProceedsAmount != null; }
            set { }
        }

        /// <summary>
        /// The date on which the insurance claim will be eligible for filing.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMODate InsuranceClaimEligibilityDate;

        /// <summary>
        /// Gets or sets a value indicating whether the InsuranceClaimEligibilityDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InsuranceClaimEligibilityDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool InsuranceClaimEligibilityDateSpecified
        {
            get { return InsuranceClaimEligibilityDate != null; }
            set { }
        }

        /// <summary>
        /// The actual date the insurance claim was filed.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMODate InsuranceClaimFiledDate;

        /// <summary>
        /// Gets or sets a value indicating whether the InsuranceClaimFiledDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InsuranceClaimFiledDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool InsuranceClaimFiledDateSpecified
        {
            get { return InsuranceClaimFiledDate != null; }
            set { }
        }

        /// <summary>
        /// The date an insurance claim for loss is paid.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMODate InsuranceClaimPaidDate;

        /// <summary>
        /// Gets or sets a value indicating whether the InsuranceClaimPaidDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InsuranceClaimPaidDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool InsuranceClaimPaidDateSpecified
        {
            get { return InsuranceClaimPaidDate != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that an insurance claim is pending.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOIndicator InsuranceClaimPendingIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the InsuranceClaimPendingIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InsuranceClaimPendingIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool InsuranceClaimPendingIndicatorSpecified
        {
            get { return InsuranceClaimPendingIndicator != null; }
            set { }
        }

        /// <summary>
        /// Specifies the type of insurance against which the claim is being made.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOEnum<InsuranceClaimBase> InsuranceClaimType;

        /// <summary>
        /// Gets or sets a value indicating whether the InsuranceClaimType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InsuranceClaimType element has been assigned a value.</value>
        [XmlIgnore]
        public bool InsuranceClaimTypeSpecified
        {
            get { return this.InsuranceClaimType != null && this.InsuranceClaimType.enumValue != InsuranceClaimBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected as the Insurance Claim Type.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOString InsuranceClaimTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the InsuranceClaimTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InsuranceClaimTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool InsuranceClaimTypeOtherDescriptionSpecified
        {
            get { return InsuranceClaimTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that a partial insurance claim is being made, not a full claim.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOIndicator InsurancePartialClaimIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the InsurancePartialClaimIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InsurancePartialClaimIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool InsurancePartialClaimIndicatorSpecified
        {
            get { return InsurancePartialClaimIndicator != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 11)]
        public INSURANCE_CLAIM_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
