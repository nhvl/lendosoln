namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class INSURANCE_CLAIMS
    {
        /// <summary>
        /// Gets a value indicating whether the INSURANCE_CLAIMS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.InsuranceClaimSpecified;
            }
        }

        /// <summary>
        /// A collection of insurance claims.
        /// </summary>
        [XmlElement("INSURANCE_CLAIM", Order = 0)]
		public List<INSURANCE_CLAIM> InsuranceClaim = new List<INSURANCE_CLAIM>();

        /// <summary>
        /// Gets or sets a value indicating whether the InsuranceClaim element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InsuranceClaim element has been assigned a value.</value>
        [XmlIgnore]
        public bool InsuranceClaimSpecified
        {
            get { return this.InsuranceClaim != null && this.InsuranceClaim.Count(i => i != null && i.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public INSURANCE_CLAIMS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
