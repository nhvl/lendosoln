namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class BUYDOWN_FUNDS
    {
        /// <summary>
        /// Gets a value indicating whether the BUYDOWN_FUNDS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BuydownFundSpecified
                    || this.BuydownFundsSummarySpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of buy down funds.
        /// </summary>
        [XmlElement("BUYDOWN_FUND", Order = 0)]
		public List<BUYDOWN_FUND> BuydownFund = new List<BUYDOWN_FUND>();

        /// <summary>
        /// Gets or sets a value indicating whether the BuyDownFund element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BuyDownFund element has been assigned a value.</value>
        [XmlIgnore]
        public bool BuydownFundSpecified
        {
            get { return this.BuydownFund != null && this.BuydownFund.Count(b => b != null && b.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// Summary of the buy down funds.
        /// </summary>
        [XmlElement("BUYDOWN_FUNDS_SUMMARY", Order = 1)]
        public BUYDOWN_FUNDS_SUMMARY BuydownFundsSummary;

        /// <summary>
        /// Gets or sets a value indicating whether the BuyDownFundsSummary element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BuyDownFundsSummary element has been assigned a value.</value>
        [XmlIgnore]
        public bool BuydownFundsSummarySpecified
        {
            get { return this.BuydownFundsSummary != null && this.BuydownFundsSummary.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public BUYDOWN_FUNDS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
