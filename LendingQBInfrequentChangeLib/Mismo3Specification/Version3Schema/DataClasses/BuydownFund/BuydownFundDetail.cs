namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class BUYDOWN_FUND_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the BUYDOWN_FUND_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BuydownAmountSpecified
                    || this.BuydownFundingTypeOtherDescriptionSpecified
                    || this.BuydownFundingTypeSpecified
                    || this.BuydownPercentSpecified
                    || this.ExtensionSpecified
                    || this.FundsSourceTypeOtherDescriptionSpecified
                    || this.FundsSourceTypeSpecified;
            }
        }

        /// <summary>
        /// The dollar amount associated with the Buy down Funding Type and/or Fund Source Type .
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount BuydownAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the BuyDownAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BuyDownAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool BuydownAmountSpecified
        {
            get { return BuydownAmount != null; }
            set { }
        }

        /// <summary>
        /// The method used for providing the buy down funds.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<BuydownFundingBase> BuydownFundingType;

        /// <summary>
        /// Gets or sets a value indicating whether the BuyDownFundingType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BuyDownFundingType element has been assigned a value.</value>
        [XmlIgnore]
        public bool BuydownFundingTypeSpecified
        {
            get { return this.BuydownFundingType != null && this.BuydownFundingType.enumValue != BuydownFundingBase.Blank; }
            set { }
        }

        /// <summary>
        /// Used to collect additional information when Other is selected as the Buy down Funding Type.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString BuydownFundingTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the BuyDownFundingTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BuyDownFundingTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool BuydownFundingTypeOtherDescriptionSpecified
        {
            get { return BuydownFundingTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The percentage associated with the Buy down Funding Type and/or Buy down Source Type.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOPercent BuydownPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the BuyDownPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BuyDownPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool BuydownPercentSpecified
        {
            get { return BuydownPercent != null; }
            set { }
        }

        /// <summary>
        /// The party providing the associated funds.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<FundsSourceBase> FundsSourceType;

        /// <summary>
        /// Gets or sets a value indicating whether the FundsSourceType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FundsSourceType element has been assigned a value.</value>
        [XmlIgnore]
        public bool FundsSourceTypeSpecified
        {
            get { return this.FundsSourceType != null && this.FundsSourceType.enumValue != FundsSourceBase.Blank; }
            set { }
        }

        /// <summary>
        /// Used to collect additional information when Other is selected for Funds Source Type.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString FundsSourceTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the FundsSourceTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FundsSourceTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool FundsSourceTypeOtherDescriptionSpecified
        {
            get { return FundsSourceTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 6)]
        public BUYDOWN_FUND_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
