namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CREDIT_RESPONSE
    {
        /// <summary>
        /// Gets a value indicating whether the CREDIT_RESPONSE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditBureauSpecified
                    || this.CreditCommentsSpecified
                    || this.CreditConsumerReferralsSpecified
                    || this.CreditErrorMessagesSpecified
                    || this.CreditFilesSpecified
                    || this.CreditInquiriesSpecified
                    || this.CreditLiabilitiesSpecified
                    || this.CreditPublicRecordsSpecified
                    || this.CreditReportPricesSpecified
                    || this.CreditRepositoryIncludedSpecified
                    || this.CreditRequestDataSpecified
                    || this.CreditResponseAlertMessagesSpecified
                    || this.CreditResponseDataInformationSpecified
                    || this.CreditResponseDetailSpecified
                    || this.CreditScoreModelsSpecified
                    || this.CreditScoresSpecified
                    || this.CreditSummariesSpecified
                    || this.CreditTradeReferencesSpecified
                    || this.ExtensionSpecified
                    || this.LoansSpecified
                    || this.PartiesSpecified
                    || this.RegulatoryProductsSpecified;
            }
        }

        /// <summary>
        /// The credit bureau providing information.
        /// </summary>
        [XmlElement("CREDIT_BUREAU", Order = 0)]
        public CREDIT_BUREAU CreditBureau;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditBureau element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditBureau element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditBureauSpecified
        {
            get { return this.CreditBureau != null && this.CreditBureau.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Credit comments in this response.
        /// </summary>
        [XmlElement("CREDIT_COMMENTS", Order = 1)]
        public CREDIT_COMMENTS CreditComments;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditComments element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditComments element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditCommentsSpecified
        {
            get { return this.CreditComments != null && this.CreditComments.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Consumer referrals in this response.
        /// </summary>
        [XmlElement("CREDIT_CONSUMER_REFERRALS", Order = 2)]
        public CREDIT_CONSUMER_REFERRALS CreditConsumerReferrals;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditConsumerReferrals element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditConsumerReferrals element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditConsumerReferralsSpecified
        {
            get { return this.CreditConsumerReferrals != null && this.CreditConsumerReferrals.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Error messages in this response.
        /// </summary>
        [XmlElement("CREDIT_ERROR_MESSAGES", Order = 3)]
        public CREDIT_ERROR_MESSAGES CreditErrorMessages;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditErrorMessages element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditErrorMessages element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditErrorMessagesSpecified
        {
            get { return this.CreditErrorMessages != null && this.CreditErrorMessages.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Credit files in this response.
        /// </summary>
        [XmlElement("CREDIT_FILES", Order = 4)]
        public CREDIT_FILES CreditFiles;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditFiles element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditFiles element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditFilesSpecified
        {
            get { return this.CreditFiles != null && this.CreditFiles.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Credit inquiries in this response.
        /// </summary>
        [XmlElement("CREDIT_INQUIRIES", Order = 5)]
        public CREDIT_INQUIRIES CreditInquiries;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditInquiries element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditInquiries element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditInquiriesSpecified
        {
            get { return this.CreditInquiries != null && this.CreditInquiries.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Liability accounts in this credit response.
        /// </summary>
        [XmlElement("CREDIT_LIABILITIES", Order = 6)]
        public CREDIT_LIABILITIES CreditLiabilities;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLiabilities element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLiabilities element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLiabilitiesSpecified
        {
            get { return this.CreditLiabilities != null && this.CreditLiabilities.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Public records related to the credit response.
        /// </summary>
        [XmlElement("CREDIT_PUBLIC_RECORDS", Order = 7)]
        public CREDIT_PUBLIC_RECORDS CreditPublicRecords;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditPublicRecords element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditPublicRecords element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditPublicRecordsSpecified
        {
            get { return this.CreditPublicRecords != null && this.CreditPublicRecords.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Pricing of the credit report.
        /// </summary>
        [XmlElement("CREDIT_REPORT_PRICES", Order = 8)]
        public CREDIT_REPORT_PRICES CreditReportPrices;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditReportPrices element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditReportPrices element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditReportPricesSpecified
        {
            get { return this.CreditReportPrices != null && this.CreditReportPrices.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The included credit repository.
        /// </summary>
        [XmlElement("CREDIT_REPOSITORY_INCLUDED", Order = 9)]
        public CREDIT_REPOSITORY_INCLUDED CreditRepositoryIncluded;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditRepositoryIncluded element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditRepositoryIncluded element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditRepositoryIncludedSpecified
        {
            get { return this.CreditRepositoryIncluded != null && this.CreditRepositoryIncluded.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Data in the credit request.
        /// </summary>
        [XmlElement("CREDIT_REQUEST_DATA", Order = 10)]
        public CREDIT_REQUEST_DATA CreditRequestData;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditRequestData element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditRequestData element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditRequestDataSpecified
        {
            get { return this.CreditRequestData != null && this.CreditRequestData.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Alert messages in this response.
        /// </summary>
        [XmlElement("CREDIT_RESPONSE_ALERT_MESSAGES", Order = 11)]
        public CREDIT_RESPONSE_ALERT_MESSAGES CreditResponseAlertMessages;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditResponseAlertMessages element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditResponseAlertMessages element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditResponseAlertMessagesSpecified
        {
            get { return this.CreditResponseAlertMessages != null && this.CreditResponseAlertMessages.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Information about this response.
        /// </summary>
        [XmlElement("CREDIT_RESPONSE_DATA_INFORMATION", Order = 12)]
        public CREDIT_RESPONSE_DATA_INFORMATION CreditResponseDataInformation;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditResponseDataInformation element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditResponseDataInformation element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditResponseDataInformationSpecified
        {
            get { return this.CreditResponseDataInformation != null && this.CreditResponseDataInformation.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details about this credit response.
        /// </summary>
        [XmlElement("CREDIT_RESPONSE_DETAIL", Order = 13)]
        public CREDIT_RESPONSE_DETAIL CreditResponseDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditResponseDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditResponseDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditResponseDetailSpecified
        {
            get { return this.CreditResponseDetail != null && this.CreditResponseDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Credit score models in this response.
        /// </summary>
        [XmlElement("CREDIT_SCORE_MODELS", Order = 14)]
        public CREDIT_SCORE_MODELS CreditScoreModels;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditScoreModels element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditScoreModels element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditScoreModelsSpecified
        {
            get { return this.CreditScoreModels != null && this.CreditScoreModels.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Credit scores in this response.
        /// </summary>
        [XmlElement("CREDIT_SCORES", Order = 15)]
        public CREDIT_SCORES CreditScores;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditScores element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditScores element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditScoresSpecified
        {
            get { return this.CreditScores != null && this.CreditScores.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Summaries of this credit response.
        /// </summary>
        [XmlElement("CREDIT_SUMMARIES", Order = 16)]
        public CREDIT_SUMMARIES CreditSummaries;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditSummaries element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditSummaries element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditSummariesSpecified
        {
            get { return this.CreditSummaries != null && this.CreditSummaries.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Trade references for this credit response.
        /// </summary>
        [XmlElement("CREDIT_TRADE_REFERENCES", Order = 17)]
        public CREDIT_TRADE_REFERENCES CreditTradeReferences;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditTradeReferences element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditTradeReferences element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditTradeReferencesSpecified
        {
            get { return this.CreditTradeReferences != null && this.CreditTradeReferences.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Loans related to this credit response.
        /// </summary>
        [XmlElement("LOANS", Order = 18)]
        public LOANS Loans;

        /// <summary>
        /// Gets or sets a value indicating whether the Loans element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Loans element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoansSpecified
        {
            get { return this.Loans != null && this.Loans.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Parties related to this credit response.
        /// </summary>
        [XmlElement("PARTIES", Order = 19)]
        public PARTIES Parties;

        /// <summary>
        /// Gets or sets a value indicating whether the Parties element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Parties element has been assigned a value.</value>
        [XmlIgnore]
        public bool PartiesSpecified
        {
            get { return this.Parties != null && this.Parties.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A regulatory product related to this credit response.
        /// </summary>
        [XmlElement("REGULATORY_PRODUCTS", Order = 20)]
        public REGULATORY_PRODUCTS RegulatoryProducts;

        /// <summary>
        /// Gets or sets a value indicating whether the RegulatoryProducts element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RegulatoryProducts element has been assigned a value.</value>
        [XmlIgnore]
        public bool RegulatoryProductsSpecified
        {
            get { return this.RegulatoryProducts != null && this.RegulatoryProducts.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 21)]
        public CREDIT_RESPONSE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
