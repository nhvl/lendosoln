namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CREDIT_RESPONSE_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the CREDIT_RESPONSE_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditRatingCodeTypeOtherDescriptionSpecified
                    || this.CreditRatingCodeTypeSpecified
                    || this.CreditReportFirstIssuedDateSpecified
                    || this.CreditReportIdentifierSpecified
                    || this.CreditReportLastUpdatedDateSpecified
                    || this.CreditReportMergeTypeSpecified
                    || this.CreditReportTransactionIdentifierSpecified
                    || this.CreditReportTypeOtherDescriptionSpecified
                    || this.CreditReportTypeSpecified
                    || this.ExtensionSpecified
                    || this.PaymentPatternRatingCodeTypeOtherDescriptionSpecified
                    || this.PaymentPatternRatingCodeTypeSpecified;
            }
        }

        /// <summary>
        /// Identifies which system of Manner Of Payment (MOP) rating codes is in the credit report. If Other is selected, then the name of the coding system will appear in the Credit Rating Code Type Other Description.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<CreditRatingCodeBase> CreditRatingCodeType;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditRatingCodeType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditRatingCodeType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditRatingCodeTypeSpecified
        {
            get { return this.CreditRatingCodeType != null && this.CreditRatingCodeType.enumValue != CreditRatingCodeBase.Blank; }
            set { }
        }

        /// <summary>
        /// If the Credit Rating Code Type is set to Other, enter its description in this data attribute.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString CreditRatingCodeTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditRatingCodeTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditRatingCodeTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditRatingCodeTypeOtherDescriptionSpecified
        {
            get { return CreditRatingCodeTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Date and time the referenced credit report was first issued.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMODate CreditReportFirstIssuedDate;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditReportFirstIssuedDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditReportFirstIssuedDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditReportFirstIssuedDateSpecified
        {
            get { return CreditReportFirstIssuedDate != null; }
            set { }
        }

        /// <summary>
        /// A reference number assigned by the credit bureau to a specific credit report. This report number is also referenced when a Reissue, Upgrade, or Status Query of an existing report is requested.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOIdentifier CreditReportIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditReportIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditReportIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditReportIdentifierSpecified
        {
            get { return CreditReportIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The date and time that the referenced credit report was last updated or revised.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMODate CreditReportLastUpdatedDate;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditReportLastUpdatedDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditReportLastUpdatedDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditReportLastUpdatedDateSpecified
        {
            get { return CreditReportLastUpdatedDate != null; }
            set { }
        }

        /// <summary>
        /// This element indicates how duplicate data from multiple sources is handled on a credit report.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOEnum<CreditReportMergeBase> CreditReportMergeType;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditReportMergeType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditReportMergeType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditReportMergeTypeSpecified
        {
            get { return this.CreditReportMergeType != null && this.CreditReportMergeType.enumValue != CreditReportMergeBase.Blank; }
            set { }
        }

        /// <summary>
        /// Uniquely identifies a specific instance of a credit report transaction.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOIdentifier CreditReportTransactionIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditReportTransactionIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditReportTransactionIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditReportTransactionIdentifierSpecified
        {
            get { return CreditReportTransactionIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The type of credit report that was requested or produced. Most common types are Merged report (data from credit data repositories is merged), and RMCR (Residential Mortgage Credit Report - contains verified liabilities, employment, etc.).
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOEnum<CreditReportBase> CreditReportType;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditReportType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditReportType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditReportTypeSpecified
        {
            get { return this.CreditReportType != null && this.CreditReportType.enumValue != CreditReportBase.Blank; }
            set { }
        }

        /// <summary>
        /// When Credit Report Type is set to Other, enter its description in this element.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOString CreditReportTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditReportTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditReportTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditReportTypeOtherDescriptionSpecified
        {
            get { return CreditReportTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Identifies which system of rating codes is used within the Credit Liability Payment Pattern Data text string.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOEnum<PaymentPatternRatingCodeBase> PaymentPatternRatingCodeType;

        /// <summary>
        /// Gets or sets a value indicating whether the PaymentPatternRatingCodeType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PaymentPatternRatingCodeType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PaymentPatternRatingCodeTypeSpecified
        {
            get { return this.PaymentPatternRatingCodeType != null && this.PaymentPatternRatingCodeType.enumValue != PaymentPatternRatingCodeBase.Blank; }
            set { }
        }

        /// <summary>
        /// When Payment Pattern Rating Code Type has a value of Other, this element identifies the rating code system applied to the Credit Liability Payment Pattern Data text string.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOString PaymentPatternRatingCodeTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PaymentPatternRatingCodeTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PaymentPatternRatingCodeTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PaymentPatternRatingCodeTypeOtherDescriptionSpecified
        {
            get { return PaymentPatternRatingCodeTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 11)]
        public CREDIT_RESPONSE_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
