namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class STATUSES
    {
        /// <summary>
        /// Gets a value indicating whether the STATUSES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.StatusSpecified;
            }
        }

        /// <summary>
        /// A collection of statuses.
        /// </summary>
        [XmlElement("STATUS", Order = 0)]
		public List<STATUS> Status = new List<STATUS>();

        /// <summary>
        /// Gets or sets a value indicating whether the Status element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Status element has been assigned a value.</value>
        [XmlIgnore]
        public bool StatusSpecified
        {
            get { return this.Status != null && this.Status.Count(s => s != null && s.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public STATUSES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
