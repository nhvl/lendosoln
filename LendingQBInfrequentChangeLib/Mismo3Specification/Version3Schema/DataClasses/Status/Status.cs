namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class STATUS
    {
        /// <summary>
        /// Gets a value indicating whether the STATUS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.StatusCodeSpecified
                    || this.StatusConditionDescriptionSpecified
                    || this.StatusDescriptionSpecified
                    || this.StatusNameSpecified;
            }
        }

        /// <summary>
        /// Code values exchanged between business partners relating to the condition of a status message.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOCode StatusCode;

        /// <summary>
        /// Gets or sets a value indicating whether the StatusCode element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the StatusCode element has been assigned a value.</value>
        [XmlIgnore]
        public bool StatusCodeSpecified
        {
            get { return StatusCode != null; }
            set { }
        }

        /// <summary>
        /// Describes the severity of a status message.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString StatusConditionDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the StatusConditionDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the StatusConditionDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool StatusConditionDescriptionSpecified
        {
            get { return StatusConditionDescription != null; }
            set { }
        }

        /// <summary>
        /// Text describing specific details of a status message.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString StatusDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the StatusDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the StatusDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool StatusDescriptionSpecified
        {
            get { return StatusDescription != null; }
            set { }
        }

        /// <summary>
        /// Business name for the value shown in the Status Code data element.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString StatusName;

        /// <summary>
        /// Gets or sets a value indicating whether the StatusName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the StatusName element has been assigned a value.</value>
        [XmlIgnore]
        public bool StatusNameSpecified
        {
            get { return StatusName != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public STATUS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
