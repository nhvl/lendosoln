namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class ELECTRONIC_SIGNATURE
    {
        /// <summary>
        /// Gets a value indicating whether the ELECTRONIC_SIGNATURE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ForeignObjectSpecified;
            }
        }

        /// <summary>
        /// A foreign object related to the signature.
        /// </summary>
        [XmlElement("FOREIGN_OBJECT", Order = 0)]
        public FOREIGN_OBJECT ForeignObject;

        /// <summary>
        /// Gets or sets a value indicating whether the ForeignObject element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ForeignObject element has been assigned a value.</value>
        [XmlIgnore]
        public bool ForeignObjectSpecified
        {
            get { return this.ForeignObject != null && this.ForeignObject.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public ELECTRONIC_SIGNATURE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
