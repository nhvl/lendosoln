namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class MODIFICATIONS
    {
        /// <summary>
        /// Gets a value indicating whether the MODIFICATIONS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ModificationSpecified
                    || this.ModificationSummarySpecified;
            }
        }

        /// <summary>
        /// A collection of loan modification.
        /// </summary>
        [XmlElement("MODIFICATION", Order = 0)]
		public List<MODIFICATION> Modification = new List<MODIFICATION>();

        /// <summary>
        /// Gets or sets a value indicating whether the Modification element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Modification element has been assigned a value.</value>
        [XmlIgnore]
        public bool ModificationSpecified
        {
            get { return this.Modification != null && this.Modification.Count(m => m != null && m.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// Summary of a loan modification.
        /// </summary>
        [XmlElement("MODIFICATION_SUMMARY", Order = 1)]
        public MODIFICATION_SUMMARY ModificationSummary;

        /// <summary>
        /// Gets or sets a value indicating whether the ModificationSummary element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ModificationSummary element has been assigned a value.</value>
        [XmlIgnore]
        public bool ModificationSummarySpecified
        {
            get { return this.ModificationSummary != null && this.ModificationSummary.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public MODIFICATIONS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
