namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class MODIFICATION_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the MODIFICATION_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BorrowerModificationClosingCostAmountSpecified
                    || this.ExtensionSpecified
                    || this.ForbearancePrincipalReductionAmountSpecified
                    || this.InterestBearingUPBAmountSpecified
                    || this.InterestForgivenessAmountSpecified
                    || this.LoanModificationActionTypeOtherDescriptionSpecified
                    || this.LoanModificationActionTypeSpecified
                    || this.LoanModificationEffectiveDateSpecified
                    || this.ModificationAdditionalEndorsementIndicatorSpecified
                    || this.ModificationAgreementAcceptanceCutoffDateSpecified
                    || this.ModificationAgreementReceivedIndicatorSpecified
                    || this.ModificationEstimatedRequestProcessingDaysCountSpecified
                    || this.ModificationSignedDateSpecified
                    || this.ModificationSubmissionCutoffDateSpecified
                    || this.ModificationTrialPlanEffectiveDateSpecified
                    || this.ModificationTrialPlanEndDateSpecified
                    || this.ModificationTrialPlanReturnOfDocumentsCutoffDateSpecified
                    || this.NonInterestBearingUPBAmountSpecified
                    || this.PreModificationInterestRatePercentSpecified
                    || this.PreModificationPrincipalAndInterestPaymentAmountSpecified
                    || this.PreModificationTotalPaymentAmountSpecified
                    || this.PreModificationUPBAmountSpecified
                    || this.PrincipalForgivenessAmountSpecified
                    || this.PrincipalReductionOfUnsecuredAmountSpecified
                    || this.ReimbursableServicerModificationCostAmountSpecified
                    || this.ServicerAttestsOnSubmissionIndicatorSpecified
                    || this.ServicerOfficerModificationSignatureDateSpecified
                    || this.TotalForgivenessAmountSpecified
                    || this.TotalStepCountSpecified;
            }
        }

        /// <summary>
        /// The dollar amount of the borrower's portion of closing costs associated with the process of modifying the terms of a mortgage.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount BorrowerModificationClosingCostAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the BorrowerModificationClosingCostAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BorrowerModificationClosingCostAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool BorrowerModificationClosingCostAmountSpecified
        {
            get { return BorrowerModificationClosingCostAmount != null; }
            set { }
        }

        /// <summary>
        /// The amount of the UPB deferred by the lender at modification.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOAmount ForbearancePrincipalReductionAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ForbearancePrincipalReductionAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ForbearancePrincipalReductionAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ForbearancePrincipalReductionAmountSpecified
        {
            get { return ForbearancePrincipalReductionAmount != null; }
            set { }
        }

        /// <summary>
        /// The portion of the UPB on which interest is being collected.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOAmount InterestBearingUPBAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the InterestBearingUPBAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InterestBearingUPBAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool InterestBearingUPBAmountSpecified
        {
            get { return InterestBearingUPBAmount != null; }
            set { }
        }

        /// <summary>
        /// The amount of interest being forgiven by the investor or lender at modification.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOAmount InterestForgivenessAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the InterestForgivenessAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InterestForgivenessAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool InterestForgivenessAmountSpecified
        {
            get { return InterestForgivenessAmount != null; }
            set { }
        }

        /// <summary>
        /// Identifies the modification action that resulted in a change or changes to the loan note terms. 
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<LoanModificationActionBase> LoanModificationActionType;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanModificationActionType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanModificationActionType element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanModificationActionTypeSpecified
        {
            get { return this.LoanModificationActionType != null && this.LoanModificationActionType.enumValue != LoanModificationActionBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Loan Modification Action Type.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString LoanModificationActionTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanModificationActionTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanModificationActionTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanModificationActionTypeOtherDescriptionSpecified
        {
            get { return LoanModificationActionTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The date on which the Modification Agreement goes into effect.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMODate LoanModificationEffectiveDate;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanModificationEffectiveDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanModificationEffectiveDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanModificationEffectiveDateSpecified
        {
            get { return LoanModificationEffectiveDate != null; }
            set { }
        }

        /// <summary>
        /// When true, the lender is required to obtain one or more subordination agreements and/or a Title Insurance Policy document endorsement to ensure the modified loan retains its first lien position.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOIndicator ModificationAdditionalEndorsementIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ModificationAdditionalEndorsementIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ModificationAdditionalEndorsementIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ModificationAdditionalEndorsementIndicatorSpecified
        {
            get { return ModificationAdditionalEndorsementIndicator != null; }
            set { }
        }

        /// <summary>
        /// The date by which the borrower must accept the terms of the modification agreement.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMODate ModificationAgreementAcceptanceCutoffDate;

        /// <summary>
        /// Gets or sets a value indicating whether the ModificationAgreementAcceptanceCutoffDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ModificationAgreementAcceptanceCutoffDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ModificationAgreementAcceptanceCutoffDateSpecified
        {
            get { return ModificationAgreementAcceptanceCutoffDate != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that the modification agreement has been received from the borrower.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOIndicator ModificationAgreementReceivedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ModificationAgreementReceivedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ModificationAgreementReceivedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ModificationAgreementReceivedIndicatorSpecified
        {
            get { return ModificationAgreementReceivedIndicator != null; }
            set { }
        }

        /// <summary>
        /// The estimated number of days required to process the modification request.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOCount ModificationEstimatedRequestProcessingDaysCount;

        /// <summary>
        /// Gets or sets a value indicating whether the ModificationEstimatedRequestProcessingDaysCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ModificationEstimatedRequestProcessingDaysCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ModificationEstimatedRequestProcessingDaysCountSpecified
        {
            get { return ModificationEstimatedRequestProcessingDaysCount != null; }
            set { }
        }

        /// <summary>
        /// The final date on which all borrowers have signed the modification agreement.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMODate ModificationSignedDate;

        /// <summary>
        /// Gets or sets a value indicating whether the ModificationSignedDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ModificationSignedDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ModificationSignedDateSpecified
        {
            get { return ModificationSignedDate != null; }
            set { }
        }

        /// <summary>
        /// The date by which the borrower must submit documentation required to determine whether they are eligible to participate in the modification program.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMODate ModificationSubmissionCutoffDate;

        /// <summary>
        /// Gets or sets a value indicating whether the ModificationSubmissionCutoffDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ModificationSubmissionCutoffDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ModificationSubmissionCutoffDateSpecified
        {
            get { return ModificationSubmissionCutoffDate != null; }
            set { }
        }

        /// <summary>
        /// The date the trial plan goes into effect; also referred to as the beginning of the Loan Modification Trial Period.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMODate ModificationTrialPlanEffectiveDate;

        /// <summary>
        /// Gets or sets a value indicating whether the ModificationTrialPlanEffectiveDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ModificationTrialPlanEffectiveDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ModificationTrialPlanEffectiveDateSpecified
        {
            get { return ModificationTrialPlanEffectiveDate != null; }
            set { }
        }

        /// <summary>
        /// The scheduled end date for the modification trial period.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMODate ModificationTrialPlanEndDate;

        /// <summary>
        /// Gets or sets a value indicating whether the ModificationTrialPlanEndDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ModificationTrialPlanEndDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ModificationTrialPlanEndDateSpecified
        {
            get { return ModificationTrialPlanEndDate != null; }
            set { }
        }

        /// <summary>
        /// The date by which the borrower must send the Loan Modification Trial Plan conditions checklist,  documentation, and first payment.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMODate ModificationTrialPlanReturnOfDocumentsCutoffDate;

        /// <summary>
        /// Gets or sets a value indicating whether the ModificationTrialPlanReturnOfDocumentsCutoffDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ModificationTrialPlanReturnOfDocumentsCutoffDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ModificationTrialPlanReturnOfDocumentsCutoffDateSpecified
        {
            get { return ModificationTrialPlanReturnOfDocumentsCutoffDate != null; }
            set { }
        }

        /// <summary>
        /// The portion of the UPB on which interest is no longer being collected.
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMOAmount NonInterestBearingUPBAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the NonInterestBearingUPBAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NonInterestBearingUPBAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool NonInterestBearingUPBAmountSpecified
        {
            get { return NonInterestBearingUPBAmount != null; }
            set { }
        }

        /// <summary>
        /// The interest rate of the modified loan prior to the modification taking place.
        /// </summary>
        [XmlElement(Order = 17)]
        public MISMOPercent PreModificationInterestRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the PreModificationInterestRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PreModificationInterestRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool PreModificationInterestRatePercentSpecified
        {
            get { return PreModificationInterestRatePercent != null; }
            set { }
        }

        /// <summary>
        /// The principal and interest payment amount of the modified loan prior to the modification taking place.
        /// </summary>
        [XmlElement(Order = 18)]
        public MISMOAmount PreModificationPrincipalAndInterestPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PreModificationPrincipalAndInterestPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PreModificationPrincipalAndInterestPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PreModificationPrincipalAndInterestPaymentAmountSpecified
        {
            get { return PreModificationPrincipalAndInterestPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// The total monthly amount including principal, interest, taxes, insurance and all other housing expenses (HOA, ground rent, etc). prior to the modification taking place. 
        /// </summary>
        [XmlElement(Order = 19)]
        public MISMOAmount PreModificationTotalPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PreModificationTotalPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PreModificationTotalPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PreModificationTotalPaymentAmountSpecified
        {
            get { return PreModificationTotalPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// The unpaid principal balance amount of the modified loan prior to the modification taking place.
        /// </summary>
        [XmlElement(Order = 20)]
        public MISMOAmount PreModificationUPBAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PreModificationUPBAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PreModificationUPBAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PreModificationUPBAmountSpecified
        {
            get { return PreModificationUPBAmount != null; }
            set { }
        }

        /// <summary>
        /// The amount of principal being forgiven by the investor or lender at modification.
        /// </summary>
        [XmlElement(Order = 21)]
        public MISMOAmount PrincipalForgivenessAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PrincipalForgivenessAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrincipalForgivenessAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrincipalForgivenessAmountSpecified
        {
            get { return PrincipalForgivenessAmount != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of principal reduction as result of a bankruptcy due to the bifurcation of the claim.
        /// </summary>
        [XmlElement(Order = 22)]
        public MISMOAmount PrincipalReductionOfUnsecuredAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PrincipalReductionOfUnsecuredAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrincipalReductionOfUnsecuredAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrincipalReductionOfUnsecuredAmountSpecified
        {
            get { return PrincipalReductionOfUnsecuredAmount != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of fees that will be reimbursed to the servicer by the investor, including notary fees, property valuation, and other required fees. 
        /// </summary>
        [XmlElement(Order = 23)]
        public MISMOAmount ReimbursableServicerModificationCostAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ReimbursableServicerModificationCostAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ReimbursableServicerModificationCostAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ReimbursableServicerModificationCostAmountSpecified
        {
            get { return ReimbursableServicerModificationCostAmount != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that the servicer attests that the program guidelines for a modification have been followed.
        /// </summary>
        [XmlElement(Order = 24)]
        public MISMOIndicator ServicerAttestsOnSubmissionIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ServicerAttestsOnSubmissionIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServicerAttestsOnSubmissionIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServicerAttestsOnSubmissionIndicatorSpecified
        {
            get { return ServicerAttestsOnSubmissionIndicator != null; }
            set { }
        }

        /// <summary>
        /// The date on which the Servicers Officer has signed off on the modification agreement.
        /// </summary>
        [XmlElement(Order = 25)]
        public MISMODate ServicerOfficerModificationSignatureDate;

        /// <summary>
        /// Gets or sets a value indicating whether the ServicerOfficerModificationSignatureDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServicerOfficerModificationSignatureDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServicerOfficerModificationSignatureDateSpecified
        {
            get { return ServicerOfficerModificationSignatureDate != null; }
            set { }
        }

        /// <summary>
        /// The total dollar amount forgiven on the loan.
        /// </summary>
        [XmlElement(Order = 26)]
        public MISMOAmount TotalForgivenessAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the TotalForgivenessAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TotalForgivenessAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TotalForgivenessAmountSpecified
        {
            get { return TotalForgivenessAmount != null; }
            set { }
        }

        /// <summary>
        /// For a loan with a step provision, the total number of step rate adjustments allowed under the step agreement.
        /// </summary>
        [XmlElement(Order = 27)]
        public MISMOCount TotalStepCount;

        /// <summary>
        /// Gets or sets a value indicating whether the TotalStepCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TotalStepCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TotalStepCountSpecified
        {
            get { return TotalStepCount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 28)]
        public MODIFICATION_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
