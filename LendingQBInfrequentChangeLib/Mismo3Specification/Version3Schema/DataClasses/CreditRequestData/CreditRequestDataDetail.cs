namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CREDIT_REQUEST_DATA_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the CREDIT_REQUEST_DATA_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditRatingCodeTypeOtherDescriptionSpecified
                    || this.CreditRatingCodeTypeSpecified
                    || this.CreditReportIdentifierSpecified
                    || this.CreditReportProductDescriptionSpecified
                    || this.CreditReportRequestActionTypeOtherDescriptionSpecified
                    || this.CreditReportRequestActionTypeSpecified
                    || this.CreditReportTransactionIdentifierSpecified
                    || this.CreditReportTypeOtherDescriptionSpecified
                    || this.CreditReportTypeSpecified
                    || this.CreditRepositoriesSelectedCountSpecified
                    || this.CreditRequestDatetimeSpecified
                    || this.CreditRequestTypeSpecified
                    || this.ExtensionSpecified
                    || this.InitialCreditReportFirstIssuedDateSpecified
                    || this.PaymentPatternRatingCodeTypeOtherDescriptionSpecified
                    || this.PaymentPatternRatingCodeTypeSpecified;
            }
        }

        /// <summary>
        /// Identifies which system of Manner Of Payment (MOP) rating codes is in the credit report. If Other is selected, then the name of the coding system will appear in the Credit Rating Code Type Other Description.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<CreditRatingCodeBase> CreditRatingCodeType;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditRatingCodeType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditRatingCodeType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditRatingCodeTypeSpecified
        {
            get { return this.CreditRatingCodeType != null && this.CreditRatingCodeType.enumValue != CreditRatingCodeBase.Blank; }
            set { }
        }

        /// <summary>
        /// If the Credit Rating Code Type is set to Other, enter its description in this data attribute.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString CreditRatingCodeTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditRatingCodeTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditRatingCodeTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditRatingCodeTypeOtherDescriptionSpecified
        {
            get { return CreditRatingCodeTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A reference number assigned by the credit bureau to a specific credit report. This report number is also referenced when a Reissue, Upgrade, or Status Query of an existing report is requested.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOIdentifier CreditReportIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditReportIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditReportIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditReportIdentifierSpecified
        {
            get { return CreditReportIdentifier != null; }
            set { }
        }

        /// <summary>
        /// This is additional information that more completely describes the credit report product being requested.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString CreditReportProductDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditReportProductDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditReportProductDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditReportProductDescriptionSpecified
        {
            get { return CreditReportProductDescription != null; }
            set { }
        }

        /// <summary>
        /// Select the type of service action being requested. If the service action refers to an existing report, specify which one in the Credit Report Identifier.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<CreditReportRequestActionBase> CreditReportRequestActionType;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditReportRequestActionType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditReportRequestActionType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditReportRequestActionTypeSpecified
        {
            get { return this.CreditReportRequestActionType != null && this.CreditReportRequestActionType.enumValue != CreditReportRequestActionBase.Blank; }
            set { }
        }

        /// <summary>
        /// This describes the desired request action when the Credit Report Request Action Type is set to Other.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString CreditReportRequestActionTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditReportRequestActionTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditReportRequestActionTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditReportRequestActionTypeOtherDescriptionSpecified
        {
            get { return CreditReportRequestActionTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Uniquely identifies a specific instance of a credit report transaction.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOIdentifier CreditReportTransactionIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditReportTransactionIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditReportTransactionIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditReportTransactionIdentifierSpecified
        {
            get { return CreditReportTransactionIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The type of credit report that was requested or produced. Most common types are Merged report (data from credit data repositories is merged), and RMCR (Residential Mortgage Credit Report - contains verified liabilities, employment, etc.).
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOEnum<CreditReportBase> CreditReportType;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditReportType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditReportType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditReportTypeSpecified
        {
            get { return this.CreditReportType != null && this.CreditReportType.enumValue != CreditReportBase.Blank; }
            set { }
        }

        /// <summary>
        /// When Credit Report Type is set to Other, enter its description in this element.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOString CreditReportTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditReportTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditReportTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditReportTypeOtherDescriptionSpecified
        {
            get { return CreditReportTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// This element stores the number of repository bureaus desired for a credit request. When a requestor uses this element, they are selecting the number of repositories to be pulled, not specific bureaus. The credit bureau would then choose which bureaus to be pulled based on a methodology agreed on with their customer (for example, zip code or customer preference).
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOCount CreditRepositoriesSelectedCount;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditRepositoriesSelectedCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditRepositoriesSelectedCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditRepositoriesSelectedCountSpecified
        {
            get { return CreditRepositoriesSelectedCount != null; }
            set { }
        }

        /// <summary>
        /// A system generated date and time stamp enclosed within each request to identify the initiation time of the credit request.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMODatetime CreditRequestDatetime;

        /// <summary>
        /// Gets or sets a value indicating whether the Credit Request date/time element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Credit Request date/time element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditRequestDatetimeSpecified
        {
            get { return CreditRequestDatetime != null; }
            set { }
        }

        /// <summary>
        /// The Credit Request Type specifies either an Individual Report on one person, or a Joint Report on two married persons.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOEnum<CreditRequestBase> CreditRequestType;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditRequestType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditRequestType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditRequestTypeSpecified
        {
            get { return this.CreditRequestType != null && this.CreditRequestType.enumValue != CreditRequestBase.Blank; }
            set { }
        }

        /// <summary>
        /// The date on which the initial credit report for this loan was first issued.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMODate InitialCreditReportFirstIssuedDate;

        /// <summary>
        /// Gets or sets a value indicating whether the InitialCreditReportFirstIssuedDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InitialCreditReportFirstIssuedDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool InitialCreditReportFirstIssuedDateSpecified
        {
            get { return InitialCreditReportFirstIssuedDate != null; }
            set { }
        }

        /// <summary>
        /// Identifies which system of rating codes is used within the Credit Liability Payment Pattern Data text string.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOEnum<PaymentPatternRatingCodeBase> PaymentPatternRatingCodeType;

        /// <summary>
        /// Gets or sets a value indicating whether the PaymentPatternRatingCodeType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PaymentPatternRatingCodeType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PaymentPatternRatingCodeTypeSpecified
        {
            get { return this.PaymentPatternRatingCodeType != null && this.PaymentPatternRatingCodeType.enumValue != PaymentPatternRatingCodeBase.Blank; }
            set { }
        }

        /// <summary>
        /// When Payment Pattern Rating Code Type has a value of Other, this element identifies the rating code system applied to the Credit Liability Payment Pattern Data text string.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOString PaymentPatternRatingCodeTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PaymentPatternRatingCodeTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PaymentPatternRatingCodeTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PaymentPatternRatingCodeTypeOtherDescriptionSpecified
        {
            get { return PaymentPatternRatingCodeTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 15)]
        public CREDIT_REQUEST_DATA_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
