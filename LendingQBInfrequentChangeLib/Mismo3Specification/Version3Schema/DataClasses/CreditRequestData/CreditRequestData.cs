namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class CREDIT_REQUEST_DATA
    {
        /// <summary>
        /// Gets a value indicating whether the CREDIT_REQUEST_DATA container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AdditionalEndUsersSpecified
                    || this.CreditFreezePinsSpecified
                    || this.CreditRepositoryIncludedSpecified
                    || this.CreditRequestDataDetailSpecified
                    || this.CreditScoreModelsSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// Additional end users of this data.
        /// </summary>
        [XmlElement("ADDITIONAL_END_USERS", Order = 0)]
        public ADDITIONAL_END_USERS AdditionalEndUsers;

        /// <summary>
        /// Gets or sets a value indicating whether the AdditionalEndUsers element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AdditionalEndUsers element has been assigned a value.</value>
        [XmlIgnore]
        public bool AdditionalEndUsersSpecified
        {
            get { return this.AdditionalEndUsers != null && this.AdditionalEndUsers.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The credit freeze PINs for this data.
        /// </summary>
        [XmlElement("CREDIT_FREEZE_PINS", Order = 1)]
        public CREDIT_FREEZE_PINS CreditFreezePins;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditFreezePins element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditFreezePins element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditFreezePinsSpecified
        {
            get { return this.CreditFreezePins != null && this.CreditFreezePins.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The repository of the credit request data.
        /// </summary>
        [XmlElement("CREDIT_REPOSITORY_INCLUDED", Order = 2)]
        public CREDIT_REPOSITORY_INCLUDED CreditRepositoryIncluded;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditRepositoryIncluded element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditRepositoryIncluded element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditRepositoryIncludedSpecified
        {
            get { return this.CreditRepositoryIncluded != null && this.CreditRepositoryIncluded.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details about the credit request data.
        /// </summary>
        [XmlElement("CREDIT_REQUEST_DATA_DETAIL", Order = 3)]
        public CREDIT_REQUEST_DATA_DETAIL CreditRequestDataDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditRequestDataDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditRequestDataDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditRequestDataDetailSpecified
        {
            get { return this.CreditRequestDataDetail != null && this.CreditRequestDataDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The models of the credit score data.
        /// </summary>
        [XmlElement("CREDIT_SCORE_MODELS", Order = 4)]
        public CREDIT_SCORE_MODELS CreditScoreModels;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditScoreModels element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditScoreModels element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditScoreModelsSpecified
        {
            get { return this.CreditScoreModels != null && this.CreditScoreModels.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 5)]
        public CREDIT_REQUEST_DATA_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
