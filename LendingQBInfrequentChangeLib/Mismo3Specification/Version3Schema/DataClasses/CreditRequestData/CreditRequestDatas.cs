namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CREDIT_REQUEST_DATAS
    {
        /// <summary>
        /// Gets a value indicating whether the CREDIT_REQUEST_DATAS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditRequestDataSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of credit request data.
        /// </summary>
        [XmlElement("CREDIT_REQUEST_DATA", Order = 0)]
		public List<CREDIT_REQUEST_DATA> CreditRequestData = new List<CREDIT_REQUEST_DATA>();

        /// <summary>
        /// Gets or sets a value indicating whether the CreditRequestData element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditRequestData element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditRequestDataSpecified
        {
            get { return this.CreditRequestData != null && this.CreditRequestData.Count(c => c != null && c.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public CREDIT_REQUEST_DATAS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
