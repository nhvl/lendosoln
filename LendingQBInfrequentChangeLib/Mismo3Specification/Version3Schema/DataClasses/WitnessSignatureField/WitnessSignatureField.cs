namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class WITNESS_SIGNATURE_FIELD
    {
        /// <summary>
        /// Gets a value indicating whether the WITNESS_SIGNATURE_FIELD container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.SignatureFieldReferenceSpecified
                    || this.WitnessSignatureDetailSpecified;
            }
        }

        /// <summary>
        /// Field reference for a signature.
        /// </summary>
        [XmlElement("SIGNATURE_FIELD_REFERENCE", Order = 0)]
        public FIELD_REFERENCE SignatureFieldReference;

        /// <summary>
        /// Gets or sets a value indicating whether the SignatureFieldReference element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SignatureFieldReference element has been assigned a value.</value>
        [XmlIgnore]
        public bool SignatureFieldReferenceSpecified
        {
            get { return this.SignatureFieldReference != null && this.SignatureFieldReference.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details on a witness signature.
        /// </summary>
        [XmlElement("WITNESS_SIGNATURE_DETAIL", Order = 1)]
        public WITNESS_SIGNATURE_DETAIL WitnessSignatureDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the WitnessSignatureDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the WitnessSignatureDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool WitnessSignatureDetailSpecified
        {
            get { return this.WitnessSignatureDetail != null && this.WitnessSignatureDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public WITNESS_SIGNATURE_FIELD_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
