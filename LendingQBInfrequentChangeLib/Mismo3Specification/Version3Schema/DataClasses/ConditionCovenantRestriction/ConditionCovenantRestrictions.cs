namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CONDITION_COVENANT_RESTRICTIONS
    {
        /// <summary>
        /// Gets a value indicating whether the CONDITION_COVENANT_RESTRICTIONS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ConditionCovenantRestrictionSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of condition covenant restrictions.
        /// </summary>
        [XmlElement("CONDITION_COVENANT_RESTRICTION", Order = 0)]
		public List<CONDITION_COVENANT_RESTRICTION> ConditionCovenantRestriction = new List<CONDITION_COVENANT_RESTRICTION>();

        /// <summary>
        /// Gets or sets a value indicating whether the ConditionCovenantRestriction element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConditionCovenantRestriction element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConditionCovenantRestrictionSpecified
        {
            get { return this.ConditionCovenantRestriction != null && this.ConditionCovenantRestriction.Count(c => c != null && c.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public CONDITION_COVENANT_RESTRICTIONS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
