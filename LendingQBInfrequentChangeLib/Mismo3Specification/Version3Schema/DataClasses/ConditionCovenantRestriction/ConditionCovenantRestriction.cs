namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class CONDITION_COVENANT_RESTRICTION
    {
        /// <summary>
        /// Gets a value indicating whether the CONDITION_COVENANT_RESTRICTION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CovenantDescriptionSpecified
                    || this.CovenantTypeOtherDescriptionSpecified
                    || this.CovenantTypeSpecified
                    || this.ExtensionSpecified
                    || this.RestrictionDescriptionSpecified
                    || this.RestrictionTypeOtherDescriptionSpecified
                    || this.RestrictionTypeSpecified
                    || this.ServitudeSignificanceTypeSpecified;
            }
        }

        /// <summary>
        /// Free-form description of the details regarding the covenant indicated.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString CovenantDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the CovenantDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CovenantDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool CovenantDescriptionSpecified
        {
            get { return CovenantDescription != null; }
            set { }
        }

        /// <summary>
        /// A covenant potentially provides benefits or burdens to the ownership bundle of property rights usually associated with the community in which the property is located. 
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<CovenantBase> CovenantType;

        /// <summary>
        /// Gets or sets a value indicating whether the CovenantType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CovenantType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CovenantTypeSpecified
        {
            get { return this.CovenantType != null && this.CovenantType.enumValue != CovenantBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Covenant Type.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString CovenantTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the CovenantTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CovenantTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool CovenantTypeOtherDescriptionSpecified
        {
            get { return CovenantTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Free-form text description for restrictions on the property.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString RestrictionDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the RestrictionDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RestrictionDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool RestrictionDescriptionSpecified
        {
            get { return RestrictionDescription != null; }
            set { }
        }

        /// <summary>
        /// A type of restriction that could impact the value or use of the property.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<RestrictionBase> RestrictionType;

        /// <summary>
        /// Gets or sets a value indicating whether the RestrictionType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RestrictionType element has been assigned a value.</value>
        [XmlIgnore]
        public bool RestrictionTypeSpecified
        {
            get { return this.RestrictionType != null && this.RestrictionType.enumValue != RestrictionBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Restriction Type.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString RestrictionTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the RestrictionTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RestrictionTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool RestrictionTypeOtherDescriptionSpecified
        {
            get { return RestrictionTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates the significance of the restriction is considered to have for the purpose in which it is being reported.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOEnum<ServitudeSignificanceBase> ServitudeSignificanceType;

        /// <summary>
        /// Gets or sets a value indicating whether the ServitudeSignificanceType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServitudeSignificanceType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServitudeSignificanceTypeSpecified
        {
            get { return this.ServitudeSignificanceType != null && this.ServitudeSignificanceType.enumValue != ServitudeSignificanceBase.Blank; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 7)]
        public CONDITION_COVENANT_RESTRICTION_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
