namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class HIGH_COST_MORTGAGES
    {
        /// <summary>
        /// Gets a value indicating whether the HIGH_COST_MORTGAGES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.HighCostMortgageSpecified;
            }
        }

        /// <summary>
        /// A collection of high cost mortgages.
        /// </summary>
        [XmlElement("HIGH_COST_MORTGAGE", Order = 0)]
		public List<HIGH_COST_MORTGAGE> HighCostMortgage = new List<HIGH_COST_MORTGAGE>();

        /// <summary>
        /// Gets or sets a value indicating whether the HighCostMortgage element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HighCostMortgage element has been assigned a value.</value>
        [XmlIgnore]
        public bool HighCostMortgageSpecified
        {
            get { return this.HighCostMortgage != null && this.HighCostMortgage.Count(h => h != null && h.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public HIGH_COST_MORTGAGES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
