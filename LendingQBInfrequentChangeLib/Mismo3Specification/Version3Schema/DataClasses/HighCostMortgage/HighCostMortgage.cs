namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class HIGH_COST_MORTGAGE
    {
        /// <summary>
        /// Gets a value indicating whether the HIGH_COST_MORTGAGE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AveragePrimeOfferRatePercentSpecified
                    || this.ExtensionSpecified
                    || this.HighCostJurisdictionNameSpecified
                    || this.HighCostJurisdictionTypeOtherDescriptionSpecified
                    || this.HighCostJurisdictionTypeSpecified
                    || this.HOEPA_APRInterestRatePercentSpecified
                    || this.HOEPARateThresholdPercentSpecified
                    || this.HOEPATotalPointsAndFeesThresholdAmountSpecified
                    || this.HOEPATotalPointsAndFeesThresholdPercentSpecified
                    || this.PreDiscountedInterestRatePercentSpecified
                    || this.RegulationZExcludedBonaFideDiscountPointsIndicatorSpecified
                    || this.RegulationZExcludedBonaFideDiscountPointsPercentSpecified
                    || this.RegulationZHighCostLoanDisclosureDeliveryDateSpecified
                    || this.RegulationZTotalAffiliateFeesAmountSpecified
                    || this.RegulationZTotalLoanAmountSpecified
                    || this.RegulationZTotalPointsAndFeesAmountSpecified
                    || this.RegulationZTotalPointsAndFeesPercentSpecified;
            }
        }

        /// <summary>
        /// An annual percentage rate that is derived from average interest rates, points, and other loan pricing terms currently offered to consumers by a representative sample of creditors for mortgage transactions that have low-risk pricing characteristics. The Federal Reserve Board publishes average prime offer rates for a broad range of types of transactions in a table updated at least weekly. This rate is used in determining Higher Priced Mortgage Loans (HPML).
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOPercent AveragePrimeOfferRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the AveragePrimeOfferRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AveragePrimeOfferRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool AveragePrimeOfferRatePercentSpecified
        {
            get { return AveragePrimeOfferRatePercent != null; }
            set { }
        }

        /// <summary>
        /// The name of the jurisdiction whose high cost rules are being applied for high cost determination purposes.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString HighCostJurisdictionName;

        /// <summary>
        /// Gets or sets a value indicating whether the HighCostJurisdictionName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HighCostJurisdictionName element has been assigned a value.</value>
        [XmlIgnore]
        public bool HighCostJurisdictionNameSpecified
        {
            get { return HighCostJurisdictionName != null; }
            set { }
        }

        /// <summary>
        /// The type of the jurisdiction whose high cost rules are being applied for high cost determination purposes.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<HighCostJurisdictionBase> HighCostJurisdictionType;

        /// <summary>
        /// Gets or sets a value indicating whether the HighCostJurisdictionType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HighCostJurisdictionType element has been assigned a value.</value>
        [XmlIgnore]
        public bool HighCostJurisdictionTypeSpecified
        {
            get { return this.HighCostJurisdictionType != null && this.HighCostJurisdictionType.enumValue != HighCostJurisdictionBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for High Cost Jurisdiction Type.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString HighCostJurisdictionTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the HighCostJurisdictionTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HighCostJurisdictionTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool HighCostJurisdictionTypeOtherDescriptionSpecified
        {
            get { return HighCostJurisdictionTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The interest rate percentage used in calculating the HOEPA APR.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOPercent HOEPA_APRInterestRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the HOEPA_APRInterestRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HOEPA_APRInterestRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool HOEPA_APRInterestRatePercentSpecified
        {
            get { return HOEPA_APRInterestRatePercent != null; }
            set { }
        }

        /// <summary>
        /// The rate limit established by Regulation Z, Truth in Lending. This amount is used for HOEPA high cost mortgage loan determination purposes.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOPercent HOEPARateThresholdPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the HOEPARateThresholdPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HOEPARateThresholdPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool HOEPARateThresholdPercentSpecified
        {
            get { return HOEPARateThresholdPercent != null; }
            set { }
        }

        /// <summary>
        /// The rate limit established by Regulation Z, Truth in Lending. This amount is used for HOEPA high cost mortgage loan determination purposes. 
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOAmount HOEPATotalPointsAndFeesThresholdAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the HOEPATotalPointsAndFeesThresholdAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HOEPATotalPointsAndFeesThresholdAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool HOEPATotalPointsAndFeesThresholdAmountSpecified
        {
            get { return HOEPATotalPointsAndFeesThresholdAmount != null; }
            set { }
        }

        /// <summary>
        /// The total amount of points and fees percentage limit established by Regulation Z, Truth in Lending. This amount is used for HOEPA high cost mortgage loan determination purposes. 
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOPercent HOEPATotalPointsAndFeesThresholdPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the HOEPATotalPointsAndFeesThresholdPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HOEPATotalPointsAndFeesThresholdPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool HOEPATotalPointsAndFeesThresholdPercentSpecified
        {
            get { return HOEPATotalPointsAndFeesThresholdPercent != null; }
            set { }
        }

        /// <summary>
        /// The interest rate prior to the application of discounts points.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOPercent PreDiscountedInterestRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the PreDiscountedInterestRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PreDiscountedInterestRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool PreDiscountedInterestRatePercentSpecified
        {
            get { return PreDiscountedInterestRatePercent != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that excluded bona fide discount points were charged for this loan.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOIndicator RegulationZExcludedBonaFideDiscountPointsIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the RegulationZExcludedBonaFideDiscountPointsIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RegulationZExcludedBonaFideDiscountPointsIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool RegulationZExcludedBonaFideDiscountPointsIndicatorSpecified
        {
            get { return RegulationZExcludedBonaFideDiscountPointsIndicator != null; }
            set { }
        }

        /// <summary>
        /// The bona fide discount points, expressed as a percent, that are excluded from points and fees calculated in accordance with Regulation Z, Truth in Lending.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOPercent RegulationZExcludedBonaFideDiscountPointsPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the RegulationZExcludedBonaFideDiscountPointsPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RegulationZExcludedBonaFideDiscountPointsPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool RegulationZExcludedBonaFideDiscountPointsPercentSpecified
        {
            get { return RegulationZExcludedBonaFideDiscountPointsPercent != null; }
            set { }
        }

        /// <summary>
        /// The date the Regulation Z (Section 32) High Cost Disclosure was delivered to the borrower.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMODate RegulationZHighCostLoanDisclosureDeliveryDate;

        /// <summary>
        /// Gets or sets a value indicating whether the RegulationZHighCostLoanDisclosureDeliveryDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RegulationZHighCostLoanDisclosureDeliveryDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool RegulationZHighCostLoanDisclosureDeliveryDateSpecified
        {
            get { return RegulationZHighCostLoanDisclosureDeliveryDate != null; }
            set { }
        }

        /// <summary>
        /// The total amount of fees, expressed in dollars, paid to an affiliate(s) of the creditor.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOAmount RegulationZTotalAffiliateFeesAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the RegulationZTotalAffiliateFeesAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RegulationZTotalAffiliateFeesAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool RegulationZTotalAffiliateFeesAmountSpecified
        {
            get { return RegulationZTotalAffiliateFeesAmount != null; }
            set { }
        }

        /// <summary>
        /// The total loan amount, expressed in dollars, calculated in accordance with Regulation Z, Truth in Lending. This amount is used in calculations for both Qualified Mortgage and HOEPA.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOAmount RegulationZTotalLoanAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the RegulationZTotalLoanAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RegulationZTotalLoanAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool RegulationZTotalLoanAmountSpecified
        {
            get { return RegulationZTotalLoanAmount != null; }
            set { }
        }

        /// <summary>
        /// The total amount of points and fees, expressed in dollars, calculated in accordance with Regulation Z, Truth in Lending.  This amount is used in calculations for both Qualified Mortgage and HOEPA purposes.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOAmount RegulationZTotalPointsAndFeesAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the RegulationZTotalPointsAndFeesAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RegulationZTotalPointsAndFeesAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool RegulationZTotalPointsAndFeesAmountSpecified
        {
            get { return RegulationZTotalPointsAndFeesAmount != null; }
            set { }
        }

        /// <summary>
        /// The total amount of points and fees, expressed as a percent of the Total Loan Amount, calculated in accordance with Regulation Z, Truth in Lending.  This percent is used in calculations for both Qualified Mortgage and HOEPA purposes.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOPercent RegulationZTotalPointsAndFeesPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the RegulationZTotalPointsAndFeesPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RegulationZTotalPointsAndFeesPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool RegulationZTotalPointsAndFeesPercentSpecified
        {
            get { return RegulationZTotalPointsAndFeesPercent != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 16)]
        public HIGH_COST_MORTGAGE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
