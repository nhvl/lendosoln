namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class ESTIMATED_PROPERTY_COST_COMPONENTS
    {
        /// <summary>
        /// Gets a value indicating whether the ESTIMATED_PROPERTY_COST_COMPONENTS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.EstimatedPropertyCostComponentSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of estimated property cost components.
        /// </summary>
        [XmlElement("ESTIMATED_PROPERTY_COST_COMPONENT", Order = 0)]
		public List<ESTIMATED_PROPERTY_COST_COMPONENT> EstimatedPropertyCostComponent = new List<ESTIMATED_PROPERTY_COST_COMPONENT>();

        /// <summary>
        /// Gets or sets a value indicating whether the EstimatedPropertyCostComponent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EstimatedPropertyCostComponent element has been assigned a value.</value>
        [XmlIgnore]
        public bool EstimatedPropertyCostComponentSpecified
        {
            get { return this.EstimatedPropertyCostComponent != null && this.EstimatedPropertyCostComponent.Count(e => e != null && e.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public ESTIMATED_PROPERTY_COST_COMPONENTS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
