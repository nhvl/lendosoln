namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class ESTIMATED_PROPERTY_COST_COMPONENT
    {
        /// <summary>
        /// Gets a value indicating whether the ESTIMATED_PROPERTY_COST_COMPONENT container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ProjectedPaymentEscrowedTypeSpecified
                    || this.ProjectedPaymentEstimatedTaxesInsuranceAssessmentComponentTypeOtherDescriptionSpecified
                    || this.ProjectedPaymentEstimatedTaxesInsuranceAssessmentComponentTypeSpecified;
            }
        }

        /// <summary>
        /// Specifies if the projected payment item is to be paid from an escrow account.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<ProjectedPaymentEscrowedBase> ProjectedPaymentEscrowedType;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectedPaymentEscrowedType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectedPaymentEscrowedType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectedPaymentEscrowedTypeSpecified
        {
            get { return this.ProjectedPaymentEscrowedType != null && this.ProjectedPaymentEscrowedType.enumValue != ProjectedPaymentEscrowedBase.Blank; }
            set { }
        }

        /// <summary>
        /// Specifies the type of payment component included in the calculation of the Projected Payment Estimated Taxes Insurance And Assessment Total Amount.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<ProjectedPaymentEstimatedTaxesInsuranceAssessmentComponentBase> ProjectedPaymentEstimatedTaxesInsuranceAssessmentComponentType;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectedPaymentEstimatedTaxesInsuranceAssessmentComponentType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectedPaymentEstimatedTaxesInsuranceAssessmentComponentType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectedPaymentEstimatedTaxesInsuranceAssessmentComponentTypeSpecified
        {
            get { return this.ProjectedPaymentEstimatedTaxesInsuranceAssessmentComponentType != null && this.ProjectedPaymentEstimatedTaxesInsuranceAssessmentComponentType.enumValue != ProjectedPaymentEstimatedTaxesInsuranceAssessmentComponentBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture additional information when Other is selected for Projected Payment Estimated Taxes Insurance Assessment Component Type.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString ProjectedPaymentEstimatedTaxesInsuranceAssessmentComponentTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectedPaymentEstimatedTaxesInsuranceAssessmentComponentTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectedPaymentEstimatedTaxesInsuranceAssessmentComponentTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectedPaymentEstimatedTaxesInsuranceAssessmentComponentTypeOtherDescriptionSpecified
        {
            get { return ProjectedPaymentEstimatedTaxesInsuranceAssessmentComponentTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public ESTIMATED_PROPERTY_COST_COMPONENT_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
