namespace Mismo3Specification.Version3Schema
{
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class INVESTOR_LOAN_INFORMATION
    {
        /// <summary>
        /// Gets a value indicating whether the INVESTOR_LOAN_INFORMATION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BaseGuarantyFeePercentSpecified
                    || this.ConcurrentServicingTransferIndicatorSpecified
                    || this.ExceptionInterestAdjustmentDayMethodTypeSpecified
                    || this.ExcessServicingFeeRatePercentSpecified
                    || this.ExecutionTypeOtherDescriptionSpecified
                    || this.ExecutionTypeSpecified
                    || this.ExtensionSpecified
                    || this.FREPrimaryMortgageMarketSurveyPublishedDateSpecified
                    || this.FREPrimaryMortgageMarketSurveyPublishedRatePercentSpecified
                    || this.FREPrimaryMortgageMarketSurveyRoundedRatePercentSpecified
                    || this.FundingInterestAdjustmentDayMethodTypeSpecified
                    || this.GuarantyFeeAddOnIndicatorSpecified
                    || this.GuarantyFeeAfterAlternatePaymentMethodPercentSpecified
                    || this.GuarantyFeePercentSpecified
                    || this.InterestVarianceToleranceMaximumAmountSpecified
                    || this.InvestorCollateralProgramIdentifierSpecified
                    || this.InvestorCommitmentTakeoutPricePercentSpecified
                    || this.InvestorCreditPolicyEffectiveDateSpecified
                    || this.InvestorDealIdentifierSpecified
                    || this.InvestorGroupingIdentifierSpecified
                    || this.InvestorGuarantyFeeAfterBuyupBuydownPercentSpecified
                    || this.InvestorHeldCollateralPercentSpecified
                    || this.InvestorInstitutionIdentifierSpecified
                    || this.InvestorLoanCommitmentExpirationDateSpecified
                    || this.InvestorLoanEarlyPayoffPenaltyWaiverIndicatorSpecified
                    || this.InvestorLoanEffectiveDateSpecified
                    || this.InvestorLoanPartialPrepaymentPenaltyWaiverIndicatorSpecified
                    || this.InvestorMasterCommitmentIdentifierSpecified
                    || this.InvestorOwnershipPercentSpecified
                    || this.InvestorOwnershipTypeSpecified
                    || this.InvestorProductPlanIdentifierSpecified
                    || this.InvestorRemittanceDaysCountSpecified
                    || this.InvestorRemittanceDaySpecified
                    || this.InvestorRemittanceIdentifierSpecified
                    || this.InvestorRemittanceTypeOtherDescriptionSpecified
                    || this.InvestorRemittanceTypeSpecified
                    || this.InvestorReportingMethodTypeSpecified
                    || this.InvestorRequiredMarginPercentSpecified
                    || this.InvestorServicingFeeAmountSpecified
                    || this.InvestorServicingFeeRatePercentSpecified
                    || this.LastReportedToInvestorPrincipalBalanceAmountSpecified
                    || this.LateDeliveryGraceDaysCountSpecified
                    || this.LenderTargetFundingDateSpecified
                    || this.LoanAcquisitionScheduledUPBAmountSpecified
                    || this.LoanBuyupBuydownBasisPointNumberSpecified
                    || this.LoanBuyupBuydownTypeOtherDescriptionSpecified
                    || this.LoanBuyupBuydownTypeSpecified
                    || this.LoanDefaultedReportingFrequencyTypeSpecified
                    || this.LoanDefaultLossPartyTypeSpecified
                    || this.LoanDelinquencyAdvanceDaysCountSpecified
                    || this.LoanFundingAdvancedAmountSpecified
                    || this.LoanPriorOwnerNameSpecified
                    || this.LoanPurchaseDiscountAmountSpecified
                    || this.LoanPurchasePremiumAmountSpecified
                    || this.LoanServicingFeeBasisPointNumberSpecified
                    || this.LoanServicingIndicatorSpecified
                    || this.LoanServicingRightsPurchaseOnlyIndicatorSpecified
                    || this.LoanYieldSpreadPremiumAmountSpecified
                    || this.LoanYieldSpreadPremiumRatePercentSpecified
                    || this.MICoverageServicingFeeRatePercentSpecified
                    || this.NonStandardServicingFeeRetainedByServicerAmountSpecified
                    || this.PassThroughCalculationMethodTypeSpecified
                    || this.PassThroughCeilingRatePercentSpecified
                    || this.PassThroughRatePercentSpecified
                    || this.PrincipalVarianceToleranceMaximumAmountSpecified
                    || this.RelatedInvestorLoanIdentifierSpecified
                    || this.RelatedLoanInvestorTypeOtherDescriptionSpecified
                    || this.RelatedLoanInvestorTypeSpecified
                    || this.RemoveFromTransferIndicatorSpecified
                    || this.REOMarketingPartyTypeOtherDescriptionSpecified
                    || this.REOMarketingPartyTypeSpecified
                    || this.ServicingFeeMinimumRatePercentSpecified
                    || this.ThirdPartyLoanAcquisitionPriceAmountSpecified
                    || this.UPBVarianceToleranceMaximumAmountSpecified
                    || this.WarehouseAdvanceAmountSpecified
                    || this.WarehouseCommitmentSubcategoryIdentifierSpecified
                    || this.WholeLoanInterestDueAmountSpecified
                    || this.WholeLoanPrincipalAmountSpecified
                    || this.WholeLoanSaleDateSpecified;
            }
        }

        /// <summary>
        /// The guaranty fee rate prior to applying any adjustments, such as buy up/buy down. This can be specified in a price sheet, commitment, or other agreement. The guaranty fee is a portion of the interest on the loan that is paid to a party to ensure the timely payment of principal and interest to the holders of securities backed by the loan.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOPercent BaseGuarantyFeePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the BaseGuarantyFeePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BaseGuarantyFeePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool BaseGuarantyFeePercentSpecified
        {
            get { return BaseGuarantyFeePercent != null; }
            set { }
        }

        /// <summary>
        /// Indicates that servicing is transferred concurrent with loan delivery.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOIndicator ConcurrentServicingTransferIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ConcurrentServicingTransferIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConcurrentServicingTransferIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConcurrentServicingTransferIndicatorSpecified
        {
            get { return ConcurrentServicingTransferIndicator != null; }
            set { }
        }

        /// <summary>
        /// The day basis on which the exception interest adjustment is computed based on the servicing contract.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<ExceptionInterestAdjustmentDayMethodBase> ExceptionInterestAdjustmentDayMethodType;

        /// <summary>
        /// Gets or sets a value indicating whether the ExceptionInterestAdjustmentDayMethodType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ExceptionInterestAdjustmentDayMethodType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExceptionInterestAdjustmentDayMethodTypeSpecified
        {
            get { return this.ExceptionInterestAdjustmentDayMethodType != null && this.ExceptionInterestAdjustmentDayMethodType.enumValue != ExceptionInterestAdjustmentDayMethodBase.Blank; }
            set { }
        }

        /// <summary>
        /// The portion of interest payments made by the mortgage borrower that a loan servicer is entitled to retain in consideration for performing mortgage servicing activities for the owner of the mortgage loan (the mortgage servicing spread) over the portion representing reasonable compensation for services performed by the loan servicer.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOPercent ExcessServicingFeeRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the ExcessServicingFeeRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ExcessServicingFeeRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExcessServicingFeeRatePercentSpecified
        {
            get { return ExcessServicingFeeRatePercent != null; }
            set { }
        }

        /// <summary>
        /// Specifies the type of execution through which the loan was purchased by an investor.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<ExecutionBase> ExecutionType;

        /// <summary>
        /// Gets or sets a value indicating whether the ExecutionType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ExecutionType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExecutionTypeSpecified
        {
            get { return this.ExecutionType != null && this.ExecutionType.enumValue != ExecutionBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Execution Type.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString ExecutionTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ExecutionTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ExecutionTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExecutionTypeOtherDescriptionSpecified
        {
            get { return ExecutionTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The date the Primary Mortgage Market Survey (PMMS) rate was published.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMODate FREPrimaryMortgageMarketSurveyPublishedDate;

        /// <summary>
        /// Gets or sets a value indicating whether the FREPrimaryMortgageMarketSurveyPublishedDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FREPrimaryMortgageMarketSurveyPublishedDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool FREPrimaryMortgageMarketSurveyPublishedDateSpecified
        {
            get { return FREPrimaryMortgageMarketSurveyPublishedDate != null; }
            set { }
        }

        /// <summary>
        /// The average rate for a 30-year fixed-rate mortgage, as determined by Freddie Mac's Primary Mortgage Market Survey (PMMS).  Survey data is collected from Monday through Wednesday and the results are posted on FreddieMac.com on Thursdays.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOPercent FREPrimaryMortgageMarketSurveyPublishedRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the FREPrimaryMortgageMarketSurveyPublishedRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FREPrimaryMortgageMarketSurveyPublishedRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool FREPrimaryMortgageMarketSurveyPublishedRatePercentSpecified
        {
            get { return FREPrimaryMortgageMarketSurveyPublishedRatePercent != null; }
            set { }
        }

        /// <summary>
        /// The rounded Primary Mortgage Market Survey (PMMS) rate applied during structuring calculation.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOPercent FREPrimaryMortgageMarketSurveyRoundedRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the FREPrimaryMortgageMarketSurveyRoundedRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FREPrimaryMortgageMarketSurveyRoundedRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool FREPrimaryMortgageMarketSurveyRoundedRatePercentSpecified
        {
            get { return FREPrimaryMortgageMarketSurveyRoundedRatePercent != null; }
            set { }
        }

        /// <summary>
        /// The day basis on which the funding interest adjustment is computed based on the purchase contract.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOEnum<FundingInterestAdjustmentDayMethodBase> FundingInterestAdjustmentDayMethodType;

        /// <summary>
        /// Gets or sets a value indicating whether the FundingInterestAdjustmentDayMethodType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FundingInterestAdjustmentDayMethodType element has been assigned a value.</value>
        [XmlIgnore]
        public bool FundingInterestAdjustmentDayMethodTypeSpecified
        {
            get { return this.FundingInterestAdjustmentDayMethodType != null && this.FundingInterestAdjustmentDayMethodType.enumValue != FundingInterestAdjustmentDayMethodBase.Blank; }
            set { }
        }

        /// <summary>
        /// Indicates whether an eligible Seller has elected the Add On or Post-Settlement delivery fees for a specific Mortgage.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOIndicator GuarantyFeeAddOnIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the GuarantyFeeAddOnIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GuarantyFeeAddOnIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool GuarantyFeeAddOnIndicatorSpecified
        {
            get { return GuarantyFeeAddOnIndicator != null; }
            set { }
        }

        /// <summary>
        /// Contractual guaranty fee (after adjusting for the alternate payment method (APM) remittance cycle, if applicable) for an Mortgage Backed Security (MBS) pool mortgage.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOPercent GuarantyFeeAfterAlternatePaymentMethodPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the GuarantyFeeAfterAlternatePaymentMethodPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GuarantyFeeAfterAlternatePaymentMethodPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool GuarantyFeeAfterAlternatePaymentMethodPercentSpecified
        {
            get { return GuarantyFeeAfterAlternatePaymentMethodPercent != null; }
            set { }
        }

        /// <summary>
        /// The guaranty fee rate after applying all adjustments to the guaranty fee, such as buy up/buy down. The guaranty fee is a portion of the interest on the loan that is paid to a party to guarantee the timely payment of interest and principal to the holders of securities backed by the loan.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOPercent GuarantyFeePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the GuarantyFeePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GuarantyFeePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool GuarantyFeePercentSpecified
        {
            get { return GuarantyFeePercent != null; }
            set { }
        }

        /// <summary>
        /// The maximum allowed dollar amount variance between the reported and the expected or scheduled interest.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOAmount InterestVarianceToleranceMaximumAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the InterestVarianceToleranceMaximumAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InterestVarianceToleranceMaximumAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool InterestVarianceToleranceMaximumAmountSpecified
        {
            get { return InterestVarianceToleranceMaximumAmount != null; }
            set { }
        }

        /// <summary>
        /// Identifies the collateral program associated with the loan as identified by a specific entity. The entity may be referenced using the OwnerURI attribute.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOIdentifier InvestorCollateralProgramIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the InvestorCollateralProgramIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InvestorCollateralProgramIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool InvestorCollateralProgramIdentifierSpecified
        {
            get { return InvestorCollateralProgramIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The investors committed takeout price for a loan expressed as a percentage.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOPercent InvestorCommitmentTakeoutPricePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the InvestorCommitmentTakeoutPricePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InvestorCommitmentTakeoutPricePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool InvestorCommitmentTakeoutPricePercentSpecified
        {
            get { return InvestorCommitmentTakeoutPricePercent != null; }
            set { }
        }

        /// <summary>
        /// The effective date of the investor credit policy applied to this loan.
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMODate InvestorCreditPolicyEffectiveDate;

        /// <summary>
        /// Gets or sets a value indicating whether the InvestorCreditPolicyEffectiveDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InvestorCreditPolicyEffectiveDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool InvestorCreditPolicyEffectiveDateSpecified
        {
            get { return InvestorCreditPolicyEffectiveDate != null; }
            set { }
        }

        /// <summary>
        /// The unique identifier assigned by the investor to a single family structured transaction.
        /// </summary>
        [XmlElement(Order = 17)]
        public MISMOIdentifier InvestorDealIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the InvestorDealIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InvestorDealIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool InvestorDealIdentifierSpecified
        {
            get { return InvestorDealIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A unique identifier that segregates loans for a specific investor according to defined characteristics, such as interest rate, service fee, pool, asset group, etc.
        /// </summary>
        [XmlElement(Order = 18)]
        public MISMOIdentifier InvestorGroupingIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the InvestorGroupingIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InvestorGroupingIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool InvestorGroupingIdentifierSpecified
        {
            get { return InvestorGroupingIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The investor guaranty fee rate after any addition or reduction to the base guaranty fee rate due to a guaranty fee buy up or buy down. The investor guaranty fee is a portion of the interest on the loan that is paid to a party to guarantee the timely payment of interest and principal to the holders of securities backed by the loan.
        /// </summary>
        [XmlElement(Order = 19)]
        public MISMOPercent InvestorGuarantyFeeAfterBuyupBuydownPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the InvestorGuarantyFeeAfterBuyUpBuyDownPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InvestorGuarantyFeeAfterBuyUpBuyDownPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool InvestorGuarantyFeeAfterBuyupBuydownPercentSpecified
        {
            get { return InvestorGuarantyFeeAfterBuyupBuydownPercent != null; }
            set { }
        }

        /// <summary>
        /// The percentage of the Original Loan Balance that is being held as collateral by the investor for default costs on the loan.
        /// </summary>
        [XmlElement(Order = 20)]
        public MISMOPercent InvestorHeldCollateralPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the InvestorHeldCollateralPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InvestorHeldCollateralPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool InvestorHeldCollateralPercentSpecified
        {
            get { return InvestorHeldCollateralPercent != null; }
            set { }
        }

        /// <summary>
        /// Identifies the secondary mortgage market institution (investor) associated with the lender or trading partner.
        /// </summary>
        [XmlElement(Order = 21)]
        public MISMOIdentifier InvestorInstitutionIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the InvestorInstitutionIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InvestorInstitutionIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool InvestorInstitutionIdentifierSpecified
        {
            get { return InvestorInstitutionIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The expiration date for a commitment by an investor to acquire a loan.
        /// </summary>
        [XmlElement(Order = 22)]
        public MISMODate InvestorLoanCommitmentExpirationDate;

        /// <summary>
        /// Gets or sets a value indicating whether the InvestorLoanCommitmentExpirationDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InvestorLoanCommitmentExpirationDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool InvestorLoanCommitmentExpirationDateSpecified
        {
            get { return InvestorLoanCommitmentExpirationDate != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the investor waives the full prepayment penalties.
        /// </summary>
        [XmlElement(Order = 23)]
        public MISMOIndicator InvestorLoanEarlyPayoffPenaltyWaiverIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the InvestorLoanEarlyPayoffPenaltyWaiverIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InvestorLoanEarlyPayoffPenaltyWaiverIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool InvestorLoanEarlyPayoffPenaltyWaiverIndicatorSpecified
        {
            get { return InvestorLoanEarlyPayoffPenaltyWaiverIndicator != null; }
            set { }
        }

        /// <summary>
        /// The due date of the first payment to the investor.
        /// </summary>
        [XmlElement(Order = 24)]
        public MISMODate InvestorLoanEffectiveDate;

        /// <summary>
        /// Gets or sets a value indicating whether the InvestorLoanEffectiveDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InvestorLoanEffectiveDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool InvestorLoanEffectiveDateSpecified
        {
            get { return InvestorLoanEffectiveDate != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the investor waives the curtailment penalties.
        /// </summary>
        [XmlElement(Order = 25)]
        public MISMOIndicator InvestorLoanPartialPrepaymentPenaltyWaiverIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the InvestorLoanPartialPrepaymentPenaltyWaiverIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InvestorLoanPartialPrepaymentPenaltyWaiverIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool InvestorLoanPartialPrepaymentPenaltyWaiverIndicatorSpecified
        {
            get { return InvestorLoanPartialPrepaymentPenaltyWaiverIndicator != null; }
            set { }
        }

        /// <summary>
        /// The unique identifier of the master commitment that states the terms under which a lender and an investor agree to deliver/receive mortgage loans in exchange for funds or securities. Subordinate commitments with further delineation of conditions are created according to the constraints of the master commitment.
        /// </summary>
        [XmlElement(Order = 26)]
        public MISMOIdentifier InvestorMasterCommitmentIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the InvestorMasterCommitmentIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InvestorMasterCommitmentIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool InvestorMasterCommitmentIdentifierSpecified
        {
            get { return InvestorMasterCommitmentIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Identifies the percentage amount of the loan owned by the investor.
        /// </summary>
        [XmlElement(Order = 27)]
        public MISMOPercent InvestorOwnershipPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the InvestorOwnershipPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InvestorOwnershipPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool InvestorOwnershipPercentSpecified
        {
            get { return InvestorOwnershipPercent != null; }
            set { }
        }

        /// <summary>
        /// Identifies the ownership status of the investor.
        /// </summary>
        [XmlElement(Order = 28)]
        public MISMOEnum<InvestorOwnershipBase> InvestorOwnershipType;

        /// <summary>
        /// Gets or sets a value indicating whether the InvestorOwnershipType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InvestorOwnershipType element has been assigned a value.</value>
        [XmlIgnore]
        public bool InvestorOwnershipTypeSpecified
        {
            get { return this.InvestorOwnershipType != null && this.InvestorOwnershipType.enumValue != InvestorOwnershipBase.Blank; }
            set { }
        }

        /// <summary>
        /// Specifies the source of the list for identifiers of the adjustable loan product being financed. Used for the Fannie Mae Product Plan, Freddie Mac Product Plan and other investor product plans.
        /// </summary>
        [XmlElement(Order = 29)]
        public MISMOIdentifier InvestorProductPlanIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the InvestorProductPlanIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InvestorProductPlanIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool InvestorProductPlanIdentifierSpecified
        {
            get { return InvestorProductPlanIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The day of the month on which principal and interest for the loan are remitted by the servicer to the investor.
        /// </summary>
        [XmlElement(Order = 30)]
        public MISMODay InvestorRemittanceDay;

        /// <summary>
        /// Gets or sets a value indicating whether the InvestorRemittanceDay element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InvestorRemittanceDay element has been assigned a value.</value>
        [XmlIgnore]
        public bool InvestorRemittanceDaySpecified
        {
            get { return InvestorRemittanceDay != null; }
            set { }
        }

        /// <summary>
        /// The number of days for accelerated remittance as specified by the investor.
        /// </summary>
        [XmlElement(Order = 31)]
        public MISMOCount InvestorRemittanceDaysCount;

        /// <summary>
        /// Gets or sets a value indicating whether the InvestorRemittanceDaysCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InvestorRemittanceDaysCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool InvestorRemittanceDaysCountSpecified
        {
            get { return InvestorRemittanceDaysCount != null; }
            set { }
        }

        /// <summary>
        /// A unique identifier of the remittance program for a specific investor under which the loan is serviced. The party assigning the identifier can be provided using the IdentifierOwnerURI.
        /// </summary>
        [XmlElement(Order = 32)]
        public MISMOIdentifier InvestorRemittanceIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the InvestorRemittanceIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InvestorRemittanceIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool InvestorRemittanceIdentifierSpecified
        {
            get { return InvestorRemittanceIdentifier != null; }
            set { }
        }

        /// <summary>
        /// This describes the contractual accounting method used to calculate the funds received by the Servicer from the borrower that are due to the investor.
        /// </summary>
        [XmlElement(Order = 33)]
        public MISMOEnum<InvestorRemittanceBase> InvestorRemittanceType;

        /// <summary>
        /// Gets or sets a value indicating whether the InvestorRemittanceType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InvestorRemittanceType element has been assigned a value.</value>
        [XmlIgnore]
        public bool InvestorRemittanceTypeSpecified
        {
            get { return this.InvestorRemittanceType != null && this.InvestorRemittanceType.enumValue != InvestorRemittanceBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture the Investor Remittance Type if Other is selected.
        /// </summary>
        [XmlElement(Order = 34)]
        public MISMOString InvestorRemittanceTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the InvestorRemittanceTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InvestorRemittanceTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool InvestorRemittanceTypeOtherDescriptionSpecified
        {
            get { return InvestorRemittanceTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the reporting method required by the investor from the servicer.
        /// </summary>
        [XmlElement(Order = 35)]
        public MISMOEnum<InvestorReportingMethodBase> InvestorReportingMethodType;

        /// <summary>
        /// Gets or sets a value indicating whether the InvestorReportingMethodType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InvestorReportingMethodType element has been assigned a value.</value>
        [XmlIgnore]
        public bool InvestorReportingMethodTypeSpecified
        {
            get { return this.InvestorReportingMethodType != null && this.InvestorReportingMethodType.enumValue != InvestorReportingMethodBase.Blank; }
            set { }
        }

        /// <summary>
        /// Field used in the pass through calculation when the Bottom Up method is used.
        /// </summary>
        [XmlElement(Order = 36)]
        public MISMOPercent InvestorRequiredMarginPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the InvestorRequiredMarginPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InvestorRequiredMarginPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool InvestorRequiredMarginPercentSpecified
        {
            get { return InvestorRequiredMarginPercent != null; }
            set { }
        }

        /// <summary>
        /// The fixed dollar amount that is paid to the servicer by the investor for servicing the mortgage if the service fee is not expressed as a rate.
        /// </summary>
        [XmlElement(Order = 37)]
        public MISMOAmount InvestorServicingFeeAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the InvestorServicingFeeAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InvestorServicingFeeAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool InvestorServicingFeeAmountSpecified
        {
            get { return InvestorServicingFeeAmount != null; }
            set { }
        }

        /// <summary>
        /// The percentage of interest collected that is paid to the servicer by the investor for servicing the mortgage (gross).
        /// </summary>
        [XmlElement(Order = 38)]
        public MISMOPercent InvestorServicingFeeRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the InvestorServicingFeeRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InvestorServicingFeeRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool InvestorServicingFeeRatePercentSpecified
        {
            get { return InvestorServicingFeeRatePercent != null; }
            set { }
        }

        /// <summary>
        /// Last principal balance reported to the investor.
        /// </summary>
        [XmlElement(Order = 39)]
        public MISMOAmount LastReportedToInvestorPrincipalBalanceAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the LastReportedToInvestorPrincipalBalanceAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LastReportedToInvestorPrincipalBalanceAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool LastReportedToInvestorPrincipalBalanceAmountSpecified
        {
            get { return LastReportedToInvestorPrincipalBalanceAmount != null; }
            set { }
        }

        /// <summary>
        /// The number of days allowed for late delivery of the loan, beyond the scheduled delivery date.
        /// </summary>
        [XmlElement(Order = 40)]
        public MISMOCount LateDeliveryGraceDaysCount;

        /// <summary>
        /// Gets or sets a value indicating whether the LateDeliveryGraceDaysCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LateDeliveryGraceDaysCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool LateDeliveryGraceDaysCountSpecified
        {
            get { return LateDeliveryGraceDaysCount != null; }
            set { }
        }

        /// <summary>
        /// The date the lender selects to have their Whole Loan purchase funded.  Proceeds are typically wired to the lender 24 hours after the purchase of the loan.  By selecting this date, the lender manages when the funds are to be wired.
        /// </summary>
        [XmlElement(Order = 41)]
        public MISMODate LenderTargetFundingDate;

        /// <summary>
        /// Gets or sets a value indicating whether the LenderTargetFundingDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LenderTargetFundingDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool LenderTargetFundingDateSpecified
        {
            get { return LenderTargetFundingDate != null; }
            set { }
        }

        /// <summary>
        /// The scheduled unpaid principal balance of the mortgage as of loan acquisition or the issue date of the associated security.
        /// </summary>
        [XmlElement(Order = 42)]
        public MISMOAmount LoanAcquisitionScheduledUPBAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanAcquisitionScheduledUPBAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanAcquisitionScheduledUPBAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanAcquisitionScheduledUPBAmountSpecified
        {
            get { return LoanAcquisitionScheduledUPBAmount != null; }
            set { }
        }

        /// <summary>
        /// The number of basis points of loan level buy up/buy down selected by the seller for this mortgage.
        /// </summary>
        [XmlElement(Order = 43)]
        public MISMONumeric LoanBuyupBuydownBasisPointNumber;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanBuyUpBuyDownBasisPointNumber element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanBuyUpBuyDownBasisPointNumber element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanBuyupBuydownBasisPointNumberSpecified
        {
            get { return LoanBuyupBuydownBasisPointNumber != null; }
            set { }
        }

        /// <summary>
        /// Specifies the type of buy up or buy down an eligible Seller has elected to exercise for a specific mortgage.
        /// </summary>
        [XmlElement(Order = 44)]
        public MISMOEnum<LoanBuyupBuydownBase> LoanBuyupBuydownType;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanBuyUpBuyDownType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanBuyUpBuyDownType element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanBuyupBuydownTypeSpecified
        {
            get { return this.LoanBuyupBuydownType != null && this.LoanBuyupBuydownType.enumValue != LoanBuyupBuydownBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to provide additional information when Other is selected for Loan Buy up Buy down Type.
        /// </summary>
        [XmlElement(Order = 45)]
        public MISMOString LoanBuyupBuydownTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanBuyUpBuyDownTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanBuyUpBuyDownTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanBuyupBuydownTypeOtherDescriptionSpecified
        {
            get { return LoanBuyupBuydownTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The frequency that the servicer must report for loans that are in default.
        /// </summary>
        [XmlElement(Order = 46)]
        public MISMOEnum<LoanDefaultedReportingFrequencyBase> LoanDefaultedReportingFrequencyType;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanDefaultedReportingFrequencyType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanDefaultedReportingFrequencyType element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanDefaultedReportingFrequencyTypeSpecified
        {
            get { return this.LoanDefaultedReportingFrequencyType != null && this.LoanDefaultedReportingFrequencyType.enumValue != LoanDefaultedReportingFrequencyBase.Blank; }
            set { }
        }

        /// <summary>
        /// Indicates the party that bears the default loss for the loan.
        /// </summary>
        [XmlElement(Order = 47)]
        public MISMOEnum<LoanDefaultLossPartyBase> LoanDefaultLossPartyType;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanDefaultLossPartyType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanDefaultLossPartyType element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanDefaultLossPartyTypeSpecified
        {
            get { return this.LoanDefaultLossPartyType != null && this.LoanDefaultLossPartyType.enumValue != LoanDefaultLossPartyBase.Blank; }
            set { }
        }

        /// <summary>
        /// The number of days after which a servicer can stop advancing funds on a delinquent loan.
        /// </summary>
        [XmlElement(Order = 48)]
        public MISMOCount LoanDelinquencyAdvanceDaysCount;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanDelinquencyAdvanceDaysCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanDelinquencyAdvanceDaysCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanDelinquencyAdvanceDaysCountSpecified
        {
            get { return LoanDelinquencyAdvanceDaysCount != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount disbursed under a loan agreement in advance of full funding.
        /// </summary>
        [XmlElement(Order = 49)]
        public MISMOAmount LoanFundingAdvancedAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanFundingAdvancedAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanFundingAdvancedAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanFundingAdvancedAmountSpecified
        {
            get { return LoanFundingAdvancedAmount != null; }
            set { }
        }

        /// <summary>
        /// The name of the legal entity from which the seller acquired the loan.
        /// </summary>
        [XmlElement(Order = 50)]
        public MISMOString LoanPriorOwnerName;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanPriorOwnerName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanPriorOwnerName element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanPriorOwnerNameSpecified
        {
            get { return LoanPriorOwnerName != null; }
            set { }
        }

        /// <summary>
        /// The difference between the dollar price paid for the loan and its UPB at the time of purchase when the UPB is less than price paid.
        /// </summary>
        [XmlElement(Order = 51)]
        public MISMOAmount LoanPurchaseDiscountAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanPurchaseDiscountAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanPurchaseDiscountAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanPurchaseDiscountAmountSpecified
        {
            get { return LoanPurchaseDiscountAmount != null; }
            set { }
        }

        /// <summary>
        /// The difference between the dollar price paid for the loan and its UPB at the time of purchase when the UPB is more than price paid.
        /// </summary>
        [XmlElement(Order = 52)]
        public MISMOAmount LoanPurchasePremiumAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanPurchasePremiumAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanPurchasePremiumAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanPurchasePremiumAmountSpecified
        {
            get { return LoanPurchasePremiumAmount != null; }
            set { }
        }

        /// <summary>
        /// The number of basis points of servicing fee selected for this mortgage.
        /// </summary>
        [XmlElement(Order = 53)]
        public MISMONumeric LoanServicingFeeBasisPointNumber;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanServicingFeeBasisPointNumber element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanServicingFeeBasisPointNumber element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanServicingFeeBasisPointNumberSpecified
        {
            get { return LoanServicingFeeBasisPointNumber != null; }
            set { }
        }

        /// <summary>
        ///  Indicates whether the seller will identify specific loan level servicing values for this mortgage.
        /// </summary>
        [XmlElement(Order = 54)]
        public MISMOIndicator LoanServicingIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanServicingIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanServicingIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanServicingIndicatorSpecified
        {
            get { return LoanServicingIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the transaction on the Investors side is a Purchase of servicing rights rather than the purchase of an asset.
        /// </summary>
        [XmlElement(Order = 55)]
        public MISMOIndicator LoanServicingRightsPurchaseOnlyIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanServicingRightsPurchaseOnlyIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanServicingRightsPurchaseOnlyIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanServicingRightsPurchaseOnlyIndicatorSpecified
        {
            get { return LoanServicingRightsPurchaseOnlyIndicator != null; }
            set { }
        }

        /// <summary>
        /// A premium, expressed as a dollar amount, calculated based upon the difference between the interest rate of the loan and the market rate at the time of funding.
        /// </summary>
        [XmlElement(Order = 56)]
        public MISMOAmount LoanYieldSpreadPremiumAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanYieldSpreadPremiumAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanYieldSpreadPremiumAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanYieldSpreadPremiumAmountSpecified
        {
            get { return LoanYieldSpreadPremiumAmount != null; }
            set { }
        }

        /// <summary>
        /// The rate, expressed as a percent, at which the lender receives excess yield on the loan.   The excess yield is the amount of interest that is left over when the servicing fee, lender guaranty fee and lender pass through rates are subtracted from the interest rate of the loan.  
        /// </summary>
        [XmlElement(Order = 57)]
        public MISMOPercent LoanYieldSpreadPremiumRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanYieldSpreadPremiumRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanYieldSpreadPremiumRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanYieldSpreadPremiumRatePercentSpecified
        {
            get { return LoanYieldSpreadPremiumRatePercent != null; }
            set { }
        }

        /// <summary>
        /// The percentage of collected servicing fee applied to servicer placed MI coverage.
        /// </summary>
        [XmlElement(Order = 58)]
        public MISMOPercent MICoverageServicingFeeRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the MICoverageServicingFeeRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MICoverageServicingFeeRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool MICoverageServicingFeeRatePercentSpecified
        {
            get { return MICoverageServicingFeeRatePercent != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of all non standard fees as defined by the investor and earned by loan administrators that reduce the amount of funds remitted to the issuing entity (including subservicing, master servicing, trustee fees, etc.).
        /// </summary>
        [XmlElement(Order = 59)]
        public MISMOAmount NonStandardServicingFeeRetainedByServicerAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the NonStandardServicingFeeRetainedByServicerAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NonStandardServicingFeeRetainedByServicerAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool NonStandardServicingFeeRetainedByServicerAmountSpecified
        {
            get { return NonStandardServicingFeeRetainedByServicerAmount != null; }
            set { }
        }

        /// <summary>
        /// Determines what formula is used to calculate the investor pass through rate as defined in the investor contract.
        /// </summary>
        [XmlElement(Order = 60)]
        public MISMOEnum<PassThroughCalculationMethodBase> PassThroughCalculationMethodType;

        /// <summary>
        /// Gets or sets a value indicating whether the PassThroughCalculationMethodType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PassThroughCalculationMethodType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PassThroughCalculationMethodTypeSpecified
        {
            get { return this.PassThroughCalculationMethodType != null && this.PassThroughCalculationMethodType.enumValue != PassThroughCalculationMethodBase.Blank; }
            set { }
        }

        /// <summary>
        /// Investor rate ceiling used in bottom up calculations.
        /// </summary>
        [XmlElement(Order = 61)]
        public MISMOPercent PassThroughCeilingRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the PassThroughCeilingRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PassThroughCeilingRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool PassThroughCeilingRatePercentSpecified
        {
            get { return PassThroughCeilingRatePercent != null; }
            set { }
        }

        /// <summary>
        /// The rate at which interest is passed by the servicer to the investor. It equals the note rate less the service fee rate and excess service fee rate.
        /// </summary>
        [XmlElement(Order = 62)]
        public MISMOPercent PassThroughRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the PassThroughRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PassThroughRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool PassThroughRatePercentSpecified
        {
            get { return PassThroughRatePercent != null; }
            set { }
        }

        /// <summary>
        /// The maximum allowed dollar amount variance between the reported and the expected or scheduled principal.
        /// </summary>
        [XmlElement(Order = 63)]
        public MISMOAmount PrincipalVarianceToleranceMaximumAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PrincipalVarianceToleranceMaximumAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrincipalVarianceToleranceMaximumAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrincipalVarianceToleranceMaximumAmountSpecified
        {
            get { return PrincipalVarianceToleranceMaximumAmount != null; }
            set { }
        }

        /// <summary>
        /// Identifier of a loan from a related or original transaction. May be used for modifications and conversion of existing loans.
        /// </summary>
        [XmlElement(Order = 64)]
        public MISMOIdentifier RelatedInvestorLoanIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the RelatedInvestorLoanIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RelatedInvestorLoanIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool RelatedInvestorLoanIdentifierSpecified
        {
            get { return RelatedInvestorLoanIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Specifies the investor of the associated loan.
        /// </summary>
        [XmlElement(Order = 65)]
        public MISMOEnum<RelatedLoanInvestorBase> RelatedLoanInvestorType;

        /// <summary>
        /// Gets or sets a value indicating whether the RelatedLoanInvestorType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RelatedLoanInvestorType element has been assigned a value.</value>
        [XmlIgnore]
        public bool RelatedLoanInvestorTypeSpecified
        {
            get { return this.RelatedLoanInvestorType != null && this.RelatedLoanInvestorType.enumValue != RelatedLoanInvestorBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Related Loan investor Type.
        /// </summary>
        [XmlElement(Order = 66)]
        public MISMOString RelatedLoanInvestorTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the RelatedLoanInvestorTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RelatedLoanInvestorTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool RelatedLoanInvestorTypeOtherDescriptionSpecified
        {
            get { return RelatedLoanInvestorTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates a request to remove a loan from a prior servicing transfer request.
        /// </summary>
        [XmlElement(Order = 67)]
        public MISMOIndicator RemoveFromTransferIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the RemoveFromTransferIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RemoveFromTransferIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool RemoveFromTransferIndicatorSpecified
        {
            get { return RemoveFromTransferIndicator != null; }
            set { }
        }

        /// <summary>
        /// Identifies the party responsible for marketing the property in case of default.
        /// </summary>
        [XmlElement(Order = 68)]
        public MISMOEnum<REOMarketingPartyBase> REOMarketingPartyType;

        /// <summary>
        /// Gets or sets a value indicating whether the REOMarketingPartyType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the REOMarketingPartyType element has been assigned a value.</value>
        [XmlIgnore]
        public bool REOMarketingPartyTypeSpecified
        {
            get { return this.REOMarketingPartyType != null && this.REOMarketingPartyType.enumValue != REOMarketingPartyBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for REO Marketing Party Type.
        /// </summary>
        [XmlElement(Order = 69)]
        public MISMOString REOMarketingPartyTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the REOMarketingPartyTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the REOMarketingPartyTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool REOMarketingPartyTypeOtherDescriptionSpecified
        {
            get { return REOMarketingPartyTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The minimum service fee rate, expressed as a percent, to be earned by the servicer.
        /// </summary>
        [XmlElement(Order = 70)]
        public MISMOPercent ServicingFeeMinimumRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the ServicingFeeMinimumRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServicingFeeMinimumRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServicingFeeMinimumRatePercentSpecified
        {
            get { return ServicingFeeMinimumRatePercent != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount that was paid by the seller to acquire the loan from a third party.
        /// </summary>
        [XmlElement(Order = 71)]
        public MISMOAmount ThirdPartyLoanAcquisitionPriceAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ThirdPartyLoanAcquisitionPriceAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ThirdPartyLoanAcquisitionPriceAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ThirdPartyLoanAcquisitionPriceAmountSpecified
        {
            get { return ThirdPartyLoanAcquisitionPriceAmount != null; }
            set { }
        }

        /// <summary>
        /// The maximum allowed dollar amount variance between the reported and the expected or scheduled unpaid principal balance (UPB).
        /// </summary>
        [XmlElement(Order = 72)]
        public MISMOAmount UPBVarianceToleranceMaximumAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the UPBVarianceToleranceMaximumAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the UPBVarianceToleranceMaximumAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool UPBVarianceToleranceMaximumAmountSpecified
        {
            get { return UPBVarianceToleranceMaximumAmount != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of the advance against a warehouse line of credit.
        /// </summary>
        [XmlElement(Order = 73)]
        public MISMOAmount WarehouseAdvanceAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the WarehouseAdvanceAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the WarehouseAdvanceAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool WarehouseAdvanceAmountSpecified
        {
            get { return WarehouseAdvanceAmount != null; }
            set { }
        }

        /// <summary>
        /// Uniquely identifies a subcategory of a warehouse commitment.
        /// </summary>
        [XmlElement(Order = 74)]
        public MISMOIdentifier WarehouseCommitmentSubcategoryIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the WarehouseCommitmentSubcategoryIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the WarehouseCommitmentSubcategoryIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool WarehouseCommitmentSubcategoryIdentifierSpecified
        {
            get { return WarehouseCommitmentSubcategoryIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of interest due on the date which the whole loan was sold to the investor.
        /// </summary>
        [XmlElement(Order = 75)]
        public MISMOAmount WholeLoanInterestDueAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the WholeLoanInterestDueAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the WholeLoanInterestDueAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool WholeLoanInterestDueAmountSpecified
        {
            get { return WholeLoanInterestDueAmount != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of principal balance outstanding on the date which the whole loan was sold to the investor.
        /// </summary>
        [XmlElement(Order = 76)]
        public MISMOAmount WholeLoanPrincipalAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the WholeLoanPrincipalAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the WholeLoanPrincipalAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool WholeLoanPrincipalAmountSpecified
        {
            get { return WholeLoanPrincipalAmount != null; }
            set { }
        }

        /// <summary>
        /// The date on which the whole loan was sold to the investor.
        /// </summary>
        [XmlElement(Order = 77)]
        public MISMODate WholeLoanSaleDate;

        /// <summary>
        /// Gets or sets a value indicating whether the WholeLoanSaleDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the WholeLoanSaleDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool WholeLoanSaleDateSpecified
        {
            get { return WholeLoanSaleDate != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 78)]
        public INVESTOR_LOAN_INFORMATION_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
