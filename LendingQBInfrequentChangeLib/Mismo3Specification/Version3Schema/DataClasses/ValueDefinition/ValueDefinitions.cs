namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class VALUE_DEFINITIONS
    {
        /// <summary>
        /// Gets a value indicating whether the VALUE_DEFINITIONS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ValueDefinitionSpecified;
            }
        }

        /// <summary>
        /// A collection of value definitions.
        /// </summary>
        [XmlElement("VALUE_DEFINITION", Order = 0)]
		public List<VALUE_DEFINITION> ValueDefinition = new List<VALUE_DEFINITION>();

        /// <summary>
        /// Gets or sets a value indicating whether the ValueDefinition element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ValueDefinition element has been assigned a value.</value>
        [XmlIgnore]
        public bool ValueDefinitionSpecified
        {
            get { return this.ValueDefinition != null && this.ValueDefinition.Count(v => v != null && v.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public VALUE_DEFINITIONS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
