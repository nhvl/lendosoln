namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class EXPENSES
    {
        /// <summary>
        /// Gets a value indicating whether the EXPENSES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExpenseSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of expenses.
        /// </summary>
        [XmlElement("EXPENSE", Order = 0)]
		public List<EXPENSE> Expense = new List<EXPENSE>();

        /// <summary>
        /// Gets or sets a value indicating whether the Expense element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Expense element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExpenseSpecified
        {
            get { return this.Expense != null && this.Expense.Count(e => e != null && e.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public EXPENSES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
