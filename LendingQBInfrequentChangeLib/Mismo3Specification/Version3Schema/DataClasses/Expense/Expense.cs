namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class EXPENSE
    {
        /// <summary>
        /// Gets a value indicating whether the container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AlimonyOwedToNameSpecified
                    || this.ExpenseDescriptionSpecified
                    || this.ExpenseMonthlyPaymentAmountSpecified
                    || this.ExpenseRemainingTermMonthsCountSpecified
                    || this.ExpenseTypeOtherDescriptionSpecified
                    || this.ExpenseTypeSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// The unstructured name of the person to whom alimony payments are owed by the borrower. Collected on the URLA in Section VI (Liabilities Alimony Owed To).
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString AlimonyOwedToName;

        /// <summary>
        /// Gets or sets a value indicating whether the AlimonyOwedToName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AlimonyOwedToName element has been assigned a value.</value>
        [XmlIgnore]
        public bool AlimonyOwedToNameSpecified
        {
            get { return AlimonyOwedToName != null; }
            set { }
        }

        /// <summary>
        /// A text description that further defines the expense. This could be used to describe the job related expenses.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString ExpenseDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ExpenseDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ExpenseDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExpenseDescriptionSpecified
        {
            get { return ExpenseDescription != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of the monthly payment required on borrowers Expense Type. 
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOAmount ExpenseMonthlyPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ExpenseMonthlyPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ExpenseMonthlyPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExpenseMonthlyPaymentAmountSpecified
        {
            get { return ExpenseMonthlyPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// The number of months the borrower must make payments in order to satisfy the identified expense.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOCount ExpenseRemainingTermMonthsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the ExpenseRemainingTermMonthsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ExpenseRemainingTermMonthsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExpenseRemainingTermMonthsCountSpecified
        {
            get { return ExpenseRemainingTermMonthsCount != null; }
            set { }
        }

        /// <summary>
        /// Specifies the general names (types) of items commonly listed as expenses of the borrower in a mortgage loan transaction. 
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<ExpenseBase> ExpenseType;

        /// <summary>
        /// Gets or sets a value indicating whether the ExpenseType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ExpenseType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExpenseTypeSpecified
        {
            get { return this.ExpenseType != null && this.ExpenseType.enumValue != ExpenseBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Expense Type.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString ExpenseTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ExpenseTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ExpenseTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExpenseTypeOtherDescriptionSpecified
        {
            get { return ExpenseTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 6)]
        public EXPENSE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// Gets or sets the $$xlink$$ label.
        /// </summary>
        [XmlAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified, Namespace = Mismo3Constants.XlinkNamespace)]
        public string label;

        /// <summary>
        /// Indicates whether the label can be serialized.
        /// </summary>
        /// <returns>A boolean indicating whether the label can be serialized.</returns>
        public bool ShouldSerializelabel()
        {
            return !string.IsNullOrEmpty(label);
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
