namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class UNDERWRITING
    {
        /// <summary>
        /// Gets a value indicating whether the UNDERWRITING container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AutomatedUnderwritingsSpecified
                    || this.ExtensionSpecified
                    || this.UnderwritingDetailSpecified;
            }
        }

        /// <summary>
        /// A collection of underwriting cases.
        /// </summary>
        [XmlElement("AUTOMATED_UNDERWRITINGS", Order = 0)]
        public AUTOMATED_UNDERWRITINGS AutomatedUnderwritings;

        /// <summary>
        /// Gets or sets a value indicating whether the AutomatedUnderwritings element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AutomatedUnderwritings element has been assigned a value.</value>
        [XmlIgnore]
        public bool AutomatedUnderwritingsSpecified
        {
            get { return this.AutomatedUnderwritings != null && this.AutomatedUnderwritings.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details about underwriting.
        /// </summary>
        [XmlElement("UNDERWRITING_DETAIL", Order = 1)]
        public UNDERWRITING_DETAIL UnderwritingDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the UnderwritingDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the UnderwritingDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool UnderwritingDetailSpecified
        {
            get { return this.UnderwritingDetail != null && this.UnderwritingDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public UNDERWRITING_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
