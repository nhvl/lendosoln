namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class UNDERWRITING_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the UNDERWRITING_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ContractualAgreementIndicatorSpecified
                    || this.ContractUnderwritingIndicatorSpecified
                    || this.ExtensionSpecified
                    || this.LoanManualUnderwritingIndicatorSpecified
                    || this.LoanManualUnderwritingOrganizationNameSpecified
                    || this.LoanUnderwritingInvestorGuidelinesIndicatorSpecified
                    || this.LoanUnderwritingSubmitterTypeOtherDescriptionSpecified
                    || this.LoanUnderwritingSubmitterTypeSpecified
                    || this.UnderwritingCommentsDescriptionSpecified;
            }
        }

        /// <summary>
        /// When true, indicates the loan was underwritten following the guidelines contained in a separate contractual agreement between the lender and the GSE Agency.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIndicator ContractualAgreementIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ContractualAgreementIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ContractualAgreementIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ContractualAgreementIndicatorSpecified
        {
            get { return ContractualAgreementIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the loan was underwritten by a contract underwriting organization.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOIndicator ContractUnderwritingIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ContractUnderwritingIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ContractUnderwritingIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ContractUnderwritingIndicatorSpecified
        {
            get { return ContractUnderwritingIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the loan was manually underwritten.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOIndicator LoanManualUnderwritingIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanManualUnderwritingIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanManualUnderwritingIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanManualUnderwritingIndicatorSpecified
        {
            get { return LoanManualUnderwritingIndicator != null; }
            set { }
        }

        /// <summary>
        /// Name of the organization that has manually underwritten the loan.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString LoanManualUnderwritingOrganizationName;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanManualUnderwritingOrganizationName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanManualUnderwritingOrganizationName element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanManualUnderwritingOrganizationNameSpecified
        {
            get { return LoanManualUnderwritingOrganizationName != null; }
            set { }
        }

        /// <summary>
        /// Indicates that a loan was underwritten to guidelines of the investor.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOIndicator LoanUnderwritingInvestorGuidelinesIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanUnderwritingInvestorGuidelinesIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanUnderwritingInvestorGuidelinesIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanUnderwritingInvestorGuidelinesIndicatorSpecified
        {
            get { return LoanUnderwritingInvestorGuidelinesIndicator != null; }
            set { }
        }

        /// <summary>
        /// Specifies the type of entity who submitted the loan for underwriting.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOEnum<LoanUnderwritingSubmitterBase> LoanUnderwritingSubmitterType;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanUnderwritingSubmitterType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanUnderwritingSubmitterType element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanUnderwritingSubmitterTypeSpecified
        {
            get { return this.LoanUnderwritingSubmitterType != null && this.LoanUnderwritingSubmitterType.enumValue != LoanUnderwritingSubmitterBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Loan Underwriting Submitter Type.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOString LoanUnderwritingSubmitterTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanUnderwritingSubmitterTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanUnderwritingSubmitterTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanUnderwritingSubmitterTypeOtherDescriptionSpecified
        {
            get { return LoanUnderwritingSubmitterTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture comments of the underwriter.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOString UnderwritingCommentsDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the UnderwritingCommentsDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the UnderwritingCommentsDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool UnderwritingCommentsDescriptionSpecified
        {
            get { return UnderwritingCommentsDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 8)]
        public UNDERWRITING_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
