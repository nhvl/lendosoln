namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class PRORATION_ITEM
    {
        /// <summary>
        /// Gets a value indicating whether the PRORATION_ITEM container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.IntegratedDisclosureLineNumberValueSpecified
                    || this.IntegratedDisclosureSectionTypeOtherDescriptionSpecified
                    || this.IntegratedDisclosureSectionTypeSpecified
                    || this.IntegratedDisclosureSubsectionTypeOtherDescriptionSpecified
                    || this.IntegratedDisclosureSubsectionTypeSpecified
                    || this.ProrationItemAmountSpecified
                    || this.ProrationItemPaidFromDateSpecified
                    || this.ProrationItemPaidThroughDateSpecified
                    || this.ProrationItemTypeOtherDescriptionSpecified
                    || this.ProrationItemTypeSpecified;
            }
        }

        /// <summary>
        /// The line identifier for the item as represented in a section of the Integrated Disclosure form.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOValue IntegratedDisclosureLineNumberValue;

        /// <summary>
        /// Gets or sets a value indicating whether the IntegratedDisclosureLineNumberValue element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IntegratedDisclosureLineNumberValue element has been assigned a value.</value>
        [XmlIgnore]
        public bool IntegratedDisclosureLineNumberValueSpecified
        {
            get { return IntegratedDisclosureLineNumberValue != null; }
            set { }
        }

        /// <summary>
        /// The title or description used to identify a primary section of the integrated disclosure document.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<IntegratedDisclosureSectionBase> IntegratedDisclosureSectionType;

        /// <summary>
        /// Gets or sets a value indicating whether the IntegratedDisclosureSectionType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IntegratedDisclosureSectionType element has been assigned a value.</value>
        [XmlIgnore]
        public bool IntegratedDisclosureSectionTypeSpecified
        {
            get { return this.IntegratedDisclosureSectionType != null && this.IntegratedDisclosureSectionType.enumValue != IntegratedDisclosureSectionBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture additional information when Other is selected for Integrated Disclosure Section Type.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString IntegratedDisclosureSectionTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the IntegratedDisclosureSectionTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IntegratedDisclosureSectionTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool IntegratedDisclosureSectionTypeOtherDescriptionSpecified
        {
            get { return IntegratedDisclosureSectionTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The title or description used to identify a secondary or subsection of the integrated disclosure document.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOEnum<IntegratedDisclosureSubsectionBase> IntegratedDisclosureSubsectionType;

        /// <summary>
        /// Gets or sets a value indicating whether the IntegratedDisclosureSubsectionType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IntegratedDisclosureSubsectionType element has been assigned a value.</value>
        [XmlIgnore]
        public bool IntegratedDisclosureSubsectionTypeSpecified
        {
            get { return this.IntegratedDisclosureSubsectionType != null && this.IntegratedDisclosureSubsectionType.enumValue != IntegratedDisclosureSubsectionBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture additional information when Other is selected for Integrated Disclosure Subsection Type.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString IntegratedDisclosureSubsectionTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the IntegratedDisclosureSubsectionTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IntegratedDisclosureSubsectionTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool IntegratedDisclosureSubsectionTypeOtherDescriptionSpecified
        {
            get { return IntegratedDisclosureSubsectionTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of the Proration Item Type.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOAmount ProrationItemAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ProrationItemAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProrationItemAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProrationItemAmountSpecified
        {
            get { return ProrationItemAmount != null; }
            set { }
        }

        /// <summary>
        /// The start date for calculating the total number of days covered by the proration item.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMODate ProrationItemPaidFromDate;

        /// <summary>
        /// Gets or sets a value indicating whether the ProrationItemPaidFromDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProrationItemPaidFromDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProrationItemPaidFromDateSpecified
        {
            get { return ProrationItemPaidFromDate != null; }
            set { }
        }

        /// <summary>
        /// The end date for calculating the total number of days covered by the proration item, inclusive of this date.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMODate ProrationItemPaidThroughDate;

        /// <summary>
        /// Gets or sets a value indicating whether the ProrationItemPaidThroughDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProrationItemPaidThroughDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProrationItemPaidThroughDateSpecified
        {
            get { return ProrationItemPaidThroughDate != null; }
            set { }
        }

        /// <summary>
        /// Identification of a monthly housing expense component that must be paid in advance.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOEnum<ProrationItemBase> ProrationItemType;

        /// <summary>
        /// Gets or sets a value indicating whether the ProrationItemType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProrationItemType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProrationItemTypeSpecified
        {
            get { return this.ProrationItemType != null && this.ProrationItemType.enumValue != ProrationItemBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Proration Item Type.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOString ProrationItemTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ProrationItemTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProrationItemTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProrationItemTypeOtherDescriptionSpecified
        {
            get { return ProrationItemTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 10)]
        public PRORATION_ITEM_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
