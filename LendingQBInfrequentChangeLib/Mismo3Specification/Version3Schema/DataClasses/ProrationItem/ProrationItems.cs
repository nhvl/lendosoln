namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PRORATION_ITEMS
    {
        /// <summary>
        /// Gets a value indicating whether the PRORATION_ITEMS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ProrationItemSpecified;
            }
        }

        /// <summary>
        /// A collection of proration items.
        /// </summary>
        [XmlElement("PRORATION_ITEM", Order = 0)]
        public List<PRORATION_ITEM> ProrationItem = new List<PRORATION_ITEM>();

        /// <summary>
        /// Gets or sets a value indicating whether the ProrationItem element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProrationItem element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProrationItemSpecified
        {
            get { return this.ProrationItem != null && this.ProrationItem.Count(p => p != null && p.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public PRORATION_ITEMS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
