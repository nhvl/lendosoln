namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class DEAL_SET_SERVICE
    {
        /// <summary>
        /// Gets a value indicating whether the DEAL_SET_SERVICE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                if (Mismo3Utilities.ExceedsThreshold(1,
                    new List<bool> { this.DataModificationSpecified, this.ServicingTransferSpecified, this.WorkoutEvaluationSpecified }))
                {
                    throw new System.Xml.Schema.XmlSchemaValidationException(
                        Mismo3Utilities.GetXSDChoiceExceptionMessage(
                        "DEAL_SET_SERVICE",
                        new List<string> { "DATA_MODIFICATION", "SERVICING_TRANSFER", "WORKOUT_EVALUATION" }));
                }

                return this.DataModificationSpecified
                    || this.ErrorsSpecified
                    || this.ExtensionSpecified
                    || this.ServicingTransferSpecified
                    || this.StatusesSpecified
                    || this.WorkoutEvaluationSpecified;
            }
        }

        /// <summary>
        /// Modification to the data.
        /// </summary>
        [XmlElement("DATA_MODIFICATION", Order = 0)]
        public DATA_MODIFICATION DataModification;

        /// <summary>
        /// Gets or sets a value indicating whether the DataModification element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DataModification element has been assigned a value.</value>
        [XmlIgnore]
        public bool DataModificationSpecified
        {
            get { return this.DataModification != null && this.DataModification.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Transfer of the loan servicing.
        /// </summary>
        [XmlElement("SERVICING_TRANSFER", Order = 1)]
        public SERVICING_TRANSFER ServicingTransfer;

        /// <summary>
        /// Gets or sets a value indicating whether the ServicingTransfer element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServicingTransfer element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServicingTransferSpecified
        {
            get { return this.ServicingTransfer != null && this.ServicingTransfer.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Workout evaluation for the service.
        /// </summary>
        [XmlElement("WORKOUT_EVALUATION", Order = 2)]
        public WORKOUT_EVALUATION WorkoutEvaluation;

        /// <summary>
        /// Gets or sets a value indicating whether the WorkoutEvaluation element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the WorkoutEvaluation element has been assigned a value.</value>
        [XmlIgnore]
        public bool WorkoutEvaluationSpecified
        {
            get { return this.WorkoutEvaluation != null && this.WorkoutEvaluation.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Errors in the service.
        /// </summary>
        [XmlElement("ERRORS", Order = 3)]
        public ERRORS Errors;

        /// <summary>
        /// Gets or sets a value indicating whether the Errors element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Errors element has been assigned a value.</value>
        [XmlIgnore]
        public bool ErrorsSpecified
        {
            get { return this.Errors != null && this.Errors.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Statuses of the service.
        /// </summary>
        [XmlElement("STATUSES", Order = 4)]
        public STATUSES Statuses;

        /// <summary>
        /// Gets or sets a value indicating whether the Statuses element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Statuses element has been assigned a value.</value>
        [XmlIgnore]
        public bool StatusesSpecified
        {
            get { return this.Statuses != null && this.Statuses.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 5)]
        public DEAL_SET_SERVICE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
