namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CONTACT_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the CONTACT_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AttorneyInFactIndicatorSpecified
                    || this.ContactPartyIdentifierSpecified
                    || this.ExtensionSpecified
                    || this.SignatoryRoleTypeSpecified;
            }
        }

        /// <summary>
        /// Indicates that the contact party is approved to act as an attorney in fact.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIndicator AttorneyInFactIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the AttorneyInFactIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AttorneyInFactIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool AttorneyInFactIndicatorSpecified
        {
            get { return AttorneyInFactIndicator != null; }
            set { }
        }

        /// <summary>
        /// A unique identifier for the contact party as defined by trading partners.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOIdentifier ContactPartyIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the ContactPartyIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ContactPartyIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool ContactPartyIdentifierSpecified
        {
            get { return ContactPartyIdentifier != null; }
            set { }
        }

        /// <summary>
        /// An authorized representative who is required to sign documents on behalf of the legal entity. To be used in conjunction with Required Signatory Count. If the rule varies per document, this information must be provided under DOCUMENT/DEAL_SETS.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<SignatoryRoleBase> SignatoryRoleType;

        /// <summary>
        /// Gets or sets a value indicating whether the SignatoryRoleType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SignatoryRoleType element has been assigned a value.</value>
        [XmlIgnore]
        public bool SignatoryRoleTypeSpecified
        {
            get { return this.SignatoryRoleType != null && this.SignatoryRoleType.enumValue != SignatoryRoleBase.Blank; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public CONTACT_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
