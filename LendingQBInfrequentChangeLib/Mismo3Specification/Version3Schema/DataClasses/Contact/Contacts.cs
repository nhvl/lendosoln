namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CONTACTS
    {
        /// <summary>
        /// Gets a value indicating whether the CONTACTS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ContactSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// Contact information points for an organization.
        /// </summary>
        [XmlElement("CONTACT", Order = 0)]
		public List<CONTACT> Contact = new List<CONTACT>();

        /// <summary>
        /// Gets or sets a value indicating whether the Contact element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Contact element has been assigned a value.</value>
        [XmlIgnore]
        public bool ContactSpecified
        {
            get { return this.Contact != null && this.Contact.Count(c => c != null && c.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public CONTACTS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
