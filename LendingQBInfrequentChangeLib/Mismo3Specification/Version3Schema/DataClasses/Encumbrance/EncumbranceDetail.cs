namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class ENCUMBRANCE_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the ENCUMBRANCE_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.EncumbranceTypeOtherDescriptionSpecified
                    || this.EncumbranceTypeSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// Identifies a right to or interest in the property that may affect value.  Certain restrictions require the appraiser to indicate with additional addendum text such as Deed Restrictions on Community Land Trust properties.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<EncumbranceBase> EncumbranceType;

        /// <summary>
        /// Gets or sets a value indicating whether the EncumbranceType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EncumbranceType element has been assigned a value.</value>
        [XmlIgnore]
        public bool EncumbranceTypeSpecified
        {
            get { return this.EncumbranceType != null && this.EncumbranceType.enumValue != EncumbranceBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString EncumbranceTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the EncumbranceTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EncumbranceTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool EncumbranceTypeOtherDescriptionSpecified
        {
            get { return EncumbranceTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public ENCUMBRANCE_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
