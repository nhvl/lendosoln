namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class ENCUMBRANCES
    {
        /// <summary>
        /// Gets a value indicating whether the ENCUMBRANCES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.EncumbranceSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of encumbrances.
        /// </summary>
        [XmlElement("ENCUMBRANCE", Order = 0)]
		public List<ENCUMBRANCE> Encumbrance = new List<ENCUMBRANCE>();

        /// <summary>
        /// Gets or sets a value indicating whether the Encumbrance element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Encumbrance element has been assigned a value.</value>
        [XmlIgnore]
        public bool EncumbranceSpecified
        {
            get { return this.Encumbrance != null && this.Encumbrance.Count(e => e != null && e.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public ENCUMBRANCES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
