namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class TREASURY_NPV
    {
        /// <summary>
        /// Gets a value indicating whether the TREASURY_NPV container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.LoanRiskPremiumPercentSpecified
                    || this.PayForPerformanceQualificationIndicatorSpecified
                    || this.TreasuryNPVAssociationFeesPreWorkoutAmountSpecified
                    || this.TreasuryNPVBaseVersionIdentifierSpecified
                    || this.TreasuryNPVCurrentEstimatedDefaultRatePercentSpecified
                    || this.TreasuryNPVDateSpecified
                    || this.TreasuryNPVLoanAmortizationTypeOtherDescriptionSpecified
                    || this.TreasuryNPVLoanAmortizationTypeSpecified
                    || this.TreasuryNPVMonthlyHazardAndFloodInsuranceAmountSpecified
                    || this.TreasuryNPVMonthsPastDueCountSpecified
                    || this.TreasuryNPVPositiveTestResultIndicatorSpecified
                    || this.TreasuryNPVPostWorkoutEstimatedDefaultRatePercentSpecified
                    || this.TreasuryNPVResultPostModificationAmountSpecified
                    || this.TreasuryNPVResultPreModificationAmountSpecified
                    || this.TreasuryNPVRunDateSpecified
                    || this.TreasuryNPVSuccessfulRunIndicatorSpecified
                    || this.TreasuryNPVSystemExceptionIndicatorSpecified
                    || this.TreasuryNPVWaterfallTestIndicatorSpecified;
            }
        }

        /// <summary>
        /// A premium, expressed as a percent,  based upon the difference between the interest rate of the loan and the market rate.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOPercent LoanRiskPremiumPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanRiskPremiumPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanRiskPremiumPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanRiskPremiumPercentSpecified
        {
            get { return LoanRiskPremiumPercent != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that the loan meets the criteria to be included in a pay for performance program.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOIndicator PayForPerformanceQualificationIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PayForPerformanceQualificationIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PayForPerformanceQualificationIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PayForPerformanceQualificationIndicatorSpecified
        {
            get { return PayForPerformanceQualificationIndicator != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of association dues and or fees, pre workout, plus any future escrow shortage payment amount.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOAmount TreasuryNPVAssociationFeesPreWorkoutAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the TreasuryNPVAssociationFeesPreWorkoutAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TreasuryNPVAssociationFeesPreWorkoutAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TreasuryNPVAssociationFeesPreWorkoutAmountSpecified
        {
            get { return TreasuryNPVAssociationFeesPreWorkoutAmount != null; }
            set { }
        }

        /// <summary>
        /// A unique identifier of the version of the base net present value calculator that was used in the assessment.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOIdentifier TreasuryNPVBaseVersionIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the TreasuryNPVBaseVersionIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TreasuryNPVBaseVersionIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool TreasuryNPVBaseVersionIdentifierSpecified
        {
            get { return TreasuryNPVBaseVersionIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The estimated rate at which the loan is expected to default prior to workout based on the Treasury Net Present Value calculation.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOPercent TreasuryNPVCurrentEstimatedDefaultRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the TreasuryNPVCurrentEstimatedDefaultRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TreasuryNPVCurrentEstimatedDefaultRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool TreasuryNPVCurrentEstimatedDefaultRatePercentSpecified
        {
            get { return TreasuryNPVCurrentEstimatedDefaultRatePercent != null; }
            set { }
        }

        /// <summary>
        /// The date on which the loan is evaluated for trial eligibility for the first time.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMODate TreasuryNPVDate;

        /// <summary>
        /// Gets or sets a value indicating whether the TreasuryNPVDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TreasuryNPVDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool TreasuryNPVDateSpecified
        {
            get { return TreasuryNPVDate != null; }
            set { }
        }

        /// <summary>
        /// The mortgage product of the loan, before the workout, as submitted to Treasury Net Present Value calculator.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOEnum<TreasuryNPVLoanAmortizationBase> TreasuryNPVLoanAmortizationType;

        /// <summary>
        /// Gets or sets a value indicating whether the TreasuryNPVLoanAmortizationType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TreasuryNPVLoanAmortizationType element has been assigned a value.</value>
        [XmlIgnore]
        public bool TreasuryNPVLoanAmortizationTypeSpecified
        {
            get { return this.TreasuryNPVLoanAmortizationType != null && this.TreasuryNPVLoanAmortizationType.enumValue != TreasuryNPVLoanAmortizationBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected as the Treasury NPV Loan Amortization Type.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOString TreasuryNPVLoanAmortizationTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the TreasuryNPVLoanAmortizationTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TreasuryNPVLoanAmortizationTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool TreasuryNPVLoanAmortizationTypeOtherDescriptionSpecified
        {
            get { return TreasuryNPVLoanAmortizationTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The borrower's monthly payment for hazard and flood insurance based on the Treasury Net Present Value calculation.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOAmount TreasuryNPVMonthlyHazardAndFloodInsuranceAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the TreasuryNPVMonthlyHazardAndFloodInsuranceAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TreasuryNPVMonthlyHazardAndFloodInsuranceAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TreasuryNPVMonthlyHazardAndFloodInsuranceAmountSpecified
        {
            get { return TreasuryNPVMonthlyHazardAndFloodInsuranceAmount != null; }
            set { }
        }

        /// <summary>
        /// The number of months between the data collection date and the current last paid installment date for the subject loan based on investor guidelines.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOCount TreasuryNPVMonthsPastDueCount;

        /// <summary>
        /// Gets or sets a value indicating whether the TreasuryNPVMonthsPastDueCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TreasuryNPVMonthsPastDueCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TreasuryNPVMonthsPastDueCountSpecified
        {
            get { return TreasuryNPVMonthsPastDueCount != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates the results of the Treasury Net Present Value test is positive.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOIndicator TreasuryNPVPositiveTestResultIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the TreasuryNPVPositiveTestResultIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TreasuryNPVPositiveTestResultIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool TreasuryNPVPositiveTestResultIndicatorSpecified
        {
            get { return TreasuryNPVPositiveTestResultIndicator != null; }
            set { }
        }

        /// <summary>
        /// The estimated rate at which the loan is expected to default after a workout based on the Treasury Net Present Value calculation.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOPercent TreasuryNPVPostWorkoutEstimatedDefaultRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the TreasuryNPVPostWorkoutEstimatedDefaultRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TreasuryNPVPostWorkoutEstimatedDefaultRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool TreasuryNPVPostWorkoutEstimatedDefaultRatePercentSpecified
        {
            get { return TreasuryNPVPostWorkoutEstimatedDefaultRatePercent != null; }
            set { }
        }

        /// <summary>
        /// The amount generated from the Treasury Net Present Value calculator assuming the modification occurs.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOAmount TreasuryNPVResultPostModificationAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the TreasuryNPVResultPostModificationAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TreasuryNPVResultPostModificationAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TreasuryNPVResultPostModificationAmountSpecified
        {
            get { return TreasuryNPVResultPostModificationAmount != null; }
            set { }
        }

        /// <summary>
        /// The amount generated from the Treasury Net Present Value calculator assuming no modification occurs.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOAmount TreasuryNPVResultPreModificationAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the TreasuryNPVResultPreModificationAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TreasuryNPVResultPreModificationAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TreasuryNPVResultPreModificationAmountSpecified
        {
            get { return TreasuryNPVResultPreModificationAmount != null; }
            set { }
        }

        /// <summary>
        /// The date on which the Treasury Net Present Value test was run or rerun.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMODate TreasuryNPVRunDate;

        /// <summary>
        /// Gets or sets a value indicating whether the TreasuryNPVRunDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TreasuryNPVRunDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool TreasuryNPVRunDateSpecified
        {
            get { return TreasuryNPVRunDate != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that the Treasury Net Present Value test was successfully run.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOIndicator TreasuryNPVSuccessfulRunIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the TreasuryNPVSuccessfulRunIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TreasuryNPVSuccessfulRunIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool TreasuryNPVSuccessfulRunIndicatorSpecified
        {
            get { return TreasuryNPVSuccessfulRunIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates there was a system exception encountered during net present value file submission.
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMOIndicator TreasuryNPVSystemExceptionIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the TreasuryNPVSystemExceptionIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TreasuryNPVSystemExceptionIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool TreasuryNPVSystemExceptionIndicatorSpecified
        {
            get { return TreasuryNPVSystemExceptionIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates the loan has passed the Treasury Net Present Value Waterfall Test. The waterfall test compares the HAMP modified loan terms and forbearance provided by the servicer with those calculated by a net present value calculator.
        /// </summary>
        [XmlElement(Order = 17)]
        public MISMOIndicator TreasuryNPVWaterfallTestIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the TreasuryNPVWaterfallTestIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TreasuryNPVWaterfallTestIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool TreasuryNPVWaterfallTestIndicatorSpecified
        {
            get { return TreasuryNPVWaterfallTestIndicator != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 18)]
        public TREASURY_NPV_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
