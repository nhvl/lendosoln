namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class MI_VALIDATION_REQUEST
    {
        /// <summary>
        /// Gets a value indicating whether the MI_VALIDATION_REQUEST container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.MIValidationRequestDetailSpecified;
            }
        }

        /// <summary>
        /// Details on a mortgage insurance validation request.
        /// </summary>
        [XmlElement("MI_VALIDATION_REQUEST_DETAIL", Order = 0)]
        public MI_VALIDATION_REQUEST_DETAIL MIValidationRequestDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the Mortgage Insurance Validation Request Detail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Mortgage Insurance Validation Request Detail element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIValidationRequestDetailSpecified
        {
            get { return this.MIValidationRequestDetail != null && this.MIValidationRequestDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public MI_VALIDATION_REQUEST_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
