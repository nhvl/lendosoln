namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class MI_VALIDATION_REQUEST_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the MI_VALIDATION_REQUEST_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.MIValidationRequestTypeOtherDescriptionSpecified
                    || this.MIValidationRequestTypeSpecified;
            }
        }

        /// <summary>
        /// To distinguish an original MI validation request for private mortgage insurance from any subsequent validation requests for the same loan.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<MIValidationRequestBase> MIValidationRequestType;

        /// <summary>
        /// Gets or sets a value indicating whether the MIValidationRequestType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIValidationRequestType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIValidationRequestTypeSpecified
        {
            get { return this.MIValidationRequestType != null && this.MIValidationRequestType.enumValue != MIValidationRequestBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field to collect MI validation request type when Other is selected for MI Validation Request Type.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString MIValidationRequestTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MIValidationRequestTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIValidationRequestTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIValidationRequestTypeOtherDescriptionSpecified
        {
            get { return MIValidationRequestTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public MI_VALIDATION_REQUEST_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
