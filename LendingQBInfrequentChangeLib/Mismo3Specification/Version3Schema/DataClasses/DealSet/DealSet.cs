namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class DEAL_SET
    {
        /// <summary>
        /// Gets a value indicating whether the DEAL_SET container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ACHSpecified
                    || this.CashRemittanceSummaryNotificationSpecified
                    || this.DealSetExpensesSpecified
                    || this.DealSetIncomesSpecified
                    || this.DealsSpecified
                    || this.ExtensionSpecified
                    || this.InvestorFeaturesSpecified
                    || this.PartiesSpecified
                    || this.PoolSpecified
                    || this.RelationshipsSpecified
                    || this.ServicerReportingSpecified;
            }
        }

        /// <summary>
        /// ACH for this deal set.
        /// </summary>
        [XmlElement("ACH", Order = 0)]
        public ACH ACH;

        /// <summary>
        /// Gets or sets a value indicating whether the ACH element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ACH element has been assigned a value.</value>
        [XmlIgnore]
        public bool ACHSpecified
        {
            get { return this.ACH != null && this.ACH.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Cash remittance summary notification for this deal set.
        /// </summary>
        [XmlElement("CASH_REMITTANCE_SUMMARY_NOTIFICATION", Order = 1)]
        public CASH_REMITTANCE_SUMMARY_NOTIFICATION CashRemittanceSummaryNotification;

        /// <summary>
        /// Gets or sets a value indicating whether the CashRemittanceSummaryNotification element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CashRemittanceSummaryNotification element has been assigned a value.</value>
        [XmlIgnore]
        public bool CashRemittanceSummaryNotificationSpecified
        {
            get { return this.CashRemittanceSummaryNotification != null && this.CashRemittanceSummaryNotification.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Expenses in this deal set.
        /// </summary>
        [XmlElement("DEAL_SET_EXPENSES", Order = 2)]
        public DEAL_SET_EXPENSES DealSetExpenses;

        /// <summary>
        /// Gets or sets a value indicating whether the DealSetExpenses element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DealSetExpenses element has been assigned a value.</value>
        [XmlIgnore]
        public bool DealSetExpensesSpecified
        {
            get { return this.DealSetExpenses != null && this.DealSetExpenses.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Incomes in this deal set.
        /// </summary>
        [XmlElement("DEAL_SET_INCOMES", Order = 3)]
        public DEAL_SET_INCOMES DealSetIncomes;

        /// <summary>
        /// Gets or sets a value indicating whether the DealSetIncomes element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DealSetIncomes element has been assigned a value.</value>
        [XmlIgnore]
        public bool DealSetIncomesSpecified
        {
            get { return this.DealSetIncomes != null && this.DealSetIncomes.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Deals in this deal set.
        /// </summary>
        [XmlElement("DEALS", Order = 4)]
        public DEALS Deals;

        /// <summary>
        /// Gets or sets a value indicating whether the Deals element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Deals element has been assigned a value.</value>
        [XmlIgnore]
        public bool DealsSpecified
        {
            get { return this.Deals != null && this.Deals.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Investor features associated with this deal set.
        /// </summary>
        [XmlElement("INVESTOR_FEATURES", Order = 5)]
        public INVESTOR_FEATURES InvestorFeatures;

        /// <summary>
        /// Gets or sets a value indicating whether the InvestorFeatures element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InvestorFeatures element has been assigned a value.</value>
        [XmlIgnore]
        public bool InvestorFeaturesSpecified
        {
            get { return this.InvestorFeatures != null && this.InvestorFeatures.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Parties associated with this deal set.
        /// </summary>
        [XmlElement("PARTIES", Order = 6)]
        public PARTIES Parties;

        /// <summary>
        /// Gets or sets a value indicating whether the Parties element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Parties element has been assigned a value.</value>
        [XmlIgnore]
        public bool PartiesSpecified
        {
            get { return this.Parties != null && this.Parties.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Pool associated with this deal set.
        /// </summary>
        [XmlElement("POOL", Order = 7)]
        public POOL Pool;

        /// <summary>
        /// Gets or sets a value indicating whether the Pool element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Pool element has been assigned a value.</value>
        [XmlIgnore]
        public bool PoolSpecified
        {
            get { return this.Pool != null && this.Pool.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Relationships associated with this deal set.
        /// </summary>
        [XmlElement("RELATIONSHIPS", Order = 8)]
        public RELATIONSHIPS Relationships;

        /// <summary>
        /// Gets or sets a value indicating whether the Relationships element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Relationships element has been assigned a value.</value>
        [XmlIgnore]
        public bool RelationshipsSpecified
        {
            get { return this.Relationships != null && this.Relationships.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Servicer reporting under this deal set.
        /// </summary>
        [XmlElement("SERVICER_REPORTING", Order = 9)]
        public SERVICER_REPORTING ServicerReporting;

        /// <summary>
        /// Gets or sets a value indicating whether the ServicerReporting element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServicerReporting element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServicerReportingSpecified
        {
            get { return this.ServicerReporting != null && this.ServicerReporting.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 10)]
        public DEAL_SET_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
