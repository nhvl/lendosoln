namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class DEAL_SETS
    {
        /// <summary>
        /// Gets a value indicating whether the DEAL_SETS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DealSetServicesSpecified
                    || this.DealSetSpecified
                    || this.ExtensionSpecified
                    || this.PartiesSpecified
                    || this.RelationshipsSpecified
                    || this.VerificationDataSpecified;
            }
        }

        /// <summary>
        /// A list of deal sets.
        /// </summary>
        [XmlElement("DEAL_SET", Order = 0)]
		public List<DEAL_SET> DealSet = new List<DEAL_SET>();

        /// <summary>
        /// Gets or sets a value indicating whether the DealSet element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DealSet element has been assigned a value.</value>
        [XmlIgnore]
        public bool DealSetSpecified
        {
            get { return this.DealSet != null && this.DealSet.Count(d => d != null && d.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// Services for these deal sets.
        /// </summary>
        [XmlElement("DEAL_SET_SERVICES", Order = 1)]
        public DEAL_SET_SERVICES DealSetServices;

        /// <summary>
        /// Gets or sets a value indicating whether the DealSetServices element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DealSetServices element has been assigned a value.</value>
        [XmlIgnore]
        public bool DealSetServicesSpecified
        {
            get { return this.DealSetServices != null && this.DealSetServices.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Parties associated with these deal sets.
        /// </summary>
        [XmlElement("PARTIES", Order = 2)]
        public PARTIES Parties;

        /// <summary>
        /// Gets or sets a value indicating whether the Parties element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Parties element has been assigned a value.</value>
        [XmlIgnore]
        public bool PartiesSpecified
        {
            get { return this.Parties != null && this.Parties.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Relationships associated with these deal sets.
        /// </summary>
        [XmlElement("RELATIONSHIPS", Order = 3)]
        public RELATIONSHIPS Relationships;

        /// <summary>
        /// Gets or sets a value indicating whether the Relationships element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Relationships element has been assigned a value.</value>
        [XmlIgnore]
        public bool RelationshipsSpecified
        {
            get { return this.Relationships != null && this.Relationships.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Verification data for these deal sets.
        /// </summary>
        [XmlElement("VERIFICATION_DATA", Order = 4)]
        public VERIFICATION_DATA VerificationData;

        /// <summary>
        /// Gets or sets a value indicating whether the VerificationData element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the VerificationData element has been assigned a value.</value>
        [XmlIgnore]
        public bool VerificationDataSpecified
        {
            get { return this.VerificationData != null && this.VerificationData.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 5)]
        public DEAL_SETS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
