namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class FOUNDATIONS
    {
        /// <summary>
        /// Gets a value indicating whether the FOUNDATIONS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.FoundationSpecified;
            }
        }

        /// <summary>
        /// A collection of foundations.
        /// </summary>
        [XmlElement("FOUNDATION", Order = 0)]
		public List<FOUNDATION> Foundation = new List<FOUNDATION>();

        /// <summary>
        /// Gets or sets a value indicating whether the Foundation element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Foundation element has been assigned a value.</value>
        [XmlIgnore]
        public bool FoundationSpecified
        {
            get { return this.Foundation != null && this.Foundation.Count(f => f != null && f.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public FOUNDATIONS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
