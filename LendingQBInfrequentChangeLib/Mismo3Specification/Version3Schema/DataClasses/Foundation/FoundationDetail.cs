namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class FOUNDATION_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the FOUNDATION_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ComponentAdjustmentAmountSpecified
                    || this.ConditionRatingDescriptionSpecified
                    || this.ConditionRatingTypeSpecified
                    || this.ExtensionSpecified
                    || this.FoundationConditionDescriptionSpecified
                    || this.FoundationDeficienciesTypeOtherDescriptionSpecified
                    || this.FoundationDeficienciesTypeSpecified
                    || this.FoundationDescriptionSpecified
                    || this.FoundationExistsIndicatorSpecified
                    || this.FoundationMaterialTypeOtherDescriptionSpecified
                    || this.FoundationMaterialTypeSpecified
                    || this.FoundationTypeOtherDescriptionSpecified
                    || this.FoundationTypeSpecified
                    || this.MaterialDescriptionSpecified
                    || this.QualityRatingDescriptionSpecified
                    || this.QualityRatingTypeSpecified;
            }
        }

        /// <summary>
        /// The dollar amount (either positive or negative) adjustment being made for a specific component of the property.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount ComponentAdjustmentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ComponentAdjustmentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ComponentAdjustmentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ComponentAdjustmentAmountSpecified
        {
            get { return ComponentAdjustmentAmount != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe in detail the condition rating of the identified component.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString ConditionRatingDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ConditionRatingDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConditionRatingDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConditionRatingDescriptionSpecified
        {
            get { return ConditionRatingDescription != null; }
            set { }
        }

        /// <summary>
        /// A rating of the condition of the identified component.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<ConditionRatingBase> ConditionRatingType;

        /// <summary>
        /// Gets or sets a value indicating whether the ConditionRatingType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConditionRatingType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConditionRatingTypeSpecified
        {
            get { return this.ConditionRatingType != null && this.ConditionRatingType.enumValue != ConditionRatingBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field describing the condition of the foundation in general.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString FoundationConditionDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the FoundationConditionDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FoundationConditionDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool FoundationConditionDescriptionSpecified
        {
            get { return FoundationConditionDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates a type of condition of the foundation which is described by Foundation Condition Comment.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<FoundationDeficienciesBase> FoundationDeficienciesType;

        /// <summary>
        /// Gets or sets a value indicating whether the FoundationDeficienciesType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FoundationDeficienciesType element has been assigned a value.</value>
        [XmlIgnore]
        public bool FoundationDeficienciesTypeSpecified
        {
            get { return this.FoundationDeficienciesType != null && this.FoundationDeficienciesType.enumValue != FoundationDeficienciesBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the foundation condition if Other is selected as the Foundation Condition Type.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString FoundationDeficienciesTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the FoundationDeficienciesTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FoundationDeficienciesTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool FoundationDeficienciesTypeOtherDescriptionSpecified
        {
            get { return FoundationDeficienciesTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe in detail the foundation as identified by the Foundation Type.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOString FoundationDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the FoundationDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FoundationDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool FoundationDescriptionSpecified
        {
            get { return FoundationDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates that there is a foundation.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOIndicator FoundationExistsIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the FoundationExistsIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FoundationExistsIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool FoundationExistsIndicatorSpecified
        {
            get { return FoundationExistsIndicator != null; }
            set { }
        }

        /// <summary>
        /// Specifies the type of material used in the foundation.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOEnum<FoundationMaterialBase> FoundationMaterialType;

        /// <summary>
        /// Gets or sets a value indicating whether the FoundationMaterialType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FoundationMaterialType element has been assigned a value.</value>
        [XmlIgnore]
        public bool FoundationMaterialTypeSpecified
        {
            get { return this.FoundationMaterialType != null && this.FoundationMaterialType.enumValue != FoundationMaterialBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Foundation Material Type.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOString FoundationMaterialTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the FoundationMaterialTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FoundationMaterialTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool FoundationMaterialTypeOtherDescriptionSpecified
        {
            get { return FoundationMaterialTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Describes the construction style of the foundation of the structure.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOEnum<FoundationBase> FoundationType;

        /// <summary>
        /// Gets or sets a value indicating whether the FoundationType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FoundationType element has been assigned a value.</value>
        [XmlIgnore]
        public bool FoundationTypeSpecified
        {
            get { return this.FoundationType != null && this.FoundationType.enumValue != FoundationBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the foundation type if Other is selected as the Foundation Type.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOString FoundationTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the FoundationTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FoundationTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool FoundationTypeOtherDescriptionSpecified
        {
            get { return FoundationTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe in detail the material used in the identified component.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOString MaterialDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MaterialDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MaterialDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MaterialDescriptionSpecified
        {
            get { return MaterialDescription != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe in detail the quality rating of the identified component.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOString QualityRatingDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the QualityRatingDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the QualityRatingDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool QualityRatingDescriptionSpecified
        {
            get { return QualityRatingDescription != null; }
            set { }
        }

        /// <summary>
        /// A rating of the quality of the identified component type.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOEnum<QualityRatingBase> QualityRatingType;

        /// <summary>
        /// Gets or sets a value indicating whether the QualityRatingType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the QualityRatingType element has been assigned a value.</value>
        [XmlIgnore]
        public bool QualityRatingTypeSpecified
        {
            get { return this.QualityRatingType != null && this.QualityRatingType.enumValue != QualityRatingBase.Blank; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 15)]
        public FOUNDATION_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
