namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class FOUNDATION
    {
        /// <summary>
        /// Gets a value indicating whether the FOUNDATION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.FoundationDetailSpecified
                    || this.FoundationFeaturesSpecified
                    || this.FoundationWallsSpecified;
            }
        }

        /// <summary>
        /// Details of the foundation.
        /// </summary>
        [XmlElement("FOUNDATION_DETAIL", Order = 0)]
        public FOUNDATION_DETAIL FoundationDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the FoundationDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FoundationDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool FoundationDetailSpecified
        {
            get { return this.FoundationDetail != null && this.FoundationDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Features of the foundation.
        /// </summary>
        [XmlElement("FOUNDATION_FEATURES", Order = 1)]
        public FOUNDATION_FEATURES FoundationFeatures;

        /// <summary>
        /// Gets or sets a value indicating whether the FoundationFeatures element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FoundationFeatures element has been assigned a value.</value>
        [XmlIgnore]
        public bool FoundationFeaturesSpecified
        {
            get { return this.FoundationFeatures != null && this.FoundationFeatures.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The walls of the foundation.
        /// </summary>
        [XmlElement("FOUNDATION_WALLS", Order = 2)]
        public FOUNDATION_WALLS FoundationWalls;

        /// <summary>
        /// Gets or sets a value indicating whether the FoundationWalls element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FoundationWalls element has been assigned a value.</value>
        [XmlIgnore]
        public bool FoundationWallsSpecified
        {
            get { return this.FoundationWalls != null && this.FoundationWalls.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public FOUNDATION_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
