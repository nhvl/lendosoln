namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class DOCUMENTS
    {
        /// <summary>
        /// Gets a value indicating whether the DOCUMENTS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DocumentSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A container for all documents in the set.
        /// </summary>
        [XmlElement("DOCUMENT", Order = 0)]
		public List<DOCUMENT> Document = new List<DOCUMENT>();

        /// <summary>
        /// Gets or sets a value indicating whether the Document element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Document element has been assigned a value.</value>
        [XmlIgnore]
        public bool DocumentSpecified
        {
            get { return this.Document != null && this.Document.Count(d => d != null && d.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public DOCUMENTS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
