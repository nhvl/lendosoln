namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the Mortgage Industry Standards Maintenance Organization reference model identifier can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the Mortgage Industry Standards Maintenance Organization reference model identifier can be serialized.</returns>
    public partial class DOCUMENT
    {
        /// <summary>
        /// Gets a value indicating whether the DOCUMENT container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                if (Mismo3Utilities.ExceedsThreshold(1,
                    new List<bool> { 
                        this.ReferenceSpecified, 
                        this.AboutVersionsSpecified
                        || this.AuditTrailSpecified
                        || this.DealSetsSpecified
                        || this.DocumentClassificationSpecified
                        || this.ExtensionSpecified
                        || this.MapSpecified
                        || this.RelationshipsSpecified
                        || this.SignatoriesSpecified
                        || this.SystemSignaturesSpecified
                        || this.UnknownVersion3DocumentSpecified
                        || this.ViewsSpecified }))
                {
                    throw new System.Xml.Schema.XmlSchemaValidationException(
                        Mismo3Utilities.GetXSDChoiceExceptionMessage(
                        "DOCUMENT",
                        new List<string> { "REFERENCE", "Any other child element of the DOCUMENT" }));
                }

                if (Mismo3Utilities.ExceedsThreshold(1,
                    new List<bool> { 
                        this.UnknownVersion3DocumentSpecified,
                        this.AuditTrailSpecified
                        || this.DealSetsSpecified
                        || this.MapSpecified
                        || this.RelationshipsSpecified
                        || this.SignatoriesSpecified
                        || this.SystemSignaturesSpecified
                        || this.ViewsSpecified }))
                {
                    throw new System.Xml.Schema.XmlSchemaValidationException(
                        Mismo3Utilities.GetXSDChoiceExceptionMessage(
                        "DOCUMENT",
                        new List<string> { "UNKNOWN_VERSION3_DOCUMENT", "AUDIT_TRAIL, DEAL_SETS, MAP, RELATIONSHIPS, SIGNATORIES, SYSTEM_SIGNATURES, and/or VIEWS" }));
                }

                return this.AboutVersionsSpecified
                    || this.AuditTrailSpecified
                    || this.DealSetsSpecified
                    || this.DocumentClassificationSpecified
                    || this.ExtensionSpecified
                    || this.MapSpecified
                    || this.ReferenceSpecified
                    || this.RelationshipsSpecified
                    || this.SignatoriesSpecified
                    || this.SystemSignaturesSpecified
                    || this.UnknownVersion3DocumentSpecified
                    || this.ViewsSpecified;
            }
        }

        /// <summary>
        /// Meta-data about the document.
        /// </summary>
        [XmlElement("ABOUT_VERSIONS", Order = 0)]
        public ABOUT_VERSIONS AboutVersions;

        /// <summary>
        /// Gets or sets a value indicating whether the AboutVersions element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AboutVersions element has been assigned a value.</value>
        [XmlIgnore]
        public bool AboutVersionsSpecified
        {
            get { return this.AboutVersions != null && this.AboutVersions.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Audit information related to the document.
        /// </summary>
        [XmlElement("AUDIT_TRAIL", Order = 1)]
        public AUDIT_TRAIL AuditTrail;

        /// <summary>
        /// Gets or sets a value indicating whether the AuditTrail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AuditTrail element has been assigned a value.</value>
        [XmlIgnore]
        public bool AuditTrailSpecified
        {
            get { return this.AuditTrail != null && this.AuditTrail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Deals related to the document.
        /// </summary>
        [XmlElement("DEAL_SETS", Order = 2)]
        public DEAL_SETS DealSets;

        /// <summary>
        /// Gets or sets a value indicating whether the DealSets element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DealSets element has been assigned a value.</value>
        [XmlIgnore]
        public bool DealSetsSpecified
        {
            get { return this.DealSets != null && this.DealSets.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The classification of this document.
        /// </summary>
        [XmlElement("DOCUMENT_CLASSIFICATION", Order = 3)]
        public DOCUMENT_CLASSIFICATION DocumentClassification;

        /// <summary>
        /// Gets or sets a value indicating whether the DocumentClassification element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DocumentClassification element has been assigned a value.</value>
        [XmlIgnore]
        public bool DocumentClassificationSpecified
        {
            get { return this.DocumentClassification != null && this.DocumentClassification.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public DOCUMENT_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// A mapping related to this document.
        /// </summary>
        [XmlElement("MAP", Order = 5)]
        public MAP Map;

        /// <summary>
        /// Gets or sets a value indicating whether the Map element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Map element has been assigned a value.</value>
        [XmlIgnore]
        public bool MapSpecified
        {
            get { return this.Map != null && this.Map.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Any references related to this document.
        /// </summary>
        [XmlElement("REFERENCE", Order = 6)]
        public REFERENCE Reference;

        /// <summary>
        /// Gets or sets a value indicating whether the Reference element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Reference element has been assigned a value.</value>
        [XmlIgnore]
        public bool ReferenceSpecified
        {
            get { return this.Reference != null && this.Reference.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Any relationships related to this document.
        /// </summary>
        [XmlElement("RELATIONSHIPS", Order = 7)]
        public RELATIONSHIPS Relationships;

        /// <summary>
        /// Gets or sets a value indicating whether the Relationships element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Relationships element has been assigned a value.</value>
        [XmlIgnore]
        public bool RelationshipsSpecified
        {
            get { return this.Relationships != null && this.Relationships.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Any signatories on this document.
        /// </summary>
        [XmlElement("SIGNATORIES", Order = 8)]
        public SIGNATORIES Signatories;

        /// <summary>
        /// Gets or sets a value indicating whether the Signatories element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Signatories element has been assigned a value.</value>
        [XmlIgnore]
        public bool SignatoriesSpecified
        {
            get { return this.Signatories != null && this.Signatories.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Any system signatures on this document.
        /// </summary>
        [XmlElement("SYSTEM_SIGNATURES", Order = 9)]
        public SYSTEM_SIGNATURES SystemSignatures;

        /// <summary>
        /// Gets or sets a value indicating whether the SystemSignatures element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SystemSignatures element has been assigned a value.</value>
        [XmlIgnore]
        public bool SystemSignaturesSpecified
        {
            get { return this.SystemSignatures != null && this.SystemSignatures.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Unknown Version 3 document.
        /// </summary>
        [XmlElement("UNKNOWN_VERSION3_DOCUMENT", Order = 10)]
        public UNKNOWN_VERSION3_DOCUMENT UnknownVersion3Document;

        /// <summary>
        /// Gets or sets a value indicating whether the UnknownVersion3Document element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the UnknownVersion3Document element has been assigned a value.</value>
        [XmlIgnore]
        public bool UnknownVersion3DocumentSpecified
        {
            get { return this.UnknownVersion3Document != null && this.UnknownVersion3Document.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Any views on this document.
        /// </summary>
        [XmlElement("VIEWS", Order = 11)]
        public VIEWS Views;

        /// <summary>
        /// Gets or sets a value indicating whether the Views element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Views element has been assigned a value.</value>
        [XmlIgnore]
        public bool ViewsSpecified
        {
            get { return this.Views != null && this.Views.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }

        /// <summary>
        /// The Mortgage Industry Standards Maintenance Organization Logical Data Dictionary Identifier is a unique value that represents the version of the Mortgage Industry Standards Maintenance Organization LDD section of the reference model to which the containing XML instance document complies.
        /// </summary>
        //// Pattern: 3\.\d*((\.\d*)?(\.\d*)?(\[[B]\d*(\-\d*)?\])?)?
        [XmlAttribute]
        public string MISMOLogicalDataDictionaryIdentifier;

        /// <summary>
        /// Indicates whether the Mortgage Industry Standards Maintenance Organization logical data dictionary identifier can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the Mortgage Industry Standards Maintenance Organization logical data dictionary identifier can be serialized.</returns>
        public bool ShouldSerializeMISMOLogicalDataDictionaryIdentifier()
        {
            return !string.IsNullOrEmpty(MISMOLogicalDataDictionaryIdentifier);
        }

        /// <summary>
        /// The Mortgage Industry Standards Maintenance Organization Reference Model Identifier is a unique value that represents the version of the Mortgage Industry Standards Maintenance Organization reference model to which the containing XML instance document complies.
        /// </summary>
        //// Pattern: 3\.\d*((\.\d*)?(\[[B]\d*(\-\d*)?\])?)?
        [XmlAttribute]
        public string MISMOReferenceModelIdentifier;

        /// <summary>
        /// Indicates whether the Mortgage Industry Standards Maintenance Organization reference model identifier can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the Mortgage Industry Standards Maintenance Organization reference model identifier can be serialized.</returns>
        public bool ShouldSerializeMISMOReferenceModelIdentifier()
        {
            return !string.IsNullOrEmpty(MISMOReferenceModelIdentifier);
        }
    }
}
