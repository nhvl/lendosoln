namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class BIRTH_INFORMATION
    {
        /// <summary>
        /// Gets a value indicating whether the BIRTH_INFORMATION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BirthDateSpecified
                    || this.CityTownVillageOfBirthNameSpecified
                    || this.CountryOfBirthNameSpecified
                    || this.ExtensionSpecified
                    || this.MothersMaidenNameSpecified
                    || this.StateProvinceOfBirthNameSpecified;
            }
        }

        /// <summary>
        /// The date of birth for the current INDIVIDUAL or CONTACT as a means of verifying identity. Note: For Party Role Type = Borrower, this would duplicate an existing field Borrower Birth Date. Actual usage would be dictated by implementation.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMODate BirthDate;

        /// <summary>
        /// Gets or sets a value indicating whether the BirthDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BirthDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool BirthDateSpecified
        {
            get { return BirthDate != null; }
            set { }
        }

        /// <summary>
        /// The City, Town or Village of birth of the individual.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString CityTownVillageOfBirthName;

        /// <summary>
        /// Gets or sets a value indicating whether the CityTownVillageOfBirthName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CityTownVillageOfBirthName element has been assigned a value.</value>
        [XmlIgnore]
        public bool CityTownVillageOfBirthNameSpecified
        {
            get { return CityTownVillageOfBirthName != null; }
            set { }
        }

        /// <summary>
        /// The country of birth of the individual.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString CountryOfBirthName;

        /// <summary>
        /// Gets or sets a value indicating whether the CountryOfBirthName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CountryOfBirthName element has been assigned a value.</value>
        [XmlIgnore]
        public bool CountryOfBirthNameSpecified
        {
            get { return CountryOfBirthName != null; }
            set { }
        }

        /// <summary>
        /// The maiden name of the mother.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString MothersMaidenName;

        /// <summary>
        /// Gets or sets a value indicating whether the MothersMaidenName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MothersMaidenName element has been assigned a value.</value>
        [XmlIgnore]
        public bool MothersMaidenNameSpecified
        {
            get { return MothersMaidenName != null; }
            set { }
        }

        /// <summary>
        /// The state or province of birth of the individual.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString StateProvinceOfBirthName;

        /// <summary>
        /// Gets or sets a value indicating whether the StateProvinceOfBirthName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the StateProvinceOfBirthName element has been assigned a value.</value>
        [XmlIgnore]
        public bool StateProvinceOfBirthNameSpecified
        {
            get { return StateProvinceOfBirthName != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 5)]
        public BIRTH_INFORMATION_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
