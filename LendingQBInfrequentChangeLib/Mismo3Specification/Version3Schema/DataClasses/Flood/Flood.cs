namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class FLOOD
    {
        /// <summary>
        /// Gets a value indicating whether the container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.FloodRequestSpecified
                    || this.FloodResponseSpecified;
            }
        }

        /// <summary>
        /// A request for flood data.
        /// </summary>
        [XmlElement("FLOOD_REQUEST", Order = 0)]
        public FLOOD_REQUEST FloodRequest;

        /// <summary>
        /// Gets or sets a value indicating whether the FloodRequest element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FloodRequest element has been assigned a value.</value>
        [XmlIgnore]
        public bool FloodRequestSpecified
        {
            get { return this.FloodRequest != null && this.FloodRequest.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A response containing flood data.
        /// </summary>
        [XmlElement("FLOOD_RESPONSE", Order = 1)]
        public FLOOD_RESPONSE FloodResponse;

        /// <summary>
        /// Gets or sets a value indicating whether the FloodResponse element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FloodResponse element has been assigned a value.</value>
        [XmlIgnore]
        public bool FloodResponseSpecified
        {
            get { return this.FloodResponse != null && this.FloodResponse.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public FLOOD_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
