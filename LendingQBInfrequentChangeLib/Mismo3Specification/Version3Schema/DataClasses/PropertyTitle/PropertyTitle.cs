namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PROPERTY_TITLE
    {
        /// <summary>
        /// Gets a value indicating whether the PROPERTY_TITLE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ClearAndMarketablePropertyTitleIndicatorSpecified
                    || this.ExtensionSpecified
                    || this.PropertyTitleTransferInspectionRequiredDescriptionSpecified
                    || this.PropertyTitleTransferInspectionRequiredIndicatorSpecified
                    || this.SellerPropertyAcquisitionDateSpecified
                    || this.TitleHolderCountSpecified
                    || this.TitlePreliminaryReportDateSpecified
                    || this.TitleProcessInsuranceTypeSpecified
                    || this.TitleProcessTypeOtherDescriptionSpecified
                    || this.TitleProcessTypeSpecified
                    || this.TitleReportItemsDescriptionSpecified
                    || this.TitleReportRequiredEndorsementsDescriptionSpecified
                    || this.TitleTransferredPastSixMonthsIndicatorSpecified;
            }
        }

        /// <summary>
        /// When true, indicates that the property has a title that is clear and marketable.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIndicator ClearAndMarketablePropertyTitleIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ClearAndMarketablePropertyTitleIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ClearAndMarketablePropertyTitleIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ClearAndMarketablePropertyTitleIndicatorSpecified
        {
            get { return ClearAndMarketablePropertyTitleIndicator != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to comment on the title transfer inspections of the property.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString PropertyTitleTransferInspectionRequiredDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyTitleTransferInspectionRequiredDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyTitleTransferInspectionRequiredDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyTitleTransferInspectionRequiredDescriptionSpecified
        {
            get { return PropertyTitleTransferInspectionRequiredDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates that an inspection of the property is required before the title can be transferred.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOIndicator PropertyTitleTransferInspectionRequiredIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyTitleTransferInspectionRequiredIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyTitleTransferInspectionRequiredIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyTitleTransferInspectionRequiredIndicatorSpecified
        {
            get { return PropertyTitleTransferInspectionRequiredIndicator != null; }
            set { }
        }

        /// <summary>
        /// Date seller acquired  the property as documented on the Title Report.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMODate SellerPropertyAcquisitionDate;

        /// <summary>
        /// Gets or sets a value indicating whether the SellerPropertyAcquisitionDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SellerPropertyAcquisitionDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool SellerPropertyAcquisitionDateSpecified
        {
            get { return SellerPropertyAcquisitionDate != null; }
            set { }
        }

        /// <summary>
        /// The total number of property sellers (excluding Company Owned Properties) from the Title Report.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOCount TitleHolderCount;

        /// <summary>
        /// Gets or sets a value indicating whether the TitleHolderCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TitleHolderCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TitleHolderCountSpecified
        {
            get { return TitleHolderCount != null; }
            set { }
        }

        /// <summary>
        /// The date on the preliminary title report.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMODate TitlePreliminaryReportDate;

        /// <summary>
        /// Gets or sets a value indicating whether the TitlePreliminaryReportDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TitlePreliminaryReportDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool TitlePreliminaryReportDateSpecified
        {
            get { return TitlePreliminaryReportDate != null; }
            set { }
        }

        /// <summary>
        /// Identifies the level of insurance being requested or delivered for the Title Process Type.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOEnum<TitleProcessInsuranceBase> TitleProcessInsuranceType;

        /// <summary>
        /// Gets or sets a value indicating whether the TitleProcessInsuranceType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TitleProcessInsuranceType element has been assigned a value.</value>
        [XmlIgnore]
        public bool TitleProcessInsuranceTypeSpecified
        {
            get { return this.TitleProcessInsuranceType != null && this.TitleProcessInsuranceType.enumValue != TitleProcessInsuranceBase.Blank; }
            set { }
        }

        /// <summary>
        /// Specifies the means used to determine that there are no legal claims against the subject property that have a higher priority than the loan.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOEnum<TitleProcessBase> TitleProcessType;

        /// <summary>
        /// Gets or sets a value indicating whether the TitleProcessType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TitleProcessType element has been assigned a value.</value>
        [XmlIgnore]
        public bool TitleProcessTypeSpecified
        {
            get { return this.TitleProcessType != null && this.TitleProcessType.enumValue != TitleProcessBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture additional information when Other is selected for Title Process Type.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOString TitleProcessTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the TitleProcessTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TitleProcessTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool TitleProcessTypeOtherDescriptionSpecified
        {
            get { return TitleProcessTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to communicate remarks pertaining to items contained within the Title Report.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOString TitleReportItemsDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the TitleReportItemsDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TitleReportItemsDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool TitleReportItemsDescriptionSpecified
        {
            get { return TitleReportItemsDescription != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe any required endorsements noted in the Title Report.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOString TitleReportRequiredEndorsementsDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the TitleReportRequiredEndorsementsDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TitleReportRequiredEndorsementsDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool TitleReportRequiredEndorsementsDescriptionSpecified
        {
            get { return TitleReportRequiredEndorsementsDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the property has been transferred in the past 6 months.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOIndicator TitleTransferredPastSixMonthsIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the TitleTransferredPastSixMonthsIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TitleTransferredPastSixMonthsIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool TitleTransferredPastSixMonthsIndicatorSpecified
        {
            get { return TitleTransferredPastSixMonthsIndicator != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 12)]
        public PROPERTY_TITLE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
