namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class BOUNDARIES
    {
        /// <summary>
        /// Gets a value indicating whether the BOUNDARIES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BoundarySpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of boundaries.
        /// </summary>
        [XmlElement("BOUNDARY", Order = 0)]
		public List<BOUNDARY> Boundary = new List<BOUNDARY>();

        /// <summary>
        /// Gets or sets a value indicating whether the Boundary element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Boundary element has been assigned a value.</value>
        [XmlIgnore]
        public bool BoundarySpecified
        {
            get { return this.Boundary != null && this.Boundary.Count(b => b != null && b.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public BOUNDARIES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
