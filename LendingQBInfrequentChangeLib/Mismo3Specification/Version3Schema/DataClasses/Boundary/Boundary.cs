namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class BOUNDARY
    {
        /// <summary>
        /// Gets a value indicating whether the BOUNDARY container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BoundaryDirectionTypeOtherDescriptionSpecified
                    || this.BoundaryDirectionTypeSpecified
                    || this.BoundaryGeospatialCoordinatesDescriptionSpecified
                    || this.BoundaryTypeOtherDescriptionSpecified
                    || this.BoundaryTypeSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// The boundary direction type indicated for a specific boundary along with a boundary type.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<BoundaryDirectionBase> BoundaryDirectionType;

        /// <summary>
        /// Gets or sets a value indicating whether the BoundaryDirectionType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BoundaryDirectionType element has been assigned a value.</value>
        [XmlIgnore]
        public bool BoundaryDirectionTypeSpecified
        {
            get { return this.BoundaryDirectionType != null && this.BoundaryDirectionType.enumValue != BoundaryDirectionBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the boundary direction if Other is selected as the Boundary Direction Type.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString BoundaryDirectionTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the BoundaryDirectionTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BoundaryDirectionTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool BoundaryDirectionTypeOtherDescriptionSpecified
        {
            get { return BoundaryDirectionTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Polygon of X-Y coordinates that represent a shape of an area and is recognizable to geospatial standards such as KML as used by commercial earth mapping services such as BING or Google Earth.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString BoundaryGeospatialCoordinatesDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the BoundaryGeospatialCoordinatesDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BoundaryGeospatialCoordinatesDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool BoundaryGeospatialCoordinatesDescriptionSpecified
        {
            get { return BoundaryGeospatialCoordinatesDescription != null; }
            set { }
        }

        /// <summary>
        /// The boundary type indicated for a specific boundary along with a boundary direction.  
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOEnum<BoundaryBase> BoundaryType;

        /// <summary>
        /// Gets or sets a value indicating whether the BoundaryType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BoundaryType element has been assigned a value.</value>
        [XmlIgnore]
        public bool BoundaryTypeSpecified
        {
            get { return this.BoundaryType != null && this.BoundaryType.enumValue != BoundaryBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the type of boundary if Other is selected as the Boundary Type.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString BoundaryTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the BoundaryTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BoundaryTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool BoundaryTypeOtherDescriptionSpecified
        {
            get { return BoundaryTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 5)]
        public BOUNDARY_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
