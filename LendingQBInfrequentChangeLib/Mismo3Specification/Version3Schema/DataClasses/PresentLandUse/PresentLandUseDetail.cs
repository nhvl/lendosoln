namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PRESENT_LAND_USE_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the PRESENT_LAND_USE_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.NeighborhoodLandUseChangeStatusTypeSpecified
                    || this.NeighborhoodLandUseCurrentDescriptionSpecified
                    || this.NeighborhoodLandUseFutureDescriptionSpecified;
            }
        }

        /// <summary>
        /// Specifies whether the land use in the neighborhood is not likely, likely or in the process of changing.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<NeighborhoodLandUseChangeStatusBase> NeighborhoodLandUseChangeStatusType;

        /// <summary>
        /// Gets or sets a value indicating whether the NeighborhoodLandUseChangeStatusType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NeighborhoodLandUseChangeStatusType element has been assigned a value.</value>
        [XmlIgnore]
        public bool NeighborhoodLandUseChangeStatusTypeSpecified
        {
            get { return this.NeighborhoodLandUseChangeStatusType != null && this.NeighborhoodLandUseChangeStatusType.enumValue != NeighborhoodLandUseChangeStatusBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the existing land use in the neighborhood.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString NeighborhoodLandUseCurrentDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the NeighborhoodLandUseCurrentDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NeighborhoodLandUseCurrentDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool NeighborhoodLandUseCurrentDescriptionSpecified
        {
            get { return NeighborhoodLandUseCurrentDescription != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe what the future land use will be in the neighborhood.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString NeighborhoodLandUseFutureDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the NeighborhoodLandUseFutureDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NeighborhoodLandUseFutureDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool NeighborhoodLandUseFutureDescriptionSpecified
        {
            get { return NeighborhoodLandUseFutureDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public PRESENT_LAND_USE_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
