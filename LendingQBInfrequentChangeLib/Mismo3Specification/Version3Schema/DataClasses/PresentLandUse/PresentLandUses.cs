namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PRESENT_LAND_USES
    {
        /// <summary>
        /// Gets a value indicating whether the PRESENT_LAND_USES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.PresentLandUseDetailSpecified
                    || this.PresentLandUseSpecified;
            }
        }

        /// <summary>
        /// A collection of present land uses.
        /// </summary>
        [XmlElement("PRESENT_LAND_USE", Order = 0)]
		public List<PRESENT_LAND_USE> PresentLandUse = new List<PRESENT_LAND_USE>();

        /// <summary>
        /// Gets or sets a value indicating whether the PresentLandUse element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PresentLandUse element has been assigned a value.</value>
        [XmlIgnore]
        public bool PresentLandUseSpecified
        {
            get { return this.PresentLandUse != null && this.PresentLandUse.Count(p => p != null && p.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// Details on the land use of a neighborhood.
        /// </summary>
        [XmlElement("PRESENT_LAND_USE_DETAIL", Order = 1)]
        public PRESENT_LAND_USE_DETAIL PresentLandUseDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the PresentLandUseDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PresentLandUseDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool PresentLandUseDetailSpecified
        {
            get { return this.PresentLandUseDetail != null && this.PresentLandUseDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public PRESENT_LAND_USES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
