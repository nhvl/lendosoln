namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class PRESENT_LAND_USE
    {
        /// <summary>
        /// Gets a value indicating whether the PRESENT_LAND_USE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.NeighborhoodLandUseTypeOtherDescriptionSpecified
                    || this.NeighborhoodLandUseTypeSpecified
                    || this.NeighborhoodPresentLandUsePercentSpecified;
            }
        }

        /// <summary>
        /// Specifies the land use category for a neighborhood.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<NeighborhoodLandUseBase> NeighborhoodLandUseType;

        /// <summary>
        /// Gets or sets a value indicating whether the NeighborhoodLandUseType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NeighborhoodLandUseType element has been assigned a value.</value>
        [XmlIgnore]
        public bool NeighborhoodLandUseTypeSpecified
        {
            get { return this.NeighborhoodLandUseType != null && this.NeighborhoodLandUseType.enumValue != NeighborhoodLandUseBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the land use if other is selected as the Neighborhood Land Use Type.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString NeighborhoodLandUseTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the NeighborhoodLandUseTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NeighborhoodLandUseTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool NeighborhoodLandUseTypeOtherDescriptionSpecified
        {
            get { return NeighborhoodLandUseTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the percentage of land used for the category specified by the Neighborhood Land Use Type.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOPercent NeighborhoodPresentLandUsePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the NeighborhoodPresentLandUsePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NeighborhoodPresentLandUsePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool NeighborhoodPresentLandUsePercentSpecified
        {
            get { return NeighborhoodPresentLandUsePercent != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public PRESENT_LAND_USE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
