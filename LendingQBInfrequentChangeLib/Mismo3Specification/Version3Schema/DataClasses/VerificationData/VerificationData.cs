namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class VERIFICATION_DATA
    {
        /// <summary>
        /// Gets a value indicating whether the VERIFICATION_DATA container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.VerificationDataInvestorSpecified
                    || this.VerificationDataLoanSpecified
                    || this.VerificationDataPayeeSpecified
                    || this.VerificationDataPoolSpecified;
            }
        }

        /// <summary>
        /// The investor for verification data.
        /// </summary>
        [XmlElement("VERIFICATION_DATA_INVESTOR", Order = 0)]
        public VERIFICATION_DATA_INVESTOR VerificationDataInvestor;

        /// <summary>
        /// Gets or sets a value indicating whether the VerificationDataInvestor element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the VerificationDataInvestor element has been assigned a value.</value>
        [XmlIgnore]
        public bool VerificationDataInvestorSpecified
        {
            get { return this.VerificationDataInvestor != null && this.VerificationDataInvestor.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The loan needing verification data.
        /// </summary>
        [XmlElement("VERIFICATION_DATA_LOAN", Order = 1)]
        public VERIFICATION_DATA_LOAN VerificationDataLoan;

        /// <summary>
        /// Gets or sets a value indicating whether the VerificationDataLoan element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the VerificationDataLoan element has been assigned a value.</value>
        [XmlIgnore]
        public bool VerificationDataLoanSpecified
        {
            get { return this.VerificationDataLoan != null && this.VerificationDataLoan.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The payee for verification data.
        /// </summary>
        [XmlElement("VERIFICATION_DATA_PAYEE", Order = 2)]
        public VERIFICATION_DATA_PAYEE VerificationDataPayee;

        /// <summary>
        /// Gets or sets a value indicating whether the VerificationDataPayee element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the VerificationDataPayee element has been assigned a value.</value>
        [XmlIgnore]
        public bool VerificationDataPayeeSpecified
        {
            get { return this.VerificationDataPayee != null && this.VerificationDataPayee.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Data pool for verification.
        /// </summary>
        [XmlElement("VERIFICATION_DATA_POOL", Order = 3)]
        public VERIFICATION_DATA_POOL VerificationDataPool;

        /// <summary>
        /// Gets or sets a value indicating whether the VerificationDataPool element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the VerificationDataPool element has been assigned a value.</value>
        [XmlIgnore]
        public bool VerificationDataPoolSpecified
        {
            get { return this.VerificationDataPool != null && this.VerificationDataPool.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public VERIFICATION_DATA_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
