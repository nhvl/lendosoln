namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class OPTIONAL_PRODUCT
    {
        /// <summary>
        /// Gets a value indicating whether the OPTIONAL_PRODUCT container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.OptionalProductCancellationReasonTypeOtherDescriptionSpecified
                    || this.OptionalProductCancellationReasonTypeSpecified
                    || this.OptionalProductChangeTypeOtherDescriptionSpecified
                    || this.OptionalProductChangeTypeSpecified
                    || this.OptionalProductEffectiveDateSpecified
                    || this.OptionalProductExpirationDateSpecified
                    || this.OptionalProductPayeeIdentifierSpecified
                    || this.OptionalProductPaymentAmountSpecified
                    || this.OptionalProductPendingChangeEffectiveDateSpecified
                    || this.OptionalProductPendingPaymentAmountSpecified
                    || this.OptionalProductPlanTypeAvailableFromLenderIndicatorSpecified
                    || this.OptionalProductPlanTypeOtherDescriptionSpecified
                    || this.OptionalProductPlanTypeRequiredIndicatorSpecified
                    || this.OptionalProductPlanTypeSpecified
                    || this.OptionalProductPremiumAmountSpecified
                    || this.OptionalProductPremiumTermMonthsCountSpecified
                    || this.OptionalProductProviderAccountIdentifierSpecified
                    || this.OptionalProductProvidersPlanIdentifierSpecified
                    || this.OptionalProductRemittanceDueDateSpecified
                    || this.OptionalProductRemittancePerYearCountSpecified;
            }
        }

        /// <summary>
        ///  Indicates the reason for cancellation of the associated optional product.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<OptionalProductCancellationReasonBase> OptionalProductCancellationReasonType;

        /// <summary>
        /// Gets or sets a value indicating whether the OptionalProductCancellationReasonType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OptionalProductCancellationReasonType element has been assigned a value.</value>
        [XmlIgnore]
        public bool OptionalProductCancellationReasonTypeSpecified
        {
            get { return this.OptionalProductCancellationReasonType != null && this.OptionalProductCancellationReasonType.enumValue != OptionalProductCancellationReasonBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Optional Product Cancellation Reason Type.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString OptionalProductCancellationReasonTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the OptionalProductCancellationReasonTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OptionalProductCancellationReasonTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool OptionalProductCancellationReasonTypeOtherDescriptionSpecified
        {
            get { return OptionalProductCancellationReasonTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates the type of change to the associated optional product.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<OptionalProductChangeBase> OptionalProductChangeType;

        /// <summary>
        /// Gets or sets a value indicating whether the OptionalProductChangeType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OptionalProductChangeType element has been assigned a value.</value>
        [XmlIgnore]
        public bool OptionalProductChangeTypeSpecified
        {
            get { return this.OptionalProductChangeType != null && this.OptionalProductChangeType.enumValue != OptionalProductChangeBase.Blank; }
            set { }
        }

        /// <summary>
        ///  A free-form text field used to collect additional information when other is selected for Optional Product Change Type.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString OptionalProductChangeTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the OptionalProductChangeTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OptionalProductChangeTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool OptionalProductChangeTypeOtherDescriptionSpecified
        {
            get { return OptionalProductChangeTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Effective date of optional product.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMODate OptionalProductEffectiveDate;

        /// <summary>
        /// Gets or sets a value indicating whether the OptionalProductEffectiveDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OptionalProductEffectiveDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool OptionalProductEffectiveDateSpecified
        {
            get { return OptionalProductEffectiveDate != null; }
            set { }
        }

        /// <summary>
        /// The scheduled termination date, if any, of an optional product plan or agreement.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMODate OptionalProductExpirationDate;

        /// <summary>
        /// Gets or sets a value indicating whether the OptionalProductExpirationDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OptionalProductExpirationDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool OptionalProductExpirationDateSpecified
        {
            get { return OptionalProductExpirationDate != null; }
            set { }
        }

        /// <summary>
        /// The payee code for the company that sells the optional product. The Optional Product Premiums will be paid to this payee.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOIdentifier OptionalProductPayeeIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the OptionalProductPayeeIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OptionalProductPayeeIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool OptionalProductPayeeIdentifierSpecified
        {
            get { return OptionalProductPayeeIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The amount currently collected with the mortgagors periodic mortgage payment and applied as an installment payment for the optional product.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOAmount OptionalProductPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the OptionalProductPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OptionalProductPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool OptionalProductPaymentAmountSpecified
        {
            get { return OptionalProductPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// The due date of the pending add, change modification, or delete cancellation to the optional product payment plan.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMODate OptionalProductPendingChangeEffectiveDate;

        /// <summary>
        /// Gets or sets a value indicating whether the OptionalProductPendingChangeEffectiveDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OptionalProductPendingChangeEffectiveDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool OptionalProductPendingChangeEffectiveDateSpecified
        {
            get { return OptionalProductPendingChangeEffectiveDate != null; }
            set { }
        }

        /// <summary>
        /// The pending amount for the optional product to be collected with the mortgagors periodic mortgage payment.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOAmount OptionalProductPendingPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the OptionalProductPendingPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OptionalProductPendingPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool OptionalProductPendingPaymentAmountSpecified
        {
            get { return OptionalProductPendingPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// The type of product purchased by the borrower from a third party, where the servicer collects funds from the borrower, normally included with the periodic mortgage payment, and remits funds to the third party.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOEnum<OptionalProductPlanBase> OptionalProductPlanType;

        /// <summary>
        /// Gets or sets a value indicating whether the OptionalProductPlanType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OptionalProductPlanType element has been assigned a value.</value>
        [XmlIgnore]
        public bool OptionalProductPlanTypeSpecified
        {
            get { return this.OptionalProductPlanType != null && this.OptionalProductPlanType.enumValue != OptionalProductPlanBase.Blank; }
            set { }
        }

        /// <summary>
        /// When true, indicates the borrower(s) may purchase the optional insurance product directly from the lender.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOIndicator OptionalProductPlanTypeAvailableFromLenderIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the OptionalProductPlanTypeAvailableFromLenderIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OptionalProductPlanTypeAvailableFromLenderIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool OptionalProductPlanTypeAvailableFromLenderIndicatorSpecified
        {
            get { return OptionalProductPlanTypeAvailableFromLenderIndicator != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field to describe an optional product that is not one of the enumerated type values.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOString OptionalProductPlanTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the OptionalProductPlanTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OptionalProductPlanTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool OptionalProductPlanTypeOtherDescriptionSpecified
        {
            get { return OptionalProductPlanTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that the Optional Product Plan Type item is required for the loan transaction.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOIndicator OptionalProductPlanTypeRequiredIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the OptionalProductPlanTypeRequiredIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OptionalProductPlanTypeRequiredIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool OptionalProductPlanTypeRequiredIndicatorSpecified
        {
            get { return OptionalProductPlanTypeRequiredIndicator != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of the premium for the individual Optional Product Plan Type. A premium is a one-time cost of the service, which differs from the Optional Product Payment Amount.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOAmount OptionalProductPremiumAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the OptionalProductPremiumAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OptionalProductPremiumAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool OptionalProductPremiumAmountSpecified
        {
            get { return OptionalProductPremiumAmount != null; }
            set { }
        }

        /// <summary>
        /// The number of months covered by the Optional Product Premium Amount for the individual Optional Product Plan Type.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOCount OptionalProductPremiumTermMonthsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the OptionalProductPremiumTermMonthsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OptionalProductPremiumTermMonthsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool OptionalProductPremiumTermMonthsCountSpecified
        {
            get { return OptionalProductPremiumTermMonthsCount != null; }
            set { }
        }

        /// <summary>
        /// An account identifier used by the product provider to identify this account in its own systems. For an optional insurance plan this would be the insurance plan certificate numbers assigned by the insurance company.
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMOIdentifier OptionalProductProviderAccountIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the OptionalProductProviderAccountIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OptionalProductProviderAccountIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool OptionalProductProviderAccountIdentifierSpecified
        {
            get { return OptionalProductProviderAccountIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A code provided by the third party that identifies the optional product, and that can be used by the servicer and the third party for reconciling activity related to a specific product.
        /// </summary>
        [XmlElement(Order = 17)]
        public MISMOIdentifier OptionalProductProvidersPlanIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the OptionalProductProvidersPlanIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OptionalProductProvidersPlanIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool OptionalProductProvidersPlanIdentifierSpecified
        {
            get { return OptionalProductProvidersPlanIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Due date of next remittance payment due to the provider (may be different from mortgage due date).
        /// </summary>
        [XmlElement(Order = 18)]
        public MISMODate OptionalProductRemittanceDueDate;

        /// <summary>
        /// Gets or sets a value indicating whether the OptionalProductRemittanceDueDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OptionalProductRemittanceDueDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool OptionalProductRemittanceDueDateSpecified
        {
            get { return OptionalProductRemittanceDueDate != null; }
            set { }
        }

        /// <summary>
        /// How often, described as number of times per year, that funds are remitted to the third-party.
        /// </summary>
        [XmlElement(Order = 19)]
        public MISMOCount OptionalProductRemittancePerYearCount;

        /// <summary>
        /// Gets or sets a value indicating whether the OptionalProductRemittancePerYearCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OptionalProductRemittancePerYearCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool OptionalProductRemittancePerYearCountSpecified
        {
            get { return OptionalProductRemittancePerYearCount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 20)]
        public OPTIONAL_PRODUCT_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
