namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class OPTIONAL_PRODUCTS
    {
        /// <summary>
        /// Gets a value indicating whether the OPTIONAL_PRODUCTS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.OptionalProductSpecified
                    || this.OptionalProductSummarySpecified;
            }
        }

        /// <summary>
        /// A collection of optional products.
        /// </summary>
        [XmlElement("OPTIONAL_PRODUCT", Order = 0)]
        public List<OPTIONAL_PRODUCT> OptionalProduct = new List<OPTIONAL_PRODUCT>();

        /// <summary>
        /// Gets or sets a value indicating whether the OptionalProduct element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OptionalProduct element has been assigned a value.</value>
        [XmlIgnore]
        public bool OptionalProductSpecified
        {
            get { return this.OptionalProduct != null && this.OptionalProduct.Count(o => o != null && o.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// Summary of the optional products.
        /// </summary>
        [XmlElement("OPTIONAL_PRODUCT_SUMMARY", Order = 1)]
        public OPTIONAL_PRODUCT_SUMMARY OptionalProductSummary;

        /// <summary>
        /// Gets or sets a value indicating whether the OptionalProductSummary element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OptionalProductSummary element has been assigned a value.</value>
        [XmlIgnore]
        public bool OptionalProductSummarySpecified
        {
            get { return this.OptionalProductSummary != null && this.OptionalProductSummary.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public OPTIONAL_PRODUCTS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
