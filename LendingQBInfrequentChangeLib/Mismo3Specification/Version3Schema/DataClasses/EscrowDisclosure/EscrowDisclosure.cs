namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class ESCROW_DISCLOSURE
    {
        /// <summary>
        /// Gets a value indicating whether the ESCROW_DISCLOSURE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.EscrowAccountActivityCurrentBalanceAmountSpecified
                    || this.EscrowAccountActivityDisbursementMonthSpecified
                    || this.EscrowAccountActivityDisbursementYearSpecified
                    || this.EscrowAccountActivityPaymentDescriptionTypeOtherDescriptionSpecified
                    || this.EscrowAccountActivityPaymentDescriptionTypeSpecified
                    || this.EscrowAccountActivityPaymentFromEscrowAccountAmountSpecified
                    || this.EscrowAccountActivityPaymentToEscrowAccountAmountSpecified
                    || this.EscrowItemTypeOtherDescriptionSpecified
                    || this.EscrowItemTypeSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// The current balance in the escrow account for the activity period, as projected during the closing documents preparation process.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount EscrowAccountActivityCurrentBalanceAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowAccountActivityCurrentBalanceAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowAccountActivityCurrentBalanceAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowAccountActivityCurrentBalanceAmountSpecified
        {
            get { return EscrowAccountActivityCurrentBalanceAmount != null; }
            set { }
        }

        /// <summary>
        /// The disbursement month of the escrow account for the activity period.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOMonth EscrowAccountActivityDisbursementMonth;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowAccountActivityDisbursementMonth element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowAccountActivityDisbursementMonth element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowAccountActivityDisbursementMonthSpecified
        {
            get { return EscrowAccountActivityDisbursementMonth != null; }
            set { }
        }

        /// <summary>
        /// The disbursement year of the escrow account for the activity period.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOYear EscrowAccountActivityDisbursementYear;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowAccountActivityDisbursementYear element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowAccountActivityDisbursementYear element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowAccountActivityDisbursementYearSpecified
        {
            get { return EscrowAccountActivityDisbursementYear != null; }
            set { }
        }

        /// <summary>
        /// A description of the escrow disbursement type included in a payment for the activity period.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOEnum<EscrowAccountActivityPaymentDescriptionBase> EscrowAccountActivityPaymentDescriptionType;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowAccountActivityPaymentDescriptionType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowAccountActivityPaymentDescriptionType element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowAccountActivityPaymentDescriptionTypeSpecified
        {
            get { return this.EscrowAccountActivityPaymentDescriptionType != null && this.EscrowAccountActivityPaymentDescriptionType.enumValue != EscrowAccountActivityPaymentDescriptionBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Escrow Account Activity Payment Description Type. 
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString EscrowAccountActivityPaymentDescriptionTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowAccountActivityPaymentDescriptionTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowAccountActivityPaymentDescriptionTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowAccountActivityPaymentDescriptionTypeOtherDescriptionSpecified
        {
            get { return EscrowAccountActivityPaymentDescriptionTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of the disbursement for the activity period.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOAmount EscrowAccountActivityPaymentFromEscrowAccountAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowAccountActivityPaymentFromEscrowAccountAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowAccountActivityPaymentFromEscrowAccountAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowAccountActivityPaymentFromEscrowAccountAmountSpecified
        {
            get { return EscrowAccountActivityPaymentFromEscrowAccountAmount != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of the payment to the escrow account for the activity period.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOAmount EscrowAccountActivityPaymentToEscrowAccountAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowAccountActivityPaymentToEscrowAccountAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowAccountActivityPaymentToEscrowAccountAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowAccountActivityPaymentToEscrowAccountAmountSpecified
        {
            get { return EscrowAccountActivityPaymentToEscrowAccountAmount != null; }
            set { }
        }

        /// <summary>
        /// Specifies the type of Escrow Item.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOEnum<EscrowItemBase> EscrowItemType;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowItemType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowItemType element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowItemTypeSpecified
        {
            get { return this.EscrowItemType != null && this.EscrowItemType.enumValue != EscrowItemBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field to collect additional information when Other is selected for Escrow Item Type.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOString EscrowItemTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowItemTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowItemTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowItemTypeOtherDescriptionSpecified
        {
            get { return EscrowItemTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 9)]
        public ESCROW_DISCLOSURE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
