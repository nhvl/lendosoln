namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class ESCROW_DISCLOSURES
    {
        /// <summary>
        /// Gets a value indicating whether the ESCROW_DISCLOSURES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.EscrowDisclosureSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of escrow disclosures.
        /// </summary>
        [XmlElement("ESCROW_DISCLOSURE", Order = 0)]
        public List<ESCROW_DISCLOSURE> EscrowDisclosure = new List<ESCROW_DISCLOSURE>();

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowDisclosure element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowDisclosure element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowDisclosureSpecified
        {
            get { return this.EscrowDisclosure != null && this.EscrowDisclosure.Count(e => e != null && e.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public ESCROW_DISCLOSURES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
