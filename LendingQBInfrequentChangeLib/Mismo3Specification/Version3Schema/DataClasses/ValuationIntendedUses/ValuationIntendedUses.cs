namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class VALUATION_INTENDED_USES
    {
        /// <summary>
        /// Gets a value indicating whether the VALUATION_INTENDED_USES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ValuationIntendedUseSpecified;
            }
        }

        /// <summary>
        /// A collection of valuation intended uses.
        /// </summary>
        [XmlElement("VALUATION_INTENDED_USE", Order = 0)]
		public List<VALUATION_INTENDED_USE> ValuationIntendedUse = new List<VALUATION_INTENDED_USE>();

        /// <summary>
        /// Gets or sets a value indicating whether the ValuationIntendedUse element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ValuationIntendedUse element has been assigned a value.</value>
        [XmlIgnore]
        public bool ValuationIntendedUseSpecified
        {
            get { return this.ValuationIntendedUse != null && this.ValuationIntendedUse.Count(v => v != null && v.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public VALUATION_INTENDED_USES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
