namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class BANKRUPTCY_STATUSES
    {
        /// <summary>
        /// Gets a value indicating whether the BANKRUPTCY_STATUSES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BankruptcyStatusSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of bankruptcy statuses.
        /// </summary>
        [XmlElement("BANKRUPTCY_STATUS", Order = 0)]
		public List<BANKRUPTCY_STATUS> BankruptcyStatus = new List<BANKRUPTCY_STATUS>();

        /// <summary>
        /// Gets or sets a value indicating whether the BankruptcyStatus element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BankruptcyStatus element has been assigned a value.</value>
        [XmlIgnore]
        public bool BankruptcyStatusSpecified
        {
            get { return this.BankruptcyStatus != null && this.BankruptcyStatus.Count(b => b != null && b.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public BANKRUPTCY_STATUSES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
