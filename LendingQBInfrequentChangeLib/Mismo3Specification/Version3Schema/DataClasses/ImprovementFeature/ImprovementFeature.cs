namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class IMPROVEMENT_FEATURE
    {
        /// <summary>
        /// Gets a value indicating whether the IMPROVEMENT_FEATURE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AmenitiesSpecified
                    || this.BathEquipmentsSpecified
                    || this.ExtensionSpecified
                    || this.KitchenEquipmentsSpecified;
            }
        }

        /// <summary>
        /// A list of amenities.
        /// </summary>
        [XmlElement("AMENITIES", Order = 0)]
        public AMENITIES Amenities;

        /// <summary>
        /// Gets or sets a value indicating whether the Amenities element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Amenities element has been assigned a value.</value>
        [XmlIgnore]
        public bool AmenitiesSpecified
        {
            get { return this.Amenities != null && this.Amenities.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A list of bath equipment.
        /// </summary>
        [XmlElement("BATH_EQUIPMENTS", Order = 1)]
        public BATH_EQUIPMENTS BathEquipments;

        /// <summary>
        /// Gets or sets a value indicating whether the BathEquipment element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BathEquipment element has been assigned a value.</value>
        [XmlIgnore]
        public bool BathEquipmentsSpecified
        {
            get { return this.BathEquipments != null && this.BathEquipments.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A list of kitchen equipment.
        /// </summary>
        [XmlElement("KITCHEN_EQUIPMENTS", Order = 2)]
        public KITCHEN_EQUIPMENTS KitchenEquipments;

        /// <summary>
        /// Gets or sets a value indicating whether the KitchenEquipment element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the KitchenEquipment element has been assigned a value.</value>
        [XmlIgnore]
        public bool KitchenEquipmentsSpecified
        {
            get { return this.KitchenEquipments != null && this.KitchenEquipments.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public IMPROVEMENT_FEATURE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
