namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class TITLE_ENDORSEMENTS
    {
        /// <summary>
        /// Gets a value indicating whether the TITLE_ENDORSEMENTS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.TitleEndorsementSpecified;
            }
        }

        /// <summary>
        /// A collection of title endorsements.
        /// </summary>
        [XmlElement("TITLE_ENDORSEMENT", Order = 0)]
		public List<TITLE_ENDORSEMENT> TitleEndorsement = new List<TITLE_ENDORSEMENT>();

        /// <summary>
        /// Gets or sets a value indicating whether the TitleEndorsement element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TitleEndorsement element has been assigned a value.</value>
        [XmlIgnore]
        public bool TitleEndorsementSpecified
        {
            get { return this.TitleEndorsement != null && this.TitleEndorsement.Count(t => t != null && t.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public TITLE_ENDORSEMENTS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
