namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class TITLE_ENDORSEMENT
    {
        /// <summary>
        /// Gets a value indicating whether the TITLE_ENDORSEMENT container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.TitleEndorsementDescriptionSpecified
                    || this.TitleEndorsementFormIdentifierSpecified
                    || this.TitleEndorsementFormNameSpecified
                    || this.TitleEndorsementIncludedIndicatorSpecified
                    || this.TitleEndorsementSourceTypeOtherDescriptionSpecified
                    || this.TitleEndorsementSourceTypeSpecified
                    || this.TitleEndorsementStatuteDescriptionSpecified
                    || this.TitleEndorsementStatuteIncludedIndicatorSpecified;
            }
        }

        /// <summary>
        /// The detailed description of the Title Endorsement Document.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString TitleEndorsementDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the TitleEndorsementDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TitleEndorsementDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool TitleEndorsementDescriptionSpecified
        {
            get { return TitleEndorsementDescription != null; }
            set { }
        }

        /// <summary>
        /// The number that uniquely identifies the Title Endorsement Document.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOIdentifier TitleEndorsementFormIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the TitleEndorsementFormIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TitleEndorsementFormIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool TitleEndorsementFormIdentifierSpecified
        {
            get { return TitleEndorsementFormIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The detailed name of the Title Endorsement Document.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString TitleEndorsementFormName;

        /// <summary>
        /// Gets or sets a value indicating whether the TitleEndorsementFormName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TitleEndorsementFormName element has been assigned a value.</value>
        [XmlIgnore]
        public bool TitleEndorsementFormNameSpecified
        {
            get { return TitleEndorsementFormName != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the Title Endorsement is included in the Title product.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOIndicator TitleEndorsementIncludedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the TitleEndorsementIncludedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TitleEndorsementIncludedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool TitleEndorsementIncludedIndicatorSpecified
        {
            get { return TitleEndorsementIncludedIndicator != null; }
            set { }
        }

        /// <summary>
        /// The business source of the title endorsement document, this can be a state, association or other document publisher.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<TitleEndorsementSourceBase> TitleEndorsementSourceType;

        /// <summary>
        /// Gets or sets a value indicating whether the TitleEndorsementSourceType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TitleEndorsementSourceType element has been assigned a value.</value>
        [XmlIgnore]
        public bool TitleEndorsementSourceTypeSpecified
        {
            get { return this.TitleEndorsementSourceType != null && this.TitleEndorsementSourceType.enumValue != TitleEndorsementSourceBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the Title Endorsement Source Type if Other is selected as the Source Type.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString TitleEndorsementSourceTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the TitleEndorsementSourceTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TitleEndorsementSourceTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool TitleEndorsementSourceTypeOtherDescriptionSpecified
        {
            get { return TitleEndorsementSourceTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The detailed description of the statute that is referenced on the Title Endorsement Document.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOString TitleEndorsementStatuteDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the TitleEndorsementStatuteDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TitleEndorsementStatuteDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool TitleEndorsementStatuteDescriptionSpecified
        {
            get { return TitleEndorsementStatuteDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates if a statute is referenced on the Title Endorsement Document.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOIndicator TitleEndorsementStatuteIncludedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the TitleEndorsementStatuteIncludedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TitleEndorsementStatuteIncludedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool TitleEndorsementStatuteIncludedIndicatorSpecified
        {
            get { return TitleEndorsementStatuteIncludedIndicator != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 8)]
        public TITLE_ENDORSEMENT_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
