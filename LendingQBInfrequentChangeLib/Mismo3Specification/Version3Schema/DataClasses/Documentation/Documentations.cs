namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class DOCUMENTATIONS
    {
        /// <summary>
        /// Gets a value indicating whether the DOCUMENTATIONS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DocumentationSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of documentation.
        /// </summary>
        [XmlElement("DOCUMENTATION", Order = 0)]
		public List<DOCUMENTATION> Documentation = new List<DOCUMENTATION>();

        /// <summary>
        /// Gets or sets a value indicating whether the Documentation element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Documentation element has been assigned a value.</value>
        [XmlIgnore]
        public bool DocumentationSpecified
        {
            get { return this.Documentation != null && this.Documentation.Count(d => d != null && d.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public DOCUMENTATIONS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
