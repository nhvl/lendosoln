namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class ESTIMATED_PROPERTY_COST_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the ESTIMATED_PROPERTY_COST_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ProjectedPaymentEstimatedTaxesInsuranceAssessmentTotalAmountSpecified;
            }
        }

        /// <summary>
        /// The estimated total of property tax, property insurance, and property assessment payment amounts during the projected payment period.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount ProjectedPaymentEstimatedTaxesInsuranceAssessmentTotalAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectedPaymentEstimatedTaxesInsuranceAssessmentTotalAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectedPaymentEstimatedTaxesInsuranceAssessmentTotalAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectedPaymentEstimatedTaxesInsuranceAssessmentTotalAmountSpecified
        {
            get { return ProjectedPaymentEstimatedTaxesInsuranceAssessmentTotalAmount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public ESTIMATED_PROPERTY_COST_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
