namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class ESTIMATED_PROPERTY_COST
    {
        /// <summary>
        /// Gets a value indicating whether the ESTIMATED_PROPERTY_COST container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.EstimatedPropertyCostComponentsSpecified
                    || this.EstimatedPropertyCostDetailSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// Components of the estimated property cost.
        /// </summary>
        [XmlElement("ESTIMATED_PROPERTY_COST_COMPONENTS", Order = 0)]
        public ESTIMATED_PROPERTY_COST_COMPONENTS EstimatedPropertyCostComponents;

        /// <summary>
        /// Gets or sets a value indicating whether the EstimatedPropertyCostComponents element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EstimatedPropertyCostComponents element has been assigned a value.</value>
        [XmlIgnore]
        public bool EstimatedPropertyCostComponentsSpecified
        {
            get { return this.EstimatedPropertyCostComponents != null && this.EstimatedPropertyCostComponents.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details on the estimated property cost.
        /// </summary>
        [XmlElement("ESTIMATED_PROPERTY_COST_DETAIL", Order = 1)]
        public ESTIMATED_PROPERTY_COST_DETAIL EstimatedPropertyCostDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the EstimatedPropertyCostDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EstimatedPropertyCostDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool EstimatedPropertyCostDetailSpecified
        {
            get { return this.EstimatedPropertyCostDetail != null && this.EstimatedPropertyCostDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public ESTIMATED_PROPERTY_COST_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
