namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class LICENSES
    {
        /// <summary>
        /// Gets a value indicating whether the LICENSES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.LicenseSpecified;
            }
        }

        /// <summary>
        /// A collection of licenses.
        /// </summary>
        [XmlElement("LICENSE", Order = 0)]
		public List<LICENSE> License = new List<LICENSE>();

        /// <summary>
        /// Gets or sets a value indicating whether the License element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the License element has been assigned a value.</value>
        [XmlIgnore]
        public bool LicenseSpecified
        {
            get { return this.License != null && this.License.Count(l => l != null && l.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public LICENSES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
