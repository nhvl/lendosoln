namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class LICENSE_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the LICENSE_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.LicenseAuthorityLevelTypeOtherDescriptionSpecified
                    || this.LicenseAuthorityLevelTypeSpecified
                    || this.LicenseExemptIndicatorSpecified
                    || this.LicenseExpirationDateSpecified
                    || this.LicenseIdentifierSpecified
                    || this.LicenseIssueDateSpecified
                    || this.LicenseIssuingAuthorityNameSpecified
                    || this.LicenseIssuingAuthorityStateCodeSpecified
                    || this.LicenseIssuingAuthorityStateNameSpecified;
            }
        }

        /// <summary>
        /// Level of authority of the license issuer.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<LicenseAuthorityLevelBase> LicenseAuthorityLevelType;

        /// <summary>
        /// Gets or sets a value indicating whether the LicenseAuthorityLevelType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LicenseAuthorityLevelType element has been assigned a value.</value>
        [XmlIgnore]
        public bool LicenseAuthorityLevelTypeSpecified
        {
            get { return this.LicenseAuthorityLevelType != null && this.LicenseAuthorityLevelType.enumValue != LicenseAuthorityLevelBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for License Authority Level Type.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString LicenseAuthorityLevelTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the LicenseAuthorityLevelTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LicenseAuthorityLevelTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool LicenseAuthorityLevelTypeOtherDescriptionSpecified
        {
            get { return LicenseAuthorityLevelTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that the party is exempt from specific licensing requirement(s).
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOIndicator LicenseExemptIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the LicenseExemptIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LicenseExemptIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool LicenseExemptIndicatorSpecified
        {
            get { return LicenseExemptIndicator != null; }
            set { }
        }

        /// <summary>
        /// The date on which the license expires.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMODate LicenseExpirationDate;

        /// <summary>
        /// Gets or sets a value indicating whether the LicenseExpirationDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LicenseExpirationDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool LicenseExpirationDateSpecified
        {
            get { return LicenseExpirationDate != null; }
            set { }
        }

        /// <summary>
        /// The identifier of the license or certificate issued to the party. The attribute IdentifierOwnerURI identifies the issuer of the license or certificate.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOIdentifier LicenseIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the LicenseIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LicenseIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool LicenseIdentifierSpecified
        {
            get { return LicenseIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The date on which the license was issued.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMODate LicenseIssueDate;

        /// <summary>
        /// Gets or sets a value indicating whether the LicenseIssueDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LicenseIssueDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool LicenseIssueDateSpecified
        {
            get { return LicenseIssueDate != null; }
            set { }
        }

        /// <summary>
        /// The name of the issuing authority through which the party obtained the license.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOString LicenseIssuingAuthorityName;

        /// <summary>
        /// Gets or sets a value indicating whether the LicenseIssuingAuthorityName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LicenseIssuingAuthorityName element has been assigned a value.</value>
        [XmlIgnore]
        public bool LicenseIssuingAuthorityNameSpecified
        {
            get { return LicenseIssuingAuthorityName != null; }
            set { }
        }

        /// <summary>
        /// The two-character representation of the US state, US Territory, Canadian Province, Military APO FPO, or Territory in which the party is licensed or certified to operate.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOCode LicenseIssuingAuthorityStateCode;

        /// <summary>
        /// Gets or sets a value indicating whether the LicenseIssuingAuthorityStateCode element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LicenseIssuingAuthorityStateCode element has been assigned a value.</value>
        [XmlIgnore]
        public bool LicenseIssuingAuthorityStateCodeSpecified
        {
            get { return LicenseIssuingAuthorityStateCode != null; }
            set { }
        }

        /// <summary>
        /// The name of the state in which the party is licensed or certified to operate. 
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOString LicenseIssuingAuthorityStateName;

        /// <summary>
        /// Gets or sets a value indicating whether the LicenseIssuingAuthorityStateName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LicenseIssuingAuthorityStateName element has been assigned a value.</value>
        [XmlIgnore]
        public bool LicenseIssuingAuthorityStateNameSpecified
        {
            get { return LicenseIssuingAuthorityStateName != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 9)]
        public LICENSE_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
