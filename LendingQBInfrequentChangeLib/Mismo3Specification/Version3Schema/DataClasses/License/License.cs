namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class LICENSE
    {
        /// <summary>
        /// Gets a value indicating whether the LICENSE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                if (Mismo3Utilities.ExceedsThreshold(1,
                    new List<bool> { this.AppraiserLicenseSpecified, this.PropertyLicenseSpecified }))
                {
                    throw new System.Xml.Schema.XmlSchemaValidationException(
                        Mismo3Utilities.GetXSDChoiceExceptionMessage(
                        "LICENSE",
                        new List<string> { "APPRAISER_LICENSE", "PROPERTY_LICENSE" }));
                }

                return this.AppraiserLicenseSpecified
                    || this.ExtensionSpecified
                    || this.LicenseDetailSpecified
                    || this.PropertyLicenseSpecified;
            }
        }

        /// <summary>
        /// License of an appraiser.
        /// </summary>
        [XmlElement("APPRAISER_LICENSE", Order = 0)]
        public APPRAISER_LICENSE AppraiserLicense;

        /// <summary>
        /// Gets or sets a value indicating whether the AppraiserLicense element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AppraiserLicense element has been assigned a value.</value>
        [XmlIgnore]
        public bool AppraiserLicenseSpecified
        {
            get { return this.AppraiserLicense != null && this.AppraiserLicense.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A license of property.
        /// </summary>
        [XmlElement("PROPERTY_LICENSE", Order = 1)]
        public PROPERTY_LICENSE PropertyLicense;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyLicense element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyLicense element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyLicenseSpecified
        {
            get { return this.PropertyLicense != null && this.PropertyLicense.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details on a license.
        /// </summary>
        [XmlElement("LICENSE_DETAIL", Order = 2)]
        public LICENSE_DETAIL LicenseDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the LicenseDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LicenseDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool LicenseDetailSpecified
        {
            get { return this.LicenseDetail != null && this.LicenseDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public LICENSE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
