namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CREDIT_INQUIRY_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the CREDIT_INQUIRY_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditBusinessTypeSpecified
                    || this.CreditInquiryDateSpecified
                    || this.CreditInquiryResultTypeSpecified
                    || this.CreditLoanTypeOtherDescriptionSpecified
                    || this.CreditLoanTypeSpecified
                    || this.DetailCreditBusinessTypeSpecified
                    || this.DuplicateGroupIdentifierSpecified
                    || this.ExtensionSpecified
                    || this.PrimaryRecordIndicatorSpecified;
            }
        }

        /// <summary>
        /// The credit repository bureaus can identify the type of business of the liability holder or the entity requesting a credit report as listed in a credit inquiry record. This information is needed for an automated scoring application. This data is available for liability and inquiry records from Equifax, Experian and Trans Union.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<CreditBusinessBase> CreditBusinessType;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditBusinessType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditBusinessType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditBusinessTypeSpecified
        {
            get { return this.CreditBusinessType != null && this.CreditBusinessType.enumValue != CreditBusinessBase.Blank; }
            set { }
        }

        /// <summary>
        /// The date of the credit inquiry.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMODate CreditInquiryDate;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditInquiryDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditInquiryDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditInquiryDateSpecified
        {
            get { return CreditInquiryDate != null; }
            set { }
        }

        /// <summary>
        /// The result from a credit inquiry - did the lender offer credit, refuse credit, etc.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<CreditInquiryResultBase> CreditInquiryResultType;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditInquiryResultType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditInquiryResultType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditInquiryResultTypeSpecified
        {
            get { return this.CreditInquiryResultType != null && this.CreditInquiryResultType.enumValue != CreditInquiryResultBase.Blank; }
            set { }
        }

        /// <summary>
        /// This element contains an enumerated list of common loan types. If the loan type is Other, then the description is entered into the Credit Loan Type Other Description element.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOEnum<CreditLoanBase> CreditLoanType;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLoanType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLoanType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLoanTypeSpecified
        {
            get { return this.CreditLoanType != null && this.CreditLoanType.enumValue != CreditLoanBase.Blank; }
            set { }
        }

        /// <summary>
        /// If Credit Loan Type was set to Other, then this element will contain a description of the loan type.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString CreditLoanTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLoanTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLoanTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLoanTypeOtherDescriptionSpecified
        {
            get { return CreditLoanTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Provides a more detailed description of the Credit Business Type.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOEnum<DetailCreditBusinessBase> DetailCreditBusinessType;

        /// <summary>
        /// Gets or sets a value indicating whether the DetailCreditBusinessType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DetailCreditBusinessType element has been assigned a value.</value>
        [XmlIgnore]
        public bool DetailCreditBusinessTypeSpecified
        {
            get { return this.DetailCreditBusinessType != null && this.DetailCreditBusinessType.enumValue != DetailCreditBusinessBase.Blank; }
            set { }
        }

        /// <summary>
        /// Data records with the same Duplicate Group Identifier value have been identified as being duplicate records from different sources.  Within a set of records that have the same Duplicate Group Identifier, one record is marked with a Primary Record Indicator set to "True", indicating that this record has been identified as the record out of the group that should be used for evaluation of credit.  The remaining records in the group should have a Primary Record Indicator set to "False".
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOIdentifier DuplicateGroupIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the DuplicateGroupIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DuplicateGroupIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool DuplicateGroupIdentifierSpecified
        {
            get { return DuplicateGroupIdentifier != null; }
            set { }
        }

        /// <summary>
        /// If this indicator is missing or if this indicator is set to "True", the data element is the primary record to be used for output and evaluation.  Most mortgage credit report data is collected from multiple sources (i.e. Equifax, Experian and Trans Union), and combined into a single data set that does not contain duplicate data.  In most credit reports, only the primary, merged data is returned.  For these reports, the Primary Record Indicator can be left out completely or if it is provided is should be set to "True".   Some credit reports are provided that contain both the primary, merged data, plus the original records that were considered duplicates of each other that were used to derive the primary record.  These duplicates that contained the original, unmerged data will have a Primary Record Indicator value of "False".
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOIndicator PrimaryRecordIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PrimaryRecordIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrimaryRecordIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrimaryRecordIndicatorSpecified
        {
            get { return PrimaryRecordIndicator != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 8)]
        public CREDIT_INQUIRY_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
