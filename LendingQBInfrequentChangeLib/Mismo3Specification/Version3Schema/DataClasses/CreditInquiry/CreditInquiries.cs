namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CREDIT_INQUIRIES
    {
        /// <summary>
        /// Gets a value indicating whether the CREDIT_INQUIRIES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditInquirySpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of credit inquiries.
        /// </summary>
        [XmlElement("CREDIT_INQUIRY", Order = 0)]
		public List<CREDIT_INQUIRY> CreditInquiry = new List<CREDIT_INQUIRY>();

        /// <summary>
        /// Gets or sets a value indicating whether the CreditInquiry element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditInquiry element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditInquirySpecified
        {
            get { return this.CreditInquiry != null && this.CreditInquiry.Count(c => c != null && c.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public CREDIT_INQUIRIES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
