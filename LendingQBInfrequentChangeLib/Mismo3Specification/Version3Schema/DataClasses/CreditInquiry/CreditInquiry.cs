namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class CREDIT_INQUIRY
    {
        /// <summary>
        /// Gets a value indicating whether the CREDIT_INQUIRY container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AddressSpecified
                    || this.CreditCommentsSpecified
                    || this.CreditInquiryDetailSpecified
                    || this.CreditRepositoriesSpecified
                    || this.ExtensionSpecified
                    || this.NameSpecified
                    || this.VerificationSpecified;
            }
        }

        /// <summary>
        /// Address related to a credit inquiry.
        /// </summary>
        [XmlElement("ADDRESS", Order = 0)]
        public ADDRESS Address;

        /// <summary>
        /// Gets or sets a value indicating whether the Address element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Address element has been assigned a value.</value>
        [XmlIgnore]
        public bool AddressSpecified
        {
            get { return this.Address != null && this.Address.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Comments on a credit inquiry.
        /// </summary>
        [XmlElement("CREDIT_COMMENTS", Order = 1)]
        public CREDIT_COMMENTS CreditComments;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditComments element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditComments element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditCommentsSpecified
        {
            get { return this.CreditComments != null && this.CreditComments.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details on a credit inquiry.
        /// </summary>
        [XmlElement("CREDIT_INQUIRY_DETAIL", Order = 2)]
        public CREDIT_INQUIRY_DETAIL CreditInquiryDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditInquiryDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditInquiryDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditInquiryDetailSpecified
        {
            get { return this.CreditInquiryDetail != null && this.CreditInquiryDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Repository for a credit inquiry.
        /// </summary>
        [XmlElement("CREDIT_REPOSITORIES", Order = 3)]
        public CREDIT_REPOSITORIES CreditRepositories;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditRepositories element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditRepositories element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditRepositoriesSpecified
        {
            get { return this.CreditRepositories != null && this.CreditRepositories.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Name of a credit inquiry.
        /// </summary>
        [XmlElement("NAME", Order = 4)]
        public NAME Name;

        /// <summary>
        /// Gets or sets a value indicating whether the Name element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Name element has been assigned a value.</value>
        [XmlIgnore]
        public bool NameSpecified
        {
            get { return this.Name != null && this.Name.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Verification of a credit inquiry.
        /// </summary>
        [XmlElement("VERIFICATION", Order = 5)]
        public VERIFICATION Verification;

        /// <summary>
        /// Gets or sets a value indicating whether the Verification element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Verification element has been assigned a value.</value>
        [XmlIgnore]
        public bool VerificationSpecified
        {
            get { return this.Verification != null && this.Verification.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 6)]
        public CREDIT_INQUIRY_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
