namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class TITLE_RESPONSE_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the TITLE_RESPONSE_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.LoanScheduledClosingDateSpecified
                    || this.TitleAgentValidationDescriptionSpecified
                    || this.TitleAgentValidationTypeOtherDescriptionSpecified
                    || this.TitleAgentValidationTypeSpecified
                    || this.TitleClearanceGradeIdentifierSpecified
                    || this.TitleInsuranceAmountSpecified
                    || this.TitlePolicyEffectiveDateSpecified
                    || this.TitlePolicyIdentifierSpecified
                    || this.TitlePolicyWitnessClauseDescriptionSpecified
                    || this.TitleResponseCommentDescriptionSpecified;
            }
        }

        /// <summary>
        /// The scheduled date for when the loan will be closed with the borrower.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMODate LoanScheduledClosingDate;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanScheduledClosingDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanScheduledClosingDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanScheduledClosingDateSpecified
        {
            get { return LoanScheduledClosingDate != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to further qualify or describe the Title Agent Validation information for the transaction.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString TitleAgentValidationDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the TitleAgentValidationDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TitleAgentValidationDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool TitleAgentValidationDescriptionSpecified
        {
            get { return TitleAgentValidationDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies whether or not the agent is allowed to close the transaction on behalf of the title underwriter.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<TitleAgentValidationBase> TitleAgentValidationType;

        /// <summary>
        /// Gets or sets a value indicating whether the TitleAgentValidationType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TitleAgentValidationType element has been assigned a value.</value>
        [XmlIgnore]
        public bool TitleAgentValidationTypeSpecified
        {
            get { return this.TitleAgentValidationType != null && this.TitleAgentValidationType.enumValue != TitleAgentValidationBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the Title Agent Validation Type if Other is selected as the Title Agent Validation Type.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString TitleAgentValidationTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the TitleAgentValidationTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TitleAgentValidationTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool TitleAgentValidationTypeOtherDescriptionSpecified
        {
            get { return TitleAgentValidationTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the clearance of defect to title.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOIdentifier TitleClearanceGradeIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the TitleClearanceGradeIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TitleClearanceGradeIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool TitleClearanceGradeIdentifierSpecified
        {
            get { return TitleClearanceGradeIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The actual coverage amount of the Title Insurance Policy document.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOAmount TitleInsuranceAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the TitleInsuranceAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TitleInsuranceAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TitleInsuranceAmountSpecified
        {
            get { return TitleInsuranceAmount != null; }
            set { }
        }

        /// <summary>
        /// The date on which the title product becomes effective.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMODate TitlePolicyEffectiveDate;

        /// <summary>
        /// Gets or sets a value indicating whether the TitlePolicyEffectiveDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TitlePolicyEffectiveDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool TitlePolicyEffectiveDateSpecified
        {
            get { return TitlePolicyEffectiveDate != null; }
            set { }
        }

        /// <summary>
        /// The identifier assigned by a title company to a preliminary certificate or final Title Insurance Policy document.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOIdentifier TitlePolicyIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the TitlePolicyIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TitlePolicyIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool TitlePolicyIdentifierSpecified
        {
            get { return TitlePolicyIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to contain the witness clause of the Title Insurance Policy document.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOString TitlePolicyWitnessClauseDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the TitlePolicyWitnessClauseDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TitlePolicyWitnessClauseDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool TitlePolicyWitnessClauseDescriptionSpecified
        {
            get { return TitlePolicyWitnessClauseDescription != null; }
            set { }
        }

        /// <summary>
        /// Any comments added by either the title processor or the service provider to the Title Product.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOString TitleResponseCommentDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the TitleResponseCommentDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TitleResponseCommentDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool TitleResponseCommentDescriptionSpecified
        {
            get { return TitleResponseCommentDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 10)]
        public TITLE_RESPONSE_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
