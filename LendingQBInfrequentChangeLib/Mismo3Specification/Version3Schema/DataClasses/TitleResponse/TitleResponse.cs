namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class TITLE_RESPONSE
    {
        /// <summary>
        /// Gets a value indicating whether the TITLE_RESPONSE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExecutionsSpecified
                    || this.ExtensionSpecified
                    || this.LegalAndVestingsSpecified
                    || this.LoansSpecified
                    || this.PartiesSpecified
                    || this.PropertySpecified
                    || this.RecordingEndorsementInformationSpecified
                    || this.RemittancesSpecified
                    || this.TitleAdditionalExceptionsSpecified
                    || this.TitleAdditionalRequirementsSpecified
                    || this.TitleEndorsementsSpecified
                    || this.TitleResponseDetailSpecified;
            }
        }

        /// <summary>
        /// Executions associated with the title.
        /// </summary>
        [XmlElement("EXECUTIONS", Order = 0)]
        public EXECUTIONS Executions;

        /// <summary>
        /// Gets or sets a value indicating whether the Executions element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Executions element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExecutionsSpecified
        {
            get { return this.Executions != null && this.Executions.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Legal and vesting for the title request.
        /// </summary>
        [XmlElement("LEGAL_AND_VESTINGS", Order = 1)]
        public LEGAL_AND_VESTINGS LegalAndVestings;

        /// <summary>
        /// Gets or sets a value indicating whether the LegalAndVesting element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LegalAndVesting element has been assigned a value.</value>
        [XmlIgnore]
        public bool LegalAndVestingsSpecified
        {
            get { return this.LegalAndVestings != null && this.LegalAndVestings.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The loans associated with the title request.
        /// </summary>
        [XmlElement("LOANS", Order = 2)]
        public LOANS Loans;

        /// <summary>
        /// Gets or sets a value indicating whether the Loans element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Loans element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoansSpecified
        {
            get { return this.Loans != null && this.Loans.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Parties involved in the title request.
        /// </summary>
        [XmlElement("PARTIES", Order = 3)]
        public PARTIES Parties;

        /// <summary>
        /// Gets or sets a value indicating whether the Parties element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Parties element has been assigned a value.</value>
        [XmlIgnore]
        public bool PartiesSpecified
        {
            get { return this.Parties != null && this.Parties.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The property for which title is required.
        /// </summary>
        [XmlElement("PROPERTY", Order = 4)]
        public PROPERTY Property;

        /// <summary>
        /// Gets or sets a value indicating whether the Property element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Property element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertySpecified
        {
            get { return this.Property != null && this.Property.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Information on the recording endorsement.
        /// </summary>
        [XmlElement("RECORDING_ENDORSEMENT_INFORMATION", Order = 5)]
        public RECORDING_ENDORSEMENT_INFORMATION RecordingEndorsementInformation;

        /// <summary>
        /// Gets or sets a value indicating whether the RecordingEndorsementInformation element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RecordingEndorsementInformation element has been assigned a value.</value>
        [XmlIgnore]
        public bool RecordingEndorsementInformationSpecified
        {
            get { return this.RecordingEndorsementInformation != null && this.RecordingEndorsementInformation.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Remittances for the title.
        /// </summary>
        [XmlElement("REMITTANCES", Order = 6)]
        public REMITTANCES Remittances;

        /// <summary>
        /// Gets or sets a value indicating whether the Remittances element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Remittances element has been assigned a value.</value>
        [XmlIgnore]
        public bool RemittancesSpecified
        {
            get { return this.Remittances != null && this.Remittances.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Additional exceptions for the title.
        /// </summary>
        [XmlElement("TITLE_ADDITIONAL_EXCEPTIONS", Order = 7)]
        public TITLE_ADDITIONAL_EXCEPTIONS TitleAdditionalExceptions;

        /// <summary>
        /// Gets or sets a value indicating whether the TitleAdditionalExceptions element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TitleAdditionalExceptions element has been assigned a value.</value>
        [XmlIgnore]
        public bool TitleAdditionalExceptionsSpecified
        {
            get { return this.TitleAdditionalExceptions != null && this.TitleAdditionalExceptions.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Additional requirements for the title.
        /// </summary>
        [XmlElement("TITLE_ADDITIONAL_REQUIREMENTS", Order = 8)]
        public TITLE_ADDITIONAL_REQUIREMENTS TitleAdditionalRequirements;

        /// <summary>
        /// Gets or sets a value indicating whether the TitleAdditionalRequirements element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TitleAdditionalRequirements element has been assigned a value.</value>
        [XmlIgnore]
        public bool TitleAdditionalRequirementsSpecified
        {
            get { return this.TitleAdditionalRequirements != null && this.TitleAdditionalRequirements.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Endorsements for the title.
        /// </summary>
        [XmlElement("TITLE_ENDORSEMENTS", Order = 9)]
        public TITLE_ENDORSEMENTS TitleEndorsements;

        /// <summary>
        /// Gets or sets a value indicating whether the TitleEndorsements element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TitleEndorsements element has been assigned a value.</value>
        [XmlIgnore]
        public bool TitleEndorsementsSpecified
        {
            get { return this.TitleEndorsements != null && this.TitleEndorsements.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details on the title response.
        /// </summary>
        [XmlElement("TITLE_RESPONSE_DETAIL", Order = 10)]
        public TITLE_RESPONSE_DETAIL TitleResponseDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the TitleResponseDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TitleResponseDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool TitleResponseDetailSpecified
        {
            get { return this.TitleResponseDetail != null && this.TitleResponseDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 11)]
        public TITLE_RESPONSE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
