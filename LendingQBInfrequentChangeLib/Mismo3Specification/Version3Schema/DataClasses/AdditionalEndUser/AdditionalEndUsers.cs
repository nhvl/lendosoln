namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class ADDITIONAL_END_USERS
    {
        /// <summary>
        /// Gets a value indicating whether the ADDITIONAL_END_USERS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AdditionalEndUserSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of additional end users.
        /// </summary>
        [XmlElement("ADDITIONAL_END_USER", Order = 0)]
		public List<ADDITIONAL_END_USER> AdditionalEndUser = new List<ADDITIONAL_END_USER>();

        /// <summary>
        /// Gets or sets a value indicating whether the AdditionalEndUser element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AdditionalEndUser element has been assigned a value.</value>
        [XmlIgnore]
        public bool AdditionalEndUserSpecified
        {
            get { return this.AdditionalEndUser != null && this.AdditionalEndUser.Count(a => a != null && a.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public ADDITIONAL_END_USERS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
