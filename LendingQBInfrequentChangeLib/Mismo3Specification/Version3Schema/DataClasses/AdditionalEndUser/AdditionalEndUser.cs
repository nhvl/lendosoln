namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class ADDITIONAL_END_USER
    {
        /// <summary>
        /// Gets a value indicating whether the ADDITIONAL_END_USER container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.EndUserBillingInstructionTypeOtherDescriptionSpecified
                    || this.EndUserBillingInstructionTypeSpecified
                    || this.EndUserNameSpecified
                    || this.ExtensionSpecified
                    || this.InternalAccountIdentifierSpecified;
            }
        }

        /// <summary>
        /// This is an enumerated list of values that identify how the charges will be billed for services related to Additional End-User processing.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<EndUserBillingInstructionBase> EndUserBillingInstructionType;

        /// <summary>
        /// Gets or sets a value indicating whether the EndUserBillingInstructionType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EndUserBillingInstructionType element has been assigned a value.</value>
        [XmlIgnore]
        public bool EndUserBillingInstructionTypeSpecified
        {
            get { return this.EndUserBillingInstructionType != null && this.EndUserBillingInstructionType.enumValue != EndUserBillingInstructionBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for End User Billing Instruction Type.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString EndUserBillingInstructionTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the EndUserBillingInstructionTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EndUserBillingInstructionTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool EndUserBillingInstructionTypeOtherDescriptionSpecified
        {
            get { return EndUserBillingInstructionTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The name of the party that has been identified as an Additional End-User of credit report data by the party requesting the credit report data.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString EndUserName;

        /// <summary>
        /// Gets or sets a value indicating whether the EndUserName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EndUserName element has been assigned a value.</value>
        [XmlIgnore]
        public bool EndUserNameSpecified
        {
            get { return EndUserName != null; }
            set { }
        }

        /// <summary>
        /// The account number or identifier assigned by a service provider to a service requestor. The Internal Account Identifier is supplied with request transactions to allow the service provider to identify the billing account to be used.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOIdentifier InternalAccountIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the InternalAccountIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InternalAccountIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool InternalAccountIdentifierSpecified
        {
            get { return InternalAccountIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public ADDITIONAL_END_USER_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
