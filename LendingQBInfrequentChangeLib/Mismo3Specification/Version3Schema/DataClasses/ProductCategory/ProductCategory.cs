namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PRODUCT_CATEGORY
    {
        /// <summary>
        /// Gets a value indicating whether the PRODUCT_CATEGORY container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ProductCategoryNameSpecified;
            }
        }

        /// <summary>
        /// A name for a collection of related products used for marketing or communication purposes.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString ProductCategoryName;

        /// <summary>
        /// Gets or sets a value indicating whether the ProductCategoryName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProductCategoryName element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProductCategoryNameSpecified
        {
            get { return ProductCategoryName != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public PRODUCT_CATEGORY_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
