namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class AFFORDABLE_LENDING
    {
        /// <summary>
        /// Gets a value indicating whether the AFFORDABLE_LENDING container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.FNMCommunityLendingProductTypeOtherDescriptionSpecified
                    || this.FNMCommunityLendingProductTypeSpecified
                    || this.FNMCommunitySecondsIndicatorSpecified
                    || this.FNMNeighborsMortgageEligibilityIndicatorSpecified
                    || this.HUDIncomeLimitAdjustmentPercentSpecified
                    || this.HUDLendingIncomeLimitAmountSpecified
                    || this.HUDMedianIncomeAmountSpecified;
            }
        }

        /// <summary>
        /// Specifies the type of the Fannie Mae community lending product that is associated with the loan.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<FNMCommunityLendingProductBase> FNMCommunityLendingProductType;

        /// <summary>
        /// Gets or sets a value indicating whether the FNMCommunityLendingProductType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FNMCommunityLendingProductType element has been assigned a value.</value>
        [XmlIgnore]
        public bool FNMCommunityLendingProductTypeSpecified
        {
            get { return this.FNMCommunityLendingProductType != null && this.FNMCommunityLendingProductType.enumValue != FNMCommunityLendingProductBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture a description of the FNM Community Lending Product Type when Other is selected.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString FNMCommunityLendingProductTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the FNMCommunityLendingProductTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FNMCommunityLendingProductTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool FNMCommunityLendingProductTypeOtherDescriptionSpecified
        {
            get { return FNMCommunityLendingProductTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether the loan is a Fannie Mae Community Seconds.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOIndicator FNMCommunitySecondsIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the FNMCommunitySecondsIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FNMCommunitySecondsIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool FNMCommunitySecondsIndicatorSpecified
        {
            get { return FNMCommunitySecondsIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether the loan is eligible to be a Fannie Mae Neighbors mortgage.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOIndicator FNMNeighborsMortgageEligibilityIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the FNMNeighborsMortgageEligibilityIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FNMNeighborsMortgageEligibilityIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool FNMNeighborsMortgageEligibilityIndicatorSpecified
        {
            get { return FNMNeighborsMortgageEligibilityIndicator != null; }
            set { }
        }

        /// <summary>
        /// The percentage by which the HUD Median Income can be exceeded in certain high cost areas.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOPercent HUDIncomeLimitAdjustmentPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the HUDIncomeLimitAdjustmentPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HUDIncomeLimitAdjustmentPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool HUDIncomeLimitAdjustmentPercentSpecified
        {
            get { return HUDIncomeLimitAdjustmentPercent != null; }
            set { }
        }

        /// <summary>
        /// The total dollar amount for HUD based on the median income and adjustment factors.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOAmount HUDLendingIncomeLimitAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the HUDLendingIncomeLimitAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HUDLendingIncomeLimitAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool HUDLendingIncomeLimitAmountSpecified
        {
            get { return HUDLendingIncomeLimitAmount != null; }
            set { }
        }

        /// <summary>
        /// The HUD estimated median family incomes to determine borrower eligibility for all applications related to affordable lending products.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOAmount HUDMedianIncomeAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the HUDMedianIncomeAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HUDMedianIncomeAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool HUDMedianIncomeAmountSpecified
        {
            get { return HUDMedianIncomeAmount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 7)]
        public AFFORDABLE_LENDING_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
