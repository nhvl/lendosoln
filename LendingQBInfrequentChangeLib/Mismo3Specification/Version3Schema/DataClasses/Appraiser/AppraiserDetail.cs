namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class APPRAISER_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the APPRAISER_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AppraisalSubCommitteeAppraiserIdentifierSpecified
                    || this.AppraiserCompanyNameSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A unique identifier for the individual appraiser as assigned by the federal authority, the Appraisal SubCommittee.  Only one identifier per individual.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIdentifier AppraisalSubCommitteeAppraiserIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the AppraisalSubCommitteeAppraiserIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AppraisalSubCommitteeAppraiserIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool AppraisalSubCommitteeAppraiserIdentifierSpecified
        {
            get { return AppraisalSubCommitteeAppraiserIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The name of the appraisal company that employed the appraiser who performed the appraisal of the property.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString AppraiserCompanyName;

        /// <summary>
        /// Gets or sets a value indicating whether the AppraiserCompanyName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AppraiserCompanyName element has been assigned a value.</value>
        [XmlIgnore]
        public bool AppraiserCompanyNameSpecified
        {
            get { return AppraiserCompanyName != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public APPRAISER_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
