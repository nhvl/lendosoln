namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class APPRAISER
    {
        /// <summary>
        /// Gets a value indicating whether the APPRAISER container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AppraiserDetailSpecified
                    || this.DesignationsSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// Information about the appraiser.
        /// </summary>
        [XmlElement("APPRAISER_DETAIL", Order = 0)]
        public APPRAISER_DETAIL AppraiserDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the AppraiserDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AppraiserDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool AppraiserDetailSpecified
        {
            get { return this.AppraiserDetail != null && this.AppraiserDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Appraiser designations.
        /// </summary>
        [XmlElement("DESIGNATIONS", Order = 1)]
        public DESIGNATIONS Designations;

        /// <summary>
        /// Gets or sets a value indicating whether the Designations element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Designations element has been assigned a value.</value>
        [XmlIgnore]
        public bool DesignationsSpecified
        {
            get { return this.Designations != null && this.Designations.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public APPRAISER_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
