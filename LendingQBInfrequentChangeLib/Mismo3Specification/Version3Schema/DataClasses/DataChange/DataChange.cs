namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class DATA_CHANGE
    {
        /// <summary>
        /// Gets a value indicating whether the DATA_CHANGE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DataItemChangeRequestSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A change request for a data item.
        /// </summary>
        [XmlElement("DATA_ITEM_CHANGE_REQUEST", Order = 0)]
        public DATA_ITEM_CHANGE_REQUEST DataItemChangeRequest;

        /// <summary>
        /// Gets or sets a value indicating whether the DataItemChangeRequest element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DataItemChangeRequest element has been assigned a value.</value>
        [XmlIgnore]
        public bool DataItemChangeRequestSpecified
        {
            get { return this.DataItemChangeRequest != null && this.DataItemChangeRequest.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public DATA_CHANGE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
