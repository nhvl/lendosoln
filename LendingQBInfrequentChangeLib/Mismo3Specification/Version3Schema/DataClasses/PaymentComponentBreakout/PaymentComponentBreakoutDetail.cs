namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PAYMENT_COMPONENT_BREAKOUT_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the PAYMENT_COMPONENT_BREAKOUT_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BuydownSubsidyPaymentAmountSpecified
                    || this.BuydownSubsidyPaymentEffectiveDateSpecified
                    || this.EscrowShortagePaymentAmountSpecified
                    || this.ExtensionSpecified
                    || this.HomeownersAssociationDuesPaymentAmountSpecified
                    || this.HUD235SubsidyPaymentAmountSpecified
                    || this.HUD235SubsidyPaymentEffectiveDateSpecified
                    || this.InsurancePaymentAmountSpecified
                    || this.InterestPaymentAmountSpecified
                    || this.PaymentStateTypeOtherDescriptionSpecified
                    || this.PaymentStateTypeSpecified
                    || this.PrincipalAndInterestPaymentAmountSpecified
                    || this.PrincipalAndInterestPaymentEffectiveDateSpecified
                    || this.PrincipalPaymentAmountSpecified
                    || this.SCRASubsidyPaymentAmountSpecified
                    || this.TaxAndInsurancePaymentAmountSpecified
                    || this.TaxAndInsurancePaymentEffectiveDateSpecified
                    || this.TaxesPaymentAmountSpecified
                    || this.TotalEscrowPaymentAmountSpecified
                    || this.TotalOptionalProductsPaymentAmountSpecified
                    || this.TotalOptionalProductsPaymentEffectiveDateSpecified
                    || this.TotalPaymentAmountSpecified;
            }
        }

        /// <summary>
        /// The buy down subsidy amount that is a part of the total payment amount being reported. 
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount BuydownSubsidyPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the Buy down Subsidy Payment Amount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Buy down Subsidy Payment Amount element has been assigned a value.</value>
        [XmlIgnore]
        public bool BuydownSubsidyPaymentAmountSpecified
        {
            get { return BuydownSubsidyPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// The effective date of the buy-down subsidy amount that is being reported.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMODate BuydownSubsidyPaymentEffectiveDate;

        /// <summary>
        /// Gets or sets a value indicating whether the Buy Down Subsidy Payment Effective Date element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Buy Down Subsidy Payment Effective Date element has been assigned a value.</value>
        [XmlIgnore]
        public bool BuydownSubsidyPaymentEffectiveDateSpecified
        {
            get { return BuydownSubsidyPaymentEffectiveDate != null; }
            set { }
        }

        /// <summary>
        /// The additional monthly escrow payment amount as the result of a shortage at the last escrow analysis.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOAmount EscrowShortagePaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowShortagePaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowShortagePaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowShortagePaymentAmountSpecified
        {
            get { return EscrowShortagePaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// The portion of the monthly mortgage payment that represents funds for the homeowners association dues.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOAmount HomeownersAssociationDuesPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the HomeownersAssociationDuesPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HomeownersAssociationDuesPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool HomeownersAssociationDuesPaymentAmountSpecified
        {
            get { return HomeownersAssociationDuesPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// The HUD 235 subsidy amount that is part of the total payment being reported.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOAmount HUD235SubsidyPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the HUD235SubsidyPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HUD235SubsidyPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool HUD235SubsidyPaymentAmountSpecified
        {
            get { return HUD235SubsidyPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// The effective date of the HUD 235 Subsidy Amount that is being reported.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMODate HUD235SubsidyPaymentEffectiveDate;

        /// <summary>
        /// Gets or sets a value indicating whether the HUD235SubsidyPaymentEffectiveDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HUD235SubsidyPaymentEffectiveDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool HUD235SubsidyPaymentEffectiveDateSpecified
        {
            get { return HUD235SubsidyPaymentEffectiveDate != null; }
            set { }
        }

        /// <summary>
        /// The portion of the monthly mortgage that is allocated to the property insurance on the subject property.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOAmount InsurancePaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the InsurancePaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InsurancePaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool InsurancePaymentAmountSpecified
        {
            get { return InsurancePaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// The portion of the monthly mortgage payment that represents the amount of interest paid on the loan.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOAmount InterestPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the InterestPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InterestPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool InterestPaymentAmountSpecified
        {
            get { return InterestPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// Specifies the state of the payment that is being reported.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOEnum<PaymentStateBase> PaymentStateType;

        /// <summary>
        /// Gets or sets a value indicating whether the PaymentStateType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PaymentStateType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PaymentStateTypeSpecified
        {
            get { return this.PaymentStateType != null && this.PaymentStateType.enumValue != PaymentStateBase.Blank; }
            set { }
        }

        /// <summary>
        /// Specifies the state of the payment that is being reported when Other is selected for Payment State Type.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOString PaymentStateTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PaymentStateTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PaymentStateTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PaymentStateTypeOtherDescriptionSpecified
        {
            get { return PaymentStateTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The principal and interest amount that is part of the total payment being reported. 
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOAmount PrincipalAndInterestPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PrincipalAndInterestPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrincipalAndInterestPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrincipalAndInterestPaymentAmountSpecified
        {
            get { return PrincipalAndInterestPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// The effective date of the Principal And Interest Amount that is being reported.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMODate PrincipalAndInterestPaymentEffectiveDate;

        /// <summary>
        /// Gets or sets a value indicating whether the PrincipalAndInterestPaymentEffectiveDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrincipalAndInterestPaymentEffectiveDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrincipalAndInterestPaymentEffectiveDateSpecified
        {
            get { return PrincipalAndInterestPaymentEffectiveDate != null; }
            set { }
        }

        /// <summary>
        /// The portion of the monthly mortgage payment that represents repayment of the amount borrowed, exclusive of interest charges.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOAmount PrincipalPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PrincipalPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrincipalPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrincipalPaymentAmountSpecified
        {
            get { return PrincipalPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// The portion of the monthly mortgage payment that represents the subsidy for participants in  the Service members Relief Act (SCRA.) For cases where service member interest relief is handled via subsidy, as opposed to changing the interest rate.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOAmount SCRASubsidyPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the SCRASubsidyPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SCRASubsidyPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool SCRASubsidyPaymentAmountSpecified
        {
            get { return SCRASubsidyPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// The taxes and insurance amount (excluding Optional Insurance) that is a part of the total payment being reported. This includes any mortgage insurance, all taxes, and property and casualty insurance amounts that are to be placed in an escrow account.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOAmount TaxAndInsurancePaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the TaxAndInsurancePaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TaxAndInsurancePaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TaxAndInsurancePaymentAmountSpecified
        {
            get { return TaxAndInsurancePaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// The effective date of the Tax and Insurance Amount that is being reported.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMODate TaxAndInsurancePaymentEffectiveDate;

        /// <summary>
        /// Gets or sets a value indicating whether the TaxAndInsurancePaymentEffectiveDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TaxAndInsurancePaymentEffectiveDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool TaxAndInsurancePaymentEffectiveDateSpecified
        {
            get { return TaxAndInsurancePaymentEffectiveDate != null; }
            set { }
        }

        /// <summary>
        /// The portion of the monthly mortgage that is allocated to the property tax on the subject property.
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMOAmount TaxesPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the TaxesPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TaxesPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TaxesPaymentAmountSpecified
        {
            get { return TaxesPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// The portion of the monthly mortgage payment that represents funds for an escrow account.
        /// </summary>
        [XmlElement(Order = 17)]
        public MISMOAmount TotalEscrowPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the TotalEscrowPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TotalEscrowPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TotalEscrowPaymentAmountSpecified
        {
            get { return TotalEscrowPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// The total optional products amount that is part of the total payment being reported.
        /// </summary>
        [XmlElement(Order = 18)]
        public MISMOAmount TotalOptionalProductsPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the TotalOptionalProductsPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TotalOptionalProductsPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TotalOptionalProductsPaymentAmountSpecified
        {
            get { return TotalOptionalProductsPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// The effective date of the Total Optional Products Amount that is being reported.
        /// </summary>
        [XmlElement(Order = 19)]
        public MISMODate TotalOptionalProductsPaymentEffectiveDate;

        /// <summary>
        /// Gets or sets a value indicating whether the TotalOptionalProductsPaymentEffectiveDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TotalOptionalProductsPaymentEffectiveDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool TotalOptionalProductsPaymentEffectiveDateSpecified
        {
            get { return TotalOptionalProductsPaymentEffectiveDate != null; }
            set { }
        }

        /// <summary>
        /// The total payment amount that is reported.
        /// </summary>
        [XmlElement(Order = 20)]
        public MISMOAmount TotalPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the TotalPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TotalPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TotalPaymentAmountSpecified
        {
            get { return TotalPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 21)]
        public PAYMENT_COMPONENT_BREAKOUT_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
