namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PAYMENT_COMPONENT_BREAKOUTS
    {
        /// <summary>
        /// Gets a value indicating whether the PAYMENT_COMPONENT_BREAKOUTS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.PaymentComponentBreakoutSpecified;
            }
        }

        /// <summary>
        /// A collection of payment component breakouts.
        /// </summary>
        [XmlElement("PAYMENT_COMPONENT_BREAKOUT", Order = 0)]
		public List<PAYMENT_COMPONENT_BREAKOUT> PaymentComponentBreakout = new List<PAYMENT_COMPONENT_BREAKOUT>();

        /// <summary>
        /// Gets or sets a value indicating whether the PaymentComponentBreakout element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PaymentComponentBreakout element has been assigned a value.</value>
        [XmlIgnore]
        public bool PaymentComponentBreakoutSpecified
        {
            get { return this.PaymentComponentBreakout != null && this.PaymentComponentBreakout.Count(p => p != null && p.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public PAYMENT_COMPONENT_BREAKOUTS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
