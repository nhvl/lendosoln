namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class MARKET_INVENTORIES
    {
        /// <summary>
        /// Gets a value indicating whether the MARKET_INVENTORIES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.MarketInventorySpecified;
            }
        }

        /// <summary>
        /// A collection of market inventories.
        /// </summary>
        [XmlElement("MARKET_INVENTORY", Order = 0)]
		public List<MARKET_INVENTORY> MarketInventory = new List<MARKET_INVENTORY>();

        /// <summary>
        /// Gets or sets a value indicating whether the MarketInventory element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MarketInventory element has been assigned a value.</value>
        [XmlIgnore]
        public bool MarketInventorySpecified
        {
            get { return this.MarketInventory != null && this.MarketInventory.Count(m => m != null && m.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public MARKET_INVENTORIES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
