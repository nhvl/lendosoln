namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class MARKET_INVENTORY
    {
        /// <summary>
        /// Gets a value indicating whether the MARKET_INVENTORY container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.MarketInventoryAmountSpecified
                    || this.MarketInventoryCountSpecified
                    || this.MarketInventoryMonthRangeTypeSpecified
                    || this.MarketInventoryRatioPercentSpecified
                    || this.MarketInventoryTrendTypeSpecified
                    || this.MarketInventoryTypeSpecified;
            }
        }

        /// <summary>
        /// The dollar amount of properties in the real estate market inventory category.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount MarketInventoryAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the MarketInventoryAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MarketInventoryAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool MarketInventoryAmountSpecified
        {
            get { return MarketInventoryAmount != null; }
            set { }
        }

        /// <summary>
        /// The number of properties in the real estate market inventory category.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOCount MarketInventoryCount;

        /// <summary>
        /// Gets or sets a value indicating whether the MarketInventoryCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MarketInventoryCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool MarketInventoryCountSpecified
        {
            get { return MarketInventoryCount != null; }
            set { }
        }

        /// <summary>
        /// Specifies the date range of the real estate market inventory analysis.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<MarketInventoryMonthRangeBase> MarketInventoryMonthRangeType;

        /// <summary>
        /// Gets or sets a value indicating whether the MarketInventoryMonthRangeType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MarketInventoryMonthRangeType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MarketInventoryMonthRangeTypeSpecified
        {
            get { return this.MarketInventoryMonthRangeType != null && this.MarketInventoryMonthRangeType.enumValue != MarketInventoryMonthRangeBase.Blank; }
            set { }
        }

        /// <summary>
        /// The count of properties in the real estate market inventory category expressed as a percentage.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOPercent MarketInventoryRatioPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the MarketInventoryRatioPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MarketInventoryRatioPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool MarketInventoryRatioPercentSpecified
        {
            get { return MarketInventoryRatioPercent != null; }
            set { }
        }

        /// <summary>
        /// Specifies the trend of the real estate market inventory type.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<MarketInventoryTrendBase> MarketInventoryTrendType;

        /// <summary>
        /// Gets or sets a value indicating whether the MarketInventoryTrendType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MarketInventoryTrendType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MarketInventoryTrendTypeSpecified
        {
            get { return this.MarketInventoryTrendType != null && this.MarketInventoryTrendType.enumValue != MarketInventoryTrendBase.Blank; }
            set { }
        }

        /// <summary>
        /// Specifies the category of real estate market inventory analysis.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOEnum<MarketInventoryBase> MarketInventoryType;

        /// <summary>
        /// Gets or sets a value indicating whether the MarketInventoryType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MarketInventoryType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MarketInventoryTypeSpecified
        {
            get { return this.MarketInventoryType != null && this.MarketInventoryType.enumValue != MarketInventoryBase.Blank; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 6)]
        public MARKET_INVENTORY_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
