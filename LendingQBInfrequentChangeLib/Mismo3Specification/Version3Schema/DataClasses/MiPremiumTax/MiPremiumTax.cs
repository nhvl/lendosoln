namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class MI_PREMIUM_TAX
    {
        /// <summary>
        /// Gets a value indicating whether the MI_PREMIUM_TAX container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.MIPremiumTaxCodeAmountSpecified
                    || this.MIPremiumTaxCodePercentSpecified
                    || this.MIPremiumTaxCodeTypeSpecified
                    || this.MIPremiumTaxingAuthorityNameSpecified
                    || this.PaymentIncludedInAPRIndicatorSpecified
                    || this.PaymentIncludedInJurisdictionHighCostIndicatorSpecified
                    || this.RegulationZPointsAndFeesIndicatorSpecified;
            }
        }

        /// <summary>
        /// To convey the type of premium tax amount(State, Municipal or County).
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount MIPremiumTaxCodeAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPremiumTaxCodeAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPremiumTaxCodeAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumTaxCodeAmountSpecified
        {
            get { return MIPremiumTaxCodeAmount != null; }
            set { }
        }

        /// <summary>
        /// To convey the  premium tax percent to be applied to the MI Premium Amount to compute the premium tax due amount for the specified term.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOPercent MIPremiumTaxCodePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPremiumTaxCodePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPremiumTaxCodePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumTaxCodePercentSpecified
        {
            get { return MIPremiumTaxCodePercent != null; }
            set { }
        }

        /// <summary>
        /// To convey the type of premium tax (State, Municipal or County).
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<MIPremiumTaxCodeBase> MIPremiumTaxCodeType;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPremiumTaxCodeType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPremiumTaxCodeType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumTaxCodeTypeSpecified
        {
            get { return this.MIPremiumTaxCodeType != null && this.MIPremiumTaxCodeType.enumValue != MIPremiumTaxCodeBase.Blank; }
            set { }
        }

        /// <summary>
        /// The name of taxing authority for the premium tax.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString MIPremiumTaxingAuthorityName;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPremiumTaxingAuthorityName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPremiumTaxingAuthorityName element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumTaxingAuthorityNameSpecified
        {
            get { return MIPremiumTaxingAuthorityName != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates fee is to be included in APR calculations.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOIndicator PaymentIncludedInAPRIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PaymentIncludedInAPRIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PaymentIncludedInAPRIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PaymentIncludedInAPRIndicatorSpecified
        {
            get { return PaymentIncludedInAPRIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates the amount of the payment is included in the points and fees calculation appropriated to the jurisdiction of the subject property.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOIndicator PaymentIncludedInJurisdictionHighCostIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PaymentIncludedInJurisdictionHighCostIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PaymentIncludedInJurisdictionHighCostIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PaymentIncludedInJurisdictionHighCostIndicatorSpecified
        {
            get { return PaymentIncludedInJurisdictionHighCostIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that item is included in the Regulation Z Points and Fees calculation.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOIndicator RegulationZPointsAndFeesIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the RegulationZPointsAndFeesIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RegulationZPointsAndFeesIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool RegulationZPointsAndFeesIndicatorSpecified
        {
            get { return RegulationZPointsAndFeesIndicator != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 7)]
        public MI_PREMIUM_TAX_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
