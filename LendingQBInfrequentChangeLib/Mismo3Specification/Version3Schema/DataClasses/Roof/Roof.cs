namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class ROOF
    {
        /// <summary>
        /// Gets a value indicating whether the ROOF container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ComponentAdjustmentAmountSpecified
                    || this.ComponentClassificationTypeSpecified
                    || this.ConditionRatingDescriptionSpecified
                    || this.ConditionRatingTypeSpecified
                    || this.ExtensionSpecified
                    || this.MaterialDescriptionSpecified
                    || this.QualityRatingDescriptionSpecified
                    || this.QualityRatingTypeSpecified
                    || this.RoofMaterialTypeOtherDescriptionSpecified
                    || this.RoofMaterialTypeSpecified;
            }
        }

        /// <summary>
        /// The dollar amount (either positive or negative) adjustment being made for a specific component of the property.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount ComponentAdjustmentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ComponentAdjustmentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ComponentAdjustmentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ComponentAdjustmentAmountSpecified
        {
            get { return ComponentAdjustmentAmount != null; }
            set { }
        }

        /// <summary>
        /// Specifies whether the component is considered real or personal property.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<ComponentClassificationBase> ComponentClassificationType;

        /// <summary>
        /// Gets or sets a value indicating whether the ComponentClassificationType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ComponentClassificationType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ComponentClassificationTypeSpecified
        {
            get { return this.ComponentClassificationType != null && this.ComponentClassificationType.enumValue != ComponentClassificationBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe in detail the condition rating of the identified component.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString ConditionRatingDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ConditionRatingDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConditionRatingDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConditionRatingDescriptionSpecified
        {
            get { return ConditionRatingDescription != null; }
            set { }
        }

        /// <summary>
        /// A rating of the condition of the identified component.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOEnum<ConditionRatingBase> ConditionRatingType;

        /// <summary>
        /// Gets or sets a value indicating whether the ConditionRatingType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConditionRatingType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConditionRatingTypeSpecified
        {
            get { return this.ConditionRatingType != null && this.ConditionRatingType.enumValue != ConditionRatingBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe in detail the material used in the identified component.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString MaterialDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MaterialDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MaterialDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MaterialDescriptionSpecified
        {
            get { return MaterialDescription != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe in detail the quality rating of the identified component.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString QualityRatingDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the QualityRatingDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the QualityRatingDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool QualityRatingDescriptionSpecified
        {
            get { return QualityRatingDescription != null; }
            set { }
        }

        /// <summary>
        /// A rating of the quality of the identified component type.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOEnum<QualityRatingBase> QualityRatingType;

        /// <summary>
        /// Gets or sets a value indicating whether the QualityRatingType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the QualityRatingType element has been assigned a value.</value>
        [XmlIgnore]
        public bool QualityRatingTypeSpecified
        {
            get { return this.QualityRatingType != null && this.QualityRatingType.enumValue != QualityRatingBase.Blank; }
            set { }
        }

        /// <summary>
        /// Identifies the type of roofing material used in the structure.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOEnum<RoofMaterialBase> RoofMaterialType;

        /// <summary>
        /// Gets or sets a value indicating whether the RoofMaterialType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RoofMaterialType element has been assigned a value.</value>
        [XmlIgnore]
        public bool RoofMaterialTypeSpecified
        {
            get { return this.RoofMaterialType != null && this.RoofMaterialType.enumValue != RoofMaterialBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the wall material if Other is selected as the Roof Material Type.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOString RoofMaterialTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the RoofMaterialTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RoofMaterialTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool RoofMaterialTypeOtherDescriptionSpecified
        {
            get { return RoofMaterialTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 9)]
        public ROOF_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
