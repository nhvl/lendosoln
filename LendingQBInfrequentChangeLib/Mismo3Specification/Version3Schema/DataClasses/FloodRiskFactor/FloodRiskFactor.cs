namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class FLOOD_RISK_FACTOR
    {
        /// <summary>
        /// Gets a value indicating whether the FLOOD_RISK_FACTOR container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BaseFloodElevationFeetNumberSpecified
                    || this.ExtensionSpecified
                    || this.FloodDepthFeetNumberSpecified
                    || this.PropertyElevationFeetNumberSpecified
                    || this.SpecialFloodHazardAreaDistanceFeetNumberSpecified;
            }
        }

        /// <summary>
        /// The elevation of flood water in relation to sea level at its highest point for a property. Measured in feet.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMONumeric BaseFloodElevationFeetNumber;

        /// <summary>
        /// Gets or sets a value indicating whether the BaseFloodElevationFeetNumber element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BaseFloodElevationFeetNumber element has been assigned a value.</value>
        [XmlIgnore]
        public bool BaseFloodElevationFeetNumberSpecified
        {
            get { return BaseFloodElevationFeetNumber != null; }
            set { }
        }

        /// <summary>
        /// The depth of the flood water in relation to the ground for a particular property.  Measured in feet.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMONumeric FloodDepthFeetNumber;

        /// <summary>
        /// Gets or sets a value indicating whether the FloodDepthFeetNumber element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FloodDepthFeetNumber element has been assigned a value.</value>
        [XmlIgnore]
        public bool FloodDepthFeetNumberSpecified
        {
            get { return FloodDepthFeetNumber != null; }
            set { }
        }

        /// <summary>
        /// The ground elevation in relation to sea level for a particular property.  Measured in feet.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMONumeric PropertyElevationFeetNumber;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyElevationFeetNumber element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyElevationFeetNumber element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyElevationFeetNumberSpecified
        {
            get { return PropertyElevationFeetNumber != null; }
            set { }
        }

        /// <summary>
        /// The distance in feet of the nearest Special Flood Hazard Area to a particular property.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMONumeric SpecialFloodHazardAreaDistanceFeetNumber;

        /// <summary>
        /// Gets or sets a value indicating whether the SpecialFloodHazardAreaDistanceFeetNumber element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SpecialFloodHazardAreaDistanceFeetNumber element has been assigned a value.</value>
        [XmlIgnore]
        public bool SpecialFloodHazardAreaDistanceFeetNumberSpecified
        {
            get { return SpecialFloodHazardAreaDistanceFeetNumber != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public FLOOD_RISK_FACTOR_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
