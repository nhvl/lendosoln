namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class SERVICE_PRODUCT_REQUEST
    {
        /// <summary>
        /// Gets a value indicating whether the SERVICE_PRODUCT_REQUEST container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ServiceProductDetailSpecified
                    || this.ServiceProductNamesSpecified;
            }
        }

        /// <summary>
        /// Details on the service product.
        /// </summary>
        [XmlElement("SERVICE_PRODUCT_DETAIL", Order = 0)]
        public SERVICE_PRODUCT_DETAIL ServiceProductDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the ServiceProductDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServiceProductDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServiceProductDetailSpecified
        {
            get { return this.ServiceProductDetail != null && this.ServiceProductDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Names of the service products.
        /// </summary>
        [XmlElement("SERVICE_PRODUCT_NAMES", Order = 1)]
        public SERVICE_PRODUCT_NAMES ServiceProductNames;

        /// <summary>
        /// Gets or sets a value indicating whether the ServiceProductNames element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServiceProductNames element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServiceProductNamesSpecified
        {
            get { return this.ServiceProductNames != null && this.ServiceProductNames.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public SERVICE_PRODUCT_REQUEST_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
