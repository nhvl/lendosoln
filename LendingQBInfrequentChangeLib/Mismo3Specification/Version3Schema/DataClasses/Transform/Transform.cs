namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class TRANSFORM
    {
        /// <summary>
        /// Gets a value indicating whether the TRANSFORM container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                if (Mismo3Utilities.ExceedsThreshold(1,
                    new List<bool> { this.TransformFilesSpecified, this.TransformPagesSpecified }))
                {
                    throw new System.Xml.Schema.XmlSchemaValidationException(
                        Mismo3Utilities.GetXSDChoiceExceptionMessage(
                        "TRANSFORM",
                        new List<string> { "TRANSFORM_FILES", "TRANSFORM_PAGES" }));
                }

                return this.ExtensionSpecified
                    || this.TransformFilesSpecified
                    || this.TransformPagesSpecified;
            }
        }

        /// <summary>
        /// A list of transform files.
        /// </summary>
        [XmlElement("TRANSFORM_FILES", Order = 0)]
        public TRANSFORM_FILES TransformFiles;

        /// <summary>
        /// Gets or sets a value indicating whether the TransformFiles element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TransformFiles element has been assigned a value.</value>
        [XmlIgnore]
        public bool TransformFilesSpecified
        {
            get { return this.TransformFiles != null && this.TransformFiles.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A list of transform pages.
        /// </summary>
        [XmlElement("TRANSFORM_PAGES", Order = 1)]
        public TRANSFORM_PAGES TransformPages;

        /// <summary>
        /// Gets or sets a value indicating whether the TransformPages element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TransformPages element has been assigned a value.</value>
        [XmlIgnore]
        public bool TransformPagesSpecified
        {
            get { return this.TransformPages != null && this.TransformPages.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public TRANSFORM_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
