namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class TRANSFORMS
    {
        /// <summary>
        /// Gets a value indicating whether the TRANSFORMS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.TransformSpecified;
            }
        }

        /// <summary>
        /// A collection of transforms.
        /// </summary>
        [XmlElement("TRANSFORM", Order = 0)]
		public List<TRANSFORM> Transform = new List<TRANSFORM>();

        /// <summary>
        /// Gets or sets a value indicating whether the Transform element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Transform element has been assigned a value.</value>
        [XmlIgnore]
        public bool TransformSpecified
        {
            get { return this.Transform != null && this.Transform.Count(t => t != null && t.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public TRANSFORMS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
