namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class IDENTIFIED_SERVICE_PROVIDERS
    {
        /// <summary>
        /// Gets a value indicating whether the IDENTIFIED_SERVICE_PROVIDERS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.IdentifiedServiceProviderSpecified;
            }
        }

        /// <summary>
        /// A collection of identified service providers.
        /// </summary>
        [XmlElement("IDENTIFIED_SERVICE_PROVIDER", Order = 0)]
		public List<IDENTIFIED_SERVICE_PROVIDER> IdentifiedServiceProvider = new List<IDENTIFIED_SERVICE_PROVIDER>();

        /// <summary>
        /// Gets or sets a value indicating whether the IdentifiedServiceProvider element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IdentifiedServiceProvider element has been assigned a value.</value>
        [XmlIgnore]
        public bool IdentifiedServiceProviderSpecified
        {
            get { return this.IdentifiedServiceProvider != null && this.IdentifiedServiceProvider.Count(i => i != null && i.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public IDENTIFIED_SERVICE_PROVIDERS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
