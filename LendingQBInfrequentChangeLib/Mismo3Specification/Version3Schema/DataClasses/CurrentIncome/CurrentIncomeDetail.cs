namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CURRENT_INCOME_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the CURRENT_INCOME_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.IncomeReportedTypeSpecified
                    || this.URLABorrowerTotalMonthlyIncomeAmountSpecified
                    || this.URLABorrowerTotalOtherIncomeAmountSpecified;
            }
        }

        /// <summary>
        /// Specifies whether the total monthly income is either Pre-tax or Post-tax.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<IncomeReportedBase> IncomeReportedType;

        /// <summary>
        /// Gets or sets a value indicating whether the IncomeReportedType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IncomeReportedType element has been assigned a value.</value>
        [XmlIgnore]
        public bool IncomeReportedTypeSpecified
        {
            get { return this.IncomeReportedType != null && this.IncomeReportedType.enumValue != IncomeReportedBase.Blank; }
            set { }
        }

        /// <summary>
        /// The total of the individual gross monthly income amount from all sources on an instance of the URLA for a borrower. Collected on the URLA in Section V. Monthly Income - Total.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOAmount URLABorrowerTotalMonthlyIncomeAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the URLABorrowerTotalMonthlyIncomeAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the URLABorrowerTotalMonthlyIncomeAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool URLABorrowerTotalMonthlyIncomeAmountSpecified
        {
            get { return URLABorrowerTotalMonthlyIncomeAmount != null; }
            set { }
        }

        /// <summary>
        /// The total of the individual gross monthly income amount from other sources as listed in the section "Describe Other Income" reported on an instance of the URLA for a borrower.  Collected on the URLA in Section V. Monthly Income - Total.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOAmount URLABorrowerTotalOtherIncomeAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the URLABorrowerTotalOtherIncomeAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the URLABorrowerTotalOtherIncomeAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool URLABorrowerTotalOtherIncomeAmountSpecified
        {
            get { return URLABorrowerTotalOtherIncomeAmount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public CURRENT_INCOME_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
