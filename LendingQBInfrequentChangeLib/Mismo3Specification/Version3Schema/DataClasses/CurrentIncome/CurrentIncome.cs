namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CURRENT_INCOME
    {
        /// <summary>
        /// Gets a value indicating whether the CURRENT_INCOME container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CurrentIncomeDetailSpecified
                    || this.CurrentIncomeItemsSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// Details on current income.
        /// </summary>
        [XmlElement("CURRENT_INCOME_DETAIL", Order = 0)]
        public CURRENT_INCOME_DETAIL CurrentIncomeDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the CurrentIncomeDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CurrentIncomeDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool CurrentIncomeDetailSpecified
        {
            get { return this.CurrentIncomeDetail != null && this.CurrentIncomeDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A list of current income items.
        /// </summary>
        [XmlElement("CURRENT_INCOME_ITEMS", Order = 1)]
        public CURRENT_INCOME_ITEMS CurrentIncomeItems;

        /// <summary>
        /// Gets or sets a value indicating whether the CurrentIncomeItems element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CurrentIncomeItems element has been assigned a value.</value>
        [XmlIgnore]
        public bool CurrentIncomeItemsSpecified
        {
            get { return this.CurrentIncomeItems != null && this.CurrentIncomeItems.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public CURRENT_INCOME_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
