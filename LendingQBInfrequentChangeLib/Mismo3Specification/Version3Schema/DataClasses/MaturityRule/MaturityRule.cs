namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class MATURITY_RULE
    {
        /// <summary>
        /// Gets a value indicating whether the MATURITY_RULE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get 
            {
                return this.BiweeklyComparableMonthlyMaturityDateSpecified
                || this.LoanMaturityDateSpecified
                || this.LoanMaturityPeriodCountSpecified
                || this.LoanMaturityPeriodTypeSpecified
                || this.LoanTermMaximumMonthsCountSpecified
                || this.MaximumMaturityTermExtensionCountSpecified
                || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// For a loan with scheduled biweekly payments this is the alternative maturity date if the loan had scheduled monthly payments.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMODate BiweeklyComparableMonthlyMaturityDate;

        /// <summary>
        /// Gets or sets a value indicating whether the BiweeklyComparableMonthlyMaturityDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BiweeklyComparableMonthlyMaturityDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool BiweeklyComparableMonthlyMaturityDateSpecified
        {
            get { return BiweeklyComparableMonthlyMaturityDate != null; }
            set { }
        }

        /// <summary>
        /// The date when the loan is scheduled to be paid in full as reflected on the Note.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMODate LoanMaturityDate;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanMaturityDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanMaturityDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanMaturityDateSpecified
        {
            get { return LoanMaturityDate != null; }
            set { }
        }

        /// <summary>
        /// The scheduled number of periods (as defined by Loan Maturity Period Type) after which the loan will mature. 
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOCount LoanMaturityPeriodCount;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanMaturityPeriodCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanMaturityPeriodCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanMaturityPeriodCountSpecified
        {
            get { return LoanMaturityPeriodCount != null; }
            set { }
        }

        /// <summary>
        /// The unit of time used to define the period over which the loan matures. Used in conjunction with Loan Maturity Period Count.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOEnum<LoanMaturityPeriodBase> LoanMaturityPeriodType;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanMaturityPeriodType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanMaturityPeriodType element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanMaturityPeriodTypeSpecified
        {
            get { return this.LoanMaturityPeriodType != null && this.LoanMaturityPeriodType.enumValue != LoanMaturityPeriodBase.Blank; }
            set { }
        }

        /// <summary>
        /// The maximum possible number of months for the term to maturity, if the term to maturity is extendable.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOCount LoanTermMaximumMonthsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanTermMaximumMonthsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanTermMaximumMonthsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanTermMaximumMonthsCountSpecified
        {
            get { return LoanTermMaximumMonthsCount != null; }
            set { }
        }

        /// <summary>
        /// The number of maturity term extensions that a borrower is allowed to exercise over the life of the loan.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOCount MaximumMaturityTermExtensionCount;

        /// <summary>
        /// Gets or sets a value indicating whether the MaximumMaturityTermExtensionCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MaximumMaturityTermExtensionCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool MaximumMaturityTermExtensionCountSpecified
        {
            get { return MaximumMaturityTermExtensionCount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 6)]
        public MATURITY_RULE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
