namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class HOUSING_EXPENSES
    {
        /// <summary>
        /// Gets a value indicating whether the HOUSING_EXPENSES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.HousingExpenseSpecified;
            }
        }

        /// <summary>
        /// A collection of housing expenses.
        /// </summary>
        [XmlElement("HOUSING_EXPENSE", Order = 0)]
		public List<HOUSING_EXPENSE> HousingExpense = new List<HOUSING_EXPENSE>();

        /// <summary>
        /// Gets or sets a value indicating whether the HousingExpense element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HousingExpense element has been assigned a value.</value>
        [XmlIgnore]
        public bool HousingExpenseSpecified
        {
            get { return this.HousingExpense != null && this.HousingExpense.Count(h => h != null && h.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public HOUSING_EXPENSES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
