namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class REPAIR_SUMMARY
    {
        /// <summary>
        /// Gets a value indicating whether the REPAIR_SUMMARY container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.RepairsDescriptionSpecified
                    || this.RepairsTotalCostAmountSpecified
                    || this.RepairsTotalCostPercentSpecified;
            }
        }

        /// <summary>
        /// A free-form text field describing the repairs to be performed on the subject property. 
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString RepairsDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the RepairsDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RepairsDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool RepairsDescriptionSpecified
        {
            get { return RepairsDescription != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of the total cost of the repairs to be performed on the subject property.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOAmount RepairsTotalCostAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the RepairsTotalCostAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RepairsTotalCostAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool RepairsTotalCostAmountSpecified
        {
            get { return RepairsTotalCostAmount != null; }
            set { }
        }

        /// <summary>
        /// The cost of repairs required to bring the property to good and habitable condition as a percentage of the property's estimated market value.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOPercent RepairsTotalCostPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the RepairsTotalCostPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RepairsTotalCostPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool RepairsTotalCostPercentSpecified
        {
            get { return RepairsTotalCostPercent != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public REPAIR_SUMMARY_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
