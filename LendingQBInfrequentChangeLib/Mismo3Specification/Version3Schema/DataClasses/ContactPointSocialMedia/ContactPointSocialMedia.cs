namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CONTACT_POINT_SOCIAL_MEDIA
    {
        /// <summary>
        /// Gets a value indicating whether the CONTACT_POINT_SOCIAL_MEDIA container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.SocialMediaIdentifierSpecified;
            }
        }

        /// <summary>
        /// The identifier of the party within a social media site.  Use the IdentifierOwnerURI attribute value to identify the social media provider.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIdentifier SocialMediaIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the SocialMediaIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SocialMediaIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool SocialMediaIdentifierSpecified
        {
            get { return SocialMediaIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public CONTACT_POINT_SOCIAL_MEDIA_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
