namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class COLLECTIONS
    {
        /// <summary>
        /// Gets a value indicating whether the COLLECTIONS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CollectionSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of collections.
        /// </summary>
        [XmlElement("COLLECTION", Order = 0)]
		public List<COLLECTION> Collection = new List<COLLECTION>();

        /// <summary>
        /// Gets or sets a value indicating whether the Collection element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Collection element has been assigned a value.</value>
        [XmlIgnore]
        public bool CollectionSpecified
        {
            get { return this.Collection != null && this.Collection.Count(c => c != null && c.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public COLLECTIONS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
