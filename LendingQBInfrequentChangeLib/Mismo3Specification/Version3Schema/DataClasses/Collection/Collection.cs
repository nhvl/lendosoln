namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class COLLECTION
    {
        /// <summary>
        /// Gets a value indicating whether the COLLECTION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BorrowerCollectionResponseIndicatorSpecified
                    || this.CollectionContactDateSpecified
                    || this.CollectionContactMethodIdentifierSpecified
                    || this.CollectionContactMethodTypeOtherDescriptionSpecified
                    || this.CollectionContactMethodTypeSpecified
                    || this.CollectionContactProductiveIndicatorSpecified
                    || this.ExtensionSpecified
                    || this.PromiseToPayIndicatorSpecified
                    || this.RightPartyContactIndicatorSpecified;
            }
        }

        /// <summary>
        /// When true, indicates that there was a response from the borrower to the specified collection effort.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIndicator BorrowerCollectionResponseIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the BorrowerCollectionResponseIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BorrowerCollectionResponseIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool BorrowerCollectionResponseIndicatorSpecified
        {
            get { return BorrowerCollectionResponseIndicator != null; }
            set { }
        }

        /// <summary>
        /// The date on which the collection contact was made.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMODate CollectionContactDate;

        /// <summary>
        /// Gets or sets a value indicating whether the CollectionContactDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CollectionContactDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool CollectionContactDateSpecified
        {
            get { return CollectionContactDate != null; }
            set { }
        }

        /// <summary>
        /// A unique identifier of the collection contact method. The party assigning the identifier can be provided using the IdentifierOwnerURI.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOIdentifier CollectionContactMethodIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the CollectionContactMethodIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CollectionContactMethodIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool CollectionContactMethodIdentifierSpecified
        {
            get { return CollectionContactMethodIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Specifies the method used to make contact with the party in a collection effort.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOEnum<CollectionContactMethodBase> CollectionContactMethodType;

        /// <summary>
        /// Gets or sets a value indicating whether the CollectionContactMethodType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CollectionContactMethodType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CollectionContactMethodTypeSpecified
        {
            get { return this.CollectionContactMethodType != null && this.CollectionContactMethodType.enumValue != CollectionContactMethodBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected as the Collection Contact Method Type.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString CollectionContactMethodTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the CollectionContactMethodTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CollectionContactMethodTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool CollectionContactMethodTypeOtherDescriptionSpecified
        {
            get { return CollectionContactMethodTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that the collection contact was considered to be productive by the servicer or investor and results in an attempt to solve the delinquency.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOIndicator CollectionContactProductiveIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the CollectionContactProductiveIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CollectionContactProductiveIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool CollectionContactProductiveIndicatorSpecified
        {
            get { return CollectionContactProductiveIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that a promise to repay the debt was made as a result of the collection contact.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOIndicator PromiseToPayIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PromiseToPayIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PromiseToPayIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PromiseToPayIndicatorSpecified
        {
            get { return PromiseToPayIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that a contact was made with the borrower, co-borrower, trusted advisor, such as a housing counselor, or other authorized party that results in a conversation about resolving the loan delinquency.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOIndicator RightPartyContactIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the RightPartyContactIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RightPartyContactIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool RightPartyContactIndicatorSpecified
        {
            get { return RightPartyContactIndicator != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 8)]
        public COLLECTION_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
