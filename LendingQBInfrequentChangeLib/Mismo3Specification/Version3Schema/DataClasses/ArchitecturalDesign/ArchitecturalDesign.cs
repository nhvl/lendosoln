namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class ARCHITECTURAL_DESIGN
    {
        /// <summary>
        /// Gets a value indicating whether the ARCHITECTURAL_DESIGN container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ArchitecturalDesignCategoryTypeOtherDescriptionSpecified
                    || this.ArchitecturalDesignCategoryTypeSpecified
                    || this.ArchitecturalDesignConfigurationTypeOtherDescriptionSpecified
                    || this.ArchitecturalDesignConfigurationTypeSpecified
                    || this.ArchitecturalDesignDescriptionSpecified
                    || this.ComponentAdjustmentAmountSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// Category of architectural design that best fits the structure being described.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<ArchitecturalDesignCategoryBase> ArchitecturalDesignCategoryType;

        /// <summary>
        /// Gets or sets a value indicating whether the ArchitecturalDesignCategoryType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ArchitecturalDesignCategoryType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ArchitecturalDesignCategoryTypeSpecified
        {
            get { return this.ArchitecturalDesignCategoryType != null && this.ArchitecturalDesignCategoryType.enumValue != ArchitecturalDesignCategoryBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Architectural Design Category Type. 
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString ArchitecturalDesignCategoryTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ArchitecturalDesignCategoryTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ArchitecturalDesignCategoryTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ArchitecturalDesignCategoryTypeOtherDescriptionSpecified
        {
            get { return ArchitecturalDesignCategoryTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the configuration of the dwellings within the structure.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<ArchitecturalDesignConfigurationBase> ArchitecturalDesignConfigurationType;

        /// <summary>
        /// Gets or sets a value indicating whether the ArchitecturalDesignConfigurationType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ArchitecturalDesignConfigurationType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ArchitecturalDesignConfigurationTypeSpecified
        {
            get { return this.ArchitecturalDesignConfigurationType != null && this.ArchitecturalDesignConfigurationType.enumValue != ArchitecturalDesignConfigurationBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the item of the structure being rated if Other is selected as the Architectural Design Configuration Type.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString ArchitecturalDesignConfigurationTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ArchitecturalDesignConfigurationTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ArchitecturalDesignConfigurationTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ArchitecturalDesignConfigurationTypeOtherDescriptionSpecified
        {
            get { return ArchitecturalDesignConfigurationTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the architectural design of the structure.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString ArchitecturalDesignDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ArchitecturalDesignDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ArchitecturalDesignDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ArchitecturalDesignDescriptionSpecified
        {
            get { return ArchitecturalDesignDescription != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount (either positive or negative) adjustment being made for a specific component of the property.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOAmount ComponentAdjustmentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ComponentAdjustmentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ComponentAdjustmentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ComponentAdjustmentAmountSpecified
        {
            get { return ComponentAdjustmentAmount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 6)]
        public ARCHITECTURAL_DESIGN_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
