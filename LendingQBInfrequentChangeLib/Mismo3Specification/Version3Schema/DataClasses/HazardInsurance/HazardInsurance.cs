namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class HAZARD_INSURANCE
    {
        /// <summary>
        /// Gets a value indicating whether the container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.FloodContractFeeAmountSpecified
                    || this.HazardInsuranceCoverageAmountSpecified
                    || this.HazardInsuranceCoverageTypeOtherDescriptionSpecified
                    || this.HazardInsuranceCoverageTypeSpecified
                    || this.HazardInsuranceEffectiveDateSpecified
                    || this.HazardInsuranceEscrowedIndicatorSpecified
                    || this.HazardInsuranceExpirationDateSpecified
                    || this.HazardInsuranceNextPremiumDueDateSpecified
                    || this.HazardInsuranceNonStandardPolicyTypeOtherDescriptionSpecified
                    || this.HazardInsuranceNonStandardPolicyTypeSpecified
                    || this.HazardInsurancePolicyCancellationDateSpecified
                    || this.HazardInsurancePolicyIdentifierSpecified
                    || this.HazardInsurancePremiumAmountSpecified
                    || this.HazardInsurancePremiumMonthsCountSpecified
                    || this.HazardInsuranceReplacementValueIndicatorSpecified
                    || this.InsuranceRequiredIndicatorSpecified;
            }
        }

        /// <summary>
        /// The amount charged by the provider to obtain the Flood Contract associated with the subject property.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount FloodContractFeeAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the FloodContractFeeAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FloodContractFeeAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool FloodContractFeeAmountSpecified
        {
            get { return FloodContractFeeAmount != null; }
            set { }
        }

        /// <summary>
        /// The coverage amount of the property casualty policy on the property securing the loan.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOAmount HazardInsuranceCoverageAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the HazardInsuranceCoverageAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HazardInsuranceCoverageAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool HazardInsuranceCoverageAmountSpecified
        {
            get { return HazardInsuranceCoverageAmount != null; }
            set { }
        }

        /// <summary>
        /// The type of loss the insurance policy covers.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<HazardInsuranceCoverageBase> HazardInsuranceCoverageType;

        /// <summary>
        /// Gets or sets a value indicating whether the HazardInsuranceCoverageType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HazardInsuranceCoverageType element has been assigned a value.</value>
        [XmlIgnore]
        public bool HazardInsuranceCoverageTypeSpecified
        {
            get { return this.HazardInsuranceCoverageType != null && this.HazardInsuranceCoverageType.enumValue != HazardInsuranceCoverageBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture the hazard insurance coverage type if Other is selected as the hazard insurance coverage type.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString HazardInsuranceCoverageTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the HazardInsuranceCoverageTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HazardInsuranceCoverageTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool HazardInsuranceCoverageTypeOtherDescriptionSpecified
        {
            get { return HazardInsuranceCoverageTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The date the hazard insurance went into effect.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMODate HazardInsuranceEffectiveDate;

        /// <summary>
        /// Gets or sets a value indicating whether the HazardInsuranceEffectiveDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HazardInsuranceEffectiveDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool HazardInsuranceEffectiveDateSpecified
        {
            get { return HazardInsuranceEffectiveDate != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether hazard is paid by the borrower or the lender.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOIndicator HazardInsuranceEscrowedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the HazardInsuranceEscrowedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HazardInsuranceEscrowedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool HazardInsuranceEscrowedIndicatorSpecified
        {
            get { return HazardInsuranceEscrowedIndicator != null; }
            set { }
        }

        /// <summary>
        /// The date the property casualty policy expires.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMODate HazardInsuranceExpirationDate;

        /// <summary>
        /// Gets or sets a value indicating whether the HazardInsuranceExpirationDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HazardInsuranceExpirationDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool HazardInsuranceExpirationDateSpecified
        {
            get { return HazardInsuranceExpirationDate != null; }
            set { }
        }

        /// <summary>
        /// The month and year the next property casualty premium is due.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMODate HazardInsuranceNextPremiumDueDate;

        /// <summary>
        /// Gets or sets a value indicating whether the HazardInsuranceNextPremiumDueDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HazardInsuranceNextPremiumDueDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool HazardInsuranceNextPremiumDueDateSpecified
        {
            get { return HazardInsuranceNextPremiumDueDate != null; }
            set { }
        }

        /// <summary>
        /// Additional Description of the Hazard Insurance coverage document.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOEnum<HazardInsuranceNonStandardPolicyBase> HazardInsuranceNonStandardPolicyType;

        /// <summary>
        /// Gets or sets a value indicating whether the HazardInsuranceNonStandardPolicyType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HazardInsuranceNonStandardPolicyType element has been assigned a value.</value>
        [XmlIgnore]
        public bool HazardInsuranceNonStandardPolicyTypeSpecified
        {
            get { return this.HazardInsuranceNonStandardPolicyType != null && this.HazardInsuranceNonStandardPolicyType.enumValue != HazardInsuranceNonStandardPolicyBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture the Hazard Insurance Non Standard Policy Type if Other is selected as the Hazard Insurance Non Standard Policy Type.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOString HazardInsuranceNonStandardPolicyTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the HazardInsuranceNonStandardPolicyTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HazardInsuranceNonStandardPolicyTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool HazardInsuranceNonStandardPolicyTypeOtherDescriptionSpecified
        {
            get { return HazardInsuranceNonStandardPolicyTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The date the insurance company terminates the policy.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMODate HazardInsurancePolicyCancellationDate;

        /// <summary>
        /// Gets or sets a value indicating whether the HazardInsurancePolicyCancellationDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HazardInsurancePolicyCancellationDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool HazardInsurancePolicyCancellationDateSpecified
        {
            get { return HazardInsurancePolicyCancellationDate != null; }
            set { }
        }

        /// <summary>
        /// The number assigned to the policy by the insurance company.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOIdentifier HazardInsurancePolicyIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the HazardInsurancePolicyIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HazardInsurancePolicyIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool HazardInsurancePolicyIdentifierSpecified
        {
            get { return HazardInsurancePolicyIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The projected property casualty premium to be paid.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOAmount HazardInsurancePremiumAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the HazardInsurancePremiumAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HazardInsurancePremiumAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool HazardInsurancePremiumAmountSpecified
        {
            get { return HazardInsurancePremiumAmount != null; }
            set { }
        }

        /// <summary>
        /// The term expressed in months between each property casualty insurance premium.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOCount HazardInsurancePremiumMonthsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the HazardInsurancePremiumMonthsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HazardInsurancePremiumMonthsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool HazardInsurancePremiumMonthsCountSpecified
        {
            get { return HazardInsurancePremiumMonthsCount != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that the coverage provides full replacement value of the property.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOIndicator HazardInsuranceReplacementValueIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the HazardInsuranceReplacementValueIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HazardInsuranceReplacementValueIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool HazardInsuranceReplacementValueIndicatorSpecified
        {
            get { return HazardInsuranceReplacementValueIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that a specific Hazard Insurance Coverage Type on the property is required for the loan transaction.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOIndicator InsuranceRequiredIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the InsuranceRequiredIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InsuranceRequiredIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool InsuranceRequiredIndicatorSpecified
        {
            get { return InsuranceRequiredIndicator != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 16)]
        public HAZARD_INSURANCE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
