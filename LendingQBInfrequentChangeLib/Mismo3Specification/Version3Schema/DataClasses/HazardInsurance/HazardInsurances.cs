namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class HAZARD_INSURANCES
    {
        /// <summary>
        /// Gets a value indicating whether the HAZARD_INSURANCES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.HazardInsuranceSpecified;
            }
        }

        /// <summary>
        /// A collection of hazard insurances.
        /// </summary>
        [XmlElement("HAZARD_INSURANCE", Order = 0)]
		public List<HAZARD_INSURANCE> HazardInsurance = new List<HAZARD_INSURANCE>();

        /// <summary>
        /// Gets or sets a value indicating whether the HazardInsurance element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HazardInsurance element has been assigned a value.</value>
        [XmlIgnore]
        public bool HazardInsuranceSpecified
        {
            get { return this.HazardInsurance != null && this.HazardInsurance.Count(h => h != null && h.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public HAZARD_INSURANCES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
