namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class TITLE_ADDITIONAL_REQUIREMENTS
    {
        /// <summary>
        /// Gets a value indicating whether the TITLE_ADDITIONAL_REQUIREMENTS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.TitleAdditionalRequirementSpecified;
            }
        }

        /// <summary>
        /// A collection of title additional requirements.
        /// </summary>
        [XmlElement("TITLE_ADDITIONAL_REQUIREMENT", Order = 0)]
		public List<TITLE_ADDITIONAL_REQUIREMENT> TitleAdditionalRequirement = new List<TITLE_ADDITIONAL_REQUIREMENT>();

        /// <summary>
        /// Gets or sets a value indicating whether the TitleAdditionalRequirement element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TitleAdditionalRequirement element has been assigned a value.</value>
        [XmlIgnore]
        public bool TitleAdditionalRequirementSpecified
        {
            get { return this.TitleAdditionalRequirement != null && this.TitleAdditionalRequirement.Count(t => t != null && t.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public TITLE_ADDITIONAL_REQUIREMENTS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
