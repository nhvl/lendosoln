namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class LOAN_PROGRAMS
    {
        /// <summary>
        /// Gets a value indicating whether the LOAN_PROGRAMS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.LoanProgramSpecified;
            }
        }

        /// <summary>
        /// A collection of loan programs.
        /// </summary>
        [XmlElement("LOAN_PROGRAM", Order = 0)]
        public List<LOAN_PROGRAM> LoanProgram = new List<LOAN_PROGRAM>();

        /// <summary>
        /// Gets or sets a value indicating whether the LoanProgram element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanProgram element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanProgramSpecified
        {
            get { return this.LoanProgram != null && this.LoanProgram.Count(l => l != null && l.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public LOAN_PROGRAMS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
