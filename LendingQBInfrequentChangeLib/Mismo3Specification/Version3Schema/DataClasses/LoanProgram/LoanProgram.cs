namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class LOAN_PROGRAM
    {
        /// <summary>
        /// Gets a value indicating whether the LOAN_PROGRAM container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.LoanProgramAffordableIndicatorSpecified
                    || this.LoanProgramIdentifierSpecified
                    || this.LoanProgramSponsoringEntityNameSpecified;
            }
        }

        /// <summary>
        /// When true, indicates that the loan program is classified as an affordable program.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIndicator LoanProgramAffordableIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanProgramAffordableIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanProgramAffordableIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanProgramAffordableIndicatorSpecified
        {
            get { return LoanProgramAffordableIndicator != null; }
            set { }
        }

        /// <summary>
        /// Identifies the mortgage program associated with the loan as defined by a specific entity.  Entity may be referenced using OwnerIdentifierURI or "Loan Program Sponsoring Entity Name".
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOIdentifier LoanProgramIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanProgramIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanProgramIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanProgramIdentifierSpecified
        {
            get { return LoanProgramIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The name of the entity that defines the loan program.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString LoanProgramSponsoringEntityName;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanProgramSponsoringEntityName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanProgramSponsoringEntityName element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanProgramSponsoringEntityNameSpecified
        {
            get { return LoanProgramSponsoringEntityName != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public LOAN_PROGRAM_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
