namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class ASSUMABILITY
    {
        /// <summary>
        /// Get a value indicating whether the ASSUMABILITY container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AssumabilityOccurrencesSpecified
                    || this.AssumabilityRuleSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// Occurrences of a loan assumption.
        /// </summary>
        [XmlElement("ASSUMABILITY_OCCURRENCES", Order = 0)]
        public ASSUMABILITY_OCCURRENCES AssumabilityOccurrences;

        /// <summary>
        /// Gets or sets a value indicating whether the Assumption Occurrences element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Assumption Occurrences element has been assigned a value.</value>
        [XmlIgnore]
        public bool AssumabilityOccurrencesSpecified
        {
            get { return this.AssumabilityOccurrences != null && this.AssumabilityOccurrences.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Rules on how a loan assumption can be handled.
        /// </summary>
        [XmlElement("ASSUMABILITY_RULE", Order = 1)]
        public ASSUMABILITY_RULE AssumabilityRule;

        /// <summary>
        /// Gets or sets a value indicating whether the Assumption Rule element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Assumption Rule element has been assigned a value.</value>
        [XmlIgnore]
        public bool AssumabilityRuleSpecified
        {
            get { return this.AssumabilityRule != null && this.AssumabilityRule.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public ASSUMABILITY_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
