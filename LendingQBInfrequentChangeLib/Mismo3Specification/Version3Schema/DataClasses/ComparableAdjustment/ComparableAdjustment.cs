namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class COMPARABLE_ADJUSTMENT
    {
        /// <summary>
        /// Gets a value indicating whether the COMPARABLE_ADJUSTMENT container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ComparableAdjustmentAmountSpecified
                    || this.ComparableAdjustmentDescriptionSpecified
                    || this.ComparableAdjustmentTypeOtherDescriptionSpecified
                    || this.ComparableAdjustmentTypeSpecified
                    || this.ExtensionSpecified
                    || this.SalesComparisonUnitOfMeasureDescriptionSpecified;
            }
        }

        /// <summary>
        /// The dollar amount of the adjustment applied to the price for the property feature identified by the Comparable Adjustment Type.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount ComparableAdjustmentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ComparableAdjustmentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ComparableAdjustmentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ComparableAdjustmentAmountSpecified
        {
            get { return ComparableAdjustmentAmount != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to further describe a property feature which is identified in the Comparable Adjustment Type.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString ComparableAdjustmentDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ComparableAdjustmentDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ComparableAdjustmentDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ComparableAdjustmentDescriptionSpecified
        {
            get { return ComparableAdjustmentDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the type of price adjustment that is made when when comparing to sales and listings to value a property. Each type corresponds to a feature of the property. E.g. Condition, age, view, etc.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<ComparableAdjustmentBase> ComparableAdjustmentType;

        /// <summary>
        /// Gets or sets a value indicating whether the ComparableAdjustmentType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ComparableAdjustmentType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ComparableAdjustmentTypeSpecified
        {
            get { return this.ComparableAdjustmentType != null && this.ComparableAdjustmentType.enumValue != ComparableAdjustmentBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture additional information when other is selected for Comparable Price Adjustment Type.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString ComparableAdjustmentTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ComparableAdjustmentTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ComparableAdjustmentTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ComparableAdjustmentTypeOtherDescriptionSpecified
        {
            get { return ComparableAdjustmentTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the unit of measure used in the Sales Comparison approach when comparing vacant land properties. (e.g. $ per lakefront acre; $ per wooded acre.).
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString SalesComparisonUnitOfMeasureDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the SalesComparisonUnitOfMeasureDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SalesComparisonUnitOfMeasureDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool SalesComparisonUnitOfMeasureDescriptionSpecified
        {
            get { return SalesComparisonUnitOfMeasureDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 5)]
        public COMPARABLE_ADJUSTMENT_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
