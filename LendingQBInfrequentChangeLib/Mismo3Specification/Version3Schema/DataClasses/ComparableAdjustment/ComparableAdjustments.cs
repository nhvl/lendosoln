namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class COMPARABLE_ADJUSTMENTS
    {
        /// <summary>
        /// Gets a value indicating whether the COMPARABLE_ADJUSTMENTS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ComparableAdjustmentSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of comparable adjustments.
        /// </summary>
        [XmlElement("COMPARABLE_ADJUSTMENT", Order = 0)]
		public List<COMPARABLE_ADJUSTMENT> ComparableAdjustment = new List<COMPARABLE_ADJUSTMENT>();

        /// <summary>
        /// Gets or sets a value indicating whether the ComparableAdjustment element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ComparableAdjustment element has been assigned a value.</value>
        [XmlIgnore]
        public bool ComparableAdjustmentSpecified
        {
            get { return this.ComparableAdjustment != null && this.ComparableAdjustment.Count(c => c != null && c.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public COMPARABLE_ADJUSTMENTS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
