namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class EXPENSE_CLAIMS
    {
        /// <summary>
        /// Gets a value indicating whether the EXPENSE_CLAIMS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExpenseClaimSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of expense claims.
        /// </summary>
        [XmlElement("EXPENSE_CLAIM", Order = 0)]
		public List<EXPENSE_CLAIM> ExpenseClaim = new List<EXPENSE_CLAIM>();

        /// <summary>
        /// Gets or sets a value indicating whether the ExpenseClaim element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ExpenseClaim element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExpenseClaimSpecified
        {
            get { return this.ExpenseClaim != null && this.ExpenseClaim.Count(e => e != null && e.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public EXPENSE_CLAIMS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
