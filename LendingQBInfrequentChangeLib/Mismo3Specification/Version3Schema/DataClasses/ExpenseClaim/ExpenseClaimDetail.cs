namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class EXPENSE_CLAIM_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the EXPENSE_CLAIM_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExpenseClaimCommentTextSpecified
                    || this.ExpenseClaimFormIdentifierSpecified
                    || this.ExpenseClaimIdentifierSpecified
                    || this.ExpenseClaimSubmissionIdentifierSpecified
                    || this.ExpenseClaimSubmissionReasonTypeOtherDescriptionSpecified
                    || this.ExpenseClaimSubmissionReasonTypeSpecified
                    || this.ExpenseClaimSubmissionTypeOtherDescriptionSpecified
                    || this.ExpenseClaimSubmissionTypeSpecified
                    || this.ExpenseClaimTotalAmountSpecified
                    || this.ExpenseClaimTotalNonReimbursableAmountSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A free-form text field used to describe or comment on some aspect of the expense claim.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString ExpenseClaimCommentText;

        /// <summary>
        /// Gets or sets a value indicating whether the ExpenseClaimCommentText element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ExpenseClaimCommentText element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExpenseClaimCommentTextSpecified
        {
            get { return ExpenseClaimCommentText != null; }
            set { }
        }

        /// <summary>
        /// A unique identifier assigned by a party to specify an expense claim form that is used to request reimbursement. The party assigning the identifier should be provided using the IdentifierOwnerURI.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOIdentifier ExpenseClaimFormIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the ExpenseClaimFormIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ExpenseClaimFormIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExpenseClaimFormIdentifierSpecified
        {
            get { return ExpenseClaimFormIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A unique identifier for the expense claim. The party assigning the identifier should be provided using the IdentifierOwnerURI.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOIdentifier ExpenseClaimIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the ExpenseClaimIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ExpenseClaimIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExpenseClaimIdentifierSpecified
        {
            get { return ExpenseClaimIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A unique identifier assigned by a party to an expense claim submission. The party assigning the identifier can be provided using the IdentifierOwnerURI.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOIdentifier ExpenseClaimSubmissionIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the ExpenseClaimSubmissionIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ExpenseClaimSubmissionIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExpenseClaimSubmissionIdentifierSpecified
        {
            get { return ExpenseClaimSubmissionIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Specifies the reason for the expense claim submission.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<ExpenseClaimSubmissionReasonBase> ExpenseClaimSubmissionReasonType;

        /// <summary>
        /// Gets or sets a value indicating whether the ExpenseClaimSubmissionReasonType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ExpenseClaimSubmissionReasonType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExpenseClaimSubmissionReasonTypeSpecified
        {
            get { return this.ExpenseClaimSubmissionReasonType != null && this.ExpenseClaimSubmissionReasonType.enumValue != ExpenseClaimSubmissionReasonBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected as the Expense Claim Submission Reason Type.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString ExpenseClaimSubmissionReasonTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ExpenseClaimSubmissionReasonTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ExpenseClaimSubmissionReasonTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExpenseClaimSubmissionReasonTypeOtherDescriptionSpecified
        {
            get { return ExpenseClaimSubmissionReasonTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the occurrence of the claim being submitted.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOEnum<ExpenseClaimSubmissionBase> ExpenseClaimSubmissionType;

        /// <summary>
        /// Gets or sets a value indicating whether the ExpenseClaimSubmissionType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ExpenseClaimSubmissionType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExpenseClaimSubmissionTypeSpecified
        {
            get { return this.ExpenseClaimSubmissionType != null && this.ExpenseClaimSubmissionType.enumValue != ExpenseClaimSubmissionBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected as the Expense Claim Submission Type.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOString ExpenseClaimSubmissionTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ExpenseClaimSubmissionTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ExpenseClaimSubmissionTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExpenseClaimSubmissionTypeOtherDescriptionSpecified
        {
            get { return ExpenseClaimSubmissionTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Total amount of all line items in the claim.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOAmount ExpenseClaimTotalAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ExpenseClaimTotalAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ExpenseClaimTotalAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExpenseClaimTotalAmountSpecified
        {
            get { return ExpenseClaimTotalAmount != null; }
            set { }
        }

        /// <summary>
        /// Total amount of all non reimbursable items associated with the claim.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOAmount ExpenseClaimTotalNonReimbursableAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ExpenseClaimTotalNonReimbursableAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ExpenseClaimTotalNonReimbursableAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExpenseClaimTotalNonReimbursableAmountSpecified
        {
            get { return ExpenseClaimTotalNonReimbursableAmount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 10)]
        public EXPENSE_CLAIM_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
