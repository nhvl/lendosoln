namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class AMORTIZATION_SCHEDULE_ITEM
    {
        /// <summary>
        /// Gets a value indicating whether the AMORTIZATION_SCHEDULE_ITEM container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AmortizationScheduleEndingBalanceAmountSpecified
                    || this.AmortizationScheduleInterestPortionAmountSpecified
                    || this.AmortizationScheduleInterestRatePercentSpecified
                    || this.AmortizationScheduleLoanToValuePercentSpecified
                    || this.AmortizationScheduleMIPaymentAmountSpecified
                    || this.AmortizationSchedulePaymentAmountSpecified
                    || this.AmortizationSchedulePaymentDueDateSpecified
                    || this.AmortizationSchedulePaymentNumberSpecified
                    || this.AmortizationSchedulePrincipalPortionAmountSpecified
                    || this.AmortizationScheduleUSDAAnnualFeePaymentAmountSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// The remaining balance due on the loan as calculated after application of the payment amount reflected on the Amortization Schedule for the loan.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount AmortizationScheduleEndingBalanceAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the AmortizationScheduleEndingBalanceAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AmortizationScheduleEndingBalanceAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool AmortizationScheduleEndingBalanceAmountSpecified
        {
            get { return AmortizationScheduleEndingBalanceAmount != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of the portion of the total payment amount which is applied to the interest due on the loan as reflected on the Amortization Schedule for the loan.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOAmount AmortizationScheduleInterestPortionAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the AmortizationScheduleInterestPortionAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AmortizationScheduleInterestPortionAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool AmortizationScheduleInterestPortionAmountSpecified
        {
            get { return AmortizationScheduleInterestPortionAmount != null; }
            set { }
        }

        /// <summary>
        /// The interest rate percent of the loan used to calculate the principal and interest or dollar amount of the payment reflected on the Amortization Schedule for the loan.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOPercent AmortizationScheduleInterestRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the AmortizationScheduleInterestRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AmortizationScheduleInterestRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool AmortizationScheduleInterestRatePercentSpecified
        {
            get { return AmortizationScheduleInterestRatePercent != null; }
            set { }
        }

        /// <summary>
        /// The loan to value percent resulting from the application of the payment reflected on the Amortization Schedule for the loan.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOPercent AmortizationScheduleLoanToValuePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the AmortizationScheduleLoanToValuePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AmortizationScheduleLoanToValuePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool AmortizationScheduleLoanToValuePercentSpecified
        {
            get { return AmortizationScheduleLoanToValuePercent != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of the MI payment due in addition to the payment amount reflected on the Amortization Schedule for the loan.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOAmount AmortizationScheduleMIPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the AmortizationScheduleMIPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AmortizationScheduleMIPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool AmortizationScheduleMIPaymentAmountSpecified
        {
            get { return AmortizationScheduleMIPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of a single payment reflected on the Amortization Schedule for the loan.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOAmount AmortizationSchedulePaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the AmortizationSchedulePaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AmortizationSchedulePaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool AmortizationSchedulePaymentAmountSpecified
        {
            get { return AmortizationSchedulePaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// The due date of a single payment reflected on the Amortization Schedule for the loan.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMODate AmortizationSchedulePaymentDueDate;

        /// <summary>
        /// Gets or sets a value indicating whether the AmortizationSchedulePaymentDueDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AmortizationSchedulePaymentDueDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool AmortizationSchedulePaymentDueDateSpecified
        {
            get { return AmortizationSchedulePaymentDueDate != null; }
            set { }
        }

        /// <summary>
        /// The number used to identify a single payment within a sequence of payments reflected on the Amortization Schedule for the loan.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMONumeric AmortizationSchedulePaymentNumber;

        /// <summary>
        /// Gets or sets a value indicating whether the AmortizationSchedulePaymentNumber element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AmortizationSchedulePaymentNumber element has been assigned a value.</value>
        [XmlIgnore]
        public bool AmortizationSchedulePaymentNumberSpecified
        {
            get { return AmortizationSchedulePaymentNumber != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of the portion of the payment amount which is applied to the principal due on the loan as reflected on the Amortization Schedule for the loan.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOAmount AmortizationSchedulePrincipalPortionAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the AmortizationSchedulePrincipalPortionAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AmortizationSchedulePrincipalPortionAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool AmortizationSchedulePrincipalPortionAmountSpecified
        {
            get { return AmortizationSchedulePrincipalPortionAmount != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of the USDA Annual Fee payment due in addition to the payment amount reflected on the Amortization Schedule for the loan.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOAmount AmortizationScheduleUSDAAnnualFeePaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the AmortizationScheduleUSDAAnnualFeePaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AmortizationScheduleUSDAAnnualFeePaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool AmortizationScheduleUSDAAnnualFeePaymentAmountSpecified
        {
            get { return AmortizationScheduleUSDAAnnualFeePaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 10)]
        public AMORTIZATION_SCHEDULE_ITEM_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
