namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class AMORTIZATION_SCHEDULE_ITEMS
    {
        /// <summary>
        /// Gets a value indicating whether the AMORTIZATION_SCHEDULE_ITEMS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AmortizationScheduleItemSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of amortization schedule items.
        /// </summary>
        [XmlElement("AMORTIZATION_SCHEDULE_ITEM", Order = 0)]
		public List<AMORTIZATION_SCHEDULE_ITEM> AmortizationScheduleItem = new List<AMORTIZATION_SCHEDULE_ITEM>();

        /// <summary>
        /// Gets or sets a value indicating whether the AmortizationScheduleItem element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AmortizationScheduleItem element has been assigned a value.</value>
        [XmlIgnore]
        public bool AmortizationScheduleItemSpecified
        {
            get { return this.AmortizationScheduleItem != null && this.AmortizationScheduleItem.Count(i => i != null && i.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public AMORTIZATION_SCHEDULE_ITEMS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
