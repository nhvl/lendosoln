namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class ALLONGE_TO_NOTE
    {
        /// <summary>
        /// Gets a value indicating whether the ALLONGE_TO_NOTE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AllongeToNoteDateSpecified
                    || this.AllongeToNoteExecutedByDescriptionSpecified
                    || this.AllongeToNoteInFavorOfDescriptionSpecified
                    || this.AllongeToNotePayToTheOrderOfDescriptionSpecified
                    || this.AllongeToNoteWithoutRecourseDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// The date of execution.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMODate AllongeToNoteDate;

        /// <summary>
        /// Gets or sets a value indicating whether the NoteDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NoteDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool AllongeToNoteDateSpecified
        {
            get { return AllongeToNoteDate != null; }
            set { }
        }

        /// <summary>
        /// Name of current note holder executing, also executing the assignment of the security instrument.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString AllongeToNoteExecutedByDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the NoteExecutedByDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NoteExecutedByDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool AllongeToNoteExecutedByDescriptionSpecified
        {
            get { return AllongeToNoteExecutedByDescription != null; }
            set { }
        }

        /// <summary>
        /// The party identified as endorsee, typically the new note holder.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString AllongeToNoteInFavorOfDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the NoteInFavorOfDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NoteInFavorOfDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool AllongeToNoteInFavorOfDescriptionSpecified
        {
            get { return AllongeToNoteInFavorOfDescription != null; }
            set { }
        }

        /// <summary>
        /// The name of new note holder identified.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString AllongeToNotePayToTheOrderOfDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the NotePayToTheOrderOfDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NotePayToTheOrderOfDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool AllongeToNotePayToTheOrderOfDescriptionSpecified
        {
            get { return AllongeToNotePayToTheOrderOfDescription != null; }
            set { }
        }

        /// <summary>
        /// Description for section pertaining to recourse and without recourse typically note holders name and address.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString AllongeToNoteWithoutRecourseDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the note without recourse element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the note without recourse element has been assigned a value.</value>
        [XmlIgnore]
        public bool AllongeToNoteWithoutRecourseDescriptionSpecified
        {
            get { return AllongeToNoteWithoutRecourseDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 5)]
        public ALLONGE_TO_NOTE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
