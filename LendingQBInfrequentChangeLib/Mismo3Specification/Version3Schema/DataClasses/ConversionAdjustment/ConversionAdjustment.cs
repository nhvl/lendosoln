namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CONVERSION_ADJUSTMENT
    {
        /// <summary>
        /// Gets a value indicating whether the CONVERSION_ADJUSTMENT container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ConversionAdjustmentLifetimeAdjustmentRuleSpecified
                    || this.ConversionOptionPeriodAdjustmentRulesSpecified
                    || this.ExtensionSpecified
                    || this.IndexRulesSpecified;
            }
        }


        /// <summary>
        /// Lifetime adjustment rule for the conversion option.
        /// </summary>
        [XmlElement("CONVERSION_ADJUSTMENT_LIFETIME_ADJUSTMENT_RULE", Order = 0)]
        public CONVERSION_ADJUSTMENT_LIFETIME_ADJUSTMENT_RULE ConversionAdjustmentLifetimeAdjustmentRule;

        /// <summary>
        /// Gets or sets a value indicating whether the ConversionAdjustmentLifetimeAdjustmentRule element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConversionAdjustmentLifetimeAdjustmentRule element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConversionAdjustmentLifetimeAdjustmentRuleSpecified
        {
            get { return this.ConversionAdjustmentLifetimeAdjustmentRule != null && this.ConversionAdjustmentLifetimeAdjustmentRule.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Periodic adjustment rules for the conversion option.
        /// </summary>
        [XmlElement("CONVERSION_OPTION_PERIOD_ADJUSTMENT_RULES", Order = 1)]
        public CONVERSION_OPTION_PERIOD_ADJUSTMENT_RULES ConversionOptionPeriodAdjustmentRules;

        /// <summary>
        /// Gets or sets a value indicating whether the ConversionOptionPeriodAdjustmentRules element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConversionOptionPeriodAdjustmentRules element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConversionOptionPeriodAdjustmentRulesSpecified
        {
            get { return this.ConversionOptionPeriodAdjustmentRules != null && this.ConversionOptionPeriodAdjustmentRules.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Rules related to the index.
        /// </summary>
        [XmlElement("INDEX_RULES", Order = 2)]
        public INDEX_RULES IndexRules;

        /// <summary>
        /// Gets or sets a value indicating whether the IndexRules element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IndexRules element has been assigned a value.</value>
        [XmlIgnore]
        public bool IndexRulesSpecified
        {
            get { return this.IndexRules != null && this.IndexRules.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public CONVERSION_ADJUSTMENT_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
