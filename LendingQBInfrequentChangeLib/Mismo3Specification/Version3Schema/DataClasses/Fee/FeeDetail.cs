namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class FEE_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the FEE_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BorrowerChosenProviderIndicatorSpecified
                    || this.ExtensionSpecified
                    || this.FeeActualTotalAmountSpecified
                    || this.FeeDescriptionSpecified
                    || this.FeeEstimatedTotalAmountSpecified
                    || this.FeePaidToTypeOtherDescriptionSpecified
                    || this.FeePaidToTypeSpecified
                    || this.FeePercentBasisTypeOtherDescriptionSpecified
                    || this.FeePercentBasisTypeSpecified
                    || this.FeeSpecifiedFixedAmountSpecified
                    || this.FeeSpecifiedHUD1LineNumberValueSpecified
                    || this.FeeSpecifiedLineNumberValueSpecified
                    || this.FeeTotalPercentSpecified
                    || this.FeeTypeOtherDescriptionSpecified
                    || this.FeeTypeSpecified
                    || this.GFEAggregationTypeOtherDescriptionSpecified
                    || this.GFEAggregationTypeSpecified
                    || this.GFEDisclosedFeeAmountSpecified
                    || this.IntegratedDisclosureLineNumberValueSpecified
                    || this.IntegratedDisclosureSectionTypeOtherDescriptionSpecified
                    || this.IntegratedDisclosureSectionTypeSpecified
                    || this.IntegratedDisclosureSubsectionTypeOtherDescriptionSpecified
                    || this.IntegratedDisclosureSubsectionTypeSpecified
                    || this.OptionalCostIndicatorSpecified
                    || this.RegulationZPointsAndFeesIndicatorSpecified
                    || this.RequiredProviderOfServiceIndicatorSpecified
                    || this.SectionClassificationTypeSpecified;
            }
        }

        /// <summary>
        /// When true, indicates the provider of the service for which the particular fee or payment is charged was selected by the borrower independent of the lender or lender-provided list.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIndicator BorrowerChosenProviderIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the BorrowerChosenProviderIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BorrowerChosenProviderIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool BorrowerChosenProviderIndicatorSpecified
        {
            get { return BorrowerChosenProviderIndicator != null; }
            set { }
        }

        /// <summary>
        /// The actual total dollar amount paid for the fee by all parties.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOAmount FeeActualTotalAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the FeeActualTotalAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeeActualTotalAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeeActualTotalAmountSpecified
        {
            get { return FeeActualTotalAmount != null; }
            set { }
        }

        /// <summary>
        /// The description of the fee.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString FeeDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the FeeDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeeDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeeDescriptionSpecified
        {
            get { return FeeDescription != null; }
            set { }
        }

        /// <summary>
        /// The total estimated dollar amount to be paid for the fee by all parties.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOAmount FeeEstimatedTotalAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the FeeEstimatedTotalAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeeEstimatedTotalAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeeEstimatedTotalAmountSpecified
        {
            get { return FeeEstimatedTotalAmount != null; }
            set { }
        }

        /// <summary>
        /// Identifies the category or type of payee to which the fee or payment will be paid.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<FeePaidToBase> FeePaidToType;

        /// <summary>
        /// Gets or sets a value indicating whether the FeePaidToType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeePaidToType element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeePaidToTypeSpecified
        {
            get { return this.FeePaidToType != null && this.FeePaidToType.enumValue != FeePaidToBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field to capture the description when Other is selected as Fee Paid To Type.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString FeePaidToTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the FeePaidToTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeePaidToTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeePaidToTypeOtherDescriptionSpecified
        {
            get { return FeePaidToTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Defines the amount upon which the Fee Total Percent is calculated.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOEnum<FeePercentBasisBase> FeePercentBasisType;

        /// <summary>
        /// Gets or sets a value indicating whether the FeePercentBasisType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeePercentBasisType element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeePercentBasisTypeSpecified
        {
            get { return this.FeePercentBasisType != null && this.FeePercentBasisType.enumValue != FeePercentBasisBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Fee Percent Basis Type.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOString FeePercentBasisTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the FeePercentBasisTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeePercentBasisTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeePercentBasisTypeOtherDescriptionSpecified
        {
            get { return FeePercentBasisTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The specified fixed dollar portion of the Fee Total Amount. This is used when the Fee Total Amount is made up of a percentage (stated in the Fee Total Percent) plus a specified fixed dollar amount.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOAmount FeeSpecifiedFixedAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the FeeSpecifiedFixedAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeeSpecifiedFixedAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeeSpecifiedFixedAmountSpecified
        {
            get { return FeeSpecifiedFixedAmount != null; }
            set { }
        }

        /// <summary>
        /// HUD-1 Line Number for fee items that pertain to miscellaneous sections of the HUD-1 document where there is not a one-to-one mapping between the line number and the line description.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOValue FeeSpecifiedHUD1LineNumberValue;

        /// <summary>
        /// Gets or sets a value indicating whether the FeeSpecifiedHUD1LineNumberValue element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeeSpecifiedHUD1LineNumberValue element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeeSpecifiedHUD1LineNumberValueSpecified
        {
            get { return FeeSpecifiedHUD1LineNumberValue != null; }
            set { }
        }

        /// <summary>
        /// The numeric or alphanumeric value used to identify the placement, location or sequence of fee information.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOString FeeSpecifiedLineNumberValue;

        /// <summary>
        /// Gets or sets a value indicating whether the FeeSpecifiedLineNumberValue element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeeSpecifiedLineNumberValue element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeeSpecifiedLineNumberValueSpecified
        {
            get { return FeeSpecifiedLineNumberValue != null; }
            set { }
        }

        /// <summary>
        /// The total percent paid by all parties for the specified Fee or Specified HUD Line Number. This is used in conjunction with the Fee Percent Basis Type to determine a dollar amount of the fee. May be used in conjunction with Specified Fixed Amount to determine the dollar amount of the fee. To be used when the fee is defined by a percent.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOPercent FeeTotalPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the FeeTotalPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeeTotalPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeeTotalPercentSpecified
        {
            get { return FeeTotalPercent != null; }
            set { }
        }

        /// <summary>
        /// Specifies the general type of fees associated with a real estate finance transaction.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOEnum<FeeBase> FeeType;

        /// <summary>
        /// Gets or sets a value indicating whether the FeeType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeeType element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeeTypeSpecified
        {
            get { return this.FeeType != null && this.FeeType.enumValue != FeeBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Fee Type.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOString FeeTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the FeeTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeeTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeeTypeOtherDescriptionSpecified
        {
            get { return FeeTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the fee or charges group, combined fee or charges category, or compensation category, to which the individual fee or charge should be associated, as reflected on a single instance of the GFE.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOEnum<GFEAggregationBase> GFEAggregationType;

        /// <summary>
        /// Gets or sets a value indicating whether the GFEAggregationType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GFEAggregationType element has been assigned a value.</value>
        [XmlIgnore]
        public bool GFEAggregationTypeSpecified
        {
            get { return this.GFEAggregationType != null && this.GFEAggregationType.enumValue != GFEAggregationBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for GFE Aggregation Type.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOString GFEAggregationTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the GFEAggregationTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GFEAggregationTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool GFEAggregationTypeOtherDescriptionSpecified
        {
            get { return GFEAggregationTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount, as disclosed on a single instance of the GFE, for the specified RESPA Fee or Specified HUD Line Number. 
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMOAmount GFEDisclosedFeeAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the GFEDisclosedFeeAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GFEDisclosedFeeAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool GFEDisclosedFeeAmountSpecified
        {
            get { return GFEDisclosedFeeAmount != null; }
            set { }
        }

        /// <summary>
        /// The line identifier for the item as represented in a section of the Integrated Disclosure form.
        /// </summary>
        [XmlElement(Order = 17)]
        public MISMOValue IntegratedDisclosureLineNumberValue;

        /// <summary>
        /// Gets or sets a value indicating whether the IntegratedDisclosureLineNumberValue element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IntegratedDisclosureLineNumberValue element has been assigned a value.</value>
        [XmlIgnore]
        public bool IntegratedDisclosureLineNumberValueSpecified
        {
            get { return IntegratedDisclosureLineNumberValue != null; }
            set { }
        }

        /// <summary>
        /// The title or description used to identify a primary section of the integrated disclosure document.
        /// </summary>
        [XmlElement(Order = 18)]
        public MISMOEnum<IntegratedDisclosureSectionBase> IntegratedDisclosureSectionType;

        /// <summary>
        /// Gets or sets a value indicating whether the IntegratedDisclosureSectionType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IntegratedDisclosureSectionType element has been assigned a value.</value>
        [XmlIgnore]
        public bool IntegratedDisclosureSectionTypeSpecified
        {
            get { return this.IntegratedDisclosureSectionType != null && this.IntegratedDisclosureSectionType.enumValue != IntegratedDisclosureSectionBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture additional information when Other is selected for Integrated Disclosure Section Type.
        /// </summary>
        [XmlElement(Order = 19)]
        public MISMOString IntegratedDisclosureSectionTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the IntegratedDisclosureSectionTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IntegratedDisclosureSectionTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool IntegratedDisclosureSectionTypeOtherDescriptionSpecified
        {
            get { return IntegratedDisclosureSectionTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The title or description used to identify a secondary or subsection of the integrated disclosure document.
        /// </summary>
        [XmlElement(Order = 20)]
        public MISMOEnum<IntegratedDisclosureSubsectionBase> IntegratedDisclosureSubsectionType;

        /// <summary>
        /// Gets or sets a value indicating whether the IntegratedDisclosureSubsectionType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IntegratedDisclosureSubsectionType element has been assigned a value.</value>
        [XmlIgnore]
        public bool IntegratedDisclosureSubsectionTypeSpecified
        {
            get { return this.IntegratedDisclosureSubsectionType != null && this.IntegratedDisclosureSubsectionType.enumValue != IntegratedDisclosureSubsectionBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture additional information when Other is selected for Integrated Disclosure Subsection Type.
        /// </summary>
        [XmlElement(Order = 21)]
        public MISMOString IntegratedDisclosureSubsectionTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the IntegratedDisclosureSubsectionTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IntegratedDisclosureSubsectionTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool IntegratedDisclosureSubsectionTypeOtherDescriptionSpecified
        {
            get { return IntegratedDisclosureSubsectionTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that the service and associated cost are not required by the creditor. This includes premiums paid for separate insurance, warranty, guarantee or event-coverage products.
        /// </summary>
        [XmlElement(Order = 22)]
        public MISMOIndicator OptionalCostIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the OptionalCostIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OptionalCostIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool OptionalCostIndicatorSpecified
        {
            get { return OptionalCostIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that item is included in the Regulation Z Points and Fees calculation.
        /// </summary>
        [XmlElement(Order = 23)]
        public MISMOIndicator RegulationZPointsAndFeesIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the RegulationZPointsAndFeesIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RegulationZPointsAndFeesIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool RegulationZPointsAndFeesIndicatorSpecified
        {
            get { return RegulationZPointsAndFeesIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates a required Provider of Service is associated with this fee or payment.
        /// </summary>
        [XmlElement(Order = 24)]
        public MISMOIndicator RequiredProviderOfServiceIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the RequiredProviderOfServiceIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RequiredProviderOfServiceIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool RequiredProviderOfServiceIndicatorSpecified
        {
            get { return RequiredProviderOfServiceIndicator != null; }
            set { }
        }

        /// <summary>
        /// The section number on the Good Faith Estimate/HUD-1 associated with a fee type.
        /// </summary>
        [XmlElement(Order = 25)]
        public MISMOEnum<SectionClassificationBase> SectionClassificationType;

        /// <summary>
        /// Gets or sets a value indicating whether the SectionClassificationType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SectionClassificationType element has been assigned a value.</value>
        [XmlIgnore]
        public bool SectionClassificationTypeSpecified
        {
            get { return this.SectionClassificationType != null && this.SectionClassificationType.enumValue != SectionClassificationBase.Blank; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 26)]
        public FEE_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null && Extension.ShouldSerialize; }
            set { }
        }
    }
}
