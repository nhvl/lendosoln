namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class FEE
    {
        /// <summary>
        /// Gets a value indicating whether the FEE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.FeeDetailSpecified
                    || this.FeePaidToSpecified
                    || this.FeePaymentsSpecified
                    || this.IdentifiedServiceProvidersSpecified
                    || this.RequiredServiceProviderSpecified;
            }
        }

        /// <summary>
        /// Details on the fee.
        /// </summary>
        [XmlElement("FEE_DETAIL", Order = 0)]
        public FEE_DETAIL FeeDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the FeeDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeeDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeeDetailSpecified
        {
            get { return this.FeeDetail != null && this.FeeDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The entity to whom the fee is paid.
        /// </summary>
        [XmlElement("FEE_PAID_TO", Order = 1)]
        public PAID_TO FeePaidTo;

        /// <summary>
        /// Gets or sets a value indicating whether the FeePaidTo element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeePaidTo element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeePaidToSpecified
        {
            get { return this.FeePaidTo != null && this.FeePaidTo.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Payments on a fee.
        /// </summary>
        [XmlElement("FEE_PAYMENTS", Order = 2)]
        public FEE_PAYMENTS FeePayments;

        /// <summary>
        /// Gets or sets a value indicating whether the FeePayments element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeePayments element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeePaymentsSpecified
        {
            get { return this.FeePayments != null && this.FeePayments.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Service providers associated with a fee.
        /// </summary>
        [XmlElement("IDENTIFIED_SERVICE_PROVIDERS", Order = 3)]
        public IDENTIFIED_SERVICE_PROVIDERS IdentifiedServiceProviders;

        /// <summary>
        /// Gets or sets a value indicating whether the IdentifiedServiceProviders element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IdentifiedServiceProviders element has been assigned a value.</value>
        [XmlIgnore]
        public bool IdentifiedServiceProvidersSpecified
        {
            get { return this.IdentifiedServiceProviders != null && this.IdentifiedServiceProviders.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Provider of a required service.
        /// </summary>
        [XmlElement("REQUIRED_SERVICE_PROVIDER", Order = 4)]
        public REQUIRED_SERVICE_PROVIDER RequiredServiceProvider;

        /// <summary>
        /// Gets or sets a value indicating whether the RequiredServiceProvider element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RequiredServiceProvider element has been assigned a value.</value>
        [XmlIgnore]
        public bool RequiredServiceProviderSpecified
        {
            get { return this.RequiredServiceProvider != null && this.RequiredServiceProvider.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 5)]
        public FEE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
