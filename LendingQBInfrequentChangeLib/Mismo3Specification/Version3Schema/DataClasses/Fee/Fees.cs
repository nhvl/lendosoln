namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class FEES
    {
        /// <summary>
        /// Gets a value indicating whether the FEES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.FeeSpecified;
            }
        }

        /// <summary>
        /// A collection of fees.
        /// </summary>
        [XmlElement("FEE", Order = 0)]
        public List<FEE> Fee = new List<FEE>();

        /// <summary>
        /// Gets or sets a value indicating whether the Fee element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Fee element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeeSpecified
        {
            get { return this.Fee != null && this.Fee.Count(f => f != null && f.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public FEES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
