namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class LEGAL_DESCRIPTIONS
    {
        /// <summary>
        /// Gets a value indicating whether the LEGAL_DESCRIPTIONS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.LegalDescriptionSpecified;
            }
        }

        /// <summary>
        /// A collection of legal descriptions.
        /// </summary>
        [XmlElement("LEGAL_DESCRIPTION", Order = 0)]
		public List<LEGAL_DESCRIPTION> LegalDescription = new List<LEGAL_DESCRIPTION>();

        /// <summary>
        /// Gets or sets a value indicating whether the LegalDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LegalDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool LegalDescriptionSpecified
        {
            get { return this.LegalDescription != null && this.LegalDescription.Count(l => l != null && l.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public LEGAL_DESCRIPTIONS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
