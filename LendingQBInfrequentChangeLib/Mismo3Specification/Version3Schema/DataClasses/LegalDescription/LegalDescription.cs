namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class LEGAL_DESCRIPTION
    {
        /// <summary>
        /// Gets a value indicating whether the LEGAL_DESCRIPTION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ParcelIdentificationsSpecified
                    || this.ParsedLegalDescriptionSpecified
                    || this.UnparsedLegalDescriptionsSpecified;
            }
        }

        /// <summary>
        /// A list of parcel identifications.
        /// </summary>
        [XmlElement("PARCEL_IDENTIFICATIONS", Order = 0)]
        public PARCEL_IDENTIFICATIONS ParcelIdentifications;

        /// <summary>
        /// Gets or sets a value indicating whether the ParcelIdentifications element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ParcelIdentifications element has been assigned a value.</value>
        [XmlIgnore]
        public bool ParcelIdentificationsSpecified
        {
            get { return this.ParcelIdentifications != null && this.ParcelIdentifications.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A list of parsed legal descriptions.
        /// </summary>
        [XmlElement("PARSED_LEGAL_DESCRIPTION", Order = 1)]
        public PARSED_LEGAL_DESCRIPTION ParsedLegalDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ParsedLegalDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ParsedLegalDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ParsedLegalDescriptionSpecified
        {
            get { return this.ParsedLegalDescription != null && this.ParsedLegalDescription.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A list of unparsed legal descriptions.
        /// </summary>
        [XmlElement("UNPARSED_LEGAL_DESCRIPTIONS", Order = 2)]
        public UNPARSED_LEGAL_DESCRIPTIONS UnparsedLegalDescriptions;

        /// <summary>
        /// Gets or sets a value indicating whether the UnparsedLegalDescriptions element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the UnparsedLegalDescriptions element has been assigned a value.</value>
        [XmlIgnore]
        public bool UnparsedLegalDescriptionsSpecified
        {
            get { return this.UnparsedLegalDescriptions != null && this.UnparsedLegalDescriptions.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public LEGAL_DESCRIPTION_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
