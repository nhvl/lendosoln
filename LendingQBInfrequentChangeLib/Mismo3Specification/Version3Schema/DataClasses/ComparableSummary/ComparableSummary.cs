namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class COMPARABLE_SUMMARY
    {
        /// <summary>
        /// Gets a value indicating whether the COMPARABLE_SUMMARY container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.SalesComparisonCurrentSalesAgreementAnalysisCommentDescriptionSpecified
                    || this.SalesComparisonTotalBedroomValueAmountSpecified
                    || this.SalesComparisonTotalGrossBuildingAreaValueAmountSpecified
                    || this.SalesComparisonTotalRoomValueAmountSpecified
                    || this.SalesComparisonTotalUnitValueAmountSpecified;
            }
        }

        /// <summary>
        /// A free-form text field used to describe or comment on the analysis of the current Sales Agreement on subject property.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString SalesComparisonCurrentSalesAgreementAnalysisCommentDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the SalesComparisonCurrentSalesAgreementAnalysisCommentDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SalesComparisonCurrentSalesAgreementAnalysisCommentDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool SalesComparisonCurrentSalesAgreementAnalysisCommentDescriptionSpecified
        {
            get { return SalesComparisonCurrentSalesAgreementAnalysisCommentDescription != null; }
            set { }
        }

        /// <summary>
        /// The dollar value of all the bedrooms. This value is determined by multiplying the Sales Comparison Value Per Bedroom Amount by the total number of bedrooms in the subject property. ().
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOAmount SalesComparisonTotalBedroomValueAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the SalesComparisonTotalBedroomValueAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SalesComparisonTotalBedroomValueAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool SalesComparisonTotalBedroomValueAmountSpecified
        {
            get { return SalesComparisonTotalBedroomValueAmount != null; }
            set { }
        }

        /// <summary>
        /// The dollar value of the gross building area. This value is determined by multiplying the Sales Comparison Value Per Gross Building Area Amount by the Gross Building Area Square Feet Count value of the subject property.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOAmount SalesComparisonTotalGrossBuildingAreaValueAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the SalesComparisonTotalGrossBuildingAreaValueAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SalesComparisonTotalGrossBuildingAreaValueAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool SalesComparisonTotalGrossBuildingAreaValueAmountSpecified
        {
            get { return SalesComparisonTotalGrossBuildingAreaValueAmount != null; }
            set { }
        }

        /// <summary>
        /// The dollar value of all of the rooms. This value is determined by multiplying the Sales Comparison Value Per Room Amount by the total number of rooms in the subject property. ().
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOAmount SalesComparisonTotalRoomValueAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the SalesComparisonTotalRoomValueAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SalesComparisonTotalRoomValueAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool SalesComparisonTotalRoomValueAmountSpecified
        {
            get { return SalesComparisonTotalRoomValueAmount != null; }
            set { }
        }

        /// <summary>
        /// The dollar value of all the living units. This value is determined by multiplying the Sales Comparison Value Per Unit Amount by the number of living units in the subject property.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOAmount SalesComparisonTotalUnitValueAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the SalesComparisonTotalUnitValueAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SalesComparisonTotalUnitValueAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool SalesComparisonTotalUnitValueAmountSpecified
        {
            get { return SalesComparisonTotalUnitValueAmount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 5)]
        public COMPARABLE_SUMMARY_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
