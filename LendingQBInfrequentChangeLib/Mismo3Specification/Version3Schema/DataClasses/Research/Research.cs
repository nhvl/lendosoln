namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class RESEARCH
    {
        /// <summary>
        /// Gets a value indicating whether the RESEARCH container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ComparableDataSourceDescriptionSpecified
                    || this.ComparableHasPriorSalesIndicatorSpecified
                    || this.ComparableListingsPriceRangeHighAmountSpecified
                    || this.ComparableListingsPriceRangeLowAmountSpecified
                    || this.ComparableListingsResearchedCountSpecified
                    || this.ComparableSalesPriceRangeHighAmountSpecified
                    || this.ComparableSalesPriceRangeLowAmountSpecified
                    || this.ComparableSalesResearchedCountSpecified
                    || this.ExtensionSpecified
                    || this.SalesHistoryNotResearchedCommentDescriptionSpecified
                    || this.SalesHistoryResearchedIndicatorSpecified
                    || this.SubjectHasPriorSalesIndicatorSpecified;
            }
        }

        /// <summary>
        /// A free-form text field used to describe the source of information for the comparable property.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString ComparableDataSourceDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ComparableDataSourceDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ComparableDataSourceDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ComparableDataSourceDescriptionSpecified
        {
            get { return ComparableDataSourceDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether the comparable property has prior sales history.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOIndicator ComparableHasPriorSalesIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ComparableHasPriorSalesIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ComparableHasPriorSalesIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ComparableHasPriorSalesIndicatorSpecified
        {
            get { return ComparableHasPriorSalesIndicator != null; }
            set { }
        }

        /// <summary>
        /// Specifies the high listing value in the range of values within the group of comparable listing properties researched.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOAmount ComparableListingsPriceRangeHighAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ComparableListingsPriceRangeHighAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ComparableListingsPriceRangeHighAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ComparableListingsPriceRangeHighAmountSpecified
        {
            get { return ComparableListingsPriceRangeHighAmount != null; }
            set { }
        }

        /// <summary>
        /// Specifies the low listing value in the range of values within the group of comparable listing properties researched.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOAmount ComparableListingsPriceRangeLowAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ComparableListingsPriceRangeLowAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ComparableListingsPriceRangeLowAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ComparableListingsPriceRangeLowAmountSpecified
        {
            get { return ComparableListingsPriceRangeLowAmount != null; }
            set { }
        }

        /// <summary>
        /// Specifies the total number of comparable property listings researched.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOCount ComparableListingsResearchedCount;

        /// <summary>
        /// Gets or sets a value indicating whether the ComparableListingsResearchedCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ComparableListingsResearchedCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ComparableListingsResearchedCountSpecified
        {
            get { return ComparableListingsResearchedCount != null; }
            set { }
        }

        /// <summary>
        /// Specifies the high sale value in the range of values within the group of comparable sales researched.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOAmount ComparableSalesPriceRangeHighAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ComparableSalesPriceRangeHighAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ComparableSalesPriceRangeHighAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ComparableSalesPriceRangeHighAmountSpecified
        {
            get { return ComparableSalesPriceRangeHighAmount != null; }
            set { }
        }

        /// <summary>
        /// Specifies the low sale value in the range of values within the group of comparable sales researched.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOAmount ComparableSalesPriceRangeLowAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ComparableSalesPriceRangeLowAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ComparableSalesPriceRangeLowAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ComparableSalesPriceRangeLowAmountSpecified
        {
            get { return ComparableSalesPriceRangeLowAmount != null; }
            set { }
        }

        /// <summary>
        /// Specifies the total number of comparable property sales researched.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOCount ComparableSalesResearchedCount;

        /// <summary>
        /// Gets or sets a value indicating whether the ComparableSalesResearchedCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ComparableSalesResearchedCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ComparableSalesResearchedCountSpecified
        {
            get { return ComparableSalesResearchedCount != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe or comment on why the sales or transfer history research of the subject property and comparable sales was not performed.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOString SalesHistoryNotResearchedCommentDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the SalesHistoryNotResearchedCommentDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SalesHistoryNotResearchedCommentDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool SalesHistoryNotResearchedCommentDescriptionSpecified
        {
            get { return SalesHistoryNotResearchedCommentDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the sales or transfer history research of the subject property and comparable sales was performed. 
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOIndicator SalesHistoryResearchedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the SalesHistoryResearchedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SalesHistoryResearchedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool SalesHistoryResearchedIndicatorSpecified
        {
            get { return SalesHistoryResearchedIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether the subject property has prior sales history.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOIndicator SubjectHasPriorSalesIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the SubjectHasPriorSalesIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SubjectHasPriorSalesIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool SubjectHasPriorSalesIndicatorSpecified
        {
            get { return SubjectHasPriorSalesIndicator != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 11)]
        public RESEARCH_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
