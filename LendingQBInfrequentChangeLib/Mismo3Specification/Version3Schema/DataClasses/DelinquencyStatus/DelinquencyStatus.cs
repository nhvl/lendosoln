namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class DELINQUENCY_STATUS
    {
        /// <summary>
        /// Gets a value indicating whether the DELINQUENCY_STATUS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DelinquentPaymentCountSpecified
                    || this.ExtensionSpecified
                    || this.LoanDelinquencyStatusDateSpecified
                    || this.LoanDelinquencyStatusTypeOtherDescriptionSpecified
                    || this.LoanDelinquencyStatusTypeSpecified
                    || this.PaymentDelinquencyStatusTypeSpecified;
            }
        }

        /// <summary>
        /// A delinquent payment is an unpaid payment. This is a count of the currently unpaid payments.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOCount DelinquentPaymentCount;

        /// <summary>
        /// Gets or sets a value indicating whether the DelinquentPaymentCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DelinquentPaymentCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool DelinquentPaymentCountSpecified
        {
            get { return DelinquentPaymentCount != null; }
            set { }
        }

        /// <summary>
        /// The date the loan delinquency status occurred.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMODate LoanDelinquencyStatusDate;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanDelinquencyStatusDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanDelinquencyStatusDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanDelinquencyStatusDateSpecified
        {
            get { return LoanDelinquencyStatusDate != null; }
            set { }
        }

        /// <summary>
        /// Loan Delinquency Status Type is used to report the status of a delinquent loan (i.e., is the loan in delinquency, foreclosure, bankruptcy, or some other status).
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<LoanDelinquencyStatusBase> LoanDelinquencyStatusType;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanDelinquencyStatusType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanDelinquencyStatusType element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanDelinquencyStatusTypeSpecified
        {
            get { return this.LoanDelinquencyStatusType != null && this.LoanDelinquencyStatusType.enumValue != LoanDelinquencyStatusBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture the Loan Delinquency Status Type if Other is selected.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString LoanDelinquencyStatusTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanDelinquencyStatusTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanDelinquencyStatusTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanDelinquencyStatusTypeOtherDescriptionSpecified
        {
            get { return LoanDelinquencyStatusTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Reports the delinquency status of the specific delinquency event.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<PaymentDelinquencyStatusBase> PaymentDelinquencyStatusType;

        /// <summary>
        /// Gets or sets a value indicating whether the PaymentDelinquencyStatusType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PaymentDelinquencyStatusType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PaymentDelinquencyStatusTypeSpecified
        {
            get { return this.PaymentDelinquencyStatusType != null && this.PaymentDelinquencyStatusType.enumValue != PaymentDelinquencyStatusBase.Blank; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 5)]
        public DELINQUENCY_STATUS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
