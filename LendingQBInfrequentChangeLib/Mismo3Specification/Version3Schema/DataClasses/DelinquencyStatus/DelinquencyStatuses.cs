namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class DELINQUENCY_STATUSES
    {
        /// <summary>
        /// Gets a value indicating whether the DELINQUENCY_STATUSES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DelinquencyStatusSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of delinquency statuses.
        /// </summary>
        [XmlElement("DELINQUENCY_STATUS", Order = 0)]
		public List<DELINQUENCY_STATUS> DelinquencyStatus = new List<DELINQUENCY_STATUS>();

        /// <summary>
        /// Gets or sets a value indicating whether the DelinquencyStatus element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DelinquencyStatus element has been assigned a value.</value>
        [XmlIgnore]
        public bool DelinquencyStatusSpecified
        {
            get { return this.DelinquencyStatus != null && this.DelinquencyStatus.Count(d => d != null && d.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public DELINQUENCY_STATUSES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
