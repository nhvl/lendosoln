namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets a value indicating whether the SecurityInvestor list has a value to serialize.
    /// </summary>
    /// <returns>A boolean indicating whether the SecurityInvestor element contains elements that should be serialized.</returns>
    public partial class SECURITY_INVESTORS
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SECURITY_INVESTORS"/> class.
        /// </summary>
        public SECURITY_INVESTORS()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SECURITY_INVESTORS"/> class.
        /// </summary>
        /// <param name="investors">Investors to initialize the list of investors with.</param>
        public SECURITY_INVESTORS(params SECURITY_INVESTOR[] investors)
        {
            foreach (SECURITY_INVESTOR investor in investors)
            {
                SecurityInvestor.Add(investor);
            }
        }

        /// <summary>
        /// Gets a value indicating whether this SECURITY_INVESTORS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get { return this.SecurityInvestorSpecified; }
        }

        /// <summary>
        /// A list of security investors.
        /// </summary>
        [XmlElement("SECURITY_INVESTOR")]
        public List<SECURITY_INVESTOR> SecurityInvestor = new List<SECURITY_INVESTOR>();

        /// <summary>
        /// Gets a value indicating whether the SecurityInvestor list has a value to serialize.
        /// </summary>
        /// <returns>A boolean indicating whether the SecurityInvestor element contains elements that should be serialized.</returns>
        [XmlIgnore]
        public bool SecurityInvestorSpecified
        {
            get { return this.SecurityInvestor != null && this.SecurityInvestor.Exists(f => f != null && f.ShouldSerialize); }
        }
    }
}
