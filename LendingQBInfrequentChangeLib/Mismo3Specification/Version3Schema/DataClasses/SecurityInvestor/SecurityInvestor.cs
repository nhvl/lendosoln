namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets a value indicating whether the Parties container has a value to serialize.
    /// </summary>
    /// <returns>A boolean indicating whether the Parties element has been assigned a value.</returns>
    public partial class SECURITY_INVESTOR
    {
        /// <summary>
        /// Gets a value indicating whether this SECURITY_INVESTOR container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get { return this.ACHSpecified || this.InvestorDetailSpecified || this.PartiesSpecified; }
        }
        
        /// <summary>
        /// An ACH container with bank account information for this security investor.
        /// </summary>
        [XmlElement("ACH", Order = 0)]
        public ACH ACH;

        /// <summary>
        /// Gets a value indicating whether the ACH container has a value to serialize.
        /// </summary>
        /// <returns>A boolean indicating whether the ACH element has been assigned a value.</returns>
        [XmlIgnore]
        private bool ACHSpecified
        {
            get { return ACH != null && ACH.ShouldSerialize; }
        }

        /// <summary>
        /// Holds a list of detail containers for this security investor.
        /// </summary>
        [XmlElement("INVESTOR_DETAIL", Order = 1)]
        public INVESTOR_DETAIL InvestorDetail;

        /// <summary>
        /// Gets a value indicating whether the InvestorDetail container has a value to serialize.
        /// </summary>
        /// <returns>A boolean indicating whether the InvestorDetail element has been assigned a value.</returns>
        [XmlIgnore]
        private bool InvestorDetailSpecified
        {
            get { return InvestorDetail != null && InvestorDetail.ShouldSerialize(); }
        }

        /// <summary>
        /// Holds a list of parties for this security investor.
        /// </summary>
        [XmlElement("PARTIES", Order = 2)]
        public PARTIES Parties;

        /// <summary>
        /// Gets a value indicating whether the Parties container has a value to serialize.
        /// </summary>
        /// <returns>A boolean indicating whether the Parties element has been assigned a value.</returns>
        [XmlIgnore]
        private bool PartiesSpecified
        {
            get { return Parties != null && Parties.ShouldSerialize; }
        }
    }
}
