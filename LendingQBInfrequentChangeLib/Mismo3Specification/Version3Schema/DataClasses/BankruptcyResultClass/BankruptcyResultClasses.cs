namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class BANKRUPTCY_RESULT_CLASSES
    {
        /// <summary>
        /// Gets a value indicating whether the BANKRUPTCY_RESULT_CLASSES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BankruptcyResultClassSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of bankruptcy result classes.
        /// </summary>
        [XmlElement("BANKRUPTCY_RESULT_CLASS", Order = 0)]
		public List<BANKRUPTCY_RESULT_CLASS> BankruptcyResultClass = new List<BANKRUPTCY_RESULT_CLASS>();

        /// <summary>
        /// Gets or sets a value indicating whether the BankruptcyResultClass element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BankruptcyResultClass element has been assigned a value.</value>
        [XmlIgnore]
        public bool BankruptcyResultClassSpecified
        {
            get { return this.BankruptcyResultClass != null && this.BankruptcyResultClass.Count(b => b != null && b.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public BANKRUPTCY_RESULT_CLASSES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
