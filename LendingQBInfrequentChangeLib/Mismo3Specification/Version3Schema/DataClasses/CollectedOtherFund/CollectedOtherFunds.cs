namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class COLLECTED_OTHER_FUNDS
    {
        /// <summary>
        /// Gets a value indicating whether the COLLECTED_OTHER_FUNDS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CollectedOtherFundSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of collected other funds.
        /// </summary>
        [XmlElement("COLLECTED_OTHER_FUND", Order = 0)]
		public List<COLLECTED_OTHER_FUND> CollectedOtherFund = new List<COLLECTED_OTHER_FUND>();

        /// <summary>
        /// Gets or sets a value indicating whether the CollectedOtherFund element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CollectedOtherFund element has been assigned a value.</value>
        [XmlIgnore]
        public bool CollectedOtherFundSpecified
        {
            get { return this.CollectedOtherFund != null && this.CollectedOtherFund.Count(c => c != null && c.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public COLLECTED_OTHER_FUNDS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
