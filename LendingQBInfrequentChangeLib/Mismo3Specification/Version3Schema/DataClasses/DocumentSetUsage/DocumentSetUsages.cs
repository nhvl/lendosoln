namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class DOCUMENT_SET_USAGES
    {
        /// <summary>
        /// Gets a value indicating whether the DOCUMENT_SET_USAGES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DocumentSetUsageSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of document set usages.
        /// </summary>
        [XmlElement("DOCUMENT_SET_USAGE", Order = 0)]
		public List<DOCUMENT_SET_USAGE> DocumentSetUsage = new List<DOCUMENT_SET_USAGE>();

        /// <summary>
        /// Gets or sets a value indicating whether the DocumentSetUsage element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DocumentSetUsage element has been assigned a value.</value>
        [XmlIgnore]
        public bool DocumentSetUsageSpecified
        {
            get { return this.DocumentSetUsage != null && this.DocumentSetUsage.Count(d => d != null && d.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public DOCUMENT_SET_USAGES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
