namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class VIEW_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the VIEW_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ViewCommentDescriptionSpecified
                    || this.ViewIsRecordedIndicatorSpecified
                    || this.ViewPageCountSpecified;
            }
        }

        /// <summary>
        /// Any information or instructions regarding a document view that a preparer includes to assist the recipient with understanding the contents or processing rules.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString ViewCommentDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ViewCommentDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ViewCommentDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ViewCommentDescriptionSpecified
        {
            get { return ViewCommentDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates that a document view is recorded in the public records.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOIndicator ViewIsRecordedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ViewIsRecordedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ViewIsRecordedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ViewIsRecordedIndicatorSpecified
        {
            get { return ViewIsRecordedIndicator != null; }
            set { }
        }

        /// <summary>
        /// The number of pages in a document view. 
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOCount ViewPageCount;

        /// <summary>
        /// Gets or sets a value indicating whether the ViewPageCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ViewPageCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ViewPageCountSpecified
        {
            get { return ViewPageCount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public VIEW_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
