namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class VIEW
    {
        /// <summary>
        /// Gets a value indicating whether the VIEW container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                if (Mismo3Utilities.ExceedsThreshold(1,
                    new List<bool> { this.ViewFilesSpecified, this.ViewPagesSpecified }))
                {
                    throw new System.Xml.Schema.XmlSchemaValidationException(
                        Mismo3Utilities.GetXSDChoiceExceptionMessage(
                        "VIEW",
                        new List<string> { "VIEW_FILES", "VIEW_PAGES" }));
                }

                return this.ExtensionSpecified
                    || this.ViewDetailSpecified
                    || this.ViewFieldsSpecified
                    || this.ViewFilesSpecified
                    || this.ViewIdentifiersSpecified
                    || this.ViewPagesSpecified;
            }
        }

        /// <summary>
        /// A list of view files.
        /// </summary>
        [XmlElement("VIEW_FILES", Order = 0)]
        public VIEW_FILES ViewFiles;

        /// <summary>
        /// Gets or sets a value indicating whether the ViewFiles element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ViewFiles element has been assigned a value.</value>
        [XmlIgnore]
        public bool ViewFilesSpecified
        {
            get { return this.ViewFiles != null && this.ViewFiles.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A list of view pages.
        /// </summary>
        [XmlElement("VIEW_PAGES", Order = 1)]
        public VIEW_PAGES ViewPages;

        /// <summary>
        /// Gets or sets a value indicating whether the ViewPages element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ViewPages element has been assigned a value.</value>
        [XmlIgnore]
        public bool ViewPagesSpecified
        {
            get { return this.ViewPages != null && this.ViewPages.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details on a view.
        /// </summary>
        [XmlElement("VIEW_DETAIL", Order = 2)]
        public VIEW_DETAIL ViewDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the ViewDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ViewDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool ViewDetailSpecified
        {
            get { return this.ViewDetail != null && this.ViewDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A list of view fields.
        /// </summary>
        [XmlElement("VIEW_FIELDS", Order = 3)]
        public VIEW_FIELDS ViewFields;

        /// <summary>
        /// Gets or sets a value indicating whether the ViewFields element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ViewFields element has been assigned a value.</value>
        [XmlIgnore]
        public bool ViewFieldsSpecified
        {
            get { return this.ViewFields != null && this.ViewFields.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A list of view identifiers.
        /// </summary>
        [XmlElement("VIEW_IDENTIFIERS", Order = 4)]
        public VIEW_IDENTIFIERS ViewIdentifiers;

        /// <summary>
        /// Gets or sets a value indicating whether the ViewIdentifiers element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ViewIdentifiers element has been assigned a value.</value>
        [XmlIgnore]
        public bool ViewIdentifiersSpecified
        {
            get { return this.ViewIdentifiers != null && this.ViewIdentifiers.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 5)]
        public VIEW_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
