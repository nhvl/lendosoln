namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class VIEWS
    {
        /// <summary>
        /// Gets a value indicating whether the VIEWS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ViewSpecified;
            }
        }

        /// <summary>
        /// A collection of views.
        /// </summary>
        [XmlElement("VIEW", Order = 0)]
		public List<VIEW> View = new List<VIEW>();

        /// <summary>
        /// Gets or sets a value indicating whether the View element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the View element has been assigned a value.</value>
        [XmlIgnore]
        public bool ViewSpecified
        {
            get { return this.View != null && this.View.Count(v => v != null && v.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public VIEWS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
