namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class INTERIOR
    {
        /// <summary>
        /// Gets a value indicating whether the INTERIOR container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AtticSpecified
                    || this.BasementSpecified
                    || this.ExtensionSpecified
                    || this.InteriorRoomSummariesSpecified
                    || this.RoomsSpecified;
            }
        }

        /// <summary>
        /// Describes the attic.
        /// </summary>
        [XmlElement("ATTIC", Order = 0)]
        public ATTIC Attic;

        /// <summary>
        /// Gets or sets a value indicating whether the Attic element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Attic element has been assigned a value.</value>
        [XmlIgnore]
        public bool AtticSpecified
        {
            get { return this.Attic != null && this.Attic.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Describes the basement.
        /// </summary>
        [XmlElement("BASEMENT", Order = 1)]
        public BASEMENT Basement;

        /// <summary>
        /// Gets or sets a value indicating whether the Basement element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Basement element has been assigned a value.</value>
        [XmlIgnore]
        public bool BasementSpecified
        {
            get { return this.Basement != null && this.Basement.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Summaries of the interior rooms.
        /// </summary>
        [XmlElement("INTERIOR_ROOM_SUMMARIES", Order = 2)]
        public INTERIOR_ROOM_SUMMARIES InteriorRoomSummaries;

        /// <summary>
        /// Gets or sets a value indicating whether the InteriorRoomSummaries element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InteriorRoomSummaries element has been assigned a value.</value>
        [XmlIgnore]
        public bool InteriorRoomSummariesSpecified
        {
            get { return this.InteriorRoomSummaries != null && this.InteriorRoomSummaries.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A list of interior rooms.
        /// </summary>
        [XmlElement("ROOMS", Order = 3)]
        public ROOMS Rooms;

        /// <summary>
        /// Gets or sets a value indicating whether the Rooms element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Rooms element has been assigned a value.</value>
        [XmlIgnore]
        public bool RoomsSpecified
        {
            get { return this.Rooms != null && this.Rooms.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public INTERIOR_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
