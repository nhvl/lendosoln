namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class INTEGRATED_DISCLOSURE_SUBSECTION_PAYMENT
    {
        /// <summary>
        /// Gets a value indicating whether the INTEGRATED_DISCLOSURE_SUBSECTION_PAYMENT container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.IntegratedDisclosureSubsectionPaidByTypeSpecified
                    || this.IntegratedDisclosureSubsectionPaymentAmountSpecified
                    || this.IntegratedDisclosureSubsectionPaymentTimingTypeSpecified;
            }
        }

        /// <summary>
        /// The role of the party making the payment of the amount associated with the Integrated Disclosure Subsection Total Amount.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<IntegratedDisclosureSubsectionPaidByBase> IntegratedDisclosureSubsectionPaidByType;

        /// <summary>
        /// Gets or sets a value indicating whether the IntegratedDisclosureSubsectionPaidByType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IntegratedDisclosureSubsectionPaidByType element has been assigned a value.</value>
        [XmlIgnore]
        public bool IntegratedDisclosureSubsectionPaidByTypeSpecified
        {
            get { return this.IntegratedDisclosureSubsectionPaidByType != null && this.IntegratedDisclosureSubsectionPaidByType.enumValue != IntegratedDisclosureSubsectionPaidByBase.Blank; }
            set { }
        }

        /// <summary>
        /// The dollar amount of the individual payment being made associated with the Integrated Disclosure Subsection Type.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOAmount IntegratedDisclosureSubsectionPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the IntegratedDisclosureSubsectionPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IntegratedDisclosureSubsectionPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool IntegratedDisclosureSubsectionPaymentAmountSpecified
        {
            get { return IntegratedDisclosureSubsectionPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// Defines the point in time during the origination process when the integrated disclosure subsection payment was paid.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<IntegratedDisclosureSubsectionPaymentTimingBase> IntegratedDisclosureSubsectionPaymentTimingType;

        /// <summary>
        /// Gets or sets a value indicating whether the IntegratedDisclosureSubsectionPaymentTimingType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IntegratedDisclosureSubsectionPaymentTimingType element has been assigned a value.</value>
        [XmlIgnore]
        public bool IntegratedDisclosureSubsectionPaymentTimingTypeSpecified
        {
            get { return this.IntegratedDisclosureSubsectionPaymentTimingType != null && this.IntegratedDisclosureSubsectionPaymentTimingType.enumValue != IntegratedDisclosureSubsectionPaymentTimingBase.Blank; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public INTEGRATED_DISCLOSURE_SUBSECTION_PAYMENT_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
