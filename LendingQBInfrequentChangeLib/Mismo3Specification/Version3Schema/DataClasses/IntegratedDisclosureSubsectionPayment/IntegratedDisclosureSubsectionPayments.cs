namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class INTEGRATED_DISCLOSURE_SUBSECTION_PAYMENTS
    {
        /// <summary>
        /// Gets a value indicating whether the INTEGRATED_DISCLOSURE_SUBSECTION_PAYMENTS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.IntegratedDisclosureSubsectionPaymentSpecified;
            }
        }

        /// <summary>
        /// A collection of integrated disclosure subsection payments.
        /// </summary>
        [XmlElement("INTEGRATED_DISCLOSURE_SUBSECTION_PAYMENT", Order = 0)]
		public List<INTEGRATED_DISCLOSURE_SUBSECTION_PAYMENT> IntegratedDisclosureSubsectionPayment = new List<INTEGRATED_DISCLOSURE_SUBSECTION_PAYMENT>();

        /// <summary>
        /// Gets or sets a value indicating whether the IntegratedDisclosureSubsectionPayment element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IntegratedDisclosureSubsectionPayment element has been assigned a value.</value>
        [XmlIgnore]
        public bool IntegratedDisclosureSubsectionPaymentSpecified
        {
            get { return this.IntegratedDisclosureSubsectionPayment != null && this.IntegratedDisclosureSubsectionPayment.Count(i => i != null && i.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public INTEGRATED_DISCLOSURE_SUBSECTION_PAYMENTS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
