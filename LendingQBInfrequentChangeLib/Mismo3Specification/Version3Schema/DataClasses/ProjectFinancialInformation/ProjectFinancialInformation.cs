namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PROJECT_FINANCIAL_INFORMATION
    {
        /// <summary>
        /// Gets a value indicating whether the PROJECT_FINANCIAL_INFORMATION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ProjectPerUnitFinancingAmountSpecified
                    || this.ProjectPerUnitLienAmountSpecified
                    || this.ProjectStockTransferFeeDescriptionSpecified
                    || this.ProjectStockTransferFeeIndicatorSpecified
                    || this.ProjectTaxAbatementsOrExemptionsDescriptionSpecified
                    || this.ProjectTaxAbatementsOrExemptionsIndicatorSpecified
                    || this.ProjectTotalSharesCountSpecified;
            }
        }

        /// <summary>
        /// Reports the amount of project financing per unit.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount ProjectPerUnitFinancingAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectPerUnitFinancingAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectPerUnitFinancingAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectPerUnitFinancingAmountSpecified
        {
            get { return ProjectPerUnitFinancingAmount != null; }
            set { }
        }

        /// <summary>
        /// Reports the amount of any liens against the project per unit.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOAmount ProjectPerUnitLienAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectPerUnitLienAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectPerUnitLienAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectPerUnitLienAmountSpecified
        {
            get { return ProjectPerUnitLienAmount != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field describing the fee incurred when transferring cooperative stock (e.g. an owner selling their interest).
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString ProjectStockTransferFeeDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectStockTransferFeeDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectStockTransferFeeDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectStockTransferFeeDescriptionSpecified
        {
            get { return ProjectStockTransferFeeDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates that transferring cooperative stock (e.g. an owner selling their interest) incurs a fee.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOIndicator ProjectStockTransferFeeIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectStockTransferFeeIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectStockTransferFeeIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectStockTransferFeeIndicatorSpecified
        {
            get { return ProjectStockTransferFeeIndicator != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field describing the nature of the tax abatements and exemptions the project is subject to.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString ProjectTaxAbatementsOrExemptionsDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectTaxAbatementsOrExemptionsDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectTaxAbatementsOrExemptionsDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectTaxAbatementsOrExemptionsDescriptionSpecified
        {
            get { return ProjectTaxAbatementsOrExemptionsDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the project is subject to tax abatements or exemptions.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOIndicator ProjectTaxAbatementsOrExemptionsIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectTaxAbatementsOrExemptionsIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectTaxAbatementsOrExemptionsIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectTaxAbatementsOrExemptionsIndicatorSpecified
        {
            get { return ProjectTaxAbatementsOrExemptionsIndicator != null; }
            set { }
        }

        /// <summary>
        /// Enumerates the total outstanding shares of the project (e.g. cooperative ownership stock).
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOCount ProjectTotalSharesCount;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectTotalSharesCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectTotalSharesCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectTotalSharesCountSpecified
        {
            get { return ProjectTotalSharesCount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 7)]
        public PROJECT_FINANCIAL_INFORMATION_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
