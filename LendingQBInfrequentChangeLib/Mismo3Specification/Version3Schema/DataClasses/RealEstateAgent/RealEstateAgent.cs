namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class REAL_ESTATE_AGENT
    {
        /// <summary>
        /// Gets a value indicating whether the REAL_ESTATE_AGENT container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.RealEstateAgentTypeOtherDescriptionSpecified
                    || this.RealEstateAgentTypeSpecified;
            }
        }

        /// <summary>
        /// Describes the type of real estate agent.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<RealEstateAgentBase> RealEstateAgentType;

        /// <summary>
        /// Gets or sets a value indicating whether the RealEstateAgentType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RealEstateAgentType element has been assigned a value.</value>
        [XmlIgnore]
        public bool RealEstateAgentTypeSpecified
        {
            get { return this.RealEstateAgentType != null && this.RealEstateAgentType.enumValue != RealEstateAgentBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the type of real estate agent if Other is selected as the real estate agent type.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString RealEstateAgentTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the RealEstateAgentTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RealEstateAgentTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool RealEstateAgentTypeOtherDescriptionSpecified
        {
            get { return RealEstateAgentTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public REAL_ESTATE_AGENT_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
