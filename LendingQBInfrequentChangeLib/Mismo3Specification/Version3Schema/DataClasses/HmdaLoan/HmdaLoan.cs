namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class HMDA_LOAN
    {
        /// <summary>
        /// Gets a value indicating whether the HMDA_LOAN container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.HMDA_HOEPALoanStatusIndicatorSpecified
                    || this.HMDAPreapprovalTypeSpecified
                    || this.HMDAPurposeOfLoanTypeSpecified
                    || this.HMDARateSpreadPercentSpecified;
            }
        }

        /// <summary>
        /// Flag used to indicate that loan is to be reported as a HOEPA (Home Ownership and Equity Protection Act of 1994) loan for HMDA reporting.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIndicator HMDA_HOEPALoanStatusIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the HMDA_HOEPALoanStatusIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HMDA_HOEPALoanStatusIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool HMDA_HOEPALoanStatusIndicatorSpecified
        {
            get { return HMDA_HOEPALoanStatusIndicator != null; }
            set { }
        }

        /// <summary>
        /// Specifies if the loan meets the HMDA Preapproval reporting requirements as defined in paragraph (b)(1).
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<HMDAPreapprovalBase> HMDAPreapprovalType;

        /// <summary>
        /// Gets or sets a value indicating whether the HMDAPreapprovalType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HMDAPreapprovalType element has been assigned a value.</value>
        [XmlIgnore]
        public bool HMDAPreapprovalTypeSpecified
        {
            get { return this.HMDAPreapprovalType != null && this.HMDAPreapprovalType.enumValue != HMDAPreapprovalBase.Blank; }
            set { }
        }

        /// <summary>
        /// Specifies the HMDA Purpose of Loan as defined in Regulation C. The definitions of a home improvement loan and a refinancing were substantially revised in the final rules adopted in 2002.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<HMDAPurposeOfLoanBase> HMDAPurposeOfLoanType;

        /// <summary>
        /// Gets or sets a value indicating whether the HMDAPurposeOfLoanType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HMDAPurposeOfLoanType element has been assigned a value.</value>
        [XmlIgnore]
        public bool HMDAPurposeOfLoanTypeSpecified
        {
            get { return this.HMDAPurposeOfLoanType != null && this.HMDAPurposeOfLoanType.enumValue != HMDAPurposeOfLoanBase.Blank; }
            set { }
        }

        /// <summary>
        /// The difference between the annual percentage rate (APR) and the average prime offer rate (APOR) as required for HMDA Reporting Requirements.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOPercent HMDARateSpreadPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the HMDARateSpreadPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HMDARateSpreadPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool HMDARateSpreadPercentSpecified
        {
            get { return HMDARateSpreadPercent != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public HMDA_LOAN_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
