namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class WORKOUT_EVALUATION_REQUEST_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the WORKOUT_EVALUATION_REQUEST_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.LoansSubmittedCountSpecified
                    || this.RequestBatchIdentifierSpecified
                    || this.ServiceRequestDatetimeSpecified
                    || this.ServiceRequestReasonIdentifierSpecified
                    || this.ServicerLossMitigationSoftwarePlatformNameSpecified
                    || this.ServicerLossMitigationSoftwarePlatformVersionIdentifierSpecified;
            }
        }

        /// <summary>
        /// The total number of loans submitted in a request.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOCount LoansSubmittedCount;

        /// <summary>
        /// Gets or sets a value indicating whether the LoansSubmittedCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoansSubmittedCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoansSubmittedCountSpecified
        {
            get { return LoansSubmittedCount != null; }
            set { }
        }

        /// <summary>
        /// A unique identifier assigned to a batch of loans for a request submission. 
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOIdentifier RequestBatchIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the RequestBatchIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RequestBatchIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool RequestBatchIdentifierSpecified
        {
            get { return RequestBatchIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The date and time when the request was generated for submission.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMODatetime ServiceRequestDatetime;

        /// <summary>
        /// Gets or sets a value indicating whether the ServiceRequestDateTime element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServiceRequestDateTime element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServiceRequestDatetimeSpecified
        {
            get { return ServiceRequestDatetime != null; }
            set { }
        }

        /// <summary>
        /// A unique identifier of the reason for submitting a request.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOIdentifier ServiceRequestReasonIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the ServiceRequestReasonIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServiceRequestReasonIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServiceRequestReasonIdentifierSpecified
        {
            get { return ServiceRequestReasonIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The name of the servicer loss mitigation software platform being utilized for a workout evaluation request submission.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString ServicerLossMitigationSoftwarePlatformName;

        /// <summary>
        /// Gets or sets a value indicating whether the ServicerLossMitigationSoftwarePlatformName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServicerLossMitigationSoftwarePlatformName element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServicerLossMitigationSoftwarePlatformNameSpecified
        {
            get { return ServicerLossMitigationSoftwarePlatformName != null; }
            set { }
        }

        /// <summary>
        /// A unique identifier of the loss mitigation software platform used by the servicer for a workout evaluation.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOIdentifier ServicerLossMitigationSoftwarePlatformVersionIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the ServicerLossMitigationSoftwarePlatformVersionIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServicerLossMitigationSoftwarePlatformVersionIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServicerLossMitigationSoftwarePlatformVersionIdentifierSpecified
        {
            get { return ServicerLossMitigationSoftwarePlatformVersionIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 6)]
        public WORKOUT_EVALUATION_REQUEST_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
