namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CLOSING_ADJUSTMENT_ITEM_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the CLOSING_ADJUSTMENT_ITEM_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ClosingAdjustmentItemAmountSpecified
                    || this.ClosingAdjustmentItemPaidByTypeSpecified
                    || this.ClosingAdjustmentItemPaidOutsideOfClosingIndicatorSpecified
                    || this.ClosingAdjustmentItemTypeOtherDescriptionSpecified
                    || this.ClosingAdjustmentItemTypeSpecified
                    || this.ClosingAdjustmentPaidFromDateSpecified
                    || this.ClosingAdjustmentPaidThroughDateSpecified
                    || this.ExtensionSpecified
                    || this.IntegratedDisclosureLineNumberValueSpecified
                    || this.IntegratedDisclosureSectionTypeOtherDescriptionSpecified
                    || this.IntegratedDisclosureSectionTypeSpecified
                    || this.IntegratedDisclosureSubsectionTypeOtherDescriptionSpecified
                    || this.IntegratedDisclosureSubsectionTypeSpecified;
            }
        }

        /// <summary>
        /// The dollar amount of the specified Closing Adjustment Item Type.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount ClosingAdjustmentItemAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ClosingAdjustmentItemAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ClosingAdjustmentItemAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ClosingAdjustmentItemAmountSpecified
        {
            get { return ClosingAdjustmentItemAmount != null; }
            set { }
        }

        /// <summary>
        /// The role of the party making the payment for the closing adjustment item.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<ClosingAdjustmentItemPaidByBase> ClosingAdjustmentItemPaidByType;

        /// <summary>
        /// Gets or sets a value indicating whether the ClosingAdjustmentItemPaidByType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ClosingAdjustmentItemPaidByType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ClosingAdjustmentItemPaidByTypeSpecified
        {
            get { return this.ClosingAdjustmentItemPaidByType != null && this.ClosingAdjustmentItemPaidByType.enumValue != ClosingAdjustmentItemPaidByBase.Blank; }
            set { }
        }

        /// <summary>
        /// When true, indicates payment of the closing adjustment item was paid outside of closing.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOIndicator ClosingAdjustmentItemPaidOutsideOfClosingIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ClosingAdjustmentItemPaidOutsideOfClosingIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ClosingAdjustmentItemPaidOutsideOfClosingIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ClosingAdjustmentItemPaidOutsideOfClosingIndicatorSpecified
        {
            get { return ClosingAdjustmentItemPaidOutsideOfClosingIndicator != null; }
            set { }
        }

        /// <summary>
        /// The type of adjustment made during the origination process.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOEnum<ClosingAdjustmentItemBase> ClosingAdjustmentItemType;

        /// <summary>
        /// Gets or sets a value indicating whether the ClosingAdjustmentItemType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ClosingAdjustmentItemType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ClosingAdjustmentItemTypeSpecified
        {
            get { return this.ClosingAdjustmentItemType != null && this.ClosingAdjustmentItemType.enumValue != ClosingAdjustmentItemBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Closing Adjustment Item Type.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString ClosingAdjustmentItemTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ClosingAdjustmentItemTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ClosingAdjustmentItemTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ClosingAdjustmentItemTypeOtherDescriptionSpecified
        {
            get { return ClosingAdjustmentItemTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The start date for calculating the total number of days covered by the closing adjustment item.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMODate ClosingAdjustmentPaidFromDate;

        /// <summary>
        /// Gets or sets a value indicating whether the ClosingAdjustmentPaidFromDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ClosingAdjustmentPaidFromDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ClosingAdjustmentPaidFromDateSpecified
        {
            get { return ClosingAdjustmentPaidFromDate != null; }
            set { }
        }

        /// <summary>
        /// The end date for calculating the total number of days covered by the closing adjustment item, inclusive of this date.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMODate ClosingAdjustmentPaidThroughDate;

        /// <summary>
        /// Gets or sets a value indicating whether the ClosingAdjustmentPaidThroughDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ClosingAdjustmentPaidThroughDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ClosingAdjustmentPaidThroughDateSpecified
        {
            get { return ClosingAdjustmentPaidThroughDate != null; }
            set { }
        }

        /// <summary>
        /// The line identifier for the item as represented in a section of the Integrated Disclosure form.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOValue IntegratedDisclosureLineNumberValue;

        /// <summary>
        /// Gets or sets a value indicating whether the IntegratedDisclosureLineNumberValue element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IntegratedDisclosureLineNumberValue element has been assigned a value.</value>
        [XmlIgnore]
        public bool IntegratedDisclosureLineNumberValueSpecified
        {
            get { return IntegratedDisclosureLineNumberValue != null; }
            set { }
        }

        /// <summary>
        /// The title or description used to identify a primary section of the integrated disclosure document.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOEnum<IntegratedDisclosureSectionBase> IntegratedDisclosureSectionType;

        /// <summary>
        /// Gets or sets a value indicating whether the IntegratedDisclosureSectionType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IntegratedDisclosureSectionType element has been assigned a value.</value>
        [XmlIgnore]
        public bool IntegratedDisclosureSectionTypeSpecified
        {
            get { return this.IntegratedDisclosureSectionType != null && this.IntegratedDisclosureSectionType.enumValue != IntegratedDisclosureSectionBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture additional information when Other is selected for Integrated Disclosure Section Type.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOString IntegratedDisclosureSectionTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the IntegratedDisclosureSectionTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IntegratedDisclosureSectionTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool IntegratedDisclosureSectionTypeOtherDescriptionSpecified
        {
            get { return IntegratedDisclosureSectionTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The title or description used to identify a secondary or subsection of the integrated disclosure document.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOEnum<IntegratedDisclosureSubsectionBase> IntegratedDisclosureSubsectionType;

        /// <summary>
        /// Gets or sets a value indicating whether the IntegratedDisclosureSubsectionType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IntegratedDisclosureSubsectionType element has been assigned a value.</value>
        [XmlIgnore]
        public bool IntegratedDisclosureSubsectionTypeSpecified
        {
            get { return this.IntegratedDisclosureSubsectionType != null && this.IntegratedDisclosureSubsectionType.enumValue != IntegratedDisclosureSubsectionBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture additional information when Other is selected for Integrated Disclosure Subsection Type.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOString IntegratedDisclosureSubsectionTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the IntegratedDisclosureSubsectionTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IntegratedDisclosureSubsectionTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool IntegratedDisclosureSubsectionTypeOtherDescriptionSpecified
        {
            get { return IntegratedDisclosureSubsectionTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 12)]
        public CLOSING_ADJUSTMENT_ITEM_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
