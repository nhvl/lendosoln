namespace Mismo3Specification.Version3Schema
{
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Other element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Other element has been assigned a value.</value>
    public partial class CLOSING_ADJUSTMENT_ITEM_DETAIL_EXTENSION
    {
        /// <summary>
        /// Gets a value indicating whether this container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.MismoSpecified
                    || this.OtherSpecified;
            }
        }

        /// <summary>
        /// A generic Mortgage Industry Standards Maintenance Organization element for allowing custom extension.
        /// </summary>
        [XmlElement("MISMO", Order = 0)]
        public MISMO_BASE Mismo;

        /// <summary>
        /// Gets or sets a value indicating whether the Mortgage Industry Standards Maintenance Organization element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Mortgage Industry Standards Maintenance Organization element has been assigned a value.</value>
        [XmlIgnore]
        public bool MismoSpecified
        {
            get { return Mismo != null; }
            set { }
        }

        /// <summary>
        /// A generic miscellaneous element for allowing custom extension.
        /// </summary>
        [XmlElement("OTHER", Order = 1)]
        public LQB_CLOSING_ADJUSTMENT_ITEM_DETAIL_EXTENSION Other;

        /// <summary>
        /// Gets or sets a value indicating whether the Other element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Other element has been assigned a value.</value>
        [XmlIgnore]
        public bool OtherSpecified
        {
            get { return this.Other != null && this.Other.ShouldSerialize; }
            set { }
        }
    }
}
