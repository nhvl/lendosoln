namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CLOSING_ADJUSTMENT_ITEMS
    {
        /// <summary>
        /// Gets a value indicating whether the CLOSING_ADJUSTMENT_ITEMS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get 
            {
                return this.ClosingAdjustmentItemSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of closing adjustment items.
        /// </summary>
        [XmlElement("CLOSING_ADJUSTMENT_ITEM", Order = 0)]
        public List<CLOSING_ADJUSTMENT_ITEM> ClosingAdjustmentItem = new List<CLOSING_ADJUSTMENT_ITEM>();

        /// <summary>
        /// Gets or sets a value indicating whether the ClosingAdjustmentItem element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ClosingAdjustmentItem element has been assigned a value.</value>
        [XmlIgnore]
        public bool ClosingAdjustmentItemSpecified
        {
            get { return ClosingAdjustmentItem != null && ClosingAdjustmentItem.Count(i => i != null && i.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public CLOSING_ADJUSTMENT_ITEMS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
