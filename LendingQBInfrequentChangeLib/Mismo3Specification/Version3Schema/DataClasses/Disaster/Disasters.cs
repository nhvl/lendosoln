namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class DISASTERS
    {
        /// <summary>
        /// Gets a value indicating whether the DISASTERS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DisasterSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of disasters.
        /// </summary>
        [XmlElement("DISASTER", Order = 0)]
		public List<DISASTER> Disaster = new List<DISASTER>();

        /// <summary>
        /// Gets or sets a value indicating whether the Disaster element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Disaster element has been assigned a value.</value>
        [XmlIgnore]
        public bool DisasterSpecified
        {
            get { return this.Disaster != null && this.Disaster.Count(d => d != null && d.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public DISASTERS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
