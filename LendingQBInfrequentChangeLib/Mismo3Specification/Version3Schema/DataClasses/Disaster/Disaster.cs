namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class DISASTER
    {
        /// <summary>
        /// Gets a value indicating whether the DISASTER container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DisasterAreaDescriptionSpecified
                    || this.DisasterDeclarationDateSpecified
                    || this.DisasterTypeOtherDescriptionSpecified
                    || this.DisasterTypeSpecified
                    || this.ExtensionSpecified
                    || this.FEMADisasterNameSpecified;
            }
        }

        /// <summary>
        /// A free form description of the area affected by the disaster.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString DisasterAreaDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the DisasterAreaDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DisasterAreaDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool DisasterAreaDescriptionSpecified
        {
            get { return DisasterAreaDescription != null; }
            set { }
        }

        /// <summary>
        /// The date on which the disaster was declared by the government.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMODate DisasterDeclarationDate;

        /// <summary>
        /// Gets or sets a value indicating whether the DisasterDeclarationDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DisasterDeclarationDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool DisasterDeclarationDateSpecified
        {
            get { return DisasterDeclarationDate != null; }
            set { }
        }

        /// <summary>
        /// Specifies the type of disaster that has affected the area. 
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<DisasterBase> DisasterType;

        /// <summary>
        /// Gets or sets a value indicating whether the DisasterType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DisasterType element has been assigned a value.</value>
        [XmlIgnore]
        public bool DisasterTypeSpecified
        {
            get { return this.DisasterType != null && this.DisasterType.enumValue != DisasterBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is  selected as the Disaster Type.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString DisasterTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the DisasterTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DisasterTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool DisasterTypeOtherDescriptionSpecified
        {
            get { return DisasterTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Name assigned by FEMA for a natural disaster that affects this property.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString FEMADisasterName;

        /// <summary>
        /// Gets or sets a value indicating whether the FEMADisasterName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FEMADisasterName element has been assigned a value.</value>
        [XmlIgnore]
        public bool FEMADisasterNameSpecified
        {
            get { return FEMADisasterName != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 5)]
        public DISASTER_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
