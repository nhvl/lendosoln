namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class TRANSFORM_PAGE_FILES
    {
        /// <summary>
        /// Gets a value indicating whether the TRANSFORM_PAGE_FILES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.TransformPageFileSpecified;
            }
        }

        /// <summary>
        /// A collection of transform page files.
        /// </summary>
        [XmlElement("TRANSFORM_PAGE_FILE", Order = 0)]
		public List<TRANSFORM_PAGE_FILE> TransformPageFile = new List<TRANSFORM_PAGE_FILE>();

        /// <summary>
        /// Gets or sets a value indicating whether the TransformPageFile element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TransformPageFile element has been assigned a value.</value>
        [XmlIgnore]
        public bool TransformPageFileSpecified
        {
            get { return this.TransformPageFile != null && this.TransformPageFile.Count(t => t != null && t.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public TRANSFORM_PAGE_FILES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
