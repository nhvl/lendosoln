namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class ENVIRONMENTAL_CONDITION
    {
        /// <summary>
        /// Gets a value indicating whether the ENVIRONMENTAL_CONDITION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.EnvironmentalConditionIdentifiedDateSpecified
                    || this.EnvironmentalConditionIdentifiedDescriptionSpecified
                    || this.EnvironmentalConditionTypeOtherDescriptionSpecified
                    || this.EnvironmentalConditionTypeSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// The date the environmental condition was identified.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMODate EnvironmentalConditionIdentifiedDate;

        /// <summary>
        /// Gets or sets a value indicating whether the EnvironmentalConditionIdentifiedDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EnvironmentalConditionIdentifiedDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool EnvironmentalConditionIdentifiedDateSpecified
        {
            get { return EnvironmentalConditionIdentifiedDate != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the details of the hazard identified.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString EnvironmentalConditionIdentifiedDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the EnvironmentalConditionIdentifiedDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EnvironmentalConditionIdentifiedDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool EnvironmentalConditionIdentifiedDescriptionSpecified
        {
            get { return EnvironmentalConditionIdentifiedDescription != null; }
            set { }
        }

        /// <summary>
        /// A type of environmental condition identified during the analysis of a lot or structure.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<EnvironmentalConditionBase> EnvironmentalConditionType;

        /// <summary>
        /// Gets or sets a value indicating whether the EnvironmentalConditionType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EnvironmentalConditionType element has been assigned a value.</value>
        [XmlIgnore]
        public bool EnvironmentalConditionTypeSpecified
        {
            get { return this.EnvironmentalConditionType != null && this.EnvironmentalConditionType.enumValue != EnvironmentalConditionBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Environmental Condition Type.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString EnvironmentalConditionTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the EnvironmentalConditionTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EnvironmentalConditionTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool EnvironmentalConditionTypeOtherDescriptionSpecified
        {
            get { return EnvironmentalConditionTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public ENVIRONMENTAL_CONDITION_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
