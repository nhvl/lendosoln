namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class ENVIRONMENTAL_CONDITIONS
    {
        /// <summary>
        /// Gets a value indicating whether the ENVIRONMENTAL_CONDITIONS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.EnvironmentalConditionSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of environmental conditions.
        /// </summary>
        [XmlElement("ENVIRONMENTAL_CONDITION", Order = 0)]
		public List<ENVIRONMENTAL_CONDITION> EnvironmentalCondition = new List<ENVIRONMENTAL_CONDITION>();

        /// <summary>
        /// Gets or sets a value indicating whether the EnvironmentalCondition element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EnvironmentalCondition element has been assigned a value.</value>
        [XmlIgnore]
        public bool EnvironmentalConditionSpecified
        {
            get { return this.EnvironmentalCondition != null && this.EnvironmentalCondition.Count(e => e != null && e.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public ENVIRONMENTAL_CONDITIONS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
