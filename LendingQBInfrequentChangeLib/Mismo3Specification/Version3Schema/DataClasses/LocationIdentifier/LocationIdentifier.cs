namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class LOCATION_IDENTIFIER
    {
        /// <summary>
        /// Gets a value indicating whether the LOCATION_IDENTIFIER container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CensusInformationSpecified
                    || this.ExtensionSpecified
                    || this.FipsInformationSpecified
                    || this.GeneralIdentifierSpecified
                    || this.GeospatialInformationSpecified
                    || this.UniquePropertyIdentificationsSpecified;
            }
        }

        /// <summary>
        /// Census information from the location.
        /// </summary>
        [XmlElement("CENSUS_INFORMATION", Order = 0)]
        public CENSUS_INFORMATION CensusInformation;

        /// <summary>
        /// Gets or sets a value indicating whether the CensusInformation element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CensusInformation element has been assigned a value.</value>
        [XmlIgnore]
        public bool CensusInformationSpecified
        {
            get { return this.CensusInformation != null && this.CensusInformation.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// FIPS information on the location.
        /// </summary>
        [XmlElement("FIPS_INFORMATION", Order = 1)]
        public FIPS_INFORMATION FipsInformation;

        /// <summary>
        /// Gets or sets a value indicating whether the FIPSInformation element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FIPSInformation element has been assigned a value.</value>
        [XmlIgnore]
        public bool FipsInformationSpecified
        {
            get { return this.FipsInformation != null && this.FipsInformation.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// General identifier for the location.
        /// </summary>
        [XmlElement("GENERAL_IDENTIFIER", Order = 2)]
        public GENERAL_IDENTIFIER GeneralIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the GeneralIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GeneralIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool GeneralIdentifierSpecified
        {
            get { return this.GeneralIdentifier != null && this.GeneralIdentifier.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Geospatial information describing the location.
        /// </summary>
        [XmlElement("GEOSPATIAL_INFORMATION", Order = 3)]
        public GEOSPATIAL_INFORMATION GeospatialInformation;

        /// <summary>
        /// Gets or sets a value indicating whether the GeospatialInformation element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GeospatialInformation element has been assigned a value.</value>
        [XmlIgnore]
        public bool GeospatialInformationSpecified
        {
            get { return this.GeospatialInformation != null && this.GeospatialInformation.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Unique identifications of the property.
        /// </summary>
        [XmlElement("UNIQUE_PROPERTY_IDENTIFICATIONS", Order = 4)]
        public UNIQUE_PROPERTY_IDENTIFICATIONS UniquePropertyIdentifications;

        /// <summary>
        /// Gets or sets a value indicating whether the UniquePropertyIdentifications element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the UniquePropertyIdentifications element has been assigned a value.</value>
        [XmlIgnore]
        public bool UniquePropertyIdentificationsSpecified
        {
            get { return this.UniquePropertyIdentifications != null && this.UniquePropertyIdentifications.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 5)]
        public LOCATION_IDENTIFIER_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
