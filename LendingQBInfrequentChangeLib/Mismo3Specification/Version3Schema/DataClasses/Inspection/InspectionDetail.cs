namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class INSPECTION_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the INSPECTION_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ExteriorInspectionSufficientIndicatorSpecified
                    || this.InspectionDateSpecified
                    || this.PropertyInaccessibleIndicatorSpecified
                    || this.PropertyInspectionOrderDateSpecified
                    || this.PropertyInspectionPurposeTypeOtherDescriptionSpecified
                    || this.PropertyInspectionPurposeTypeSpecified
                    || this.PropertyInspectionRequestCommentDescriptionSpecified
                    || this.PropertyInspectionResultCommentDescriptionSpecified
                    || this.PropertyInspectionTypeOtherDescriptionSpecified
                    || this.PropertyInspectionTypeSpecified
                    || this.ValuationUseTypeSpecified;
            }
        }

        /// <summary>
        /// Indicates that an exterior only inspection is sufficient.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIndicator ExteriorInspectionSufficientIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ExteriorInspectionSufficientIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ExteriorInspectionSufficientIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExteriorInspectionSufficientIndicatorSpecified
        {
            get { return ExteriorInspectionSufficientIndicator != null; }
            set { }
        }

        /// <summary>
        /// The date the inspection of the property was performed.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMODate InspectionDate;

        /// <summary>
        /// Gets or sets a value indicating whether the InspectionDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InspectionDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool InspectionDateSpecified
        {
            get { return InspectionDate != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that the property is not accessible for inspection.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOIndicator PropertyInaccessibleIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyInaccessibleIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyInaccessibleIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyInaccessibleIndicatorSpecified
        {
            get { return PropertyInaccessibleIndicator != null; }
            set { }
        }

        /// <summary>
        /// The date the property inspection was ordered.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMODate PropertyInspectionOrderDate;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyInspectionOrderDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyInspectionOrderDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyInspectionOrderDateSpecified
        {
            get { return PropertyInspectionOrderDate != null; }
            set { }
        }

        /// <summary>
        /// Identifies the scope of the inspection performed and the type of inspector that is performing the inspection. 
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<PropertyInspectionPurposeBase> PropertyInspectionPurposeType;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyInspectionPurposeType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyInspectionPurposeType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyInspectionPurposeTypeSpecified
        {
            get { return this.PropertyInspectionPurposeType != null && this.PropertyInspectionPurposeType.enumValue != PropertyInspectionPurposeBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form field used to capture additional information when the selection for Property Inspection Purpose Type is Other.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString PropertyInspectionPurposeTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyInspectionPurposeTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyInspectionPurposeTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyInspectionPurposeTypeOtherDescriptionSpecified
        {
            get { return PropertyInspectionPurposeTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Free form commentary about the request for the inspection.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOString PropertyInspectionRequestCommentDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyInspectionRequestCommentDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyInspectionRequestCommentDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyInspectionRequestCommentDescriptionSpecified
        {
            get { return PropertyInspectionRequestCommentDescription != null; }
            set { }
        }

        /// <summary>
        /// Free form commentary about the results of the inspection.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOString PropertyInspectionResultCommentDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyInspectionResultCommentDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyInspectionResultCommentDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyInspectionResultCommentDescriptionSpecified
        {
            get { return PropertyInspectionResultCommentDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the extent of the inspection.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOEnum<PropertyInspectionBase> PropertyInspectionType;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyInspectionType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyInspectionType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyInspectionTypeSpecified
        {
            get { return this.PropertyInspectionType != null && this.PropertyInspectionType.enumValue != PropertyInspectionBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form field used to capture additional information when the selection for Property Inspection Type is Other.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOString PropertyInspectionTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyInspectionTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyInspectionTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyInspectionTypeOtherDescriptionSpecified
        {
            get { return PropertyInspectionTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Used as an attribute on PROPERTY to specify whether the referenced property is the Subject property or a property valuation comparable.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOEnum<ValuationUseBase> ValuationUseType;

        /// <summary>
        /// Gets or sets a value indicating whether the ValuationUseType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ValuationUseType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ValuationUseTypeSpecified
        {
            get { return this.ValuationUseType != null && this.ValuationUseType.enumValue != ValuationUseBase.Blank; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 11)]
        public INSPECTION_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
