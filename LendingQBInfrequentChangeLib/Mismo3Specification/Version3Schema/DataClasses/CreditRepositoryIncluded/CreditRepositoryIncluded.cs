namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CREDIT_REPOSITORY_INCLUDED
    {
        /// <summary>
        /// Gets a value indicating whether the CREDIT_REPOSITORY_INCLUDED container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditRepositoryIncludedEquifaxIndicatorSpecified
                    || this.CreditRepositoryIncludedExperianIndicatorSpecified
                    || this.CreditRepositoryIncludedOtherRepositoryNameSpecified
                    || this.CreditRepositoryIncludedTransUnionIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// This element indicates whether Equifax is included in either the request or response.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIndicator CreditRepositoryIncludedEquifaxIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditRepositoryIncludedEquifaxIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditRepositoryIncludedEquifaxIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditRepositoryIncludedEquifaxIndicatorSpecified
        {
            get { return CreditRepositoryIncludedEquifaxIndicator != null; }
            set { }
        }

        /// <summary>
        /// This element indicates whether Experian is included in either the request or response.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOIndicator CreditRepositoryIncludedExperianIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditRepositoryIncludedExperianIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditRepositoryIncludedExperianIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditRepositoryIncludedExperianIndicatorSpecified
        {
            get { return CreditRepositoryIncludedExperianIndicator != null; }
            set { }
        }

        /// <summary>
        /// This element indicates whether another repository source is included in either the request or response.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString CreditRepositoryIncludedOtherRepositoryName;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditRepositoryIncludedOtherRepositoryName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditRepositoryIncludedOtherRepositoryName element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditRepositoryIncludedOtherRepositoryNameSpecified
        {
            get { return CreditRepositoryIncludedOtherRepositoryName != null; }
            set { }
        }

        /// <summary>
        /// This element indicates whether Trans Union is included in either the request or response.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOIndicator CreditRepositoryIncludedTransUnionIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditRepositoryIncludedTransUnionIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditRepositoryIncludedTransUnionIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditRepositoryIncludedTransUnionIndicatorSpecified
        {
            get { return CreditRepositoryIncludedTransUnionIndicator != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public CREDIT_REPOSITORY_INCLUDED_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
