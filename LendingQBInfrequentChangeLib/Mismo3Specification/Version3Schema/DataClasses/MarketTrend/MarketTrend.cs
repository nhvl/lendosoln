namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class MARKET_TREND
    {
        /// <summary>
        /// Gets a value indicating whether the MARKET_TREND container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.MarketTrendPeriodTypeSpecified
                    || this.MarketTrendsAdverseFinancingIndicatorSpecified
                    || this.MarketTrendsForeclosureActivityIndicatorSpecified
                    || this.MarketTrendsHistoricAnalysisCommentDescriptionSpecified
                    || this.MarketTrendsHistoricPricesTypeSpecified
                    || this.MarketTrendsInterestRatesTypeSpecified
                    || this.MarketTrendsReconciliationCommentDescriptionSpecified
                    || this.MarketTrendsSalesActivityTypeSpecified;
            }
        }

        /// <summary>
        /// Specifies the date range of the real estate market analysis.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<MarketTrendPeriodBase> MarketTrendPeriodType;

        /// <summary>
        /// Gets or sets a value indicating whether the MarketTrendPeriodType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MarketTrendPeriodType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MarketTrendPeriodTypeSpecified
        {
            get { return this.MarketTrendPeriodType != null && this.MarketTrendPeriodType.enumValue != MarketTrendPeriodBase.Blank; }
            set { }
        }

        /// <summary>
        /// Indicates that a trend toward adverse financing conditions is occurring.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOIndicator MarketTrendsAdverseFinancingIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the MarketTrendsAdverseFinancingIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MarketTrendsAdverseFinancingIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool MarketTrendsAdverseFinancingIndicatorSpecified
        {
            get { return MarketTrendsAdverseFinancingIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether the REO or foreclosure activity is significant. 
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOIndicator MarketTrendsForeclosureActivityIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the MarketTrendsForeclosureActivityIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MarketTrendsForeclosureActivityIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool MarketTrendsForeclosureActivityIndicatorSpecified
        {
            get { return MarketTrendsForeclosureActivityIndicator != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe or analyze the historic trends and factors relevant to forecasting the sales price of a property.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString MarketTrendsHistoricAnalysisCommentDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MarketTrendsHistoricAnalysisCommentDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MarketTrendsHistoricAnalysisCommentDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MarketTrendsHistoricAnalysisCommentDescriptionSpecified
        {
            get { return MarketTrendsHistoricAnalysisCommentDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the historic trend of sales prices in a neighborhood (e.g. Increasing, Stable, or Decreasing).
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<MarketTrendsHistoricPricesBase> MarketTrendsHistoricPricesType;

        /// <summary>
        /// Gets or sets a value indicating whether the MarketTrendsHistoricPricesType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MarketTrendsHistoricPricesType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MarketTrendsHistoricPricesTypeSpecified
        {
            get { return this.MarketTrendsHistoricPricesType != null && this.MarketTrendsHistoricPricesType.enumValue != MarketTrendsHistoricPricesBase.Blank; }
            set { }
        }

        /// <summary>
        /// Specifies the mortgage interest rate trend (e.g. Decreasing, Stable, or Increasing).
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOEnum<MarketTrendsInterestRatesBase> MarketTrendsInterestRatesType;

        /// <summary>
        /// Gets or sets a value indicating whether the MarketTrendsInterestRatesType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MarketTrendsInterestRatesType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MarketTrendsInterestRatesTypeSpecified
        {
            get { return this.MarketTrendsInterestRatesType != null && this.MarketTrendsInterestRatesType.enumValue != MarketTrendsInterestRatesBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe or analyze the anticipated trend of market conditions and prices during the marketing time of the prospective subject property. 
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOString MarketTrendsReconciliationCommentDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MarketTrendsReconciliationCommentDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MarketTrendsReconciliationCommentDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MarketTrendsReconciliationCommentDescriptionSpecified
        {
            get { return MarketTrendsReconciliationCommentDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the sale activity trend of properties similar to the subject property (e.g. Decreasing, Stable, Increasing).
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOEnum<MarketTrendsSalesActivityBase> MarketTrendsSalesActivityType;

        /// <summary>
        /// Gets or sets a value indicating whether the MarketTrendsSalesActivityType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MarketTrendsSalesActivityType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MarketTrendsSalesActivityTypeSpecified
        {
            get { return this.MarketTrendsSalesActivityType != null && this.MarketTrendsSalesActivityType.enumValue != MarketTrendsSalesActivityBase.Blank; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 8)]
        public MARKET_TREND_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
