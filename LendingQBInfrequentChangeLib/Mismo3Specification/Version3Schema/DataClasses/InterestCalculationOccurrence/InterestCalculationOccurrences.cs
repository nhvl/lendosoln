namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class INTEREST_CALCULATION_OCCURRENCES
    {
        /// <summary>
        /// Gets a value indicating whether the INTEREST_CALCULATION_OCCURRENCES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.InterestCalculationOccurrenceSpecified;
            }
        }

        /// <summary>
        /// A collection of interest calculation occurrences.
        /// </summary>
        [XmlElement("INTEREST_CALCULATION_OCCURRENCE", Order = 0)]
		public List<INTEREST_CALCULATION_OCCURRENCE> InterestCalculationOccurrence = new List<INTEREST_CALCULATION_OCCURRENCE>();

        /// <summary>
        /// Gets or sets a value indicating whether the InterestCalculationOccurrence element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InterestCalculationOccurrence element has been assigned a value.</value>
        [XmlIgnore]
        public bool InterestCalculationOccurrenceSpecified
        {
            get { return this.InterestCalculationOccurrence != null && this.InterestCalculationOccurrence.Count(i => i != null && i.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public INTEREST_CALCULATION_OCCURRENCES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
