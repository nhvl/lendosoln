namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class INTEREST_CALCULATION_OCCURRENCE
    {
        /// <summary>
        /// Gets a value indicating whether the INTEREST_CALCULATION_OCCURRENCE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CurrentAccruedInterestAmountSpecified
                    || this.DailySimpleInterestDueAmountSpecified
                    || this.ExtensionSpecified
                    || this.InterestDueFromClosingAmountSpecified;
            }
        }

        /// <summary>
        /// The dollar amount of interest accrued on the loan between the last paid installment date and the date reported.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount CurrentAccruedInterestAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the CurrentAccruedInterestAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CurrentAccruedInterestAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool CurrentAccruedInterestAmountSpecified
        {
            get { return CurrentAccruedInterestAmount != null; }
            set { }
        }

        /// <summary>
        /// The amount of interest accrued but not yet paid for Daily Simple Interest loans. This interest is considered overdue interest and is paid first when a payment is applied.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOAmount DailySimpleInterestDueAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the DailySimpleInterestDueAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DailySimpleInterestDueAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool DailySimpleInterestDueAmountSpecified
        {
            get { return DailySimpleInterestDueAmount != null; }
            set { }
        }

        /// <summary>
        /// The amount of interest due with the first payment in addition to the regular payment as a result of the loan funding on a date other than one month prior to the due date (if interest is paid in arrears). This amount will be zero if odd interest was paid at closing.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOAmount InterestDueFromClosingAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the InterestDueFromClosingAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InterestDueFromClosingAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool InterestDueFromClosingAmountSpecified
        {
            get { return InterestDueFromClosingAmount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public INTEREST_CALCULATION_OCCURRENCE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
