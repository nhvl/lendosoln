namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PHASES
    {
        /// <summary>
        /// Gets a value indicating whether the PHASES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.PhaseSpecified;
            }
        }

        /// <summary>
        /// A collection of phases.
        /// </summary>
        [XmlElement("PHASE", Order = 0)]
		public List<PHASE> Phase = new List<PHASE>();

        /// <summary>
        /// Gets or sets a value indicating whether the Phase element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Phase element has been assigned a value.</value>
        [XmlIgnore]
        public bool PhaseSpecified
        {
            get { return this.Phase != null && this.Phase.Count(p => p != null && p.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public PHASES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
