namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class PHASE
    {
        /// <summary>
        /// Gets a value indicating whether the PHASE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.PhaseCarStoragesSpecified
                    || this.PhaseCommonElementsSpecified
                    || this.PhaseConversionSpecified
                    || this.PhaseDetailSpecified
                    || this.PhaseHousingUnitInventoriesSpecified
                    || this.PhaseStructuresSpecified;
            }
        }

        /// <summary>
        /// Car storages in the phase.
        /// </summary>
        [XmlElement("PHASE_CAR_STORAGES", Order = 0)]
        public CAR_STORAGES PhaseCarStorages;

        /// <summary>
        /// Gets or sets a value indicating whether the PhaseCarStorages element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PhaseCarStorages element has been assigned a value.</value>
        [XmlIgnore]
        public bool PhaseCarStoragesSpecified
        {
            get { return this.PhaseCarStorages != null && this.PhaseCarStorages.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Common elements in the phase.
        /// </summary>
        [XmlElement("PHASE_COMMON_ELEMENTS", Order = 1)]
        public COMMON_ELEMENTS PhaseCommonElements;

        /// <summary>
        /// Gets or sets a value indicating whether the PhaseCommonElements element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PhaseCommonElements element has been assigned a value.</value>
        [XmlIgnore]
        public bool PhaseCommonElementsSpecified
        {
            get { return this.PhaseCommonElements != null && this.PhaseCommonElements.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The phase conversion.
        /// </summary>
        [XmlElement("PHASE_CONVERSION", Order = 2)]
        public CONVERSION PhaseConversion;

        /// <summary>
        /// Gets or sets a value indicating whether the PhaseConversion element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PhaseConversion element has been assigned a value.</value>
        [XmlIgnore]
        public bool PhaseConversionSpecified
        {
            get { return this.PhaseConversion != null && this.PhaseConversion.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details about the phase.
        /// </summary>
        [XmlElement("PHASE_DETAIL", Order = 3)]
        public PHASE_DETAIL PhaseDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the PhaseDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PhaseDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool PhaseDetailSpecified
        {
            get { return this.PhaseDetail != null && this.PhaseDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A list of phase housing unit inventories.
        /// </summary>
        [XmlElement("PHASE_HOUSING_UNIT_INVENTORIES", Order = 4)]
        public HOUSING_UNIT_INVENTORIES PhaseHousingUnitInventories;

        /// <summary>
        /// Gets or sets a value indicating whether the PhaseHousingUnitInventories element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PhaseHousingUnitInventories element has been assigned a value.</value>
        [XmlIgnore]
        public bool PhaseHousingUnitInventoriesSpecified
        {
            get { return this.PhaseHousingUnitInventories != null && this.PhaseHousingUnitInventories.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A list of phase structures.
        /// </summary>
        [XmlElement("PHASE_STRUCTURES", Order = 5)]
        public PHASE_STRUCTURES PhaseStructures;

        /// <summary>
        /// Gets or sets a value indicating whether the PhaseStructures element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PhaseStructures element has been assigned a value.</value>
        [XmlIgnore]
        public bool PhaseStructuresSpecified
        {
            get { return this.PhaseStructures != null && this.PhaseStructures.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 6)]
        public PHASE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
