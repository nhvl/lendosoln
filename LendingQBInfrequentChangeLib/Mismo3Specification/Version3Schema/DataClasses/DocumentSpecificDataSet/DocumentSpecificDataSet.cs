namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class DOCUMENT_SPECIFIC_DATA_SET
    {
        /// <summary>
        /// Gets a value indicating whether the DOCUMENT_SPECIFIC_DATA_SET container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                if (Mismo3Utilities.ExceedsThreshold(1,
                    new List<bool> { 
                        this.AssignmentSpecified,
                        this.GFESpecified,
                        this.HUD1Specified,
                        this.IntegratedDisclosureSpecified,
                        this.NoteSpecified,
                        this.NoticeOfRightToCancelSpecified,
                        this.SecurityInstrumentSpecified,
                        this.TILDisclosureSpecified,
                        this.URLASpecified }))
                {
                    throw new System.Xml.Schema.XmlSchemaValidationException(
                        Mismo3Utilities.GetXSDChoiceExceptionMessage(
                        "DOCUMENT_SPECIFIC_DATA_SET",
                        new List<string> { "ASSIGNMENT", "GFE", "HUD1", "INTEGRATED_DISCLOSURE", "NOTE", "NOTICE_OF_RIGHT_TO_CANCEL", "SECURITY_INSTRUMENT", "TIL_DISCLOSURE", "URLA" }));
                }

                return this.AssignmentSpecified
                    || this.DocumentClassesSpecified
                    || this.ExecutionSpecified
                    || this.ExtensionSpecified
                    || this.GFESpecified
                    || this.HUD1Specified
                    || this.IntegratedDisclosureSpecified
                    || this.NoteSpecified
                    || this.NoticeOfRightToCancelSpecified
                    || this.RecordingEndorsementsSpecified
                    || this.SecurityInstrumentSpecified
                    || this.TILDisclosureSpecified
                    || this.URLASpecified;
            }
        }

        /// <summary>
        /// An instance of the assignment document.
        /// </summary>
        [XmlElement("ASSIGNMENT", Order = 0)]
        public ASSIGNMENT Assignment;

        /// <summary>
        /// Gets or sets a value indicating whether the Assignment element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Assignment element has been assigned a value.</value>
        [XmlIgnore]
        public bool AssignmentSpecified
        {
            get { return this.Assignment != null && this.Assignment.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// An instance of the GFE.
        /// </summary>
        [XmlElement("GFE", Order = 1)]
        public GFE GFE;

        /// <summary>
        /// Gets or sets a value indicating whether the GFE element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GFE element has been assigned a value.</value>
        [XmlIgnore]
        public bool GFESpecified
        {
            get { return this.GFE != null && this.GFE.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// An instance of the HUD1.
        /// </summary>
        [XmlElement("HUD1", Order = 2)]
        public HUD1 HUD1;

        /// <summary>
        /// Gets or sets a value indicating whether the HUD1 element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HUD1 element has been assigned a value.</value>
        [XmlIgnore]
        public bool HUD1Specified
        {
            get { return this.HUD1 != null && this.HUD1.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// An instance of the integrated disclosure.
        /// </summary>
        [XmlElement("INTEGRATED_DISCLOSURE", Order = 3)]
        public INTEGRATED_DISCLOSURE IntegratedDisclosure;

        /// <summary>
        /// Gets or sets a value indicating whether the IntegratedDisclosure element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IntegratedDisclosure element has been assigned a value.</value>
        [XmlIgnore]
        public bool IntegratedDisclosureSpecified
        {
            get { return this.IntegratedDisclosure != null && this.IntegratedDisclosure.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// An instance of the note.
        /// </summary>
        [XmlElement("NOTE", Order = 4)]
        public NOTE Note;

        /// <summary>
        /// Gets or sets a value indicating whether the Note element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Note element has been assigned a value.</value>
        [XmlIgnore]
        public bool NoteSpecified
        {
            get { return this.Note != null && this.Note.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// An instance of the notice of right to cancel.
        /// </summary>
        [XmlElement("NOTICE_OF_RIGHT_TO_CANCEL", Order = 5)]
        public NOTICE_OF_RIGHT_TO_CANCEL NoticeOfRightToCancel;

        /// <summary>
        /// Gets or sets a value indicating whether the NoticeOfRightToCancel element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NoticeOfRightToCancel element has been assigned a value.</value>
        [XmlIgnore]
        public bool NoticeOfRightToCancelSpecified
        {
            get { return this.NoticeOfRightToCancel != null && this.NoticeOfRightToCancel.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// An instance of the security instrument.
        /// </summary>
        [XmlElement("SECURITY_INSTRUMENT", Order = 6)]
        public SECURITY_INSTRUMENT SecurityInstrument;

        /// <summary>
        /// Gets or sets a value indicating whether the SecurityInstrument element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SecurityInstrument element has been assigned a value.</value>
        [XmlIgnore]
        public bool SecurityInstrumentSpecified
        {
            get { return this.SecurityInstrument != null && this.SecurityInstrument.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// An instance of the TIL disclosure.
        /// </summary>
        [XmlElement("TIL_DISCLOSURE", Order = 7)]
        public TIL_DISCLOSURE TILDisclosure;

        /// <summary>
        /// Gets or sets a value indicating whether the TILDisclosure element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TILDisclosure element has been assigned a value.</value>
        [XmlIgnore]
        public bool TILDisclosureSpecified
        {
            get { return this.TILDisclosure != null && this.TILDisclosure.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// An instance of the URLA.
        /// </summary>
        [XmlElement("URLA", Order = 8)]
        public URLA URLA;

        /// <summary>
        /// Gets or sets a value indicating whether the URLA element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the URLA element has been assigned a value.</value>
        [XmlIgnore]
        public bool URLASpecified
        {
            get { return this.URLA != null && this.URLA.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Document classes in the data set.
        /// </summary>
        [XmlElement("DOCUMENT_CLASSES", Order = 9)]
        public DOCUMENT_CLASSES DocumentClasses;

        /// <summary>
        /// Gets or sets a value indicating whether the DocumentClasses element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DocumentClasses element has been assigned a value.</value>
        [XmlIgnore]
        public bool DocumentClassesSpecified
        {
            get { return this.DocumentClasses != null && this.DocumentClasses.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Execution of the dataset.
        /// </summary>
        [XmlElement("EXECUTION", Order = 10)]
        public EXECUTION Execution;

        /// <summary>
        /// Gets or sets a value indicating whether the Execution element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Execution element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExecutionSpecified
        {
            get { return this.Execution != null && this.Execution.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Endorsements of the recording.
        /// </summary>
        [XmlElement("RECORDING_ENDORSEMENTS", Order = 11)]
        public RECORDING_ENDORSEMENTS RecordingEndorsements;

        /// <summary>
        /// Gets or sets a value indicating whether the RecordingEndorsements element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RecordingEndorsements element has been assigned a value.</value>
        [XmlIgnore]
        public bool RecordingEndorsementsSpecified
        {
            get { return this.RecordingEndorsements != null && this.RecordingEndorsements.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 12)]
        public DOCUMENT_SPECIFIC_DATA_SET_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null && Extension.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
