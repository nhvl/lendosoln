namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class DOCUMENT_SPECIFIC_DATA_SETS
    {
        /// <summary>
        /// Gets a value indicating whether the DOCUMENT_SPECIFIC_DATA_SETS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DocumentSpecificDataSetSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of document specific data sets.
        /// </summary>
        [XmlElement("DOCUMENT_SPECIFIC_DATA_SET", Order = 0)]
		public List<DOCUMENT_SPECIFIC_DATA_SET> DocumentSpecificDataSet = new List<DOCUMENT_SPECIFIC_DATA_SET>();

        /// <summary>
        /// Gets or sets a value indicating whether the DocumentSpecificDataSet element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DocumentSpecificDataSet element has been assigned a value.</value>
        [XmlIgnore]
        public bool DocumentSpecificDataSetSpecified
        {
            get { return this.DocumentSpecificDataSet != null && this.DocumentSpecificDataSet.Count(d => d != null && d.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public DOCUMENT_SPECIFIC_DATA_SETS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
