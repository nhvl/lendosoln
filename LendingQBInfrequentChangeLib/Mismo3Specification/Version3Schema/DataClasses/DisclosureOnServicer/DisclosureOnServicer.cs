namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class DISCLOSURE_ON_SERVICER
    {
        /// <summary>
        /// Gets a value indicating whether the DISCLOSURE_ON_SERVICER container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ServicerDaysOfTheWeekDescriptionSpecified
                    || this.ServicerDirectInquiryToDescriptionSpecified
                    || this.ServicerHoursOfTheDayDescriptionSpecified
                    || this.ServicerInquiryTelephoneValueSpecified
                    || this.ServicerPaymentAcceptanceEndDateSpecified
                    || this.ServicerPaymentAcceptanceStartDateSpecified
                    || this.ServicingTransferEffectiveDateSpecified
                    || this.TransferOfServicingDisclosureTypeOtherDescriptionSpecified
                    || this.TransferOfServicingDisclosureTypeSpecified;
            }
        }

        /// <summary>
        /// The normal business days of the week to contact Servicer with inquiries, as reflected on the Notice Of Assignment, Sale, Or Transfer Of Servicing Rights document.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString ServicerDaysOfTheWeekDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ServicerDaysOfTheWeekDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServicerDaysOfTheWeekDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServicerDaysOfTheWeekDescriptionSpecified
        {
            get { return ServicerDaysOfTheWeekDescription != null; }
            set { }
        }

        /// <summary>
        /// The description of the party to contact with servicing related questions, as reflected on the Notice Of Assignment, Sale, Or Transfer Of Servicing Rights document.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString ServicerDirectInquiryToDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ServicerDirectInquiryToDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServicerDirectInquiryToDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServicerDirectInquiryToDescriptionSpecified
        {
            get { return ServicerDirectInquiryToDescription != null; }
            set { }
        }

        /// <summary>
        /// The normal business hours to contact Servicer with inquiries, as reflected on the Notice Of Assignment, Sale, Or Transfer Of Servicing Rights document.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString ServicerHoursOfTheDayDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ServicerHoursOfTheDayDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServicerHoursOfTheDayDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServicerHoursOfTheDayDescriptionSpecified
        {
            get { return ServicerHoursOfTheDayDescription != null; }
            set { }
        }

        /// <summary>
        /// The telephone number for a servicer inquiry.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMONumericString ServicerInquiryTelephoneValue;

        /// <summary>
        /// Gets or sets a value indicating whether the telephone number element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the telephone number element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServicerInquiryTelephoneValueSpecified
        {
            get { return ServicerInquiryTelephoneValue != null; }
            set { }
        }

        /// <summary>
        /// The date that the current Servicer will stop accepting payments from the borrower for the mortgage loan, as reflected on the Notice Of Assignment, Sale, Or Transfer Of Servicing Rights document.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMODate ServicerPaymentAcceptanceEndDate;

        /// <summary>
        /// Gets or sets a value indicating whether the ServicerPaymentAcceptanceEndDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServicerPaymentAcceptanceEndDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServicerPaymentAcceptanceEndDateSpecified
        {
            get { return ServicerPaymentAcceptanceEndDate != null; }
            set { }
        }

        /// <summary>
        /// The date that the new Servicer will start accepting payments from the borrower for the mortgage loan, as reflected on the Notice Of Assignment, Sale, Or Transfer Of Servicing Rights document.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMODate ServicerPaymentAcceptanceStartDate;

        /// <summary>
        /// Gets or sets a value indicating whether the ServicerPaymentAcceptanceStartDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServicerPaymentAcceptanceStartDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServicerPaymentAcceptanceStartDateSpecified
        {
            get { return ServicerPaymentAcceptanceStartDate != null; }
            set { }
        }

        /// <summary>
        /// The date at which the transfer of servicing is effective as reflected on the Notice Of Assignment, Sale, Or Transfer of Servicing Rights document.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMODate ServicingTransferEffectiveDate;

        /// <summary>
        /// Gets or sets a value indicating whether the ServicingTransferEffectiveDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServicingTransferEffectiveDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServicingTransferEffectiveDateSpecified
        {
            get { return ServicingTransferEffectiveDate != null; }
            set { }
        }

        /// <summary>
        /// Specifies detail about the party servicing the loan as stated on the Servicing Disclosure Statement document.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOEnum<TransferOfServicingDisclosureBase> TransferOfServicingDisclosureType;

        /// <summary>
        /// Gets or sets a value indicating whether the TransferOfServicingDisclosureType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TransferOfServicingDisclosureType element has been assigned a value.</value>
        [XmlIgnore]
        public bool TransferOfServicingDisclosureTypeSpecified
        {
            get { return this.TransferOfServicingDisclosureType != null && this.TransferOfServicingDisclosureType.enumValue != TransferOfServicingDisclosureBase.Blank; }
            set { }
        }

        /// <summary>
        /// When Other is specified in Transfer Of Servicing Disclosure Type, this data element contains the description.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOString TransferOfServicingDisclosureTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the TransferOfServicingDisclosureTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TransferOfServicingDisclosureTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool TransferOfServicingDisclosureTypeOtherDescriptionSpecified
        {
            get { return TransferOfServicingDisclosureTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 9)]
        public DISCLOSURE_ON_SERVICER_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
