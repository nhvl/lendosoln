namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class DELINQUENCY_SUMMARY
    {
        /// <summary>
        /// Gets a value indicating whether the DELINQUENCY_SUMMARY container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DelinquentPaymentCountSpecified
                    || this.DelinquentPaymentsOverPastTwelveMonthsCountSpecified
                    || this.ExtensionSpecified
                    || this.LoanDelinquencyHistoryPeriodMonthsCountSpecified
                    || this.LoanReinstateTotalAmountSpecified
                    || this.NetLiquidationProceedsAmountSpecified
                    || this.OnTimePaymentCountSpecified
                    || this.PaymentDelinquentDaysCountSpecified
                    || this.TotalDelinquentInterestAmountSpecified
                    || this.TotalDelinquentInterestReportedDateSpecified
                    || this.TotalLiquidationExpenseAmountSpecified;
            }
        }

        /// <summary>
        /// A delinquent payment is an unpaid payment. This is a count of the currently unpaid payments.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOCount DelinquentPaymentCount;

        /// <summary>
        /// Gets or sets a value indicating whether the DelinquentPaymentCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DelinquentPaymentCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool DelinquentPaymentCountSpecified
        {
            get { return DelinquentPaymentCount != null; }
            set { }
        }

        /// <summary>
        /// The number of times during the past twelve months that the payment on the subject loan was delinquent.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOCount DelinquentPaymentsOverPastTwelveMonthsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the DelinquentPaymentsOverPastTwelveMonthsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DelinquentPaymentsOverPastTwelveMonthsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool DelinquentPaymentsOverPastTwelveMonthsCountSpecified
        {
            get { return DelinquentPaymentsOverPastTwelveMonthsCount != null; }
            set { }
        }

        /// <summary>
        /// The duration in months of the period for which the history of the late mortgage is provided.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOCount LoanDelinquencyHistoryPeriodMonthsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanDelinquencyHistoryPeriodMonthsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanDelinquencyHistoryPeriodMonthsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanDelinquencyHistoryPeriodMonthsCountSpecified
        {
            get { return LoanDelinquencyHistoryPeriodMonthsCount != null; }
            set { }
        }

        /// <summary>
        /// The total amount required to reinstate the loan to current status.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOAmount LoanReinstateTotalAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanReinstateTotalAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanReinstateTotalAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanReinstateTotalAmountSpecified
        {
            get { return LoanReinstateTotalAmount != null; }
            set { }
        }

        /// <summary>
        /// Amounts received in connection with the taking of a mortgage property by exercising the power of eminent domain or condemnation, liquidation of a defaulted loan prior to taking title, REO disposition, or any other purchase/repurchase/sale of a defaulted mortgage loan or REO property, plus the sum of all insurance proceeds and any other sources of revenue, less all unrecovered expenses associated with the property.  Revenues may include proceeds from a loan or REO disposition (sale), or NOI on a defaulted mortgage loan prior to becoming an REO.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOAmount NetLiquidationProceedsAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the NetLiquidationProceedsAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NetLiquidationProceedsAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool NetLiquidationProceedsAmountSpecified
        {
            get { return NetLiquidationProceedsAmount != null; }
            set { }
        }

        /// <summary>
        /// The number of on time payments since origination or the last delinquency.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOCount OnTimePaymentCount;

        /// <summary>
        /// Gets or sets a value indicating whether the OnTimePaymentCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OnTimePaymentCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool OnTimePaymentCountSpecified
        {
            get { return OnTimePaymentCount != null; }
            set { }
        }

        /// <summary>
        /// The number of days the borrower payment is currently past due.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOCount PaymentDelinquentDaysCount;

        /// <summary>
        /// Gets or sets a value indicating whether the PaymentDelinquentDaysCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PaymentDelinquentDaysCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PaymentDelinquentDaysCountSpecified
        {
            get { return PaymentDelinquentDaysCount != null; }
            set { }
        }

        /// <summary>
        /// Sum of all past due interest from the last paid installment due date (DDLPI) to current date. It is computed based on scheduled amortization.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOAmount TotalDelinquentInterestAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the TotalDelinquentInterestAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TotalDelinquentInterestAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TotalDelinquentInterestAmountSpecified
        {
            get { return TotalDelinquentInterestAmount != null; }
            set { }
        }

        /// <summary>
        /// Date on which Total Delinquent Interest Amount was reported.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMODate TotalDelinquentInterestReportedDate;

        /// <summary>
        /// Gets or sets a value indicating whether the TotalDelinquentInterestReportedDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TotalDelinquentInterestReportedDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool TotalDelinquentInterestReportedDateSpecified
        {
            get { return TotalDelinquentInterestReportedDate != null; }
            set { }
        }

        /// <summary>
        /// Expenses for a defaulted mortgage loan which have not been recovered from the investor or trust by the servicer in a prior activity period and are being charged to the investor or trust during this period. 
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOAmount TotalLiquidationExpenseAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the TotalLiquidationExpenseAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TotalLiquidationExpenseAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TotalLiquidationExpenseAmountSpecified
        {
            get { return TotalLiquidationExpenseAmount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 10)]
        public DELINQUENCY_SUMMARY_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
