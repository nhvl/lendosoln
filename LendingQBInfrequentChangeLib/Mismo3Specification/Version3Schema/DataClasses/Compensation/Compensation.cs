namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class COMPENSATION
    {
        /// <summary>
        /// Gets a value indicating whether the COMPENSATION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CompensationActualAmountSpecified
                    || this.CompensationEstimatedAmountSpecified
                    || this.CompensationPaidByTypeOtherDescriptionSpecified
                    || this.CompensationPaidByTypeSpecified
                    || this.CompensationPaidToTypeOtherDescriptionSpecified
                    || this.CompensationPaidToTypeSpecified
                    || this.CompensationPercentSpecified
                    || this.CompensationPlanIdentifierSpecified
                    || this.CompensationPlanMaximumAmountSpecified
                    || this.CompensationPlanMinimumAmountSpecified
                    || this.CompensationSpecifiedFixedAmountSpecified
                    || this.CompensationTypeOtherDescriptionSpecified
                    || this.CompensationTypeSpecified
                    || this.ExtensionSpecified
                    || this.GFEAggregationTypeOtherDescriptionSpecified
                    || this.GFEAggregationTypeSpecified
                    || this.GFEDisclosedCompensationAmountSpecified
                    || this.IntegratedDisclosureLineNumberValueSpecified
                    || this.IntegratedDisclosureSectionTypeOtherDescriptionSpecified
                    || this.IntegratedDisclosureSectionTypeSpecified
                    || this.IntegratedDisclosureSubsectionTypeOtherDescriptionSpecified
                    || this.IntegratedDisclosureSubsectionTypeSpecified
                    || this.PaymentIncludedInAPRIndicatorSpecified
                    || this.PaymentIncludedInJurisdictionHighCostIndicatorSpecified
                    || this.RegulationZPointsAndFeesIndicatorSpecified;
            }
        }

        /// <summary>
        /// The actual dollar amount of compensation paid.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount CompensationActualAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the CompensationActualAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CompensationActualAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool CompensationActualAmountSpecified
        {
            get { return CompensationActualAmount != null; }
            set { }
        }

        /// <summary>
        /// The estimated dollar amount of compensation to be paid.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOAmount CompensationEstimatedAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the CompensationEstimatedAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CompensationEstimatedAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool CompensationEstimatedAmountSpecified
        {
            get { return CompensationEstimatedAmount != null; }
            set { }
        }

        /// <summary>
        /// Identifies the party responsible for paying the compensation amount due. 
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<CompensationPaidByBase> CompensationPaidByType;

        /// <summary>
        /// Gets or sets a value indicating whether the CompensationPaidByType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CompensationPaidByType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CompensationPaidByTypeSpecified
        {
            get { return this.CompensationPaidByType != null && this.CompensationPaidByType.enumValue != CompensationPaidByBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the party that has paid the compensation when Other is selected as the Compensation Paid By Type.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString CompensationPaidByTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the CompensationPaidByTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CompensationPaidByTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool CompensationPaidByTypeOtherDescriptionSpecified
        {
            get { return CompensationPaidByTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Identifies the party to which payment of the compensation amount will be made.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<CompensationPaidToBase> CompensationPaidToType;

        /// <summary>
        /// Gets or sets a value indicating whether the CompensationPaidToType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CompensationPaidToType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CompensationPaidToTypeSpecified
        {
            get { return this.CompensationPaidToType != null && this.CompensationPaidToType.enumValue != CompensationPaidToBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the party to which compensation has been paid when Other is selected as the Compensation Paid To Type.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString CompensationPaidToTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the CompensationPaidToTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CompensationPaidToTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool CompensationPaidToTypeOtherDescriptionSpecified
        {
            get { return CompensationPaidToTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The percent rate of the loan amount used to calculated the dollar amount of the compensation to be paid.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOPercent CompensationPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the CompensationPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CompensationPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool CompensationPercentSpecified
        {
            get { return CompensationPercent != null; }
            set { }
        }

        /// <summary>
        /// A unique identifier assigned to a compensation plan by the party offering the plan. The party assigning the identifier can be specified using the IdentifierOwnerURI attribute.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOIdentifier CompensationPlanIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the CompensationPlanIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CompensationPlanIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool CompensationPlanIdentifierSpecified
        {
            get { return CompensationPlanIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The maximum amount of compensation that may be paid as defined by the compensation plan.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOAmount CompensationPlanMaximumAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the CompensationPlanMaximumAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CompensationPlanMaximumAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool CompensationPlanMaximumAmountSpecified
        {
            get { return CompensationPlanMaximumAmount != null; }
            set { }
        }

        /// <summary>
        /// The minimum amount of compensation that may be paid as defined by the compensation plan.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOAmount CompensationPlanMinimumAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the CompensationPlanMinimumAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CompensationPlanMinimumAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool CompensationPlanMinimumAmountSpecified
        {
            get { return CompensationPlanMinimumAmount != null; }
            set { }
        }

        /// <summary>
        /// The specified fixed dollar portion of the total amount of compensation. This is used when the Compensation Amount is made up of a percentage (stated in the Compensation Percent) plus a specified fixed dollar amount.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOAmount CompensationSpecifiedFixedAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the CompensationSpecifiedFixedAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CompensationSpecifiedFixedAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool CompensationSpecifiedFixedAmountSpecified
        {
            get { return CompensationSpecifiedFixedAmount != null; }
            set { }
        }

        /// <summary>
        /// Identifies the type(s) of compensation being paid in the loan transaction.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOEnum<CompensationBase> CompensationType;

        /// <summary>
        /// Gets or sets a value indicating whether the CompensationType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CompensationType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CompensationTypeSpecified
        {
            get { return this.CompensationType != null && this.CompensationType.enumValue != CompensationBase.Blank; }
            set { }
        }

        /// <summary>
        /// When Other is specified as the Compensation Type, this element contains the description.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOString CompensationTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the CompensationTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CompensationTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool CompensationTypeOtherDescriptionSpecified
        {
            get { return CompensationTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the fee or charges group, combined fee or charges category, or compensation category, to which the individual fee or charge should be associated, as reflected on a single instance of the GFE.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOEnum<GFEAggregationBase> GFEAggregationType;

        /// <summary>
        /// Gets or sets a value indicating whether the GFEAggregationType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GFEAggregationType element has been assigned a value.</value>
        [XmlIgnore]
        public bool GFEAggregationTypeSpecified
        {
            get { return this.GFEAggregationType != null && this.GFEAggregationType.enumValue != GFEAggregationBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for GFE Aggregation Type.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOString GFEAggregationTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the GFEAggregationTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GFEAggregationTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool GFEAggregationTypeOtherDescriptionSpecified
        {
            get { return GFEAggregationTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of compensation paid to the broker and/or lender in the loan transaction, as disclosed on a single instance of the GFE.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOAmount GFEDisclosedCompensationAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the GFEDisclosedCompensationAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GFEDisclosedCompensationAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool GFEDisclosedCompensationAmountSpecified
        {
            get { return GFEDisclosedCompensationAmount != null; }
            set { }
        }

        /// <summary>
        /// The line identifier for the item as represented in a section of the Integrated Disclosure form.
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMOValue IntegratedDisclosureLineNumberValue;

        /// <summary>
        /// Gets or sets a value indicating whether the IntegratedDisclosureLineNumberValue element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IntegratedDisclosureLineNumberValue element has been assigned a value.</value>
        [XmlIgnore]
        public bool IntegratedDisclosureLineNumberValueSpecified
        {
            get { return IntegratedDisclosureLineNumberValue != null; }
            set { }
        }

        /// <summary>
        /// The title or description used to identify a primary section of the integrated disclosure document.
        /// </summary>
        [XmlElement(Order = 17)]
        public MISMOEnum<IntegratedDisclosureSectionBase> IntegratedDisclosureSectionType;

        /// <summary>
        /// Gets or sets a value indicating whether the IntegratedDisclosureSectionType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IntegratedDisclosureSectionType element has been assigned a value.</value>
        [XmlIgnore]
        public bool IntegratedDisclosureSectionTypeSpecified
        {
            get { return this.IntegratedDisclosureSectionType != null && this.IntegratedDisclosureSectionType.enumValue != IntegratedDisclosureSectionBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture additional information when Other is selected for Integrated Disclosure Section Type.
        /// </summary>
        [XmlElement(Order = 18)]
        public MISMOString IntegratedDisclosureSectionTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the IntegratedDisclosureSectionTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IntegratedDisclosureSectionTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool IntegratedDisclosureSectionTypeOtherDescriptionSpecified
        {
            get { return IntegratedDisclosureSectionTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The title or description used to identify a secondary or subsection of the integrated disclosure document.
        /// </summary>
        [XmlElement(Order = 19)]
        public MISMOEnum<IntegratedDisclosureSubsectionBase> IntegratedDisclosureSubsectionType;

        /// <summary>
        /// Gets or sets a value indicating whether the IntegratedDisclosureSubsectionType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IntegratedDisclosureSubsectionType element has been assigned a value.</value>
        [XmlIgnore]
        public bool IntegratedDisclosureSubsectionTypeSpecified
        {
            get { return this.IntegratedDisclosureSubsectionType != null && this.IntegratedDisclosureSubsectionType.enumValue != IntegratedDisclosureSubsectionBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture additional information when Other is selected for Integrated Disclosure Subsection Type.
        /// </summary>
        [XmlElement(Order = 20)]
        public MISMOString IntegratedDisclosureSubsectionTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the IntegratedDisclosureSubsectionTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IntegratedDisclosureSubsectionTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool IntegratedDisclosureSubsectionTypeOtherDescriptionSpecified
        {
            get { return IntegratedDisclosureSubsectionTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates fee is to be included in APR calculations.
        /// </summary>
        [XmlElement(Order = 21)]
        public MISMOIndicator PaymentIncludedInAPRIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PaymentIncludedInAPRIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PaymentIncludedInAPRIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PaymentIncludedInAPRIndicatorSpecified
        {
            get { return PaymentIncludedInAPRIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates the amount of the payment is included in the points and fees calculation appropriated to the jurisdiction of the subject property.
        /// </summary>
        [XmlElement(Order = 22)]
        public MISMOIndicator PaymentIncludedInJurisdictionHighCostIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PaymentIncludedInJurisdictionHighCostIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PaymentIncludedInJurisdictionHighCostIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PaymentIncludedInJurisdictionHighCostIndicatorSpecified
        {
            get { return PaymentIncludedInJurisdictionHighCostIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that item is included in the Regulation Z Points and Fees calculation.
        /// </summary>
        [XmlElement(Order = 23)]
        public MISMOIndicator RegulationZPointsAndFeesIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the RegulationZPointsAndFeesIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RegulationZPointsAndFeesIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool RegulationZPointsAndFeesIndicatorSpecified
        {
            get { return RegulationZPointsAndFeesIndicator != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 24)]
        public COMPENSATION_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
