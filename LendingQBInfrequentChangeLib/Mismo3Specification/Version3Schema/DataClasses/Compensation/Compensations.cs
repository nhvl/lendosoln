namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class COMPENSATIONS
    {
        /// <summary>
        /// Gets a value indicating whether the COMPENSATIONS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CompensationSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of compensation.
        /// </summary>
        [XmlElement("COMPENSATION", Order = 0)]
		public List<COMPENSATION> Compensation = new List<COMPENSATION>();

        /// <summary>
        /// Gets or sets a value indicating whether the Compensation element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Compensation element has been assigned a value.</value>
        [XmlIgnore]
        public bool CompensationSpecified
        {
            get { return this.Compensation != null && this.Compensation.Count(c => c != null && c.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public COMPENSATIONS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
