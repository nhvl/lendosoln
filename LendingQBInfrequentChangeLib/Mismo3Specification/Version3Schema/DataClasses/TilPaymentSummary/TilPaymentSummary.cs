namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class TIL_PAYMENT_SUMMARY
    {
        /// <summary>
        /// Gets a value indicating whether the TIL_PAYMENT_SUMMARY container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.TilPaymentSummaryDetailSpecified
                    || this.TilPaymentSummaryItemsSpecified;
            }
        }

        /// <summary>
        /// Details on a TIL payment summary.
        /// </summary>
        [XmlElement("TIL_PAYMENT_SUMMARY_DETAIL", Order = 0)]
        public TIL_PAYMENT_SUMMARY_DETAIL TilPaymentSummaryDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the TILPaymentSummaryDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TILPaymentSummaryDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool TilPaymentSummaryDetailSpecified
        {
            get { return this.TilPaymentSummaryDetail != null && this.TilPaymentSummaryDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Summary items on a TIL payment.
        /// </summary>
        [XmlElement("TIL_PAYMENT_SUMMARY_ITEMS", Order = 1)]
        public TIL_PAYMENT_SUMMARY_ITEMS TilPaymentSummaryItems;

        /// <summary>
        /// Gets or sets a value indicating whether the TILPaymentSummaryItems element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TILPaymentSummaryItems element has been assigned a value.</value>
        [XmlIgnore]
        public bool TilPaymentSummaryItemsSpecified
        {
            get { return this.TilPaymentSummaryItems != null && this.TilPaymentSummaryItems.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public TIL_PAYMENT_SUMMARY_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
