namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class TIL_PAYMENT_SUMMARY_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the TIL_PAYMENT_SUMMARY_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.MinimumPaymentOnlyAdditionalBorrowedAmountSpecified
                    || this.MinimumPaymentOnlyAdditionalBorrowedDateSpecified
                    || this.NonPrincipalAndInterestPaymentIncludesMortgageInsuranceIndicatorSpecified
                    || this.NonPrincipalAndInterestPaymentIncludesPrivateMortgageInsuranceIndicatorSpecified
                    || this.NonPrincipalAndInterestPaymentIncludesTaxesAndInsuranceEscrowsIndicatorSpecified;
            }
        }

        /// <summary>
        /// For a negative amortization loan, the additional amount by which the unpaid principal loan balance will increase if only minimum payments are made.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount MinimumPaymentOnlyAdditionalBorrowedAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the MinimumPaymentOnlyAdditionalBorrowedAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MinimumPaymentOnlyAdditionalBorrowedAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool MinimumPaymentOnlyAdditionalBorrowedAmountSpecified
        {
            get { return MinimumPaymentOnlyAdditionalBorrowedAmount != null; }
            set { }
        }

        /// <summary>
        /// For a negative amortization loan, the date by which the unpaid principal loan balance will increase if only minimum payments are made.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMODate MinimumPaymentOnlyAdditionalBorrowedDate;

        /// <summary>
        /// Gets or sets a value indicating whether the MinimumPaymentOnlyAdditionalBorrowedDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MinimumPaymentOnlyAdditionalBorrowedDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool MinimumPaymentOnlyAdditionalBorrowedDateSpecified
        {
            get { return MinimumPaymentOnlyAdditionalBorrowedDate != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates a portion of the non-principal-and-interest payment amount reflected in the TIL Disclosure document summary table includes a payment amount for FHA mortgage insurance.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOIndicator NonPrincipalAndInterestPaymentIncludesMortgageInsuranceIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the NonPrincipalAndInterestPaymentIncludesMortgageInsuranceIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NonPrincipalAndInterestPaymentIncludesMortgageInsuranceIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool NonPrincipalAndInterestPaymentIncludesMortgageInsuranceIndicatorSpecified
        {
            get { return NonPrincipalAndInterestPaymentIncludesMortgageInsuranceIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates a portion of the non-principal-and-interest payment amount reflected in the TIL Disclosure document summary table includes a payment amount for private mortgage insurance.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOIndicator NonPrincipalAndInterestPaymentIncludesPrivateMortgageInsuranceIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the NonPrincipalAndInterestPaymentIncludesPrivateMortgageInsuranceIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NonPrincipalAndInterestPaymentIncludesPrivateMortgageInsuranceIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool NonPrincipalAndInterestPaymentIncludesPrivateMortgageInsuranceIndicatorSpecified
        {
            get { return NonPrincipalAndInterestPaymentIncludesPrivateMortgageInsuranceIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates a portion of the non-principal-and-interest payment amount reflected in the TIL Disclosure document summary table includes taxes and insurance escrow payments.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOIndicator NonPrincipalAndInterestPaymentIncludesTaxesAndInsuranceEscrowsIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the NonPrincipalAndInterestPaymentIncludesTaxesAndInsuranceEscrowsIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NonPrincipalAndInterestPaymentIncludesTaxesAndInsuranceEscrowsIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool NonPrincipalAndInterestPaymentIncludesTaxesAndInsuranceEscrowsIndicatorSpecified
        {
            get { return NonPrincipalAndInterestPaymentIncludesTaxesAndInsuranceEscrowsIndicator != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 5)]
        public TIL_PAYMENT_SUMMARY_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
