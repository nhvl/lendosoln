namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class COMPARABLE_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the COMPARABLE_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AdjustedSalesPriceAmountSpecified
                    || this.AdjustedSalesPricePerBedroomAmountSpecified
                    || this.AdjustedSalesPricePerRoomAmountSpecified
                    || this.AdjustedSalesPricePerUnitAmountSpecified
                    || this.ClosedSaleCommentDescriptionSpecified
                    || this.ComparableIdentifierSpecified
                    || this.ComparableListingCommentDescriptionSpecified
                    || this.ComparablePropertyCommentDescriptionSpecified
                    || this.ComparableSaleFurnishedIndicatorSpecified
                    || this.ComparableTypeOtherDescriptionSpecified
                    || this.ComparableTypeSpecified
                    || this.CompetitiveListingsCommentDescriptionSpecified
                    || this.ContractDateUnknownIndicatorSpecified
                    || this.ExtensionSpecified
                    || this.GrossMonthlyRentMultiplierPercentSpecified
                    || this.MLSNumberIdentifierSpecified
                    || this.MonthlyRentAmountSpecified
                    || this.MortgageTypeOtherDescriptionSpecified
                    || this.MortgageTypeSpecified
                    || this.NoFinancingTransactionIndicatorSpecified
                    || this.PricePerSquareFootAmountSpecified
                    || this.ProximityToSubjectDescriptionSpecified
                    || this.RentControlStatusTypeSpecified
                    || this.SalePriceTotalAdjustmentAmountSpecified
                    || this.SalePriceTotalAdjustmentNetPercentSpecified
                    || this.SalesPricePerBedroomAmountSpecified
                    || this.SalesPricePerGrossBuildingAreaAmountSpecified
                    || this.SalesPricePerGrossLivingAreaAmountSpecified
                    || this.SalesPricePerOwnershipShareAmountSpecified
                    || this.SalesPricePerRoomAmountSpecified
                    || this.SalesPricePerUnitAmountSpecified
                    || this.SalesPriceToListPriceRatePercentSpecified
                    || this.SalesPriceTotalAdjustmentGrossPercentSpecified;
            }
        }

        /// <summary>
        /// The sales price of a property after it has been adjusted during the application of the Sales Comparison approach for property valuation.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount AdjustedSalesPriceAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the AdjustedSalesPriceAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AdjustedSalesPriceAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool AdjustedSalesPriceAmountSpecified
        {
            get { return AdjustedSalesPriceAmount != null; }
            set { }
        }

        /// <summary>
        /// The dollar value of a bedroom determined by dividing the adjusted sales price by the number of bedrooms in a property. 
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOAmount AdjustedSalesPricePerBedroomAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the AdjustedSalesPricePerBedroomAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AdjustedSalesPricePerBedroomAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool AdjustedSalesPricePerBedroomAmountSpecified
        {
            get { return AdjustedSalesPricePerBedroomAmount != null; }
            set { }
        }

        /// <summary>
        /// The dollar value of a room determined by dividing the adjusted sales price by the number of rooms in a property. 
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOAmount AdjustedSalesPricePerRoomAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the AdjustedSalesPricePerRoomAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AdjustedSalesPricePerRoomAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool AdjustedSalesPricePerRoomAmountSpecified
        {
            get { return AdjustedSalesPricePerRoomAmount != null; }
            set { }
        }

        /// <summary>
        /// The dollar value of a living unit determined by dividing the adjusted sales price by the number of living units in a multi-unit property. 
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOAmount AdjustedSalesPricePerUnitAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the AdjustedSalesPricePerUnitAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AdjustedSalesPricePerUnitAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool AdjustedSalesPricePerUnitAmountSpecified
        {
            get { return AdjustedSalesPricePerUnitAmount != null; }
            set { }
        }

        /// <summary>
        /// Text comments about the closed sale information.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString ClosedSaleCommentDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ClosedSaleCommentDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ClosedSaleCommentDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ClosedSaleCommentDescriptionSpecified
        {
            get { return ClosedSaleCommentDescription != null; }
            set { }
        }

        /// <summary>
        /// Identifies the positional location on the appraisal form in which the comparable appears.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOIdentifier ComparableIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the ComparableIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ComparableIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool ComparableIdentifierSpecified
        {
            get { return ComparableIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Text comments about the listing information.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOString ComparableListingCommentDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ComparableListingCommentDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ComparableListingCommentDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ComparableListingCommentDescriptionSpecified
        {
            get { return ComparableListingCommentDescription != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe or comment on the comparable property used in the application of the Sales Comparison approach to property valuation.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOString ComparablePropertyCommentDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ComparablePropertyCommentDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ComparablePropertyCommentDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ComparablePropertyCommentDescriptionSpecified
        {
            get { return ComparablePropertyCommentDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether the comparable property is furnished.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOIndicator ComparableSaleFurnishedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ComparableSaleFurnishedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ComparableSaleFurnishedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ComparableSaleFurnishedIndicatorSpecified
        {
            get { return ComparableSaleFurnishedIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates the status of the property as either a competitive offering, comparable offering, comparable active listing, comparable expired listing  or closed sale.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOEnum<ComparableBase> ComparableType;

        /// <summary>
        /// Gets or sets a value indicating whether the ComparableType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ComparableType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ComparableTypeSpecified
        {
            get { return this.ComparableType != null && this.ComparableType.enumValue != ComparableBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Comparable Type.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOString ComparableTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ComparableTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ComparableTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ComparableTypeOtherDescriptionSpecified
        {
            get { return ComparableTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe and reconcile the competitive listings used.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOString CompetitiveListingsCommentDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the CompetitiveListingsCommentDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CompetitiveListingsCommentDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool CompetitiveListingsCommentDescriptionSpecified
        {
            get { return CompetitiveListingsCommentDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates if the contract date is unavailable for a settled sale.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOIndicator ContractDateUnknownIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ContractDateUnknownIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ContractDateUnknownIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ContractDateUnknownIndicatorSpecified
        {
            get { return ContractDateUnknownIndicator != null; }
            set { }
        }

        /// <summary>
        /// Specifies the multiplier factor used in calculating the Gross Monthly Rent of a property.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOPercent GrossMonthlyRentMultiplierPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the GrossMonthlyRentMultiplierPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GrossMonthlyRentMultiplierPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool GrossMonthlyRentMultiplierPercentSpecified
        {
            get { return GrossMonthlyRentMultiplierPercent != null; }
            set { }
        }

        /// <summary>
        /// Identifies the record number in the local MLS from which the listing was obtained.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOIdentifier MLSNumberIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the MLSNumberIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MLSNumberIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool MLSNumberIdentifierSpecified
        {
            get { return MLSNumberIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The amount paid by the borrower or co-borrower for the rental of a residence property. This information is normally provided during interview with the borrower or obtained from a loan application, and is sometimes verified with the residence property landlord.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOAmount MonthlyRentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the MonthlyRentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MonthlyRentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool MonthlyRentAmountSpecified
        {
            get { return MonthlyRentAmount != null; }
            set { }
        }

        /// <summary>
        /// Identifies the highest level private or public sector entity under whose guidelines the mortgage is originated.
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMOEnum<MortgageBase> MortgageType;

        /// <summary>
        /// Gets or sets a value indicating whether the MortgageType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MortgageType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MortgageTypeSpecified
        {
            get { return this.MortgageType != null && this.MortgageType.enumValue != MortgageBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information or a description of the mortgage type when Other is selected.
        /// </summary>
        [XmlElement(Order = 17)]
        public MISMOString MortgageTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MortgageTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MortgageTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MortgageTypeOtherDescriptionSpecified
        {
            get { return MortgageTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the transaction was executed without financing, e.g. cash or gift. 
        /// </summary>
        [XmlElement(Order = 18)]
        public MISMOIndicator NoFinancingTransactionIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the NoFinancingTransactionIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NoFinancingTransactionIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool NoFinancingTransactionIndicatorSpecified
        {
            get { return NoFinancingTransactionIndicator != null; }
            set { }
        }

        /// <summary>
        /// The price in terms of dollars per square foot. This is a ratio between a price and an area, but it is a monetary value rather than a factor or rate. (i.e. sales price per gross living area of a property).
        /// </summary>
        [XmlElement(Order = 19)]
        public MISMOAmount PricePerSquareFootAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PricePerSquareFootAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PricePerSquareFootAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PricePerSquareFootAmountSpecified
        {
            get { return PricePerSquareFootAmount != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the proximity of a comparable property to the subject property.
        /// </summary>
        [XmlElement(Order = 20)]
        public MISMOString ProximityToSubjectDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ProximityToSubjectDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProximityToSubjectDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProximityToSubjectDescriptionSpecified
        {
            get { return ProximityToSubjectDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the existence or likelihood of rent controls such as controls on businesses and apartment complexes.
        /// </summary>
        [XmlElement(Order = 21)]
        public MISMOEnum<RentControlStatusBase> RentControlStatusType;

        /// <summary>
        /// Gets or sets a value indicating whether the RentControlStatusType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RentControlStatusType element has been assigned a value.</value>
        [XmlIgnore]
        public bool RentControlStatusTypeSpecified
        {
            get { return this.RentControlStatusType != null && this.RentControlStatusType.enumValue != RentControlStatusBase.Blank; }
            set { }
        }

        /// <summary>
        /// The dollar value of the total adjustments made to a comparable property sales price during the application of the Sales Comparison approach to determine the market value of the subject property.
        /// </summary>
        [XmlElement(Order = 22)]
        public MISMOAmount SalePriceTotalAdjustmentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the SalePriceTotalAdjustmentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SalePriceTotalAdjustmentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool SalePriceTotalAdjustmentAmountSpecified
        {
            get { return SalePriceTotalAdjustmentAmount != null; }
            set { }
        }

        /// <summary>
        /// The percentage of the net sales price adjustments to the sales price of a property during the application of the sales comparison approach to property valuation.
        /// </summary>
        [XmlElement(Order = 23)]
        public MISMOPercent SalePriceTotalAdjustmentNetPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the SalePriceTotalAdjustmentNetPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SalePriceTotalAdjustmentNetPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool SalePriceTotalAdjustmentNetPercentSpecified
        {
            get { return SalePriceTotalAdjustmentNetPercent != null; }
            set { }
        }

        /// <summary>
        /// The dollar value of a bedroom of the property determined by dividing the sales price by the total number of bedrooms in the property.
        /// </summary>
        [XmlElement(Order = 24)]
        public MISMOAmount SalesPricePerBedroomAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the SalesPricePerBedroomAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SalesPricePerBedroomAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool SalesPricePerBedroomAmountSpecified
        {
            get { return SalesPricePerBedroomAmount != null; }
            set { }
        }

        /// <summary>
        /// The sales price in terms of dollars per square foot of the gross building area.  This is a ratio between the sales price and an area, but it is a monetary value rather than a factor or rate.
        /// </summary>
        [XmlElement(Order = 25)]
        public MISMOAmount SalesPricePerGrossBuildingAreaAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the SalesPricePerGrossBuildingAreaAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SalesPricePerGrossBuildingAreaAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool SalesPricePerGrossBuildingAreaAmountSpecified
        {
            get { return SalesPricePerGrossBuildingAreaAmount != null; }
            set { }
        }

        /// <summary>
        /// The sales price in terms of dollars per square foot of gross living area. This is a ratio between the sales price and an area, but it is a monetary value rather than a factor or rate.
        /// </summary>
        [XmlElement(Order = 26)]
        public MISMOAmount SalesPricePerGrossLivingAreaAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the SalesPricePerGrossLivingAreaAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SalesPricePerGrossLivingAreaAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool SalesPricePerGrossLivingAreaAmountSpecified
        {
            get { return SalesPricePerGrossLivingAreaAmount != null; }
            set { }
        }

        /// <summary>
        /// The dollar value of one share of the property determined by dividing the sales price by the total number of property ownership shares.
        /// </summary>
        [XmlElement(Order = 27)]
        public MISMOAmount SalesPricePerOwnershipShareAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the SalesPricePerOwnershipShareAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SalesPricePerOwnershipShareAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool SalesPricePerOwnershipShareAmountSpecified
        {
            get { return SalesPricePerOwnershipShareAmount != null; }
            set { }
        }

        /// <summary>
        /// The dollar value of one room of the property determined by dividing the sales price by the total number of rooms in the property.
        /// </summary>
        [XmlElement(Order = 28)]
        public MISMOAmount SalesPricePerRoomAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the SalesPricePerRoomAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SalesPricePerRoomAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool SalesPricePerRoomAmountSpecified
        {
            get { return SalesPricePerRoomAmount != null; }
            set { }
        }

        /// <summary>
        /// The dollar value of a living unit in a multi-unit property determined by dividing the sales price by the total number of units within the property.
        /// </summary>
        [XmlElement(Order = 29)]
        public MISMOAmount SalesPricePerUnitAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the SalesPricePerUnitAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SalesPricePerUnitAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool SalesPricePerUnitAmountSpecified
        {
            get { return SalesPricePerUnitAmount != null; }
            set { }
        }

        /// <summary>
        /// Specifies the ratio of the property sales price to its list price.
        /// </summary>
        [XmlElement(Order = 30)]
        public MISMOPercent SalesPriceToListPriceRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the SalesPriceToListPriceRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SalesPriceToListPriceRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool SalesPriceToListPriceRatePercentSpecified
        {
            get { return SalesPriceToListPriceRatePercent != null; }
            set { }
        }

        /// <summary>
        /// The percentage of the gross sales price adjustments (i.e. sum of the absolute adjustment values) to the sales price of a property during the application of the sales comparison approach for property valuation.
        /// </summary>
        [XmlElement(Order = 31)]
        public MISMOPercent SalesPriceTotalAdjustmentGrossPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the SalesPriceTotalAdjustmentGrossPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SalesPriceTotalAdjustmentGrossPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool SalesPriceTotalAdjustmentGrossPercentSpecified
        {
            get { return SalesPriceTotalAdjustmentGrossPercent != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 32)]
        public COMPARABLE_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
