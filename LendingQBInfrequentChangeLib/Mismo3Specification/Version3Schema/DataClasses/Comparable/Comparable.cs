namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class COMPARABLE
    {
        /// <summary>
        /// Gets a value indicating whether the COMPARABLE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ComparableAdjustmentsSpecified
                    || this.ComparableDetailSpecified
                    || this.ComparableSummarySpecified
                    || this.ExtensionSpecified
                    || this.ResearchSpecified;
            }
        }

        /// <summary>
        /// Adjustments to the comparable.
        /// </summary>
        [XmlElement("COMPARABLE_ADJUSTMENTS", Order = 0)]
        public COMPARABLE_ADJUSTMENTS ComparableAdjustments;

        /// <summary>
        /// Gets or sets a value indicating whether the ComparableAdjustments element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ComparableAdjustments element has been assigned a value.</value>
        [XmlIgnore]
        public bool ComparableAdjustmentsSpecified
        {
            get { return this.ComparableAdjustments != null && this.ComparableAdjustments.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details on the comparable.
        /// </summary>
        [XmlElement("COMPARABLE_DETAIL", Order = 1)]
        public COMPARABLE_DETAIL ComparableDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the ComparableDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ComparableDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool ComparableDetailSpecified
        {
            get { return this.ComparableDetail != null && this.ComparableDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Summary of the comparable.
        /// </summary>
        [XmlElement("COMPARABLE_SUMMARY", Order = 2)]
        public COMPARABLE_SUMMARY ComparableSummary;

        /// <summary>
        /// Gets or sets a value indicating whether the ComparableSummary element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ComparableSummary element has been assigned a value.</value>
        [XmlIgnore]
        public bool ComparableSummarySpecified
        {
            get { return this.ComparableSummary != null && this.ComparableSummary.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Research for the comparable.
        /// </summary>
        [XmlElement("RESEARCH", Order = 3)]
        public RESEARCH Research;

        /// <summary>
        /// Gets or sets a value indicating whether the Research element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Research element has been assigned a value.</value>
        [XmlIgnore]
        public bool ResearchSpecified
        {
            get { return this.Research != null && this.Research.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public COMPARABLE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
