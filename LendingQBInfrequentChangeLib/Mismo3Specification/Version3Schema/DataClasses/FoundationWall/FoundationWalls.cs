namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class FOUNDATION_WALLS
    {
        /// <summary>
        /// Gets a value indicating whether the FOUNDATION_WALLS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.FoundationWallSpecified;
            }
        }

        /// <summary>
        /// A collection of foundation walls.
        /// </summary>
        [XmlElement("FOUNDATION_WALL", Order = 0)]
		public List<FOUNDATION_WALL> FoundationWall = new List<FOUNDATION_WALL>();

        /// <summary>
        /// Gets or sets a value indicating whether the FoundationWall element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FoundationWall element has been assigned a value.</value>
        [XmlIgnore]
        public bool FoundationWallSpecified
        {
            get { return this.FoundationWall != null && this.FoundationWall.Count(f => f != null && f.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public FOUNDATION_WALLS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
