namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class SECURITY_INSTRUMENT
    {
        /// <summary>
        /// Gets a value indicating whether the SECURITY_INSTRUMENT container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.SecurityInstrumentAddendumsSpecified
                    || this.SecurityInstrumentDetailSpecified
                    || this.SecurityInstrumentRidersSpecified;
            }
        }

        /// <summary>
        /// Addendums to a security instrument.
        /// </summary>
        [XmlElement("SECURITY_INSTRUMENT_ADDENDUMS", Order = 0)]
        public SECURITY_INSTRUMENT_ADDENDUMS SecurityInstrumentAddendums;

        /// <summary>
        /// Gets or sets a value indicating whether the SecurityInstrumentAddendums element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SecurityInstrumentAddendums element has been assigned a value.</value>
        [XmlIgnore]
        public bool SecurityInstrumentAddendumsSpecified
        {
            get { return this.SecurityInstrumentAddendums != null && this.SecurityInstrumentAddendums.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details on a security instrument.
        /// </summary>
        [XmlElement("SECURITY_INSTRUMENT_DETAIL", Order = 1)]
        public SECURITY_INSTRUMENT_DETAIL SecurityInstrumentDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the SecurityInstrumentDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SecurityInstrumentDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool SecurityInstrumentDetailSpecified
        {
            get { return this.SecurityInstrumentDetail != null && this.SecurityInstrumentDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A list of security instrument riders.
        /// </summary>
        [XmlElement("SECURITY_INSTRUMENT_RIDERS", Order = 2)]
        public SECURITY_INSTRUMENT_RIDERS SecurityInstrumentRiders;

        /// <summary>
        /// Gets or sets a value indicating whether the SecurityInstrumentRiders element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SecurityInstrumentRiders element has been assigned a value.</value>
        [XmlIgnore]
        public bool SecurityInstrumentRidersSpecified
        {
            get { return this.SecurityInstrumentRiders != null && this.SecurityInstrumentRiders.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public SECURITY_INSTRUMENT_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
