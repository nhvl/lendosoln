namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class SECURITY_INSTRUMENT_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the SECURITY_INSTRUMENT_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DefaultAcknowledgmentOfCashAdvanceAgainstNonHomesteadPropertyIndicatorSpecified
                    || this.DefaultApplicationFeesAmountSpecified
                    || this.DefaultClosingPreparationFeesAmountSpecified
                    || this.DefaultLendingInstitutionPostOfficeBoxIdentifierSpecified
                    || this.DocumentDrawnInNameSpecified
                    || this.ExtensionSpecified
                    || this.NameDocumentsDrawnInTypeOtherDescriptionSpecified
                    || this.NameDocumentsDrawnInTypeSpecified
                    || this.SecurityInstrumentAssumptionFeeAmountSpecified
                    || this.SecurityInstrumentAttorneyFeeMinimumAmountSpecified
                    || this.SecurityInstrumentAttorneyFeePercentSpecified
                    || this.SecurityInstrumentCertifyingAttorneyNameSpecified
                    || this.SecurityInstrumentDateSpecified
                    || this.SecurityInstrumentMaximumPrincipalIndebtednessAmountSpecified
                    || this.SecurityInstrumentMultipleUnitsRealPropertyIsImprovedOrToBeImprovedIndicatorSpecified
                    || this.SecurityInstrumentNoteHolderNameSpecified
                    || this.SecurityInstrumentNoticeOfConfidentialityRightsDescriptionSpecified
                    || this.SecurityInstrumentOtherFeesAmountSpecified
                    || this.SecurityInstrumentOtherFeesDescriptionSpecified
                    || this.SecurityInstrumentOweltyOfPartitionIndicatorSpecified
                    || this.SecurityInstrumentPersonAuthorizedToReleaseLienNameSpecified
                    || this.SecurityInstrumentPersonAuthorizedToReleaseLienTelephoneValueSpecified
                    || this.SecurityInstrumentPersonAuthorizedToReleaseLienTitleDescriptionSpecified
                    || this.SecurityInstrumentPurchaseMoneyIndicatorSpecified
                    || this.SecurityInstrumentRealPropertyImprovedOrToBeImprovedIndicatorSpecified
                    || this.SecurityInstrumentRealPropertyImprovementsNotCoveredIndicatorSpecified
                    || this.SecurityInstrumentRecordingRequestedByNameSpecified
                    || this.SecurityInstrumentRenewalAndExtensionOfLiensAgainstHomesteadPropertyIndicatorSpecified
                    || this.SecurityInstrumentSignerForRegistersOfficeNameSpecified
                    || this.SecurityInstrumentTaxSerialNumberIdentifierSpecified
                    || this.SecurityInstrumentTrusteeFeePercentSpecified
                    || this.SecurityInstrumentVendorsLienDescriptionSpecified
                    || this.SecurityInstrumentVendorsLienIndicatorSpecified
                    || this.SecurityInstrumentVestingDescriptionSpecified;
            }
        }

        /// <summary>
        /// When true, indicates an Acknowledgement for Non Homestead Property cash advance on default section of the security instrument.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIndicator DefaultAcknowledgmentOfCashAdvanceAgainstNonHomesteadPropertyIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the DefaultAcknowledgmentOfCashAdvanceAgainstNonHomesteadPropertyIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DefaultAcknowledgmentOfCashAdvanceAgainstNonHomesteadPropertyIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool DefaultAcknowledgmentOfCashAdvanceAgainstNonHomesteadPropertyIndicatorSpecified
        {
            get { return DefaultAcknowledgmentOfCashAdvanceAgainstNonHomesteadPropertyIndicator != null; }
            set { }
        }

        /// <summary>
        /// Application Fees disclosed in the default section of the security instrument.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOAmount DefaultApplicationFeesAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the DefaultApplicationFeesAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DefaultApplicationFeesAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool DefaultApplicationFeesAmountSpecified
        {
            get { return DefaultApplicationFeesAmount != null; }
            set { }
        }

        /// <summary>
        /// Preparation fee amount disclosed in the default section of the security instrument.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOAmount DefaultClosingPreparationFeesAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the DefaultClosingPreparationFeesAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DefaultClosingPreparationFeesAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool DefaultClosingPreparationFeesAmountSpecified
        {
            get { return DefaultClosingPreparationFeesAmount != null; }
            set { }
        }

        /// <summary>
        /// Lenders Post Office Box included in the default section of the security instrument.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOIdentifier DefaultLendingInstitutionPostOfficeBoxIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the DefaultLendingInstitutionPostOfficeBoxIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DefaultLendingInstitutionPostOfficeBoxIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool DefaultLendingInstitutionPostOfficeBoxIdentifierSpecified
        {
            get { return DefaultLendingInstitutionPostOfficeBoxIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The party in whose name the documents were drawn.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString DocumentDrawnInName;

        /// <summary>
        /// Gets or sets a value indicating whether the DocumentDrawnInName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DocumentDrawnInName element has been assigned a value.</value>
        [XmlIgnore]
        public bool DocumentDrawnInNameSpecified
        {
            get { return DocumentDrawnInName != null; }
            set { }
        }

        /// <summary>
        /// Name of the party to appear on the Security Instrument.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOEnum<NameDocumentsDrawnInBase> NameDocumentsDrawnInType;

        /// <summary>
        /// Gets or sets a value indicating whether the NameDocumentsDrawnInType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NameDocumentsDrawnInType element has been assigned a value.</value>
        [XmlIgnore]
        public bool NameDocumentsDrawnInTypeSpecified
        {
            get { return this.NameDocumentsDrawnInType != null && this.NameDocumentsDrawnInType.enumValue != NameDocumentsDrawnInBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Name Documents Drawn Type.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOString NameDocumentsDrawnInTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the NameDocumentsDrawnInTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NameDocumentsDrawnInTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool NameDocumentsDrawnInTypeOtherDescriptionSpecified
        {
            get { return NameDocumentsDrawnInTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The assumption fee expressed in dollars that is charged by the Lender if the there is an assumption on the loan.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOAmount SecurityInstrumentAssumptionFeeAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the SecurityInstrumentAssumptionFeeAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SecurityInstrumentAssumptionFeeAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool SecurityInstrumentAssumptionFeeAmountSpecified
        {
            get { return SecurityInstrumentAssumptionFeeAmount != null; }
            set { }
        }

        /// <summary>
        /// The minimum amount of attorney fees that the borrower will be charged resulting from an acceleration and/or foreclosure of mortgage.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOAmount SecurityInstrumentAttorneyFeeMinimumAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the SecurityInstrumentAttorneyFeeMinimumAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SecurityInstrumentAttorneyFeeMinimumAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool SecurityInstrumentAttorneyFeeMinimumAmountSpecified
        {
            get { return SecurityInstrumentAttorneyFeeMinimumAmount != null; }
            set { }
        }

        /// <summary>
        /// The attorney fees the borrower will be charged, expressed as a percentage of the amount decreed for principal and interest resulting from an acceleration and/or foreclosure of mortgage.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOPercent SecurityInstrumentAttorneyFeePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the SecurityInstrumentAttorneyFeePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SecurityInstrumentAttorneyFeePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool SecurityInstrumentAttorneyFeePercentSpecified
        {
            get { return SecurityInstrumentAttorneyFeePercent != null; }
            set { }
        }

        /// <summary>
        /// The Name of the Attorney who prepared or supervised the preparation of the security instrument. This field is used, for example, on the Maryland Mortgage.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOString SecurityInstrumentCertifyingAttorneyName;

        /// <summary>
        /// Gets or sets a value indicating whether the SecurityInstrumentCertifyingAttorneyName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SecurityInstrumentCertifyingAttorneyName element has been assigned a value.</value>
        [XmlIgnore]
        public bool SecurityInstrumentCertifyingAttorneyNameSpecified
        {
            get { return SecurityInstrumentCertifyingAttorneyName != null; }
            set { }
        }

        /// <summary>
        /// The date referenced in the security instrument.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMODate SecurityInstrumentDate;

        /// <summary>
        /// Gets or sets a value indicating whether the SecurityInstrumentDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SecurityInstrumentDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool SecurityInstrumentDateSpecified
        {
            get { return SecurityInstrumentDate != null; }
            set { }
        }

        /// <summary>
        /// The maximum principal indebtedness amount in the state of Tennessee as stated on the deed of trust.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOAmount SecurityInstrumentMaximumPrincipalIndebtednessAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the SecurityInstrumentMaximumPrincipalIndebtednessAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SecurityInstrumentMaximumPrincipalIndebtednessAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool SecurityInstrumentMaximumPrincipalIndebtednessAmountSpecified
        {
            get { return SecurityInstrumentMaximumPrincipalIndebtednessAmount != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates the Multiple Units Real Property Is Improved Or To Be Improved check box on the New York Mortgage should be checked.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOIndicator SecurityInstrumentMultipleUnitsRealPropertyIsImprovedOrToBeImprovedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the SecurityInstrumentMultipleUnitsRealPropertyIsImprovedOrToBeImprovedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SecurityInstrumentMultipleUnitsRealPropertyIsImprovedOrToBeImprovedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool SecurityInstrumentMultipleUnitsRealPropertyIsImprovedOrToBeImprovedIndicatorSpecified
        {
            get { return SecurityInstrumentMultipleUnitsRealPropertyIsImprovedOrToBeImprovedIndicator != null; }
            set { }
        }

        /// <summary>
        /// The name of the Note Holder.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOString SecurityInstrumentNoteHolderName;

        /// <summary>
        /// Gets or sets a value indicating whether the SecurityInstrumentNoteHolderName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SecurityInstrumentNoteHolderName element has been assigned a value.</value>
        [XmlIgnore]
        public bool SecurityInstrumentNoteHolderNameSpecified
        {
            get { return SecurityInstrumentNoteHolderName != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture the notice of confidentiality rights verbiage. This field is used, for example, on the TX Security Instrument. 
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOString SecurityInstrumentNoticeOfConfidentialityRightsDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the SecurityInstrumentNoticeOfConfidentialityRightsDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SecurityInstrumentNoticeOfConfidentialityRightsDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool SecurityInstrumentNoticeOfConfidentialityRightsDescriptionSpecified
        {
            get { return SecurityInstrumentNoticeOfConfidentialityRightsDescription != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of the other fees required to be disclosed on the Rhode Island security instrument.
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMOAmount SecurityInstrumentOtherFeesAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the SecurityInstrumentOtherFeesAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SecurityInstrumentOtherFeesAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool SecurityInstrumentOtherFeesAmountSpecified
        {
            get { return SecurityInstrumentOtherFeesAmount != null; }
            set { }
        }

        /// <summary>
        /// The description of the other fees required to be disclosed on the Rhode Island security instrument.
        /// </summary>
        [XmlElement(Order = 17)]
        public MISMOString SecurityInstrumentOtherFeesDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the SecurityInstrumentOtherFeesDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SecurityInstrumentOtherFeesDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool SecurityInstrumentOtherFeesDescriptionSpecified
        {
            get { return SecurityInstrumentOtherFeesDescription != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates the partition check box on the Texas security instrument should be checked.
        /// </summary>
        [XmlElement(Order = 18)]
        public MISMOIndicator SecurityInstrumentOweltyOfPartitionIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the indicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the indicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool SecurityInstrumentOweltyOfPartitionIndicatorSpecified
        {
            get { return SecurityInstrumentOweltyOfPartitionIndicator != null; }
            set { }
        }

        /// <summary>
        /// On the Arkansas Mortgage, the Name of the person who is authorized to release lien. This field is used, for example, on the Arkansas Mortgage.
        /// </summary>
        [XmlElement(Order = 19)]
        public MISMOString SecurityInstrumentPersonAuthorizedToReleaseLienName;

        /// <summary>
        /// Gets or sets a value indicating whether the SecurityInstrumentPersonAuthorizedToReleaseLienName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SecurityInstrumentPersonAuthorizedToReleaseLienName element has been assigned a value.</value>
        [XmlIgnore]
        public bool SecurityInstrumentPersonAuthorizedToReleaseLienNameSpecified
        {
            get { return SecurityInstrumentPersonAuthorizedToReleaseLienName != null; }
            set { }
        }

        /// <summary>
        /// Value of the security instrument person authorized to release lien.
        /// </summary>
        [XmlElement(Order = 20)]
        public MISMONumericString SecurityInstrumentPersonAuthorizedToReleaseLienTelephoneValue;

        /// <summary>
        /// Gets or sets a value indicating whether the SecurityInstrumentPersonAuthorizedToReleaseLienTelephoneValue element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SecurityInstrumentPersonAuthorizedToReleaseLienTelephoneValue element has been assigned a value.</value>
        [XmlIgnore]
        public bool SecurityInstrumentPersonAuthorizedToReleaseLienTelephoneValueSpecified
        {
            get { return SecurityInstrumentPersonAuthorizedToReleaseLienTelephoneValue != null; }
            set { }
        }

        /// <summary>
        /// The Title of the person who is authorized to release lien. This field is use, for example, on the Arkansas Mortgage.
        /// </summary>
        [XmlElement(Order = 21)]
        public MISMOString SecurityInstrumentPersonAuthorizedToReleaseLienTitleDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the SecurityInstrumentPersonAuthorizedToReleaseLienTitleDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SecurityInstrumentPersonAuthorizedToReleaseLienTitleDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool SecurityInstrumentPersonAuthorizedToReleaseLienTitleDescriptionSpecified
        {
            get { return SecurityInstrumentPersonAuthorizedToReleaseLienTitleDescription != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates the Purchase Money Transaction check box on the Texas security instrument should be checked.
        /// </summary>
        [XmlElement(Order = 22)]
        public MISMOIndicator SecurityInstrumentPurchaseMoneyIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the SecurityInstrumentPurchaseMoneyIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SecurityInstrumentPurchaseMoneyIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool SecurityInstrumentPurchaseMoneyIndicatorSpecified
        {
            get { return SecurityInstrumentPurchaseMoneyIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates the Real Property Is Improved Or To Be Improved check box on the New York Mortgage should be checked.
        /// </summary>
        [XmlElement(Order = 23)]
        public MISMOIndicator SecurityInstrumentRealPropertyImprovedOrToBeImprovedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the SecurityInstrumentRealPropertyImprovedOrToBeImprovedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SecurityInstrumentRealPropertyImprovedOrToBeImprovedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool SecurityInstrumentRealPropertyImprovedOrToBeImprovedIndicatorSpecified
        {
            get { return SecurityInstrumentRealPropertyImprovedOrToBeImprovedIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates the Real Property Improvements Not Covered check box on the New York Mortgage should be checked.
        /// </summary>
        [XmlElement(Order = 24)]
        public MISMOIndicator SecurityInstrumentRealPropertyImprovementsNotCoveredIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the SecurityInstrumentRealPropertyImprovementsNotCoveredIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SecurityInstrumentRealPropertyImprovementsNotCoveredIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool SecurityInstrumentRealPropertyImprovementsNotCoveredIndicatorSpecified
        {
            get { return SecurityInstrumentRealPropertyImprovementsNotCoveredIndicator != null; }
            set { }
        }

        /// <summary>
        /// The name of the person requesting the recording of the security instrument.
        /// </summary>
        [XmlElement(Order = 25)]
        public MISMOString SecurityInstrumentRecordingRequestedByName;

        /// <summary>
        /// Gets or sets a value indicating whether the SecurityInstrumentRecordingRequestedByName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SecurityInstrumentRecordingRequestedByName element has been assigned a value.</value>
        [XmlIgnore]
        public bool SecurityInstrumentRecordingRequestedByNameSpecified
        {
            get { return SecurityInstrumentRecordingRequestedByName != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates on the Texas security Instrument that the Note is in renewal and extension.
        /// </summary>
        [XmlElement(Order = 26)]
        public MISMOIndicator SecurityInstrumentRenewalAndExtensionOfLiensAgainstHomesteadPropertyIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the SecurityInstrumentRenewalAndExtensionOfLiensAgainstHomesteadPropertyIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SecurityInstrumentRenewalAndExtensionOfLiensAgainstHomesteadPropertyIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool SecurityInstrumentRenewalAndExtensionOfLiensAgainstHomesteadPropertyIndicatorSpecified
        {
            get { return SecurityInstrumentRenewalAndExtensionOfLiensAgainstHomesteadPropertyIndicator != null; }
            set { }
        }

        /// <summary>
        /// The name of the person who is authorized to sign for the office recorder . This field is used, for example, on the North Carolina Deed of Trust.
        /// </summary>
        [XmlElement(Order = 27)]
        public MISMOString SecurityInstrumentSignerForRegistersOfficeName;

        /// <summary>
        /// Gets or sets a value indicating whether the SecurityInstrumentSignerForRegistersOfficeName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SecurityInstrumentSignerForRegistersOfficeName element has been assigned a value.</value>
        [XmlIgnore]
        public bool SecurityInstrumentSignerForRegistersOfficeNameSpecified
        {
            get { return SecurityInstrumentSignerForRegistersOfficeName != null; }
            set { }
        }

        /// <summary>
        /// The identifier of Tax Serial Number for the Utah security instrument.
        /// </summary>
        [XmlElement(Order = 28)]
        public MISMOIdentifier SecurityInstrumentTaxSerialNumberIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the SecurityInstrumentTaxSerialNumberIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SecurityInstrumentTaxSerialNumberIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool SecurityInstrumentTaxSerialNumberIdentifierSpecified
        {
            get { return SecurityInstrumentTaxSerialNumberIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The trustee fee expressed as a percentage of the gross sale price and attorneys fees.
        /// </summary>
        [XmlElement(Order = 29)]
        public MISMOPercent SecurityInstrumentTrusteeFeePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the SecurityInstrumentTrusteeFeePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SecurityInstrumentTrusteeFeePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool SecurityInstrumentTrusteeFeePercentSpecified
        {
            get { return SecurityInstrumentTrusteeFeePercent != null; }
            set { }
        }

        /// <summary>
        /// The description of the Vendors Lien as it appears on the Texas security instrument.
        /// </summary>
        [XmlElement(Order = 30)]
        public MISMOString SecurityInstrumentVendorsLienDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the SecurityInstrumentVendorsLienDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SecurityInstrumentVendorsLienDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool SecurityInstrumentVendorsLienDescriptionSpecified
        {
            get { return SecurityInstrumentVendorsLienDescription != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates the Vendors Lien check box on the Texas security instrument should be checked.
        /// </summary>
        [XmlElement(Order = 31)]
        public MISMOIndicator SecurityInstrumentVendorsLienIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the SecurityInstrumentVendorsLienIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SecurityInstrumentVendorsLienIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool SecurityInstrumentVendorsLienIndicatorSpecified
        {
            get { return SecurityInstrumentVendorsLienIndicator != null; }
            set { }
        }

        /// <summary>
        /// The description of how title is held on the security instrument.
        /// </summary>
        [XmlElement(Order = 32)]
        public MISMOString SecurityInstrumentVestingDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the SecurityInstrumentVestingDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SecurityInstrumentVestingDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool SecurityInstrumentVestingDescriptionSpecified
        {
            get { return SecurityInstrumentVestingDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 33)]
        public SECURITY_INSTRUMENT_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
