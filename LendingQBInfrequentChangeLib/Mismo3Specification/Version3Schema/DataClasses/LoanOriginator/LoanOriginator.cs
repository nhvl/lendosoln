namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class LOAN_ORIGINATOR
    {
        /// <summary>
        /// Gets a value indicating whether the LOAN_ORIGINATOR container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.LoanOriginatorTypeOtherDescriptionSpecified
                    || this.LoanOriginatorTypeSpecified;
            }
        }

        /// <summary>
        /// Specifies the type of party that originated the loan.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<LoanOriginatorBase> LoanOriginatorType;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanOriginatorType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanOriginatorType element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanOriginatorTypeSpecified
        {
            get { return this.LoanOriginatorType != null && this.LoanOriginatorType.enumValue != LoanOriginatorBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Loan Originator Type.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString LoanOriginatorTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanOriginatorTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanOriginatorTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanOriginatorTypeOtherDescriptionSpecified
        {
            get { return LoanOriginatorTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public LOAN_ORIGINATOR_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
