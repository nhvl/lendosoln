namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class FOREIGN_OBJECTS
    {
        /// <summary>
        /// Gets a value indicating whether the FOREIGN_OBJECTS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ForeignObjectSpecified;
            }
        }

        /// <summary>
        /// A collection of foreign objects.
        /// </summary>
        [XmlElement("FOREIGN_OBJECT", Order = 0)]
		public List<FOREIGN_OBJECT> ForeignObject = new List<FOREIGN_OBJECT>();

        /// <summary>
        /// Gets or sets a value indicating whether the ForeignObject element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ForeignObject element has been assigned a value.</value>
        [XmlIgnore]
        public bool ForeignObjectSpecified
        {
            get { return this.ForeignObject != null && this.ForeignObject.Count(f => f != null && f.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public FOREIGN_OBJECTS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
