namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class FOREIGN_OBJECT
    {
        /// <summary>
        /// Gets a value indicating whether the FOREIGN_OBJECT container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                if (Mismo3Utilities.ExceedsThreshold(1,
                    new List<bool> { 
                        this.ReferenceSpecified, 
                        this.CharacterEncodingSetTypeOtherDescriptionSpecified
                        || this.CharacterEncodingSetTypeSpecified
                        || this.EmbeddedContentXMLSpecified
                        || this.ExtensionSpecified
                        || this.MIMETypeIdentifierSpecified
                        || this.MIMETypeVersionIdentifierSpecified
                        || this.ObjectCreatedDatetimeSpecified
                        || this.ObjectDescriptionSpecified
                        || this.ObjectEncodingTypeOtherDescriptionSpecified
                        || this.ObjectEncodingTypeSpecified
                        || this.ObjectNameSpecified
                        || this.ObjectURLSpecified
                        || this.OriginalCreatorDigestValueSpecified
                        || this.UnencodedObjectByteCountSpecified }))
                {
                    throw new System.Xml.Schema.XmlSchemaValidationException(
                        Mismo3Utilities.GetXSDChoiceExceptionMessage(
                        "FOREIGN_OBJECT",
                        new List<string> { "REFERENCE", "Any other child element of the FOREIGN_OBJECT" }));
                }

                if (Mismo3Utilities.ExceedsThreshold(1,
                    new List<bool> { this.EmbeddedContentXMLSpecified, this.ObjectURLSpecified }))
                {
                    throw new System.Xml.Schema.XmlSchemaValidationException(
                        Mismo3Utilities.GetXSDChoiceExceptionMessage(
                        "FOREIGN_OBJECT",
                        new List<string> { "EmbeddedContentXML", "ObjectURL" }));
                }

                return this.CharacterEncodingSetTypeOtherDescriptionSpecified
                    || this.CharacterEncodingSetTypeSpecified
                    || this.EmbeddedContentXMLSpecified
                    || this.ExtensionSpecified
                    || this.MIMETypeIdentifierSpecified
                    || this.MIMETypeVersionIdentifierSpecified
                    || this.ObjectCreatedDatetimeSpecified
                    || this.ObjectDescriptionSpecified
                    || this.ObjectEncodingTypeOtherDescriptionSpecified
                    || this.ObjectEncodingTypeSpecified
                    || this.ObjectNameSpecified
                    || this.ObjectURLSpecified
                    || this.OriginalCreatorDigestValueSpecified
                    || this.ReferenceSpecified
                    || this.UnencodedObjectByteCountSpecified;
            }
        }

        /// <summary>
        /// Specifies the character encoding set used by the resource content.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<CharacterEncodingSetBase> CharacterEncodingSetType;

        /// <summary>
        /// Gets or sets a value indicating whether the CharacterEncodingSetType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CharacterEncodingSetType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CharacterEncodingSetTypeSpecified
        {
            get { return this.CharacterEncodingSetType != null && this.CharacterEncodingSetType.enumValue != CharacterEncodingSetBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture the resource Character Encoding Set type if Other is selected for CharacterEncodingSetType.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString CharacterEncodingSetTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the CharacterEncodingSetTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CharacterEncodingSetTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool CharacterEncodingSetTypeOtherDescriptionSpecified
        {
            get { return CharacterEncodingSetTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public FOREIGN_OBJECT_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        ///  The XML compatible byte stream in the encoding of the EncodingType that when decoded is a resource of the MediaType using characters from the CharacterEncodingSetType. If a binary EncodingType is not used (e.g. "None" or "EscapedXML") then the CharacterEncodingSetType must be the same as that used for the XML instance document. 
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOXML EmbeddedContentXML;

        /// <summary>
        /// Gets or sets a value indicating whether the EmbeddedContentXML element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EmbeddedContentXML element has been assigned a value.</value>
        [XmlIgnore]
        public bool EmbeddedContentXMLSpecified
        {
            get { return EmbeddedContentXML != null; }
            set { }
        }

        /// <summary>
        /// Indicates the Multipurpose Internet Mail Extensions type of the data in the resource.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOIdentifier MIMETypeIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the MIMETypeIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIMETypeIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIMETypeIdentifierSpecified
        {
            get { return MIMETypeIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The version of the content type. Used when the MIMETypeIdentifier is not enough to uniquely identify the content type of the a foreign object.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOIdentifier MIMETypeVersionIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the MIMETypeVersionIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIMETypeVersionIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIMETypeVersionIdentifierSpecified
        {
            get { return MIMETypeVersionIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The date and time that the resource was created.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMODatetime ObjectCreatedDatetime;

        /// <summary>
        /// Gets or sets a value indicating whether the date/time element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the date/time element has been assigned a value.</value>
        [XmlIgnore]
        public bool ObjectCreatedDatetimeSpecified
        {
            get { return ObjectCreatedDatetime != null; }
            set { }
        }

        /// <summary>
        /// Provides additional information about the resource, such as the software version that generated the resource.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOString ObjectDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ObjectDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ObjectDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ObjectDescriptionSpecified
        {
            get { return ObjectDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the type of encoding used by the resource.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOEnum<ObjectEncodingBase> ObjectEncodingType;

        /// <summary>
        /// Gets or sets a value indicating whether the ObjectEncodingType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ObjectEncodingType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ObjectEncodingTypeSpecified
        {
            get { return this.ObjectEncodingType != null && this.ObjectEncodingType.enumValue != ObjectEncodingBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture the resource Encoding Type if Other is selected for EncodingType.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOString ObjectEncodingTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ObjectEncodingTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ObjectEncodingTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ObjectEncodingTypeOtherDescriptionSpecified
        {
            get { return ObjectEncodingTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Identifies the resource name (e.g. CreditReport0001.pdf).
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOString ObjectName;

        /// <summary>
        /// Gets or sets a value indicating whether the ObjectName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ObjectName element has been assigned a value.</value>
        [XmlIgnore]
        public bool ObjectNameSpecified
        {
            get { return ObjectName != null; }
            set { }
        }

        /// <summary>
        /// The URL where the resource may be found when the location is external to the current XML document.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOObjectURL ObjectURL;

        /// <summary>
        /// Gets or sets a value indicating whether the ObjectURL element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ObjectURL element has been assigned a value.</value>
        [XmlIgnore]
        public bool ObjectURLSpecified
        {
            get { return ObjectURL != null; }
            set { }
        }

        /// <summary>
        /// Optional placeholder for storing the original digest of the resource at the time of its creation.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOValue OriginalCreatorDigestValue;

        /// <summary>
        /// Gets or sets a value indicating whether the OriginalCreatorDigestValue element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OriginalCreatorDigestValue element has been assigned a value.</value>
        [XmlIgnore]
        public bool OriginalCreatorDigestValueSpecified
        {
            get { return OriginalCreatorDigestValue != null; }
            set { }
        }

        /// <summary>
        /// A reference for this foreign object.
        /// </summary>
        [XmlElement("REFERENCE", Order = 13)]
        public REFERENCE Reference;

        /// <summary>
        /// Gets or sets a value indicating whether the Reference element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Reference element has been assigned a value.</value>
        [XmlIgnore]
        public bool ReferenceSpecified
        {
            get { return this.Reference != null && this.Reference.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The size in bytes of the original unencoded object contained or referenced by this FOREIGN_OBJECT.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOCount UnencodedObjectByteCount;

        /// <summary>
        /// Gets or sets a value indicating whether the UnencodedObjectByteCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the UnencodedObjectByteCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool UnencodedObjectByteCountSpecified
        {
            get { return UnencodedObjectByteCount != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
