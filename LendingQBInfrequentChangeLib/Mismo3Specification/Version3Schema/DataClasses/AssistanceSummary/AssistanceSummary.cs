namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class ASSISTANCE_SUMMARY
    {
        /// <summary>
        /// Gets a value indicating whether the ASSISTANCE_SUMMARY container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AssistanceActualDisbursementEndDateSpecified
                    || this.AssistanceScheduledDisbursementEndDateSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// The date on which the assistance disbursement to the recipient ended or was cancelled  in connection with a workout.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMODate AssistanceActualDisbursementEndDate;

        /// <summary>
        /// Gets or sets a value indicating whether the AssistanceActualDisbursementEndDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AssistanceActualDisbursementEndDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool AssistanceActualDisbursementEndDateSpecified
        {
            get { return AssistanceActualDisbursementEndDate != null; }
            set { }
        }

        /// <summary>
        /// The date on which the assistance disbursement to the recipient is scheduled to end in connection with a workout.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMODate AssistanceScheduledDisbursementEndDate;

        /// <summary>
        /// Gets or sets a value indicating whether the AssistanceScheduledDisbursementEndDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AssistanceScheduledDisbursementEndDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool AssistanceScheduledDisbursementEndDateSpecified
        {
            get { return AssistanceScheduledDisbursementEndDate != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public ASSISTANCE_SUMMARY_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
