namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class ADDITIONAL_CHARGE
    {
        /// <summary>
        /// Gets a value indicating whether the ADDITIONAL_CHARGE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AdditionalChargeAmountSpecified
                    || this.AdditionalChargeDateSpecified
                    || this.AdditionalChargeTypeOtherDescriptionSpecified
                    || this.AdditionalChargeTypeSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// The monetary amount of a charge, an adjustment, a loss, a recovery, or an advance associated with a loan.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount AdditionalChargeAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the AdditionalChargeAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AdditionalChargeAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool AdditionalChargeAmountSpecified
        {
            get { return AdditionalChargeAmount != null; }
            set { }
        }

        /// <summary>
        ///  The date of the additional charge being reported to the Investor. 
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMODate AdditionalChargeDate;

        /// <summary>
        /// Gets or sets a value indicating whether the AdditionalChargeDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AdditionalChargeDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool AdditionalChargeDateSpecified
        {
            get { return AdditionalChargeDate != null; }
            set { }
        }

        /// <summary>
        /// A description of a charge, an adjustment, a loss, a recovery, or an advance associated with a loan.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<AdditionalChargeBase> AdditionalChargeType;

        /// <summary>
        /// Gets or sets a value indicating whether the AdditionalChargeType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AdditionalChargeType element has been assigned a value.</value>
        [XmlIgnore]
        public bool AdditionalChargeTypeSpecified
        {
            get { return this.AdditionalChargeType != null && this.AdditionalChargeType.enumValue != AdditionalChargeBase.Blank; }
            set { }
        }

        /// <summary>
        ///  A free-form text field used to collect additional information when Other is selected for Additional Charge Type.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString AdditionalChargeTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the AdditionalChargeTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AdditionalChargeTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool AdditionalChargeTypeOtherDescriptionSpecified
        {
            get { return AdditionalChargeTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public ADDITIONAL_CHARGE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
