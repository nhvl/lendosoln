namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class ADDITIONAL_CHARGES
    {
        /// <summary>
        /// Gets a value indicating whether the ADDITIONAL_CHARGES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AdditionalChargeSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of additional charges.
        /// </summary>
        [XmlElement("ADDITIONAL_CHARGE", Order = 0)]
		public List<ADDITIONAL_CHARGE> AdditionalCharge = new List<ADDITIONAL_CHARGE>();

        /// <summary>
        /// Gets or sets a value indicating whether the AdditionalCharge element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AdditionalCharge element has been assigned a value.</value>
        [XmlIgnore]
        public bool AdditionalChargeSpecified
        {
            get { return this.AdditionalCharge != null && this.AdditionalCharge.Count(a => a != null && a.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public ADDITIONAL_CHARGES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
