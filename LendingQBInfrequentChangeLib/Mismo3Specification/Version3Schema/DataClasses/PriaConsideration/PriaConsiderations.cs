namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PRIA_CONSIDERATIONS
    {
        /// <summary>
        /// Gets a value indicating whether the PRIA_CONSIDERATIONS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.PRIAConsiderationSpecified;
            }
        }

        /// <summary>
        /// A collection of considerations.
        /// </summary>
        [XmlElement("PRIA_CONSIDERATION", Order = 0)]
		public List<PRIA_CONSIDERATION> PRIAConsideration = new List<PRIA_CONSIDERATION>();

        /// <summary>
        /// Gets or sets a value indicating whether the Property Records Industry Association Consideration element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Property Records Industry Association Consideration element has been assigned a value.</value>
        [XmlIgnore]
        public bool PRIAConsiderationSpecified
        {
            get { return this.PRIAConsideration != null && this.PRIAConsideration.Count(p => p != null && p.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public PRIA_CONSIDERATIONS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
