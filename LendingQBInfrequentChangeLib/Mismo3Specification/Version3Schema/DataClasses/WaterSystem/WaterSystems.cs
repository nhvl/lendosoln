namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class WATER_SYSTEMS
    {
        /// <summary>
        /// Gets a value indicating whether the WATER_SYSTEMS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.WaterSystemSpecified;
            }
        }

        /// <summary>
        /// A collection of water systems.
        /// </summary>
        [XmlElement("WATER_SYSTEM", Order = 0)]
		public List<WATER_SYSTEM> WaterSystem = new List<WATER_SYSTEM>();

        /// <summary>
        /// Gets or sets a value indicating whether the WaterSystem element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the WaterSystem element has been assigned a value.</value>
        [XmlIgnore]
        public bool WaterSystemSpecified
        {
            get { return this.WaterSystem != null && this.WaterSystem.Count(w => w != null && w.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public WATER_SYSTEMS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
