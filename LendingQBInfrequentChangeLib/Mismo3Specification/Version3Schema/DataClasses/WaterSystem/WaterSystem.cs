namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class WATER_SYSTEM
    {
        /// <summary>
        /// Gets a value indicating whether the WATER_SYSTEM container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.WaterHeaterTypeOtherDescriptionSpecified
                    || this.WaterHeaterTypeSpecified
                    || this.WaterTreatmentTypeOtherDescriptionSpecified
                    || this.WaterTreatmentTypeSpecified;
            }
        }

        /// <summary>
        /// A collection of values that specify the type of water heating system present in a property.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<WaterHeaterBase> WaterHeaterType;

        /// <summary>
        /// Gets or sets a value indicating whether the WaterHeaterType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the WaterHeaterType element has been assigned a value.</value>
        [XmlIgnore]
        public bool WaterHeaterTypeSpecified
        {
            get { return this.WaterHeaterType != null && this.WaterHeaterType.enumValue != WaterHeaterBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field that specifies the enumeration when the value of Other is used for the Water Heater Type.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString WaterHeaterTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the WaterHeaterTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the WaterHeaterTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool WaterHeaterTypeOtherDescriptionSpecified
        {
            get { return WaterHeaterTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A collection of values that specify the type of water treatment system present in a property.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<WaterTreatmentBase> WaterTreatmentType;

        /// <summary>
        /// Gets or sets a value indicating whether the WaterTreatmentType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the WaterTreatmentType element has been assigned a value.</value>
        [XmlIgnore]
        public bool WaterTreatmentTypeSpecified
        {
            get { return this.WaterTreatmentType != null && this.WaterTreatmentType.enumValue != WaterTreatmentBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field that specifies the enumeration when the value of Other is used for the Water Treatment Type.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString WaterTreatmentTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the WaterTreatmentTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the WaterTreatmentTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool WaterTreatmentTypeOtherDescriptionSpecified
        {
            get { return WaterTreatmentTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public WATER_SYSTEM_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
