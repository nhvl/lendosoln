namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class INTEREST_CALCULATION
    {
        /// <summary>
        /// Gets a value indicating whether the INTEREST_CALCULATION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.InterestCalculationOccurrencesSpecified
                    || this.InterestCalculationRulesSpecified;
            }
        }

        /// <summary>
        /// Occurrences of calculating interest.
        /// </summary>
        [XmlElement("INTEREST_CALCULATION_OCCURRENCES", Order = 0)]
        public INTEREST_CALCULATION_OCCURRENCES InterestCalculationOccurrences;

        /// <summary>
        /// Gets or sets a value indicating whether the InterestCalculationOccurrences element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InterestCalculationOccurrences element has been assigned a value.</value>
        [XmlIgnore]
        public bool InterestCalculationOccurrencesSpecified
        {
            get { return this.InterestCalculationOccurrences != null && this.InterestCalculationOccurrences.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Rules for calculating interest.
        /// </summary>
        [XmlElement("INTEREST_CALCULATION_RULES", Order = 1)]
        public INTEREST_CALCULATION_RULES InterestCalculationRules;

        /// <summary>
        /// Gets or sets a value indicating whether the InterestCalculationRules element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InterestCalculationRules element has been assigned a value.</value>
        [XmlIgnore]
        public bool InterestCalculationRulesSpecified
        {
            get { return this.InterestCalculationRules != null && this.InterestCalculationRules.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public INTEREST_CALCULATION_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
