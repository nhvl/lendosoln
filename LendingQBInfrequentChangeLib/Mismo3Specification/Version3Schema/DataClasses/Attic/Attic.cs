namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class ATTIC
    {
        /// <summary>
        /// Gets a value indicating whether the ATTIC container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AtticDetailSpecified
                    || this.AtticFeaturesSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// Details on an attic.
        /// </summary>
        [XmlElement("ATTIC_DETAIL", Order = 0)]
        public ATTIC_DETAIL AtticDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the AtticDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AtticDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool AtticDetailSpecified
        {
            get { return this.AtticDetail != null && this.AtticDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Features of an attic.
        /// </summary>
        [XmlElement("ATTIC_FEATURES", Order = 1)]
        public ATTIC_FEATURES AtticFeatures;

        /// <summary>
        /// Gets or sets a value indicating whether the AtticFeatures element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AtticFeatures element has been assigned a value.</value>
        [XmlIgnore]
        public bool AtticFeaturesSpecified
        {
            get { return this.AtticFeatures != null && this.AtticFeatures.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public ATTIC_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
