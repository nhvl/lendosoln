namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class MI_APPLICATION_RESPONSE_DETAIL_3_3_1
    {
        /// <summary>
        /// Gets a value indicating whether the MI_APPLICATION_RESPONSE_DETAIL_3_3_1 container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.MICoveragePlanTypeOtherDescriptionSpecified
                    || this.MICoveragePlanTypeSpecified
                    || this.MIPremiumRefundableTypeOtherDescriptionSpecified
                    || this.MIPremiumRefundableTypeSpecified;
            }
        }

        /// <summary>
        /// Specifies the coverage category of mortgage insurance applicable to the loan.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<MICoveragePlanBase> MICoveragePlanType;

        /// <summary>
        /// Gets or sets a value indicating whether the MICoveragePlanType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MICoveragePlanType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MICoveragePlanTypeSpecified
        {
            get { return this.MICoveragePlanType != null && this.MICoveragePlanType.enumValue != MICoveragePlanBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture the description of the type of MI Coverage Plan if Other is selected as the MICoveragePlanType.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString MICoveragePlanTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MICoveragePlanTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MICoveragePlanTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MICoveragePlanTypeOtherDescriptionSpecified
        {
            get { return MICoveragePlanTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies how the unearned portion of any private mortgage insurance premiums will be treated if the private mortgage insurance coverage is canceled.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<MIPremiumRefundableBase> MIPremiumRefundableType;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPremiumRefundableType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPremiumRefundableType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumRefundableTypeSpecified
        {
            get { return this.MIPremiumRefundableType != null && this.MIPremiumRefundableType.enumValue != MIPremiumRefundableBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture the description of the type of MI Premium Refundable if Other is selected as the MIPremiumRefundableType.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString MIPremiumRefundableTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPremiumRefundableTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPremiumRefundableTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumRefundableTypeOtherDescriptionSpecified
        {
            get { return MIPremiumRefundableTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public MI_APPLICATION_RESPONSE_DETAIL_3_3_1_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
