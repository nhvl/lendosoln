namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class MI_APPLICATION_RESPONSE_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the MI_APPLICATION_RESPONSE_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.MI_LTVPercentSpecified
                    || this.MIApplicationTypeOtherDescriptionSpecified
                    || this.MIApplicationTypeSpecified
                    || this.MICertificateExpirationDateSpecified
                    || this.MICertificateIdentifierSpecified
                    || this.MICertificateTypeSpecified
                    || this.MICommentDescriptionSpecified
                    || this.MICompanyNameTypeOtherDescriptionSpecified
                    || this.MICompanyNameTypeSpecified
                    || this.MICoveragePercentSpecified
                    || this.MIDecisionTypeSpecified
                    || this.MIDurationTypeOtherDescriptionSpecified
                    || this.MIDurationTypeSpecified
                    || this.MIInitialPremiumAtClosingTypeOtherDescriptionSpecified
                    || this.MIInitialPremiumAtClosingTypeSpecified
                    || this.MILenderIdentifierSpecified
                    || this.MIPremiumFromClosingAmountSpecified
                    || this.MIPremiumPaymentTypeOtherDescriptionSpecified
                    || this.MIPremiumPaymentTypeSpecified
                    || this.MIPremiumRatePlanTypeOtherDescriptionSpecified
                    || this.MIPremiumRatePlanTypeSpecified
                    || this.MIPremiumSourceTypeOtherDescriptionSpecified
                    || this.MIPremiumSourceTypeSpecified
                    || this.MIServiceTypeOtherDescriptionSpecified
                    || this.MIServiceTypeSpecified
                    || this.MISourceTypeOtherDescriptionSpecified
                    || this.MISourceTypeSpecified
                    || this.MITransactionIdentifierSpecified;
            }
        }

        /// <summary>
        /// The LTV as calculated by the MI company.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOPercent MI_LTVPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the MI_LTVPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MI_LTVPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool MI_LTVPercentSpecified
        {
            get { return MI_LTVPercent != null; }
            set { }
        }

        /// <summary>
        /// Specifies the way the lender directs the mortgage insurer to handle the processing of the loan.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<MIApplicationBase> MIApplicationType;

        /// <summary>
        /// Gets or sets a value indicating whether the MIApplicationType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIApplicationType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIApplicationTypeSpecified
        {
            get { return this.MIApplicationType != null && this.MIApplicationType.enumValue != MIApplicationBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture the description of the type of MI Application if Other is selected as the MIApplicationType.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString MIApplicationTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MIApplicationTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIApplicationTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIApplicationTypeOtherDescriptionSpecified
        {
            get { return MIApplicationTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The expiration date of the Commitment/Certificate issued by the MI.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMODate MICertificateExpirationDate;

        /// <summary>
        /// Gets or sets a value indicating whether the MICertificateExpirationDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MICertificateExpirationDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool MICertificateExpirationDateSpecified
        {
            get { return MICertificateExpirationDate != null; }
            set { }
        }

        /// <summary>
        /// The number assigned by the private mortgage insurance company to track a loan.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOIdentifier MICertificateIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the MICertificateIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MICertificateIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool MICertificateIdentifierSpecified
        {
            get { return MICertificateIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Specifies the type of mortgage insurance certificate required by the customer.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOEnum<MICertificateBase> MICertificateType;

        /// <summary>
        /// Gets or sets a value indicating whether the MICertificateType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MICertificateType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MICertificateTypeSpecified
        {
            get { return this.MICertificateType != null && this.MICertificateType.enumValue != MICertificateBase.Blank; }
            set { }
        }

        /// <summary>
        /// A place holder for comments from the MI company regarding the loan in question.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOString MICommentDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MICommentDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MICommentDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MICommentDescriptionSpecified
        {
            get { return MICommentDescription != null; }
            set { }
        }

        /// <summary>
        /// To convey the private MI company short/common name from whom the private mortgage insurance coverage was obtained.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOEnum<MICompanyNameBase> MICompanyNameType;

        /// <summary>
        /// Gets or sets a value indicating whether the MICompanyNameType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MICompanyNameType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MICompanyNameTypeSpecified
        {
            get { return this.MICompanyNameType != null && this.MICompanyNameType.enumValue != MICompanyNameBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the MI company name when Other is selected for the MI Company Name Type.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOString MICompanyNameTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MICompanyNameTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MICompanyNameTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MICompanyNameTypeOtherDescriptionSpecified
        {
            get { return MICompanyNameTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The percentage of mortgage insurance coverage obtained.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOPercent MICoveragePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the MICoveragePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MICoveragePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool MICoveragePercentSpecified
        {
            get { return MICoveragePercent != null; }
            set { }
        }

        /// <summary>
        /// To convey the mortgage insurers decision regarding the application for mortgage insurance.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOEnum<MIDecisionBase> MIDecisionType;

        /// <summary>
        /// Gets or sets a value indicating whether the MIDecisionType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIDecisionType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIDecisionTypeSpecified
        {
            get { return this.MIDecisionType != null && this.MIDecisionType.enumValue != MIDecisionBase.Blank; }
            set { }
        }

        /// <summary>
        /// Specifies the period of time for which mortgage insurance coverage has been obtained/paid.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOEnum<MIDurationBase> MIDurationType;

        /// <summary>
        /// Gets or sets a value indicating whether the MIDurationType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIDurationType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIDurationTypeSpecified
        {
            get { return this.MIDurationType != null && this.MIDurationType.enumValue != MIDurationBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture the description of the type of MI Duration if Other is selected as the MIDurationType.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOString MIDurationTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MIDurationTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIDurationTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIDurationTypeOtherDescriptionSpecified
        {
            get { return MIDurationTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies whether the initial Mortgage Insurance premium is prepaid upon closing or deferred until first principle payment.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOEnum<MIInitialPremiumAtClosingBase> MIInitialPremiumAtClosingType;

        /// <summary>
        /// Gets or sets a value indicating whether the MIInitialPremiumAtClosingType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIInitialPremiumAtClosingType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIInitialPremiumAtClosingTypeSpecified
        {
            get { return this.MIInitialPremiumAtClosingType != null && this.MIInitialPremiumAtClosingType.enumValue != MIInitialPremiumAtClosingBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture the description of the type of MI Initial Premium At Closing if Other is selected as the MIInitialPremiumAtClosingType.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOString MIInitialPremiumAtClosingTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MIInitialPremiumAtClosingTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIInitialPremiumAtClosingTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIInitialPremiumAtClosingTypeOtherDescriptionSpecified
        {
            get { return MIInitialPremiumAtClosingTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The unique number assigned by the private mortgage insurer identifying the lender entity.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOIdentifier MILenderIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the MILenderIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MILenderIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool MILenderIdentifierSpecified
        {
            get { return MILenderIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Amount of MI premium due from closing. Zero for deferred monthly; otherwise is a months, years or life premium for regular monthly, annual, or life of loan premium plans.
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMOAmount MIPremiumFromClosingAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPremiumFromClosingAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPremiumFromClosingAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumFromClosingAmountSpecified
        {
            get { return MIPremiumFromClosingAmount != null; }
            set { }
        }

        /// <summary>
        /// Defines how the MI premium payments are to be paid.
        /// </summary>
        [XmlElement(Order = 17)]
        public MISMOEnum<MIPremiumPaymentBase> MIPremiumPaymentType;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPremiumPaymentType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPremiumPaymentType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumPaymentTypeSpecified
        {
            get { return this.MIPremiumPaymentType != null && this.MIPremiumPaymentType.enumValue != MIPremiumPaymentBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture the description of the type of MI Premium Payment if Other is selected as the MIPremiumPaymentType.
        /// </summary>
        [XmlElement(Order = 18)]
        public MISMOString MIPremiumPaymentTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPremiumPaymentTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPremiumPaymentTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumPaymentTypeOtherDescriptionSpecified
        {
            get { return MIPremiumPaymentTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies pattern of Mortgage Insurance premium rates charged over the life of the coverage.
        /// </summary>
        [XmlElement(Order = 19)]
        public MISMOEnum<MIPremiumRatePlanBase> MIPremiumRatePlanType;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPremiumRatePlanType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPremiumRatePlanType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumRatePlanTypeSpecified
        {
            get { return this.MIPremiumRatePlanType != null && this.MIPremiumRatePlanType.enumValue != MIPremiumRatePlanBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture the description of the type of MI Premium Rate Plan if Other is selected as the MIPremiumRatePlanType.
        /// </summary>
        [XmlElement(Order = 20)]
        public MISMOString MIPremiumRatePlanTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPremiumRatePlanTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPremiumRatePlanTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumRatePlanTypeOtherDescriptionSpecified
        {
            get { return MIPremiumRatePlanTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Defines the source of the MI premium payment.
        /// </summary>
        [XmlElement(Order = 21)]
        public MISMOEnum<MIPremiumSourceBase> MIPremiumSourceType;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPremiumSourceType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPremiumSourceType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumSourceTypeSpecified
        {
            get { return this.MIPremiumSourceType != null && this.MIPremiumSourceType.enumValue != MIPremiumSourceBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field to collect MI premium source when Other is selected for MI Premium Source Type.
        /// </summary>
        [XmlElement(Order = 22)]
        public MISMOString MIPremiumSourceTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPremiumSourceTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPremiumSourceTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumSourceTypeOtherDescriptionSpecified
        {
            get { return MIPremiumSourceTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Defines the type of service provided by the mortgage insurer.
        /// </summary>
        [XmlElement(Order = 23)]
        public MISMOEnum<MIServiceBase> MIServiceType;

        /// <summary>
        /// Gets or sets a value indicating whether the MIServiceType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIServiceType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIServiceTypeSpecified
        {
            get { return this.MIServiceType != null && this.MIServiceType.enumValue != MIServiceBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the MI Service Type when Other is selected for the MI Service Type.
        /// </summary>
        [XmlElement(Order = 24)]
        public MISMOString MIServiceTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MIServiceTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIServiceTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIServiceTypeOtherDescriptionSpecified
        {
            get { return MIServiceTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The source of mortgage protection insurance.
        /// </summary>
        [XmlElement(Order = 25)]
        public MISMOEnum<MISourceBase> MISourceType;

        /// <summary>
        /// Gets or sets a value indicating whether the MISourceType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MISourceType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MISourceTypeSpecified
        {
            get { return this.MISourceType != null && this.MISourceType.enumValue != MISourceBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for MI Source Type.
        /// </summary>
        [XmlElement(Order = 26)]
        public MISMOString MISourceTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MISourceTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MISourceTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MISourceTypeOtherDescriptionSpecified
        {
            get { return MISourceTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A unique tracking identifier assigned by the sender which is typically returned to the sender in the resulting response.
        /// </summary>
        [XmlElement(Order = 27)]
        public MISMOIdentifier MITransactionIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the MITransactionIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MITransactionIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool MITransactionIdentifierSpecified
        {
            get { return MITransactionIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 28)]
        public MI_APPLICATION_RESPONSE_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
