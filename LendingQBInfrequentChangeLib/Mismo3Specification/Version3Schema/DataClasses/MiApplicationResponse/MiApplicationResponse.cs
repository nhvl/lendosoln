namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class MI_APPLICATION_RESPONSE
    {
        /// <summary>
        /// Gets a value indicating whether the MI_APPLICATION_RESPONSE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ContactPointsSpecified
                    || this.ExtensionSpecified
                    || this.LoanIdentifiersSpecified
                    || this.MIApplicationResponseDetailSpecified
                    || this.MIPremiumsSpecified
                    || this.MIPremiumTaxesSpecified
                    || this.PartiesSpecified;
            }
        }

        /// <summary>
        /// Contact points for the mortgage insurer.
        /// </summary>
        [XmlElement("CONTACT_POINTS", Order = 0)]
        public CONTACT_POINTS ContactPoints;

        /// <summary>
        /// Gets or sets a value indicating whether the ContactPoints element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ContactPoints element has been assigned a value.</value>
        [XmlIgnore]
        public bool ContactPointsSpecified
        {
            get { return this.ContactPoints != null && this.ContactPoints.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Identifiers for the loans requiring mortgage insurance.
        /// </summary>
        [XmlElement("LOAN_IDENTIFIERS", Order = 1)]
        public LOAN_IDENTIFIERS LoanIdentifiers;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanIdentifiers element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanIdentifiers element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanIdentifiersSpecified
        {
            get { return this.LoanIdentifiers != null && this.LoanIdentifiers.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details on the response for mortgage insurance.
        /// </summary>
        [XmlElement("MI_APPLICATION_RESPONSE_DETAIL", Order = 2)]
        public MI_APPLICATION_RESPONSE_DETAIL MIApplicationResponseDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the Mortgage Insurance Application Response Detail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Mortgage Insurance Application Response Detail element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIApplicationResponseDetailSpecified
        {
            get { return this.MIApplicationResponseDetail != null && this.MIApplicationResponseDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Taxes associated with the mortgage insurance.
        /// </summary>
        [XmlElement("MI_PREMIUM_TAXES", Order = 3)]
        public MI_PREMIUM_TAXES MIPremiumTaxes;

        /// <summary>
        /// Gets or sets a value indicating whether the Mortgage Insurance PremiumTaxes element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Mortgage Insurance PremiumTaxes element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumTaxesSpecified
        {
            get { return this.MIPremiumTaxes != null && this.MIPremiumTaxes.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Premiums associated with the mortgage insurance.
        /// </summary>
        [XmlElement("MI_PREMIUMS", Order = 4)]
        public MI_PREMIUMS MIPremiums;

        /// <summary>
        /// Gets or sets a value indicating whether the Mortgage Insurance Premiums element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Mortgage Insurance Premiums element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumsSpecified
        {
            get { return this.MIPremiums != null && this.MIPremiums.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Parties associated with the mortgage insurance response.
        /// </summary>
        [XmlElement("PARTIES", Order = 5)]
        public PARTIES Parties;

        /// <summary>
        /// Gets or sets a value indicating whether the Parties element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Parties element has been assigned a value.</value>
        [XmlIgnore]
        public bool PartiesSpecified
        {
            get { return this.Parties != null && this.Parties.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 6)]
        public MI_APPLICATION_RESPONSE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
