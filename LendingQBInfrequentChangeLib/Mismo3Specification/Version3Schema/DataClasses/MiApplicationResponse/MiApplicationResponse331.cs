namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class MI_APPLICATION_RESPONSE_3_3_1
    {
        /// <summary>
        /// Gets a value indicating whether the MI_APPLICATION_RESPONSE_3_3_1 container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ConditionsSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// Conditions of mortgage insurance.
        /// </summary>
        [XmlElement("CONDITIONS", Order = 0)]
        public CONDITIONS Conditions;

        /// <summary>
        /// Gets or sets a value indicating whether the Conditions element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Conditions element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConditionsSpecified
        {
            get { return this.Conditions != null && this.Conditions.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public MI_APPLICATION_RESPONSE_3_3_1_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
