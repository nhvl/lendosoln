namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class URLA_TOTAL
    {
        /// <summary>
        /// Gets a value indicating whether the URLA_TOTAL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.URLATotalAssetsAmountSpecified
                    || this.URLATotalBaseIncomeAmountSpecified
                    || this.URLATotalBonusIncomeAmountSpecified
                    || this.URLATotalCashFromToBorrowerAmountSpecified
                    || this.URLATotalCombinedPresentHousingExpenseAmountSpecified
                    || this.URLATotalCombinedProposedHousingExpenseAmountSpecified
                    || this.URLATotalCommissionsIncomeAmountSpecified
                    || this.URLATotalDividendsInterestIncomeAmountSpecified
                    || this.URLATotalLiabilityMonthlyPaymentsAmountSpecified
                    || this.URLATotalLiabilityUPBAmountSpecified
                    || this.URLATotalLotAndImprovementsAmountSpecified
                    || this.URLATotalMonthlyIncomeAmountSpecified
                    || this.URLATotalNetRentalIncomeAmountSpecified
                    || this.URLATotalNetWorthAmountSpecified
                    || this.URLATotalOtherTypesOfIncomeAmountSpecified
                    || this.URLATotalOvertimeIncomeAmountSpecified
                    || this.URLATotalREOLienInstallmentAmountSpecified
                    || this.URLATotalREOLienUPBAmountSpecified
                    || this.URLATotalREOMaintenanceExpenseAmountSpecified
                    || this.URLATotalREOMarketValueAmountSpecified
                    || this.URLATotalREORentalIncomeGrossAmountSpecified
                    || this.URLATotalREORentalIncomeNetAmountSpecified
                    || this.URLATotalTransactionCostAmountSpecified;
            }
        }

        /// <summary>
        /// The total of all assets for all borrowers reported on an instance of the URLA. Collected in Section VI. Assets and Liabilities. 
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount URLATotalAssetsAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the URLATotalAssetsAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the URLATotalAssetsAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool URLATotalAssetsAmountSpecified
        {
            get { return URLATotalAssetsAmount != null; }
            set { }
        }

        /// <summary>
        /// The total of all combined monthly proposed housing expenses reported on an instance of the URLA. Collected on the URLA in Section V. Monthly Income. 
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOAmount URLATotalBaseIncomeAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the URLATotalBaseIncomeAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the URLATotalBaseIncomeAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool URLATotalBaseIncomeAmountSpecified
        {
            get { return URLATotalBaseIncomeAmount != null; }
            set { }
        }

        /// <summary>
        /// The total of gross monthly bonus income amount for all borrowers reported on an instance of the URLA. Collected on the URLA in Section V. Monthly Income. 
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOAmount URLATotalBonusIncomeAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the URLATotalBonusIncomeAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the URLATotalBonusIncomeAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool URLATotalBonusIncomeAmountSpecified
        {
            get { return URLATotalBonusIncomeAmount != null; }
            set { }
        }

        /// <summary>
        /// The total of cash due either to or from the borrower at settlement reported on an instance of the URLA. Collected on Section VII. Details of Transaction. 
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOAmount URLATotalCashFromToBorrowerAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the URLATotalCashFromToBorrowerAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the URLATotalCashFromToBorrowerAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool URLATotalCashFromToBorrowerAmountSpecified
        {
            get { return URLATotalCashFromToBorrowerAmount != null; }
            set { }
        }

        /// <summary>
        /// The total of all combined monthly present housing expenses reported on an instance of the URLA. Collected on the URLA in Section V. Monthly Income. 
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOAmount URLATotalCombinedPresentHousingExpenseAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the URLATotalCombinedPresentHousingExpenseAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the URLATotalCombinedPresentHousingExpenseAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool URLATotalCombinedPresentHousingExpenseAmountSpecified
        {
            get { return URLATotalCombinedPresentHousingExpenseAmount != null; }
            set { }
        }

        /// <summary>
        /// The total of all combined monthly proposed housing expenses reported on an instance of the URLA. Collected on the URLA in Section V. Monthly Income. 
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOAmount URLATotalCombinedProposedHousingExpenseAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the URLATotalCombinedProposedHousingExpenseAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the URLATotalCombinedProposedHousingExpenseAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool URLATotalCombinedProposedHousingExpenseAmountSpecified
        {
            get { return URLATotalCombinedProposedHousingExpenseAmount != null; }
            set { }
        }

        /// <summary>
        /// The total of gross monthly commission income amount for all borrowers reported on an instance of the URLA. Collected on the URLA in Section V. Monthly Income. 
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOAmount URLATotalCommissionsIncomeAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the URLATotalCommissionsIncomeAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the URLATotalCommissionsIncomeAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool URLATotalCommissionsIncomeAmountSpecified
        {
            get { return URLATotalCommissionsIncomeAmount != null; }
            set { }
        }

        /// <summary>
        /// The total of gross monthly dividend and interest income amount for all borrowers reported on the URLA. Collected on the URLA in Section V. Monthly Income. 
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOAmount URLATotalDividendsInterestIncomeAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the URLATotalDividendsInterestIncomeAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the URLATotalDividendsInterestIncomeAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool URLATotalDividendsInterestIncomeAmountSpecified
        {
            get { return URLATotalDividendsInterestIncomeAmount != null; }
            set { }
        }

        /// <summary>
        /// The total of all liabilities for all borrowers reported on an instance of the URLA. Collected in Section VI. Assets and Liabilities. 
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOAmount URLATotalLiabilityMonthlyPaymentsAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the URLATotalLiabilityMonthlyPaymentsAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the URLATotalLiabilityMonthlyPaymentsAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool URLATotalLiabilityMonthlyPaymentsAmountSpecified
        {
            get { return URLATotalLiabilityMonthlyPaymentsAmount != null; }
            set { }
        }

        /// <summary>
        /// The total of all unpaid liabilities for all borrowers reported on an instance of the URLA. Collected in Section VI. Assets and Liabilities. 
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOAmount URLATotalLiabilityUPBAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the URLATotalLiabilityUPBAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the URLATotalLiabilityUPBAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool URLATotalLiabilityUPBAmountSpecified
        {
            get { return URLATotalLiabilityUPBAmount != null; }
            set { }
        }

        /// <summary>
        /// The total of the present value of the lot added to the cost of improvements reported on an instance of the URLA. Collected on the URLA in Construction section. 
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOAmount URLATotalLotAndImprovementsAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the URLATotalLotAndImprovementsAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the URLATotalLotAndImprovementsAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool URLATotalLotAndImprovementsAmountSpecified
        {
            get { return URLATotalLotAndImprovementsAmount != null; }
            set { }
        }

        /// <summary>
        /// The total of all monthly income from all borrowers reported on an instance of the URLA. Collected on the URLA in Section V. Monthly Income. 
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOAmount URLATotalMonthlyIncomeAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the URLATotalMonthlyIncomeAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the URLATotalMonthlyIncomeAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool URLATotalMonthlyIncomeAmountSpecified
        {
            get { return URLATotalMonthlyIncomeAmount != null; }
            set { }
        }

        /// <summary>
        /// The total net rental income amount for all borrowers reported on an instance of the URLA. Collected on the URLA in Section V. Monthly Income. 
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOAmount URLATotalNetRentalIncomeAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the URLATotalNetRentalIncomeAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the URLATotalNetRentalIncomeAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool URLATotalNetRentalIncomeAmountSpecified
        {
            get { return URLATotalNetRentalIncomeAmount != null; }
            set { }
        }

        /// <summary>
        /// The total net worth for all borrowers reported on an instance of the URLA. Collected in Section VI. Assets and Liabilities. 
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOAmount URLATotalNetWorthAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the URLATotalNetWorthAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the URLATotalNetWorthAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool URLATotalNetWorthAmountSpecified
        {
            get { return URLATotalNetWorthAmount != null; }
            set { }
        }

        /// <summary>
        /// The total of gross monthly income amount from other sources as listed in the section "Describe Other Income" for all borrowers reported on an instance of the URLA. Collected on the URLA in Section V. Monthly Income. 
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOAmount URLATotalOtherTypesOfIncomeAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the URLATotalOtherTypesOfIncomeAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the URLATotalOtherTypesOfIncomeAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool URLATotalOtherTypesOfIncomeAmountSpecified
        {
            get { return URLATotalOtherTypesOfIncomeAmount != null; }
            set { }
        }

        /// <summary>
        /// The total of gross monthly overtime income amount for all borrowers reported on an instance of the URLA. Collected on the URLA in Section V. Monthly Income. 
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOAmount URLATotalOvertimeIncomeAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the URLATotalOvertimeIncomeAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the URLATotalOvertimeIncomeAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool URLATotalOvertimeIncomeAmountSpecified
        {
            get { return URLATotalOvertimeIncomeAmount != null; }
            set { }
        }

        /// <summary>
        /// The total of the monthly payments for all liens for all real estate owned reported on an instance of the URLA. Collected on the URLA in Section VI. Assets and Liabilities. Mortgage Payments Total. 
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMOAmount URLATotalREOLienInstallmentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the URLATotalREOLienInstallmentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the URLATotalREOLienInstallmentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool URLATotalREOLienInstallmentAmountSpecified
        {
            get { return URLATotalREOLienInstallmentAmount != null; }
            set { }
        }

        /// <summary>
        /// The total of the unpaid principal balance of all liens against all real estate owned reported on an instance of the URLA. Collected on the URLA in Section VI. Assets and Liabilities. Amount of Mortgages and Liens Total. 
        /// </summary>
        [XmlElement(Order = 17)]
        public MISMOAmount URLATotalREOLienUPBAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the URLATotalREOLienUPBAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the URLATotalREOLienUPBAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool URLATotalREOLienUPBAmountSpecified
        {
            get { return URLATotalREOLienUPBAmount != null; }
            set { }
        }

        /// <summary>
        /// The total of all maintenance expenses for all real estate owned reported on an instance of the URLA. Collected on the URLA in Section VI. Assets and Liabilities. Insurance, Maintenance, Taxes and Insurance Total. 
        /// </summary>
        [XmlElement(Order = 18)]
        public MISMOAmount URLATotalREOMaintenanceExpenseAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the URLATotalREOMaintenanceExpenseAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the URLATotalREOMaintenanceExpenseAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool URLATotalREOMaintenanceExpenseAmountSpecified
        {
            get { return URLATotalREOMaintenanceExpenseAmount != null; }
            set { }
        }

        /// <summary>
        /// The total present market value for all real estate owned for all borrowers reported on an instance of the URLA. Collected on the URLA in Section VI. Assets and Liabilities. Present Market Value Total. 
        /// </summary>
        [XmlElement(Order = 19)]
        public MISMOAmount URLATotalREOMarketValueAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the URLATotalREOMarketValueAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the URLATotalREOMarketValueAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool URLATotalREOMarketValueAmountSpecified
        {
            get { return URLATotalREOMarketValueAmount != null; }
            set { }
        }

        /// <summary>
        /// The total of gross rental income for all real estate owned reported on an instance of the URLA. Collected on the URLA in Section VI. Assets and Liabilities. Gross Rental Income Total. 
        /// </summary>
        [XmlElement(Order = 20)]
        public MISMOAmount URLATotalREORentalIncomeGrossAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the URLATotalREORentalIncomeGrossAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the URLATotalREORentalIncomeGrossAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool URLATotalREORentalIncomeGrossAmountSpecified
        {
            get { return URLATotalREORentalIncomeGrossAmount != null; }
            set { }
        }

        /// <summary>
        /// The total of the net rental income for all real estate owned reported on an instance of the URLA. Collected on the URLA in Section VI. Assets and Liabilities. Net Rental Income Total. 
        /// </summary>
        [XmlElement(Order = 21)]
        public MISMOAmount URLATotalREORentalIncomeNetAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the URLATotalREORentalIncomeNetAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the URLATotalREORentalIncomeNetAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool URLATotalREORentalIncomeNetAmountSpecified
        {
            get { return URLATotalREORentalIncomeNetAmount != null; }
            set { }
        }

        /// <summary>
        /// The total costs of all details of transaction items reported on an instance of the URLA. Collected on Section VII. Details of Transaction. 
        /// </summary>
        [XmlElement(Order = 22)]
        public MISMOAmount URLATotalTransactionCostAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the URLATotalTransactionCostAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the URLATotalTransactionCostAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool URLATotalTransactionCostAmountSpecified
        {
            get { return URLATotalTransactionCostAmount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 23)]
        public URLA_TOTAL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
