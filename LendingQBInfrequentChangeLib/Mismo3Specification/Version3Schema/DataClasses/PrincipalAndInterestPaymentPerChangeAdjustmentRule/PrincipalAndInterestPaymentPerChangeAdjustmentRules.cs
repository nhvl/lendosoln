namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PRINCIPAL_AND_INTEREST_PAYMENT_PER_CHANGE_ADJUSTMENT_RULES
    {
        /// <summary>
        /// Gets a value indicating whether the PRINCIPAL_AND_INTEREST_PAYMENT_PER_CHANGE_ADJUSTMENT_RULES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.PrincipalAndInterestPaymentPerChangeAdjustmentRuleSpecified;
            }
        }

        /// <summary>
        /// A collection of principal and interest payment per change adjustment rules.
        /// </summary>
        [XmlElement("PRINCIPAL_AND_INTEREST_PAYMENT_PER_CHANGE_ADJUSTMENT_RULE", Order = 0)]
		public List<PRINCIPAL_AND_INTEREST_PAYMENT_PER_CHANGE_ADJUSTMENT_RULE> PrincipalAndInterestPaymentPerChangeAdjustmentRule = new List<PRINCIPAL_AND_INTEREST_PAYMENT_PER_CHANGE_ADJUSTMENT_RULE>();

        /// <summary>
        /// Gets or sets a value indicating whether the PrincipalAndInterestPaymentPerChangeAdjustmentRule element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrincipalAndInterestPaymentPerChangeAdjustmentRule element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrincipalAndInterestPaymentPerChangeAdjustmentRuleSpecified
        {
            get { return this.PrincipalAndInterestPaymentPerChangeAdjustmentRule != null && this.PrincipalAndInterestPaymentPerChangeAdjustmentRule.Count(p => p != null && p.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public PRINCIPAL_AND_INTEREST_PAYMENT_PER_CHANGE_ADJUSTMENT_RULES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
