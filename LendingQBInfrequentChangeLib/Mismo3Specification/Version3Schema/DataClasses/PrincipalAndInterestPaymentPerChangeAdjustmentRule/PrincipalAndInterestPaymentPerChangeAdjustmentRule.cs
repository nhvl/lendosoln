namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class PRINCIPAL_AND_INTEREST_PAYMENT_PER_CHANGE_ADJUSTMENT_RULE
    {
        /// <summary>
        /// Gets a value indicating whether the PRINCIPAL_AND_INTEREST_PAYMENT_PER_CHANGE_ADJUSTMENT_RULE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AdjustmentRuleTypeSpecified
                    || this.ExtensionSpecified
                    || this.NoPrincipalAndInterestPaymentCapsFeatureDescriptionSpecified
                    || this.PaymentsBetweenPrincipalAndInterestPaymentChangesCountSpecified
                    || this.PerChangeMaximumPrincipalAndInterestPaymentAmountSpecified
                    || this.PerChangeMaximumPrincipalAndInterestPaymentDecreaseAmountSpecified
                    || this.PerChangeMaximumPrincipalAndInterestPaymentDecreasePercentSpecified
                    || this.PerChangeMaximumPrincipalAndInterestPaymentIncreaseAmountSpecified
                    || this.PerChangeMaximumPrincipalAndInterestPaymentIncreasePercentSpecified
                    || this.PerChangeMinimumPrincipalAndInterestPaymentAmountSpecified
                    || this.PerChangePrincipalAndInterestCalculationTypeOtherDescriptionSpecified
                    || this.PerChangePrincipalAndInterestCalculationTypeSpecified
                    || this.PerChangePrincipalAndInterestPaymentAdjustmentAmountSpecified
                    || this.PerChangePrincipalAndInterestPaymentAdjustmentEffectiveDateSpecified
                    || this.PerChangePrincipalAndInterestPaymentAdjustmentEffectiveMonthsCountSpecified
                    || this.PerChangePrincipalAndInterestPaymentAdjustmentFrequencyMonthsCountSpecified
                    || this.PerChangePrincipalAndInterestPaymentAdjustmentPercentSpecified
                    || this.PerChangePrincipalAndInterestPaymentMinimumDecreaseRatePercentSpecified
                    || this.PerChangePrincipalAndInterestPaymentMinimumIncreaseRatePercentSpecified;
            }
        }

        /// <summary>
        /// Specifies whether the occurrence of the adjustment is the first change or a subsequent change.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<AdjustmentRuleBase> AdjustmentRuleType;

        /// <summary>
        /// Gets or sets a value indicating whether the AdjustmentRuleType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AdjustmentRuleType element has been assigned a value.</value>
        [XmlIgnore]
        public bool AdjustmentRuleTypeSpecified
        {
            get { return this.AdjustmentRuleType != null && this.AdjustmentRuleType.enumValue != AdjustmentRuleBase.Blank; }
            set { }
        }

        /// <summary>
        /// A description of any principal and interest payment calculation period that ignores per change or lifetime payment limits.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString NoPrincipalAndInterestPaymentCapsFeatureDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the NoPrincipalAndInterestPaymentCapsFeatureDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NoPrincipalAndInterestPaymentCapsFeatureDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool NoPrincipalAndInterestPaymentCapsFeatureDescriptionSpecified
        {
            get { return NoPrincipalAndInterestPaymentCapsFeatureDescription != null; }
            set { }
        }

        /// <summary>
        /// The number of payments between scheduled principal and interest payment changes.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOCount PaymentsBetweenPrincipalAndInterestPaymentChangesCount;

        /// <summary>
        /// Gets or sets a value indicating whether the PaymentsBetweenPrincipalAndInterestPaymentChangesCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PaymentsBetweenPrincipalAndInterestPaymentChangesCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PaymentsBetweenPrincipalAndInterestPaymentChangesCountSpecified
        {
            get { return PaymentsBetweenPrincipalAndInterestPaymentChangesCount != null; }
            set { }
        }

        /// <summary>
        /// The maximum dollar amount of the principal and interest payment possible for the specified payment change. 
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOAmount PerChangeMaximumPrincipalAndInterestPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PerChangeMaximumPrincipalAndInterestPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PerChangeMaximumPrincipalAndInterestPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PerChangeMaximumPrincipalAndInterestPaymentAmountSpecified
        {
            get { return PerChangeMaximumPrincipalAndInterestPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// The maximum dollar amount by which the principal and interest payment can decrease from the previous principal and interest payment.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOAmount PerChangeMaximumPrincipalAndInterestPaymentDecreaseAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PerChangeMaximumPrincipalAndInterestPaymentDecreaseAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PerChangeMaximumPrincipalAndInterestPaymentDecreaseAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PerChangeMaximumPrincipalAndInterestPaymentDecreaseAmountSpecified
        {
            get { return PerChangeMaximumPrincipalAndInterestPaymentDecreaseAmount != null; }
            set { }
        }

        /// <summary>
        /// The maximum number of percentage points by which the principal and interest payment can decrease from the previous principal and interest payment.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOPercent PerChangeMaximumPrincipalAndInterestPaymentDecreasePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the PerChangeMaximumPrincipalAndInterestPaymentDecreasePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PerChangeMaximumPrincipalAndInterestPaymentDecreasePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool PerChangeMaximumPrincipalAndInterestPaymentDecreasePercentSpecified
        {
            get { return PerChangeMaximumPrincipalAndInterestPaymentDecreasePercent != null; }
            set { }
        }

        /// <summary>
        /// The maximum dollar amount by which the principal and interest payment can increase from the previous principal and interest payment.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOAmount PerChangeMaximumPrincipalAndInterestPaymentIncreaseAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PerChangeMaximumPrincipalAndInterestPaymentIncreaseAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PerChangeMaximumPrincipalAndInterestPaymentIncreaseAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PerChangeMaximumPrincipalAndInterestPaymentIncreaseAmountSpecified
        {
            get { return PerChangeMaximumPrincipalAndInterestPaymentIncreaseAmount != null; }
            set { }
        }

        /// <summary>
        /// The maximum number of percentage points by which the principal and interest payment can increase from the previous principal and interest payment.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOPercent PerChangeMaximumPrincipalAndInterestPaymentIncreasePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the PerChangeMaximumPrincipalAndInterestPaymentIncreasePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PerChangeMaximumPrincipalAndInterestPaymentIncreasePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool PerChangeMaximumPrincipalAndInterestPaymentIncreasePercentSpecified
        {
            get { return PerChangeMaximumPrincipalAndInterestPaymentIncreasePercent != null; }
            set { }
        }

        /// <summary>
        /// The minimum dollar amount of the principal and interest payment possible for the specified payment change. 
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOAmount PerChangeMinimumPrincipalAndInterestPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PerChangeMinimumPrincipalAndInterestPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PerChangeMinimumPrincipalAndInterestPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PerChangeMinimumPrincipalAndInterestPaymentAmountSpecified
        {
            get { return PerChangeMinimumPrincipalAndInterestPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// Describes the method employed to calculate the principal and interest payment of the loan.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOEnum<PerChangePrincipalAndInterestCalculationBase> PerChangePrincipalAndInterestCalculationType;

        /// <summary>
        /// Gets or sets a value indicating whether the PerChangePrincipalAndInterestCalculationType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PerChangePrincipalAndInterestCalculationType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PerChangePrincipalAndInterestCalculationTypeSpecified
        {
            get { return this.PerChangePrincipalAndInterestCalculationType != null && this.PerChangePrincipalAndInterestCalculationType.enumValue != PerChangePrincipalAndInterestCalculationBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Per Change Principal And Interest Payment Adjustment Calculation Method Type.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOString PerChangePrincipalAndInterestCalculationTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PerChangePrincipalAndInterestCalculationTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PerChangePrincipalAndInterestCalculationTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PerChangePrincipalAndInterestCalculationTypeOtherDescriptionSpecified
        {
            get { return PerChangePrincipalAndInterestCalculationTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount by which the principal and interest payment adjusts.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOAmount PerChangePrincipalAndInterestPaymentAdjustmentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PerChangePrincipalAndInterestPaymentAdjustmentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PerChangePrincipalAndInterestPaymentAdjustmentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PerChangePrincipalAndInterestPaymentAdjustmentAmountSpecified
        {
            get { return PerChangePrincipalAndInterestPaymentAdjustmentAmount != null; }
            set { }
        }

        /// <summary>
        /// The payment due date when the principal and interest payment per change adjustment rule first becomes applicable. The per change rule remains in effect unless another per change rule with a later date is present on the loan.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMODate PerChangePrincipalAndInterestPaymentAdjustmentEffectiveDate;

        /// <summary>
        /// Gets or sets a value indicating whether the PerChangePrincipalAndInterestPaymentAdjustmentEffectiveDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PerChangePrincipalAndInterestPaymentAdjustmentEffectiveDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool PerChangePrincipalAndInterestPaymentAdjustmentEffectiveDateSpecified
        {
            get { return PerChangePrincipalAndInterestPaymentAdjustmentEffectiveDate != null; }
            set { }
        }

        /// <summary>
        /// The number of months over which the set of note terms governing principal and interest payment adjustments are in effect.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOCount PerChangePrincipalAndInterestPaymentAdjustmentEffectiveMonthsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the PerChangePrincipalAndInterestPaymentAdjustmentEffectiveMonthsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PerChangePrincipalAndInterestPaymentAdjustmentEffectiveMonthsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PerChangePrincipalAndInterestPaymentAdjustmentEffectiveMonthsCountSpecified
        {
            get { return PerChangePrincipalAndInterestPaymentAdjustmentEffectiveMonthsCount != null; }
            set { }
        }

        /// <summary>
        /// The number of months between payment adjustments, if the payment on the subject loan can change.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOCount PerChangePrincipalAndInterestPaymentAdjustmentFrequencyMonthsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the PerChangePrincipalAndInterestPaymentAdjustmentFrequencyMonthsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PerChangePrincipalAndInterestPaymentAdjustmentFrequencyMonthsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PerChangePrincipalAndInterestPaymentAdjustmentFrequencyMonthsCountSpecified
        {
            get { return PerChangePrincipalAndInterestPaymentAdjustmentFrequencyMonthsCount != null; }
            set { }
        }

        /// <summary>
        /// The percent by which the principal and interest payment adjusts.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOPercent PerChangePrincipalAndInterestPaymentAdjustmentPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the PerChangePrincipalAndInterestPaymentAdjustmentPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PerChangePrincipalAndInterestPaymentAdjustmentPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool PerChangePrincipalAndInterestPaymentAdjustmentPercentSpecified
        {
            get { return PerChangePrincipalAndInterestPaymentAdjustmentPercent != null; }
            set { }
        }

        /// <summary>
        /// The minimum percentage by which an ARM loans principal and interest payment must decrease relative to the prior principal and interest payment before it is actually changed on the loan. If the new principal and interest payment does not decrease by at least than this amount, the principal and interest payment remains the same.
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMOPercent PerChangePrincipalAndInterestPaymentMinimumDecreaseRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the PerChangePrincipalAndInterestPaymentMinimumDecreaseRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PerChangePrincipalAndInterestPaymentMinimumDecreaseRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool PerChangePrincipalAndInterestPaymentMinimumDecreaseRatePercentSpecified
        {
            get { return PerChangePrincipalAndInterestPaymentMinimumDecreaseRatePercent != null; }
            set { }
        }

        /// <summary>
        /// The minimum percentage by which an ARM loans principal and interest payment must increase relative to the prior principal and interest payment before it is actually changed on the loan. If the new principal and interest payment does not increase by at least this amount, the principal and interest payment remains the same.
        /// </summary>
        [XmlElement(Order = 17)]
        public MISMOPercent PerChangePrincipalAndInterestPaymentMinimumIncreaseRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the PerChangePrincipalAndInterestPaymentMinimumIncreaseRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PerChangePrincipalAndInterestPaymentMinimumIncreaseRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool PerChangePrincipalAndInterestPaymentMinimumIncreaseRatePercentSpecified
        {
            get { return PerChangePrincipalAndInterestPaymentMinimumIncreaseRatePercent != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 18)]
        public PRINCIPAL_AND_INTEREST_PAYMENT_PER_CHANGE_ADJUSTMENT_RULE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
