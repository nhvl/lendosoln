namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class RECORDING_ENDORSEMENTS
    {
        /// <summary>
        /// Gets a value indicating whether the RECORDING_ENDORSEMENTS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.RecordingEndorsementSpecified;
            }
        }

        /// <summary>
        /// A collection of recording endorsements.
        /// </summary>
        [XmlElement("RECORDING_ENDORSEMENT", Order = 0)]
		public List<RECORDING_ENDORSEMENT> RecordingEndorsement = new List<RECORDING_ENDORSEMENT>();

        /// <summary>
        /// Gets or sets a value indicating whether the RecordingEndorsement element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RecordingEndorsement element has been assigned a value.</value>
        [XmlIgnore]
        public bool RecordingEndorsementSpecified
        {
            get { return this.RecordingEndorsement != null && this.RecordingEndorsement.Count(r => r != null && r.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public RECORDING_ENDORSEMENTS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
