namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class VERIFICATION_OF_INCOME_REQUEST
    {
        /// <summary>
        /// Gets a value indicating whether the VERIFICATION_OF_INCOME_REQUEST container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.RequestForTaxReturnDocumentationSpecified;
            }
        }

        /// <summary>
        /// A request for tax return documents.
        /// </summary>
        [XmlElement("REQUEST_FOR_TAX_RETURN_DOCUMENTATION", Order = 0)]
        public REQUEST_FOR_TAX_RETURN_DOCUMENTATION RequestForTaxReturnDocumentation;

        /// <summary>
        /// Gets or sets a value indicating whether the RequestForTaxReturnDocumentation element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RequestForTaxReturnDocumentation element has been assigned a value.</value>
        [XmlIgnore]
        public bool RequestForTaxReturnDocumentationSpecified
        {
            get { return this.RequestForTaxReturnDocumentation != null && this.RequestForTaxReturnDocumentation.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public VERIFICATION_OF_INCOME_REQUEST_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
