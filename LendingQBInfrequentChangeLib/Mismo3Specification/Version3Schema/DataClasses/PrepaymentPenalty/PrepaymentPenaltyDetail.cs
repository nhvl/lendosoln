namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PREPAYMENT_PENALTY_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the PREPAYMENT_PENALTY_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.PrepaymentPenaltyWaivedAmountSpecified
                    || this.PrepaymentPenaltyWaivedReasonDescriptionSpecified;
            }
        }

        /// <summary>
        /// The total amount of the prepayment penalty that was incurred but waived per the investor guidelines.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount PrepaymentPenaltyWaivedAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaymentPenaltyWaivedAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaymentPenaltyWaivedAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaymentPenaltyWaivedAmountSpecified
        {
            get { return PrepaymentPenaltyWaivedAmount != null; }
            set { }
        }

        /// <summary>
        /// Describes the reason that a prepayment penalty due from a borrower was not collected.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString PrepaymentPenaltyWaivedReasonDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaymentPenaltyWaivedReasonDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaymentPenaltyWaivedReasonDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaymentPenaltyWaivedReasonDescriptionSpecified
        {
            get { return PrepaymentPenaltyWaivedReasonDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public PREPAYMENT_PENALTY_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
