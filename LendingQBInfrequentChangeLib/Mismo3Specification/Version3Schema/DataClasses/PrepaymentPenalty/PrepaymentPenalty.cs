namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PREPAYMENT_PENALTY
    {
        /// <summary>
        /// Gets a value indicating whether the PREPAYMENT_PENALTY container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.PrepaymentPenaltyDetailSpecified
                    || this.PrepaymentPenaltyLifetimeRuleSpecified
                    || this.PrepaymentPenaltyPerChangeRulesSpecified;
            }
        }

        /// <summary>
        /// Details on the prepayment penalty.
        /// </summary>
        [XmlElement("PREPAYMENT_PENALTY_DETAIL", Order = 0)]
        public PREPAYMENT_PENALTY_DETAIL PrepaymentPenaltyDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaymentPenaltyDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaymentPenaltyDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaymentPenaltyDetailSpecified
        {
            get { return this.PrepaymentPenaltyDetail != null && this.PrepaymentPenaltyDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A lifetime rule on a prepayment penalty.
        /// </summary>
        [XmlElement("PREPAYMENT_PENALTY_LIFETIME_RULE", Order = 1)]
        public PREPAYMENT_PENALTY_LIFETIME_RULE PrepaymentPenaltyLifetimeRule;

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaymentPenaltyLifetimeRule element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaymentPenaltyLifetimeRule element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaymentPenaltyLifetimeRuleSpecified
        {
            get { return this.PrepaymentPenaltyLifetimeRule != null && this.PrepaymentPenaltyLifetimeRule.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A collection of prepayment penalty per change rules.
        /// </summary>
        [XmlElement("PREPAYMENT_PENALTY_PER_CHANGE_RULES", Order = 2)]
        public PREPAYMENT_PENALTY_PER_CHANGE_RULES PrepaymentPenaltyPerChangeRules;

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaymentPenaltyPerChangeRules element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaymentPenaltyPerChangeRules element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaymentPenaltyPerChangeRulesSpecified
        {
            get { return this.PrepaymentPenaltyPerChangeRules != null && this.PrepaymentPenaltyPerChangeRules.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public PREPAYMENT_PENALTY_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
