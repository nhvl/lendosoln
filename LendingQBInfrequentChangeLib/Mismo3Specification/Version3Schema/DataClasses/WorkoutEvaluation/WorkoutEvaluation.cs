namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class WORKOUT_EVALUATION
    {
        /// <summary>
        /// Gets a value indicating whether the WORKOUT_EVALUATION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.WorkoutEvaluationRequestSpecified
                    || this.WorkoutEvaluationResponseSpecified;
            }
        }

        /// <summary>
        /// A request for workout evaluation.
        /// </summary>
        [XmlElement("WORKOUT_EVALUATION_REQUEST", Order = 0)]
        public WORKOUT_EVALUATION_REQUEST WorkoutEvaluationRequest;

        /// <summary>
        /// Gets or sets a value indicating whether the WorkoutEvaluationRequest element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the WorkoutEvaluationRequest element has been assigned a value.</value>
        [XmlIgnore]
        public bool WorkoutEvaluationRequestSpecified
        {
            get { return this.WorkoutEvaluationRequest != null && this.WorkoutEvaluationRequest.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A response to a workout evaluation request.
        /// </summary>
        [XmlElement("WORKOUT_EVALUATION_RESPONSE", Order = 1)]
        public WORKOUT_EVALUATION_RESPONSE WorkoutEvaluationResponse;

        /// <summary>
        /// Gets or sets a value indicating whether the WorkoutEvaluationResponse element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the WorkoutEvaluationResponse element has been assigned a value.</value>
        [XmlIgnore]
        public bool WorkoutEvaluationResponseSpecified
        {
            get { return this.WorkoutEvaluationResponse != null && this.WorkoutEvaluationResponse.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public WORKOUT_EVALUATION_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
