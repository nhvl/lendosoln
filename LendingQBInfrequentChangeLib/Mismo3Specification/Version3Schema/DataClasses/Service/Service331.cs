namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class SERVICE_3_3_1
    {
        /// <summary>
        /// Gets a value indicating whether the SERVICE_3_3_1 container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ReasonsSpecified;
            }
        }

        /// <summary>
        /// Reason for a service.
        /// </summary>
        [XmlElement("REASONS", Order = 0)]
        public REASONS Reasons;

        /// <summary>
        /// Gets or sets a value indicating whether the Reasons element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Reasons element has been assigned a value.</value>
        [XmlIgnore]
        public bool ReasonsSpecified
        {
            get { return this.Reasons != null && this.Reasons.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public SERVICE_3_3_1_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
