namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class SERVICE
    {
        /// <summary>
        /// Gets a value indicating whether the SERVICE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                if (Mismo3Utilities.ExceedsThreshold(1,
                    new List<bool> { 
                        this.AutomatedUnderwritingSystemSpecified,
                        this.ClaimSpecified,
                        this.CreditSpecified,
                        this.DataChangeSpecified,
                        this.DocumentManagementSpecified,
                        this.FloodSpecified,
                        this.FraudSpecified,
                        this.InspectionServiceSpecified,
                        this.LoanDeliverySpecified,
                        this.MERSServiceSpecified,
                        this.MISpecified,
                        this.PRIASpecified,
                        this.TaxSpecified,
                        this.TitleSpecified,
                        this.ValuationSpecified,
                        this.VerificationOfIncomeSpecified }))
                {
                    throw new System.Xml.Schema.XmlSchemaValidationException(
                        Mismo3Utilities.GetXSDChoiceExceptionMessage(
                        "SERVICE",
                        new List<string> { 
                            "AUTOMATED_UNDERWRITING_SYSTEM", 
                            "CLAIM",
                            "CREDIT",
                            "DATA_CHANGE",
                            "DOCUMENT_MANAGEMENT",
                            "FLOOD",
                            "FRAUD",
                            "INSPECTION_SERVICE",
                            "LOAN_DELIVERY",
                            "MERS_SERVICE",
                            "MI",
                            "PRIA",
                            "TAX",
                            "TITLE",
                            "VALUATION",
                            "VERIFICATION_OF_INCOME"}));
                }

                return this.AutomatedUnderwritingSystemSpecified
                    || this.ClaimSpecified
                    || this.CreditSpecified
                    || this.DataChangeSpecified
                    || this.DocumentManagementSpecified
                    || this.ErrorsSpecified
                    || this.ExtensionSpecified
                    || this.FloodSpecified
                    || this.FraudSpecified
                    || this.InspectionServiceSpecified
                    || this.LoanDeliverySpecified
                    || this.MERSServiceSpecified
                    || this.MISpecified
                    || this.PRIASpecified
                    || this.RelationshipsSpecified
                    || this.ReportingInformationSpecified
                    || this.ServicePaymentsSpecified
                    || this.ServiceProductFulfillmentSpecified
                    || this.ServiceProductSpecified
                    || this.StatusesSpecified
                    || this.TaxSpecified
                    || this.TitleSpecified
                    || this.ValuationSpecified
                    || this.VerificationOfIncomeSpecified;
            }
        }

        /// <summary>
        /// The automated underwriting service.
        /// </summary>
        [XmlElement("AUTOMATED_UNDERWRITING_SYSTEM", Order = 0)]
        public AUTOMATED_UNDERWRITING_SYSTEM AutomatedUnderwritingSystem;

        /// <summary>
        /// Gets or sets a value indicating whether the AutomatedUnderwritingSystem element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AutomatedUnderwritingSystem element has been assigned a value.</value>
        [XmlIgnore]
        public bool AutomatedUnderwritingSystemSpecified
        {
            get { return this.AutomatedUnderwritingSystem != null && this.AutomatedUnderwritingSystem.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The claim service.
        /// </summary>
        [XmlElement("CLAIM", Order = 1)]
        public CLAIM Claim;

        /// <summary>
        /// Gets or sets a value indicating whether the Claim element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Claim element has been assigned a value.</value>
        [XmlIgnore]
        public bool ClaimSpecified
        {
            get { return this.Claim != null && this.Claim.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The credit service.
        /// </summary>
        [XmlElement("CREDIT", typeof(CREDIT), Order = 2)]
        public CREDIT Credit;

        /// <summary>
        /// Gets or sets a value indicating whether the Credit element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Credit element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditSpecified
        {
            get { return this.Credit != null && this.Credit.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The data change service.
        /// </summary>
        [XmlElement("DATA_CHANGE", Order = 3)]
        public DATA_CHANGE DataChange;

        /// <summary>
        /// Gets or sets a value indicating whether the DataChange element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DataChange element has been assigned a value.</value>
        [XmlIgnore]
        public bool DataChangeSpecified
        {
            get { return this.DataChange != null && this.DataChange.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The document management service.
        /// </summary>
        [XmlElement("DOCUMENT_MANAGEMENT", Order = 4)]
        public DOCUMENT_MANAGEMENT DocumentManagement;

        /// <summary>
        /// Gets or sets a value indicating whether the DocumentManagement element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DocumentManagement element has been assigned a value.</value>
        [XmlIgnore]
        public bool DocumentManagementSpecified
        {
            get { return this.DocumentManagement != null && this.DocumentManagement.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The flood service.
        /// </summary>
        [XmlElement("FLOOD", Order = 5)]
        public FLOOD Flood;

        /// <summary>
        /// Gets or sets a value indicating whether the Flood element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Flood element has been assigned a value.</value>
        [XmlIgnore]
        public bool FloodSpecified
        {
            get { return this.Flood != null && this.Flood.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The fraud service.
        /// </summary>
        [XmlElement("FRAUD", Order = 6)]
        public FRAUD Fraud;

        /// <summary>
        /// Gets or sets a value indicating whether the Fraud element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Fraud element has been assigned a value.</value>
        [XmlIgnore]
        public bool FraudSpecified
        {
            get { return this.Fraud != null && this.Fraud.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The inspection service.
        /// </summary>
        [XmlElement("INSPECTION_SERVICE", Order = 7)]
        public INSPECTION_SERVICE InspectionService;

        /// <summary>
        /// Gets or sets a value indicating whether the InspectionService element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InspectionService element has been assigned a value.</value>
        [XmlIgnore]
        public bool InspectionServiceSpecified
        {
            get { return this.InspectionService != null && this.InspectionService.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The loan delivery service.
        /// </summary>
        [XmlElement("LOAN_DELIVERY", Order = 8)]
        public LOAN_DELIVERY LoanDelivery;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanDelivery element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanDelivery element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanDeliverySpecified
        {
            get { return this.LoanDelivery != null && this.LoanDelivery.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The MERS service.
        /// </summary>
        [XmlElement("MERS_SERVICE", Order = 9)]
        public MERS_SERVICE MERSService;

        /// <summary>
        /// Gets or sets a value indicating whether the MERSService element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MERSService element has been assigned a value.</value>
        [XmlIgnore]
        public bool MERSServiceSpecified
        {
            get { return this.MERSService != null && this.MERSService.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Mortgage insurance service.
        /// </summary>
        [XmlElement("MI", Order = 10)]
        public MI MI;

        /// <summary>
        /// Gets or sets a value indicating whether the MI element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MI element has been assigned a value.</value>
        [XmlIgnore]
        public bool MISpecified
        {
            get { return this.MI != null && this.MI.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The service PRIA.
        /// </summary>
        [XmlElement("PRIA", Order = 11)]
        public PRIA PRIA;

        /// <summary>
        /// Gets or sets a value indicating whether the PRIA element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PRIA element has been assigned a value.</value>
        [XmlIgnore]
        public bool PRIASpecified
        {
            get { return this.PRIA != null && this.PRIA.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Tax for the service.
        /// </summary>
        [XmlElement("TAX", Order = 12)]
        public TAX Tax;

        /// <summary>
        /// Gets or sets a value indicating whether the Tax element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Tax element has been assigned a value.</value>
        [XmlIgnore]
        public bool TaxSpecified
        {
            get { return this.Tax != null && this.Tax.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Title of the service.
        /// </summary>
        [XmlElement("TITLE", Order = 13)]
        public TITLE Title;

        /// <summary>
        /// Gets or sets a value indicating whether the Title element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Title element has been assigned a value.</value>
        [XmlIgnore]
        public bool TitleSpecified
        {
            get { return this.Title != null && this.Title.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Valuation of the service.
        /// </summary>
        [XmlElement("VALUATION", Order = 14)]
        public VALUATION Valuation;

        /// <summary>
        /// Gets or sets a value indicating whether the Valuation element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Valuation element has been assigned a value.</value>
        [XmlIgnore]
        public bool ValuationSpecified
        {
            get { return this.Valuation != null && this.Valuation.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Verification of income for the service.
        /// </summary>
        [XmlElement("VERIFICATION_OF_INCOME", Order = 15)]
        public VERIFICATION_OF_INCOME VerificationOfIncome;

        /// <summary>
        /// Gets or sets a value indicating whether the VerificationOfIncome element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the VerificationOfIncome element has been assigned a value.</value>
        [XmlIgnore]
        public bool VerificationOfIncomeSpecified
        {
            get { return this.VerificationOfIncome != null && this.VerificationOfIncome.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Errors in the service.
        /// </summary>
        [XmlElement("ERRORS", Order = 16)]
        public ERRORS Errors;

        /// <summary>
        /// Gets or sets a value indicating whether the Errors element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Errors element has been assigned a value.</value>
        [XmlIgnore]
        public bool ErrorsSpecified
        {
            get { return this.Errors != null && this.Errors.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Relationships related to the service.
        /// </summary>
        [XmlElement("RELATIONSHIPS", Order = 17)]
        public RELATIONSHIPS Relationships;

        /// <summary>
        /// Gets or sets a value indicating whether the Relationships element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Relationships element has been assigned a value.</value>
        [XmlIgnore]
        public bool RelationshipsSpecified
        {
            get { return this.Relationships != null && this.Relationships.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Information about the service.
        /// </summary>
        [XmlElement("REPORTING_INFORMATION", Order = 18)]
        public REPORTING_INFORMATION ReportingInformation;

        /// <summary>
        /// Gets or sets a value indicating whether the ReportingInformation element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ReportingInformation element has been assigned a value.</value>
        [XmlIgnore]
        public bool ReportingInformationSpecified
        {
            get { return this.ReportingInformation != null && this.ReportingInformation.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Payments for service.
        /// </summary>
        [XmlElement("SERVICE_PAYMENTS", Order = 19)]
        public SERVICE_PAYMENTS ServicePayments;

        /// <summary>
        /// Gets or sets a value indicating whether the ServicePayments element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServicePayments element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServicePaymentsSpecified
        {
            get { return this.ServicePayments != null && this.ServicePayments.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Products of the service.
        /// </summary>
        [XmlElement("SERVICE_PRODUCT", Order = 20)]
        public SERVICE_PRODUCT ServiceProduct;

        /// <summary>
        /// Gets or sets a value indicating whether the ServiceProduct element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServiceProduct element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServiceProductSpecified
        {
            get { return this.ServiceProduct != null && this.ServiceProduct.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Product fulfillments of the services.
        /// </summary>
        [XmlElement("SERVICE_PRODUCT_FULFILLMENT", Order = 21)]
        public SERVICE_PRODUCT_FULFILLMENT ServiceProductFulfillment;

        /// <summary>
        /// Gets or sets a value indicating whether the ServiceProductFulfillment element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServiceProductFulfillment element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServiceProductFulfillmentSpecified
        {
            get { return this.ServiceProductFulfillment != null && this.ServiceProductFulfillment.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Statuses of the collective services.
        /// </summary>
        [XmlElement("STATUSES", Order = 22)]
        public STATUSES Statuses;

        /// <summary>
        /// Gets or sets a value indicating whether the Statuses element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Statuses element has been assigned a value.</value>
        [XmlIgnore]
        public bool StatusesSpecified
        {
            get { return this.Statuses != null && this.Statuses.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 23)]
        public SERVICE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
