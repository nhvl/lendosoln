namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class SERVICES
    {
        /// <summary>
        /// Gets a value indicating whether the SERVICES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ForeignObjectsSpecified
                    || this.ServiceSpecified;
            }
        }

        /// <summary>
        /// Foreign objects related to these services.
        /// </summary>
        [XmlElement("FOREIGN_OBJECTS", Order = 0)]
        public FOREIGN_OBJECTS ForeignObjects;

        /// <summary>
        /// Gets or sets a value indicating whether the ForeignObjects element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ForeignObjects element has been assigned a value.</value>
        [XmlIgnore]
        public bool ForeignObjectsSpecified
        {
            get { return this.ForeignObjects != null && this.ForeignObjects.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A collection of services.
        /// </summary>
        [XmlElement("SERVICE", Order = 1)]
		public List<SERVICE> Service = new List<SERVICE>();

        /// <summary>
        /// Gets or sets a value indicating whether the Service element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Service element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServiceSpecified
        {
            get { return this.Service != null && this.Service.Count(s => s != null && s.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public SERVICES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
