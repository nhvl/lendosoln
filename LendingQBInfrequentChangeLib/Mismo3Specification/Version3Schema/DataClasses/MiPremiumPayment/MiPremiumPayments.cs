namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class MI_PREMIUM_PAYMENTS
    {
        /// <summary>
        /// Gets a value indicating whether the MI_PREMIUM_PAYMENTS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.MiPremiumPaymentSpecified;
            }
        }

        /// <summary>
        /// A collection of mortgage insurance premium payments.
        /// </summary>
        [XmlElement("MI_PREMIUM_PAYMENT", Order = 0)]
		public List<MI_PREMIUM_PAYMENT> MiPremiumPayment = new List<MI_PREMIUM_PAYMENT>();

        /// <summary>
        /// Gets or sets a value indicating whether the Mortgage Insurance Premium Payment element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Mortgage Insurance Premium Payment element has been assigned a value.</value>
        [XmlIgnore]
        public bool MiPremiumPaymentSpecified
        {
            get { return this.MiPremiumPayment != null && this.MiPremiumPayment.Count(m => m != null && m.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public MI_PREMIUM_PAYMENTS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
