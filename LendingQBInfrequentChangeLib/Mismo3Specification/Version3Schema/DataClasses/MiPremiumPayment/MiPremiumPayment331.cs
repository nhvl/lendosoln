namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class MI_PREMIUM_PAYMENT_3_3_1
    {
        /// <summary>
        /// Gets a value indicating whether the MI_PREMIUM_PAYMENT_3_3_1 container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.MIPremiumBlendedPaymentAndTaxAmountSpecified
                    || this.MIPremiumBlendedPaymentAndTaxPercentSpecified;
            }
        }

        /// <summary>
        /// A calculated value representing the sum of the premium amount and the tax amount for the payment of a mortgage insurance policy.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount MIPremiumBlendedPaymentAndTaxAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPremiumBlendedPaymentAndTaxAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPremiumBlendedPaymentAndTaxAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumBlendedPaymentAndTaxAmountSpecified
        {
            get { return MIPremiumBlendedPaymentAndTaxAmount != null; }
            set { }
        }

        /// <summary>
        /// A calculated value representing the combination of the mortgage insurance percentage and the tax percentage for the payment of a mortgage insurance policy.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOPercent MIPremiumBlendedPaymentAndTaxPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPremiumBlendedPaymentAndTaxPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPremiumBlendedPaymentAndTaxPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumBlendedPaymentAndTaxPercentSpecified
        {
            get { return MIPremiumBlendedPaymentAndTaxPercent != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public MI_PREMIUM_PAYMENT_3_3_1_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
