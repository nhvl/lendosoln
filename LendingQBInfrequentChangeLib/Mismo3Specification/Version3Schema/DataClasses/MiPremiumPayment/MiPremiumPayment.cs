namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class MI_PREMIUM_PAYMENT
    {
        /// <summary>
        /// Gets a value indicating whether the MI_PREMIUM_PAYMENT container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.MIPremiumActualPaymentAmountSpecified
                    || this.MIPremiumEstimatedPaymentAmountSpecified
                    || this.MIPremiumPaymentPaidByTypeOtherDescriptionSpecified
                    || this.MIPremiumPaymentPaidByTypeSpecified
                    || this.MIPremiumPaymentTimingTypeOtherDescriptionSpecified
                    || this.MIPremiumPaymentTimingTypeSpecified
                    || this.PaymentFinancedIndicatorSpecified
                    || this.PaymentIncludedInAPRIndicatorSpecified
                    || this.PaymentIncludedInJurisdictionHighCostIndicatorSpecified
                    || this.RegulationZPointsAndFeesIndicatorSpecified;
            }
        }

        /// <summary>
        /// The dollar amount paid by a specific party at a specified time for a specified MI premium payment.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount MIPremiumActualPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPremiumActualPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPremiumActualPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumActualPaymentAmountSpecified
        {
            get { return MIPremiumActualPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// The estimated dollar amount to be paid by a specific party at a specified time for a specified MI premium payment.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOAmount MIPremiumEstimatedPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPremiumEstimatedPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPremiumEstimatedPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumEstimatedPaymentAmountSpecified
        {
            get { return MIPremiumEstimatedPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// The role of the party making the MI Premium payment.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<MIPremiumPaymentPaidByBase> MIPremiumPaymentPaidByType;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPremiumPaymentPaidByType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPremiumPaymentPaidByType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumPaymentPaidByTypeSpecified
        {
            get { return this.MIPremiumPaymentPaidByType != null && this.MIPremiumPaymentPaidByType.enumValue != MIPremiumPaymentPaidByBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for MI Premium Payment Paid By Type.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString MIPremiumPaymentPaidByTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPremiumPaymentPaidByTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPremiumPaymentPaidByTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumPaymentPaidByTypeOtherDescriptionSpecified
        {
            get { return MIPremiumPaymentPaidByTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Defines the point in time during the origination process when the MI Premium payment was paid.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<MIPremiumPaymentTimingBase> MIPremiumPaymentTimingType;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPremiumPaymentTimingType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPremiumPaymentTimingType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumPaymentTimingTypeSpecified
        {
            get { return this.MIPremiumPaymentTimingType != null && this.MIPremiumPaymentTimingType.enumValue != MIPremiumPaymentTimingBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for MI Premium Payment Timing Type.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString MIPremiumPaymentTimingTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPremiumPaymentTimingTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPremiumPaymentTimingTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumPaymentTimingTypeOtherDescriptionSpecified
        {
            get { return MIPremiumPaymentTimingTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that the payment of any portion of the fee was financed into the original loan amount.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOIndicator PaymentFinancedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PaymentFinancedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PaymentFinancedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PaymentFinancedIndicatorSpecified
        {
            get { return PaymentFinancedIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates fee is to be included in APR calculations.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOIndicator PaymentIncludedInAPRIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PaymentIncludedInAPRIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PaymentIncludedInAPRIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PaymentIncludedInAPRIndicatorSpecified
        {
            get { return PaymentIncludedInAPRIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates the amount of the payment is included in the points and fees calculation appropriated to the jurisdiction of the subject property.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOIndicator PaymentIncludedInJurisdictionHighCostIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PaymentIncludedInJurisdictionHighCostIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PaymentIncludedInJurisdictionHighCostIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PaymentIncludedInJurisdictionHighCostIndicatorSpecified
        {
            get { return PaymentIncludedInJurisdictionHighCostIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that item is included in the Regulation Z Points and Fees calculation.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOIndicator RegulationZPointsAndFeesIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the RegulationZPointsAndFeesIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RegulationZPointsAndFeesIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool RegulationZPointsAndFeesIndicatorSpecified
        {
            get { return RegulationZPointsAndFeesIndicator != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 10)]
        public MI_PREMIUM_PAYMENT_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
