namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class UNPLATTED_LAND
    {
        /// <summary>
        /// Gets a value indicating whether the UNPLATTED_LAND container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AbstractIdentifierSpecified
                    || this.BaseIdentifierSpecified
                    || this.ExtensionSpecified
                    || this.LegalTractIdentifierSpecified
                    || this.MeridianIdentifierSpecified
                    || this.MetesAndBoundsRemainingDescriptionSpecified
                    || this.QuarterSectionIdentifierSpecified
                    || this.RangeIdentifierSpecified
                    || this.SectionIdentifierSpecified
                    || this.TownshipIdentifierSpecified
                    || this.UnplattedLandTypeIdentifierSpecified
                    || this.UnplattedLandTypeOtherDescriptionSpecified
                    || this.UnplattedLandTypeSpecified;
            }
        }

        /// <summary>
        /// A unique number (or alpha-numeric designator) assigned to a land Title Abstract document (i.e.: a sequential collection of documents relating to a specific parcel of land).  The boundaries of a parcel of real property is determined from the information contained in the Title Abstract document.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIdentifier AbstractIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the AbstractIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AbstractIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool AbstractIdentifierSpecified
        {
            get { return AbstractIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The Government Survey Base coordinate of the property that is the east-west line from which to measure Township lines (six [6] miles apart) north or south of the base line. For example: Township 2 North, Range 2 West measured from a Base and Meridian point.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOIdentifier BaseIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the BaseIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BaseIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool BaseIdentifierSpecified
        {
            get { return BaseIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A tract identifier used to legally identify a property. This identifier may be different than the census tract identifier.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOIdentifier LegalTractIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the LegalTractIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LegalTractIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool LegalTractIdentifierSpecified
        {
            get { return LegalTractIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The Government survey Meridian of the property is the north-south line from which to measure Range lines east and west of the Meridian, or starting point.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOIdentifier MeridianIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the MeridianIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MeridianIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool MeridianIdentifierSpecified
        {
            get { return MeridianIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The text of the remaining portion of a Metes and Bounds description after indexing information is populated into the other data fields. 
        /// For example: Commencing 96.8 rods North and 155 rods West from the Southeast corner of Section 11, Township 1 South, Range 1 West, Salt Lake Meridian, West 105.495 feet; thence South 98 feet; thence West 60 feet; thence South 34 feet; thence East 10.03 rods; thence North 8 rods to the point of beginning. 
        /// There are two other terms that often appear in metes and bounds descriptions: a chain, which is approximately 66 feet, and a rod which is approximately one-quarter of a chain or 16.5 feet.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString MetesAndBoundsRemainingDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MetesAndBoundsRemainingDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MetesAndBoundsRemainingDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MetesAndBoundsRemainingDescriptionSpecified
        {
            get { return MetesAndBoundsRemainingDescription != null; }
            set { }
        }

        /// <summary>
        /// Surveying Sections (one mile by one mile) are divided into quarters that are half-mile by half-mile (160 acre) squares.  These are identified as the Northeast, Northwest, Southwest and Southeast quarter sections.  Quarter sections can also be divided into individual quarters resulting in quarter-quarter sections containing 40 acres.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOIdentifier QuarterSectionIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the QuarterSectionIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the QuarterSectionIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool QuarterSectionIdentifierSpecified
        {
            get { return QuarterSectionIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Surveying demarcations used in the Public Land Survey System. Range lines run north and south so as to measure east and west distances in columns.  Range lines are spaced every six (6) miles.  Commonly used in a grid along with Township and Section to describe an area of land at the county level.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOIdentifier RangeIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the RangeIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RangeIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool RangeIdentifierSpecified
        {
            get { return RangeIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The thirty-six (36) square miles outlined by a surveying township.  Sections are numbered right to left then dropping down a row and continuing left to right beginning in the northeast section.  Each Section contains 640 acres.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOIdentifier SectionIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the SectionIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SectionIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool SectionIdentifierSpecified
        {
            get { return SectionIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Surveying demarcations used in the Public Land Survey System.  Town lines run east and west so as to measure north and south distances in rows.  Town lines are spaced every six miles.  Commonly used in a grid along with Range and Section to describe an area of land at the county level.
        /// The resulting 6 mile by six mile square is referred to as a surveying township.  This may or may not align with political townships. 
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOIdentifier TownshipIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the TownshipIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TownshipIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool TownshipIdentifierSpecified
        {
            get { return TownshipIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The type of land description system used for lands, either Government Survey, or metes and bounds, or other (e.g. land grants).
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOEnum<UnplattedLandBase> UnplattedLandType;

        /// <summary>
        /// Gets or sets a value indicating whether the LandType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LandType element has been assigned a value.</value>
        [XmlIgnore]
        public bool UnplattedLandTypeSpecified
        {
            get { return this.UnplattedLandType != null && this.UnplattedLandType.enumValue != UnplattedLandBase.Blank; }
            set { }
        }

        /// <summary>
        /// Optional text field used to capture an alpha/numeric reference related to an enumeration selected from Description Type.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOIdentifier UnplattedLandTypeIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the LandTypeIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LandTypeIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool UnplattedLandTypeIdentifierSpecified
        {
            get { return UnplattedLandTypeIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A text field used to specify the Description Type when "Other" is selected from the enumerated list.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOString UnplattedLandTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the LandTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LandTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool UnplattedLandTypeOtherDescriptionSpecified
        {
            get { return UnplattedLandTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 12)]
        public UNPLATTED_LAND_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
