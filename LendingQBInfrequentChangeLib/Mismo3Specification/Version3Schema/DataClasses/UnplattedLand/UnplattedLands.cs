namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class UNPLATTED_LANDS
    {
        /// <summary>
        /// Gets a value indicating whether the UNPLATTED_LANDS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.UnplattedLandSpecified;
            }
        }

        /// <summary>
        /// A collection of lands.
        /// </summary>
        [XmlElement("UNPLATTED_LAND", Order = 0)]
		public List<UNPLATTED_LAND> UnplattedLand = new List<UNPLATTED_LAND>();

        /// <summary>
        /// Gets or sets a value indicating whether the Land element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Land element has been assigned a value.</value>
        [XmlIgnore]
        public bool UnplattedLandSpecified
        {
            get { return this.UnplattedLand != null && this.UnplattedLand.Count(u => u != null && u.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public UNPLATTED_LANDS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
