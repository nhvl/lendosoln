namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class UNIT_CHARGE_UTILITIES
    {
        /// <summary>
        /// Gets a value indicating whether the UNIT_CHARGE_UTILITIES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.UnitChargeUtilitySpecified;
            }
        }

        /// <summary>
        /// A collection of unit charge utilities.
        /// </summary>
        [XmlElement("UNIT_CHARGE_UTILITY", Order = 0)]
		public List<UNIT_CHARGE_UTILITY> UnitChargeUtility = new List<UNIT_CHARGE_UTILITY>();

        /// <summary>
        /// Gets or sets a value indicating whether the UnitChargeUtility element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the UnitChargeUtility element has been assigned a value.</value>
        [XmlIgnore]
        public bool UnitChargeUtilitySpecified
        {
            get { return this.UnitChargeUtility != null && this.UnitChargeUtility.Count(u => u != null && u.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public UNIT_CHARGE_UTILITIES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
