namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class UNIT_CHARGE_UTILITY
    {
        /// <summary>
        /// Gets a value indicating whether the UNIT_CHARGE_UTILITY container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.UnitChargeUtilityIncludedInAssessmentIndicatorSpecified
                    || this.UnitChargeUtilityTypeOtherDescriptionSpecified
                    || this.UnitChargeUtilityTypeSpecified;
            }
        }

        /// <summary>
        /// Indicates that the utility specified in Unit Charge Utility Type is included in the fee reported in Unit Charge Amount.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIndicator UnitChargeUtilityIncludedInAssessmentIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the UnitChargeUtilityIncludedInAssessmentIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the UnitChargeUtilityIncludedInAssessmentIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool UnitChargeUtilityIncludedInAssessmentIndicatorSpecified
        {
            get { return UnitChargeUtilityIncludedInAssessmentIndicator != null; }
            set { }
        }

        /// <summary>
        /// Specifies a utility included in the unit monthly or annual assessment charges.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<UnitChargeUtilityBase> UnitChargeUtilityType;

        /// <summary>
        /// Gets or sets a value indicating whether the UnitChargeUtilityType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the UnitChargeUtilityType element has been assigned a value.</value>
        [XmlIgnore]
        public bool UnitChargeUtilityTypeSpecified
        {
            get { return this.UnitChargeUtilityType != null && this.UnitChargeUtilityType.enumValue != UnitChargeUtilityBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe a utility if Other is selected as the Unit Charge Utility Type.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString UnitChargeUtilityTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the UnitChargeUtilityTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the UnitChargeUtilityTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool UnitChargeUtilityTypeOtherDescriptionSpecified
        {
            get { return UnitChargeUtilityTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public UNIT_CHARGE_UTILITY_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
