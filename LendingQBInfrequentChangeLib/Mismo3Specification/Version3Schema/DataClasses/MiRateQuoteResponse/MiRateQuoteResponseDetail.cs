namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class MI_RATE_QUOTE_RESPONSE_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the MI_RATE_QUOTE_RESPONSE_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BorrowerRequestedInterestRatePercentSpecified
                    || this.ExtensionSpecified
                    || this.LoanIdentifierSpecified
                    || this.LoanIdentifierTypeOtherDescriptionSpecified
                    || this.LoanIdentifierTypeSpecified
                    || this.MIApplicationTypeOtherDescriptionSpecified
                    || this.MIApplicationTypeSpecified
                    || this.MICompanyNameTypeOtherDescriptionSpecified
                    || this.MICompanyNameTypeSpecified
                    || this.MICoveragePercentSpecified
                    || this.MILenderIdentifierSpecified
                    || this.MIRateQuoteDisclaimerTextSpecified
                    || this.MIRateQuoteExpirationDateSpecified
                    || this.MIServiceTypeOtherDescriptionSpecified
                    || this.MIServiceTypeSpecified
                    || this.MITransactionIdentifierSpecified
                    || this.RateQuoteAllProductIndicatorSpecified;
            }
        }

        /// <summary>
        /// The interest rate that is being requested on the loan application. Collected on the URLA in Section I (Interest Rate).
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOPercent BorrowerRequestedInterestRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the BorrowerRequestedInterestRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BorrowerRequestedInterestRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool BorrowerRequestedInterestRatePercentSpecified
        {
            get { return BorrowerRequestedInterestRatePercent != null; }
            set { }
        }

        /// <summary>
        /// The value of the identifier for the specified type.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOIdentifier LoanIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanIdentifierSpecified
        {
            get { return LoanIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The type of identifier used for a loan.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<LoanIdentifierBase> LoanIdentifierType;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanIdentifierType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanIdentifierType element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanIdentifierTypeSpecified
        {
            get { return this.LoanIdentifierType != null && this.LoanIdentifierType.enumValue != LoanIdentifierBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the Loan Identifier Type when Other is selected as the Loan Identifier Type.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString LoanIdentifierTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanIdentifierTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanIdentifierTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanIdentifierTypeOtherDescriptionSpecified
        {
            get { return LoanIdentifierTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the way the lender directs the mortgage insurer to handle the processing of the loan.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<MIApplicationBase> MIApplicationType;

        /// <summary>
        /// Gets or sets a value indicating whether the MIApplicationType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIApplicationType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIApplicationTypeSpecified
        {
            get { return this.MIApplicationType != null && this.MIApplicationType.enumValue != MIApplicationBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture the description of the type of MI Application if Other is selected as the MIApplicationType.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString MIApplicationTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MIApplicationTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIApplicationTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIApplicationTypeOtherDescriptionSpecified
        {
            get { return MIApplicationTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// To convey the private MI company short/common name from whom the private mortgage insurance coverage was obtained.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOEnum<MICompanyNameBase> MICompanyNameType;

        /// <summary>
        /// Gets or sets a value indicating whether the MICompanyNameType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MICompanyNameType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MICompanyNameTypeSpecified
        {
            get { return this.MICompanyNameType != null && this.MICompanyNameType.enumValue != MICompanyNameBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the MI company name when Other is selected for the MI Company Name Type.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOString MICompanyNameTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MICompanyNameTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MICompanyNameTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MICompanyNameTypeOtherDescriptionSpecified
        {
            get { return MICompanyNameTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The percentage of mortgage insurance coverage obtained.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOPercent MICoveragePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the MICoveragePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MICoveragePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool MICoveragePercentSpecified
        {
            get { return MICoveragePercent != null; }
            set { }
        }

        /// <summary>
        /// The unique number assigned by the private mortgage insurer identifying the lender entity.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOIdentifier MILenderIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the MILenderIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MILenderIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool MILenderIdentifierSpecified
        {
            get { return MILenderIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Free-form text field used to describe the legal terms under which the rate quote is valid.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOString MIRateQuoteDisclaimerText;

        /// <summary>
        /// Gets or sets a value indicating whether the MIRateQuoteDisclaimerText element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIRateQuoteDisclaimerText element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIRateQuoteDisclaimerTextSpecified
        {
            get { return MIRateQuoteDisclaimerText != null; }
            set { }
        }

        /// <summary>
        /// The date after which the rate quoted is no longer valid.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMODate MIRateQuoteExpirationDate;

        /// <summary>
        /// Gets or sets a value indicating whether the MIRateQuoteExpirationDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIRateQuoteExpirationDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIRateQuoteExpirationDateSpecified
        {
            get { return MIRateQuoteExpirationDate != null; }
            set { }
        }

        /// <summary>
        /// Defines the type of service provided by the mortgage insurer.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOEnum<MIServiceBase> MIServiceType;

        /// <summary>
        /// Gets or sets a value indicating whether the MIServiceType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIServiceType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIServiceTypeSpecified
        {
            get { return this.MIServiceType != null && this.MIServiceType.enumValue != MIServiceBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the MI Service Type when Other is selected for the MI Service Type.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOString MIServiceTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MIServiceTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIServiceTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIServiceTypeOtherDescriptionSpecified
        {
            get { return MIServiceTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A unique tracking identifier assigned by the sender which is typically returned to the sender in the resulting response.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOIdentifier MITransactionIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the MITransactionIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MITransactionIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool MITransactionIdentifierSpecified
        {
            get { return MITransactionIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Indicates to the MI provider that requesting organization desires all appropriate MI products be considered and returned in the rate quote response.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOIndicator RateQuoteAllProductIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the RateQuoteAllProductIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RateQuoteAllProductIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool RateQuoteAllProductIndicatorSpecified
        {
            get { return RateQuoteAllProductIndicator != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 16)]
        public MI_RATE_QUOTE_RESPONSE_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
