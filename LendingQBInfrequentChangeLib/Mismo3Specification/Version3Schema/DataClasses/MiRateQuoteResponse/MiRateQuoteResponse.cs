namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class MI_RATE_QUOTE_RESPONSE
    {
        /// <summary>
        /// Gets a value indicating whether the MI_RATE_QUOTE_RESPONSE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.MIRateQuoteProductsSpecified
                    || this.MIRateQuoteResponseDetailSpecified;
            }
        }

        /// <summary>
        /// Details on mortgage insurance products.
        /// </summary>
        [XmlElement("MI_RATE_QUOTE_PRODUCTS", Order = 0)]
        public MI_RATE_QUOTE_PRODUCTS MIRateQuoteProducts;

        /// <summary>
        /// Gets or sets a value indicating whether the Mortgage Insurance Rate Quote Products element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Mortgage Insurance Rate Quote Products element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIRateQuoteProductsSpecified
        {
            get { return this.MIRateQuoteProducts != null && this.MIRateQuoteProducts.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details on a mortgage insurance rate quote.
        /// </summary>
        [XmlElement("MI_RATE_QUOTE_RESPONSE_DETAIL", Order = 1)]
        public MI_RATE_QUOTE_RESPONSE_DETAIL MIRateQuoteResponseDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the Mortgage Insurance Rate Quote Response Detail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Mortgage Insurance Rate Quote Response Detail element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIRateQuoteResponseDetailSpecified
        {
            get { return this.MIRateQuoteResponseDetail != null && this.MIRateQuoteResponseDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public MI_RATE_QUOTE_RESPONSE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
