namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class ALIAS_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the ALIAS_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AliasAccountIdentifierSpecified
                    || this.AliasCreditorNameSpecified
                    || this.AliasTaxReturnIndicatorSpecified
                    || this.AliasTypeOtherDescriptionSpecified
                    || this.AliasTypeSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A unique identifier of the account under which credit has been granted while using the identified alias (alternate name.) For a borrower alias, this may be collected on the URLA in Section VI.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIdentifier AliasAccountIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the AliasAccountIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AliasAccountIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool AliasAccountIdentifierSpecified
        {
            get { return AliasAccountIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The unstructured name of the grantor of credit to the party while using the identified alias (alternate name). For a borrower alias, this may be collected on the URLA in Section VI.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString AliasCreditorName;

        /// <summary>
        /// Gets or sets a value indicating whether the AliasCreditorName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AliasCreditorName element has been assigned a value.</value>
        [XmlIgnore]
        public bool AliasCreditorNameSpecified
        {
            get { return AliasCreditorName != null; }
            set { }
        }

        /// <summary>
        /// When true indicates that this alias name was used on a tax return.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOIndicator AliasTaxReturnIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the AliasTaxReturnIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AliasTaxReturnIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool AliasTaxReturnIndicatorSpecified
        {
            get { return AliasTaxReturnIndicator != null; }
            set { }
        }

        /// <summary>
        /// A description of the type of ALIAS.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOEnum<AliasBase> AliasType;

        /// <summary>
        /// Gets or sets a value indicating whether the AliasType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AliasType element has been assigned a value.</value>
        [XmlIgnore]
        public bool AliasTypeSpecified
        {
            get { return this.AliasType != null && this.AliasType.enumValue != AliasBase.Blank; }
            set { }
        }

        /// <summary>
        /// A description of an ALIAS type not included in the enumerated list.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString AliasTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the AliasTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AliasTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool AliasTypeOtherDescriptionSpecified
        {
            get { return AliasTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 5)]
        public ALIAS_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
