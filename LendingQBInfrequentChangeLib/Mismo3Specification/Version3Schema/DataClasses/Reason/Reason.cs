namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class REASON
    {
        /// <summary>
        /// Gets a value indicating whether the REASON container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ReasonCodeDescriptionSpecified
                    || this.ReasonCodeSpecified
                    || this.ReasonCommentTextSpecified;
            }
        }

        /// <summary>
        /// Code values exchanged between business partners relating to the reason for a service request or response.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOCode ReasonCode;

        /// <summary>
        /// Gets or sets a value indicating whether the ReasonCode element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ReasonCode element has been assigned a value.</value>
        [XmlIgnore]
        public bool ReasonCodeSpecified
        {
            get { return ReasonCode != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field describing the Reason Code.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString ReasonCodeDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ReasonCodeDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ReasonCodeDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ReasonCodeDescriptionSpecified
        {
            get { return ReasonCodeDescription != null; }
            set { }
        }

        /// <summary>
        /// A free-form alphanumeric string which contains additional details about the reason for a service request or response.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString ReasonCommentText;

        /// <summary>
        /// Gets or sets a value indicating whether the ReasonCommentText element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ReasonCommentText element has been assigned a value.</value>
        [XmlIgnore]
        public bool ReasonCommentTextSpecified
        {
            get { return ReasonCommentText != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public REASON_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
