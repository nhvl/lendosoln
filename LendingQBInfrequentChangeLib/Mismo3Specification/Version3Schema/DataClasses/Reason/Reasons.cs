namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class REASONS
    {
        /// <summary>
        /// Gets a value indicating whether the REASONS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ReasonSpecified;
            }
        }

        /// <summary>
        /// A collection of reasons.
        /// </summary>
        [XmlElement("REASON", Order = 0)]
		public List<REASON> Reason = new List<REASON>();

        /// <summary>
        /// Gets or sets a value indicating whether the Reason element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Reason element has been assigned a value.</value>
        [XmlIgnore]
        public bool ReasonSpecified
        {
            get { return this.Reason != null && this.Reason.Count(r => r != null && r.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public REASONS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
