namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class VERIFICATION_DATA_INVESTOR
    {
        /// <summary>
        /// Gets a value indicating whether the VERIFICATION_DATA_INVESTOR container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.InvestorTotalCountSpecified;
            }
        }

        /// <summary>
        /// Provides a count of the Investor containers within this data set.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOCount InvestorTotalCount;

        /// <summary>
        /// Gets or sets a value indicating whether the InvestorTotalCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InvestorTotalCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool InvestorTotalCountSpecified
        {
            get { return InvestorTotalCount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public VERIFICATION_DATA_INVESTOR_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
