namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CREDIT_FILE_OWNING_BUREAU_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the CREDIT_FILE_OWNING_BUREAU_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditFileOwningBureauIdentifierSpecified
                    || this.CreditFileOwningBureauNameSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// The identification number of the credit bureau that owns or manages the credit file being referenced.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIdentifier CreditFileOwningBureauIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditFileOwningBureauIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditFileOwningBureauIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditFileOwningBureauIdentifierSpecified
        {
            get { return CreditFileOwningBureauIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The name of the credit bureau that owns or manages the credit file being referenced.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString CreditFileOwningBureauName;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditFileOwningBureauName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditFileOwningBureauName element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditFileOwningBureauNameSpecified
        {
            get { return CreditFileOwningBureauName != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public CREDIT_FILE_OWNING_BUREAU_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
