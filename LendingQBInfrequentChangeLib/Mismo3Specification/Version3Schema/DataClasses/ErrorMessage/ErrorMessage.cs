namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class ERROR_MESSAGE
    {
        /// <summary>
        /// Gets a value indicating whether the ERROR_MESSAGE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ErrorMessageCategoryCodeSpecified
                    || this.ErrorMessageCodeSpecified
                    || this.ErrorMessageTextSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// Identifies a user-defined category for an error message.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOCode ErrorMessageCategoryCode;

        /// <summary>
        /// Gets or sets a value indicating whether the ErrorMessageCategoryCode element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ErrorMessageCategoryCode element has been assigned a value.</value>
        [XmlIgnore]
        public bool ErrorMessageCategoryCodeSpecified
        {
            get { return ErrorMessageCategoryCode != null; }
            set { }
        }

        /// <summary>
        /// A unique code for a specific error message.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOCode ErrorMessageCode;

        /// <summary>
        /// Gets or sets a value indicating whether the ErrorMessageCode element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ErrorMessageCode element has been assigned a value.</value>
        [XmlIgnore]
        public bool ErrorMessageCodeSpecified
        {
            get { return ErrorMessageCode != null; }
            set { }
        }

        /// <summary>
        /// Text describing specific details of an error message.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString ErrorMessageText;

        /// <summary>
        /// Gets or sets a value indicating whether the ErrorMessageText element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ErrorMessageText element has been assigned a value.</value>
        [XmlIgnore]
        public bool ErrorMessageTextSpecified
        {
            get { return ErrorMessageText != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public ERROR_MESSAGE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
