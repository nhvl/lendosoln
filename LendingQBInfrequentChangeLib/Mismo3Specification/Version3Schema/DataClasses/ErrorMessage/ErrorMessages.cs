namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class ERROR_MESSAGES
    {
        /// <summary>
        /// Gets a value indicating whether the ERROR_MESSAGES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ErrorMessageSpecified
                    || this.ErrorMessageSummarySpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of error messages.
        /// </summary>
        [XmlElement("ERROR_MESSAGE", Order = 0)]
		public List<ERROR_MESSAGE> ErrorMessage = new List<ERROR_MESSAGE>();

        /// <summary>
        /// Gets or sets a value indicating whether the ErrorMessage element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ErrorMessage element has been assigned a value.</value>
        [XmlIgnore]
        public bool ErrorMessageSpecified
        {
            get { return this.ErrorMessage != null && this.ErrorMessage.Count(e => e != null && e.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A summary of an error message.
        /// </summary>
        [XmlElement("ERROR_MESSAGE_SUMMARY", Order = 1)]
        public ERROR_MESSAGE_SUMMARY ErrorMessageSummary;

        /// <summary>
        /// Gets or sets a value indicating whether the ErrorMessageSummary element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ErrorMessageSummary element has been assigned a value.</value>
        [XmlIgnore]
        public bool ErrorMessageSummarySpecified
        {
            get { return this.ErrorMessageSummary != null && this.ErrorMessageSummary.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public ERROR_MESSAGES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
