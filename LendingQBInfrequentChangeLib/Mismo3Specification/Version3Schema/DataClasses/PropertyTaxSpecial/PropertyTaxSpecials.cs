namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PROPERTY_TAX_SPECIALS
    {
        /// <summary>
        /// Gets a value indicating whether the PROPERTY_TAX_SPECIALS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.PropertyTaxSpecialSpecified;
            }
        }

        /// <summary>
        /// A collection of special property taxes.
        /// </summary>
        [XmlElement("PROPERTY_TAX_SPECIAL", Order = 0)]
		public List<PROPERTY_TAX_SPECIAL> PropertyTaxSpecial = new List<PROPERTY_TAX_SPECIAL>();

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyTaxSpecial element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyTaxSpecial element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyTaxSpecialSpecified
        {
            get { return this.PropertyTaxSpecial != null && this.PropertyTaxSpecial.Count(p => p != null && p.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public PROPERTY_TAX_SPECIALS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
