namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class AMORTIZATION
    {
        /// <summary>
        /// Gets a value indicating whether the AMORTIZATION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AmortizationRuleSpecified
                    || this.AmortizationScheduleItemsSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// Rule for how amortization can be handled.
        /// </summary>
        [XmlElement("AMORTIZATION_RULE", Order = 0)]
        public AMORTIZATION_RULE AmortizationRule;

        /// <summary>
        /// Gets or sets a value indicating whether the AmortizationRule element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AmortizationRule element has been assigned a value.</value>
        [XmlIgnore]
        public bool AmortizationRuleSpecified
        {
            get { return this.AmortizationRule != null && this.AmortizationRule.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Schedule items for amortization.
        /// </summary>
        [XmlElement("AMORTIZATION_SCHEDULE_ITEMS", Order = 1)]
        public AMORTIZATION_SCHEDULE_ITEMS AmortizationScheduleItems;

        /// <summary>
        /// Gets or sets a value indicating whether the AmortizationScheduleItems element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AmortizationScheduleItems element has been assigned a value.</value>
        [XmlIgnore]
        public bool AmortizationScheduleItemsSpecified
        {
            get { return this.AmortizationScheduleItems != null && this.AmortizationScheduleItems.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public AMORTIZATION_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
