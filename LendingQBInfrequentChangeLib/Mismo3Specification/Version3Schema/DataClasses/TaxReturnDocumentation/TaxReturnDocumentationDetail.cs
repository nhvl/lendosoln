namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class TAX_RETURN_DOCUMENTATION_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the TAX_RETURN_DOCUMENTATION_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AccountTranscriptIndicatorSpecified
                    || this.ExtensionSpecified
                    || this.InformationalFormSeriesTranscriptIndicatorSpecified
                    || this.IRSDocumentTypeOtherDescriptionSpecified
                    || this.IRSDocumentTypeSpecified
                    || this.OtherTranscriptDescriptionSpecified
                    || this.RecordOfAccountIndicatorSpecified
                    || this.ReturnTranscriptIndicatorSpecified
                    || this.VerificationOfNonfilingIndicatorSpecified;
            }
        }

        /// <summary>
        /// When true, indicates that the user requested an account transcript which contains information about the financial status, penalties, and adjustments. Used on the IRS Form 4506T.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIndicator AccountTranscriptIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the AccountTranscriptIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AccountTranscriptIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool AccountTranscriptIndicatorSpecified
        {
            get { return AccountTranscriptIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that the user requested additional informational form series that are not specifically tax returns such as:  W-2s, 1099s, 1098s, and 5498s. Used on the IRS Form 4506T.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOIndicator InformationalFormSeriesTranscriptIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the InformationalFormSeriesTranscriptIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InformationalFormSeriesTranscriptIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool InformationalFormSeriesTranscriptIndicatorSpecified
        {
            get { return InformationalFormSeriesTranscriptIndicator != null; }
            set { }
        }

        /// <summary>
        /// Specifies the form type that is being requested from the IRS.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<IRSDocumentBase> IRSDocumentType;

        /// <summary>
        /// Gets or sets a value indicating whether the IRSDocumentType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IRSDocumentType element has been assigned a value.</value>
        [XmlIgnore]
        public bool IRSDocumentTypeSpecified
        {
            get { return this.IRSDocumentType != null && this.IRSDocumentType.enumValue != IRSDocumentBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for IRS  Document Type.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString IRSDocumentTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the IRSDocumentTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IRSDocumentTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool IRSDocumentTypeOtherDescriptionSpecified
        {
            get { return IRSDocumentTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Supports ad hoc requested items from the IRS that may not be specifically called out. 
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString OtherTranscriptDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the OtherTranscriptDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OtherTranscriptDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool OtherTranscriptDescriptionSpecified
        {
            get { return OtherTranscriptDescription != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates the user  requested a detailed combination of Return Transcripts and Account Transcripts which contain supplemental financial status. Used on the IRS Form 4506T.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOIndicator RecordOfAccountIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the RecordOfAccountIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RecordOfAccountIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool RecordOfAccountIndicatorSpecified
        {
            get { return RecordOfAccountIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates the user requested a traditional tax return as originally filed with the IRS for 1040s, 1065s, and 1120s. Used on the IRS Form 4506T.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOIndicator ReturnTranscriptIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ReturnTranscriptIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ReturnTranscriptIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ReturnTranscriptIndicatorSpecified
        {
            get { return ReturnTranscriptIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates a request for proof from the IRS that the taxpayer did not file a return for the requested tax year period. Used on the IRS Form 4506T.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOIndicator VerificationOfNonfilingIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the VerificationOfNonFilingIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the VerificationOfNonFilingIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool VerificationOfNonfilingIndicatorSpecified
        {
            get { return VerificationOfNonfilingIndicator != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 8)]
        public TAX_RETURN_DOCUMENTATION_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
