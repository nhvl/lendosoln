namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class TAX_RETURN_DOCUMENTATIONS
    {
        /// <summary>
        /// Gets a value indicating whether the TAX_RETURN_DOCUMENTATIONS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.TaxReturnDocumentationSpecified;
            }
        }

        /// <summary>
        /// A collection of tax return documentation.
        /// </summary>
        [XmlElement("TAX_RETURN_DOCUMENTATION", Order = 0)]
		public List<TAX_RETURN_DOCUMENTATION> TaxReturnDocumentation = new List<TAX_RETURN_DOCUMENTATION>();

        /// <summary>
        /// Gets or sets a value indicating whether the TaxReturnDocumentation element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TaxReturnDocumentation element has been assigned a value.</value>
        [XmlIgnore]
        public bool TaxReturnDocumentationSpecified
        {
            get { return this.TaxReturnDocumentation != null && this.TaxReturnDocumentation.Count(t => t != null && t.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public TAX_RETURN_DOCUMENTATIONS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
