namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class ERROR_MESSAGE_SUMMARY
    {
        /// <summary>
        /// Gets a value indicating whether the ERROR_MESSAGE_SUMMARY container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ErrorCodeSpecified
                    || this.ErrorSummaryTextSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A unique code associated with an error.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOCode ErrorCode;

        /// <summary>
        /// Gets or sets a value indicating whether the ErrorCode element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ErrorCode element has been assigned a value.</value>
        [XmlIgnore]
        public bool ErrorCodeSpecified
        {
            get { return ErrorCode != null; }
            set { }
        }

        /// <summary>
        /// Textual description of an error.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString ErrorSummaryText;

        /// <summary>
        /// Gets or sets a value indicating whether the ErrorSummaryText element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ErrorSummaryText element has been assigned a value.</value>
        [XmlIgnore]
        public bool ErrorSummaryTextSpecified
        {
            get { return ErrorSummaryText != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public ERROR_MESSAGE_SUMMARY_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
