namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class LOSS_PAYEE
    {
        /// <summary>
        /// Gets a value indicating whether the LOSS_PAYEE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.LossPayeeTypeOtherDescriptionSpecified
                    || this.LossPayeeTypeSpecified;
            }
        }

        /// <summary>
        /// Specifies the general types of loss payees associated with the mortgage loan.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<LossPayeeBase> LossPayeeType;

        /// <summary>
        /// Gets or sets a value indicating whether the LossPayeeType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LossPayeeType element has been assigned a value.</value>
        [XmlIgnore]
        public bool LossPayeeTypeSpecified
        {
            get { return this.LossPayeeType != null && this.LossPayeeType.enumValue != LossPayeeBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture the loss payee type if Other is selected as the loss payee type.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString LossPayeeTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the LossPayeeTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LossPayeeTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool LossPayeeTypeOtherDescriptionSpecified
        {
            get { return LossPayeeTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public LOSS_PAYEE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
