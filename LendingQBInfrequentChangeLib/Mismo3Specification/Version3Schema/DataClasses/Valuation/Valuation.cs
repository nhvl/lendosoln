namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class VALUATION
    {
        /// <summary>
        /// Gets a value indicating whether the VALUATION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ValuationRequestSpecified
                    || this.ValuationResponseSpecified;
            }
        }

        /// <summary>
        /// A request for valuation service.
        /// </summary>
        [XmlElement("VALUATION_REQUEST", Order = 0)]
        public VALUATION_REQUEST ValuationRequest;

        /// <summary>
        /// Gets or sets a value indicating whether the ValuationRequest element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ValuationRequest element has been assigned a value.</value>
        [XmlIgnore]
        public bool ValuationRequestSpecified
        {
            get { return this.ValuationRequest != null && this.ValuationRequest.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A response containing valuation information.
        /// </summary>
        [XmlElement("VALUATION_RESPONSE", Order = 1)]
        public VALUATION_RESPONSE ValuationResponse;

        /// <summary>
        /// Gets or sets a value indicating whether the ValuationResponse element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ValuationResponse element has been assigned a value.</value>
        [XmlIgnore]
        public bool ValuationResponseSpecified
        {
            get { return this.ValuationResponse != null && this.ValuationResponse.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public VALUATION_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
