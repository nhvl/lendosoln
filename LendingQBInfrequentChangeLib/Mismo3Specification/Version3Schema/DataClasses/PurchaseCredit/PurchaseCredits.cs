namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PURCHASE_CREDITS
    {
        /// <summary>
        /// Gets a value indicating whether the PURCHASE_CREDITS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.PurchaseCreditSpecified;
            }
        }

        /// <summary>
        /// A collection of purchase credits.
        /// </summary>
        [XmlElement("PURCHASE_CREDIT", Order = 0)]
		public List<PURCHASE_CREDIT> PurchaseCredit = new List<PURCHASE_CREDIT>();

        /// <summary>
        /// Gets or sets a value indicating whether the PurchaseCredit element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PurchaseCredit element has been assigned a value.</value>
        [XmlIgnore]
        public bool PurchaseCreditSpecified
        {
            get { return this.PurchaseCredit != null && this.PurchaseCredit.Count(p => p != null && p.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public PURCHASE_CREDITS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
