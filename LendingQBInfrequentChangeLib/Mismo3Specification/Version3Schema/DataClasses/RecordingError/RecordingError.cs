namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class RECORDING_ERROR
    {
        /// <summary>
        /// Gets a value indicating whether the RECORDING_ERROR container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ErrorXPathSpecified
                    || this.ExtensionSpecified
                    || this.RecordingErrorDescriptionSpecified
                    || this.RecordingErrorSeverityTypeOtherDescriptionSpecified
                    || this.RecordingErrorSeverityTypeSpecified
                    || this.RecordingErrorTypeOtherDescriptionSpecified
                    || this.RecordingErrorTypeSpecified;
            }
        }

        /// <summary>
        /// The location in the payload where the error occurred, referenced in a XPath expression.  (example "/Property@Address" points to the location where the address is located.).
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOXPath ErrorXPath;

        /// <summary>
        /// Gets or sets a value indicating whether the ErrorXPath element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ErrorXPath element has been assigned a value.</value>
        [XmlIgnore]
        public bool ErrorXPathSpecified
        {
            get { return ErrorXPath != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field for a description of the exact error.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString RecordingErrorDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the RecordingErrorDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RecordingErrorDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool RecordingErrorDescriptionSpecified
        {
            get { return RecordingErrorDescription != null; }
            set { }
        }

        /// <summary>
        /// A description of the severity of the error.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<RecordingErrorSeverityBase> RecordingErrorSeverityType;

        /// <summary>
        /// Gets or sets a value indicating whether the RecordingErrorSeverityType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RecordingErrorSeverityType element has been assigned a value.</value>
        [XmlIgnore]
        public bool RecordingErrorSeverityTypeSpecified
        {
            get { return this.RecordingErrorSeverityType != null && this.RecordingErrorSeverityType.enumValue != RecordingErrorSeverityBase.Blank; }
            set { }
        }

        /// <summary>
        /// The description of the error severity when Other is chosen from the enumerated list.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString RecordingErrorSeverityTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the RecordingErrorSeverityTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RecordingErrorSeverityTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool RecordingErrorSeverityTypeOtherDescriptionSpecified
        {
            get { return RecordingErrorSeverityTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A categorical description of the error that occurred.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<RecordingErrorBase> RecordingErrorType;

        /// <summary>
        /// Gets or sets a value indicating whether the RecordingErrorType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RecordingErrorType element has been assigned a value.</value>
        [XmlIgnore]
        public bool RecordingErrorTypeSpecified
        {
            get { return this.RecordingErrorType != null && this.RecordingErrorType.enumValue != RecordingErrorBase.Blank; }
            set { }
        }

        /// <summary>
        /// A description of the error that is not included in the enumerated list.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString RecordingErrorTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the RecordingErrorTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RecordingErrorTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool RecordingErrorTypeOtherDescriptionSpecified
        {
            get { return RecordingErrorTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 6)]
        public RECORDING_ERROR_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
