namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class RECORDING_ERRORS
    {
        /// <summary>
        /// Gets a value indicating whether the RECORDING_ERRORS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.RecordingErrorSpecified;
            }
        }

        /// <summary>
        /// A collection of recording errors.
        /// </summary>
        [XmlElement("RECORDING_ERROR", Order = 0)]
		public List<RECORDING_ERROR> RecordingError = new List<RECORDING_ERROR>();

        /// <summary>
        /// Gets or sets a value indicating whether the RecordingError element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RecordingError element has been assigned a value.</value>
        [XmlIgnore]
        public bool RecordingErrorSpecified
        {
            get { return this.RecordingError != null && this.RecordingError.Count(r => r != null && r.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public RECORDING_ERRORS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
