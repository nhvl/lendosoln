namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class SERVITUDE_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the SERVITUDE_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.IndividualPropertyServitudeDescriptionSpecified
                    || this.PrivatelyCreatedServitudeTypeOtherDescriptionSpecified
                    || this.PrivatelyCreatedServitudeTypeSpecified
                    || this.ServitudeDescriptionSpecified
                    || this.ServitudeTypeOtherDescriptionSpecified
                    || this.ServitudeTypeSpecified;
            }
        }

        /// <summary>
        /// Free-form description of the details regarding the Individual Property Servitude indicated.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString IndividualPropertyServitudeDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the IndividualPropertyServitudeDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IndividualPropertyServitudeDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool IndividualPropertyServitudeDescriptionSpecified
        {
            get { return IndividualPropertyServitudeDescription != null; }
            set { }
        }

        /// <summary>
        /// The creation of a right or obligation that may be imposed or allowed on the property, is privately created and applied to an individual property. See ENCUMBERANCE for rights or obligations that affect property ownership and recorded in the local jurisdiction. 
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<PrivatelyCreatedServitudeBase> PrivatelyCreatedServitudeType;

        /// <summary>
        /// Gets or sets a value indicating whether the PrivatelyCreatedServitudeType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrivatelyCreatedServitudeType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrivatelyCreatedServitudeTypeSpecified
        {
            get { return this.PrivatelyCreatedServitudeType != null && this.PrivatelyCreatedServitudeType.enumValue != PrivatelyCreatedServitudeBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Privately Created Servitude Type.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString PrivatelyCreatedServitudeTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PrivatelyCreatedServitudeTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrivatelyCreatedServitudeTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrivatelyCreatedServitudeTypeOtherDescriptionSpecified
        {
            get { return PrivatelyCreatedServitudeTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Details regarding the servitude and includes details such as enforcement, background, authorities.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString ServitudeDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ServitudeDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServitudeDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServitudeDescriptionSpecified
        {
            get { return ServitudeDescription != null; }
            set { }
        }

        /// <summary>
        /// The specific condition, covenant and or restriction that may be imposed or allowed on the property, is privately created and is uniformly applied to many properties.  The perspective of burden or benefit is based on the context of use or reporting viewpoint.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<ServitudeBase> ServitudeType;

        /// <summary>
        /// Gets or sets a value indicating whether the ServitudeType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServitudeType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServitudeTypeSpecified
        {
            get { return this.ServitudeType != null && this.ServitudeType.enumValue != ServitudeBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Servitude Type.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString ServitudeTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ServitudeTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServitudeTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServitudeTypeOtherDescriptionSpecified
        {
            get { return ServitudeTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 6)]
        public SERVITUDE_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
