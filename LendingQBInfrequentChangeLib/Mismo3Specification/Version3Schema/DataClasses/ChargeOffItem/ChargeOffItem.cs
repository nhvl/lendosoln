namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class CHARGE_OFF_ITEM
    {
        /// <summary>
        /// Gets a value indicating whether the CHARGE_OFF_ITEM container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ChargeOffItemAmountSpecified
                    || this.ChargeOffItemTypeOtherDescriptionSpecified
                    || this.ChargeOffItemTypeSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// The dollar amount of the Charge Off Item Type.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount ChargeOffItemAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ChargeOffItemAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ChargeOffItemAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ChargeOffItemAmountSpecified
        {
            get { return ChargeOffItemAmount != null; }
            set { }
        }

        /// <summary>
        /// Specifies the type of loss that is to be charged off. 
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<ChargeOffItemBase> ChargeOffItemType;

        /// <summary>
        /// Gets or sets a value indicating whether the ChargeOffItemType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ChargeOffItemType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ChargeOffItemTypeSpecified
        {
            get { return this.ChargeOffItemType != null && this.ChargeOffItemType.enumValue != ChargeOffItemBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Charge Off Item Type.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString ChargeOffItemTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ChargeOffItemTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ChargeOffItemTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ChargeOffItemTypeOtherDescriptionSpecified
        {
            get { return ChargeOffItemTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public CHARGE_OFF_ITEM_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
