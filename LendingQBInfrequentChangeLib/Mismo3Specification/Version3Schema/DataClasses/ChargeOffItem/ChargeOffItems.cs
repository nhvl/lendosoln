namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CHARGE_OFF_ITEMS
    {
        /// <summary>
        /// Gets a value indicating whether the CHARGE_OFF_ITEMS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ChargeOffItemSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of charge off items.
        /// </summary>
        [XmlElement("CHARGE_OFF_ITEM", Order = 0)]
		public List<CHARGE_OFF_ITEM> ChargeOffItem = new List<CHARGE_OFF_ITEM>();

        /// <summary>
        /// Gets or sets a value indicating whether the ChargeOffItem element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ChargeOffItem element has been assigned a value.</value>
        [XmlIgnore]
        public bool ChargeOffItemSpecified
        {
            get { return this.ChargeOffItem != null && this.ChargeOffItem.Count(c => c != null && c.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public CHARGE_OFF_ITEMS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
