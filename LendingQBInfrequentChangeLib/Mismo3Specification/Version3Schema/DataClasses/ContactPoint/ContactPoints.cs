namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CONTACT_POINTS
    {
        /// <summary>
        /// Gets a value indicating whether the CONTACT_POINTS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ContactPointSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A list of contact points.
        /// </summary>
        [XmlElement("CONTACT_POINT", Order = 0)]
		public List<CONTACT_POINT> ContactPoint = new List<CONTACT_POINT>();

        /// <summary>
        /// Gets or sets a value indicating whether the ContactPoint element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ContactPoint element has been assigned a value.</value>
        [XmlIgnore]
        public bool ContactPointSpecified
        {
            get { return this.ContactPoint != null && this.ContactPoint.Count(c => c != null && c.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public CONTACT_POINTS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
