namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class CONTACT_POINT
    {
        /// <summary>
        /// Gets a value indicating whether the CONTACT_POINT container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                if (Mismo3Utilities.ExceedsThreshold(1, 
                    new List<bool> { this.ContactPointEmailSpecified, this.ContactPointSocialMediaSpecified, this.ContactPointTelephoneSpecified, this.OtherContactPointSpecified }))
                {
                    throw new System.Xml.Schema.XmlSchemaValidationException(
                        Mismo3Utilities.GetXSDChoiceExceptionMessage(
                        "CONTACT_POINT",
                        new List<string> { "CONTACT_POINT_EMAIL", "CONTACT_POINT_SOCIAL_MEDIA", "CONTACT_POINT_TELEPHONE", "OTHER_CONTACT_POINT" }));
                }

                return this.ContactPointDetailSpecified
                    || this.ContactPointEmailSpecified
                    || this.ContactPointSocialMediaSpecified
                    || this.ContactPointTelephoneSpecified
                    || this.ExtensionSpecified
                    || this.OtherContactPointSpecified;
            }
        }

        /// <summary>
        /// An email contact point.
        /// </summary>
        [XmlElement("CONTACT_POINT_EMAIL", Order = 0)]
        public CONTACT_POINT_EMAIL ContactPointEmail;

        /// <summary>
        /// Gets or sets a value indicating whether the ContactPointEmail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ContactPointEmail element has been assigned a value.</value>
        [XmlIgnore]
        public bool ContactPointEmailSpecified
        {
            get { return this.ContactPointEmail != null && this.ContactPointEmail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A social media contact point.
        /// </summary>
        [XmlElement("CONTACT_POINT_SOCIAL_MEDIA", Order = 1)]
        public CONTACT_POINT_SOCIAL_MEDIA ContactPointSocialMedia;

        /// <summary>
        /// Gets or sets a value indicating whether the ContactPointSocialMedia element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ContactPointSocialMedia element has been assigned a value.</value>
        [XmlIgnore]
        public bool ContactPointSocialMediaSpecified
        {
            get { return this.ContactPointSocialMedia != null && this.ContactPointSocialMedia.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A telephone contact point.
        /// </summary>
        [XmlElement("CONTACT_POINT_TELEPHONE", Order = 2)]
        public CONTACT_POINT_TELEPHONE ContactPointTelephone;

        /// <summary>
        /// Gets or sets a value indicating whether the ContactPointTelephone element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ContactPointTelephone element has been assigned a value.</value>
        [XmlIgnore]
        public bool ContactPointTelephoneSpecified
        {
            get { return this.ContactPointTelephone != null && this.ContactPointTelephone.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A contact point that is not email, fax, or phone.
        /// </summary>
        [XmlElement("OTHER_CONTACT_POINT", Order = 3)]
        public OTHER_CONTACT_POINT OtherContactPoint;

        /// <summary>
        /// Gets or sets a value indicating whether the OtherContactPoint element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OtherContactPoint element has been assigned a value.</value>
        [XmlIgnore]
        public bool OtherContactPointSpecified
        {
            get { return this.OtherContactPoint != null && this.OtherContactPoint.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Information about a contact point.
        /// </summary>
        [XmlElement("CONTACT_POINT_DETAIL", Order = 4)]
        public CONTACT_POINT_DETAIL ContactPointDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the ContactPointDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ContactPointDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool ContactPointDetailSpecified
        {
            get { return this.ContactPointDetail != null && this.ContactPointDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 5)]
        public CONTACT_POINT_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
