namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CONTACT_POINT_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the CONTACT_POINT_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ContactPointPreferenceIndicatorSpecified
                    || this.ContactPointRoleTypeOtherDescriptionSpecified
                    || this.ContactPointRoleTypeSpecified
                    || this.ExtensionSpecified
                    || this.RequestNoContactIndicatorSpecified;
            }
        }

        /// <summary>
        /// This indicator is set to be true when the particular contacts Contact Point (i.e. phone number) is preferred over any others.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIndicator ContactPointPreferenceIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ContactPointPreferenceIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ContactPointPreferenceIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ContactPointPreferenceIndicatorSpecified
        {
            get { return ContactPointPreferenceIndicator != null; }
            set { }
        }

        /// <summary>
        /// This element sets the type of role (i.e. Home, Work or Mobile) used for the Contact Point Type (Phone, Fax, Email).
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<ContactPointRoleBase> ContactPointRoleType;

        /// <summary>
        /// Gets or sets a value indicating whether the ContactPointRoleType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ContactPointRoleType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ContactPointRoleTypeSpecified
        {
            get { return this.ContactPointRoleType != null && this.ContactPointRoleType.enumValue != ContactPointRoleBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the Contact Point Role Type if Other is selected as the Contact Point Role Type.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString ContactPointRoleTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ContactPointRoleTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ContactPointRoleTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ContactPointRoleTypeOtherDescriptionSpecified
        {
            get { return ContactPointRoleTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that the party has requested not to be contacted by this electronic method.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOIndicator RequestNoContactIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the RequestNoContactIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RequestNoContactIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool RequestNoContactIndicatorSpecified
        {
            get { return RequestNoContactIndicator != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public CONTACT_POINT_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
