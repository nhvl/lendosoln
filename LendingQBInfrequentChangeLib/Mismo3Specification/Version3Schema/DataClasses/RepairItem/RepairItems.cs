namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class REPAIR_ITEMS
    {
        /// <summary>
        /// Gets a value indicating whether the REPAIR_ITEMS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.RepairItemSpecified;
            }
        }

        /// <summary>
        /// Container for items to be repaired.
        /// </summary>
        [XmlElement("REPAIR_ITEM", Order = 0)]
		public List<REPAIR_ITEM> RepairItem = new List<REPAIR_ITEM>();

        /// <summary>
        /// Gets or sets a value indicating whether the RepairItem element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RepairItem element has been assigned a value.</value>
        [XmlIgnore]
        public bool RepairItemSpecified
        {
            get { return this.RepairItem != null && this.RepairItem.Count(r => r != null && r.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public REPAIR_ITEMS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
