namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class REPAIR_ITEM
    {
        /// <summary>
        /// Gets a value indicating whether the REPAIR_ITEM container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.RepairItemCostAmountSpecified
                    || this.RepairItemDescriptionSpecified;
            }
        }

        /// <summary>
        /// The dollar amount of the cost to perform a particular repair on the subject property.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount RepairItemCostAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the RepairItemCostAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RepairItemCostAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool RepairItemCostAmountSpecified
        {
            get { return RepairItemCostAmount != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field describing a particular repair to be performed on the subject property.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString RepairItemDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the RepairItemDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RepairItemDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool RepairItemDescriptionSpecified
        {
            get { return RepairItemDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public REPAIR_ITEM_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
