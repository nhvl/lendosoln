namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class DATA_MODIFICATION_REQUEST
    {
        /// <summary>
        /// Gets a value indicating whether the DATA_MODIFICATION_REQUEST container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DataItemChangeRequestSpecified
                    || this.DataModificationRequestDetailSpecified
                    || this.DealSetSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A request for a change to a data item.
        /// </summary>
        [XmlElement("DATA_ITEM_CHANGE_REQUEST", Order = 0)]
        public DATA_ITEM_CHANGE_REQUEST DataItemChangeRequest;

        /// <summary>
        /// Gets or sets a value indicating whether the DataItemChangeRequest element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DataItemChangeRequest element has been assigned a value.</value>
        [XmlIgnore]
        public bool DataItemChangeRequestSpecified
        {
            get { return this.DataItemChangeRequest != null && this.DataItemChangeRequest.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details on a data modification request.
        /// </summary>
        [XmlElement("DATA_MODIFICATION_REQUEST_DETAIL", Order = 1)]
        public DATA_MODIFICATION_REQUEST_DETAIL DataModificationRequestDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the DataModificationRequestDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DataModificationRequestDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool DataModificationRequestDetailSpecified
        {
            get { return this.DataModificationRequestDetail != null && this.DataModificationRequestDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A set of loans.
        /// </summary>
        [XmlElement("DEAL_SET", Order = 2)]
        public DEAL_SET DealSet;

        /// <summary>
        /// Gets or sets a value indicating whether the DealSet element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DealSet element has been assigned a value.</value>
        [XmlIgnore]
        public bool DealSetSpecified
        {
            get { return this.DealSet != null && this.DealSet.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public DATA_MODIFICATION_REQUEST_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
