namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class DATA_MODIFICATION_REQUEST_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the DATA_MODIFICATION_REQUEST_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.RequestDatetimeSpecified;
            }
        }

        /// <summary>
        /// A system generated date and time stamp enclosed within each request to identify the initiation time of the request.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMODatetime RequestDatetime;

        /// <summary>
        /// Gets or sets a value indicating whether the RequestDateTime element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RequestDateTime element has been assigned a value.</value>
        [XmlIgnore]
        public bool RequestDatetimeSpecified
        {
            get { return RequestDatetime != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public DATA_MODIFICATION_REQUEST_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
