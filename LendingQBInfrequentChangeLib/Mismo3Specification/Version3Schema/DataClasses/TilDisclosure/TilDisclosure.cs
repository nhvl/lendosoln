namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class TIL_DISCLOSURE
    {
        /// <summary>
        /// Gets a value indicating whether the TIL_DISCLOSURE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.TilDisclosureDetailSpecified
                    || this.TilPaymentSummarySpecified;
            }
        }

        /// <summary>
        /// Details on the TIL disclosure.
        /// </summary>
        [XmlElement("TIL_DISCLOSURE_DETAIL", Order = 0)]
        public TIL_DISCLOSURE_DETAIL TilDisclosureDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the TILDisclosureDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TILDisclosureDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool TilDisclosureDetailSpecified
        {
            get { return this.TilDisclosureDetail != null && this.TilDisclosureDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Summary on the TIL payment.
        /// </summary>
        [XmlElement("TIL_PAYMENT_SUMMARY", Order = 1)]
        public TIL_PAYMENT_SUMMARY TilPaymentSummary;

        /// <summary>
        /// Gets or sets a value indicating whether the TILPaymentSummary element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TILPaymentSummary element has been assigned a value.</value>
        [XmlIgnore]
        public bool TilPaymentSummarySpecified
        {
            get { return this.TilPaymentSummary != null && this.TilPaymentSummary.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public TIL_DISCLOSURE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
