namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class TIL_DISCLOSURE_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the TIL_DISCLOSURE_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DatesAndNumericalDisclosuresAreEstimatesIndicatorSpecified
                    || this.ExtensionSpecified
                    || this.TILDisclosureDateSpecified
                    || this.TILPrimaryBoxesLoanCalculationsAreEstimatesIndicatorSpecified;
            }
        }

        /// <summary>
        /// When true, indicates that all dates and numerical values disclosed on the TIL (except the late payment disclosures) are estimates.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIndicator DatesAndNumericalDisclosuresAreEstimatesIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the DatesAndNumericalDisclosuresAreEstimatesIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DatesAndNumericalDisclosuresAreEstimatesIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool DatesAndNumericalDisclosuresAreEstimatesIndicatorSpecified
        {
            get { return DatesAndNumericalDisclosuresAreEstimatesIndicator != null; }
            set { }
        }

        /// <summary>
        /// The date as reflected on a single instance of the TIL Disclosure document.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMODate TILDisclosureDate;

        /// <summary>
        /// Gets or sets a value indicating whether the TILDisclosureDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TILDisclosureDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool TILDisclosureDateSpecified
        {
            get { return TILDisclosureDate != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that the calculated values for the APR, Finance Charge, Amount Financed, Total of Payments, and Total Sales Price (disclosed in the five primary boxes of the TIL) are estimated values.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOIndicator TILPrimaryBoxesLoanCalculationsAreEstimatesIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the TILPrimaryBoxesLoanCalculationsAreEstimatesIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TILPrimaryBoxesLoanCalculationsAreEstimatesIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool TILPrimaryBoxesLoanCalculationsAreEstimatesIndicatorSpecified
        {
            get { return TILPrimaryBoxesLoanCalculationsAreEstimatesIndicator != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public TIL_DISCLOSURE_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
