namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class NOTARY_CERTIFICATE_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the NOTARY_CERTIFICATE_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.NotaryAppearanceDateSpecified
                    || this.NotaryAppearedBeforeNamesDescriptionSpecified
                    || this.NotaryAppearedBeforeTitlesDescriptionSpecified
                    || this.NotaryCertificateSignerCompanyNameSpecified
                    || this.NotaryCertificateSignerFullNameSpecified
                    || this.NotaryCertificateSignerTitleDescriptionSpecified
                    || this.NotaryCertificateSigningCountyNameSpecified
                    || this.NotaryCertificateSigningStateNameSpecified;
            }
        }

        /// <summary>
        /// The date the party or parties signing the document appeared before the notary.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMODate NotaryAppearanceDate;

        /// <summary>
        /// Gets or sets a value indicating whether the NotaryAppearanceDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NotaryAppearanceDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool NotaryAppearanceDateSpecified
        {
            get { return NotaryAppearanceDate != null; }
            set { }
        }

        /// <summary>
        /// The name(s) of all parties that appeared before the notary represented in a single consolidated block of text.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString NotaryAppearedBeforeNamesDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the NotaryAppearedBeforeNamesDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NotaryAppearedBeforeNamesDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool NotaryAppearedBeforeNamesDescriptionSpecified
        {
            get { return NotaryAppearedBeforeNamesDescription != null; }
            set { }
        }

        /// <summary>
        /// The title(s) of all parties that appeared before the notary represented in a single consolidated block of text.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString NotaryAppearedBeforeTitlesDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the NotaryAppearedBeforeTitlesDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NotaryAppearedBeforeTitlesDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool NotaryAppearedBeforeTitlesDescriptionSpecified
        {
            get { return NotaryAppearedBeforeTitlesDescription != null; }
            set { }
        }

        /// <summary>
        /// The company name of the party signing before the notary.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString NotaryCertificateSignerCompanyName;

        /// <summary>
        /// Gets or sets a value indicating whether the NotaryCertificateSignerCompanyName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NotaryCertificateSignerCompanyName element has been assigned a value.</value>
        [XmlIgnore]
        public bool NotaryCertificateSignerCompanyNameSpecified
        {
            get { return NotaryCertificateSignerCompanyName != null; }
            set { }
        }

        /// <summary>
        /// The unparsed name of the party signing before the notary.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString NotaryCertificateSignerFullName;

        /// <summary>
        /// Gets or sets a value indicating whether the NotaryCertificateSignerFullName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NotaryCertificateSignerFullName element has been assigned a value.</value>
        [XmlIgnore]
        public bool NotaryCertificateSignerFullNameSpecified
        {
            get { return NotaryCertificateSignerFullName != null; }
            set { }
        }

        /// <summary>
        /// The title of the party signing before the notary.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString NotaryCertificateSignerTitleDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the NotaryCertificateSignerTitleDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NotaryCertificateSignerTitleDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool NotaryCertificateSignerTitleDescriptionSpecified
        {
            get { return NotaryCertificateSignerTitleDescription != null; }
            set { }
        }

        /// <summary>
        /// The county in which the notary performs the notarial act.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOString NotaryCertificateSigningCountyName;

        /// <summary>
        /// Gets or sets a value indicating whether the NotaryCertificateSigningCountyName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NotaryCertificateSigningCountyName element has been assigned a value.</value>
        [XmlIgnore]
        public bool NotaryCertificateSigningCountyNameSpecified
        {
            get { return NotaryCertificateSigningCountyName != null; }
            set { }
        }

        /// <summary>
        /// The state in which the notary performs the notarial act.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOString NotaryCertificateSigningStateName;

        /// <summary>
        /// Gets or sets a value indicating whether the NotaryCertificateSigningStateName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NotaryCertificateSigningStateName element has been assigned a value.</value>
        [XmlIgnore]
        public bool NotaryCertificateSigningStateNameSpecified
        {
            get { return NotaryCertificateSigningStateName != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 8)]
        public NOTARY_CERTIFICATE_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
