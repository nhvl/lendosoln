namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class NOTARY_CERTIFICATE
    {
        /// <summary>
        /// Gets a value indicating whether the NOTARY_CERTIFICATE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.NotaryCertificateDetailSpecified
                    || this.NotaryCertificateSignerIdentificationSpecified;
            }
        }

        /// <summary>
        /// Details on a notary certificate.
        /// </summary>
        [XmlElement("NOTARY_CERTIFICATE_DETAIL", Order = 0)]
        public NOTARY_CERTIFICATE_DETAIL NotaryCertificateDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the NotaryCertificateDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NotaryCertificateDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool NotaryCertificateDetailSpecified
        {
            get { return this.NotaryCertificateDetail != null && this.NotaryCertificateDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Identification of a notary certificate signer.
        /// </summary>
        [XmlElement("NOTARY_CERTIFICATE_SIGNER_IDENTIFICATION", Order = 1)]
        public NOTARY_CERTIFICATE_SIGNER_IDENTIFICATION NotaryCertificateSignerIdentification;

        /// <summary>
        /// Gets or sets a value indicating whether the NotaryCertificateSignerIdentification element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NotaryCertificateSignerIdentification element has been assigned a value.</value>
        [XmlIgnore]
        public bool NotaryCertificateSignerIdentificationSpecified
        {
            get { return this.NotaryCertificateSignerIdentification != null && this.NotaryCertificateSignerIdentification.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public NOTARY_CERTIFICATE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
