namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class VIEW_FIELD
    {
        /// <summary>
        /// Gets a value indicating whether the VIEW_FIELD container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                if (Mismo3Utilities.ExceedsThreshold(1,
                    new List<bool> { 
                        this.ForeignSystemSignatureFieldSpecified,
                        this.InteractiveFieldSpecified,
                        this.NotarySignatureFieldSpecified,
                        this.RecordingEndorsementFieldSpecified,
                        this.StakeholderSignatureFieldSpecified,
                        this.WitnessSignatureFieldSpecified }))
                {
                    throw new System.Xml.Schema.XmlSchemaValidationException(
                        Mismo3Utilities.GetXSDChoiceExceptionMessage(
                        "VIEW_FIELD",
                        new List<string> { 
                            "FOREIGN_SYSTEM_SIGNATURE_FIELD", 
                            "INTERACTIVE_FIELD",
                            "NOTARY_SIGNATURE_FIELD",
                            "RECORDING_ENDORSEMENT_FIELD",
                            "STAKEHOLDER_SIGNATURE_FIELD",
                            "WITNESS_SIGNATURE_FIELD"}));
                }

                return this.ExtensionSpecified
                    || this.ForeignSystemSignatureFieldSpecified
                    || this.InteractiveFieldSpecified
                    || this.NotarySignatureFieldSpecified
                    || this.RecordingEndorsementFieldSpecified
                    || this.StakeholderSignatureFieldSpecified
                    || this.WitnessSignatureFieldSpecified;
            }
        }

        /// <summary>
        /// A field for a foreign system signature.
        /// </summary>
        [XmlElement("FOREIGN_SYSTEM_SIGNATURE_FIELD", Order = 0)]
        public FOREIGN_SYSTEM_SIGNATURE_FIELD ForeignSystemSignatureField;

        /// <summary>
        /// Gets or sets a value indicating whether the ForeignSystemSignatureField element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ForeignSystemSignatureField element has been assigned a value.</value>
        [XmlIgnore]
        public bool ForeignSystemSignatureFieldSpecified
        {
            get { return this.ForeignSystemSignatureField != null && this.ForeignSystemSignatureField.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// An interactive field.
        /// </summary>
        [XmlElement("INTERACTIVE_FIELD", Order = 1)]
        public INTERACTIVE_FIELD InteractiveField;

        /// <summary>
        /// Gets or sets a value indicating whether the InteractiveField element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InteractiveField element has been assigned a value.</value>
        [XmlIgnore]
        public bool InteractiveFieldSpecified
        {
            get { return this.InteractiveField != null && this.InteractiveField.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A field for a notary signature.
        /// </summary>
        [XmlElement("NOTARY_SIGNATURE_FIELD", Order = 2)]
        public NOTARY_SIGNATURE_FIELD NotarySignatureField;

        /// <summary>
        /// Gets or sets a value indicating whether the NotarySignatureField element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NotarySignatureField element has been assigned a value.</value>
        [XmlIgnore]
        public bool NotarySignatureFieldSpecified
        {
            get { return this.NotarySignatureField != null && this.NotarySignatureField.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A field for a recording endorsement.
        /// </summary>
        [XmlElement("RECORDING_ENDORSEMENT_FIELD", Order = 3)]
        public RECORDING_ENDORSEMENT_FIELD RecordingEndorsementField;

        /// <summary>
        /// Gets or sets a value indicating whether the RecordingEndorsementField element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RecordingEndorsementField element has been assigned a value.</value>
        [XmlIgnore]
        public bool RecordingEndorsementFieldSpecified
        {
            get { return this.RecordingEndorsementField != null && this.RecordingEndorsementField.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A field for a stakeholder signature.
        /// </summary>
        [XmlElement("STAKEHOLDER_SIGNATURE_FIELD", Order = 4)]
        public STAKEHOLDER_SIGNATURE_FIELD StakeholderSignatureField;

        /// <summary>
        /// Gets or sets a value indicating whether the StakeholderSignatureField element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the StakeholderSignatureField element has been assigned a value.</value>
        [XmlIgnore]
        public bool StakeholderSignatureFieldSpecified
        {
            get { return this.StakeholderSignatureField != null && this.StakeholderSignatureField.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A field for a witness signature.
        /// </summary>
        [XmlElement("WITNESS_SIGNATURE_FIELD", Order = 5)]
        public WITNESS_SIGNATURE_FIELD WitnessSignatureField;

        /// <summary>
        /// Gets or sets a value indicating whether the WitnessSignatureField element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the WitnessSignatureField element has been assigned a value.</value>
        [XmlIgnore]
        public bool WitnessSignatureFieldSpecified
        {
            get { return this.WitnessSignatureField != null && this.WitnessSignatureField.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 6)]
        public VIEW_FIELD_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
