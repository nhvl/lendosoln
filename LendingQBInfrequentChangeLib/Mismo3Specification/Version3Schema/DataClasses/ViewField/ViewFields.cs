namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class VIEW_FIELDS
    {
        /// <summary>
        /// Gets a value indicating whether the VIEW_FIELDS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ViewFieldSpecified;
            }
        }

        /// <summary>
        /// A collection of view fields.
        /// </summary>
        [XmlElement("VIEW_FIELD", Order = 0)]
		public List<VIEW_FIELD> ViewField = new List<VIEW_FIELD>();

        /// <summary>
        /// Gets or sets a value indicating whether the ViewField element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ViewField element has been assigned a value.</value>
        [XmlIgnore]
        public bool ViewFieldSpecified
        {
            get { return this.ViewField != null && this.ViewField.Count(v => v != null && v.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public VIEW_FIELDS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
