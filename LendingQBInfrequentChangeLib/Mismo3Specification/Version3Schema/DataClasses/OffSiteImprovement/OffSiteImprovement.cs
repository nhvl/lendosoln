namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class OFF_SITE_IMPROVEMENT
    {
        /// <summary>
        /// Gets a value indicating whether the OFF_SITE_IMPROVEMENT container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ConditionRatingTypeSpecified
                    || this.ExtensionSpecified
                    || this.OffSiteImprovementDescriptionSpecified
                    || this.OffSiteImprovementOwnershipTypeSpecified
                    || this.OffSiteImprovementsUtilityNotTypicalDescriptionSpecified
                    || this.OffSiteImprovementsUtilityTypicalIndicatorSpecified
                    || this.OffSiteImprovementTypeOtherDescriptionSpecified
                    || this.OffSiteImprovementTypeSpecified;
            }
        }

        /// <summary>
        /// A rating of the condition of the identified component.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<ConditionRatingBase> ConditionRatingType;

        /// <summary>
        /// Gets or sets a value indicating whether the ConditionRatingType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConditionRatingType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConditionRatingTypeSpecified
        {
            get { return this.ConditionRatingType != null && this.ConditionRatingType.enumValue != ConditionRatingBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe in detail the off site improvement specified by the Off Site Improvement Type.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString OffSiteImprovementDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the OffSiteImprovementDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OffSiteImprovementDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool OffSiteImprovementDescriptionSpecified
        {
            get { return OffSiteImprovementDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the ownership or responsibility type of an off site improvement feature such as a public or private road.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<OffSiteImprovementOwnershipBase> OffSiteImprovementOwnershipType;

        /// <summary>
        /// Gets or sets a value indicating whether the OffSiteImprovementOwnershipType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OffSiteImprovementOwnershipType element has been assigned a value.</value>
        [XmlIgnore]
        public bool OffSiteImprovementOwnershipTypeSpecified
        {
            get { return this.OffSiteImprovementOwnershipType != null && this.OffSiteImprovementOwnershipType.enumValue != OffSiteImprovementOwnershipBase.Blank; }
            set { }
        }

        /// <summary>
        /// Describes how the utilities for the off site improvements differ from what is typical for the market area.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString OffSiteImprovementsUtilityNotTypicalDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the OffSiteImprovementsUtilityNotTypicalDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OffSiteImprovementsUtilityNotTypicalDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool OffSiteImprovementsUtilityNotTypicalDescriptionSpecified
        {
            get { return OffSiteImprovementsUtilityNotTypicalDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the utilities for the off site improvements are typical for the market area.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOIndicator OffSiteImprovementsUtilityTypicalIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the OffSiteImprovementsUtilityTypicalIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OffSiteImprovementsUtilityTypicalIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool OffSiteImprovementsUtilityTypicalIndicatorSpecified
        {
            get { return OffSiteImprovementsUtilityTypicalIndicator != null; }
            set { }
        }

        /// <summary>
        /// Used to specify a particular off site improvement feature of the property.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOEnum<OffSiteImprovementBase> OffSiteImprovementType;

        /// <summary>
        /// Gets or sets a value indicating whether the OffSiteImprovementType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OffSiteImprovementType element has been assigned a value.</value>
        [XmlIgnore]
        public bool OffSiteImprovementTypeSpecified
        {
            get { return this.OffSiteImprovementType != null && this.OffSiteImprovementType.enumValue != OffSiteImprovementBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the off site improvement if Other is selected as the Property Off Site Improvements Features Type.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOString OffSiteImprovementTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the OffSiteImprovementTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OffSiteImprovementTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool OffSiteImprovementTypeOtherDescriptionSpecified
        {
            get { return OffSiteImprovementTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 7)]
        public OFF_SITE_IMPROVEMENT_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
