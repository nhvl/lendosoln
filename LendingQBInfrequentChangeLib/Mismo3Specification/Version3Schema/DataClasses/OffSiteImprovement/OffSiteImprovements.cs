namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class OFF_SITE_IMPROVEMENTS
    {
        /// <summary>
        /// Gets a value indicating whether the OFF_SITE_IMPROVEMENTS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.OffSiteImprovementSpecified;
            }
        }

        /// <summary>
        /// A collection of offsite improvements.
        /// </summary>
        [XmlElement("OFF_SITE_IMPROVEMENT", Order = 0)]
		public List<OFF_SITE_IMPROVEMENT> OffSiteImprovement = new List<OFF_SITE_IMPROVEMENT>();

        /// <summary>
        /// Gets or sets a value indicating whether the OffSiteImprovement element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OffSiteImprovement element has been assigned a value.</value>
        [XmlIgnore]
        public bool OffSiteImprovementSpecified
        {
            get { return this.OffSiteImprovement != null && this.OffSiteImprovement.Count(o => o != null && o.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public OFF_SITE_IMPROVEMENTS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
