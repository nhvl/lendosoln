namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class LENDER_3_3_1
    {
        /// <summary>
        /// Gets a value indicating whether the LENDER_3_3_1 container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditUnionIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// When true, indicates that the lender is a Credit Union.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIndicator CreditUnionIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditUnionIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditUnionIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditUnionIndicatorSpecified
        {
            get { return CreditUnionIndicator != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public LENDER_3_3_1_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
