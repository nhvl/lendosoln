namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class LENDER
    {
        /// <summary>
        /// Gets a value indicating whether the LENDER container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.LenderDocumentsOrderedByNameSpecified
                    || this.LenderFunderNameSpecified
                    || this.RegulatoryAgencyLenderIdentifierSpecified;
            }
        }

        /// <summary>
        /// The name of lenders associate who is responsible for ordering or drawing the loan documents.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString LenderDocumentsOrderedByName;

        /// <summary>
        /// Gets or sets a value indicating whether the LenderDocumentsOrderedByName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LenderDocumentsOrderedByName element has been assigned a value.</value>
        [XmlIgnore]
        public bool LenderDocumentsOrderedByNameSpecified
        {
            get { return LenderDocumentsOrderedByName != null; }
            set { }
        }

        /// <summary>
        /// The name of lenders associate who is responsible for supervising the funding of the loan.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString LenderFunderName;

        /// <summary>
        /// Gets or sets a value indicating whether the LenderFunderName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LenderFunderName element has been assigned a value.</value>
        [XmlIgnore]
        public bool LenderFunderNameSpecified
        {
            get { return LenderFunderName != null; }
            set { }
        }

        /// <summary>
        /// Identification of the lender funding the loan. The identification is as follows: FDIC-insured lenders indicate their FDIC Insurance Certificate Number; Federally-insured credit unions indicate their charter/insurance number; Farm Credit institutions indicate their UNINUM number. Other lenders who fund loans sold to or securitized by FNMA or FHLMC enter FNMA or FHLMC seller/servicer number.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOIdentifier RegulatoryAgencyLenderIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the RegulatoryAgencyLenderIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RegulatoryAgencyLenderIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool RegulatoryAgencyLenderIdentifierSpecified
        {
            get { return RegulatoryAgencyLenderIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public LENDER_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
