namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class UNPARSED_LEGAL_DESCRIPTIONS
    {
        /// <summary>
        /// Gets a value indicating whether the UNPARSED_LEGAL_DESCRIPTIONS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.UnparsedLegalDescriptionSpecified;
            }
        }

        /// <summary>
        /// A collection of unparsed legal descriptions.
        /// </summary>
        [XmlElement("UNPARSED_LEGAL_DESCRIPTION", Order = 0)]
		public List<UNPARSED_LEGAL_DESCRIPTION> UnparsedLegalDescription = new List<UNPARSED_LEGAL_DESCRIPTION>();

        /// <summary>
        /// Gets or sets a value indicating whether the UnparsedLegalDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the UnparsedLegalDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool UnparsedLegalDescriptionSpecified
        {
            get { return this.UnparsedLegalDescription != null && this.UnparsedLegalDescription.Count(u => u != null && u.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public UNPARSED_LEGAL_DESCRIPTIONS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
