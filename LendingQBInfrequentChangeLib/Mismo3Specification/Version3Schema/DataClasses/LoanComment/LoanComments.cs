namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class LOAN_COMMENTS
    {
        /// <summary>
        /// Gets a value indicating whether the LOAN_COMMENTS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.LoanCommentSpecified;
            }
        }

        /// <summary>
        /// A collection of loan comments.
        /// </summary>
        [XmlElement("LOAN_COMMENT", Order = 0)]
        public List<LOAN_COMMENT> LoanComment = new List<LOAN_COMMENT>();

        /// <summary>
        /// Gets or sets a value indicating whether the LoanComment element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanComment element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanCommentSpecified
        {
            get { return this.LoanComment != null && this.LoanComment.Count(l => l != null && l.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public LOAN_COMMENTS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
