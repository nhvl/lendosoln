namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class OWNED_PROPERTY
    {
        /// <summary>
        /// Gets a value indicating whether the OWNED_PROPERTY container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.OwnedPropertyDetailSpecified
                    || this.PropertySpecified;
            }
        }

        /// <summary>
        /// Details about the owned property.
        /// </summary>
        [XmlElement("OWNED_PROPERTY_DETAIL", Order = 0)]
        public OWNED_PROPERTY_DETAIL OwnedPropertyDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the OwnedPropertyDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OwnedPropertyDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool OwnedPropertyDetailSpecified
        {
            get { return this.OwnedPropertyDetail != null && this.OwnedPropertyDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The property-specific information about the owned property.
        /// </summary>
        [XmlElement("PROPERTY", Order = 1)]
        public PROPERTY Property;

        /// <summary>
        /// Gets or sets a value indicating whether the Property element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Property element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertySpecified
        {
            get { return this.Property != null && this.Property.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public OWNED_PROPERTY_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
