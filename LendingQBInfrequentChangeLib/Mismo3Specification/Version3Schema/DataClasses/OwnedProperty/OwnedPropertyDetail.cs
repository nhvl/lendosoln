namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class OWNED_PROPERTY_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the OWNED_PROPERTY_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.OwnedPropertyDispositionStatusTypeSpecified
                    || this.OwnedPropertyLienInstallmentAmountSpecified
                    || this.OwnedPropertyLienUPBAmountSpecified
                    || this.OwnedPropertyMaintenanceExpenseAmountSpecified
                    || this.OwnedPropertyOwnedUnitCountSpecified
                    || this.OwnedPropertyRentalIncomeGrossAmountSpecified
                    || this.OwnedPropertyRentalIncomeNetAmountSpecified;
            }
        }

        /// <summary>
        /// The applicants intended disposition of an owned real property. One source of this data is the URLA in Section VI (Schedule of Real Estate Owned).
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<OwnedPropertyDispositionStatusBase> OwnedPropertyDispositionStatusType;

        /// <summary>
        /// Gets or sets a value indicating whether the OwnedPropertyDispositionStatusType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OwnedPropertyDispositionStatusType element has been assigned a value.</value>
        [XmlIgnore]
        public bool OwnedPropertyDispositionStatusTypeSpecified
        {
            get { return this.OwnedPropertyDispositionStatusType != null && this.OwnedPropertyDispositionStatusType.enumValue != OwnedPropertyDispositionStatusBase.Blank; }
            set { }
        }

        /// <summary>
        /// The total monthly payment made on outstanding mortgages and liens against the owned real property.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOAmount OwnedPropertyLienInstallmentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the OwnedPropertyLienInstallmentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OwnedPropertyLienInstallmentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool OwnedPropertyLienInstallmentAmountSpecified
        {
            get { return OwnedPropertyLienInstallmentAmount != null; }
            set { }
        }

        /// <summary>
        /// The total amount of all remaining mortgages and liens against the owned real property.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOAmount OwnedPropertyLienUPBAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the OwnedPropertyLienUPBAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OwnedPropertyLienUPBAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool OwnedPropertyLienUPBAmountSpecified
        {
            get { return OwnedPropertyLienUPBAmount != null; }
            set { }
        }

        /// <summary>
        /// The monthly expense amount for property taxes, maintenance and insurance on the owned real property.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOAmount OwnedPropertyMaintenanceExpenseAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the OwnedPropertyMaintenanceExpenseAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OwnedPropertyMaintenanceExpenseAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool OwnedPropertyMaintenanceExpenseAmountSpecified
        {
            get { return OwnedPropertyMaintenanceExpenseAmount != null; }
            set { }
        }

        /// <summary>
        /// The number of individual family dwelling units that are in the owned property.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOCount OwnedPropertyOwnedUnitCount;

        /// <summary>
        /// Gets or sets a value indicating whether the OwnedPropertyOwnedUnitCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OwnedPropertyOwnedUnitCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool OwnedPropertyOwnedUnitCountSpecified
        {
            get { return OwnedPropertyOwnedUnitCount != null; }
            set { }
        }

        /// <summary>
        /// The amount of revenue generated by the owned property from rent on a monthly basis.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOAmount OwnedPropertyRentalIncomeGrossAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the OwnedPropertyRentalIncomeGrossAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OwnedPropertyRentalIncomeGrossAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool OwnedPropertyRentalIncomeGrossAmountSpecified
        {
            get { return OwnedPropertyRentalIncomeGrossAmount != null; }
            set { }
        }

        /// <summary>
        /// The amount of the rental income that is net of mortgage payments, insurance, maintenance, taxes, and miscellaneous expenses that is generated by the owned property on a monthly basis.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOAmount OwnedPropertyRentalIncomeNetAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the OwnedPropertyRentalIncomeNetAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OwnedPropertyRentalIncomeNetAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool OwnedPropertyRentalIncomeNetAmountSpecified
        {
            get { return OwnedPropertyRentalIncomeNetAmount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 7)]
        public OWNED_PROPERTY_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null ? Extension.ShouldSerialize : false; }
            set { }
        }
    }
}
