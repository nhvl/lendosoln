namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class BASEMENT
    {
        /// <summary>
        /// Gets a value indicating whether the BASEMENT container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BasementDetailSpecified
                    || this.BasementFeaturesSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// Details on a basement.
        /// </summary>
        [XmlElement("BASEMENT_DETAIL", Order = 0)]
        public BASEMENT_DETAIL BasementDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the BasementDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BasementDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool BasementDetailSpecified
        {
            get { return this.BasementDetail != null && this.BasementDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Features of a basement.
        /// </summary>
        [XmlElement("BASEMENT_FEATURES", Order = 1)]
        public BASEMENT_FEATURES BasementFeatures;

        /// <summary>
        /// Gets or sets a value indicating whether the BasementFeatures element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BasementFeatures element has been assigned a value.</value>
        [XmlIgnore]
        public bool BasementFeaturesSpecified
        {
            get { return this.BasementFeatures != null && this.BasementFeatures.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public BASEMENT_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
