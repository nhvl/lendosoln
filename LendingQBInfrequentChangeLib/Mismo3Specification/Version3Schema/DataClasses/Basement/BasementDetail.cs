namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class BASEMENT_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the BASEMENT_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BasementCeilingMaterialDescriptionSpecified
                    || this.BasementExitTypeSpecified
                    || this.BasementFinishDescriptionSpecified
                    || this.BasementFinishedAreaSquareFeetNumberSpecified
                    || this.BasementFinishedIndicatorSpecified
                    || this.BasementFinishedPercentSpecified
                    || this.BasementFirstFloorAreaPercentSpecified
                    || this.BasementFloorMaterialDescriptionSpecified
                    || this.BasementWallMaterialDescriptionSpecified
                    || this.ComponentAdjustmentAmountSpecified
                    || this.ConditionRatingDescriptionSpecified
                    || this.ConditionRatingTypeSpecified
                    || this.ExtensionSpecified
                    || this.QualityRatingDescriptionSpecified
                    || this.QualityRatingTypeSpecified
                    || this.SquareFeetNumberSpecified;
            }
        }

        /// <summary>
        /// Describes the materials used to finish the basement ceiling.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString BasementCeilingMaterialDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the BasementCeilingMaterialDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BasementCeilingMaterialDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool BasementCeilingMaterialDescriptionSpecified
        {
            get { return BasementCeilingMaterialDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the type of exit for a basement.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<BasementExitBase> BasementExitType;

        /// <summary>
        /// Gets or sets a value indicating whether the BasementExitType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BasementExitType element has been assigned a value.</value>
        [XmlIgnore]
        public bool BasementExitTypeSpecified
        {
            get { return this.BasementExitType != null && this.BasementExitType.enumValue != BasementExitBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field describing the finish of the basement.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString BasementFinishDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the BasementFinishDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BasementFinishDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool BasementFinishDescriptionSpecified
        {
            get { return BasementFinishDescription != null; }
            set { }
        }

        /// <summary>
        /// The total finished area of the basement in square feet.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMONumeric BasementFinishedAreaSquareFeetNumber;

        /// <summary>
        /// Gets or sets a value indicating whether the BasementFinishedAreaSquareFeetNumber element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BasementFinishedAreaSquareFeetNumber element has been assigned a value.</value>
        [XmlIgnore]
        public bool BasementFinishedAreaSquareFeetNumberSpecified
        {
            get { return BasementFinishedAreaSquareFeetNumber != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the basement is finished.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOIndicator BasementFinishedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the BasementFinishedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BasementFinishedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool BasementFinishedIndicatorSpecified
        {
            get { return BasementFinishedIndicator != null; }
            set { }
        }

        /// <summary>
        /// The percentage of the basement that is considered finished.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOPercent BasementFinishedPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the BasementFinishedPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BasementFinishedPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool BasementFinishedPercentSpecified
        {
            get { return BasementFinishedPercent != null; }
            set { }
        }

        /// <summary>
        /// Specifies the percent of the first floor which the basement overlaps.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOPercent BasementFirstFloorAreaPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the BasementFirstFloorAreaPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BasementFirstFloorAreaPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool BasementFirstFloorAreaPercentSpecified
        {
            get { return BasementFirstFloorAreaPercent != null; }
            set { }
        }

        /// <summary>
        /// Describes the materials used to finish the basement floor.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOString BasementFloorMaterialDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the BasementFloorMaterialDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BasementFloorMaterialDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool BasementFloorMaterialDescriptionSpecified
        {
            get { return BasementFloorMaterialDescription != null; }
            set { }
        }

        /// <summary>
        /// Describes the materials used to finish the basement walls.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOString BasementWallMaterialDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the BasementWallMaterialDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BasementWallMaterialDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool BasementWallMaterialDescriptionSpecified
        {
            get { return BasementWallMaterialDescription != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount (either positive or negative) adjustment being made for a specific component of the property.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOAmount ComponentAdjustmentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ComponentAdjustmentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ComponentAdjustmentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ComponentAdjustmentAmountSpecified
        {
            get { return ComponentAdjustmentAmount != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe in detail the condition rating of the identified component.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOString ConditionRatingDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ConditionRatingDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConditionRatingDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConditionRatingDescriptionSpecified
        {
            get { return ConditionRatingDescription != null; }
            set { }
        }

        /// <summary>
        /// A rating of the condition of the identified component.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOEnum<ConditionRatingBase> ConditionRatingType;

        /// <summary>
        /// Gets or sets a value indicating whether the ConditionRatingType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConditionRatingType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConditionRatingTypeSpecified
        {
            get { return this.ConditionRatingType != null && this.ConditionRatingType.enumValue != ConditionRatingBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe in detail the quality rating of the identified component.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOString QualityRatingDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the QualityRatingDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the QualityRatingDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool QualityRatingDescriptionSpecified
        {
            get { return QualityRatingDescription != null; }
            set { }
        }

        /// <summary>
        /// A rating of the quality of the identified component type.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOEnum<QualityRatingBase> QualityRatingType;

        /// <summary>
        /// Gets or sets a value indicating whether the QualityRatingType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the QualityRatingType element has been assigned a value.</value>
        [XmlIgnore]
        public bool QualityRatingTypeSpecified
        {
            get { return this.QualityRatingType != null && this.QualityRatingType.enumValue != QualityRatingBase.Blank; }
            set { }
        }

        /// <summary>
        /// Identifies the total area measured in square feet.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMONumeric SquareFeetNumber;

        /// <summary>
        /// Gets or sets a value indicating whether the SquareFeetNumber element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SquareFeetNumber element has been assigned a value.</value>
        [XmlIgnore]
        public bool SquareFeetNumberSpecified
        {
            get { return SquareFeetNumber != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 15)]
        public BASEMENT_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
