namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class ASSET_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the ASSET_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AssetAccountIdentifierSpecified
                    || this.AssetAccountInNameOfDescriptionSpecified
                    || this.AssetAccountTypeOtherDescriptionSpecified
                    || this.AssetAccountTypeSpecified
                    || this.AssetCashOrMarketValueAmountSpecified
                    || this.AssetDescriptionSpecified
                    || this.AssetLiquidityIndicatorSpecified
                    || this.AssetNetValueAmountSpecified
                    || this.AssetTypeOtherDescriptionSpecified
                    || this.AssetTypeSpecified
                    || this.AutomobileMakeDescriptionSpecified
                    || this.AutomobileModelYearSpecified
                    || this.ExtensionSpecified
                    || this.LifeInsuranceFaceValueAmountSpecified
                    || this.StockBondMutualFundShareCountSpecified
                    || this.VerifiedIndicatorSpecified;
            }
        }

        /// <summary>
        /// A unique identifier of the borrowers asset. Collected on the URLA in Section VI (Assets).
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIdentifier AssetAccountIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the AssetAccountIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AssetAccountIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool AssetAccountIdentifierSpecified
        {
            get { return AssetAccountIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The name of the persons or entities in whose name the account is held.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString AssetAccountInNameOfDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the AssetAccountInNameOfDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AssetAccountInNameOfDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool AssetAccountInNameOfDescriptionSpecified
        {
            get { return AssetAccountInNameOfDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the type of account in which the assets are held. Usually used when pledging assets as collateral.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<AssetAccountBase> AssetAccountType;

        /// <summary>
        /// Gets or sets a value indicating whether the AssetAccountType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AssetAccountType element has been assigned a value.</value>
        [XmlIgnore]
        public bool AssetAccountTypeSpecified
        {
            get { return this.AssetAccountType != null && this.AssetAccountType.enumValue != AssetAccountBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Asset Account Type.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString AssetAccountTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the AssetAccountTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AssetAccountTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool AssetAccountTypeOtherDescriptionSpecified
        {
            get { return AssetAccountTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The gross dollar amount of the cash or market value of the borrowers Asset Type. The market value is the price at which an asset is transferred between a willing buyer and a willing seller, each of whom has a reasonable knowledge of all pertinent facts and neither being under any compulsion to buy or sell. (e.g., IBMs closing quote) Market value and cash values tend to be equal. 
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOAmount AssetCashOrMarketValueAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the AssetCashOrMarketValueAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AssetCashOrMarketValueAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool AssetCashOrMarketValueAmountSpecified
        {
            get { return AssetCashOrMarketValueAmount != null; }
            set { }
        }

        /// <summary>
        /// A text description that further defines the Asset. This could be used to describe the shares associated with the stocks, bonds or mutual funds, retirement funds or business owned that the borrower has disclosed (named) as an asset.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString AssetDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the AssetDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AssetDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool AssetDescriptionSpecified
        {
            get { return AssetDescription != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that the asset is classified as liquid. When false, indicates that the asset is classified as non liquid.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOIndicator AssetLiquidityIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the AssetLiquidityIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AssetLiquidityIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool AssetLiquidityIndicatorSpecified
        {
            get { return AssetLiquidityIndicator != null; }
            set { }
        }

        /// <summary>
        /// The cash or market value of the asset less any outstanding liabilities or obligations.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOAmount AssetNetValueAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the AssetNetValueAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AssetNetValueAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool AssetNetValueAmountSpecified
        {
            get { return AssetNetValueAmount != null; }
            set { }
        }

        /// <summary>
        /// Specifies the general names (type) of items commonly listed as financial assets of the borrower(s) in a mortgage loan transaction. Assets may be either liquid or fixed and are associated with a corresponding asset amount.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOEnum<AssetBase> AssetType;

        /// <summary>
        /// Gets or sets a value indicating whether the AssetType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AssetType element has been assigned a value.</value>
        [XmlIgnore]
        public bool AssetTypeSpecified
        {
            get { return this.AssetType != null && this.AssetType.enumValue != AssetBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected as the Asset Type.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOString AssetTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the AssetTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AssetTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool AssetTypeOtherDescriptionSpecified
        {
            get { return AssetTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture the make and model of borrowers vehicle disclosed as asset. Collected on the URLA in Section VI Assets (Automobiles Owned).
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOString AutomobileMakeDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the AutomobileMakeDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AutomobileMakeDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool AutomobileMakeDescriptionSpecified
        {
            get { return AutomobileMakeDescription != null; }
            set { }
        }

        /// <summary>
        /// A field used to capture the model year of borrowers vehicle disclosed as asset. Collected on the URLA in Section VI Assets (Automobiles Owned).
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOYear AutomobileModelYear;

        /// <summary>
        /// Gets or sets a value indicating whether the AutomobileModelYear element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AutomobileModelYear element has been assigned a value.</value>
        [XmlIgnore]
        public bool AutomobileModelYearSpecified
        {
            get { return AutomobileModelYear != null; }
            set { }
        }

        /// <summary>
        /// The value of the life insurance face number.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOAmount LifeInsuranceFaceValueAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the Life Insurance Face Value Amount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Life Insurance Face Value Amount element has been assigned a value.</value>
        [XmlIgnore]
        public bool LifeInsuranceFaceValueAmountSpecified
        {
            get { return LifeInsuranceFaceValueAmount != null; }
            set { }
        }

        /// <summary>
        /// The number of shares associated with the stocks or bonds that the borrower has disclosed (named) as an asset. Collected on the URLA in Section VI Assets (Stocks and Bonds).
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOCount StockBondMutualFundShareCount;

        /// <summary>
        /// Gets or sets a value indicating whether the StockBondMutualFundShareCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the StockBondMutualFundShareCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool StockBondMutualFundShareCountSpecified
        {
            get { return StockBondMutualFundShareCount != null; }
            set { }
        }

        /// <summary>
        /// Flag to indicate if asset value has been verified based on trading partner rules and definitions.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOIndicator VerifiedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the VerifiedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the VerifiedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool VerifiedIndicatorSpecified
        {
            get { return VerifiedIndicator != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 15)]
        public ASSET_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
