namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class ASSET
    {
        /// <summary>
        /// Gets a value indicating whether the ASSET container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AssetDetailSpecified
                    || this.AssetDocumentationsSpecified
                    || this.AssetHolderSpecified
                    || this.OwnedPropertySpecified
                    || this.VerificationSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// Details about the asset.
        /// </summary>
        [XmlElement("ASSET_DETAIL", Order = 0)]
        public ASSET_DETAIL AssetDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the AssetDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AssetDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool AssetDetailSpecified
        {
            get { return this.AssetDetail != null && this.AssetDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Documentation related to the asset.
        /// </summary>
        [XmlElement("ASSET_DOCUMENTATIONS", Order = 1)]
        public ASSET_DOCUMENTATIONS AssetDocumentations;

        /// <summary>
        /// Gets or sets a value indicating whether the AssetDocumentations element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AssetDocumentations element has been assigned a value.</value>
        [XmlIgnore]
        public bool AssetDocumentationsSpecified
        {
            get { return this.AssetDocumentations != null && this.AssetDocumentations.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Information on who holds the asset.
        /// </summary>
        [XmlElement("ASSET_HOLDER", Order = 2)]
        public ASSET_HOLDER AssetHolder;

        /// <summary>
        /// Gets or sets a value indicating whether the AssetHolder element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AssetHolder element has been assigned a value.</value>
        [XmlIgnore]
        public bool AssetHolderSpecified
        {
            get { return this.AssetHolder != null && this.AssetHolder.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details about an owned property asset.
        /// </summary>
        [XmlElement("OWNED_PROPERTY", Order = 3)]
        public OWNED_PROPERTY OwnedProperty;

        /// <summary>
        /// Gets or sets a value indicating whether the OwnedProperty element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OwnedProperty element has been assigned a value.</value>
        [XmlIgnore]
        public bool OwnedPropertySpecified
        {
            get { return this.OwnedProperty != null && this.OwnedProperty.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Verification of an asset.
        /// </summary>
        [XmlElement("VERIFICATION", Order = 4)]
        public VERIFICATION Verification;

        /// <summary>
        /// Gets or sets a value indicating whether the Verification element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Verification element has been assigned a value.</value>
        [XmlIgnore]
        public bool VerificationSpecified
        {
            get { return this.Verification != null && this.Verification.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 5)]
        public ASSET_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// Gets or sets the $$xlink$$ label.
        /// </summary>
        [XmlAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified, Namespace = Mismo3Constants.XlinkNamespace)]
        public string label;

        /// <summary>
        /// Indicates whether the label can be serialized.
        /// </summary>
        /// <returns>A boolean indicating whether the label can be serialized.</returns>
        public bool ShouldSerializelabel()
        {
            return !string.IsNullOrEmpty(label);
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
