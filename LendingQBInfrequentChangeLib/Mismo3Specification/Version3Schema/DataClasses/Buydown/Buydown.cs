namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class BUYDOWN
    {
        /// <summary>
        /// Gets a value indicating whether the BUYDOWN container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BuydownFundsSpecified
                    || this.BuydownOccurrencesSpecified
                    || this.BuydownRuleSpecified
                    || this.BuydownSchedulesSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// Funds for the buy down.
        /// </summary>
        [XmlElement("BUYDOWN_FUNDS", Order = 0)]
        public BUYDOWN_FUNDS BuydownFunds;

        /// <summary>
        /// Gets or sets a value indicating whether the BuyDownFunds element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BuyDownFunds element has been assigned a value.</value>
        [XmlIgnore]
        public bool BuydownFundsSpecified
        {
            get { return this.BuydownFunds != null && this.BuydownFunds.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Occurrences of the buy down.
        /// </summary>
        [XmlElement("BUYDOWN_OCCURRENCES", Order = 1)]
        public BUYDOWN_OCCURRENCES BuydownOccurrences;

        /// <summary>
        /// Gets or sets a value indicating whether the BuyDownOccurrences element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BuyDownOccurrences element has been assigned a value.</value>
        [XmlIgnore]
        public bool BuydownOccurrencesSpecified
        {
            get { return this.BuydownOccurrences != null && this.BuydownOccurrences.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Rule for the buy down.
        /// </summary>
        [XmlElement("BUYDOWN_RULE", Order = 2)]
        public BUYDOWN_RULE BuydownRule;

        /// <summary>
        /// Gets or sets a value indicating whether the BuyDownRule element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BuyDownRule element has been assigned a value.</value>
        [XmlIgnore]
        public bool BuydownRuleSpecified
        {
            get { return this.BuydownRule != null && this.BuydownRule.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Schedules for the buy down.
        /// </summary>
        [XmlElement("BUYDOWN_SCHEDULES", Order = 3)]
        public BUYDOWN_SCHEDULES BuydownSchedules;

        /// <summary>
        /// Gets or sets a value indicating whether the BuyDownSchedules element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BuyDownSchedules element has been assigned a value.</value>
        [XmlIgnore]
        public bool BuydownSchedulesSpecified
        {
            get { return this.BuydownSchedules != null && this.BuydownSchedules.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public BUYDOWN_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
