namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CREDIT_FILE_VARIATIONS
    {
        /// <summary>
        /// Gets a value indicating whether the CREDIT_FILE_VARIATIONS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditFileVariationSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of credit file variations.
        /// </summary>
        [XmlElement("CREDIT_FILE_VARIATION", Order = 0)]
		public List<CREDIT_FILE_VARIATION> CreditFileVariation = new List<CREDIT_FILE_VARIATION>();

        /// <summary>
        /// Gets or sets a value indicating whether the CreditFileVariation element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditFileVariation element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditFileVariationSpecified
        {
            get { return this.CreditFileVariation != null && this.CreditFileVariation.Count(c => c != null && c.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public CREDIT_FILE_VARIATIONS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
