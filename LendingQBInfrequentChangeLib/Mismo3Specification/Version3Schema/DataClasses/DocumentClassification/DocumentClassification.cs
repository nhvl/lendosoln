namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class DOCUMENT_CLASSIFICATION
    {
        /// <summary>
        /// Gets a value indicating whether the DOCUMENT_CLASSIFICATION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DocumentClassesSpecified
                    || this.DocumentClassificationDetailSpecified
                    || this.DocumentFormContributorsSpecified
                    || this.DocumentUsagesSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A list of document classes.
        /// </summary>
        [XmlElement("DOCUMENT_CLASSES", Order = 0)]
        public DOCUMENT_CLASSES DocumentClasses;

        /// <summary>
        /// Gets or sets a value indicating whether the DocumentClasses element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DocumentClasses element has been assigned a value.</value>
        [XmlIgnore]
        public bool DocumentClassesSpecified
        {
            get { return this.DocumentClasses != null && this.DocumentClasses.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details on a document classification.
        /// </summary>
        [XmlElement("DOCUMENT_CLASSIFICATION_DETAIL", Order = 1)]
        public DOCUMENT_CLASSIFICATION_DETAIL DocumentClassificationDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the DocumentClassificationDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DocumentClassificationDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool DocumentClassificationDetailSpecified
        {
            get { return this.DocumentClassificationDetail != null && this.DocumentClassificationDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A list of document form contributors.
        /// </summary>
        [XmlElement("DOCUMENT_FORM_CONTRIBUTORS", Order = 2)]
        public DOCUMENT_FORM_CONTRIBUTORS DocumentFormContributors;

        /// <summary>
        /// Gets or sets a value indicating whether the DocumentFormContributors element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DocumentFormContributors element has been assigned a value.</value>
        [XmlIgnore]
        public bool DocumentFormContributorsSpecified
        {
            get { return this.DocumentFormContributors != null && this.DocumentFormContributors.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A list of document usages.
        /// </summary>
        [XmlElement("DOCUMENT_USAGES", Order = 3)]
        public DOCUMENT_USAGES DocumentUsages;

        /// <summary>
        /// Gets or sets a value indicating whether the DocumentUsages element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DocumentUsages element has been assigned a value.</value>
        [XmlIgnore]
        public bool DocumentUsagesSpecified
        {
            get { return this.DocumentUsages != null && this.DocumentUsages.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public DOCUMENT_CLASSIFICATION_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
