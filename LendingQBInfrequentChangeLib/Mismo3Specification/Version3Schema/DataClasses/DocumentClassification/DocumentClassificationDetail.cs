namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class DOCUMENT_CLASSIFICATION_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the DOCUMENT_CLASSIFICATION_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AcceptableSigningMethodTypeSpecified
                    || this.DocumentFormIssuingEntityNameTypeOtherDescriptionSpecified
                    || this.DocumentFormIssuingEntityNameTypeSpecified
                    || this.DocumentFormIssuingEntityNumberIdentifierSpecified
                    || this.DocumentFormIssuingEntityVersionIdentifierSpecified
                    || this.DocumentFormPublisherEntityNameSpecified
                    || this.DocumentFormPublisherNumberIdentifierSpecified
                    || this.DocumentFormPublisherVersionIdentifierSpecified
                    || this.DocumentNameSpecified
                    || this.DocumentNegotiableInstrumentIndicatorSpecified
                    || this.DocumentPeriodEndDateSpecified
                    || this.DocumentPeriodStartDateSpecified
                    || this.DocumentRecordationProcessingTypeSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// An instruction as to the signing methods that are acceptable for this document. It does not reflect a document instance state.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<AcceptableSigningMethodBase> AcceptableSigningMethodType;

        /// <summary>
        /// Gets or sets a value indicating whether the AcceptableSigningMethodType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AcceptableSigningMethodType element has been assigned a value.</value>
        [XmlIgnore]
        public bool AcceptableSigningMethodTypeSpecified
        {
            get { return this.AcceptableSigningMethodType != null && this.AcceptableSigningMethodType.enumValue != AcceptableSigningMethodBase.Blank; }
            set { }
        }

        /// <summary>
        /// Identifies the organization that defined the document form/template.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<DocumentFormIssuingEntityNameBase> DocumentFormIssuingEntityNameType;

        /// <summary>
        /// Gets or sets a value indicating whether the DocumentFormIssuingEntityNameType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DocumentFormIssuingEntityNameType element has been assigned a value.</value>
        [XmlIgnore]
        public bool DocumentFormIssuingEntityNameTypeSpecified
        {
            get { return this.DocumentFormIssuingEntityNameType != null && this.DocumentFormIssuingEntityNameType.enumValue != DocumentFormIssuingEntityNameBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Document Form Issuing Entity Name Type.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString DocumentFormIssuingEntityNameTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the DocumentFormIssuingEntityNameTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DocumentFormIssuingEntityNameTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool DocumentFormIssuingEntityNameTypeOtherDescriptionSpecified
        {
            get { return DocumentFormIssuingEntityNameTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The identifier that the issuer assigned to the document form/template. The identifier normally does not include the issuer identifier unless the document is generally known by that identifier.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOIdentifier DocumentFormIssuingEntityNumberIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the DocumentFormIssuingEntityNumberIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DocumentFormIssuingEntityNumberIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool DocumentFormIssuingEntityNumberIdentifierSpecified
        {
            get { return DocumentFormIssuingEntityNumberIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The version identifier that the issuer assigned to the document form/template.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOIdentifier DocumentFormIssuingEntityVersionIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the DocumentFormIssuingEntityVersionIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DocumentFormIssuingEntityVersionIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool DocumentFormIssuingEntityVersionIdentifierSpecified
        {
            get { return DocumentFormIssuingEntityVersionIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The name of the organization that made the form/template available.  Example: D+H.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString DocumentFormPublisherEntityName;

        /// <summary>
        /// Gets or sets a value indicating whether the DocumentFormPublisherEntityName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DocumentFormPublisherEntityName element has been assigned a value.</value>
        [XmlIgnore]
        public bool DocumentFormPublisherEntityNameSpecified
        {
            get { return DocumentFormPublisherEntityName != null; }
            set { }
        }

        /// <summary>
        /// The identifier that the publisher assigned to the document form/template.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOIdentifier DocumentFormPublisherNumberIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the DocumentFormPublisherNumberIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DocumentFormPublisherNumberIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool DocumentFormPublisherNumberIdentifierSpecified
        {
            get { return DocumentFormPublisherNumberIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The version identifier that the publisher assigned to the document form/template.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOIdentifier DocumentFormPublisherVersionIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the DocumentFormPublisherVersionIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DocumentFormPublisherVersionIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool DocumentFormPublisherVersionIdentifierSpecified
        {
            get { return DocumentFormPublisherVersionIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The title that appears on the document view. The name or label that is used for this document. It may be the same as the Document Type. For example, a  promissory may have the document name "Gold Star Customer Agreement".
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOString DocumentName;

        /// <summary>
        /// Gets or sets a value indicating whether the DocumentName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DocumentName element has been assigned a value.</value>
        [XmlIgnore]
        public bool DocumentNameSpecified
        {
            get { return DocumentName != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that the fully executed, authoritative instance of this document is a negotiable instrument. 
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOIndicator DocumentNegotiableInstrumentIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the DocumentNegotiableInstrumentIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DocumentNegotiableInstrumentIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool DocumentNegotiableInstrumentIndicatorSpecified
        {
            get { return DocumentNegotiableInstrumentIndicator != null; }
            set { }
        }

        /// <summary>
        /// The PeriodEndDate is used for the ending date of the period covered in the document.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMODate DocumentPeriodEndDate;

        /// <summary>
        /// Gets or sets a value indicating whether the DocumentPeriodEndDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DocumentPeriodEndDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool DocumentPeriodEndDateSpecified
        {
            get { return DocumentPeriodEndDate != null; }
            set { }
        }

        /// <summary>
        /// The starting date of the period covered in the document. For documents that cover an exact period, e.g. a year or a year and month, only provide those portions of the date. For an IRS tax document this is the tax year on the document.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMODate DocumentPeriodStartDate;

        /// <summary>
        /// Gets or sets a value indicating whether the DocumentPeriodStartDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DocumentPeriodStartDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool DocumentPeriodStartDateSpecified
        {
            get { return DocumentPeriodStartDate != null; }
            set { }
        }

        /// <summary>
        /// Describes whether the document must be delivered to the recorder and recorded by the recorder.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOEnum<DocumentRecordationProcessingBase> DocumentRecordationProcessingType;

        /// <summary>
        /// Gets or sets a value indicating whether the DocumentRecordationProcessingType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DocumentRecordationProcessingType element has been assigned a value.</value>
        [XmlIgnore]
        public bool DocumentRecordationProcessingTypeSpecified
        {
            get { return this.DocumentRecordationProcessingType != null && this.DocumentRecordationProcessingType.enumValue != DocumentRecordationProcessingBase.Blank; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 13)]
        public DOCUMENT_CLASSIFICATION_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
