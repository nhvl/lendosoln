namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class STRUCTURE_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the STRUCTURE_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AccessoryUnitCountSpecified
                    || this.AdditionsDescriptionSpecified
                    || this.AgeYearsCountSpecified
                    || this.AtticExistsIndicatorSpecified
                    || this.BuildingCountSpecified
                    || this.CharacteristicsAffectMarketabilityDescriptionSpecified
                    || this.ElevatorCountSpecified
                    || this.ExtensionSpecified
                    || this.GrossBuildingAreaSquareFeetNumberSpecified
                    || this.GrossLivingAreaSquareFeetDataSourceDescriptionSpecified
                    || this.LivingUnitCountSpecified
                    || this.RentControlDescriptionSpecified
                    || this.RentControlStatusTypeSpecified
                    || this.StoriesNumberSpecified
                    || this.StructureConditionDescriptionSpecified
                    || this.StructureConstructionStatusDescriptionSpecified
                    || this.StructureNeverOccupiedIndicatorSpecified
                    || this.StructureSingleFamilyResidenceIndicatorSpecified
                    || this.StructureStateDescriptionSpecified
                    || this.StructureStateTypeSpecified;
            }
        }

        /// <summary>
        /// Describes the number of accessory units associated with the structure.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOCount AccessoryUnitCount;

        /// <summary>
        /// Gets or sets a value indicating whether the AccessoryUnitCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AccessoryUnitCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool AccessoryUnitCountSpecified
        {
            get { return AccessoryUnitCount != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field describing the additions made to the structure.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString AdditionsDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the AdditionsDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AdditionsDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool AdditionsDescriptionSpecified
        {
            get { return AdditionsDescription != null; }
            set { }
        }

        /// <summary>
        /// The chronological age measured in years. (i.e. the age of a structure).
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOCount AgeYearsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the AgeYearsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AgeYearsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool AgeYearsCountSpecified
        {
            get { return AgeYearsCount != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the structure has an attic.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOIndicator AtticExistsIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the AtticExistsIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AtticExistsIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool AtticExistsIndicatorSpecified
        {
            get { return AtticExistsIndicator != null; }
            set { }
        }

        /// <summary>
        /// The number of separate buildings that compose the structure. This is generally one, but may be multiple buildings for detached duplexes that comprise a 2-4 unit.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOCount BuildingCount;

        /// <summary>
        /// Gets or sets a value indicating whether the BuildingCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BuildingCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool BuildingCountSpecified
        {
            get { return BuildingCount != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field describing the characteristics that affect marketability.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString CharacteristicsAffectMarketabilityDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the CharacteristicsAffectMarketabilityDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CharacteristicsAffectMarketabilityDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool CharacteristicsAffectMarketabilityDescriptionSpecified
        {
            get { return CharacteristicsAffectMarketabilityDescription != null; }
            set { }
        }

        /// <summary>
        /// Number of elevators.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOCount ElevatorCount;

        /// <summary>
        /// Gets or sets a value indicating whether the ElevatorCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ElevatorCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ElevatorCountSpecified
        {
            get { return ElevatorCount != null; }
            set { }
        }

        /// <summary>
        /// The total area of the structure not limited to living areas.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMONumeric GrossBuildingAreaSquareFeetNumber;

        /// <summary>
        /// Gets or sets a value indicating whether the GrossBuildingAreaSquareFeetNumber element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GrossBuildingAreaSquareFeetNumber element has been assigned a value.</value>
        [XmlIgnore]
        public bool GrossBuildingAreaSquareFeetNumberSpecified
        {
            get { return GrossBuildingAreaSquareFeetNumber != null; }
            set { }
        }

        /// <summary>
        /// A free-from text field describing where the value of Gross Living Area Square Feet Count came from.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOString GrossLivingAreaSquareFeetDataSourceDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the GrossLivingAreaSquareFeetDataSourceDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GrossLivingAreaSquareFeetDataSourceDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool GrossLivingAreaSquareFeetDataSourceDescriptionSpecified
        {
            get { return GrossLivingAreaSquareFeetDataSourceDescription != null; }
            set { }
        }

        /// <summary>
        /// Number of separate living units (i.e. in a structure such as an apartment or duplex).
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOCount LivingUnitCount;

        /// <summary>
        /// Gets or sets a value indicating whether the LivingUnitCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LivingUnitCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool LivingUnitCountSpecified
        {
            get { return LivingUnitCount != null; }
            set { }
        }

        /// <summary>
        /// Description of the existing or proposed rent controls for a property.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOString RentControlDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the RentControlDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RentControlDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool RentControlDescriptionSpecified
        {
            get { return RentControlDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the existence or likelihood of rent controls such as controls on businesses and apartment complexes.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOEnum<RentControlStatusBase> RentControlStatusType;

        /// <summary>
        /// Gets or sets a value indicating whether the RentControlStatusType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RentControlStatusType element has been assigned a value.</value>
        [XmlIgnore]
        public bool RentControlStatusTypeSpecified
        {
            get { return this.RentControlStatusType != null && this.RentControlStatusType.enumValue != RentControlStatusBase.Blank; }
            set { }
        }

        /// <summary>
        /// Specifies the number of habitable levels above grade in a structure such as in a 2 story house, a cape cod with 1.5  stories or a 2 story condominium unit in a multi-level building.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMONumeric StoriesNumber;

        /// <summary>
        /// Gets or sets a value indicating whether the StoriesNumber element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the StoriesNumber element has been assigned a value.</value>
        [XmlIgnore]
        public bool StoriesNumberSpecified
        {
            get { return StoriesNumber != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the general condition of the structure.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOString StructureConditionDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the StructureConditionDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the StructureConditionDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool StructureConditionDescriptionSpecified
        {
            get { return StructureConditionDescription != null; }
            set { }
        }

        /// <summary>
        /// A free-form field used to hold additional comments concerning the current status of the construction of the structure.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOString StructureConstructionStatusDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the StructureConstructionStatusDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the StructureConstructionStatusDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool StructureConstructionStatusDescriptionSpecified
        {
            get { return StructureConstructionStatusDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the structure has never been occupied since it was completed.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOIndicator StructureNeverOccupiedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the StructureNeverOccupiedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the StructureNeverOccupiedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool StructureNeverOccupiedIndicatorSpecified
        {
            get { return StructureNeverOccupiedIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the structure is a single-family residence.
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMOIndicator StructureSingleFamilyResidenceIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the StructureSingleFamilyResidenceIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the StructureSingleFamilyResidenceIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool StructureSingleFamilyResidenceIndicatorSpecified
        {
            get { return StructureSingleFamilyResidenceIndicator != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the existence or proposed construction of a structure.
        /// </summary>
        [XmlElement(Order = 17)]
        public MISMOString StructureStateDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the StructureStateDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the StructureStateDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool StructureStateDescriptionSpecified
        {
            get { return StructureStateDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether the construction of the structure is complete. 
        /// </summary>
        [XmlElement(Order = 18)]
        public MISMOEnum<StructureStateBase> StructureStateType;

        /// <summary>
        /// Gets or sets a value indicating whether the StructureStateType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the StructureStateType element has been assigned a value.</value>
        [XmlIgnore]
        public bool StructureStateTypeSpecified
        {
            get { return this.StructureStateType != null && this.StructureStateType.enumValue != StructureStateBase.Blank; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 19)]
        public STRUCTURE_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
