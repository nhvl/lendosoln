namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CLAIM_REQUEST
    {
        /// <summary>
        /// Gets a value indicating whether the CLAIM_REQUEST container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExpenseClaimsSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A list of expense claims.
        /// </summary>
        [XmlElement("EXPENSE_CLAIMS", Order = 0)]
        public EXPENSE_CLAIMS ExpenseClaims;

        /// <summary>
        /// Gets or sets a value indicating whether the ExpenseClaims element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ExpenseClaims element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExpenseClaimsSpecified
        {
            get { return this.ExpenseClaims != null && this.ExpenseClaims.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public CLAIM_REQUEST_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
