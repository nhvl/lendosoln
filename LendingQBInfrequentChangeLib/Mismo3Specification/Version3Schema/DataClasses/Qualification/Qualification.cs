namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class QUALIFICATION
    {
        /// <summary>
        /// Gets a value indicating whether the QUALIFICATION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BorrowerReservesMonthlyPaymentCountSpecified
                    || this.ExtensionSpecified
                    || this.HouseholdOccupancyCountSpecified
                    || this.HousingExpenseRatioPercentSpecified
                    || this.IncomeReportedTypeSpecified
                    || this.ProjectedReservesAmountSpecified
                    || this.QualifyingPaymentAmountSpecified
                    || this.QualifyingRatePercentSpecified
                    || this.QualifyingRateTypeOtherDescriptionSpecified
                    || this.QualifyingRateTypeSpecified
                    || this.SubjectPropertyNetCashFlowAmountSpecified
                    || this.SummaryInterestedPartyContributionsPercentSpecified
                    || this.TotalDebtExpenseRatioPercentSpecified
                    || this.TotalExpensesMonthlyPaymentAmountSpecified
                    || this.TotalLiabilitiesMonthlyPaymentAmountSpecified
                    || this.TotalMonthlyCurrentHousingExpenseAmountSpecified
                    || this.TotalMonthlyIncomeAmountSpecified
                    || this.TotalMonthlyProposedHousingExpenseAmountSpecified
                    || this.TotalNonFirstLienMortgageDebtAmountSpecified
                    || this.TotalVerifiedReservesAmountSpecified
                    || this.VerifiedAssetsTotalAmountSpecified;
            }
        }

        /// <summary>
        /// The number of loan payments that are available to the borrower from verified financial reserves after closing. 
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOCount BorrowerReservesMonthlyPaymentCount;

        /// <summary>
        /// Gets or sets a value indicating whether the BorrowerReservesMonthlyPaymentCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BorrowerReservesMonthlyPaymentCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool BorrowerReservesMonthlyPaymentCountSpecified
        {
            get { return BorrowerReservesMonthlyPaymentCount != null; }
            set { }
        }

        /// <summary>
        /// The number of household occupants.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOCount HouseholdOccupancyCount;

        /// <summary>
        /// Gets or sets a value indicating whether the HouseholdOccupancyCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HouseholdOccupancyCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool HouseholdOccupancyCountSpecified
        {
            get { return HouseholdOccupancyCount != null; }
            set { }
        }

        /// <summary>
        /// The ratio of the proposed monthly housing expenses to the qualifying income of the borrowers as per lender or investor guidelines. AKA Front End Ratio.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOPercent HousingExpenseRatioPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the HousingExpenseRatioPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HousingExpenseRatioPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool HousingExpenseRatioPercentSpecified
        {
            get { return HousingExpenseRatioPercent != null; }
            set { }
        }

        /// <summary>
        /// Specifies whether the total monthly income is either Pre-tax or Post-tax.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOEnum<IncomeReportedBase> IncomeReportedType;

        /// <summary>
        /// Gets or sets a value indicating whether the IncomeReportedType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IncomeReportedType element has been assigned a value.</value>
        [XmlIgnore]
        public bool IncomeReportedTypeSpecified
        {
            get { return this.IncomeReportedType != null && this.IncomeReportedType.enumValue != IncomeReportedBase.Blank; }
            set { }
        }

        /// <summary>
        /// Based on investor guidelines, the projected dollar amount of remaining liquid assets after settlement. .
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOAmount ProjectedReservesAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectedReservesAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectedReservesAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectedReservesAmountSpecified
        {
            get { return ProjectedReservesAmount != null; }
            set { }
        }

        /// <summary>
        /// The PITI used to qualify the borrower for a loan, calculated using the investor or regulator specified interest rate that is higher than the initial rate.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOAmount QualifyingPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the QualifyingPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the QualifyingPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool QualifyingPaymentAmountSpecified
        {
            get { return QualifyingPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// The rate used to calculate the borrowers payment (PITI) to qualify the borrower for the subject mortgage.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOPercent QualifyingRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the QualifyingRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the QualifyingRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool QualifyingRatePercentSpecified
        {
            get { return QualifyingRatePercent != null; }
            set { }
        }

        /// <summary>
        /// Indicates what interest rate was used to qualify the borrower in correlation with the note rate.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOEnum<QualifyingRateBase> QualifyingRateType;

        /// <summary>
        /// Gets or sets a value indicating whether the QualifyingRateType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the QualifyingRateType element has been assigned a value.</value>
        [XmlIgnore]
        public bool QualifyingRateTypeSpecified
        {
            get { return this.QualifyingRateType != null && this.QualifyingRateType.enumValue != QualifyingRateBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture the Qualifying Rate type if Other is selected.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOString QualifyingRateTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the QualifyingRateTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the QualifyingRateTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool QualifyingRateTypeOtherDescriptionSpecified
        {
            get { return QualifyingRateTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The positive or negative amount of the existing or proposed cash flow on the subject property where the property is an investment.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOAmount SubjectPropertyNetCashFlowAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the SubjectPropertyNetCashFlowAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SubjectPropertyNetCashFlowAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool SubjectPropertyNetCashFlowAmountSpecified
        {
            get { return SubjectPropertyNetCashFlowAmount != null; }
            set { }
        }

        /// <summary>
        /// Percentage of the loan amount contributed by all interested parties. This may be captured on the Uniform Underwriting Transmittal Summary (1008/1077). 
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOPercent SummaryInterestedPartyContributionsPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the SummaryInterestedPartyContributionsPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SummaryInterestedPartyContributionsPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool SummaryInterestedPartyContributionsPercentSpecified
        {
            get { return SummaryInterestedPartyContributionsPercent != null; }
            set { }
        }

        /// <summary>
        /// The ratio of all monthly debt payments of the borrowers, including proposed housing expenses, to the qualifying income of the borrowers. AKA Back End Ratio.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOPercent TotalDebtExpenseRatioPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the TotalDebtExpenseRatioPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TotalDebtExpenseRatioPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool TotalDebtExpenseRatioPercentSpecified
        {
            get { return TotalDebtExpenseRatioPercent != null; }
            set { }
        }

        /// <summary>
        /// The total monthly expenses for all borrowers on the loan. Expenses Include non-housing items commonly listed as expenses of the borrower in a mortgage loan transaction.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOAmount TotalExpensesMonthlyPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the TotalExpensesMonthlyPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TotalExpensesMonthlyPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TotalExpensesMonthlyPaymentAmountSpecified
        {
            get { return TotalExpensesMonthlyPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// The total monthly liabilities for all borrowers on the loan.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOAmount TotalLiabilitiesMonthlyPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the TotalLiabilitiesMonthlyPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TotalLiabilitiesMonthlyPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TotalLiabilitiesMonthlyPaymentAmountSpecified
        {
            get { return TotalLiabilitiesMonthlyPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// The total monthly qualifying current housing expense for all borrowers on the loan.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOAmount TotalMonthlyCurrentHousingExpenseAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the TotalMonthlyCurrentHousingExpenseAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TotalMonthlyCurrentHousingExpenseAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TotalMonthlyCurrentHousingExpenseAmountSpecified
        {
            get { return TotalMonthlyCurrentHousingExpenseAmount != null; }
            set { }
        }

        /// <summary>
        /// The total monthly qualifying income for all borrowers on the loan.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOAmount TotalMonthlyIncomeAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the TotalMonthlyIncomeAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TotalMonthlyIncomeAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TotalMonthlyIncomeAmountSpecified
        {
            get { return TotalMonthlyIncomeAmount != null; }
            set { }
        }

        /// <summary>
        /// The total monthly proposed housing expense for all borrowers on the loan.
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMOAmount TotalMonthlyProposedHousingExpenseAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the TotalMonthlyProposedHousingExpenseAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TotalMonthlyProposedHousingExpenseAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TotalMonthlyProposedHousingExpenseAmountSpecified
        {
            get { return TotalMonthlyProposedHousingExpenseAmount != null; }
            set { }
        }

        /// <summary>
        /// The total current combined debt excluding first lien mortgage debt for all borrowers on the mortgage note.
        /// </summary>
        [XmlElement(Order = 17)]
        public MISMOAmount TotalNonFirstLienMortgageDebtAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the TotalNonFirstLienMortgageDebtAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TotalNonFirstLienMortgageDebtAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TotalNonFirstLienMortgageDebtAmountSpecified
        {
            get { return TotalNonFirstLienMortgageDebtAmount != null; }
            set { }
        }

        /// <summary>
        /// The actual dollar amount of remaining verified liquid assets after settlement. 
        /// </summary>
        [XmlElement(Order = 18)]
        public MISMOAmount TotalVerifiedReservesAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the TotalVerifiedReservesAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TotalVerifiedReservesAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TotalVerifiedReservesAmountSpecified
        {
            get { return TotalVerifiedReservesAmount != null; }
            set { }
        }

        /// <summary>
        /// The total amount of all assets of the borrowers  that have been verified by the lender. This may be used on the Uniform Underwriting Transmittal Summary. Calculated results field.
        /// </summary>
        [XmlElement(Order = 19)]
        public MISMOAmount VerifiedAssetsTotalAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the VerifiedAssetsTotalAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the VerifiedAssetsTotalAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool VerifiedAssetsTotalAmountSpecified
        {
            get { return VerifiedAssetsTotalAmount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 20)]
        public QUALIFICATION_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
