namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Relationship element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Relationship element has been assigned a value.</value>
    public partial class RELATIONSHIPS
    {
        /// <summary>
        /// Gets a value indicating whether the RELATIONSHIPS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.RelationshipSpecified;
            }
        }

        /// <summary>
        /// A collection of relationships.
        /// </summary>
        [XmlElement("RELATIONSHIP", Order = 0)]
		public List<RELATIONSHIP> Relationship = new List<RELATIONSHIP>();

        /// <summary>
        /// Gets or sets a value indicating whether the Relationship element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Relationship element has been assigned a value.</value>
        [XmlIgnore]
        public bool RelationshipSpecified
        {
            get { return this.Relationship != null && this.Relationship.Count(r => r != null && r.ShouldSerialize) > 0; }
            set { }
        }
    }
}
