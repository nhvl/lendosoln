namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the to identifier can be serialized.
    /// </summary>
    /// <returns>A boolean indicating whether the to identifier can be serialized.</returns>
    public partial class RELATIONSHIP
    {
        /// <summary>
        /// Gets a value indicating whether the RELATIONSHIP container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ShouldSerializearcrole()
                    || this.ShouldSerializefrom()
                    || this.ShouldSerializeto();
            }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }

        /// <summary>
        /// Gets or sets the arc role.
        /// </summary>
        [XmlAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified, Namespace = Mismo3Constants.XlinkNamespace)]
        public string arcrole;

        /// <summary>
        /// Indicates whether the arcrole can be serialized.
        /// </summary>
        /// <returns>A boolean indicating whether the arcrole can be serialized.</returns>
        public bool ShouldSerializearcrole()
        {
            return !string.IsNullOrEmpty(arcrole);
        }

        /// <summary>
        /// Gets or sets an identifier for the source data point.
        /// </summary>
        [XmlAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified, Namespace = Mismo3Constants.XlinkNamespace)]
        public string from;
        
        /// <summary>
        /// Indicates whether the from identifier can be serialized.
        /// </summary>
        /// <returns>A boolean indicating whether the from identifier can be serialized.</returns>
        public bool ShouldSerializefrom()
        {
            return !string.IsNullOrEmpty(from);
        }

        /// <summary>
        /// Gets or sets an identifier for the target data point.
        /// </summary>
        [XmlAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified, Namespace = Mismo3Constants.XlinkNamespace)]
        public string to;

        /// <summary>
        /// Indicates whether the to identifier can be serialized.
        /// </summary>
        /// <returns>A boolean indicating whether the to identifier can be serialized.</returns>
        public bool ShouldSerializeto()
        {
            return !string.IsNullOrEmpty(to);
        }
    }
}
