namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PARTIES
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PARTIES" /> class.
        /// </summary>
        public PARTIES()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PARTIES" /> class. Adds parties from the parameters to the list of parties.
        /// </summary>
        /// <param name="roles">The party to add to the newly initialized list of parties.</param>
        public PARTIES(params PARTY[] parties)
        {
            foreach (PARTY party in parties)
            {
                this.Party.Add(party);
            }
        }

        /// <summary>
        /// Gets a value indicating whether the PARTIES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.PartySpecified;
            }
        }

        /// <summary>
        /// A list of parties.
        /// </summary>
        [XmlElement("PARTY", Order = 0)]
		public List<PARTY> Party = new List<PARTY>();

        /// <summary>
        /// Gets or sets a value indicating whether the Party element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Party element has been assigned a value.</value>
        [XmlIgnore]
        public bool PartySpecified
        {
            get { return this.Party != null && this.Party.Count(p => p != null && p.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public PARTIES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
