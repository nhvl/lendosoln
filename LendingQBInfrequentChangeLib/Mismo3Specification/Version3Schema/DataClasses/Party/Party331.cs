namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PARTY_3_3_1
    {
        /// <summary>
        /// Gets a value indicating whether the PARTY_3_3_1 container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.LanguagesSpecified;
            }
        }

        /// <summary>
        /// Languages associated with this party.
        /// </summary>
        [XmlElement("LANGUAGES", Order = 0)]
        public LANGUAGES Languages;

        /// <summary>
        /// Gets or sets a value indicating whether the Languages element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Languages element has been assigned a value.</value>
        [XmlIgnore]
        public bool LanguagesSpecified
        {
            get { return this.Languages != null && this.Languages.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public PARTY_3_3_1_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
