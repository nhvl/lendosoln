namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class PARTY
    {
        /// <summary>
        /// Gets a value indicating whether the PARTY container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                if (Mismo3Utilities.ExceedsThreshold(1,
                    new List<bool> { 
                        this.ReferenceSpecified, 
                        this.AddressesSpecified
                        || this.IndividualSpecified
                        || this.LegalEntitySpecified
                        || this.PartyExtensionSpecified
                        || this.RolesSpecified
                        || this.TaxpayerIdentifiersSpecified }))
                {
                    throw new System.Xml.Schema.XmlSchemaValidationException(
                        Mismo3Utilities.GetXSDChoiceExceptionMessage(
                        "PARTY",
                        new List<string> { "REFERENCE", "Any other child element of the PARTY" }));
                }

                if (Mismo3Utilities.ExceedsThreshold(1,
                    new List<bool> { this.IndividualSpecified, this.LegalEntitySpecified }))
                {
                    throw new System.Xml.Schema.XmlSchemaValidationException(
                        Mismo3Utilities.GetXSDChoiceExceptionMessage(
                        "PARTY",
                        new List<string> { "INDIVIDUAL", "LEGAL_ENTITY" }));
                }

                return this.AddressesSpecified
                    || this.IndividualSpecified
                    || this.LegalEntitySpecified
                    || this.PartyExtensionSpecified
                    || this.ReferenceSpecified
                    || this.RolesSpecified
                    || this.TaxpayerIdentifiersSpecified;
            }
        }

        /// <summary>
        /// Any references associated with this party.
        /// </summary>
        [XmlElement("REFERENCE", Order = 0)]
        public REFERENCE Reference;

        /// <summary>
        /// Gets or sets a value indicating whether the Reference element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Reference element has been assigned a value.</value>
        [XmlIgnore]
        public bool ReferenceSpecified
        {
            get { return this.Reference != null && this.Reference.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Any individual associated with this party.
        /// </summary>
        [XmlElement("INDIVIDUAL", Order = 1)]
        public INDIVIDUAL Individual;

        /// <summary>
        /// Gets or sets a value indicating whether the Individual element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Individual element has been assigned a value.</value>
        [XmlIgnore]
        public bool IndividualSpecified
        {
            get { return this.Individual != null && this.Individual.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Any legal entity associated with this party.
        /// </summary>
        [XmlElement("LEGAL_ENTITY", Order = 2)]
        public LEGAL_ENTITY LegalEntity;

        /// <summary>
        /// Gets or sets a value indicating whether the LegalEntity element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LegalEntity element has been assigned a value.</value>
        [XmlIgnore]
        public bool LegalEntitySpecified
        {
            get { return this.LegalEntity != null && this.LegalEntity.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A list of addresses associated with this party.
        /// </summary>
        [XmlElement("ADDRESSES", Order = 3)]
        public ADDRESSES Addresses;

        /// <summary>
        /// Gets or sets a value indicating whether the Addresses element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Addresses element has been assigned a value.</value>
        [XmlIgnore]
        public bool AddressesSpecified
        {
            get { return this.Addresses != null && this.Addresses.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A list of role for this party.
        /// </summary>
        [XmlElement("ROLES", Order = 4)]
        public ROLES Roles;

        /// <summary>
        /// Gets or sets a value indicating whether the Roles element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Roles element has been assigned a value.</value>
        [XmlIgnore]
        public bool RolesSpecified
        {
            get { return this.Roles != null && this.Roles.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A list of taxpayers identifiers for this party.
        /// </summary>
        [XmlElement("TAXPAYER_IDENTIFIERS", Order = 5)]
        public TAXPAYER_IDENTIFIERS TaxpayerIdentifiers;

        /// <summary>
        /// Gets or sets a value indicating whether the TaxpayerIdentifiers element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TaxpayerIdentifiers element has been assigned a value.</value>
        [XmlIgnore]
        public bool TaxpayerIdentifiersSpecified
        {
            get { return this.TaxpayerIdentifiers != null && this.TaxpayerIdentifiers.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 6)]
        public PARTY_EXTENSION PartyExtension;

        /// <summary>
        /// Gets or sets a value indicating whether the PartyExtension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PartyExtension element has been assigned a value.</value>
        [XmlIgnore]
        public bool PartyExtensionSpecified
        {
            get { return this.PartyExtension != null && this.PartyExtension.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
