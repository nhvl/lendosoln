namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class ROOM_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the ROOM_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AboveGradeIndicatorSpecified
                    || this.ComponentAdjustmentAmountSpecified
                    || this.ConditionRatingDescriptionSpecified
                    || this.ConditionRatingTypeSpecified
                    || this.ExtensionSpecified
                    || this.LengthFeetNumberSpecified
                    || this.LevelTypeOtherDescriptionSpecified
                    || this.LevelTypeSpecified
                    || this.RoomTypeOtherDescriptionSpecified
                    || this.RoomTypeSpecified
                    || this.RoomTypeSummaryCountSpecified
                    || this.SquareFeetNumberSpecified
                    || this.WidthFeetNumberSpecified;
            }
        }

        /// <summary>
        /// Indicates the room is above grade and can be included in GLA.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIndicator AboveGradeIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the AboveGradeIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AboveGradeIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool AboveGradeIndicatorSpecified
        {
            get { return AboveGradeIndicator != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount (either positive or negative) adjustment being made for a specific component of the property.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOAmount ComponentAdjustmentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ComponentAdjustmentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ComponentAdjustmentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ComponentAdjustmentAmountSpecified
        {
            get { return ComponentAdjustmentAmount != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe in detail the condition rating of the identified component.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString ConditionRatingDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ConditionRatingDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConditionRatingDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConditionRatingDescriptionSpecified
        {
            get { return ConditionRatingDescription != null; }
            set { }
        }

        /// <summary>
        /// A rating of the condition of the identified component.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOEnum<ConditionRatingBase> ConditionRatingType;

        /// <summary>
        /// Gets or sets a value indicating whether the ConditionRatingType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConditionRatingType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConditionRatingTypeSpecified
        {
            get { return this.ConditionRatingType != null && this.ConditionRatingType.enumValue != ConditionRatingBase.Blank; }
            set { }
        }

        /// <summary>
        /// The length measured in linear feet.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMONumeric LengthFeetNumber;

        /// <summary>
        /// Gets or sets a value indicating whether the LengthFeetNumber element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LengthFeetNumber element has been assigned a value.</value>
        [XmlIgnore]
        public bool LengthFeetNumberSpecified
        {
            get { return LengthFeetNumber != null; }
            set { }
        }

        /// <summary>
        /// Specifies a particular level in the improvement of a property.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOEnum<LevelBase> LevelType;

        /// <summary>
        /// Gets or sets a value indicating whether the LevelType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LevelType element has been assigned a value.</value>
        [XmlIgnore]
        public bool LevelTypeSpecified
        {
            get { return this.LevelType != null && this.LevelType.enumValue != LevelBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the level if Other is selected as the Level Type.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOString LevelTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the LevelTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LevelTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool LevelTypeOtherDescriptionSpecified
        {
            get { return LevelTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies a type of room normally found in a living unit or residence .
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOEnum<RoomBase> RoomType;

        /// <summary>
        /// Gets or sets a value indicating whether the RoomType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RoomType element has been assigned a value.</value>
        [XmlIgnore]
        public bool RoomTypeSpecified
        {
            get { return this.RoomType != null && this.RoomType.enumValue != RoomBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the room if Other is selected as the Room Type.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOString RoomTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the RoomTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RoomTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool RoomTypeOtherDescriptionSpecified
        {
            get { return RoomTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates the number of rooms of the type specified Room Type. (e.g. 2 dining rooms, 3 bedrooms, etc. ).
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOCount RoomTypeSummaryCount;

        /// <summary>
        /// Gets or sets a value indicating whether the RoomTypeSummaryCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RoomTypeSummaryCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool RoomTypeSummaryCountSpecified
        {
            get { return RoomTypeSummaryCount != null; }
            set { }
        }

        /// <summary>
        /// Identifies the total area measured in square feet.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMONumeric SquareFeetNumber;

        /// <summary>
        /// Gets or sets a value indicating whether the SquareFeetNumber element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SquareFeetNumber element has been assigned a value.</value>
        [XmlIgnore]
        public bool SquareFeetNumberSpecified
        {
            get { return SquareFeetNumber != null; }
            set { }
        }

        /// <summary>
        /// The width measured in linear feet.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMONumeric WidthFeetNumber;

        /// <summary>
        /// Gets or sets a value indicating whether the WidthFeetNumber element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the WidthFeetNumber element has been assigned a value.</value>
        [XmlIgnore]
        public bool WidthFeetNumberSpecified
        {
            get { return WidthFeetNumber != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 12)]
        public ROOM_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
