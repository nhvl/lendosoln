namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class ROOMS
    {
        /// <summary>
        /// Gets a value indicating whether the ROOMS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.RoomSpecified;
            }
        }

        /// <summary>
        /// A collection of rooms.
        /// </summary>
        [XmlElement("ROOM", Order = 0)]
		public List<ROOM> Room = new List<ROOM>();

        /// <summary>
        /// Gets or sets a value indicating whether the Room element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Room element has been assigned a value.</value>
        [XmlIgnore]
        public bool RoomSpecified
        {
            get { return this.Room != null && this.Room.Count(r => r != null && r.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public ROOMS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
