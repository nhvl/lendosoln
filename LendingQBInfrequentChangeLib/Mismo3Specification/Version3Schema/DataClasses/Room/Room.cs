namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class ROOM
    {
        /// <summary>
        /// Gets a value indicating whether the ROOM container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.FloorCoveringSpecified
                    || this.RoomDetailSpecified
                    || this.RoomFeaturesSpecified;
            }
        }

        /// <summary>
        /// Floor coverings in a room.
        /// </summary>
        [XmlElement("FLOOR_COVERING", Order = 0)]
        public FLOOR_COVERING FloorCovering;

        /// <summary>
        /// Gets or sets a value indicating whether the FloorCovering element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FloorCovering element has been assigned a value.</value>
        [XmlIgnore]
        public bool FloorCoveringSpecified
        {
            get { return this.FloorCovering != null && this.FloorCovering.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details on a room.
        /// </summary>
        [XmlElement("ROOM_DETAIL", Order = 1)]
        public ROOM_DETAIL RoomDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the RoomDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RoomDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool RoomDetailSpecified
        {
            get { return this.RoomDetail != null && this.RoomDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A list of room features.
        /// </summary>
        [XmlElement("ROOM_FEATURES", Order = 2)]
        public ROOM_FEATURES RoomFeatures;

        /// <summary>
        /// Gets or sets a value indicating whether the RoomFeatures element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RoomFeatures element has been assigned a value.</value>
        [XmlIgnore]
        public bool RoomFeaturesSpecified
        {
            get { return this.RoomFeatures != null && this.RoomFeatures.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public ROOM_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
