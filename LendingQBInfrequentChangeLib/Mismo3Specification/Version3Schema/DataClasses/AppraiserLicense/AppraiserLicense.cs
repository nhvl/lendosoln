namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class APPRAISER_LICENSE
    {
        /// <summary>
        /// Gets a value indicating whether the APPRAISER_LICENSE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AppraiserLicenseTypeOtherDescriptionSpecified
                    || this.AppraiserLicenseTypeSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// Used to specify the different types of appraisal licenses that can be held by an appraiser.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<AppraiserLicenseBase> AppraiserLicenseType;

        /// <summary>
        /// Gets or sets a value indicating whether the AppraiserLicenseType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AppraiserLicenseType element has been assigned a value.</value>
        [XmlIgnore]
        public bool AppraiserLicenseTypeSpecified
        {
            get { return this.AppraiserLicenseType != null && this.AppraiserLicenseType.enumValue != AppraiserLicenseBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture additional information when Other is selected for Appraisal License Type.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString AppraiserLicenseTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the AppraiserLicenseTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AppraiserLicenseTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool AppraiserLicenseTypeOtherDescriptionSpecified
        {
            get { return AppraiserLicenseTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public APPRAISER_LICENSE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
