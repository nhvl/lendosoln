namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class PREPAID_ITEM
    {
        /// <summary>
        /// Gets a value indicating whether the PREPAID_ITEM container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.IdentifiedServiceProvidersSpecified
                    || this.PrepaidItemDetailSpecified
                    || this.PrepaidItemPaidToSpecified
                    || this.PrepaidItemPaymentsSpecified
                    || this.RequiredServiceProviderSpecified;
            }
        }

        /// <summary>
        /// Service providers identified, but not required, by the lender.
        /// </summary>
        [XmlElement("IDENTIFIED_SERVICE_PROVIDERS", Order = 0)]
        public IDENTIFIED_SERVICE_PROVIDERS IdentifiedServiceProviders;

        /// <summary>
        /// Gets or sets a value indicating whether the IdentifiedServiceProviders element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IdentifiedServiceProviders element has been assigned a value.</value>
        [XmlIgnore]
        public bool IdentifiedServiceProvidersSpecified
        {
            get { return this.IdentifiedServiceProviders != null && this.IdentifiedServiceProviders.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details on the prepaid item.
        /// </summary>
        [XmlElement("PREPAID_ITEM_DETAIL", Order = 1)]
        public PREPAID_ITEM_DETAIL PrepaidItemDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaidItemDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaidItemDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaidItemDetailSpecified
        {
            get { return this.PrepaidItemDetail != null && this.PrepaidItemDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The entity to whom the prepaid item is paid.
        /// </summary>
        [XmlElement("PREPAID_ITEM_PAID_TO", Order = 2)]
        public PAID_TO PrepaidItemPaidTo;

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaidItemPaidTo element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaidItemPaidTo element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaidItemPaidToSpecified
        {
            get { return this.PrepaidItemPaidTo != null && this.PrepaidItemPaidTo.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Payments on prepaid items.
        /// </summary>
        [XmlElement("PREPAID_ITEM_PAYMENTS", Order = 3)]
        public PREPAID_ITEM_PAYMENTS PrepaidItemPayments;

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaidItemPayments element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaidItemPayments element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaidItemPaymentsSpecified
        {
            get { return this.PrepaidItemPayments != null && this.PrepaidItemPayments.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A service provider required by the lender.
        /// </summary>
        [XmlElement("REQUIRED_SERVICE_PROVIDER", Order = 4)]
        public REQUIRED_SERVICE_PROVIDER RequiredServiceProvider;

        /// <summary>
        /// Gets or sets a value indicating whether the RequiredServiceProvider element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RequiredServiceProvider element has been assigned a value.</value>
        [XmlIgnore]
        public bool RequiredServiceProviderSpecified
        {
            get { return this.RequiredServiceProvider != null && this.RequiredServiceProvider.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 5)]
        public PREPAID_ITEM_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
