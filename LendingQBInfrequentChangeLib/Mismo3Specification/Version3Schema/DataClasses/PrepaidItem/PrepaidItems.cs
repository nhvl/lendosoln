namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PREPAID_ITEMS
    {
        /// <summary>
        /// Gets a value indicating whether the PREPAID_ITEMS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.PrepaidItemSpecified;
            }
        }

        /// <summary>
        /// A collection of prepaid items.
        /// </summary>
        [XmlElement("PREPAID_ITEM", Order = 0)]
        public List<PREPAID_ITEM> PrepaidItem = new List<PREPAID_ITEM>();

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaidItem element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaidItem element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaidItemSpecified
        {
            get { return this.PrepaidItem != null && this.PrepaidItem.Count(p => p != null && p.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public PREPAID_ITEMS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
