namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PREPAID_ITEM_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the PREPAID_ITEM_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BorrowerChosenProviderIndicatorSpecified
                    || this.ExtensionSpecified
                    || this.FeePaidToTypeOtherDescriptionSpecified
                    || this.FeePaidToTypeSpecified
                    || this.IntegratedDisclosureLineNumberValueSpecified
                    || this.IntegratedDisclosureSectionTypeOtherDescriptionSpecified
                    || this.IntegratedDisclosureSectionTypeSpecified
                    || this.PrepaidItemActualTotalAmountSpecified
                    || this.PrepaidItemEstimatedTotalAmountSpecified
                    || this.PrepaidItemMonthlyAmountSpecified
                    || this.PrepaidItemMonthsPaidCountSpecified
                    || this.PrepaidItemNumberOfDaysCountSpecified
                    || this.PrepaidItemPaidFromDateSpecified
                    || this.PrepaidItemPaidThroughDateSpecified
                    || this.PrepaidItemPerDiemAmountSpecified
                    || this.PrepaidItemPerDiemCalculationMethodTypeOtherDescriptionSpecified
                    || this.PrepaidItemPerDiemCalculationMethodTypeSpecified
                    || this.PrepaidItemPerDiemPrintDecimalCountSpecified
                    || this.PrepaidItemPerDiemRoundingDecimalCountSpecified
                    || this.PrepaidItemPerDiemRoundingTypeOtherDescriptionSpecified
                    || this.PrepaidItemPerDiemRoundingTypeSpecified
                    || this.PrepaidItemRefundableAmountSpecified
                    || this.PrepaidItemRefundableConditionsDescriptionSpecified
                    || this.PrepaidItemRefundableIndicatorSpecified
                    || this.PrepaidItemTypeOtherDescriptionSpecified
                    || this.PrepaidItemTypeSpecified
                    || this.RegulationZPointsAndFeesIndicatorSpecified
                    || this.RequiredProviderOfServiceIndicatorSpecified;
            }
        }

        /// <summary>
        /// When true, indicates the provider of the service for which the particular fee or payment is charged was selected by the borrower independent of the lender or lender-provided list.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIndicator BorrowerChosenProviderIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the BorrowerChosenProviderIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BorrowerChosenProviderIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool BorrowerChosenProviderIndicatorSpecified
        {
            get { return BorrowerChosenProviderIndicator != null; }
            set { }
        }

        /// <summary>
        /// Identifies the category or type of payee to which the fee or payment will be paid.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<FeePaidToBase> FeePaidToType;

        /// <summary>
        /// Gets or sets a value indicating whether the FeePaidToType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeePaidToType element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeePaidToTypeSpecified
        {
            get { return this.FeePaidToType != null && this.FeePaidToType.enumValue != FeePaidToBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field to capture the description when Other is selected as Fee Paid To Type.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString FeePaidToTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the FeePaidToTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeePaidToTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeePaidToTypeOtherDescriptionSpecified
        {
            get { return FeePaidToTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The line identifier for the item as represented in a section of the Integrated Disclosure form.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOValue IntegratedDisclosureLineNumberValue;

        /// <summary>
        /// Gets or sets a value indicating whether the IntegratedDisclosureLineNumberValue element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IntegratedDisclosureLineNumberValue element has been assigned a value.</value>
        [XmlIgnore]
        public bool IntegratedDisclosureLineNumberValueSpecified
        {
            get { return IntegratedDisclosureLineNumberValue != null; }
            set { }
        }

        /// <summary>
        /// The title or description used to identify a primary section of the integrated disclosure document.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<IntegratedDisclosureSectionBase> IntegratedDisclosureSectionType;

        /// <summary>
        /// Gets or sets a value indicating whether the IntegratedDisclosureSectionType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IntegratedDisclosureSectionType element has been assigned a value.</value>
        [XmlIgnore]
        public bool IntegratedDisclosureSectionTypeSpecified
        {
            get { return this.IntegratedDisclosureSectionType != null && this.IntegratedDisclosureSectionType.enumValue != IntegratedDisclosureSectionBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture additional information when Other is selected for Integrated Disclosure Section Type.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString IntegratedDisclosureSectionTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the IntegratedDisclosureSectionTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IntegratedDisclosureSectionTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool IntegratedDisclosureSectionTypeOtherDescriptionSpecified
        {
            get { return IntegratedDisclosureSectionTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The total actual dollar amount paid for the prepaid item by all parties.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOAmount PrepaidItemActualTotalAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaidItemActualTotalAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaidItemActualTotalAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaidItemActualTotalAmountSpecified
        {
            get { return PrepaidItemActualTotalAmount != null; }
            set { }
        }

        /// <summary>
        /// The total actual dollar amount paid for the prepaid item by all parties.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOAmount PrepaidItemEstimatedTotalAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaidItemEstimatedTotalAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaidItemEstimatedTotalAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaidItemEstimatedTotalAmountSpecified
        {
            get { return PrepaidItemEstimatedTotalAmount != null; }
            set { }
        }

        /// <summary>
        /// The monthly dollar amount charged for the prepaid item.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOAmount PrepaidItemMonthlyAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaidItemMonthlyAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaidItemMonthlyAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaidItemMonthlyAmountSpecified
        {
            get { return PrepaidItemMonthlyAmount != null; }
            set { }
        }

        /// <summary>
        /// The number of months covered by the Prepaid Item Type.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOCount PrepaidItemMonthsPaidCount;

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaidItemMonthsPaidCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaidItemMonthsPaidCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaidItemMonthsPaidCountSpecified
        {
            get { return PrepaidItemMonthsPaidCount != null; }
            set { }
        }

        /// <summary>
        /// The total number of days for which the prepaid item is to be paid.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOCount PrepaidItemNumberOfDaysCount;

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaidItemNumberOfDaysCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaidItemNumberOfDaysCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaidItemNumberOfDaysCountSpecified
        {
            get { return PrepaidItemNumberOfDaysCount != null; }
            set { }
        }

        /// <summary>
        /// The start date for calculating the total number of days covered by the prepaid item.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMODate PrepaidItemPaidFromDate;

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaidItemPaidFromDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaidItemPaidFromDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaidItemPaidFromDateSpecified
        {
            get { return PrepaidItemPaidFromDate != null; }
            set { }
        }

        /// <summary>
        /// The end date for calculating the total number of days covered by the prepaid item, inclusive of this date.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMODate PrepaidItemPaidThroughDate;

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaidItemPaidThroughDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaidItemPaidThroughDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaidItemPaidThroughDateSpecified
        {
            get { return PrepaidItemPaidThroughDate != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount charged per day for the prepaid item.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOAmount PrepaidItemPerDiemAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaidItemPerDiemAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaidItemPerDiemAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaidItemPerDiemAmountSpecified
        {
            get { return PrepaidItemPerDiemAmount != null; }
            set { }
        }

        /// <summary>
        /// The number of days in the year used in calculating the dollar amount charged per day for the prepaid item per diem amount.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOEnum<PrepaidItemPerDiemCalculationMethodBase> PrepaidItemPerDiemCalculationMethodType;

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaidItemPerDiemCalculationMethodType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaidItemPerDiemCalculationMethodType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaidItemPerDiemCalculationMethodTypeSpecified
        {
            get { return this.PrepaidItemPerDiemCalculationMethodType != null && this.PrepaidItemPerDiemCalculationMethodType.enumValue != PrepaidItemPerDiemCalculationMethodBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Prepaid Item Per Diem Calculation Method Type.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOString PrepaidItemPerDiemCalculationMethodTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaidItemPerDiemCalculationMethodTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaidItemPerDiemCalculationMethodTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaidItemPerDiemCalculationMethodTypeOtherDescriptionSpecified
        {
            get { return PrepaidItemPerDiemCalculationMethodTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Defines the number of decimals displayed or printed for the dollar amount of the amount charged per day for the prepaid item.
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMOCount PrepaidItemPerDiemPrintDecimalCount;

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaidItemPerDiemPrintDecimalCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaidItemPerDiemPrintDecimalCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaidItemPerDiemPrintDecimalCountSpecified
        {
            get { return PrepaidItemPerDiemPrintDecimalCount != null; }
            set { }
        }

        /// <summary>
        /// Defines the number of decimals for the dollar amount of the amount charged per day for the prepaid item prior to rounding. 
        /// </summary>
        [XmlElement(Order = 17)]
        public MISMOCount PrepaidItemPerDiemRoundingDecimalCount;

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaidItemPerDiemRoundingDecimalCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaidItemPerDiemRoundingDecimalCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaidItemPerDiemRoundingDecimalCountSpecified
        {
            get { return PrepaidItemPerDiemRoundingDecimalCount != null; }
            set { }
        }

        /// <summary>
        /// Defines the rounding method applied to the calculation of the dollar amount charged per day for the prepaid item.
        /// </summary>
        [XmlElement(Order = 18)]
        public MISMOEnum<PrepaidItemPerDiemRoundingBase> PrepaidItemPerDiemRoundingType;

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaidItemPerDiemRoundingType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaidItemPerDiemRoundingType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaidItemPerDiemRoundingTypeSpecified
        {
            get { return this.PrepaidItemPerDiemRoundingType != null && this.PrepaidItemPerDiemRoundingType.enumValue != PrepaidItemPerDiemRoundingBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Prepaid Item Per Diem Rounding Type.
        /// </summary>
        [XmlElement(Order = 19)]
        public MISMOString PrepaidItemPerDiemRoundingTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaidItemPerDiemRoundingTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaidItemPerDiemRoundingTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaidItemPerDiemRoundingTypeOtherDescriptionSpecified
        {
            get { return PrepaidItemPerDiemRoundingTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The amount of the prepaid item that may be refundable.
        /// </summary>
        [XmlElement(Order = 20)]
        public MISMOAmount PrepaidItemRefundableAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaidItemRefundableAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaidItemRefundableAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaidItemRefundableAmountSpecified
        {
            get { return PrepaidItemRefundableAmount != null; }
            set { }
        }

        /// <summary>
        /// A description of the conditions under which the prepaid item is refundable.
        /// </summary>
        [XmlElement(Order = 21)]
        public MISMOString PrepaidItemRefundableConditionsDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaidItemRefundableConditionsDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaidItemRefundableConditionsDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaidItemRefundableConditionsDescriptionSpecified
        {
            get { return PrepaidItemRefundableConditionsDescription != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates the prepaid item amount may be refundable under certain conditions.
        /// </summary>
        [XmlElement(Order = 22)]
        public MISMOIndicator PrepaidItemRefundableIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaidItemRefundableIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaidItemRefundableIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaidItemRefundableIndicatorSpecified
        {
            get { return PrepaidItemRefundableIndicator != null; }
            set { }
        }

        /// <summary>
        /// Identification of a monthly housing expense component that must be paid in advance.
        /// </summary>
        [XmlElement(Order = 23)]
        public MISMOEnum<PrepaidItemBase> PrepaidItemType;

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaidItemType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaidItemType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaidItemTypeSpecified
        {
            get { return this.PrepaidItemType != null && this.PrepaidItemType.enumValue != PrepaidItemBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Prepaid Item Type.
        /// </summary>
        [XmlElement(Order = 24)]
        public MISMOString PrepaidItemTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaidItemTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaidItemTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaidItemTypeOtherDescriptionSpecified
        {
            get { return PrepaidItemTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that item is included in the Regulation Z Points and Fees calculation.
        /// </summary>
        [XmlElement(Order = 25)]
        public MISMOIndicator RegulationZPointsAndFeesIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the RegulationZPointsAndFeesIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RegulationZPointsAndFeesIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool RegulationZPointsAndFeesIndicatorSpecified
        {
            get { return RegulationZPointsAndFeesIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates a required Provider of Service is associated with this fee or payment.
        /// </summary>
        [XmlElement(Order = 26)]
        public MISMOIndicator RequiredProviderOfServiceIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the RequiredProviderOfServiceIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RequiredProviderOfServiceIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool RequiredProviderOfServiceIndicatorSpecified
        {
            get { return RequiredProviderOfServiceIndicator != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 27)]
        public PREPAID_ITEM_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null && Extension.ShouldSerialize; }
            set { }
        }
    }
}
