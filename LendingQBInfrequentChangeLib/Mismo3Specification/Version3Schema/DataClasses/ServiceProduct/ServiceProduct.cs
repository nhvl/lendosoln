namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class SERVICE_PRODUCT
    {
        /// <summary>
        /// Gets a value indicating whether the SERVICE_PRODUCT container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ServiceProductRequestSpecified
                    || this.ServiceProductResponseSpecified;
            }
        }

        /// <summary>
        /// A request for a service product.
        /// </summary>
        [XmlElement("SERVICE_PRODUCT_REQUEST", Order = 0)]
        public SERVICE_PRODUCT_REQUEST ServiceProductRequest;

        /// <summary>
        /// Gets or sets a value indicating whether the ServiceProductRequest element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServiceProductRequest element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServiceProductRequestSpecified
        {
            get { return this.ServiceProductRequest != null && this.ServiceProductRequest.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Response to a request for a service product.
        /// </summary>
        [XmlElement("SERVICE_PRODUCT_RESPONSE", Order = 1)]
        public SERVICE_PRODUCT_RESPONSE ServiceProductResponse;

        /// <summary>
        /// Gets or sets a value indicating whether the ServiceProductResponse element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServiceProductResponse element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServiceProductResponseSpecified
        {
            get { return this.ServiceProductResponse != null && this.ServiceProductResponse.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public SERVICE_PRODUCT_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
