namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class SERVICE_PRODUCT_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the SERVICE_PRODUCT_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ServiceExpediteIndicatorSpecified
                    || this.ServiceNeedByDateSpecified
                    || this.ServiceProductDescriptionSpecified
                    || this.ServiceProductIdentifierSpecified;
            }
        }

        /// <summary>
        /// When true, indicates the requested service is must be completed as soon as possible.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIndicator ServiceExpediteIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ServiceExpediteIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServiceExpediteIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServiceExpediteIndicatorSpecified
        {
            get { return ServiceExpediteIndicator != null; }
            set { }
        }

        /// <summary>
        /// The date by which the requested service is needed.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMODate ServiceNeedByDate;

        /// <summary>
        /// Gets or sets a value indicating whether the ServiceNeedByDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServiceNeedByDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServiceNeedByDateSpecified
        {
            get { return ServiceNeedByDate != null; }
            set { }
        }

        /// <summary>
        /// A descriptive name for the product type, such as: Lenders Policy.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString ServiceProductDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ServiceProductDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServiceProductDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServiceProductDescriptionSpecified
        {
            get { return ServiceProductDescription != null; }
            set { }
        }

        /// <summary>
        /// A unique identifier that correlates to a specific request product type.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOIdentifier ServiceProductIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the ServiceProductIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServiceProductIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServiceProductIdentifierSpecified
        {
            get { return ServiceProductIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public SERVICE_PRODUCT_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
