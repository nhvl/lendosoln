namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class WORKOUTS
    {
        /// <summary>
        /// Gets a value indicating whether the WORKOUTS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.WorkoutSpecified
                    || this.WorkoutSummarySpecified;
            }
        }

        /// <summary>
        /// A collection of workouts.
        /// </summary>
        [XmlElement("WORKOUT", Order = 0)]
		public List<WORKOUT> Workout = new List<WORKOUT>();

        /// <summary>
        /// Gets or sets a value indicating whether the Workout element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Workout element has been assigned a value.</value>
        [XmlIgnore]
        public bool WorkoutSpecified
        {
            get { return this.Workout != null && this.Workout.Count(w => w != null && w.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// Summary of a workout set.
        /// </summary>
        [XmlElement("WORKOUT_SUMMARY", Order = 1)]
        public WORKOUT_SUMMARY WorkoutSummary;

        /// <summary>
        /// Gets or sets a value indicating whether the WorkoutSummary element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the WorkoutSummary element has been assigned a value.</value>
        [XmlIgnore]
        public bool WorkoutSummarySpecified
        {
            get { return this.WorkoutSummary != null && this.WorkoutSummary.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public WORKOUTS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
