namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class WORKOUT_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the WORKOUT_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ActiveWorkoutDocumentStatusDateSpecified
                    || this.ActiveWorkoutDocumentStatusTypeOtherDescriptionSpecified
                    || this.ActiveWorkoutDocumentStatusTypeSpecified
                    || this.BorrowerContributionReceivedIndicatorSpecified
                    || this.BorrowerContributionRequiredIndicatorSpecified
                    || this.DaysSinceEndOfPriorWorkoutCountSpecified
                    || this.DeedInLieuOfferExpirationDateSpecified
                    || this.DeedInLieuSignedDateSpecified
                    || this.DeedInLieuTransitionDurationTypeSpecified
                    || this.ExtensionSpecified
                    || this.ForbearancePaymentNextDueDateSpecified
                    || this.ForbearancePaymentTermExtensionApprovalDateSpecified
                    || this.ListingRequirementWaivedIndicatorSpecified
                    || this.MaximumBorrowerContributionAmountSpecified
                    || this.MaximumPromissoryNotePaymentAmountSpecified
                    || this.MIWorkoutApprovalConditionTypeOtherDescriptionSpecified
                    || this.MIWorkoutApprovalConditionTypeSpecified
                    || this.MIWorkoutDecisionTypeOtherDescriptionSpecified
                    || this.MIWorkoutDecisionTypeSpecified
                    || this.MIWorkoutDelegationTypeOtherDescriptionSpecified
                    || this.MIWorkoutDelegationTypeSpecified
                    || this.MIWorkoutDenialReasonTypeOtherDescriptionSpecified
                    || this.MIWorkoutDenialReasonTypeSpecified
                    || this.PaymentForbearanceTermMonthsCountSpecified
                    || this.PaymentReductionAmountSpecified
                    || this.PaymentReductionPercentSpecified
                    || this.PaymentReductionSponsorSharePercentSpecified
                    || this.PriorWorkoutSubsequentPaymentDelinquencySeverityCountSpecified
                    || this.RepaymentPlanMaximumMonthsCountSpecified
                    || this.RepaymentPlanPaymentThresholdPercentSpecified
                    || this.RepaymentPlanScheduledPaymentAmountSpecified
                    || this.SubordinateLienOfferAcceptedIndicatorSpecified
                    || this.TotalExpenseForgivenAmountSpecified
                    || this.TotalIndebtednessAmountSpecified
                    || this.TotalSubordinateLienOfferedAmountSpecified
                    || this.TotalSubordinateLienSettlementAmountSpecified
                    || this.WorkoutBorrowerCashContributionAmountSpecified
                    || this.WorkoutEffectiveDateSpecified
                    || this.WorkoutEvaluatedDatetimeSpecified
                    || this.WorkoutPriorityOrderNumberSpecified
                    || this.WorkoutProgramIdentifierSpecified
                    || this.WorkoutQualificationStatusDateSpecified
                    || this.WorkoutQualificationStatusTypeOtherDescriptionSpecified
                    || this.WorkoutQualificationStatusTypeSpecified
                    || this.WorkoutRuleVersionIdentifierSpecified
                    || this.WorkoutScheduledCompletionDateSpecified
                    || this.WorkoutSubmissionDateSpecified
                    || this.WorkoutSubtypeIdentifierSpecified
                    || this.WorkoutSuitabilityNumberSpecified
                    || this.WorkoutTermExtendedDateSpecified
                    || this.WorkoutTypeOtherDescriptionSpecified
                    || this.WorkoutTypeSpecified
                    || this.WorkoutUnsuccessfulReasonTypeOtherDescriptionSpecified
                    || this.WorkoutUnsuccessfulReasonTypeSpecified;
            }
        }

        /// <summary>
        /// Specifies the date of the document status for an active workout.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMODate ActiveWorkoutDocumentStatusDate;

        /// <summary>
        /// Gets or sets a value indicating whether the ActiveWorkoutDocumentStatusDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ActiveWorkoutDocumentStatusDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ActiveWorkoutDocumentStatusDateSpecified
        {
            get { return ActiveWorkoutDocumentStatusDate != null; }
            set { }
        }

        /// <summary>
        /// Specifies the status of the documentation for an active workout.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<ActiveWorkoutDocumentStatusBase> ActiveWorkoutDocumentStatusType;

        /// <summary>
        /// Gets or sets a value indicating whether the ActiveWorkoutDocumentStatusType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ActiveWorkoutDocumentStatusType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ActiveWorkoutDocumentStatusTypeSpecified
        {
            get { return this.ActiveWorkoutDocumentStatusType != null && this.ActiveWorkoutDocumentStatusType.enumValue != ActiveWorkoutDocumentStatusBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected as the Active Workout Document Status Type.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString ActiveWorkoutDocumentStatusTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ActiveWorkoutDocumentStatusTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ActiveWorkoutDocumentStatusTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ActiveWorkoutDocumentStatusTypeOtherDescriptionSpecified
        {
            get { return ActiveWorkoutDocumentStatusTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates the borrower contribution to the workout was received.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOIndicator BorrowerContributionReceivedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the BorrowerContributionReceivedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BorrowerContributionReceivedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool BorrowerContributionReceivedIndicatorSpecified
        {
            get { return BorrowerContributionReceivedIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates a borrower contribution was a required condition of a workout.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOIndicator BorrowerContributionRequiredIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the BorrowerContributionRequiredIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BorrowerContributionRequiredIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool BorrowerContributionRequiredIndicatorSpecified
        {
            get { return BorrowerContributionRequiredIndicator != null; }
            set { }
        }

        /// <summary>
        /// A count of the number of days since a prior workout. This is the difference between the current date and the last status date of the prior workout.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOCount DaysSinceEndOfPriorWorkoutCount;

        /// <summary>
        /// Gets or sets a value indicating whether the DaysSinceEndOfPriorWorkoutCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DaysSinceEndOfPriorWorkoutCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool DaysSinceEndOfPriorWorkoutCountSpecified
        {
            get { return DaysSinceEndOfPriorWorkoutCount != null; }
            set { }
        }

        /// <summary>
        /// The latest date that the deed in lieu agreement may be signed. The expiration date of the offer to accept a deed in lieu.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMODate DeedInLieuOfferExpirationDate;

        /// <summary>
        /// Gets or sets a value indicating whether the DeedInLieuOfferExpirationDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DeedInLieuOfferExpirationDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool DeedInLieuOfferExpirationDateSpecified
        {
            get { return DeedInLieuOfferExpirationDate != null; }
            set { }
        }

        /// <summary>
        /// The date on which the deed in lieu agreement was signed.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMODate DeedInLieuSignedDate;

        /// <summary>
        /// Gets or sets a value indicating whether the DeedInLieuSignedDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DeedInLieuSignedDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool DeedInLieuSignedDateSpecified
        {
            get { return DeedInLieuSignedDate != null; }
            set { }
        }

        /// <summary>
        /// The measure of time during which the borrower will remain in the property after a Deed In Lieu is completed.  Duration is determined based on trading party policy guidelines.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOEnum<DeedInLieuTransitionDurationBase> DeedInLieuTransitionDurationType;

        /// <summary>
        /// Gets or sets a value indicating whether the DeedInLieuTransitionDurationType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DeedInLieuTransitionDurationType element has been assigned a value.</value>
        [XmlIgnore]
        public bool DeedInLieuTransitionDurationTypeSpecified
        {
            get { return this.DeedInLieuTransitionDurationType != null && this.DeedInLieuTransitionDurationType.enumValue != DeedInLieuTransitionDurationBase.Blank; }
            set { }
        }

        /// <summary>
        /// The due date of the next  forbearance payment.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMODate ForbearancePaymentNextDueDate;

        /// <summary>
        /// Gets or sets a value indicating whether the ForbearancePaymentNextDueDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ForbearancePaymentNextDueDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ForbearancePaymentNextDueDateSpecified
        {
            get { return ForbearancePaymentNextDueDate != null; }
            set { }
        }

        /// <summary>
        /// The date on which the servicer approves extension of the forbearance term.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMODate ForbearancePaymentTermExtensionApprovalDate;

        /// <summary>
        /// Gets or sets a value indicating whether the ForbearancePaymentTermExtensionApprovalDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ForbearancePaymentTermExtensionApprovalDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ForbearancePaymentTermExtensionApprovalDateSpecified
        {
            get { return ForbearancePaymentTermExtensionApprovalDate != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that the investor has waived the 90 day property listing requirement.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOIndicator ListingRequirementWaivedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ListingRequirementWaivedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ListingRequirementWaivedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ListingRequirementWaivedIndicatorSpecified
        {
            get { return ListingRequirementWaivedIndicator != null; }
            set { }
        }

        /// <summary>
        /// The maximum amount the borrower is able to contribute to reduce investor losses for a  workout where the borrower is permitted to remain in the property during a transition period.  The borrower may make the contribution in cash or a contract.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOAmount MaximumBorrowerContributionAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the MaximumBorrowerContributionAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MaximumBorrowerContributionAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool MaximumBorrowerContributionAmountSpecified
        {
            get { return MaximumBorrowerContributionAmount != null; }
            set { }
        }

        /// <summary>
        /// The maximum promissory note payment amount the borrower can afford based on investor guidelines.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOAmount MaximumPromissoryNotePaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the MaximumPromissoryNotePaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MaximumPromissoryNotePaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool MaximumPromissoryNotePaymentAmountSpecified
        {
            get { return MaximumPromissoryNotePaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// Specifies the condition under which the MI has approved a workout.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOEnum<MIWorkoutApprovalConditionBase> MIWorkoutApprovalConditionType;

        /// <summary>
        /// Gets or sets a value indicating whether the MIWorkoutApprovalConditionType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIWorkoutApprovalConditionType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIWorkoutApprovalConditionTypeSpecified
        {
            get { return this.MIWorkoutApprovalConditionType != null && this.MIWorkoutApprovalConditionType.enumValue != MIWorkoutApprovalConditionBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected as the MI Workout Approval Condition Type.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOString MIWorkoutApprovalConditionTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MIWorkoutApprovalConditionTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIWorkoutApprovalConditionTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIWorkoutApprovalConditionTypeOtherDescriptionSpecified
        {
            get { return MIWorkoutApprovalConditionTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the decision made by the mortgage insurer on the workout.
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMOEnum<MIWorkoutDecisionBase> MIWorkoutDecisionType;

        /// <summary>
        /// Gets or sets a value indicating whether the MIWorkoutDecisionType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIWorkoutDecisionType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIWorkoutDecisionTypeSpecified
        {
            get { return this.MIWorkoutDecisionType != null && this.MIWorkoutDecisionType.enumValue != MIWorkoutDecisionBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected as the MI Workout Decision Type.
        /// </summary>
        [XmlElement(Order = 17)]
        public MISMOString MIWorkoutDecisionTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MIWorkoutDecisionTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIWorkoutDecisionTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIWorkoutDecisionTypeOtherDescriptionSpecified
        {
            get { return MIWorkoutDecisionTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The decision authority made by the MI company on the workout is delegated to another party.
        /// </summary>
        [XmlElement(Order = 18)]
        public MISMOEnum<MIWorkoutDelegationBase> MIWorkoutDelegationType;

        /// <summary>
        /// Gets or sets a value indicating whether the MIWorkoutDelegationType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIWorkoutDelegationType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIWorkoutDelegationTypeSpecified
        {
            get { return this.MIWorkoutDelegationType != null && this.MIWorkoutDelegationType.enumValue != MIWorkoutDelegationBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected as the MI Workout Delegation Type.
        /// </summary>
        [XmlElement(Order = 19)]
        public MISMOString MIWorkoutDelegationTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MIWorkoutDelegationTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIWorkoutDelegationTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIWorkoutDelegationTypeOtherDescriptionSpecified
        {
            get { return MIWorkoutDelegationTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the reason for which the MI has denied a workout.
        /// </summary>
        [XmlElement(Order = 20)]
        public MISMOEnum<MIWorkoutDenialReasonBase> MIWorkoutDenialReasonType;

        /// <summary>
        /// Gets or sets a value indicating whether the MIWorkoutDenialReasonType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIWorkoutDenialReasonType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIWorkoutDenialReasonTypeSpecified
        {
            get { return this.MIWorkoutDenialReasonType != null && this.MIWorkoutDenialReasonType.enumValue != MIWorkoutDenialReasonBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected as the MI Workout Denial Reason Type.
        /// </summary>
        [XmlElement(Order = 21)]
        public MISMOString MIWorkoutDenialReasonTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MIWorkoutDenialReasonTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIWorkoutDenialReasonTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIWorkoutDenialReasonTypeOtherDescriptionSpecified
        {
            get { return MIWorkoutDenialReasonTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The number of months in which the forbearance period will last.
        /// </summary>
        [XmlElement(Order = 22)]
        public MISMOCount PaymentForbearanceTermMonthsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the PaymentForbearanceTermMonthsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PaymentForbearanceTermMonthsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PaymentForbearanceTermMonthsCountSpecified
        {
            get { return PaymentForbearanceTermMonthsCount != null; }
            set { }
        }

        /// <summary>
        /// If the proposed workout provides a reduction in the borrower's monthly payment, the pre-workout monthly payment  minus the post-workout monthly payment.
        /// </summary>
        [XmlElement(Order = 23)]
        public MISMOAmount PaymentReductionAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PaymentReductionAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PaymentReductionAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PaymentReductionAmountSpecified
        {
            get { return PaymentReductionAmount != null; }
            set { }
        }

        /// <summary>
        /// The pre-workout monthly payment minus the post-workout monthly payment divided by the pre-workout monthly payment, expressed as a percentage.
        /// </summary>
        [XmlElement(Order = 24)]
        public MISMOPercent PaymentReductionPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the PaymentReductionPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PaymentReductionPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool PaymentReductionPercentSpecified
        {
            get { return PaymentReductionPercent != null; }
            set { }
        }

        /// <summary>
        /// The percentage of  borrower monthly payment reduction shared by the program sponsor. 
        /// </summary>
        [XmlElement(Order = 25)]
        public MISMOPercent PaymentReductionSponsorSharePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the PaymentReductionSponsorSharePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PaymentReductionSponsorSharePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool PaymentReductionSponsorSharePercentSpecified
        {
            get { return PaymentReductionSponsorSharePercent != null; }
            set { }
        }

        /// <summary>
        /// The greatest number of sequential missed payments, based on investor guidelines, within the first 12 months of the effective date or completion date of the particular prior workout type where the prior workout was completed by the borrower.
        /// </summary>
        [XmlElement(Order = 26)]
        public MISMOCount PriorWorkoutSubsequentPaymentDelinquencySeverityCount;

        /// <summary>
        /// Gets or sets a value indicating whether the PriorWorkoutSubsequentPaymentDelinquencySeverityCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PriorWorkoutSubsequentPaymentDelinquencySeverityCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PriorWorkoutSubsequentPaymentDelinquencySeverityCountSpecified
        {
            get { return PriorWorkoutSubsequentPaymentDelinquencySeverityCount != null; }
            set { }
        }

        /// <summary>
        /// The maximum length in months for the repayment plan.
        /// </summary>
        [XmlElement(Order = 27)]
        public MISMOCount RepaymentPlanMaximumMonthsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the RepaymentPlanMaximumMonthsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RepaymentPlanMaximumMonthsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool RepaymentPlanMaximumMonthsCountSpecified
        {
            get { return RepaymentPlanMaximumMonthsCount != null; }
            set { }
        }

        /// <summary>
        /// The percent of the contractual payment which the Repayment Plan Scheduled Payment Amount may not exceed.
        /// </summary>
        [XmlElement(Order = 28)]
        public MISMOPercent RepaymentPlanPaymentThresholdPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the RepaymentPlanPaymentThresholdPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RepaymentPlanPaymentThresholdPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool RepaymentPlanPaymentThresholdPercentSpecified
        {
            get { return RepaymentPlanPaymentThresholdPercent != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount that the borrower would pay to satisfy the existing delinquency when using the Repayment Plan Maximum Months Count.
        /// </summary>
        [XmlElement(Order = 29)]
        public MISMOAmount RepaymentPlanScheduledPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the RepaymentPlanScheduledPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RepaymentPlanScheduledPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool RepaymentPlanScheduledPaymentAmountSpecified
        {
            get { return RepaymentPlanScheduledPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that the subordinate lien holders have accepted the offered amount and will release the lien.
        /// </summary>
        [XmlElement(Order = 30)]
        public MISMOIndicator SubordinateLienOfferAcceptedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the SubordinateLienOfferAcceptedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SubordinateLienOfferAcceptedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool SubordinateLienOfferAcceptedIndicatorSpecified
        {
            get { return SubordinateLienOfferAcceptedIndicator != null; }
            set { }
        }

        /// <summary>
        /// The total amount of borrower expenses classified as recoverable that are being forgiven at completion of a workout.
        /// </summary>
        [XmlElement(Order = 31)]
        public MISMOAmount TotalExpenseForgivenAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the TotalExpenseForgivenAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TotalExpenseForgivenAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TotalExpenseForgivenAmountSpecified
        {
            get { return TotalExpenseForgivenAmount != null; }
            set { }
        }

        /// <summary>
        /// Total amount owed to an investor including interest-bearing and non-interest bearing UPB, accrued interest, escrow advances, and allowable expenses as of the transaction date.
        /// </summary>
        [XmlElement(Order = 32)]
        public MISMOAmount TotalIndebtednessAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the TotalIndebtednessAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TotalIndebtednessAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TotalIndebtednessAmountSpecified
        {
            get { return TotalIndebtednessAmount != null; }
            set { }
        }

        /// <summary>
        /// The total dollar amount offered by the investor to subordinate lien holders for release of the lien.
        /// </summary>
        [XmlElement(Order = 33)]
        public MISMOAmount TotalSubordinateLienOfferedAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the TotalSubordinateLienOfferedAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TotalSubordinateLienOfferedAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TotalSubordinateLienOfferedAmountSpecified
        {
            get { return TotalSubordinateLienOfferedAmount != null; }
            set { }
        }

        /// <summary>
        /// The total negotiated dollar amount to settle all subordinate liens being released excluding the subject mortgage loan amount.
        /// </summary>
        [XmlElement(Order = 34)]
        public MISMOAmount TotalSubordinateLienSettlementAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the TotalSubordinateLienSettlementAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TotalSubordinateLienSettlementAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TotalSubordinateLienSettlementAmountSpecified
        {
            get { return TotalSubordinateLienSettlementAmount != null; }
            set { }
        }

        /// <summary>
        /// The amount of cash the borrower is able to contribute to the workout transaction.
        /// </summary>
        [XmlElement(Order = 35)]
        public MISMOAmount WorkoutBorrowerCashContributionAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the WorkoutBorrowerCashContributionAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the WorkoutBorrowerCashContributionAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool WorkoutBorrowerCashContributionAmountSpecified
        {
            get { return WorkoutBorrowerCashContributionAmount != null; }
            set { }
        }

        /// <summary>
        /// The date on which the workout agreement goes into effect.
        /// </summary>
        [XmlElement(Order = 36)]
        public MISMODate WorkoutEffectiveDate;

        /// <summary>
        /// Gets or sets a value indicating whether the WorkoutEffectiveDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the WorkoutEffectiveDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool WorkoutEffectiveDateSpecified
        {
            get { return WorkoutEffectiveDate != null; }
            set { }
        }

        /// <summary>
        /// The date and time that the workout was evaluated.
        /// </summary>
        [XmlElement(Order = 37)]
        public MISMODatetime WorkoutEvaluatedDatetime;

        /// <summary>
        /// Gets or sets a value indicating whether the date/time element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the date/time element has been assigned a value.</value>
        [XmlIgnore]
        public bool WorkoutEvaluatedDatetimeSpecified
        {
            get { return WorkoutEvaluatedDatetime != null; }
            set { }
        }

        /// <summary>
        /// Order in which the workout types should be considered by the servicer.
        /// </summary>
        [XmlElement(Order = 38)]
        public MISMONumeric WorkoutPriorityOrderNumber;

        /// <summary>
        /// Gets or sets a value indicating whether the WorkoutPriorityOrderNumber element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the WorkoutPriorityOrderNumber element has been assigned a value.</value>
        [XmlIgnore]
        public bool WorkoutPriorityOrderNumberSpecified
        {
            get { return WorkoutPriorityOrderNumber != null; }
            set { }
        }

        /// <summary>
        /// Identifies the program associated with the workout.
        /// </summary>
        [XmlElement(Order = 39)]
        public MISMOIdentifier WorkoutProgramIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the WorkoutProgramIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the WorkoutProgramIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool WorkoutProgramIdentifierSpecified
        {
            get { return WorkoutProgramIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The date associated with the specified Workout Qualification Status Type.
        /// </summary>
        [XmlElement(Order = 40)]
        public MISMODate WorkoutQualificationStatusDate;

        /// <summary>
        /// Gets or sets a value indicating whether the WorkoutQualificationStatusDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the WorkoutQualificationStatusDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool WorkoutQualificationStatusDateSpecified
        {
            get { return WorkoutQualificationStatusDate != null; }
            set { }
        }

        /// <summary>
        /// Specifies the qualification status of the workout.
        /// </summary>
        [XmlElement(Order = 41)]
        public MISMOEnum<WorkoutQualificationStatusBase> WorkoutQualificationStatusType;

        /// <summary>
        /// Gets or sets a value indicating whether the WorkoutQualificationStatusType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the WorkoutQualificationStatusType element has been assigned a value.</value>
        [XmlIgnore]
        public bool WorkoutQualificationStatusTypeSpecified
        {
            get { return this.WorkoutQualificationStatusType != null && this.WorkoutQualificationStatusType.enumValue != WorkoutQualificationStatusBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected as the Workout  Qualification Status Type.
        /// </summary>
        [XmlElement(Order = 42)]
        public MISMOString WorkoutQualificationStatusTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the WorkoutQualificationStatusTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the WorkoutQualificationStatusTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool WorkoutQualificationStatusTypeOtherDescriptionSpecified
        {
            get { return WorkoutQualificationStatusTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A unique identifier of the version of the workout rules used in a workout evaluation.
        /// </summary>
        [XmlElement(Order = 43)]
        public MISMOIdentifier WorkoutRuleVersionIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the WorkoutRuleVersionIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the WorkoutRuleVersionIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool WorkoutRuleVersionIdentifierSpecified
        {
            get { return WorkoutRuleVersionIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The date on which the workout is scheduled to be completed.
        /// </summary>
        [XmlElement(Order = 44)]
        public MISMODate WorkoutScheduledCompletionDate;

        /// <summary>
        /// Gets or sets a value indicating whether the WorkoutScheduledCompletionDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the WorkoutScheduledCompletionDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool WorkoutScheduledCompletionDateSpecified
        {
            get { return WorkoutScheduledCompletionDate != null; }
            set { }
        }

        /// <summary>
        /// The date on which the most recent workout was submitted.
        /// </summary>
        [XmlElement(Order = 45)]
        public MISMODate WorkoutSubmissionDate;

        /// <summary>
        /// Gets or sets a value indicating whether the WorkoutSubmissionDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the WorkoutSubmissionDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool WorkoutSubmissionDateSpecified
        {
            get { return WorkoutSubmissionDate != null; }
            set { }
        }

        /// <summary>
        /// A unique identifier of the investor defined subtype of workout category that is under evaluation. 
        /// </summary>
        [XmlElement(Order = 46)]
        public MISMOIdentifier WorkoutSubtypeIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the WorkoutSubtypeIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the WorkoutSubtypeIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool WorkoutSubtypeIdentifierSpecified
        {
            get { return WorkoutSubtypeIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Specifies a hierarchy of suitability of all workouts offered to the borrower based on eligibility of the borrower.
        /// </summary>
        [XmlElement(Order = 47)]
        public MISMONumeric WorkoutSuitabilityNumber;

        /// <summary>
        /// Gets or sets a value indicating whether the WorkoutSuitabilityNumber element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the WorkoutSuitabilityNumber element has been assigned a value.</value>
        [XmlIgnore]
        public bool WorkoutSuitabilityNumberSpecified
        {
            get { return WorkoutSuitabilityNumber != null; }
            set { }
        }

        /// <summary>
        /// The date to which the term of the workout was extended.
        /// </summary>
        [XmlElement(Order = 48)]
        public MISMODate WorkoutTermExtendedDate;

        /// <summary>
        /// Gets or sets a value indicating whether the WorkoutTermExtendedDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the WorkoutTermExtendedDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool WorkoutTermExtendedDateSpecified
        {
            get { return WorkoutTermExtendedDate != null; }
            set { }
        }

        /// <summary>
        /// The method used to facilitate the workout.
        /// </summary>
        [XmlElement(Order = 49)]
        public MISMOEnum<WorkoutBase> WorkoutType;

        /// <summary>
        /// Gets or sets a value indicating whether the WorkoutType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the WorkoutType element has been assigned a value.</value>
        [XmlIgnore]
        public bool WorkoutTypeSpecified
        {
            get { return this.WorkoutType != null && this.WorkoutType.enumValue != WorkoutBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Workout Type.
        /// </summary>
        [XmlElement(Order = 50)]
        public MISMOString WorkoutTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the WorkoutTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the WorkoutTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool WorkoutTypeOtherDescriptionSpecified
        {
            get { return WorkoutTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the reason that a workout was not successful.
        /// </summary>
        [XmlElement(Order = 51)]
        public MISMOEnum<WorkoutUnsuccessfulReasonBase> WorkoutUnsuccessfulReasonType;

        /// <summary>
        /// Gets or sets a value indicating whether the WorkoutUnsuccessfulReasonType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the WorkoutUnsuccessfulReasonType element has been assigned a value.</value>
        [XmlIgnore]
        public bool WorkoutUnsuccessfulReasonTypeSpecified
        {
            get { return this.WorkoutUnsuccessfulReasonType != null && this.WorkoutUnsuccessfulReasonType.enumValue != WorkoutUnsuccessfulReasonBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected as the Workout Unsuccessful Reason Type.
        /// </summary>
        [XmlElement(Order = 52)]
        public MISMOString WorkoutUnsuccessfulReasonTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the WorkoutUnsuccessfulReasonTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the WorkoutUnsuccessfulReasonTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool WorkoutUnsuccessfulReasonTypeOtherDescriptionSpecified
        {
            get { return WorkoutUnsuccessfulReasonTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 53)]
        public WORKOUT_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
