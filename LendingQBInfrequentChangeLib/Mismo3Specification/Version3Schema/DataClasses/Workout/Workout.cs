namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class WORKOUT
    {
        /// <summary>
        /// Gets a value indicating whether the WORKOUT container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AssistancesSpecified
                    || this.ContributionsSpecified
                    || this.ErrorsSpecified
                    || this.ExtensionSpecified
                    || this.RepaymentPlansSpecified
                    || this.SolicitationsSpecified
                    || this.TrialSpecified
                    || this.WorkoutCommentsSpecified
                    || this.WorkoutDetailSpecified
                    || this.WorkoutStatusesSpecified;
            }
        }

        /// <summary>
        /// Assistances related to a workout.
        /// </summary>
        [XmlElement("ASSISTANCES", Order = 0)]
        public ASSISTANCES Assistances;

        /// <summary>
        /// Gets or sets a value indicating whether the Assistances element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Assistances element has been assigned a value.</value>
        [XmlIgnore]
        public bool AssistancesSpecified
        {
            get { return this.Assistances != null && this.Assistances.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Contributions related to a workout.
        /// </summary>
        [XmlElement("CONTRIBUTIONS", Order = 1)]
        public CONTRIBUTIONS Contributions;

        /// <summary>
        /// Gets or sets a value indicating whether the Contributions element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Contributions element has been assigned a value.</value>
        [XmlIgnore]
        public bool ContributionsSpecified
        {
            get { return this.Contributions != null && this.Contributions.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Errors related to a workout.
        /// </summary>
        [XmlElement("ERRORS", Order = 2)]
        public ERRORS Errors;

        /// <summary>
        /// Gets or sets a value indicating whether the Errors element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Errors element has been assigned a value.</value>
        [XmlIgnore]
        public bool ErrorsSpecified
        {
            get { return this.Errors != null && this.Errors.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Repayment plans related to a workout.
        /// </summary>
        [XmlElement("REPAYMENT_PLANS", Order = 3)]
        public REPAYMENT_PLANS RepaymentPlans;

        /// <summary>
        /// Gets or sets a value indicating whether the RepaymentPlans element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RepaymentPlans element has been assigned a value.</value>
        [XmlIgnore]
        public bool RepaymentPlansSpecified
        {
            get { return this.RepaymentPlans != null && this.RepaymentPlans.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Solicitations related to a workout.
        /// </summary>
        [XmlElement("SOLICITATIONS", Order = 4)]
        public SOLICITATIONS Solicitations;

        /// <summary>
        /// Gets or sets a value indicating whether the Solicitations element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Solicitations element has been assigned a value.</value>
        [XmlIgnore]
        public bool SolicitationsSpecified
        {
            get { return this.Solicitations != null && this.Solicitations.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Trial related to a workout.
        /// </summary>
        [XmlElement("TRIAL", Order = 5)]
        public TRIAL Trial;

        /// <summary>
        /// Gets or sets a value indicating whether the Trial element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Trial element has been assigned a value.</value>
        [XmlIgnore]
        public bool TrialSpecified
        {
            get { return this.Trial != null && this.Trial.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Comments about a workout.
        /// </summary>
        [XmlElement("WORKOUT_COMMENTS", Order = 6)]
        public WORKOUT_COMMENTS WorkoutComments;

        /// <summary>
        /// Gets or sets a value indicating whether the WorkoutComments element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the WorkoutComments element has been assigned a value.</value>
        [XmlIgnore]
        public bool WorkoutCommentsSpecified
        {
            get { return this.WorkoutComments != null && this.WorkoutComments.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details about a workout.
        /// </summary>
        [XmlElement("WORKOUT_DETAIL", Order = 7)]
        public WORKOUT_DETAIL WorkoutDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the WorkoutDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the WorkoutDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool WorkoutDetailSpecified
        {
            get { return this.WorkoutDetail != null && this.WorkoutDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Status of a workout.
        /// </summary>
        [XmlElement("WORKOUT_STATUSES", Order = 8)]
        public WORKOUT_STATUSES WorkoutStatuses;

        /// <summary>
        /// Gets or sets a value indicating whether the WorkoutStatuses element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the WorkoutStatuses element has been assigned a value.</value>
        [XmlIgnore]
        public bool WorkoutStatusesSpecified
        {
            get { return this.WorkoutStatuses != null && this.WorkoutStatuses.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 9)]
        public WORKOUT_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
