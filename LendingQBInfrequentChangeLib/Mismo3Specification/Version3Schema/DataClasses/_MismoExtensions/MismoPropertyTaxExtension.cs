﻿namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    public class MISMO_PROPERTY_TAX_EXTENSION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.EscrowItemDisbursementsSpecified;
            }
        }

        [XmlElement("ESCROW_ITEM_DISBURSEMENTS", Order = 0)]
        public ESCROW_ITEM_DISBURSEMENTS EscrowItemDisbursements;

        [XmlIgnore]
        public bool EscrowItemDisbursementsSpecified
        {
            get { return this.EscrowItemDisbursements != null && this.EscrowItemDisbursements.ShouldSerialize; }
            set { }
        }
    }
}