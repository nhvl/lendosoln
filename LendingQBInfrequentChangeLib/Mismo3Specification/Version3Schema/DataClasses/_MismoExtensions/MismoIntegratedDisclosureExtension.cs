namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the TermsOfLoan element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the TermsOfLoan element has been assigned a value.</value>
    public partial class MISMO_INTEGRATED_DISCLOSURE_EXTENSION
    {
        /// <summary>
        /// Gets a value indicating whether the MISMO_INTEGRATED_DISCLOSURE_EXTENSION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AdjustmentSpecified
                    || this.FeeInformationSpecified
                    || this.InterestOnlySpecified
                    || this.PaymentSpecified
                    || this.TermsOfLoanSpecified;
            }
        }

        /// <summary>
        /// An adjustment to the loan.
        /// </summary>
        [XmlElement("ADJUSTMENT", Order = 0)]
        public ADJUSTMENT Adjustment;

        /// <summary>
        /// Gets or sets a value indicating whether the Adjustment element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Adjustment element has been assigned a value.</value>
        [XmlIgnore]
        public bool AdjustmentSpecified
        {
            get { return this.Adjustment != null && this.Adjustment.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Fees associated with the integrated disclosure.
        /// </summary>
        [XmlElement("FEE_INFORMATION", Order = 1)]
        public FEE_INFORMATION FeeInformation;

        /// <summary>
        /// Gets or sets a value indicating whether the FeeInformation element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeeInformation element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeeInformationSpecified
        {
            get { return this.FeeInformation != null && this.FeeInformation.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The interest-only option for the loan.
        /// </summary>
        [XmlElement("INTEREST_ONLY", Order = 2)]
        public INTEREST_ONLY InterestOnly;

        /// <summary>
        /// Gets or sets a value indicating whether the InterestOnly element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InterestOnly element has been assigned a value.</value>
        [XmlIgnore]
        public bool InterestOnlySpecified
        {
            get { return this.InterestOnly != null && this.InterestOnly.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Information on the monthly payment.
        /// </summary>
        [XmlElement("PAYMENT", Order = 3)]
        public PAYMENT Payment;

        /// <summary>
        /// Gets or sets a value indicating whether the Payment element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Payment element has been assigned a value.</value>
        [XmlIgnore]
        public bool PaymentSpecified
        {
            get { return this.Payment != null && this.Payment.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The terms of this loan.
        /// </summary>
        [XmlElement("TERMS_OF_LOAN", Order = 4)]
        public TERMS_OF_LOAN TermsOfLoan;

        /// <summary>
        /// Gets or sets a value indicating whether the TermsOfLoan element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TermsOfLoan element has been assigned a value.</value>
        [XmlIgnore]
        public bool TermsOfLoanSpecified
        {
            get { return this.TermsOfLoan != null && this.TermsOfLoan.ShouldSerialize; }
            set { }
        }
    }
}
