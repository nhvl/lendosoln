﻿namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A MISMO extension to the CONSTRUCTION element.
    /// </summary>
    public class MISMO_CONSTRUCTION_EXTENSION
    {
        /// <summary>
        /// Gets a value indicating whether the element should be serialized.
        /// </summary>
        public bool ShouldSerialize
        {
            get
            {
                return this.AdjustmentSpecified;
            }
        }

        /// <summary>
        /// A MISMO adjustment element.
        /// </summary>
        [XmlElement("Adjustment", Order = 0, Namespace = Mismo3Constants.MismoNamespace)]
        public ADJUSTMENT Adjustment;

        /// <summary>
        /// Gets a value indicating whether the <see cref="Adjustment"/> element has data to serialize.
        /// </summary>
        [XmlIgnore]
        public bool AdjustmentSpecified
        {
            get { return this.Adjustment != null && this.Adjustment.ShouldSerialize; }
            set { }
        }
    }
}
