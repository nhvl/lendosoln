namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CONVERSION_ADJUSTMENT_LIFETIME_ADJUSTMENT_RULE
    {
        /// <summary>
        /// Gets a value indicating whether the CONVERSION_ADJUSTMENT_LIFETIME_ADJUSTMENT_RULE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ConversionDateMaximumExtensionMonthsCountSpecified
                    || this.ConversionMaximumAllowedCountSpecified
                    || this.ConversionOptionDurationMonthsCountSpecified
                    || this.ConversionOptionMarginRatePercentSpecified
                    || this.ConversionOptionMaximumRatePercentSpecified
                    || this.ConversionOptionMinimumRatePercentSpecified
                    || this.ConversionOptionNoteTermGreaterThanFifteenYearsAdditionalPercentSpecified
                    || this.ConversionOptionNoteTermLessThanFifteenYearsAdditionalPercentSpecified
                    || this.ConversionScheduleTypeOtherDescriptionSpecified
                    || this.ConversionScheduleTypeSpecified
                    || this.ConversionTypeOtherDescriptionSpecified
                    || this.ConversionTypeSpecified
                    || this.ExtensionSpecified;
            }
        }


        /// <summary>
        /// The maximum number of months by which a loan can extend its conversion date.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOCount ConversionDateMaximumExtensionMonthsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the ConversionDateMaximumExtensionMonthsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConversionDateMaximumExtensionMonthsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConversionDateMaximumExtensionMonthsCountSpecified
        {
            get { return ConversionDateMaximumExtensionMonthsCount != null; }
            set { }
        }

        /// <summary>
        /// The maximum number of times a borrower can exercise conversion options.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOCount ConversionMaximumAllowedCount;

        /// <summary>
        /// Gets or sets a value indicating whether the ConversionMaximumAllowedCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConversionMaximumAllowedCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConversionMaximumAllowedCountSpecified
        {
            get { return ConversionMaximumAllowedCount != null; }
            set { }
        }

        /// <summary>
        /// The duration (in months) of the conversion option.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOCount ConversionOptionDurationMonthsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the ConversionOptionDurationMonthsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConversionOptionDurationMonthsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConversionOptionDurationMonthsCountSpecified
        {
            get { return ConversionOptionDurationMonthsCount != null; }
            set { }
        }

        /// <summary>
        /// The number of percentage points to be added to the index when calculating a new interest rate for the option to convert an ARM loan to a fixed rate loan.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOPercent ConversionOptionMarginRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the ConversionOptionMarginRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConversionOptionMarginRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConversionOptionMarginRatePercentSpecified
        {
            get { return ConversionOptionMarginRatePercent != null; }
            set { }
        }

        /// <summary>
        /// The highest rate, expressed as a percent, to which the interest rate can increase when calculating a new interest rate for the option to convert an ARM loan to a fixed rate loan.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOPercent ConversionOptionMaximumRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the ConversionOptionMaximumRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConversionOptionMaximumRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConversionOptionMaximumRatePercentSpecified
        {
            get { return ConversionOptionMaximumRatePercent != null; }
            set { }
        }

        /// <summary>
        /// The lowest rate, expressed as a percent, to which the interest rate can decrease when calculating a new interest rate for the option to convert an ARM loan to a fixed rate loan.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOPercent ConversionOptionMinimumRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the ConversionOptionMinimumRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConversionOptionMinimumRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConversionOptionMinimumRatePercentSpecified
        {
            get { return ConversionOptionMinimumRatePercent != null; }
            set { }
        }

        /// <summary>
        /// The number of percentage points added to the net yield to obtain the new fixed interest rate when the original term of the Note is greater than 15 years.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOPercent ConversionOptionNoteTermGreaterThanFifteenYearsAdditionalPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the ConversionOptionNoteTermGreaterThanFifteenYearsAdditionalPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConversionOptionNoteTermGreaterThanFifteenYearsAdditionalPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConversionOptionNoteTermGreaterThanFifteenYearsAdditionalPercentSpecified
        {
            get { return ConversionOptionNoteTermGreaterThanFifteenYearsAdditionalPercent != null; }
            set { }
        }

        /// <summary>
        /// The percent added to the net yield to obtain the new fixed interest rate when the original term of the Note is less than 15 years.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOPercent ConversionOptionNoteTermLessThanFifteenYearsAdditionalPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the ConversionOptionNoteTermLessThanFifteenYearsAdditionalPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConversionOptionNoteTermLessThanFifteenYearsAdditionalPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConversionOptionNoteTermLessThanFifteenYearsAdditionalPercentSpecified
        {
            get { return ConversionOptionNoteTermLessThanFifteenYearsAdditionalPercent != null; }
            set { }
        }

        /// <summary>
        /// Describes when the loan is eligible to convert.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOEnum<ConversionScheduleBase> ConversionScheduleType;

        /// <summary>
        /// Gets or sets a value indicating whether the ConversionScheduleType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConversionScheduleType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConversionScheduleTypeSpecified
        {
            get { return this.ConversionScheduleType != null && this.ConversionScheduleType.enumValue != ConversionScheduleBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Conversion Schedule Type.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOString ConversionScheduleTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ConversionScheduleTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConversionScheduleTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConversionScheduleTypeOtherDescriptionSpecified
        {
            get { return ConversionScheduleTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the type of conversion permissible for a loan as stated on the mortgage documents.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOEnum<ConversionBase> ConversionType;

        /// <summary>
        /// Gets or sets a value indicating whether the ConversionType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConversionType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConversionTypeSpecified
        {
            get { return this.ConversionType != null && this.ConversionType.enumValue != ConversionBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Conversion Type.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOString ConversionTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ConversionTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConversionTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConversionTypeOtherDescriptionSpecified
        {
            get { return ConversionTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 12)]
        public CONVERSION_ADJUSTMENT_LIFETIME_ADJUSTMENT_RULE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
