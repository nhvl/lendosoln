namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class APPROACH_TO_VALUE
    {
        /// <summary>
        /// Gets a value indicating whether the APPROACH_TO_VALUE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CostApproachSpecified
                    || this.IncomeApproachSpecified
                    || this.SalesComparisonApproachSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// An approach using cost.
        /// </summary>
        [XmlElement("COST_APPROACH", Order = 0)]
        public COST_APPROACH CostApproach;

        /// <summary>
        /// Gets or sets a value indicating whether the CostApproach element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CostApproach element has been assigned a value.</value>
        [XmlIgnore]
        public bool CostApproachSpecified
        {
            get { return CostApproach != null && CostApproach.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// An approach using income.
        /// </summary>
        [XmlElement("INCOME_APPROACH", Order = 1)]
        public INCOME_APPROACH IncomeApproach;

        /// <summary>
        /// Gets or sets a value indicating whether the IncomeApproach element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IncomeApproach element has been assigned a value.</value>
        [XmlIgnore]
        public bool IncomeApproachSpecified
        {
            get { return IncomeApproach != null && IncomeApproach.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// An approach using sales comparison.
        /// </summary>
        [XmlElement("SALES_COMPARISON_APPROACH", Order = 2)]
        public SALES_COMPARISON_APPROACH SalesComparisonApproach;

        /// <summary>
        /// Gets or sets a value indicating whether the SalesComparisonApproach element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SalesComparisonApproach element has been assigned a value.</value>
        [XmlIgnore]
        public bool SalesComparisonApproachSpecified
        {
            get { return SalesComparisonApproach != null && SalesComparisonApproach.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public APPROACH_TO_VALUE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
