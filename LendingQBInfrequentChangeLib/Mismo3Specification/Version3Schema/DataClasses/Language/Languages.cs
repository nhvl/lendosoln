namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class LANGUAGES
    {
        /// <summary>
        /// Gets a value indicating whether the LANGUAGES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.LanguageSpecified;
            }
        }

        /// <summary>
        /// A list of languages associated with a party.
        /// </summary>
        [XmlElement("LANGUAGE", Order = 0)]
		public List<LANGUAGE> Language = new List<LANGUAGE>();

        /// <summary>
        /// Gets or sets a value indicating whether the Language element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Language element has been assigned a value.</value>
        [XmlIgnore]
        public bool LanguageSpecified
        {
            get { return this.Language != null && this.Language.Count(l => l != null && l.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public LANGUAGES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
