namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class DOCUMENT_FORM_CONTRIBUTORS
    {
        /// <summary>
        /// Gets a value indicating whether the DOCUMENT_FORM_CONTRIBUTORS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DocumentFormContributorSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of document form contributors.
        /// </summary>
        [XmlElement("DOCUMENT_FORM_CONTRIBUTOR", Order = 0)]
		public List<DOCUMENT_FORM_CONTRIBUTOR> DocumentFormContributor = new List<DOCUMENT_FORM_CONTRIBUTOR>();

        /// <summary>
        /// Gets or sets a value indicating whether the DocumentFormContributor element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DocumentFormContributor element has been assigned a value.</value>
        [XmlIgnore]
        public bool DocumentFormContributorSpecified
        {
            get { return this.DocumentFormContributor != null && this.DocumentFormContributor.Count(d => d != null && d.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public DOCUMENT_FORM_CONTRIBUTORS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
