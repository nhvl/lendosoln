namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class DOCUMENT_FORM_CONTRIBUTOR
    {
        /// <summary>
        /// Gets a value indicating whether the DOCUMENT_FORM_CONTRIBUTOR container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DocumentFormContributorCodeSpecified
                    || this.DocumentFormContributorDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A code provided by agreement between trading partners to identify the form/template contributor. The CodeOwnerURI attribute is used to disambiguate, for example between two cities having the same name but located in different states.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOCode DocumentFormContributorCode;

        /// <summary>
        /// Gets or sets a value indicating whether the DocumentFormContributorCode element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DocumentFormContributorCode element has been assigned a value.</value>
        [XmlIgnore]
        public bool DocumentFormContributorCodeSpecified
        {
            get { return DocumentFormContributorCode != null; }
            set { }
        }

        /// <summary>
        /// A free-form text description of the form/template contributor as identified by the Document Form Contributor Code.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString DocumentFormContributorDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the DocumentFormContributorDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DocumentFormContributorDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool DocumentFormContributorDescriptionSpecified
        {
            get { return DocumentFormContributorDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public DOCUMENT_FORM_CONTRIBUTOR_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
