namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class GREEN_BUILDING_CERTIFICATION
    {
        /// <summary>
        /// Gets a value indicating whether the GREEN_BUILDING_CERTIFICATION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.GreenCertificationAssociationNameSpecified
                    || this.GreenCertificationLevelAwardedDateSpecified
                    || this.GreenCertificationLevelNameSpecified
                    || this.GreenCertificationLevelScoreValueSpecified;
            }
        }

        /// <summary>
        /// The name of the association or program providing the Green certification such as National Association of Homebuilders or LEED.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString GreenCertificationAssociationName;

        /// <summary>
        /// Gets or sets a value indicating whether the GreenCertificationAssociationName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GreenCertificationAssociationName element has been assigned a value.</value>
        [XmlIgnore]
        public bool GreenCertificationAssociationNameSpecified
        {
            get { return GreenCertificationAssociationName != null; }
            set { }
        }

        /// <summary>
        /// The date the Green program certification was obtained for this property. 
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMODate GreenCertificationLevelAwardedDate;

        /// <summary>
        /// Gets or sets a value indicating whether the GreenCertificationLevelAwardedDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GreenCertificationLevelAwardedDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool GreenCertificationLevelAwardedDateSpecified
        {
            get { return GreenCertificationLevelAwardedDate != null; }
            set { }
        }

        /// <summary>
        /// The level or rating from the indicated Green program that was certified for this property.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString GreenCertificationLevelName;

        /// <summary>
        /// Gets or sets a value indicating whether the GreenCertificationLevelName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GreenCertificationLevelName element has been assigned a value.</value>
        [XmlIgnore]
        public bool GreenCertificationLevelNameSpecified
        {
            get { return GreenCertificationLevelName != null; }
            set { }
        }

        /// <summary>
        /// Value of the green certification level.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOValue GreenCertificationLevelScoreValue;

        /// <summary>
        /// Gets or sets a value indicating whether the GreenCertificationLevelScoreNumValue element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GreenCertificationLevelScoreNumValue element has been assigned a value.</value>
        [XmlIgnore]
        public bool GreenCertificationLevelScoreValueSpecified
        {
            get { return GreenCertificationLevelScoreValue != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public GREEN_BUILDING_CERTIFICATION_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
