namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class GREEN_BUILDING_CERTIFICATIONS
    {
        /// <summary>
        /// Gets a value indicating whether the GREEN_BUILDING_CERTIFICATIONS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.GreenBuildingCertificationSpecified;
            }
        }

        /// <summary>
        /// A collection of green building certifications.
        /// </summary>
        [XmlElement("GREEN_BUILDING_CERTIFICATION", Order = 0)]
		public List<GREEN_BUILDING_CERTIFICATION> GreenBuildingCertification = new List<GREEN_BUILDING_CERTIFICATION>();

        /// <summary>
        /// Gets or sets a value indicating whether the GreenBuildingCertification element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GreenBuildingCertification element has been assigned a value.</value>
        [XmlIgnore]
        public bool GreenBuildingCertificationSpecified
        {
            get { return this.GreenBuildingCertification != null && this.GreenBuildingCertification.Count(g => g != null && g.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public GREEN_BUILDING_CERTIFICATIONS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
