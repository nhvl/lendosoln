namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class GOVERNMENT_BORROWER
    {
        /// <summary>
        /// Gets a value indicating whether the container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CAIVRSIdentifierSpecified
                    || this.ExtensionSpecified
                    || this.FHA_VABorrowerCertificationSalesPriceExceedsAppraisedValueTypeSpecified
                    || this.FHABorrowerCertificationLeadPaintIndicatorSpecified
                    || this.FHABorrowerCertificationOriginalMortgageAmountSpecified
                    || this.FHABorrowerCertificationOwnFourOrMoreDwellingsIndicatorSpecified
                    || this.FHABorrowerCertificationOwnOtherPropertyIndicatorSpecified
                    || this.FHABorrowerCertificationPropertySoldCityNameSpecified
                    || this.FHABorrowerCertificationPropertySoldPostalCodeSpecified
                    || this.FHABorrowerCertificationPropertySoldStateNameSpecified
                    || this.FHABorrowerCertificationPropertySoldStreetAddressLineTextSpecified
                    || this.FHABorrowerCertificationPropertyToBeSoldIndicatorSpecified
                    || this.FHABorrowerCertificationRentalIndicatorSpecified
                    || this.FHABorrowerCertificationSalesPriceAmountSpecified
                    || this.VABorrowerCertificationOccupancyTypeSpecified
                    || this.VABorrowerSurvivingSpouseIndicatorSpecified
                    || this.VACoBorrowerNonTaxableIncomeAmountSpecified
                    || this.VACoBorrowerTaxableIncomeAmountSpecified
                    || this.VAFederalTaxAmountSpecified
                    || this.VALocalTaxAmountSpecified
                    || this.VAPrimaryBorrowerNonTaxableIncomeAmountSpecified
                    || this.VAPrimaryBorrowerTaxableIncomeAmountSpecified
                    || this.VASocialSecurityTaxAmountSpecified
                    || this.VAStateTaxAmountSpecified
                    || this.VeteranStatusIndicatorSpecified;
            }
        }

        /// <summary>
        /// An identifier assigned by HUDs Credit Alert Interactive Voice Response System, an interactive system used to check the applicants outstanding federal obligations.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIdentifier CAIVRSIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the CAIVRSIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CAIVRSIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool CAIVRSIdentifierSpecified
        {
            get { return CAIVRSIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Property built prior to 1978, Lead Paint Notice provided to borrower. FHA Borrower Certification, HUD-92900-A (HUD/VA Addendum to URLA), section 25(6).
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOIndicator FHABorrowerCertificationLeadPaintIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the FHABorrowerCertificationLeadPaintIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FHABorrowerCertificationLeadPaintIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool FHABorrowerCertificationLeadPaintIndicatorSpecified
        {
            get { return FHABorrowerCertificationLeadPaintIndicator != null; }
            set { }
        }

        /// <summary>
        /// Original mortgage amount of property that was sold with in the past 60 months secured by HUD/FHA mortgage. FHA Borrower Certification HUD92900-A (FHA/VA Addendum to URLA) value used in Section 22c: Original Mortgage Amount.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOAmount FHABorrowerCertificationOriginalMortgageAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the FHABorrowerCertificationOriginalMortgageAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FHABorrowerCertificationOriginalMortgageAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool FHABorrowerCertificationOriginalMortgageAmountSpecified
        {
            get { return FHABorrowerCertificationOriginalMortgageAmount != null; }
            set { }
        }

        /// <summary>
        /// Does borrower own four or more dwellings. FHA Borrower Certification, HUD-92900-A (HUD/VA Addendum to URLA), section 22f.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOIndicator FHABorrowerCertificationOwnFourOrMoreDwellingsIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the FHABorrowerCertificationOwnFourOrMoreDwellingsIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FHABorrowerCertificationOwnFourOrMoreDwellingsIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool FHABorrowerCertificationOwnFourOrMoreDwellingsIndicatorSpecified
        {
            get { return FHABorrowerCertificationOwnFourOrMoreDwellingsIndicator != null; }
            set { }
        }

        /// <summary>
        /// Do you own or have you sold other real estate within the past 60 months on which there was a HUD/FHA mortgage? FHA Borrower Certification, HUD-92900-A, (HUD/VA Addendum to URLA) Section 22a:.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOIndicator FHABorrowerCertificationOwnOtherPropertyIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the FHABorrowerCertificationOwnOtherPropertyIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FHABorrowerCertificationOwnOtherPropertyIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool FHABorrowerCertificationOwnOtherPropertyIndicatorSpecified
        {
            get { return FHABorrowerCertificationOwnOtherPropertyIndicator != null; }
            set { }
        }

        /// <summary>
        /// City of property sold in the past 60 months on which there was a HUD/FHA mortgage. FHA Borrower Certification, HUD-92900-A (HUD/VA Addendum to URLA), section 22d.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString FHABorrowerCertificationPropertySoldCityName;

        /// <summary>
        /// Gets or sets a value indicating whether the FHABorrowerCertificationPropertySoldCityName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FHABorrowerCertificationPropertySoldCityName element has been assigned a value.</value>
        [XmlIgnore]
        public bool FHABorrowerCertificationPropertySoldCityNameSpecified
        {
            get { return FHABorrowerCertificationPropertySoldCityName != null; }
            set { }
        }

        /// <summary>
        /// Postal Code of property sold in the past 60 months on which there was a HUD/FHA mortgage. FHA Borrower Certification, HUD-92900-A (HUD/VA Addendum to URLA), section 22d.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOCode FHABorrowerCertificationPropertySoldPostalCode;

        /// <summary>
        /// Gets or sets a value indicating whether the FHABorrowerCertificationPropertySoldPostalCode element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FHABorrowerCertificationPropertySoldPostalCode element has been assigned a value.</value>
        [XmlIgnore]
        public bool FHABorrowerCertificationPropertySoldPostalCodeSpecified
        {
            get { return FHABorrowerCertificationPropertySoldPostalCode != null; }
            set { }
        }

        /// <summary>
        /// State of property sold in the past 60 months on which there was a HUD/FHA mortgage. FHA Borrower Certification, HUD-92900-A (HUD/VA Addendum to URLA), section 22d.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOString FHABorrowerCertificationPropertySoldStateName;

        /// <summary>
        /// Gets or sets a value indicating whether the FHABorrowerCertificationPropertySoldStateName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FHABorrowerCertificationPropertySoldStateName element has been assigned a value.</value>
        [XmlIgnore]
        public bool FHABorrowerCertificationPropertySoldStateNameSpecified
        {
            get { return FHABorrowerCertificationPropertySoldStateName != null; }
            set { }
        }

        /// <summary>
        /// Street address of property sold in the past 60 months on which there was a HUD/FHA mortgage. FHA Borrower Certification, HUD-92900-A (HUD/VA Addendum to URLA), section 22d.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOString FHABorrowerCertificationPropertySoldStreetAddressLineText;

        /// <summary>
        /// Gets or sets a value indicating whether the FHABorrowerCertificationPropertySoldStreetAddressLineText element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FHABorrowerCertificationPropertySoldStreetAddressLineText element has been assigned a value.</value>
        [XmlIgnore]
        public bool FHABorrowerCertificationPropertySoldStreetAddressLineTextSpecified
        {
            get { return FHABorrowerCertificationPropertySoldStreetAddressLineText != null; }
            set { }
        }

        /// <summary>
        /// If owned property within the past 60 months that was secured by a HUD/FHA mortgage, is it to be sold? FHA Borrower Certification, HUD-92900-A, (HUD/VA Addendum to URLA) Section 22a:.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOIndicator FHABorrowerCertificationPropertyToBeSoldIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the FHABorrowerCertificationPropertyToBeSoldIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FHABorrowerCertificationPropertyToBeSoldIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool FHABorrowerCertificationPropertyToBeSoldIndicatorSpecified
        {
            get { return FHABorrowerCertificationPropertyToBeSoldIndicator != null; }
            set { }
        }

        /// <summary>
        /// Property covered by mortgage is to be rented, or is adjacent to rental properties involving eight or more dwelling units in which the borrower has a financial interest. FHA Borrower Certification, HUD-92900-A (HUD/VA Addendum to URLA), section 22e.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOIndicator FHABorrowerCertificationRentalIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the FHABorrowerCertificationRentalIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FHABorrowerCertificationRentalIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool FHABorrowerCertificationRentalIndicatorSpecified
        {
            get { return FHABorrowerCertificationRentalIndicator != null; }
            set { }
        }

        /// <summary>
        /// Sales price of property sold in the past 60 months that was secured by HUD/FHA mortgage. FHA Borrower Certification, HUD-92900-A, (HUD/VA Addendum to URLA) Section 22b:.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOAmount FHABorrowerCertificationSalesPriceAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the FHABorrowerCertificationSalesPriceAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FHABorrowerCertificationSalesPriceAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool FHABorrowerCertificationSalesPriceAmountSpecified
        {
            get { return FHABorrowerCertificationSalesPriceAmount != null; }
            set { }
        }

        /// <summary>
        /// FHA/VA Borrower acknowledgement when Sales Price exceeds the Appraised Value. FHA/VA Borrower Certification, HUD-92900-A (HUD/VA Addendum to URLA), section 25(3).
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOEnum<FHA_VABorrowerCertificationSalesPriceExceedsAppraisedValueBase> FHA_VABorrowerCertificationSalesPriceExceedsAppraisedValueType;

        /// <summary>
        /// Gets or sets a value indicating whether the FHA_VABorrowerCertificationSalesPriceExceedsAppraisedValueType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FHA_VABorrowerCertificationSalesPriceExceedsAppraisedValueType element has been assigned a value.</value>
        [XmlIgnore]
        public bool FHA_VABorrowerCertificationSalesPriceExceedsAppraisedValueTypeSpecified
        {
            get { return this.FHA_VABorrowerCertificationSalesPriceExceedsAppraisedValueType != null && this.FHA_VABorrowerCertificationSalesPriceExceedsAppraisedValueType.enumValue != FHA_VABorrowerCertificationSalesPriceExceedsAppraisedValueBase.Blank; }
            set { }
        }

        /// <summary>
        /// VA Borrower Certification type of occupancy. VA Borrower Certification, HUD-92900-A (HUD/VA Addendum to URLA), section 25(2).
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOEnum<VABorrowerCertificationOccupancyBase> VABorrowerCertificationOccupancyType;

        /// <summary>
        /// Gets or sets a value indicating whether the VABorrowerCertificationOccupancyType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the VABorrowerCertificationOccupancyType element has been assigned a value.</value>
        [XmlIgnore]
        public bool VABorrowerCertificationOccupancyTypeSpecified
        {
            get { return this.VABorrowerCertificationOccupancyType != null && this.VABorrowerCertificationOccupancyType.enumValue != VABorrowerCertificationOccupancyBase.Blank; }
            set { }
        }

        /// <summary>
        /// VA Borrower Surviving Spouse Indicator.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOIndicator VABorrowerSurvivingSpouseIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the VABorrowerSurvivingSpouseIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the VABorrowerSurvivingSpouseIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool VABorrowerSurvivingSpouseIndicatorSpecified
        {
            get { return VABorrowerSurvivingSpouseIndicator != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of the monthly income that is not subject to taxation for a co-borrower.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOAmount VACoBorrowerNonTaxableIncomeAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the VACoBorrowerNonTaxableIncomeAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the VACoBorrowerNonTaxableIncomeAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool VACoBorrowerNonTaxableIncomeAmountSpecified
        {
            get { return VACoBorrowerNonTaxableIncomeAmount != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of the monthly income that is subject to taxation for a co-borrower.
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMOAmount VACoBorrowerTaxableIncomeAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the VACoBorrowerTaxableIncomeAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the VACoBorrowerTaxableIncomeAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool VACoBorrowerTaxableIncomeAmountSpecified
        {
            get { return VACoBorrowerTaxableIncomeAmount != null; }
            set { }
        }

        /// <summary>
        /// The monthly amount of federal income tax from the applicable table for VA loans.
        /// </summary>
        [XmlElement(Order = 17)]
        public MISMOAmount VAFederalTaxAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the VAFederalTaxAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the VAFederalTaxAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool VAFederalTaxAmountSpecified
        {
            get { return VAFederalTaxAmount != null; }
            set { }
        }

        /// <summary>
        /// The monthly amount of income tax other than that paid to state or federal governments from the applicable table for VA loans.
        /// </summary>
        [XmlElement(Order = 18)]
        public MISMOAmount VALocalTaxAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the VALocalTaxAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the VALocalTaxAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool VALocalTaxAmountSpecified
        {
            get { return VALocalTaxAmount != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of the monthly income that is not subject to taxation of the primary borrower.
        /// </summary>
        [XmlElement(Order = 19)]
        public MISMOAmount VAPrimaryBorrowerNonTaxableIncomeAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the VAPrimaryBorrowerNonTaxableIncomeAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the VAPrimaryBorrowerNonTaxableIncomeAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool VAPrimaryBorrowerNonTaxableIncomeAmountSpecified
        {
            get { return VAPrimaryBorrowerNonTaxableIncomeAmount != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of monthly income of the borrower  that is subject to taxation.
        /// </summary>
        [XmlElement(Order = 20)]
        public MISMOAmount VAPrimaryBorrowerTaxableIncomeAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the VAPrimaryBorrowerTaxableIncomeAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the VAPrimaryBorrowerTaxableIncomeAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool VAPrimaryBorrowerTaxableIncomeAmountSpecified
        {
            get { return VAPrimaryBorrowerTaxableIncomeAmount != null; }
            set { }
        }

        /// <summary>
        /// The monthly amount of social security tax from the applicable table for VA loans.
        /// </summary>
        [XmlElement(Order = 21)]
        public MISMOAmount VASocialSecurityTaxAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the VASocialSecurityTaxAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the VASocialSecurityTaxAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool VASocialSecurityTaxAmountSpecified
        {
            get { return VASocialSecurityTaxAmount != null; }
            set { }
        }

        /// <summary>
        /// The monthly amount of state income tax from the applicable table for VA loans.
        /// </summary>
        [XmlElement(Order = 22)]
        public MISMOAmount VAStateTaxAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the VAStateTaxAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the VAStateTaxAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool VAStateTaxAmountSpecified
        {
            get { return VAStateTaxAmount != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether the borrower qualifies as a Veteran under HUD guidelines and that the loan application should be evaluated as a 203(b)(2) Veteran Loan. If any borrower qualifies as a Veteran under VA guidelines then the loan (including all borrowers) may be processed as a VA loan application.
        /// </summary>
        [XmlElement(Order = 23)]
        public MISMOIndicator VeteranStatusIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the VeteranStatusIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the VeteranStatusIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool VeteranStatusIndicatorSpecified
        {
            get { return VeteranStatusIndicator != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 24)]
        public GOVERNMENT_BORROWER_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
