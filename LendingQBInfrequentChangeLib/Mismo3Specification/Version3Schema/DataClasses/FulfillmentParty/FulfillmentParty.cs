namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class FULFILLMENT_PARTY
    {
        /// <summary>
        /// Gets a value indicating whether the FULFILLMENT_PARTY container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.FulfillmentPartyRoleTypeOtherDescriptionSpecified
                    || this.FulfillmentPartyRoleTypeSpecified
                    || this.FulfillmentPartyServiceTypeOtherDescriptionSpecified
                    || this.FulfillmentPartyServiceTypeSpecified;
            }
        }

        /// <summary>
        /// This describes the fulfillment party's profession.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<FulfillmentPartyRoleBase> FulfillmentPartyRoleType;

        /// <summary>
        /// Gets or sets a value indicating whether the FulfillmentPartyRoleType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FulfillmentPartyRoleType element has been assigned a value.</value>
        [XmlIgnore]
        public bool FulfillmentPartyRoleTypeSpecified
        {
            get { return this.FulfillmentPartyRoleType != null && this.FulfillmentPartyRoleType.enumValue != FulfillmentPartyRoleBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture the fulfillment party role if Other is selected as the Fulfillment Party Role Type.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString FulfillmentPartyRoleTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the FulfillmentPartyRoleTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FulfillmentPartyRoleTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool FulfillmentPartyRoleTypeOtherDescriptionSpecified
        {
            get { return FulfillmentPartyRoleTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Defines the type of service provided by the fulfillment party.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<FulfillmentPartyServiceBase> FulfillmentPartyServiceType;

        /// <summary>
        /// Gets or sets a value indicating whether the FulfillmentPartyServiceType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FulfillmentPartyServiceType element has been assigned a value.</value>
        [XmlIgnore]
        public bool FulfillmentPartyServiceTypeSpecified
        {
            get { return this.FulfillmentPartyServiceType != null && this.FulfillmentPartyServiceType.enumValue != FulfillmentPartyServiceBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture the fulfillment party type if Other is selected as the Fulfillment Party Service Type.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString FulfillmentPartyServiceTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the FulfillmentPartyServiceTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FulfillmentPartyServiceTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool FulfillmentPartyServiceTypeOtherDescriptionSpecified
        {
            get { return FulfillmentPartyServiceTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public FULFILLMENT_PARTY_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
