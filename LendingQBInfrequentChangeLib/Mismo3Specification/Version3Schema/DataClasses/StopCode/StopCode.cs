namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class STOP_CODE
    {
        /// <summary>
        /// Gets a value indicating whether the STOP_CODE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.StopCodeActionTypeOtherDescriptionSpecified
                    || this.StopCodeActionTypeSpecified
                    || this.StopCodeConditionTypeOtherDescriptionSpecified
                    || this.StopCodeConditionTypeSpecified
                    || this.StopCodeExpirationDateSpecified;
            }
        }

        /// <summary>
        /// Indicates special handling action to be taken.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<StopCodeActionBase> StopCodeActionType;

        /// <summary>
        /// Gets or sets a value indicating whether the StopCodeActionType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the StopCodeActionType element has been assigned a value.</value>
        [XmlIgnore]
        public bool StopCodeActionTypeSpecified
        {
            get { return this.StopCodeActionType != null && this.StopCodeActionType.enumValue != StopCodeActionBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture the stop code action type if Other is selected, this may be a system specific code and or a system specific code and description.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString StopCodeActionTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the StopCodeActionTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the StopCodeActionTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool StopCodeActionTypeOtherDescriptionSpecified
        {
            get { return StopCodeActionTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates the loan condition that is related to the stop code action type for the loan.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<StopCodeConditionBase> StopCodeConditionType;

        /// <summary>
        /// Gets or sets a value indicating whether the StopCodeConditionType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the StopCodeConditionType element has been assigned a value.</value>
        [XmlIgnore]
        public bool StopCodeConditionTypeSpecified
        {
            get { return this.StopCodeConditionType != null && this.StopCodeConditionType.enumValue != StopCodeConditionBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture the stop code condition type if Other is selected, this may be a system specific code and or a system specific code and description.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString StopCodeConditionTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the StopCodeConditionTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the StopCodeConditionTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool StopCodeConditionTypeOtherDescriptionSpecified
        {
            get { return StopCodeConditionTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates the expiration of any stop.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMODate StopCodeExpirationDate;

        /// <summary>
        /// Gets or sets a value indicating whether the StopCodeExpirationDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the StopCodeExpirationDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool StopCodeExpirationDateSpecified
        {
            get { return StopCodeExpirationDate != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 5)]
        public STOP_CODE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
