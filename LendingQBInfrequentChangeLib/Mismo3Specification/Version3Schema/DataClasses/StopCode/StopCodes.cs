namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class STOP_CODES
    {
        /// <summary>
        /// Gets a value indicating whether the STOP_CODES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.StopCodeSpecified;
            }
        }

        /// <summary>
        /// A collection of stop codes.
        /// </summary>
        [XmlElement("STOP_CODE", Order = 0)]
		public List<STOP_CODE> StopCode = new List<STOP_CODE>();

        /// <summary>
        /// Gets or sets a value indicating whether the StopCode element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the StopCode element has been assigned a value.</value>
        [XmlIgnore]
        public bool StopCodeSpecified
        {
            get { return this.StopCode != null && this.StopCode.Count(s => s != null && s.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public STOP_CODES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
