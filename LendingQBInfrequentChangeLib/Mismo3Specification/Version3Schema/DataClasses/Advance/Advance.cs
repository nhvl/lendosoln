namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class ADVANCE
    {
        /// <summary>
        /// Gets a value indicating whether the ADVANCE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AdvanceRecoveryWaivedAmountSpecified
                    || this.AdvanceTypeOtherDescriptionSpecified
                    || this.AdvanceTypeSpecified
                    || this.CumulativeAdvanceAmountSpecified
                    || this.CurrentAdvanceAmountSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// The portion of advances that is to be forgiven.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount AdvanceRecoveryWaivedAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the AdvanceRecoveryWaivedAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AdvanceRecoveryWaivedAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool AdvanceRecoveryWaivedAmountSpecified
        {
            get { return AdvanceRecoveryWaivedAmount != null; }
            set { }
        }

        /// <summary>
        /// Specifies the type of advance made on the loan by the servicer.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<AdvanceBase> AdvanceType;

        /// <summary>
        /// Gets or sets a value indicating whether the AdvanceType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AdvanceType element has been assigned a value.</value>
        [XmlIgnore]
        public bool AdvanceTypeSpecified
        {
            get { return this.AdvanceType != null && this.AdvanceType.enumValue != AdvanceBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture additional information when Other is selected for Advance Type.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString AdvanceTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the AdvanceTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AdvanceTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool AdvanceTypeOtherDescriptionSpecified
        {
            get { return AdvanceTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of the cumulative total advances made by a servicer for a specified Advance Type.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOAmount CumulativeAdvanceAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the CumulativeAdvanceAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CumulativeAdvanceAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool CumulativeAdvanceAmountSpecified
        {
            get { return CumulativeAdvanceAmount != null; }
            set { }
        }

        /// <summary>
        /// The total dollar amount advanced by the servicer for a specified Advance Type for the current reporting period.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOAmount CurrentAdvanceAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the CurrentAdvanceAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CurrentAdvanceAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool CurrentAdvanceAmountSpecified
        {
            get { return CurrentAdvanceAmount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 5)]
        public ADVANCE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
