namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class ADVANCES
    {
        /// <summary>
        /// Gets a value indicating whether the ADVANCES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AdvanceSpecified
                    || this.AdvanceSummarySpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of advances.
        /// </summary>
        [XmlElement("ADVANCE", Order = 0)]
		public List<ADVANCE> Advance = new List<ADVANCE>();

        /// <summary>
        /// Gets or sets a value indicating whether the Advance element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Advance element has been assigned a value.</value>
        [XmlIgnore]
        public bool AdvanceSpecified
        {
            get { return this.Advance != null && this.Advance.Count(a => a != null && a.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// Summary of an advance.
        /// </summary>
        [XmlElement("ADVANCE_SUMMARY", Order = 1)]
        public ADVANCE_SUMMARY AdvanceSummary;

        /// <summary>
        /// Gets or sets a value indicating whether the AdvanceSummary element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AdvanceSummary element has been assigned a value.</value>
        [XmlIgnore]
        public bool AdvanceSummarySpecified
        {
            get { return this.AdvanceSummary != null && this.AdvanceSummary.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public ADVANCES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
