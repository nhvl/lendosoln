namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class SALES_HISTORY
    {
        /// <summary>
        /// Gets a value indicating whether the SALES_HISTORY container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ArmsLengthIndicatorSpecified
                    || this.ExtensionSpecified
                    || this.ForSaleByOwnerIndicatorSpecified
                    || this.OwnershipTransferDateSpecified
                    || this.OwnershipTransferTransactionAmountSpecified
                    || this.OwnershipTransferTransactionTypeOtherDescriptionSpecified
                    || this.OwnershipTransferTransactionTypeSpecified
                    || this.PurchasePriceAmountSpecified
                    || this.SaleTypeOtherDescriptionSpecified
                    || this.SaleTypeSpecified;
            }
        }

        /// <summary>
        /// Indicates that this is an Arms Length Transaction. An Arms length transaction is between a willing buyer and a willing seller with no undue influence on either party and there is no relationship between the parties except that of the specific transaction.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIndicator ArmsLengthIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ArmsLengthIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ArmsLengthIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ArmsLengthIndicatorSpecified
        {
            get { return ArmsLengthIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that the property is for sale by owner.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOIndicator ForSaleByOwnerIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ForSaleByOwnerIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ForSaleByOwnerIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ForSaleByOwnerIndicatorSpecified
        {
            get { return ForSaleByOwnerIndicator != null; }
            set { }
        }

        /// <summary>
        /// The date of the transfer of ownership of real property as recognized in the jurisdiction in which it is located.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMODate OwnershipTransferDate;

        /// <summary>
        /// Gets or sets a value indicating whether the OwnershipTransferDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OwnershipTransferDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool OwnershipTransferDateSpecified
        {
            get { return OwnershipTransferDate != null; }
            set { }
        }

        /// <summary>
        /// The amount of transaction (typically the sale price) related to the transfer of ownership of real property as recognized in the jurisdiction in which it is located.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOAmount OwnershipTransferTransactionAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the OwnershipTransferTransactionAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OwnershipTransferTransactionAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool OwnershipTransferTransactionAmountSpecified
        {
            get { return OwnershipTransferTransactionAmount != null; }
            set { }
        }

        /// <summary>
        /// The type of ownership transfer of real property as recognized in the jurisdiction in which it is located.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<OwnershipTransferTransactionBase> OwnershipTransferTransactionType;

        /// <summary>
        /// Gets or sets a value indicating whether the OwnershipTransferTransactionType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OwnershipTransferTransactionType element has been assigned a value.</value>
        [XmlIgnore]
        public bool OwnershipTransferTransactionTypeSpecified
        {
            get { return this.OwnershipTransferTransactionType != null && this.OwnershipTransferTransactionType.enumValue != OwnershipTransferTransactionBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Ownership Transfer Transaction Type.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString OwnershipTransferTransactionTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the OwnershipTransferTransactionTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OwnershipTransferTransactionTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool OwnershipTransferTransactionTypeOtherDescriptionSpecified
        {
            get { return OwnershipTransferTransactionTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The total dollar amount paid by the borrower for the property. The purchase price is presented on the offer to purchase.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOAmount PurchasePriceAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PurchasePriceAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PurchasePriceAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PurchasePriceAmountSpecified
        {
            get { return PurchasePriceAmount != null; }
            set { }
        }

        /// <summary>
        /// Quantifies the type of sales transaction for the property as indicated from the analysis of the sales contract.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOEnum<SaleBase> SaleType;

        /// <summary>
        /// Gets or sets a value indicating whether the SaleType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SaleType element has been assigned a value.</value>
        [XmlIgnore]
        public bool SaleTypeSpecified
        {
            get { return this.SaleType != null && this.SaleType.enumValue != SaleBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for the ListingStatusType.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOString SaleTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the SaleTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SaleTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool SaleTypeOtherDescriptionSpecified
        {
            get { return SaleTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 9)]
        public SALES_HISTORY_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
