namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class SALES_HISTORIES
    {
        /// <summary>
        /// Gets a value indicating whether the SALES_HISTORIES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.SalesHistorySpecified;
            }
        }

        /// <summary>
        /// A collection of sales histories.
        /// </summary>
        [XmlElement("SALES_HISTORY", Order = 0)]
		public List<SALES_HISTORY> SalesHistory = new List<SALES_HISTORY>();

        /// <summary>
        /// Gets or sets a value indicating whether the SalesHistory element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SalesHistory element has been assigned a value.</value>
        [XmlIgnore]
        public bool SalesHistorySpecified
        {
            get { return this.SalesHistory != null && this.SalesHistory.Count(s => s != null && s.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public SALES_HISTORIES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
