namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class LOAN_IDENTIFIERS
    {
        /// <summary>
        /// Gets a value indicating whether the LOAN_IDENTIFIERS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.LoanIdentifierSpecified;
            }
        }

        /// <summary>
        /// A collection of loan identifiers.
        /// </summary>
        [XmlElement("LOAN_IDENTIFIER", Order = 0)]
        public List<LOAN_IDENTIFIER> LoanIdentifier = new List<LOAN_IDENTIFIER>();

        /// <summary>
        /// Gets or sets a value indicating whether the LoanIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanIdentifierSpecified
        {
            get { return LoanIdentifier != null && LoanIdentifier.Count(l => l != null && l.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public LOAN_IDENTIFIERS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
