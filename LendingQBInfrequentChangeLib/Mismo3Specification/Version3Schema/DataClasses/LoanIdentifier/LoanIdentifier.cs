namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class LOAN_IDENTIFIER
    {
        /// <summary>
        /// Gets a value indicating whether the LOAN_IDENTIFIER container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return LoanIdentifierSpecified
                || this.LoanIdentifierTypeSpecified
                || this.LoanIdentifierTypeOtherDescriptionSpecified
                || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// The value of the identifier for the specified type.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIdentifier LoanIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanIdentifierSpecified
        {
            get { return LoanIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The type of identifier used for a loan.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<LoanIdentifierBase> LoanIdentifierType;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanIdentifierType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanIdentifierType element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanIdentifierTypeSpecified
        {
            get { return this.LoanIdentifierType != null && this.LoanIdentifierType.enumValue != LoanIdentifierBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the Loan Identifier Type when Other is selected as the Loan Identifier Type.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString LoanIdentifierTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanIdentifierTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanIdentifierTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanIdentifierTypeOtherDescriptionSpecified
        {
            get { return LoanIdentifierTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public LOAN_IDENTIFIER_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
