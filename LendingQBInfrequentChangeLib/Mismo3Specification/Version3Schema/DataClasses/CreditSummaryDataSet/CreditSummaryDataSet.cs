namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class CREDIT_SUMMARY_DATA_SET
    {
        /// <summary>
        /// Gets a value indicating whether the CREDIT_SUMMARY_DATA_SET container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditSummaryDataSetNameSpecified
                    || this.CreditSummaryDataSetValueSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// The Credit Summary Data Set Name-Value pair data provides a standard method of providing a set of summary data. The Data Set Name identifies the type of summary data. The Data Set Value contains the actual data described by the Data Set Name.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString CreditSummaryDataSetName;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditSummaryDataSetName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditSummaryDataSetName element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditSummaryDataSetNameSpecified
        {
            get { return CreditSummaryDataSetName != null; }
            set { }
        }

        /// <summary>
        /// This element holds the actual data value described by the Credit Summary Data Set Name attribute.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString CreditSummaryDataSetValue;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditSummaryDataSetValue element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditSummaryDataSetValue element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditSummaryDataSetValueSpecified
        {
            get { return CreditSummaryDataSetValue != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public CREDIT_SUMMARY_DATA_SET_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
