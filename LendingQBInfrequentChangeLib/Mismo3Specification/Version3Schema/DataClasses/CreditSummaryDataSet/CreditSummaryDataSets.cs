namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CREDIT_SUMMARY_DATA_SETS
    {
        /// <summary>
        /// Gets a value indicating whether the CREDIT_SUMMARY_DATA_SETS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditSummaryDataSetSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of credit summary data sets.
        /// </summary>
        [XmlElement("CREDIT_SUMMARY_DATA_SET", Order = 0)]
		public List<CREDIT_SUMMARY_DATA_SET> CreditSummaryDataSet = new List<CREDIT_SUMMARY_DATA_SET>();

        /// <summary>
        /// Gets or sets a value indicating whether the CreditSummaryDataSet element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditSummaryDataSet element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditSummaryDataSetSpecified
        {
            get { return this.CreditSummaryDataSet != null && this.CreditSummaryDataSet.Count(c => c != null && c.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public CREDIT_SUMMARY_DATA_SETS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
