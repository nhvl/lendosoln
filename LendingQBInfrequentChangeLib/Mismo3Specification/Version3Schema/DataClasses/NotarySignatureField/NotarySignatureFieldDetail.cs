namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class NOTARY_SIGNATURE_FIELD_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the NOTARY_SIGNATURE_FIELD_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.SignatureTypeOtherDescriptionSpecified
                    || this.SignatureTypeSpecified;
            }
        }

        /// <summary>
        /// Contains the signature type proposed by the document creator. The actual type of the signature applied to the signature field may be different (please see audit trail).
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<SignatureBase> SignatureType;

        /// <summary>
        /// Gets or sets a value indicating whether the SignatureType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SignatureType element has been assigned a value.</value>
        [XmlIgnore]
        public bool SignatureTypeSpecified
        {
            get { return this.SignatureType != null && this.SignatureType.enumValue != SignatureBase.Blank; }
            set { }
        }

        /// <summary>
        /// Contains a description of the signature type when the value of Other is used for the SignatureType.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString SignatureTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the SignatureTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SignatureTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool SignatureTypeOtherDescriptionSpecified
        {
            get { return SignatureTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public NOTARY_SIGNATURE_FIELD_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
