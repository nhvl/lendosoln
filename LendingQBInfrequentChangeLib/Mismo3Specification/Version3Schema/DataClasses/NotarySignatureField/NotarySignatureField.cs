namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class NOTARY_SIGNATURE_FIELD
    {
        /// <summary>
        /// Gets a value indicating whether the NOTARY_SIGNATURE_FIELD container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.NotarialCertificateLanguageFieldReferenceSpecified
                    || this.NotarialSealFieldReferenceSpecified
                    || this.NotarySignatureFieldDetailSpecified
                    || this.SignatureFieldReferenceSpecified;
            }
        }

        /// <summary>
        /// Reference for a notarial certificate language field.
        /// </summary>
        [XmlElement("NOTARIAL_CERTIFICATE_LANGUAGE_FIELD_REFERENCE", Order = 0)]
        public FIELD_REFERENCE NotarialCertificateLanguageFieldReference;

        /// <summary>
        /// Gets or sets a value indicating whether the NotarialCertificateLanguageFieldReference element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NotarialCertificateLanguageFieldReference element has been assigned a value.</value>
        [XmlIgnore]
        public bool NotarialCertificateLanguageFieldReferenceSpecified
        {
            get { return this.NotarialCertificateLanguageFieldReference != null && this.NotarialCertificateLanguageFieldReference.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Reference for a notarial seal field.
        /// </summary>
        [XmlElement("NOTARIAL_SEAL_FIELD_REFERENCE", Order = 1)]
        public FIELD_REFERENCE NotarialSealFieldReference;

        /// <summary>
        /// Gets or sets a value indicating whether the NotarialSealFieldReference element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NotarialSealFieldReference element has been assigned a value.</value>
        [XmlIgnore]
        public bool NotarialSealFieldReferenceSpecified
        {
            get { return this.NotarialSealFieldReference != null && this.NotarialSealFieldReference.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details on a notary signature field.
        /// </summary>
        [XmlElement("NOTARY_SIGNATURE_FIELD_DETAIL", Order = 2)]
        public NOTARY_SIGNATURE_FIELD_DETAIL NotarySignatureFieldDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the NotarySignatureFieldDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NotarySignatureFieldDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool NotarySignatureFieldDetailSpecified
        {
            get { return this.NotarySignatureFieldDetail != null && this.NotarySignatureFieldDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Field reference for a signature.
        /// </summary>
        [XmlElement("SIGNATURE_FIELD_REFERENCE", Order = 3)]
        public FIELD_REFERENCE SignatureFieldReference;

        /// <summary>
        /// Gets or sets a value indicating whether the SignatureFieldReference element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SignatureFieldReference element has been assigned a value.</value>
        [XmlIgnore]
        public bool SignatureFieldReferenceSpecified
        {
            get { return this.SignatureFieldReference != null && this.SignatureFieldReference.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public NOTARY_SIGNATURE_FIELD_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
