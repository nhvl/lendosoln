namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class AUTOMATED_UNDERWRITING_SYSTEM
    {
        /// <summary>
        /// Gets a value indicating whether the AUTOMATED_UNDERWRITING_SYSTEM container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AutomatedUnderwritingSystemRequestSpecified
                    || this.AutomatedUnderwritingSystemResponseSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// The request to the AUS.
        /// </summary>
        [XmlElement("AUTOMATED_UNDERWRITING_SYSTEM_REQUEST", Order = 0)]
        public AUTOMATED_UNDERWRITING_SYSTEM_REQUEST AutomatedUnderwritingSystemRequest;

        /// <summary>
        /// Gets or sets a value indicating whether the AutomatedUnderwritingSystemRequest element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AutomatedUnderwritingSystemRequest element has been assigned a value.</value>
        [XmlIgnore]
        public bool AutomatedUnderwritingSystemRequestSpecified
        {
            get { return this.AutomatedUnderwritingSystemRequest != null && this.AutomatedUnderwritingSystemRequest.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The response from the AUS.
        /// </summary>
        [XmlElement("AUTOMATED_UNDERWRITING_SYSTEM_RESPONSE", Order = 1)]
        public AUTOMATED_UNDERWRITING_SYSTEM_RESPONSE AutomatedUnderwritingSystemResponse;

        /// <summary>
        /// Gets or sets a value indicating whether the AutomatedUnderwritingSystemResponse element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AutomatedUnderwritingSystemResponse element has been assigned a value.</value>
        [XmlIgnore]
        public bool AutomatedUnderwritingSystemResponseSpecified
        {
            get { return this.AutomatedUnderwritingSystemResponse != null && this.AutomatedUnderwritingSystemResponse.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public AUTOMATED_UNDERWRITING_SYSTEM_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
