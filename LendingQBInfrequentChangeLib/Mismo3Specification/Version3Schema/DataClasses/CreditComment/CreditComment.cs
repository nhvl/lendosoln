namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class CREDIT_COMMENT
    {
        /// <summary>
        /// Gets a value indicating whether the CREDIT_COMMENT container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditCommentCodeSourceTypeOtherDescriptionSpecified
                    || this.CreditCommentCodeSourceTypeSpecified
                    || this.CreditCommentCodeSpecified
                    || this.CreditCommentReportedDateSpecified
                    || this.CreditCommentSourceTypeSpecified
                    || this.CreditCommentTextSpecified
                    || this.CreditCommentTypeOtherDescriptionSpecified
                    || this.CreditCommentTypeSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// This is a code value that may be associated with Credit Comment Text data. This could be a repository bureau code (Equifax, Experian, Trans Union), or it could be a code provided by a lender, credit bureau or other source that is specified in the Credit Comment Source Type.  The code value should be exactly as shown in the source document without any other characters such as a prefix.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOCode CreditCommentCode;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditCommentCode element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditCommentCode element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditCommentCodeSpecified
        {
            get { return CreditCommentCode != null; }
            set { }
        }

        /// <summary>
        /// This data element provides an enumerated list of sources for the Credit Comment data.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<CreditCommentCodeSourceBase> CreditCommentCodeSourceType;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditCommentCodeSourceType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditCommentCodeSourceType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditCommentCodeSourceTypeSpecified
        {
            get { return this.CreditCommentCodeSourceType != null && this.CreditCommentCodeSourceType.enumValue != CreditCommentCodeSourceBase.Blank; }
            set { }
        }

        /// <summary>
        /// When Credit Comment Source Type is set to Other, enter its value in this data element.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString CreditCommentCodeSourceTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditCommentCodeSourceTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditCommentCodeSourceTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditCommentCodeSourceTypeOtherDescriptionSpecified
        {
            get { return CreditCommentCodeSourceTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates the date that the credit bureaus added the comment to the file of the borrower.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMODate CreditCommentReportedDate;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditCommentReportedDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditCommentReportedDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditCommentReportedDateSpecified
        {
            get { return CreditCommentReportedDate != null; }
            set { }
        }

        /// <summary>
        /// This data element provides an enumerated list of sources for the Credit Comment data.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<CreditCommentSourceBase> CreditCommentSourceType;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditCommentSourceType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditCommentSourceType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditCommentSourceTypeSpecified
        {
            get { return this.CreditCommentSourceType != null && this.CreditCommentSourceType.enumValue != CreditCommentSourceBase.Blank; }
            set { }
        }

        /// <summary>
        /// This data element holds comment text data that can appear in various locations on a credit report. The Credit Comment Source Type describes the source of the comment - borrower, credit bureau, lender, or repository bureau.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString CreditCommentText;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditCommentText element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditCommentText element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditCommentTextSpecified
        {
            get { return CreditCommentText != null; }
            set { }
        }

        /// <summary>
        /// Allows the credit bureau to identify the type of credit comment (e.g. Consumer Statement).
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOEnum<CreditCommentBase> CreditCommentType;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditCommentType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditCommentType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditCommentTypeSpecified
        {
            get { return this.CreditCommentType != null && this.CreditCommentType.enumValue != CreditCommentBase.Blank; }
            set { }
        }

        /// <summary>
        /// When Credit Comment Type is set to Other, enter its value in this data element.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOString CreditCommentTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditCommentTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditCommentTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditCommentTypeOtherDescriptionSpecified
        {
            get { return CreditCommentTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 8)]
        public CREDIT_COMMENT_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
