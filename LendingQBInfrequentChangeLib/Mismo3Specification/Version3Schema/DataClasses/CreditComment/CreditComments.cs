namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CREDIT_COMMENTS
    {
        /// <summary>
        /// Gets a value indicating whether the CREDIT_COMMENTS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditCommentSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of credit comments.
        /// </summary>
        [XmlElement("CREDIT_COMMENT", Order = 0)]
		public List<CREDIT_COMMENT> CreditComment = new List<CREDIT_COMMENT>();

        /// <summary>
        /// Gets or sets a value indicating whether the CreditComment element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditComment element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditCommentSpecified
        {
            get { return this.CreditComment != null && this.CreditComment.Count(c => c != null && c.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public CREDIT_COMMENTS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
