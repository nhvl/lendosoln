namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class MI_APPLICATION_REQUEST_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the MI_APPLICATION_REQUEST_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.InvestorProgramNameTypeOtherDescriptionSpecified
                    || this.InvestorProgramNameTypeSpecified
                    || this.MIApplicationRequestTypeOtherDescriptionSpecified
                    || this.MIApplicationRequestTypeSpecified
                    || this.MICompanyNameTypeOtherDescriptionSpecified
                    || this.MICompanyNameTypeSpecified
                    || this.RateQuoteAllProductIndicatorSpecified
                    || this.RateQuoteProductComparisonIndicatorSpecified
                    || this.RateQuoteTypeOtherDescriptionSpecified
                    || this.RateQuoteTypeSpecified;
            }
        }

        /// <summary>
        /// To identify loans originated for specific investor programs.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<InvestorProgramNameBase> InvestorProgramNameType;

        /// <summary>
        /// Gets or sets a value indicating whether the InvestorProgramNameType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InvestorProgramNameType element has been assigned a value.</value>
        [XmlIgnore]
        public bool InvestorProgramNameTypeSpecified
        {
            get { return this.InvestorProgramNameType != null && this.InvestorProgramNameType.enumValue != InvestorProgramNameBase.Blank; }
            set { }
        }

        /// <summary>
        /// When Other is specified in Investor Program Name Type, this element contains the description.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString InvestorProgramNameTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the InvestorProgramNameTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InvestorProgramNameTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool InvestorProgramNameTypeOtherDescriptionSpecified
        {
            get { return InvestorProgramNameTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// To distinguish an original request application for private mortgage insurance from any subsequent requests for the same loan, and from the MIs response to the order.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<MIApplicationRequestBase> MIApplicationRequestType;

        /// <summary>
        /// Gets or sets a value indicating whether the MIApplicationRequestType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIApplicationRequestType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIApplicationRequestTypeSpecified
        {
            get { return this.MIApplicationRequestType != null && this.MIApplicationRequestType.enumValue != MIApplicationRequestBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture the description of the type of MI Request if Other is selected as the MI Application Request Type.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString MIApplicationRequestTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MIApplicationRequestTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIApplicationRequestTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIApplicationRequestTypeOtherDescriptionSpecified
        {
            get { return MIApplicationRequestTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// To convey the private MI company short/common name from whom the private mortgage insurance coverage was obtained.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<MICompanyNameBase> MICompanyNameType;

        /// <summary>
        /// Gets or sets a value indicating whether the MICompanyNameType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MICompanyNameType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MICompanyNameTypeSpecified
        {
            get { return this.MICompanyNameType != null && this.MICompanyNameType.enumValue != MICompanyNameBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the MI company name when Other is selected for the MI Company Name Type.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString MICompanyNameTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MICompanyNameTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MICompanyNameTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MICompanyNameTypeOtherDescriptionSpecified
        {
            get { return MICompanyNameTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates to the MI provider that requesting organization desires all appropriate MI products be considered and returned in the rate quote response.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOIndicator RateQuoteAllProductIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the RateQuoteAllProductIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RateQuoteAllProductIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool RateQuoteAllProductIndicatorSpecified
        {
            get { return RateQuoteAllProductIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates to the MI provider that all available comparison calculations be executed comparing various MI products being requested.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOIndicator RateQuoteProductComparisonIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the RateQuoteProductComparisonIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RateQuoteProductComparisonIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool RateQuoteProductComparisonIndicatorSpecified
        {
            get { return RateQuoteProductComparisonIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates the type of data set included with the request. Initial limited data, or complete detail data.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOEnum<RateQuoteBase> RateQuoteType;

        /// <summary>
        /// Gets or sets a value indicating whether the RateQuoteType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RateQuoteType element has been assigned a value.</value>
        [XmlIgnore]
        public bool RateQuoteTypeSpecified
        {
            get { return this.RateQuoteType != null && this.RateQuoteType.enumValue != RateQuoteBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Rate Quote Type.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOString RateQuoteTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the RateQuoteTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RateQuoteTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool RateQuoteTypeOtherDescriptionSpecified
        {
            get { return RateQuoteTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 10)]
        public MI_APPLICATION_REQUEST_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
