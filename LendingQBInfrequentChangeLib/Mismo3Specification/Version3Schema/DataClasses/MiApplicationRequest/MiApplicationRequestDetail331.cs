namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class MI_APPLICATION_REQUEST_DETAIL_3_3_1
    {
        /// <summary>
        /// Gets a value indicating whether the MI_APPLICATION_REQUEST_DETAIL_3_3_1 container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.MICertificateTypeSpecified
                    || this.MICoveragePlanTypeOtherDescriptionSpecified
                    || this.MICoveragePlanTypeSpecified
                    || this.SubordinateFinancingIsNewIndicatorSpecified;
            }
        }

        /// <summary>
        /// Specifies the type of mortgage insurance certificate required by the customer.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<MICertificateBase> MICertificateType;

        /// <summary>
        /// Gets or sets a value indicating whether the MICertificateType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MICertificateType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MICertificateTypeSpecified
        {
            get { return this.MICertificateType != null && this.MICertificateType.enumValue != MICertificateBase.Blank; }
            set { }
        }

        /// <summary>
        /// Specifies the coverage category of mortgage insurance applicable to the loan.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<MICoveragePlanBase> MICoveragePlanType;

        /// <summary>
        /// Gets or sets a value indicating whether the MICoveragePlanType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MICoveragePlanType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MICoveragePlanTypeSpecified
        {
            get { return this.MICoveragePlanType != null && this.MICoveragePlanType.enumValue != MICoveragePlanBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture the description of the type of MI Coverage Plan if Other is selected as the MICoveragePlanType.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString MICoveragePlanTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MICoveragePlanTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MICoveragePlanTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MICoveragePlanTypeOtherDescriptionSpecified
        {
            get { return MICoveragePlanTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// When true indicates that subordinate financing is being created simultaneously with the subject loan.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOIndicator SubordinateFinancingIsNewIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the SubordinateFinancingIsNewIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SubordinateFinancingIsNewIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool SubordinateFinancingIsNewIndicatorSpecified
        {
            get { return SubordinateFinancingIsNewIndicator != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public MI_APPLICATION_REQUEST_DETAIL_3_3_1_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
