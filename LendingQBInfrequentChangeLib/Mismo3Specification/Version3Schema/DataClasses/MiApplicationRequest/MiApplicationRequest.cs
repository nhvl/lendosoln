namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class MI_APPLICATION_REQUEST
    {
        /// <summary>
        /// Gets a value indicating whether the MI_APPLICATION_REQUEST container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.MiApplicationRequestDetailSpecified
                    || this.MIApplicationSpecified
                    || this.MIProductsSpecified;
            }
        }

        /// <summary>
        /// An application for mortgage insurance.
        /// </summary>
        [XmlElement("MI_APPLICATION", Order = 0)]
        public MI_APPLICATION MIApplication;

        /// <summary>
        /// Gets or sets a value indicating whether the Mortgage Insurance Application element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Mortgage Insurance Application element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIApplicationSpecified
        {
            get { return this.MIApplication != null && this.MIApplication.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details on an application request.
        /// </summary>
        [XmlElement("MI_APPLICATION_REQUEST_DETAIL", Order = 1)]
        public MI_APPLICATION_REQUEST_DETAIL MIApplicationRequestDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the Mortgage Insurance Application Request Detail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Mortgage Insurance Application Request Detail element has been assigned a value.</value>
        [XmlIgnore]
        public bool MiApplicationRequestDetailSpecified
        {
            get { return this.MIApplicationRequestDetail != null && this.MIApplicationRequestDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A list of MI products.
        /// </summary>
        [XmlElement("MI_PRODUCTS", Order = 2)]
        public MI_PRODUCTS MIProducts;

        /// <summary>
        /// Gets or sets a value indicating whether the Mortgage Insurance Products element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Mortgage Insurance Products element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIProductsSpecified
        {
            get { return this.MIProducts != null && this.MIProducts.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public MI_APPLICATION_REQUEST_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
