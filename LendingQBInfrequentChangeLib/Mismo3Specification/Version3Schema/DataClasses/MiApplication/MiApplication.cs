namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class MI_APPLICATION
    {
        /// <summary>
        /// Gets a value indicating whether the MI_APPLICATION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.MIApplicationTypeOtherDescriptionSpecified
                    || this.MIApplicationTypeSpecified
                    || this.MICertificateIdentifierSpecified
                    || this.MILenderIdentifierSpecified
                    || this.MIServiceTypeOtherDescriptionSpecified
                    || this.MIServiceTypeSpecified
                    || this.MITransactionIdentifierSpecified;
            }
        }

        /// <summary>
        /// Specifies the way the lender directs the mortgage insurer to handle the processing of the loan.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<MIApplicationBase> MIApplicationType;

        /// <summary>
        /// Gets or sets a value indicating whether the MIApplicationType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIApplicationType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIApplicationTypeSpecified
        {
            get { return this.MIApplicationType != null && this.MIApplicationType.enumValue != MIApplicationBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture the description of the type of MI Application if Other is selected as the MIApplicationType.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString MIApplicationTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MIApplicationTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIApplicationTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIApplicationTypeOtherDescriptionSpecified
        {
            get { return MIApplicationTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The number assigned by the private mortgage insurance company to track a loan.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOIdentifier MICertificateIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the MICertificateIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MICertificateIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool MICertificateIdentifierSpecified
        {
            get { return MICertificateIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The unique number assigned by the private mortgage insurer identifying the lender entity.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOIdentifier MILenderIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the MILenderIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MILenderIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool MILenderIdentifierSpecified
        {
            get { return MILenderIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Defines the type of service provided by the mortgage insurer.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<MIServiceBase> MIServiceType;

        /// <summary>
        /// Gets or sets a value indicating whether the MIServiceType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIServiceType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIServiceTypeSpecified
        {
            get { return this.MIServiceType != null && this.MIServiceType.enumValue != MIServiceBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the MI Service Type when Other is selected for the MI Service Type.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString MIServiceTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MIServiceTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIServiceTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIServiceTypeOtherDescriptionSpecified
        {
            get { return MIServiceTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A unique tracking identifier assigned by the sender which is typically returned to the sender in the resulting response.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOIdentifier MITransactionIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the MITransactionIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MITransactionIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool MITransactionIdentifierSpecified
        {
            get { return MITransactionIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 7)]
        public MI_APPLICATION_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
