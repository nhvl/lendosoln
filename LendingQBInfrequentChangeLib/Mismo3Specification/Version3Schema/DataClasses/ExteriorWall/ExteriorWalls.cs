namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class EXTERIOR_WALLS
    {
        /// <summary>
        /// Gets a value indicating whether the EXTERIOR_WALLS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ExteriorWallSpecified;
            }
        }

        /// <summary>
        /// A collection of exterior walls.
        /// </summary>
        [XmlElement("EXTERIOR_WALL", Order = 0)]
		public List<EXTERIOR_WALL> ExteriorWall = new List<EXTERIOR_WALL>();

        /// <summary>
        /// Gets or sets a value indicating whether the ExteriorWall element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ExteriorWall element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExteriorWallSpecified
        {
            get { return this.ExteriorWall != null && this.ExteriorWall.Count(e => e != null && e.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public EXTERIOR_WALLS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
