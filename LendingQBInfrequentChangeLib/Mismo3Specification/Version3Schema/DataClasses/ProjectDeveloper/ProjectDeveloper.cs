namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PROJECT_DEVELOPER
    {
        /// <summary>
        /// Gets a value indicating whether the PROJECT_DEVELOPER container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ProjectDeveloperControlOfProjectManagementDescriptionSpecified
                    || this.ProjectDeveloperControlsProjectManagementIndicatorSpecified
                    || this.ProjectDeveloperTransferConcessionsDescriptionSpecified
                    || this.ProjectDeveloperTransferConcessionsIndicatorSpecified;
            }
        }

        /// <summary>
        /// Describes the nature of the control the developer has over the project management (e.g. the Homeowners Association).
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString ProjectDeveloperControlOfProjectManagementDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectDeveloperControlOfProjectManagementDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectDeveloperControlOfProjectManagementDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectDeveloperControlOfProjectManagementDescriptionSpecified
        {
            get { return ProjectDeveloperControlOfProjectManagementDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates that developer is in control of the management of the project (e.g. Home Owners Association).
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOIndicator ProjectDeveloperControlsProjectManagementIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectDeveloperControlsProjectManagementIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectDeveloperControlsProjectManagementIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectDeveloperControlsProjectManagementIndicatorSpecified
        {
            get { return ProjectDeveloperControlsProjectManagementIndicator != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field describing the concessions by the developer to support or promote the transfer of ownership (e.g. cooperative stock).
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString ProjectDeveloperTransferConcessionsDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectDeveloperTransferConcessionsDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectDeveloperTransferConcessionsDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectDeveloperTransferConcessionsDescriptionSpecified
        {
            get { return ProjectDeveloperTransferConcessionsDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the developer offers concessions to support or promote the transfer of ownership (e.g. cooperative stock).
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOIndicator ProjectDeveloperTransferConcessionsIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectDeveloperTransferConcessionsIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectDeveloperTransferConcessionsIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectDeveloperTransferConcessionsIndicatorSpecified
        {
            get { return ProjectDeveloperTransferConcessionsIndicator != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public PROJECT_DEVELOPER_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
