namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class HUD1_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the HUD1_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.HUD1CashToOrFromBorrowerIndicatorSpecified
                    || this.HUD1CashToOrFromSellerIndicatorSpecified
                    || this.HUD1ConventionalInsuredIndicatorSpecified
                    || this.HUD1FileNumberIdentifierSpecified
                    || this.HUD1SettlementDateSpecified;
            }
        }

        /// <summary>
        /// When True, indicates that cash is due to the Borrower. When False, indicates that cash is due from the borrower. This field is for use as required on the HUD-1. 
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIndicator HUD1CashToOrFromBorrowerIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the HUD1CashToOrFromBorrowerIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HUD1CashToOrFromBorrowerIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool HUD1CashToOrFromBorrowerIndicatorSpecified
        {
            get { return HUD1CashToOrFromBorrowerIndicator != null; }
            set { }
        }

        /// <summary>
        /// When True, indicates that cash is due to the Seller. When False, indicates that cash is due from the Seller. This field is for use as required on the HUD-1. 
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOIndicator HUD1CashToOrFromSellerIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the HUD1CashToOrFromSellerIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HUD1CashToOrFromSellerIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool HUD1CashToOrFromSellerIndicatorSpecified
        {
            get { return HUD1CashToOrFromSellerIndicator != null; }
            set { }
        }

        /// <summary>
        /// When True, this field is used to indicate, as required on the HUD-1, that the loan transaction is a conventional loan with Mortgage Insurance required. 
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOIndicator HUD1ConventionalInsuredIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the HUD1ConventionalInsuredIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HUD1ConventionalInsuredIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool HUD1ConventionalInsuredIndicatorSpecified
        {
            get { return HUD1ConventionalInsuredIndicator != null; }
            set { }
        }

        /// <summary>
        /// The File Number listed on the HUD-1 Section B, item number 6. 
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOIdentifier HUD1FileNumberIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the HUD1FileNumberIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HUD1FileNumberIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool HUD1FileNumberIdentifierSpecified
        {
            get { return HUD1FileNumberIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A placeholder for the date specified as the Settlement Date on the HUD-1. This field contains the one date of multiple dates associated with the loan transaction (e.g., Disbursement Date, Closing Date, Recording Date) that is identified as the Settlement Date as required on the HUD-1. 
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMODate HUD1SettlementDate;

        /// <summary>
        /// Gets or sets a value indicating whether the HUD1SettlementDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HUD1SettlementDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool HUD1SettlementDateSpecified
        {
            get { return HUD1SettlementDate != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 5)]
        public HUD1_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
