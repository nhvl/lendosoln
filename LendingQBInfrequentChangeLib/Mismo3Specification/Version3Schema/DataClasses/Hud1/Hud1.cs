namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class HUD1
    {
        /// <summary>
        /// Gets a value indicating whether the HUD1 container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.Hud1DetailSpecified
                    || this.Hud1LineItemsSpecified;
            }
        }

        /// <summary>
        /// Details on the HUD1.
        /// </summary>
        [XmlElement("HUD1_DETAIL", Order = 0)]
        public HUD1_DETAIL Hud1Detail;

        /// <summary>
        /// Gets or sets a value indicating whether the HUD1Detail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HUD1Detail element has been assigned a value.</value>
        [XmlIgnore]
        public bool Hud1DetailSpecified
        {
            get { return this.Hud1Detail != null && this.Hud1Detail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A list of HUD1 line items.
        /// </summary>
        [XmlElement("HUD1_LINE_ITEMS", Order = 1)]
        public HUD1_LINE_ITEMS Hud1LineItems;

        /// <summary>
        /// Gets or sets a value indicating whether the HUD1LineItems element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HUD1LineItems element has been assigned a value.</value>
        [XmlIgnore]
        public bool Hud1LineItemsSpecified
        {
            get { return this.Hud1LineItems != null && this.Hud1LineItems.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public HUD1_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
