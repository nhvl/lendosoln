namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class CURRENT_INCOME_ITEM
    {
        /// <summary>
        /// Gets a value indicating whether the CURRENT_INCOME_ITEM container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CurrentIncomeItemDetailSpecified
                    || this.ExtensionSpecified
                    || this.IncomeDocumentationsSpecified
                    || this.VerificationSpecified;
            }
        }

        /// <summary>
        /// Details on a current income item.
        /// </summary>
        [XmlElement("CURRENT_INCOME_ITEM_DETAIL", Order = 0)]
        public CURRENT_INCOME_ITEM_DETAIL CurrentIncomeItemDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the CurrentIncomeItemDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CurrentIncomeItemDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool CurrentIncomeItemDetailSpecified
        {
            get { return this.CurrentIncomeItemDetail != null && this.CurrentIncomeItemDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Documentation of income.
        /// </summary>
        [XmlElement("INCOME_DOCUMENTATIONS", Order = 1)]
        public INCOME_DOCUMENTATIONS IncomeDocumentations;

        /// <summary>
        /// Gets or sets a value indicating whether the IncomeDocumentations element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IncomeDocumentations element has been assigned a value.</value>
        [XmlIgnore]
        public bool IncomeDocumentationsSpecified
        {
            get { return this.IncomeDocumentations != null && this.IncomeDocumentations.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Income verification.
        /// </summary>
        [XmlElement("VERIFICATION", Order = 2)]
        public VERIFICATION Verification;

        /// <summary>
        /// Gets or sets a value indicating whether the Verification element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Verification element has been assigned a value.</value>
        [XmlIgnore]
        public bool VerificationSpecified
        {
            get { return this.Verification != null && this.Verification.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public CURRENT_INCOME_ITEM_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
