namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CURRENT_INCOME_ITEMS
    {
        /// <summary>
        /// Gets a value indicating whether the CURRENT_INCOME_ITEMS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CurrentIncomeItemSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of current income items.
        /// </summary>
        [XmlElement("CURRENT_INCOME_ITEM", Order = 0)]
		public List<CURRENT_INCOME_ITEM> CurrentIncomeItem = new List<CURRENT_INCOME_ITEM>();

        /// <summary>
        /// Gets or sets a value indicating whether the CurrentIncomeItem element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CurrentIncomeItem element has been assigned a value.</value>
        [XmlIgnore]
        public bool CurrentIncomeItemSpecified
        {
            get { return this.CurrentIncomeItem != null && this.CurrentIncomeItem.Count(c => c != null && c.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public CURRENT_INCOME_ITEMS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
