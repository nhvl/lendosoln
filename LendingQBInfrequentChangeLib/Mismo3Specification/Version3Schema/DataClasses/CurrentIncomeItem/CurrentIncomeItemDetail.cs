namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CURRENT_INCOME_ITEM_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the CURRENT_INCOME_ITEM_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CurrentIncomeMonthlyTotalAmountSpecified
                    || this.ExtensionSpecified
                    || this.IncomeFederalTaxExemptIndicatorSpecified
                    || this.IncomeTypeOtherDescriptionSpecified
                    || this.IncomeTypeSpecified;
            }
        }

        /// <summary>
        /// The total dollar amount per month of the borrowers current income of a single Income Type from both primary and secondary sources.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount CurrentIncomeMonthlyTotalAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the CurrentIncomeMonthlyTotalAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CurrentIncomeMonthlyTotalAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool CurrentIncomeMonthlyTotalAmountSpecified
        {
            get { return CurrentIncomeMonthlyTotalAmount != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the income is legally exempt from federal tax.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOIndicator IncomeFederalTaxExemptIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the IncomeFederalTaxExemptIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IncomeFederalTaxExemptIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool IncomeFederalTaxExemptIndicatorSpecified
        {
            get { return IncomeFederalTaxExemptIndicator != null; }
            set { }
        }

        /// <summary>
        /// Specifies the general names (types) of items commonly listed as income of the borrower in a mortgage loan transaction.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<IncomeBase> IncomeType;

        /// <summary>
        /// Gets or sets a value indicating whether the IncomeType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IncomeType element has been assigned a value.</value>
        [XmlIgnore]
        public bool IncomeTypeSpecified
        {
            get { return this.IncomeType != null && this.IncomeType.enumValue != IncomeBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Income Type.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString IncomeTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the IncomeTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IncomeTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool IncomeTypeOtherDescriptionSpecified
        {
            get { return IncomeTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public CURRENT_INCOME_ITEM_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
