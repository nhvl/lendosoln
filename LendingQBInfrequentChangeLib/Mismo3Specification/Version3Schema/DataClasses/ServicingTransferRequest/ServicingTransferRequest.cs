namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class SERVICING_TRANSFER_REQUEST
    {
        /// <summary>
        /// Gets a value indicating whether the SERVICING_TRANSFER_REQUEST container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DealSetSpecified
                    || this.ExtensionSpecified
                    || this.ServicingTransferRequestDetailSpecified;
            }
        }

        /// <summary>
        /// A set of loans.
        /// </summary>
        [XmlElement("DEAL_SET", Order = 0)]
        public DEAL_SET DealSet;

        /// <summary>
        /// Gets or sets a value indicating whether the DealSet element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DealSet element has been assigned a value.</value>
        [XmlIgnore]
        public bool DealSetSpecified
        {
            get { return this.DealSet != null && this.DealSet.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details on a servicing transfer request.
        /// </summary>
        [XmlElement("SERVICING_TRANSFER_REQUEST_DETAIL", Order = 1)]
        public SERVICING_TRANSFER_REQUEST_DETAIL ServicingTransferRequestDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the ServicingTransferRequestDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServicingTransferRequestDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServicingTransferRequestDetailSpecified
        {
            get { return this.ServicingTransferRequestDetail != null && this.ServicingTransferRequestDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public SERVICING_TRANSFER_REQUEST_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
