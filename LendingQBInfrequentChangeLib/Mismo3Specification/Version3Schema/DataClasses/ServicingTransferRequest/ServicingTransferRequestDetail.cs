namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class SERVICING_TRANSFER_REQUEST_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the SERVICING_TRANSFER_REQUEST_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ServicingTransferEffectiveDateSpecified
                    || this.TransferRequestProductIdentifierSpecified
                    || this.TransferRequestTypeOtherDescriptionSpecified
                    || this.TransferRequestTypeSpecified;
            }
        }

        /// <summary>
        /// The date at which the transfer of servicing is effective as reflected on the Notice Of Assignment, Sale, Or Transfer of Servicing Rights document.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMODate ServicingTransferEffectiveDate;

        /// <summary>
        /// Gets or sets a value indicating whether the ServicingTransferEffectiveDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServicingTransferEffectiveDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServicingTransferEffectiveDateSpecified
        {
            get { return ServicingTransferEffectiveDate != null; }
            set { }
        }

        /// <summary>
        /// Identifies the product type that is the subject of a servicing transfer request.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOIdentifier TransferRequestProductIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the TransferRequestProductIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TransferRequestProductIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool TransferRequestProductIdentifierSpecified
        {
            get { return TransferRequestProductIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Specifies the nature or extent of a servicing transfer request.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<TransferRequestBase> TransferRequestType;

        /// <summary>
        /// Gets or sets a value indicating whether the TransferRequestType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TransferRequestType element has been assigned a value.</value>
        [XmlIgnore]
        public bool TransferRequestTypeSpecified
        {
            get { return this.TransferRequestType != null && this.TransferRequestType.enumValue != TransferRequestBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected as the Transfer Request Type.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString TransferRequestTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the TransferRequestTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TransferRequestTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool TransferRequestTypeOtherDescriptionSpecified
        {
            get { return TransferRequestTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public SERVICING_TRANSFER_REQUEST_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
