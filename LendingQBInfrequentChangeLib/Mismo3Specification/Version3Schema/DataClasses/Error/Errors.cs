namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class ERRORS
    {
        /// <summary>
        /// Gets a value indicating whether the ERRORS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ErrorSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of errors.
        /// </summary>
        [XmlElement("ERROR", Order = 0)]
		public List<ERROR> Error = new List<ERROR>();

        /// <summary>
        /// Gets or sets a value indicating whether the Error element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Error element has been assigned a value.</value>
        [XmlIgnore]
        public bool ErrorSpecified
        {
            get { return this.Error != null && this.Error.Count(e => e != null && e.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public ERRORS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
