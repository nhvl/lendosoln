namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class RESPONDING_PARTY
    {
        /// <summary>
        /// Gets a value indicating whether the RESPONDING_PARTY container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.RespondingPartyTransactionIdentifierSpecified;
            }
        }

        /// <summary>
        /// The identifier created and used by the responding party to track a transaction or associated interactions.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIdentifier RespondingPartyTransactionIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the RespondingPartyTransactionIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RespondingPartyTransactionIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool RespondingPartyTransactionIdentifierSpecified
        {
            get { return RespondingPartyTransactionIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public RESPONDING_PARTY_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
