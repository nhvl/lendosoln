namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class STRUCTURE_ANALYSIS_SUMMARY
    {
        /// <summary>
        /// Gets a value indicating whether the STRUCTURE_ANALYSIS_SUMMARY container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AnalysisComponentActualTotalCostAmountSpecified
                    || this.AnalysisComponentEstimatedTotalCostAmountSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// The actual amount of the total cost of a structural components that are being analyzed. 
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount AnalysisComponentActualTotalCostAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the AnalysisComponentActualTotalCostAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AnalysisComponentActualTotalCostAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool AnalysisComponentActualTotalCostAmountSpecified
        {
            get { return AnalysisComponentActualTotalCostAmount != null; }
            set { }
        }

        /// <summary>
        /// The estimated amount of the total cost of a structural components that are being analyzed.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOAmount AnalysisComponentEstimatedTotalCostAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the AnalysisComponentEstimatedTotalCostAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AnalysisComponentEstimatedTotalCostAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool AnalysisComponentEstimatedTotalCostAmountSpecified
        {
            get { return AnalysisComponentEstimatedTotalCostAmount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public STRUCTURE_ANALYSIS_SUMMARY_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
