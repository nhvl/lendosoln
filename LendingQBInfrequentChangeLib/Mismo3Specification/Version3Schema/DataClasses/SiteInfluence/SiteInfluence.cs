namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class SITE_INFLUENCE
    {
        /// <summary>
        /// Gets a value indicating whether the SITE_INFLUENCE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.InfluenceImpactTypeSpecified
                    || this.SiteInfluenceDescriptionSpecified
                    || this.SiteInfluenceTypeOtherDescriptionSpecified
                    || this.SiteInfluenceTypeSpecified;
            }
        }

        /// <summary>
        /// Identifies the impact of the indicated Influence on the subject property as used in the appraiser's analysis.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<InfluenceImpactBase> InfluenceImpactType;

        /// <summary>
        /// Gets or sets a value indicating whether the InfluenceImpactType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InfluenceImpactType element has been assigned a value.</value>
        [XmlIgnore]
        public bool InfluenceImpactTypeSpecified
        {
            get { return this.InfluenceImpactType != null && this.InfluenceImpactType.enumValue != InfluenceImpactBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe factors affecting the site.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString SiteInfluenceDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the SiteInfluenceDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SiteInfluenceDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool SiteInfluenceDescriptionSpecified
        {
            get { return SiteInfluenceDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies a type of condition that affects the property.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<SiteInfluenceBase> SiteInfluenceType;

        /// <summary>
        /// Gets or sets a value indicating whether the SiteInfluenceType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SiteInfluenceType element has been assigned a value.</value>
        [XmlIgnore]
        public bool SiteInfluenceTypeSpecified
        {
            get { return this.SiteInfluenceType != null && this.SiteInfluenceType.enumValue != SiteInfluenceBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Site Influence Type. 
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString SiteInfluenceTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the SiteInfluenceTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SiteInfluenceTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool SiteInfluenceTypeOtherDescriptionSpecified
        {
            get { return SiteInfluenceTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public SITE_INFLUENCE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
