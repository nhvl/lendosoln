namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class GENERAL_IDENTIFIER
    {
        /// <summary>
        /// Gets a value indicating whether the GENERAL_IDENTIFIER container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CongressionalDistrictIdentifierSpecified
                    || this.CoreBasedStatisticalAreaCodeSpecified
                    || this.CoreBasedStatisticalAreaDivisionCodeSpecified
                    || this.ExtensionSpecified
                    || this.JudicialDistrictNameSpecified
                    || this.JudicialDivisionNameSpecified
                    || this.MapReferenceIdentifierSpecified
                    || this.MapReferenceSecondIdentifierSpecified
                    || this.MSAIdentifierSpecified
                    || this.MunicipalityNameSpecified
                    || this.RecordingJurisdictionNameSpecified
                    || this.RecordingJurisdictionTypeOtherDescriptionSpecified
                    || this.RecordingJurisdictionTypeSpecified
                    || this.SchoolDistrictNameSpecified
                    || this.UnincorporatedAreaNameSpecified;
            }
        }

        /// <summary>
        ///  A two-digit numeric code to identify each Congressional District within a State. These codes are unique within each State. A congressional district identifies an electoral constituency that elects a single member of a congress.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIdentifier CongressionalDistrictIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the CongressionalDistrictIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CongressionalDistrictIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool CongressionalDistrictIdentifierSpecified
        {
            get { return CongressionalDistrictIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A five digit code representing a functional region based around an urban center of at least 10,000 people, based on standards published by the Office of Management and Budget (OMB) in 2000. The CBSA code refers collectively to metropolitan statistical areas.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOCode CoreBasedStatisticalAreaCode;

        /// <summary>
        /// Gets or sets a value indicating whether the CoreBasedStatisticalAreaCode element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CoreBasedStatisticalAreaCode element has been assigned a value.</value>
        [XmlIgnore]
        public bool CoreBasedStatisticalAreaCodeSpecified
        {
            get { return CoreBasedStatisticalAreaCode != null; }
            set { }
        }

        /// <summary>
        /// Metropolitan Divisions are defined within Metropolitan Statistical Areas that have a single core with a population of at least 2.5 million. Not all Metropolitan Statistical Areas with urbanized areas of this size will contain Metropolitan Divisions. Metropolitan Division codes are distinguished by a five digit code ending in 4.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOCode CoreBasedStatisticalAreaDivisionCode;

        /// <summary>
        /// Gets or sets a value indicating whether the CoreBasedStatisticalAreaDivisionCode element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CoreBasedStatisticalAreaDivisionCode element has been assigned a value.</value>
        [XmlIgnore]
        public bool CoreBasedStatisticalAreaDivisionCodeSpecified
        {
            get { return CoreBasedStatisticalAreaDivisionCode != null; }
            set { }
        }

        /// <summary>
        /// The name of the Judicial District in which the subject property resides.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString JudicialDistrictName;

        /// <summary>
        /// Gets or sets a value indicating whether the JudicialDistrictName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the JudicialDistrictName element has been assigned a value.</value>
        [XmlIgnore]
        public bool JudicialDistrictNameSpecified
        {
            get { return JudicialDistrictName != null; }
            set { }
        }

        /// <summary>
        /// The name of the Judicial Division in which the subject property resides.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString JudicialDivisionName;

        /// <summary>
        /// Gets or sets a value indicating whether the JudicialDivisionName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the JudicialDivisionName element has been assigned a value.</value>
        [XmlIgnore]
        public bool JudicialDivisionNameSpecified
        {
            get { return JudicialDivisionName != null; }
            set { }
        }

        /// <summary>
        /// A reference to a regionally specific map document that assists in locating a property. May refer to locally available published map products (e.g. Thomas Map in CA) or a county tax map.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOIdentifier MapReferenceIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the MapReferenceIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MapReferenceIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool MapReferenceIdentifierSpecified
        {
            get { return MapReferenceIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A secondary reference to a regionally specific map document that assists in locating a property. May refer to locally available published map products (e.g. Thomas Map in CA) or a county tax map.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOIdentifier MapReferenceSecondIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the MapReferenceSecondIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MapReferenceSecondIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool MapReferenceSecondIdentifierSpecified
        {
            get { return MapReferenceSecondIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Identifies Metropolitan Statistical Area (MSA) where subject property is located. A MSA is a contiguous geographic area consisting of an urban center city and its surrounding suburbs.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOIdentifier MSAIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the MSAIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MSAIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool MSAIdentifierSpecified
        {
            get { return MSAIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The name of the municipality in which property is located.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOString MunicipalityName;

        /// <summary>
        /// Gets or sets a value indicating whether the MunicipalityName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MunicipalityName element has been assigned a value.</value>
        [XmlIgnore]
        public bool MunicipalityNameSpecified
        {
            get { return MunicipalityName != null; }
            set { }
        }

        /// <summary>
        /// The name of the Recording Jurisdiction in which the subject property resides.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOString RecordingJurisdictionName;

        /// <summary>
        /// Gets or sets a value indicating whether the RecordingJurisdictionName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RecordingJurisdictionName element has been assigned a value.</value>
        [XmlIgnore]
        public bool RecordingJurisdictionNameSpecified
        {
            get { return RecordingJurisdictionName != null; }
            set { }
        }

        /// <summary>
        /// The type of Recording Jurisdiction in which the subject property is located.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOEnum<RecordingJurisdictionBase> RecordingJurisdictionType;

        /// <summary>
        /// Gets or sets a value indicating whether the RecordingJurisdictionType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RecordingJurisdictionType element has been assigned a value.</value>
        [XmlIgnore]
        public bool RecordingJurisdictionTypeSpecified
        {
            get { return this.RecordingJurisdictionType != null && this.RecordingJurisdictionType.enumValue != RecordingJurisdictionBase.Blank; }
            set { }
        }

        /// <summary>
        /// The description of the Recording Jurisdiction Type when Other is selected from the enumerated list.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOString RecordingJurisdictionTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the RecordingJurisdictionTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RecordingJurisdictionTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool RecordingJurisdictionTypeOtherDescriptionSpecified
        {
            get { return RecordingJurisdictionTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The name of the school district in which the property is located.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOString SchoolDistrictName;

        /// <summary>
        /// Gets or sets a value indicating whether the SchoolDistrictName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SchoolDistrictName element has been assigned a value.</value>
        [XmlIgnore]
        public bool SchoolDistrictNameSpecified
        {
            get { return SchoolDistrictName != null; }
            set { }
        }

        /// <summary>
        /// Area where the property resides if it is not incorporated into a city, town, village, etc.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOString UnincorporatedAreaName;

        /// <summary>
        /// Gets or sets a value indicating whether the UnincorporatedAreaName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the UnincorporatedAreaName element has been assigned a value.</value>
        [XmlIgnore]
        public bool UnincorporatedAreaNameSpecified
        {
            get { return UnincorporatedAreaName != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 14)]
        public GENERAL_IDENTIFIER_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
