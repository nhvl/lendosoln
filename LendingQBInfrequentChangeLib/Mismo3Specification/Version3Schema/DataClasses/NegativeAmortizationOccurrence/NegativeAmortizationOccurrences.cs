namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class NEGATIVE_AMORTIZATION_OCCURRENCES
    {
        /// <summary>
        /// Gets a value indicating whether the NEGATIVE_AMORTIZATION_OCCURRENCES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.NegativeAmortizationOccurrenceSpecified;
            }
        }

        /// <summary>
        /// A collection of negative amortization occurrences.
        /// </summary>
        [XmlElement("NEGATIVE_AMORTIZATION_OCCURRENCE", Order = 0)]
        public List<NEGATIVE_AMORTIZATION_OCCURRENCE> NegativeAmortizationOccurrence = new List<NEGATIVE_AMORTIZATION_OCCURRENCE>();

        /// <summary>
        /// Gets or sets a value indicating whether the NegativeAmortizationOccurrence element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NegativeAmortizationOccurrence element has been assigned a value.</value>
        [XmlIgnore]
        public bool NegativeAmortizationOccurrenceSpecified
        {
            get { return this.NegativeAmortizationOccurrence != null && this.NegativeAmortizationOccurrence.Count(n => n != null && n.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public NEGATIVE_AMORTIZATION_OCCURRENCES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
