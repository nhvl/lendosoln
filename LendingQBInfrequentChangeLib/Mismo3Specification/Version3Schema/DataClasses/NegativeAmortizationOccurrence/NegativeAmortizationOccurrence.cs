namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class NEGATIVE_AMORTIZATION_OCCURRENCE
    {
        /// <summary>
        /// Gets a value indicating whether the NEGATIVE_AMORTIZATION_OCCURRENCE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CurrentMonthlyNegativeAmortizationAmountSpecified
                    || this.CurrentNegativeAmortizationBalanceAmountSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// The dollar amount of negative amortization for the current activity period.  
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount CurrentMonthlyNegativeAmortizationAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the CurrentMonthlyNegativeAmortizationAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CurrentMonthlyNegativeAmortizationAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool CurrentMonthlyNegativeAmortizationAmountSpecified
        {
            get { return CurrentMonthlyNegativeAmortizationAmount != null; }
            set { }
        }

        /// <summary>
        /// The current balance of interest accumulated according to the note terms permitting negative amortization for a loan or loan component.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOAmount CurrentNegativeAmortizationBalanceAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the CurrentNegativeAmortizationBalanceAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CurrentNegativeAmortizationBalanceAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool CurrentNegativeAmortizationBalanceAmountSpecified
        {
            get { return CurrentNegativeAmortizationBalanceAmount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public NEGATIVE_AMORTIZATION_OCCURRENCE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
