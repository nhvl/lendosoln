namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PRINCIPAL_AND_INTEREST_PAYMENT_ADJUSTMENT
    {
        /// <summary>
        /// Gets a value indicating whether the PRINCIPAL_AND_INTEREST_PAYMENT_ADJUSTMENT container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.IndexRulesSpecified
                    || this.PrincipalAndInterestAdjustmentLimitedPaymentOptionsSpecified
                    || this.PrincipalAndInterestPaymentLifetimeAdjustmentRuleSpecified
                    || this.PrincipalAndInterestPaymentPerChangeAdjustmentRulesSpecified
                    || this.PrincipalAndInterestPaymentPeriodicAdjustmentRulesSpecified;
            }
        }

        /// <summary>
        /// Rules related to the index.
        /// </summary>
        [XmlElement("INDEX_RULES", Order = 0)]
        public INDEX_RULES IndexRules;

        /// <summary>
        /// Gets or sets a value indicating whether the IndexRules element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IndexRules element has been assigned a value.</value>
        [XmlIgnore]
        public bool IndexRulesSpecified
        {
            get { return this.IndexRules != null && this.IndexRules.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Limited payment options for the principal and interest payment.
        /// </summary>
        [XmlElement("PRINCIPAL_AND_INTEREST_ADJUSTMENT_LIMITED_PAYMENT_OPTIONS", Order = 1)]
        public PRINCIPAL_AND_INTEREST_ADJUSTMENT_LIMITED_PAYMENT_OPTIONS PrincipalAndInterestAdjustmentLimitedPaymentOptions;

        /// <summary>
        /// Gets or sets a value indicating whether the PrincipalAndInterestAdjustmentLimitedPaymentOptions element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrincipalAndInterestAdjustmentLimitedPaymentOptions element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrincipalAndInterestAdjustmentLimitedPaymentOptionsSpecified
        {
            get { return this.PrincipalAndInterestAdjustmentLimitedPaymentOptions != null && this.PrincipalAndInterestAdjustmentLimitedPaymentOptions.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Lifetime adjustment rules for the principal and interest payment.
        /// </summary>
        [XmlElement("PRINCIPAL_AND_INTEREST_PAYMENT_LIFETIME_ADJUSTMENT_RULE", Order = 2)]
        public PRINCIPAL_AND_INTEREST_PAYMENT_LIFETIME_ADJUSTMENT_RULE PrincipalAndInterestPaymentLifetimeAdjustmentRule;

        /// <summary>
        /// Gets or sets a value indicating whether the PrincipalAndInterestPaymentLifetimeAdjustmentRule element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrincipalAndInterestPaymentLifetimeAdjustmentRule element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrincipalAndInterestPaymentLifetimeAdjustmentRuleSpecified
        {
            get { return this.PrincipalAndInterestPaymentLifetimeAdjustmentRule != null && this.PrincipalAndInterestPaymentLifetimeAdjustmentRule.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Rules per change for the principal and interest payment.
        /// </summary>
        [XmlElement("PRINCIPAL_AND_INTEREST_PAYMENT_PER_CHANGE_ADJUSTMENT_RULES", Order = 3)]
        public PRINCIPAL_AND_INTEREST_PAYMENT_PER_CHANGE_ADJUSTMENT_RULES PrincipalAndInterestPaymentPerChangeAdjustmentRules;

        /// <summary>
        /// Gets or sets a value indicating whether the PrincipalAndInterestPaymentPerChangeAdjustmentRules element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrincipalAndInterestPaymentPerChangeAdjustmentRules element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrincipalAndInterestPaymentPerChangeAdjustmentRulesSpecified
        {
            get { return this.PrincipalAndInterestPaymentPerChangeAdjustmentRules != null && this.PrincipalAndInterestPaymentPerChangeAdjustmentRules.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Rules for the periodic adjustment of the principal and interest payment.
        /// </summary>
        [XmlElement("PRINCIPAL_AND_INTEREST_PAYMENT_PERIODIC_ADJUSTMENT_RULES", Order = 4)]
        public PRINCIPAL_AND_INTEREST_PAYMENT_PERIODIC_ADJUSTMENT_RULES PrincipalAndInterestPaymentPeriodicAdjustmentRules;

        /// <summary>
        /// Gets or sets a value indicating whether the PrincipalAndInterestPaymentPeriodicAdjustmentRules element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrincipalAndInterestPaymentPeriodicAdjustmentRules element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrincipalAndInterestPaymentPeriodicAdjustmentRulesSpecified
        {
            get { return this.PrincipalAndInterestPaymentPeriodicAdjustmentRules != null && this.PrincipalAndInterestPaymentPeriodicAdjustmentRules.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 5)]
        public PRINCIPAL_AND_INTEREST_PAYMENT_ADJUSTMENT_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
