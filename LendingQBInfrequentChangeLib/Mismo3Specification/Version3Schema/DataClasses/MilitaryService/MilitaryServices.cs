namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class MILITARY_SERVICES
    {
        /// <summary>
        /// Gets a value indicating whether the MILITARY_SERVICES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.MilitaryServiceSpecified;
            }
        }

        /// <summary>
        /// A collection of military services.
        /// </summary>
        [XmlElement("MILITARY_SERVICE", Order = 0)]
		public List<MILITARY_SERVICE> MilitaryService = new List<MILITARY_SERVICE>();

        /// <summary>
        /// Gets or sets a value indicating whether the MilitaryService element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MilitaryService element has been assigned a value.</value>
        [XmlIgnore]
        public bool MilitaryServiceSpecified
        {
            get { return this.MilitaryService != null && this.MilitaryService.Count(m => m != null && m.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public MILITARY_SERVICES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
