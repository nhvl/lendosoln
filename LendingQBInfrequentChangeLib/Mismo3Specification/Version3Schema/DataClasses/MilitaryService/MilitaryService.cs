namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class MILITARY_SERVICE
    {
        /// <summary>
        /// Gets a value indicating whether the MILITARY_SERVICE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.MilitaryBranchTypeOtherDescriptionSpecified
                    || this.MilitaryBranchTypeSpecified
                    || this.MilitaryServiceFromDateSpecified
                    || this.MilitaryServiceNumberIdentifierSpecified
                    || this.MilitaryServiceServedAsNameSpecified
                    || this.MilitaryServiceToDateSpecified
                    || this.MilitaryStatusTypeOtherDescriptionSpecified
                    || this.MilitaryStatusTypeSpecified
                    || this.VADisabilityBenefitClaimIndicatorSpecified
                    || this.VAEligibilityIndicatorSpecified;
            }
        }

        /// <summary>
        /// Specifies the branch of service to which the VA borrower belongs.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<MilitaryBranchBase> MilitaryBranchType;

        /// <summary>
        /// Gets or sets a value indicating whether the MilitaryBranchType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MilitaryBranchType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MilitaryBranchTypeSpecified
        {
            get { return this.MilitaryBranchType != null && this.MilitaryBranchType.enumValue != MilitaryBranchBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field which contains the description of the branch of the military to which the VA borrower belongs when Other is selected as the Military Branch Type.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString MilitaryBranchTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MilitaryBranchTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MilitaryBranchTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MilitaryBranchTypeOtherDescriptionSpecified
        {
            get { return MilitaryBranchTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The date this period of service began.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMODate MilitaryServiceFromDate;

        /// <summary>
        /// Gets or sets a value indicating whether the MilitaryServiceFromDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MilitaryServiceFromDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool MilitaryServiceFromDateSpecified
        {
            get { return MilitaryServiceFromDate != null; }
            set { }
        }

        /// <summary>
        /// VA military service identification number of the borrower.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOIdentifier MilitaryServiceNumberIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the MilitaryServiceNumberIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MilitaryServiceNumberIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool MilitaryServiceNumberIdentifierSpecified
        {
            get { return MilitaryServiceNumberIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Name of veteran exactly as it appears on separation papers of Statement of Service.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString MilitaryServiceServedAsName;

        /// <summary>
        /// Gets or sets a value indicating whether the MilitaryServiceServedAsName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MilitaryServiceServedAsName element has been assigned a value.</value>
        [XmlIgnore]
        public bool MilitaryServiceServedAsNameSpecified
        {
            get { return MilitaryServiceServedAsName != null; }
            set { }
        }

        /// <summary>
        /// The date this period of service ended.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMODate MilitaryServiceToDate;

        /// <summary>
        /// Gets or sets a value indicating whether the MilitaryServiceToDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MilitaryServiceToDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool MilitaryServiceToDateSpecified
        {
            get { return MilitaryServiceToDate != null; }
            set { }
        }

        /// <summary>
        /// Specifies the current military status of the VA borrower.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOEnum<MilitaryStatusBase> MilitaryStatusType;

        /// <summary>
        /// Gets or sets a value indicating whether the MilitaryStatusType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MilitaryStatusType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MilitaryStatusTypeSpecified
        {
            get { return this.MilitaryStatusType != null && this.MilitaryStatusType.enumValue != MilitaryStatusBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field which contains the military status type of the VA when Other is selected as the Military Status Type.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOString MilitaryStatusTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MilitaryStatusTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MilitaryStatusTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MilitaryStatusTypeOtherDescriptionSpecified
        {
            get { return MilitaryStatusTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates the borrower has filed a claim for VA disability benefits prior to discharge from active duty service.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOIndicator VADisabilityBenefitClaimIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the VADisabilityBenefitClaimIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the VADisabilityBenefitClaimIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool VADisabilityBenefitClaimIndicatorSpecified
        {
            get { return VADisabilityBenefitClaimIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that this instance of military service is used to determine VA eligibility.
        /// NOTE: Only one instance of this container/element should have this set to true.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOIndicator VAEligibilityIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the VAEligibilityIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the VAEligibilityIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool VAEligibilityIndicatorSpecified
        {
            get { return VAEligibilityIndicator != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 10)]
        public MILITARY_SERVICE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
