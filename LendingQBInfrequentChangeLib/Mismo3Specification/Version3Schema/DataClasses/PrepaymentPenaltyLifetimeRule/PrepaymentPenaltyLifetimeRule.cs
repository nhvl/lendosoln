namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PREPAYMENT_PENALTY_LIFETIME_RULE
    {
        /// <summary>
        /// Gets a value indicating whether the PREPAYMENT_PENALTY_LIFETIME_RULE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.PrepaymentFinanceChargeRefundableIndicatorSpecified
                    || this.PrepaymentPenaltyDisclosureDateSpecified
                    || this.PrepaymentPenaltyExpirationDateSpecified
                    || this.PrepaymentPenaltyExpirationMonthsCountSpecified
                    || this.PrepaymentPenaltyMaximumLifeOfLoanAmountSpecified
                    || this.PrepaymentPenaltyTextDescriptionSpecified
                    || this.PrepaymentPenaltyWaiverTypeOtherDescriptionSpecified
                    || this.PrepaymentPenaltyWaiverTypeSpecified;
            }
        }

        /// <summary>
        /// Indicates whether the finance charge is refundable if the loan is paid in full before the maturity date.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIndicator PrepaymentFinanceChargeRefundableIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaymentFinanceChargeRefundableIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaymentFinanceChargeRefundableIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaymentFinanceChargeRefundableIndicatorSpecified
        {
            get { return PrepaymentFinanceChargeRefundableIndicator != null; }
            set { }
        }

        /// <summary>
        /// The date the prepayment penalty was acknowledged by borrower.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMODate PrepaymentPenaltyDisclosureDate;

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaymentPenaltyDisclosureDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaymentPenaltyDisclosureDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaymentPenaltyDisclosureDateSpecified
        {
            get { return PrepaymentPenaltyDisclosureDate != null; }
            set { }
        }

        /// <summary>
        /// The last date when a prepayment penalty may be imposed.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMODate PrepaymentPenaltyExpirationDate;

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaymentPenaltyExpirationDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaymentPenaltyExpirationDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaymentPenaltyExpirationDateSpecified
        {
            get { return PrepaymentPenaltyExpirationDate != null; }
            set { }
        }

        /// <summary>
        /// The number of months from the loan closing that a prepayment penalty may be imposed.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOCount PrepaymentPenaltyExpirationMonthsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaymentPenaltyExpirationMonthsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaymentPenaltyExpirationMonthsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaymentPenaltyExpirationMonthsCountSpecified
        {
            get { return PrepaymentPenaltyExpirationMonthsCount != null; }
            set { }
        }

        /// <summary>
        /// Maximum dollar amount that can be charged as a prepayment penalty over the life of the loan.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOAmount PrepaymentPenaltyMaximumLifeOfLoanAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaymentPenaltyMaximumLifeOfLoanAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaymentPenaltyMaximumLifeOfLoanAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaymentPenaltyMaximumLifeOfLoanAmountSpecified
        {
            get { return PrepaymentPenaltyMaximumLifeOfLoanAmount != null; }
            set { }
        }

        /// <summary>
        /// The free-form text which describes the prepayment penalty terms.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString PrepaymentPenaltyTextDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaymentPenaltyTextDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaymentPenaltyTextDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaymentPenaltyTextDescriptionSpecified
        {
            get { return PrepaymentPenaltyTextDescription != null; }
            set { }
        }

        /// <summary>
        /// Denotes conditions under which the prepayment penalty is waived.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOEnum<PrepaymentPenaltyWaiverBase> PrepaymentPenaltyWaiverType;

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaymentPenaltyWaiverType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaymentPenaltyWaiverType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaymentPenaltyWaiverTypeSpecified
        {
            get { return this.PrepaymentPenaltyWaiverType != null && this.PrepaymentPenaltyWaiverType.enumValue != PrepaymentPenaltyWaiverBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture additional information when Other is selected for Prepayment Penalty Waiver Type.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOString PrepaymentPenaltyWaiverTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaymentPenaltyWaiverTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaymentPenaltyWaiverTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaymentPenaltyWaiverTypeOtherDescriptionSpecified
        {
            get { return PrepaymentPenaltyWaiverTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 8)]
        public PREPAYMENT_PENALTY_LIFETIME_RULE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
