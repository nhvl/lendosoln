namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class ASSIGNMENT
    {
        /// <summary>
        /// Gets a value indicating whether the container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AssignmentDateSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// Date of assignment document.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMODate AssignmentDate;

        /// <summary>
        /// Gets or sets a value indicating whether the AssignmentDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AssignmentDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool AssignmentDateSpecified
        {
            get { return AssignmentDate != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public ASSIGNMENT_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
