namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class DEAL_SET_EXPENSE
    {
        /// <summary>
        /// Gets a value indicating whether the DEAL_SET_EXPENSE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DealSetExpenseAmountSpecified
                    || this.DealSetExpenseTypeOtherDescriptionSpecified
                    || this.DealSetExpenseTypeSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// The dollar amount associated with the Deal Set Expense Type.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount DealSetExpenseAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the DealSetExpenseAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DealSetExpenseAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool DealSetExpenseAmountSpecified
        {
            get { return DealSetExpenseAmount != null; }
            set { }
        }

        /// <summary>
        /// Specifies the type of expense for  reimbursed tasks or services performed.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<DealSetExpenseBase> DealSetExpenseType;

        /// <summary>
        /// Gets or sets a value indicating whether the DealSetExpenseType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DealSetExpenseType element has been assigned a value.</value>
        [XmlIgnore]
        public bool DealSetExpenseTypeSpecified
        {
            get { return this.DealSetExpenseType != null && this.DealSetExpenseType.enumValue != DealSetExpenseBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture additional information when Other is selected for Deal Set Expense Type.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString DealSetExpenseTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the DealSetExpenseTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DealSetExpenseTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool DealSetExpenseTypeOtherDescriptionSpecified
        {
            get { return DealSetExpenseTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public DEAL_SET_EXPENSE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
