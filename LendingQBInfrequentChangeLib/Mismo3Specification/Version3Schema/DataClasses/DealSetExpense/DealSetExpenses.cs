namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class DEAL_SET_EXPENSES
    {
        /// <summary>
        /// Gets a value indicating whether the DEAL_SET_EXPENSES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DealSetExpenseSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A list of expenses associated with a deal set.
        /// </summary>
        [XmlElement("DEAL_SET_EXPENSE", Order = 0)]
		public List<DEAL_SET_EXPENSE> DealSetExpense = new List<DEAL_SET_EXPENSE>();

        /// <summary>
        /// Gets or sets a value indicating whether the DealSetExpense element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DealSetExpense element has been assigned a value.</value>
        [XmlIgnore]
        public bool DealSetExpenseSpecified
        {
            get { return this.DealSetExpense != null && this.DealSetExpense.Count(d => d != null && d.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public DEAL_SET_EXPENSES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
