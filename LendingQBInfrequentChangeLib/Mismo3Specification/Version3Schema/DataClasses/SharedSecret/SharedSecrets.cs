namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class SHARED_SECRETS
    {
        /// <summary>
        /// Gets a value indicating whether the SHARED_SECRETS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.SharedSecretSpecified;
            }
        }

        /// <summary>
        /// A list of shared secrets.
        /// </summary>
        [XmlElement("SHARED_SECRET", Order = 0)]
		public List<SHARED_SECRET> SharedSecret = new List<SHARED_SECRET>();

        /// <summary>
        /// Gets or sets a value indicating whether the SharedSecret element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SharedSecret element has been assigned a value.</value>
        [XmlIgnore]
        public bool SharedSecretSpecified
        {
            get { return this.SharedSecret != null && this.SharedSecret.Count(s => s != null && s.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public SHARED_SECRETS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
