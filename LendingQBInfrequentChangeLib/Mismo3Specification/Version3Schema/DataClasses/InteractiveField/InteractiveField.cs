namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class INTERACTIVE_FIELD
    {
        /// <summary>
        /// Gets a value indicating whether the INTERACTIVE_FIELD container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.InteractiveFieldDetailSpecified
                    || this.InteractiveFieldReferenceSpecified;
            }
        }

        /// <summary>
        /// Details on an interactive field.
        /// </summary>
        [XmlElement("INTERACTIVE_FIELD_DETAIL", Order = 0)]
        public INTERACTIVE_FIELD_DETAIL InteractiveFieldDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the InteractiveFieldDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InteractiveFieldDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool InteractiveFieldDetailSpecified
        {
            get { return this.InteractiveFieldDetail != null && this.InteractiveFieldDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Reference to an interactive field.
        /// </summary>
        [XmlElement("INTERACTIVE_FIELD_REFERENCE", Order = 1)]
        public FIELD_REFERENCE InteractiveFieldReference;

        /// <summary>
        /// Gets or sets a value indicating whether the InteractiveFieldReference element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InteractiveFieldReference element has been assigned a value.</value>
        [XmlIgnore]
        public bool InteractiveFieldReferenceSpecified
        {
            get { return this.InteractiveFieldReference != null && this.InteractiveFieldReference.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public INTERACTIVE_FIELD_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
