namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class INTERACTIVE_FIELD_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the INTERACTIVE_FIELD_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.MutuallyExclusiveGroupNameSpecified;
            }
        }

        /// <summary>
        /// Specifies the name of the mutually exclusive group this interactive field belongs to. A mutually exclusive field group is the set of fields that share the same value for this element. At any given time, only one of the fields in the group can be checked. This feature is used to define interactive fields that are used to express a choice of the signer with regards to specific provisions offered in the document. 
        /// For example, one could create a field group to allow the signer to select the type of insurance coverage desired; there is one field associated with each coverage option, by clicking on one of the fields, the signer is declaring his/her option for the associate coverage option. 
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString MutuallyExclusiveGroupName;

        /// <summary>
        /// Gets or sets a value indicating whether the MutuallyExclusiveGroupName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MutuallyExclusiveGroupName element has been assigned a value.</value>
        [XmlIgnore]
        public bool MutuallyExclusiveGroupNameSpecified
        {
            get { return MutuallyExclusiveGroupName != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public INTERACTIVE_FIELD_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
