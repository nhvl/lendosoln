namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class DATA_ITEM_CHANGE_CONTEXT_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the DATA_ITEM_CHANGE_CONTEXT_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DataItemChangeContextDescriptionSpecified
                    || this.DataItemChangeContextXPathSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A description of the context for the included data item change containers.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString DataItemChangeContextDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the DataItemChangeContextDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DataItemChangeContextDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool DataItemChangeContextDescriptionSpecified
        {
            get { return DataItemChangeContextDescription != null; }
            set { }
        }

        /// <summary>
        /// An absolute XPath expression that further identifies the context for the included data item change containers (e.g. a way to express the Seller Servicer Number). . Unlike the Data Item Change Absolute Context Path Identifier this XPath expression will not necessarily create a valid XPath expression when combined with the relative XPath expression used in the individual data item changes. Please see the Implementation Guide for a description of the valid expressions and examples of its use.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOXPath DataItemChangeContextXPath;

        /// <summary>
        /// Gets or sets a value indicating whether the DataItemChangeContextXPath element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DataItemChangeContextXPath element has been assigned a value.</value>
        [XmlIgnore]
        public bool DataItemChangeContextXPathSpecified
        {
            get { return DataItemChangeContextXPath != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public DATA_ITEM_CHANGE_CONTEXT_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
