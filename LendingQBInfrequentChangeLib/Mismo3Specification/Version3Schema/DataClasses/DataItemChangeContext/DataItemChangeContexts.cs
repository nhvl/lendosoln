namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class DATA_ITEM_CHANGE_CONTEXTS
    {
        /// <summary>
        /// Gets a value indicating whether the DATA_ITEM_CHANGE_CONTEXTS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DataItemChangeContextSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of data item change contexts.
        /// </summary>
        [XmlElement("DATA_ITEM_CHANGE_CONTEXT", Order = 0)]
		public List<DATA_ITEM_CHANGE_CONTEXT> DataItemChangeContext = new List<DATA_ITEM_CHANGE_CONTEXT>();

        /// <summary>
        /// Gets or sets a value indicating whether the DataItemChangeContext element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DataItemChangeContext element has been assigned a value.</value>
        [XmlIgnore]
        public bool DataItemChangeContextSpecified
        {
            get { return this.DataItemChangeContext != null && this.DataItemChangeContext.Count(d => d != null && d.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public DATA_ITEM_CHANGE_CONTEXTS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
