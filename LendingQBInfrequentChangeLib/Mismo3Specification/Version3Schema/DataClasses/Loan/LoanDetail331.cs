namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class LOAN_DETAIL_3_3_1
    {
        /// <summary>
        /// Gets a value indicating whether the LOAN_DETAIL_3_3_1 container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ClosingDisclosureWaitingPeriodWaivedIndicatorSpecified
                    || this.ExtensionSpecified
                    || this.LoanEstimateWaitingPeriodWaivedIndicatorSpecified
                    || this.RightOfRescissionWaitingPeriodWaivedIndicatorSpecified;
            }
        }

        /// <summary>
        /// When true, indicates that upon receipt of the Closing Disclosure, and due to a bona fide personal financial emergency, the consumer modified or waived the three-business-day waiting period that must be met prior to consummation of the mortgage loan transaction.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIndicator ClosingDisclosureWaitingPeriodWaivedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ClosingDisclosureWaitingPeriodWaivedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ClosingDisclosureWaitingPeriodWaivedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ClosingDisclosureWaitingPeriodWaivedIndicatorSpecified
        {
            get { return ClosingDisclosureWaitingPeriodWaivedIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that upon receipt of the Loan Estimate, and due to a bona fide personal financial emergency, the consumer modified or waived the seven-business-day waiting period that must be met prior to consummation of the mortgage loan transaction.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOIndicator LoanEstimateWaitingPeriodWaivedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanEstimateWaitingPeriodWaivedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanEstimateWaitingPeriodWaivedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanEstimateWaitingPeriodWaivedIndicatorSpecified
        {
            get { return LoanEstimateWaitingPeriodWaivedIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that due to a bona fide personal financial emergency, the consumer modified or waived the right to rescind waiting period requirement that must be met prior to consummation of the mortgage loan transaction.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOIndicator RightOfRescissionWaitingPeriodWaivedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the RightOfRescissionWaitingPeriodWaivedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RightOfRescissionWaitingPeriodWaivedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool RightOfRescissionWaitingPeriodWaivedIndicatorSpecified
        {
            get { return RightOfRescissionWaitingPeriodWaivedIndicator != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public LOAN_DETAIL_3_3_1_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
