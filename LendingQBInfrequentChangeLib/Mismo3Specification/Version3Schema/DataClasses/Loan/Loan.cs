namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the Selection Type can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the Selection Type can be serialized.</returns>
    public partial class LOAN
    {
        /// <summary>
        /// Gets a value indicating whether the LOAN container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AchSpecified
                    || this.AdjustmentSpecified
                    || this.AffordableLendingSpecified
                    || this.AmortizationSpecified
                    || this.AssumabilitySpecified
                    || this.BillingAddressSpecified
                    || this.BuydownSpecified
                    || this.CapitalizationsSpecified
                    || this.ChargeOffSpecified
                    || this.ClosingInformationSpecified
                    || this.CollectionsSpecified
                    || this.ConstructionSpecified
                    || this.ContractVariancesSpecified
                    || this.CreditEnhancementsSpecified
                    || this.DocumentationsSpecified
                    || this.DocumentSpecificDataSetsSpecified
                    || this.DownPaymentsSpecified
                    || this.DrawSpecified
                    || this.EscrowSpecified
                    || this.ExtensionSpecified
                    || this.FeeInformationSpecified
                    || this.ForeclosuresSpecified
                    || this.GovernmentLoanSpecified
                    || this.HelocSpecified
                    || this.HighCostMortgagesSpecified
                    || this.HMDALoanSpecified
                    || this.HousingExpensesSpecified
                    || this.InsuranceClaimsSpecified
                    || this.InterestCalculationSpecified
                    || this.InterestOnlySpecified
                    || this.InvestorFeaturesSpecified
                    || this.InvestorLoanInformationSpecified
                    || this.IsIneligibleLoanProductSpecified
                    || this.LateChargeSpecified
                    || this.LoanCommentsSpecified
                    || this.LoanDetailSpecified
                    || this.LoanIdentifiersSpecified
                    || this.LoanLevelCreditSpecified
                    || this.LoanProductSpecified
                    || this.LoanProgramsSpecified
                    || this.LoanRoleTypeSpecified
                    || this.LoanStateSpecified
                    || this.LoanStatusesSpecified
                    || this.LtvSpecified
                    || this.MaturitySpecified
                    || this.MERSRegistrationsSpecified
                    || this.MIDataSpecified
                    || this.ModificationsSpecified
                    || this.MortgageScoresSpecified
                    || this.NegativeAmortizationSpecified
                    || this.OptionalProductsSpecified
                    || this.OriginationFundsSpecified
                    || this.OriginationSystemsSpecified
                    || this.PaymentSpecified
                    || this.PrepaymentPenaltySpecified
                    || this.PurchaseCreditsSpecified
                    || this.QualificationSpecified
                    || this.QualifiedMortgageSpecified
                    || this.RefinanceSpecified
                    || this.RehabilitationSpecified
                    || this.ReverseMortgageSpecified
                    || this.SelectionTypeSpecified
                    || this.ServicingSpecified
                    || this.TermsOfLoanSpecified
                    || this.TreasuryNpvSpecified
                    || this.UnderwritingSpecified
                    || this.WorkoutsSpecified;
            }
        }

        /// <summary>
        /// ACH information for this loan.
        /// </summary>
        [XmlElement("ACH", Order = 0)]
        public ACH Ach;

        /// <summary>
        /// Gets or sets a value indicating whether the Ach element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Ach element has been assigned a value.</value>
        [XmlIgnore]
        public bool AchSpecified
        {
            get { return this.Ach != null && this.Ach.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// An adjustment to the loan.
        /// </summary>
        [XmlElement("ADJUSTMENT", Order = 1)]
        public ADJUSTMENT Adjustment;

        /// <summary>
        /// Gets or sets a value indicating whether the Adjustment element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Adjustment element has been assigned a value.</value>
        [XmlIgnore]
        public bool AdjustmentSpecified
        {
            get { return this.Adjustment != null && this.Adjustment.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Information on the affordable lending product.
        /// </summary>
        [XmlElement("AFFORDABLE_LENDING", Order = 2)]
        public AFFORDABLE_LENDING AffordableLending;

        /// <summary>
        /// Gets or sets a value indicating whether the AffordableLending element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AffordableLending element has been assigned a value.</value>
        [XmlIgnore]
        public bool AffordableLendingSpecified
        {
            get { return this.AffordableLending != null && this.AffordableLending.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Amortization specifications.
        /// </summary>
        [XmlElement("AMORTIZATION", Order = 3)]
        public AMORTIZATION Amortization;

        /// <summary>
        /// Gets or sets a value indicating whether the Amortization element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Amortization element has been assigned a value.</value>
        [XmlIgnore]
        public bool AmortizationSpecified
        {
            get { return this.Amortization != null && this.Amortization.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// An assumption option for the loan.
        /// </summary>
        [XmlElement("ASSUMABILITY", Order = 4)]
        public ASSUMABILITY Assumability;

        /// <summary>
        /// Gets or sets a value indicating whether the assumption option element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the assumption option element has been assigned a value.</value>
        [XmlIgnore]
        public bool AssumabilitySpecified
        {
            get { return Assumability != null && Assumability.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Billing address for the borrower.
        /// </summary>
        [XmlElement("BILLING_ADDRESS", Order = 5)]
        public BILLING_ADDRESS BillingAddress;

        /// <summary>
        /// Gets or sets a value indicating whether the BillingAddress element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BillingAddress element has been assigned a value.</value>
        [XmlIgnore]
        public bool BillingAddressSpecified
        {
            get { return this.BillingAddress != null && this.BillingAddress.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A buy down associated with this loan.
        /// </summary>
        [XmlElement("BUYDOWN", Order = 6)]
        public BUYDOWN Buydown;

        /// <summary>
        /// Gets or sets a value indicating whether the BuyDown element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BuyDown element has been assigned a value.</value>
        [XmlIgnore]
        public bool BuydownSpecified
        {
            get { return this.Buydown != null && this.Buydown.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Capitalizations on this loan.
        /// </summary>
        [XmlElement("CAPITALIZATIONS", Order = 7)]
        public CAPITALIZATIONS Capitalizations;

        /// <summary>
        /// Gets or sets a value indicating whether the Capitalizations element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Capitalizations element has been assigned a value.</value>
        [XmlIgnore]
        public bool CapitalizationsSpecified
        {
            get { return this.Capitalizations != null && this.Capitalizations.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Information on a charge-off.
        /// </summary>
        [XmlElement("CHARGE_OFF", Order = 8)]
        public CHARGE_OFF ChargeOff;

        /// <summary>
        /// Gets or sets a value indicating whether the ChargeOff element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ChargeOff element has been assigned a value.</value>
        [XmlIgnore]
        public bool ChargeOffSpecified
        {
            get { return this.ChargeOff != null && this.ChargeOff.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Information for closing this loan.
        /// </summary>
        [XmlElement("CLOSING_INFORMATION", Order = 9)]
        public CLOSING_INFORMATION ClosingInformation;

        /// <summary>
        /// Gets or sets a value indicating whether the ClosingInformation element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ClosingInformation element has been assigned a value.</value>
        [XmlIgnore]
        public bool ClosingInformationSpecified
        {
            get { return ClosingInformation != null && ClosingInformation.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Information on collections.
        /// </summary>
        [XmlElement("COLLECTIONS", Order = 10)]
        public COLLECTIONS Collections;

        /// <summary>
        /// Gets or sets a value indicating whether the Collections element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Collections element has been assigned a value.</value>
        [XmlIgnore]
        public bool CollectionsSpecified
        {
            get { return this.Collections != null && this.Collections.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Information on new construction.
        /// </summary>
        [XmlElement("CONSTRUCTION", Order = 11)]
        public CONSTRUCTION Construction;

        /// <summary>
        /// Gets or sets a value indicating whether the Construction element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Construction element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConstructionSpecified
        {
            get { return this.Construction != null && this.Construction.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Variances in the loan contract.
        /// </summary>
        [XmlElement("CONTRACT_VARIANCES", Order = 12)]
        public CONTRACT_VARIANCES ContractVariances;

        /// <summary>
        /// Gets or sets a value indicating whether the ContractVariances element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ContractVariances element has been assigned a value.</value>
        [XmlIgnore]
        public bool ContractVariancesSpecified
        {
            get { return this.ContractVariances != null && this.ContractVariances.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Credit enhancements related to this loan.
        /// </summary>
        [XmlElement("CREDIT_ENHANCEMENTS", Order = 13)]
        public CREDIT_ENHANCEMENTS CreditEnhancements;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditEnhancements element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditEnhancements element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditEnhancementsSpecified
        {
            get { return this.CreditEnhancements != null && this.CreditEnhancements.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Datasets relating to different types of documentation.
        /// </summary>
        [XmlElement("DOCUMENT_SPECIFIC_DATA_SETS", Order = 14)]
        public DOCUMENT_SPECIFIC_DATA_SETS DocumentSpecificDataSets;

        /// <summary>
        /// Gets or sets a value indicating whether the DocumentSpecificDataSets element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DocumentSpecificDataSets element has been assigned a value.</value>
        [XmlIgnore]
        public bool DocumentSpecificDataSetsSpecified
        {
            get { return this.DocumentSpecificDataSets != null && this.DocumentSpecificDataSets.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Documentation on this loan.
        /// </summary>
        [XmlElement("DOCUMENTATIONS", Order = 15)]
        public DOCUMENTATIONS Documentations;

        /// <summary>
        /// Gets or sets a value indicating whether the Documentations element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Documentations element has been assigned a value.</value>
        [XmlIgnore]
        public bool DocumentationsSpecified
        {
            get { return this.Documentations != null && this.Documentations.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Down payment on this loan.
        /// </summary>
        [XmlElement("DOWN_PAYMENTS", Order = 16)]
        public DOWN_PAYMENTS DownPayments;

        /// <summary>
        /// Gets or sets a value indicating whether the DownPayments element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DownPayments element has been assigned a value.</value>
        [XmlIgnore]
        public bool DownPaymentsSpecified
        {
            get { return this.DownPayments != null && this.DownPayments.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Draw information for a line of credit.
        /// </summary>
        [XmlElement("DRAW", Order = 17)]
        public DRAW Draw;

        /// <summary>
        /// Gets or sets a value indicating whether the Draw element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Draw element has been assigned a value.</value>
        [XmlIgnore]
        public bool DrawSpecified
        {
            get { return this.Draw != null && this.Draw.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Information on the escrow account for this loan.
        /// </summary>
        [XmlElement("ESCROW", Order = 18)]
        public ESCROW Escrow;

        /// <summary>
        /// Gets or sets a value indicating whether the Escrow element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Escrow element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowSpecified
        {
            get { return this.Escrow != null && this.Escrow.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Fees associated with this loan.
        /// </summary>
        [XmlElement("FEE_INFORMATION", Order = 19)]
        public FEE_INFORMATION FeeInformation;

        /// <summary>
        /// Gets or sets a value indicating whether the FeeInformation element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeeInformation element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeeInformationSpecified
        {
            get { return this.FeeInformation != null && this.FeeInformation.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Information on foreclosure.
        /// </summary>
        [XmlElement("FORECLOSURES", Order = 20)]
        public FORECLOSURES Foreclosures;

        /// <summary>
        /// Gets or sets a value indicating whether the Foreclosures element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Foreclosures element has been assigned a value.</value>
        [XmlIgnore]
        public bool ForeclosuresSpecified
        {
            get { return this.Foreclosures != null && this.Foreclosures.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Government loan information.
        /// </summary>
        [XmlElement("GOVERNMENT_LOAN", Order = 21)]
        public GOVERNMENT_LOAN GovernmentLoan;

        /// <summary>
        /// Gets or sets a value indicating whether the GovernmentLoan element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GovernmentLoan element has been assigned a value.</value>
        [XmlIgnore]
        public bool GovernmentLoanSpecified
        {
            get { return this.GovernmentLoan != null && this.GovernmentLoan.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Home Equity Line Of Credit information.
        /// </summary>
        [XmlElement("HELOC", Order = 22)]
        public HELOC Heloc;

        /// <summary>
        /// Gets or sets a value indicating whether the HELOC element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HELOC element has been assigned a value.</value>
        [XmlIgnore]
        public bool HelocSpecified
        {
            get { return this.Heloc != null && this.Heloc.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// High cost mortgage information.
        /// </summary>
        [XmlElement("HIGH_COST_MORTGAGES", Order = 23)]
        public HIGH_COST_MORTGAGES HighCostMortgages;

        /// <summary>
        /// Gets or sets a value indicating whether the HighCostMortgages element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HighCostMortgages element has been assigned a value.</value>
        [XmlIgnore]
        public bool HighCostMortgagesSpecified
        {
            get { return this.HighCostMortgages != null && this.HighCostMortgages.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// HMDA loan information.
        /// </summary>
        [XmlElement("HMDA_LOAN", Order = 24)]
        public HMDA_LOAN HMDALoan;

        /// <summary>
        /// Gets or sets a value indicating whether the HMDA Loan element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HMDA Loan element has been assigned a value.</value>
        [XmlIgnore]
        public bool HMDALoanSpecified
        {
            get { return this.HMDALoan != null && this.HMDALoan.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Housing expenses related to this loan.
        /// </summary>
        [XmlElement("HOUSING_EXPENSES", Order = 25)]
        public HOUSING_EXPENSES HousingExpenses;

        /// <summary>
        /// Gets or sets a value indicating whether the HousingExpenses element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HousingExpenses element has been assigned a value.</value>
        [XmlIgnore]
        public bool HousingExpensesSpecified
        {
            get { return this.HousingExpenses != null && this.HousingExpenses.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Insurance claims on this loan.
        /// </summary>
        [XmlElement("INSURANCE_CLAIMS", Order = 26)]
        public INSURANCE_CLAIMS InsuranceClaims;

        /// <summary>
        /// Gets or sets a value indicating whether the InsuranceClaims element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InsuranceClaims element has been assigned a value.</value>
        [XmlIgnore]
        public bool InsuranceClaimsSpecified
        {
            get { return this.InsuranceClaims != null && this.InsuranceClaims.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Calculation procedure of the interest.
        /// </summary>
        [XmlElement("INTEREST_CALCULATION", Order = 27)]
        public INTEREST_CALCULATION InterestCalculation;

        /// <summary>
        /// Gets or sets a value indicating whether the InterestCalculation element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InterestCalculation element has been assigned a value.</value>
        [XmlIgnore]
        public bool InterestCalculationSpecified
        {
            get { return this.InterestCalculation != null && this.InterestCalculation.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The interest-only option for the loan.
        /// </summary>
        [XmlElement("INTEREST_ONLY", Order = 28)]
        public INTEREST_ONLY InterestOnly;

        /// <summary>
        /// Gets or sets a value indicating whether the InterestOnly element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InterestOnly element has been assigned a value.</value>
        [XmlIgnore]
        public bool InterestOnlySpecified
        {
            get { return this.InterestOnly != null && this.InterestOnly.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Features related to the investor.
        /// </summary>
        [XmlElement("INVESTOR_FEATURES", Order = 29)]
        public INVESTOR_FEATURES InvestorFeatures;

        /// <summary>
        /// Gets or sets a value indicating whether the InvestorFeatures element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InvestorFeatures element has been assigned a value.</value>
        [XmlIgnore]
        public bool InvestorFeaturesSpecified
        {
            get { return this.InvestorFeatures != null && this.InvestorFeatures.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Information on the investor for the loan.
        /// </summary>
        [XmlElement("INVESTOR_LOAN_INFORMATION", Order = 30)]
        public INVESTOR_LOAN_INFORMATION InvestorLoanInformation;

        /// <summary>
        /// Gets or sets a value indicating whether the InvestorLoanInformation element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InvestorLoanInformation element has been assigned a value.</value>
        [XmlIgnore]
        public bool InvestorLoanInformationSpecified
        {
            get { return this.InvestorLoanInformation != null && this.InvestorLoanInformation.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Rules for late charges.
        /// </summary>
        [XmlElement("LATE_CHARGE", Order = 31)]
        public LATE_CHARGE LateCharge;

        /// <summary>
        /// Gets or sets a value indicating whether the LateCharge element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LateCharge element has been assigned a value.</value>
        [XmlIgnore]
        public bool LateChargeSpecified
        {
            get { return LateCharge != null && LateCharge.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Comments on the loan.
        /// </summary>
        [XmlElement("LOAN_COMMENTS", Order = 32)]
        public LOAN_COMMENTS LoanComments;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanComments element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanComments element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanCommentsSpecified
        {
            get { return this.LoanComments != null && this.LoanComments.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details on the loan.
        /// </summary>
        [XmlElement("LOAN_DETAIL", Order = 33)]
        public LOAN_DETAIL LoanDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanDetailSpecified
        {
            get { return this.LoanDetail != null && this.LoanDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Identifiers for the loan.
        /// </summary>
        [XmlElement("LOAN_IDENTIFIERS", Order = 34)]
        public LOAN_IDENTIFIERS LoanIdentifiers;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanIdentifiers element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanIdentifiers element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanIdentifiersSpecified
        {
            get { return this.LoanIdentifiers != null && this.LoanIdentifiers.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Loan level credit for this product.
        /// </summary>
        [XmlElement("LOAN_LEVEL_CREDIT", Order = 35)]
        public LOAN_LEVEL_CREDIT LoanLevelCredit;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanLevelCredit element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanLevelCredit element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanLevelCreditSpecified
        {
            get { return this.LoanLevelCredit != null && this.LoanLevelCredit.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The product type of this loan.
        /// </summary>
        [XmlElement("LOAN_PRODUCT", Order = 36)]
        public LOAN_PRODUCT LoanProduct;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanProduct element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanProduct element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanProductSpecified
        {
            get { return LoanProduct != null && LoanProduct.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Programs associated with this loan.
        /// </summary>
        [XmlElement("LOAN_PROGRAMS", Order = 37)]
        public LOAN_PROGRAMS LoanPrograms;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanPrograms element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanPrograms element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanProgramsSpecified
        {
            get { return this.LoanPrograms != null && this.LoanPrograms.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The state of the loan.
        /// </summary>
        [XmlElement("LOAN_STATE", Order = 38)]
        public LOAN_STATE LoanState;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanState element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanState element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanStateSpecified
        {
            get { return this.LoanState != null && this.LoanState.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Statuses of this loan.
        /// </summary>
        [XmlElement("LOAN_STATUSES", Order = 39)]
        public LOAN_STATUSES LoanStatuses;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanStatuses element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanStatuses element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanStatusesSpecified
        {
            get { return this.LoanStatuses != null && this.LoanStatuses.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The loan-to-value ratio.
        /// </summary>
        [XmlElement("LTV", Order = 40)]
        public LTV Ltv;

        /// <summary>
        /// Gets or sets a value indicating whether the LTV element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LTV element has been assigned a value.</value>
        [XmlIgnore]
        public bool LtvSpecified
        {
            get { return Ltv != null && Ltv.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Maturity information for this loan product.
        /// </summary>
        [XmlElement("MATURITY", Order = 41)]
        public MATURITY Maturity;

        /// <summary>
        /// Gets or sets a value indicating whether the Maturity element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Maturity element has been assigned a value.</value>
        [XmlIgnore]
        public bool MaturitySpecified
        {
            get { return Maturity != null && Maturity.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Information on MERS registration.
        /// </summary>
        [XmlElement("MERS_REGISTRATIONS", Order = 42)]
        public MERS_REGISTRATIONS MERSRegistrations;

        /// <summary>
        /// Gets or sets a value indicating whether the MERS Registrations element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MERS Registrations element has been assigned a value.</value>
        [XmlIgnore]
        public bool MERSRegistrationsSpecified
        {
            get { return MERSRegistrations != null && MERSRegistrations.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Data on the mortgage insurance policy.
        /// </summary>
        [XmlElement("MI_DATA", Order = 43)]
        public MI_DATA MIData;

        /// <summary>
        /// Gets or sets a value indicating whether the Mortgage Insurance Data element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Mortgage Insurance Data element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIDataSpecified
        {
            get { return MIData != null && MIData.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Modifications to the note.
        /// </summary>
        [XmlElement("MODIFICATIONS", Order = 44)]
        public MODIFICATIONS Modifications;

        /// <summary>
        /// Gets or sets a value indicating whether the Modifications element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Modifications element has been assigned a value.</value>
        [XmlIgnore]
        public bool ModificationsSpecified
        {
            get { return this.Modifications != null && this.Modifications.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Scores for this mortgage loan.
        /// </summary>
        [XmlElement("MORTGAGE_SCORES", Order = 45)]
        public MORTGAGE_SCORES MortgageScores;

        /// <summary>
        /// Gets or sets a value indicating whether the MortgageScores element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MortgageScores element has been assigned a value.</value>
        [XmlIgnore]
        public bool MortgageScoresSpecified
        {
            get { return this.MortgageScores != null && this.MortgageScores.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Information on possible negative amortization on this loan.
        /// </summary>
        [XmlElement("NEGATIVE_AMORTIZATION", Order = 46)]
        public NEGATIVE_AMORTIZATION NegativeAmortization;

        /// <summary>
        /// Gets or sets a value indicating whether the NegativeAmortization element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NegativeAmortization element has been assigned a value.</value>
        [XmlIgnore]
        public bool NegativeAmortizationSpecified
        {
            get { return this.NegativeAmortization != null && this.NegativeAmortization.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Any optional products included with this loan transaction.
        /// </summary>
        [XmlElement("OPTIONAL_PRODUCTS", Order = 47)]
        public OPTIONAL_PRODUCTS OptionalProducts;

        /// <summary>
        /// Gets or sets a value indicating whether the OptionalProducts element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OptionalProducts element has been assigned a value.</value>
        [XmlIgnore]
        public bool OptionalProductsSpecified
        {
            get { return this.OptionalProducts != null && this.OptionalProducts.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Information on origination funds.
        /// </summary>
        [XmlElement("ORIGINATION_FUNDS", Order = 48)]
        public ORIGINATION_FUNDS OriginationFunds;

        /// <summary>
        /// Gets or sets a value indicating whether the OriginationFunds element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OriginationFunds element has been assigned a value.</value>
        [XmlIgnore]
        public bool OriginationFundsSpecified
        {
            get { return this.OriginationFunds != null && this.OriginationFunds.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Information on origination systems.
        /// </summary>
        [XmlElement("ORIGINATION_SYSTEMS", Order = 49)]
        public ORIGINATION_SYSTEMS OriginationSystems;

        /// <summary>
        /// Gets or sets a value indicating whether the OriginationSystems element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OriginationSystems element has been assigned a value.</value>
        [XmlIgnore]
        public bool OriginationSystemsSpecified
        {
            get { return this.OriginationSystems != null && this.OriginationSystems.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Information on the monthly payment.
        /// </summary>
        [XmlElement("PAYMENT", Order = 50)]
        public PAYMENT Payment;

        /// <summary>
        /// Gets or sets a value indicating whether the Payment element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Payment element has been assigned a value.</value>
        [XmlIgnore]
        public bool PaymentSpecified
        {
            get { return this.Payment != null && this.Payment.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Information on the prepayment penalty.
        /// </summary>
        [XmlElement("PREPAYMENT_PENALTY", Order = 51)]
        public PREPAYMENT_PENALTY PrepaymentPenalty;

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaymentPenalty element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaymentPenalty element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaymentPenaltySpecified
        {
            get { return this.PrepaymentPenalty != null && this.PrepaymentPenalty.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Purchase credit for this loan.
        /// </summary>
        [XmlElement("PURCHASE_CREDITS", Order = 52)]
        public PURCHASE_CREDITS PurchaseCredits;

        /// <summary>
        /// Gets or sets a value indicating whether the PurchaseCredits element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PurchaseCredits element has been assigned a value.</value>
        [XmlIgnore]
        public bool PurchaseCreditsSpecified
        {
            get { return this.PurchaseCredits != null && this.PurchaseCredits.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Loan qualification information.
        /// </summary>
        [XmlElement("QUALIFICATION", Order = 53)]
        public QUALIFICATION Qualification;

        /// <summary>
        /// Gets or sets a value indicating whether the Qualification element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Qualification element has been assigned a value.</value>
        [XmlIgnore]
        public bool QualificationSpecified
        {
            get { return this.Qualification != null && this.Qualification.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Qualified mortgage information for this loan.
        /// </summary>
        [XmlElement("QUALIFIED_MORTGAGE", Order = 54)]
        public QUALIFIED_MORTGAGE QualifiedMortgage;

        /// <summary>
        /// Gets or sets a value indicating whether the QualifiedMortgage element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the QualifiedMortgage element has been assigned a value.</value>
        [XmlIgnore]
        public bool QualifiedMortgageSpecified
        {
            get { return this.QualifiedMortgage != null && this.QualifiedMortgage.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Refinancing information.
        /// </summary>
        [XmlElement("REFINANCE", Order = 55)]
        public REFINANCE Refinance;

        /// <summary>
        /// Gets or sets a value indicating whether the Refinance element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Refinance element has been assigned a value.</value>
        [XmlIgnore]
        public bool RefinanceSpecified
        {
            get { return this.Refinance != null && this.Refinance.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Rehabilitation for this loan.
        /// </summary>
        [XmlElement("REHABILITATION", Order = 56)]
        public REHABILITATION Rehabilitation;

        /// <summary>
        /// Gets or sets a value indicating whether the Rehabilitation element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Rehabilitation element has been assigned a value.</value>
        [XmlIgnore]
        public bool RehabilitationSpecified
        {
            get { return this.Rehabilitation != null && this.Rehabilitation.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Reverse mortgage product information.
        /// </summary>
        [XmlElement("REVERSE_MORTGAGE", Order = 57)]
        public REVERSE_MORTGAGE ReverseMortgage;

        /// <summary>
        /// Gets or sets a value indicating whether the ReverseMortgage element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ReverseMortgage element has been assigned a value.</value>
        [XmlIgnore]
        public bool ReverseMortgageSpecified
        {
            get { return this.ReverseMortgage != null && this.ReverseMortgage.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Information on the servicer for this loan.
        /// </summary>
        [XmlElement("SERVICING", Order = 58)]
        public SERVICING Servicing;

        /// <summary>
        /// Gets or sets a value indicating whether the Servicing element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Servicing element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServicingSpecified
        {
            get { return this.Servicing != null && this.Servicing.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The terms of this loan.
        /// </summary>
        [XmlElement("TERMS_OF_LOAN", Order = 59)]
        public TERMS_OF_LOAN TermsOfLoan;

        /// <summary>
        /// Gets or sets a value indicating whether the TermsOfLoan element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TermsOfLoan element has been assigned a value.</value>
        [XmlIgnore]
        public bool TermsOfLoanSpecified
        {
            get { return this.TermsOfLoan != null && this.TermsOfLoan.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Treasury NPV for this loan.
        /// </summary>
        [XmlElement("TREASURY_NPV", Order = 60)]
        public TREASURY_NPV TreasuryNpv;

        /// <summary>
        /// Gets or sets a value indicating whether the TreasuryNPV element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TreasuryNPV element has been assigned a value.</value>
        [XmlIgnore]
        public bool TreasuryNpvSpecified
        {
            get { return this.TreasuryNpv != null && this.TreasuryNpv.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Underwriting for this loan.
        /// </summary>
        [XmlElement("UNDERWRITING", Order = 61)]
        public UNDERWRITING Underwriting;

        /// <summary>
        /// Gets or sets a value indicating whether the Underwriting element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Underwriting element has been assigned a value.</value>
        [XmlIgnore]
        public bool UnderwritingSpecified
        {
            get { return this.Underwriting != null && this.Underwriting.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Workouts related to the loan.
        /// </summary>
        [XmlElement("WORKOUTS", Order = 62)]
        public WORKOUTS Workouts;

        /// <summary>
        /// Gets or sets a value indicating whether the Workouts element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Workouts element has been assigned a value.</value>
        [XmlIgnore]
        public bool WorkoutsSpecified
        {
            get { return this.Workouts != null && this.Workouts.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 63)]
        public LOAN_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null && Extension.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Gets or sets the $$xlink$$ label.
        /// </summary>
        [XmlAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified, Namespace = Mismo3Constants.XlinkNamespace)]
        public string label;

        /// <summary>
        /// Indicates whether the label can be serialized.
        /// </summary>
        /// <returns>A boolean indicating whether the label can be serialized.</returns>
        public bool ShouldSerializelabel()
        {
            return !string.IsNullOrEmpty(label);
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }

        /// <summary>
        /// When true, indicates this Loan Product is ineligible based on Deal characteristics.
        /// </summary>
        [XmlIgnore]
        private bool isIneligibleLoanProduct;

        /// <summary>
        /// Gets or sets a value indicating whether this is an ineligible loan product.
        /// </summary>
        /// <value>A boolean indicating whether the loan product is ineligible based on Deal characteristics.</value>
        [XmlIgnore]
        public bool IsIneligibleLoanProduct
        {
            get { return isIneligibleLoanProduct; }
            set { this.isIneligibleLoanProduct = value; this.IsIneligibleLoanProductSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the Ineligible Loan Product indicator has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool IsIneligibleLoanProductSpecified = false;

        /// <summary>
        /// Gets or sets the Ineligible Loan Product indicator member as a string for serialization.
        /// </summary>
        /// <value>A boolean indicating whether the ineligible loan product indicator element has been assigned a value.</value>
        [XmlAttribute(AttributeName = "IneligibleLoanProductIndicator")]
        public string IneligibleLoanProductIndicatorSerialized
        {
            get { return Mismo3Utilities.ConvertBoolToString(IsIneligibleLoanProduct); }
            set { }
        }

        /// <summary>
        /// Indicates whether the Ineligible Loan Product indicator can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the Ineligible Loan Product indicator can be serialized.</returns>
        public bool ShouldSerializeIneligibleLoanProductIndicatorSerialized()
        {
            return IsIneligibleLoanProductSpecified;
        }

        /// <summary>
        /// An attribute of LOAN used to identify the role the associated LOAN plays in the transaction.
        /// </summary>
        [XmlIgnore]
        private LoanRoleBase loanRoleType;

        /// <summary>
        /// Gets or sets the Loan Role Type.
        /// </summary>
        /// <value>A selected value from the Loan Role Type enumeration.</value>
        [XmlIgnore]
        public LoanRoleBase LoanRoleType
        {
            get { return loanRoleType; }
            set 
            { 
                this.loanRoleType = value; 
                this.LoanRoleTypeSpecified = this.loanRoleType != LoanRoleBase.Blank; 
            }
        }

        /// <summary>
        /// Indicates whether a Loan Role Type has been assigned.
        /// </summary>
        [XmlIgnore]
        protected bool LoanRoleTypeSpecified = false;

        /// <summary>
        /// Gets or sets the Loan Role Type member as a string for serialization.
        /// </summary>
        /// <value>A boolean indicating whether the Loan Role Type has been assigned a value.</value>
        [XmlAttribute(AttributeName = "LoanRoleType")]
        public string LoanRoleTypeSerialized
        {
            get { return Mismo3Utilities.GetXmlEnumName(LoanRoleType); }
            set { }
        }

        /// <summary>
        /// Indicates whether the Loan Role Type can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the Loan Role Type can be serialized.</returns>
        public bool ShouldSerializeLoanRoleTypeSerialized()
        {
            return LoanRoleTypeSpecified;
        }

        /// <summary>
        /// A classification of the collection of loan parameters to support borrower shopping for loan products or proposed loan products that would suit the current borrower needs.
        /// </summary>
        [XmlIgnore]
        private SelectionBase selectionType;

        /// <summary>
        /// Gets or sets the Selection Type.
        /// </summary>
        /// <value>A selected value from the Selection Type enumeration.</value>
        [XmlIgnore]
        public SelectionBase SelectionType
        {
            get { return selectionType; }
            set 
            { 
                this.selectionType = value;
                this.SelectionTypeSpecified = this.selectionType != SelectionBase.Blank;
            }
        }

        /// <summary>
        /// Indicates whether the Selection Type has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SelectionTypeSpecified = false;

        /// <summary>
        /// Gets or sets the Selection Type member as a string for serialization.
        /// </summary>
        /// <value>A boolean indicating whether the selection type element has been assigned a value.</value>
        [XmlAttribute(AttributeName = "SelectionType")]
        public string SelectionTypeSerialized
        {
            get { return Mismo3Utilities.GetXmlEnumName(SelectionType); }
            set { }
        }

        /// <summary>
        /// Indicates whether the Selection Type can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the Selection Type can be serialized.</returns>
        public bool ShouldSerializeSelectionTypeSerialized()
        {
            return SelectionTypeSpecified;
        }
    }
}
