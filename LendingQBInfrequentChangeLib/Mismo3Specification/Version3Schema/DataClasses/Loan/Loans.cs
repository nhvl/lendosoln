namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class LOANS
    {
        /// <summary>
        /// Gets a value indicating whether the LOANS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CombinedLtvsSpecified
                    || this.ExtensionSpecified
                    || this.LoanSpecified;
            }
        }

        /// <summary>
        /// The combined LTVs associated with the loan collection.
        /// </summary>
        [XmlElement("COMBINED_LTVS", Order = 0)]
        public COMBINED_LTVS CombinedLtvs;

        /// <summary>
        /// Gets or sets a value indicating whether the CombinedLTV element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CombinedLTV element has been assigned a value.</value>
        [XmlIgnore]
        public bool CombinedLtvsSpecified
        {
            get { return this.CombinedLtvs != null && this.CombinedLtvs.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A collection of loans.
        /// </summary>
        [XmlElement("LOAN", Order = 1)]
        public List<LOAN> Loan = new List<LOAN>();

        /// <summary>
        /// Gets or sets a value indicating whether the Loan element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Loan element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanSpecified
        {
            get { return this.Loan != null && this.Loan.Count(l => l != null && l.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public LOANS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
