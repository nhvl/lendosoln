namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class LOAN_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the LOAN_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AcceleratedPaymentProgramIndicatorSpecified
                    || this.ACHIndicatorSpecified
                    || this.ActiveLitigationIndicatorSpecified
                    || this.ApplicationReceivedDateSpecified
                    || this.ArmsLengthIndicatorSpecified
                    || this.AssumabilityIndicatorSpecified
                    || this.AssumedIndicatorSpecified
                    || this.BalloonGuaranteedRefinancingIndicatorSpecified
                    || this.BalloonIndicatorSpecified
                    || this.BalloonPaymentAmountSpecified
                    || this.BalloonPaymentMaximumAmountSpecified
                    || this.BalloonPaymentMinimumAmountSpecified
                    || this.BalloonResetIndicatorSpecified
                    || this.BalloonResetOptionIndicatorSpecified
                    || this.BelowMarketSubordinateFinancingIndicatorSpecified
                    || this.BiweeklyToMonthlyOnDefaultIndicatorSpecified
                    || this.BorrowerCountSpecified
                    || this.BuydownTemporarySubsidyFundingIndicatorSpecified
                    || this.CapitalizedFeesIndicatorSpecified
                    || this.CapitalizedLoanIndicatorSpecified
                    || this.ClosingCostFinancedIndicatorSpecified
                    || this.CollateralPledgedToNameSpecified
                    || this.ConcurrentOriginationIndicatorSpecified
                    || this.ConcurrentOriginationLenderIndicatorSpecified
                    || this.ConformingIndicatorSpecified
                    || this.ConstructionLoanIndicatorSpecified
                    || this.ConvertibleIndicatorSpecified
                    || this.CreditEnhancementIndicatorSpecified
                    || this.CreditorServicingOfLoanStatementTypeOtherDescriptionSpecified
                    || this.CreditorServicingOfLoanStatementTypeSpecified
                    || this.CurrentInterestRatePercentSpecified
                    || this.DeferredInterestBalanceAmountSpecified
                    || this.DemandFeatureIndicatorSpecified
                    || this.EligibleForLenderPaidMIIndicatorSpecified
                    || this.EmployeeLoanProgramIndicatorSpecified
                    || this.ENoteCertifiedIndicatorSpecified
                    || this.ENoteIndicatorSpecified
                    || this.EscrowAbsenceReasonTypeOtherDescriptionSpecified
                    || this.EscrowAbsenceReasonTypeSpecified
                    || this.EscrowAccountLenderRequirementTypeSpecified
                    || this.EscrowAccountRequestedIndicatorSpecified
                    || this.EscrowIndicatorSpecified
                    || this.EscrowNotAllowedIndicatorSpecified
                    || this.EscrowsElectedByBorrowerIndicatorSpecified
                    || this.ExtensionSpecified
                    || this.HELOCIndicatorSpecified
                    || this.HigherPricedMortgageLoanIndicatorSpecified
                    || this.HomeBuyersHomeownershipEducationCertificateIndicatorSpecified
                    || this.InitialFixedPeriodEffectiveMonthsCountSpecified
                    || this.InterestCreditedToBorrowerIndicatorSpecified
                    || this.InterestOnlyIndicatorSpecified
                    || this.InterestRateIncreaseIndicatorSpecified
                    || this.JurisdictionHighCostLoanIndicatorSpecified
                    || this.LenderPlacedHazardInsuranceIndicatorSpecified
                    || this.LenderSelfInsuredIndicatorSpecified
                    || this.LendingLimitAmountSpecified
                    || this.LendingLimitTypeOtherDescriptionSpecified
                    || this.LendingLimitTypeSpecified
                    || this.LienHolderSameAsSubjectLoanIndicatorSpecified
                    || this.LoanAffordableIndicatorSpecified
                    || this.LoanAgeMonthsCountSpecified
                    || this.LoanAmountIncreaseIndicatorSpecified
                    || this.LoanApprovalDateSpecified
                    || this.LoanApprovalExpirationDateSpecified
                    || this.LoanApprovalPeriodDaysCountSpecified
                    || this.LoanClosingStatusTypeSpecified
                    || this.LoanCommitmentDateSpecified
                    || this.LoanCommitmentExpirationDateSpecified
                    || this.LoanCommitmentPeriodDaysCountSpecified
                    || this.LoanFundingDateSpecified
                    || this.LoanRepaymentTypeOtherDescriptionSpecified
                    || this.LoanRepaymentTypeSpecified
                    || this.LoanSaleFundingDateSpecified
                    || this.LoanSecuredByTypeOtherDescriptionSpecified
                    || this.LoanSecuredByTypeSpecified
                    || this.LoanSecuredIndicatorSpecified
                    || this.LoanSellerProvidedInvestmentRatingDescriptionSpecified
                    || this.LostNoteAffidavitIndicatorSpecified
                    || this.MICoverageExistsIndicatorSpecified
                    || this.MIRequiredIndicatorSpecified
                    || this.MortgageModificationIndicatorSpecified
                    || this.NegativeAmortizationIndicatorSpecified
                    || this.OriginalLoanStandardEscrowProvisionsIndicatorSpecified
                    || this.OverdueInterestAmountSpecified
                    || this.PaymentIncreaseIndicatorSpecified
                    || this.PiggybackLoanIndicatorSpecified
                    || this.PoolIdentifierSpecified
                    || this.PoolIssueDateSpecified
                    || this.PoolPrefixIdentifierSpecified
                    || this.PrepaymentPenaltyIndicatorSpecified
                    || this.PrepaymentPenaltyWaivedIndicatorSpecified
                    || this.PrepaymentRestrictionIndicatorSpecified
                    || this.PropertiesFinancedByLenderCountSpecified
                    || this.PropertyInspectionWaiverIndicatorSpecified
                    || this.QualifiedMortgageIndicatorSpecified
                    || this.RecoverableCorporateAdvanceBalanceFromBorrowerAmountSpecified
                    || this.RecoverableCorporateAdvanceBalanceFromThirdPartyAmountSpecified
                    || this.RefundOfOverpaidInterestCalendarYearAmountSpecified
                    || this.RegulationZHighCostLoanIndicatorSpecified
                    || this.RelocationLoanIndicatorSpecified
                    || this.RemainingUnearnedInterestAmountSpecified
                    || this.ReverseMortgageIndicatorSpecified
                    || this.SCRAIndicatorSpecified
                    || this.SCRAReliefForbearanceRequestedIndicatorSpecified
                    || this.SCRAReliefStatusTypeOtherDescriptionSpecified
                    || this.SCRAReliefStatusTypeSpecified
                    || this.SeasonalPaymentFeatureDescriptionSpecified
                    || this.SeasonalPaymentFeatureIndicatorSpecified
                    || this.ServicingTransferProhibitedIndicatorSpecified
                    || this.ServicingTransferStatusTypeSpecified
                    || this.SharedAppreciationIndicatorSpecified
                    || this.SharedEquityIndicatorSpecified
                    || this.SignedAuthorizationToRequestTaxRecordsIndicatorSpecified
                    || this.StepPaymentsFeatureDescriptionSpecified
                    || this.TaxRecordsObtainedIndicatorSpecified
                    || this.TimelyPaymentRateReductionIndicatorSpecified
                    || this.TotalMortgagedPropertiesCountSpecified
                    || this.TotalSalesConcessionAmountSpecified
                    || this.TotalSubordinateFinancingAmountSpecified
                    || this.WarehouseLenderIndicatorSpecified
                    || this.WorkoutActiveIndicatorSpecified;
            }
        }

        /// <summary>
        /// Indicates that mortgagor participates in some sort of accelerated principal re-payment program which may be administered by a third party.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIndicator AcceleratedPaymentProgramIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the AcceleratedPaymentProgramIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AcceleratedPaymentProgramIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool AcceleratedPaymentProgramIndicatorSpecified
        {
            get { return AcceleratedPaymentProgramIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that a loan is enrolled in Automatic Clearing House (ACH) payment drafting.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOIndicator ACHIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ACHIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ACHIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ACHIndicatorSpecified
        {
            get { return ACHIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that the loan is subject to active litigation.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOIndicator ActiveLitigationIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ActiveLitigationIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ActiveLitigationIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ActiveLitigationIndicatorSpecified
        {
            get { return ActiveLitigationIndicator != null; }
            set { }
        }

        /// <summary>
        /// The date the creditor or originator received the application from the borrower for the subject mortgage loan that would trigger the TIL Disclosure document.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMODate ApplicationReceivedDate;

        /// <summary>
        /// Gets or sets a value indicating whether the ApplicationReceivedDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ApplicationReceivedDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ApplicationReceivedDateSpecified
        {
            get { return ApplicationReceivedDate != null; }
            set { }
        }

        /// <summary>
        /// Indicates that this is an Arms Length Transaction. An Arms length transaction is between a willing buyer and a willing seller with no undue influence on either party and there is no relationship between the parties except that of the specific transaction.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOIndicator ArmsLengthIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ArmsLengthIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ArmsLengthIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ArmsLengthIndicatorSpecified
        {
            get { return ArmsLengthIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether the loan is assumable by another borrower.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOIndicator AssumabilityIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the indicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the indicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool AssumabilityIndicatorSpecified
        {
            get { return AssumabilityIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates if the loan has been assumed.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOIndicator AssumedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the AssumedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AssumedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool AssumedIndicatorSpecified
        {
            get { return AssumedIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates refinancing of the balloon mortgage loan  is guaranteed by the lender.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOIndicator BalloonGuaranteedRefinancingIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the BalloonGuaranteedRefinancingIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BalloonGuaranteedRefinancingIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool BalloonGuaranteedRefinancingIndicatorSpecified
        {
            get { return BalloonGuaranteedRefinancingIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether or not a final balloon payment is required under the terms of the loan repayment schedule to fully pay off the loan.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOIndicator BalloonIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the BalloonIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BalloonIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool BalloonIndicatorSpecified
        {
            get { return BalloonIndicator != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of the final, or lump sum, balloon payment.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOAmount BalloonPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the BalloonPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BalloonPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool BalloonPaymentAmountSpecified
        {
            get { return BalloonPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// The largest scheduled lump sum payment over the life of loan. This applies when there are scheduled lump sum payments that are more than twice as large as the average of earlier scheduled payments.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOAmount BalloonPaymentMaximumAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the BalloonPaymentMaximumAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BalloonPaymentMaximumAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool BalloonPaymentMaximumAmountSpecified
        {
            get { return BalloonPaymentMaximumAmount != null; }
            set { }
        }

        /// <summary>
        /// The smallest scheduled lump sum payment over the life of loan. This applies when there are scheduled lump sum payments that are more than twice as large as the average of earlier scheduled payments.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOAmount BalloonPaymentMinimumAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the BalloonPaymentMinimumAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BalloonPaymentMinimumAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool BalloonPaymentMinimumAmountSpecified
        {
            get { return BalloonPaymentMinimumAmount != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that the balloon loan has been reset to the current market rate.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOIndicator BalloonResetIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the BalloonResetIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BalloonResetIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool BalloonResetIndicatorSpecified
        {
            get { return BalloonResetIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates the balloon loan has an option to reset.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOIndicator BalloonResetOptionIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the BalloonResetOptionIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BalloonResetOptionIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool BalloonResetOptionIndicatorSpecified
        {
            get { return BalloonResetOptionIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether the seller provided below-market subordinate financing.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOIndicator BelowMarketSubordinateFinancingIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the BelowMarketSubordinateFinancingIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BelowMarketSubordinateFinancingIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool BelowMarketSubordinateFinancingIndicatorSpecified
        {
            get { return BelowMarketSubordinateFinancingIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates if the lender allows conversion from Bi-weekly payments to monthly upon a default by the borrower.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOIndicator BiweeklyToMonthlyOnDefaultIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the BiweeklyToMonthlyOnDefaultIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BiweeklyToMonthlyOnDefaultIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool BiweeklyToMonthlyOnDefaultIndicatorSpecified
        {
            get { return BiweeklyToMonthlyOnDefaultIndicator != null; }
            set { }
        }

        /// <summary>
        /// The number of borrowers obligated on the note.
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMOCount BorrowerCount;

        /// <summary>
        /// Gets or sets a value indicating whether the BorrowerCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BorrowerCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool BorrowerCountSpecified
        {
            get { return BorrowerCount != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether there is buy down funding on this loan. A buy down is money paid by the borrower or third party for the purpose of reducing the interest rate and/or the monthly payments on a temporary basis.
        /// </summary>
        [XmlElement(Order = 17)]
        public MISMOIndicator BuydownTemporarySubsidyFundingIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the BuyDownTemporarySubsidyFundingIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BuyDownTemporarySubsidyFundingIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool BuydownTemporarySubsidyFundingIndicatorSpecified
        {
            get { return BuydownTemporarySubsidyFundingIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the mortgage balance includes capitalized origination fees.
        /// </summary>
        [XmlElement(Order = 18)]
        public MISMOIndicator CapitalizedFeesIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the CapitalizedFeesIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CapitalizedFeesIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool CapitalizedFeesIndicatorSpecified
        {
            get { return CapitalizedFeesIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates that interest accrued, escrow disbursements made and/or fees charged are included in the unpaid principal balance.
        /// </summary>
        [XmlElement(Order = 19)]
        public MISMOIndicator CapitalizedLoanIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the CapitalizedLoanIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CapitalizedLoanIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool CapitalizedLoanIndicatorSpecified
        {
            get { return CapitalizedLoanIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates that some or all of the closing cost were financed into the loan amount.
        /// </summary>
        [XmlElement(Order = 20)]
        public MISMOIndicator ClosingCostFinancedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ClosingCostFinancedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ClosingCostFinancedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ClosingCostFinancedIndicatorSpecified
        {
            get { return ClosingCostFinancedIndicator != null; }
            set { }
        }

        /// <summary>
        /// Specifies to whom the loan has been pledged as collateral.
        /// </summary>
        [XmlElement(Order = 21)]
        public MISMOString CollateralPledgedToName;

        /// <summary>
        /// Gets or sets a value indicating whether the CollateralPledgedToName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CollateralPledgedToName element has been assigned a value.</value>
        [XmlIgnore]
        public bool CollateralPledgedToNameSpecified
        {
            get { return CollateralPledgedToName != null; }
            set { }
        }

        /// <summary>
        /// Indicates that a new first mortgage will be closed concurrently with a second or a HELOC loan.
        /// </summary>
        [XmlElement(Order = 22)]
        public MISMOIndicator ConcurrentOriginationIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ConcurrentOriginationIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConcurrentOriginationIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConcurrentOriginationIndicatorSpecified
        {
            get { return ConcurrentOriginationIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates that a concurrent loan is from the same lender. This is in conjunction with Concurrent Origination Indicator.
        /// </summary>
        [XmlElement(Order = 23)]
        public MISMOIndicator ConcurrentOriginationLenderIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ConcurrentOriginationLenderIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConcurrentOriginationLenderIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConcurrentOriginationLenderIndicatorSpecified
        {
            get { return ConcurrentOriginationLenderIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that the loan amount is a conforming loan amount by Fannie Mae and Freddie Mac standards.
        /// </summary>
        [XmlElement(Order = 24)]
        public MISMOIndicator ConformingIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ConformingIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConformingIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConformingIndicatorSpecified
        {
            get { return ConformingIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether or not this is a construction loan.
        /// </summary>
        [XmlElement(Order = 25)]
        public MISMOIndicator ConstructionLoanIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ConstructionLoanIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConstructionLoanIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConstructionLoanIndicatorSpecified
        {
            get { return ConstructionLoanIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the loan has a convertible characteristic.
        /// </summary>
        [XmlElement(Order = 26)]
        public MISMOIndicator ConvertibleIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ConvertibleIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConvertibleIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConvertibleIndicatorSpecified
        {
            get { return ConvertibleIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that the loan has a credit enhancement.
        /// </summary>
        [XmlElement(Order = 27)]
        public MISMOIndicator CreditEnhancementIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditEnhancementIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditEnhancementIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditEnhancementIndicatorSpecified
        {
            get { return CreditEnhancementIndicator != null; }
            set { }
        }

        /// <summary>
        /// Specifies the certainty of the originating lender's or creditor's intention to service the mortgage loan.
        /// </summary>
        [XmlElement(Order = 28)]
        public MISMOEnum<CreditorServicingOfLoanStatementBase> CreditorServicingOfLoanStatementType;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditorServicingOfLoanStatementType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditorServicingOfLoanStatementType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditorServicingOfLoanStatementTypeSpecified
        {
            get { return this.CreditorServicingOfLoanStatementType != null && this.CreditorServicingOfLoanStatementType.enumValue != CreditorServicingOfLoanStatementBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture additional information when Other is selected for Creditor Servicing Of Loan Statement Type.
        /// </summary>
        [XmlElement(Order = 29)]
        public MISMOString CreditorServicingOfLoanStatementTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditorServicingOfLoanStatementTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditorServicingOfLoanStatementTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditorServicingOfLoanStatementTypeOtherDescriptionSpecified
        {
            get { return CreditorServicingOfLoanStatementTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The current interest rate, expressed as a percent, for this loan.
        /// </summary>
        [XmlElement(Order = 30)]
        public MISMOPercent CurrentInterestRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the CurrentInterestRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CurrentInterestRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool CurrentInterestRatePercentSpecified
        {
            get { return CurrentInterestRatePercent != null; }
            set { }
        }

        /// <summary>
        /// Accrued Uncollected Interest.
        /// </summary>
        [XmlElement(Order = 31)]
        public MISMOAmount DeferredInterestBalanceAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the DeferredInterestBalanceAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DeferredInterestBalanceAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool DeferredInterestBalanceAmountSpecified
        {
            get { return DeferredInterestBalanceAmount != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that the loan has a demand feature.
        /// </summary>
        [XmlElement(Order = 32)]
        public MISMOIndicator DemandFeatureIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the DemandFeatureIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DemandFeatureIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool DemandFeatureIndicatorSpecified
        {
            get { return DemandFeatureIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the mortgage loan is eligible for lender-paid MI.
        /// </summary>
        [XmlElement(Order = 33)]
        public MISMOIndicator EligibleForLenderPaidMIIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the EligibleForLenderPaidMIIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EligibleForLenderPaidMIIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool EligibleForLenderPaidMIIndicatorSpecified
        {
            get { return EligibleForLenderPaidMIIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates if the loan is part of an employee loan program.
        /// </summary>
        [XmlElement(Order = 34)]
        public MISMOIndicator EmployeeLoanProgramIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the EmployeeLoanProgramIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EmployeeLoanProgramIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool EmployeeLoanProgramIndicatorSpecified
        {
            get { return EmployeeLoanProgramIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether the loan delivery data has been automatically certified against eNote data.
        /// </summary>
        [XmlElement(Order = 35)]
        public MISMOIndicator ENoteCertifiedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ENoteCertifiedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ENoteCertifiedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ENoteCertifiedIndicatorSpecified
        {
            get { return ENoteCertifiedIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates that an eNote is used as part of this loan.
        /// </summary>
        [XmlElement(Order = 36)]
        public MISMOIndicator ENoteIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ENoteIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ENoteIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ENoteIndicatorSpecified
        {
            get { return ENoteIndicator != null; }
            set { }
        }

        /// <summary>
        /// Specifies the reason the loan does not have an escrow account.
        /// </summary>
        [XmlElement(Order = 37)]
        public MISMOEnum<EscrowAbsenceReasonBase> EscrowAbsenceReasonType;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowAbsenceReasonType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowAbsenceReasonType element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowAbsenceReasonTypeSpecified
        {
            get { return this.EscrowAbsenceReasonType != null && this.EscrowAbsenceReasonType.enumValue != EscrowAbsenceReasonBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Escrow Absence Reason Type.
        /// </summary>
        [XmlElement(Order = 38)]
        public MISMOString EscrowAbsenceReasonTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowAbsenceReasonTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowAbsenceReasonTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowAbsenceReasonTypeOtherDescriptionSpecified
        {
            get { return EscrowAbsenceReasonTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Identifies the escrow account status in terms of the requirements of the lender in the mortgage loan transaction.
        /// </summary>
        [XmlElement(Order = 39)]
        public MISMOEnum<EscrowAccountLenderRequirementBase> EscrowAccountLenderRequirementType;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowAccountLenderRequirementType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowAccountLenderRequirementType element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowAccountLenderRequirementTypeSpecified
        {
            get { return this.EscrowAccountLenderRequirementType != null && this.EscrowAccountLenderRequirementType.enumValue != EscrowAccountLenderRequirementBase.Blank; }
            set { }
        }

        /// <summary>
        /// When true, indicates that the establishment of an escrow account has been requested by the borrower(s) for the loan. 
        /// </summary>
        [XmlElement(Order = 40)]
        public MISMOIndicator EscrowAccountRequestedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowAccountRequestedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowAccountRequestedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowAccountRequestedIndicatorSpecified
        {
            get { return EscrowAccountRequestedIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether or not escrows are associated with this loan.
        /// </summary>
        [XmlElement(Order = 41)]
        public MISMOIndicator EscrowIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowIndicatorSpecified
        {
            get { return EscrowIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that, under applicable law, a servicer may not establish an escrow account for the mortgage loan being modified.
        /// </summary>
        [XmlElement(Order = 42)]
        public MISMOIndicator EscrowNotAllowedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowNotAllowedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowNotAllowedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowNotAllowedIndicatorSpecified
        {
            get { return EscrowNotAllowedIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates the borrower has elected to establish an escrow account.
        /// </summary>
        [XmlElement(Order = 43)]
        public MISMOIndicator EscrowsElectedByBorrowerIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowsElectedByBorrowerIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowsElectedByBorrowerIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowsElectedByBorrowerIndicatorSpecified
        {
            get { return EscrowsElectedByBorrowerIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether or not a loan is a Home Equity Line of Credit (HELOC).
        /// </summary>
        [XmlElement(Order = 44)]
        public MISMOIndicator HELOCIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the HELOCIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HELOCIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool HELOCIndicatorSpecified
        {
            get { return HELOCIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates the transaction is considered to be a higher priced mortgage loan under Regulation Z requirements.
        /// </summary>
        [XmlElement(Order = 45)]
        public MISMOIndicator HigherPricedMortgageLoanIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the HigherPricedMortgageLoanIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HigherPricedMortgageLoanIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool HigherPricedMortgageLoanIndicatorSpecified
        {
            get { return HigherPricedMortgageLoanIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether the Home Buyers/Home Ownership Education certificate is included in the loan file.
        /// </summary>
        [XmlElement(Order = 46)]
        public MISMOIndicator HomeBuyersHomeownershipEducationCertificateIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the HomeBuyersHomeownershipEducationCertificateIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HomeBuyersHomeownershipEducationCertificateIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool HomeBuyersHomeownershipEducationCertificateIndicatorSpecified
        {
            get { return HomeBuyersHomeownershipEducationCertificateIndicator != null; }
            set { }
        }

        /// <summary>
        /// The number of months that the initial fixed period of a hybrid ARM is in effect.
        /// </summary>
        [XmlElement(Order = 47)]
        public MISMOCount InitialFixedPeriodEffectiveMonthsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the InitialFixedPeriodEffectiveMonthsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InitialFixedPeriodEffectiveMonthsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool InitialFixedPeriodEffectiveMonthsCountSpecified
        {
            get { return InitialFixedPeriodEffectiveMonthsCount != null; }
            set { }
        }

        /// <summary>
        /// Identifies whether interest was credited to the borrower at time of closing for a closed-end mortgage.
        /// </summary>
        [XmlElement(Order = 48)]
        public MISMOIndicator InterestCreditedToBorrowerIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the InterestCreditedToBorrowerIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InterestCreditedToBorrowerIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool InterestCreditedToBorrowerIndicatorSpecified
        {
            get { return InterestCreditedToBorrowerIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether loan is set up with interest only payments.
        /// </summary>
        [XmlElement(Order = 49)]
        public MISMOIndicator InterestOnlyIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the InterestOnlyIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InterestOnlyIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool InterestOnlyIndicatorSpecified
        {
            get { return InterestOnlyIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that the interest rate on the loan can increase after the closing.
        /// </summary>
        [XmlElement(Order = 50)]
        public MISMOIndicator InterestRateIncreaseIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the InterestRateIncreaseIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InterestRateIncreaseIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool InterestRateIncreaseIndicatorSpecified
        {
            get { return InterestRateIncreaseIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates the loan is determined to be high cost under the jurisdictions high cost rules or requirements.
        /// </summary>
        [XmlElement(Order = 51)]
        public MISMOIndicator JurisdictionHighCostLoanIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the JurisdictionHighCostLoanIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the JurisdictionHighCostLoanIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool JurisdictionHighCostLoanIndicatorSpecified
        {
            get { return JurisdictionHighCostLoanIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that there is lender placed hazard insurance of any kind associated with this loan.
        /// </summary>
        [XmlElement(Order = 52)]
        public MISMOIndicator LenderPlacedHazardInsuranceIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the LenderPlacedHazardInsuranceIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LenderPlacedHazardInsuranceIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool LenderPlacedHazardInsuranceIndicatorSpecified
        {
            get { return LenderPlacedHazardInsuranceIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the Lender will provide self-insurance.
        /// </summary>
        [XmlElement(Order = 53)]
        public MISMOIndicator LenderSelfInsuredIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the LenderSelfInsuredIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LenderSelfInsuredIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool LenderSelfInsuredIndicatorSpecified
        {
            get { return LenderSelfInsuredIndicator != null; }
            set { }
        }

        /// <summary>
        /// The maximum dollar amount limit for the mortgage loan.
        /// </summary>
        [XmlElement(Order = 54)]
        public MISMOAmount LendingLimitAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the LendingLimitAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LendingLimitAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool LendingLimitAmountSpecified
        {
            get { return LendingLimitAmount != null; }
            set { }
        }

        /// <summary>
        /// Specifies the criteria, guidelines or calculation used to establish the Lending Limit Amount.
        /// </summary>
        [XmlElement(Order = 55)]
        public MISMOEnum<LendingLimitBase> LendingLimitType;

        /// <summary>
        /// Gets or sets a value indicating whether the LendingLimitType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LendingLimitType element has been assigned a value.</value>
        [XmlIgnore]
        public bool LendingLimitTypeSpecified
        {
            get { return this.LendingLimitType != null && this.LendingLimitType.enumValue != LendingLimitBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Other is selected for the Lending Limit Type.
        /// </summary>
        [XmlElement(Order = 56)]
        public MISMOString LendingLimitTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the LendingLimitTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LendingLimitTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool LendingLimitTypeOtherDescriptionSpecified
        {
            get { return LendingLimitTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the lien holder of the related loan is the same as the lien holder of the subject loan.
        /// </summary>
        [XmlElement(Order = 57)]
        public MISMOIndicator LienHolderSameAsSubjectLoanIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the LienHolderSameAsSubjectLoanIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LienHolderSameAsSubjectLoanIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool LienHolderSameAsSubjectLoanIndicatorSpecified
        {
            get { return LienHolderSameAsSubjectLoanIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that the loan is classified as an affordable loan by the lender or the investor.
        /// </summary>
        [XmlElement(Order = 58)]
        public MISMOIndicator LoanAffordableIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanAffordableIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanAffordableIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanAffordableIndicatorSpecified
        {
            get { return LoanAffordableIndicator != null; }
            set { }
        }

        /// <summary>
        /// The age of the loan expressed in months calculated as defined by trading partner agreement.
        /// </summary>
        [XmlElement(Order = 59)]
        public MISMOCount LoanAgeMonthsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanAgeMonthsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanAgeMonthsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanAgeMonthsCountSpecified
        {
            get { return LoanAgeMonthsCount != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that the loan amount can increase after closing.
        /// </summary>
        [XmlElement(Order = 60)]
        public MISMOIndicator LoanAmountIncreaseIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanAmountIncreaseIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanAmountIncreaseIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanAmountIncreaseIndicatorSpecified
        {
            get { return LoanAmountIncreaseIndicator != null; }
            set { }
        }

        /// <summary>
        /// The date the mortgage loan approval was made by the lender to the borrower. A loan approval involves verifying credit, down payment, employment history, etc.  of the borrower(s) and will generally state that the borrower is approved for a loan upon satisfaction of certain conditions.
        /// </summary>
        [XmlElement(Order = 61)]
        public MISMODate LoanApprovalDate;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanApprovalDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanApprovalDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanApprovalDateSpecified
        {
            get { return LoanApprovalDate != null; }
            set { }
        }

        /// <summary>
        /// The date the mortgage loan approval expires.
        /// </summary>
        [XmlElement(Order = 62)]
        public MISMODate LoanApprovalExpirationDate;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanApprovalExpirationDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanApprovalExpirationDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanApprovalExpirationDateSpecified
        {
            get { return LoanApprovalExpirationDate != null; }
            set { }
        }

        /// <summary>
        /// The number of days the mortgage loan approval is in effect.
        /// </summary>
        [XmlElement(Order = 63)]
        public MISMOCount LoanApprovalPeriodDaysCount;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanApprovalPeriodDaysCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanApprovalPeriodDaysCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanApprovalPeriodDaysCountSpecified
        {
            get { return LoanApprovalPeriodDaysCount != null; }
            set { }
        }

        /// <summary>
        /// Defines whether the loan is a closed loan purchase or a table funded loan.
        /// </summary>
        [XmlElement(Order = 64)]
        public MISMOEnum<LoanClosingStatusBase> LoanClosingStatusType;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanClosingStatusType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanClosingStatusType element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanClosingStatusTypeSpecified
        {
            get { return this.LoanClosingStatusType != null && this.LoanClosingStatusType.enumValue != LoanClosingStatusBase.Blank; }
            set { }
        }

        /// <summary>
        /// The date the mortgage loan commitment to the borrower was made. The loan commitment is a  formal, binding offer from a lender that a specified amount of loan or line of credit will be made available to the named borrower at a certain interest rate, during a certain period and, usually, for a certain purpose. The commitment also specifies the terms and requirements under which the loan will be advanced.
        /// </summary>
        [XmlElement(Order = 65)]
        public MISMODate LoanCommitmentDate;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanCommitmentDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanCommitmentDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanCommitmentDateSpecified
        {
            get { return LoanCommitmentDate != null; }
            set { }
        }

        /// <summary>
        /// The date the mortgage loan commitment expires.
        /// </summary>
        [XmlElement(Order = 66)]
        public MISMODate LoanCommitmentExpirationDate;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanCommitmentExpirationDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanCommitmentExpirationDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanCommitmentExpirationDateSpecified
        {
            get { return LoanCommitmentExpirationDate != null; }
            set { }
        }

        /// <summary>
        /// The number of days the mortgage loan commitment is in effect.
        /// </summary>
        [XmlElement(Order = 67)]
        public MISMOCount LoanCommitmentPeriodDaysCount;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanCommitmentPeriodDaysCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanCommitmentPeriodDaysCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanCommitmentPeriodDaysCountSpecified
        {
            get { return LoanCommitmentPeriodDaysCount != null; }
            set { }
        }

        /// <summary>
        /// The date the loan funds were released from the original lender to the closing agent for disbursement to the borrower.
        /// </summary>
        [XmlElement(Order = 68)]
        public MISMODate LoanFundingDate;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanFundingDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanFundingDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanFundingDateSpecified
        {
            get { return LoanFundingDate != null; }
            set { }
        }

        /// <summary>
        /// Specifies whether the loan payment schedule has been calculated to fully repay the loan over the mortgage term.
        /// </summary>
        [XmlElement(Order = 69)]
        public MISMOEnum<LoanRepaymentBase> LoanRepaymentType;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanRepaymentType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanRepaymentType element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanRepaymentTypeSpecified
        {
            get { return this.LoanRepaymentType != null && this.LoanRepaymentType.enumValue != LoanRepaymentBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Loan Repayment Type.
        /// </summary>
        [XmlElement(Order = 70)]
        public MISMOString LoanRepaymentTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanRepaymentTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanRepaymentTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanRepaymentTypeOtherDescriptionSpecified
        {
            get { return LoanRepaymentTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The date the investor releases funds to the seller of the loan.
        /// </summary>
        [XmlElement(Order = 71)]
        public MISMODate LoanSaleFundingDate;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanSaleFundingDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanSaleFundingDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanSaleFundingDateSpecified
        {
            get { return LoanSaleFundingDate != null; }
            set { }
        }

        /// <summary>
        /// Specifies the type of collateral securing the loan.
        /// </summary>
        [XmlElement(Order = 72)]
        public MISMOEnum<LoanSecuredByBase> LoanSecuredByType;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanSecuredByType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanSecuredByType element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanSecuredByTypeSpecified
        {
            get { return this.LoanSecuredByType != null && this.LoanSecuredByType.enumValue != LoanSecuredByBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Loan Secured By Type.
        /// </summary>
        [XmlElement(Order = 73)]
        public MISMOString LoanSecuredByTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanSecuredByTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanSecuredByTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanSecuredByTypeOtherDescriptionSpecified
        {
            get { return LoanSecuredByTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        ///  When true, indicates that the loan is secured by some type of collateral.
        /// </summary>
        [XmlElement(Order = 74)]
        public MISMOIndicator LoanSecuredIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanSecuredIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanSecuredIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanSecuredIndicatorSpecified
        {
            get { return LoanSecuredIndicator != null; }
            set { }
        }

        /// <summary>
        /// The widely-recognized alpha code (e.g., AA, A, B, etc.) provided by the seller which specifies the credit risk class of a financial asset (e.g. a loan or mortgage backed security).
        /// </summary>
        [XmlElement(Order = 75)]
        public MISMOString LoanSellerProvidedInvestmentRatingDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanSellerProvidedInvestmentRatingDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanSellerProvidedInvestmentRatingDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanSellerProvidedInvestmentRatingDescriptionSpecified
        {
            get { return LoanSellerProvidedInvestmentRatingDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether or not a lost note affidavit (LNA) is associated with the loan.
        /// </summary>
        [XmlElement(Order = 76)]
        public MISMOIndicator LostNoteAffidavitIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the LostNoteAffidavitIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LostNoteAffidavitIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool LostNoteAffidavitIndicatorSpecified
        {
            get { return LostNoteAffidavitIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that the loan is covered by mortgage insurance.
        /// </summary>
        [XmlElement(Order = 77)]
        public MISMOIndicator MICoverageExistsIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the MICoverageExistsIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MICoverageExistsIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool MICoverageExistsIndicatorSpecified
        {
            get { return MICoverageExistsIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates that Mortgage Insurance is or was required as a condition for originating the loan.
        /// </summary>
        [XmlElement(Order = 78)]
        public MISMOIndicator MIRequiredIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the MIRequiredIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIRequiredIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIRequiredIndicatorSpecified
        {
            get { return MIRequiredIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates that a loan modification exists.
        /// </summary>
        [XmlElement(Order = 79)]
        public MISMOIndicator MortgageModificationIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the MortgageModificationIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MortgageModificationIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool MortgageModificationIndicatorSpecified
        {
            get { return MortgageModificationIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether the loan allows negative amortization.
        /// </summary>
        [XmlElement(Order = 80)]
        public MISMOIndicator NegativeAmortizationIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the NegativeAmortizationIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NegativeAmortizationIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool NegativeAmortizationIndicatorSpecified
        {
            get { return NegativeAmortizationIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true indicates the original loan documents included standard Fannie Mae/Freddie Mac Uniform Instrument provisions for escrow items.
        /// </summary>
        [XmlElement(Order = 81)]
        public MISMOIndicator OriginalLoanStandardEscrowProvisionsIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the OriginalLoanStandardEscrowProvisionsIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OriginalLoanStandardEscrowProvisionsIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool OriginalLoanStandardEscrowProvisionsIndicatorSpecified
        {
            get { return OriginalLoanStandardEscrowProvisionsIndicator != null; }
            set { }
        }

        /// <summary>
        /// The amount of interest accrued but not yet paid for Daily Simple Interest loans. This interest is considered overdue interest and is paid first when a payment is applied.
        /// </summary>
        [XmlElement(Order = 82)]
        public MISMOAmount OverdueInterestAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the OverdueInterestAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OverdueInterestAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool OverdueInterestAmountSpecified
        {
            get { return OverdueInterestAmount != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that the payments on the loan can increase after the closing.
        /// </summary>
        [XmlElement(Order = 83)]
        public MISMOIndicator PaymentIncreaseIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PaymentIncreaseIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PaymentIncreaseIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PaymentIncreaseIndicatorSpecified
        {
            get { return PaymentIncreaseIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates a second lien being carried under the same loan number as a first mortgage. Generally one mortgage is covered by two notes and the borrower makes one combined payment.
        /// </summary>
        [XmlElement(Order = 84)]
        public MISMOIndicator PiggybackLoanIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PiggybackLoanIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PiggybackLoanIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PiggybackLoanIndicatorSpecified
        {
            get { return PiggybackLoanIndicator != null; }
            set { }
        }

        /// <summary>
        /// The unique identifier for a group or pool of loans.  May include relevant prefix and suffix when not parsed into applicable fields.  See Pool Prefix Identifier or Pool Suffix Identifier.
        /// </summary>
        [XmlElement(Order = 85)]
        public MISMOIdentifier PoolIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the PoolIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PoolIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool PoolIdentifierSpecified
        {
            get { return PoolIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The date a mortgage backed security is issued to investors.
        /// </summary>
        [XmlElement(Order = 86)]
        public MISMODate PoolIssueDate;

        /// <summary>
        /// Gets or sets a value indicating whether the PoolIssueDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PoolIssueDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool PoolIssueDateSpecified
        {
            get { return PoolIssueDate != null; }
            set { }
        }

        /// <summary>
        /// The prefix associated with the pool identifier.
        /// </summary>
        [XmlElement(Order = 87)]
        public MISMOIdentifier PoolPrefixIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the PoolPrefixIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PoolPrefixIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool PoolPrefixIdentifierSpecified
        {
            get { return PoolPrefixIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether the loan includes a penalty charged to the borrower in the event of prepayment.
        /// </summary>
        [XmlElement(Order = 88)]
        public MISMOIndicator PrepaymentPenaltyIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaymentPenaltyIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaymentPenaltyIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaymentPenaltyIndicatorSpecified
        {
            get { return PrepaymentPenaltyIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the prepayment penalty has been waived.
        /// </summary>
        [XmlElement(Order = 89)]
        public MISMOIndicator PrepaymentPenaltyWaivedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaymentPenaltyWaivedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaymentPenaltyWaivedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaymentPenaltyWaivedIndicatorSpecified
        {
            get { return PrepaymentPenaltyWaivedIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether the loan has any restrictions on the prepayment of the loan (i.e. paying off the principal and or interest at a rate faster than prescribed by the payment schedule).
        /// </summary>
        [XmlElement(Order = 90)]
        public MISMOIndicator PrepaymentRestrictionIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaymentRestrictionIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaymentRestrictionIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaymentRestrictionIndicatorSpecified
        {
            get { return PrepaymentRestrictionIndicator != null; }
            set { }
        }

        /// <summary>
        /// Case or loan level count of properties already financed by this lender for the borrower(s) named on the loan application.  Does not include subject property.
        /// </summary>
        [XmlElement(Order = 91)]
        public MISMOCount PropertiesFinancedByLenderCount;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertiesFinancedByLenderCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertiesFinancedByLenderCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertiesFinancedByLenderCountSpecified
        {
            get { return PropertiesFinancedByLenderCount != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether a documented inspection of the property has been waived.
        /// </summary>
        [XmlElement(Order = 92)]
        public MISMOIndicator PropertyInspectionWaiverIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyInspectionWaiverIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyInspectionWaiverIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyInspectionWaiverIndicatorSpecified
        {
            get { return PropertyInspectionWaiverIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates the loan is determined to be a qualified mortgage under Regulation Z requirements.
        /// </summary>
        [XmlElement(Order = 93)]
        public MISMOIndicator QualifiedMortgageIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the QualifiedMortgageIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the QualifiedMortgageIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool QualifiedMortgageIndicatorSpecified
        {
            get { return QualifiedMortgageIndicator != null; }
            set { }
        }

        /// <summary>
        /// Monies advanced on the loan (i.e. delinquency expense, tax penalty, repairs, etc.).
        /// </summary>
        [XmlElement(Order = 94)]
        public MISMOAmount RecoverableCorporateAdvanceBalanceFromBorrowerAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the RecoverableCorporateAdvanceBalanceFromBorrowerAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RecoverableCorporateAdvanceBalanceFromBorrowerAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool RecoverableCorporateAdvanceBalanceFromBorrowerAmountSpecified
        {
            get { return RecoverableCorporateAdvanceBalanceFromBorrowerAmount != null; }
            set { }
        }

        /// <summary>
        /// Monies advanced on the loan (i.e. delinquency expense, tax penalty, repairs, etc.).
        /// </summary>
        [XmlElement(Order = 95)]
        public MISMOAmount RecoverableCorporateAdvanceBalanceFromThirdPartyAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the RecoverableCorporateAdvanceBalanceFromThirdPartyAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RecoverableCorporateAdvanceBalanceFromThirdPartyAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool RecoverableCorporateAdvanceBalanceFromThirdPartyAmountSpecified
        {
            get { return RecoverableCorporateAdvanceBalanceFromThirdPartyAmount != null; }
            set { }
        }

        /// <summary>
        /// The amount of overpaid interest refunded to the borrower for the calendar year.
        /// </summary>
        [XmlElement(Order = 96)]
        public MISMOAmount RefundOfOverpaidInterestCalendarYearAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the RefundOfOverpaidInterestCalendarYearAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RefundOfOverpaidInterestCalendarYearAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool RefundOfOverpaidInterestCalendarYearAmountSpecified
        {
            get { return RefundOfOverpaidInterestCalendarYearAmount != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates the loan is determined to be high cost under Regulation Z.
        /// </summary>
        [XmlElement(Order = 97)]
        public MISMOIndicator RegulationZHighCostLoanIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the RegulationZHighCostLoanIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RegulationZHighCostLoanIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool RegulationZHighCostLoanIndicatorSpecified
        {
            get { return RegulationZHighCostLoanIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates if the loan is part of a corporate relocation program.
        /// </summary>
        [XmlElement(Order = 98)]
        public MISMOIndicator RelocationLoanIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the RelocationLoanIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RelocationLoanIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool RelocationLoanIndicatorSpecified
        {
            get { return RelocationLoanIndicator != null; }
            set { }
        }

        /// <summary>
        /// The remaining amount of interest scheduled to be paid on a rule of 78s loan.
        /// </summary>
        [XmlElement(Order = 99)]
        public MISMOAmount RemainingUnearnedInterestAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the RemainingUnearnedInterestAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RemainingUnearnedInterestAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool RemainingUnearnedInterestAmountSpecified
        {
            get { return RemainingUnearnedInterestAmount != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates the loan program uses present equity in the property to fund payments from the lender to the borrower-in lieu of the borrower receiving the proceeds of the loan in a lump sum. As a general rule no payments are made from the borrower to the lender and interest is accrued for payment at the end of the mortgage.
        /// </summary>
        [XmlElement(Order = 100)]
        public MISMOIndicator ReverseMortgageIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ReverseMortgageIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ReverseMortgageIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ReverseMortgageIndicatorSpecified
        {
            get { return ReverseMortgageIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that the loan is subject to the Service members Civil Relief Act (SCRA). 
        /// </summary>
        [XmlElement(Order = 101)]
        public MISMOIndicator SCRAIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the SCRAIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SCRAIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool SCRAIndicatorSpecified
        {
            get { return SCRAIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true indicates that relief under Service member Civil Relief Act (SCRA) has been requested.
        /// </summary>
        [XmlElement(Order = 102)]
        public MISMOIndicator SCRAReliefForbearanceRequestedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the SCRAReliefForbearanceRequestedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SCRAReliefForbearanceRequestedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool SCRAReliefForbearanceRequestedIndicatorSpecified
        {
            get { return SCRAReliefForbearanceRequestedIndicator != null; }
            set { }
        }

        /// <summary>
        /// Specifies the status of relief under the Service members Civil Relief Act (SCRA) for any borrower on the loan.
        /// </summary>
        [XmlElement(Order = 103)]
        public MISMOEnum<SCRAReliefStatusBase> SCRAReliefStatusType;

        /// <summary>
        /// Gets or sets a value indicating whether the SCRAReliefStatusType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SCRAReliefStatusType element has been assigned a value.</value>
        [XmlIgnore]
        public bool SCRAReliefStatusTypeSpecified
        {
            get { return this.SCRAReliefStatusType != null && this.SCRAReliefStatusType.enumValue != SCRAReliefStatusBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected as the Service member Relief Status Type.
        /// </summary>
        [XmlElement(Order = 104)]
        public MISMOString SCRAReliefStatusTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the SCRAReliefStatusTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SCRAReliefStatusTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool SCRAReliefStatusTypeOtherDescriptionSpecified
        {
            get { return SCRAReliefStatusTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the details of the seasonal payment feature, including the period during which periodic payments are not scheduled.
        /// </summary>
        [XmlElement(Order = 105)]
        public MISMOString SeasonalPaymentFeatureDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the SeasonalPaymentFeatureDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SeasonalPaymentFeatureDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool SeasonalPaymentFeatureDescriptionSpecified
        {
            get { return SeasonalPaymentFeatureDescription != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates the loan contains a seasonal payment feature.
        /// </summary>
        [XmlElement(Order = 106)]
        public MISMOIndicator SeasonalPaymentFeatureIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the SeasonalPaymentFeatureIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SeasonalPaymentFeatureIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool SeasonalPaymentFeatureIndicatorSpecified
        {
            get { return SeasonalPaymentFeatureIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that the transfer of servicing is prohibited during life of the loan or for a designated period of time or until a certain event or condition is satisfied.
        /// </summary>
        [XmlElement(Order = 107)]
        public MISMOIndicator ServicingTransferProhibitedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ServicingTransferProhibitedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServicingTransferProhibitedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServicingTransferProhibitedIndicatorSpecified
        {
            get { return ServicingTransferProhibitedIndicator != null; }
            set { }
        }

        /// <summary>
        /// Defines whether the servicing on the loan will be retained or transferred to another party. Typically only on closed loans.
        /// </summary>
        [XmlElement(Order = 108)]
        public MISMOEnum<ServicingTransferStatusBase> ServicingTransferStatusType;

        /// <summary>
        /// Gets or sets a value indicating whether the ServicingTransferStatusType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServicingTransferStatusType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServicingTransferStatusTypeSpecified
        {
            get { return this.ServicingTransferStatusType != null && this.ServicingTransferStatusType.enumValue != ServicingTransferStatusBase.Blank; }
            set { }
        }

        /// <summary>
        /// Indicates the loan is one in which the lender, in exchange for a loan with a favorable interest rate, participates in the profits (if any) the borrow receives when the property is eventually sold.
        /// </summary>
        [XmlElement(Order = 109)]
        public MISMOIndicator SharedAppreciationIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the SharedAppreciationIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SharedAppreciationIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool SharedAppreciationIndicatorSpecified
        {
            get { return SharedAppreciationIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates the mortgage is for resale-restricted, owner-occupied housing in which the rights, responsibilities, and benefits of residential property ownership are shared between individual homeowners and another party.
        /// </summary>
        [XmlElement(Order = 110)]
        public MISMOIndicator SharedEquityIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the SharedEquityIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SharedEquityIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool SharedEquityIndicatorSpecified
        {
            get { return SharedEquityIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the IRS form (4506 or 4506T) that gives the lender the authority to request copies of tax records is present.
        /// </summary>
        [XmlElement(Order = 111)]
        public MISMOIndicator SignedAuthorizationToRequestTaxRecordsIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the SignedAuthorizationToRequestTaxRecordsIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SignedAuthorizationToRequestTaxRecordsIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool SignedAuthorizationToRequestTaxRecordsIndicatorSpecified
        {
            get { return SignedAuthorizationToRequestTaxRecordsIndicator != null; }
            set { }
        }

        /// <summary>
        /// Specifies the details of the step payments feature, including the period during which periodic payments are scheduled to increase.
        /// </summary>
        [XmlElement(Order = 112)]
        public MISMOString StepPaymentsFeatureDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the StepPaymentsFeatureDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the StepPaymentsFeatureDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool StepPaymentsFeatureDescriptionSpecified
        {
            get { return StepPaymentsFeatureDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the IRS form (4506 or 4506T) that gives the lender the authority to request copies of tax records has been executed by the lender.
        /// </summary>
        [XmlElement(Order = 113)]
        public MISMOIndicator TaxRecordsObtainedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the TaxRecordsObtainedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TaxRecordsObtainedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool TaxRecordsObtainedIndicatorSpecified
        {
            get { return TaxRecordsObtainedIndicator != null; }
            set { }
        }

        /// <summary>
        /// Flag indicating if the loan is eligible for a rate reduction if payments are received in a timely manner.
        /// </summary>
        [XmlElement(Order = 114)]
        public MISMOIndicator TimelyPaymentRateReductionIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the TimelyPaymentRateReductionIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TimelyPaymentRateReductionIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool TimelyPaymentRateReductionIndicatorSpecified
        {
            get { return TimelyPaymentRateReductionIndicator != null; }
            set { }
        }

        /// <summary>
        /// The number of 1-4 unit properties that are financed and owned and/or obligated on by the borrower(s). A jointly owned/obligated property by multiple borrowers would count only once.
        /// </summary>
        [XmlElement(Order = 115)]
        public MISMOCount TotalMortgagedPropertiesCount;

        /// <summary>
        /// Gets or sets a value indicating whether the TotalMortgagedPropertiesCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TotalMortgagedPropertiesCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TotalMortgagedPropertiesCountSpecified
        {
            get { return TotalMortgagedPropertiesCount != null; }
            set { }
        }

        /// <summary>
        /// The sum total dollar amount of the value of all sales concessions granted by an interested party including such items as furniture, carpeting, decorator allowances, automobiles, vacations, securities, giveaways or other sales incentives.
        /// </summary>
        [XmlElement(Order = 116)]
        public MISMOAmount TotalSalesConcessionAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the TotalSalesConcessionAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TotalSalesConcessionAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TotalSalesConcessionAmountSpecified
        {
            get { return TotalSalesConcessionAmount != null; }
            set { }
        }

        /// <summary>
        /// The total amount of subordinate financing associated with the loan. For example, the subordinate financing amount as disclosed in the Details of Transaction section of the URLA.
        /// </summary>
        [XmlElement(Order = 117)]
        public MISMOAmount TotalSubordinateFinancingAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the TotalSubordinateFinancingAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TotalSubordinateFinancingAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TotalSubordinateFinancingAmountSpecified
        {
            get { return TotalSubordinateFinancingAmount != null; }
            set { }
        }

        /// <summary>
        /// An indicator denoting whether a Warehouse Bank is involved in the mortgage loan transaction through a relationship with the lender.
        /// </summary>
        [XmlElement(Order = 118)]
        public MISMOIndicator WarehouseLenderIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the WarehouseLenderIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the WarehouseLenderIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool WarehouseLenderIndicatorSpecified
        {
            get { return WarehouseLenderIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that the loan is actively in the workout process.
        /// </summary>
        [XmlElement(Order = 119)]
        public MISMOIndicator WorkoutActiveIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the WorkoutActiveIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the WorkoutActiveIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool WorkoutActiveIndicatorSpecified
        {
            get { return WorkoutActiveIndicator != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 120)]
        public LOAN_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
