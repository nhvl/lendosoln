namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class NEGATIVE_AMORTIZATION
    {
        /// <summary>
        /// Gets a value indicating whether the NEGATIVE_AMORTIZATION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.NegativeAmortizationOccurrencesSpecified
                    || this.NegativeAmortizationRuleSpecified;
            }
        }

        /// <summary>
        /// Occurrences of negative amortization.
        /// </summary>
        [XmlElement("NEGATIVE_AMORTIZATION_OCCURRENCES", Order = 0)]
        public NEGATIVE_AMORTIZATION_OCCURRENCES NegativeAmortizationOccurrences;

        /// <summary>
        /// Gets or sets a value indicating whether the NegativeAmortizationOccurrences element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NegativeAmortizationOccurrences element has been assigned a value.</value>
        [XmlIgnore]
        public bool NegativeAmortizationOccurrencesSpecified
        {
            get { return this.NegativeAmortizationOccurrences != null && this.NegativeAmortizationOccurrences.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A rule about negative amortization.
        /// </summary>
        [XmlElement("NEGATIVE_AMORTIZATION_RULE", Order = 1)]
        public NEGATIVE_AMORTIZATION_RULE NegativeAmortizationRule;

        /// <summary>
        /// Gets or sets a value indicating whether the NegativeAmortizationRule element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NegativeAmortizationRule element has been assigned a value.</value>
        [XmlIgnore]
        public bool NegativeAmortizationRuleSpecified
        {
            get { return this.NegativeAmortizationRule != null && this.NegativeAmortizationRule.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public NEGATIVE_AMORTIZATION_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
