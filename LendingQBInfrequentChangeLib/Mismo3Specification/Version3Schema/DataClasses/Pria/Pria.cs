namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PRIA
    {
        /// <summary>
        /// Gets a value indicating whether the PRIA container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.PRIARequestSpecified
                    || this.PRIAResponseSpecified;
            }
        }

        /// <summary>
        /// A Property Records Industry Association request.
        /// </summary>
        [XmlElement("PRIA_REQUEST", Order = 0)]
        public PRIA_REQUEST PRIARequest;

        /// <summary>
        /// Gets or sets a value indicating whether the Property Records Industry Association Request element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Property Records Industry Association Request element has been assigned a value.</value>
        [XmlIgnore]
        public bool PRIARequestSpecified
        {
            get { return this.PRIARequest != null && this.PRIARequest.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A Property Records Industry Association response.
        /// </summary>
        [XmlElement("PRIA_RESPONSE", Order = 1)]
        public PRIA_RESPONSE PRIAResponse;

        /// <summary>
        /// Gets or sets a value indicating whether the Property Records Industry Association Response element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Property Records Industry Association Response element has been assigned a value.</value>
        [XmlIgnore]
        public bool PRIAResponseSpecified
        {
            get { return this.PRIAResponse != null && this.PRIAResponse.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public PRIA_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
