namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class COUNSELING
    {
        /// <summary>
        /// Gets a value indicating whether the COUNSELING container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CounselingDetailSpecified
                    || this.CounselingEventsSpecified
                    || this.ExtensionSpecified;
            }
        }


        /// <summary>
        /// Details on counseling.
        /// </summary>
        [XmlElement("COUNSELING_DETAIL", Order = 0)]
        public COUNSELING_DETAIL CounselingDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the CounselingDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CounselingDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool CounselingDetailSpecified
        {
            get { return this.CounselingDetail != null && this.CounselingDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A set of counseling events.
        /// </summary>
        [XmlElement("COUNSELING_EVENTS", Order = 1)]
        public COUNSELING_EVENTS CounselingEvents;

        /// <summary>
        /// Gets or sets a value indicating whether the CounselingEvents element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CounselingEvents element has been assigned a value.</value>
        [XmlIgnore]
        public bool CounselingEventsSpecified
        {
            get { return this.CounselingEvents != null && this.CounselingEvents.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public COUNSELING_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
