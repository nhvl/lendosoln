namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class COUNSELING_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the COUNSELING_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CounselingConfirmationIndicatorSpecified
                    || this.ExtensionSpecified
                    || this.HousingCounselingAgenciesListProvidedDateSpecified
                    || this.HUDCounselingInitiatedIndicatorSpecified;
            }
        }


        /// <summary>
        /// Indicates whether or not the applicant has completed the Homebuyer Education program.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIndicator CounselingConfirmationIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the CounselingConfirmationIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CounselingConfirmationIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool CounselingConfirmationIndicatorSpecified
        {
            get { return CounselingConfirmationIndicator != null; }
            set { }
        }

        /// <summary>
        /// The date the lender or originator provides the borrower with a written list of homeownership counseling organizations in the borrower's location.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMODate HousingCounselingAgenciesListProvidedDate;

        /// <summary>
        /// Gets or sets a value indicating whether the HousingCounselingAgenciesListProvidedDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HousingCounselingAgenciesListProvidedDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool HousingCounselingAgenciesListProvidedDateSpecified
        {
            get { return HousingCounselingAgenciesListProvidedDate != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that the  borrower has agreed to HUD counseling as a condition of the loan modification.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOIndicator HUDCounselingInitiatedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the HUDCounselingInitiatedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HUDCounselingInitiatedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool HUDCounselingInitiatedIndicatorSpecified
        {
            get { return HUDCounselingInitiatedIndicator != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public COUNSELING_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
