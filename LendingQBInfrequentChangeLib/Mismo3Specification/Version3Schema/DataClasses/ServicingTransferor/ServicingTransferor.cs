namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class SERVICING_TRANSFEROR
    {
        /// <summary>
        /// Gets a value indicating whether the SERVICING_TRANSFEROR container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ServicingTransferorInactiveIndicatorSpecified;
            }
        }

        /// <summary>
        /// If true, indicates the servicing transferor does not wish to remain an active servicer with the investor.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIndicator ServicingTransferorInactiveIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ServicingTransferorInactiveIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServicingTransferorInactiveIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServicingTransferorInactiveIndicatorSpecified
        {
            get { return ServicingTransferorInactiveIndicator != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public SERVICING_TRANSFEROR_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
