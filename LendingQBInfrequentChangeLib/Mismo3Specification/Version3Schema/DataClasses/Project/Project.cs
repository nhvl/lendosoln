namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PROJECT
    {
        /// <summary>
        /// Gets a value indicating whether the PROJECT container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DataSourcesSpecified
                    || this.ExtensionSpecified
                    || this.PhasesSpecified
                    || this.ProjectAnalysisSpecified
                    || this.ProjectBlanketFinancingsSpecified
                    || this.ProjectCarStoragesSpecified
                    || this.ProjectCommonElementsSpecified
                    || this.ProjectConversionSpecified
                    || this.ProjectDetailSpecified
                    || this.ProjectDeveloperSpecified
                    || this.ProjectFinancialInformationSpecified
                    || this.ProjectHousingUnitInventoriesSpecified
                    || this.ProjectManagementSpecified
                    || this.ProjectOwnershipSpecified
                    || this.ProjectRatingsSpecified;
            }
        }

        /// <summary>
        /// Data sources for the project.
        /// </summary>
        [XmlElement("DATA_SOURCES", Order = 0)]
        public DATA_SOURCES DataSources;

        /// <summary>
        /// Gets or sets a value indicating whether the DataSources element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DataSources element has been assigned a value.</value>
        [XmlIgnore]
        public bool DataSourcesSpecified
        {
            get { return this.DataSources != null && this.DataSources.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Phases of the project.
        /// </summary>
        [XmlElement("PHASES", Order = 1)]
        public PHASES Phases;

        /// <summary>
        /// Gets or sets a value indicating whether the Phases element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Phases element has been assigned a value.</value>
        [XmlIgnore]
        public bool PhasesSpecified
        {
            get { return this.Phases != null && this.Phases.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Analysis of the project.
        /// </summary>
        [XmlElement("PROJECT_ANALYSIS", Order = 2)]
        public PROJECT_ANALYSIS ProjectAnalysis;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectAnalysis element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectAnalysis element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectAnalysisSpecified
        {
            get { return this.ProjectAnalysis != null && this.ProjectAnalysis.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Blanket financing for the project.
        /// </summary>
        [XmlElement("PROJECT_BLANKET_FINANCINGS", Order = 3)]
        public PROJECT_BLANKET_FINANCINGS ProjectBlanketFinancings;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectBlanketFinancings element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectBlanketFinancings element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectBlanketFinancingsSpecified
        {
            get { return this.ProjectBlanketFinancings != null && this.ProjectBlanketFinancings.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Car storages of the project.
        /// </summary>
        [XmlElement("PROJECT_CAR_STORAGES", Order = 4)]
        public CAR_STORAGES ProjectCarStorages;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectCarStorages element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectCarStorages element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectCarStoragesSpecified
        {
            get { return this.ProjectCarStorages != null && this.ProjectCarStorages.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Common elements of the project.
        /// </summary>
        [XmlElement("PROJECT_COMMON_ELEMENTS", Order = 5)]
        public COMMON_ELEMENTS ProjectCommonElements;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectCommonElements element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectCommonElements element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectCommonElementsSpecified
        {
            get { return this.ProjectCommonElements != null && this.ProjectCommonElements.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The project conversion.
        /// </summary>
        [XmlElement("PROJECT_CONVERSION", Order = 6)]
        public CONVERSION ProjectConversion;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectConversion element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectConversion element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectConversionSpecified
        {
            get { return this.ProjectConversion != null && this.ProjectConversion.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details on the project.
        /// </summary>
        [XmlElement("PROJECT_DETAIL", Order = 7)]
        public PROJECT_DETAIL ProjectDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectDetailSpecified
        {
            get { return this.ProjectDetail != null && this.ProjectDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Developer for the project.
        /// </summary>
        [XmlElement("PROJECT_DEVELOPER", Order = 8)]
        public PROJECT_DEVELOPER ProjectDeveloper;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectDeveloper element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectDeveloper element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectDeveloperSpecified
        {
            get { return this.ProjectDeveloper != null && this.ProjectDeveloper.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Financial information on the project.
        /// </summary>
        [XmlElement("PROJECT_FINANCIAL_INFORMATION", Order = 9)]
        public PROJECT_FINANCIAL_INFORMATION ProjectFinancialInformation;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectFinancialInformation element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectFinancialInformation element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectFinancialInformationSpecified
        {
            get { return this.ProjectFinancialInformation != null && this.ProjectFinancialInformation.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Housing unit inventories for the project.
        /// </summary>
        [XmlElement("PROJECT_HOUSING_UNIT_INVENTORIES", Order = 10)]
        public HOUSING_UNIT_INVENTORIES ProjectHousingUnitInventories;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectHousingUnitInventories element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectHousingUnitInventories element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectHousingUnitInventoriesSpecified
        {
            get { return this.ProjectHousingUnitInventories != null && this.ProjectHousingUnitInventories.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Management for the project.
        /// </summary>
        [XmlElement("PROJECT_MANAGEMENT", Order = 11)]
        public PROJECT_MANAGEMENT ProjectManagement;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectManagement element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectManagement element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectManagementSpecified
        {
            get { return this.ProjectManagement != null && this.ProjectManagement.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Describes the ownership of the project.
        /// </summary>
        [XmlElement("PROJECT_OWNERSHIP", Order = 12)]
        public PROJECT_OWNERSHIP ProjectOwnership;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectOwnership element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectOwnership element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectOwnershipSpecified
        {
            get { return this.ProjectOwnership != null && this.ProjectOwnership.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A list of project ratings.
        /// </summary>
        [XmlElement("PROJECT_RATINGS", Order = 13)]
        public PROJECT_RATINGS ProjectRatings;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectRatings element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectRatings element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectRatingsSpecified
        {
            get { return this.ProjectRatings != null && this.ProjectRatings.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 14)]
        public PROJECT_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
