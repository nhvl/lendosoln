namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CREDIT_SCORE_PROVIDER
    {
        /// <summary>
        /// Gets a value indicating whether the CREDIT_SCORE_PROVIDER container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AddressSpecified
                    || this.CreditScoreProviderDetailSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// Address of a credit score provider.
        /// </summary>
        [XmlElement("ADDRESS", Order = 0)]
        public ADDRESS Address;

        /// <summary>
        /// Gets or sets a value indicating whether the Address element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Address element has been assigned a value.</value>
        [XmlIgnore]
        public bool AddressSpecified
        {
            get { return this.Address != null && this.Address.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details on a credit score provider.
        /// </summary>
        [XmlElement("CREDIT_SCORE_PROVIDER_DETAIL", Order = 1)]
        public CREDIT_SCORE_PROVIDER_DETAIL CreditScoreProviderDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditScoreProviderDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditScoreProviderDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditScoreProviderDetailSpecified
        {
            get { return this.CreditScoreProviderDetail != null && this.CreditScoreProviderDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public CREDIT_SCORE_PROVIDER_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
