namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CREDIT_SCORE_PROVIDER_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the CREDIT_SCORE_PROVIDER_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditScoreProviderNameSpecified
                    || this.CreditScoreProviderURLSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// Identifies the name of the company providing the credit score data. This is one of the requirements of the 2003 FACT Act.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString CreditScoreProviderName;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditScoreProviderName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditScoreProviderName element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditScoreProviderNameSpecified
        {
            get { return CreditScoreProviderName != null; }
            set { }
        }

        /// <summary>
        /// Specifies the URL or web site address of the company providing the credit score data.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOURL CreditScoreProviderURL;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditScoreProviderURL element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditScoreProviderURL element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditScoreProviderURLSpecified
        {
            get { return CreditScoreProviderURL != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public CREDIT_SCORE_PROVIDER_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
