namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class FORECLOSURE_EXPENSES
    {
        /// <summary>
        /// Gets a value indicating whether the FORECLOSURE_EXPENSES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ForeclosureExpenseSpecified
                    || this.ForeclosureExpenseSummarySpecified;
            }
        }

        /// <summary>
        /// A collection of foreclosure expenses.
        /// </summary>
        [XmlElement("FORECLOSURE_EXPENSE", Order = 0)]
		public List<FORECLOSURE_EXPENSE> ForeclosureExpense = new List<FORECLOSURE_EXPENSE>();

        /// <summary>
        /// Gets or sets a value indicating whether the ForeclosureExpense element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ForeclosureExpense element has been assigned a value.</value>
        [XmlIgnore]
        public bool ForeclosureExpenseSpecified
        {
            get { return this.ForeclosureExpense != null && this.ForeclosureExpense.Count(f => f != null && f.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A summary of foreclosure expenses.
        /// </summary>
        [XmlElement("FORECLOSURE_EXPENSE_SUMMARY", Order = 1)]
        public FORECLOSURE_EXPENSE_SUMMARY ForeclosureExpenseSummary;

        /// <summary>
        /// Gets or sets a value indicating whether the ForeclosureExpenseSummary element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ForeclosureExpenseSummary element has been assigned a value.</value>
        [XmlIgnore]
        public bool ForeclosureExpenseSummarySpecified
        {
            get { return this.ForeclosureExpenseSummary != null && this.ForeclosureExpenseSummary.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public FORECLOSURE_EXPENSES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
