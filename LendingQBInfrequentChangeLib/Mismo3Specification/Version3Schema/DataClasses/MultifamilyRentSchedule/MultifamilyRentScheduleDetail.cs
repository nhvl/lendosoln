namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class MULTIFAMILY_RENT_SCHEDULE_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the MULTIFAMILY_RENT_SCHEDULE_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.MarketRentalDataCommentDescriptionSpecified
                    || this.RentalActualAdditionalMonthlyIncomeAmountSpecified
                    || this.RentalActualGrossMonthlyRentAmountSpecified
                    || this.RentalActualTotalMonthlyIncomeAmountSpecified
                    || this.RentalEstimatedAdditionalMonthlyIncomeAmountSpecified
                    || this.RentalEstimatedGrossMonthlyRentAmountSpecified
                    || this.RentalEstimatedTotalMonthlyIncomeAmountSpecified;
            }
        }

        /// <summary>
        /// A free-form text field used to describe or comment on the market rental data used in the determination of the Estimated Market Monthly Rent Amount for a living unit or property.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString MarketRentalDataCommentDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MarketRentalDataCommentDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MarketRentalDataCommentDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MarketRentalDataCommentDescriptionSpecified
        {
            get { return MarketRentalDataCommentDescription != null; }
            set { }
        }

        /// <summary>
        /// The dollar value of additional monthly income derived as a result of renting property.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOAmount RentalActualAdditionalMonthlyIncomeAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the RentalActualAdditionalMonthlyIncomeAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RentalActualAdditionalMonthlyIncomeAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool RentalActualAdditionalMonthlyIncomeAmountSpecified
        {
            get { return RentalActualAdditionalMonthlyIncomeAmount != null; }
            set { }
        }

        /// <summary>
        /// The dollar value of the gross monthly rental income derived from a single rental unit or property.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOAmount RentalActualGrossMonthlyRentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the RentalActualGrossMonthlyRentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RentalActualGrossMonthlyRentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool RentalActualGrossMonthlyRentAmountSpecified
        {
            get { return RentalActualGrossMonthlyRentAmount != null; }
            set { }
        }

        /// <summary>
        /// The dollar value of the total monthly income including rental and other supplemental income.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOAmount RentalActualTotalMonthlyIncomeAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the RentalActualTotalMonthlyIncomeAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RentalActualTotalMonthlyIncomeAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool RentalActualTotalMonthlyIncomeAmountSpecified
        {
            get { return RentalActualTotalMonthlyIncomeAmount != null; }
            set { }
        }

        /// <summary>
        /// The dollar value of estimated additional monthly income derived as a result of renting property.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOAmount RentalEstimatedAdditionalMonthlyIncomeAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the RentalEstimatedAdditionalMonthlyIncomeAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RentalEstimatedAdditionalMonthlyIncomeAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool RentalEstimatedAdditionalMonthlyIncomeAmountSpecified
        {
            get { return RentalEstimatedAdditionalMonthlyIncomeAmount != null; }
            set { }
        }

        /// <summary>
        /// The dollar value of the estimated gross monthly rental income derived from single rental unit or property.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOAmount RentalEstimatedGrossMonthlyRentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the RentalEstimatedGrossMonthlyRentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RentalEstimatedGrossMonthlyRentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool RentalEstimatedGrossMonthlyRentAmountSpecified
        {
            get { return RentalEstimatedGrossMonthlyRentAmount != null; }
            set { }
        }

        /// <summary>
        /// The dollar value of the total estimated market income including estimated market rental and supplemental income.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOAmount RentalEstimatedTotalMonthlyIncomeAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the RentalEstimatedTotalMonthlyIncomeAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RentalEstimatedTotalMonthlyIncomeAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool RentalEstimatedTotalMonthlyIncomeAmountSpecified
        {
            get { return RentalEstimatedTotalMonthlyIncomeAmount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 7)]
        public MULTIFAMILY_RENT_SCHEDULE_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
