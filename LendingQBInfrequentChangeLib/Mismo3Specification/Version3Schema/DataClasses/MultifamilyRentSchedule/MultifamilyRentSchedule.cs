namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class MULTIFAMILY_RENT_SCHEDULE
    {
        /// <summary>
        /// Gets a value indicating whether the MULTIFAMILY_RENT_SCHEDULE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.MultifamilyRentScheduleDetailSpecified
                    || this.RentIncludesUtilitiesSpecified
                    || this.UnitRentSchedulesSpecified;
            }
        }

        /// <summary>
        /// Rent schedule for a multi-family situation.
        /// </summary>
        [XmlElement("MULTIFAMILY_RENT_SCHEDULE_DETAIL", Order = 0)]
        public MULTIFAMILY_RENT_SCHEDULE_DETAIL MultifamilyRentScheduleDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the MultifamilyRentScheduleDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MultifamilyRentScheduleDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool MultifamilyRentScheduleDetailSpecified
        {
            get { return this.MultifamilyRentScheduleDetail != null && this.MultifamilyRentScheduleDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Value of rent plus utilities.
        /// </summary>
        [XmlElement("RENT_INCLUDES_UTILITIES", Order = 1)]
        public RENT_INCLUDES_UTILITIES RentIncludesUtilities;

        /// <summary>
        /// Gets or sets a value indicating whether the RentIncludesUtilities element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RentIncludesUtilities element has been assigned a value.</value>
        [XmlIgnore]
        public bool RentIncludesUtilitiesSpecified
        {
            get { return this.RentIncludesUtilities != null && this.RentIncludesUtilities.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Rent schedules per unit.
        /// </summary>
        [XmlElement("UNIT_RENT_SCHEDULES", Order = 2)]
        public UNIT_RENT_SCHEDULES UnitRentSchedules;

        /// <summary>
        /// Gets or sets a value indicating whether the UnitRentSchedules element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the UnitRentSchedules element has been assigned a value.</value>
        [XmlIgnore]
        public bool UnitRentSchedulesSpecified
        {
            get { return this.UnitRentSchedules != null && this.UnitRentSchedules.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public MULTIFAMILY_RENT_SCHEDULE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
