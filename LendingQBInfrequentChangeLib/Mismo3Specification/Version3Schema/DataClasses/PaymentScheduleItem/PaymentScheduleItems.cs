namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PAYMENT_SCHEDULE_ITEMS
    {
        /// <summary>
        /// Gets a value indicating whether the PAYMENT_SCHEDULE_ITEMS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.PaymentScheduleItemSpecified;
            }
        }

        /// <summary>
        /// A collection of payment schedule items.
        /// </summary>
        [XmlElement("PAYMENT_SCHEDULE_ITEM", Order = 0)]
		public List<PAYMENT_SCHEDULE_ITEM> PaymentScheduleItem = new List<PAYMENT_SCHEDULE_ITEM>();

        /// <summary>
        /// Gets or sets a value indicating whether the PaymentScheduleItem element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PaymentScheduleItem element has been assigned a value.</value>
        [XmlIgnore]
        public bool PaymentScheduleItemSpecified
        {
            get { return this.PaymentScheduleItem != null && this.PaymentScheduleItem.Count(p => p != null && p.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public PAYMENT_SCHEDULE_ITEMS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
