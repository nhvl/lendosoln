namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class PAYMENT_SCHEDULE_ITEM
    {
        /// <summary>
        /// Gets a value indicating whether the PAYMENT_SCHEDULE_ITEM container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.PaymentSchedulePaymentAmountSpecified
                    || this.PaymentSchedulePaymentsCountSpecified
                    || this.PaymentSchedulePaymentStartDateSpecified
                    || this.PaymentSchedulePaymentVaryingToAmountSpecified
                    || this.PaymentSchedulePrincipalAndInterestPaymentAmountSpecified;
            }
        }

        /// <summary>
        /// The dollar amount of the payment reflected in the payment schedule on the TIL Disclosure document.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount PaymentSchedulePaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PaymentSchedulePaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PaymentSchedulePaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PaymentSchedulePaymentAmountSpecified
        {
            get { return PaymentSchedulePaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// The total number of payments due of a particular dollar amount reflected in the payment schedule of the TIL Disclosure document.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOCount PaymentSchedulePaymentsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the PaymentSchedulePaymentsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PaymentSchedulePaymentsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PaymentSchedulePaymentsCountSpecified
        {
            get { return PaymentSchedulePaymentsCount != null; }
            set { }
        }

        /// <summary>
        /// The date used to indicate the first date a scheduled payment is due. This date is used to indicate the start dates of any payments within a sequence of scheduled payments that may be reflected on the TIL Disclosure document.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMODate PaymentSchedulePaymentStartDate;

        /// <summary>
        /// Gets or sets a value indicating whether the PaymentSchedulePaymentStartDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PaymentSchedulePaymentStartDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool PaymentSchedulePaymentStartDateSpecified
        {
            get { return PaymentSchedulePaymentStartDate != null; }
            set { }
        }

        /// <summary>
        /// The lower payment amount of the monthly loan payment as reflected in the Payment Schedule on the TIL Disclosure document. This field is used to reflect the lower monthly payment amount in a range of monthly payment amounts typically calculated in an FHA transaction or a declining method PMI transaction and is required to be disclosed on the TIL Disclosure document. 
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOAmount PaymentSchedulePaymentVaryingToAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PaymentSchedulePaymentVaryingToAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PaymentSchedulePaymentVaryingToAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PaymentSchedulePaymentVaryingToAmountSpecified
        {
            get { return PaymentSchedulePaymentVaryingToAmount != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of the portion of the total payment amount which is applied to the principal and interest due on the loan as reflected on the Payment Schedule for the loan.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOAmount PaymentSchedulePrincipalAndInterestPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PaymentSchedulePrincipalAndInterestPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PaymentSchedulePrincipalAndInterestPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PaymentSchedulePrincipalAndInterestPaymentAmountSpecified
        {
            get { return PaymentSchedulePrincipalAndInterestPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 5)]
        public PAYMENT_SCHEDULE_ITEM_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
