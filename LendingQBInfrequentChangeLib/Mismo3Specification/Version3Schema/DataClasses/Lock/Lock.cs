namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class LOCK
    {
        /// <summary>
        /// Gets a value indicating whether the LOCK container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.LoanPriceQuoteIdentifierSpecified
                || this.LoanPriceQuoteTypeOtherDescriptionSpecified
                || this.LoanPriceQuoteTypeSpecified
                || this.LockDatetimeSpecified
                || this.LockDurationDaysCountSpecified
                || this.LockExpirationDatetimeSpecified
                || this.LockIdentifierSpecified
                || this.LockRequestedExtensionDaysCountSpecified
                || this.LockStatusTypeOtherDescriptionSpecified
                || this.LockStatusTypeSpecified
                || this.PriceRequestIdentifierSpecified
                || this.PriceResponseIdentifierSpecified
                || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A unique identifier for a loan price quote, assigned by the party that makes the price quote for tracking purposes.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIdentifier LoanPriceQuoteIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanPriceQuoteIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanPriceQuoteIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanPriceQuoteIdentifierSpecified
        {
            get { return LoanPriceQuoteIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The supply chain of loans, both before and after a loan closing involves funds to purchase the asset and expected or real income from the sale of the asset.  In the supply chain one organizations sale price is the purchase price of another organization. 
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<LoanPriceQuoteBase> LoanPriceQuoteType;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanPriceQuoteType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanPriceQuoteType element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanPriceQuoteTypeSpecified
        {
            get { return this.LoanPriceQuoteType != null && this.LoanPriceQuoteType.enumValue != LoanPriceQuoteBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information or a description of the Loan Price Quote Type when Other is selected as the value.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString LoanPriceQuoteTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanPriceQuoteTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanPriceQuoteTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanPriceQuoteTypeOtherDescriptionSpecified
        {
            get { return LoanPriceQuoteTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The date and time on which the agreement to lock a price and interest rate was made.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMODatetime LockDatetime;

        /// <summary>
        /// Gets or sets a value indicating whether the Lock date/time element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Lock date/time element has been assigned a value.</value>
        [XmlIgnore]
        public bool LockDatetimeSpecified
        {
            get { return LockDatetime != null; }
            set { }
        }

        /// <summary>
        /// The number of days during which the loan price and interest rate lock is in effect.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOCount LockDurationDaysCount;

        /// <summary>
        /// Gets or sets a value indicating whether the LockDurationDaysCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LockDurationDaysCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool LockDurationDaysCountSpecified
        {
            get { return LockDurationDaysCount != null; }
            set { }
        }

        /// <summary>
        /// The date and time that the loan price and interest rate expires.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMODatetime LockExpirationDatetime;

        /// <summary>
        /// Gets or sets a value indicating whether the Lock Expiration date/time element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Lock Expiration date/time element has been assigned a value.</value>
        [XmlIgnore]
        public bool LockExpirationDatetimeSpecified
        {
            get { return LockExpirationDatetime != null; }
            set { }
        }

        /// <summary>
        /// A unique identifier for a price and interest rate lock; used for tracking purposes by the parties to the lock. 
        /// Replaced by use of Loan Identifier Type.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOIdentifier LockIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the LockIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LockIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool LockIdentifierSpecified
        {
            get { return LockIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Specifies the number of days requested to extend the loan price and interest rate lock beyond the current lock expiration.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOCount LockRequestedExtensionDaysCount;

        /// <summary>
        /// Gets or sets a value indicating whether the LockRequestedExtensionDaysCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LockRequestedExtensionDaysCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool LockRequestedExtensionDaysCountSpecified
        {
            get { return LockRequestedExtensionDaysCount != null; }
            set { }
        }

        /// <summary>
        /// The state of the loan price and interest rate lock that is established between a borrower and a lender or between a lender and an investor.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOEnum<LockStatusBase> LockStatusType;

        /// <summary>
        /// Gets or sets a value indicating whether the LockStatusType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LockStatusType element has been assigned a value.</value>
        [XmlIgnore]
        public bool LockStatusTypeSpecified
        {
            get { return this.LockStatusType != null && this.LockStatusType.enumValue != LockStatusBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information or a description of the Lock Status Type when Other is selected as the value.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOString LockStatusTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the LockStatusTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LockStatusTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool LockStatusTypeOtherDescriptionSpecified
        {
            get { return LockStatusTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A unique identifier for a price request, assigned by the requestor, used for tracking the request and any responses.
        /// Replaced by use of Loan Identifier Type.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOIdentifier PriceRequestIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the PriceRequestIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PriceRequestIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool PriceRequestIdentifierSpecified
        {
            get { return PriceRequestIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A unique identifier for a price response, assigned by the responder, used to track or refer to the price response.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOIdentifier PriceResponseIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the PriceResponseIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PriceResponseIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool PriceResponseIdentifierSpecified
        {
            get { return PriceResponseIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 12)]
        public LOCK_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
