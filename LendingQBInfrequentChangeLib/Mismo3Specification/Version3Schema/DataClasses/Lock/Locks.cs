namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class LOCKS
    {
        /// <summary>
        /// Gets a value indicating whether the LOCKS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.LockSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of locks.
        /// </summary>
        [XmlElement("LOCK", Order = 0)]
		public List<LOCK> Lock = new List<LOCK>();

        /// <summary>
        /// Gets or sets a value indicating whether the Lock element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Lock element has been assigned a value.</value>
        [XmlIgnore]
        public bool LockSpecified
        {
            get { return Lock != null && Lock.Count(l => l != null && l.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public LOCKS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
