namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class SIGNATORIES
    {
        /// <summary>
        /// Gets a value indicating whether the SIGNATORIES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.SignatorySpecified;
            }
        }

        /// <summary>
        /// A collection of signatories.
        /// </summary>
        [XmlElement("SIGNATORY", Order = 0)]
		public List<SIGNATORY> Signatory = new List<SIGNATORY>();

        /// <summary>
        /// Gets or sets a value indicating whether the Signatory element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Signatory element has been assigned a value.</value>
        [XmlIgnore]
        public bool SignatorySpecified
        {
            get { return this.Signatory != null && this.Signatory.Count(s => s != null && s.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public SIGNATORIES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
