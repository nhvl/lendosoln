namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class SIGNATORY
    {
        /// <summary>
        /// Gets a value indicating whether the SIGNATORY container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ElectronicSignatureSpecified
                    || this.ExecutionSpecified
                    || this.ExtensionSpecified
                    || this.NotaryCertificateSpecified;
            }
        }

        /// <summary>
        /// An electronic signature.
        /// </summary>
        [XmlElement("ELECTRONIC_SIGNATURE", Order = 0)]
        public ELECTRONIC_SIGNATURE ElectronicSignature;

        /// <summary>
        /// Gets or sets a value indicating whether the ElectronicSignature element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ElectronicSignature element has been assigned a value.</value>
        [XmlIgnore]
        public bool ElectronicSignatureSpecified
        {
            get { return this.ElectronicSignature != null && this.ElectronicSignature.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The signing event.
        /// </summary>
        [XmlElement("EXECUTION", Order = 1)]
        public EXECUTION Execution;

        /// <summary>
        /// Gets or sets a value indicating whether the Execution element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Execution element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExecutionSpecified
        {
            get { return this.Execution != null && this.Execution.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A notary certificate related to the signatory.
        /// </summary>
        [XmlElement("NOTARY_CERTIFICATE", Order = 2)]
        public NOTARY_CERTIFICATE NotaryCertificate;

        /// <summary>
        /// Gets or sets a value indicating whether the NotaryCertificate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NotaryCertificate element has been assigned a value.</value>
        [XmlIgnore]
        public bool NotaryCertificateSpecified
        {
            get { return this.NotaryCertificate != null && this.NotaryCertificate.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public SIGNATORY_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
