namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class BUYDOWN_OCCURRENCES
    {
        /// <summary>
        /// Gets a value indicating whether the BUYDOWN_OCCURRENCES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BuydownOccurrenceSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of buy down occurrences.
        /// </summary>
        [XmlElement("BUYDOWN_OCCURRENCE", Order = 0)]
		public List<BUYDOWN_OCCURRENCE> BuydownOccurrence = new List<BUYDOWN_OCCURRENCE>();

        /// <summary>
        /// Gets or sets a value indicating whether the BuyDownOccurrence element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BuyDownOccurrence element has been assigned a value.</value>
        [XmlIgnore]
        public bool BuydownOccurrenceSpecified
        {
            get { return this.BuydownOccurrence != null && this.BuydownOccurrence.Count(b => b != null && b.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public BUYDOWN_OCCURRENCES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
