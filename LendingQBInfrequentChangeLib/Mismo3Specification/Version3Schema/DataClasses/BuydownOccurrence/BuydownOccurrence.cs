namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class BUYDOWN_OCCURRENCE
    {
        /// <summary>
        /// Gets a value indicating whether the BUYDOWN_OCCURRENCE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BuydownInitialEffectiveInterestRatePercentSpecified
                    || this.BuydownOccurrenceEffectiveDateSpecified
                    || this.ExtensionSpecified
                    || this.RemainingBuydownBalanceAmountSpecified;
            }
        }

        /// <summary>
        /// The final bought down interest rate. The rate that the applicant will pay after the buy down is applied.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOPercent BuydownInitialEffectiveInterestRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the BuyDownInitialEffectiveInterestRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BuyDownInitialEffectiveInterestRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool BuydownInitialEffectiveInterestRatePercentSpecified
        {
            get { return BuydownInitialEffectiveInterestRatePercent != null; }
            set { }
        }

        /// <summary>
        /// The effective date of the Buy down Occurrence that is being reported.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMODate BuydownOccurrenceEffectiveDate;

        /// <summary>
        /// Gets or sets a value indicating whether the BuyDownOccurrenceEffectiveDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BuyDownOccurrenceEffectiveDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool BuydownOccurrenceEffectiveDateSpecified
        {
            get { return BuydownOccurrenceEffectiveDate != null; }
            set { }
        }

        /// <summary>
        /// Current remaining buy down balance.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOAmount RemainingBuydownBalanceAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the RemainingBuyDownBalanceAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RemainingBuyDownBalanceAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool RemainingBuydownBalanceAmountSpecified
        {
            get { return RemainingBuydownBalanceAmount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public BUYDOWN_OCCURRENCE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
