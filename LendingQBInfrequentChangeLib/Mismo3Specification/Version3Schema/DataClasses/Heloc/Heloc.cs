namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class HELOC
    {
        /// <summary>
        /// Gets a value indicating whether the HELOC container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.HELOCOccurrencesSpecified
                    || this.HELOCRuleSpecified;
            }
        }

        /// <summary>
        /// Occurrences associated with a HELOC.
        /// </summary>
        [XmlElement("HELOC_OCCURRENCES", Order = 0)]
        public HELOC_OCCURRENCES HELOCOccurrences;

        /// <summary>
        /// Gets or sets a value indicating whether the Home Equity Line Of Credit Occurrences element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Home Equity Line Of Credit Occurrences element has been assigned a value.</value>
        [XmlIgnore]
        public bool HELOCOccurrencesSpecified
        {
            get { return this.HELOCOccurrences != null && this.HELOCOccurrences.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A rule on a HELOC.
        /// </summary>
        [XmlElement("HELOC_RULE", Order = 1)]
        public HELOC_RULE HELOCRule;

        /// <summary>
        /// Gets or sets a value indicating whether the Home Equity Line Of Credit Rule element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Home Equity Line Of Credit Rule element has been assigned a value.</value>
        [XmlIgnore]
        public bool HELOCRuleSpecified
        {
            get { return this.HELOCRule != null && this.HELOCRule.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public HELOC_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
