namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class LATE_CHARGE_RULE
    {
        /// <summary>
        /// Gets a value indicating whether the LATE_CHARGE_RULE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.LateChargeAmountSpecified
                    || this.LateChargeGracePeriodDaysCountSpecified
                    || this.LateChargeMaximumAmountSpecified
                    || this.LateChargeMinimumAmountSpecified
                    || this.LateChargeRatePercentSpecified
                    || this.LateChargeTypeSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// The dollar amount that a borrower is required to pay for failure to make a regular installment within the period specified on the note. This is used when late charge is defined as a flat dollar amount.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount LateChargeAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the LateChargeAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LateChargeAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool LateChargeAmountSpecified
        {
            get { return LateChargeAmount != null; }
            set { }
        }

        /// <summary>
        /// The grace period in days for this loan before a late charge will be applied.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOCount LateChargeGracePeriodDaysCount;

        /// <summary>
        /// Gets or sets a value indicating whether the LateChargeGracePeriodDaysCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LateChargeGracePeriodDaysCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool LateChargeGracePeriodDaysCountSpecified
        {
            get { return LateChargeGracePeriodDaysCount != null; }
            set { }
        }

        /// <summary>
        /// The maximum monthly late charge amount allowed.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOAmount LateChargeMaximumAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the LateChargeMaximumAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LateChargeMaximumAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool LateChargeMaximumAmountSpecified
        {
            get { return LateChargeMaximumAmount != null; }
            set { }
        }

        /// <summary>
        /// The minimum monthly late charge amount allowed.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOAmount LateChargeMinimumAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the LateChargeMinimumAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LateChargeMinimumAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool LateChargeMinimumAmountSpecified
        {
            get { return LateChargeMinimumAmount != null; }
            set { }
        }

        /// <summary>
        /// The percentage that a borrower is required to pay for failure to make a regular installment within the period specified on the note. This is the amount specified by the late charge code and is used when the late charge is not a flat dollar.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOPercent LateChargeRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the LateChargeRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LateChargeRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool LateChargeRatePercentSpecified
        {
            get { return LateChargeRatePercent != null; }
            set { }
        }

        /// <summary>
        /// The type of late charge that may be applied to the loan.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOEnum<LateChargeBase> LateChargeType;

        /// <summary>
        /// Gets or sets a value indicating whether the LateChargeType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LateChargeType element has been assigned a value.</value>
        [XmlIgnore]
        public bool LateChargeTypeSpecified
        {
            get { return this.LateChargeType != null && this.LateChargeType.enumValue != LateChargeBase.Blank; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 6)]
        public LATE_CHARGE_RULE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
