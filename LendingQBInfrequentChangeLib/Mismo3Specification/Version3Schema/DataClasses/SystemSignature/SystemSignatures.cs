namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class SYSTEM_SIGNATURES
    {
        /// <summary>
        /// Gets a value indicating whether the SYSTEM_SIGNATURES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.SystemSignatureSpecified;
            }
        }

        /// <summary>
        /// A collection of system signatures.
        /// </summary>
        [XmlElement("SYSTEM_SIGNATURE", Order = 0)]
		public List<SYSTEM_SIGNATURE> SystemSignature = new List<SYSTEM_SIGNATURE>();

        /// <summary>
        /// Gets or sets a value indicating whether the SystemSignature element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SystemSignature element has been assigned a value.</value>
        [XmlIgnore]
        public bool SystemSignatureSpecified
        {
            get { return this.SystemSignature != null && this.SystemSignature.Count(s => s != null && s.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public SYSTEM_SIGNATURES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
