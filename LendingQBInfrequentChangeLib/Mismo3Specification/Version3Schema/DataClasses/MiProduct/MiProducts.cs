namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class MI_PRODUCTS
    {
        /// <summary>
        /// Gets a value indicating whether the MI_PRODUCTS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.MIProductSpecified;
            }
        }

        /// <summary>
        /// A collection of mortgage insurance products.
        /// </summary>
        [XmlElement("MI_PRODUCT", Order = 0)]
		public List<MI_PRODUCT> MIProduct = new List<MI_PRODUCT>();

        /// <summary>
        /// Gets or sets a value indicating whether the Mortgage Insurance Product element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Mortgage Insurance Product element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIProductSpecified
        {
            get { return this.MIProduct != null && this.MIProduct.Count(m => m != null && m.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public MI_PRODUCTS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
