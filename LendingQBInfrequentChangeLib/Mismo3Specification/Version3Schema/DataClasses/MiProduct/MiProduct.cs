namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class MI_PRODUCT
    {
        /// <summary>
        /// Gets a value indicating whether the MI_PRODUCT container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.MICoveragePercentSpecified
                    || this.MIDurationTypeOtherDescriptionSpecified
                    || this.MIDurationTypeSpecified
                    || this.MIInitialPremiumAtClosingTypeOtherDescriptionSpecified
                    || this.MIInitialPremiumAtClosingTypeSpecified
                    || this.MILenderSpecialProgramDescriptionSpecified
                    || this.MIPremiumCalculationTypeOtherDescriptionSpecified
                    || this.MIPremiumCalculationTypeSpecified
                    || this.MIPremiumFinancedIndicatorSpecified
                    || this.MIPremiumPaymentTypeOtherDescriptionSpecified
                    || this.MIPremiumPaymentTypeSpecified
                    || this.MIPremiumRatePlanTypeOtherDescriptionSpecified
                    || this.MIPremiumRatePlanTypeSpecified
                    || this.MIPremiumRefundableTypeOtherDescriptionSpecified
                    || this.MIPremiumRefundableTypeSpecified
                    || this.MIPremiumSourceTypeOtherDescriptionSpecified
                    || this.MIPremiumSourceTypeSpecified
                    || this.MIPremiumUpfrontPercentSpecified
                    || this.MISpecialPricingDescriptionSpecified
                    || this.MISubPrimeProgramTypeOtherDescriptionSpecified
                    || this.MISubPrimeProgramTypeSpecified;
            }
        }

        /// <summary>
        /// The percentage of mortgage insurance coverage obtained.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOPercent MICoveragePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the MICoveragePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MICoveragePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool MICoveragePercentSpecified
        {
            get { return MICoveragePercent != null; }
            set { }
        }

        /// <summary>
        /// Specifies the period of time for which mortgage insurance coverage has been obtained/paid.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<MIDurationBase> MIDurationType;

        /// <summary>
        /// Gets or sets a value indicating whether the MIDurationType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIDurationType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIDurationTypeSpecified
        {
            get { return this.MIDurationType != null && this.MIDurationType.enumValue != MIDurationBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture the description of the type of MI Duration if Other is selected as the MIDurationType.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString MIDurationTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MIDurationTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIDurationTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIDurationTypeOtherDescriptionSpecified
        {
            get { return MIDurationTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies whether the initial Mortgage Insurance premium is prepaid upon closing or deferred until first principle payment.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOEnum<MIInitialPremiumAtClosingBase> MIInitialPremiumAtClosingType;

        /// <summary>
        /// Gets or sets a value indicating whether the MIInitialPremiumAtClosingType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIInitialPremiumAtClosingType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIInitialPremiumAtClosingTypeSpecified
        {
            get { return this.MIInitialPremiumAtClosingType != null && this.MIInitialPremiumAtClosingType.enumValue != MIInitialPremiumAtClosingBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture the description of the type of MI Initial Premium At Closing if Other is selected as the MIInitialPremiumAtClosingType.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString MIInitialPremiumAtClosingTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MIInitialPremiumAtClosingTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIInitialPremiumAtClosingTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIInitialPremiumAtClosingTypeOtherDescriptionSpecified
        {
            get { return MIInitialPremiumAtClosingTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// To convey information describing a certain class of lender loans. Not to be confused with Mortgage Insurance Special Pricing Description.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString MILenderSpecialProgramDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MILenderSpecialProgramDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MILenderSpecialProgramDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MILenderSpecialProgramDescriptionSpecified
        {
            get { return MILenderSpecialProgramDescription != null; }
            set { }
        }

        /// <summary>
        /// Method by which new PMI premium is calculated.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOEnum<MIPremiumCalculationBase> MIPremiumCalculationType;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPremiumCalculationType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPremiumCalculationType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumCalculationTypeSpecified
        {
            get { return this.MIPremiumCalculationType != null && this.MIPremiumCalculationType.enumValue != MIPremiumCalculationBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for MI Premium Calculation Type Other Description.  
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOString MIPremiumCalculationTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPremiumCalculationTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPremiumCalculationTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumCalculationTypeOtherDescriptionSpecified
        {
            get { return MIPremiumCalculationTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether mortgage insurance premium has been added to loan amount.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOIndicator MIPremiumFinancedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPremiumFinancedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPremiumFinancedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumFinancedIndicatorSpecified
        {
            get { return MIPremiumFinancedIndicator != null; }
            set { }
        }

        /// <summary>
        /// Defines how the MI premium payments are to be paid.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOEnum<MIPremiumPaymentBase> MIPremiumPaymentType;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPremiumPaymentType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPremiumPaymentType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumPaymentTypeSpecified
        {
            get { return this.MIPremiumPaymentType != null && this.MIPremiumPaymentType.enumValue != MIPremiumPaymentBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture the description of the type of MI Premium Payment if Other is selected as the MIPremiumPaymentType.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOString MIPremiumPaymentTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPremiumPaymentTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPremiumPaymentTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumPaymentTypeOtherDescriptionSpecified
        {
            get { return MIPremiumPaymentTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies pattern of Mortgage Insurance premium rates charged over the life of the coverage.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOEnum<MIPremiumRatePlanBase> MIPremiumRatePlanType;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPremiumRatePlanType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPremiumRatePlanType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumRatePlanTypeSpecified
        {
            get { return this.MIPremiumRatePlanType != null && this.MIPremiumRatePlanType.enumValue != MIPremiumRatePlanBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture the description of the type of MI Premium Rate Plan if Other is selected as the MIPremiumRatePlanType.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOString MIPremiumRatePlanTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPremiumRatePlanTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPremiumRatePlanTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumRatePlanTypeOtherDescriptionSpecified
        {
            get { return MIPremiumRatePlanTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies how the unearned portion of any private mortgage insurance premiums will be treated if the private mortgage insurance coverage is canceled.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOEnum<MIPremiumRefundableBase> MIPremiumRefundableType;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPremiumRefundableType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPremiumRefundableType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumRefundableTypeSpecified
        {
            get { return this.MIPremiumRefundableType != null && this.MIPremiumRefundableType.enumValue != MIPremiumRefundableBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture the description of the type of MI Premium Refundable if Other is selected as the MIPremiumRefundableType.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOString MIPremiumRefundableTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPremiumRefundableTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPremiumRefundableTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumRefundableTypeOtherDescriptionSpecified
        {
            get { return MIPremiumRefundableTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Defines the source of the MI premium payment.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOEnum<MIPremiumSourceBase> MIPremiumSourceType;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPremiumSourceType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPremiumSourceType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumSourceTypeSpecified
        {
            get { return this.MIPremiumSourceType != null && this.MIPremiumSourceType.enumValue != MIPremiumSourceBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field to collect MI premium source when Other is selected for MI Premium Source Type.
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMOString MIPremiumSourceTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPremiumSourceTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPremiumSourceTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumSourceTypeOtherDescriptionSpecified
        {
            get { return MIPremiumSourceTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Reflects the actual up-front MI Premium percent associated with the plan.
        /// </summary>
        [XmlElement(Order = 17)]
        public MISMOPercent MIPremiumUpfrontPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPremiumUpfrontPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPremiumUpfrontPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumUpfrontPercentSpecified
        {
            get { return MIPremiumUpfrontPercent != null; }
            set { }
        }

        /// <summary>
        /// To convey information about a special pricing option or special deal number associated with a certain class of lender loans.
        /// </summary>
        [XmlElement(Order = 18)]
        public MISMOString MISpecialPricingDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MISpecialPricingDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MISpecialPricingDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MISpecialPricingDescriptionSpecified
        {
            get { return MISpecialPricingDescription != null; }
            set { }
        }

        /// <summary>
        /// To convey the loan is processed as sub prime.
        /// </summary>
        [XmlElement(Order = 19)]
        public MISMOEnum<MISubPrimeProgramBase> MISubPrimeProgramType;

        /// <summary>
        /// Gets or sets a value indicating whether the MISubPrimeProgramType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MISubPrimeProgramType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MISubPrimeProgramTypeSpecified
        {
            get { return this.MISubPrimeProgramType != null && this.MISubPrimeProgramType.enumValue != MISubPrimeProgramBase.Blank; }
            set { }
        }

        /// <summary>
        /// The description of the MI Sub Prime Program Type when Other is selected as the option from the enumerated list.
        /// </summary>
        [XmlElement(Order = 20)]
        public MISMOString MISubPrimeProgramTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MISubPrimeProgramTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MISubPrimeProgramTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MISubPrimeProgramTypeOtherDescriptionSpecified
        {
            get { return MISubPrimeProgramTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 21)]
        public MI_PRODUCT_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
