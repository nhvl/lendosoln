namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class MI_PRODUCTS_3_3_1
    {
        /// <summary>
        /// Gets a value indicating whether the MI_PRODUCTS_3_3_1 container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.MIPremiumsSpecified
                    || this.MIPremiumTaxesSpecified;
            }
        }

        /// <summary>
        /// Taxes on a mortgage insurance product.
        /// </summary>
        [XmlElement("MI_PREMIUM_TAXES", Order = 0)]
        public MI_PREMIUM_TAXES MIPremiumTaxes;

        /// <summary>
        /// Gets or sets a value indicating whether the Mortgage Insurance PremiumTaxes element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Mortgage Insurance PremiumTaxes element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumTaxesSpecified
        {
            get { return this.MIPremiumTaxes != null && this.MIPremiumTaxes.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Premiums on a mortgage insurance product.
        /// </summary>
        [XmlElement("MI_PREMIUMS", Order = 1)]
        public MI_PREMIUMS MIPremiums;

        /// <summary>
        /// Gets or sets a value indicating whether the Mortgage Insurance Premiums element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Mortgage Insurance Premiums element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumsSpecified
        {
            get { return this.MIPremiums != null && this.MIPremiums.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public MI_PRODUCTS_3_3_1_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
