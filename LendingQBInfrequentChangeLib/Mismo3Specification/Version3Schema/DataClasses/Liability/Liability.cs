namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class LIABILITY
    {
        /// <summary>
        /// Gets a value indicating whether the LIABILITY container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.LiabilityDetailSpecified
                    || this.LiabilityHolderSpecified
                    || this.PayoffSpecified;
            }
        }

        /// <summary>
        /// Details about the liability.
        /// </summary>
        [XmlElement("LIABILITY_DETAIL", Order = 0)]
        public LIABILITY_DETAIL LiabilityDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the LiabilityDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LiabilityDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool LiabilityDetailSpecified
        {
            get { return this.LiabilityDetail != null && this.LiabilityDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The person holding the liability.
        /// </summary>
        [XmlElement("LIABILITY_HOLDER", Order = 1)]
        public LIABILITY_HOLDER LiabilityHolder;

        /// <summary>
        /// Gets or sets a value indicating whether the LiabilityHolder element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LiabilityHolder element has been assigned a value.</value>
        [XmlIgnore]
        public bool LiabilityHolderSpecified
        {
            get { return this.LiabilityHolder != null && this.LiabilityHolder.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A liability payoff.
        /// </summary>
        [XmlElement("PAYOFF", Order = 2)]
        public PAYOFF Payoff;

        /// <summary>
        /// Gets or sets a value indicating whether the Payoff element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Payoff element has been assigned a value.</value>
        [XmlIgnore]
        public bool PayoffSpecified
        {
            get { return this.Payoff != null && this.Payoff.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public LIABILITY_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// Gets or sets the $$xlink$$ label.
        /// </summary>
        [XmlAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified, Namespace = Mismo3Constants.XlinkNamespace)]
        public string label;

        /// <summary>
        /// Indicates whether the label can be serialized.
        /// </summary>
        /// <returns>A boolean indicating whether the label can be serialized.</returns>
        public bool ShouldSerializelabel()
        {
            return !string.IsNullOrEmpty(label);
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
