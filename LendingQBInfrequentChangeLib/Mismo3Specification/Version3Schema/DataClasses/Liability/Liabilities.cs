namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class LIABILITIES
    {
        /// <summary>
        /// Gets a value indicating whether the LIABILITIES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.LiabilitySpecified;
            }
        }

        /// <summary>
        /// A collection of liabilities.
        /// </summary>
        [XmlElement("LIABILITY", Order = 0)]
		public List<LIABILITY> Liability = new List<LIABILITY>();

        /// <summary>
        /// Gets or sets a value indicating whether the Liability element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Liability element has been assigned a value.</value>
        [XmlIgnore]
        public bool LiabilitySpecified
        {
            get { return this.Liability != null && this.Liability.Count(l => l != null && l.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public LIABILITIES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
