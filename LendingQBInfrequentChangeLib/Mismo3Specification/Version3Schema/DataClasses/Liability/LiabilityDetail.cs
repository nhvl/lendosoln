namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class LIABILITY_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the LIABILITY_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.LiabilityAccountIdentifierSpecified
                    || this.LiabilityDescriptionSpecified
                    || this.LiabilityExclusionIndicatorSpecified
                    || this.LiabilityMonthlyPaymentAmountSpecified
                    || this.LiabilityPayoffStatusIndicatorSpecified
                    || this.LiabilityPayoffWithCurrentAssetsIndicatorSpecified
                    || this.LiabilityRemainingTermMonthsCountSpecified
                    || this.LiabilityTypeOtherDescriptionSpecified
                    || this.LiabilityTypeSpecified
                    || this.LiabilityUnpaidBalanceAmountSpecified
                    || this.SubjectLoanResubordinationIndicatorSpecified;
            }
        }

        /// <summary>
        /// A unique identifier of a borrowers liability. Collected on the URLA in Section VI (Liabilities).
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIdentifier LiabilityAccountIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the LiabilityAccountIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LiabilityAccountIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool LiabilityAccountIdentifierSpecified
        {
            get { return LiabilityAccountIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A text description that further defines the Liability. This could be used to describe the job related expenses.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString LiabilityDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the LiabilityDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LiabilityDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool LiabilityDescriptionSpecified
        {
            get { return LiabilityDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether the liability is to be excluded from inclusion in calculations associated with processing the loan.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOIndicator LiabilityExclusionIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the LiabilityExclusionIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LiabilityExclusionIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool LiabilityExclusionIndicatorSpecified
        {
            get { return LiabilityExclusionIndicator != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of the monthly payment required on borrowers Liability Type.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOAmount LiabilityMonthlyPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the LiabilityMonthlyPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LiabilityMonthlyPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool LiabilityMonthlyPaymentAmountSpecified
        {
            get { return LiabilityMonthlyPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// Identifies whether the liability will be paid at or before closing. Collected on the URLA in Section VI (Liabilities - Indicated By *).
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOIndicator LiabilityPayoffStatusIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the LiabilityPayoffStatusIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LiabilityPayoffStatusIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool LiabilityPayoffStatusIndicatorSpecified
        {
            get { return LiabilityPayoffStatusIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates a liability is to be paid using borrowers current assets, prior to closing, and not out of cash from closing.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOIndicator LiabilityPayoffWithCurrentAssetsIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the LiabilityPayoffWithCurrentAssetsIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LiabilityPayoffWithCurrentAssetsIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool LiabilityPayoffWithCurrentAssetsIndicatorSpecified
        {
            get { return LiabilityPayoffWithCurrentAssetsIndicator != null; }
            set { }
        }

        /// <summary>
        /// The number of months the borrower must make payments in order to satisfy the identified liability. Collected on the URLA in Section VI (Liabilities - Months Left to Pay).
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOCount LiabilityRemainingTermMonthsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the LiabilityRemainingTermMonthsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LiabilityRemainingTermMonthsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool LiabilityRemainingTermMonthsCountSpecified
        {
            get { return LiabilityRemainingTermMonthsCount != null; }
            set { }
        }

        /// <summary>
        /// Specifies the general names (types) of items commonly listed as liabilities of the borrower in a mortgage loan transaction.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOEnum<LiabilityBase> LiabilityType;

        /// <summary>
        /// Gets or sets a value indicating whether the LiabilityType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LiabilityType element has been assigned a value.</value>
        [XmlIgnore]
        public bool LiabilityTypeSpecified
        {
            get { return this.LiabilityType != null && this.LiabilityType.enumValue != LiabilityBase.Blank; }
            set { }
        }

        /// <summary>
        /// Specifies the other liability type when Other is selected.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOString LiabilityTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the LiabilityTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LiabilityTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool LiabilityTypeOtherDescriptionSpecified
        {
            get { return LiabilityTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of the current outstanding balance on borrowers Liability Type.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOAmount LiabilityUnpaidBalanceAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the LiabilityUnpaidBalanceAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LiabilityUnpaidBalanceAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool LiabilityUnpaidBalanceAmountSpecified
        {
            get { return LiabilityUnpaidBalanceAmount != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether the liability will be re-subordinated to the subject loan after closing.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOIndicator SubjectLoanResubordinationIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the indicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the indicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool SubjectLoanResubordinationIndicatorSpecified
        {
            get { return SubjectLoanResubordinationIndicator != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 11)]
        public LIABILITY_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
