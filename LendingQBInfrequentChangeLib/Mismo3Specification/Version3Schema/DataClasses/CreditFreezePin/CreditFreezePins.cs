namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CREDIT_FREEZE_PINS
    {
        /// <summary>
        /// Gets a value indicating whether the CREDIT_FREEZE_PINS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditFreezePinSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of credit freeze PINs.
        /// </summary>
        [XmlElement("CREDIT_FREEZE_PIN", Order = 0)]
		public List<CREDIT_FREEZE_PIN> CreditFreezePin = new List<CREDIT_FREEZE_PIN>();

        /// <summary>
        /// Gets or sets a value indicating whether the CreditFreezePin element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditFreezePin element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditFreezePinSpecified
        {
            get { return this.CreditFreezePin != null && this.CreditFreezePin.Count(c => c != null && c.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public CREDIT_FREEZE_PINS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
