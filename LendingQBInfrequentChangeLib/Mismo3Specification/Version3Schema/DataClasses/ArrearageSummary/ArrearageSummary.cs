namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class ARREARAGE_SUMMARY
    {
        /// <summary>
        /// Gets a value indicating whether the ARREARAGE_SUMMARY container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.MonthlyArrearagePaidAmountSpecified
                    || this.TotalArrearageAmountSpecified
                    || this.TotalInterestArrearageAmountSpecified
                    || this.TotalProjectedInterestArrearageAmountSpecified;
            }
        }

        /// <summary>
        /// The total amount paid in a month for all arrearage components.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount MonthlyArrearagePaidAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the MonthlyArrearagePaidAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MonthlyArrearagePaidAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool MonthlyArrearagePaidAmountSpecified
        {
            get { return MonthlyArrearagePaidAmount != null; }
            set { }
        }

        /// <summary>
        /// The cumulative delinquent payment amount, total amount required to bring loan current. Can include principal and interest, taxes, escrowed HOA dues, late fees.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOAmount TotalArrearageAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the TotalArrearageAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TotalArrearageAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TotalArrearageAmountSpecified
        {
            get { return TotalArrearageAmount != null; }
            set { }
        }

        /// <summary>
        /// The amount of delinquent interest from the loans last paid installment date through the end of the preceding month.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOAmount TotalInterestArrearageAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the TotalInterestArrearageAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TotalInterestArrearageAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TotalInterestArrearageAmountSpecified
        {
            get { return TotalInterestArrearageAmount != null; }
            set { }
        }

        /// <summary>
        /// The projected total dollar amount of delinquent interest for interest capitalization.  It is the amount of delinquent interest from the loans expected last paid installment date prior to the loan workout until one month prior to the Loan Workout Effective Date.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOAmount TotalProjectedInterestArrearageAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the TotalProjectedInterestArrearageAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TotalProjectedInterestArrearageAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TotalProjectedInterestArrearageAmountSpecified
        {
            get { return TotalProjectedInterestArrearageAmount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public ARREARAGE_SUMMARY_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
