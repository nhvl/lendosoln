namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class ORIGINATING_RECORDING_REQUEST
    {
        /// <summary>
        /// Gets a value indicating whether the ORIGINATING_RECORDING_REQUEST container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DigitalSignatureDigestValueSpecified
                    || this.ExtensionSpecified
                    || this.OriginatingRecordingRequestIdentifierSpecified;
            }
        }

        /// <summary>
        /// The hash value from the digital signature used to tamper-seal a document.  It is included here to enable data integrity checking.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOValue DigitalSignatureDigestValue;

        /// <summary>
        /// Gets or sets a value indicating whether the DigitalSignatureDigestValue element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DigitalSignatureDigestValue element has been assigned a value.</value>
        [XmlIgnore]
        public bool DigitalSignatureDigestValueSpecified
        {
            get { return DigitalSignatureDigestValue != null; }
            set { }
        }

        /// <summary>
        /// The Package Identifier from the originating request is to be included here.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOIdentifier OriginatingRecordingRequestIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the OriginatingRecordingRequestIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OriginatingRecordingRequestIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool OriginatingRecordingRequestIdentifierSpecified
        {
            get { return OriginatingRecordingRequestIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public ORIGINATING_RECORDING_REQUEST_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
