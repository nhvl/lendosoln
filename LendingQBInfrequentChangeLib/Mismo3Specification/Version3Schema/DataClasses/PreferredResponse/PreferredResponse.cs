namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class PREFERRED_RESPONSE
    {
        /// <summary>
        /// Gets a value indicating whether the PREFERRED_RESPONSE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.MIMETypeIdentifierSpecified
                    || this.PreferredResponseDestinationDescriptionSpecified
                    || this.PreferredResponseFormatTypeOtherDescriptionSpecified
                    || this.PreferredResponseFormatTypeSpecified
                    || this.PreferredResponseMethodTypeOtherDescriptionSpecified
                    || this.PreferredResponseMethodTypeSpecified
                    || this.PreferredResponseUseEmbeddedFileIndicatorSpecified
                    || this.PreferredResponseVersionIdentifierSpecified;
            }
        }

        /// <summary>
        /// Indicates the Multipurpose Internet Mail Extensions type of the data in the resource. In addition, there is a defined syntax for custom MIME types that should be followed when the type is not registered with IANA.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIdentifier MIMETypeIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the MIMETypeIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIMETypeIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIMETypeIdentifierSpecified
        {
            get { return MIMETypeIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A text description of the destination to which the response should be delivered.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString PreferredResponseDestinationDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PreferredResponseDestinationDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PreferredResponseDestinationDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PreferredResponseDestinationDescriptionSpecified
        {
            get { return PreferredResponseDestinationDescription != null; }
            set { }
        }

        /// <summary>
        /// Designate the response format type for the service or product being requested. The format options are Text, PCL, PDF, XML or Other. If Other is selected, enter the format in Response Format Other.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<PreferredResponseFormatBase> PreferredResponseFormatType;

        /// <summary>
        /// Gets or sets a value indicating whether the PreferredResponseFormatType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PreferredResponseFormatType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PreferredResponseFormatTypeSpecified
        {
            get { return this.PreferredResponseFormatType != null && this.PreferredResponseFormatType.enumValue != PreferredResponseFormatBase.Blank; }
            set { }
        }

        /// <summary>
        /// Describes the Other format selected in the Response Format element.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString PreferredResponseFormatTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PreferredResponseFormatTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PreferredResponseFormatTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PreferredResponseFormatTypeOtherDescriptionSpecified
        {
            get { return PreferredResponseFormatTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the method for delivering the service or product requested - Email, Fax, File, FTP, HTTP, etc. The destination information is listed in the Response Destination element.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<PreferredResponseMethodBase> PreferredResponseMethodType;

        /// <summary>
        /// Gets or sets a value indicating whether the PreferredResponseMethodType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PreferredResponseMethodType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PreferredResponseMethodTypeSpecified
        {
            get { return this.PreferredResponseMethodType != null && this.PreferredResponseMethodType.enumValue != PreferredResponseMethodBase.Blank; }
            set { }
        }

        /// <summary>
        /// Specify a delivery method not listed in the Response Method element.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString PreferredResponseMethodTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PreferredResponseMethodTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PreferredResponseMethodTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PreferredResponseMethodTypeOtherDescriptionSpecified
        {
            get { return PreferredResponseMethodTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Set to Y if the specified Preferred Response is to be contained in the Embedded File element. Set to N if it is to be provided as a separate file.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOIndicator PreferredResponseUseEmbeddedFileIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PreferredResponseUseEmbeddedFileIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PreferredResponseUseEmbeddedFileIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PreferredResponseUseEmbeddedFileIndicatorSpecified
        {
            get { return PreferredResponseUseEmbeddedFileIndicator != null; }
            set { }
        }

        /// <summary>
        /// Designates the Version of the Preferred Response Format that the requester expects to receive in the response (i.e. MISMO 2.1 to indicate that the requester expects to receive a MISMO version 2.1 response file).
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOIdentifier PreferredResponseVersionIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the PreferredResponseVersionIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PreferredResponseVersionIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool PreferredResponseVersionIdentifierSpecified
        {
            get { return PreferredResponseVersionIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 8)]
        public PREFERRED_RESPONSE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
