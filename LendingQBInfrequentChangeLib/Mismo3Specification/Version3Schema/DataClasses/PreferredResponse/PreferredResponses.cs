namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PREFERRED_RESPONSES
    {
        /// <summary>
        /// Gets a value indicating whether the PREFERRED_RESPONSES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.PreferredResponseSpecified;
            }
        }

        /// <summary>
        /// A collection of preferred responses.
        /// </summary>
        [XmlElement("PREFERRED_RESPONSE", Order = 0)]
		public List<PREFERRED_RESPONSE> PreferredResponse = new List<PREFERRED_RESPONSE>();

        /// <summary>
        /// Gets or sets a value indicating whether the PreferredResponse element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PreferredResponse element has been assigned a value.</value>
        [XmlIgnore]
        public bool PreferredResponseSpecified
        {
            get { return this.PreferredResponse != null && this.PreferredResponse.Count(p => p != null && p.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public PREFERRED_RESPONSES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
