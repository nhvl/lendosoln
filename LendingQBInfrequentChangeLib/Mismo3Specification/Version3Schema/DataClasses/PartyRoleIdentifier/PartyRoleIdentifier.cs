namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class PARTY_ROLE_IDENTIFIER
    {
        /// <summary>
        /// Gets a value indicating whether the PARTY_ROLE_IDENTIFIER container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.PartyRoleIdentifierCertificationStatusCodeSpecified
                    || this.PartyRoleIdentifierCertificationStatusDateSpecified
                    || this.PartyRoleIdentifierDescriptionSpecified
                    || this.PartyRoleIdentifierExpiryDateSpecified
                    || this.PartyRoleIdentifierExpiryReasonCodeSpecified
                    || this.PartyRoleIdentifierExpiryReasonDescriptionSpecified
                    || this.PartyRoleIdentifierFirstAssignmentDateSpecified
                    || this.PartyRoleIdentifierLastUpdateDateSpecified
                    || this.PartyRoleIdentifierOfficialRegistryNameSpecified
                    || this.PartyRoleIdentifierReferenceDescriptionSpecified
                    || this.PartyRoleIdentifierScheduledCertificationStatusDateSpecified
                    || this.PartyRoleIdentifierSpecified
                    || this.PartyRoleIdentifierStatusCodeSpecified
                    || this.PartyRoleIdentifierStatusDateSpecified;
            }
        }

        /// <summary>
        /// The unique identifier assigned to the party role.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIdentifier PartyRoleIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the PartyRoleIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PartyRoleIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool PartyRoleIdentifierSpecified
        {
            get { return PartyRoleIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A coded indication of the certification or validation  status of the party role identifier in its associated registry.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOCode PartyRoleIdentifierCertificationStatusCode;

        /// <summary>
        /// Gets or sets a value indicating whether the PartyRoleIdentifierCertificationStatusCode element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PartyRoleIdentifierCertificationStatusCode element has been assigned a value.</value>
        [XmlIgnore]
        public bool PartyRoleIdentifierCertificationStatusCodeSpecified
        {
            get { return PartyRoleIdentifierCertificationStatusCode != null; }
            set { }
        }

        /// <summary>
        /// The date associated with the certification or validation status of the party role identifier in its associated registry.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMODate PartyRoleIdentifierCertificationStatusDate;

        /// <summary>
        /// Gets or sets a value indicating whether the PartyRoleIdentifierCertificationStatusDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PartyRoleIdentifierCertificationStatusDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool PartyRoleIdentifierCertificationStatusDateSpecified
        {
            get { return PartyRoleIdentifierCertificationStatusDate != null; }
            set { }
        }

        /// <summary>
        /// The name describing the organization or registry that maintains the identification number scheme indicated for the specific instance of the container.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString PartyRoleIdentifierDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PartyRoleIdentifierDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PartyRoleIdentifierDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PartyRoleIdentifierDescriptionSpecified
        {
            get { return PartyRoleIdentifierDescription != null; }
            set { }
        }

        /// <summary>
        /// Where applicable, the date of expiry for the identification record associated with the Party Role Identifier (identification number).
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMODate PartyRoleIdentifierExpiryDate;

        /// <summary>
        /// Gets or sets a value indicating whether the PartyRoleIdentifierExpiryDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PartyRoleIdentifierExpiryDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool PartyRoleIdentifierExpiryDateSpecified
        {
            get { return PartyRoleIdentifierExpiryDate != null; }
            set { }
        }

        /// <summary>
        /// The encoded reason for the record expiry or termination of the Party Role Identifier.  Should be used in association with Party Role Identifier Expiry Date, if used. 
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOCode PartyRoleIdentifierExpiryReasonCode;

        /// <summary>
        /// Gets or sets a value indicating whether the PartyRoleIdentifierExpiryReasonCode element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PartyRoleIdentifierExpiryReasonCode element has been assigned a value.</value>
        [XmlIgnore]
        public bool PartyRoleIdentifierExpiryReasonCodeSpecified
        {
            get { return PartyRoleIdentifierExpiryReasonCode != null; }
            set { }
        }

        /// <summary>
        /// The textual reason for the record expiry or termination of the Party Role Identifier.  Should be used in association with Party Role Identifier Expiry Date, if applicable.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOString PartyRoleIdentifierExpiryReasonDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PartyRoleIdentifierExpiryReasonDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PartyRoleIdentifierExpiryReasonDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PartyRoleIdentifierExpiryReasonDescriptionSpecified
        {
            get { return PartyRoleIdentifierExpiryReasonDescription != null; }
            set { }
        }

        /// <summary>
        /// The date when the first assignment of the Party Role Identifier (identification number) was obtained, assigned or issued. 
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMODate PartyRoleIdentifierFirstAssignmentDate;

        /// <summary>
        /// Gets or sets a value indicating whether the PartyRoleIdentifierFirstAssignmentDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PartyRoleIdentifierFirstAssignmentDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool PartyRoleIdentifierFirstAssignmentDateSpecified
        {
            get { return PartyRoleIdentifierFirstAssignmentDate != null; }
            set { }
        }

        /// <summary>
        /// The date of the last update of the information record associated with Party Role Identifier (identification number).
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMODate PartyRoleIdentifierLastUpdateDate;

        /// <summary>
        /// Gets or sets a value indicating whether the PartyRoleIdentifierLastUpdateDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PartyRoleIdentifierLastUpdateDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool PartyRoleIdentifierLastUpdateDateSpecified
        {
            get { return PartyRoleIdentifierLastUpdateDate != null; }
            set { }
        }

        /// <summary>
        /// The legal name of the identified party associated with the Party Role Identifier, as registered in or stored in the respective registry indicated by the Party Role Identifier Description and/or URI.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOString PartyRoleIdentifierOfficialRegistryName;

        /// <summary>
        /// Gets or sets a value indicating whether the PartyRoleIdentifierOfficialRegistryName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PartyRoleIdentifierOfficialRegistryName element has been assigned a value.</value>
        [XmlIgnore]
        public bool PartyRoleIdentifierOfficialRegistryNameSpecified
        {
            get { return PartyRoleIdentifierOfficialRegistryName != null; }
            set { }
        }

        /// <summary>
        /// Describes a reference associated with the party role identifier.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOString PartyRoleIdentifierReferenceDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PartyRoleIdentifierReferenceDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PartyRoleIdentifierReferenceDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PartyRoleIdentifierReferenceDescriptionSpecified
        {
            get { return PartyRoleIdentifierReferenceDescription != null; }
            set { }
        }

        /// <summary>
        /// The date associated with the next scheduled certification or validation to be conducted  of the party role identifier in its associated registry.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMODate PartyRoleIdentifierScheduledCertificationStatusDate;

        /// <summary>
        /// Gets or sets a value indicating whether the PartyRoleIdentifierScheduledCertificationStatusDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PartyRoleIdentifierScheduledCertificationStatusDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool PartyRoleIdentifierScheduledCertificationStatusDateSpecified
        {
            get { return PartyRoleIdentifierScheduledCertificationStatusDate != null; }
            set { }
        }

        /// <summary>
        /// Indicates the status of the party role identifier in the registry associated with the identifier.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOCode PartyRoleIdentifierStatusCode;

        /// <summary>
        /// Gets or sets a value indicating whether the PartyRoleIdentifierStatusCode element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PartyRoleIdentifierStatusCode element has been assigned a value.</value>
        [XmlIgnore]
        public bool PartyRoleIdentifierStatusCodeSpecified
        {
            get { return PartyRoleIdentifierStatusCode != null; }
            set { }
        }

        /// <summary>
        /// Indicates the date associated with status of the identifier in its associated registry.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMODate PartyRoleIdentifierStatusDate;

        /// <summary>
        /// Gets or sets a value indicating whether the PartyRoleIdentifierStatusDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PartyRoleIdentifierStatusDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool PartyRoleIdentifierStatusDateSpecified
        {
            get { return PartyRoleIdentifierStatusDate != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 14)]
        public PARTY_ROLE_IDENTIFIER_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
