namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PARTY_ROLE_IDENTIFIERS
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PARTY_ROLE_IDENTIFIERS" /> class.
        /// </summary>
        public PARTY_ROLE_IDENTIFIERS()
        { 
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PARTY_ROLE_IDENTIFIERS" /> class. Adds a single role identifier to the list of identifiers.
        /// </summary>
        /// <param name="partyRoleIdentifier">The identifier to add to the list of identifiers.</param>
        public PARTY_ROLE_IDENTIFIERS(PARTY_ROLE_IDENTIFIER partyRoleIdentifier)
        {
            this.PartyRoleIdentifier.Add(partyRoleIdentifier);
        }


        /// <summary>
        /// Gets a value indicating whether the PARTY_ROLE_IDENTIFIERS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.PartyRoleIdentifierSpecified;
            }
        }

        /// <summary>
        /// A collection of party role identifiers.
        /// </summary>
        [XmlElement("PARTY_ROLE_IDENTIFIER", Order = 0)]
		public List<PARTY_ROLE_IDENTIFIER> PartyRoleIdentifier = new List<PARTY_ROLE_IDENTIFIER>();

        /// <summary>
        /// Gets or sets a value indicating whether the PartyRoleIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PartyRoleIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool PartyRoleIdentifierSpecified
        {
            get { return this.PartyRoleIdentifier != null && this.PartyRoleIdentifier.Count(p => p != null && p.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public PARTY_ROLE_IDENTIFIERS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
