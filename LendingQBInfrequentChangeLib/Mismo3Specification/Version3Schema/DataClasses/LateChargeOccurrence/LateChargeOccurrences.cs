namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class LATE_CHARGE_OCCURRENCES
    {
        /// <summary>
        /// Gets a value indicating whether the LATE_CHARGE_OCCURRENCES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.LateChargeOccurrenceSpecified;
            }
        }

        /// <summary>
        /// A collection of late charge occurrences.
        /// </summary>
        [XmlElement("LATE_CHARGE_OCCURRENCE", Order = 0)]
		public List<LATE_CHARGE_OCCURRENCE> LateChargeOccurrence = new List<LATE_CHARGE_OCCURRENCE>();

        /// <summary>
        /// Gets or sets a value indicating whether the LateChargeOccurrence element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LateChargeOccurrence element has been assigned a value.</value>
        [XmlIgnore]
        public bool LateChargeOccurrenceSpecified
        {
            get { return this.LateChargeOccurrence != null && this.LateChargeOccurrence.Count(l => l != null && l.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public LATE_CHARGE_OCCURRENCES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
