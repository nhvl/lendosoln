namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class SECURITY_INSTRUMENT_RIDERS
    {
        /// <summary>
        /// Gets a value indicating whether the SECURITY_INSTRUMENT_RIDERS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.SecurityInstrumentRiderSpecified;
            }
        }

        /// <summary>
        /// A collection of security instrument riders.
        /// </summary>
        [XmlElement("SECURITY_INSTRUMENT_RIDER", Order = 0)]
		public List<SECURITY_INSTRUMENT_RIDER> SecurityInstrumentRider = new List<SECURITY_INSTRUMENT_RIDER>();

        /// <summary>
        /// Gets or sets a value indicating whether the SecurityInstrumentRider element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SecurityInstrumentRider element has been assigned a value.</value>
        [XmlIgnore]
        public bool SecurityInstrumentRiderSpecified
        {
            get { return this.SecurityInstrumentRider != null && this.SecurityInstrumentRider.Count(s => s != null && s.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public SECURITY_INSTRUMENT_RIDERS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
