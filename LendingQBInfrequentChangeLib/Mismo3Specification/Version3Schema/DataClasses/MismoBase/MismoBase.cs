﻿namespace Mismo3Specification.Version3Schema
{
    using System.Xml;
    using System.Xml.Serialization;

    public partial class MISMO_BASE
    {
        /// <summary>
        /// Gets a value indicating whether the element should be serialized.
        /// </summary>
        /// <remarks>Always returns false as this placeholder element should never be exported.</remarks>
        [XmlIgnore]
        public virtual bool ShouldSerialize
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets or sets the child elements of this extension.
        /// </summary>
        [XmlAnyElement]
        public XmlElement[] Elements { get; set; }
    }
}
