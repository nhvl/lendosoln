namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CREDIT_LIABILITY_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the CREDIT_LIABILITY_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditBusinessTypeSpecified
                    || this.CreditCounselingIndicatorSpecified
                    || this.CreditLiabilityAccountBalanceDateSpecified
                    || this.CreditLiabilityAccountClosedDateSpecified
                    || this.CreditLiabilityAccountIdentifierSpecified
                    || this.CreditLiabilityAccountOpenedDateSpecified
                    || this.CreditLiabilityAccountOwnershipTypeSpecified
                    || this.CreditLiabilityAccountPaidDateSpecified
                    || this.CreditLiabilityAccountReportedDateSpecified
                    || this.CreditLiabilityAccountStatusDateSpecified
                    || this.CreditLiabilityAccountStatusTypeSpecified
                    || this.CreditLiabilityAccountTypeSpecified
                    || this.CreditLiabilityBalloonPaymentAmountSpecified
                    || this.CreditLiabilityBalloonPaymentDueDateSpecified
                    || this.CreditLiabilityChargeOffAmountSpecified
                    || this.CreditLiabilityChargeOffDateSpecified
                    || this.CreditLiabilityCollateralDescriptionSpecified
                    || this.CreditLiabilityConsumerDisputeIndicatorSpecified
                    || this.CreditLiabilityCreditLimitAmountSpecified
                    || this.CreditLiabilityDerogatoryDataIndicatorSpecified
                    || this.CreditLiabilityHighBalanceAmountSpecified
                    || this.CreditLiabilityLastActivityDateSpecified
                    || this.CreditLiabilityManualUpdateIndicatorSpecified
                    || this.CreditLiabilityMonthlyPaymentAmountSpecified
                    || this.CreditLiabilityMonthsRemainingCountSpecified
                    || this.CreditLiabilityMonthsReviewedCountSpecified
                    || this.CreditLiabilityOriginalCreditorNameSpecified
                    || this.CreditLiabilityPastDueAmountSpecified
                    || this.CreditLiabilityTermsDescriptionSpecified
                    || this.CreditLiabilityTermsMonthsCountSpecified
                    || this.CreditLiabilityTermsSourceTypeSpecified
                    || this.CreditLiabilityUnpaidBalanceAmountSpecified
                    || this.CreditLoanTypeOtherDescriptionSpecified
                    || this.CreditLoanTypeSpecified
                    || this.DeferredPaymentAmountSpecified
                    || this.DeferredPaymentDateSpecified
                    || this.DetailCreditBusinessTypeSpecified
                    || this.DuplicateGroupIdentifierSpecified
                    || this.ExtensionSpecified
                    || this.PrimaryRecordIndicatorSpecified;
            }
        }

        /// <summary>
        /// The credit repository bureaus can identify the type of business of the liability holder or the entity requesting a credit report as listed in a credit inquiry record. This information is needed for an automated scoring application. This data is available for liability and inquiry records from Equifax, Experian and Trans Union.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<CreditBusinessBase> CreditBusinessType;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditBusinessType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditBusinessType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditBusinessTypeSpecified
        {
            get { return this.CreditBusinessType != null && this.CreditBusinessType.enumValue != CreditBusinessBase.Blank; }
            set { }
        }

        /// <summary>
        /// This indicator is set to Y when the credit liability is being managed by a credit counseling agency.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOIndicator CreditCounselingIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditCounselingIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditCounselingIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditCounselingIndicatorSpecified
        {
            get { return CreditCounselingIndicator != null; }
            set { }
        }

        /// <summary>
        /// The date that the reported Credit Liability Unpaid Balance Amount was reached.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMODate CreditLiabilityAccountBalanceDate;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLiabilityAccountBalanceDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLiabilityAccountBalanceDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLiabilityAccountBalanceDateSpecified
        {
            get { return CreditLiabilityAccountBalanceDate != null; }
            set { }
        }

        /// <summary>
        /// The date that the credit liability account was closed by the liability holder (by either the borrower or the liability holder).
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMODate CreditLiabilityAccountClosedDate;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLiabilityAccountClosedDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLiabilityAccountClosedDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLiabilityAccountClosedDateSpecified
        {
            get { return CreditLiabilityAccountClosedDate != null; }
            set { }
        }

        /// <summary>
        /// The account number assigned to the liability account as provided by the credit repository bureau.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOIdentifier CreditLiabilityAccountIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLiabilityAccountIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLiabilityAccountIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLiabilityAccountIdentifierSpecified
        {
            get { return CreditLiabilityAccountIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The date that the credit liability account was opened by the liability holder.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMODate CreditLiabilityAccountOpenedDate;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLiabilityAccountOpenedDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLiabilityAccountOpenedDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLiabilityAccountOpenedDateSpecified
        {
            get { return CreditLiabilityAccountOpenedDate != null; }
            set { }
        }

        /// <summary>
        /// Describes the ownership relation of the borrower(s) to the liability account (i.e. are they solely responsible, jointly responsible, a co-signer on the account, etc.).
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOEnum<CreditLiabilityAccountOwnershipBase> CreditLiabilityAccountOwnershipType;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLiabilityAccountOwnershipType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLiabilityAccountOwnershipType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLiabilityAccountOwnershipTypeSpecified
        {
            get { return this.CreditLiabilityAccountOwnershipType != null && this.CreditLiabilityAccountOwnershipType.enumValue != CreditLiabilityAccountOwnershipBase.Blank; }
            set { }
        }

        /// <summary>
        /// The date that the credit liability was reported paid to zero by the liability holder.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMODate CreditLiabilityAccountPaidDate;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLiabilityAccountPaidDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLiabilityAccountPaidDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLiabilityAccountPaidDateSpecified
        {
            get { return CreditLiabilityAccountPaidDate != null; }
            set { }
        }

        /// <summary>
        /// The date that the credit liability data was reported by the liability holder.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMODate CreditLiabilityAccountReportedDate;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLiabilityAccountReportedDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLiabilityAccountReportedDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLiabilityAccountReportedDateSpecified
        {
            get { return CreditLiabilityAccountReportedDate != null; }
            set { }
        }

        /// <summary>
        /// The Status Date is the date that the accounts Current Rating was changed to that status by the liability holder. If the Current Rating status is Bankruptcy, for example, the Status Date would be when the account first went into bankruptcy.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMODate CreditLiabilityAccountStatusDate;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLiabilityAccountStatusDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLiabilityAccountStatusDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLiabilityAccountStatusDateSpecified
        {
            get { return CreditLiabilityAccountStatusDate != null; }
            set { }
        }

        /// <summary>
        /// This data element is an enumerated status list for an account as of the Account Status Date.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOEnum<CreditLiabilityAccountStatusBase> CreditLiabilityAccountStatusType;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLiabilityAccountStatusType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLiabilityAccountStatusType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLiabilityAccountStatusTypeSpecified
        {
            get { return this.CreditLiabilityAccountStatusType != null && this.CreditLiabilityAccountStatusType.enumValue != CreditLiabilityAccountStatusBase.Blank; }
            set { }
        }

        /// <summary>
        /// This describes the type of account established with the Liability Account Holder - credit line, installment, mortgage, open, revolving).
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOEnum<CreditLiabilityAccountBase> CreditLiabilityAccountType;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLiabilityAccountType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLiabilityAccountType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLiabilityAccountTypeSpecified
        {
            get { return this.CreditLiabilityAccountType != null && this.CreditLiabilityAccountType.enumValue != CreditLiabilityAccountBase.Blank; }
            set { }
        }

        /// <summary>
        /// The amount that will be due on the account in the form a lump sum balloon payment.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOAmount CreditLiabilityBalloonPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLiabilityBalloonPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLiabilityBalloonPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLiabilityBalloonPaymentAmountSpecified
        {
            get { return CreditLiabilityBalloonPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// According to the contract / invoice, the date that full payment is due on an account.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMODate CreditLiabilityBalloonPaymentDueDate;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLiabilityBalloonPaymentDueDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLiabilityBalloonPaymentDueDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLiabilityBalloonPaymentDueDateSpecified
        {
            get { return CreditLiabilityBalloonPaymentDueDate != null; }
            set { }
        }

        /// <summary>
        /// The amount charged off by a liability holder, usually for an account in collection.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOAmount CreditLiabilityChargeOffAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLiabilityChargeOffAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLiabilityChargeOffAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLiabilityChargeOffAmountSpecified
        {
            get { return CreditLiabilityChargeOffAmount != null; }
            set { }
        }

        /// <summary>
        /// The date that the creditor put the account in a "charge off " status and declared the account balance as uncollectable.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMODate CreditLiabilityChargeOffDate;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLiabilityChargeOffDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLiabilityChargeOffDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLiabilityChargeOffDateSpecified
        {
            get { return CreditLiabilityChargeOffDate != null; }
            set { }
        }

        /// <summary>
        /// A description of any collateral for the loan (for example, 97 Corvette).
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMOString CreditLiabilityCollateralDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLiabilityCollateralDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLiabilityCollateralDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLiabilityCollateralDescriptionSpecified
        {
            get { return CreditLiabilityCollateralDescription != null; }
            set { }
        }

        /// <summary>
        /// This element indicates whether or not the consumer (borrower) disputes information reported for this account and has filed a FCRA dispute document.
        /// </summary>
        [XmlElement(Order = 17)]
        public MISMOIndicator CreditLiabilityConsumerDisputeIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLiabilityConsumerDisputeIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLiabilityConsumerDisputeIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLiabilityConsumerDisputeIndicatorSpecified
        {
            get { return CreditLiabilityConsumerDisputeIndicator != null; }
            set { }
        }

        /// <summary>
        /// The current credit limit for the account as reported by the liability holder.
        /// </summary>
        [XmlElement(Order = 18)]
        public MISMOAmount CreditLiabilityCreditLimitAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLiabilityCreditLimitAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLiabilityCreditLimitAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLiabilityCreditLimitAmountSpecified
        {
            get { return CreditLiabilityCreditLimitAmount != null; }
            set { }
        }

        /// <summary>
        /// This element indicates whether or not the liability account contains adverse or derogatory information.
        /// </summary>
        [XmlElement(Order = 19)]
        public MISMOIndicator CreditLiabilityDerogatoryDataIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLiabilityDerogatoryDataIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLiabilityDerogatoryDataIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLiabilityDerogatoryDataIndicatorSpecified
        {
            get { return CreditLiabilityDerogatoryDataIndicator != null; }
            set { }
        }

        /// <summary>
        /// The highest balance recorded for the account by the liability holder.
        /// </summary>
        [XmlElement(Order = 20)]
        public MISMOAmount CreditLiabilityHighBalanceAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLiabilityHighBalanceAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLiabilityHighBalanceAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLiabilityHighBalanceAmountSpecified
        {
            get { return CreditLiabilityHighBalanceAmount != null; }
            set { }
        }

        /// <summary>
        /// The date of last account activity (such as last charge or last payment)reported by the liability holder.
        /// </summary>
        [XmlElement(Order = 21)]
        public MISMODate CreditLiabilityLastActivityDate;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLiabilityLastActivityDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLiabilityLastActivityDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLiabilityLastActivityDateSpecified
        {
            get { return CreditLiabilityLastActivityDate != null; }
            set { }
        }

        /// <summary>
        /// This element indicates whether or not the data in the liability account has been manually updated by the credit bureau based on an investigation.
        /// </summary>
        [XmlElement(Order = 22)]
        public MISMOIndicator CreditLiabilityManualUpdateIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLiabilityManualUpdateIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLiabilityManualUpdateIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLiabilityManualUpdateIndicatorSpecified
        {
            get { return CreditLiabilityManualUpdateIndicator != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of the monthly payment required on borrowers liability account as reported on a credit report. The amount is either calculated by the credit bureau or provided by the credit liability holder.
        /// </summary>
        [XmlElement(Order = 23)]
        public MISMOAmount CreditLiabilityMonthlyPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLiabilityMonthlyPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLiabilityMonthlyPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLiabilityMonthlyPaymentAmountSpecified
        {
            get { return CreditLiabilityMonthlyPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// The number of months from the Credit Liability Account Reported Date that are remaining to be paid on an installment loan account.
        /// </summary>
        [XmlElement(Order = 24)]
        public MISMOCount CreditLiabilityMonthsRemainingCount;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLiabilityMonthsRemainingCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLiabilityMonthsRemainingCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLiabilityMonthsRemainingCountSpecified
        {
            get { return CreditLiabilityMonthsRemainingCount != null; }
            set { }
        }

        /// <summary>
        /// The number of months of account history reported by the liability holder. This is the period over which Late Counts are accumulated.
        /// </summary>
        [XmlElement(Order = 25)]
        public MISMOCount CreditLiabilityMonthsReviewedCount;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLiabilityMonthsReviewedCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLiabilityMonthsReviewedCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLiabilityMonthsReviewedCountSpecified
        {
            get { return CreditLiabilityMonthsReviewedCount != null; }
            set { }
        }

        /// <summary>
        /// When a liability account has been transferred or is being handled by a collection agency, this element indicates the name of original creditor.
        /// </summary>
        [XmlElement(Order = 26)]
        public MISMOString CreditLiabilityOriginalCreditorName;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLiabilityOriginalCreditorName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLiabilityOriginalCreditorName element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLiabilityOriginalCreditorNameSpecified
        {
            get { return CreditLiabilityOriginalCreditorName != null; }
            set { }
        }

        /// <summary>
        /// The amount that the borrower is currently past due as reported by the liability holder.
        /// </summary>
        [XmlElement(Order = 27)]
        public MISMOAmount CreditLiabilityPastDueAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLiabilityPastDueAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLiabilityPastDueAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLiabilityPastDueAmountSpecified
        {
            get { return CreditLiabilityPastDueAmount != null; }
            set { }
        }

        /// <summary>
        /// A text description for the loan terms (for example, 360 months at $820 per month).
        /// </summary>
        [XmlElement(Order = 28)]
        public MISMOString CreditLiabilityTermsDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLiabilityTermsDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLiabilityTermsDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLiabilityTermsDescriptionSpecified
        {
            get { return CreditLiabilityTermsDescription != null; }
            set { }
        }

        /// <summary>
        /// The total number of months established as terms for repayment of a loan.
        /// </summary>
        [XmlElement(Order = 29)]
        public MISMOCount CreditLiabilityTermsMonthsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLiabilityTermsMonthsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLiabilityTermsMonthsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLiabilityTermsMonthsCountSpecified
        {
            get { return CreditLiabilityTermsMonthsCount != null; }
            set { }
        }

        /// <summary>
        /// This enumerated element indicates whether the monthly payment terms amount was provided by the repository bureau, or calculated by the credit bureau.
        /// </summary>
        [XmlElement(Order = 30)]
        public MISMOEnum<CreditLiabilityTermsSourceBase> CreditLiabilityTermsSourceType;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLiabilityTermsSourceType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLiabilityTermsSourceType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLiabilityTermsSourceTypeSpecified
        {
            get { return this.CreditLiabilityTermsSourceType != null && this.CreditLiabilityTermsSourceType.enumValue != CreditLiabilityTermsSourceBase.Blank; }
            set { }
        }

        /// <summary>
        /// The balance owed on a liability account as of the Date Reported by a repository bureau or credit bureau.
        /// </summary>
        [XmlElement(Order = 31)]
        public MISMOAmount CreditLiabilityUnpaidBalanceAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLiabilityUnpaidBalanceAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLiabilityUnpaidBalanceAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLiabilityUnpaidBalanceAmountSpecified
        {
            get { return CreditLiabilityUnpaidBalanceAmount != null; }
            set { }
        }

        /// <summary>
        /// This element contains an enumerated list of common loan types. If the loan type is Other, then the description is entered into the Credit Loan Type Other Description element.
        /// </summary>
        [XmlElement(Order = 32)]
        public MISMOEnum<CreditLoanBase> CreditLoanType;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLoanType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLoanType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLoanTypeSpecified
        {
            get { return this.CreditLoanType != null && this.CreditLoanType.enumValue != CreditLoanBase.Blank; }
            set { }
        }

        /// <summary>
        /// If Credit Loan Type was set to Other, then this element will contain a description of the loan type.
        /// </summary>
        [XmlElement(Order = 33)]
        public MISMOString CreditLoanTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLoanTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLoanTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLoanTypeOtherDescriptionSpecified
        {
            get { return CreditLoanTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// For loans allowing deferred payments, this is the amount of the loan that is to be paid, in part or in full, at a later date.
        /// </summary>
        [XmlElement(Order = 34)]
        public MISMOAmount DeferredPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the DeferredPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DeferredPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool DeferredPaymentAmountSpecified
        {
            get { return DeferredPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// For loans allowing deferred payments, this is the date that payments are due to resume.
        /// </summary>
        [XmlElement(Order = 35)]
        public MISMODate DeferredPaymentDate;

        /// <summary>
        /// Gets or sets a value indicating whether the DeferredPaymentDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DeferredPaymentDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool DeferredPaymentDateSpecified
        {
            get { return DeferredPaymentDate != null; }
            set { }
        }

        /// <summary>
        /// Provides a more detailed description of the Credit Business Type.
        /// </summary>
        [XmlElement(Order = 36)]
        public MISMOEnum<DetailCreditBusinessBase> DetailCreditBusinessType;

        /// <summary>
        /// Gets or sets a value indicating whether the DetailCreditBusinessType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DetailCreditBusinessType element has been assigned a value.</value>
        [XmlIgnore]
        public bool DetailCreditBusinessTypeSpecified
        {
            get { return this.DetailCreditBusinessType != null && this.DetailCreditBusinessType.enumValue != DetailCreditBusinessBase.Blank; }
            set { }
        }

        /// <summary>
        /// Data records with the same Duplicate Group Identifier value have been identified as being duplicate records from different sources.  Within a set of records that have the same Duplicate Group Identifier, one record is marked with a Primary Record Indicator set to "True", indicating that this record has been identified as the record out of the group that should be used for evaluation of credit.  The remaining records in the group should have a Primary Record Indicator set to "False".
        /// </summary>
        [XmlElement(Order = 37)]
        public MISMOIdentifier DuplicateGroupIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the DuplicateGroupIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DuplicateGroupIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool DuplicateGroupIdentifierSpecified
        {
            get { return DuplicateGroupIdentifier != null; }
            set { }
        }

        /// <summary>
        /// If this indicator is missing or if this indicator is set to "True", the data element is the primary record to be used for output and evaluation.  Most mortgage credit report data is collected from multiple sources (i.e. Equifax, Experian and Trans Union), and combined into a single data set that does not contain duplicate data.  In most credit reports, only the primary, merged data is returned.  For these reports, the Primary Record Indicator can be left out completely or if it is provided is should be set to "True".   Some credit reports are provided that contain both the primary, merged data, plus the original records that were considered duplicates of each other that were used to derive the primary record.  These duplicates that contained the original, unmerged data will have a Primary Record Indicator value of "False".
        /// </summary>
        [XmlElement(Order = 38)]
        public MISMOIndicator PrimaryRecordIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PrimaryRecordIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrimaryRecordIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrimaryRecordIndicatorSpecified
        {
            get { return PrimaryRecordIndicator != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 39)]
        public CREDIT_LIABILITY_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
