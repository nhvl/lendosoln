namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class CREDIT_LIABILITY
    {
        /// <summary>
        /// Gets a value indicating whether the CREDIT_LIABILITY container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditCommentsSpecified
                    || this.CreditLiabilityCreditorSpecified
                    || this.CreditLiabilityCurrentRatingSpecified
                    || this.CreditLiabilityDetailSpecified
                    || this.CreditLiabilityHighestAdverseRatingSpecified
                    || this.CreditLiabilityLateCountSpecified
                    || this.CreditLiabilityMostRecentAdverseRatingSpecified
                    || this.CreditLiabilityPaymentPatternSpecified
                    || this.CreditLiabilityPriorAdverseRatingsSpecified
                    || this.CreditRepositoriesSpecified
                    || this.ExtensionSpecified
                    || this.VerificationSpecified;
            }
        }

        /// <summary>
        /// Comments on the liability account.
        /// </summary>
        [XmlElement("CREDIT_COMMENTS", Order = 0)]
        public CREDIT_COMMENTS CreditComments;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditComments element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditComments element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditCommentsSpecified
        {
            get { return this.CreditComments != null && this.CreditComments.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Creditor for the liability account.
        /// </summary>
        [XmlElement("CREDIT_LIABILITY_CREDITOR", Order = 1)]
        public CREDIT_LIABILITY_CREDITOR CreditLiabilityCreditor;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLiabilityCreditor element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLiabilityCreditor element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLiabilityCreditorSpecified
        {
            get { return this.CreditLiabilityCreditor != null && this.CreditLiabilityCreditor.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Current rating on the liability account.
        /// </summary>
        [XmlElement("CREDIT_LIABILITY_CURRENT_RATING", Order = 2)]
        public CREDIT_LIABILITY_CURRENT_RATING CreditLiabilityCurrentRating;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLiabilityCurrentRating element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLiabilityCurrentRating element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLiabilityCurrentRatingSpecified
        {
            get { return this.CreditLiabilityCurrentRating != null && this.CreditLiabilityCurrentRating.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details on the liability account.
        /// </summary>
        [XmlElement("CREDIT_LIABILITY_DETAIL", Order = 3)]
        public CREDIT_LIABILITY_DETAIL CreditLiabilityDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLiabilityDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLiabilityDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLiabilityDetailSpecified
        {
            get { return this.CreditLiabilityDetail != null && this.CreditLiabilityDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Highest adverse rating on the liability account.
        /// </summary>
        [XmlElement("CREDIT_LIABILITY_HIGHEST_ADVERSE_RATING", Order = 4)]
        public CREDIT_LIABILITY_HIGHEST_ADVERSE_RATING CreditLiabilityHighestAdverseRating;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLiabilityHighestAdverseRating element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLiabilityHighestAdverseRating element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLiabilityHighestAdverseRatingSpecified
        {
            get { return this.CreditLiabilityHighestAdverseRating != null && this.CreditLiabilityHighestAdverseRating.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Late payment count on the liability account.
        /// </summary>
        [XmlElement("CREDIT_LIABILITY_LATE_COUNT", Order = 5)]
        public CREDIT_LIABILITY_LATE_COUNT CreditLiabilityLateCount;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLiabilityLateCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLiabilityLateCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLiabilityLateCountSpecified
        {
            get { return this.CreditLiabilityLateCount != null && this.CreditLiabilityLateCount.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Most recent adverse rating of the liability account.
        /// </summary>
        [XmlElement("CREDIT_LIABILITY_MOST_RECENT_ADVERSE_RATING", Order = 6)]
        public CREDIT_LIABILITY_MOST_RECENT_ADVERSE_RATING CreditLiabilityMostRecentAdverseRating;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLiabilityMostRecentAdverseRating element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLiabilityMostRecentAdverseRating element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLiabilityMostRecentAdverseRatingSpecified
        {
            get { return this.CreditLiabilityMostRecentAdverseRating != null && this.CreditLiabilityMostRecentAdverseRating.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Payment pattern for the liability account.
        /// </summary>
        [XmlElement("CREDIT_LIABILITY_PAYMENT_PATTERN", Order = 7)]
        public CREDIT_LIABILITY_PAYMENT_PATTERN CreditLiabilityPaymentPattern;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLiabilityPaymentPattern element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLiabilityPaymentPattern element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLiabilityPaymentPatternSpecified
        {
            get { return this.CreditLiabilityPaymentPattern != null && this.CreditLiabilityPaymentPattern.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Prior adverse ratings of the liability account.
        /// </summary>
        [XmlElement("CREDIT_LIABILITY_PRIOR_ADVERSE_RATINGS", Order = 8)]
        public CREDIT_LIABILITY_PRIOR_ADVERSE_RATINGS CreditLiabilityPriorAdverseRatings;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLiabilityPriorAdverseRatings element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLiabilityPriorAdverseRatings element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLiabilityPriorAdverseRatingsSpecified
        {
            get { return this.CreditLiabilityPriorAdverseRatings != null && this.CreditLiabilityPriorAdverseRatings.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Repositories for the liability.
        /// </summary>
        [XmlElement("CREDIT_REPOSITORIES", Order = 9)]
        public CREDIT_REPOSITORIES CreditRepositories;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditRepositories element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditRepositories element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditRepositoriesSpecified
        {
            get { return this.CreditRepositories != null && this.CreditRepositories.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Verification of the liability account.
        /// </summary>
        [XmlElement("VERIFICATION", Order = 10)]
        public VERIFICATION Verification;

        /// <summary>
        /// Gets or sets a value indicating whether the Verification element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Verification element has been assigned a value.</value>
        [XmlIgnore]
        public bool VerificationSpecified
        {
            get { return this.Verification != null && this.Verification.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 11)]
        public CREDIT_LIABILITY_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
