namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CREDIT_LIABILITIES
    {
        /// <summary>
        /// Gets a value indicating whether the CREDIT_LIABILITIES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditLiabilitySpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of liability accounts.
        /// </summary>
        [XmlElement("CREDIT_LIABILITY", Order = 0)]
		public List<CREDIT_LIABILITY> CreditLiability = new List<CREDIT_LIABILITY>();

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLiability element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLiability element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLiabilitySpecified
        {
            get { return this.CreditLiability != null && this.CreditLiability.Count(c => c != null && c.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public CREDIT_LIABILITIES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
