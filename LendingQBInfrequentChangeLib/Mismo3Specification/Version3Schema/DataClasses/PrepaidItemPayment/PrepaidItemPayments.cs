namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PREPAID_ITEM_PAYMENTS
    {
        /// <summary>
        /// Gets a value indicating whether the PREPAID_ITEM_PAYMENTS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.PrepaidItemPaymentSpecified;
            }
        }

        /// <summary>
        /// A collection of prepaid item payments.
        /// </summary>
        [XmlElement("PREPAID_ITEM_PAYMENT", Order = 0)]
        public List<PREPAID_ITEM_PAYMENT> PrepaidItemPayment = new List<PREPAID_ITEM_PAYMENT>();

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaidItemPayment element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaidItemPayment element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaidItemPaymentSpecified
        {
            get { return this.PrepaidItemPayment != null && this.PrepaidItemPayment.Count(p => p != null && p.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public PREPAID_ITEM_PAYMENTS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
