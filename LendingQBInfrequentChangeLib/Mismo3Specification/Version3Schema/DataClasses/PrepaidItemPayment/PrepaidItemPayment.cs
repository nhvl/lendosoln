namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class PREPAID_ITEM_PAYMENT
    {
        /// <summary>
        /// Gets a value indicating whether the PREPAID_ITEM_PAYMENT container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.PaymentFinancedIndicatorSpecified
                    || this.PaymentIncludedInAPRIndicatorSpecified
                    || this.PaymentIncludedInJurisdictionHighCostIndicatorSpecified
                    || this.PrepaidItemActualPaymentAmountSpecified
                    || this.PrepaidItemEstimatedPaymentAmountSpecified
                    || this.PrepaidItemPaymentPaidByTypeOtherDescriptionSpecified
                    || this.PrepaidItemPaymentPaidByTypeSpecified
                    || this.PrepaidItemPaymentTimingTypeOtherDescriptionSpecified
                    || this.PrepaidItemPaymentTimingTypeSpecified
                    || this.RegulationZPointsAndFeesIndicatorSpecified;
            }
        }

        /// <summary>
        /// When true, indicates that the payment of any portion of the fee was financed into the original loan amount.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIndicator PaymentFinancedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PaymentFinancedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PaymentFinancedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PaymentFinancedIndicatorSpecified
        {
            get { return PaymentFinancedIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates fee is to be included in APR calculations.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOIndicator PaymentIncludedInAPRIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PaymentIncludedInAPRIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PaymentIncludedInAPRIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PaymentIncludedInAPRIndicatorSpecified
        {
            get { return PaymentIncludedInAPRIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates the amount of the payment is included in the points and fees calculation appropriated to the jurisdiction of the subject property.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOIndicator PaymentIncludedInJurisdictionHighCostIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PaymentIncludedInJurisdictionHighCostIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PaymentIncludedInJurisdictionHighCostIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PaymentIncludedInJurisdictionHighCostIndicatorSpecified
        {
            get { return PaymentIncludedInJurisdictionHighCostIndicator != null; }
            set { }
        }

        /// <summary>
        /// The actual dollar amount paid by a specific party at a specified time for a specified Prepaid Item Type.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOAmount PrepaidItemActualPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaidItemActualPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaidItemActualPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaidItemActualPaymentAmountSpecified
        {
            get { return PrepaidItemActualPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// The estimated dollar amount to be paid by a specific party at a specified time for a specified Prepaid Item Type.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOAmount PrepaidItemEstimatedPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaidItemEstimatedPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaidItemEstimatedPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaidItemEstimatedPaymentAmountSpecified
        {
            get { return PrepaidItemEstimatedPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// The role of the party making the breakout payment.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOEnum<PrepaidItemPaymentPaidByBase> PrepaidItemPaymentPaidByType;

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaidItemPaymentPaidByType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaidItemPaymentPaidByType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaidItemPaymentPaidByTypeSpecified
        {
            get { return this.PrepaidItemPaymentPaidByType != null && this.PrepaidItemPaymentPaidByType.enumValue != PrepaidItemPaymentPaidByBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Prepaid Item Paid By Type.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOString PrepaidItemPaymentPaidByTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaidItemPaymentPaidByTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaidItemPaymentPaidByTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaidItemPaymentPaidByTypeOtherDescriptionSpecified
        {
            get { return PrepaidItemPaymentPaidByTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Defines the point in time during the origination process when the prepaid item payment was paid.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOEnum<PrepaidItemPaymentTimingBase> PrepaidItemPaymentTimingType;

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaidItemPaymentTimingType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaidItemPaymentTimingType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaidItemPaymentTimingTypeSpecified
        {
            get { return this.PrepaidItemPaymentTimingType != null && this.PrepaidItemPaymentTimingType.enumValue != PrepaidItemPaymentTimingBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Prepaid Item Payment Timing Type.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOString PrepaidItemPaymentTimingTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaidItemPaymentTimingTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaidItemPaymentTimingTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaidItemPaymentTimingTypeOtherDescriptionSpecified
        {
            get { return PrepaidItemPaymentTimingTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that item is included in the Regulation Z Points and Fees calculation.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOIndicator RegulationZPointsAndFeesIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the RegulationZPointsAndFeesIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RegulationZPointsAndFeesIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool RegulationZPointsAndFeesIndicatorSpecified
        {
            get { return RegulationZPointsAndFeesIndicator != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 10)]
        public PREPAID_ITEM_PAYMENT_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
