namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PARSED_LEGAL_DESCRIPTION
    {
        /// <summary>
        /// Gets a value indicating whether the PARSED_LEGAL_DESCRIPTION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.PlattedLandsSpecified
                    || this.UnplattedLandsSpecified;
            }
        }

        /// <summary>
        /// A list of platted lands.
        /// </summary>
        [XmlElement("PLATTED_LANDS", Order = 0)]
        public PLATTED_LANDS PlattedLands;

        /// <summary>
        /// Gets or sets a value indicating whether the PlattedLands element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PlattedLands element has been assigned a value.</value>
        [XmlIgnore]
        public bool PlattedLandsSpecified
        {
            get { return this.PlattedLands != null && this.PlattedLands.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A list of lands.
        /// </summary>
        [XmlElement("UNPLATTED_LANDS", Order = 1)]
        public UNPLATTED_LANDS UnplattedLands;

        /// <summary>
        /// Gets or sets a value indicating whether the Lands element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Lands element has been assigned a value.</value>
        [XmlIgnore]
        public bool UnplattedLandsSpecified
        {
            get { return this.UnplattedLands != null && this.UnplattedLands.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public PARSED_LEGAL_DESCRIPTION_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
