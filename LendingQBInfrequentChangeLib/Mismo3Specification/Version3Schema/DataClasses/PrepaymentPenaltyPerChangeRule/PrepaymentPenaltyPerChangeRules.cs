namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PREPAYMENT_PENALTY_PER_CHANGE_RULES
    {
        /// <summary>
        /// Gets a value indicating whether the PREPAYMENT_PENALTY_PER_CHANGE_RULES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.PrepaymentPenaltyPerChangeRuleSpecified;
            }
        }

        /// <summary>
        /// A collection of prepayment penalty per change rules.
        /// </summary>
        [XmlElement("PREPAYMENT_PENALTY_PER_CHANGE_RULE", Order = 0)]
		public List<PREPAYMENT_PENALTY_PER_CHANGE_RULE> PrepaymentPenaltyPerChangeRule = new List<PREPAYMENT_PENALTY_PER_CHANGE_RULE>();

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaymentPenaltyPerChangeRule element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaymentPenaltyPerChangeRule element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaymentPenaltyPerChangeRuleSpecified
        {
            get { return this.PrepaymentPenaltyPerChangeRule != null && this.PrepaymentPenaltyPerChangeRule.Count(p => p != null && p.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public PREPAYMENT_PENALTY_PER_CHANGE_RULES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
