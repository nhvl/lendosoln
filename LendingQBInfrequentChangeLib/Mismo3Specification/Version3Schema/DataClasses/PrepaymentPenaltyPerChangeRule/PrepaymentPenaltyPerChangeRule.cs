namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class PREPAYMENT_PENALTY_PER_CHANGE_RULE
    {
        /// <summary>
        /// Gets a value indicating whether the PREPAYMENT_PENALTY_PER_CHANGE_RULE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.PrepaymentPenaltyCalculationValueTypeOtherDescriptionSpecified
                    || this.PrepaymentPenaltyCalculationValueTypeSpecified
                    || this.PrepaymentPenaltyCurtailmentAmountSpecified
                    || this.PrepaymentPenaltyCurtailmentPercentSpecified
                    || this.PrepaymentPenaltyEffectiveDateSpecified
                    || this.PrepaymentPenaltyFixedAmountSpecified
                    || this.PrepaymentPenaltyMaximumAmountSpecified
                    || this.PrepaymentPenaltyOptionTypeSpecified
                    || this.PrepaymentPenaltyPercentSpecified
                    || this.PrepaymentPenaltyPeriodCountSpecified
                    || this.PrepaymentPenaltyPeriodicDaysCountSpecified
                    || this.PrepaymentPenaltyPeriodTypeSpecified
                    || this.PrepaymentPenaltyPrincipalBalanceTypeOtherDescriptionSpecified
                    || this.PrepaymentPenaltyPrincipalBalanceTypeSpecified
                    || this.PrepaymentPenaltyTypeOtherDescriptionSpecified
                    || this.PrepaymentPenaltyTypeSpecified;
            }
        }

        /// <summary>
        /// The value used to calculate the amount of the prepayment penalty.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<PrepaymentPenaltyCalculationValueBase> PrepaymentPenaltyCalculationValueType;

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaymentPenaltyCalculationValueType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaymentPenaltyCalculationValueType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaymentPenaltyCalculationValueTypeSpecified
        {
            get { return this.PrepaymentPenaltyCalculationValueType != null && this.PrepaymentPenaltyCalculationValueType.enumValue != PrepaymentPenaltyCalculationValueBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Prepayment Penalty Calculation Value Type.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString PrepaymentPenaltyCalculationValueTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaymentPenaltyCalculationValueTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaymentPenaltyCalculationValueTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaymentPenaltyCalculationValueTypeOtherDescriptionSpecified
        {
            get { return PrepaymentPenaltyCalculationValueTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of the curtailments which triggers the prepayment penalty.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOAmount PrepaymentPenaltyCurtailmentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaymentPenaltyCurtailmentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaymentPenaltyCurtailmentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaymentPenaltyCurtailmentAmountSpecified
        {
            get { return PrepaymentPenaltyCurtailmentAmount != null; }
            set { }
        }

        /// <summary>
        /// The percentage of the curtailment(s) to principal balance that triggers the prepayment penalty.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOPercent PrepaymentPenaltyCurtailmentPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaymentPenaltyCurtailmentPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaymentPenaltyCurtailmentPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaymentPenaltyCurtailmentPercentSpecified
        {
            get { return PrepaymentPenaltyCurtailmentPercent != null; }
            set { }
        }

        /// <summary>
        /// The date that the prepayment penalty becomes effective.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMODate PrepaymentPenaltyEffectiveDate;

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaymentPenaltyEffectiveDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaymentPenaltyEffectiveDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaymentPenaltyEffectiveDateSpecified
        {
            get { return PrepaymentPenaltyEffectiveDate != null; }
            set { }
        }

        /// <summary>
        /// A fixed amount to be charged as a prepayment penalty.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOAmount PrepaymentPenaltyFixedAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaymentPenaltyFixedAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaymentPenaltyFixedAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaymentPenaltyFixedAmountSpecified
        {
            get { return PrepaymentPenaltyFixedAmount != null; }
            set { }
        }

        /// <summary>
        /// The maximum dollar amount that can be charged for any single prepayment penalty.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOAmount PrepaymentPenaltyMaximumAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaymentPenaltyMaximumAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaymentPenaltyMaximumAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaymentPenaltyMaximumAmountSpecified
        {
            get { return PrepaymentPenaltyMaximumAmount != null; }
            set { }
        }

        /// <summary>
        /// Characteristic of prepayment penalty that indicates under what transaction type the penalty may be applied (hard/soft).
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOEnum<PrepaymentPenaltyOptionBase> PrepaymentPenaltyOptionType;

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaymentPenaltyOptionType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaymentPenaltyOptionType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaymentPenaltyOptionTypeSpecified
        {
            get { return this.PrepaymentPenaltyOptionType != null && this.PrepaymentPenaltyOptionType.enumValue != PrepaymentPenaltyOptionBase.Blank; }
            set { }
        }

        /// <summary>
        /// The percentage of the appropriate balance to be charged as a penalty at time of prepayment.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOPercent PrepaymentPenaltyPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaymentPenaltyPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaymentPenaltyPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaymentPenaltyPercentSpecified
        {
            get { return PrepaymentPenaltyPercent != null; }
            set { }
        }

        /// <summary>
        /// The number of time periods, as defined by Prepayment Penalty Period Type, when the prepayment penalty applies.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOCount PrepaymentPenaltyPeriodCount;

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaymentPenaltyPeriodCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaymentPenaltyPeriodCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaymentPenaltyPeriodCountSpecified
        {
            get { return PrepaymentPenaltyPeriodCount != null; }
            set { }
        }

        /// <summary>
        /// The number of days in a year to be used in the calculation of the prepayment penalty. Common numbers are 360, 364, 365, 365.25 and 366.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOCount PrepaymentPenaltyPeriodicDaysCount;

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaymentPenaltyPeriodicDaysCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaymentPenaltyPeriodicDaysCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaymentPenaltyPeriodicDaysCountSpecified
        {
            get { return PrepaymentPenaltyPeriodicDaysCount != null; }
            set { }
        }

        /// <summary>
        /// The unit of time used for defining the duration of the prepayment penalty period (e.g., day, month).
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOEnum<PrepaymentPenaltyPeriodBase> PrepaymentPenaltyPeriodType;

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaymentPenaltyPeriodType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaymentPenaltyPeriodType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaymentPenaltyPeriodTypeSpecified
        {
            get { return this.PrepaymentPenaltyPeriodType != null && this.PrepaymentPenaltyPeriodType.enumValue != PrepaymentPenaltyPeriodBase.Blank; }
            set { }
        }

        /// <summary>
        /// The principal balance used to calculate the amount of the prepayment penalty. 
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOEnum<PrepaymentPenaltyPrincipalBalanceBase> PrepaymentPenaltyPrincipalBalanceType;

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaymentPenaltyPrincipalBalanceType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaymentPenaltyPrincipalBalanceType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaymentPenaltyPrincipalBalanceTypeSpecified
        {
            get { return this.PrepaymentPenaltyPrincipalBalanceType != null && this.PrepaymentPenaltyPrincipalBalanceType.enumValue != PrepaymentPenaltyPrincipalBalanceBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Prepayment Penalty Principal Balance Type.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOString PrepaymentPenaltyPrincipalBalanceTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaymentPenaltyPrincipalBalanceTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaymentPenaltyPrincipalBalanceTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaymentPenaltyPrincipalBalanceTypeOtherDescriptionSpecified
        {
            get { return PrepaymentPenaltyPrincipalBalanceTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Identifies the type of early principal repayment eligible for a penalty charge.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOEnum<PrepaymentPenaltyBase> PrepaymentPenaltyType;

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaymentPenaltyType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaymentPenaltyType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaymentPenaltyTypeSpecified
        {
            get { return this.PrepaymentPenaltyType != null && this.PrepaymentPenaltyType.enumValue != PrepaymentPenaltyBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Prepayment Penalty Type.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOString PrepaymentPenaltyTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaymentPenaltyTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaymentPenaltyTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaymentPenaltyTypeOtherDescriptionSpecified
        {
            get { return PrepaymentPenaltyTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 16)]
        public PREPAYMENT_PENALTY_PER_CHANGE_RULE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
