namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class GFE_SECTION_SUMMARIES
    {
        /// <summary>
        /// Gets a value indicating whether the GFE_SECTION_SUMMARIES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.GfeSectionSummarySpecified;
            }
        }

        /// <summary>
        /// A collection of GFE section summaries.
        /// </summary>
        [XmlElement("GFE_SECTION_SUMMARY", Order = 0)]
		public List<GFE_SECTION_SUMMARY> GfeSectionSummary = new List<GFE_SECTION_SUMMARY>();

        /// <summary>
        /// Gets or sets a value indicating whether the GFESectionSummary element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GFESectionSummary element has been assigned a value.</value>
        [XmlIgnore]
        public bool GfeSectionSummarySpecified
        {
            get { return this.GfeSectionSummary != null && this.GfeSectionSummary.Count(g => g != null && g.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public GFE_SECTION_SUMMARIES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
