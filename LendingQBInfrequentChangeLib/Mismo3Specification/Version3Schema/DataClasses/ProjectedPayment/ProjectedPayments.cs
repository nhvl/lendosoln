namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PROJECTED_PAYMENTS
    {
        /// <summary>
        /// Gets a value indicating whether the PROJECTED_PAYMENTS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ProjectedPaymentSpecified;
            }
        }

        /// <summary>
        /// A collection of projected payments.
        /// </summary>
        [XmlElement("PROJECTED_PAYMENT", Order = 0)]
        public List<PROJECTED_PAYMENT> ProjectedPayment = new List<PROJECTED_PAYMENT>();

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectedPayment element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectedPayment element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectedPaymentSpecified
        {
            get { return this.ProjectedPayment != null && this.ProjectedPayment.Count(p => p != null && p.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public PROJECTED_PAYMENTS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
