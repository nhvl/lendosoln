namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class PROJECTED_PAYMENT
    {
        /// <summary>
        /// Gets a value indicating whether the PROJECTED_PAYMENT container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.PaymentFrequencyTypeOtherDescriptionSpecified
                    || this.PaymentFrequencyTypeSpecified
                    || this.ProjectedPaymentCalculationPeriodEndNumberSpecified
                    || this.ProjectedPaymentCalculationPeriodStartNumberSpecified
                    || this.ProjectedPaymentCalculationPeriodTermTypeOtherDescriptionSpecified
                    || this.ProjectedPaymentCalculationPeriodTermTypeSpecified
                    || this.ProjectedPaymentEstimatedEscrowPaymentAmountSpecified
                    || this.ProjectedPaymentEstimatedTotalMaximumPaymentAmountSpecified
                    || this.ProjectedPaymentEstimatedTotalMinimumPaymentAmountSpecified
                    || this.ProjectedPaymentMIPaymentAmountSpecified
                    || this.ProjectedPaymentPrincipalAndInterestMaximumPaymentAmountSpecified
                    || this.ProjectedPaymentPrincipalAndInterestMinimumPaymentAmountSpecified;
            }
        }

        /// <summary>
        /// Specifies the frequency of the mortgage payment.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<PaymentFrequencyBase> PaymentFrequencyType;

        /// <summary>
        /// Gets or sets a value indicating whether the PaymentFrequencyType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PaymentFrequencyType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PaymentFrequencyTypeSpecified
        {
            get { return this.PaymentFrequencyType != null && this.PaymentFrequencyType.enumValue != PaymentFrequencyBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture the payment frequency type if Other is selected as the payment frequency.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString PaymentFrequencyTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PaymentFrequencyTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PaymentFrequencyTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PaymentFrequencyTypeOtherDescriptionSpecified
        {
            get { return PaymentFrequencyTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The end number of the projected payment period. Used in conjunction with Projected Payment Calculation Period Term Type.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMONumeric ProjectedPaymentCalculationPeriodEndNumber;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectedPaymentCalculationPeriodEndNumber element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectedPaymentCalculationPeriodEndNumber element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectedPaymentCalculationPeriodEndNumberSpecified
        {
            get { return ProjectedPaymentCalculationPeriodEndNumber != null; }
            set { }
        }

        /// <summary>
        /// The start number of the projected payment period. Used in conjunction with Projected Payment Calculation Period Term Type.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMONumeric ProjectedPaymentCalculationPeriodStartNumber;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectedPaymentCalculationPeriodStartNumber element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectedPaymentCalculationPeriodStartNumber element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectedPaymentCalculationPeriodStartNumberSpecified
        {
            get { return ProjectedPaymentCalculationPeriodStartNumber != null; }
            set { }
        }

        /// <summary>
        /// The duration of time used to define the projected payment period.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<ProjectedPaymentCalculationPeriodTermBase> ProjectedPaymentCalculationPeriodTermType;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectedPaymentCalculationPeriodTermType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectedPaymentCalculationPeriodTermType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectedPaymentCalculationPeriodTermTypeSpecified
        {
            get { return this.ProjectedPaymentCalculationPeriodTermType != null && this.ProjectedPaymentCalculationPeriodTermType.enumValue != ProjectedPaymentCalculationPeriodTermBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture additional information when Other is selected for Projected Payment Calculation Period Term Type.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString ProjectedPaymentCalculationPeriodTermTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectedPaymentCalculationPeriodTermTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectedPaymentCalculationPeriodTermTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectedPaymentCalculationPeriodTermTypeOtherDescriptionSpecified
        {
            get { return ProjectedPaymentCalculationPeriodTermTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The estimated amount of the payment that is applied toward escrows during the projected payment period.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOAmount ProjectedPaymentEstimatedEscrowPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectedPaymentEstimatedEscrowPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectedPaymentEstimatedEscrowPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectedPaymentEstimatedEscrowPaymentAmountSpecified
        {
            get { return ProjectedPaymentEstimatedEscrowPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// The estimated maximum amount of the total payment during the projected payment period.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOAmount ProjectedPaymentEstimatedTotalMaximumPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectedPaymentEstimatedTotalMaximumPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectedPaymentEstimatedTotalMaximumPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectedPaymentEstimatedTotalMaximumPaymentAmountSpecified
        {
            get { return ProjectedPaymentEstimatedTotalMaximumPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// The estimated minimum amount of the total payment during the projected payment period.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOAmount ProjectedPaymentEstimatedTotalMinimumPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectedPaymentEstimatedTotalMinimumPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectedPaymentEstimatedTotalMinimumPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectedPaymentEstimatedTotalMinimumPaymentAmountSpecified
        {
            get { return ProjectedPaymentEstimatedTotalMinimumPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// The amount of the payment that is applied toward mortgage insurance during the projected payment period.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOAmount ProjectedPaymentMIPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectedPaymentMIPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectedPaymentMIPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectedPaymentMIPaymentAmountSpecified
        {
            get { return ProjectedPaymentMIPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// The maximum amount of the principal and interest payment during the projected payment period.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOAmount ProjectedPaymentPrincipalAndInterestMaximumPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectedPaymentPrincipalAndInterestMaximumPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectedPaymentPrincipalAndInterestMaximumPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectedPaymentPrincipalAndInterestMaximumPaymentAmountSpecified
        {
            get { return ProjectedPaymentPrincipalAndInterestMaximumPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// The minimum amount of the principal and interest payment during the projected payment period.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOAmount ProjectedPaymentPrincipalAndInterestMinimumPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectedPaymentPrincipalAndInterestMinimumPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectedPaymentPrincipalAndInterestMinimumPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectedPaymentPrincipalAndInterestMinimumPaymentAmountSpecified
        {
            get { return ProjectedPaymentPrincipalAndInterestMinimumPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 12)]
        public PROJECTED_PAYMENT_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null ? Extension.ShouldSerialize : false; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
