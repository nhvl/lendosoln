namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class COMBINED_LTVS
    {
        /// <summary>
        /// Gets a value indicating whether the COMBINED_LTVS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CombinedLtvSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of combined LTVs.
        /// </summary>
        [XmlElement("COMBINED_LTV", Order = 0)]
		public List<COMBINED_LTV> CombinedLtv = new List<COMBINED_LTV>();

        /// <summary>
        /// Gets or sets a value indicating whether the CombinedLTV element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CombinedLTV element has been assigned a value.</value>
        [XmlIgnore]
        public bool CombinedLtvSpecified
        {
            get { return this.CombinedLtv != null && this.CombinedLtv.Count(c => c != null && c.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public COMBINED_LTVS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
