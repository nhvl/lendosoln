namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class COMBINED_LTV
    {
        /// <summary>
        /// Gets a value indicating whether the COMBINED_LTV container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CombinedLTVRatioPercentSpecified
                    || this.ExtensionSpecified
                    || this.HomeEquityCombinedLTVRatioPercentSpecified
                    || this.LTVCalculationDateSpecified
                    || this.OriginalLTVIndicatorSpecified;
            }
        }

        /// <summary>
        /// The result of dividing the combined unpaid principal balance (UPB) amounts of the first and all subordinate mortgages, excluding undrawn home equity lines of credit amounts, by the value of the subject property. 
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOPercent CombinedLTVRatioPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the CombinedLTVRatioPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CombinedLTVRatioPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool CombinedLTVRatioPercentSpecified
        {
            get { return CombinedLTVRatioPercent != null; }
            set { }
        }

        /// <summary>
        /// The result of dividing the sum of the unpaid principal balance (UPB) of the first mortgage, the full amount of any home equity line of credit (whether drawn or undrawn), and the balance of any other subordinate financing by the value of the subject property.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOPercent HomeEquityCombinedLTVRatioPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the HomeEquityCombinedLTVRatioPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HomeEquityCombinedLTVRatioPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool HomeEquityCombinedLTVRatioPercentSpecified
        {
            get { return HomeEquityCombinedLTVRatioPercent != null; }
            set { }
        }

        /// <summary>
        /// The date the LTV was calculated.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMODate LTVCalculationDate;

        /// <summary>
        /// Gets or sets a value indicating whether the LTVCalculationDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LTVCalculationDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool LTVCalculationDateSpecified
        {
            get { return LTVCalculationDate != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the combined LTV values are for the LOAN containers whose Loan State Type is At Closing.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOIndicator OriginalLTVIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the OriginalLTVIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OriginalLTVIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool OriginalLTVIndicatorSpecified
        {
            get { return OriginalLTVIndicator != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public COMBINED_LTV_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
