namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class DATA_ITEM_CHANGE_REQUEST_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the DATA_ITEM_CHANGE_REQUEST_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DataItemChangeAbsoluteXPathSpecified
                    || this.ExtensionSpecified
                    || this.RequestDatetimeSpecified;
            }
        }

        /// <summary>
        /// An absolute XPath expression that identifies the context for the included data item change containers. The relative XPath expressions in the data item change containers must be relative to this absolute XPath expression.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOXPath DataItemChangeAbsoluteXPath;

        /// <summary>
        /// Gets or sets a value indicating whether the DataItemChangeAbsoluteXPath element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DataItemChangeAbsoluteXPath element has been assigned a value.</value>
        [XmlIgnore]
        public bool DataItemChangeAbsoluteXPathSpecified
        {
            get { return DataItemChangeAbsoluteXPath != null; }
            set { }
        }

        /// <summary>
        /// A system generated date and time stamp enclosed within each request to identify the initiation time of the request.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMODatetime RequestDatetime;

        /// <summary>
        /// Gets or sets a value indicating whether the RequestDateTime element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RequestDateTime element has been assigned a value.</value>
        [XmlIgnore]
        public bool RequestDatetimeSpecified
        {
            get { return RequestDatetime != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public DATA_ITEM_CHANGE_REQUEST_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
