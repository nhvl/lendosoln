namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class DATA_ITEM_CHANGE_REQUEST
    {
        /// <summary>
        /// Gets a value indicating whether the DATA_ITEM_CHANGE_REQUEST container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DataItemChangeContextsSpecified
                    || this.DataItemChangeRequestDetailSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A list of data item change contexts.
        /// </summary>
        [XmlElement("DATA_ITEM_CHANGE_CONTEXTS", Order = 0)]
        public DATA_ITEM_CHANGE_CONTEXTS DataItemChangeContexts;

        /// <summary>
        /// Gets or sets a value indicating whether the DataItemChangeContexts element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DataItemChangeContexts element has been assigned a value.</value>
        [XmlIgnore]
        public bool DataItemChangeContextsSpecified
        {
            get { return this.DataItemChangeContexts != null && this.DataItemChangeContexts.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details on a data item change request.
        /// </summary>
        [XmlElement("DATA_ITEM_CHANGE_REQUEST_DETAIL", Order = 1)]
        public DATA_ITEM_CHANGE_REQUEST_DETAIL DataItemChangeRequestDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the DataItemChangeRequestDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DataItemChangeRequestDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool DataItemChangeRequestDetailSpecified
        {
            get { return this.DataItemChangeRequestDetail != null && this.DataItemChangeRequestDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public DATA_ITEM_CHANGE_REQUEST_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
