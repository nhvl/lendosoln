namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class MANUFACTURED_HOME_SECTIONS
    {
        /// <summary>
        /// Gets a value indicating whether the MANUFACTURED_HOME_SECTIONS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ManufacturedHomeSectionSpecified;
            }
        }

        /// <summary>
        /// A collection of manufactured home sections.
        /// </summary>
        [XmlElement("MANUFACTURED_HOME_SECTION", Order = 0)]
		public List<MANUFACTURED_HOME_SECTION> ManufacturedHomeSection = new List<MANUFACTURED_HOME_SECTION>();

        /// <summary>
        /// Gets or sets a value indicating whether the ManufacturedHomeSection element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ManufacturedHomeSection element has been assigned a value.</value>
        [XmlIgnore]
        public bool ManufacturedHomeSectionSpecified
        {
            get { return this.ManufacturedHomeSection != null && this.ManufacturedHomeSection.Count(m => m != null && m.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public MANUFACTURED_HOME_SECTIONS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
