namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class MANUFACTURED_HOME_SECTION
    {
        /// <summary>
        /// Gets a value indicating whether the MANUFACTURED_HOME_SECTION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.LengthFeetNumberSpecified
                    || this.ManufacturedHomeHUDCertificationLabelIdentifierSpecified
                    || this.ManufacturedHomeSectionTypeOtherDescriptionSpecified
                    || this.ManufacturedHomeSectionTypeSpecified
                    || this.SquareFeetNumberSpecified
                    || this.WidthFeetNumberSpecified;
            }
        }

        /// <summary>
        /// The length measured in linear feet.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMONumeric LengthFeetNumber;

        /// <summary>
        /// Gets or sets a value indicating whether the LengthFeetNumber element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LengthFeetNumber element has been assigned a value.</value>
        [XmlIgnore]
        public bool LengthFeetNumberSpecified
        {
            get { return LengthFeetNumber != null; }
            set { }
        }

        /// <summary>
        /// An identifier made up of 3 letters followed by 6 numbers (in some cases 7 numbers). It may be found on a (red) metal label affixed to the exterior of the manufactured home. It might read something like ULI 012345. As per HUD regulations, the label identifier consists of a 3 letter designation which identifies the production inspection primary inspection agency, and which the Sectary shall assign. Each label shall be marked with a 6 digit number which the label supplier shall furnish. The labels shall be stamped with numbers sequentially.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOIdentifier ManufacturedHomeHUDCertificationLabelIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the ManufacturedHomeHUDCertificationLabelIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ManufacturedHomeHUDCertificationLabelIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool ManufacturedHomeHUDCertificationLabelIdentifierSpecified
        {
            get { return ManufacturedHomeHUDCertificationLabelIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Specifies the section of the manufactured home being measured.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<ManufacturedHomeSectionBase> ManufacturedHomeSectionType;

        /// <summary>
        /// Gets or sets a value indicating whether the ManufacturedHomeSectionType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ManufacturedHomeSectionType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ManufacturedHomeSectionTypeSpecified
        {
            get { return this.ManufacturedHomeSectionType != null && this.ManufacturedHomeSectionType.enumValue != ManufacturedHomeSectionBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Manufactured Home Section Type.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString ManufacturedHomeSectionTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ManufacturedHomeSectionTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ManufacturedHomeSectionTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ManufacturedHomeSectionTypeOtherDescriptionSpecified
        {
            get { return ManufacturedHomeSectionTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Identifies the total area measured in square feet.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMONumeric SquareFeetNumber;

        /// <summary>
        /// Gets or sets a value indicating whether the SquareFeetNumber element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SquareFeetNumber element has been assigned a value.</value>
        [XmlIgnore]
        public bool SquareFeetNumberSpecified
        {
            get { return SquareFeetNumber != null; }
            set { }
        }

        /// <summary>
        /// The width measured in linear feet.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMONumeric WidthFeetNumber;

        /// <summary>
        /// Gets or sets a value indicating whether the WidthFeetNumber element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the WidthFeetNumber element has been assigned a value.</value>
        [XmlIgnore]
        public bool WidthFeetNumberSpecified
        {
            get { return WidthFeetNumber != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 6)]
        public MANUFACTURED_HOME_SECTION_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
