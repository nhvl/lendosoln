namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CREDIT_SUMMARIES
    {
        /// <summary>
        /// Gets a value indicating whether the CREDIT_SUMMARIES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditSummarySpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of credit summaries.
        /// </summary>
        [XmlElement("CREDIT_SUMMARY", Order = 0)]
		public List<CREDIT_SUMMARY> CreditSummary = new List<CREDIT_SUMMARY>();

        /// <summary>
        /// Gets or sets a value indicating whether the CreditSummary element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditSummary element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditSummarySpecified
        {
            get { return this.CreditSummary != null && this.CreditSummary.Count(c => c != null && c.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public CREDIT_SUMMARIES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
