namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CREDIT_SUMMARY_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the CREDIT_SUMMARY_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditSummaryNameSpecified
                    || this.CreditSummaryTextSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// This is the name used to label or describe an individual group of summary data elements. It is useful when there are multiple groups of summary data elements within a credit report.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString CreditSummaryName;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditSummaryName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditSummaryName element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditSummaryNameSpecified
        {
            get { return CreditSummaryName != null; }
            set { }
        }

        /// <summary>
        /// Any type of additional summary data that is provided by the credit bureau.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString CreditSummaryText;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditSummaryText element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditSummaryText element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditSummaryTextSpecified
        {
            get { return CreditSummaryText != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public CREDIT_SUMMARY_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
