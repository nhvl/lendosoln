namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class GFE_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the GFE_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.GFECreditOrChargeForChosenInterestRateTypeOtherDescriptionSpecified
                    || this.GFECreditOrChargeForChosenInterestRateTypeSpecified
                    || this.GFEDisclosureDateSpecified
                    || this.GFEInterestRateAvailableThroughDateSpecified
                    || this.GFELoanOriginatorFeePaymentCreditTypeOtherDescriptionSpecified
                    || this.GFELoanOriginatorFeePaymentCreditTypeSpecified
                    || this.GFEMonthlyPaymentFirstIncreasePaymentAmountSpecified
                    || this.GFEMonthlyPaymentMaximumAmountSpecified
                    || this.GFERateLockMinimumDaysPriorToSettlementCountSpecified
                    || this.GFERateLockPeriodDaysCountSpecified
                    || this.GFERedisclosureReasonDateSpecified
                    || this.GFERedisclosureReasonDescriptionSpecified
                    || this.GFESettlementChargesAvailableThroughDateSpecified;
            }
        }

        /// <summary>
        /// Identifies the type of credit or charge to the Borrower associated with the Borrower chosen interest rate, as reflected on a single instance of the GFE.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<GFECreditOrChargeForChosenInterestRateBase> GFECreditOrChargeForChosenInterestRateType;

        /// <summary>
        /// Gets or sets a value indicating whether the GFECreditOrChargeForChosenInterestRateType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GFECreditOrChargeForChosenInterestRateType element has been assigned a value.</value>
        [XmlIgnore]
        public bool GFECreditOrChargeForChosenInterestRateTypeSpecified
        {
            get { return this.GFECreditOrChargeForChosenInterestRateType != null && this.GFECreditOrChargeForChosenInterestRateType.enumValue != GFECreditOrChargeForChosenInterestRateBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field which contains the description of the type of GFE credit or charge when Other is selected as the GFE Credit Or Charge For Chosen Interest Rate Type.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString GFECreditOrChargeForChosenInterestRateTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the GFECreditOrChargeForChosenInterestRateTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GFECreditOrChargeForChosenInterestRateTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool GFECreditOrChargeForChosenInterestRateTypeOtherDescriptionSpecified
        {
            get { return GFECreditOrChargeForChosenInterestRateTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The date as reflected on a single instance of the Good Faith Estimate.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMODate GFEDisclosureDate;

        /// <summary>
        /// Gets or sets a value indicating whether the GFEDisclosureDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GFEDisclosureDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool GFEDisclosureDateSpecified
        {
            get { return GFEDisclosureDate != null; }
            set { }
        }

        /// <summary>
        /// Specifies the date through which the initial interest rate, as reflected on a single instance of the GFE, is available.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMODate GFEInterestRateAvailableThroughDate;

        /// <summary>
        /// Gets or sets a value indicating whether the GFEInterestRateAvailableThroughDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GFEInterestRateAvailableThroughDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool GFEInterestRateAvailableThroughDateSpecified
        {
            get { return GFEInterestRateAvailableThroughDate != null; }
            set { }
        }

        /// <summary>
        /// Identifies the type of application of loan originator credit(s) to be reflected on the GFE and the HUD1.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<GFELoanOriginatorFeePaymentCreditBase> GFELoanOriginatorFeePaymentCreditType;

        /// <summary>
        /// Gets or sets a value indicating whether the GFELoanOriginatorFeePaymentCreditType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GFELoanOriginatorFeePaymentCreditType element has been assigned a value.</value>
        [XmlIgnore]
        public bool GFELoanOriginatorFeePaymentCreditTypeSpecified
        {
            get { return this.GFELoanOriginatorFeePaymentCreditType != null && this.GFELoanOriginatorFeePaymentCreditType.enumValue != GFELoanOriginatorFeePaymentCreditBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field which contains the description of the type of loan originator credit(s) when Other is selected as the GFE Loan Originator Fee Payment Credit Type.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString GFELoanOriginatorFeePaymentCreditTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the GFELoanOriginatorFeePaymentCreditTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GFELoanOriginatorFeePaymentCreditTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool GFELoanOriginatorFeePaymentCreditTypeOtherDescriptionSpecified
        {
            get { return GFELoanOriginatorFeePaymentCreditTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The amount of the monthly loan payment (comprised of principal and interest and monthly MI) after the first payment increase is in effect, as reflected on a single instance of the GFE. 
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOAmount GFEMonthlyPaymentFirstIncreasePaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the GFEMonthlyPaymentFirstIncreasePaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GFEMonthlyPaymentFirstIncreasePaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool GFEMonthlyPaymentFirstIncreasePaymentAmountSpecified
        {
            get { return GFEMonthlyPaymentFirstIncreasePaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// The maximum amount of the monthly loan payment (comprised of principal and interest and monthly MI) that may be due, as reflected on a single instance of the GFE. 
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOAmount GFEMonthlyPaymentMaximumAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the GFEMonthlyPaymentMaximumAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GFEMonthlyPaymentMaximumAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool GFEMonthlyPaymentMaximumAmountSpecified
        {
            get { return GFEMonthlyPaymentMaximumAmount != null; }
            set { }
        }

        /// <summary>
        /// Specifies the minimum number of days before settlement by which the interest rate must be locked, as reflected on a single instance of the GFE. 
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOCount GFERateLockMinimumDaysPriorToSettlementCount;

        /// <summary>
        /// Gets or sets a value indicating whether the GFERateLockMinimumDaysPriorToSettlementCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GFERateLockMinimumDaysPriorToSettlementCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool GFERateLockMinimumDaysPriorToSettlementCountSpecified
        {
            get { return GFERateLockMinimumDaysPriorToSettlementCount != null; }
            set { }
        }

        /// <summary>
        /// Specifies the length of the rate lock period, in terms of a number of days, as reflected on a single instance of the GFE. 
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOCount GFERateLockPeriodDaysCount;

        /// <summary>
        /// Gets or sets a value indicating whether the GFERateLockPeriodDaysCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GFERateLockPeriodDaysCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool GFERateLockPeriodDaysCountSpecified
        {
            get { return GFERateLockPeriodDaysCount != null; }
            set { }
        }

        /// <summary>
        /// The date of the occurrence of the circumstance(s) that have changed to warrant re-disclosure.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMODate GFERedisclosureReasonDate;

        /// <summary>
        /// Gets or sets a value indicating whether the GFERe-disclosureReasonDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GFERe-disclosureReasonDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool GFERedisclosureReasonDateSpecified
        {
            get { return GFERedisclosureReasonDate != null; }
            set { }
        }

        /// <summary>
        /// The description of the circumstance(s) that have changed to warrant re-disclosure.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOString GFERedisclosureReasonDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the GFERe-DisclosureReasonDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GFERe-DisclosureReasonDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool GFERedisclosureReasonDescriptionSpecified
        {
            get { return GFERedisclosureReasonDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the date through which the estimated amount for all other settlement charges, as reflected on a single instance of the GFE, is available.  
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMODate GFESettlementChargesAvailableThroughDate;

        /// <summary>
        /// Gets or sets a value indicating whether the GFESettlementChargesAvailableThroughDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GFESettlementChargesAvailableThroughDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool GFESettlementChargesAvailableThroughDateSpecified
        {
            get { return GFESettlementChargesAvailableThroughDate != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 13)]
        public GFE_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
