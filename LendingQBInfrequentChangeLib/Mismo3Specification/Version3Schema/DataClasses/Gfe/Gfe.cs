namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class GFE
    {
        /// <summary>
        /// Gets a value indicating whether the container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.GfeDetailSpecified
                    || this.GfeSectionSummariesSpecified;
            }
        }

        /// <summary>
        /// Details on the GFE.
        /// </summary>
        [XmlElement("GFE_DETAIL", Order = 0)]
        public GFE_DETAIL GfeDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the GFEDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GFEDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool GfeDetailSpecified
        {
            get { return this.GfeDetail != null && this.GfeDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Summaries of GFE sections.
        /// </summary>
        [XmlElement("GFE_SECTION_SUMMARIES", Order = 1)]
        public GFE_SECTION_SUMMARIES GfeSectionSummaries;

        /// <summary>
        /// Gets or sets a value indicating whether the GFESectionSummaries element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GFESectionSummaries element has been assigned a value.</value>
        [XmlIgnore]
        public bool GfeSectionSummariesSpecified
        {
            get { return this.GfeSectionSummaries != null && this.GfeSectionSummaries.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public GFE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
