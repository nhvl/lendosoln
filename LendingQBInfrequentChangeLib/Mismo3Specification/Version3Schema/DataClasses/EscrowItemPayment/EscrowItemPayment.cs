namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class ESCROW_ITEM_PAYMENT
    {
        /// <summary>
        /// Gets a value indicating whether the ESCROW_ITEM_PAYMENT container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.EscrowItemActualPaymentAmountSpecified
                    || this.EscrowItemEstimatedPaymentAmountSpecified
                    || this.EscrowItemPaymentPaidByTypeOtherDescriptionSpecified
                    || this.EscrowItemPaymentPaidByTypeSpecified
                    || this.EscrowItemPaymentTimingTypeOtherDescriptionSpecified
                    || this.EscrowItemPaymentTimingTypeSpecified
                    || this.ExtensionSpecified
                    || this.PaymentFinancedIndicatorSpecified
                    || this.PaymentIncludedInAPRIndicatorSpecified
                    || this.PaymentIncludedInJurisdictionHighCostIndicatorSpecified
                    || this.RegulationZPointsAndFeesIndicatorSpecified;
            }
        }

        /// <summary>
        /// The actual dollar amount paid by a specific party at a specified time for a specified escrow item.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount EscrowItemActualPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowItemActualPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowItemActualPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowItemActualPaymentAmountSpecified
        {
            get { return EscrowItemActualPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// The estimated dollar amount paid by a specific party at a specified time for a specified escrow item..
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOAmount EscrowItemEstimatedPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowItemEstimatedPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowItemEstimatedPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowItemEstimatedPaymentAmountSpecified
        {
            get { return EscrowItemEstimatedPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// The role of the party making the escrow item payment.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<EscrowItemPaymentPaidByBase> EscrowItemPaymentPaidByType;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowItemPaymentPaidByType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowItemPaymentPaidByType element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowItemPaymentPaidByTypeSpecified
        {
            get { return this.EscrowItemPaymentPaidByType != null && this.EscrowItemPaymentPaidByType.enumValue != EscrowItemPaymentPaidByBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Escrow Item Paid By Type.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString EscrowItemPaymentPaidByTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowItemPaymentPaidByTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowItemPaymentPaidByTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowItemPaymentPaidByTypeOtherDescriptionSpecified
        {
            get { return EscrowItemPaymentPaidByTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Defines the point in time during the origination process when the escrow item payment was paid.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<EscrowItemPaymentTimingBase> EscrowItemPaymentTimingType;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowItemPaymentTimingType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowItemPaymentTimingType element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowItemPaymentTimingTypeSpecified
        {
            get { return this.EscrowItemPaymentTimingType != null && this.EscrowItemPaymentTimingType.enumValue != EscrowItemPaymentTimingBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Escrow Item Payment Timing Type.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString EscrowItemPaymentTimingTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowItemPaymentTimingTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowItemPaymentTimingTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowItemPaymentTimingTypeOtherDescriptionSpecified
        {
            get { return EscrowItemPaymentTimingTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that the payment of any portion of the fee was financed into the original loan amount.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOIndicator PaymentFinancedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PaymentFinancedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PaymentFinancedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PaymentFinancedIndicatorSpecified
        {
            get { return PaymentFinancedIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates fee is to be included in APR calculations.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOIndicator PaymentIncludedInAPRIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PaymentIncludedInAPRIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PaymentIncludedInAPRIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PaymentIncludedInAPRIndicatorSpecified
        {
            get { return PaymentIncludedInAPRIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates the amount of the payment is included in the points and fees calculation appropriated to the jurisdiction of the subject property.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOIndicator PaymentIncludedInJurisdictionHighCostIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PaymentIncludedInJurisdictionHighCostIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PaymentIncludedInJurisdictionHighCostIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PaymentIncludedInJurisdictionHighCostIndicatorSpecified
        {
            get { return PaymentIncludedInJurisdictionHighCostIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that item is included in the Regulation Z Points and Fees calculation.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOIndicator RegulationZPointsAndFeesIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the RegulationZPointsAndFeesIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RegulationZPointsAndFeesIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool RegulationZPointsAndFeesIndicatorSpecified
        {
            get { return RegulationZPointsAndFeesIndicator != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 10)]
        public ESCROW_ITEM_PAYMENT_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
