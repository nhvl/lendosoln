namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class ESCROW_ITEM_PAYMENTS
    {
        /// <summary>
        /// Gets a value indicating whether the ESCROW_ITEM_PAYMENTS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.EscrowItemPaymentSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of escrow item payments.
        /// </summary>
        [XmlElement("ESCROW_ITEM_PAYMENT", Order = 0)]
        public List<ESCROW_ITEM_PAYMENT> EscrowItemPayment = new List<ESCROW_ITEM_PAYMENT>();

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowItemPayment element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowItemPayment element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowItemPaymentSpecified
        {
            get { return this.EscrowItemPayment != null && this.EscrowItemPayment.Count(e => e != null && e.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public ESCROW_ITEM_PAYMENTS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
