namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets a value indicating whether the DocumentCertificationDetail has a value to serialize.
    /// </summary>
    /// <returns>A value indicating whether the DocumentCertificationDetail has a value to serialize.</returns>
    public partial class DOCUMENT_CERTIFICATION
    {
        /// <summary>
        /// Gets a value indicating whether the DOCUMENT_CERTIFICATION container should be serialized
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get { return this.DocumentCertificationDetailSpecified; }
        }

        /// <summary>
        /// A container for details about this document certification.
        /// </summary>
        [XmlElement("DOCUMENT_CERTIFICATION_DETAIL", Order = 0)]
        public DOCUMENT_CERTIFICATION_DETAIL DocumentCertificationDetail;

        /// <summary>
        /// Gets a value indicating whether the DocumentCertificationDetail has a value to serialize.
        /// </summary>
        /// <returns>A value indicating whether the DocumentCertificationDetail has a value to serialize.</returns>
        [XmlIgnore]
        public bool DocumentCertificationDetailSpecified
        {
            get { return DocumentCertificationDetail != null && DocumentCertificationDetail.ShouldSerialize; }
        }
    }
}
