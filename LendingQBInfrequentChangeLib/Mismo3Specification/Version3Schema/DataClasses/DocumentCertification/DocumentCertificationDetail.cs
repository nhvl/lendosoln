namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets a value indicating whether the DocumentSubmissionIndicator has a value to serialize.
    /// </summary>
    /// <returns>A value indicating whether the DocumentSubmissionIndicator has a value to serialize.</returns>
    public partial class DOCUMENT_CERTIFICATION_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the DOCUMENT_CERTIFICATION_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get { return 
                this.DocumentFormPublisherNumberIdentifierSpecified ||
                this.DocumentRequiredIndicatorSpecified ||
                this.DocumentSubmissionIndicatorSpecified; }
        }

        /// <summary>
        /// Cert Agreement. This data point is used in conjunction with DocumentSubmissionIndicator and DocumentRequiredIndicator. This captures the form name of 11711A. 
        /// Used in Ginnie Mae Exporter.
        /// </summary>
        [XmlElement("DocumentFormPublisherNumberIdentifier", Order = 0)]
        public MISMOIdentifier DocumentFormPublisherNumberIdentifier;

        /// <summary>
        /// Gets a value indicating whether the DocumentFormPublisherNumberIdentifier has a value to serialize.
        /// </summary>
        /// <returns>A value indicating whether the DocumentFormPublisherNumberIdentifier has a value to serialize.</returns>
        [XmlIgnore]
        public bool DocumentFormPublisherNumberIdentifierSpecified
        {
            get { return DocumentFormPublisherNumberIdentifier != null; }
        }

        /// <summary>
        /// Cert agreement. This data point is used in conjunction with DocumentFormPublisherNumberIdentifier and DocumentSubmissionIndicator.
        /// </summary>
        [XmlElement("DocumentRequiredIndicator", Order = 1)]
        public MISMOIndicator DocumentRequiredIndicator;

        /// <summary>
        /// Gets a value indicating whether the DocumentRequiredIndicator has a value to serialize.
        /// </summary>
        /// <returns>A value indicating whether the DocumentRequiredIndicator has a value to serialize.</returns>
        [XmlIgnore]
        public bool DocumentRequiredIndicatorSpecified
        {
            get { return DocumentRequiredIndicator != null; }
        }

        /// <summary>
        /// Sent 11711. This data point is used in conjunction with DocumentFormPublisherNumberIdentifier and DocumentRequiredIndicator.
        /// </summary>
        [XmlElement("DocumentSubmissionIndicator", Order = 2)]
        public MISMOIndicator DocumentSubmissionIndicator;

        /// <summary>
        /// Gets a value indicating whether the DocumentSubmissionIndicator has a value to serialize.
        /// </summary>
        /// <returns>A value indicating whether the DocumentSubmissionIndicator has a value to serialize.</returns>
        [XmlIgnore]
        public bool DocumentSubmissionIndicatorSpecified
        {
            get { return DocumentSubmissionIndicator != null; }
        }
    }
}
