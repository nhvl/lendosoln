namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets a value indicating whether the DocumentCertification element has a value to serialize.
    /// </summary>
    /// <returns>A boolean indicating whether the DocumentCertification element has been assigned a value.</returns>
    public partial class DOCUMENT_CERTIFICATIONS
    {
        /// <summary>
        /// Creates an instance of the <see cref="DOCUMENT_CERTIFICATIONS"/> class.
        /// </summary>
        public DOCUMENT_CERTIFICATIONS()
        {
        }

        /// <summary>
        /// Creates an instance of the <see cref="DOCUMENT_CERTIFICATIONS"/> class.
        /// </summary>
        /// <param name="certification">A certification to add to the newly initialized object.</param>
        public DOCUMENT_CERTIFICATIONS(DOCUMENT_CERTIFICATION certification)
        {
            this.DocumentCertification.Add(certification);
        }

        /// <summary>
        /// Gets a value indicating whether the DOCUMENT_CERTIFICATIONS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get { return this.DocumentCertificationSpecified; }
        }

        /// <summary>
        /// A list of document certifications for the document.
        /// </summary>
        [XmlElement("DOCUMENT_CERTIFICATION", Order = 0)]
        public List<DOCUMENT_CERTIFICATION> DocumentCertification = new List<DOCUMENT_CERTIFICATION>();

        /// <summary>
        /// Gets a value indicating whether the DocumentCertification element has a value to serialize.
        /// </summary>
        /// <returns>A boolean indicating whether the DocumentCertification element has been assigned a value.</returns>
        [XmlIgnore]
        public bool DocumentCertificationSpecified
        {
            get { return this.DocumentCertification != null && this.DocumentCertification.Count(f => f != null && f.ShouldSerialize) > 0; }
        }
    }
}
