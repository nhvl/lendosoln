namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class WORKOUT_COMMENTS
    {
        /// <summary>
        /// Gets a value indicating whether the WORKOUT_COMMENTS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.WorkoutCommentSpecified;
            }
        }

        /// <summary>
        /// A collection of workout comments.
        /// </summary>
        [XmlElement("WORKOUT_COMMENT", Order = 0)]
		public List<WORKOUT_COMMENT> WorkoutComment = new List<WORKOUT_COMMENT>();

        /// <summary>
        /// Gets or sets a value indicating whether the WorkoutComment element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the WorkoutComment element has been assigned a value.</value>
        [XmlIgnore]
        public bool WorkoutCommentSpecified
        {
            get { return this.WorkoutComment != null && this.WorkoutComment.Count(w => w != null && w.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public WORKOUT_COMMENTS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
