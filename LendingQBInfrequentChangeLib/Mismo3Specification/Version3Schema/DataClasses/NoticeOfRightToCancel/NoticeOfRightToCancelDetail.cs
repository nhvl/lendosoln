namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class NOTICE_OF_RIGHT_TO_CANCEL_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the NOTICE_OF_RIGHT_TO_CANCEL_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.NoticeOfRightToCancelTransactionDateSpecified;
            }
        }

        /// <summary>
        /// The date specified as the transaction date on the Notice Of Right To Cancel document.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMODate NoticeOfRightToCancelTransactionDate;

        /// <summary>
        /// Gets or sets a value indicating whether the NoticeOfRightToCancelTransactionDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NoticeOfRightToCancelTransactionDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool NoticeOfRightToCancelTransactionDateSpecified
        {
            get { return NoticeOfRightToCancelTransactionDate != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public NOTICE_OF_RIGHT_TO_CANCEL_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
