namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class NOTICE_OF_RIGHT_TO_CANCEL
    {
        /// <summary>
        /// Gets a value indicating whether the NOTICE_OF_RIGHT_TO_CANCEL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.NoticeOfRightToCancelDetailSpecified
                    || this.RightToCancelContactSpecified;
            }
        }

        /// <summary>
        /// Details on a notice of right to cancel.
        /// </summary>
        [XmlElement("NOTICE_OF_RIGHT_TO_CANCEL_DETAIL", Order = 0)]
        public NOTICE_OF_RIGHT_TO_CANCEL_DETAIL NoticeOfRightToCancelDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the NoticeOfRightToCancelDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NoticeOfRightToCancelDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool NoticeOfRightToCancelDetailSpecified
        {
            get { return this.NoticeOfRightToCancelDetail != null && this.NoticeOfRightToCancelDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The entity to whom a notice of intent to cancel the contract must be sent.
        /// </summary>
        [XmlElement("RIGHT_TO_CANCEL_CONTACT", Order = 1)]
        public RIGHT_TO_CANCEL_CONTACT RightToCancelContact;

        /// <summary>
        /// Gets or sets a value indicating whether the RightToCancelContact element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RightToCancelContact element has been assigned a value.</value>
        [XmlIgnore]
        public bool RightToCancelContactSpecified
        {
            get { return this.RightToCancelContact != null && this.RightToCancelContact.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public NOTICE_OF_RIGHT_TO_CANCEL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
