namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class REGULATORY_AGENCY
    {
        /// <summary>
        /// Gets a value indicating whether the REGULATORY_AGENCY container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.RegulatoryAgencyTypeOtherDescriptionSpecified
                    || this.RegulatoryAgencyTypeSpecified;
            }
        }

        /// <summary>
        /// Identifies the type of regulatory agency.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<RegulatoryAgencyBase> RegulatoryAgencyType;

        /// <summary>
        /// Gets or sets a value indicating whether the RegulatoryAgencyType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RegulatoryAgencyType element has been assigned a value.</value>
        [XmlIgnore]
        public bool RegulatoryAgencyTypeSpecified
        {
            get { return this.RegulatoryAgencyType != null && this.RegulatoryAgencyType.enumValue != RegulatoryAgencyBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the amenity if Other is selected as the Regulatory Agency Type.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString RegulatoryAgencyTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the RegulatoryAgencyTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RegulatoryAgencyTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool RegulatoryAgencyTypeOtherDescriptionSpecified
        {
            get { return RegulatoryAgencyTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public REGULATORY_AGENCY_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
