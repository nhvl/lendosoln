namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class SALES_CONCESSION
    {
        /// <summary>
        /// Gets a value indicating whether the SALES_CONCESSION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.SalesConcessionAmountSpecified
                    || this.SalesConcessionProvidedByTypeOtherDescriptionSpecified
                    || this.SalesConcessionProvidedByTypeSpecified
                    || this.SalesConcessionTypeOtherDescriptionSpecified
                    || this.SalesConcessionTypeSpecified;
            }
        }

        /// <summary>
        /// The dollar amount of the value of the individual sales concession corresponding to the Sales Concession Type.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount SalesConcessionAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the SalesConcessionAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SalesConcessionAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool SalesConcessionAmountSpecified
        {
            get { return SalesConcessionAmount != null; }
            set { }
        }

        /// <summary>
        /// Specifies the party providing the sales concession.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<SalesConcessionProvidedByBase> SalesConcessionProvidedByType;

        /// <summary>
        /// Gets or sets a value indicating whether the SalesConcessionProvidedByType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SalesConcessionProvidedByType element has been assigned a value.</value>
        [XmlIgnore]
        public bool SalesConcessionProvidedByTypeSpecified
        {
            get { return this.SalesConcessionProvidedByType != null && this.SalesConcessionProvidedByType.enumValue != SalesConcessionProvidedByBase.Blank; }
            set { }
        }

        /// <summary>
        ///  A free-form text field used to collect additional information when Other is selected for Sales Concession Provided By Type. 
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString SalesConcessionProvidedByTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the SalesConcessionProvidedByTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SalesConcessionProvidedByTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool SalesConcessionProvidedByTypeOtherDescriptionSpecified
        {
            get { return SalesConcessionProvidedByTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Quantifies the type of concessions related to the transaction for which the contract has been analyzed.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOEnum<SalesConcessionBase> SalesConcessionType;

        /// <summary>
        /// Gets or sets a value indicating whether the SalesConcessionType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SalesConcessionType element has been assigned a value.</value>
        [XmlIgnore]
        public bool SalesConcessionTypeSpecified
        {
            get { return this.SalesConcessionType != null && this.SalesConcessionType.enumValue != SalesConcessionBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used if SalesConcessionType is Other.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString SalesConcessionTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the SalesConcessionTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SalesConcessionTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool SalesConcessionTypeOtherDescriptionSpecified
        {
            get { return SalesConcessionTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 5)]
        public SALES_CONCESSION_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
