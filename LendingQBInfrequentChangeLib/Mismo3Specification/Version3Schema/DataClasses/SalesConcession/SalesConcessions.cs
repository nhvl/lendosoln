namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class SALES_CONCESSIONS
    {
        /// <summary>
        /// Gets a value indicating whether the SALES_CONCESSIONS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.SalesConcessionSpecified;
            }
        }

        /// <summary>
        /// A collection of sales concessions.
        /// </summary>
        [XmlElement("SALES_CONCESSION", Order = 0)]
		public List<SALES_CONCESSION> SalesConcession = new List<SALES_CONCESSION>();

        /// <summary>
        /// Gets or sets a value indicating whether the SalesConcession element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SalesConcession element has been assigned a value.</value>
        [XmlIgnore]
        public bool SalesConcessionSpecified
        {
            get { return this.SalesConcession != null && this.SalesConcession.Count(s => s != null && s.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public SALES_CONCESSIONS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
