namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class EXEMPTIONS
    {
        /// <summary>
        /// Gets a value indicating whether the EXEMPTIONS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExemptionSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of exemptions.
        /// </summary>
        [XmlElement("EXEMPTION", Order = 0)]
		public List<EXEMPTION> Exemption = new List<EXEMPTION>();

        /// <summary>
        /// Gets or sets a value indicating whether the Exemption element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Exemption element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExemptionSpecified
        {
            get { return this.Exemption != null && this.Exemption.Count(e => e != null && e.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public EXEMPTIONS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
