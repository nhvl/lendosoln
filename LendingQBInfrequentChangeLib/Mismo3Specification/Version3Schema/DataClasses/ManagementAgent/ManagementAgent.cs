namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class MANAGEMENT_AGENT
    {
        /// <summary>
        /// Gets a value indicating whether the MANAGEMENT_AGENT container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                if (Mismo3Utilities.ExceedsThreshold(1,
                    new List<bool> { this.IndividualSpecified, this.LegalEntitySpecified }))
                {
                    throw new System.Xml.Schema.XmlSchemaValidationException(
                        Mismo3Utilities.GetXSDChoiceExceptionMessage(
                        "MANAGEMENT_AGENT",
                        new List<string> { "INDIVIDUAL", "LEGAL_ENTITY" }));
                }

                return this.ExtensionSpecified
                    || this.IndividualSpecified
                    || this.LegalEntitySpecified;
            }
        }

        /// <summary>
        /// The individual representing the management agent.
        /// </summary>
        [XmlElement("INDIVIDUAL", Order = 0)]
        public INDIVIDUAL Individual;

        /// <summary>
        /// Gets or sets a value indicating whether the Individual element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Individual element has been assigned a value.</value>
        [XmlIgnore]
        public bool IndividualSpecified
        {
            get { return this.Individual != null && this.Individual.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The legal entity of the management agent.
        /// </summary>
        [XmlElement("LEGAL_ENTITY", Order = 1)]
        public LEGAL_ENTITY LegalEntity;

        /// <summary>
        /// Gets or sets a value indicating whether the LegalEntity element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LegalEntity element has been assigned a value.</value>
        [XmlIgnore]
        public bool LegalEntitySpecified
        {
            get { return this.LegalEntity != null && this.LegalEntity.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public MANAGEMENT_AGENT_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
