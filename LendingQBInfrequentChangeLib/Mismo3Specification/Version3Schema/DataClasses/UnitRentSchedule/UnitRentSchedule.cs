namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class UNIT_RENT_SCHEDULE
    {
        /// <summary>
        /// Gets a value indicating whether the UNIT_RENT_SCHEDULE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.LeaseExpirationDateSpecified
                    || this.LeaseStartDateSpecified
                    || this.UnitActualRentAmountSpecified
                    || this.UnitFurnishedActualRentAmountSpecified
                    || this.UnitFurnishedMarketRentAmountSpecified
                    || this.UnitMarketRentAmountSpecified
                    || this.UnitUnfurnishedActualRentAmountSpecified
                    || this.UnitUnfurnishedMarketRentAmountSpecified;
            }
        }

        /// <summary>
        /// The date the lease expires.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMODate LeaseExpirationDate;

        /// <summary>
        /// Gets or sets a value indicating whether the LeaseExpirationDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LeaseExpirationDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool LeaseExpirationDateSpecified
        {
            get { return LeaseExpirationDate != null; }
            set { }
        }

        /// <summary>
        /// The date the lease starts.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMODate LeaseStartDate;

        /// <summary>
        /// Gets or sets a value indicating whether the LeaseStartDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LeaseStartDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool LeaseStartDateSpecified
        {
            get { return LeaseStartDate != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of actual rent for a living unit in a multi-unit property.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOAmount UnitActualRentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the UnitActualRentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the UnitActualRentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool UnitActualRentAmountSpecified
        {
            get { return UnitActualRentAmount != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of rent for a furnished living unit in a multi-unit property.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOAmount UnitFurnishedActualRentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the UnitFurnishedActualRentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the UnitFurnishedActualRentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool UnitFurnishedActualRentAmountSpecified
        {
            get { return UnitFurnishedActualRentAmount != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of the market rent for a furnished living unit in a multi-unit property.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOAmount UnitFurnishedMarketRentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the UnitFurnishedMarketRentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the UnitFurnishedMarketRentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool UnitFurnishedMarketRentAmountSpecified
        {
            get { return UnitFurnishedMarketRentAmount != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of the market rent for a living unit in a multi-unit property.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOAmount UnitMarketRentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the UnitMarketRentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the UnitMarketRentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool UnitMarketRentAmountSpecified
        {
            get { return UnitMarketRentAmount != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of rent for an unfurnished living unit in a multi-unit property.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOAmount UnitUnfurnishedActualRentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the UnitUnfurnishedActualRentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the UnitUnfurnishedActualRentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool UnitUnfurnishedActualRentAmountSpecified
        {
            get { return UnitUnfurnishedActualRentAmount != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of the market rent for an unfurnished living unit in a multi-unit property. 
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOAmount UnitUnfurnishedMarketRentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the UnitUnfurnishedMarketRentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the UnitUnfurnishedMarketRentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool UnitUnfurnishedMarketRentAmountSpecified
        {
            get { return UnitUnfurnishedMarketRentAmount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 8)]
        public UNIT_RENT_SCHEDULE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
