namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class UNIT_RENT_SCHEDULES
    {
        /// <summary>
        /// Gets a value indicating whether the UNIT_RENT_SCHEDULES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.UnitRentScheduleSpecified;
            }
        }

        /// <summary>
        /// Container for rent schedules for the unit.
        /// </summary>
        [XmlElement("UNIT_RENT_SCHEDULE", Order = 0)]
		public List<UNIT_RENT_SCHEDULE> UnitRentSchedule = new List<UNIT_RENT_SCHEDULE>();

        /// <summary>
        /// Gets or sets a value indicating whether the UnitRentSchedule element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the UnitRentSchedule element has been assigned a value.</value>
        [XmlIgnore]
        public bool UnitRentScheduleSpecified
        {
            get { return this.UnitRentSchedule != null && this.UnitRentSchedule.Count(u => u != null && u.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public UNIT_RENT_SCHEDULES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
