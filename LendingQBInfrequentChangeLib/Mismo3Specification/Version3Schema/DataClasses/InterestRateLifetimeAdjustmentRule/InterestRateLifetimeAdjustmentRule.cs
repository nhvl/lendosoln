namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class INTEREST_RATE_LIFETIME_ADJUSTMENT_RULE
    {
        /// <summary>
        /// Gets a value indicating whether the INTEREST_RATE_LIFETIME_ADJUSTMENT_RULE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CarryoverRateIndicatorSpecified
                    || this.CeilingRatePercentEarliestEffectiveMonthsCountSpecified
                    || this.CeilingRatePercentSpecified
                    || this.DeferredInterestExclusionIndicatorSpecified
                    || this.ExtensionSpecified
                    || this.FirstRateChangeMonthsCountSpecified
                    || this.FirstRateChangePaymentEffectiveDateSpecified
                    || this.FloorRatePercentSpecified
                    || this.InterestRateAdjustmentCalculationMethodTypeOtherDescriptionSpecified
                    || this.InterestRateAdjustmentCalculationMethodTypeSpecified
                    || this.InterestRateAveragenumValueCountSpecified
                    || this.InterestRateLifetimeAdjustmentCeilingTypeOtherDescriptionSpecified
                    || this.InterestRateLifetimeAdjustmentCeilingTypeSpecified
                    || this.InterestRateLifetimeAdjustmentFloorTypeOtherDescriptionSpecified
                    || this.InterestRateLifetimeAdjustmentFloorTypeSpecified
                    || this.InterestRateRoundingPercentSpecified
                    || this.InterestRateRoundingTypeSpecified
                    || this.InterestRateTruncatedDigitsCountSpecified
                    || this.LoanMarginCalculationMethodTypeSpecified
                    || this.MarginRatePercentSpecified
                    || this.MaximumDecreaseRatePercentSpecified
                    || this.MaximumIncreaseRatePercentSpecified
                    || this.PaymentsBetweenInterestRatenumValuesCountSpecified
                    || this.PaymentsBetweenRateChangesCountSpecified
                    || this.TimelyPaymentRateReductionPercentSpecified;
            }
        }

        /// <summary>
        /// Indicates if the difference between capped and uncapped interest rate is to be carried over and applied to a future interest rate change. If Carryover Rate Indicator is present, any accumulated rate is placed in Carryover Rate Percent.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIndicator CarryoverRateIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the CarryoverRateIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CarryoverRateIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool CarryoverRateIndicatorSpecified
        {
            get { return CarryoverRateIndicator != null; }
            set { }
        }

        /// <summary>
        /// The stated maximum percentage to which the interest rate can increase over the life of the loan.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOPercent CeilingRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the CeilingRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CeilingRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool CeilingRatePercentSpecified
        {
            get { return CeilingRatePercent != null; }
            set { }
        }

        /// <summary>
        /// The minimum number of months at which the maximum interest rate could be reached.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOCount CeilingRatePercentEarliestEffectiveMonthsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the CeilingRatePercentEarliestEffectiveMonthsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CeilingRatePercentEarliestEffectiveMonthsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool CeilingRatePercentEarliestEffectiveMonthsCountSpecified
        {
            get { return CeilingRatePercentEarliestEffectiveMonthsCount != null; }
            set { }
        }

        /// <summary>
        /// Indicates that any accumulated deferred interest is excluded from the principal balance when a new payment is calculated for an ARM loan with negative amortization.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOIndicator DeferredInterestExclusionIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the DeferredInterestExclusionIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DeferredInterestExclusionIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool DeferredInterestExclusionIndicatorSpecified
        {
            get { return DeferredInterestExclusionIndicator != null; }
            set { }
        }

        /// <summary>
        /// The number of months after origination in which the first interest rate adjustment occurs, if the interest rate on the subject loan can change.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOCount FirstRateChangeMonthsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the FirstRateChangeMonthsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FirstRateChangeMonthsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool FirstRateChangeMonthsCountSpecified
        {
            get { return FirstRateChangeMonthsCount != null; }
            set { }
        }

        /// <summary>
        /// The due date of the first payment reflecting the first interest rate change for the loan.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMODate FirstRateChangePaymentEffectiveDate;

        /// <summary>
        /// Gets or sets a value indicating whether the FirstRateChangePaymentEffectiveDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FirstRateChangePaymentEffectiveDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool FirstRateChangePaymentEffectiveDateSpecified
        {
            get { return FirstRateChangePaymentEffectiveDate != null; }
            set { }
        }

        /// <summary>
        /// The stated minimum percentage the interest rate can decrease to over the life of the loan.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOPercent FloorRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the FloorRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FloorRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool FloorRatePercentSpecified
        {
            get { return FloorRatePercent != null; }
            set { }
        }

        /// <summary>
        /// Specifies a method employed to calculate the interest rate of the loan.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOEnum<InterestRateAdjustmentCalculationMethodBase> InterestRateAdjustmentCalculationMethodType;

        /// <summary>
        /// Gets or sets a value indicating whether the InterestRateAdjustmentCalculationMethodType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InterestRateAdjustmentCalculationMethodType element has been assigned a value.</value>
        [XmlIgnore]
        public bool InterestRateAdjustmentCalculationMethodTypeSpecified
        {
            get { return this.InterestRateAdjustmentCalculationMethodType != null && this.InterestRateAdjustmentCalculationMethodType.enumValue != InterestRateAdjustmentCalculationMethodBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Interest Rate Adjustment Calculation Method Type.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOString InterestRateAdjustmentCalculationMethodTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the InterestRateAdjustmentCalculationMethodTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InterestRateAdjustmentCalculationMethodTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool InterestRateAdjustmentCalculationMethodTypeOtherDescriptionSpecified
        {
            get { return InterestRateAdjustmentCalculationMethodTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Value of the interest rate average.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOCount InterestRateAveragenumValueCount;

        /// <summary>
        /// Gets or sets a value indicating whether the InterestRateAverageNumValueCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InterestRateAverageNumValueCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool InterestRateAveragenumValueCountSpecified
        {
            get { return InterestRateAveragenumValueCount != null; }
            set { }
        }

        /// <summary>
        /// Describes if a lifetime ceiling exists for the specified plan and the method of calculating the ceiling. This field used in conjunction with the Maximum Increase Rate.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOEnum<InterestRateLifetimeAdjustmentCeilingBase> InterestRateLifetimeAdjustmentCeilingType;

        /// <summary>
        /// Gets or sets a value indicating whether the InterestRateLifetimeAdjustmentCeilingType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InterestRateLifetimeAdjustmentCeilingType element has been assigned a value.</value>
        [XmlIgnore]
        public bool InterestRateLifetimeAdjustmentCeilingTypeSpecified
        {
            get { return this.InterestRateLifetimeAdjustmentCeilingType != null && this.InterestRateLifetimeAdjustmentCeilingType.enumValue != InterestRateLifetimeAdjustmentCeilingBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Interest Rate Lifetime Adjustment Ceiling Type.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOString InterestRateLifetimeAdjustmentCeilingTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the InterestRateLifetimeAdjustmentCeilingTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InterestRateLifetimeAdjustmentCeilingTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool InterestRateLifetimeAdjustmentCeilingTypeOtherDescriptionSpecified
        {
            get { return InterestRateLifetimeAdjustmentCeilingTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Describes if a lifetime floor exists for the specified plan and the method of calculating the floor. This field used in conjunction with the Maximum Decrease Rate.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOEnum<InterestRateLifetimeAdjustmentFloorBase> InterestRateLifetimeAdjustmentFloorType;

        /// <summary>
        /// Gets or sets a value indicating whether the InterestRateLifetimeAdjustmentFloorType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InterestRateLifetimeAdjustmentFloorType element has been assigned a value.</value>
        [XmlIgnore]
        public bool InterestRateLifetimeAdjustmentFloorTypeSpecified
        {
            get { return this.InterestRateLifetimeAdjustmentFloorType != null && this.InterestRateLifetimeAdjustmentFloorType.enumValue != InterestRateLifetimeAdjustmentFloorBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Interest Rate Lifetime Adjustment Floor Type.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOString InterestRateLifetimeAdjustmentFloorTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the InterestRateLifetimeAdjustmentFloorTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InterestRateLifetimeAdjustmentFloorTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool InterestRateLifetimeAdjustmentFloorTypeOtherDescriptionSpecified
        {
            get { return InterestRateLifetimeAdjustmentFloorTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The percentage to which the interest rate is rounded when a new interest rate is calculated. This field is used in conjunction with Interest Rate Rounding Type which indicates how rounding should occur.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOPercent InterestRateRoundingPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the InterestRateRoundingPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InterestRateRoundingPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool InterestRateRoundingPercentSpecified
        {
            get { return InterestRateRoundingPercent != null; }
            set { }
        }

        /// <summary>
        /// Specifies how the interest rate is rounded when a new interest rate is calculated for an ARM change. The interest rate can be rounded Up, Down or to the Nearest Percent. This field is used in conjunction with Interest Rate Rounding Percent which indicates the percentage to which the rounding occurs.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOEnum<InterestRateRoundingBase> InterestRateRoundingType;

        /// <summary>
        /// Gets or sets a value indicating whether the InterestRateRoundingType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InterestRateRoundingType element has been assigned a value.</value>
        [XmlIgnore]
        public bool InterestRateRoundingTypeSpecified
        {
            get { return this.InterestRateRoundingType != null && this.InterestRateRoundingType.enumValue != InterestRateRoundingBase.Blank; }
            set { }
        }

        /// <summary>
        /// Indicated the number of digits the interest rate should be truncated to.
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMOCount InterestRateTruncatedDigitsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the InterestRateTruncatedDigitsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InterestRateTruncatedDigitsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool InterestRateTruncatedDigitsCountSpecified
        {
            get { return InterestRateTruncatedDigitsCount != null; }
            set { }
        }

        /// <summary>
        /// Describes how to calculate the margin on the loan.
        /// </summary>
        [XmlElement(Order = 17)]
        public MISMOEnum<LoanMarginCalculationMethodBase> LoanMarginCalculationMethodType;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanMarginCalculationMethodType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanMarginCalculationMethodType element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanMarginCalculationMethodTypeSpecified
        {
            get { return this.LoanMarginCalculationMethodType != null && this.LoanMarginCalculationMethodType.enumValue != LoanMarginCalculationMethodBase.Blank; }
            set { }
        }

        /// <summary>
        ///  The number of percentage points to be added to the index to arrive at the new interest rate.
        /// </summary>
        [XmlElement(Order = 18)]
        public MISMOPercent MarginRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the MarginRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MarginRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool MarginRatePercentSpecified
        {
            get { return MarginRatePercent != null; }
            set { }
        }

        /// <summary>
        /// The maximum number of percentage points by which the interest rate can decrease from the original interest rate over the life of the loan.
        /// </summary>
        [XmlElement(Order = 19)]
        public MISMOPercent MaximumDecreaseRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the MaximumDecreaseRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MaximumDecreaseRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool MaximumDecreaseRatePercentSpecified
        {
            get { return MaximumDecreaseRatePercent != null; }
            set { }
        }

        /// <summary>
        /// The maximum number of percentage points by which the interest rate can increase from the original interest rate over the life of the loan.
        /// </summary>
        [XmlElement(Order = 20)]
        public MISMOPercent MaximumIncreaseRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the MaximumIncreaseRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MaximumIncreaseRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool MaximumIncreaseRatePercentSpecified
        {
            get { return MaximumIncreaseRatePercent != null; }
            set { }
        }

        /// <summary>
        /// Value of payments between interest rate.
        /// </summary>
        [XmlElement(Order = 21)]
        public MISMOCount PaymentsBetweenInterestRatenumValuesCount;

        /// <summary>
        /// Gets or sets a value indicating whether the PaymentsBetweenInterestRateNumValuesCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PaymentsBetweenInterestRateNumValuesCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PaymentsBetweenInterestRatenumValuesCountSpecified
        {
            get { return PaymentsBetweenInterestRatenumValuesCount != null; }
            set { }
        }

        /// <summary>
        /// The number of payments between rate changes.
        /// </summary>
        [XmlElement(Order = 22)]
        public MISMOCount PaymentsBetweenRateChangesCount;

        /// <summary>
        /// Gets or sets a value indicating whether the PaymentsBetweenRateChangesCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PaymentsBetweenRateChangesCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PaymentsBetweenRateChangesCountSpecified
        {
            get { return PaymentsBetweenRateChangesCount != null; }
            set { }
        }

        /// <summary>
        /// The percentage amount the interest rate will be reduced if payments are made in a timely manner.
        /// </summary>
        [XmlElement(Order = 23)]
        public MISMOPercent TimelyPaymentRateReductionPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the TimelyPaymentRateReductionPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TimelyPaymentRateReductionPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool TimelyPaymentRateReductionPercentSpecified
        {
            get { return TimelyPaymentRateReductionPercent != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 24)]
        public INTEREST_RATE_LIFETIME_ADJUSTMENT_RULE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
