namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class LATE_CHARGE
    {
        /// <summary>
        /// Gets a value indicating whether the LATE_CHARGE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.LateChargeOccurrencesSpecified
                || this.LateChargeRuleSpecified
                || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A list of late charge occurrences.
        /// </summary>
        [XmlElement("LATE_CHARGE_OCCURRENCES", Order = 0)]
        public LATE_CHARGE_OCCURRENCES LateChargeOccurrences;

        /// <summary>
        /// Gets or sets a value indicating whether the LateChargeOccurrences element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LateChargeOccurrences element has been assigned a value.</value>
        [XmlIgnore]
        public bool LateChargeOccurrencesSpecified
        {
            get { return this.LateChargeOccurrences != null && this.LateChargeOccurrences.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A rule on how a late charge will be handled.
        /// </summary>
        [XmlElement("LATE_CHARGE_RULE", Order = 1)]
        public LATE_CHARGE_RULE LateChargeRule;

        /// <summary>
        /// Gets or sets a value indicating whether the LateChargeRule element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LateChargeRule element has been assigned a value.</value>
        [XmlIgnore]
        public bool LateChargeRuleSpecified
        {
            get { return LateChargeRule != null && LateChargeRule.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public LATE_CHARGE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
