namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class ARREARAGE
    {
        /// <summary>
        /// Gets a value indicating whether the ARREARAGE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ArrearageComponentsSpecified
                    || this.ArrearageSummarySpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// Components of the arrearage.
        /// </summary>
        [XmlElement("ARREARAGE_COMPONENTS", Order = 0)]
        public ARREARAGE_COMPONENTS ArrearageComponents;

        /// <summary>
        /// Gets or sets a value indicating whether the ArrearageComponents element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ArrearageComponents element has been assigned a value.</value>
        [XmlIgnore]
        public bool ArrearageComponentsSpecified
        {
            get { return this.ArrearageComponents != null && this.ArrearageComponents.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Summary of the arrearage.
        /// </summary>
        [XmlElement("ARREARAGE_SUMMARY", Order = 1)]
        public ARREARAGE_SUMMARY ArrearageSummary;

        /// <summary>
        /// Gets or sets a value indicating whether the ArrearageSummary element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ArrearageSummary element has been assigned a value.</value>
        [XmlIgnore]
        public bool ArrearageSummarySpecified
        {
            get { return this.ArrearageSummary != null && this.ArrearageSummary.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public ARREARAGE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
