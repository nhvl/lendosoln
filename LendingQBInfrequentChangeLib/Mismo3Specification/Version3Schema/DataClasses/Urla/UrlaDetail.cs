namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class URLA_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the URLA_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AdditionalBorrowerAssetsConsideredIndicatorSpecified
                    || this.AdditionalBorrowerAssetsNotConsideredIndicatorSpecified
                    || this.AlterationsImprovementsAndRepairsAmountSpecified
                    || this.ApplicationSignedByLoanOriginatorDateSpecified
                    || this.ApplicationTakenMethodTypeSpecified
                    || this.ARMTypeDescriptionSpecified
                    || this.BorrowerPaidDiscountPointsTotalAmountSpecified
                    || this.BorrowerRequestedInterestRatePercentSpecified
                    || this.BorrowerRequestedLoanAmountSpecified
                    || this.EstimatedClosingCostsAmountSpecified
                    || this.ExtensionSpecified
                    || this.LenderRegistrationIdentifierSpecified
                    || this.MIAndFundingFeeFinancedAmountSpecified
                    || this.MIAndFundingFeeTotalAmountSpecified
                    || this.PrepaidItemsEstimatedAmountSpecified
                    || this.PurchasePriceAmountSpecified
                    || this.PurchasePriceNetAmountSpecified
                    || this.RefinanceImprovementCostsAmountSpecified
                    || this.RefinanceImprovementsTypeSpecified
                    || this.RefinanceIncludingDebtsToBePaidOffAmountSpecified
                    || this.RefinanceProposedImprovementsDescriptionSpecified
                    || this.RefundableApplicationFeeIndicatorSpecified
                    || this.RequiredDepositIndicatorSpecified
                    || this.SellerPaidClosingCostsAmountSpecified;
            }
        }

        /// <summary>
        /// The income or assets of a person other than the borrower (including the spouse of the borrower ) will be used as a basis for loan qualification.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIndicator AdditionalBorrowerAssetsConsideredIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the AdditionalBorrowerAssetsConsideredIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AdditionalBorrowerAssetsConsideredIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool AdditionalBorrowerAssetsConsideredIndicatorSpecified
        {
            get { return AdditionalBorrowerAssetsConsideredIndicator != null; }
            set { }
        }

        /// <summary>
        /// The income or assets of the borrowers spouse will not be used as a basis for loan qualification, but his or her liabilities must be considered because the borrower resides in a community property state, the security property is located in a community property state, or the borrower is relying on other property located in a community property state as a basis for repayment of the loan.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOIndicator AdditionalBorrowerAssetsNotConsideredIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the AdditionalBorrowerAssetsNotConsideredIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AdditionalBorrowerAssetsNotConsideredIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool AdditionalBorrowerAssetsNotConsideredIndicatorSpecified
        {
            get { return AdditionalBorrowerAssetsNotConsideredIndicator != null; }
            set { }
        }

        /// <summary>
        /// The cost of any alterations, improvements, repairs and rehabilitation to be made on the subject property. Collected on the URLA in Section VII line b.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOAmount AlterationsImprovementsAndRepairsAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the AlterationsImprovementsAndRepairsAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AlterationsImprovementsAndRepairsAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool AlterationsImprovementsAndRepairsAmountSpecified
        {
            get { return AlterationsImprovementsAndRepairsAmount != null; }
            set { }
        }

        /// <summary>
        /// The date on which the Loan Originator obtained HMDA information and signed the loan application. Collected on the URLA in Section X (Interviewers Signature Date).
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMODate ApplicationSignedByLoanOriginatorDate;

        /// <summary>
        /// Gets or sets a value indicating whether the ApplicationSignedByLoanOriginatorDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ApplicationSignedByLoanOriginatorDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ApplicationSignedByLoanOriginatorDateSpecified
        {
            get { return ApplicationSignedByLoanOriginatorDate != null; }
            set { }
        }

        /// <summary>
        ///  Specifies the method used to take the application as determined by the Loan Originator. Collected on the URLA in Section X (This information was provided:).
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<ApplicationTakenMethodBase> ApplicationTakenMethodType;

        /// <summary>
        /// Gets or sets a value indicating whether the ApplicationTakenMethodType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ApplicationTakenMethodType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ApplicationTakenMethodTypeSpecified
        {
            get { return this.ApplicationTakenMethodType != null && this.ApplicationTakenMethodType.enumValue != ApplicationTakenMethodBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information or a description of the ARM program. Collected on the URLA in Section I (Amortization Type:ARM).
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString ARMTypeDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ARMTypeDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ARMTypeDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ARMTypeDescriptionSpecified
        {
            get { return ARMTypeDescription != null; }
            set { }
        }

        /// <summary>
        /// The total dollar amount of discount points that are paid by the borrower. Collected on the URLA in Section VII line h.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOAmount BorrowerPaidDiscountPointsTotalAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the BorrowerPaidDiscountPointsTotalAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BorrowerPaidDiscountPointsTotalAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool BorrowerPaidDiscountPointsTotalAmountSpecified
        {
            get { return BorrowerPaidDiscountPointsTotalAmount != null; }
            set { }
        }

        /// <summary>
        /// The interest rate that is being requested on the loan application. Collected on the URLA in Section I (Interest Rate).
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOPercent BorrowerRequestedInterestRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the BorrowerRequestedInterestRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BorrowerRequestedInterestRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool BorrowerRequestedInterestRatePercentSpecified
        {
            get { return BorrowerRequestedInterestRatePercent != null; }
            set { }
        }

        /// <summary>
        /// The total dollar amount of the mortgage note being originated that is being requested by the borrower. This amount may include financed PMI, MIP and Funding Fees. It is collected on the URLA in Section I (Amount) and in Section VII line o.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOAmount BorrowerRequestedLoanAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the BorrowerRequestedLoanAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BorrowerRequestedLoanAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool BorrowerRequestedLoanAmountSpecified
        {
            get { return BorrowerRequestedLoanAmount != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of the estimated loan fees, title fees, appraisal fees and other closing costs associated with the subject transaction excluding discount points. Collected on the URLA in Section VII line f.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOAmount EstimatedClosingCostsAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the EstimatedClosingCostsAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EstimatedClosingCostsAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool EstimatedClosingCostsAmountSpecified
        {
            get { return EstimatedClosingCostsAmount != null; }
            set { }
        }

        /// <summary>
        /// A tracking number assigned by the wholesale lender to identify the loan for pipeline management.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOIdentifier LenderRegistrationIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the LenderRegistrationIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LenderRegistrationIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool LenderRegistrationIdentifierSpecified
        {
            get { return LenderRegistrationIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of private mortgage insurance premium, FHA up-front MI Premium or VA funding fee that is rolled into loan amount. Used if PMI / MIP / VA funding fee will be added to loan amount. Collected on the URLA in Section VII line n.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOAmount MIAndFundingFeeFinancedAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the MIAndFundingFeeFinancedAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIAndFundingFeeFinancedAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIAndFundingFeeFinancedAmountSpecified
        {
            get { return MIAndFundingFeeFinancedAmount != null; }
            set { }
        }

        /// <summary>
        /// The total amount paid to initiate private mortgage insurance, FHA up-front MI premium or the VA funding fee. Collected on the URLA in Section VII line g.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOAmount MIAndFundingFeeTotalAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the MIAndFundingFeeTotalAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIAndFundingFeeTotalAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIAndFundingFeeTotalAmountSpecified
        {
            get { return MIAndFundingFeeTotalAmount != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of escrow / impound to be held by the lender, which is to be collected at closing for future payment of taxes, insurance or other obligations on the subject property. Collected on the URLA in Section VII line e.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOAmount PrepaidItemsEstimatedAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaidItemsEstimatedAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaidItemsEstimatedAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaidItemsEstimatedAmountSpecified
        {
            get { return PrepaidItemsEstimatedAmount != null; }
            set { }
        }

        /// <summary>
        /// The total dollar amount paid by the borrower for the property. The purchase price is presented on the offer to purchase.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOAmount PurchasePriceAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PurchasePriceAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PurchasePriceAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PurchasePriceAmountSpecified
        {
            get { return PurchasePriceAmount != null; }
            set { }
        }

        /// <summary>
        /// The result of subtracting financing and sales concession amounts above those allowed by guidelines from the Purchase Price Amount of the subject property. 
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOAmount PurchasePriceNetAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PurchasePriceNetAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PurchasePriceNetAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PurchasePriceNetAmountSpecified
        {
            get { return PurchasePriceNetAmount != null; }
            set { }
        }

        /// <summary>
        /// The total dollar amount of improvements either made or to be made at time of refinance. Collected on the URLA in Section II Describe Improvements Costs.
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMOAmount RefinanceImprovementCostsAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the RefinanceImprovementCostsAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RefinanceImprovementCostsAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool RefinanceImprovementCostsAmountSpecified
        {
            get { return RefinanceImprovementCostsAmount != null; }
            set { }
        }

        /// <summary>
        /// Specifies whether the improvements associated with a refinance have been made to the property or if they are planned. Collected on the URLA in Section II (Refinance - Describe Improvements - Check Box.).
        /// </summary>
        [XmlElement(Order = 17)]
        public MISMOEnum<RefinanceImprovementsBase> RefinanceImprovementsType;

        /// <summary>
        /// Gets or sets a value indicating whether the RefinanceImprovementsType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RefinanceImprovementsType element has been assigned a value.</value>
        [XmlIgnore]
        public bool RefinanceImprovementsTypeSpecified
        {
            get { return this.RefinanceImprovementsType != null && this.RefinanceImprovementsType.enumValue != RefinanceImprovementsBase.Blank; }
            set { }
        }

        /// <summary>
        /// The total amount of all debt to be repaid with funds from refinancing the mortgage loan on the subject property. Collected on the URLA in Section VII line d.
        /// </summary>
        [XmlElement(Order = 18)]
        public MISMOAmount RefinanceIncludingDebtsToBePaidOffAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the RefinanceIncludingDebtsToBePaidOffAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RefinanceIncludingDebtsToBePaidOffAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool RefinanceIncludingDebtsToBePaidOffAmountSpecified
        {
            get { return RefinanceIncludingDebtsToBePaidOffAmount != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture additional information text entries on the URLA associated with the improvements to be made on property being refinanced. Collected on the URLA in Section II (Refinance - Describe Improvements To Be Made).
        /// </summary>
        [XmlElement(Order = 19)]
        public MISMOString RefinanceProposedImprovementsDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the RefinanceProposedImprovementsDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RefinanceProposedImprovementsDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool RefinanceProposedImprovementsDescriptionSpecified
        {
            get { return RefinanceProposedImprovementsDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether the Application Fee is refundable.
        /// </summary>
        [XmlElement(Order = 20)]
        public MISMOIndicator RefundableApplicationFeeIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the RefundableApplicationFeeIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RefundableApplicationFeeIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool RefundableApplicationFeeIndicatorSpecified
        {
            get { return RefundableApplicationFeeIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether a deposit balance is required to be maintained at an account at the lending institution as a condition of the loan. This does not affect the Annual Percentage Rate or any fees. This is disclosed on the TIL Disclosure document.
        /// </summary>
        [XmlElement(Order = 21)]
        public MISMOIndicator RequiredDepositIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the RequiredDepositIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RequiredDepositIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool RequiredDepositIndicatorSpecified
        {
            get { return RequiredDepositIndicator != null; }
            set { }
        }

        /// <summary>
        /// The estimated closing costs that the seller of the subject property will pay. Collected on the URLA in Section VII line k.
        /// </summary>
        [XmlElement(Order = 22)]
        public MISMOAmount SellerPaidClosingCostsAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the SellerPaidClosingCostsAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SellerPaidClosingCostsAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool SellerPaidClosingCostsAmountSpecified
        {
            get { return SellerPaidClosingCostsAmount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 23)]
        public URLA_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null ? Extension.ShouldSerialize : false; }
            set { }
        }
    }
}
