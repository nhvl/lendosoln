namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class URLA
    {
        /// <summary>
        /// Gets a value indicating whether the URLA container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.UrlaDetailSpecified
                    || this.UrlaTotalHousingExpensesSpecified
                    || this.UrlaTotalSpecified;
            }
        }

        /// <summary>
        /// Details on the URLA.
        /// </summary>
        [XmlElement("URLA_DETAIL", Order = 0)]
        public URLA_DETAIL UrlaDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the URLADetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the URLADetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool UrlaDetailSpecified
        {
            get { return this.UrlaDetail != null && this.UrlaDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The URLA total.
        /// </summary>
        [XmlElement("URLA_TOTAL", Order = 1)]
        public URLA_TOTAL UrlaTotal;

        /// <summary>
        /// Gets or sets a value indicating whether the URLATotal element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the URLATotal element has been assigned a value.</value>
        [XmlIgnore]
        public bool UrlaTotalSpecified
        {
            get { return this.UrlaTotal != null && this.UrlaTotal.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Total housing expenses on the URLA.
        /// </summary>
        [XmlElement("URLA_TOTAL_HOUSING_EXPENSES", Order = 2)]
        public URLA_TOTAL_HOUSING_EXPENSES UrlaTotalHousingExpenses;

        /// <summary>
        /// Gets or sets a value indicating whether the URLATotalHousingExpenses element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the URLATotalHousingExpenses element has been assigned a value.</value>
        [XmlIgnore]
        public bool UrlaTotalHousingExpensesSpecified
        {
            get { return this.UrlaTotalHousingExpenses != null && this.UrlaTotalHousingExpenses.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public URLA_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
