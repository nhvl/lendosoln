namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class SERVICER
    {
        /// <summary>
        /// Gets a value indicating whether the SERVICER container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ServicerProgramIdentifierSpecified
                    || this.ServicerRoleEffectiveDateSpecified
                    || this.ServicerTypeOtherDescriptionSpecified
                    || this.ServicerTypeSpecified
                    || this.ServicingTransferEffectiveDateSpecified
                    || this.ServicingTransferRoleTypeSpecified
                    || this.SubservicerRightsTypeOtherDescriptionSpecified
                    || this.SubservicerRightsTypeSpecified
                    || this.TransferOfServicingDisclosureTypeOtherDescriptionSpecified
                    || this.TransferOfServicingDisclosureTypeSpecified;
            }
        }

        /// <summary>
        /// A unique identifier of the program in which the servicer is eligible to participate. The party assigning the identifier can be provided using the IdentifierOwnerURI.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIdentifier ServicerProgramIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the ServicerProgramIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServicerProgramIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServicerProgramIdentifierSpecified
        {
            get { return ServicerProgramIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The date on which the servicer role type became active.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMODate ServicerRoleEffectiveDate;

        /// <summary>
        /// Gets or sets a value indicating whether the ServicerRoleEffectiveDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServicerRoleEffectiveDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServicerRoleEffectiveDateSpecified
        {
            get { return ServicerRoleEffectiveDate != null; }
            set { }
        }

        /// <summary>
        /// Specifies a type of Servicer.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<ServicerBase> ServicerType;

        /// <summary>
        /// Gets or sets a value indicating whether the ServicerType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServicerType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServicerTypeSpecified
        {
            get { return this.ServicerType != null && this.ServicerType.enumValue != ServicerBase.Blank; }
            set { }
        }

        /// <summary>
        /// When Other is specified in Servicer Type, this data element contains the description.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString ServicerTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ServicerTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServicerTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServicerTypeOtherDescriptionSpecified
        {
            get { return ServicerTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The date at which the transfer of servicing is effective as reflected on the Notice Of Assignment, Sale, Or Transfer of Servicing Rights document.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMODate ServicingTransferEffectiveDate;

        /// <summary>
        /// Gets or sets a value indicating whether the ServicingTransferEffectiveDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServicingTransferEffectiveDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServicingTransferEffectiveDateSpecified
        {
            get { return ServicingTransferEffectiveDate != null; }
            set { }
        }

        /// <summary>
        /// The role of this servicer in a servicing transfer transaction.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOEnum<ServicingTransferRoleBase> ServicingTransferRoleType;

        /// <summary>
        /// Gets or sets a value indicating whether the ServicingTransferRoleType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServicingTransferRoleType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServicingTransferRoleTypeSpecified
        {
            get { return this.ServicingTransferRoleType != null && this.ServicingTransferRoleType.enumValue != ServicingTransferRoleBase.Blank; }
            set { }
        }

        /// <summary>
        /// Specifies the servicing rights that a sub servicer holds.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOEnum<SubservicerRightsBase> SubservicerRightsType;

        /// <summary>
        /// Gets or sets a value indicating whether the SubServicerRightsType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SubServicerRightsType element has been assigned a value.</value>
        [XmlIgnore]
        public bool SubservicerRightsTypeSpecified
        {
            get { return this.SubservicerRightsType != null && this.SubservicerRightsType.enumValue != SubservicerRightsBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected as the Sub servicer Rights Type.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOString SubservicerRightsTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the SubServicerRightsTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SubServicerRightsTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool SubservicerRightsTypeOtherDescriptionSpecified
        {
            get { return SubservicerRightsTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies detail about the party servicing the loan as stated on the Servicing Disclosure Statement document.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOEnum<TransferOfServicingDisclosureBase> TransferOfServicingDisclosureType;

        /// <summary>
        /// Gets or sets a value indicating whether the TransferOfServicingDisclosureType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TransferOfServicingDisclosureType element has been assigned a value.</value>
        [XmlIgnore]
        public bool TransferOfServicingDisclosureTypeSpecified
        {
            get { return this.TransferOfServicingDisclosureType != null && this.TransferOfServicingDisclosureType.enumValue != TransferOfServicingDisclosureBase.Blank; }
            set { }
        }

        /// <summary>
        /// When Other is specified in Transfer Of Servicing Disclosure Type, this data element contains the description.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOString TransferOfServicingDisclosureTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the TransferOfServicingDisclosureTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TransferOfServicingDisclosureTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool TransferOfServicingDisclosureTypeOtherDescriptionSpecified
        {
            get { return TransferOfServicingDisclosureTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 10)]
        public SERVICER_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
