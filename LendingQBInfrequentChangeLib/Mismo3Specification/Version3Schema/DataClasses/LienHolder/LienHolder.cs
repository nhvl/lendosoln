namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class LIEN_HOLDER
    {
        /// <summary>
        /// Gets a value indicating whether the LIEN_HOLDER container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.LienHolderTypeOtherDescriptionSpecified
                    || this.LienHolderTypeSpecified;
            }
        }

        /// <summary>
        /// Describes the holder of the lien.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<LienHolderBase> LienHolderType;

        /// <summary>
        /// Gets or sets a value indicating whether the LienHolderType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LienHolderType element has been assigned a value.</value>
        [XmlIgnore]
        public bool LienHolderTypeSpecified
        {
            get { return this.LienHolderType != null && this.LienHolderType.enumValue != LienHolderBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Lien Holder Type.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString LienHolderTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the LienHolderTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LienHolderTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool LienHolderTypeOtherDescriptionSpecified
        {
            get { return LienHolderTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public LIEN_HOLDER_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
