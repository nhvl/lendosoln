namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class PRINCIPAL_AND_INTEREST_ADJUSTMENT_LIMITED_PAYMENT_OPTION
    {
        /// <summary>
        /// Gets a value indicating whether the PRINCIPAL_AND_INTEREST_ADJUSTMENT_LIMITED_PAYMENT_OPTION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.LimitedPrincipalAndInterestPaymentActivationAmountSpecified
                    || this.LimitedPrincipalAndInterestPaymentActivationIndicatorSpecified
                    || this.LimitedPrincipalAndInterestPaymentActivationPercentSpecified
                    || this.LimitedPrincipalAndInterestPaymentEffectiveDateSpecified
                    || this.LimitedPrincipalAndInterestPaymentOptionDescriptionSpecified
                    || this.LimitedPrincipalAndInterestPaymentPeriodEndDateSpecified
                    || this.LimitedPrincipalAndInterestPaymentPullTheNoteIndicatorSpecified;
            }
        }

        /// <summary>
        /// The minimum dollar increase between two successive calculated principal and interest payments that requires an optional principal and interest payment to be offered.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount LimitedPrincipalAndInterestPaymentActivationAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the LimitedPrincipalAndInterestPaymentActivationAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LimitedPrincipalAndInterestPaymentActivationAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool LimitedPrincipalAndInterestPaymentActivationAmountSpecified
        {
            get { return LimitedPrincipalAndInterestPaymentActivationAmount != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether a limited principal and interest payment should only be calculated if the new Interest rate is greater than the original interest rate. If no, the limited principal and interest payment is always calculated.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOIndicator LimitedPrincipalAndInterestPaymentActivationIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the LimitedPrincipalAndInterestPaymentActivationIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LimitedPrincipalAndInterestPaymentActivationIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool LimitedPrincipalAndInterestPaymentActivationIndicatorSpecified
        {
            get { return LimitedPrincipalAndInterestPaymentActivationIndicator != null; }
            set { }
        }

        /// <summary>
        /// The minimum percent increase between two successive calculated principal and interest payments that requires an optional principal and interest payment to be offered.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOPercent LimitedPrincipalAndInterestPaymentActivationPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the LimitedPrincipalAndInterestPaymentActivationPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LimitedPrincipalAndInterestPaymentActivationPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool LimitedPrincipalAndInterestPaymentActivationPercentSpecified
        {
            get { return LimitedPrincipalAndInterestPaymentActivationPercent != null; }
            set { }
        }

        /// <summary>
        /// The payment due date on which the limited principal and interest payment option was elected.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMODate LimitedPrincipalAndInterestPaymentEffectiveDate;

        /// <summary>
        /// Gets or sets a value indicating whether the LimitedPrincipalAndInterestPaymentEffectiveDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LimitedPrincipalAndInterestPaymentEffectiveDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool LimitedPrincipalAndInterestPaymentEffectiveDateSpecified
        {
            get { return LimitedPrincipalAndInterestPaymentEffectiveDate != null; }
            set { }
        }

        /// <summary>
        /// The description of the limited principal and interest payment option.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString LimitedPrincipalAndInterestPaymentOptionDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the LimitedPrincipalAndInterestPaymentOptionDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LimitedPrincipalAndInterestPaymentOptionDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool LimitedPrincipalAndInterestPaymentOptionDescriptionSpecified
        {
            get { return LimitedPrincipalAndInterestPaymentOptionDescription != null; }
            set { }
        }

        /// <summary>
        /// The date on which the limited principal and interest payment option period ends.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMODate LimitedPrincipalAndInterestPaymentPeriodEndDate;

        /// <summary>
        /// Gets or sets a value indicating whether the LimitedPrincipalAndInterestPaymentPeriodEndDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LimitedPrincipalAndInterestPaymentPeriodEndDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool LimitedPrincipalAndInterestPaymentPeriodEndDateSpecified
        {
            get { return LimitedPrincipalAndInterestPaymentPeriodEndDate != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the mortgage note is of an exceptional nature and requires manual review.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOIndicator LimitedPrincipalAndInterestPaymentPullTheNoteIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the LimitedPrincipalAndInterestPaymentPullTheNoteIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LimitedPrincipalAndInterestPaymentPullTheNoteIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool LimitedPrincipalAndInterestPaymentPullTheNoteIndicatorSpecified
        {
            get { return LimitedPrincipalAndInterestPaymentPullTheNoteIndicator != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 7)]
        public PRINCIPAL_AND_INTEREST_ADJUSTMENT_LIMITED_PAYMENT_OPTION_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
