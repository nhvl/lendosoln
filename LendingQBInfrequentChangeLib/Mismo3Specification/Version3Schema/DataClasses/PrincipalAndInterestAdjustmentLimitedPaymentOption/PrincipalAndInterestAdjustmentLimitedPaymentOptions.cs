namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PRINCIPAL_AND_INTEREST_ADJUSTMENT_LIMITED_PAYMENT_OPTIONS
    {
        /// <summary>
        /// Gets a value indicating whether the PRINCIPAL_AND_INTEREST_ADJUSTMENT_LIMITED_PAYMENT_OPTIONS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.PrincipalAndInterestAdjustmentLimitedPaymentOptionSpecified;
            }
        }

        /// <summary>
        /// A collection of principal and interest adjustment limited payment options.
        /// </summary>
        [XmlElement("PRINCIPAL_AND_INTEREST_ADJUSTMENT_LIMITED_PAYMENT_OPTION", Order = 0)]
		public List<PRINCIPAL_AND_INTEREST_ADJUSTMENT_LIMITED_PAYMENT_OPTION> PrincipalAndInterestAdjustmentLimitedPaymentOption = new List<PRINCIPAL_AND_INTEREST_ADJUSTMENT_LIMITED_PAYMENT_OPTION>();

        /// <summary>
        /// Gets or sets a value indicating whether the PrincipalAndInterestAdjustmentLimitedPaymentOption element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrincipalAndInterestAdjustmentLimitedPaymentOption element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrincipalAndInterestAdjustmentLimitedPaymentOptionSpecified
        {
            get { return this.PrincipalAndInterestAdjustmentLimitedPaymentOption != null && this.PrincipalAndInterestAdjustmentLimitedPaymentOption.Count(p => p != null && p.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public PRINCIPAL_AND_INTEREST_ADJUSTMENT_LIMITED_PAYMENT_OPTIONS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
