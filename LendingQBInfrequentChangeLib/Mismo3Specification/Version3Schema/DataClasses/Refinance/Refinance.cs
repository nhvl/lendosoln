namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class REFINANCE
    {
        /// <summary>
        /// Gets a value indicating whether the REFINANCE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CurrentFirstMortgageHolderTypeOtherDescriptionSpecified
                    || this.CurrentFirstMortgageHolderTypeSpecified
                    || this.EstimatedCashOutAmountSpecified
                    || this.ExtensionSpecified
                    || this.NonStructuralAlterationsConventionalAmountSpecified
                    || this.RefinanceAssetsAcquiredAmountSpecified
                    || this.RefinanceCashOutAmountSpecified
                    || this.RefinanceCashOutDeterminationTypeSpecified
                    || this.RefinanceCashOutPercentSpecified
                    || this.RefinanceDebtReductionAmountSpecified
                    || this.RefinanceHomeImprovementAmountSpecified
                    || this.RefinancePrimaryPurposeTypeOtherDescriptionSpecified
                    || this.RefinancePrimaryPurposeTypeSpecified
                    || this.RefinanceSameLenderIndicatorSpecified
                    || this.SecondaryFinancingRefinanceIndicatorSpecified
                    || this.StructuralAlterationsConventionalAmountSpecified
                    || this.TotalPriorLienPayoffAmountSpecified
                    || this.TotalPriorLienPrepaymentPenaltyAmountSpecified;
            }
        }

        /// <summary>
        /// Specifies the investor holding the first mortgage that is being refinanced.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<CurrentFirstMortgageHolderBase> CurrentFirstMortgageHolderType;

        /// <summary>
        /// Gets or sets a value indicating whether the CurrentFirstMortgageHolderType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CurrentFirstMortgageHolderType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CurrentFirstMortgageHolderTypeSpecified
        {
            get { return this.CurrentFirstMortgageHolderType != null && this.CurrentFirstMortgageHolderType.enumValue != CurrentFirstMortgageHolderBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Current First Mortgage Holder Type.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString CurrentFirstMortgageHolderTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the CurrentFirstMortgageHolderTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CurrentFirstMortgageHolderTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool CurrentFirstMortgageHolderTypeOtherDescriptionSpecified
        {
            get { return CurrentFirstMortgageHolderTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The estimated amount of cash that the borrower may receive at closing based on a calculation agreed to by the parties to the transaction.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOAmount EstimatedCashOutAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the EstimatedCashOutAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EstimatedCashOutAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool EstimatedCashOutAmountSpecified
        {
            get { return EstimatedCashOutAmount != null; }
            set { }
        }

        /// <summary>
        /// The amount of non-structural alterations, improvements and repairs.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOAmount NonStructuralAlterationsConventionalAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the NonStructuralAlterationsConventionalAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NonStructuralAlterationsConventionalAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool NonStructuralAlterationsConventionalAmountSpecified
        {
            get { return NonStructuralAlterationsConventionalAmount != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount applied to assets acquired with the proceeds from a refinance transaction.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOAmount RefinanceAssetsAcquiredAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the RefinanceAssetsAcquiredAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RefinanceAssetsAcquiredAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool RefinanceAssetsAcquiredAmountSpecified
        {
            get { return RefinanceAssetsAcquiredAmount != null; }
            set { }
        }

        /// <summary>
        /// The amount of cash the borrower will receive at the closing of the loan on a refinance transaction.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOAmount RefinanceCashOutAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the RefinanceCashOutAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RefinanceCashOutAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool RefinanceCashOutAmountSpecified
        {
            get { return RefinanceCashOutAmount != null; }
            set { }
        }

        /// <summary>
        /// Specifies how the lender has classified a refinanced loan.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOEnum<RefinanceCashOutDeterminationBase> RefinanceCashOutDeterminationType;

        /// <summary>
        /// Gets or sets a value indicating whether the RefinanceCashOutDeterminationType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RefinanceCashOutDeterminationType element has been assigned a value.</value>
        [XmlIgnore]
        public bool RefinanceCashOutDeterminationTypeSpecified
        {
            get { return this.RefinanceCashOutDeterminationType != null && this.RefinanceCashOutDeterminationType.enumValue != RefinanceCashOutDeterminationBase.Blank; }
            set { }
        }

        /// <summary>
        /// Percentage of Base Loan Amount that will be dispersed as cash to the borrower.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOPercent RefinanceCashOutPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the RefinanceCashOutPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RefinanceCashOutPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool RefinanceCashOutPercentSpecified
        {
            get { return RefinanceCashOutPercent != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount applied to debt reduction from the proceeds from refinance transaction.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOAmount RefinanceDebtReductionAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the RefinanceDebtReductionAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RefinanceDebtReductionAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool RefinanceDebtReductionAmountSpecified
        {
            get { return RefinanceDebtReductionAmount != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount applied to home improvements from the proceeds from a refinance transaction. 
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOAmount RefinanceHomeImprovementAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the RefinanceHomeImprovementAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RefinanceHomeImprovementAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool RefinanceHomeImprovementAmountSpecified
        {
            get { return RefinanceHomeImprovementAmount != null; }
            set { }
        }

        /// <summary>
        /// Specifies the primary purpose of the refinance.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOEnum<RefinancePrimaryPurposeBase> RefinancePrimaryPurposeType;

        /// <summary>
        /// Gets or sets a value indicating whether the RefinancePrimaryPurposeType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RefinancePrimaryPurposeType element has been assigned a value.</value>
        [XmlIgnore]
        public bool RefinancePrimaryPurposeTypeSpecified
        {
            get { return this.RefinancePrimaryPurposeType != null && this.RefinancePrimaryPurposeType.enumValue != RefinancePrimaryPurposeBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Refinance Primary Purpose Type.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOString RefinancePrimaryPurposeTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the RefinancePrimaryPurposeTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RefinancePrimaryPurposeTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool RefinancePrimaryPurposeTypeOtherDescriptionSpecified
        {
            get { return RefinancePrimaryPurposeTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that the subject loan is refinanced with the current holder, current servicer or an affiliate of either.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOIndicator RefinanceSameLenderIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the RefinanceSameLenderIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RefinanceSameLenderIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool RefinanceSameLenderIndicatorSpecified
        {
            get { return RefinanceSameLenderIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether a subordinate mortgage will be paid off by the refinance and rolled into the new first mortgage balance. Used for refinances.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOIndicator SecondaryFinancingRefinanceIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the SecondaryFinancingRefinanceIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SecondaryFinancingRefinanceIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool SecondaryFinancingRefinanceIndicatorSpecified
        {
            get { return SecondaryFinancingRefinanceIndicator != null; }
            set { }
        }

        /// <summary>
        /// The amount of structural alterations, improvements, and repairs.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOAmount StructuralAlterationsConventionalAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the StructuralAlterationsConventionalAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the StructuralAlterationsConventionalAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool StructuralAlterationsConventionalAmountSpecified
        {
            get { return StructuralAlterationsConventionalAmount != null; }
            set { }
        }

        /// <summary>
        /// The total dollar amount of the refinance proceeds that is applied to liens on the subject property. 
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOAmount TotalPriorLienPayoffAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the TotalPriorLienPayoffAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TotalPriorLienPayoffAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TotalPriorLienPayoffAmountSpecified
        {
            get { return TotalPriorLienPayoffAmount != null; }
            set { }
        }

        /// <summary>
        /// The total dollar amount of the prepayment penalty  that is incurred for all liens being paid off as a result of the refinance.
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMOAmount TotalPriorLienPrepaymentPenaltyAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the TotalPriorLienPrepaymentPenaltyAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TotalPriorLienPrepaymentPenaltyAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TotalPriorLienPrepaymentPenaltyAmountSpecified
        {
            get { return TotalPriorLienPrepaymentPenaltyAmount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 17)]
        public REFINANCE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
