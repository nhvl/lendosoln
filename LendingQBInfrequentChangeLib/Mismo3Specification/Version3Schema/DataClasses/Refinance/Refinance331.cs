namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class REFINANCE_3_3_1
    {
        /// <summary>
        /// Gets a value indicating whether the REFINANCE_3_3_1 container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.StateRefinanceProgramTypeOtherDescriptionSpecified
                    || this.StateRefinanceProgramTypeSpecified;
            }
        }

        /// <summary>
        /// A collection of values that specify a state program under which a refinance transaction is executed.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<StateRefinanceProgramBase> StateRefinanceProgramType;

        /// <summary>
        /// Gets or sets a value indicating whether the StateRefinanceProgramType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the StateRefinanceProgramType element has been assigned a value.</value>
        [XmlIgnore]
        public bool StateRefinanceProgramTypeSpecified
        {
            get { return this.StateRefinanceProgramType != null && this.StateRefinanceProgramType.enumValue != StateRefinanceProgramBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field that specifies the enumeration when the value of Other is used for the State Refinance Program Type.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString StateRefinanceProgramTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the StateRefinanceProgramTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the StateRefinanceProgramTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool StateRefinanceProgramTypeOtherDescriptionSpecified
        {
            get { return StateRefinanceProgramTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public REFINANCE_3_3_1_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
