namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class INTEREST_RATE_ADJUSTMENT
    {
        /// <summary>
        /// Gets a value indicating whether the INTEREST_RATE_ADJUSTMENT container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.IndexRulesSpecified
                    || this.InterestRateLifetimeAdjustmentRuleSpecified
                    || this.InterestRatePerChangeAdjustmentRulesSpecified
                    || this.InterestRatePeriodicAdjustmentRulesSpecified;
            }
        }

        /// <summary>
        /// Rules related to the index.
        /// </summary>
        [XmlElement("INDEX_RULES", Order = 0)]
        public INDEX_RULES IndexRules;

        /// <summary>
        /// Gets or sets a value indicating whether the IndexRules element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IndexRules element has been assigned a value.</value>
        [XmlIgnore]
        public bool IndexRulesSpecified
        {
            get { return this.IndexRules != null && this.IndexRules.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Lifetime adjustment rules for the interest rate.
        /// </summary>
        [XmlElement("INTEREST_RATE_LIFETIME_ADJUSTMENT_RULE", Order = 1)]
        public INTEREST_RATE_LIFETIME_ADJUSTMENT_RULE InterestRateLifetimeAdjustmentRule;

        /// <summary>
        /// Gets or sets a value indicating whether the InterestRateLifetimeAdjustmentRule element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InterestRateLifetimeAdjustmentRule element has been assigned a value.</value>
        [XmlIgnore]
        public bool InterestRateLifetimeAdjustmentRuleSpecified
        {
            get { return this.InterestRateLifetimeAdjustmentRule != null && this.InterestRateLifetimeAdjustmentRule.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Rules per change of the interest rate.
        /// </summary>
        [XmlElement("INTEREST_RATE_PER_CHANGE_ADJUSTMENT_RULES", Order = 2)]
        public INTEREST_RATE_PER_CHANGE_ADJUSTMENT_RULES InterestRatePerChangeAdjustmentRules;

        /// <summary>
        /// Gets or sets a value indicating whether the InterestRatePerChangeAdjustmentRules element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InterestRatePerChangeAdjustmentRules element has been assigned a value.</value>
        [XmlIgnore]
        public bool InterestRatePerChangeAdjustmentRulesSpecified
        {
            get { return this.InterestRatePerChangeAdjustmentRules != null && this.InterestRatePerChangeAdjustmentRules.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Rules for the periodic adjustment of the interest rate.
        /// </summary>
        [XmlElement("INTEREST_RATE_PERIODIC_ADJUSTMENT_RULES", Order = 3)]
        public INTEREST_RATE_PERIODIC_ADJUSTMENT_RULES InterestRatePeriodicAdjustmentRules;

        /// <summary>
        /// Gets or sets a value indicating whether the InterestRatePeriodicAdjustmentRules element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InterestRatePeriodicAdjustmentRules element has been assigned a value.</value>
        [XmlIgnore]
        public bool InterestRatePeriodicAdjustmentRulesSpecified
        {
            get { return this.InterestRatePeriodicAdjustmentRules != null && this.InterestRatePeriodicAdjustmentRules.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public INTEREST_RATE_ADJUSTMENT_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
