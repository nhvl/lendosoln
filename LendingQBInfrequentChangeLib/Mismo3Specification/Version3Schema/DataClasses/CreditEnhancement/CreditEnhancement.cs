namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class CREDIT_ENHANCEMENT
    {
        /// <summary>
        /// Gets a value indicating whether the CREDIT_ENHANCEMENT container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditEnhancementEffectiveDateSpecified
                    || this.CreditEnhancementEffectivePeriodTypeSpecified
                    || this.CreditEnhancementExpirationDateSpecified
                    || this.CreditEnhancementPartyRoleTypeOtherDescriptionSpecified
                    || this.CreditEnhancementPartyRoleTypeSpecified
                    || this.CreditEnhancementPeriodBasedIndicatorSpecified
                    || this.CreditEnhancementTypeOtherDescriptionSpecified
                    || this.CreditEnhancementTypeSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// The date that the selected credit enhancement becomes effective.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMODate CreditEnhancementEffectiveDate;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditEnhancementEffectiveDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditEnhancementEffectiveDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditEnhancementEffectiveDateSpecified
        {
            get { return CreditEnhancementEffectiveDate != null; }
            set { }
        }

        /// <summary>
        /// Specifies the period of time for which the selected credit enhancement option is in effect.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<CreditEnhancementEffectivePeriodBase> CreditEnhancementEffectivePeriodType;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditEnhancementEffectivePeriodType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditEnhancementEffectivePeriodType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditEnhancementEffectivePeriodTypeSpecified
        {
            get { return this.CreditEnhancementEffectivePeriodType != null && this.CreditEnhancementEffectivePeriodType.enumValue != CreditEnhancementEffectivePeriodBase.Blank; }
            set { }
        }

        /// <summary>
        /// The date that the selected credit enhancement expires.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMODate CreditEnhancementExpirationDate;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditEnhancementExpirationDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditEnhancementExpirationDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditEnhancementExpirationDateSpecified
        {
            get { return CreditEnhancementExpirationDate != null; }
            set { }
        }

        /// <summary>
        /// Indicates the party that bears the default loss associated with the credit enhancement type.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOEnum<CreditEnhancementPartyRoleBase> CreditEnhancementPartyRoleType;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditEnhancementPartyRoleType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditEnhancementPartyRoleType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditEnhancementPartyRoleTypeSpecified
        {
            get { return this.CreditEnhancementPartyRoleType != null && this.CreditEnhancementPartyRoleType.enumValue != CreditEnhancementPartyRoleBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Credit Enhancement Party Role Type.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString CreditEnhancementPartyRoleTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditEnhancementPartyRoleTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditEnhancementPartyRoleTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditEnhancementPartyRoleTypeOtherDescriptionSpecified
        {
            get { return CreditEnhancementPartyRoleTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether the Credit Enhancement Type is period based.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOIndicator CreditEnhancementPeriodBasedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditEnhancementPeriodBasedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditEnhancementPeriodBasedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditEnhancementPeriodBasedIndicatorSpecified
        {
            get { return CreditEnhancementPeriodBasedIndicator != null; }
            set { }
        }

        /// <summary>
        /// Specifies the type of risk offset on the loan.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOEnum<CreditEnhancementBase> CreditEnhancementType;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditEnhancementType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditEnhancementType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditEnhancementTypeSpecified
        {
            get { return this.CreditEnhancementType != null && this.CreditEnhancementType.enumValue != CreditEnhancementBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Credit Enhancement Type.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOString CreditEnhancementTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditEnhancementTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditEnhancementTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditEnhancementTypeOtherDescriptionSpecified
        {
            get { return CreditEnhancementTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 8)]
        public CREDIT_ENHANCEMENT_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
