namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CREDIT_ENHANCEMENTS
    {
        /// <summary>
        /// Gets a value indicating whether the CREDIT_ENHANCEMENTS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditEnhancementSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of credit enhancements.
        /// </summary>
        [XmlElement("CREDIT_ENHANCEMENT", Order = 0)]
		public List<CREDIT_ENHANCEMENT> CreditEnhancement = new List<CREDIT_ENHANCEMENT>();

        /// <summary>
        /// Gets or sets a value indicating whether the CreditEnhancement element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditEnhancement element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditEnhancementSpecified
        {
            get { return this.CreditEnhancement != null && this.CreditEnhancement.Count(c => c != null && c.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public CREDIT_ENHANCEMENTS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
