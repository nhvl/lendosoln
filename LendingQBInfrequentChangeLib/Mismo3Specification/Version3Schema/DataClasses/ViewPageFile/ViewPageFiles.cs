namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class VIEW_PAGE_FILES
    {
        /// <summary>
        /// Gets a value indicating whether the VIEW_PAGE_FILES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ViewPageFileSpecified;
            }
        }

        /// <summary>
        /// A collection of view page files.
        /// </summary>
        [XmlElement("VIEW_PAGE_FILE", Order = 0)]
		public List<VIEW_PAGE_FILE> ViewPageFile = new List<VIEW_PAGE_FILE>();

        /// <summary>
        /// Gets or sets a value indicating whether the ViewPageFile element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ViewPageFile element has been assigned a value.</value>
        [XmlIgnore]
        public bool ViewPageFileSpecified
        {
            get { return this.ViewPageFile != null && this.ViewPageFile.Count(v => v != null && v.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public VIEW_PAGE_FILES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
