namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PERIODIC_LATE_COUNTS
    {
        /// <summary>
        /// Gets a value indicating whether the PERIODIC_LATE_COUNTS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.PeriodicLateCountSpecified;
            }
        }

        /// <summary>
        /// A collection of periodic late counts.
        /// </summary>
        [XmlElement("PERIODIC_LATE_COUNT", Order = 0)]
		public List<PERIODIC_LATE_COUNT> PeriodicLateCount = new List<PERIODIC_LATE_COUNT>();

        /// <summary>
        /// Gets or sets a value indicating whether the PeriodicLateCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PeriodicLateCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PeriodicLateCountSpecified
        {
            get { return this.PeriodicLateCount != null && this.PeriodicLateCount.Count(p => p != null && p.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public PERIODIC_LATE_COUNTS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
