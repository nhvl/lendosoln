namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class PERIODIC_LATE_COUNT
    {
        /// <summary>
        /// Gets a value indicating whether the PERIODIC_LATE_COUNT container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.Periodic120DaysLateCountSpecified
                    || this.Periodic30DaysLateCountSpecified
                    || this.Periodic60DaysLateCountSpecified
                    || this.Periodic90DaysLateCountSpecified
                    || this.PeriodicLateCountTypeOtherDescriptionSpecified
                    || this.PeriodicLateCountTypeSpecified;
            }
        }

        /// <summary>
        /// Indicates how many times the account was late 120 days during the Periodic Late Count Type specified.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOCount Periodic120DaysLateCount;

        /// <summary>
        /// Gets or sets a value indicating whether the Periodic120DaysLateCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Periodic120DaysLateCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool Periodic120DaysLateCountSpecified
        {
            get { return Periodic120DaysLateCount != null; }
            set { }
        }

        /// <summary>
        /// Indicates how many times the account was late 30 days during the Periodic Late Count Type specified.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOCount Periodic30DaysLateCount;

        /// <summary>
        /// Gets or sets a value indicating whether the Periodic30DaysLateCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Periodic30DaysLateCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool Periodic30DaysLateCountSpecified
        {
            get { return Periodic30DaysLateCount != null; }
            set { }
        }

        /// <summary>
        /// Indicates how many times the account was late 60 days during the Periodic Late Count Type specified.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOCount Periodic60DaysLateCount;

        /// <summary>
        /// Gets or sets a value indicating whether the Periodic60DaysLateCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Periodic60DaysLateCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool Periodic60DaysLateCountSpecified
        {
            get { return Periodic60DaysLateCount != null; }
            set { }
        }

        /// <summary>
        /// Indicates how many times the account was late 90 days during the Periodic Late Count Type specified.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOCount Periodic90DaysLateCount;

        /// <summary>
        /// Gets or sets a value indicating whether the Periodic90DaysLateCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Periodic90DaysLateCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool Periodic90DaysLateCountSpecified
        {
            get { return Periodic90DaysLateCount != null; }
            set { }
        }

        /// <summary>
        /// Indicates the time interval for the Late Count data. Possible intervals are First Year. Second Year, Third Year.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<PeriodicLateCountBase> PeriodicLateCountType;

        /// <summary>
        /// Gets or sets a value indicating whether the PeriodicLateCountType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PeriodicLateCountType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PeriodicLateCountTypeSpecified
        {
            get { return this.PeriodicLateCountType != null && this.PeriodicLateCountType.enumValue != PeriodicLateCountBase.Blank; }
            set { }
        }

        /// <summary>
        /// When Periodic Late Count Type is set to Other, enter the value here.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString PeriodicLateCountTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PeriodicLateCountTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PeriodicLateCountTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PeriodicLateCountTypeOtherDescriptionSpecified
        {
            get { return PeriodicLateCountTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 6)]
        public PERIODIC_LATE_COUNT_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
