namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class ATTIC_FEATURES
    {
        /// <summary>
        /// Gets a value indicating whether the ATTIC_FEATURES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AtticFeatureSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of attic features.
        /// </summary>
        [XmlElement("ATTIC_FEATURE", Order = 0)]
		public List<ATTIC_FEATURE> AtticFeature = new List<ATTIC_FEATURE>();

        /// <summary>
        /// Gets or sets a value indicating whether the AtticFeature element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AtticFeature element has been assigned a value.</value>
        [XmlIgnore]
        public bool AtticFeatureSpecified
        {
            get { return this.AtticFeature != null && this.AtticFeature.Count(a => a != null && a.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public ATTIC_FEATURES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
