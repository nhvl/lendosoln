namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class ATTIC_FEATURE
    {
        /// <summary>
        /// Gets a value indicating whether the ATTIC_FEATURE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AtticFeatureDescriptionSpecified
                    || this.AtticFeatureTypeOtherDescriptionSpecified
                    || this.AtticFeatureTypeSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        ///  A free-form text field used to describe in detail the attic feature. 
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString AtticFeatureDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the AtticFeatureDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AtticFeatureDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool AtticFeatureDescriptionSpecified
        {
            get { return AtticFeatureDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the aspect or feature that is present in the attic of the structure.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<AtticFeatureBase> AtticFeatureType;

        /// <summary>
        /// Gets or sets a value indicating whether the AtticFeatureType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AtticFeatureType element has been assigned a value.</value>
        [XmlIgnore]
        public bool AtticFeatureTypeSpecified
        {
            get { return this.AtticFeatureType != null && this.AtticFeatureType.enumValue != AtticFeatureBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the attic feature if Other is selected as the Attic Feature Type.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString AtticFeatureTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the AtticFeatureTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AtticFeatureTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool AtticFeatureTypeOtherDescriptionSpecified
        {
            get { return AtticFeatureTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public ATTIC_FEATURE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
