namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CHARGE_OFF
    {
        /// <summary>
        /// Gets a value indicating whether the CHARGE_OFF container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ChargeOffDetailSpecified
                    || this.ChargeOffItemsSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// Details on the charge off.
        /// </summary>
        [XmlElement("CHARGE_OFF_DETAIL", Order = 0)]
        public CHARGE_OFF_DETAIL ChargeOffDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the ChargeOffDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ChargeOffDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool ChargeOffDetailSpecified
        {
            get { return this.ChargeOffDetail != null && this.ChargeOffDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A list of charge off items.
        /// </summary>
        [XmlElement("CHARGE_OFF_ITEMS", Order = 1)]
        public CHARGE_OFF_ITEMS ChargeOffItems;

        /// <summary>
        /// Gets or sets a value indicating whether the ChargeOffItems element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ChargeOffItems element has been assigned a value.</value>
        [XmlIgnore]
        public bool ChargeOffItemsSpecified
        {
            get { return this.ChargeOffItems != null && this.ChargeOffItems.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public CHARGE_OFF_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
