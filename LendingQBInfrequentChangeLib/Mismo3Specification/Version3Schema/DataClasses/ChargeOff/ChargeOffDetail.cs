namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CHARGE_OFF_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the CHARGE_OFF_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ChargeOffDateSpecified
                    || this.ChargeOffReasonDescriptionSpecified
                    || this.ChargeOffReasonIdentifierSpecified
                    || this.ExtensionSpecified
                    || this.LienReleaseIndicatorSpecified
                    || this.LienReleaseIndicatorSpecified;
            }
        }

        /// <summary>
        /// The date the total charge off amount was charged off.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMODate ChargeOffDate;

        /// <summary>
        /// Gets or sets a value indicating whether the ChargeOffDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ChargeOffDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ChargeOffDateSpecified
        {
            get { return ChargeOffDate != null; }
            set { }
        }

        /// <summary>
        /// A description of the reasons for the charge off decision.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString ChargeOffReasonDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ChargeOffReasonDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ChargeOffReasonDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ChargeOffReasonDescriptionSpecified
        {
            get { return ChargeOffReasonDescription != null; }
            set { }
        }

        /// <summary>
        /// A unique identifier assigned by a party to specify a reason for the charge off. The party assigning the identifier can be provided using the IdentifierOwnerURI.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOIdentifier ChargeOffReasonIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the ChargeOffReasonIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ChargeOffReasonIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool ChargeOffReasonIdentifierSpecified
        {
            get { return ChargeOffReasonIdentifier != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that a lien will be released in the debt charge off consideration.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOIndicator LienReleaseIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the LienReleaseIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LienReleaseIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool LienReleaseIndicatorSpecified
        {
            get { return LienReleaseIndicator != null; }
            set { }
        }

        /// <summary>
        /// Cumulative total loss amount charged off on the loan.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOAmount TotalChargeOffAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the TotalChargeOffAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TotalChargeOffAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TotalChargeOffAmountSpecified
        {
            get { return TotalChargeOffAmount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 5)]
        public CHARGE_OFF_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
