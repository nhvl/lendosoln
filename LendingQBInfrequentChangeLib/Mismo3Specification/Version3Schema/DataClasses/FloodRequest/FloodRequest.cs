namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class FLOOD_REQUEST
    {
        /// <summary>
        /// Gets a value indicating whether the FLOOD_REQUEST container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.FloodRequestDetailSpecified
                    || this.FloodRequestDisputeSpecified;
            }
        }

        /// <summary>
        /// Details on a flood request.
        /// </summary>
        [XmlElement("FLOOD_REQUEST_DETAIL", Order = 0)]
        public FLOOD_REQUEST_DETAIL FloodRequestDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the FloodRequestDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FloodRequestDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool FloodRequestDetailSpecified
        {
            get { return this.FloodRequestDetail != null && this.FloodRequestDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A dispute against a flood request.
        /// </summary>
        [XmlElement("FLOOD_REQUEST_DISPUTE", Order = 1)]
        public FLOOD_REQUEST_DISPUTE FloodRequestDispute;

        /// <summary>
        /// Gets or sets a value indicating whether the FloodRequestDispute element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FloodRequestDispute element has been assigned a value.</value>
        [XmlIgnore]
        public bool FloodRequestDisputeSpecified
        {
            get { return this.FloodRequestDispute != null && this.FloodRequestDispute.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public FLOOD_REQUEST_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
