namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class FLOOD_REQUEST_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the FLOOD_REQUEST_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BorrowerFloodAcknowledgementLetterIndicatorSpecified
                    || this.ExtensionSpecified
                    || this.FEMAAdditionalLenderDescriptionSpecified
                    || this.FloodCertificationIdentifierSpecified
                    || this.FloodInsuranceAmountSpecified
                    || this.FloodRequestActionTypeSpecified
                    || this.FloodRequestCommentTextSpecified
                    || this.FloodRequestRushIndicatorSpecified
                    || this.FloodTransactionIdentifierSpecified
                    || this.NewServicerAccountIdentifierSpecified
                    || this.OrderingSystemNameSpecified
                    || this.OrderingSystemVersionIdentifierSpecified
                    || this.OriginalFloodDeterminationLoanIdentifierSpecified;
            }
        }

        /// <summary>
        /// Indicates if a Borrower Acknowledgement Letter needs to be processed with the order.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIndicator BorrowerFloodAcknowledgementLetterIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the BorrowerFloodAcknowledgementLetterIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BorrowerFloodAcknowledgementLetterIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool BorrowerFloodAcknowledgementLetterIndicatorSpecified
        {
            get { return BorrowerFloodAcknowledgementLetterIndicator != null; }
            set { }
        }

        /// <summary>
        /// Additional information for the lender in Box 1 of the FEMA document.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString FEMAAdditionalLenderDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the FEMAAdditionalLenderDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FEMAAdditionalLenderDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool FEMAAdditionalLenderDescriptionSpecified
        {
            get { return FEMAAdditionalLenderDescription != null; }
            set { }
        }

        /// <summary>
        /// The certification number assigned by the compliance company when an order is received from the submitting / requesting party.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOIdentifier FloodCertificationIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the FloodCertificationIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FloodCertificationIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool FloodCertificationIdentifierSpecified
        {
            get { return FloodCertificationIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The minimal Federal requirement for flood insurance coverage, the lesser of: the outstanding principal loan balance; the value of the improved property, mobile home and/or personal property used to secure the loan; or the maximum statutory limit of flood insurance coverage.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOAmount FloodInsuranceAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the FloodInsuranceAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FloodInsuranceAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool FloodInsuranceAmountSpecified
        {
            get { return FloodInsuranceAmount != null; }
            set { }
        }

        /// <summary>
        /// Describes the type of action to be performed for the request in question.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<FloodRequestActionBase> FloodRequestActionType;

        /// <summary>
        /// Gets or sets a value indicating whether the FloodRequestActionType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FloodRequestActionType element has been assigned a value.</value>
        [XmlIgnore]
        public bool FloodRequestActionTypeSpecified
        {
            get { return this.FloodRequestActionType != null && this.FloodRequestActionType.enumValue != FloodRequestActionBase.Blank; }
            set { }
        }

        /// <summary>
        /// Comments for the flood request in question.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString FloodRequestCommentText;

        /// <summary>
        /// Gets or sets a value indicating whether the FloodRequestCommentText element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FloodRequestCommentText element has been assigned a value.</value>
        [XmlIgnore]
        public bool FloodRequestCommentTextSpecified
        {
            get { return FloodRequestCommentText != null; }
            set { }
        }

        /// <summary>
        /// Indicates that this order has a high priority in need for completion.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOIndicator FloodRequestRushIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the FloodRequestRushIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FloodRequestRushIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool FloodRequestRushIndicatorSpecified
        {
            get { return FloodRequestRushIndicator != null; }
            set { }
        }

        /// <summary>
        /// A unique tracking identifier assigned by the sender which is typically returned to the sender in the resulting response.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOIdentifier FloodTransactionIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the FloodTransactionIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FloodTransactionIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool FloodTransactionIdentifierSpecified
        {
            get { return FloodTransactionIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The vendor account code assigned to the Lender who receives the transfer.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOIdentifier NewServicerAccountIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the NewServicerAccountIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NewServicerAccountIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool NewServicerAccountIdentifierSpecified
        {
            get { return NewServicerAccountIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The name of the system that originated the order.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOString OrderingSystemName;

        /// <summary>
        /// Gets or sets a value indicating whether the OrderingSystemName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OrderingSystemName element has been assigned a value.</value>
        [XmlIgnore]
        public bool OrderingSystemNameSpecified
        {
            get { return OrderingSystemName != null; }
            set { }
        }

        /// <summary>
        /// The version of the system that originated the order.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOIdentifier OrderingSystemVersionIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the OrderingSystemVersionIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OrderingSystemVersionIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool OrderingSystemVersionIdentifierSpecified
        {
            get { return OrderingSystemVersionIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The loan number assigned to the original flood determination order. Used for identifying a loan that has been previously certified (e.g., refinance, second, servicing transfer, etc.).
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOIdentifier OriginalFloodDeterminationLoanIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the OriginalFloodDeterminationLoanIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OriginalFloodDeterminationLoanIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool OriginalFloodDeterminationLoanIdentifierSpecified
        {
            get { return OriginalFloodDeterminationLoanIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 12)]
        public FLOOD_REQUEST_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
