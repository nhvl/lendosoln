namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class HELOC_RULE
    {
        /// <summary>
        /// Gets a value indicating whether the HELOC_RULE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.HELOCAnnualFeeAmountSpecified
                    || this.HELOCCreditCardAccountIdentifierSpecified
                    || this.HELOCCreditCardIndicatorSpecified
                    || this.HELOCCreditLineDrawAccessFeeAmountSpecified
                    || this.HELOCDailyPeriodicInterestRateCalculationTypeSpecified
                    || this.HELOCInitialAdvanceAmountSpecified
                    || this.HELOCMaximumAPRPercentSpecified
                    || this.HELOCMaximumBalanceAmountSpecified
                    || this.HELOCMinimumAdvanceAmountSpecified
                    || this.HELOCMinimumPaymentAmountSpecified
                    || this.HELOCMinimumPaymentPercentSpecified
                    || this.HELOCRepayPeriodMonthsCountSpecified
                    || this.HELOCReturnedCheckChargeAmountSpecified
                    || this.HELOCStopPaymentChargeAmountSpecified
                    || this.HELOCTeaserMarginRatePercentSpecified
                    || this.HELOCTeaserRateTypeSpecified
                    || this.HELOCTeaserTermMonthsCountSpecified
                    || this.HELOCTerminationFeeAmountSpecified
                    || this.HELOCTerminationPeriodMonthsCountSpecified;
            }
        }

        /// <summary>
        /// The dollar amount of an annual fee associated with the loan and charged to the borrower.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount HELOCAnnualFeeAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the HELOCAnnualFeeAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HELOCAnnualFeeAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool HELOCAnnualFeeAmountSpecified
        {
            get { return HELOCAnnualFeeAmount != null; }
            set { }
        }

        /// <summary>
        /// The account number assigned for the HELOC credit card.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOIdentifier HELOCCreditCardAccountIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the HELOCCreditCardAccountIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HELOCCreditCardAccountIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool HELOCCreditCardAccountIdentifierSpecified
        {
            get { return HELOCCreditCardAccountIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Indicates the loan payments are to be charged to a credit card account.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOIndicator HELOCCreditCardIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the HELOCCreditCardIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HELOCCreditCardIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool HELOCCreditCardIndicatorSpecified
        {
            get { return HELOCCreditCardIndicator != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of the transaction fee, including any minimum fee or per-transaction fee that will be charged for a draw on the HELOC.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOAmount HELOCCreditLineDrawAccessFeeAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the HELOCCreditLineDrawAccessFeeAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HELOCCreditLineDrawAccessFeeAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool HELOCCreditLineDrawAccessFeeAmountSpecified
        {
            get { return HELOCCreditLineDrawAccessFeeAmount != null; }
            set { }
        }

        /// <summary>
        /// The factor used in calculating the dollar amount of daily periodic interest charged in the HELOC loan transaction.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<HELOCDailyPeriodicInterestRateCalculationBase> HELOCDailyPeriodicInterestRateCalculationType;

        /// <summary>
        /// Gets or sets a value indicating whether the HELOCDailyPeriodicInterestRateCalculationType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HELOCDailyPeriodicInterestRateCalculationType element has been assigned a value.</value>
        [XmlIgnore]
        public bool HELOCDailyPeriodicInterestRateCalculationTypeSpecified
        {
            get { return this.HELOCDailyPeriodicInterestRateCalculationType != null && this.HELOCDailyPeriodicInterestRateCalculationType.enumValue != HELOCDailyPeriodicInterestRateCalculationBase.Blank; }
            set { }
        }

        /// <summary>
        /// The amount of money that a borrower receives at closing in a HELOC transaction.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOAmount HELOCInitialAdvanceAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the HELOCInitialAdvanceAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HELOCInitialAdvanceAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool HELOCInitialAdvanceAmountSpecified
        {
            get { return HELOCInitialAdvanceAmount != null; }
            set { }
        }

        /// <summary>
        /// The maximum annual percentage rate (APR) that can be charged in association with the HELOC loan transaction.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOPercent HELOCMaximumAPRPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the HELOCMaximumAPRPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HELOCMaximumAPRPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool HELOCMaximumAPRPercentSpecified
        {
            get { return HELOCMaximumAPRPercent != null; }
            set { }
        }

        /// <summary>
        /// The maximum dollar amount of credit available to the borrower on a Home Equity Line of Credit (HELOC) regardless of whether the borrower has accessed the amount.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOAmount HELOCMaximumBalanceAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the HELOCMaximumBalanceAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HELOCMaximumBalanceAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool HELOCMaximumBalanceAmountSpecified
        {
            get { return HELOCMaximumBalanceAmount != null; }
            set { }
        }

        /// <summary>
        /// The minimum amount that will be allowed as an advance from the total Credit limit in a HELOC transaction.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOAmount HELOCMinimumAdvanceAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the HELOCMinimumAdvanceAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HELOCMinimumAdvanceAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool HELOCMinimumAdvanceAmountSpecified
        {
            get { return HELOCMinimumAdvanceAmount != null; }
            set { }
        }

        /// <summary>
        /// The minimum dollar amount that can be paid as a monthly payment for the HELOC loan.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOAmount HELOCMinimumPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the HELOCMinimumPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HELOCMinimumPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool HELOCMinimumPaymentAmountSpecified
        {
            get { return HELOCMinimumPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// The minimum percentage rate that can be used to calculate the monthly payment for the HELOC loan.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOPercent HELOCMinimumPaymentPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the HELOCMinimumPaymentPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HELOCMinimumPaymentPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool HELOCMinimumPaymentPercentSpecified
        {
            get { return HELOCMinimumPaymentPercent != null; }
            set { }
        }

        /// <summary>
        /// The term in months by which full payment of the HELOC loan balance must be made.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOCount HELOCRepayPeriodMonthsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the HELOCRepayPeriodMonthsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HELOCRepayPeriodMonthsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool HELOCRepayPeriodMonthsCountSpecified
        {
            get { return HELOCRepayPeriodMonthsCount != null; }
            set { }
        }

        /// <summary>
        /// The amount that can be charged in the case of a payment made on the HELOC loan being returned for non sufficient funds.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOAmount HELOCReturnedCheckChargeAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the HELOCReturnedCheckChargeAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HELOCReturnedCheckChargeAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool HELOCReturnedCheckChargeAmountSpecified
        {
            get { return HELOCReturnedCheckChargeAmount != null; }
            set { }
        }

        /// <summary>
        /// The amount that can be charged when a stop payment is made on a loan HELOC payment.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOAmount HELOCStopPaymentChargeAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the HELOCStopPaymentChargeAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HELOCStopPaymentChargeAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool HELOCStopPaymentChargeAmountSpecified
        {
            get { return HELOCStopPaymentChargeAmount != null; }
            set { }
        }

        /// <summary>
        /// The margin rate, expressed as a percent, stated on the line of credit with a variable introductory (teaser) rate.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOPercent HELOCTeaserMarginRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the HELOCTeaserMarginRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HELOCTeaserMarginRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool HELOCTeaserMarginRatePercentSpecified
        {
            get { return HELOCTeaserMarginRatePercent != null; }
            set { }
        }

        /// <summary>
        /// The type of introductory (teaser) rate on a line of credit.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOEnum<HELOCTeaserRateBase> HELOCTeaserRateType;

        /// <summary>
        /// Gets or sets a value indicating whether the HELOCTeaserRateType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HELOCTeaserRateType element has been assigned a value.</value>
        [XmlIgnore]
        public bool HELOCTeaserRateTypeSpecified
        {
            get { return this.HELOCTeaserRateType != null && this.HELOCTeaserRateType.enumValue != HELOCTeaserRateBase.Blank; }
            set { }
        }

        /// <summary>
        /// The number of months the discounted interest rate is used to determine the payment amount.
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMOCount HELOCTeaserTermMonthsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the HELOCTeaserTermMonthsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HELOCTeaserTermMonthsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool HELOCTeaserTermMonthsCountSpecified
        {
            get { return HELOCTeaserTermMonthsCount != null; }
            set { }
        }

        /// <summary>
        /// The fee amount charged when the HELOC transaction is paid off early.
        /// </summary>
        [XmlElement(Order = 17)]
        public MISMOAmount HELOCTerminationFeeAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the HELOCTerminationFeeAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HELOCTerminationFeeAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool HELOCTerminationFeeAmountSpecified
        {
            get { return HELOCTerminationFeeAmount != null; }
            set { }
        }

        /// <summary>
        /// The number of months until the HELOC is terminated.
        /// </summary>
        [XmlElement(Order = 18)]
        public MISMOCount HELOCTerminationPeriodMonthsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the HELOCTerminationPeriodMonthsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HELOCTerminationPeriodMonthsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool HELOCTerminationPeriodMonthsCountSpecified
        {
            get { return HELOCTerminationPeriodMonthsCount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 19)]
        public HELOC_RULE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
