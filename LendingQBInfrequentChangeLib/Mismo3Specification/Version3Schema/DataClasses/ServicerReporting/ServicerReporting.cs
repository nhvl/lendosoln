namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class SERVICER_REPORTING
    {
        /// <summary>
        /// Gets a value indicating whether the SERVICER_REPORTING container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ServicerReportingDetailSpecified
                    || this.ServicingCommentsSpecified;
            }
        }

        /// <summary>
        /// Details on servicer reporting.
        /// </summary>
        [XmlElement("SERVICER_REPORTING_DETAIL", Order = 0)]
        public SERVICER_REPORTING_DETAIL ServicerReportingDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the ServicerReportingDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServicerReportingDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServicerReportingDetailSpecified
        {
            get { return this.ServicerReportingDetail != null && this.ServicerReportingDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A list of servicing comments.
        /// </summary>
        [XmlElement("SERVICING_COMMENTS", Order = 1)]
        public SERVICING_COMMENTS ServicingComments;

        /// <summary>
        /// Gets or sets a value indicating whether the ServicingComments element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServicingComments element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServicingCommentsSpecified
        {
            get { return this.ServicingComments != null && this.ServicingComments.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public SERVICER_REPORTING_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
