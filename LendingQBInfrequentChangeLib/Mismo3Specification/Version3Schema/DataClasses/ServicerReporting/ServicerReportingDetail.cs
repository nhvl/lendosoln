namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class SERVICER_REPORTING_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the SERVICER_REPORTING_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.InvestorReportingCycleDateSpecified
                    || this.InvestorReportingCycleDescriptionSpecified
                    || this.PoolReportingRevisionIndicatorSpecified
                    || this.PortfolioDelinquencyIndicatorSpecified;
            }
        }

        /// <summary>
        ///  The date associated with the investor reporting cycle that the servicer is reporting to the investor.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMODate InvestorReportingCycleDate;

        /// <summary>
        /// Gets or sets a value indicating whether the InvestorReportingCycleDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InvestorReportingCycleDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool InvestorReportingCycleDateSpecified
        {
            get { return InvestorReportingCycleDate != null; }
            set { }
        }

        /// <summary>
        /// A description, as defined by the Servicer and Investor, of the processing and reporting cycle to which the activity pertains.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString InvestorReportingCycleDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the InvestorReportingCycleDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InvestorReportingCycleDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool InvestorReportingCycleDescriptionSpecified
        {
            get { return InvestorReportingCycleDescription != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that the information being reported is used to change the pool data for the pool reporting already submitted.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOIndicator PoolReportingRevisionIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PoolReportingRevisionIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PoolReportingRevisionIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PoolReportingRevisionIndicatorSpecified
        {
            get { return PoolReportingRevisionIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the Servicer has loans in their portfolio that are 30 days or more delinquent or in Bankruptcy at the end of the reporting cycle.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOIndicator PortfolioDelinquencyIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PortfolioDelinquencyIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PortfolioDelinquencyIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PortfolioDelinquencyIndicatorSpecified
        {
            get { return PortfolioDelinquencyIndicator != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public SERVICER_REPORTING_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
