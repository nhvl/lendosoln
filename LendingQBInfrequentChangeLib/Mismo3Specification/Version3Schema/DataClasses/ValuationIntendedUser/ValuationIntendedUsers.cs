namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class VALUATION_INTENDED_USERS
    {
        /// <summary>
        /// Gets a value indicating whether the VALUATION_INTENDED_USERS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ValuationIntendedUserSpecified;
            }
        }

        /// <summary>
        /// A collection of valuation intended users.
        /// </summary>
        [XmlElement("VALUATION_INTENDED_USER", Order = 0)]
		public List<VALUATION_INTENDED_USER> ValuationIntendedUser = new List<VALUATION_INTENDED_USER>();

        /// <summary>
        /// Gets or sets a value indicating whether the ValuationIntendedUser element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ValuationIntendedUser element has been assigned a value.</value>
        [XmlIgnore]
        public bool ValuationIntendedUserSpecified
        {
            get { return this.ValuationIntendedUser != null && this.ValuationIntendedUser.Count(v => v != null && v.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public VALUATION_INTENDED_USERS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
