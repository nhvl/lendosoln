namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class VALUATION_INTENDED_USER
    {
        /// <summary>
        /// Gets a value indicating whether the VALUATION_INTENDED_USER container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ValuationIntendedUserDescriptionSpecified
                    || this.ValuationIntendedUserTypeOtherDescriptionSpecified
                    || this.ValuationIntendedUserTypeSpecified;
            }
        }

        /// <summary>
        /// Detail or commentary from the appraiser regarding the Intended User declared in a valuation product or service and for which he is obligated.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString ValuationIntendedUserDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ValuationIntendedUserDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ValuationIntendedUserDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ValuationIntendedUserDescriptionSpecified
        {
            get { return ValuationIntendedUserDescription != null; }
            set { }
        }

        /// <summary>
        /// Specific indications of the types of users authorized by the appraiser for a given valuation product.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<ValuationIntendedUserBase> ValuationIntendedUserType;

        /// <summary>
        /// Gets or sets a value indicating whether the ValuationIntendedUserType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ValuationIntendedUserType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ValuationIntendedUserTypeSpecified
        {
            get { return this.ValuationIntendedUserType != null && this.ValuationIntendedUserType.enumValue != ValuationIntendedUserBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Valuation Intended User Type.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString ValuationIntendedUserTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ValuationIntendedUserTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ValuationIntendedUserTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ValuationIntendedUserTypeOtherDescriptionSpecified
        {
            get { return this.ValuationIntendedUserTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public VALUATION_INTENDED_USER_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
