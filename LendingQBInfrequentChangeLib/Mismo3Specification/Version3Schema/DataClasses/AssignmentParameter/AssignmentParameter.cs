namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class ASSIGNMENT_PARAMETER
    {
        /// <summary>
        /// Gets a value indicating whether the ASSIGNMENT_PARAMETER container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AssignmentConditionTypeOtherDescriptionSpecified
                    || this.AssignmentConditionTypeSpecified
                    || this.AssignmentDescriptionSpecified
                    || this.AssignmentParameterPriorityTypeSpecified
                    || this.AssignmentParameterSourceTypeOtherDescriptionSpecified
                    || this.AssignmentParameterSourceTypeSpecified
                    || this.AssignmentParameterTypeOtherDescriptionSpecified
                    || this.AssignmentParameterTypeSpecified
                    || this.ExtensionSpecified
                    || this.VendorSpecialtyTypeOtherDescriptionSpecified
                    || this.VendorSpecialtyTypeSpecified
                    || this.VendorWatchListIndicationDescriptionSpecified;
            }
        }

        /// <summary>
        /// Indicates the type of conditions applied at the assignment level affecting the vendor selection process.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<AssignmentConditionBase> AssignmentConditionType;

        /// <summary>
        /// Gets or sets a value indicating whether the AssignmentConditionType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AssignmentConditionType element has been assigned a value.</value>
        [XmlIgnore]
        public bool AssignmentConditionTypeSpecified
        {
            get { return this.AssignmentConditionType != null && this.AssignmentConditionType.enumValue != AssignmentConditionBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Assignment Condition Type.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString AssignmentConditionTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the AssignmentConditionTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AssignmentConditionTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool AssignmentConditionTypeOtherDescriptionSpecified
        {
            get { return AssignmentConditionTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A free-form description of the assignment, assignment conditions or parameters that require elaboration.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString AssignmentDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the AssignmentDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AssignmentDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool AssignmentDescriptionSpecified
        {
            get { return AssignmentDescription != null; }
            set { }
        }

        /// <summary>
        /// Describes the rule authority to be applied when using the assignment parameter.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOEnum<AssignmentParameterPriorityBase> AssignmentParameterPriorityType;

        /// <summary>
        /// Gets or sets a value indicating whether the AssignmentParameterPriorityType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AssignmentParameterPriorityType element has been assigned a value.</value>
        [XmlIgnore]
        public bool AssignmentParameterPriorityTypeSpecified
        {
            get { return this.AssignmentParameterPriorityType != null && this.AssignmentParameterPriorityType.enumValue != AssignmentParameterPriorityBase.Blank; }
            set { }
        }

        /// <summary>
        /// Describes the type of source for the assignment instruction being applied.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<AssignmentParameterSourceBase> AssignmentParameterSourceType;

        /// <summary>
        /// Gets or sets a value indicating whether the AssignmentParameterSourceType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AssignmentParameterSourceType element has been assigned a value.</value>
        [XmlIgnore]
        public bool AssignmentParameterSourceTypeSpecified
        {
            get { return this.AssignmentParameterSourceType != null && this.AssignmentParameterSourceType.enumValue != AssignmentParameterSourceBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Assignment Parameter Source Type.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString AssignmentParameterSourceTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the AssignmentParameterSourceTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AssignmentParameterSourceTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool AssignmentParameterSourceTypeOtherDescriptionSpecified
        {
            get { return AssignmentParameterSourceTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The specific assignment parameter to be applied by the valuation professional in the execution of the assignment order.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOEnum<AssignmentParameterBase> AssignmentParameterType;

        /// <summary>
        /// Gets or sets a value indicating whether the AssignmentParameterType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AssignmentParameterType element has been assigned a value.</value>
        [XmlIgnore]
        public bool AssignmentParameterTypeSpecified
        {
            get { return this.AssignmentParameterType != null && this.AssignmentParameterType.enumValue != AssignmentParameterBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Assignment Parameter Type.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOString AssignmentParameterTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the AssignmentParameterTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AssignmentParameterTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool AssignmentParameterTypeOtherDescriptionSpecified
        {
            get { return AssignmentParameterTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Special skills or property type knowledge that may be stipulated for the vendor selection in the assignment or request for service.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOEnum<VendorSpecialtyBase> VendorSpecialtyType;

        /// <summary>
        /// Gets or sets a value indicating whether the VendorSpecialtyType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the VendorSpecialtyType element has been assigned a value.</value>
        [XmlIgnore]
        public bool VendorSpecialtyTypeSpecified
        {
            get { return this.VendorSpecialtyType != null && this.VendorSpecialtyType.enumValue != VendorSpecialtyBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Vendor Specialty Type.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOString VendorSpecialtyTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the VendorSpecialtyTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the VendorSpecialtyTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool VendorSpecialtyTypeOtherDescriptionSpecified
        {
            get { return VendorSpecialtyTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates the details of watch list handling that should be observed in the assignment.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOString VendorWatchListIndicationDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the VendorWatchListIndicationDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the VendorWatchListIndicationDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool VendorWatchListIndicationDescriptionSpecified
        {
            get { return VendorWatchListIndicationDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 11)]
        public ASSIGNMENT_PARAMETER_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
