namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class ASSIGNMENT_PARAMETERS
    {
        /// <summary>
        /// Gets a value indicating whether the ASSIGNMENT_PARAMETERS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AssignmentParameterSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of assignment parameters.
        /// </summary>
        [XmlElement("ASSIGNMENT_PARAMETER", Order = 0)]
		public List<ASSIGNMENT_PARAMETER> AssignmentParameter = new List<ASSIGNMENT_PARAMETER>();

        /// <summary>
        /// Gets or sets a value indicating whether the AssignmentParameter element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AssignmentParameter element has been assigned a value.</value>
        [XmlIgnore]
        public bool AssignmentParameterSpecified
        {
            get { return this.AssignmentParameter != null && this.AssignmentParameter.Count(a => a != null && a.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public ASSIGNMENT_PARAMETERS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
