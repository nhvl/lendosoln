namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class DATA_ITEM_CHANGES
    {
        /// <summary>
        /// Gets a value indicating whether the DATA_ITEM_CHANGES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DataItemChangeSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of data item changes.
        /// </summary>
        [XmlElement("DATA_ITEM_CHANGE", Order = 0)]
		public List<DATA_ITEM_CHANGE> DataItemChange = new List<DATA_ITEM_CHANGE>();

        /// <summary>
        /// Gets or sets a value indicating whether the DataItemChange element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DataItemChange element has been assigned a value.</value>
        [XmlIgnore]
        public bool DataItemChangeSpecified
        {
            get { return this.DataItemChange != null && this.DataItemChange.Count(d => d != null && d.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public DATA_ITEM_CHANGES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
