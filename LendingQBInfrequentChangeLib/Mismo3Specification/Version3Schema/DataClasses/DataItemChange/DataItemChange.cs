namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class DATA_ITEM_CHANGE
    {
        /// <summary>
        /// Gets a value indicating whether the DATA_ITEM_CHANGE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DataItemChangeEffectiveDateSpecified
                    || this.DataItemChangeExpirationDateSpecified
                    || this.DataItemChangeValueSpecified
                    || this.DataItemChangePriorValueSpecified
                    || this.DataItemChangeTypeSpecified
                    || this.DataItemChangeXPathSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// The date the specified value for the data item becomes effective.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMODate DataItemChangeEffectiveDate;

        /// <summary>
        /// Gets or sets a value indicating whether the DataItemChangeEffectiveDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DataItemChangeEffectiveDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool DataItemChangeEffectiveDateSpecified
        {
            get { return DataItemChangeEffectiveDate != null; }
            set { }
        }

        /// <summary>
        /// The date the value for the specified data item is no longer effective.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMODate DataItemChangeExpirationDate;

        /// <summary>
        /// Gets or sets a value indicating whether the DataItemChangeExpirationDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DataItemChangeExpirationDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool DataItemChangeExpirationDateSpecified
        {
            get { return DataItemChangeExpirationDate != null; }
            set { }
        }

        /// <summary>
        /// The previously reported value of the data item.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOValue DataItemChangePriorValue;

        /// <summary>
        /// Gets or sets a value indicating whether the DataItemChangePriorValue element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DataItemChangePriorValue element has been assigned a value.</value>
        [XmlIgnore]
        public bool DataItemChangePriorValueSpecified
        {
            get { return DataItemChangePriorValue != null; }
            set { }
        }

        /// <summary>
        /// Describes the type of change for the data item.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOEnum<DataItemChangeBase> DataItemChangeType;

        /// <summary>
        /// Gets or sets a value indicating whether the DataItemChangeType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DataItemChangeType element has been assigned a value.</value>
        [XmlIgnore]
        public bool DataItemChangeTypeSpecified
        {
            get { return this.DataItemChangeType != null && this.DataItemChangeType.enumValue != DataItemChangeBase.Blank; }
            set { }
        }

        /// <summary>
        /// The value of a data item change number.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOValue DataItemChangeValue;

        /// <summary>
        /// Gets or sets a value indicating whether the DataItemChangeValue element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DataItemChangeValue element has been assigned a value.</value>
        [XmlIgnore]
        public bool DataItemChangeValueSpecified
        {
            get { return DataItemChangeValue != null; }
            set { }
        }

        /// <summary>
        /// A valid relative XPath expression identifying a specific data item in the context specified by the absolute context path identifier. (e.g. the Servicing Transfer LOAN structure. Please see the Implementation Guide for a more detailed description of the valid expressions.).
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOXPath DataItemChangeXPath;

        /// <summary>
        /// Gets or sets a value indicating whether the DataItemChangeXPath element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DataItemChangeXPath element has been assigned a value.</value>
        [XmlIgnore]
        public bool DataItemChangeXPathSpecified
        {
            get { return DataItemChangeXPath != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 6)]
        public DATA_ITEM_CHANGE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
