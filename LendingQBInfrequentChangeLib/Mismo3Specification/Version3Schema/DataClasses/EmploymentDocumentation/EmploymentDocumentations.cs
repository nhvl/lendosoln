namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class EMPLOYMENT_DOCUMENTATIONS
    {
        /// <summary>
        /// Gets a value indicating whether the EMPLOYMENT_DOCUMENTATIONS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.EmploymentDocumentationSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of employment documentation.
        /// </summary>
        [XmlElement("EMPLOYMENT_DOCUMENTATION", Order = 0)]
		public List<EMPLOYMENT_DOCUMENTATION> EmploymentDocumentation = new List<EMPLOYMENT_DOCUMENTATION>();

        /// <summary>
        /// Gets or sets a value indicating whether the EmploymentDocumentation element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EmploymentDocumentation element has been assigned a value.</value>
        [XmlIgnore]
        public bool EmploymentDocumentationSpecified
        {
            get { return this.EmploymentDocumentation != null && this.EmploymentDocumentation.Count(e => e != null && e.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public EMPLOYMENT_DOCUMENTATIONS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
