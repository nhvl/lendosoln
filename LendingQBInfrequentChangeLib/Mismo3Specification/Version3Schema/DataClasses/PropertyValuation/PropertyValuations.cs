namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PROPERTY_VALUATIONS
    {
        /// <summary>
        /// Gets a value indicating whether the PROPERTY_VALUATIONS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.PropertyValuationSpecified;
            }
        }

        /// <summary>
        /// A collection of property valuations.
        /// </summary>
        [XmlElement("PROPERTY_VALUATION", Order = 0)]
		public List<PROPERTY_VALUATION> PropertyValuation = new List<PROPERTY_VALUATION>();

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyValuation element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyValuation element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyValuationSpecified
        {
            get { return this.PropertyValuation != null && this.PropertyValuation.Count(p => p != null && p.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public PROPERTY_VALUATIONS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
