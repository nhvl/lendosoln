namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PROPERTY_VALUATION_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the PROPERTY_VALUATION_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AppraisalIdentifierSpecified
                    || this.ExtensionSpecified
                    || this.PropertyAppraisedWithinPreviousYearDescriptionSpecified
                    || this.PropertyAppraisedWithinPreviousYearIndicatorSpecified
                    || this.PropertyInspectionRequestCommentDescriptionSpecified
                    || this.PropertyInspectionResultCommentDescriptionSpecified
                    || this.PropertyInspectionTypeOtherDescriptionSpecified
                    || this.PropertyInspectionTypeSpecified
                    || this.PropertyMostRecentValuationOrderDateSpecified
                    || this.PropertyReplacementValueAmountSpecified
                    || this.PropertyValuationAgeDaysCountSpecified
                    || this.PropertyValuationAmountSpecified
                    || this.PropertyValuationCommentTextSpecified
                    || this.PropertyValuationConditionalConclusionTypeOtherDescriptionSpecified
                    || this.PropertyValuationConditionalConclusionTypeSpecified
                    || this.PropertyValuationEffectiveDateSpecified
                    || this.PropertyValuationFormTypeOtherDescriptionSpecified
                    || this.PropertyValuationFormTypeSpecified
                    || this.PropertyValuationMethodTypeOtherDescriptionSpecified
                    || this.PropertyValuationMethodTypeSpecified
                    || this.PropertyValuationServiceTypeOtherDescriptionSpecified
                    || this.PropertyValuationServiceTypeSpecified
                    || this.PropertyValuationStateTypeSpecified
                    || this.RepairsTotalCostAmountSpecified;
            }
        }

        /// <summary>
        /// A unique identifier assigned by a party for all appraisal data delivered to the party for this loan. The party assigning the identifier can be provided using the IdentifierOwnerURI associated with a MISMO identifier.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIdentifier AppraisalIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the AppraisalIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AppraisalIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool AppraisalIdentifierSpecified
        {
            get { return AppraisalIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field describing the results of an appraisal within the previous 12 months.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString PropertyAppraisedWithinPreviousYearDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyAppraisedWithinPreviousYearDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyAppraisedWithinPreviousYearDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyAppraisedWithinPreviousYearDescriptionSpecified
        {
            get { return PropertyAppraisedWithinPreviousYearDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the property was appraised within the previous 12 months.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOIndicator PropertyAppraisedWithinPreviousYearIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyAppraisedWithinPreviousYearIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyAppraisedWithinPreviousYearIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyAppraisedWithinPreviousYearIndicatorSpecified
        {
            get { return PropertyAppraisedWithinPreviousYearIndicator != null; }
            set { }
        }

        /// <summary>
        /// Free form commentary about the request for the inspection.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString PropertyInspectionRequestCommentDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyInspectionRequestCommentDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyInspectionRequestCommentDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyInspectionRequestCommentDescriptionSpecified
        {
            get { return PropertyInspectionRequestCommentDescription != null; }
            set { }
        }

        /// <summary>
        /// Free form commentary about the results of the inspection.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString PropertyInspectionResultCommentDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyInspectionResultCommentDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyInspectionResultCommentDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyInspectionResultCommentDescriptionSpecified
        {
            get { return PropertyInspectionResultCommentDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the extent of the inspection.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOEnum<PropertyInspectionBase> PropertyInspectionType;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyInspectionType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyInspectionType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyInspectionTypeSpecified
        {
            get { return this.PropertyInspectionType != null && this.PropertyInspectionType.enumValue != PropertyInspectionBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form field used to capture additional information when the selection for Property Inspection Type is Other.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOString PropertyInspectionTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyInspectionTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyInspectionTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyInspectionTypeOtherDescriptionSpecified
        {
            get { return PropertyInspectionTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The date that the most recent valuation of the property was ordered.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMODate PropertyMostRecentValuationOrderDate;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyMostRecentValuationOrderDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyMostRecentValuationOrderDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyMostRecentValuationOrderDateSpecified
        {
            get { return PropertyMostRecentValuationOrderDate != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount that would be needed to replace or restore the property to its original condition.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOAmount PropertyReplacementValueAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyReplacementValueAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyReplacementValueAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyReplacementValueAmountSpecified
        {
            get { return PropertyReplacementValueAmount != null; }
            set { }
        }

        /// <summary>
        /// The age of the property valuation expressed in days, calculated by taking the current date minus the Property Valuation Effective Date.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOCount PropertyValuationAgeDaysCount;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyValuationAgeDaysCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyValuationAgeDaysCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyValuationAgeDaysCountSpecified
        {
            get { return PropertyValuationAgeDaysCount != null; }
            set { }
        }

        /// <summary>
        /// Statement of value of the property from a valid property valuation source.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOAmount PropertyValuationAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyValuationAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyValuationAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyValuationAmountSpecified
        {
            get { return PropertyValuationAmount != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to provide commentary on the property value analysis. 
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOString PropertyValuationCommentText;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyValuationCommentText element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyValuationCommentText element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyValuationCommentTextSpecified
        {
            get { return PropertyValuationCommentText != null; }
            set { }
        }

        /// <summary>
        /// Specifies the conclusions of the appraisal that there is a dependency on future repairs or activities.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOEnum<PropertyValuationConditionalConclusionBase> PropertyValuationConditionalConclusionType;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyValuationConditionalConclusionType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyValuationConditionalConclusionType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyValuationConditionalConclusionTypeSpecified
        {
            get { return this.PropertyValuationConditionalConclusionType != null && this.PropertyValuationConditionalConclusionType.enumValue != PropertyValuationConditionalConclusionBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Property Valuation Conditional Conclusion Type.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOString PropertyValuationConditionalConclusionTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyValuationConditionalConclusionTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyValuationConditionalConclusionTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyValuationConditionalConclusionTypeOtherDescriptionSpecified
        {
            get { return PropertyValuationConditionalConclusionTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Effective date of the property valuation on the subject property.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMODate PropertyValuationEffectiveDate;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyValuationEffectiveDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyValuationEffectiveDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyValuationEffectiveDateSpecified
        {
            get { return PropertyValuationEffectiveDate != null; }
            set { }
        }

        /// <summary>
        /// Specifies the form used to provide the property valuation.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOEnum<PropertyValuationFormBase> PropertyValuationFormType;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyValuationFormType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyValuationFormType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyValuationFormTypeSpecified
        {
            get { return this.PropertyValuationFormType != null && this.PropertyValuationFormType.enumValue != PropertyValuationFormBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Property Valuation Form Type.
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMOString PropertyValuationFormTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyValuationFormTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyValuationFormTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyValuationFormTypeOtherDescriptionSpecified
        {
            get { return PropertyValuationFormTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the method by which the property value was  assessed.
        /// </summary>
        [XmlElement(Order = 17)]
        public MISMOEnum<PropertyValuationMethodBase> PropertyValuationMethodType;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyValuationMethodType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyValuationMethodType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyValuationMethodTypeSpecified
        {
            get { return this.PropertyValuationMethodType != null && this.PropertyValuationMethodType.enumValue != PropertyValuationMethodBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture a description of the Property Valuation Method Type when Other is selected.
        /// </summary>
        [XmlElement(Order = 18)]
        public MISMOString PropertyValuationMethodTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyValuationMethodTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyValuationMethodTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyValuationMethodTypeOtherDescriptionSpecified
        {
            get { return PropertyValuationMethodTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Provides a categorization for valuation products in terms of USPAP compliance and level of professional services required.
        /// </summary>
        [XmlElement(Order = 19)]
        public MISMOEnum<PropertyValuationServiceBase> PropertyValuationServiceType;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyValuationServiceType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyValuationServiceType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyValuationServiceTypeSpecified
        {
            get { return this.PropertyValuationServiceType != null && this.PropertyValuationServiceType.enumValue != PropertyValuationServiceBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field which contains the description of the categorization for valuation products when Other is selected as the Property Valuation Service Type. 
        /// </summary>
        [XmlElement(Order = 20)]
        public MISMOString PropertyValuationServiceTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyValuationServiceTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyValuationServiceTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyValuationServiceTypeOtherDescriptionSpecified
        {
            get { return PropertyValuationServiceTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies that the subject property valuation amount is the amount at the point of origination or a subsequent evaluation.
        /// </summary>
        [XmlElement(Order = 21)]
        public MISMOEnum<PropertyValuationStateBase> PropertyValuationStateType;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyValuationStateType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyValuationStateType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyValuationStateTypeSpecified
        {
            get { return this.PropertyValuationStateType != null && this.PropertyValuationStateType.enumValue != PropertyValuationStateBase.Blank; }
            set { }
        }

        /// <summary>
        /// The dollar amount of the total cost of the repairs to be performed on the subject property.
        /// </summary>
        [XmlElement(Order = 22)]
        public MISMOAmount RepairsTotalCostAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the RepairsTotalCostAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RepairsTotalCostAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool RepairsTotalCostAmountSpecified
        {
            get { return RepairsTotalCostAmount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 23)]
        public PROPERTY_VALUATION_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
