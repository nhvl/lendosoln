namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class PROPERTY_VALUATION
    {
        /// <summary>
        /// Gets a value indicating whether the PROPERTY_VALUATION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AvmsSpecified
                    || this.ExtensionSpecified
                    || this.PropertyValuationDetailSpecified;
            }
        }

        /// <summary>
        /// A list of AVMs.
        /// </summary>
        [XmlElement("AVMS", Order = 0)]
        public AVMS Avms;

        /// <summary>
        /// Gets or sets a value indicating whether the AVM element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AVM element has been assigned a value.</value>
        [XmlIgnore]
        public bool AvmsSpecified
        {
            get { return this.Avms != null && this.Avms.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details on a property valuation.
        /// </summary>
        [XmlElement("PROPERTY_VALUATION_DETAIL", Order = 1)]
        public PROPERTY_VALUATION_DETAIL PropertyValuationDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyValuationDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyValuationDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyValuationDetailSpecified
        {
            get { return this.PropertyValuationDetail != null && this.PropertyValuationDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public PROPERTY_VALUATION_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
