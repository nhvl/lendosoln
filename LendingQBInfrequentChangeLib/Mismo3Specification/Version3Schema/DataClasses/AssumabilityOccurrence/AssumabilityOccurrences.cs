namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class ASSUMABILITY_OCCURRENCES
    {
        /// <summary>
        /// Gets a value indicating whether the ASSUMABILITY_OCCURRENCES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AssumabilityOccurrenceSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of assumption occurrences.
        /// </summary>
        [XmlElement("ASSUMABILITY_OCCURRENCE", Order = 0)]
		public List<ASSUMABILITY_OCCURRENCE> AssumabilityOccurrence = new List<ASSUMABILITY_OCCURRENCE>();

        /// <summary>
        /// Gets or sets a value indicating whether the Assumption Occurrence element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Assumption Occurrence element has been assigned a value.</value>
        [XmlIgnore]
        public bool AssumabilityOccurrenceSpecified
        {
            get { return this.AssumabilityOccurrence != null && this.AssumabilityOccurrence.Count(a => a != null && a.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public ASSUMABILITY_OCCURRENCES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
