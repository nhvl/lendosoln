namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class ADVANCE_SUMMARY
    {
        /// <summary>
        /// Gets a value indicating whether the ADVANCE_SUMMARY container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.PrincipalOrInterestAdvanceEndDateSpecified
                    || this.TotalAdvancesAmountSpecified
                    || this.TotalAdvancesExcludingPrincipalAndInterestAmountSpecified
                    || this.TotalRecoverableAdvancesAmountSpecified;
            }
        }

        /// <summary>
        /// The payment due date for which the servicer ceased advancing principal or interest.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMODate PrincipalOrInterestAdvanceEndDate;

        /// <summary>
        /// Gets or sets a value indicating whether the PrincipalOrInterestAdvanceEndDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrincipalOrInterestAdvanceEndDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrincipalOrInterestAdvanceEndDateSpecified
        {
            get { return PrincipalOrInterestAdvanceEndDate != null; }
            set { }
        }

        /// <summary>
        /// The total dollar amount of all advances for the loan.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOAmount TotalAdvancesAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the TotalAdvancesAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TotalAdvancesAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TotalAdvancesAmountSpecified
        {
            get { return TotalAdvancesAmount != null; }
            set { }
        }

        /// <summary>
        /// The total dollar amount of advances of fees or payments to keep the property in good standing before the foreclosure excluding principal and interest payments.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOAmount TotalAdvancesExcludingPrincipalAndInterestAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the TotalAdvancesExcludingPrincipalAndInterestAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TotalAdvancesExcludingPrincipalAndInterestAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TotalAdvancesExcludingPrincipalAndInterestAmountSpecified
        {
            get { return TotalAdvancesExcludingPrincipalAndInterestAmount != null; }
            set { }
        }

        /// <summary>
        /// Total recoverable monies advanced on the loan (i.e. delinquency expense, tax penalty, repairs, etc.).
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOAmount TotalRecoverableAdvancesAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the TotalRecoverableAdvancesAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TotalRecoverableAdvancesAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TotalRecoverableAdvancesAmountSpecified
        {
            get { return TotalRecoverableAdvancesAmount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public ADVANCE_SUMMARY_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
