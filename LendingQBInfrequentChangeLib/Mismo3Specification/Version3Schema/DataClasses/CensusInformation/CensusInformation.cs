namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CENSUS_INFORMATION
    {
        /// <summary>
        /// Gets a value indicating whether the CENSUS_INFORMATION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CensusBlockGroupIdentifierSpecified
                    || this.CensusBlockIdentifierSpecified
                    || this.CensusTractBaseIdentifierSpecified
                    || this.CensusTractIdentifierSpecified
                    || this.CensusTractSuffixIdentifierSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A single digit number which is the first digit of all the blocks which comprise the block group. A statistical subdivision of a census tract, a block group is the smallest geographic unit for which the Census Bureau tabulates sample data. Example: block group 3 consists of all blocks within a 2000 census tract numbering from 3000 to 3999. In 1990, block group 3 consisted of all blocks numbered from 301 to 399Z.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIdentifier CensusBlockGroupIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the CensusBlockGroupIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CensusBlockGroupIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool CensusBlockGroupIdentifierSpecified
        {
            get { return CensusBlockGroupIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A four-digit number to identify a census block. A subdivision of a census tract, a block is the smallest geographic unit for which the Census Bureau tabulates 100-percent data. Many blocks correspond to individual city blocks bounded by streets, but blocks - especially in rural areas -- may include many square miles and may have some boundaries that are not streets.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOIdentifier CensusBlockIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the CensusBlockIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CensusBlockIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool CensusBlockIdentifierSpecified
        {
            get { return CensusBlockIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A four-digit number used to identify a census tract. A census tract is a small, relatively permanent statistical subdivision of a county delineated by a local committee of census data users for the purpose of presenting data.Census tract numbers are always unique within a county and usually unique within an MA. Almost all census tract numbers range from 0001 to 9499. Leading zeros are not shown on the Census Bureau's maps or in its printed reports.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOIdentifier CensusTractBaseIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the CensusTractBaseIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CensusTractBaseIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool CensusTractBaseIdentifierSpecified
        {
            get { return CensusTractBaseIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Identifies census tract as defined by the U.S. Census Bureau where subject property is located.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOIdentifier CensusTractIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the CensusTractIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CensusTractIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool CensusTractIdentifierSpecified
        {
            get { return CensusTractIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Census tract suffixes may range from 01 to 98. Many census tracts do not have a suffix; in such cases, the suffix field is either left blank or is zero-filled. The decimal point separating the four-digit basic tract number from the two-digit suffix is shown in printed reports, in microfiche, and on census maps; in machine-readable files, the decimal point is implied.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOIdentifier CensusTractSuffixIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the CensusTractSuffixIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CensusTractSuffixIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool CensusTractSuffixIdentifierSpecified
        {
            get { return CensusTractSuffixIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 5)]
        public CENSUS_INFORMATION_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
