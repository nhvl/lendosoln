namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PHASE_STRUCTURE_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the PHASE_STRUCTURE_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ElevatorCountSpecified
                    || this.ExtensionSpecified
                    || this.PropertyStructureBuiltYearSpecified
                    || this.StoriesNumberSpecified;
            }
        }

        /// <summary>
        /// Number of elevators.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOCount ElevatorCount;

        /// <summary>
        /// Gets or sets a value indicating whether the ElevatorCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ElevatorCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ElevatorCountSpecified
        {
            get { return ElevatorCount != null; }
            set { }
        }

        /// <summary>
        /// The year in which the dwelling on the property was completed.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOYear PropertyStructureBuiltYear;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyStructureBuiltYear element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyStructureBuiltYear element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyStructureBuiltYearSpecified
        {
            get { return PropertyStructureBuiltYear != null; }
            set { }
        }

        /// <summary>
        /// Specifies the number of habitable levels above grade in a structure such as in a 2 story house, a cape cod with 1.5  stories or a 2 story condominium unit in a multi-level building.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMONumeric StoriesNumber;

        /// <summary>
        /// Gets or sets a value indicating whether the StoriesNumber element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the StoriesNumber element has been assigned a value.</value>
        [XmlIgnore]
        public bool StoriesNumberSpecified
        {
            get { return StoriesNumber != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public PHASE_STRUCTURE_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
