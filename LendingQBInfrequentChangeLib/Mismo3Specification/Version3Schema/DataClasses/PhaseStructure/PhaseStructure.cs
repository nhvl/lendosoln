namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class PHASE_STRUCTURE
    {
        /// <summary>
        /// Gets a value indicating whether the PHASE_STRUCTURE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.PhaseStructureCarStoragesSpecified
                    || this.PhaseStructureCommonElementsSpecified
                    || this.PhaseStructureConversionSpecified
                    || this.PhaseStructureDetailSpecified
                    || this.PhaseStructureHousingUnitInventoriesSpecified;
            }
        }

        /// <summary>
        /// Car storages in the phase structure.
        /// </summary>
        [XmlElement("PHASE_STRUCTURE_CAR_STORAGES", Order = 0)]
        public CAR_STORAGES PhaseStructureCarStorages;

        /// <summary>
        /// Gets or sets a value indicating whether the PhaseStructureCarStorages element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PhaseStructureCarStorages element has been assigned a value.</value>
        [XmlIgnore]
        public bool PhaseStructureCarStoragesSpecified
        {
            get { return this.PhaseStructureCarStorages != null && this.PhaseStructureCarStorages.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Common elements of the phase structure.
        /// </summary>
        [XmlElement("PHASE_STRUCTURE_COMMON_ELEMENTS", Order = 1)]
        public COMMON_ELEMENTS PhaseStructureCommonElements;

        /// <summary>
        /// Gets or sets a value indicating whether the PhaseStructureCommonElements element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PhaseStructureCommonElements element has been assigned a value.</value>
        [XmlIgnore]
        public bool PhaseStructureCommonElementsSpecified
        {
            get { return this.PhaseStructureCommonElements != null && this.PhaseStructureCommonElements.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Conversion related to the phase structure.
        /// </summary>
        [XmlElement("PHASE_STRUCTURE_CONVERSION", Order = 2)]
        public CONVERSION PhaseStructureConversion;

        /// <summary>
        /// Gets or sets a value indicating whether the PhaseStructureConversion element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PhaseStructureConversion element has been assigned a value.</value>
        [XmlIgnore]
        public bool PhaseStructureConversionSpecified
        {
            get { return this.PhaseStructureConversion != null && this.PhaseStructureConversion.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details on the phase structure.
        /// </summary>
        [XmlElement("PHASE_STRUCTURE_DETAIL", Order = 3)]
        public PHASE_STRUCTURE_DETAIL PhaseStructureDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the PhaseStructureDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PhaseStructureDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool PhaseStructureDetailSpecified
        {
            get { return this.PhaseStructureDetail != null && this.PhaseStructureDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Phase structure of the housing unit inventories.
        /// </summary>
        [XmlElement("PHASE_STRUCTURE_HOUSING_UNIT_INVENTORIES", Order = 4)]
        public HOUSING_UNIT_INVENTORIES PhaseStructureHousingUnitInventories;

        /// <summary>
        /// Gets or sets a value indicating whether the PhaseStructureHousingUnitInventories element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PhaseStructureHousingUnitInventories element has been assigned a value.</value>
        [XmlIgnore]
        public bool PhaseStructureHousingUnitInventoriesSpecified
        {
            get { return this.PhaseStructureHousingUnitInventories != null && this.PhaseStructureHousingUnitInventories.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 5)]
        public PHASE_STRUCTURE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
