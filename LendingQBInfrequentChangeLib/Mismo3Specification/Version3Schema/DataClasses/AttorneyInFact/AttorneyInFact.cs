namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class ATTORNEY_IN_FACT
    {
        /// <summary>
        /// Gets a value indicating whether the container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AttorneyInFactSigningCapacityTextSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// The specific text which describes the signing capacity of the individual acting as attorney-in-fact.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString AttorneyInFactSigningCapacityText;

        /// <summary>
        /// Gets or sets a value indicating whether the AttorneyInFactSigningCapacityText element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AttorneyInFactSigningCapacityText element has been assigned a value.</value>
        [XmlIgnore]
        public bool AttorneyInFactSigningCapacityTextSpecified
        {
            get { return AttorneyInFactSigningCapacityText != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public ATTORNEY_IN_FACT_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
