namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class NEW_IMPROVEMENTS
    {
        /// <summary>
        /// Gets a value indicating whether the NEW_IMPROVEMENTS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.NewImprovementSpecified;
            }
        }

        /// <summary>
        /// A collection of new improvements.
        /// </summary>
        [XmlElement("NEW_IMPROVEMENT", Order = 0)]
		public List<NEW_IMPROVEMENT> NewImprovement = new List<NEW_IMPROVEMENT>();

        /// <summary>
        /// Gets or sets a value indicating whether the NewImprovement element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NewImprovement element has been assigned a value.</value>
        [XmlIgnore]
        public bool NewImprovementSpecified
        {
            get { return this.NewImprovement != null && this.NewImprovement.Count(n => n != null && n.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public NEW_IMPROVEMENTS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
