namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class NEW_IMPROVEMENT
    {
        /// <summary>
        /// Gets a value indicating whether the NEW_IMPROVEMENT container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.NewImprovementCostAmountSpecified
                    || this.NewImprovementCostDescriptionSpecified
                    || this.NewImprovementTypeOtherDescriptionSpecified
                    || this.NewImprovementTypeSpecified
                    || this.PricePerSquareFootAmountSpecified
                    || this.SquareFeetNumberSpecified;
            }
        }

        /// <summary>
        /// The dollar value of total estimated costs to reproduce new the property improvement.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount NewImprovementCostAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the NewImprovementCostAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NewImprovementCostAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool NewImprovementCostAmountSpecified
        {
            get { return NewImprovementCostAmount != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture additional information about new property improvement reproduction costs.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString NewImprovementCostDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the NewImprovementCostDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NewImprovementCostDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool NewImprovementCostDescriptionSpecified
        {
            get { return NewImprovementCostDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the type of property improvement whose costs to reproduce are being estimated.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<NewImprovementBase> NewImprovementType;

        /// <summary>
        /// Gets or sets a value indicating whether the NewImprovementType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NewImprovementType element has been assigned a value.</value>
        [XmlIgnore]
        public bool NewImprovementTypeSpecified
        {
            get { return this.NewImprovementType != null && this.NewImprovementType.enumValue != NewImprovementBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture additional information when Other is selected for New Improvement Type.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString NewImprovementTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the NewImprovementTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NewImprovementTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool NewImprovementTypeOtherDescriptionSpecified
        {
            get { return NewImprovementTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The price in terms of dollars per square foot. This is a ratio between a price and an area, but it is a monetary value rather than a factor or rate. (i.e. sales price per gross living area of a property).
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOAmount PricePerSquareFootAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PricePerSquareFootAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PricePerSquareFootAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PricePerSquareFootAmountSpecified
        {
            get { return PricePerSquareFootAmount != null; }
            set { }
        }

        /// <summary>
        /// Identifies the total area measured in square feet.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMONumeric SquareFeetNumber;

        /// <summary>
        /// Gets or sets a value indicating whether the SquareFeetNumber element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SquareFeetNumber element has been assigned a value.</value>
        [XmlIgnore]
        public bool SquareFeetNumberSpecified
        {
            get { return SquareFeetNumber != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 6)]
        public NEW_IMPROVEMENT_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
