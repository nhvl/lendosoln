namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class BANKRUPTCY_DISPOSITIONS
    {
        /// <summary>
        /// Gets a value indicating whether the BANKRUPTCY_DISPOSITIONS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BankruptcyDispositionSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of bankruptcy dispositions.
        /// </summary>
        [XmlElement("BANKRUPTCY_DISPOSITION", Order = 0)]
		public List<BANKRUPTCY_DISPOSITION> BankruptcyDisposition = new List<BANKRUPTCY_DISPOSITION>();

        /// <summary>
        /// Gets or sets a value indicating whether the BankruptcyDisposition element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BankruptcyDisposition element has been assigned a value.</value>
        [XmlIgnore]
        public bool BankruptcyDispositionSpecified
        {
            get { return this.BankruptcyDisposition != null && this.BankruptcyDisposition.Count(b => b != null && b.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public BANKRUPTCY_DISPOSITIONS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
