namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class BANKRUPTCY_DISPOSITION
    {
        /// <summary>
        /// Gets a value indicating whether the BANKRUPTCY_DISPOSITION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BankruptcyDispositionDateSpecified
                    || this.BankruptcyDispositionTypeOtherDescriptionSpecified
                    || this.BankruptcyDispositionTypeSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// The date when the Bankruptcy Disposition Type occurred.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMODate BankruptcyDispositionDate;

        /// <summary>
        /// Gets or sets a value indicating whether the BankruptcyDispositionDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BankruptcyDispositionDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool BankruptcyDispositionDateSpecified
        {
            get { return BankruptcyDispositionDate != null; }
            set { }
        }

        /// <summary>
        /// The administrative standing of the bankruptcy case based upon a court ruling at a procedural milestone.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<BankruptcyDispositionBase> BankruptcyDispositionType;

        /// <summary>
        /// Gets or sets a value indicating whether the BankruptcyDispositionType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BankruptcyDispositionType element has been assigned a value.</value>
        [XmlIgnore]
        public bool BankruptcyDispositionTypeSpecified
        {
            get { return this.BankruptcyDispositionType != null && this.BankruptcyDispositionType.enumValue != BankruptcyDispositionBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture additional information when Other is selected for Bankruptcy Disposition Type.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString BankruptcyDispositionTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the BankruptcyDispositionTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BankruptcyDispositionTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool BankruptcyDispositionTypeOtherDescriptionSpecified
        {
            get { return BankruptcyDispositionTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public BANKRUPTCY_DISPOSITION_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
