namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class REPORTING_INFORMATION
    {
        /// <summary>
        /// Gets a value indicating whether the REPORTING_INFORMATION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ReportingInformationIdentifierSpecified
                    || this.ReportingInformationNameSpecified;
            }
        }

        /// <summary>
        /// A generic placeholder for a value to be associated with a reporting and/or billing description for the Receiving Party to utilize for a transaction exchange.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIdentifier ReportingInformationIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the ReportingInformationIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ReportingInformationIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool ReportingInformationIdentifierSpecified
        {
            get { return ReportingInformationIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A generic placeholder to add reporting and/or billing descriptions for the Receiving Party to utilize in transactions.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString ReportingInformationName;

        /// <summary>
        /// Gets or sets a value indicating whether the ReportingInformationName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ReportingInformationName element has been assigned a value.</value>
        [XmlIgnore]
        public bool ReportingInformationNameSpecified
        {
            get { return ReportingInformationName != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public REPORTING_INFORMATION_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
