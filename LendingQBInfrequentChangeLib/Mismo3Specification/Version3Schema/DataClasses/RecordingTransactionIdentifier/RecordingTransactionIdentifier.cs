namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class RECORDING_TRANSACTION_IDENTIFIER
    {
        /// <summary>
        /// Gets a value indicating whether the RECORDING_TRANSACTION_IDENTIFIER container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.RecordingTransactionIdentifierSpecified
                    || this.RecordingTransactionIdentifierTypeOtherDescriptionSpecified
                    || this.RecordingTransactionIdentifierTypeSpecified;
            }
        }

        /// <summary>
        /// The actual identifying alphanumeric entry.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIdentifier RecordingTransactionIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the RecordingTransactionIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RecordingTransactionIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool RecordingTransactionIdentifierSpecified
        {
            get { return RecordingTransactionIdentifier != null; }
            set { }
        }

        /// <summary>
        /// This enumerated attribute identifies what agency the transaction is intended for.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<RecordingTransactionIdentifierBase> RecordingTransactionIdentifierType;

        /// <summary>
        /// Gets or sets a value indicating whether the RecordingTransactionIdentifierType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RecordingTransactionIdentifierType element has been assigned a value.</value>
        [XmlIgnore]
        public bool RecordingTransactionIdentifierTypeSpecified
        {
            get { return this.RecordingTransactionIdentifierType != null && this.RecordingTransactionIdentifierType.enumValue != RecordingTransactionIdentifierBase.Blank; }
            set { }
        }

        /// <summary>
        /// A description of the type of transaction when Other is selected in the enumerated list.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString RecordingTransactionIdentifierTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the RecordingTransactionIdentifierTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RecordingTransactionIdentifierTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool RecordingTransactionIdentifierTypeOtherDescriptionSpecified
        {
            get { return RecordingTransactionIdentifierTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public RECORDING_TRANSACTION_IDENTIFIER_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
