namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class RESIDENTIAL_RENTAL_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the RESIDENTIAL_RENTAL_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DataSourceDescriptionSpecified
                    || this.ExpenseAdjustedMonthlyRentAmountSpecified
                    || this.ExtensionSpecified
                    || this.LeaseExpirationDateSpecified
                    || this.LeaseStartDateSpecified
                    || this.MarketAdjustedMonthlyRentAmountSpecified
                    || this.MonthlyFurnitureRentAmountSpecified
                    || this.MonthlyRentAmountSpecified
                    || this.RentalMonthlyUtilitiesCostAmountSpecified
                    || this.RentTotalAdjustmentAmountSpecified
                    || this.RentTotalAdjustmentGrossPercentSpecified
                    || this.RentTotalAdjustmentNetPercentSpecified
                    || this.RentTotalAdjustmentPositiveIndicatorSpecified;
            }
        }

        /// <summary>
        /// A free-form text field used to describe the source of the information. 
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString DataSourceDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the DataSourceDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DataSourceDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool DataSourceDescriptionSpecified
        {
            get { return DataSourceDescription != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount paid monthly for rent less the amounts paid monthly for utilities and furniture.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOAmount ExpenseAdjustedMonthlyRentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ExpenseAdjustedMonthlyRentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ExpenseAdjustedMonthlyRentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExpenseAdjustedMonthlyRentAmountSpecified
        {
            get { return ExpenseAdjustedMonthlyRentAmount != null; }
            set { }
        }

        /// <summary>
        /// The date the lease expires.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMODate LeaseExpirationDate;

        /// <summary>
        /// Gets or sets a value indicating whether the LeaseExpirationDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LeaseExpirationDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool LeaseExpirationDateSpecified
        {
            get { return LeaseExpirationDate != null; }
            set { }
        }

        /// <summary>
        /// The date the lease starts.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMODate LeaseStartDate;

        /// <summary>
        /// Gets or sets a value indicating whether the LeaseStartDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LeaseStartDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool LeaseStartDateSpecified
        {
            get { return LeaseStartDate != null; }
            set { }
        }

        /// <summary>
        /// The dollar value of the estimated market rent of a comparable rental property determined by applying market driven rent adjustments during the application of a Rent Schedule Comparison.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOAmount MarketAdjustedMonthlyRentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the MarketAdjustedMonthlyRentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MarketAdjustedMonthlyRentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool MarketAdjustedMonthlyRentAmountSpecified
        {
            get { return MarketAdjustedMonthlyRentAmount != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount paid monthly for the rental of furniture.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOAmount MonthlyFurnitureRentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the MonthlyFurnitureRentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MonthlyFurnitureRentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool MonthlyFurnitureRentAmountSpecified
        {
            get { return MonthlyFurnitureRentAmount != null; }
            set { }
        }

        /// <summary>
        /// The amount paid by the borrower or co-borrower for the rental of a residence property. This information is normally provided during interview with the borrower or obtained from a loan application, and is sometimes verified with the residence property landlord.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOAmount MonthlyRentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the MonthlyRentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MonthlyRentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool MonthlyRentAmountSpecified
        {
            get { return MonthlyRentAmount != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount paid monthly for utilities associated with a rental property or unit.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOAmount RentalMonthlyUtilitiesCostAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the RentalMonthlyUtilitiesCostAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RentalMonthlyUtilitiesCostAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool RentalMonthlyUtilitiesCostAmountSpecified
        {
            get { return RentalMonthlyUtilitiesCostAmount != null; }
            set { }
        }

        /// <summary>
        /// The dollar value of the total adjustments made to a comparable rental during the application of a Rent Schedule Comparison in the process of determining the monthly market rent of the comparable.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOAmount RentTotalAdjustmentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the RentTotalAdjustmentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RentTotalAdjustmentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool RentTotalAdjustmentAmountSpecified
        {
            get { return RentTotalAdjustmentAmount != null; }
            set { }
        }

        /// <summary>
        /// The percentage of the gross rent adjustments (i.e. sum of the absolute adjustment values) to the Expense Adjusted Monthly Rent of a comparable rental property.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOPercent RentTotalAdjustmentGrossPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the RentTotalAdjustmentGrossPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RentTotalAdjustmentGrossPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool RentTotalAdjustmentGrossPercentSpecified
        {
            get { return RentTotalAdjustmentGrossPercent != null; }
            set { }
        }

        /// <summary>
        /// The percentage of the net rent adjustments to the Expense Adjusted Monthly Rent of a comparable rental property.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOPercent RentTotalAdjustmentNetPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the RentTotalAdjustmentNetPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RentTotalAdjustmentNetPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool RentTotalAdjustmentNetPercentSpecified
        {
            get { return RentTotalAdjustmentNetPercent != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether the total market rent adjustments made to a comparable property during the application of the Rent Schedule Comparison were positive.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOIndicator RentTotalAdjustmentPositiveIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the RentTotalAdjustmentPositiveIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RentTotalAdjustmentPositiveIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool RentTotalAdjustmentPositiveIndicatorSpecified
        {
            get { return RentTotalAdjustmentPositiveIndicator != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 12)]
        public RESIDENTIAL_RENTAL_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
