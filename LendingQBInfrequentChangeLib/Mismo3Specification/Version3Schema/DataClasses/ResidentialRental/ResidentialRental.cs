namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class RESIDENTIAL_RENTAL
    {
        /// <summary>
        /// Gets a value indicating whether the RESIDENTIAL_RENTAL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.RentAdjustmentsSpecified
                    || this.ResidentialRentalDetailSpecified;
            }
        }

        /// <summary>
        /// Information on adjustments to the rent.
        /// </summary>
        [XmlElement("RENT_ADJUSTMENTS", Order = 0)]
        public RENT_ADJUSTMENTS RentAdjustments;

        /// <summary>
        /// Gets or sets a value indicating whether the RentAdjustments element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RentAdjustments element has been assigned a value.</value>
        [XmlIgnore]
        public bool RentAdjustmentsSpecified
        {
            get { return this.RentAdjustments != null && this.RentAdjustments.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Information on the rental situation.
        /// </summary>
        [XmlElement("RESIDENTIAL_RENTAL_DETAIL", Order = 1)]
        public RESIDENTIAL_RENTAL_DETAIL ResidentialRentalDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the ResidentialRentalDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ResidentialRentalDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool ResidentialRentalDetailSpecified
        {
            get { return this.ResidentialRentalDetail != null && this.ResidentialRentalDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public RESIDENTIAL_RENTAL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
