namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class GOVERNMENT_LOAN
    {
        /// <summary>
        /// Gets a value indicating whether the GOVERNMENT_LOAN container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AgencyProgramDescriptionSpecified
                    || this.BorrowerFinancedFHADiscountPointsAmountSpecified
                    || this.BorrowerFundingFeePercentSpecified
                    || this.BorrowerHomeInspectionChosenIndicatorSpecified
                    || this.BorrowerPaidFHA_VAClosingCostsAmountSpecified
                    || this.BorrowerPaidFHA_VAClosingCostsPercentSpecified
                    || this.DaysToFHA_MIEligibilityCountSpecified
                    || this.ExtensionSpecified
                    || this.FHA_MIPremiumRefundAmountSpecified
                    || this.FHAAlimonyLiabilityTreatmentTypeSpecified
                    || this.FHAAnniversaryDateSpecified
                    || this.FHAAnnualPremiumAmountSpecified
                    || this.FHAAnnualPremiumPercentSpecified
                    || this.FHAAssignmentDateSpecified
                    || this.FHAAutomatedDataProcessingIdentifierSpecified
                    || this.FHACoverageRenewalRatePercentSpecified
                    || this.FHAEndorsementDateSpecified
                    || this.FHAEnergyRelatedRepairsOrImprovementsAmountSpecified
                    || this.FHAGeneralServicesAdministrationIdentifierSpecified
                    || this.FHAInsuranceProgramTypeSpecified
                    || this.FHALimitedDenialParticipationIdentifierSpecified
                    || this.FHALoanLenderIdentifierSpecified
                    || this.FHALoanSponsorIdentifierSpecified
                    || this.FHANonOwnerOccupancyRiderRule248IndicatorSpecified
                    || this.FHAPendingPremiumAmountSpecified
                    || this.FHAPendingPremiumChangeDateSpecified
                    || this.FHAPremiumAnniversaryYearToDateRemittanceAmountSpecified
                    || this.FHARefinanceInterestOnExistingLienAmountSpecified
                    || this.FHARefinanceOriginalExistingFHACaseIdentifierSpecified
                    || this.FHARefinanceOriginalExistingUpfrontMIPremiumAmountSpecified
                    || this.FHAUnderwriterComputerizedHomesUnderwritingSystemIdentifierSpecified
                    || this.FHAUpfrontPremiumAmountSpecified
                    || this.FHAUpfrontPremiumPercentSpecified
                    || this.GovernmentLoanApplicationTypeSpecified
                    || this.GovernmentMortgageCreditCertificateAmountSpecified
                    || this.GovernmentRefinanceTypeOtherDescriptionSpecified
                    || this.GovernmentRefinanceTypeSpecified
                    || this.HUDAdequateAvailableAssetsIndicatorSpecified
                    || this.HUDAdequateEffectiveIncomeIndicatorSpecified
                    || this.HUDCreditCharacteristicsIndicatorSpecified
                    || this.HUDStableEffectiveIncomeIndicatorSpecified
                    || this.MasterCertificateOfReasonablenumValueIdentifierSpecified
                    || this.OtherPartyPaidFHA_VAClosingCostsAmountSpecified
                    || this.OtherPartyPaidFHA_VAClosingCostsPercentSpecified
                    || this.PreviousVAHomeLoanIndicatorSpecified
                    || this.PropertyEnergyEfficientHomeIndicatorSpecified
                    || this.RuralHousingConditionalGuarantyExpirationDateSpecified
                    || this.RuralHousingConditionalGuarantyInterestRatePercentSpecified
                    || this.SectionOfActTypeOtherDescriptionSpecified
                    || this.SectionOfActTypeSpecified
                    || this.SellerPaidFHA_VAClosingCostsPercentSpecified
                    || this.SoldUnderHUDSingleFamilyPropertyDispositionProgramIndicatorSpecified
                    || this.USDA198021RuralHousingAdjustedAnnualHouseholdIncomeAmountSpecified
                    || this.VAAppraisalTypeOtherDescriptionSpecified
                    || this.VAAppraisalTypeSpecified
                    || this.VABorrowerCoBorrowerMarriedIndicatorSpecified
                    || this.VAClaimFolderIdentifierSpecified
                    || this.VAEntitlementAmountSpecified
                    || this.VAEntitlementIdentifierSpecified
                    || this.VAFundingFeeExemptionTypeOtherDescriptionSpecified
                    || this.VAFundingFeeExemptionTypeSpecified
                    || this.VAHouseholdSizeCountSpecified
                    || this.VALoanProcedureTypeOtherDescriptionSpecified
                    || this.VALoanProcedureTypeSpecified
                    || this.VALoanProgramTypeOtherDescriptionSpecified
                    || this.VALoanProgramTypeSpecified
                    || this.VAMaintenanceExpenseMonthlyAmountSpecified
                    || this.VAReasonablenumValueImprovementsCompletionIndicatorSpecified
                    || this.VAResidualIncomeAmountSpecified
                    || this.VAResidualIncomeGuidelineAmountSpecified
                    || this.VATitleVestingTypeOtherDescriptionSpecified
                    || this.VATitleVestingTypeSpecified
                    || this.VAUtilityExpenseMonthlyAmountSpecified;
            }
        }

        /// <summary>
        /// Describes the federal, state, or local agency program under which the loan is originated.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString AgencyProgramDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the AgencyProgramDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AgencyProgramDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool AgencyProgramDescriptionSpecified
        {
            get { return AgencyProgramDescription != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of discount points allowable by the applicable FHA guidelines to be financed on a FHA no cash out refinance. Used for FHA loans. Note: Discount points financed and not financed by borrower are equal to the Discount Points Total Amount Paid by Borrower - FHA.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOAmount BorrowerFinancedFHADiscountPointsAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the BorrowerFinancedFHADiscountPointsAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BorrowerFinancedFHADiscountPointsAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool BorrowerFinancedFHADiscountPointsAmountSpecified
        {
            get { return BorrowerFinancedFHADiscountPointsAmount != null; }
            set { }
        }

        /// <summary>
        /// The funding fee percentage due from the borrower. The fee is entered as a percentage.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOPercent BorrowerFundingFeePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the BorrowerFundingFeePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BorrowerFundingFeePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool BorrowerFundingFeePercentSpecified
        {
            get { return BorrowerFundingFeePercent != null; }
            set { }
        }

        /// <summary>
        /// When true indicates that borrower(s) have chosen to have a home inspection performed.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOIndicator BorrowerHomeInspectionChosenIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the BorrowerHomeInspectionChosenIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BorrowerHomeInspectionChosenIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool BorrowerHomeInspectionChosenIndicatorSpecified
        {
            get { return BorrowerHomeInspectionChosenIndicator != null; }
            set { }
        }

        /// <summary>
        /// The estimated dollar amount of closing costs that will be paid by the borrower on the subject property excluding discount points and prepaid items. Used for FHA and VA loans.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOAmount BorrowerPaidFHA_VAClosingCostsAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the BorrowerPaidFHA_VAClosingCostsAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BorrowerPaidFHA_VAClosingCostsAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool BorrowerPaidFHA_VAClosingCostsAmountSpecified
        {
            get { return BorrowerPaidFHA_VAClosingCostsAmount != null; }
            set { }
        }

        /// <summary>
        /// The percentage of the estimated closing costs that will be paid by the borrower on the subject property excluding discount points and prepaid amount.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOPercent BorrowerPaidFHA_VAClosingCostsPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the BorrowerPaidFHA_VAClosingCostsPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BorrowerPaidFHA_VAClosingCostsPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool BorrowerPaidFHA_VAClosingCostsPercentSpecified
        {
            get { return BorrowerPaidFHA_VAClosingCostsPercent != null; }
            set { }
        }

        /// <summary>
        /// Specifies the number of days in which the borrower will be determined to be eligible for insurance under the National Housing Act.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOCount DaysToFHA_MIEligibilityCount;

        /// <summary>
        /// Gets or sets a value indicating whether the DaysToFHA_MIEligibilityCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DaysToFHA_MIEligibilityCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool DaysToFHA_MIEligibilityCountSpecified
        {
            get { return DaysToFHA_MIEligibilityCount != null; }
            set { }
        }

        /// <summary>
        /// For refinance loans, the amount of the MIP that will be refunded to the borrower.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOAmount FHA_MIPremiumRefundAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the FHA_MIPremiumRefundAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FHA_MIPremiumRefundAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool FHA_MIPremiumRefundAmountSpecified
        {
            get { return FHA_MIPremiumRefundAmount != null; }
            set { }
        }

        /// <summary>
        /// Identifies how the borrower or lender prefers that the judgment process treat alimony in an FHA transaction.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOEnum<FHAAlimonyLiabilityTreatmentBase> FHAAlimonyLiabilityTreatmentType;

        /// <summary>
        /// Gets or sets a value indicating whether the FHAAlimonyLiabilityTreatmentType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FHAAlimonyLiabilityTreatmentType element has been assigned a value.</value>
        [XmlIgnore]
        public bool FHAAlimonyLiabilityTreatmentTypeSpecified
        {
            get { return this.FHAAlimonyLiabilityTreatmentType != null && this.FHAAlimonyLiabilityTreatmentType.enumValue != FHAAlimonyLiabilityTreatmentBase.Blank; }
            set { }
        }

        /// <summary>
        /// Date the next premium change will be effective (YYYY-MM) for an FHA insured loan.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMODate FHAAnniversaryDate;

        /// <summary>
        /// Gets or sets a value indicating whether the FHAAnniversaryDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FHAAnniversaryDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool FHAAnniversaryDateSpecified
        {
            get { return FHAAnniversaryDate != null; }
            set { }
        }

        /// <summary>
        /// Amount of FHA insurance paid annually.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOAmount FHAAnnualPremiumAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the FHAAnnualPremiumAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FHAAnnualPremiumAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool FHAAnnualPremiumAmountSpecified
        {
            get { return FHAAnnualPremiumAmount != null; }
            set { }
        }

        /// <summary>
        /// The mortgage insurance premium (MIP) percentage used to compute the annual FHA premium amount.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOPercent FHAAnnualPremiumPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the FHAAnnualPremiumPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FHAAnnualPremiumPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool FHAAnnualPremiumPercentSpecified
        {
            get { return FHAAnnualPremiumPercent != null; }
            set { }
        }

        /// <summary>
        /// The date the lender registers the loan through FHA connection. (This date can impact the upfront and renewal MI rates for FHA Loans.).
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMODate FHAAssignmentDate;

        /// <summary>
        /// Gets or sets a value indicating whether the FHAAssignmentDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FHAAssignmentDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool FHAAssignmentDateSpecified
        {
            get { return FHAAssignmentDate != null; }
            set { }
        }

        /// <summary>
        /// The unique identifier for the Automated Data Processing (ADP) code used for Direct Endorsement (DE) processing of FHA loans.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOIdentifier FHAAutomatedDataProcessingIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the FHAAutomatedDataProcessingIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FHAAutomatedDataProcessingIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool FHAAutomatedDataProcessingIdentifierSpecified
        {
            get { return FHAAutomatedDataProcessingIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Indicates the percentage of the renewal mortgage insurance premiums for FHA loans.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOPercent FHACoverageRenewalRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the FHACoverageRenewalRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FHACoverageRenewalRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool FHACoverageRenewalRatePercentSpecified
        {
            get { return FHACoverageRenewalRatePercent != null; }
            set { }
        }

        /// <summary>
        /// The date of the FHA mortgage insurance certificate endorsement.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMODate FHAEndorsementDate;

        /// <summary>
        /// Gets or sets a value indicating whether the FHAEndorsementDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FHAEndorsementDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool FHAEndorsementDateSpecified
        {
            get { return FHAEndorsementDate != null; }
            set { }
        }

        /// <summary>
        /// Dollar amount considered for energy related items.
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMOAmount FHAEnergyRelatedRepairsOrImprovementsAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the FHAEnergyRelatedRepairsOrImprovementsAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FHAEnergyRelatedRepairsOrImprovementsAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool FHAEnergyRelatedRepairsOrImprovementsAmountSpecified
        {
            get { return FHAEnergyRelatedRepairsOrImprovementsAmount != null; }
            set { }
        }

        /// <summary>
        /// The General Services Administrations (GSA) identifier from the List of Parties Excluded from Federal Procurement or Non-procurement Programs on the FHA Mortgage Credit Analysis Worksheet document.
        /// </summary>
        [XmlElement(Order = 17)]
        public MISMOIdentifier FHAGeneralServicesAdministrationIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the FHAGeneralServicesAdministrationIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FHAGeneralServicesAdministrationIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool FHAGeneralServicesAdministrationIdentifierSpecified
        {
            get { return FHAGeneralServicesAdministrationIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Type of FHA mortgage protection insurance program.
        /// </summary>
        [XmlElement(Order = 18)]
        public MISMOEnum<FHAInsuranceProgramBase> FHAInsuranceProgramType;

        /// <summary>
        /// Gets or sets a value indicating whether the FHAInsuranceProgramType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FHAInsuranceProgramType element has been assigned a value.</value>
        [XmlIgnore]
        public bool FHAInsuranceProgramTypeSpecified
        {
            get { return this.FHAInsuranceProgramType != null && this.FHAInsuranceProgramType.enumValue != FHAInsuranceProgramBase.Blank; }
            set { }
        }

        /// <summary>
        /// Identifier obtained from review of HUDs Limited Denial of Participation (LDP) placed on the FHA Mortgage Credit Analysis Worksheet document.
        /// </summary>
        [XmlElement(Order = 19)]
        public MISMOIdentifier FHALimitedDenialParticipationIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the FHALimitedDenialParticipationIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FHALimitedDenialParticipationIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool FHALimitedDenialParticipationIdentifierSpecified
        {
            get { return FHALimitedDenialParticipationIdentifier != null; }
            set { }
        }

        /// <summary>
        /// An FHA-assigned identifier of the originator making an underwriting request using the FHA Total Scorecard program.
        /// </summary>
        [XmlElement(Order = 20)]
        public MISMOIdentifier FHALoanLenderIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the FHALoanLenderIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FHALoanLenderIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool FHALoanLenderIdentifierSpecified
        {
            get { return FHALoanLenderIdentifier != null; }
            set { }
        }

        /// <summary>
        /// An FHA-assigned identifier of the lender acting as an approved sponsor for the originator making the underwriting request using the FHA Total Scorecard program.
        /// </summary>
        [XmlElement(Order = 21)]
        public MISMOIdentifier FHALoanSponsorIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the FHALoanSponsorIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FHALoanSponsorIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool FHALoanSponsorIdentifierSpecified
        {
            get { return FHALoanSponsorIdentifier != null; }
            set { }
        }

        /// <summary>
        /// On the FHA Non Owner Occupancy Rider, this indicates that the Borrower is an Indian Tribe as provided in Section 248 of the National Housing Act or a serviceperson who is unable to occupy the Property because of his or her duty assignment as provided in Section 216 or Subsection(b)(4) or (f) of Section 222 of the National Housing Act.
        /// </summary>
        [XmlElement(Order = 22)]
        public MISMOIndicator FHANonOwnerOccupancyRiderRule248Indicator;

        /// <summary>
        /// Gets or sets a value indicating whether the FHANonOwnerOccupancyRiderRule248Indicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FHANonOwnerOccupancyRiderRule248Indicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool FHANonOwnerOccupancyRiderRule248IndicatorSpecified
        {
            get { return FHANonOwnerOccupancyRiderRule248Indicator != null; }
            set { }
        }

        /// <summary>
        /// New FHA premium payment amount.
        /// </summary>
        [XmlElement(Order = 23)]
        public MISMOAmount FHAPendingPremiumAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the FHAPendingPremiumAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FHAPendingPremiumAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool FHAPendingPremiumAmountSpecified
        {
            get { return FHAPendingPremiumAmount != null; }
            set { }
        }

        /// <summary>
        /// Effective date of pending premium amount for an FHA insured loan.
        /// </summary>
        [XmlElement(Order = 24)]
        public MISMODate FHAPendingPremiumChangeDate;

        /// <summary>
        /// Gets or sets a value indicating whether the FHAPendingPremiumChangeDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FHAPendingPremiumChangeDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool FHAPendingPremiumChangeDateSpecified
        {
            get { return FHAPendingPremiumChangeDate != null; }
            set { }
        }

        /// <summary>
        /// The amount of FHA Mortgage Insurance Premium (MIP) paid in current anniversary year.
        /// </summary>
        [XmlElement(Order = 25)]
        public MISMOAmount FHAPremiumAnniversaryYearToDateRemittanceAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the FHAPremiumAnniversaryYearToDateRemittanceAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FHAPremiumAnniversaryYearToDateRemittanceAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool FHAPremiumAnniversaryYearToDateRemittanceAmountSpecified
        {
            get { return FHAPremiumAnniversaryYearToDateRemittanceAmount != null; }
            set { }
        }

        /// <summary>
        /// Dollar amount of interest on existing lien, for FHA refinance.
        /// </summary>
        [XmlElement(Order = 26)]
        public MISMOAmount FHARefinanceInterestOnExistingLienAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the FHARefinanceInterestOnExistingLienAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FHARefinanceInterestOnExistingLienAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool FHARefinanceInterestOnExistingLienAmountSpecified
        {
            get { return FHARefinanceInterestOnExistingLienAmount != null; }
            set { }
        }

        /// <summary>
        /// Original FHA Case Number for refinance.
        /// </summary>
        [XmlElement(Order = 27)]
        public MISMOIdentifier FHARefinanceOriginalExistingFHACaseIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the FHARefinanceOriginalExistingFHACaseIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FHARefinanceOriginalExistingFHACaseIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool FHARefinanceOriginalExistingFHACaseIdentifierSpecified
        {
            get { return FHARefinanceOriginalExistingFHACaseIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Amount paid for Original Upfront MIP for refinance loan.
        /// </summary>
        [XmlElement(Order = 28)]
        public MISMOAmount FHARefinanceOriginalExistingUpfrontMIPremiumAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the FHARefinanceOriginalExistingUpfrontMIPremiumAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FHARefinanceOriginalExistingUpfrontMIPremiumAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool FHARefinanceOriginalExistingUpfrontMIPremiumAmountSpecified
        {
            get { return FHARefinanceOriginalExistingUpfrontMIPremiumAmount != null; }
            set { }
        }

        /// <summary>
        /// The FHA assigned number that identifies the Direct Endorsement underwriter authorized to approve the loan on behalf of the FHA. (Frequently referred to as the CHUMS ID.).
        /// </summary>
        [XmlElement(Order = 29)]
        public MISMOIdentifier FHAUnderwriterComputerizedHomesUnderwritingSystemIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the FHAUnderwriterComputerizedHomesUnderwritingSystemIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FHAUnderwriterComputerizedHomesUnderwritingSystemIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool FHAUnderwriterComputerizedHomesUnderwritingSystemIdentifierSpecified
        {
            get { return FHAUnderwriterComputerizedHomesUnderwritingSystemIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Amount of FHA mortgage insurance paid at closing.
        /// </summary>
        [XmlElement(Order = 30)]
        public MISMOAmount FHAUpfrontPremiumAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the FHAUpfrontPremiumAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FHAUpfrontPremiumAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool FHAUpfrontPremiumAmountSpecified
        {
            get { return FHAUpfrontPremiumAmount != null; }
            set { }
        }

        /// <summary>
        /// The percentage of upfront MIP, used for FHA loans.
        /// </summary>
        [XmlElement(Order = 31)]
        public MISMOPercent FHAUpfrontPremiumPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the FHAUpfrontPremiumPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FHAUpfrontPremiumPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool FHAUpfrontPremiumPercentSpecified
        {
            get { return FHAUpfrontPremiumPercent != null; }
            set { }
        }

        /// <summary>
        /// Specifies the type of government loan application. This information is referenced on VA forms 26-1802a and 26-1820, for example.
        /// </summary>
        [XmlElement(Order = 32)]
        public MISMOEnum<GovernmentLoanApplicationBase> GovernmentLoanApplicationType;

        /// <summary>
        /// Gets or sets a value indicating whether the GovernmentLoanApplicationType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GovernmentLoanApplicationType element has been assigned a value.</value>
        [XmlIgnore]
        public bool GovernmentLoanApplicationTypeSpecified
        {
            get { return this.GovernmentLoanApplicationType != null && this.GovernmentLoanApplicationType.enumValue != GovernmentLoanApplicationBase.Blank; }
            set { }
        }

        /// <summary>
        /// The dollar amount of the Mortgage Credit Certificate. This is entered as a whole number. It is an annual tax credit. FHA allows it to be treated as monthly income. May also be used for VA loans.
        /// </summary>
        [XmlElement(Order = 33)]
        public MISMOAmount GovernmentMortgageCreditCertificateAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the GovernmentMortgageCreditCertificateAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GovernmentMortgageCreditCertificateAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool GovernmentMortgageCreditCertificateAmountSpecified
        {
            get { return GovernmentMortgageCreditCertificateAmount != null; }
            set { }
        }

        /// <summary>
        /// Indicates the amount of documentation for FHA/VA no cash out refinance loans.
        /// </summary>
        [XmlElement(Order = 34)]
        public MISMOEnum<GovernmentRefinanceBase> GovernmentRefinanceType;

        /// <summary>
        /// Gets or sets a value indicating whether the GovernmentRefinanceType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GovernmentRefinanceType element has been assigned a value.</value>
        [XmlIgnore]
        public bool GovernmentRefinanceTypeSpecified
        {
            get { return this.GovernmentRefinanceType != null && this.GovernmentRefinanceType.enumValue != GovernmentRefinanceBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Government Refinance Type.
        /// </summary>
        [XmlElement(Order = 35)]
        public MISMOString GovernmentRefinanceTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the GovernmentRefinanceTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GovernmentRefinanceTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool GovernmentRefinanceTypeOtherDescriptionSpecified
        {
            get { return GovernmentRefinanceTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates that borrower has adequate available assets for HUD approval.
        /// </summary>
        [XmlElement(Order = 36)]
        public MISMOIndicator HUDAdequateAvailableAssetsIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the HUDAdequateAvailableAssetsIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HUDAdequateAvailableAssetsIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool HUDAdequateAvailableAssetsIndicatorSpecified
        {
            get { return HUDAdequateAvailableAssetsIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates borrower effective income is adequate for HUD approval.
        /// </summary>
        [XmlElement(Order = 37)]
        public MISMOIndicator HUDAdequateEffectiveIncomeIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the HUDAdequateEffectiveIncomeIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HUDAdequateEffectiveIncomeIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool HUDAdequateEffectiveIncomeIndicatorSpecified
        {
            get { return HUDAdequateEffectiveIncomeIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates borrower meets credit characteristics for HUD approval.
        /// </summary>
        [XmlElement(Order = 38)]
        public MISMOIndicator HUDCreditCharacteristicsIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the HUDCreditCharacteristicsIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HUDCreditCharacteristicsIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool HUDCreditCharacteristicsIndicatorSpecified
        {
            get { return HUDCreditCharacteristicsIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates borrowers stable effective income meets HUD approval.
        /// </summary>
        [XmlElement(Order = 39)]
        public MISMOIndicator HUDStableEffectiveIncomeIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the HUDStableEffectiveIncomeIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HUDStableEffectiveIncomeIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool HUDStableEffectiveIncomeIndicatorSpecified
        {
            get { return HUDStableEffectiveIncomeIndicator != null; }
            set { }
        }

        /// <summary>
        /// Value identifier for master certificate.
        /// </summary>
        [XmlElement(Order = 40)]
        public MISMOIdentifier MasterCertificateOfReasonablenumValueIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the MasterCertificateOfReasonableNumValueIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MasterCertificateOfReasonableNumValueIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool MasterCertificateOfReasonablenumValueIdentifierSpecified
        {
            get { return MasterCertificateOfReasonablenumValueIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The estimated closing costs which will be paid by a party, other than the borrower or seller, on the subject property. Used for FHA or VA loans.
        /// </summary>
        [XmlElement(Order = 41)]
        public MISMOAmount OtherPartyPaidFHA_VAClosingCostsAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the OtherPartyPaidFHA_VAClosingCostsAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OtherPartyPaidFHA_VAClosingCostsAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool OtherPartyPaidFHA_VAClosingCostsAmountSpecified
        {
            get { return OtherPartyPaidFHA_VAClosingCostsAmount != null; }
            set { }
        }

        /// <summary>
        /// The percentage of estimated closing costs that will be paid by any party other than the borrower or seller including any seller paid prepaid amounts but excluding discount points.
        /// </summary>
        [XmlElement(Order = 42)]
        public MISMOPercent OtherPartyPaidFHA_VAClosingCostsPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the OtherPartyPaidFHA_VAClosingCostsPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OtherPartyPaidFHA_VAClosingCostsPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool OtherPartyPaidFHA_VAClosingCostsPercentSpecified
        {
            get { return OtherPartyPaidFHA_VAClosingCostsPercent != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates the borrower has previously had a VA home loan.
        /// </summary>
        [XmlElement(Order = 43)]
        public MISMOIndicator PreviousVAHomeLoanIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PreviousVAHomeLoanIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PreviousVAHomeLoanIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PreviousVAHomeLoanIndicatorSpecified
        {
            get { return PreviousVAHomeLoanIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the appraiser states that the property meets FHA /VA Energy Efficient Home guidelines. Usually applies to new construction.
        /// </summary>
        [XmlElement(Order = 44)]
        public MISMOIndicator PropertyEnergyEfficientHomeIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyEnergyEfficientHomeIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyEnergyEfficientHomeIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyEnergyEfficientHomeIndicatorSpecified
        {
            get { return PropertyEnergyEfficientHomeIndicator != null; }
            set { }
        }

        /// <summary>
        /// For Rural Housing loans, the expiration date of the guaranty.
        /// </summary>
        [XmlElement(Order = 45)]
        public MISMODate RuralHousingConditionalGuarantyExpirationDate;

        /// <summary>
        /// Gets or sets a value indicating whether the RuralHousingConditionalGuarantyExpirationDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RuralHousingConditionalGuarantyExpirationDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool RuralHousingConditionalGuarantyExpirationDateSpecified
        {
            get { return RuralHousingConditionalGuarantyExpirationDate != null; }
            set { }
        }

        /// <summary>
        /// For Rural Housing loans, the interest rate included in the USDA guaranty.
        /// </summary>
        [XmlElement(Order = 46)]
        public MISMOPercent RuralHousingConditionalGuarantyInterestRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the RuralHousingConditionalGuarantyInterestRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RuralHousingConditionalGuarantyInterestRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool RuralHousingConditionalGuarantyInterestRatePercentSpecified
        {
            get { return RuralHousingConditionalGuarantyInterestRatePercent != null; }
            set { }
        }

        /// <summary>
        /// Identifies the section of the National Housing Act which defines underwriting guidelines for VA or FHA loan evaluations.
        /// </summary>
        [XmlElement(Order = 47)]
        public MISMOEnum<SectionOfActBase> SectionOfActType;

        /// <summary>
        /// Gets or sets a value indicating whether the SectionOfActType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SectionOfActType element has been assigned a value.</value>
        [XmlIgnore]
        public bool SectionOfActTypeSpecified
        {
            get { return this.SectionOfActType != null && this.SectionOfActType.enumValue != SectionOfActBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Section of Act Type.
        /// </summary>
        [XmlElement(Order = 48)]
        public MISMOString SectionOfActTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the SectionOfActTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SectionOfActTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool SectionOfActTypeOtherDescriptionSpecified
        {
            get { return SectionOfActTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The percentage of estimated closing costs that will be paid by the seller of the subject property, including any seller prepaid amount, but excluding discount points.
        /// </summary>
        [XmlElement(Order = 49)]
        public MISMOPercent SellerPaidFHA_VAClosingCostsPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the SellerPaidFHA_VAClosingCostsPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SellerPaidFHA_VAClosingCostsPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool SellerPaidFHA_VAClosingCostsPercentSpecified
        {
            get { return SellerPaidFHA_VAClosingCostsPercent != null; }
            set { }
        }

        /// <summary>
        /// On the FHA Non Owner Occupancy Rider, this indicates that the Security Instrument applies to Property sold under HUD Single Family Property Disposition Program and meets the requirements thereof.
        /// </summary>
        [XmlElement(Order = 50)]
        public MISMOIndicator SoldUnderHUDSingleFamilyPropertyDispositionProgramIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the SoldUnderHUDSingleFamilyPropertyDispositionProgramIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SoldUnderHUDSingleFamilyPropertyDispositionProgramIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool SoldUnderHUDSingleFamilyPropertyDispositionProgramIndicatorSpecified
        {
            get { return SoldUnderHUDSingleFamilyPropertyDispositionProgramIndicator != null; }
            set { }
        }

        /// <summary>
        /// The total annual income of all members of the household as defined by Rural Housing Program Guidelines.  This amount can be found on USDA Form 1980-21, Line 7. 
        /// </summary>
        [XmlElement(Order = 51)]
        public MISMOAmount USDA198021RuralHousingAdjustedAnnualHouseholdIncomeAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the USDA198021RuralHousingAdjustedAnnualHouseholdIncomeAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the USDA198021RuralHousingAdjustedAnnualHouseholdIncomeAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool USDA198021RuralHousingAdjustedAnnualHouseholdIncomeAmountSpecified
        {
            get { return USDA198021RuralHousingAdjustedAnnualHouseholdIncomeAmount != null; }
            set { }
        }

        /// <summary>
        /// Specifies the property appraisal type used, as appears on VA Loan Summary Sheet (VA 26-0286), for example.
        /// </summary>
        [XmlElement(Order = 52)]
        public MISMOEnum<VAAppraisalBase> VAAppraisalType;

        /// <summary>
        /// Gets or sets a value indicating whether the VAAppraisalType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the VAAppraisalType element has been assigned a value.</value>
        [XmlIgnore]
        public bool VAAppraisalTypeSpecified
        {
            get { return this.VAAppraisalType != null && this.VAAppraisalType.enumValue != VAAppraisalBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field which contains the description of the type of VA appraisal when Other is selected as the VA Appraisal Type.
        /// </summary>
        [XmlElement(Order = 53)]
        public MISMOString VAAppraisalTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the VAAppraisalTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the VAAppraisalTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool VAAppraisalTypeOtherDescriptionSpecified
        {
            get { return VAAppraisalTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether the co-borrower is married to the borrower. Used for VA loans only.
        /// </summary>
        [XmlElement(Order = 54)]
        public MISMOIndicator VABorrowerCoBorrowerMarriedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the VABorrowerCoBorrowerMarriedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the VABorrowerCoBorrowerMarriedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool VABorrowerCoBorrowerMarriedIndicatorSpecified
        {
            get { return VABorrowerCoBorrowerMarriedIndicator != null; }
            set { }
        }

        /// <summary>
        /// Identification number of the associated VA claim folder.
        /// </summary>
        [XmlElement(Order = 55)]
        public MISMOIdentifier VAClaimFolderIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the VAClaimFolderIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the VAClaimFolderIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool VAClaimFolderIdentifierSpecified
        {
            get { return VAClaimFolderIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of guarantee by the VA available to the veteran borrower.
        /// </summary>
        [XmlElement(Order = 56)]
        public MISMOAmount VAEntitlementAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the VAEntitlementAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the VAEntitlementAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool VAEntitlementAmountSpecified
        {
            get { return VAEntitlementAmount != null; }
            set { }
        }

        /// <summary>
        /// VA entitlement identifier from Certificate of Eligibility for VA Loan Summary Sheet.
        /// </summary>
        [XmlElement(Order = 57)]
        public MISMOIdentifier VAEntitlementIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the VAEntitlementIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the VAEntitlementIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool VAEntitlementIdentifierSpecified
        {
            get { return VAEntitlementIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The Exemption Status given by the VA Administration  (exemption from VA Funding Fees).
        /// </summary>
        [XmlElement(Order = 58)]
        public MISMOEnum<VAFundingFeeExemptionBase> VAFundingFeeExemptionType;

        /// <summary>
        /// Gets or sets a value indicating whether the VAFundingFeeExemptionType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the VAFundingFeeExemptionType element has been assigned a value.</value>
        [XmlIgnore]
        public bool VAFundingFeeExemptionTypeSpecified
        {
            get { return this.VAFundingFeeExemptionType != null && this.VAFundingFeeExemptionType.enumValue != VAFundingFeeExemptionBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for VA  Funding Fee Exemption Type.
        /// </summary>
        [XmlElement(Order = 59)]
        public MISMOString VAFundingFeeExemptionTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the VAFundingFeeExemptionTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the VAFundingFeeExemptionTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool VAFundingFeeExemptionTypeOtherDescriptionSpecified
        {
            get { return VAFundingFeeExemptionTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Size of household at the loan level as defined by the VA. The lender must meet VA requirements for determining the size of household.
        /// </summary>
        [XmlElement(Order = 60)]
        public MISMOCount VAHouseholdSizeCount;

        /// <summary>
        /// Gets or sets a value indicating whether the VAHouseholdSizeCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the VAHouseholdSizeCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool VAHouseholdSizeCountSpecified
        {
            get { return VAHouseholdSizeCount != null; }
            set { }
        }

        /// <summary>
        /// Specifies the procedure used to qualify the borrower(s).
        /// </summary>
        [XmlElement(Order = 61)]
        public MISMOEnum<VALoanProcedureBase> VALoanProcedureType;

        /// <summary>
        /// Gets or sets a value indicating whether the VALoanProcedureType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the VALoanProcedureType element has been assigned a value.</value>
        [XmlIgnore]
        public bool VALoanProcedureTypeSpecified
        {
            get { return this.VALoanProcedureType != null && this.VALoanProcedureType.enumValue != VALoanProcedureBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field which contains the description of the type of VA loan procedure when Other is selected as the VA Loan Procedure Type. 
        /// </summary>
        [XmlElement(Order = 62)]
        public MISMOString VALoanProcedureTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the VALoanProcedureTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the VALoanProcedureTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool VALoanProcedureTypeOtherDescriptionSpecified
        {
            get { return VALoanProcedureTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Identifies the type of VA Loan Program established by Title 38 of the US Code.
        /// </summary>
        [XmlElement(Order = 63)]
        public MISMOEnum<VALoanProgramBase> VALoanProgramType;

        /// <summary>
        /// Gets or sets a value indicating whether the VALoanProgramType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the VALoanProgramType element has been assigned a value.</value>
        [XmlIgnore]
        public bool VALoanProgramTypeSpecified
        {
            get { return this.VALoanProgramType != null && this.VALoanProgramType.enumValue != VALoanProgramBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the VA Loan Program Type if Other is selected as the VA Loan Program Type.
        /// </summary>
        [XmlElement(Order = 64)]
        public MISMOString VALoanProgramTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the VALoanProgramTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the VALoanProgramTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool VALoanProgramTypeOtherDescriptionSpecified
        {
            get { return VALoanProgramTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The estimated dollar amount of monthly maintenance expense used for VA loans.
        /// </summary>
        [XmlElement(Order = 65)]
        public MISMOAmount VAMaintenanceExpenseMonthlyAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the VAMaintenanceExpenseMonthlyAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the VAMaintenanceExpenseMonthlyAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool VAMaintenanceExpenseMonthlyAmountSpecified
        {
            get { return VAMaintenanceExpenseMonthlyAmount != null; }
            set { }
        }

        /// <summary>
        /// Indicator for the VA reasonable value improvements completion.
        /// </summary>
        [XmlElement(Order = 66)]
        public MISMOIndicator VAReasonablenumValueImprovementsCompletionIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the VAReasonableNumValueImprovementsCompletionIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the VAReasonableNumValueImprovementsCompletionIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool VAReasonablenumValueImprovementsCompletionIndicatorSpecified
        {
            get { return VAReasonablenumValueImprovementsCompletionIndicator != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of the monthly balance of funds available for family support after meeting the borrowers shelter expenses, debts, and taxes as defined by the VA.
        /// </summary>
        [XmlElement(Order = 67)]
        public MISMOAmount VAResidualIncomeAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the VAResidualIncomeAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the VAResidualIncomeAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool VAResidualIncomeAmountSpecified
        {
            get { return VAResidualIncomeAmount != null; }
            set { }
        }

        /// <summary>
        /// Dollar amount used as a guideline for assessing residual income.
        /// </summary>
        [XmlElement(Order = 68)]
        public MISMOAmount VAResidualIncomeGuidelineAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the VAResidualIncomeGuidelineAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the VAResidualIncomeGuidelineAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool VAResidualIncomeGuidelineAmountSpecified
        {
            get { return VAResidualIncomeGuidelineAmount != null; }
            set { }
        }

        /// <summary>
        /// The VA specific vesting under which the property is, or will be, held.
        /// </summary>
        [XmlElement(Order = 69)]
        public MISMOEnum<VATitleVestingBase> VATitleVestingType;

        /// <summary>
        /// Gets or sets a value indicating whether the VATitleVestingType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the VATitleVestingType element has been assigned a value.</value>
        [XmlIgnore]
        public bool VATitleVestingTypeSpecified
        {
            get { return this.VATitleVestingType != null && this.VATitleVestingType.enumValue != VATitleVestingBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field which contains the description of the type of VA title vesting when Other is selected as the VA Title Vesting Type.
        /// </summary>
        [XmlElement(Order = 70)]
        public MISMOString VATitleVestingTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the VATitleVestingTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the VATitleVestingTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool VATitleVestingTypeOtherDescriptionSpecified
        {
            get { return VATitleVestingTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The estimated dollar amount of monthly utility expense used for VA loans.
        /// </summary>
        [XmlElement(Order = 71)]
        public MISMOAmount VAUtilityExpenseMonthlyAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the VAUtilityExpenseMonthlyAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the VAUtilityExpenseMonthlyAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool VAUtilityExpenseMonthlyAmountSpecified
        {
            get { return VAUtilityExpenseMonthlyAmount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 72)]
        public GOVERNMENT_LOAN_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
