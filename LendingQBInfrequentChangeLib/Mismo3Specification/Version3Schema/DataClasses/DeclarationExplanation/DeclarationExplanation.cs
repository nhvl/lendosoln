namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class DECLARATION_EXPLANATION
    {
        /// <summary>
        /// Gets a value indicating whether the DECLARATION_EXPLANATION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DeclarationExplanationDescriptionSpecified
                    || this.DeclarationExplanationTypeOtherDescriptionSpecified
                    || this.DeclarationExplanationTypeSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A free-form text field used to capture explanations to any yes answer to a declaration on lines a through i on the loan application. Captured on the URLA in a continuation sheet.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString DeclarationExplanationDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the DeclarationExplanationDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DeclarationExplanationDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool DeclarationExplanationDescriptionSpecified
        {
            get { return DeclarationExplanationDescription != null; }
            set { }
        }

        /// <summary>
        /// Identifies the particular question asked in the declarations section of the loan application. Used in conjunction with the text field to capture additional information.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<DeclarationExplanationBase> DeclarationExplanationType;

        /// <summary>
        /// Gets or sets a value indicating whether the DeclarationExplanationType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DeclarationExplanationType element has been assigned a value.</value>
        [XmlIgnore]
        public bool DeclarationExplanationTypeSpecified
        {
            get { return this.DeclarationExplanationType != null && this.DeclarationExplanationType.enumValue != DeclarationExplanationBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the kind of declaration if Other is selected as the Declaration Explanation Type.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString DeclarationExplanationTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the DeclarationExplanationTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DeclarationExplanationTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool DeclarationExplanationTypeOtherDescriptionSpecified
        {
            get { return DeclarationExplanationTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public DECLARATION_EXPLANATION_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
