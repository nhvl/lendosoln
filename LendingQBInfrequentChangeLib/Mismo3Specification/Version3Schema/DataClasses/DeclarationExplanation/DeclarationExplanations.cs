namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class DECLARATION_EXPLANATIONS
    {
        /// <summary>
        /// Gets a value indicating whether the DECLARATION_EXPLANATIONS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DeclarationExplanationSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of declaration explanations.
        /// </summary>
        [XmlElement("DECLARATION_EXPLANATION", Order = 0)]
		public List<DECLARATION_EXPLANATION> DeclarationExplanation = new List<DECLARATION_EXPLANATION>();

        /// <summary>
        /// Gets or sets a value indicating whether the DeclarationExplanation element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DeclarationExplanation element has been assigned a value.</value>
        [XmlIgnore]
        public bool DeclarationExplanationSpecified
        {
            get { return this.DeclarationExplanation != null && this.DeclarationExplanation.Count(d => d != null && d.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public DECLARATION_EXPLANATIONS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
