namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class ATTORNEY
    {
        /// <summary>
        /// Gets a value indicating whether the ATTORNEY container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AttorneyFirmNameSpecified
                    || this.AttorneyFunctionTypeOtherDescriptionSpecified
                    || this.AttorneyFunctionTypeSpecified
                    || this.ExtensionSpecified
                    || this.RepresentedPartyTypeOtherDescriptionSpecified
                    || this.RepresentedPartyTypeSpecified;
            }
        }

        /// <summary>
        /// The name of the firm of the attorney providing the representation.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString AttorneyFirmName;

        /// <summary>
        /// Gets or sets a value indicating whether the AttorneyFirmName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AttorneyFirmName element has been assigned a value.</value>
        [XmlIgnore]
        public bool AttorneyFirmNameSpecified
        {
            get { return AttorneyFirmName != null; }
            set { }
        }

        /// <summary>
        /// The type of function the attorney is performing in this instance.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<AttorneyFunctionBase> AttorneyFunctionType;

        /// <summary>
        /// Gets or sets a value indicating whether the AttorneyFunctionType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AttorneyFunctionType element has been assigned a value.</value>
        [XmlIgnore]
        public bool AttorneyFunctionTypeSpecified
        {
            get { return this.AttorneyFunctionType != null && this.AttorneyFunctionType.enumValue != AttorneyFunctionBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Attorney Function Type.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString AttorneyFunctionTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the AttorneyFunctionTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AttorneyFunctionTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool AttorneyFunctionTypeOtherDescriptionSpecified
        {
            get { return AttorneyFunctionTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The party that the attorney represents.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOEnum<RepresentedPartyBase> RepresentedPartyType;

        /// <summary>
        /// Gets or sets a value indicating whether the RepresentedPartyType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RepresentedPartyType element has been assigned a value.</value>
        [XmlIgnore]
        public bool RepresentedPartyTypeSpecified
        {
            get { return this.RepresentedPartyType != null && this.RepresentedPartyType.enumValue != RepresentedPartyBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected as the Represented Party Type.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString RepresentedPartyTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the RepresentedPartyTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RepresentedPartyTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool RepresentedPartyTypeOtherDescriptionSpecified
        {
            get { return RepresentedPartyTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 5)]
        public ATTORNEY_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return ExtensionSpecified; }
            set { }
        }
    }
}
