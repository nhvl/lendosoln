namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class FEE_PAYMENTS
    {
        /// <summary>
        /// Gets a value indicating whether the FEE_PAYMENTS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.FeePaymentSpecified;
            }
        }

        /// <summary>
        /// A collection of fee payments.
        /// </summary>
        [XmlElement("FEE_PAYMENT", Order = 0)]
        public List<FEE_PAYMENT> FeePayment = new List<FEE_PAYMENT>();

        /// <summary>
        /// Gets or sets a value indicating whether the FeePayment element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeePayment element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeePaymentSpecified
        {
            get { return this.FeePayment != null && this.FeePayment.Count(f => f != null && f.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public FEE_PAYMENTS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
