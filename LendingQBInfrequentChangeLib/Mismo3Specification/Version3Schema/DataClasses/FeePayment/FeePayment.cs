namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class FEE_PAYMENT
    {
        /// <summary>
        /// Gets a value indicating whether the FEE_PAYMENT container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.FeeActualPaymentAmountSpecified
                    || this.FeeEstimatedPaymentAmountSpecified
                    || this.FeePaymentAllowableFHAClosingCostIndicatorSpecified
                    || this.FeePaymentCollectedByTypeOtherDescriptionSpecified
                    || this.FeePaymentCollectedByTypeSpecified
                    || this.FeePaymentNetDueAmountSpecified
                    || this.FeePaymentPaidByTypeSpecified
                    || this.FeePaymentPaidByTypeThirdPartyNameSpecified
                    || this.FeePaymentPaidOutsideOfClosingIndicatorSpecified
                    || this.FeePaymentPercentSpecified
                    || this.FeePaymentProcessTypeSpecified
                    || this.FeePaymentRefundableAmountSpecified
                    || this.FeePaymentRefundableConditionsDescriptionSpecified
                    || this.FeePaymentRefundableIndicatorSpecified
                    || this.FeePaymentResponsiblePartyTypeOtherDescriptionSpecified
                    || this.FeePaymentResponsiblePartyTypeSpecified
                    || this.PaymentFinancedIndicatorSpecified
                    || this.PaymentIncludedInAPRIndicatorSpecified
                    || this.PaymentIncludedInJurisdictionHighCostIndicatorSpecified
                    || this.RegulationZPointsAndFeesIndicatorSpecified;
            }
        }

        /// <summary>
        /// The actual dollar amount paid by a specific party at a specified time for a specified fee.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount FeeActualPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the FeeActualPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeeActualPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeeActualPaymentAmountSpecified
        {
            get { return FeeActualPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// The estimated dollar amount paid by a specific party at a specified time for a specified fee.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOAmount FeeEstimatedPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the FeeEstimatedPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeeEstimatedPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeeEstimatedPaymentAmountSpecified
        {
            get { return FeeEstimatedPaymentAmount != null; }
            set { }
        }

        /// <summary>
        ///  When true, indicates the fee is an allowable FHA Closing Cost.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOIndicator FeePaymentAllowableFHAClosingCostIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the FeePaymentAllowableFHAClosingCostIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeePaymentAllowableFHAClosingCostIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeePaymentAllowableFHAClosingCostIndicatorSpecified
        {
            get { return FeePaymentAllowableFHAClosingCostIndicator != null; }
            set { }
        }

        /// <summary>
        /// Identifies the role of the party collecting the fee.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOEnum<FeePaymentCollectedByBase> FeePaymentCollectedByType;

        /// <summary>
        /// Gets or sets a value indicating whether the FeePaymentCollectedByType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeePaymentCollectedByType element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeePaymentCollectedByTypeSpecified
        {
            get { return this.FeePaymentCollectedByType != null && this.FeePaymentCollectedByType.enumValue != FeePaymentCollectedByBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field to capture the description when Other is selected as Fee Payment Collected By Type.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString FeePaymentCollectedByTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the FeePaymentCollectedByTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeePaymentCollectedByTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeePaymentCollectedByTypeOtherDescriptionSpecified
        {
            get { return FeePaymentCollectedByTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The portion or remaining amount of the fee payment still due from the Borrower or Seller, after application of Paid Out Of Closing and Paid Through Closing fee payment amounts. 
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOAmount FeePaymentNetDueAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the FeePaymentNetDueAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeePaymentNetDueAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeePaymentNetDueAmountSpecified
        {
            get { return FeePaymentNetDueAmount != null; }
            set { }
        }

        /// <summary>
        /// The role of the party making the payment of fee.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOEnum<FeePaymentPaidByBase> FeePaymentPaidByType;

        /// <summary>
        /// Gets or sets a value indicating whether the FeePaymentPaidByType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeePaymentPaidByType element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeePaymentPaidByTypeSpecified
        {
            get { return this.FeePaymentPaidByType != null && this.FeePaymentPaidByType.enumValue != FeePaymentPaidByBase.Blank; }
            set { }
        }

        /// <summary>
        /// The name or description of the party paying the fee when Third Party is the selected enumerated value for Fee Payment Paid By Type.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOString FeePaymentPaidByTypeThirdPartyName;

        /// <summary>
        /// Gets or sets a value indicating whether the FeePaymentPaidByTypeThirdPartyName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeePaymentPaidByTypeThirdPartyName element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeePaymentPaidByTypeThirdPartyNameSpecified
        {
            get { return FeePaymentPaidByTypeThirdPartyName != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates payment of fee was paid outside of closing.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOIndicator FeePaymentPaidOutsideOfClosingIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the FeePaymentPaidOutsideOfClosingIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeePaymentPaidOutsideOfClosingIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeePaymentPaidOutsideOfClosingIndicatorSpecified
        {
            get { return FeePaymentPaidOutsideOfClosingIndicator != null; }
            set { }
        }

        /// <summary>
        /// The portion of the Fee Total Percent that is to be paid by the specified party. The party is identified in the Fee Payment Paid By Type.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOPercent FeePaymentPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the FeePaymentPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeePaymentPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeePaymentPercentSpecified
        {
            get { return FeePaymentPercent != null; }
            set { }
        }

        /// <summary>
        /// Processing - typical buyer paid fee to be disclosed on GFE.
        /// Closing - typical seller fee not disclosed on GFE.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOEnum<FeePaymentProcessBase> FeePaymentProcessType;

        /// <summary>
        /// Gets or sets a value indicating whether the FeePaymentProcessType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeePaymentProcessType element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeePaymentProcessTypeSpecified
        {
            get { return this.FeePaymentProcessType != null && this.FeePaymentProcessType.enumValue != FeePaymentProcessBase.Blank; }
            set { }
        }

        /// <summary>
        /// The amount of the fee that is refundable.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOAmount FeePaymentRefundableAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the FeePaymentRefundableAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeePaymentRefundableAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeePaymentRefundableAmountSpecified
        {
            get { return FeePaymentRefundableAmount != null; }
            set { }
        }

        /// <summary>
        /// A description of the conditions under which the fee is refundable.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOString FeePaymentRefundableConditionsDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the FeePaymentRefundableConditionsDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeePaymentRefundableConditionsDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeePaymentRefundableConditionsDescriptionSpecified
        {
            get { return FeePaymentRefundableConditionsDescription != null; }
            set { }
        }

        /// <summary>
        /// When true, the fee may be refundable under certain conditions.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOIndicator FeePaymentRefundableIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the FeePaymentRefundableIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeePaymentRefundableIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeePaymentRefundableIndicatorSpecified
        {
            get { return FeePaymentRefundableIndicator != null; }
            set { }
        }

        /// <summary>
        /// Identifies the fee payment as the responsibility of the Buyer, Seller or other party of the loan transaction. 
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOEnum<FeePaymentResponsiblePartyBase> FeePaymentResponsiblePartyType;

        /// <summary>
        /// Gets or sets a value indicating whether the FeePaymentResponsiblePartyType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeePaymentResponsiblePartyType element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeePaymentResponsiblePartyTypeSpecified
        {
            get { return this.FeePaymentResponsiblePartyType != null && this.FeePaymentResponsiblePartyType.enumValue != FeePaymentResponsiblePartyBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the party responsible for the fee payment when Other is selected as the Fee Payment Responsible Party Type. 
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOString FeePaymentResponsiblePartyTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the FeePaymentResponsiblePartyTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeePaymentResponsiblePartyTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeePaymentResponsiblePartyTypeOtherDescriptionSpecified
        {
            get { return FeePaymentResponsiblePartyTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that the payment of any portion of the fee was financed into the original loan amount.
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMOIndicator PaymentFinancedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PaymentFinancedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PaymentFinancedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PaymentFinancedIndicatorSpecified
        {
            get { return PaymentFinancedIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates fee is to be included in APR calculations.
        /// </summary>
        [XmlElement(Order = 17)]
        public MISMOIndicator PaymentIncludedInAPRIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PaymentIncludedInAPRIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PaymentIncludedInAPRIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PaymentIncludedInAPRIndicatorSpecified
        {
            get { return PaymentIncludedInAPRIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates the amount of the payment is included in the points and fees calculation appropriated to the jurisdiction of the subject property.
        /// </summary>
        [XmlElement(Order = 18)]
        public MISMOIndicator PaymentIncludedInJurisdictionHighCostIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PaymentIncludedInJurisdictionHighCostIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PaymentIncludedInJurisdictionHighCostIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PaymentIncludedInJurisdictionHighCostIndicatorSpecified
        {
            get { return PaymentIncludedInJurisdictionHighCostIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that item is included in the Regulation Z Points and Fees calculation.
        /// </summary>
        [XmlElement(Order = 19)]
        public MISMOIndicator RegulationZPointsAndFeesIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the RegulationZPointsAndFeesIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RegulationZPointsAndFeesIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool RegulationZPointsAndFeesIndicatorSpecified
        {
            get { return RegulationZPointsAndFeesIndicator != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 20)]
        public FEE_PAYMENT_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
