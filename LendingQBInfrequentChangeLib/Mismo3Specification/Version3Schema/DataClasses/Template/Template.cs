namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class TEMPLATE
    {
        /// <summary>
        /// Gets a value indicating whether the TEMPLATE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                if (Mismo3Utilities.ExceedsThreshold(1,
                    new List<bool> { this.TemplateFilesSpecified, this.TemplatePagesSpecified }))
                {
                    throw new System.Xml.Schema.XmlSchemaValidationException(
                        Mismo3Utilities.GetXSDChoiceExceptionMessage(
                        "TEMPLATE",
                        new List<string> { "TEMPLATE_FILES", "TEMPLATE_PAGES"}));
                }

                return this.ExtensionSpecified
                    || this.TemplateFilesSpecified
                    || this.TemplatePagesSpecified;
            }
        }

        /// <summary>
        /// A list of template files.
        /// </summary>
        [XmlElement("TEMPLATE_FILES", Order = 0)]
        public TEMPLATE_FILES TemplateFiles;

        /// <summary>
        /// Gets or sets a value indicating whether the TemplateFiles element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TemplateFiles element has been assigned a value.</value>
        [XmlIgnore]
        public bool TemplateFilesSpecified
        {
            get { return this.TemplateFiles != null && this.TemplateFiles.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A list of template pages.
        /// </summary>
        [XmlElement("TEMPLATE_PAGES", Order = 1)]
        public TEMPLATE_PAGES TemplatePages;

        /// <summary>
        /// Gets or sets a value indicating whether the TemplatePages element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TemplatePages element has been assigned a value.</value>
        [XmlIgnore]
        public bool TemplatePagesSpecified
        {
            get { return this.TemplatePages != null && this.TemplatePages.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public TEMPLATE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
