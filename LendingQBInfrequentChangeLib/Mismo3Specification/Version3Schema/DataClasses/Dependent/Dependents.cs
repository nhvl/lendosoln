namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class DEPENDENTS
    {
        /// <summary>
        /// Gets a value indicating whether the DEPENDENTS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DependentSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of dependents.
        /// </summary>
        [XmlElement("DEPENDENT", Order = 0)]
		public List<DEPENDENT> Dependent = new List<DEPENDENT>();

        /// <summary>
        /// Gets or sets a value indicating whether the Dependent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Dependent element has been assigned a value.</value>
        [XmlIgnore]
        public bool DependentSpecified
        {
            get { return this.Dependent != null && this.Dependent.Count(d => d != null && d.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public DEPENDENTS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
