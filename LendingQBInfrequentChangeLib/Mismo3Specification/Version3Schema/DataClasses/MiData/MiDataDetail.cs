namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class MI_DATA_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the MI_DATA_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BorrowerMITerminationDateSpecified
                    || this.ExtensionSpecified
                    || this.FeePaidToTypeOtherDescriptionSpecified
                    || this.FeePaidToTypeSpecified
                    || this.IntegratedDisclosureLineNumberValueSpecified
                    || this.IntegratedDisclosureSectionTypeOtherDescriptionSpecified
                    || this.IntegratedDisclosureSectionTypeSpecified
                    || this.LenderPaidMIInterestRateAdjustmentPercentSpecified
                    || this.MI_LTVCutoffPercentSpecified
                    || this.MI_LTVCutoffTypeSpecified
                    || this.MICancellationDateSpecified
                    || this.MICertificateIdentifierSpecified
                    || this.MICertificationStatusTypeOtherDescriptionSpecified
                    || this.MICertificationStatusTypeSpecified
                    || this.MICollectedNumberOfMonthsCountSpecified
                    || this.MICompanyNameTypeOtherDescriptionSpecified
                    || this.MICompanyNameTypeSpecified
                    || this.MICoverageEffectiveDateSpecified
                    || this.MICoveragePercentSpecified
                    || this.MICoveragePlanTypeOtherDescriptionSpecified
                    || this.MICoveragePlanTypeSpecified
                    || this.MICushionNumberOfMonthsCountSpecified
                    || this.MIDurationTypeOtherDescriptionSpecified
                    || this.MIDurationTypeSpecified
                    || this.MIEscrowedIndicatorSpecified
                    || this.MIEscrowIncludedInAggregateIndicatorSpecified
                    || this.MIInitialPremiumAmountSpecified
                    || this.MIInitialPremiumAtClosingTypeOtherDescriptionSpecified
                    || this.MIInitialPremiumAtClosingTypeSpecified
                    || this.MIInitialPremiumRatePercentSpecified
                    || this.MIPremiumCalendarYearAmountSpecified
                    || this.MIPremiumFinancedAmountSpecified
                    || this.MIPremiumFinancedIndicatorSpecified
                    || this.MIPremiumFromClosingAmountSpecified
                    || this.MIPremiumPaymentTypeOtherDescriptionSpecified
                    || this.MIPremiumPaymentTypeSpecified
                    || this.MIPremiumRefundableAmountSpecified
                    || this.MIPremiumRefundableConditionsDescriptionSpecified
                    || this.MIPremiumRefundableTypeOtherDescriptionSpecified
                    || this.MIPremiumRefundableTypeSpecified
                    || this.MIPremiumSourceTypeOtherDescriptionSpecified
                    || this.MIPremiumSourceTypeSpecified
                    || this.MIRemovalIdentifierSpecified
                    || this.MIScheduledTerminationDateSpecified
                    || this.MISourceTypeOtherDescriptionSpecified
                    || this.MISourceTypeSpecified
                    || this.PrimaryMIAbsenceReasonTypeOtherDescriptionSpecified
                    || this.PrimaryMIAbsenceReasonTypeSpecified
                    || this.RequiredProviderOfServiceIndicatorSpecified
                    || this.ScheduledAmortizationMidpointDateSpecified
                    || this.SellerMIPaidToDateSpecified;
            }
        }

        /// <summary>
        /// The date that the borrower may request cancellation of MI insurance as identified by HOEPA regulations.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMODate BorrowerMITerminationDate;

        /// <summary>
        /// Gets or sets a value indicating whether the BorrowerMITerminationDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BorrowerMITerminationDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool BorrowerMITerminationDateSpecified
        {
            get { return BorrowerMITerminationDate != null; }
            set { }
        }

        /// <summary>
        /// Identifies the category or type of payee to which the fee or payment will be paid.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<FeePaidToBase> FeePaidToType;

        /// <summary>
        /// Gets or sets a value indicating whether the FeePaidToType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeePaidToType element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeePaidToTypeSpecified
        {
            get { return this.FeePaidToType != null && this.FeePaidToType.enumValue != FeePaidToBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field to capture the description when Other is selected as Fee Paid To Type.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString FeePaidToTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the FeePaidToTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeePaidToTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeePaidToTypeOtherDescriptionSpecified
        {
            get { return FeePaidToTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The line identifier for the item as represented in a section of the Integrated Disclosure form.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOValue IntegratedDisclosureLineNumberValue;

        /// <summary>
        /// Gets or sets a value indicating whether the IntegratedDisclosureLineNumberValue element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IntegratedDisclosureLineNumberValue element has been assigned a value.</value>
        [XmlIgnore]
        public bool IntegratedDisclosureLineNumberValueSpecified
        {
            get { return IntegratedDisclosureLineNumberValue != null; }
            set { }
        }

        /// <summary>
        /// The title or description used to identify a primary section of the integrated disclosure document.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<IntegratedDisclosureSectionBase> IntegratedDisclosureSectionType;

        /// <summary>
        /// Gets or sets a value indicating whether the IntegratedDisclosureSectionType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IntegratedDisclosureSectionType element has been assigned a value.</value>
        [XmlIgnore]
        public bool IntegratedDisclosureSectionTypeSpecified
        {
            get { return this.IntegratedDisclosureSectionType != null && this.IntegratedDisclosureSectionType.enumValue != IntegratedDisclosureSectionBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture additional information when Other is selected for Integrated Disclosure Section Type.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString IntegratedDisclosureSectionTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the IntegratedDisclosureSectionTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IntegratedDisclosureSectionTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool IntegratedDisclosureSectionTypeOtherDescriptionSpecified
        {
            get { return IntegratedDisclosureSectionTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The percentage of the mortgage interest rate allocated to fund lender purchased mortgage insurance premiums.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOPercent LenderPaidMIInterestRateAdjustmentPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the LenderPaidMIInterestRateAdjustmentPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LenderPaidMIInterestRateAdjustmentPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool LenderPaidMIInterestRateAdjustmentPercentSpecified
        {
            get { return LenderPaidMIInterestRateAdjustmentPercent != null; }
            set { }
        }

        /// <summary>
        /// LTV cutoff for collection of mortgage insurance.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOPercent MI_LTVCutoffPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the MI_LTVCutoffPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MI_LTVCutoffPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool MI_LTVCutoffPercentSpecified
        {
            get { return MI_LTVCutoffPercent != null; }
            set { }
        }

        /// <summary>
        /// Specifies the basis of the mortgage insurance LTV cutoff.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOEnum<MI_LTVCutoffBase> MI_LTVCutoffType;

        /// <summary>
        /// Gets or sets a value indicating whether the MI_LTVCutoffType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MI_LTVCutoffType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MI_LTVCutoffTypeSpecified
        {
            get { return this.MI_LTVCutoffType != null && this.MI_LTVCutoffType.enumValue != MI_LTVCutoffBase.Blank; }
            set { }
        }

        /// <summary>
        /// Actual cancellation date of mortgage insurance coverage.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMODate MICancellationDate;

        /// <summary>
        /// Gets or sets a value indicating whether the MICancellationDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MICancellationDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool MICancellationDateSpecified
        {
            get { return MICancellationDate != null; }
            set { }
        }

        /// <summary>
        /// The number assigned by the private mortgage insurance company to track a loan.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOIdentifier MICertificateIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the MICertificateIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MICertificateIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool MICertificateIdentifierSpecified
        {
            get { return MICertificateIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Defines the party that will obtain an MI Certificate.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOEnum<MICertificationStatusBase> MICertificationStatusType;

        /// <summary>
        /// Gets or sets a value indicating whether the MICertificationStatusType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MICertificationStatusType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MICertificationStatusTypeSpecified
        {
            get { return this.MICertificationStatusType != null && this.MICertificationStatusType.enumValue != MICertificationStatusBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for MI Certification Status Type.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOString MICertificationStatusTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MICertificationStatusTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MICertificationStatusTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MICertificationStatusTypeOtherDescriptionSpecified
        {
            get { return MICertificationStatusTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The number of months of MI escrow that is collected at settlement.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOCount MICollectedNumberOfMonthsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the MICollectedNumberOfMonthsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MICollectedNumberOfMonthsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool MICollectedNumberOfMonthsCountSpecified
        {
            get { return MICollectedNumberOfMonthsCount != null; }
            set { }
        }

        /// <summary>
        /// To convey the private MI company short/common name from whom the private mortgage insurance coverage was obtained.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOEnum<MICompanyNameBase> MICompanyNameType;

        /// <summary>
        /// Gets or sets a value indicating whether the MICompanyNameType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MICompanyNameType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MICompanyNameTypeSpecified
        {
            get { return this.MICompanyNameType != null && this.MICompanyNameType.enumValue != MICompanyNameBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the MI company name when Other is selected for the MI Company Name Type.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOString MICompanyNameTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MICompanyNameTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MICompanyNameTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MICompanyNameTypeOtherDescriptionSpecified
        {
            get { return MICompanyNameTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The date the current MI policy went into effect.
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMODate MICoverageEffectiveDate;

        /// <summary>
        /// Gets or sets a value indicating whether the MICoverageEffectiveDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MICoverageEffectiveDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool MICoverageEffectiveDateSpecified
        {
            get { return MICoverageEffectiveDate != null; }
            set { }
        }

        /// <summary>
        /// The percentage of mortgage insurance coverage obtained.
        /// </summary>
        [XmlElement(Order = 17)]
        public MISMOPercent MICoveragePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the MICoveragePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MICoveragePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool MICoveragePercentSpecified
        {
            get { return MICoveragePercent != null; }
            set { }
        }

        /// <summary>
        /// Specifies the coverage category of mortgage insurance applicable to the loan.
        /// </summary>
        [XmlElement(Order = 18)]
        public MISMOEnum<MICoveragePlanBase> MICoveragePlanType;

        /// <summary>
        /// Gets or sets a value indicating whether the MICoveragePlanType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MICoveragePlanType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MICoveragePlanTypeSpecified
        {
            get { return this.MICoveragePlanType != null && this.MICoveragePlanType.enumValue != MICoveragePlanBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture the description of the type of MI Coverage Plan if Other is selected as the MICoveragePlanType.
        /// </summary>
        [XmlElement(Order = 19)]
        public MISMOString MICoveragePlanTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MICoveragePlanTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MICoveragePlanTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MICoveragePlanTypeOtherDescriptionSpecified
        {
            get { return MICoveragePlanTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The number of months of MI escrow that is to be considered in the cushion for aggregate accounting.
        /// </summary>
        [XmlElement(Order = 20)]
        public MISMOCount MICushionNumberOfMonthsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the MICushionNumberOfMonthsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MICushionNumberOfMonthsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool MICushionNumberOfMonthsCountSpecified
        {
            get { return MICushionNumberOfMonthsCount != null; }
            set { }
        }

        /// <summary>
        /// Specifies the period of time for which mortgage insurance coverage has been obtained/paid.
        /// </summary>
        [XmlElement(Order = 21)]
        public MISMOEnum<MIDurationBase> MIDurationType;

        /// <summary>
        /// Gets or sets a value indicating whether the MIDurationType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIDurationType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIDurationTypeSpecified
        {
            get { return this.MIDurationType != null && this.MIDurationType.enumValue != MIDurationBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture the description of the type of MI Duration if Other is selected as the MIDurationType.
        /// </summary>
        [XmlElement(Order = 22)]
        public MISMOString MIDurationTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MIDurationTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIDurationTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIDurationTypeOtherDescriptionSpecified
        {
            get { return MIDurationTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether mortgage insurance premium is being collected by the lender/servicer and deposited into an escrow account from which a future payment will be made.
        /// </summary>
        [XmlElement(Order = 23)]
        public MISMOIndicator MIEscrowedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the MIEscrowedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIEscrowedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIEscrowedIndicatorSpecified
        {
            get { return MIEscrowedIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether Private Mortgage Insurance Escrow is to be included in the cushion for Aggregate Accounting.
        /// </summary>
        [XmlElement(Order = 24)]
        public MISMOIndicator MIEscrowIncludedInAggregateIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the MIEscrowIncludedInAggregateIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIEscrowIncludedInAggregateIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIEscrowIncludedInAggregateIndicatorSpecified
        {
            get { return MIEscrowIncludedInAggregateIndicator != null; }
            set { }
        }

        /// <summary>
        /// Amount of the first premium paid to MI. For monthly plans, one months premium; for all other plans, Initial Premium Rate times Base Loan Amount.
        /// </summary>
        [XmlElement(Order = 25)]
        public MISMOAmount MIInitialPremiumAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the MIInitialPremiumAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIInitialPremiumAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIInitialPremiumAmountSpecified
        {
            get { return MIInitialPremiumAmount != null; }
            set { }
        }

        /// <summary>
        /// Specifies whether the initial Mortgage Insurance premium is prepaid upon closing or deferred until first principle payment.
        /// </summary>
        [XmlElement(Order = 26)]
        public MISMOEnum<MIInitialPremiumAtClosingBase> MIInitialPremiumAtClosingType;

        /// <summary>
        /// Gets or sets a value indicating whether the MIInitialPremiumAtClosingType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIInitialPremiumAtClosingType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIInitialPremiumAtClosingTypeSpecified
        {
            get { return this.MIInitialPremiumAtClosingType != null && this.MIInitialPremiumAtClosingType.enumValue != MIInitialPremiumAtClosingBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture the description of the type of MI Initial Premium At Closing if Other is selected as the MIInitialPremiumAtClosingType.
        /// </summary>
        [XmlElement(Order = 27)]
        public MISMOString MIInitialPremiumAtClosingTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MIInitialPremiumAtClosingTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIInitialPremiumAtClosingTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIInitialPremiumAtClosingTypeOtherDescriptionSpecified
        {
            get { return MIInitialPremiumAtClosingTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// MI initial premium rate percent used to compute the initial MI premium amount.
        /// </summary>
        [XmlElement(Order = 28)]
        public MISMOPercent MIInitialPremiumRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the MIInitialPremiumRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIInitialPremiumRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIInitialPremiumRatePercentSpecified
        {
            get { return MIInitialPremiumRatePercent != null; }
            set { }
        }

        /// <summary>
        /// The amount of the Mortgage Insurance Premiums that the Borrower has paid for the calendar year.
        /// </summary>
        [XmlElement(Order = 29)]
        public MISMOAmount MIPremiumCalendarYearAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPremiumCalendarYearAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPremiumCalendarYearAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumCalendarYearAmountSpecified
        {
            get { return MIPremiumCalendarYearAmount != null; }
            set { }
        }

        /// <summary>
        /// The amount of the up-front premium that is financed.
        /// </summary>
        [XmlElement(Order = 30)]
        public MISMOAmount MIPremiumFinancedAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPremiumFinancedAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPremiumFinancedAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumFinancedAmountSpecified
        {
            get { return MIPremiumFinancedAmount != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether mortgage insurance premium has been added to loan amount.
        /// </summary>
        [XmlElement(Order = 31)]
        public MISMOIndicator MIPremiumFinancedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPremiumFinancedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPremiumFinancedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumFinancedIndicatorSpecified
        {
            get { return MIPremiumFinancedIndicator != null; }
            set { }
        }

        /// <summary>
        /// Amount of MI premium due from closing. Zero for deferred monthly; otherwise is a months, years or life premium for regular monthly, annual, or life of loan premium plans.
        /// </summary>
        [XmlElement(Order = 32)]
        public MISMOAmount MIPremiumFromClosingAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPremiumFromClosingAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPremiumFromClosingAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumFromClosingAmountSpecified
        {
            get { return MIPremiumFromClosingAmount != null; }
            set { }
        }

        /// <summary>
        /// Defines how the MI premium payments are to be paid.
        /// </summary>
        [XmlElement(Order = 33)]
        public MISMOEnum<MIPremiumPaymentBase> MIPremiumPaymentType;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPremiumPaymentType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPremiumPaymentType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumPaymentTypeSpecified
        {
            get { return this.MIPremiumPaymentType != null && this.MIPremiumPaymentType.enumValue != MIPremiumPaymentBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture the description of the type of MI Premium Payment if Other is selected as the MIPremiumPaymentType.
        /// </summary>
        [XmlElement(Order = 34)]
        public MISMOString MIPremiumPaymentTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPremiumPaymentTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPremiumPaymentTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumPaymentTypeOtherDescriptionSpecified
        {
            get { return MIPremiumPaymentTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The amount of the MI Premium that may be refundable.  
        /// </summary>
        [XmlElement(Order = 35)]
        public MISMOAmount MIPremiumRefundableAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPremiumRefundableAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPremiumRefundableAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumRefundableAmountSpecified
        {
            get { return MIPremiumRefundableAmount != null; }
            set { }
        }

        /// <summary>
        /// A description of the conditions under which the MI Premium is refundable.  
        /// </summary>
        [XmlElement(Order = 36)]
        public MISMOString MIPremiumRefundableConditionsDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPremiumRefundableConditionsDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPremiumRefundableConditionsDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumRefundableConditionsDescriptionSpecified
        {
            get { return MIPremiumRefundableConditionsDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies how the unearned portion of any private mortgage insurance premiums will be treated if the private mortgage insurance coverage is canceled.
        /// </summary>
        [XmlElement(Order = 37)]
        public MISMOEnum<MIPremiumRefundableBase> MIPremiumRefundableType;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPremiumRefundableType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPremiumRefundableType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumRefundableTypeSpecified
        {
            get { return this.MIPremiumRefundableType != null && this.MIPremiumRefundableType.enumValue != MIPremiumRefundableBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture the description of the type of MI Premium Refundable if Other is selected as the MIPremiumRefundableType.
        /// </summary>
        [XmlElement(Order = 38)]
        public MISMOString MIPremiumRefundableTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPremiumRefundableTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPremiumRefundableTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumRefundableTypeOtherDescriptionSpecified
        {
            get { return MIPremiumRefundableTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Defines the source of the MI premium payment.
        /// </summary>
        [XmlElement(Order = 39)]
        public MISMOEnum<MIPremiumSourceBase> MIPremiumSourceType;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPremiumSourceType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPremiumSourceType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumSourceTypeSpecified
        {
            get { return this.MIPremiumSourceType != null && this.MIPremiumSourceType.enumValue != MIPremiumSourceBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field to collect MI premium source when Other is selected for MI Premium Source Type.
        /// </summary>
        [XmlElement(Order = 40)]
        public MISMOString MIPremiumSourceTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPremiumSourceTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPremiumSourceTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumSourceTypeOtherDescriptionSpecified
        {
            get { return MIPremiumSourceTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The reason for removal of individual loan record from an MI reporting process.
        /// </summary>
        [XmlElement(Order = 41)]
        public MISMOIdentifier MIRemovalIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the MIRemovalIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIRemovalIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIRemovalIdentifierSpecified
        {
            get { return MIRemovalIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The projected mandatory date the mortgage insurance must be terminated when a set loan to value percent is reached as identified by HOEPA regulations. Also know as Mandatory MI Termination Date on PMI disclosure documents.
        /// </summary>
        [XmlElement(Order = 42)]
        public MISMODate MIScheduledTerminationDate;

        /// <summary>
        /// Gets or sets a value indicating whether the MIScheduledTerminationDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIScheduledTerminationDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIScheduledTerminationDateSpecified
        {
            get { return MIScheduledTerminationDate != null; }
            set { }
        }

        /// <summary>
        /// The source of mortgage protection insurance.
        /// </summary>
        [XmlElement(Order = 43)]
        public MISMOEnum<MISourceBase> MISourceType;

        /// <summary>
        /// Gets or sets a value indicating whether the MISourceType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MISourceType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MISourceTypeSpecified
        {
            get { return this.MISourceType != null && this.MISourceType.enumValue != MISourceBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for MI Source Type.
        /// </summary>
        [XmlElement(Order = 44)]
        public MISMOString MISourceTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MISourceTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MISourceTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MISourceTypeOtherDescriptionSpecified
        {
            get { return MISourceTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the reason that primary mortgage insurance is not required or provided.
        /// </summary>
        [XmlElement(Order = 45)]
        public MISMOEnum<PrimaryMIAbsenceReasonBase> PrimaryMIAbsenceReasonType;

        /// <summary>
        /// Gets or sets a value indicating whether the PrimaryMIAbsenceReasonType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrimaryMIAbsenceReasonType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrimaryMIAbsenceReasonTypeSpecified
        {
            get { return this.PrimaryMIAbsenceReasonType != null && this.PrimaryMIAbsenceReasonType.enumValue != PrimaryMIAbsenceReasonBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Primary MI Absence Reason Type.
        /// </summary>
        [XmlElement(Order = 46)]
        public MISMOString PrimaryMIAbsenceReasonTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PrimaryMIAbsenceReasonTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrimaryMIAbsenceReasonTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrimaryMIAbsenceReasonTypeOtherDescriptionSpecified
        {
            get { return PrimaryMIAbsenceReasonTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates a required Provider of Service is associated with this fee or payment.
        /// </summary>
        [XmlElement(Order = 47)]
        public MISMOIndicator RequiredProviderOfServiceIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the RequiredProviderOfServiceIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RequiredProviderOfServiceIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool RequiredProviderOfServiceIndicatorSpecified
        {
            get { return RequiredProviderOfServiceIndicator != null; }
            set { }
        }

        /// <summary>
        /// Based on scheduled amortization, date at which the loan due date reaches the midpoint of the loans amortization period (e.g., for a 360 month loan, the date of month number 181).
        /// </summary>
        [XmlElement(Order = 48)]
        public MISMODate ScheduledAmortizationMidpointDate;

        /// <summary>
        /// Gets or sets a value indicating whether the ScheduledAmortizationMidpointDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ScheduledAmortizationMidpointDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ScheduledAmortizationMidpointDateSpecified
        {
            get { return ScheduledAmortizationMidpointDate != null; }
            set { }
        }

        /// <summary>
        /// The date through which MI has been paid by the seller.
        /// </summary>
        [XmlElement(Order = 49)]
        public MISMODate SellerMIPaidToDate;

        /// <summary>
        /// Gets or sets a value indicating whether the SellerMIPaidToDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SellerMIPaidToDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool SellerMIPaidToDateSpecified
        {
            get { return SellerMIPaidToDate != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 50)]
        public MI_DATA_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
