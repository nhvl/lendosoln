namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class MI_DATA
    {
        /// <summary>
        /// Gets a value indicating whether the MI_DATA container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.MiDataDetailSpecified
                || this.MiPaidToSpecified
                || this.MiPoolInsuranceSpecified
                || this.MiPremiumsSpecified
                || this.MiPremiumTaxesSpecified
                || this.RequiredServiceProviderSpecified
                || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// Details on the mortgage insurance data.
        /// </summary>
        [XmlElement("MI_DATA_DETAIL", Order = 0)]
        public MI_DATA_DETAIL MiDataDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the Mortgage Insurance Data Detail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Mortgage Insurance Data Detail element has been assigned a value.</value>
        [XmlIgnore]
        public bool MiDataDetailSpecified
        {
            get { return this.MiDataDetail != null && this.MiDataDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The payee of the mortgage insurance policy.
        /// </summary>
        [XmlElement("MI_PAID_TO", Order = 1)]
        public PAID_TO MiPaidTo;

        /// <summary>
        /// Gets or sets a value indicating whether the Mortgage Insurance Paid To element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Mortgage Insurance Paid To element has been assigned a value.</value>
        [XmlIgnore]
        public bool MiPaidToSpecified
        {
            get { return this.MiPaidTo != null && this.MiPaidTo.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Mortgage insurance pool insurance.
        /// </summary>
        [XmlElement("MI_POOL_INSURANCE", Order = 2)]
        public MI_POOL_INSURANCE MiPoolInsurance;

        /// <summary>
        /// Gets or sets a value indicating whether the Mortgage Insurance Pool Insurance element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Mortgage Insurance Pool Insurance element has been assigned a value.</value>
        [XmlIgnore]
        public bool MiPoolInsuranceSpecified
        {
            get { return this.MiPoolInsurance != null && this.MiPoolInsurance.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Premium taxes on mortgage insurance.
        /// </summary>
        [XmlElement("MI_PREMIUM_TAXES", Order = 3)]
        public MI_PREMIUM_TAXES MiPremiumTaxes;

        /// <summary>
        /// Gets or sets a value indicating whether the Mortgage Insurance PremiumTaxes element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Mortgage Insurance PremiumTaxes element has been assigned a value.</value>
        [XmlIgnore]
        public bool MiPremiumTaxesSpecified
        {
            get { return this.MiPremiumTaxes != null && this.MiPremiumTaxes.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Mortgage insurance premiums.
        /// </summary>
        [XmlElement("MI_PREMIUMS", Order = 4)]
        public MI_PREMIUMS MiPremiums;

        /// <summary>
        /// Gets or sets a value indicating whether the Mortgage Insurance Premiums element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Mortgage Insurance Premiums element has been assigned a value.</value>
        [XmlIgnore]
        public bool MiPremiumsSpecified
        {
            get { return this.MiPremiums != null && this.MiPremiums.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A service provider required by the loan owner.
        /// </summary>
        [XmlElement("REQUIRED_SERVICE_PROVIDER", Order = 5)]
        public REQUIRED_SERVICE_PROVIDER RequiredServiceProvider;

        /// <summary>
        /// Gets or sets a value indicating whether the RequiredServiceProvider element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RequiredServiceProvider element has been assigned a value.</value>
        [XmlIgnore]
        public bool RequiredServiceProviderSpecified
        {
            get { return this.RequiredServiceProvider != null && this.RequiredServiceProvider.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 6)]
        public MI_DATA_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
