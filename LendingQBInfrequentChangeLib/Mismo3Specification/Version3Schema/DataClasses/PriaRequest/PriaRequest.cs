namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PRIA_REQUEST
    {
        /// <summary>
        /// Gets a value indicating whether the PRIA_REQUEST container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.OriginatingRecordingRequestSpecified
                    || this.PRIAConsiderationsSpecified
                    || this.PRIARequestDetailSpecified
                    || this.RecordingTransactionIdentifierSpecified;
            }
        }

        /// <summary>
        /// An originating recording related to the request.
        /// </summary>
        [XmlElement("ORIGINATING_RECORDING_REQUEST", Order = 0)]
        public ORIGINATING_RECORDING_REQUEST OriginatingRecordingRequest;

        /// <summary>
        /// Gets or sets a value indicating whether the OriginatingRecordingRequest element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OriginatingRecordingRequest element has been assigned a value.</value>
        [XmlIgnore]
        public bool OriginatingRecordingRequestSpecified
        {
            get { return this.OriginatingRecordingRequest != null && this.OriginatingRecordingRequest.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Considerations about this request.
        /// </summary>
        [XmlElement("PRIA_CONSIDERATIONS", Order = 1)]
        public PRIA_CONSIDERATIONS PRIAConsiderations;

        /// <summary>
        /// Gets or sets a value indicating whether the Property Records Industry Association Considerations element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Property Records Industry Association Considerations element has been assigned a value.</value>
        [XmlIgnore]
        public bool PRIAConsiderationsSpecified
        {
            get { return this.PRIAConsiderations != null && this.PRIAConsiderations.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details about a Property Records Industry Association request.
        /// </summary>
        [XmlElement("PRIA_REQUEST_DETAIL", Order = 2)]
        public PRIA_REQUEST_DETAIL PRIARequestDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the Property Records Industry Association RequestDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Property Records Industry Association RequestDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool PRIARequestDetailSpecified
        {
            get { return this.PRIARequestDetail != null && this.PRIARequestDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Identifier for a recording transaction.
        /// </summary>
        [XmlElement("RECORDING_TRANSACTION_IDENTIFIER", Order = 3)]
        public RECORDING_TRANSACTION_IDENTIFIER RecordingTransactionIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the RecordingTransactionIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RecordingTransactionIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool RecordingTransactionIdentifierSpecified
        {
            get { return this.RecordingTransactionIdentifier != null && this.RecordingTransactionIdentifier.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public PRIA_REQUEST_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
