namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PRIA_REQUEST_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the PRIA_REQUEST_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.PRIARecordingJurisdictionIdentifierSpecified
                    || this.PRIARequestRelatedDocumentsIndicatorSpecified
                    || this.PRIARequestTypeOtherDescriptionSpecified
                    || this.PRIARequestTypeSpecified;
            }
        }

        /// <summary>
        /// Identifies the recording jurisdiction in which the document needs to be recorded on public record. The identifier scheme is represented by:  - the first two characters are the United States Postal Service (USPS) state or territory abbreviation.  - the next three characters are a three digit numeric value.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIdentifier PRIARecordingJurisdictionIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the PRIARecordingJurisdictionIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PRIARecordingJurisdictionIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool PRIARecordingJurisdictionIdentifierSpecified
        {
            get { return PRIARecordingJurisdictionIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether the documents sent are related to each other.  If "yes", all documents must pass recording tests for any to be recorded.  If "no", one document can fail recording without preventing the remaining documents from being recorded.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOIndicator PRIARequestRelatedDocumentsIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PRIARequestRelatedDocumentsIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PRIARequestRelatedDocumentsIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PRIARequestRelatedDocumentsIndicatorSpecified
        {
            get { return PRIARequestRelatedDocumentsIndicator != null; }
            set { }
        }

        /// <summary>
        /// A description of the action being requested.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<PRIARequestBase> PRIARequestType;

        /// <summary>
        /// Gets or sets a value indicating whether the PRIARequestType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PRIARequestType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PRIARequestTypeSpecified
        {
            get { return this.PRIARequestType != null && this.PRIARequestType.enumValue != PRIARequestBase.Blank; }
            set { }
        }

        /// <summary>
        /// A description of the type of request when Other is selected from the enumerated list.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString PRIARequestTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PRIARequestTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PRIARequestTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PRIARequestTypeOtherDescriptionSpecified
        {
            get { return PRIARequestTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public PRIA_REQUEST_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
