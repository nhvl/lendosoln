namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class VALUATION_RECONCILIATION
    {
        /// <summary>
        /// Gets a value indicating whether the VALUATION_RECONCILIATION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.RepairSpecified
                    || this.ValuationReconciliationDetailSpecified;
            }
        }

        /// <summary>
        /// Information on needed repairs.
        /// </summary>
        [XmlElement("REPAIR", Order = 0)]
        public REPAIR Repair;

        /// <summary>
        /// Gets or sets a value indicating whether the Repair element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Repair element has been assigned a value.</value>
        [XmlIgnore]
        public bool RepairSpecified
        {
            get { return this.Repair != null && this.Repair.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Information about the valuation reconciliation.
        /// </summary>
        [XmlElement("VALUATION_RECONCILIATION_DETAIL", Order = 1)]
        public VALUATION_RECONCILIATION_DETAIL ValuationReconciliationDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the ValuationReconciliationDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ValuationReconciliationDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool ValuationReconciliationDetailSpecified
        {
            get { return this.ValuationReconciliationDetail != null && this.ValuationReconciliationDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public VALUATION_RECONCILIATION_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
