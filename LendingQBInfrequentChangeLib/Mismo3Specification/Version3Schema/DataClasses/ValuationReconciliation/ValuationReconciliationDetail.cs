namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class VALUATION_RECONCILIATION_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the VALUATION_RECONCILIATION_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.OpinionOfValueAmountSpecified
                    || this.PropertyValuationConditionalConclusionTypeOtherDescriptionSpecified
                    || this.PropertyValuationConditionalConclusionTypeSpecified
                    || this.ValuationReconciliationConditionsCommentDescriptionSpecified
                    || this.ValuationReconciliationSummaryCommentDescriptionSpecified;
            }
        }

        /// <summary>
        /// The reconciled opinion of value in the appraisal report representing the conclusion of the approaches to value methods used by the appraiser.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount OpinionOfValueAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the OpinionOfValueAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OpinionOfValueAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool OpinionOfValueAmountSpecified
        {
            get { return OpinionOfValueAmount != null; }
            set { }
        }

        /// <summary>
        /// Specifies the conclusions of the appraisal that there is a dependency on future repairs or activities.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<PropertyValuationConditionalConclusionBase> PropertyValuationConditionalConclusionType;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyValuationConditionalConclusionType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyValuationConditionalConclusionType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyValuationConditionalConclusionTypeSpecified
        {
            get { return this.PropertyValuationConditionalConclusionType != null && this.PropertyValuationConditionalConclusionType.enumValue != PropertyValuationConditionalConclusionBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Property Valuation Conditional Conclusion Type.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString PropertyValuationConditionalConclusionTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyValuationConditionalConclusionTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyValuationConditionalConclusionTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyValuationConditionalConclusionTypeOtherDescriptionSpecified
        {
            get { return PropertyValuationConditionalConclusionTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to summarize and clarify the conditions of the subject property upon which its valuation is based.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString ValuationReconciliationConditionsCommentDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ValuationReconciliationConditionsCommentDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ValuationReconciliationConditionsCommentDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ValuationReconciliationConditionsCommentDescriptionSpecified
        {
            get { return ValuationReconciliationConditionsCommentDescription != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe or reconcile the different property valuation methods used to arrive at the final property valuation.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString ValuationReconciliationSummaryCommentDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ValuationReconciliationSummaryCommentDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ValuationReconciliationSummaryCommentDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ValuationReconciliationSummaryCommentDescriptionSpecified
        {
            get { return ValuationReconciliationSummaryCommentDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 5)]
        public VALUATION_RECONCILIATION_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
