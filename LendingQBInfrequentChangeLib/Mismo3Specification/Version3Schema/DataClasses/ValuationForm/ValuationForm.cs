namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class VALUATION_FORM
    {
        /// <summary>
        /// Gets a value indicating whether the VALUATION_FORM container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.PropertyValuationFormIdentifierSpecified
                    || this.PropertyValuationFormTypeOtherDescriptionSpecified
                    || this.PropertyValuationFormTypeSpecified
                    || this.PropertyValuationFormVersionIdentifierSpecified
                    || this.ValuationSoftwareProductNameSpecified
                    || this.ValuationSoftwareProductVersionIdentifierSpecified;
            }
        }

        /// <summary>
        /// The name or number assigned to a form by the form author.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIdentifier PropertyValuationFormIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyValuationFormIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyValuationFormIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyValuationFormIdentifierSpecified
        {
            get { return PropertyValuationFormIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Specifies the form used to provide the property valuation.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<PropertyValuationFormBase> PropertyValuationFormType;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyValuationFormType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyValuationFormType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyValuationFormTypeSpecified
        {
            get { return this.PropertyValuationFormType != null && this.PropertyValuationFormType.enumValue != PropertyValuationFormBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Property Valuation Form Type.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString PropertyValuationFormTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyValuationFormTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyValuationFormTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyValuationFormTypeOtherDescriptionSpecified
        {
            get { return PropertyValuationFormTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The version identifier for the property valuation form, as assigned by the publisher of the form.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOIdentifier PropertyValuationFormVersionIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyValuationFormVersionIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyValuationFormVersionIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyValuationFormVersionIdentifierSpecified
        {
            get { return PropertyValuationFormVersionIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Specifies the name of valuation software product used to create the XML data.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString ValuationSoftwareProductName;

        /// <summary>
        /// Gets or sets a value indicating whether the ValuationSoftwareProductName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ValuationSoftwareProductName element has been assigned a value.</value>
        [XmlIgnore]
        public bool ValuationSoftwareProductNameSpecified
        {
            get { return ValuationSoftwareProductName != null; }
            set { }
        }

        /// <summary>
        /// Specifies the version of the valuation software product used to create the XML data.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOIdentifier ValuationSoftwareProductVersionIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the ValuationSoftwareProductVersionIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ValuationSoftwareProductVersionIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool ValuationSoftwareProductVersionIdentifierSpecified
        {
            get { return ValuationSoftwareProductVersionIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 6)]
        public VALUATION_FORM_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
