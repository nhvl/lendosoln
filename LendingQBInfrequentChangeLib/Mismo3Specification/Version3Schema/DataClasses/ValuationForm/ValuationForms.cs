namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class VALUATION_FORMS
    {
        /// <summary>
        /// Gets a value indicating whether the VALUATION_FORMS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ValuationFormSpecified;
            }
        }

        /// <summary>
        /// Container for multiple valuation forms.
        /// </summary>
        [XmlElement("VALUATION_FORM", Order = 0)]
		public List<VALUATION_FORM> ValuationForm = new List<VALUATION_FORM>();

        /// <summary>
        /// Gets or sets a value indicating whether the ValuationForm element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ValuationForm element has been assigned a value.</value>
        [XmlIgnore]
        public bool ValuationFormSpecified
        {
            get { return this.ValuationForm != null && this.ValuationForm.Count(v => v != null && v.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public VALUATION_FORMS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
