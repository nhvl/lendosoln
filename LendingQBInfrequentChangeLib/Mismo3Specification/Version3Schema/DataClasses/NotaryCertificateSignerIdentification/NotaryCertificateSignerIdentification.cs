namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class NOTARY_CERTIFICATE_SIGNER_IDENTIFICATION
    {
        /// <summary>
        /// Gets a value indicating whether the NOTARY_CERTIFICATE_SIGNER_IDENTIFICATION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.NotaryCertificateSignerIdentificationDescriptionSpecified
                    || this.NotaryCertificateSignerIdentificationTypeSpecified;
            }
        }

        /// <summary>
        /// The description of the type of identification provided by the party signing before the notary, e.g. drivers license and state of issuance.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString NotaryCertificateSignerIdentificationDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the NotaryCertificateSignerIdentificationDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NotaryCertificateSignerIdentificationDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool NotaryCertificateSignerIdentificationDescriptionSpecified
        {
            get { return NotaryCertificateSignerIdentificationDescription != null; }
            set { }
        }

        /// <summary>
        /// The type of identification provided by the party signing before the notary.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<NotaryCertificateSignerIdentificationBase> NotaryCertificateSignerIdentificationType;

        /// <summary>
        /// Gets or sets a value indicating whether the NotaryCertificateSignerIdentificationType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NotaryCertificateSignerIdentificationType element has been assigned a value.</value>
        [XmlIgnore]
        public bool NotaryCertificateSignerIdentificationTypeSpecified
        {
            get { return this.NotaryCertificateSignerIdentificationType != null && this.NotaryCertificateSignerIdentificationType.enumValue != NotaryCertificateSignerIdentificationBase.Blank; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public NOTARY_CERTIFICATE_SIGNER_IDENTIFICATION_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
