namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class LOAN_PRODUCT_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the LOAN_PRODUCT_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AntiSteeringComparisonTypeOtherDescriptionSpecified
                || this.AntiSteeringComparisonTypeSpecified
                || this.BorrowerSelectedAntiSteeringComparisonTypeOtherDescriptionSpecified
                || this.BorrowerSelectedAntiSteeringComparisonTypeSpecified
                || this.DiscountPointsTotalAmountSpecified
                || this.FNMHomeImprovementProductTypeSpecified
                || this.GFEComparisonTypeOtherDescriptionSpecified
                || this.GFEComparisonTypeSpecified
                || this.LoanProductStatusTypeSpecified
                || this.NegotiatedRequirementDescriptionSpecified
                || this.OriginationPointsOrFeesTotalAmountSpecified
                || this.OriginationPointsOrFeesTotalRatePercentSpecified
                || this.ProductDescriptionSpecified
                || this.ProductIdentifierSpecified
                || this.ProductNameSpecified
                || this.ProductProviderNameSpecified
                || this.ProductProviderTypeOtherDescriptionSpecified
                || this.ProductProviderTypeSpecified
                || this.RefinanceProgramIdentifierSpecified
                || this.SettlementChargesAmountSpecified
                || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// Specifies how the loan conditions have been changed for this comparison instance for the purposes of anti steering (safe harbor) regulatory requirements.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<AntiSteeringComparisonBase> AntiSteeringComparisonType;

        /// <summary>
        /// Gets or sets a value indicating whether the AntiSteeringComparisonType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AntiSteeringComparisonType element has been assigned a value.</value>
        [XmlIgnore]
        public bool AntiSteeringComparisonTypeSpecified
        {
            get { return this.AntiSteeringComparisonType != null && this.AntiSteeringComparisonType.enumValue != AntiSteeringComparisonBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field which contains the description of the Anti-Steering comparison type when Other is selected as the Anti-Steering Comparison Type.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString AntiSteeringComparisonTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the AntiSteeringComparisonTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AntiSteeringComparisonTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool AntiSteeringComparisonTypeOtherDescriptionSpecified
        {
            get { return AntiSteeringComparisonTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the loan characteristic(s) used as the basis for the comparison loan chosen by the Borrower.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<AntiSteeringComparisonBase> BorrowerSelectedAntiSteeringComparisonType;

        /// <summary>
        /// Gets or sets a value indicating whether the BorrowerSelectedAntiSteeringComparisonType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BorrowerSelectedAntiSteeringComparisonType element has been assigned a value.</value>
        [XmlIgnore]
        public bool BorrowerSelectedAntiSteeringComparisonTypeSpecified
        {
            get { return this.BorrowerSelectedAntiSteeringComparisonType != null && this.BorrowerSelectedAntiSteeringComparisonType.enumValue != AntiSteeringComparisonBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field which contains the description of the Anti-Steering comparison type when Other is selected as the Borrower Selected Anti-Steering Comparison Type.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString BorrowerSelectedAntiSteeringComparisonTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the BorrowerSelectedAntiSteeringComparisonTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BorrowerSelectedAntiSteeringComparisonTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool BorrowerSelectedAntiSteeringComparisonTypeOtherDescriptionSpecified
        {
            get { return BorrowerSelectedAntiSteeringComparisonTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The total dollar amount estimated or paid as discount points for this loan product.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOAmount DiscountPointsTotalAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the DiscountPointsTotalAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DiscountPointsTotalAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool DiscountPointsTotalAmountSpecified
        {
            get { return DiscountPointsTotalAmount != null; }
            set { }
        }

        /// <summary>
        /// Denotes the Fannie Mae specific home improvement product.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOEnum<FNMHomeImprovementProductBase> FNMHomeImprovementProductType;

        /// <summary>
        /// Gets or sets a value indicating whether the FNMHomeImprovementProductType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FNMHomeImprovementProductType element has been assigned a value.</value>
        [XmlIgnore]
        public bool FNMHomeImprovementProductTypeSpecified
        {
            get { return this.FNMHomeImprovementProductType != null && this.FNMHomeImprovementProductType.enumValue != FNMHomeImprovementProductBase.Blank; }
            set { }
        }

        /// <summary>
        /// Specifies how the loan conditions have been changed for this comparison instance for the purposes of GFE requirements.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOEnum<GFEComparisonBase> GFEComparisonType;

        /// <summary>
        /// Gets or sets a value indicating whether the GFEComparisonType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GFEComparisonType element has been assigned a value.</value>
        [XmlIgnore]
        public bool GFEComparisonTypeSpecified
        {
            get { return this.GFEComparisonType != null && this.GFEComparisonType.enumValue != GFEComparisonBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field which contains the description of the GFE comparison type when Other is selected as the GFE Comparison Type.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOString GFEComparisonTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the GFEComparisonTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GFEComparisonTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool GFEComparisonTypeOtherDescriptionSpecified
        {
            get { return GFEComparisonTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The status of the particular loan product.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOEnum<LoanProductStatusBase> LoanProductStatusType;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanProductStatusType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanProductStatusType element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanProductStatusTypeSpecified
        {
            get { return this.LoanProductStatusType != null && this.LoanProductStatusType.enumValue != LoanProductStatusBase.Blank; }
            set { }
        }

        /// <summary>
        /// Describes an addition or exception to standard requirements.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOString NegotiatedRequirementDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the NegotiatedRequirementDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NegotiatedRequirementDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool NegotiatedRequirementDescriptionSpecified
        {
            get { return NegotiatedRequirementDescription != null; }
            set { }
        }

        /// <summary>
        /// The total dollar amount estimated or paid as origination points or fees for this loan product at the selected rate.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOAmount OriginationPointsOrFeesTotalAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the OriginationPointsOrFeesTotalAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OriginationPointsOrFeesTotalAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool OriginationPointsOrFeesTotalAmountSpecified
        {
            get { return OriginationPointsOrFeesTotalAmount != null; }
            set { }
        }

        /// <summary>
        /// The total dollar amount estimated or paid as origination points or fees for this loan product, reflected as a percentage of the loan amount.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOPercent OriginationPointsOrFeesTotalRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the OriginationPointsOrFeesTotalRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OriginationPointsOrFeesTotalRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool OriginationPointsOrFeesTotalRatePercentSpecified
        {
            get { return OriginationPointsOrFeesTotalRatePercent != null; }
            set { }
        }

        /// <summary>
        /// A free-form text description of a Product, which is a provider-defined offering of goods or services, including mortgage loans.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOString ProductDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ProductDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProductDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProductDescriptionSpecified
        {
            get { return ProductDescription != null; }
            set { }
        }

        /// <summary>
        /// The providers unique identifier of a product, which is a provider-defined offering of goods or services, including mortgage loans.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOIdentifier ProductIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the ProductIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProductIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProductIdentifierSpecified
        {
            get { return ProductIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The business name of a product, which is a provider defined offering of goods or services, including mortgage loans.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOString ProductName;

        /// <summary>
        /// Gets or sets a value indicating whether the ProductName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProductName element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProductNameSpecified
        {
            get { return ProductName != null; }
            set { }
        }

        /// <summary>
        /// The name of the provider of the defined offering of goods or services.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOString ProductProviderName;

        /// <summary>
        /// Gets or sets a value indicating whether the ProductProviderName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProductProviderName element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProductProviderNameSpecified
        {
            get { return ProductProviderName != null; }
            set { }
        }

        /// <summary>
        /// Specifies the role of the provider of the defined offering of goods or services.
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMOEnum<ProductProviderBase> ProductProviderType;

        /// <summary>
        /// Gets or sets a value indicating whether the ProductProviderType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProductProviderType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProductProviderTypeSpecified
        {
            get { return this.ProductProviderType != null && this.ProductProviderType.enumValue != ProductProviderBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Product Provider Type.
        /// </summary>
        [XmlElement(Order = 17)]
        public MISMOString ProductProviderTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ProductProviderTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProductProviderTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProductProviderTypeOtherDescriptionSpecified
        {
            get { return ProductProviderTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Identifies the refinance program associated with the loan as identified by a specific entity.
        /// </summary>
        [XmlElement(Order = 18)]
        public MISMOIdentifier RefinanceProgramIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the RefinanceProgramIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RefinanceProgramIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool RefinanceProgramIdentifierSpecified
        {
            get { return RefinanceProgramIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The total dollar amount estimated or paid as settlement charges for this loan product.
        /// </summary>
        [XmlElement(Order = 19)]
        public MISMOAmount SettlementChargesAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the SettlementChargesAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SettlementChargesAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool SettlementChargesAmountSpecified
        {
            get { return SettlementChargesAmount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 20)]
        public LOAN_PRODUCT_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
