namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class LOAN_PRODUCT
    {
        /// <summary>
        /// Gets a value indicating whether the LOAN_PRODUCT container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get 
            {
                return this.DisqualificationReasonsSpecified
                || this.LoanPriceQuotesSpecified
                || this.LoanProductDetailSpecified
                || this.LocksSpecified
                || this.ProductCategorySpecified
                || this.ProductComponentsSpecified
                || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// Disqualification reasons related to a loan product.
        /// </summary>
        [XmlElement("DISQUALIFICATION_REASONS", Order = 0)]
        public DISQUALIFICATION_REASONS DisqualificationReasons;

        /// <summary>
        /// Gets or sets a value indicating whether the DisqualificationReasons element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DisqualificationReasons element has been assigned a value.</value>
        [XmlIgnore]
        public bool DisqualificationReasonsSpecified
        {
            get { return this.DisqualificationReasons != null && this.DisqualificationReasons.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Price quotes on a loan product.
        /// </summary>
        [XmlElement("LOAN_PRICE_QUOTES", Order = 1)]
        public LOAN_PRICE_QUOTES LoanPriceQuotes;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanPriceQuotes element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanPriceQuotes element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanPriceQuotesSpecified
        {
            get { return this.LoanPriceQuotes != null && this.LoanPriceQuotes.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details on a loan product.
        /// </summary>
        [XmlElement("LOAN_PRODUCT_DETAIL", Order = 2)]
        public LOAN_PRODUCT_DETAIL LoanProductDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanProductDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanProductDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanProductDetailSpecified
        {
            get { return LoanProductDetail != null && LoanProductDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Locks on a loan product.
        /// </summary>
        [XmlElement("LOCKS", Order = 3)]
        public LOCKS Locks;

        /// <summary>
        /// Gets or sets a value indicating whether the Locks element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Locks element has been assigned a value.</value>
        [XmlIgnore]
        public bool LocksSpecified
        {
            get { return Locks != null && Locks.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Category of a loan product.
        /// </summary>
        [XmlElement("PRODUCT_CATEGORY", Order = 4)]
        public PRODUCT_CATEGORY ProductCategory;

        /// <summary>
        /// Gets or sets a value indicating whether the ProductCategory element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProductCategory element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProductCategorySpecified
        {
            get { return this.ProductCategory != null && this.ProductCategory.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Components of a loan product.
        /// </summary>
        [XmlElement("PRODUCT_COMPONENTS", Order = 5)]
        public PRODUCT_COMPONENTS ProductComponents;

        /// <summary>
        /// Gets or sets a value indicating whether the ProductComponents element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProductComponents element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProductComponentsSpecified
        {
            get { return this.ProductComponents != null && this.ProductComponents.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 6)]
        public LOAN_PRODUCT_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
