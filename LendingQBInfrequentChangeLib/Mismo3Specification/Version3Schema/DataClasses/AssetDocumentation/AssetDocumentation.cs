namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class ASSET_DOCUMENTATION
    {
        /// <summary>
        /// Gets a value indicating whether the ASSET_DOCUMENTATION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AssetDocumentTypeOtherDescriptionSpecified
                    || this.AssetDocumentTypeSpecified
                    || this.AssetVerificationRangeCountSpecified
                    || this.AssetVerificationRangeTypeOtherDescriptionSpecified
                    || this.AssetVerificationRangeTypeSpecified
                    || this.DocumentationStateTypeOtherDescriptionSpecified
                    || this.DocumentationStateTypeSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// The type of documentation used as a means of identifying or verifying assets used in the transaction.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<AssetDocumentBase> AssetDocumentType;

        /// <summary>
        /// Gets or sets a value indicating whether the AssetDocumentType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AssetDocumentType element has been assigned a value.</value>
        [XmlIgnore]
        public bool AssetDocumentTypeSpecified
        {
            get { return this.AssetDocumentType != null && this.AssetDocumentType.enumValue != AssetDocumentBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture the Asset Document Type if Other is selected. 
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString AssetDocumentTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the AssetDocumentTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AssetDocumentTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool AssetDocumentTypeOtherDescriptionSpecified
        {
            get { return AssetDocumentTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The number of time periods as defined by the Verification Range Type for which documentation is collected.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOCount AssetVerificationRangeCount;

        /// <summary>
        /// Gets or sets a value indicating whether the AssetVerificationRangeCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AssetVerificationRangeCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool AssetVerificationRangeCountSpecified
        {
            get { return AssetVerificationRangeCount != null; }
            set { }
        }

        /// <summary>
        /// Specifies the period or range of time for which the specific type of Documentation Type is collected.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOEnum<VerificationRangeBase> AssetVerificationRangeType;

        /// <summary>
        /// Gets or sets a value indicating whether the AssetVerificationRangeType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AssetVerificationRangeType element has been assigned a value.</value>
        [XmlIgnore]
        public bool AssetVerificationRangeTypeSpecified
        {
            get { return this.AssetVerificationRangeType != null && this.AssetVerificationRangeType.enumValue != VerificationRangeBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when other is selected for Verification Range Type.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString AssetVerificationRangeTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the AssetVerificationRangeTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AssetVerificationRangeTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool AssetVerificationRangeTypeOtherDescriptionSpecified
        {
            get { return AssetVerificationRangeTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Identifies the state of the documentation associated with the loan.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOEnum<DocumentationStateBase> DocumentationStateType;

        /// <summary>
        /// Gets or sets a value indicating whether the DocumentationStateType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DocumentationStateType element has been assigned a value.</value>
        [XmlIgnore]
        public bool DocumentationStateTypeSpecified
        {
            get { return this.DocumentationStateType != null && this.DocumentationStateType.enumValue != DocumentationStateBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Documentation State Type.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOString DocumentationStateTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the DocumentationStateTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DocumentationStateTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool DocumentationStateTypeOtherDescriptionSpecified
        {
            get { return DocumentationStateTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 7)]
        public ASSET_DOCUMENTATION_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
