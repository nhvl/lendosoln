namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class ASSET_DOCUMENTATIONS
    {
        /// <summary>
        /// Gets a value indicating whether the ASSET_DOCUMENTATIONS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AssetDocumentationSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of asset documentations.
        /// </summary>
        [XmlElement("ASSET_DOCUMENTATION", Order = 0)]
		public List<ASSET_DOCUMENTATION> AssetDocumentation = new List<ASSET_DOCUMENTATION>();

        /// <summary>
        /// Gets or sets a value indicating whether the AssetDocumentation element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AssetDocumentation element has been assigned a value.</value>
        [XmlIgnore]
        public bool AssetDocumentationSpecified
        {
            get { return this.AssetDocumentation != null && this.AssetDocumentation.Count(a => a != null && a.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public ASSET_DOCUMENTATIONS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
