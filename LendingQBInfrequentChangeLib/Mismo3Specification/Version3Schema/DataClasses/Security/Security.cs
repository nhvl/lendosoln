namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets a value indicating whether the SecurityInvestors container has a value to serialize.
    /// </summary>
    /// <returns>A boolean indicating whether the SecurityInvestors element has been assigned a value.</returns>
    public partial class SECURITY
    {
        /// <summary>
        /// Gets a value indicating whether the SECURITY container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get { return this.SecurityDetailSpecified || this.SecurityInvestorsSpecified; }
        }

        /// <summary>
        /// Holds the details of this security.
        /// </summary>
        [XmlElement("SECURITY_DETAIL", Order = 1)]
        public SECURITY_DETAIL SecurityDetail;

        /// <summary>
        /// Gets a value indicating whether the SecurityDetail has a value to serialize.
        /// </summary>
        /// <returns>A boolean indicating whether the SecurityDetail element has been assigned a value.</returns>
        [XmlIgnore]
        public bool SecurityDetailSpecified
        {
            get { return SecurityDetail != null && SecurityDetail.ShouldSerialize; }
        }

        /// <summary>
        /// Contains a list of investors in this security.
        /// </summary>
        [XmlElement("SECURITY_INVESTORS", Order = 0)]
        public SECURITY_INVESTORS SecurityInvestors;

        /// <summary>
        /// Gets a value indicating whether the SecurityInvestors container has a value to serialize.
        /// </summary>
        /// <returns>A boolean indicating whether the SecurityInvestors element has been assigned a value.</returns>
        [XmlIgnore]
        public bool SecurityInvestorsSpecified
        {
            get { return SecurityInvestors != null && SecurityInvestors.ShouldSerialize; }
        }
    }
}
