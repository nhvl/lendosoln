namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets a value indicating whether the SecurityTradeBookEntryDate has a value to serialize.
    /// </summary>
    /// <returns>A boolean indicating whether the SecurityTradeBookEntryDate element has been assigned a value.</returns>
    public partial class SECURITY_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the SECURITY_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get { return SecurityTradeBookEntryDateSpecified(); }
        }

        /// <summary>
        /// The date that the security will be delivered to the designated book entry account.
        /// </summary>
        [XmlElement("SecurityTradeBookEntryDate", Order = 0)]
        public MISMODate SecurityTradeBookEntryDate;

        /// <summary>
        /// Gets a value indicating whether the SecurityTradeBookEntryDate has a value to serialize.
        /// </summary>
        /// <returns>A boolean indicating whether the SecurityTradeBookEntryDate element has been assigned a value.</returns>
        public bool SecurityTradeBookEntryDateSpecified()
        {
            return SecurityTradeBookEntryDate != null && !string.IsNullOrEmpty(SecurityTradeBookEntryDate.Value);
        }
    }
}
