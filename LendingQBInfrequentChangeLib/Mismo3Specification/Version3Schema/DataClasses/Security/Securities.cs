namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets a value indicating whether the Security has a value to serialize.
    /// </summary>
    /// <returns>A boolean indicating whether the Security element has been assigned a value.</returns>
    public partial class SECURITIES
    {
        /// <summary>
        /// Creates an instance of the SECURITIES class.
        /// </summary>
        public SECURITIES()
        {
        }

        /// <summary>
        /// Creates an instance of the <see cref="SECURITIES"/> class.
        /// </summary>
        /// <param name="securities">Securities to add to the new object.</param>
        public SECURITIES(params SECURITY[] securities)
        {
            foreach (SECURITY security in securities)
            {
                this.Security.Add(security);
            }
        }

        /// <summary>
        /// Gets a value indicating whether the SECURITIES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get { return SecuritySpecified; }
        }

        /// <summary>
        /// A list of securities.
        /// </summary>
        [XmlElement("SECURITY", Order = 0)]
        public List<SECURITY> Security = new List<SECURITY>();

        /// <summary>
        /// Gets a value indicating whether the Security has a value to serialize.
        /// </summary>
        /// <returns>A boolean indicating whether the Security element has been assigned a value.</returns>
        [XmlIgnore]
        public bool SecuritySpecified
        {
            get { return this.Security != null && this.Security.Count(f => f != null && f.ShouldSerialize) > 0; }
        }
    }
}
