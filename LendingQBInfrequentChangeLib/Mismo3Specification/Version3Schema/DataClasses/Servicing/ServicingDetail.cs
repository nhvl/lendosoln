namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class SERVICING_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the SERVICING_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DefaultStatusTypeOtherDescriptionSpecified
                    || this.DefaultStatusTypeSpecified
                    || this.ExtensionSpecified
                    || this.LoanActivityReportingTransactionIdentifierSpecified
                    || this.LoanImminentDefaultSourceTypeOtherDescriptionSpecified
                    || this.LoanImminentDefaultSourceTypeSpecified
                    || this.PreviouslyReportedInformationRevisionIndicatorSpecified
                    || this.ServicerWelcomeCallPerformedDateSpecified
                    || this.SFDMSAutomatedDefaultProcessingIdentifierSpecified
                    || this.TitleReportLastReceivedDateSpecified;
            }
        }

        /// <summary>
        /// Indicates the default status of a particular loan.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<DefaultStatusBase> DefaultStatusType;

        /// <summary>
        /// Gets or sets a value indicating whether the DefaultStatusType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DefaultStatusType element has been assigned a value.</value>
        [XmlIgnore]
        public bool DefaultStatusTypeSpecified
        {
            get { return this.DefaultStatusType != null && this.DefaultStatusType.enumValue != DefaultStatusBase.Blank; }
            set { }
        }

        /// <summary>
        /// Description of the current status of the loan payments when the type is "Other". 
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString DefaultStatusTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the DefaultStatusTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DefaultStatusTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool DefaultStatusTypeOtherDescriptionSpecified
        {
            get { return DefaultStatusTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A unique identifier for a loan activity reporting transaction.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOIdentifier LoanActivityReportingTransactionIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanActivityReportingTransactionIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanActivityReportingTransactionIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanActivityReportingTransactionIdentifierSpecified
        {
            get { return LoanActivityReportingTransactionIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The source of the information that the loan is in a state of imminent default.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOEnum<LoanImminentDefaultSourceBase> LoanImminentDefaultSourceType;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanImminentDefaultSourceType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanImminentDefaultSourceType element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanImminentDefaultSourceTypeSpecified
        {
            get { return this.LoanImminentDefaultSourceType != null && this.LoanImminentDefaultSourceType.enumValue != LoanImminentDefaultSourceBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected as the Loan Imminent Default Source Type.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString LoanImminentDefaultSourceTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanImminentDefaultSourceTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanImminentDefaultSourceTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanImminentDefaultSourceTypeOtherDescriptionSpecified
        {
            get { return LoanImminentDefaultSourceTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates previously reported activity or information is being revised.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOIndicator PreviouslyReportedInformationRevisionIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PreviouslyReportedInformationRevisionIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PreviouslyReportedInformationRevisionIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PreviouslyReportedInformationRevisionIndicatorSpecified
        {
            get { return PreviouslyReportedInformationRevisionIndicator != null; }
            set { }
        }

        /// <summary>
        /// The date of the welcome call to the borrower for a servicing transfer was performed.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMODate ServicerWelcomeCallPerformedDate;

        /// <summary>
        /// Gets or sets a value indicating whether the ServicerWelcomeCallPerformedDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServicerWelcomeCallPerformedDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServicerWelcomeCallPerformedDateSpecified
        {
            get { return ServicerWelcomeCallPerformedDate != null; }
            set { }
        }

        /// <summary>
        /// Identifies the insuring section and the applicable HUD insurance fund; identifies an assistance payment contract; identifies where the lender has the option under its contract of mortgage ins to receive benefits in default. (Reference HUD SFDMS Guide).
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOIdentifier SFDMSAutomatedDefaultProcessingIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the SFDMSAutomatedDefaultProcessingIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SFDMSAutomatedDefaultProcessingIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool SFDMSAutomatedDefaultProcessingIdentifierSpecified
        {
            get { return SFDMSAutomatedDefaultProcessingIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Date the latest title report was received by the servicer.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMODate TitleReportLastReceivedDate;

        /// <summary>
        /// Gets or sets a value indicating whether the TitleReportLastReceivedDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TitleReportLastReceivedDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool TitleReportLastReceivedDateSpecified
        {
            get { return TitleReportLastReceivedDate != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 9)]
        public SERVICING_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
