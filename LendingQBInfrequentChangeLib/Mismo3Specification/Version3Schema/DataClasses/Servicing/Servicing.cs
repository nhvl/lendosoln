namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class SERVICING
    {
        /// <summary>
        /// Gets a value indicating whether the SERVICING container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AdvancesSpecified
                    || this.CreditBureauReportingSpecified
                    || this.DelinquenciesSpecified
                    || this.DisclosureOnServicerSpecified
                    || this.ExtensionSpecified
                    || this.MonetaryEventsSpecified
                    || this.MonetaryEventSummariesSpecified
                    || this.ServicerQualifiedWrittenRequestMailToSpecified
                    || this.ServicingCommentsSpecified
                    || this.ServicingDetailSpecified
                    || this.StopCodesSpecified;
            }
        }

        /// <summary>
        /// Advances related to the service.
        /// </summary>
        [XmlElement("ADVANCES", Order = 0)]
        public ADVANCES Advances;

        /// <summary>
        /// Gets or sets a value indicating whether the Advances element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Advances element has been assigned a value.</value>
        [XmlIgnore]
        public bool AdvancesSpecified
        {
            get { return this.Advances != null && this.Advances.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Credit bureau reporting related to the service.
        /// </summary>
        [XmlElement("CREDIT_BUREAU_REPORTING", Order = 1)]
        public CREDIT_BUREAU_REPORTING CreditBureauReporting;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditBureauReporting element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditBureauReporting element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditBureauReportingSpecified
        {
            get { return this.CreditBureauReporting != null && this.CreditBureauReporting.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Delinquencies related to the service.
        /// </summary>
        [XmlElement("DELINQUENCIES", Order = 2)]
        public DELINQUENCIES Delinquencies;

        /// <summary>
        /// Gets or sets a value indicating whether the Delinquencies element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Delinquencies element has been assigned a value.</value>
        [XmlIgnore]
        public bool DelinquenciesSpecified
        {
            get { return this.Delinquencies != null && this.Delinquencies.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Disclosure on the servicing entity.
        /// </summary>
        [XmlElement("DISCLOSURE_ON_SERVICER", Order = 3)]
        public DISCLOSURE_ON_SERVICER DisclosureOnServicer;

        /// <summary>
        /// Gets or sets a value indicating whether the DisclosureOnServicer element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DisclosureOnServicer element has been assigned a value.</value>
        [XmlIgnore]
        public bool DisclosureOnServicerSpecified
        {
            get { return this.DisclosureOnServicer != null && this.DisclosureOnServicer.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Summaries of monetary events.
        /// </summary>
        [XmlElement("MONETARY_EVENT_SUMMARIES", Order = 4)]
        public MONETARY_EVENT_SUMMARIES MonetaryEventSummaries;

        /// <summary>
        /// Gets or sets a value indicating whether the MonetaryEventSummaries element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MonetaryEventSummaries element has been assigned a value.</value>
        [XmlIgnore]
        public bool MonetaryEventSummariesSpecified
        {
            get { return this.MonetaryEventSummaries != null && this.MonetaryEventSummaries.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Monetary events during servicing.
        /// </summary>
        [XmlElement("MONETARY_EVENTS", Order = 5)]
        public MONETARY_EVENTS MonetaryEvents;

        /// <summary>
        /// Gets or sets a value indicating whether the MonetaryEvents element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MonetaryEvents element has been assigned a value.</value>
        [XmlIgnore]
        public bool MonetaryEventsSpecified
        {
            get { return this.MonetaryEvents != null && this.MonetaryEvents.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Mailing address for service requests.
        /// </summary>
        [XmlElement("SERVICER_QUALIFIED_WRITTEN_REQUEST_MAIL_TO", Order = 6)]
        public SERVICER_QUALIFIED_WRITTEN_REQUEST_MAIL_TO ServicerQualifiedWrittenRequestMailTo;

        /// <summary>
        /// Gets or sets a value indicating whether the ServicerQualifiedWrittenRequestMailTo element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServicerQualifiedWrittenRequestMailTo element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServicerQualifiedWrittenRequestMailToSpecified
        {
            get { return this.ServicerQualifiedWrittenRequestMailTo != null && this.ServicerQualifiedWrittenRequestMailTo.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Comments about the servicing.
        /// </summary>
        [XmlElement("SERVICING_COMMENTS", Order = 7)]
        public SERVICING_COMMENTS ServicingComments;

        /// <summary>
        /// Gets or sets a value indicating whether the ServicingComments element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServicingComments element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServicingCommentsSpecified
        {
            get { return this.ServicingComments != null && this.ServicingComments.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details about the servicing.
        /// </summary>
        [XmlElement("SERVICING_DETAIL", Order = 8)]
        public SERVICING_DETAIL ServicingDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the ServicingDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServicingDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServicingDetailSpecified
        {
            get { return this.ServicingDetail != null && this.ServicingDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Stop codes for the servicing.
        /// </summary>
        [XmlElement("STOP_CODES", Order = 9)]
        public STOP_CODES StopCodes;

        /// <summary>
        /// Gets or sets a value indicating whether the StopCodes element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the StopCodes element has been assigned a value.</value>
        [XmlIgnore]
        public bool StopCodesSpecified
        {
            get { return this.StopCodes != null && this.StopCodes.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 10)]
        public SERVICING_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
