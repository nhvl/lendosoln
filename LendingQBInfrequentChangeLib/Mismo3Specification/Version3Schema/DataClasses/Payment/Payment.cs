namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PAYMENT
    {
        /// <summary>
        /// Gets a value indicating whether the PAYMENT container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ArrearageSpecified
                    || this.ExtensionSpecified
                    || this.PartialPaymentsSpecified
                    || this.PaymentComponentBreakoutsSpecified
                    || this.PaymentRuleSpecified
                    || this.PaymentScheduleItemsSpecified
                    || this.PaymentSummarySpecified
                    || this.PeriodicLateCountsSpecified
                    || this.SkipPaymentSpecified;
            }
        }

        /// <summary>
        /// Any arrearage associated with the payment.
        /// </summary>
        [XmlElement("ARREARAGE", Order = 0)]
        public ARREARAGE Arrearage;

        /// <summary>
        /// Gets or sets a value indicating whether the Arrearage element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Arrearage element has been assigned a value.</value>
        [XmlIgnore]
        public bool ArrearageSpecified
        {
            get { return this.Arrearage != null && this.Arrearage.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// List of partial payments.
        /// </summary>
        [XmlElement("PARTIAL_PAYMENTS", Order = 1)]
        public PARTIAL_PAYMENTS PartialPayments;

        /// <summary>
        /// Gets or sets a value indicating whether the PartialPayments element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PartialPayments element has been assigned a value.</value>
        [XmlIgnore]
        public bool PartialPaymentsSpecified
        {
            get { return this.PartialPayments != null && this.PartialPayments.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// List of payment components.
        /// </summary>
        [XmlElement("PAYMENT_COMPONENT_BREAKOUTS", Order = 2)]
        public PAYMENT_COMPONENT_BREAKOUTS PaymentComponentBreakouts;

        /// <summary>
        /// Gets or sets a value indicating whether the PaymentComponentBreakouts element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PaymentComponentBreakouts element has been assigned a value.</value>
        [XmlIgnore]
        public bool PaymentComponentBreakoutsSpecified
        {
            get { return this.PaymentComponentBreakouts != null && this.PaymentComponentBreakouts.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Rules about payment.
        /// </summary>
        [XmlElement("PAYMENT_RULE", Order = 3)]
        public PAYMENT_RULE PaymentRule;

        /// <summary>
        /// Gets or sets a value indicating whether the PaymentRule element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PaymentRule element has been assigned a value.</value>
        [XmlIgnore]
        public bool PaymentRuleSpecified
        {
            get { return this.PaymentRule != null && this.PaymentRule.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Payment schedule item list.
        /// </summary>
        [XmlElement("PAYMENT_SCHEDULE_ITEMS", Order = 4)]
        public PAYMENT_SCHEDULE_ITEMS PaymentScheduleItems;

        /// <summary>
        /// Gets or sets a value indicating whether the PaymentScheduleItems element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PaymentScheduleItems element has been assigned a value.</value>
        [XmlIgnore]
        public bool PaymentScheduleItemsSpecified
        {
            get { return this.PaymentScheduleItems != null && this.PaymentScheduleItems.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Summary of payment.
        /// </summary>
        [XmlElement("PAYMENT_SUMMARY", Order = 5)]
        public PAYMENT_SUMMARY PaymentSummary;

        /// <summary>
        /// Gets or sets a value indicating whether the PaymentSummary element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PaymentSummary element has been assigned a value.</value>
        [XmlIgnore]
        public bool PaymentSummarySpecified
        {
            get { return this.PaymentSummary != null && this.PaymentSummary.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Late payment counts.
        /// </summary>
        [XmlElement("PERIODIC_LATE_COUNTS", Order = 6)]
        public PERIODIC_LATE_COUNTS PeriodicLateCounts;

        /// <summary>
        /// Gets or sets a value indicating whether the PeriodicLateCounts element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PeriodicLateCounts element has been assigned a value.</value>
        [XmlIgnore]
        public bool PeriodicLateCountsSpecified
        {
            get { return this.PeriodicLateCounts != null && this.PeriodicLateCounts.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Information on payment skip.
        /// </summary>
        [XmlElement("SKIP_PAYMENT", Order = 7)]
        public SKIP_PAYMENT SkipPayment;

        /// <summary>
        /// Gets or sets a value indicating whether the SkipPayment element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SkipPayment element has been assigned a value.</value>
        [XmlIgnore]
        public bool SkipPaymentSpecified
        {
            get { return this.SkipPayment != null && this.SkipPayment.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 8)]
        public PAYMENT_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
