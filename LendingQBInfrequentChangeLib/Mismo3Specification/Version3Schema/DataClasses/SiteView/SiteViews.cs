namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class SITE_VIEWS
    {
        /// <summary>
        /// Gets a value indicating whether the SITE_VIEWS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.SiteViewSpecified;
            }
        }

        /// <summary>
        /// A collection of site views.
        /// </summary>
        [XmlElement("SITE_VIEW", Order = 0)]
		public List<SITE_VIEW> SiteView = new List<SITE_VIEW>();

        /// <summary>
        /// Gets or sets a value indicating whether the SiteView element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SiteView element has been assigned a value.</value>
        [XmlIgnore]
        public bool SiteViewSpecified
        {
            get { return this.SiteView != null && this.SiteView.Count(s => s != null && s.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public SITE_VIEWS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
