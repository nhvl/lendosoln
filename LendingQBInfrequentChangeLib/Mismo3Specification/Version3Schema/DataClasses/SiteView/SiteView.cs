namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class SITE_VIEW
    {
        /// <summary>
        /// Gets a value indicating whether the SITE_VIEW container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.InfluenceImpactTypeSpecified
                    || this.SiteViewDescriptionSpecified
                    || this.SiteViewTypeOtherDescriptionSpecified
                    || this.SiteViewTypeSpecified;
            }
        }

        /// <summary>
        /// Identifies the impact of the indicated Influence on the subject property as used in the appraiser's analysis.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<InfluenceImpactBase> InfluenceImpactType;

        /// <summary>
        /// Gets or sets a value indicating whether the InfluenceImpactType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InfluenceImpactType element has been assigned a value.</value>
        [XmlIgnore]
        public bool InfluenceImpactTypeSpecified
        {
            get { return this.InfluenceImpactType != null && this.InfluenceImpactType.enumValue != InfluenceImpactBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the views from the site.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString SiteViewDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the SiteViewDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SiteViewDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool SiteViewDescriptionSpecified
        {
            get { return SiteViewDescription != null; }
            set { }
        }

        /// <summary>
        /// Identifies specific view types as observed for valuation purposes by the appraiser that may affect desirability and market value.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<SiteViewBase> SiteViewType;

        /// <summary>
        /// Gets or sets a value indicating whether the SiteViewType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SiteViewType element has been assigned a value.</value>
        [XmlIgnore]
        public bool SiteViewTypeSpecified
        {
            get { return this.SiteViewType != null && this.SiteViewType.enumValue != SiteViewBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Site View Type.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString SiteViewTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the SiteViewTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SiteViewTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool SiteViewTypeOtherDescriptionSpecified
        {
            get { return SiteViewTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public SITE_VIEW_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
