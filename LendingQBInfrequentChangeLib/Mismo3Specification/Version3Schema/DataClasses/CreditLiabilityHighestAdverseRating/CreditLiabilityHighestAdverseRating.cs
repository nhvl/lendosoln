namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CREDIT_LIABILITY_HIGHEST_ADVERSE_RATING
    {
        /// <summary>
        /// Gets a value indicating whether the CREDIT_LIABILITY_HIGHEST_ADVERSE_RATING container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditLiabilityHighestAdverseRatingAmountSpecified
                    || this.CreditLiabilityHighestAdverseRatingCodeSpecified
                    || this.CreditLiabilityHighestAdverseRatingDateSpecified
                    || this.CreditLiabilityHighestAdverseRatingTypeSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// The highest delinquent amount on the account as reported by the liability holder.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount CreditLiabilityHighestAdverseRatingAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLiabilityHighestAdverseRatingAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLiabilityHighestAdverseRatingAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLiabilityHighestAdverseRatingAmountSpecified
        {
            get { return CreditLiabilityHighestAdverseRatingAmount != null; }
            set { }
        }

        /// <summary>
        /// The liability accounts highest (or worst) rating code for how timely the borrower has been making payments on this account. This is also know as the Manner Of Payment or MOP code. See the MISMO Implementation Guide: Credit Reporting for more information on MOP codes.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOCode CreditLiabilityHighestAdverseRatingCode;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLiabilityHighestAdverseRatingCode element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLiabilityHighestAdverseRatingCode element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLiabilityHighestAdverseRatingCodeSpecified
        {
            get { return CreditLiabilityHighestAdverseRatingCode != null; }
            set { }
        }

        /// <summary>
        /// The date of the highest (worst) delinquency reported by the liability holder.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMODate CreditLiabilityHighestAdverseRatingDate;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLiabilityHighestAdverseRatingDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLiabilityHighestAdverseRatingDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLiabilityHighestAdverseRatingDateSpecified
        {
            get { return CreditLiabilityHighestAdverseRatingDate != null; }
            set { }
        }

        /// <summary>
        /// The liability accounts highest (or worst) rating type for how timely the borrower has been making payments on this account.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOEnum<CreditLiabilityHighestAdverseRatingBase> CreditLiabilityHighestAdverseRatingType;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLiabilityHighestAdverseRatingType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLiabilityHighestAdverseRatingType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLiabilityHighestAdverseRatingTypeSpecified
        {
            get { return this.CreditLiabilityHighestAdverseRatingType != null && this.CreditLiabilityHighestAdverseRatingType.enumValue != CreditLiabilityHighestAdverseRatingBase.Blank; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public CREDIT_LIABILITY_HIGHEST_ADVERSE_RATING_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
