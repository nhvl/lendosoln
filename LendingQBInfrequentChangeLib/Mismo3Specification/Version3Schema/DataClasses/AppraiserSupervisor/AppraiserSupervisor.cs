namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class APPRAISER_SUPERVISOR
    {
        /// <summary>
        /// Gets a value indicating whether the APPRAISER_SUPERVISOR container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AppraiserSupervisorDetailSpecified
                    || this.DesignationsSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// Details about the appraiser supervisor.
        /// </summary>
        [XmlElement("APPRAISER_SUPERVISOR_DETAIL", Order = 0)]
        public APPRAISER_SUPERVISOR_DETAIL AppraiserSupervisorDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the AppraiserSupervisorDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AppraiserSupervisorDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool AppraiserSupervisorDetailSpecified
        {
            get { return this.AppraiserSupervisorDetail != null && this.AppraiserSupervisorDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Designations of the appraiser supervisor.
        /// </summary>
        [XmlElement("DESIGNATIONS", Order = 1)]
        public DESIGNATIONS Designations;

        /// <summary>
        /// Gets or sets a value indicating whether the Designations element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Designations element has been assigned a value.</value>
        [XmlIgnore]
        public bool DesignationsSpecified
        {
            get { return this.Designations != null && this.Designations.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public APPRAISER_SUPERVISOR_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
