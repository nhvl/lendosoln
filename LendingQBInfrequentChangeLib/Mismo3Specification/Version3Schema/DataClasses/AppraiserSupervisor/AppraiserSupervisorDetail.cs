namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class APPRAISER_SUPERVISOR_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the APPRAISER_SUPERVISOR_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AppraiserSupervisorCompanyNameSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// Name of the company with which the supervisor is associated.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString AppraiserSupervisorCompanyName;

        /// <summary>
        /// Gets or sets a value indicating whether the AppraiserSupervisorCompanyName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AppraiserSupervisorCompanyName element has been assigned a value.</value>
        [XmlIgnore]
        public bool AppraiserSupervisorCompanyNameSpecified
        {
            get { return AppraiserSupervisorCompanyName != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public APPRAISER_SUPERVISOR_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
