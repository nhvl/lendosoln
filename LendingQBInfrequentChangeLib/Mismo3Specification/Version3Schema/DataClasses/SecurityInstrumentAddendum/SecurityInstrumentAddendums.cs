namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class SECURITY_INSTRUMENT_ADDENDUMS
    {
        /// <summary>
        /// Gets a value indicating whether the SECURITY_INSTRUMENT_ADDENDUMS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.SecurityInstrumentAddendumSpecified;
            }
        }

        /// <summary>
        /// A collection of security instrument addendums.
        /// </summary>
        [XmlElement("SECURITY_INSTRUMENT_ADDENDUM", Order = 0)]
		public List<SECURITY_INSTRUMENT_ADDENDUM> SecurityInstrumentAddendum = new List<SECURITY_INSTRUMENT_ADDENDUM>();

        /// <summary>
        /// Gets or sets a value indicating whether the SecurityInstrumentAddendum element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SecurityInstrumentAddendum element has been assigned a value.</value>
        [XmlIgnore]
        public bool SecurityInstrumentAddendumSpecified
        {
            get { return this.SecurityInstrumentAddendum != null && this.SecurityInstrumentAddendum.Count(s => s != null && s.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public SECURITY_INSTRUMENT_ADDENDUMS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
