namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class SECURITY_INSTRUMENT_ADDENDUM
    {
        /// <summary>
        /// Gets a value indicating whether the SECURITY_INSTRUMENT_ADDENDUM container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.SecurityInstrumentAddendumTypeOtherDescriptionSpecified
                    || this.SecurityInstrumentAddendumTypeSpecified;
            }
        }

        /// <summary>
        /// Specifies the type of addendum referenced on the Security Instrument.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<SecurityInstrumentAddendumBase> SecurityInstrumentAddendumType;

        /// <summary>
        /// Gets or sets a value indicating whether the SecurityInstrumentAddendumType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SecurityInstrumentAddendumType element has been assigned a value.</value>
        [XmlIgnore]
        public bool SecurityInstrumentAddendumTypeSpecified
        {
            get { return this.SecurityInstrumentAddendumType != null && this.SecurityInstrumentAddendumType.enumValue != SecurityInstrumentAddendumBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Security Instrument Addendum Type.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString SecurityInstrumentAddendumTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the SecurityInstrumentAddendumTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SecurityInstrumentAddendumTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool SecurityInstrumentAddendumTypeOtherDescriptionSpecified
        {
            get { return SecurityInstrumentAddendumTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public SECURITY_INSTRUMENT_ADDENDUM_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
