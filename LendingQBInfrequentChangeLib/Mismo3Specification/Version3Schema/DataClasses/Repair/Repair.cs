namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class REPAIR
    {
        /// <summary>
        /// Gets a value indicating whether the REPAIR container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.RepairItemsSpecified
                    || this.RepairSummarySpecified;
            }
        }

        /// <summary>
        /// The items to be repaired.
        /// </summary>
        [XmlElement("REPAIR_ITEMS", Order = 0)]
        public REPAIR_ITEMS RepairItems;

        /// <summary>
        /// Gets or sets a value indicating whether the RepairItems element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RepairItems element has been assigned a value.</value>
        [XmlIgnore]
        public bool RepairItemsSpecified
        {
            get { return this.RepairItems != null && this.RepairItems.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Summary of items to be repaired.
        /// </summary>
        [XmlElement("REPAIR_SUMMARY", Order = 1)]
        public REPAIR_SUMMARY RepairSummary;

        /// <summary>
        /// Gets or sets a value indicating whether the RepairSummary element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RepairSummary element has been assigned a value.</value>
        [XmlIgnore]
        public bool RepairSummarySpecified
        {
            get { return this.RepairSummary != null && this.RepairSummary.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public REPAIR_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
