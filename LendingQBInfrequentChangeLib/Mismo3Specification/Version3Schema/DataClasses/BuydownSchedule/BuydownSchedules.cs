namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class BUYDOWN_SCHEDULES
    {
        /// <summary>
        /// Gets a value indicating whether the BUYDOWN_SCHEDULES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BuydownScheduleSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of buy down schedules.
        /// </summary>
        [XmlElement("BUYDOWN_SCHEDULE", Order = 0)]
		public List<BUYDOWN_SCHEDULE> BuydownSchedule = new List<BUYDOWN_SCHEDULE>();

        /// <summary>
        /// Gets or sets a value indicating whether the BuyDownSchedule element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BuyDownSchedule element has been assigned a value.</value>
        [XmlIgnore]
        public bool BuydownScheduleSpecified
        {
            get { return this.BuydownSchedule != null && this.BuydownSchedule.Count(b => b != null && b.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public BUYDOWN_SCHEDULES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
