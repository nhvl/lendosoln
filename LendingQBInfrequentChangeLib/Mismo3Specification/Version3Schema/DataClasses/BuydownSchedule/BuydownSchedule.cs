namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class BUYDOWN_SCHEDULE
    {
        /// <summary>
        /// Gets a value indicating whether the BUYDOWN_SCHEDULE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BuydownScheduleAdjustmentPercentSpecified
                    || this.BuydownSchedulePeriodicPaymentAmountSpecified
                    || this.BuydownSchedulePeriodicPaymentEffectiveDateSpecified
                    || this.BuydownSchedulePeriodicPaymentsCountSpecified
                    || this.BuydownSchedulePeriodIdentifierSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// The effective reduction against the initial note rate while this buy down payment is in effective.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOPercent BuydownScheduleAdjustmentPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the BuyDownScheduleAdjustmentPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BuyDownScheduleAdjustmentPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool BuydownScheduleAdjustmentPercentSpecified
        {
            get { return BuydownScheduleAdjustmentPercent != null; }
            set { }
        }

        /// <summary>
        /// The amount of buy down for each payment per the buy down schedule.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOAmount BuydownSchedulePeriodicPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the BuyDownSchedulePeriodicPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BuyDownSchedulePeriodicPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool BuydownSchedulePeriodicPaymentAmountSpecified
        {
            get { return BuydownSchedulePeriodicPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// The first date the specific buy down payment becomes effective.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMODate BuydownSchedulePeriodicPaymentEffectiveDate;

        /// <summary>
        /// Gets or sets a value indicating whether the BuyDownSchedulePeriodicPaymentEffectiveDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BuyDownSchedulePeriodicPaymentEffectiveDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool BuydownSchedulePeriodicPaymentEffectiveDateSpecified
        {
            get { return BuydownSchedulePeriodicPaymentEffectiveDate != null; }
            set { }
        }

        /// <summary>
        /// Number of payments in the period. Referred to at time of loan application as the period to next adjustment when establishing the terms of the buy down agreement.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOCount BuydownSchedulePeriodicPaymentsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the BuyDownSchedulePeriodicPaymentsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BuyDownSchedulePeriodicPaymentsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool BuydownSchedulePeriodicPaymentsCountSpecified
        {
            get { return BuydownSchedulePeriodicPaymentsCount != null; }
            set { }
        }

        /// <summary>
        /// A unique sequence number for the entries in the buy down schedule. Used to establish correct sequence of the payment elements when an effective date has not yet been established.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOIdentifier BuydownSchedulePeriodIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the BuyDownSchedulePeriodIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BuyDownSchedulePeriodIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool BuydownSchedulePeriodIdentifierSpecified
        {
            get { return BuydownSchedulePeriodIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 5)]
        public BUYDOWN_SCHEDULE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
