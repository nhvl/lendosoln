namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class POOL_CERTIFICATE
    {
        /// <summary>
        /// Gets a value indicating whether the POOL_CERTIFICATE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.PoolCertificateCurrentAmountSpecified
                    || this.PoolCertificateHolderPayeeIdentifierSpecified
                    || this.PoolCertificateIdentifierSpecified
                    || this.PoolCertificateInitialPaymentDateSpecified
                    || this.PoolCertificateIssueDateSpecified
                    || this.PoolCertificateMaturityDateSpecified
                    || this.PoolCertificateOriginalAmountSpecified
                    || this.PoolCertificatePrincipalPaidAmountSpecified;
            }
        }

        /// <summary>
        /// The current dollar amount of the certificate.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount PoolCertificateCurrentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PoolCertificateCurrentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PoolCertificateCurrentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PoolCertificateCurrentAmountSpecified
        {
            get { return PoolCertificateCurrentAmount != null; }
            set { }
        }

        /// <summary>
        /// The Payee Identifier which identifies the certificate holder.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOIdentifier PoolCertificateHolderPayeeIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the PoolCertificateHolderPayeeIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PoolCertificateHolderPayeeIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool PoolCertificateHolderPayeeIdentifierSpecified
        {
            get { return PoolCertificateHolderPayeeIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The certificate identifier assigned to the pool security.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOIdentifier PoolCertificateIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the PoolCertificateIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PoolCertificateIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool PoolCertificateIdentifierSpecified
        {
            get { return PoolCertificateIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The date that the first payment is due to the security holder for the certificate.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMODate PoolCertificateInitialPaymentDate;

        /// <summary>
        /// Gets or sets a value indicating whether the PoolCertificateInitialPaymentDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PoolCertificateInitialPaymentDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool PoolCertificateInitialPaymentDateSpecified
        {
            get { return PoolCertificateInitialPaymentDate != null; }
            set { }
        }

        /// <summary>
        /// The date the certificate was issued.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMODate PoolCertificateIssueDate;

        /// <summary>
        /// Gets or sets a value indicating whether the PoolCertificateIssueDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PoolCertificateIssueDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool PoolCertificateIssueDateSpecified
        {
            get { return PoolCertificateIssueDate != null; }
            set { }
        }

        /// <summary>
        /// The date that the final payment is due to the security holder for the certificate.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMODate PoolCertificateMaturityDate;

        /// <summary>
        /// Gets or sets a value indicating whether the PoolCertificateMaturityDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PoolCertificateMaturityDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool PoolCertificateMaturityDateSpecified
        {
            get { return PoolCertificateMaturityDate != null; }
            set { }
        }

        /// <summary>
        /// The original dollar amount of the certificate.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOAmount PoolCertificateOriginalAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PoolCertificateOriginalAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PoolCertificateOriginalAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PoolCertificateOriginalAmountSpecified
        {
            get { return PoolCertificateOriginalAmount != null; }
            set { }
        }

        /// <summary>
        /// The amount of principal paid to the security holder for the last accounting period.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOAmount PoolCertificatePrincipalPaidAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PoolCertificatePrincipalPaidAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PoolCertificatePrincipalPaidAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PoolCertificatePrincipalPaidAmountSpecified
        {
            get { return PoolCertificatePrincipalPaidAmount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 8)]
        public POOL_CERTIFICATE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
