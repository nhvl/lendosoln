namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CAPITALIZATION_SUMMARY
    {
        /// <summary>
        /// Gets a value indicating whether the CAPITALIZATION_SUMMARY container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ContributionToReduceCapitalizedAdvancesAmountSpecified
                    || this.ContributionToReduceCapitalizedDelinquentInterestAmountSpecified
                    || this.ExtensionSpecified
                    || this.TotalCapitalizedAmountSpecified
                    || this.TotalCapitalizedFeesAmountSpecified;
            }
        }

        /// <summary>
        /// The dollar amount reported by the servicer that is used to offset some or all of the advances for capitalization contributed by the borrower, MI or hazard insurance.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount ContributionToReduceCapitalizedAdvancesAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ContributionToReduceCapitalizedAdvancesAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ContributionToReduceCapitalizedAdvancesAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ContributionToReduceCapitalizedAdvancesAmountSpecified
        {
            get { return ContributionToReduceCapitalizedAdvancesAmount != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount reported by the servicer that is used to offset some or all of  the delinquent interest for capitalization contributed by the borrower, MI or hazard insurance.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOAmount ContributionToReduceCapitalizedDelinquentInterestAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ContributionToReduceCapitalizedDelinquentInterestAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ContributionToReduceCapitalizedDelinquentInterestAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ContributionToReduceCapitalizedDelinquentInterestAmountSpecified
        {
            get { return ContributionToReduceCapitalizedDelinquentInterestAmount != null; }
            set { }
        }

        /// <summary>
        /// The total amount of arrearage capitalized to loan balance.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOAmount TotalCapitalizedAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the TotalCapitalizedAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TotalCapitalizedAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TotalCapitalizedAmountSpecified
        {
            get { return TotalCapitalizedAmount != null; }
            set { }
        }

        /// <summary>
        /// The total dollar amount of all fees and charges that are capitalized into the principal balance.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOAmount TotalCapitalizedFeesAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the TotalCapitalizedFeesAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TotalCapitalizedFeesAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TotalCapitalizedFeesAmountSpecified
        {
            get { return TotalCapitalizedFeesAmount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public CAPITALIZATION_SUMMARY_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
