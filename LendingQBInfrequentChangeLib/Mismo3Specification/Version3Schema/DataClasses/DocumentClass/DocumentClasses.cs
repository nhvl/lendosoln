namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class DOCUMENT_CLASSES
    {
        /// <summary>
        /// Gets a value indicating whether the DOCUMENT_CLASSES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DocumentClassSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of document classes.
        /// </summary>
        [XmlElement("DOCUMENT_CLASS", Order = 0)]
		public List<DOCUMENT_CLASS> DocumentClass = new List<DOCUMENT_CLASS>();

        /// <summary>
        /// Gets or sets a value indicating whether the DocumentClass element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DocumentClass element has been assigned a value.</value>
        [XmlIgnore]
        public bool DocumentClassSpecified
        {
            get { return this.DocumentClass != null && this.DocumentClass.Count(d => d != null && d.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public DOCUMENT_CLASSES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
