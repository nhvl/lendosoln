namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class DOCUMENT_CLASS
    {
        /// <summary>
        /// Gets a value indicating whether the DOCUMENT_CLASS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DocumentTypeOtherDescriptionSpecified
                    || this.DocumentTypeSpecified
                    || this.ExtensionSpecified
                    || this.RecordingJurisdictionDocumentCodeSpecified;
            }
        }

        /// <summary>
        /// Identifies a specific document defined at a granular level. May be modified or further constrained by other Document data to make the document instance more specific. Further defined in the MISMO Document Classification I-Guide.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<DocumentBase> DocumentType;

        /// <summary>
        /// Gets or sets a value indicating whether the DocumentType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DocumentType element has been assigned a value.</value>
        [XmlIgnore]
        public bool DocumentTypeSpecified
        {
            get { return this.DocumentType != null && this.DocumentType.enumValue != DocumentBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Document Type.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString DocumentTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the DocumentTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DocumentTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool DocumentTypeOtherDescriptionSpecified
        {
            get { return DocumentTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Code used by recorder in indexing software. Specific to county where document is recorded.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOCode RecordingJurisdictionDocumentCode;

        /// <summary>
        /// Gets or sets a value indicating whether the RecordingJurisdictionDocumentCode element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RecordingJurisdictionDocumentCode element has been assigned a value.</value>
        [XmlIgnore]
        public bool RecordingJurisdictionDocumentCodeSpecified
        {
            get { return RecordingJurisdictionDocumentCode != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public DOCUMENT_CLASS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
