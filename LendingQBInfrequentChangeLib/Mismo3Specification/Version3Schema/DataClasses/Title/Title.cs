namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class TITLE
    {
        /// <summary>
        /// Gets a value indicating whether the TITLE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.TitleRequestSpecified
                    || this.TitleResponseSpecified;
            }
        }

        /// <summary>
        /// A request for title.
        /// </summary>
        [XmlElement("TITLE_REQUEST", Order = 0)]
        public TITLE_REQUEST TitleRequest;

        /// <summary>
        /// Gets or sets a value indicating whether the TitleRequest element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TitleRequest element has been assigned a value.</value>
        [XmlIgnore]
        public bool TitleRequestSpecified
        {
            get { return this.TitleRequest != null && this.TitleRequest.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A response to a title request.
        /// </summary>
        [XmlElement("TITLE_RESPONSE", Order = 1)]
        public TITLE_RESPONSE TitleResponse;

        /// <summary>
        /// Gets or sets a value indicating whether the TitleResponse element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TitleResponse element has been assigned a value.</value>
        [XmlIgnore]
        public bool TitleResponseSpecified
        {
            get { return this.TitleResponse != null && this.TitleResponse.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public TITLE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
