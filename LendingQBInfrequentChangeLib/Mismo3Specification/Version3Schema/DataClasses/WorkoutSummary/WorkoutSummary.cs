namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class WORKOUT_SUMMARY
    {
        /// <summary>
        /// Gets a value indicating whether the WORKOUT_SUMMARY container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.LifeOfLoanBorrowerAssistancePaidAmountSpecified
                    || this.LifeOfLoanWorkoutModificationCountSpecified
                    || this.LifeOfLoanWorkoutModificationDefaultedCountSpecified;
            }
        }

        /// <summary>
        /// The total dollar amount of all borrower assistance paid to the servicer on behalf of the borrower associated with any workouts over the life of the loan. Repayment of funds is not expected.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount LifeOfLoanBorrowerAssistancePaidAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the LifeOfLoanBorrowerAssistancePaidAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LifeOfLoanBorrowerAssistancePaidAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool LifeOfLoanBorrowerAssistancePaidAmountSpecified
        {
            get { return LifeOfLoanBorrowerAssistancePaidAmount != null; }
            set { }
        }

        /// <summary>
        /// A count of the number of prior completed modifications associated with workouts over the life of the loan.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOCount LifeOfLoanWorkoutModificationCount;

        /// <summary>
        /// Gets or sets a value indicating whether the LifeOfLoanWorkoutModificationCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LifeOfLoanWorkoutModificationCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool LifeOfLoanWorkoutModificationCountSpecified
        {
            get { return LifeOfLoanWorkoutModificationCount != null; }
            set { }
        }

        /// <summary>
        /// A count of the number of prior completed modifications where the loan subsequently missed two or more payments.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOCount LifeOfLoanWorkoutModificationDefaultedCount;

        /// <summary>
        /// Gets or sets a value indicating whether the LifeOfLoanWorkoutModificationDefaultedCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LifeOfLoanWorkoutModificationDefaultedCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool LifeOfLoanWorkoutModificationDefaultedCountSpecified
        {
            get { return LifeOfLoanWorkoutModificationDefaultedCount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public WORKOUT_SUMMARY_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
