namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class LOAN_LEVEL_CREDIT
    {
        /// <summary>
        /// Gets a value indicating whether the LOAN_LEVEL_CREDIT container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditScoreProviderSpecified
                    || this.ExtensionSpecified
                    || this.LoanLevelCreditDetailSpecified;
            }
        }

        /// <summary>
        /// The provider of a credit score.
        /// </summary>
        [XmlElement("CREDIT_SCORE_PROVIDER", Order = 0)]
        public CREDIT_SCORE_PROVIDER CreditScoreProvider;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditScoreProvider element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditScoreProvider element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditScoreProviderSpecified
        {
            get { return this.CreditScoreProvider != null && this.CreditScoreProvider.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details on a loan level credit.
        /// </summary>
        [XmlElement("LOAN_LEVEL_CREDIT_DETAIL", Order = 1)]
        public LOAN_LEVEL_CREDIT_DETAIL LoanLevelCreditDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanLevelCreditDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanLevelCreditDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanLevelCreditDetailSpecified
        {
            get { return this.LoanLevelCreditDetail != null && this.LoanLevelCreditDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public LOAN_LEVEL_CREDIT_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
