namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class LOAN_LEVEL_CREDIT_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the LOAN_LEVEL_CREDIT_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditReferenceTypeOtherDescriptionSpecified
                    || this.CreditReferenceTypeSpecified
                    || this.CreditScoreImpairmentTypeOtherDescriptionSpecified
                    || this.CreditScoreImpairmentTypeSpecified
                    || this.CreditScoreModelNameTypeOtherDescriptionSpecified
                    || this.CreditScoreModelNameTypeSpecified
                    || this.ExtensionSpecified
                    || this.LoanCreditHistoryAgeTypeOtherDescriptionSpecified
                    || this.LoanCreditHistoryAgeTypeSpecified
                    || this.LoanLevelCreditScoreValueSpecified
                    || this.LoanLevelCreditScoreSelectionMethodTypeOtherDescriptionSpecified
                    || this.LoanLevelCreditScoreSelectionMethodTypeSpecified
                    || this.RiskUpgradeIndicatorSpecified;
            }
        }

        /// <summary>
        /// Specifies the type of credit history or type of payments used as credit history to establish acceptable credit reputation.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<CreditReferenceBase> CreditReferenceType;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditReferenceType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditReferenceType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditReferenceTypeSpecified
        {
            get { return this.CreditReferenceType != null && this.CreditReferenceType.enumValue != CreditReferenceBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture the Credit Reference Type if Other is selected.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString CreditReferenceTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditReferenceTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditReferenceTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditReferenceTypeOtherDescriptionSpecified
        {
            get { return CreditReferenceTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Identifies a characteristic of the Credit Score that impairs its effectiveness as an indicator of credit risk.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<CreditScoreImpairmentBase> CreditScoreImpairmentType;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditScoreImpairmentType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditScoreImpairmentType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditScoreImpairmentTypeSpecified
        {
            get { return this.CreditScoreImpairmentType != null && this.CreditScoreImpairmentType.enumValue != CreditScoreImpairmentBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Credit Score Impairment Type.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString CreditScoreImpairmentTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditScoreImpairmentTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditScoreImpairmentTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditScoreImpairmentTypeOtherDescriptionSpecified
        {
            get { return CreditScoreImpairmentTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Identifies the score algorithm model name used to produce the referenced credit risk score.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<CreditScoreModelNameBase> CreditScoreModelNameType;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditScoreModelNameType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditScoreModelNameType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditScoreModelNameTypeSpecified
        {
            get { return this.CreditScoreModelNameType != null && this.CreditScoreModelNameType.enumValue != CreditScoreModelNameBase.Blank; }
            set { }
        }

        /// <summary>
        /// When the Credit Score Model Name Type is set to Other, this element holds the description.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString CreditScoreModelNameTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditScoreModelNameTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditScoreModelNameTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditScoreModelNameTypeOtherDescriptionSpecified
        {
            get { return CreditScoreModelNameTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Describes the period of time prior to the note date within which the credit history was obtained.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOEnum<LoanCreditHistoryAgeBase> LoanCreditHistoryAgeType;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanCreditHistoryAgeType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanCreditHistoryAgeType element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanCreditHistoryAgeTypeSpecified
        {
            get { return this.LoanCreditHistoryAgeType != null && this.LoanCreditHistoryAgeType.enumValue != LoanCreditHistoryAgeBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for the Loan Credit History Age Type.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOString LoanCreditHistoryAgeTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanCreditHistoryAgeTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanCreditHistoryAgeTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanCreditHistoryAgeTypeOtherDescriptionSpecified
        {
            get { return LoanCreditHistoryAgeTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the method used to select the Loan Credit Score across all borrowers.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOEnum<LoanLevelCreditScoreSelectionMethodBase> LoanLevelCreditScoreSelectionMethodType;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanLevelCreditScoreSelectionMethodType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanLevelCreditScoreSelectionMethodType element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanLevelCreditScoreSelectionMethodTypeSpecified
        {
            get { return this.LoanLevelCreditScoreSelectionMethodType != null && this.LoanLevelCreditScoreSelectionMethodType.enumValue != LoanLevelCreditScoreSelectionMethodBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for the Loan Credit Score Selection Method Type.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOString LoanLevelCreditScoreSelectionMethodTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanLevelCreditScoreSelectionMethodTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanLevelCreditScoreSelectionMethodTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanLevelCreditScoreSelectionMethodTypeOtherDescriptionSpecified
        {
            get { return LoanLevelCreditScoreSelectionMethodTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The value of loan level credit score.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOValue LoanLevelCreditScoreValue;

        /// <summary>
        /// Gets or sets a value indicating whether the Loan Level Credit Score Value element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Loan Level Credit Score Value element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanLevelCreditScoreValueSpecified
        {
            get { return LoanLevelCreditScoreValue != null; }
            set { }
        }

        /// <summary>
        /// Designates that there was an improvement to the credit grade due to compensating factors.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOIndicator RiskUpgradeIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the RiskUpgradeIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RiskUpgradeIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool RiskUpgradeIndicatorSpecified
        {
            get { return RiskUpgradeIndicator != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 12)]
        public LOAN_LEVEL_CREDIT_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
