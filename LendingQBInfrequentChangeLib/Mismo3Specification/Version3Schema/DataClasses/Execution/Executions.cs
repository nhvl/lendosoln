namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class EXECUTIONS
    {
        /// <summary>
        /// Gets a value indicating whether the EXECUTIONS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExecutionSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of executions.
        /// </summary>
        [XmlElement("EXECUTION", Order = 0)]
		public List<EXECUTION> Execution = new List<EXECUTION>();

        /// <summary>
        /// Gets or sets a value indicating whether the Execution element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Execution element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExecutionSpecified
        {
            get { return this.Execution != null && this.Execution.Count(e => e != null && e.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public EXECUTIONS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
