namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class EXECUTION_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the EXECUTION_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ActualSignatureTypeOtherDescriptionSpecified
                    || this.ActualSignatureTypeSpecified
                    || this.ExecutionDateSpecified
                    || this.ExecutionDatetimeSpecified
                    || this.ExecutionJudicialDistrictNameSpecified
                    || this.ExecutionJudicialDistrictTypeOtherDescriptionSpecified
                    || this.ExecutionJudicialDistrictTypeSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// The ActualSignatureType contains the actual type of the signature applied to the signature field.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<SignatureBase> ActualSignatureType;

        /// <summary>
        /// Gets or sets a value indicating whether the ActualSignatureType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ActualSignatureType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ActualSignatureTypeSpecified
        {
            get { return this.ActualSignatureType != null && this.ActualSignatureType.enumValue != SignatureBase.Blank; }
            set { }
        }

        /// <summary>
        /// Contains a description of the signature type when the value of “Other” is used for the ActualSignatureType attribute.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString ActualSignatureTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ActualSignatureTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ActualSignatureTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ActualSignatureTypeOtherDescriptionSpecified
        {
            get { return ActualSignatureTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The date the signature was or will be affixed to the document.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMODate ExecutionDate;

        /// <summary>
        /// Gets or sets a value indicating whether the ExecutionDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ExecutionDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExecutionDateSpecified
        {
            get { return ExecutionDate != null; }
            set { }
        }

        /// <summary>
        /// The date and time the signature was affixed to the document. 
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMODatetime ExecutionDatetime;

        /// <summary>
        /// Gets or sets a value indicating whether the ExecutionDateTime element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ExecutionDateTime element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExecutionDatetimeSpecified
        {
            get { return ExecutionDatetime != null; }
            set { }
        }

        /// <summary>
        /// The name of the judicial district where documents were signed (executed).
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString ExecutionJudicialDistrictName;

        /// <summary>
        /// Gets or sets a value indicating whether the ExecutionJudicialDistrictName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ExecutionJudicialDistrictName element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExecutionJudicialDistrictNameSpecified
        {
            get { return ExecutionJudicialDistrictName != null; }
            set { }
        }

        /// <summary>
        /// The type of judicial district where documents were signed (executed).
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOEnum<ExecutionJudicialDistrictBase> ExecutionJudicialDistrictType;

        /// <summary>
        /// Gets or sets a value indicating whether the ExecutionJudicialDistrictType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ExecutionJudicialDistrictType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExecutionJudicialDistrictTypeSpecified
        {
            get { return this.ExecutionJudicialDistrictType != null && this.ExecutionJudicialDistrictType.enumValue != ExecutionJudicialDistrictBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Execution Judicial District Type.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOString ExecutionJudicialDistrictTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ExecutionJudicialDistrictTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ExecutionJudicialDistrictTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExecutionJudicialDistrictTypeOtherDescriptionSpecified
        {
            get { return ExecutionJudicialDistrictTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 7)]
        public EXECUTION_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
