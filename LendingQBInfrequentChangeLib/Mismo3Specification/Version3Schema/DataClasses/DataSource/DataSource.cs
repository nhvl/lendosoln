namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class DATA_SOURCE
    {
        /// <summary>
        /// Gets a value indicating whether the DATA_SOURCE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DataSourceDescriptionSpecified
                    || this.DataSourceEffectiveDateSpecified
                    || this.DataSourceNameSpecified
                    || this.DataSourceTypeOtherDescriptionSpecified
                    || this.DataSourceTypeSpecified
                    || this.DataSourceURLSpecified
                    || this.DataSourceVerificationDescriptionSpecified
                    || this.DataSourceVersionIdentifierSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A free-form text field used to describe the source of the information. 
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString DataSourceDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the DataSourceDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DataSourceDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool DataSourceDescriptionSpecified
        {
            get { return DataSourceDescription != null; }
            set { }
        }

        /// <summary>
        /// The effective date of the source of information.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMODate DataSourceEffectiveDate;

        /// <summary>
        /// Gets or sets a value indicating whether the DataSourceEffectiveDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DataSourceEffectiveDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool DataSourceEffectiveDateSpecified
        {
            get { return DataSourceEffectiveDate != null; }
            set { }
        }

        /// <summary>
        /// The name associated with the provider or source of the data.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString DataSourceName;

        /// <summary>
        /// Gets or sets a value indicating whether the DataSourceName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DataSourceName element has been assigned a value.</value>
        [XmlIgnore]
        public bool DataSourceNameSpecified
        {
            get { return DataSourceName != null; }
            set { }
        }

        /// <summary>
        /// Used to identify sources of data used in the description and analysis of the structure.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOEnum<DataSourceBase> DataSourceType;

        /// <summary>
        /// Gets or sets a value indicating whether the DataSourceType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DataSourceType element has been assigned a value.</value>
        [XmlIgnore]
        public bool DataSourceTypeSpecified
        {
            get { return this.DataSourceType != null && this.DataSourceType.enumValue != DataSourceBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the data source type if Other is selected as the Structure Data Source Type.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString DataSourceTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the DataSourceTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DataSourceTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool DataSourceTypeOtherDescriptionSpecified
        {
            get { return DataSourceTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the URL or web site address of the company providing the data.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOURL DataSourceURL;

        /// <summary>
        /// Gets or sets a value indicating whether the DataSourceURL element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DataSourceURL element has been assigned a value.</value>
        [XmlIgnore]
        public bool DataSourceURLSpecified
        {
            get { return DataSourceURL != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe or comment on methods or other sources used to verify the accuracy of the data source referenced in Data Source Description.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOString DataSourceVerificationDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the DataSourceVerificationDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DataSourceVerificationDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool DataSourceVerificationDescriptionSpecified
        {
            get { return DataSourceVerificationDescription != null; }
            set { }
        }

        /// <summary>
        /// The version for the specified source of the data. 
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOIdentifier DataSourceVersionIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the DataSourceVersionIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DataSourceVersionIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool DataSourceVersionIdentifierSpecified
        {
            get { return DataSourceVersionIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 8)]
        public DATA_SOURCE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
