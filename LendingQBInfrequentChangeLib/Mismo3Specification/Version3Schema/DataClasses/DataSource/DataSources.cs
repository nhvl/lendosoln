namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class DATA_SOURCES
    {
        /// <summary>
        /// Gets a value indicating whether the DATA_SOURCES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DataSourceSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of data sources.
        /// </summary>
        [XmlElement("DATA_SOURCE", Order = 0)]
		public List<DATA_SOURCE> DataSource = new List<DATA_SOURCE>();

        /// <summary>
        /// Gets or sets a value indicating whether the DataSource element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DataSource element has been assigned a value.</value>
        [XmlIgnore]
        public bool DataSourceSpecified
        {
            get { return this.DataSource != null && this.DataSource.Count(d => d != null && d.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public DATA_SOURCES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
