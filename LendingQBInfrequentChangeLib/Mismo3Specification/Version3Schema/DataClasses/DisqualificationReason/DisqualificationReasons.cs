namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class DISQUALIFICATION_REASONS
    {
        /// <summary>
        /// Gets a value indicating whether the DISQUALIFICATION_REASONS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DisqualificationReasonSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of disqualification reasons.
        /// </summary>
        [XmlElement("DISQUALIFICATION_REASON", Order = 0)]
		public List<DISQUALIFICATION_REASON> DisqualificationReason = new List<DISQUALIFICATION_REASON>();

        /// <summary>
        /// Gets or sets a value indicating whether the DisqualificationReason element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DisqualificationReason element has been assigned a value.</value>
        [XmlIgnore]
        public bool DisqualificationReasonSpecified
        {
            get { return this.DisqualificationReason != null && this.DisqualificationReason.Count(d => d != null && d.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public DISQUALIFICATION_REASONS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
