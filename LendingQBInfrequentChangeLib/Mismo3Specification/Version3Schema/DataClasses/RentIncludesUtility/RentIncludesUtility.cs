namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class RENT_INCLUDES_UTILITY
    {
        /// <summary>
        /// Gets a value indicating whether the RENT_INCLUDES_UTILITY container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.RentIncludesUtilityTypeOtherDescriptionSpecified
                    || this.RentIncludesUtilityTypeSpecified;
            }
        }

        /// <summary>
        /// Specifies the type of utility that is included in the monthly rent.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<RentIncludesUtilityBase> RentIncludesUtilityType;

        /// <summary>
        /// Gets or sets a value indicating whether the RentIncludesUtilityType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RentIncludesUtilityType element has been assigned a value.</value>
        [XmlIgnore]
        public bool RentIncludesUtilityTypeSpecified
        {
            get { return this.RentIncludesUtilityType != null && this.RentIncludesUtilityType.enumValue != RentIncludesUtilityBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture additional information when Other is selected for Rent Includes Utility Type.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString RentIncludesUtilityTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the RentIncludesUtilityTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RentIncludesUtilityTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool RentIncludesUtilityTypeOtherDescriptionSpecified
        {
            get { return RentIncludesUtilityTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public RENT_INCLUDES_UTILITY_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
