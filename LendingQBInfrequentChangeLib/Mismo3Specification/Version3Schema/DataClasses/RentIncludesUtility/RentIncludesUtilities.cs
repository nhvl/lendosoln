namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class RENT_INCLUDES_UTILITIES
    {
        /// <summary>
        /// Gets a value indicating whether the RENT_INCLUDES_UTILITIES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.RentIncludesUtilitySpecified;
            }
        }

        /// <summary>
        /// Container for values of rent including utilities.
        /// </summary>
        [XmlElement("RENT_INCLUDES_UTILITY", Order = 0)]
		public List<RENT_INCLUDES_UTILITY> RentIncludesUtility = new List<RENT_INCLUDES_UTILITY>();

        /// <summary>
        /// Gets or sets a value indicating whether the RentIncludesUtility element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RentIncludesUtility element has been assigned a value.</value>
        [XmlIgnore]
        public bool RentIncludesUtilitySpecified
        {
            get { return this.RentIncludesUtility != null && this.RentIncludesUtility.Count(r => r != null && r.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public RENT_INCLUDES_UTILITIES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
