namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class AUDIT_TRAIL
    {
        /// <summary>
        /// Gets a value indicating whether the container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AuditTrailEntriesSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// Past actions performed on the document.
        /// </summary>
        [XmlElement("AUDIT_TRAIL_ENTRIES", Order = 0)]
        public AUDIT_TRAIL_ENTRIES AuditTrailEntries;

        /// <summary>
        /// Gets or sets a value indicating whether the AuditTrailEntries element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AuditTrailEntries element has been assigned a value.</value>
        [XmlIgnore]
        public bool AuditTrailEntriesSpecified
        {
            get { return this.AuditTrailEntries != null && this.AuditTrailEntries.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public AUDIT_TRAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
