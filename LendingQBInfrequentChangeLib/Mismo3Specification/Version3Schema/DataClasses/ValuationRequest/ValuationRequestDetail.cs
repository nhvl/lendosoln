namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class VALUATION_REQUEST_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the VALUATION_REQUEST_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AppraiserFileIdentifierSpecified
                    || this.ExtensionSpecified
                    || this.OrderingSystemNameSpecified
                    || this.PriorInterestIndicatorSpecified
                    || this.PropertyValuationFormIdentifierSpecified
                    || this.PropertyValuationFormTypeOtherDescriptionSpecified
                    || this.PropertyValuationFormTypeSpecified
                    || this.ValuationRequestActionTypeOtherDescriptionSpecified
                    || this.ValuationRequestActionTypeSpecified
                    || this.ValuationRequestCommentTextSpecified
                    || this.VendorOrderIdentifierSpecified
                    || this.VendorRelationshipTypeOtherDescriptionSpecified
                    || this.VendorRelationshipTypeSpecified;
            }
        }

        /// <summary>
        /// This is an identifier or number used by the appraiser to identify their reports. It is generally specific to the appraiser.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIdentifier AppraiserFileIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the AppraiserFileIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AppraiserFileIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool AppraiserFileIdentifierSpecified
        {
            get { return AppraiserFileIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The name of the system that originated the order.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString OrderingSystemName;

        /// <summary>
        /// Gets or sets a value indicating whether the OrderingSystemName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OrderingSystemName element has been assigned a value.</value>
        [XmlIgnore]
        public bool OrderingSystemNameSpecified
        {
            get { return OrderingSystemName != null; }
            set { }
        }

        /// <summary>
        /// Indicates if the valuation professional has had a prior interest in the property.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOIndicator PriorInterestIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PriorInterestIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PriorInterestIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PriorInterestIndicatorSpecified
        {
            get { return PriorInterestIndicator != null; }
            set { }
        }

        /// <summary>
        /// The name or number assigned to a form by the form author.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOIdentifier PropertyValuationFormIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyValuationFormIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyValuationFormIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyValuationFormIdentifierSpecified
        {
            get { return PropertyValuationFormIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Specifies the form used to provide the property valuation.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<PropertyValuationFormBase> PropertyValuationFormType;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyValuationFormType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyValuationFormType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyValuationFormTypeSpecified
        {
            get { return this.PropertyValuationFormType != null && this.PropertyValuationFormType.enumValue != PropertyValuationFormBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Property Valuation Form Type.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString PropertyValuationFormTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyValuationFormTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyValuationFormTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyValuationFormTypeOtherDescriptionSpecified
        {
            get { return PropertyValuationFormTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Defines the valuation service requested.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOEnum<ValuationRequestActionBase> ValuationRequestActionType;

        /// <summary>
        /// Gets or sets a value indicating whether the ValuationRequestActionType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ValuationRequestActionType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ValuationRequestActionTypeSpecified
        {
            get { return this.ValuationRequestActionType != null && this.ValuationRequestActionType.enumValue != ValuationRequestActionBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the Valuation Request Action Type if Other is selected.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOString ValuationRequestActionTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ValuationRequestActionTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ValuationRequestActionTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ValuationRequestActionTypeOtherDescriptionSpecified
        {
            get { return ValuationRequestActionTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used for comments for the valuation request in question.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOString ValuationRequestCommentText;

        /// <summary>
        /// Gets or sets a value indicating whether the ValuationRequestCommentText element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ValuationRequestCommentText element has been assigned a value.</value>
        [XmlIgnore]
        public bool ValuationRequestCommentTextSpecified
        {
            get { return ValuationRequestCommentText != null; }
            set { }
        }

        /// <summary>
        /// A string that uniquely identifies a specific order for a vendor set by the vendor.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOIdentifier VendorOrderIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the VendorOrderIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the VendorOrderIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool VendorOrderIdentifierSpecified
        {
            get { return VendorOrderIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The type of relationship between the valuation professional and the engaging organization.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOEnum<VendorRelationshipBase> VendorRelationshipType;

        /// <summary>
        /// Gets or sets a value indicating whether the VendorRelationshipType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the VendorRelationshipType element has been assigned a value.</value>
        [XmlIgnore]
        public bool VendorRelationshipTypeSpecified
        {
            get { return this.VendorRelationshipType != null && this.VendorRelationshipType.enumValue != VendorRelationshipBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Vendor Relationship Type.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOString VendorRelationshipTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the VendorRelationshipTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the VendorRelationshipTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool VendorRelationshipTypeOtherDescriptionSpecified
        {
            get { return VendorRelationshipTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 12)]
        public VALUATION_REQUEST_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
