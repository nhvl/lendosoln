namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class VALUATION_REQUEST
    {
        /// <summary>
        /// Gets a value indicating whether the VALUATION_REQUEST container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AssignmentParametersSpecified
                    || this.ExtensionSpecified
                    || this.ScopeOfWorkSpecified
                    || this.ValuationRequestDetailSpecified;
            }
        }

        /// <summary>
        /// Assignment parameters for valuation.
        /// </summary>
        [XmlElement("ASSIGNMENT_PARAMETERS", Order = 0)]
        public ASSIGNMENT_PARAMETERS AssignmentParameters;

        /// <summary>
        /// Gets or sets a value indicating whether the AssignmentParameters element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AssignmentParameters element has been assigned a value.</value>
        [XmlIgnore]
        public bool AssignmentParametersSpecified
        {
            get { return this.AssignmentParameters != null && this.AssignmentParameters.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Scope of work for valuation.
        /// </summary>
        [XmlElement("SCOPE_OF_WORK", Order = 1)]
        public SCOPE_OF_WORK ScopeOfWork;

        /// <summary>
        /// Gets or sets a value indicating whether the ScopeOfWork element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ScopeOfWork element has been assigned a value.</value>
        [XmlIgnore]
        public bool ScopeOfWorkSpecified
        {
            get { return this.ScopeOfWork != null && this.ScopeOfWork.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details on a valuation request.
        /// </summary>
        [XmlElement("VALUATION_REQUEST_DETAIL", Order = 2)]
        public VALUATION_REQUEST_DETAIL ValuationRequestDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the ValuationRequestDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ValuationRequestDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool ValuationRequestDetailSpecified
        {
            get { return this.ValuationRequestDetail != null && this.ValuationRequestDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public VALUATION_REQUEST_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
