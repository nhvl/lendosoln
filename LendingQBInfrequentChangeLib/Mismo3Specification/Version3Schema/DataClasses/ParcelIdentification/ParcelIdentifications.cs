namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PARCEL_IDENTIFICATIONS
    {
        /// <summary>
        /// Gets a value indicating whether the PARCEL_IDENTIFICATIONS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ParcelIdentificationSpecified;
            }
        }

        /// <summary>
        /// A collection of parcel identifications.
        /// </summary>
        [XmlElement("PARCEL_IDENTIFICATION", Order = 0)]
		public List<PARCEL_IDENTIFICATION> ParcelIdentification = new List<PARCEL_IDENTIFICATION>();

        /// <summary>
        /// Gets or sets a value indicating whether the ParcelIdentification element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ParcelIdentification element has been assigned a value.</value>
        [XmlIgnore]
        public bool ParcelIdentificationSpecified
        {
            get { return this.ParcelIdentification != null && this.ParcelIdentification.Count(p => p != null && p.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public PARCEL_IDENTIFICATIONS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
