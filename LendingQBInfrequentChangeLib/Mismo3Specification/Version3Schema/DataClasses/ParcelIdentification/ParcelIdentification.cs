namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class PARCEL_IDENTIFICATION
    {
        /// <summary>
        /// Gets a value indicating whether the PARCEL_IDENTIFICATION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ParcelIdentificationTypeOtherDescriptionSpecified
                    || this.ParcelIdentificationTypeSpecified
                    || this.ParcelIdentifierSpecified;
            }
        }

        /// <summary>
        /// Specifies other parcel identification types that are used by taxing authorities or others to identify a parcel of property.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<ParcelIdentificationBase> ParcelIdentificationType;

        /// <summary>
        /// Gets or sets a value indicating whether the ParcelIdentificationType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ParcelIdentificationType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ParcelIdentificationTypeSpecified
        {
            get { return this.ParcelIdentificationType != null && this.ParcelIdentificationType.enumValue != ParcelIdentificationBase.Blank; }
            set { }
        }

        /// <summary>
        /// A text field used to specify the kind of Parcel identifier when "Other" is selected from the enumerated list.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString ParcelIdentificationTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ParcelIdentificationTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ParcelIdentificationTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ParcelIdentificationTypeOtherDescriptionSpecified
        {
            get { return ParcelIdentificationTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The actual identifier value of the type specified by Parcel Identifier Type.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOIdentifier ParcelIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the ParcelIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ParcelIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool ParcelIdentifierSpecified
        {
            get { return ParcelIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public PARCEL_IDENTIFICATION_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
