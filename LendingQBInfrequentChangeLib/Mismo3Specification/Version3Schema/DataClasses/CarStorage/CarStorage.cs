namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class CAR_STORAGE
    {
        /// <summary>
        /// Gets a value indicating whether the CAR_STORAGE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AdequateIndicatorSpecified
                    || this.CarStorageAttachmentTypeSpecified
                    || this.CarStorageDescriptionSpecified
                    || this.CarStorageIndicatorSpecified
                    || this.CarStorageTypeOtherDescriptionSpecified
                    || this.CarStorageTypeSpecified
                    || this.ComponentAdjustmentAmountSpecified
                    || this.ConditionRatingDescriptionSpecified
                    || this.ConditionRatingTypeSpecified
                    || this.ExtensionSpecified
                    || this.MaterialDescriptionSpecified
                    || this.ParkingSpaceIdentifierSpecified
                    || this.ParkingSpacesCountSpecified
                    || this.ProjectCarStorageAdequacyEffectOnMarketabilityDescriptionSpecified
                    || this.ProjectCarStorageDescriptionSpecified
                    || this.ProjectCarStorageSpacesToUnitsRatioPercentSpecified
                    || this.ProjectGuestParkingIndicatorSpecified
                    || this.ProjectGuestParkingSpacesCountSpecified
                    || this.ProjectParkingSpaceAssignmentTypeOtherDescriptionSpecified
                    || this.ProjectParkingSpaceAssignmentTypeSpecified
                    || this.QualityRatingDescriptionSpecified
                    || this.QualityRatingTypeSpecified;
            }
        }

        /// <summary>
        /// Indicates that this feature is adequate for normal needs.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIndicator AdequateIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the AdequateIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AdequateIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool AdequateIndicatorSpecified
        {
            get { return AdequateIndicator != null; }
            set { }
        }

        /// <summary>
        /// Specifies if the car storage as specified by Car Storage Type is attached or detached.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<CarStorageAttachmentBase> CarStorageAttachmentType;

        /// <summary>
        /// Gets or sets a value indicating whether the CarStorageAttachmentType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CarStorageAttachmentType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CarStorageAttachmentTypeSpecified
        {
            get { return this.CarStorageAttachmentType != null && this.CarStorageAttachmentType.enumValue != CarStorageAttachmentBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe in detail the identified car storage.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString CarStorageDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the CarStorageDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CarStorageDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool CarStorageDescriptionSpecified
        {
            get { return CarStorageDescription != null; }
            set { }
        }

        /// <summary>
        /// When true Indicates the presence of car storage.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOIndicator CarStorageIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the CarStorageIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CarStorageIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool CarStorageIndicatorSpecified
        {
            get { return CarStorageIndicator != null; }
            set { }
        }

        /// <summary>
        /// Specifies the location and type of the car storage.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<CarStorageBase> CarStorageType;

        /// <summary>
        /// Gets or sets a value indicating whether the CarStorageType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CarStorageType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CarStorageTypeSpecified
        {
            get { return this.CarStorageType != null && this.CarStorageType.enumValue != CarStorageBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the car storage location and type if Other is selected as the Car Storage Type.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString CarStorageTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the CarStorageTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CarStorageTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool CarStorageTypeOtherDescriptionSpecified
        {
            get { return CarStorageTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount (either positive or negative) adjustment being made for a specific component of the property.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOAmount ComponentAdjustmentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ComponentAdjustmentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ComponentAdjustmentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ComponentAdjustmentAmountSpecified
        {
            get { return ComponentAdjustmentAmount != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe in detail the condition rating of the identified component.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOString ConditionRatingDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ConditionRatingDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConditionRatingDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConditionRatingDescriptionSpecified
        {
            get { return ConditionRatingDescription != null; }
            set { }
        }

        /// <summary>
        /// A rating of the condition of the identified component.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOEnum<ConditionRatingBase> ConditionRatingType;

        /// <summary>
        /// Gets or sets a value indicating whether the ConditionRatingType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConditionRatingType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConditionRatingTypeSpecified
        {
            get { return this.ConditionRatingType != null && this.ConditionRatingType.enumValue != ConditionRatingBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe in detail the material used in the identified component.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOString MaterialDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MaterialDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MaterialDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MaterialDescriptionSpecified
        {
            get { return MaterialDescription != null; }
            set { }
        }

        /// <summary>
        /// The identification of the parking space or spaces assigned to the subject unit (e.g. space numbers).
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOIdentifier ParkingSpaceIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the ParkingSpaceIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ParkingSpaceIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool ParkingSpaceIdentifierSpecified
        {
            get { return ParkingSpaceIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Specifies the number of vehicle parking spaces for the subject unit.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOCount ParkingSpacesCount;

        /// <summary>
        /// Gets or sets a value indicating whether the ParkingSpacesCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ParkingSpacesCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ParkingSpacesCountSpecified
        {
            get { return ParkingSpacesCount != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field describing the effect of the adequacy or inadequacy of parking on the marketability of the project or a unit thereof.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOString ProjectCarStorageAdequacyEffectOnMarketabilityDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectCarStorageAdequacyEffectOnMarketabilityDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectCarStorageAdequacyEffectOnMarketabilityDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectCarStorageAdequacyEffectOnMarketabilityDescriptionSpecified
        {
            get { return ProjectCarStorageAdequacyEffectOnMarketabilityDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the details, condition, restrictions, proximity, etc. of car storage for the entire project.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOString ProjectCarStorageDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectCarStorageDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectCarStorageDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectCarStorageDescriptionSpecified
        {
            get { return ProjectCarStorageDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the ratio of the total number of parking spaces to the number of units in the project.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOPercent ProjectCarStorageSpacesToUnitsRatioPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectCarStorageSpacesToUnitsRatioPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectCarStorageSpacesToUnitsRatioPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectCarStorageSpacesToUnitsRatioPercentSpecified
        {
            get { return ProjectCarStorageSpacesToUnitsRatioPercent != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the project does have parking for guests (i.e. non-residents).
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOIndicator ProjectGuestParkingIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectGuestParkingIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectGuestParkingIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectGuestParkingIndicatorSpecified
        {
            get { return ProjectGuestParkingIndicator != null; }
            set { }
        }

        /// <summary>
        /// The total number of guest or common-area parking spaces in the project.
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMOCount ProjectGuestParkingSpacesCount;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectGuestParkingSpacesCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectGuestParkingSpacesCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectGuestParkingSpacesCountSpecified
        {
            get { return ProjectGuestParkingSpacesCount != null; }
            set { }
        }

        /// <summary>
        /// Specifies the type of arrangement between the project and the assigned car storage for the subject unit.
        /// </summary>
        [XmlElement(Order = 17)]
        public MISMOEnum<ProjectParkingSpaceAssignmentBase> ProjectParkingSpaceAssignmentType;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectParkingSpaceAssignmentType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectParkingSpaceAssignmentType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectParkingSpaceAssignmentTypeSpecified
        {
            get { return this.ProjectParkingSpaceAssignmentType != null && this.ProjectParkingSpaceAssignmentType.enumValue != ProjectParkingSpaceAssignmentBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Parking Space Assignment Type.
        /// </summary>
        [XmlElement(Order = 18)]
        public MISMOString ProjectParkingSpaceAssignmentTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectParkingSpaceAssignmentTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectParkingSpaceAssignmentTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectParkingSpaceAssignmentTypeOtherDescriptionSpecified
        {
            get { return ProjectParkingSpaceAssignmentTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe in detail the quality rating of the identified component.
        /// </summary>
        [XmlElement(Order = 19)]
        public MISMOString QualityRatingDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the QualityRatingDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the QualityRatingDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool QualityRatingDescriptionSpecified
        {
            get { return QualityRatingDescription != null; }
            set { }
        }

        /// <summary>
        /// A rating of the quality of the identified component type.
        /// </summary>
        [XmlElement(Order = 20)]
        public MISMOEnum<QualityRatingBase> QualityRatingType;

        /// <summary>
        /// Gets or sets a value indicating whether the QualityRatingType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the QualityRatingType element has been assigned a value.</value>
        [XmlIgnore]
        public bool QualityRatingTypeSpecified
        {
            get { return this.QualityRatingType != null && this.QualityRatingType.enumValue != QualityRatingBase.Blank; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 21)]
        public CAR_STORAGE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
