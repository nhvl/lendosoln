namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CAR_STORAGES
    {
        /// <summary>
        /// Gets a value indicating whether the CAR_STORAGES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CarStorageSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of car storages.
        /// </summary>
        [XmlElement("CAR_STORAGE", Order = 0)]
		public List<CAR_STORAGE> CarStorage = new List<CAR_STORAGE>();

        /// <summary>
        /// Gets or sets a value indicating whether the CarStorage element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CarStorage element has been assigned a value.</value>
        [XmlIgnore]
        public bool CarStorageSpecified
        {
            get { return this.CarStorage != null && this.CarStorage.Count(c => c != null && c.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public CAR_STORAGES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
