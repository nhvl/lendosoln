namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class MARKET
    {
        /// <summary>
        /// Gets a value indicating whether the MARKET container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.MarketInventoriesSpecified
                    || this.MarketTrendSpecified;
            }
        }

        /// <summary>
        /// The real estate market inventory.
        /// </summary>
        [XmlElement("MARKET_INVENTORIES", Order = 0)]
        public MARKET_INVENTORIES MarketInventories;

        /// <summary>
        /// Gets or sets a value indicating whether the MarketInventories element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MarketInventories element has been assigned a value.</value>
        [XmlIgnore]
        public bool MarketInventoriesSpecified
        {
            get { return this.MarketInventories != null && this.MarketInventories.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A trend in the market.
        /// </summary>
        [XmlElement("MARKET_TREND", Order = 1)]
        public MARKET_TREND MarketTrend;

        /// <summary>
        /// Gets or sets a value indicating whether the MarketTrend element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MarketTrend element has been assigned a value.</value>
        [XmlIgnore]
        public bool MarketTrendSpecified
        {
            get { return this.MarketTrend != null && this.MarketTrend.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public MARKET_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
