namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class RECORDING_ENDORSEMENT_INFORMATION
    {
        /// <summary>
        /// Gets a value indicating whether the RECORDING_ENDORSEMENT_INFORMATION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CountyOfRecordationNameSpecified
                    || this.ExtensionSpecified
                    || this.FirstPageNumberValueSpecified
                    || this.InstrumentNumberIdentifierSpecified
                    || this.LastPageNumberValueSpecified
                    || this.OfficeOfRecordationTypeOtherDescriptionSpecified
                    || this.OfficeOfRecordationTypeSpecified
                    || this.PRIARecordingJurisdictionIdentifierSpecified
                    || this.RecordedDatetimeSpecified
                    || this.RecordingEndorsementIdentifierSpecified
                    || this.RecordingEndorsementTypeOtherDescriptionSpecified
                    || this.RecordingEndorsementTypeSpecified
                    || this.RecordingOfficersNameSpecified
                    || this.RecordingPagesCountSpecified
                    || this.StateOfRecordationNameSpecified
                    || this.VolumeIdentifierSpecified
                    || this.VolumeTypeOtherDescriptionSpecified
                    || this.VolumeTypeSpecified;
            }
        }

        /// <summary>
        /// The county of recordation name referenced in the subject recordable document.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString CountyOfRecordationName;

        /// <summary>
        /// Gets or sets a value indicating whether the CountyOfRecordationName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CountyOfRecordationName element has been assigned a value.</value>
        [XmlIgnore]
        public bool CountyOfRecordationNameSpecified
        {
            get { return CountyOfRecordationName != null; }
            set { }
        }

        /// <summary>
        /// The first page of the recorded document.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOValue FirstPageNumberValue;

        /// <summary>
        /// Gets or sets a value indicating whether the FirstPageNumberValue element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FirstPageNumberValue element has been assigned a value.</value>
        [XmlIgnore]
        public bool FirstPageNumberValueSpecified
        {
            get { return FirstPageNumberValue != null; }
            set { }
        }

        /// <summary>
        /// Sequential number assigned to each recorded document by the a County Recorder.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOIdentifier InstrumentNumberIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the InstrumentNumberIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InstrumentNumberIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool InstrumentNumberIdentifierSpecified
        {
            get { return InstrumentNumberIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The last page of the recorded document.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOValue LastPageNumberValue;

        /// <summary>
        /// Gets or sets a value indicating whether the LastPageNumberValue element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LastPageNumberValue element has been assigned a value.</value>
        [XmlIgnore]
        public bool LastPageNumberValueSpecified
        {
            get { return LastPageNumberValue != null; }
            set { }
        }

        /// <summary>
        /// The Office of Recordation type referenced in the subject recordable document.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<OfficeOfRecordationBase> OfficeOfRecordationType;

        /// <summary>
        /// Gets or sets a value indicating whether the OfficeOfRecordationType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OfficeOfRecordationType element has been assigned a value.</value>
        [XmlIgnore]
        public bool OfficeOfRecordationTypeSpecified
        {
            get { return this.OfficeOfRecordationType != null && this.OfficeOfRecordationType.enumValue != OfficeOfRecordationBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture the Office of Recordation type if Other is selected.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString OfficeOfRecordationTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the OfficeOfRecordationTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OfficeOfRecordationTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool OfficeOfRecordationTypeOtherDescriptionSpecified
        {
            get { return OfficeOfRecordationTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Identifies the recording jurisdiction in which the document needs to be recorded on public record. The identifier scheme is represented by:  - the first two characters are the United States Postal Service (USPS) state or territory abbreviation.  - the next three characters are a three digit numeric value.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOIdentifier PRIARecordingJurisdictionIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the PRIARecordingJurisdictionIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PRIARecordingJurisdictionIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool PRIARecordingJurisdictionIdentifierSpecified
        {
            get { return PRIARecordingJurisdictionIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Date and time the document was recorded by recorder.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMODatetime RecordedDatetime;

        /// <summary>
        /// Gets or sets a value indicating whether the RecordedDateTime element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RecordedDateTime element has been assigned a value.</value>
        [XmlIgnore]
        public bool RecordedDatetimeSpecified
        {
            get { return RecordedDatetime != null; }
            set { }
        }

        /// <summary>
        /// Endorsement identification number assigned to each recording transaction, such as an audit number. This differs from the Recording Endorsement Instrument Number.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOIdentifier RecordingEndorsementIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the RecordingEndorsementIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RecordingEndorsementIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool RecordingEndorsementIdentifierSpecified
        {
            get { return RecordingEndorsementIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Describes the source and purpose of the recording endorsement information.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOEnum<RecordingEndorsementBase> RecordingEndorsementType;

        /// <summary>
        /// Gets or sets a value indicating whether the RecordingEndorsementType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RecordingEndorsementType element has been assigned a value.</value>
        [XmlIgnore]
        public bool RecordingEndorsementTypeSpecified
        {
            get { return this.RecordingEndorsementType != null && this.RecordingEndorsementType.enumValue != RecordingEndorsementBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture the Recording Endorsement Type if Other is selected.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOString RecordingEndorsementTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the RecordingEndorsementTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RecordingEndorsementTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool RecordingEndorsementTypeOtherDescriptionSpecified
        {
            get { return RecordingEndorsementTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Name of the County Recording Official.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOString RecordingOfficersName;

        /// <summary>
        /// Gets or sets a value indicating whether the RecordingOfficersName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RecordingOfficersName element has been assigned a value.</value>
        [XmlIgnore]
        public bool RecordingOfficersNameSpecified
        {
            get { return RecordingOfficersName != null; }
            set { }
        }

        /// <summary>
        /// Number of pages in document being recorded.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOCount RecordingPagesCount;

        /// <summary>
        /// Gets or sets a value indicating whether the RecordingPagesCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RecordingPagesCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool RecordingPagesCountSpecified
        {
            get { return RecordingPagesCount != null; }
            set { }
        }

        /// <summary>
        /// The state of recordation referenced in the subject recordable document.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOString StateOfRecordationName;

        /// <summary>
        /// Gets or sets a value indicating whether the StateOfRecordationName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the StateOfRecordationName element has been assigned a value.</value>
        [XmlIgnore]
        public bool StateOfRecordationNameSpecified
        {
            get { return StateOfRecordationName != null; }
            set { }
        }

        /// <summary>
        /// Volume or Book number assigned to recorder document by the recorder.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOIdentifier VolumeIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the VolumeIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the VolumeIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool VolumeIdentifierSpecified
        {
            get { return VolumeIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Volume type of the document referenced  in the subject document.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOEnum<VolumeBase> VolumeType;

        /// <summary>
        /// Gets or sets a value indicating whether the VolumeType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the VolumeType element has been assigned a value.</value>
        [XmlIgnore]
        public bool VolumeTypeSpecified
        {
            get { return this.VolumeType != null && this.VolumeType.enumValue != VolumeBase.Blank; }
            set { }
        }

        /// <summary>
        /// A description of the volume type when Other is selected from the enumerated list.
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMOString VolumeTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the VolumeTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the VolumeTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool VolumeTypeOtherDescriptionSpecified
        {
            get { return VolumeTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 17)]
        public RECORDING_ENDORSEMENT_INFORMATION_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
