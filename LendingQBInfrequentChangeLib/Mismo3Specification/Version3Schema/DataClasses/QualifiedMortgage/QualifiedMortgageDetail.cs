namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class QUALIFIED_MORTGAGE_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the QUALIFIED_MORTGAGE_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AbilityToRepayInterestRateDebtExpenseRatioPercentSpecified
                    || this.AbilityToRepayMethodTypeSpecified
                    || this.ExtensionSpecified
                    || this.PortfolioPeriodEffectiveMonthsCountSpecified
                    || this.PresumptionOfComplianceTypeSpecified
                    || this.QualifiedMortgageHighestRateDebtExpenseRatioPercentSpecified
                    || this.QualifiedMortgageMinimumRequiredFICOScoreValueSpecified
                    || this.QualifiedMortgageTemporaryGSETypeSpecified
                    || this.QualifiedMortgageTotalPointsAndFeesThresholdAmountSpecified
                    || this.QualifiedMortgageTotalPointsAndFeesThresholdPercentSpecified
                    || this.QualifiedMortgageTypeOtherDescriptionSpecified
                    || this.QualifiedMortgageTypeSpecified
                    || this.SafeHarborThresholdAmountSpecified;
            }
        }

        /// <summary>
        /// The ratio of all debt payments of the borrowers, including proposed housing expenses, to the qualifying income of the borrowers (back-end ratio) based on the note interest rate.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOPercent AbilityToRepayInterestRateDebtExpenseRatioPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the AbilityToRepayInterestRateDebtExpenseRatioPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AbilityToRepayInterestRateDebtExpenseRatioPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool AbilityToRepayInterestRateDebtExpenseRatioPercentSpecified
        {
            get { return AbilityToRepayInterestRateDebtExpenseRatioPercent != null; }
            set { }
        }

        /// <summary>
        /// Specifies the method by which the creditor satisfied Regulation Z Ability-to-Repay requirements.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<AbilityToRepayMethodBase> AbilityToRepayMethodType;

        /// <summary>
        /// Gets or sets a value indicating whether the AbilityToRepayMethodType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AbilityToRepayMethodType element has been assigned a value.</value>
        [XmlIgnore]
        public bool AbilityToRepayMethodTypeSpecified
        {
            get { return this.AbilityToRepayMethodType != null && this.AbilityToRepayMethodType.enumValue != AbilityToRepayMethodBase.Blank; }
            set { }
        }

        /// <summary>
        /// The number of months after the loan was consummated that the originating lender has held or serviced the loan.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOCount PortfolioPeriodEffectiveMonthsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the PortfolioPeriodEffectiveMonthsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PortfolioPeriodEffectiveMonthsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PortfolioPeriodEffectiveMonthsCountSpecified
        {
            get { return PortfolioPeriodEffectiveMonthsCount != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates the borrower has previously received a Chapter 7 bankruptcy discharge.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOEnum<PresumptionOfComplianceBase> PresumptionOfComplianceType;

        /// <summary>
        /// Gets or sets a value indicating whether the PresumptionOfComplianceType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PresumptionOfComplianceType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PresumptionOfComplianceTypeSpecified
        {
            get { return this.PresumptionOfComplianceType != null && this.PresumptionOfComplianceType.enumValue != PresumptionOfComplianceBase.Blank; }
            set { }
        }

        /// <summary>
        /// The ratio of all debt payments of the borrowers, including proposed housing expenses, to the qualifying income of the borrowers (back-end ratio) based on the highest interest rate that may apply during the first five years of principal and interest payments. Specific to Qualified Mortgage only.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOPercent QualifiedMortgageHighestRateDebtExpenseRatioPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the QualifiedMortgageHighestRateDebtExpenseRatioPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the QualifiedMortgageHighestRateDebtExpenseRatioPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool QualifiedMortgageHighestRateDebtExpenseRatioPercentSpecified
        {
            get { return QualifiedMortgageHighestRateDebtExpenseRatioPercent != null; }
            set { }
        }

        /// <summary>
        /// The minimum required FICO score to qualify.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOValue QualifiedMortgageMinimumRequiredFICOScoreValue;

        /// <summary>
        /// Gets or sets a value indicating whether the minimum FICO score element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the minimum FICO score element has been assigned a value.</value>
        [XmlIgnore]
        public bool QualifiedMortgageMinimumRequiredFICOScoreValueSpecified
        {
            get { return QualifiedMortgageMinimumRequiredFICOScoreValue != null; }
            set { }
        }

        /// <summary>
        /// Specifies the GSE Guidelines by which Temporary Qualified Mortgage eligibility was determined.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOEnum<QualifiedMortgageTemporaryGSEBase> QualifiedMortgageTemporaryGSEType;

        /// <summary>
        /// Gets or sets a value indicating whether the QualifiedMortgageTemporaryGSEType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the QualifiedMortgageTemporaryGSEType element has been assigned a value.</value>
        [XmlIgnore]
        public bool QualifiedMortgageTemporaryGSETypeSpecified
        {
            get { return this.QualifiedMortgageTemporaryGSEType != null && this.QualifiedMortgageTemporaryGSEType.enumValue != QualifiedMortgageTemporaryGSEBase.Blank; }
            set { }
        }

        /// <summary>
        /// The total amount of points and fees dollar amount limit established by Regulation Z, Truth in Lending.  This amount is used in calculations for Qualified Mortgage purposes.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOAmount QualifiedMortgageTotalPointsAndFeesThresholdAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the QualifiedMortgageTotalPointsAndFeesThresholdAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the QualifiedMortgageTotalPointsAndFeesThresholdAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool QualifiedMortgageTotalPointsAndFeesThresholdAmountSpecified
        {
            get { return QualifiedMortgageTotalPointsAndFeesThresholdAmount != null; }
            set { }
        }

        /// <summary>
        /// The total amount of points and fees percentage limit established by Regulation Z, Truth in Lending.  This amount is used in calculations for Qualified Mortgage purposes.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOPercent QualifiedMortgageTotalPointsAndFeesThresholdPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the QualifiedMortgageTotalPointsAndFeesThresholdPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the QualifiedMortgageTotalPointsAndFeesThresholdPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool QualifiedMortgageTotalPointsAndFeesThresholdPercentSpecified
        {
            get { return QualifiedMortgageTotalPointsAndFeesThresholdPercent != null; }
            set { }
        }

        /// <summary>
        /// Identifies the specific standard used to satisfy Regulation Z Qualified Mortgage standards.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOEnum<QualifiedMortgageBase> QualifiedMortgageType;

        /// <summary>
        /// Gets or sets a value indicating whether the QualifiedMortgageType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the QualifiedMortgageType element has been assigned a value.</value>
        [XmlIgnore]
        public bool QualifiedMortgageTypeSpecified
        {
            get { return this.QualifiedMortgageType != null && this.QualifiedMortgageType.enumValue != QualifiedMortgageBase.Blank; }
            set { }
        }

        /// <summary>
        /// Identifies the specific standard used to satisfy Regulation Z Qualified Mortgage standards.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOString QualifiedMortgageTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the QualifiedMortgageTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the QualifiedMortgageTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool QualifiedMortgageTypeOtherDescriptionSpecified
        {
            get { return QualifiedMortgageTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Calculated amount required as a component in specifying the presumption of compliance type.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOAmount SafeHarborThresholdAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the SafeHarborThresholdAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SafeHarborThresholdAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool SafeHarborThresholdAmountSpecified
        {
            get { return SafeHarborThresholdAmount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 12)]
        public QUALIFIED_MORTGAGE_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
