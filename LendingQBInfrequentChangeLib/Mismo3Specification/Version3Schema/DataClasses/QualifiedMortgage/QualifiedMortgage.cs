namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class QUALIFIED_MORTGAGE
    {
        /// <summary>
        /// Gets a value indicating whether the QUALIFIED_MORTGAGE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExemptionsSpecified
                    || this.ExtensionSpecified
                    || this.QualifiedMortgageDetailSpecified;
            }
        }

        /// <summary>
        /// Exemptions related to a qualified mortgage.
        /// </summary>
        [XmlElement("EXEMPTIONS", Order = 0)]
        public EXEMPTIONS Exemptions;

        /// <summary>
        /// Gets or sets a value indicating whether the Exemptions element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Exemptions element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExemptionsSpecified
        {
            get { return this.Exemptions != null && this.Exemptions.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details on a qualified mortgage.
        /// </summary>
        [XmlElement("QUALIFIED_MORTGAGE_DETAIL", Order = 1)]
        public QUALIFIED_MORTGAGE_DETAIL QualifiedMortgageDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the QualifiedMortgageDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the QualifiedMortgageDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool QualifiedMortgageDetailSpecified
        {
            get { return this.QualifiedMortgageDetail != null && this.QualifiedMortgageDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public QUALIFIED_MORTGAGE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
