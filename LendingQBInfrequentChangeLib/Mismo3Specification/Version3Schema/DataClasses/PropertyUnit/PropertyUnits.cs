namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PROPERTY_UNITS
    {
        /// <summary>
        /// Gets a value indicating whether the PROPERTY_UNITS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.PropertyUnitSpecified;
            }
        }

        /// <summary>
        /// A collection of property units.
        /// </summary>
        [XmlElement("PROPERTY_UNIT", Order = 0)]
		public List<PROPERTY_UNIT> PropertyUnit = new List<PROPERTY_UNIT>();

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyUnit element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyUnit element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyUnitSpecified
        {
            get { return this.PropertyUnit != null && this.PropertyUnit.Count(p => p != null && p.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public PROPERTY_UNITS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
