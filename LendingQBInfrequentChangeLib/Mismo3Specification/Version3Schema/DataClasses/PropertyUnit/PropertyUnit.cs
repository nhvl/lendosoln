namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class PROPERTY_UNIT
    {
        /// <summary>
        /// Gets a value indicating whether the PROPERTY_UNIT container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AddressSpecified
                    || this.ExtensionSpecified
                    || this.PropertyUnitDetailSpecified
                    || this.UnitChargeSpecified;
            }
        }

        /// <summary>
        /// The address of the property unit.
        /// </summary>
        [XmlElement("ADDRESS", Order = 0)]
        public ADDRESS Address;

        /// <summary>
        /// Gets or sets a value indicating whether the Address element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Address element has been assigned a value.</value>
        [XmlIgnore]
        public bool AddressSpecified
        {
            get { return this.Address != null && this.Address.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details on the property unit.
        /// </summary>
        [XmlElement("PROPERTY_UNIT_DETAIL", Order = 1)]
        public PROPERTY_UNIT_DETAIL PropertyUnitDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyUnitDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyUnitDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyUnitDetailSpecified
        {
            get { return this.PropertyUnitDetail != null && this.PropertyUnitDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A unit charge related to the property unit.
        /// </summary>
        [XmlElement("UNIT_CHARGE", Order = 2)]
        public UNIT_CHARGE UnitCharge;

        /// <summary>
        /// Gets or sets a value indicating whether the UnitCharge element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the UnitCharge element has been assigned a value.</value>
        [XmlIgnore]
        public bool UnitChargeSpecified
        {
            get { return this.UnitCharge != null && this.UnitCharge.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public PROPERTY_UNIT_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
