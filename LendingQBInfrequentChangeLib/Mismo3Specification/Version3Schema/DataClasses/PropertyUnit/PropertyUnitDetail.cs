namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PROPERTY_UNIT_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the PROPERTY_UNIT_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BathroomNumberSpecified
                    || this.BedroomCountSpecified
                    || this.CooperativeAssignmentOfLeaseDateSpecified
                    || this.CooperativeProprietaryLeaseDateSpecified
                    || this.CooperativeStockCertificateNumberIdentifierSpecified
                    || this.CooperativeUnitSharesCountSpecified
                    || this.ExtensionSpecified
                    || this.FloorIdentifierSpecified
                    || this.LeaseExpirationDateSpecified
                    || this.LevelCountSpecified
                    || this.PropertyDwellingUnitEligibleRentAmountSpecified
                    || this.PropertyDwellingUnitLeaseProvidedIndicatorSpecified
                    || this.PropertyDwellingUnitPastDueRentAmountSpecified
                    || this.SquareFeetPerUnitNumberSpecified
                    || this.TotalRoomCountSpecified
                    || this.UnitIdentifierSpecified
                    || this.UnitOccupancyTypeSpecified;
            }
        }

        /// <summary>
        /// The number of bathrooms per unit.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMONumeric BathroomNumber;

        /// <summary>
        /// Gets or sets a value indicating whether the BathroomNumber element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BathroomNumber element has been assigned a value.</value>
        [XmlIgnore]
        public bool BathroomNumberSpecified
        {
            get { return BathroomNumber != null; }
            set { }
        }

        /// <summary>
        /// The number of bedrooms per unit.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOCount BedroomCount;

        /// <summary>
        /// Gets or sets a value indicating whether the BedroomCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BedroomCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool BedroomCountSpecified
        {
            get { return BedroomCount != null; }
            set { }
        }

        /// <summary>
        /// The date of transfer of the lessor's or lessee's interest in a lease. Assignment of lease is the transfer of a lease by a lessee to a third party, with all its rights and obligations. The lessee transfers the entire unexpired remainder of the lease term to a third party. The assignor remains liable under the original lease contract unless expressly released by the landlord. Some leases may contain restrictions on assignments or subleases.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMODate CooperativeAssignmentOfLeaseDate;

        /// <summary>
        /// Gets or sets a value indicating whether the CooperativeAssignmentOfLeaseDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CooperativeAssignmentOfLeaseDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool CooperativeAssignmentOfLeaseDateSpecified
        {
            get { return CooperativeAssignmentOfLeaseDate != null; }
            set { }
        }

        /// <summary>
        /// The closing date of a lease agreement between the Co-op and the shareholder allowing use of a particular unit or apartment. A proprietary lease is a lease given by a corporation to another. It is often used in a co-op context, where the owner is given a certain number of shares in the co-op, along with a proprietary lease for one of the residences in the building. In this manner, a stockholder in the co-op has a certain number of shares in the co-op, along with a lease to use one of the apartments under the conditions specified.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMODate CooperativeProprietaryLeaseDate;

        /// <summary>
        /// Gets or sets a value indicating whether the CooperativeProprietaryLeaseDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CooperativeProprietaryLeaseDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool CooperativeProprietaryLeaseDateSpecified
        {
            get { return CooperativeProprietaryLeaseDate != null; }
            set { }
        }

        /// <summary>
        /// The stock certification number or identifier for the Co-op shares being purchased.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOIdentifier CooperativeStockCertificateNumberIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the CooperativeStockCertificateNumberIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CooperativeStockCertificateNumberIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool CooperativeStockCertificateNumberIdentifierSpecified
        {
            get { return CooperativeStockCertificateNumberIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Reports the number of shares (e.g. of cooperative ownership stock) the subject unit controls.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOCount CooperativeUnitSharesCount;

        /// <summary>
        /// Gets or sets a value indicating whether the CooperativeUnitSharesCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CooperativeUnitSharesCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool CooperativeUnitSharesCountSpecified
        {
            get { return CooperativeUnitSharesCount != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field identifying the floor of the unit.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOIdentifier FloorIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the FloorIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FloorIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool FloorIdentifierSpecified
        {
            get { return FloorIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The date the lease expires.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMODate LeaseExpirationDate;

        /// <summary>
        /// Gets or sets a value indicating whether the LeaseExpirationDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LeaseExpirationDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool LeaseExpirationDateSpecified
        {
            get { return LeaseExpirationDate != null; }
            set { }
        }

        /// <summary>
        /// The number of structure levels.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOCount LevelCount;

        /// <summary>
        /// Gets or sets a value indicating whether the LevelCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LevelCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool LevelCountSpecified
        {
            get { return LevelCount != null; }
            set { }
        }

        /// <summary>
        /// The actual contract rent amount if the property dwelling unit is rented.  If there is no active lease this is the monthly market value amount.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOAmount PropertyDwellingUnitEligibleRentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyDwellingUnitEligibleRentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyDwellingUnitEligibleRentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyDwellingUnitEligibleRentAmountSpecified
        {
            get { return PropertyDwellingUnitEligibleRentAmount != null; }
            set { }
        }

        /// <summary>
        /// Indicates that a lease agreement has been provided for the dwelling unit.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOIndicator PropertyDwellingUnitLeaseProvidedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyDwellingUnitLeaseProvidedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyDwellingUnitLeaseProvidedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyDwellingUnitLeaseProvidedIndicatorSpecified
        {
            get { return PropertyDwellingUnitLeaseProvidedIndicator != null; }
            set { }
        }

        /// <summary>
        /// The total dollar amount of monthly rent that is past due for the unit.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOAmount PropertyDwellingUnitPastDueRentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyDwellingUnitPastDueRentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyDwellingUnitPastDueRentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyDwellingUnitPastDueRentAmountSpecified
        {
            get { return PropertyDwellingUnitPastDueRentAmount != null; }
            set { }
        }

        /// <summary>
        /// The total square footage divided by the number of units.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMONumeric SquareFeetPerUnitNumber;

        /// <summary>
        /// Gets or sets a value indicating whether the SquareFeetPerUnitNumber element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SquareFeetPerUnitNumber element has been assigned a value.</value>
        [XmlIgnore]
        public bool SquareFeetPerUnitNumberSpecified
        {
            get { return SquareFeetPerUnitNumber != null; }
            set { }
        }

        /// <summary>
        /// Specifies the total number of livable rooms.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOCount TotalRoomCount;

        /// <summary>
        /// Gets or sets a value indicating whether the TotalRoomCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TotalRoomCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TotalRoomCountSpecified
        {
            get { return TotalRoomCount != null; }
            set { }
        }

        /// <summary>
        /// The identification of the unit.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOIdentifier UnitIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the UnitIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the UnitIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool UnitIdentifierSpecified
        {
            get { return UnitIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Specifies the current occupancy status of the specified unit within a property or an individual unit or a group of units within project.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOEnum<UnitOccupancyBase> UnitOccupancyType;

        /// <summary>
        /// Gets or sets a value indicating whether the UnitOccupancyType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the UnitOccupancyType element has been assigned a value.</value>
        [XmlIgnore]
        public bool UnitOccupancyTypeSpecified
        {
            get { return this.UnitOccupancyType != null && this.UnitOccupancyType.enumValue != UnitOccupancyBase.Blank; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 16)]
        public PROPERTY_UNIT_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
