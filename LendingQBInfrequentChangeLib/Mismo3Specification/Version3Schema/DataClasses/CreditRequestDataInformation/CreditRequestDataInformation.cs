namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CREDIT_REQUEST_DATA_INFORMATION
    {
        /// <summary>
        /// Gets a value indicating whether the CREDIT_REQUEST_DATA_INFORMATION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DataVersionsSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// Versions of credit request data.
        /// </summary>
        [XmlElement("DATA_VERSIONS", Order = 0)]
        public DATA_VERSIONS DataVersions;

        /// <summary>
        /// Gets or sets a value indicating whether the DataVersions element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DataVersions element has been assigned a value.</value>
        [XmlIgnore]
        public bool DataVersionsSpecified
        {
            get { return this.DataVersions != null && this.DataVersions.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public CREDIT_REQUEST_DATA_INFORMATION_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
