namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class ERROR_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the ERROR_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ErrorCategoryCodeSpecified
                    || this.ExtensionSpecified
                    || this.RuleCodeSpecified
                    || this.RuleDescriptionSpecified;
            }
        }

        /// <summary>
        /// Identifies a user-defined category for an error.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOCode ErrorCategoryCode;

        /// <summary>
        /// Gets or sets a value indicating whether the ErrorCategoryCode element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ErrorCategoryCode element has been assigned a value.</value>
        [XmlIgnore]
        public bool ErrorCategoryCodeSpecified
        {
            get { return ErrorCategoryCode != null; }
            set { }
        }

        /// <summary>
        /// A unique code associated with an executed rule.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOCode RuleCode;

        /// <summary>
        /// Gets or sets a value indicating whether the RuleCode element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RuleCode element has been assigned a value.</value>
        [XmlIgnore]
        public bool RuleCodeSpecified
        {
            get { return RuleCode != null; }
            set { }
        }

        /// <summary>
        /// AText description of an executed rule.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString RuleDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the RuleDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RuleDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool RuleDescriptionSpecified
        {
            get { return RuleDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public ERROR_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
