namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class IMPROVEMENT
    {
        /// <summary>
        /// Gets a value indicating whether the IMPROVEMENT container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CarStoragesSpecified
                    || this.ComparisonToNeighborhoodsSpecified
                    || this.ExtensionSpecified
                    || this.GreenBuildingCertificationsSpecified
                    || this.ImprovementFeatureSpecified
                    || this.InteriorSpecified
                    || this.PropertyPreservationsSpecified
                    || this.StructureSpecified
                    || this.SystemSpecified;
            }
        }

        /// <summary>
        /// Car storage improvements.
        /// </summary>
        [XmlElement("CAR_STORAGES", Order = 0)]
        public CAR_STORAGES CarStorages;

        /// <summary>
        /// Gets or sets a value indicating whether the CarStorages element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CarStorages element has been assigned a value.</value>
        [XmlIgnore]
        public bool CarStoragesSpecified
        {
            get { return this.CarStorages != null && this.CarStorages.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Comparison of the property to its neighborhood.
        /// </summary>
        [XmlElement("COMPARISON_TO_NEIGHBORHOODS", Order = 1)]
        public COMPARISON_TO_NEIGHBORHOODS ComparisonToNeighborhoods;

        /// <summary>
        /// Gets or sets a value indicating whether the ComparisonToNeighborhoods element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ComparisonToNeighborhoods element has been assigned a value.</value>
        [XmlIgnore]
        public bool ComparisonToNeighborhoodsSpecified
        {
            get { return this.ComparisonToNeighborhoods != null && this.ComparisonToNeighborhoods.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Green building certifications for the structure.
        /// </summary>
        [XmlElement("GREEN_BUILDING_CERTIFICATIONS", Order = 2)]
        public GREEN_BUILDING_CERTIFICATIONS GreenBuildingCertifications;

        /// <summary>
        /// Gets or sets a value indicating whether the GreenBuildingCertifications element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GreenBuildingCertifications element has been assigned a value.</value>
        [XmlIgnore]
        public bool GreenBuildingCertificationsSpecified
        {
            get { return this.GreenBuildingCertifications != null && this.GreenBuildingCertifications.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// An improvement feature.
        /// </summary>
        [XmlElement("IMPROVEMENT_FEATURE", Order = 3)]
        public IMPROVEMENT_FEATURE ImprovementFeature;

        /// <summary>
        /// Gets or sets a value indicating whether the ImprovementFeature element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ImprovementFeature element has been assigned a value.</value>
        [XmlIgnore]
        public bool ImprovementFeatureSpecified
        {
            get { return this.ImprovementFeature != null && this.ImprovementFeature.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// An interior improvement.
        /// </summary>
        [XmlElement("INTERIOR", Order = 4)]
        public INTERIOR Interior;

        /// <summary>
        /// Gets or sets a value indicating whether the Interior element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Interior element has been assigned a value.</value>
        [XmlIgnore]
        public bool InteriorSpecified
        {
            get { return this.Interior != null && this.Interior.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A property preservation improvement.
        /// </summary>
        [XmlElement("PROPERTY_PRESERVATIONS", Order = 5)]
        public PROPERTY_PRESERVATIONS PropertyPreservations;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyPreservations element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyPreservations element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyPreservationsSpecified
        {
            get { return this.PropertyPreservations != null && this.PropertyPreservations.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A structural improvement.
        /// </summary>
        [XmlElement("STRUCTURE", Order = 6)]
        public STRUCTURE Structure;

        /// <summary>
        /// Gets or sets a value indicating whether the Structure element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Structure element has been assigned a value.</value>
        [XmlIgnore]
        public bool StructureSpecified
        {
            get { return this.Structure != null && this.Structure.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A system improvement.
        /// </summary>
        [XmlElement("SYSTEM", Order = 7)]
        public SYSTEM System;

        /// <summary>
        /// Gets or sets a value indicating whether the System element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the System element has been assigned a value.</value>
        [XmlIgnore]
        public bool SystemSpecified
        {
            get { return this.System != null && this.System.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 8)]
        public IMPROVEMENT_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
