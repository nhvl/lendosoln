namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class ESCROW_ITEM_DISBURSEMENT
    {
        /// <summary>
        /// Gets a value indicating whether the ESCROW_ITEM_DISBURSEMENT container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.EscrowItemDisbursedDateSpecified
                    || this.EscrowItemDisbursementAmountSpecified
                    || this.EscrowItemDisbursementDateSpecified
                    || this.EscrowItemDisbursementTermMonthsCountSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// The date the escrow disbursement amount for the associated escrow item disbursement date was actually disbursed to the payee for the indicated escrow item. If no date provided, indicates the escrow disbursement amount for the associated escrow item disbursement date has not been disbursed to the payee.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMODate EscrowItemDisbursedDate;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowItemDisbursedDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowItemDisbursedDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowItemDisbursedDateSpecified
        {
            get { return EscrowItemDisbursedDate != null; }
            set { }
        }

        /// <summary>
        /// The premium to be paid for the associated escrow item and the due date.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOAmount EscrowItemDisbursementAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowItemDisbursementAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowItemDisbursementAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowItemDisbursementAmountSpecified
        {
            get { return EscrowItemDisbursementAmount != null; }
            set { }
        }

        /// <summary>
        /// The date the escrow item type is due in the aggregate cycle.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMODate EscrowItemDisbursementDate;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowItemDisbursementDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowItemDisbursementDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowItemDisbursementDateSpecified
        {
            get { return EscrowItemDisbursementDate != null; }
            set { }
        }

        /// <summary>
        /// The term expressed in months between each premium disbursement associated with the escrow item and the due date.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOCount EscrowItemDisbursementTermMonthsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowItemDisbursementTermMonthsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowItemDisbursementTermMonthsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowItemDisbursementTermMonthsCountSpecified
        {
            get { return EscrowItemDisbursementTermMonthsCount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public ESCROW_ITEM_DISBURSEMENT_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
