namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class ESCROW_ITEM_DISBURSEMENTS
    {
        /// <summary>
        /// Gets a value indicating whether the ESCROW_ITEM_DISBURSEMENTS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.EscrowItemDisbursementSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of escrow item disbursements.
        /// </summary>
        [XmlElement("ESCROW_ITEM_DISBURSEMENT", Order = 0)]
        public List<ESCROW_ITEM_DISBURSEMENT> EscrowItemDisbursement = new List<ESCROW_ITEM_DISBURSEMENT>();

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowItemDisbursement element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowItemDisbursement element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowItemDisbursementSpecified
        {
            get { return this.EscrowItemDisbursement != null && this.EscrowItemDisbursement.Count(e => e != null && e.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public ESCROW_ITEM_DISBURSEMENTS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
