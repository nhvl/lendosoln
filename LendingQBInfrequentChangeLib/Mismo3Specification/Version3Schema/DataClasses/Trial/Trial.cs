namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class TRIAL
    {
        /// <summary>
        /// Gets a value indicating whether the TRIAL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.HardshipAffidavitWaivedIndicatorSpecified
                    || this.PriorToTrialPlanExpectedPaymentAmountSpecified
                    || this.ServicerAttestedLastTrialPaymentIndicatorSpecified
                    || this.TrialPaymentCurrentIndicatorSpecified
                    || this.TrialPeriodTotalPrincipalAppliedAmountSpecified
                    || this.TrialPlanAgreementSignedDateSpecified
                    || this.TrialPlanCommitmentDateSpecified
                    || this.TrialPlanEstimatedPaymentAmountSpecified
                    || this.TrialPlanExtendableIndicatorSpecified
                    || this.TrialPlanExtendedIndicatorSpecified
                    || this.TrialPlanExtendedMonthsCountSpecified
                    || this.TrialPlanExtensionAdditionalPaymentRequiredIndicatorSpecified
                    || this.TrialPlanFirstPaymentReceivedDateSpecified
                    || this.TrialPlanIncomeVerificationWaivedIndicatorSpecified
                    || this.TrialPlanInterestAppliedAmountSpecified
                    || this.TrialPlanPaymentRemainingAmountSpecified
                    || this.TrialPlanPaymentsReceivedCountSpecified
                    || this.TrialPlanScheduledPaymentAmountSpecified
                    || this.TrialPlanUnsuccessfulReasonTypeOtherDescriptionSpecified
                    || this.TrialPlanUnsuccessfulReasonTypeSpecified;
            }
        }

        /// <summary>
        /// When true, indicates a trial period plan was mailed to the borrower rather than requiring the borrower to  complete a hardship affidavit.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIndicator HardshipAffidavitWaivedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the HardshipAffidavitWaivedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HardshipAffidavitWaivedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool HardshipAffidavitWaivedIndicatorSpecified
        {
            get { return HardshipAffidavitWaivedIndicator != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount that the servicer expects the borrower to pay between trial structuring and the start of the trial period.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOAmount PriorToTrialPlanExpectedPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PriorToTrialPlanExpectedPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PriorToTrialPlanExpectedPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PriorToTrialPlanExpectedPaymentAmountSpecified
        {
            get { return PriorToTrialPlanExpectedPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that the final trial payment has been received and all the updates for the official modification have been made.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOIndicator ServicerAttestedLastTrialPaymentIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ServicerAttestedLastTrialPaymentIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServicerAttestedLastTrialPaymentIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServicerAttestedLastTrialPaymentIndicatorSpecified
        {
            get { return ServicerAttestedLastTrialPaymentIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether the trial payments for this loan are current.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOIndicator TrialPaymentCurrentIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the TrialPaymentCurrentIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TrialPaymentCurrentIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool TrialPaymentCurrentIndicatorSpecified
        {
            get { return TrialPaymentCurrentIndicator != null; }
            set { }
        }

        /// <summary>
        /// The total amount applied to principal during the trial period reported once the trail period has been completed.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOAmount TrialPeriodTotalPrincipalAppliedAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the TrialPeriodTotalPrincipalAppliedAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TrialPeriodTotalPrincipalAppliedAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TrialPeriodTotalPrincipalAppliedAmountSpecified
        {
            get { return TrialPeriodTotalPrincipalAppliedAmount != null; }
            set { }
        }

        /// <summary>
        /// The date on which the borrower signed the trial plan agreement.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMODate TrialPlanAgreementSignedDate;

        /// <summary>
        /// Gets or sets a value indicating whether the TrialPlanAgreementSignedDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TrialPlanAgreementSignedDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool TrialPlanAgreementSignedDateSpecified
        {
            get { return TrialPlanAgreementSignedDate != null; }
            set { }
        }

        /// <summary>
        /// The date on which the borrower was determined to be eligible for a trial plan and the servicer commitment was made.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMODate TrialPlanCommitmentDate;

        /// <summary>
        /// Gets or sets a value indicating whether the TrialPlanCommitmentDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TrialPlanCommitmentDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool TrialPlanCommitmentDateSpecified
        {
            get { return TrialPlanCommitmentDate != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of the estimated monthly payment that the borrower will have to make during the trial period in order to qualify for the workout.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOAmount TrialPlanEstimatedPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the TrialPlanEstimatedPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TrialPlanEstimatedPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TrialPlanEstimatedPaymentAmountSpecified
        {
            get { return TrialPlanEstimatedPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that the trial plan has an option to be extended.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOIndicator TrialPlanExtendableIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the TrialPlanExtendableIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TrialPlanExtendableIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool TrialPlanExtendableIndicatorSpecified
        {
            get { return TrialPlanExtendableIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that the trial plan has been extended.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOIndicator TrialPlanExtendedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the TrialPlanExtendedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TrialPlanExtendedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool TrialPlanExtendedIndicatorSpecified
        {
            get { return TrialPlanExtendedIndicator != null; }
            set { }
        }

        /// <summary>
        /// The number of months the trail plan was extended.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOCount TrialPlanExtendedMonthsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the TrialPlanExtendedMonthsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TrialPlanExtendedMonthsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TrialPlanExtendedMonthsCountSpecified
        {
            get { return TrialPlanExtendedMonthsCount != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that an additional payment is required for an extension of the trial.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOIndicator TrialPlanExtensionAdditionalPaymentRequiredIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the TrialPlanExtensionAdditionalPaymentRequiredIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TrialPlanExtensionAdditionalPaymentRequiredIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool TrialPlanExtensionAdditionalPaymentRequiredIndicatorSpecified
        {
            get { return TrialPlanExtensionAdditionalPaymentRequiredIndicator != null; }
            set { }
        }

        /// <summary>
        /// The date on which the first payment of the trial plan was received.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMODate TrialPlanFirstPaymentReceivedDate;

        /// <summary>
        /// Gets or sets a value indicating whether the TrialPlanFirstPaymentReceivedDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TrialPlanFirstPaymentReceivedDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool TrialPlanFirstPaymentReceivedDateSpecified
        {
            get { return TrialPlanFirstPaymentReceivedDate != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that verification for the income documentation was waived for the trial plan.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOIndicator TrialPlanIncomeVerificationWaivedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the TrialPlanIncomeVerificationWaivedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TrialPlanIncomeVerificationWaivedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool TrialPlanIncomeVerificationWaivedIndicatorSpecified
        {
            get { return TrialPlanIncomeVerificationWaivedIndicator != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of the interest portion of all contractual payments applied during the trial period.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOAmount TrialPlanInterestAppliedAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the TrialPlanInterestAppliedAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TrialPlanInterestAppliedAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TrialPlanInterestAppliedAmountSpecified
        {
            get { return TrialPlanInterestAppliedAmount != null; }
            set { }
        }

        /// <summary>
        /// Total dollar amount of all remaining trial plan payments expected to be collected from the borrower prior to the end of the trial period.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOAmount TrialPlanPaymentRemainingAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the TrialPlanPaymentRemainingAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TrialPlanPaymentRemainingAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TrialPlanPaymentRemainingAmountSpecified
        {
            get { return TrialPlanPaymentRemainingAmount != null; }
            set { }
        }

        /// <summary>
        /// The total number of payments that have been received by the servicer for the trial plan.
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMOCount TrialPlanPaymentsReceivedCount;

        /// <summary>
        /// Gets or sets a value indicating whether the TrialPlanPaymentsReceivedCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TrialPlanPaymentsReceivedCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TrialPlanPaymentsReceivedCountSpecified
        {
            get { return TrialPlanPaymentsReceivedCount != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of the regularly scheduled payment amount that the borrower must pay during the trial period in order to qualify for the workout.
        /// </summary>
        [XmlElement(Order = 17)]
        public MISMOAmount TrialPlanScheduledPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the TrialPlanScheduledPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TrialPlanScheduledPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TrialPlanScheduledPaymentAmountSpecified
        {
            get { return TrialPlanScheduledPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// The reason that the trial plan was unsuccessful.
        /// </summary>
        [XmlElement(Order = 18)]
        public MISMOEnum<TrialPlanUnsuccessfulReasonBase> TrialPlanUnsuccessfulReasonType;

        /// <summary>
        /// Gets or sets a value indicating whether the TrialPlanUnsuccessfulReasonType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TrialPlanUnsuccessfulReasonType element has been assigned a value.</value>
        [XmlIgnore]
        public bool TrialPlanUnsuccessfulReasonTypeSpecified
        {
            get { return this.TrialPlanUnsuccessfulReasonType != null && this.TrialPlanUnsuccessfulReasonType.enumValue != TrialPlanUnsuccessfulReasonBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture additional information when Other is selected for Trial Plan Unsuccessful Reason Type.
        /// </summary>
        [XmlElement(Order = 19)]
        public MISMOString TrialPlanUnsuccessfulReasonTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the TrialPlanUnsuccessfulReasonTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TrialPlanUnsuccessfulReasonTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool TrialPlanUnsuccessfulReasonTypeOtherDescriptionSpecified
        {
            get { return TrialPlanUnsuccessfulReasonTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 20)]
        public TRIAL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
