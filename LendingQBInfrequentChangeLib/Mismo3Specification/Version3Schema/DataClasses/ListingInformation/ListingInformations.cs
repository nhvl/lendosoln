namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class LISTING_INFORMATIONS
    {
        /// <summary>
        /// Gets a value indicating whether the LISTING_INFORMATIONS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ListingInformationSpecified;
            }
        }

        /// <summary>
        /// A collection of listing information.
        /// </summary>
        [XmlElement("LISTING_INFORMATION", Order = 0)]
		public List<LISTING_INFORMATION> ListingInformation = new List<LISTING_INFORMATION>();

        /// <summary>
        /// Gets or sets a value indicating whether the ListingInformation element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ListingInformation element has been assigned a value.</value>
        [XmlIgnore]
        public bool ListingInformationSpecified
        {
            get { return this.ListingInformation != null && this.ListingInformation.Count(l => l != null && l.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public LISTING_INFORMATIONS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
