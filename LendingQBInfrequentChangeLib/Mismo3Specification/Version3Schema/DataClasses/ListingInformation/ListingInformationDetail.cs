namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class LISTING_INFORMATION_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the LISTING_INFORMATION_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CumulativeDaysOnMarketCountSpecified
                    || this.CurrentListPriceAmountSpecified
                    || this.CurrentlyListedIndicatorSpecified
                    || this.DaysOnMarketCountSpecified
                    || this.ExtensionSpecified
                    || this.FinalListPriceAmountSpecified
                    || this.ForSaleByOwnerIndicatorSpecified
                    || this.LastListPriceRevisionDateSpecified
                    || this.ListedWithinPreviousYearDescriptionSpecified
                    || this.ListedWithinPreviousYearIndicatorSpecified
                    || this.ListingStatusDateSpecified
                    || this.ListingStatusTypeOtherDescriptionSpecified
                    || this.ListingStatusTypeSpecified
                    || this.MLSNumberIdentifierSpecified
                    || this.PropertyForecastSalePriceAmountSpecified
                    || this.PropertyTypicalMarketingDaysDurationTypeSpecified
                    || this.ShortSaleOfferingTypeOtherDescriptionSpecified
                    || this.ShortSaleOfferingTypeSpecified;
            }
        }

        /// <summary>
        /// The total number of days a property is on the market unsold - inclusive of all listing status change dates, from the original listing date to time the listing is selected for the valuation report. 
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOCount CumulativeDaysOnMarketCount;

        /// <summary>
        /// Gets or sets a value indicating whether the CumulativeDaysOnMarketCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CumulativeDaysOnMarketCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool CumulativeDaysOnMarketCountSpecified
        {
            get { return CumulativeDaysOnMarketCount != null; }
            set { }
        }

        /// <summary>
        /// Current list price (e.g. of the property).
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOAmount CurrentListPriceAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the CurrentListPriceAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CurrentListPriceAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool CurrentListPriceAmountSpecified
        {
            get { return CurrentListPriceAmount != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the property is currently listed for sale in MLS.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOIndicator CurrentlyListedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the CurrentlyListedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CurrentlyListedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool CurrentlyListedIndicatorSpecified
        {
            get { return CurrentlyListedIndicator != null; }
            set { }
        }

        /// <summary>
        /// The total number of days on the property has been on the market from the last material status date to the time listing selected for the valuation.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOCount DaysOnMarketCount;

        /// <summary>
        /// Gets or sets a value indicating whether the DaysOnMarketCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DaysOnMarketCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool DaysOnMarketCountSpecified
        {
            get { return DaysOnMarketCount != null; }
            set { }
        }

        /// <summary>
        /// The final list price of the property.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOAmount FinalListPriceAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the FinalListPriceAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FinalListPriceAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool FinalListPriceAmountSpecified
        {
            get { return FinalListPriceAmount != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that the property is for sale by owner.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOIndicator ForSaleByOwnerIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ForSaleByOwnerIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ForSaleByOwnerIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ForSaleByOwnerIndicatorSpecified
        {
            get { return ForSaleByOwnerIndicator != null; }
            set { }
        }

        /// <summary>
        /// The most recent date when the property list price was revised.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMODate LastListPriceRevisionDate;

        /// <summary>
        /// Gets or sets a value indicating whether the LastListPriceRevisionDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LastListPriceRevisionDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool LastListPriceRevisionDateSpecified
        {
            get { return LastListPriceRevisionDate != null; }
            set { }
        }

        /// <summary>
        /// A free-form text describing the offering prices, dates, and data sources of the previous twelve (12) months of listing.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOString ListedWithinPreviousYearDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ListedWithinPreviousYearDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ListedWithinPreviousYearDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ListedWithinPreviousYearDescriptionSpecified
        {
            get { return ListedWithinPreviousYearDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the property has been listed within the previous twelve (12) months.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOIndicator ListedWithinPreviousYearIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ListedWithinPreviousYearIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ListedWithinPreviousYearIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ListedWithinPreviousYearIndicatorSpecified
        {
            get { return ListedWithinPreviousYearIndicator != null; }
            set { }
        }

        /// <summary>
        /// The disposition date related to the Listing Status Type.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMODate ListingStatusDate;

        /// <summary>
        /// Gets or sets a value indicating whether the ListingStatusDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ListingStatusDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ListingStatusDateSpecified
        {
            get { return ListingStatusDate != null; }
            set { }
        }

        /// <summary>
        /// The disposition of the listing.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOEnum<ListingStatusBase> ListingStatusType;

        /// <summary>
        /// Gets or sets a value indicating whether the ListingStatusType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ListingStatusType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ListingStatusTypeSpecified
        {
            get { return this.ListingStatusType != null && this.ListingStatusType.enumValue != ListingStatusBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for the ListingStatusType.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOString ListingStatusTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ListingStatusTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ListingStatusTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ListingStatusTypeOtherDescriptionSpecified
        {
            get { return ListingStatusTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Identifies the record number in the local MLS from which the listing was obtained.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOIdentifier MLSNumberIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the MLSNumberIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MLSNumberIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool MLSNumberIdentifierSpecified
        {
            get { return MLSNumberIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The dollar value of the forecast sales price of the subject property.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOAmount PropertyForecastSalePriceAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyForecastSalePriceAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyForecastSalePriceAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyForecastSalePriceAmountSpecified
        {
            get { return PropertyForecastSalePriceAmount != null; }
            set { }
        }

        /// <summary>
        /// Specifies the estimated marketing duration in terms of days for the subject property before it is sold. The use of days to indicate the marketing duration is specific to the Employee Relocation Appraisal Forms.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOEnum<PropertyTypicalMarketingDaysDurationBase> PropertyTypicalMarketingDaysDurationType;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyTypicalMarketingDaysDurationType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyTypicalMarketingDaysDurationType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyTypicalMarketingDaysDurationTypeSpecified
        {
            get { return this.PropertyTypicalMarketingDaysDurationType != null && this.PropertyTypicalMarketingDaysDurationType.enumValue != PropertyTypicalMarketingDaysDurationBase.Blank; }
            set { }
        }

        /// <summary>
        /// Describes type of short sale related to a property.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOEnum<ShortSaleOfferingBase> ShortSaleOfferingType;

        /// <summary>
        /// Gets or sets a value indicating whether the ShortSaleOfferingType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ShortSaleOfferingType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ShortSaleOfferingTypeSpecified
        {
            get { return this.ShortSaleOfferingType != null && this.ShortSaleOfferingType.enumValue != ShortSaleOfferingBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Short Sale Offering Type.
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMOString ShortSaleOfferingTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ShortSaleOfferingTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ShortSaleOfferingTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ShortSaleOfferingTypeOtherDescriptionSpecified
        {
            get { return ShortSaleOfferingTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 17)]
        public LISTING_INFORMATION_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
