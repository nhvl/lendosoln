namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CLOSING_AGENT
    {
        /// <summary>
        /// Gets a value indicating whether the CLOSING_AGENT container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ClosingAgentTypeOtherDescriptionSpecified
                    || this.ClosingAgentTypeSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// This designates the type of the closing agent.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<ClosingAgentBase> ClosingAgentType;

        /// <summary>
        /// Gets or sets a value indicating whether the ClosingAgentType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ClosingAgentType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ClosingAgentTypeSpecified
        {
            get { return this.ClosingAgentType != null && this.ClosingAgentType.enumValue != ClosingAgentBase.Blank; }
            set { }
        }

        /// <summary>
        /// When Other is specified in Closing Agent Type, this data element contains the description.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString ClosingAgentTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ClosingAgentTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ClosingAgentTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ClosingAgentTypeOtherDescriptionSpecified
        {
            get { return ClosingAgentTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public CLOSING_AGENT_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
