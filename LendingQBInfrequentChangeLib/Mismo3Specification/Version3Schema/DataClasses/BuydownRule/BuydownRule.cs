namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class BUYDOWN_RULE
    {
        /// <summary>
        /// Gets a value indicating whether the BUYDOWN_RULE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BuydownBaseDateTypeOtherDescriptionSpecified
                    || this.BuydownBaseDateTypeSpecified
                    || this.BuydownCalculationTypeSpecified
                    || this.BuydownChangeFrequencyMonthsCountSpecified
                    || this.BuydownDurationMonthsCountSpecified
                    || this.BuydownIncreaseRatePercentSpecified
                    || this.BuydownInitialDiscountPercentSpecified
                    || this.BuydownLenderFundingIndicatorSpecified
                    || this.BuydownPayoffDisbursementTypeSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// The date from which the duration of the buy down is counted.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<BuydownBaseDateBase> BuydownBaseDateType;

        /// <summary>
        /// Gets or sets a value indicating whether the BuyDownBaseDateType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BuyDownBaseDateType element has been assigned a value.</value>
        [XmlIgnore]
        public bool BuydownBaseDateTypeSpecified
        {
            get { return this.BuydownBaseDateType != null && this.BuydownBaseDateType.enumValue != BuydownBaseDateBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Buy down Base Date Type.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString BuydownBaseDateTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the BuyDownBaseDateTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BuyDownBaseDateTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool BuydownBaseDateTypeOtherDescriptionSpecified
        {
            get { return BuydownBaseDateTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The value to be used to base buy down calculations. Original loan amount or declining loan amount.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<BuydownCalculationBase> BuydownCalculationType;

        /// <summary>
        /// Gets or sets a value indicating whether the BuyDownCalculationType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BuyDownCalculationType element has been assigned a value.</value>
        [XmlIgnore]
        public bool BuydownCalculationTypeSpecified
        {
            get { return this.BuydownCalculationType != null && this.BuydownCalculationType.enumValue != BuydownCalculationBase.Blank; }
            set { }
        }

        /// <summary>
        /// The time interval in months between interest rate increases during the buy down period. For example if the interest rate increases annually during a two year buy down, the frequency of interest rate change is 12 months.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOCount BuydownChangeFrequencyMonthsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the BuyDownChangeFrequencyMonthsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BuyDownChangeFrequencyMonthsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool BuydownChangeFrequencyMonthsCountSpecified
        {
            get { return BuydownChangeFrequencyMonthsCount != null; }
            set { }
        }

        /// <summary>
        /// The total number of months during which any buy down is in effect. This represents the accumulation of all of the buy down periods.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOCount BuydownDurationMonthsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the BuyDownDurationMonthsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BuyDownDurationMonthsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool BuydownDurationMonthsCountSpecified
        {
            get { return BuydownDurationMonthsCount != null; }
            set { }
        }

        /// <summary>
        /// The amount by which the interest rate can increase at each adjustment period within the buy down duration.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOPercent BuydownIncreaseRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the BuyDownIncreaseRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BuyDownIncreaseRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool BuydownIncreaseRatePercentSpecified
        {
            get { return BuydownIncreaseRatePercent != null; }
            set { }
        }

        /// <summary>
        /// The percent by which the interest rate was bought down at origination. For example, for a 3-2-1 buy down this would be 3.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOPercent BuydownInitialDiscountPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the BuyDownInitialDiscountPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BuyDownInitialDiscountPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool BuydownInitialDiscountPercentSpecified
        {
            get { return BuydownInitialDiscountPercent != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether or not the lender is funding the interest rate buy down.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOIndicator BuydownLenderFundingIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the BuyDownLenderFundingIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BuyDownLenderFundingIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool BuydownLenderFundingIndicatorSpecified
        {
            get { return BuydownLenderFundingIndicator != null; }
            set { }
        }

        /// <summary>
        /// Describes how the remaining buy down funds are disbursed at time of payoff.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOEnum<BuydownPayoffDisbursementBase> BuydownPayoffDisbursementType;

        /// <summary>
        /// Gets or sets a value indicating whether the BuyDownPayoffDisbursementType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BuyDownPayoffDisbursementType element has been assigned a value.</value>
        [XmlIgnore]
        public bool BuydownPayoffDisbursementTypeSpecified
        {
            get { return this.BuydownPayoffDisbursementType != null && this.BuydownPayoffDisbursementType.enumValue != BuydownPayoffDisbursementBase.Blank; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 9)]
        public BUYDOWN_RULE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
