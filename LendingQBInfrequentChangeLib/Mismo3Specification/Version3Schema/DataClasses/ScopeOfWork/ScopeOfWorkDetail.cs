namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class SCOPE_OF_WORK_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the SCOPE_OF_WORK_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.PropertyInspectionRequestCommentDescriptionSpecified
                    || this.PropertyInspectionResultCommentDescriptionSpecified
                    || this.PropertyInspectionTypeOtherDescriptionSpecified
                    || this.PropertyInspectionTypeSpecified
                    || this.PropertyValuationServiceTypeOtherDescriptionSpecified
                    || this.PropertyValuationServiceTypeSpecified
                    || this.USPAPReportDescriptionSpecified
                    || this.ValuationAssignmentTypeOtherDescriptionSpecified
                    || this.ValuationAssignmentTypeSpecified
                    || this.ValuationScopeOfWorkDescriptionSpecified;
            }
        }

        /// <summary>
        /// Free form commentary about the request for the inspection.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString PropertyInspectionRequestCommentDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyInspectionRequestCommentDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyInspectionRequestCommentDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyInspectionRequestCommentDescriptionSpecified
        {
            get { return PropertyInspectionRequestCommentDescription != null; }
            set { }
        }

        /// <summary>
        /// Free form commentary about the results of the inspection.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString PropertyInspectionResultCommentDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyInspectionResultCommentDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyInspectionResultCommentDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyInspectionResultCommentDescriptionSpecified
        {
            get { return PropertyInspectionResultCommentDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the extent of the inspection.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<PropertyInspectionBase> PropertyInspectionType;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyInspectionType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyInspectionType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyInspectionTypeSpecified
        {
            get { return this.PropertyInspectionType != null && this.PropertyInspectionType.enumValue != PropertyInspectionBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form field used to capture additional information when the selection for Property Inspection Type is Other.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString PropertyInspectionTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyInspectionTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyInspectionTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyInspectionTypeOtherDescriptionSpecified
        {
            get { return PropertyInspectionTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Provides a categorization for valuation products in terms of USPAP compliance and level of professional services required.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<PropertyValuationServiceBase> PropertyValuationServiceType;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyValuationServiceType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyValuationServiceType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyValuationServiceTypeSpecified
        {
            get { return this.PropertyValuationServiceType != null && this.PropertyValuationServiceType.enumValue != PropertyValuationServiceBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field which contains the description of the categorization for valuation products when Other is selected as the Property Valuation Service Type. 
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString PropertyValuationServiceTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyValuationServiceTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyValuationServiceTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyValuationServiceTypeOtherDescriptionSpecified
        {
            get { return PropertyValuationServiceTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the type of the appraisal report according to the Uniform Standard of Professional Appraisal Practice for appraisal report categories.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOString USPAPReportDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the USPAPReportDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the USPAPReportDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool USPAPReportDescriptionSpecified
        {
            get { return USPAPReportDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the business reason that motivated the order for a valuation.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOEnum<ValuationAssignmentBase> ValuationAssignmentType;

        /// <summary>
        /// Gets or sets a value indicating whether the ValuationAssignmentType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ValuationAssignmentType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ValuationAssignmentTypeSpecified
        {
            get { return this.ValuationAssignmentType != null && this.ValuationAssignmentType.enumValue != ValuationAssignmentBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when other is selected for Valuation Assignment Type.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOString ValuationAssignmentTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ValuationAssignmentTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ValuationAssignmentTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ValuationAssignmentTypeOtherDescriptionSpecified
        {
            get { return ValuationAssignmentTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Describes the scope of work identified as necessary by the appraiser to determine a valuation with respect to its intended use and users.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOString ValuationScopeOfWorkDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ValuationScopeOfWorkDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ValuationScopeOfWorkDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ValuationScopeOfWorkDescriptionSpecified
        {
            get { return ValuationScopeOfWorkDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 10)]
        public SCOPE_OF_WORK_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
