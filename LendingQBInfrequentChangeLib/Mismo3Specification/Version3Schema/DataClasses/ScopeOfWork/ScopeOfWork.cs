namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class SCOPE_OF_WORK
    {
        /// <summary>
        /// Gets a value indicating whether the SCOPE_OF_WORK container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.EncumbrancesSpecified
                    || this.ExtensionSpecified
                    || this.RealPropertyInterestsSpecified
                    || this.ScopeOfWorkDetailSpecified
                    || this.ValuationIntendedUsersSpecified
                    || this.ValuationIntendedUsesSpecified
                    || this.ValueDefinitionsSpecified;
            }
        }

        /// <summary>
        /// Encumbrances on the scope of work.
        /// </summary>
        [XmlElement("ENCUMBRANCES", Order = 0)]
        public ENCUMBRANCES Encumbrances;

        /// <summary>
        /// Gets or sets a value indicating whether the Encumbrances element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Encumbrances element has been assigned a value.</value>
        [XmlIgnore]
        public bool EncumbrancesSpecified
        {
            get { return this.Encumbrances != null && this.Encumbrances.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Real estate interests.
        /// </summary>
        [XmlElement("REAL_PROPERTY_INTERESTS", Order = 1)]
        public REAL_PROPERTY_INTERESTS RealPropertyInterests;

        /// <summary>
        /// Gets or sets a value indicating whether the RealPropertyInterests element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RealPropertyInterests element has been assigned a value.</value>
        [XmlIgnore]
        public bool RealPropertyInterestsSpecified
        {
            get { return this.RealPropertyInterests != null && this.RealPropertyInterests.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details of the scope of work.
        /// </summary>
        [XmlElement("SCOPE_OF_WORK_DETAIL", Order = 2)]
        public SCOPE_OF_WORK_DETAIL ScopeOfWorkDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the ScopeOfWorkDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ScopeOfWorkDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool ScopeOfWorkDetailSpecified
        {
            get { return this.ScopeOfWorkDetail != null && this.ScopeOfWorkDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A list of valuation intended users.
        /// </summary>
        [XmlElement("VALUATION_INTENDED_USERS", Order = 3)]
        public VALUATION_INTENDED_USERS ValuationIntendedUsers;

        /// <summary>
        /// Gets or sets a value indicating whether the ValuationIntendedUsers element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ValuationIntendedUsers element has been assigned a value.</value>
        [XmlIgnore]
        public bool ValuationIntendedUsersSpecified
        {
            get { return this.ValuationIntendedUsers != null && this.ValuationIntendedUsers.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A list of valuation intended uses.
        /// </summary>
        [XmlElement("VALUATION_INTENDED_USES", Order = 4)]
        public VALUATION_INTENDED_USES ValuationIntendedUses;

        /// <summary>
        /// Gets or sets a value indicating whether the ValuationIntendedUses element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ValuationIntendedUses element has been assigned a value.</value>
        [XmlIgnore]
        public bool ValuationIntendedUsesSpecified
        {
            get { return this.ValuationIntendedUses != null && this.ValuationIntendedUses.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A list of value definitions.
        /// </summary>
        [XmlElement("VALUE_DEFINITIONS", Order = 5)]
        public VALUE_DEFINITIONS ValueDefinitions;

        /// <summary>
        /// Gets or sets a value indicating whether the ValueDefinitions element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ValueDefinitions element has been assigned a value.</value>
        [XmlIgnore]
        public bool ValueDefinitionsSpecified
        {
            get { return this.ValueDefinitions != null && this.ValueDefinitions.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 6)]
        public SCOPE_OF_WORK_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
