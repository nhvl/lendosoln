namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class CONTRIBUTION
    {
        /// <summary>
        /// Gets a value indicating whether the CONTRIBUTION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ActualContributionAmountSpecified
                    || this.ContributionSourceTypeOtherDescriptionSpecified
                    || this.ContributionSourceTypeSpecified
                    || this.ContributionTypeOtherDescriptionSpecified
                    || this.ContributionTypeSpecified
                    || this.EstimatedContributionAmountSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// The actual dollar amount of the contribution.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount ActualContributionAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ActualContributionAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ActualContributionAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ActualContributionAmountSpecified
        {
            get { return ActualContributionAmount != null; }
            set { }
        }

        /// <summary>
        /// The type of party that is providing the contribution associated with a workout.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<ContributionSourceBase> ContributionSourceType;

        /// <summary>
        /// Gets or sets a value indicating whether the ContributionSourceType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ContributionSourceType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ContributionSourceTypeSpecified
        {
            get { return this.ContributionSourceType != null && this.ContributionSourceType.enumValue != ContributionSourceBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture additional information when Other is selected for Contribution Source Type.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString ContributionSourceTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ContributionSourceTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ContributionSourceTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ContributionSourceTypeOtherDescriptionSpecified
        {
            get { return ContributionSourceTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The type of contribution associated with a workout.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOEnum<ContributionBase> ContributionType;

        /// <summary>
        /// Gets or sets a value indicating whether the ContributionType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ContributionType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ContributionTypeSpecified
        {
            get { return this.ContributionType != null && this.ContributionType.enumValue != ContributionBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture additional information when Other is selected for Contribution Type.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString ContributionTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ContributionTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ContributionTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ContributionTypeOtherDescriptionSpecified
        {
            get { return ContributionTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The estimated dollar amount of the contribution.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOAmount EstimatedContributionAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the EstimatedContributionAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EstimatedContributionAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool EstimatedContributionAmountSpecified
        {
            get { return EstimatedContributionAmount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 6)]
        public CONTRIBUTION_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
