namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CONTRIBUTIONS
    {
        /// <summary>
        /// Gets a value indicating whether the CONTRIBUTIONS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ContributionSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of contributions.
        /// </summary>
        [XmlElement("CONTRIBUTION", Order = 0)]
		public List<CONTRIBUTION> Contribution = new List<CONTRIBUTION>();

        /// <summary>
        /// Gets or sets a value indicating whether the Contribution element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Contribution element has been assigned a value.</value>
        [XmlIgnore]
        public bool ContributionSpecified
        {
            get { return this.Contribution != null && this.Contribution.Count(c => c != null && c.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public CONTRIBUTIONS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
