namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class ADDRESS
    {
        /// <summary>
        /// Gets a value indicating whether the ADDRESS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AddressAdditionalLineTextSpecified
                    || this.AddressLineTextSpecified
                    || this.AddressTypeOtherDescriptionSpecified
                    || this.AddressTypeSpecified
                    || this.AddressUnitDesignatorTypeOtherDescriptionSpecified
                    || this.AddressUnitDesignatorTypeSpecified
                    || this.AddressUnitIdentifierSpecified
                    || this.CarrierRouteCodeSpecified
                    || this.CityNameSpecified
                    || this.CountryCodeSpecified
                    || this.CountryNameSpecified
                    || this.CountyCodeSpecified
                    || this.CountyNameSpecified
                    || this.DeliveryPointBarCodeCheckValueSpecified
                    || this.DeliveryPointBarCodeValueSpecified
                    || this.ExtensionSpecified
                    || this.MailStopCodeSpecified
                    || this.PlusFourZipCodeSpecified
                    || this.PostalCodeSpecified
                    || this.PostOfficeBoxIdentifierSpecified
                    || this.RuralRouteIdentifierSpecified
                    || this.StateCodeSpecified
                    || this.StateNameSpecified
                    || this.StreetNameSpecified
                    || this.StreetPostDirectionalTextSpecified
                    || this.StreetPreDirectionalTextSpecified
                    || this.StreetPrimaryNumberTextSpecified
                    || this.StreetSuffixTextSpecified;
            }
        }

        /// <summary>
        /// Address information that cannot be contained in the Address Line Text.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString AddressAdditionalLineText;

        /// <summary>
        /// Gets or sets a value indicating whether the AddressAdditionalLineText element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AddressAdditionalLineText element has been assigned a value.</value>
        [XmlIgnore]
        public bool AddressAdditionalLineTextSpecified
        {
            get { return AddressAdditionalLineText != null; }
            set { }
        }

        /// <summary>
        /// The address with the address number, pre-directional, street name, post-directional, address unit designators and address unit value.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString AddressLineText;

        /// <summary>
        /// Gets or sets a value indicating whether the AddressLineText element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AddressLineText element has been assigned a value.</value>
        [XmlIgnore]
        public bool AddressLineTextSpecified
        {
            get { return AddressLineText != null; }
            set { }
        }

        /// <summary>
        /// Specifies the type of address.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<AddressBase> AddressType;

        /// <summary>
        /// Gets or sets a value indicating whether the AddressType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AddressType element has been assigned a value.</value>
        [XmlIgnore]
        public bool AddressTypeSpecified
        {
            get { return this.AddressType != null && this.AddressType.enumValue != AddressBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Address Type.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString AddressTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the AddressTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AddressTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool AddressTypeOtherDescriptionSpecified
        {
            get { return AddressTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// An additional address designation that further defines the delivery location. Example: Apartment, Building, Condo, Suite, Room, Mail Stop, Unit, etc.  This list is based on the USPS Publication 28 on Postal Addressing Standards with the addition of Condo based on mortgage industry need.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<AddressUnitDesignatorBase> AddressUnitDesignatorType;

        /// <summary>
        /// Gets or sets a value indicating whether the AddressUnitDesignatorType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AddressUnitDesignatorType element has been assigned a value.</value>
        [XmlIgnore]
        public bool AddressUnitDesignatorTypeSpecified
        {
            get { return this.AddressUnitDesignatorType != null && this.AddressUnitDesignatorType.enumValue != AddressUnitDesignatorBase.Blank; }
            set { }
        }

        /// <summary>
        /// The description of the Address Unit Designator Type when Other is selected as the option from the enumerated list.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString AddressUnitDesignatorTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the AddressUnitDesignatorTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AddressUnitDesignatorTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool AddressUnitDesignatorTypeOtherDescriptionSpecified
        {
            get { return AddressUnitDesignatorTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The identifier value associated with the Secondary Address Unit Designator. Example: 123, C, B1C, etc.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOIdentifier AddressUnitIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the AddressUnitIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AddressUnitIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool AddressUnitIdentifierSpecified
        {
            get { return AddressUnitIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A postal carrier route code is a part of the complete carrier route. The carrier route code is 4 characters consisting of 1 letter for the type of carrier route  and 3 digits for the carrier route. The complete carrier route identifies a group of addresses to which the USPS assigns the same code to aid in mail delivery.  The complete Carrier Route consists of 9 digits – 5 numbers for the ZIP Code and the 4 characters for the carrier route code. 
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOCode CarrierRouteCode;

        /// <summary>
        /// Gets or sets a value indicating whether the CarrierRouteCode element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CarrierRouteCode element has been assigned a value.</value>
        [XmlIgnore]
        public bool CarrierRouteCodeSpecified
        {
            get { return CarrierRouteCode != null; }
            set { }
        }

        /// <summary>
        /// The name of the city.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOString CityName;

        /// <summary>
        /// Gets or sets a value indicating whether the CityName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CityName element has been assigned a value.</value>
        [XmlIgnore]
        public bool CityNameSpecified
        {
            get { return CityName != null; }
            set { }
        }

        /// <summary>
        /// The two-character representation of the country.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOCode CountryCode;

        /// <summary>
        /// Gets or sets a value indicating whether the CountryCode element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CountryCode element has been assigned a value.</value>
        [XmlIgnore]
        public bool CountryCodeSpecified
        {
            get { return CountryCode != null; }
            set { }
        }

        /// <summary>
        /// The name of the country.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOString CountryName;

        /// <summary>
        /// Gets or sets a value indicating whether the CountryName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CountryName element has been assigned a value.</value>
        [XmlIgnore]
        public bool CountryNameSpecified
        {
            get { return CountryName != null; }
            set { }
        }

        /// <summary>
        /// Code identifying the county within a state. (Designator Code based on FIPS Publication 6-4).
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOCode CountyCode;

        /// <summary>
        /// Gets or sets a value indicating whether the CountyCode element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CountyCode element has been assigned a value.</value>
        [XmlIgnore]
        public bool CountyCodeSpecified
        {
            get { return CountyCode != null; }
            set { }
        }

        /// <summary>
        /// The name of the county within a state. (Designator Name based on FIPS Publication 6-4).
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOString CountyName;

        /// <summary>
        /// Gets or sets a value indicating whether the CountyName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CountyName element has been assigned a value.</value>
        [XmlIgnore]
        public bool CountyNameSpecified
        {
            get { return CountyName != null; }
            set { }
        }

        /// <summary>
        /// A check digit calculated using the MOD 10 method on the total of all 10 digits that make up the bar code for use in the Delivery Point Bar Code (DPBC). The DPBC is an 11 digit (plus check digit) barcode, containing the ZIP Code, ZIP+4 Code, and the delivery point code. This is the predominant barcode in use currently (as of 2008), and it enables the Postal Service to sort mail into delivery point (address) sequence.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOValue DeliveryPointBarCodeCheckValue;

        /// <summary>
        /// Gets or sets a value indicating whether the DeliveryPointBarCodeCheckValue element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DeliveryPointBarCodeCheckValue element has been assigned a value.</value>
        [XmlIgnore]
        public bool DeliveryPointBarCodeCheckValueSpecified
        {
            get { return DeliveryPointBarCodeCheckValue != null; }
            set { }
        }

        /// <summary>
        /// The barcode number of the delivery point.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOValue DeliveryPointBarCodeValue;

        /// <summary>
        /// Gets or sets a value indicating whether the DeliveryPointBarCodeValue element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DeliveryPointBarCodeValue element has been assigned a value.</value>
        [XmlIgnore]
        public bool DeliveryPointBarCodeValueSpecified
        {
            get { return DeliveryPointBarCodeValue != null; }
            set { }
        }

        /// <summary>
        /// Uniquely identifies an individual or office within a private company for the purposes of internal mail distribution. USPS calls this a MailStop Code (MSC).
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOCode MailStopCode;

        /// <summary>
        /// Gets or sets a value indicating whether the MailStopCode element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MailStopCode element has been assigned a value.</value>
        [XmlIgnore]
        public bool MailStopCodeSpecified
        {
            get { return MailStopCode != null; }
            set { }
        }

        /// <summary>
        /// This is the additional four digits in the code.
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMOCode PlusFourZipCode;

        /// <summary>
        /// Gets or sets a value indicating whether the PlusFourZipCode element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PlusFourZipCode element has been assigned a value.</value>
        [XmlIgnore]
        public bool PlusFourZipCodeSpecified
        {
            get { return PlusFourZipCode != null; }
            set { }
        }

        /// <summary>
        /// The postal code (ZIP Code in the US) for the address. ZIP Code may be either 5 or 9 digits.
        /// </summary>
        [XmlElement(Order = 17)]
        public MISMOCode PostalCode;

        /// <summary>
        /// Gets or sets a value indicating whether the PostalCode element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PostalCode element has been assigned a value.</value>
        [XmlIgnore]
        public bool PostalCodeSpecified
        {
            get { return PostalCode != null; }
            set { }
        }

        /// <summary>
        /// The identifier of the locked box located in the post office lobby or other authorized place that customers may rent for delivery of mail.
        /// </summary>
        [XmlElement(Order = 18)]
        public MISMOIdentifier PostOfficeBoxIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the PostOfficeBoxIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PostOfficeBoxIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool PostOfficeBoxIdentifierSpecified
        {
            get { return PostOfficeBoxIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A unique alphanumeric string identifying the rural route for the delivery of mail, as assigned by the USPS.
        /// </summary>
        [XmlElement(Order = 19)]
        public MISMOIdentifier RuralRouteIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the RuralRouteIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RuralRouteIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool RuralRouteIdentifierSpecified
        {
            get { return RuralRouteIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The two-character representation of the US state, US Territory, Canadian Province, Military APO FPO, or Territory.
        /// </summary>
        [XmlElement(Order = 20)]
        public MISMOCode StateCode;

        /// <summary>
        /// Gets or sets a value indicating whether the StateCode element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the StateCode element has been assigned a value.</value>
        [XmlIgnore]
        public bool StateCodeSpecified
        {
            get { return StateCode != null; }
            set { }
        }

        /// <summary>
        /// The name of the US state, US Territory, Canadian Province, Military APO FPO, or Territory.
        /// </summary>
        [XmlElement(Order = 21)]
        public MISMOString StateName;

        /// <summary>
        /// Gets or sets a value indicating whether the StateName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the StateName element has been assigned a value.</value>
        [XmlIgnore]
        public bool StateNameSpecified
        {
            get { return StateName != null; }
            set { }
        }

        /// <summary>
        /// The official name of a street assigned by a local governing authority.
        /// </summary>
        [XmlElement(Order = 22)]
        public MISMOString StreetName;

        /// <summary>
        /// Gets or sets a value indicating whether the StreetName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the StreetName element has been assigned a value.</value>
        [XmlIgnore]
        public bool StreetNameSpecified
        {
            get { return StreetName != null; }
            set { }
        }

        /// <summary>
        /// The directional symbol that represents the sector of a city where a street address is located. Example: E, W, N, S, NE, NW, SE, SW.
        /// </summary>
        [XmlElement(Order = 23)]
        public MISMOString StreetPostDirectionalText;

        /// <summary>
        /// Gets or sets a value indicating whether the StreetPostDirectionalText element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the StreetPostDirectionalText element has been assigned a value.</value>
        [XmlIgnore]
        public bool StreetPostDirectionalTextSpecified
        {
            get { return StreetPostDirectionalText != null; }
            set { }
        }

        /// <summary>
        /// The street vector or the direction the street has taken from some arbitrary starting point. Example: E, W, N, S, NE, NW, SE, SW.
        /// </summary>
        [XmlElement(Order = 24)]
        public MISMOString StreetPreDirectionalText;

        /// <summary>
        /// Gets or sets a value indicating whether the StreetPreDirectionalText element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the StreetPreDirectionalText element has been assigned a value.</value>
        [XmlIgnore]
        public bool StreetPreDirectionalTextSpecified
        {
            get { return StreetPreDirectionalText != null; }
            set { }
        }

        /// <summary>
        /// The number assigned to a building or land parcel along the street to identify location and ensure accurate mail delivery.
        /// </summary>
        [XmlElement(Order = 25)]
        public MISMOString StreetPrimaryNumberText;

        /// <summary>
        /// Gets or sets a value indicating whether the StreetPrimaryNumberText element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the StreetPrimaryNumberText element has been assigned a value.</value>
        [XmlIgnore]
        public bool StreetPrimaryNumberTextSpecified
        {
            get { return StreetPrimaryNumberText != null; }
            set { }
        }

        /// <summary>
        /// The trailing designator in a street address or the appropriate abbreviation. Example: Drive, Way, Court, Street, etc.
        /// </summary>
        [XmlElement(Order = 26)]
        public MISMOString StreetSuffixText;

        /// <summary>
        /// Gets or sets a value indicating whether the StreetSuffixText element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the StreetSuffixText element has been assigned a value.</value>
        [XmlIgnore]
        public bool StreetSuffixTextSpecified
        {
            get { return StreetSuffixText != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 27)]
        public ADDRESS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
