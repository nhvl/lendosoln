namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class ADDRESS_3_3_1
    {
        /// <summary>
        /// Gets a value indicating whether the ADDRESS_3_3_1 container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AddressFormatTypeOtherDescriptionSpecified
                    || this.AddressFormatTypeSpecified
                    || this.AttentionToNameSpecified
                    || this.ExtensionSpecified
                    || this.HighwayContractRouteIdentifierSpecified
                    || this.RuralRouteBoxIdentifierSpecified;
            }
        }

        /// <summary>
        /// A collection of values that specify an address format as defined by the USPS Publication 28 Appendix A2 Address Types.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<AddressFormatBase> AddressFormatType;

        /// <summary>
        /// Gets or sets a value indicating whether the AddressFormatType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AddressFormatType element has been assigned a value.</value>
        [XmlIgnore]
        public bool AddressFormatTypeSpecified
        {
            get { return this.AddressFormatType != null && this.AddressFormatType.enumValue != AddressFormatBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field that specifies the enumeration when the value of Other is used for the Address Format Type.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString AddressFormatTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the AddressFormatTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AddressFormatTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool AddressFormatTypeOtherDescriptionSpecified
        {
            get { return AddressFormatTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A label that identifies the person or department to whose attention the mailing is sent.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString AttentionToName;

        /// <summary>
        /// Gets or sets a value indicating whether the AttentionToName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AttentionToName element has been assigned a value.</value>
        [XmlIgnore]
        public bool AttentionToNameSpecified
        {
            get { return AttentionToName != null; }
            set { }
        }

        /// <summary>
        /// A unique alphanumeric string identifying the highway contract route for the delivery of mail, as assigned by the USPS.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOIdentifier HighwayContractRouteIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the HighwayContractRouteIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HighwayContractRouteIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool HighwayContractRouteIdentifierSpecified
        {
            get { return HighwayContractRouteIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A unique alphanumeric string identifying the box located on a rural route for the delivery of mail, as assigned by the USPS.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOIdentifier RuralRouteBoxIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the RuralRouteBoxIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RuralRouteBoxIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool RuralRouteBoxIdentifierSpecified
        {
            get { return RuralRouteBoxIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 5)]
        public ADDRESS_3_3_1_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
