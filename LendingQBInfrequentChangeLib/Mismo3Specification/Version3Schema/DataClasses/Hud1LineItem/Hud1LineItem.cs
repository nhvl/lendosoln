namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class HUD1_LINE_ITEM
    {
        /// <summary>
        /// Gets a value indicating whether the HUD1_LINE_ITEM container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.HUD1LineItemAdditionalTextDescriptionSpecified
                    || this.HUD1LineItemAmountSpecified
                    || this.HUD1LineItemDescriptionSpecified
                    || this.HUD1LineItemFromDateSpecified
                    || this.HUD1LineItemPaidByTypeSpecified
                    || this.HUD1LineItemToDateSpecified
                    || this.HUD1SpecifiedLineNumberValueSpecified;
            }
        }

        /// <summary>
        /// A free-form text field used to provide a supplemental comment or remark to an individual HUD1 Line Item. This text is in addition to the HUD1 Line Item Description.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString HUD1LineItemAdditionalTextDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the HUD1LineItemAdditionalTextDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HUD1LineItemAdditionalTextDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool HUD1LineItemAdditionalTextDescriptionSpecified
        {
            get { return HUD1LineItemAdditionalTextDescription != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount for the specified HUD-1 line item.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOAmount HUD1LineItemAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the HUD1LineItemAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HUD1LineItemAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool HUD1LineItemAmountSpecified
        {
            get { return HUD1LineItemAmount != null; }
            set { }
        }

        /// <summary>
        /// The description of the specified HUD-1 line item.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString HUD1LineItemDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the HUD1LineItemDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HUD1LineItemDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool HUD1LineItemDescriptionSpecified
        {
            get { return HUD1LineItemDescription != null; }
            set { }
        }

        /// <summary>
        /// Items that require adjustments for a time period require the listing of the period in which they were paid. This is the start date of the payment adjustment as reflected on the description line on the HUD-1. 
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMODate HUD1LineItemFromDate;

        /// <summary>
        /// Gets or sets a value indicating whether the HUD1LineItemFromDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HUD1LineItemFromDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool HUD1LineItemFromDateSpecified
        {
            get { return HUD1LineItemFromDate != null; }
            set { }
        }

        /// <summary>
        /// The party responsible for payment of the specified HUD-1 line item.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<HUD1LineItemPaidByBase> HUD1LineItemPaidByType;

        /// <summary>
        /// Gets or sets a value indicating whether the HUD1LineItemPaidByType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HUD1LineItemPaidByType element has been assigned a value.</value>
        [XmlIgnore]
        public bool HUD1LineItemPaidByTypeSpecified
        {
            get { return this.HUD1LineItemPaidByType != null && this.HUD1LineItemPaidByType.enumValue != HUD1LineItemPaidByBase.Blank; }
            set { }
        }

        /// <summary>
        /// Items that require adjustments for a time period require the listing of the period in which they were paid. This is the end date of the payment adjustment as reflected on the description line on the HUD-1. 
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMODate HUD1LineItemToDate;

        /// <summary>
        /// Gets or sets a value indicating whether the HUD1LineItemToDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HUD1LineItemToDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool HUD1LineItemToDateSpecified
        {
            get { return HUD1LineItemToDate != null; }
            set { }
        }

        /// <summary>
        /// The specified Line Item Number on the HUD-1.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOValue HUD1SpecifiedLineNumberValue;

        /// <summary>
        /// Gets or sets a value indicating whether the HUD1SpecifiedLineNumberValue element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HUD1SpecifiedLineNumberValue element has been assigned a value.</value>
        [XmlIgnore]
        public bool HUD1SpecifiedLineNumberValueSpecified
        {
            get { return HUD1SpecifiedLineNumberValue != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 7)]
        public HUD1_LINE_ITEM_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
