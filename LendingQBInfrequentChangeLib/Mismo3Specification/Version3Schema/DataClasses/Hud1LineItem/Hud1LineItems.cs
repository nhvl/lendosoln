namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class HUD1_LINE_ITEMS
    {
        /// <summary>
        /// Gets a value indicating whether the HUD1_LINE_ITEMS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.Hud1LineItemSpecified;
            }
        }

        /// <summary>
        /// A collection of HUD 1 line items.
        /// </summary>
        [XmlElement("HUD1_LINE_ITEM", Order = 10)]
		public List<HUD1_LINE_ITEM> Hud1LineItem = new List<HUD1_LINE_ITEM>();

        /// <summary>
        /// Gets or sets a value indicating whether the HUD1LineItem element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HUD1LineItem element has been assigned a value.</value>
        [XmlIgnore]
        public bool Hud1LineItemSpecified
        {
            get { return this.Hud1LineItem != null && this.Hud1LineItem.Count(h => h != null && h.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public HUD1_LINE_ITEMS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
