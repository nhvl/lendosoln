namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CLOSING_INFORMATION_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the CLOSING_INFORMATION_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CashFromBorrowerAtClosingAmountSpecified
                    || this.CashFromSellerAtClosingAmountSpecified
                    || this.CashToBorrowerAtClosingAmountSpecified
                    || this.CashToSellerAtClosingAmountSpecified
                    || this.ClosingAgentOrderNumberIdentifierSpecified
                    || this.ClosingDateSpecified
                    || this.ClosingDocumentReceivedDateSpecified
                    || this.ClosingDocumentsExpirationDateSpecified
                    || this.ClosingDocumentsExpirationTimeSpecified
                    || this.CurrentRateSetDateSpecified
                    || this.DisbursementDateSpecified
                    || this.DocumentOrderClassificationTypeSpecified
                    || this.DocumentPreparationDateSpecified
                    || this.EstimatedPrepaidDaysCountSpecified
                    || this.EstimatedPrepaidDaysPaidByTypeOtherDescriptionSpecified
                    || this.EstimatedPrepaidDaysPaidByTypeSpecified
                    || this.ExtensionSpecified
                    || this.FundByDateSpecified
                    || this.LoanEstimatedClosingDateSpecified
                    || this.LoanScheduledClosingDateSpecified
                    || this.RescissionDateSpecified;
            }
        }

        /// <summary>
        /// The total dollar amount required from the borrower to consummate the closing of a real estate transaction.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount CashFromBorrowerAtClosingAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the CashFromBorrowerAtClosingAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CashFromBorrowerAtClosingAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool CashFromBorrowerAtClosingAmountSpecified
        {
            get { return CashFromBorrowerAtClosingAmount != null; }
            set { }
        }

        /// <summary>
        /// The total dollar amount required from the seller to consummate the closing of a real estate transaction.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOAmount CashFromSellerAtClosingAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the CashFromSellerAtClosingAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CashFromSellerAtClosingAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool CashFromSellerAtClosingAmountSpecified
        {
            get { return CashFromSellerAtClosingAmount != null; }
            set { }
        }

        /// <summary>
        /// The total dollar amount received by the borrower to consummate the closing of a real estate transaction.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOAmount CashToBorrowerAtClosingAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the CashToBorrowerAtClosingAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CashToBorrowerAtClosingAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool CashToBorrowerAtClosingAmountSpecified
        {
            get { return CashToBorrowerAtClosingAmount != null; }
            set { }
        }

        /// <summary>
        /// The total dollar amount received by the seller to consummate the closing of a real estate transaction.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOAmount CashToSellerAtClosingAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the CashToSellerAtClosingAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CashToSellerAtClosingAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool CashToSellerAtClosingAmountSpecified
        {
            get { return CashToSellerAtClosingAmount != null; }
            set { }
        }

        /// <summary>
        /// The identifying file number assigned to the transaction by the Closing Agent also known as a Settlement Agent.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOIdentifier ClosingAgentOrderNumberIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the ClosingAgentOrderNumberIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ClosingAgentOrderNumberIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool ClosingAgentOrderNumberIdentifierSpecified
        {
            get { return ClosingAgentOrderNumberIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The date the closing documents were fully executed and the borrower becomes contractually responsible for the debt.  Depending on the location (wet-sign, dry-sign, table-funded and/or escrow state) of the loan closing, the Closing Date may be the same date as either the Document Preparation Date and/or the Execution Date and/or the Recorded Date.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMODate ClosingDate;

        /// <summary>
        /// Gets or sets a value indicating whether the ClosingDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ClosingDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ClosingDateSpecified
        {
            get { return ClosingDate != null; }
            set { }
        }

        /// <summary>
        /// The date the mortgage loan closing document(s) was/were received by the Closing Agent.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMODate ClosingDocumentReceivedDate;

        /// <summary>
        /// Gets or sets a value indicating whether the ClosingDocumentReceivedDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ClosingDocumentReceivedDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ClosingDocumentReceivedDateSpecified
        {
            get { return ClosingDocumentReceivedDate != null; }
            set { }
        }

        /// <summary>
        /// The date on which the terms and conditions disclosed within the closing documents expire.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMODate ClosingDocumentsExpirationDate;

        /// <summary>
        /// Gets or sets a value indicating whether the ClosingDocumentsExpirationDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ClosingDocumentsExpirationDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ClosingDocumentsExpirationDateSpecified
        {
            get { return ClosingDocumentsExpirationDate != null; }
            set { }
        }

        /// <summary>
        /// The time of day on the date on which the terms and conditions disclosed within the closing documents expire.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOTime ClosingDocumentsExpirationTime;

        /// <summary>
        /// Gets or sets a value indicating whether the ClosingDocumentsExpirationTime element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ClosingDocumentsExpirationTime element has been assigned a value.</value>
        [XmlIgnore]
        public bool ClosingDocumentsExpirationTimeSpecified
        {
            get { return ClosingDocumentsExpirationTime != null; }
            set { }
        }

        /// <summary>
        /// Date on which the interest rate for the loan was set by the lender for the final time before closing.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMODate CurrentRateSetDate;

        /// <summary>
        /// Gets or sets a value indicating whether the CurrentRateSetDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CurrentRateSetDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool CurrentRateSetDateSpecified
        {
            get { return CurrentRateSetDate != null; }
            set { }
        }

        /// <summary>
        /// The date the loan funds are disbursed to the borrower or another party on the borrower's behalf.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMODate DisbursementDate;

        /// <summary>
        /// Gets or sets a value indicating whether the DisbursementDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DisbursementDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool DisbursementDateSpecified
        {
            get { return DisbursementDate != null; }
            set { }
        }

        /// <summary>
        /// Defines the type of the document set that is being requested. Identifies if the documents requested are to be an opening or closing document package.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOEnum<DocumentOrderClassificationBase> DocumentOrderClassificationType;

        /// <summary>
        /// Gets or sets a value indicating whether the DocumentOrderClassificationType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DocumentOrderClassificationType element has been assigned a value.</value>
        [XmlIgnore]
        public bool DocumentOrderClassificationTypeSpecified
        {
            get { return this.DocumentOrderClassificationType != null && this.DocumentOrderClassificationType.enumValue != DocumentOrderClassificationBase.Blank; }
            set { }
        }

        /// <summary>
        /// The date the closing documents were prepared.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMODate DocumentPreparationDate;

        /// <summary>
        /// Gets or sets a value indicating whether the DocumentPreparationDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DocumentPreparationDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool DocumentPreparationDateSpecified
        {
            get { return DocumentPreparationDate != null; }
            set { }
        }

        /// <summary>
        /// This is the estimated number of odd days for prepaid interest that are to be included in the prepaid charges.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOCount EstimatedPrepaidDaysCount;

        /// <summary>
        /// Gets or sets a value indicating whether the EstimatedPrepaidDaysCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EstimatedPrepaidDaysCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool EstimatedPrepaidDaysCountSpecified
        {
            get { return EstimatedPrepaidDaysCount != null; }
            set { }
        }

        /// <summary>
        /// Party responsible for payment of estimated odd days interest.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOEnum<EstimatedPrepaidDaysPaidByBase> EstimatedPrepaidDaysPaidByType;

        /// <summary>
        /// Gets or sets a value indicating whether the EstimatedPrepaidDaysPaidByType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EstimatedPrepaidDaysPaidByType element has been assigned a value.</value>
        [XmlIgnore]
        public bool EstimatedPrepaidDaysPaidByTypeSpecified
        {
            get { return this.EstimatedPrepaidDaysPaidByType != null && this.EstimatedPrepaidDaysPaidByType.enumValue != EstimatedPrepaidDaysPaidByBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture the prepaid days contributor type if Other is selected as the estimated prepaid days paid by type.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOString EstimatedPrepaidDaysPaidByTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the EstimatedPrepaidDaysPaidByTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EstimatedPrepaidDaysPaidByTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool EstimatedPrepaidDaysPaidByTypeOtherDescriptionSpecified
        {
            get { return EstimatedPrepaidDaysPaidByTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The last date for which the closing package is valid.
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMODate FundByDate;

        /// <summary>
        /// Gets or sets a value indicating whether the FundByDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FundByDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool FundByDateSpecified
        {
            get { return FundByDate != null; }
            set { }
        }

        /// <summary>
        /// The estimated closing date for loan at the time of application.
        /// </summary>
        [XmlElement(Order = 17)]
        public MISMODate LoanEstimatedClosingDate;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanEstimatedClosingDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanEstimatedClosingDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanEstimatedClosingDateSpecified
        {
            get { return LoanEstimatedClosingDate != null; }
            set { }
        }

        /// <summary>
        /// The scheduled date for when the loan will be closed with the borrower.
        /// </summary>
        [XmlElement(Order = 18)]
        public MISMODate LoanScheduledClosingDate;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanScheduledClosingDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanScheduledClosingDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanScheduledClosingDateSpecified
        {
            get { return LoanScheduledClosingDate != null; }
            set { }
        }

        /// <summary>
        /// The last date that borrowers have to cancel a mortgage transaction. Normally applies to second mortgages and refinances. Usually three business days following closing or document execution date.
        /// </summary>
        [XmlElement(Order = 19)]
        public MISMODate RescissionDate;

        /// <summary>
        /// Gets or sets a value indicating whether the RescissionDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RescissionDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool RescissionDateSpecified
        {
            get { return RescissionDate != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 20)]
        public CLOSING_INFORMATION_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null ? Extension.ShouldSerialize : false; }
            set { }
        }
    }
}
