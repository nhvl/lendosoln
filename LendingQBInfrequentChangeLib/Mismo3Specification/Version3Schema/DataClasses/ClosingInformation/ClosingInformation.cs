namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CLOSING_INFORMATION
    {
        /// <summary>
        /// Gets a value indicating whether the CLOSING_INFORMATION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ClosingAdjustmentItemsSpecified
                || this.ClosingCostFundsSpecified
                || this.ClosingInformationDetailSpecified
                || this.ClosingInstructionSpecified
                || this.CollectedOtherFundsSpecified
                || this.CompensationsSpecified
                || this.PrepaidItemsSpecified
                || this.ProrationItemsSpecified
                || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// Adjustment items at closing.
        /// </summary>
        [XmlElement("CLOSING_ADJUSTMENT_ITEMS", Order = 0)]
        public CLOSING_ADJUSTMENT_ITEMS ClosingAdjustmentItems;

        /// <summary>
        /// Gets or sets a value indicating whether the ClosingAdjustmentItems element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ClosingAdjustmentItems element has been assigned a value.</value>
        [XmlIgnore]
        public bool ClosingAdjustmentItemsSpecified
        {
            get { return ClosingAdjustmentItems != null && ClosingAdjustmentItems.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Funds for closing costs.
        /// </summary>
        [XmlElement("CLOSING_COST_FUNDS", Order = 1)]
        public CLOSING_COST_FUNDS ClosingCostFunds;

        /// <summary>
        /// Gets or sets a value indicating whether the ClosingCostFunds element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ClosingCostFunds element has been assigned a value.</value>
        [XmlIgnore]
        public bool ClosingCostFundsSpecified
        {
            get { return ClosingCostFunds != null && ClosingCostFunds.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details on the closing information.
        /// </summary>
        [XmlElement("CLOSING_INFORMATION_DETAIL", Order = 2)]
        public CLOSING_INFORMATION_DETAIL ClosingInformationDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the ClosingInformationDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ClosingInformationDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool ClosingInformationDetailSpecified
        {
            get { return this.ClosingInformationDetail != null && this.ClosingInformationDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Instructions for closing.
        /// </summary>
        [XmlElement("CLOSING_INSTRUCTION", Order = 3)]
        public CLOSING_INSTRUCTION ClosingInstruction;

        /// <summary>
        /// Gets or sets a value indicating whether the ClosingInstruction element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ClosingInstruction element has been assigned a value.</value>
        [XmlIgnore]
        public bool ClosingInstructionSpecified
        {
            get { return this.ClosingInstruction != null && this.ClosingInstruction.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A list of other collected funds.
        /// </summary>
        [XmlElement("COLLECTED_OTHER_FUNDS", Order = 4)]
        public COLLECTED_OTHER_FUNDS CollectedOtherFunds;

        /// <summary>
        /// Gets or sets a value indicating whether the CollectedOtherFunds element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CollectedOtherFunds element has been assigned a value.</value>
        [XmlIgnore]
        public bool CollectedOtherFundsSpecified
        {
            get { return this.CollectedOtherFunds != null && this.CollectedOtherFunds.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A list of compensations.
        /// </summary>
        [XmlElement("COMPENSATIONS", Order = 5)]
        public COMPENSATIONS Compensations;

        /// <summary>
        /// Gets or sets a value indicating whether the Compensations element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Compensations element has been assigned a value.</value>
        [XmlIgnore]
        public bool CompensationsSpecified
        {
            get { return this.Compensations != null && this.Compensations.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A list of prepaid items.
        /// </summary>
        [XmlElement("PREPAID_ITEMS", Order = 6)]
        public PREPAID_ITEMS PrepaidItems;

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaidItems element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaidItems element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaidItemsSpecified
        {
            get { return this.PrepaidItems != null && this.PrepaidItems.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A list of proration items.
        /// </summary>
        [XmlElement("PRORATION_ITEMS", Order = 7)]
        public PRORATION_ITEMS ProrationItems;

        /// <summary>
        /// Gets or sets a value indicating whether the ProrationItems element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProrationItems element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProrationItemsSpecified
        {
            get { return this.ProrationItems != null && this.ProrationItems.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 8)]
        public CLOSING_INFORMATION_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
