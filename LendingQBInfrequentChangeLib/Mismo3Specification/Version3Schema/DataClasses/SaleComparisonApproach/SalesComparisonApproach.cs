namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class SALES_COMPARISON_APPROACH
    {
        /// <summary>
        /// Gets a value indicating whether the SALES_COMPARISON_APPROACH container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.SalesComparisonCommentDescriptionSpecified
                    || this.SalesComparisonValuePerBedroomAmountSpecified
                    || this.SalesComparisonValuePerGrossBuildingAreaAmountSpecified
                    || this.SalesComparisonValuePerRoomAmountSpecified
                    || this.SalesComparisonValuePerUnitAmountSpecified
                    || this.ValueIndicatedBySalesComparisonAmountSpecified;
            }
        }

        /// <summary>
        /// A free-form text field used to describe or comment on the on the Sales Comparison approach in valuating the subject property.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString SalesComparisonCommentDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the SalesComparisonCommentDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SalesComparisonCommentDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool SalesComparisonCommentDescriptionSpecified
        {
            get { return SalesComparisonCommentDescription != null; }
            set { }
        }

        /// <summary>
        /// The dollar value of a bedroom as indicated by the Sales Comparison approach to property valuation.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOAmount SalesComparisonValuePerBedroomAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the SalesComparisonValuePerBedroomAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SalesComparisonValuePerBedroomAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool SalesComparisonValuePerBedroomAmountSpecified
        {
            get { return SalesComparisonValuePerBedroomAmount != null; }
            set { }
        }

        /// <summary>
        /// The dollar value per square foot of living area as indicated by the Sales Comparison approach to property valuation.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOAmount SalesComparisonValuePerGrossBuildingAreaAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the SalesComparisonValuePerGrossBuildingAreaAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SalesComparisonValuePerGrossBuildingAreaAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool SalesComparisonValuePerGrossBuildingAreaAmountSpecified
        {
            get { return SalesComparisonValuePerGrossBuildingAreaAmount != null; }
            set { }
        }

        /// <summary>
        /// The dollar value of a room as indicated by the Sales Comparison approach to property valuation.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOAmount SalesComparisonValuePerRoomAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the SalesComparisonValuePerRoomAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SalesComparisonValuePerRoomAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool SalesComparisonValuePerRoomAmountSpecified
        {
            get { return SalesComparisonValuePerRoomAmount != null; }
            set { }
        }

        /// <summary>
        /// The dollar value of a living unit as indicated by the Sales Comparison approach to property valuation.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOAmount SalesComparisonValuePerUnitAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the SalesComparisonValuePerUnitAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SalesComparisonValuePerUnitAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool SalesComparisonValuePerUnitAmountSpecified
        {
            get { return SalesComparisonValuePerUnitAmount != null; }
            set { }
        }

        /// <summary>
        /// The dollar value of the subject property indicated by the Sales Comparison approach.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOAmount ValueIndicatedBySalesComparisonAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ValueIndicatedBySalesComparisonAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ValueIndicatedBySalesComparisonAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ValueIndicatedBySalesComparisonAmountSpecified
        {
            get { return ValueIndicatedBySalesComparisonAmount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 6)]
        public SALES_COMPARISON_APPROACH_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
