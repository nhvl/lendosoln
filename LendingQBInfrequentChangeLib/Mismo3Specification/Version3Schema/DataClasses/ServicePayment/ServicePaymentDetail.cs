namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class SERVICE_PAYMENT_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the SERVICE_PAYMENT_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.InternalAccountIdentifierSpecified
                    || this.ServicePaymentAccountIdentifierSpecified
                    || this.ServicePaymentAmountSpecified
                    || this.ServicePaymentCreditAccountExpirationDateSpecified
                    || this.ServicePaymentCreditMethodTypeOtherDescriptionSpecified
                    || this.ServicePaymentCreditMethodTypeSpecified
                    || this.ServicePaymentMethodTypeOtherDescriptionSpecified
                    || this.ServicePaymentMethodTypeSpecified
                    || this.ServicePaymentOnAccountIdentifierSpecified
                    || this.ServicePaymentOnAccountMaximumDebitAmountSpecified
                    || this.ServicePaymentOnAccountMinimumBalanceAmountSpecified
                    || this.ServicePaymentReferenceIdentifierSpecified
                    || this.ServicePaymentSecondaryCreditAccountIdentifierSpecified;
            }
        }

        /// <summary>
        /// The account number or identifier assigned by a service provider to a service requestor. The Internal Account Identifier is supplied with request transactions to allow the service provider to identify the billing account to be used.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIdentifier InternalAccountIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the InternalAccountIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InternalAccountIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool InternalAccountIdentifierSpecified
        {
            get { return InternalAccountIdentifier != null; }
            set { }
        }

        /// <summary>
        /// An alphanumeric designation assigned to the account that payment will be processed from.  For credit card transactions this is the account number imprinted on the credit card.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOIdentifier ServicePaymentAccountIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the ServicePaymentAccountIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServicePaymentAccountIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServicePaymentAccountIdentifierSpecified
        {
            get { return ServicePaymentAccountIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The total amount of payment being tendered.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOAmount ServicePaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ServicePaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServicePaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServicePaymentAmountSpecified
        {
            get { return ServicePaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// For credit card transactions this is the credit card expiration date imprinted on the credit card.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMODate ServicePaymentCreditAccountExpirationDate;

        /// <summary>
        /// Gets or sets a value indicating whether the ServicePaymentCreditAccountExpirationDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServicePaymentCreditAccountExpirationDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServicePaymentCreditAccountExpirationDateSpecified
        {
            get { return ServicePaymentCreditAccountExpirationDate != null; }
            set { }
        }

        /// <summary>
        /// This enumerated attribute identifies the type of credit card or other payment method.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<ServicePaymentCreditMethodBase> ServicePaymentCreditMethodType;

        /// <summary>
        /// Gets or sets a value indicating whether the ServicePaymentCreditMethodType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServicePaymentCreditMethodType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServicePaymentCreditMethodTypeSpecified
        {
            get { return this.ServicePaymentCreditMethodType != null && this.ServicePaymentCreditMethodType.enumValue != ServicePaymentCreditMethodBase.Blank; }
            set { }
        }

        /// <summary>
        /// When Service Payment Method Type is set to Other, enter the description in this data attribute.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString ServicePaymentCreditMethodTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ServicePaymentCreditMethodTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServicePaymentCreditMethodTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServicePaymentCreditMethodTypeOtherDescriptionSpecified
        {
            get { return ServicePaymentCreditMethodTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A description of the method of funds transfer to be used for the transaction.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOEnum<ServicePaymentMethodBase> ServicePaymentMethodType;

        /// <summary>
        /// Gets or sets a value indicating whether the ServicePaymentMethodType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServicePaymentMethodType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServicePaymentMethodTypeSpecified
        {
            get { return this.ServicePaymentMethodType != null && this.ServicePaymentMethodType.enumValue != ServicePaymentMethodBase.Blank; }
            set { }
        }

        /// <summary>
        /// A description of the type of payment when Other is selected from the enumerated list.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOString ServicePaymentMethodTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ServicePaymentMethodTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServicePaymentMethodTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServicePaymentMethodTypeOtherDescriptionSpecified
        {
            get { return ServicePaymentMethodTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// An alphanumeric designation assigned to the prepaid account to be debited for the payment.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOIdentifier ServicePaymentOnAccountIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the ServicePaymentOnAccountIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServicePaymentOnAccountIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServicePaymentOnAccountIdentifierSpecified
        {
            get { return ServicePaymentOnAccountIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The maximum dollar amount that can be debited from the prepaid account on any single transaction.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOAmount ServicePaymentOnAccountMaximumDebitAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ServicePaymentOnAccountMaximumDebitAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServicePaymentOnAccountMaximumDebitAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServicePaymentOnAccountMaximumDebitAmountSpecified
        {
            get { return ServicePaymentOnAccountMaximumDebitAmount != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount at which the system should send a message that the prepaid account needs to be replenished.  It may also trigger the system to discontinue processing documents until more funds are added to the account.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOAmount ServicePaymentOnAccountMinimumBalanceAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ServicePaymentOnAccountMinimumBalanceAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServicePaymentOnAccountMinimumBalanceAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServicePaymentOnAccountMinimumBalanceAmountSpecified
        {
            get { return ServicePaymentOnAccountMinimumBalanceAmount != null; }
            set { }
        }

        /// <summary>
        /// A unique identifier assigned to each payment transaction for tracking and auditing purposes.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOIdentifier ServicePaymentReferenceIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the ServicePaymentReferenceIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServicePaymentReferenceIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServicePaymentReferenceIdentifierSpecified
        {
            get { return ServicePaymentReferenceIdentifier != null; }
            set { }
        }

        /// <summary>
        /// For credit card transactions this is an additional identifier that appears on the credit card in various locations depending on the Credit Card Type.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOIdentifier ServicePaymentSecondaryCreditAccountIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the ServicePaymentSecondaryCreditAccountIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServicePaymentSecondaryCreditAccountIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServicePaymentSecondaryCreditAccountIdentifierSpecified
        {
            get { return ServicePaymentSecondaryCreditAccountIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 13)]
        public SERVICE_PAYMENT_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
