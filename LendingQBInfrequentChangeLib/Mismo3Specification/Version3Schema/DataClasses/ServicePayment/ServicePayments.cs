namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class SERVICE_PAYMENTS
    {
        /// <summary>
        /// Gets a value indicating whether the SERVICE_PAYMENTS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ServicePaymentSpecified;
            }
        }

        /// <summary>
        /// A collection of service payments.
        /// </summary>
        [XmlElement("SERVICE_PAYMENT", Order = 0)]
		public List<SERVICE_PAYMENT> ServicePayment = new List<SERVICE_PAYMENT>();

        /// <summary>
        /// Gets or sets a value indicating whether the ServicePayment element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServicePayment element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServicePaymentSpecified
        {
            get { return this.ServicePayment != null && this.ServicePayment.Count(s => s != null && s.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public SERVICE_PAYMENTS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
