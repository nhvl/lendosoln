namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class SERVICE_PRODUCT_NAME_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the SERVICE_PRODUCT_NAME_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ServiceProductNameDescriptionSpecified
                    || this.ServiceProductNameIdentifierSpecified;
            }
        }

        /// <summary>
        /// A descriptive name for the response product, such as: Super Eagle.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString ServiceProductNameDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ServiceProductNameDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServiceProductNameDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServiceProductNameDescriptionSpecified
        {
            get { return ServiceProductNameDescription != null; }
            set { }
        }

        /// <summary>
        /// A unique identifier that correlates to a specific product name; usually a number.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOIdentifier ServiceProductNameIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the ServiceProductNameIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServiceProductNameIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServiceProductNameIdentifierSpecified
        {
            get { return ServiceProductNameIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public SERVICE_PRODUCT_NAME_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
