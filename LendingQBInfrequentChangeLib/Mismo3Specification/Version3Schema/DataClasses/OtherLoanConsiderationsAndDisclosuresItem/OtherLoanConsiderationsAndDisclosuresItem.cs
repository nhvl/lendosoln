namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class OTHER_LOAN_CONSIDERATIONS_AND_DISCLOSURES_ITEM
    {
        /// <summary>
        /// Gets a value indicating whether the OTHER_LOAN_CONSIDERATIONS_AND_DISCLOSURES_ITEM container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.LoanConsiderationDisclosureStatementDescriptionSpecified
                    || this.LoanConsiderationDisclosureStatementTypeOtherDescriptionSpecified
                    || this.LoanConsiderationDisclosureStatementTypeSpecified;
            }
        }

        /// <summary>
        /// A statement required to be disclosed to, or considered by, the borrower of certain information pertaining to loan program features, creditor policies, and requirements in or arising from the legal obligation or loan. Used in conjunction with Loan Consideration Disclosure Statement Type.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString LoanConsiderationDisclosureStatementDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanConsiderationDisclosureStatementDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanConsiderationDisclosureStatementDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanConsiderationDisclosureStatementDescriptionSpecified
        {
            get { return LoanConsiderationDisclosureStatementDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the type of statement required to be disclosed to, or considered by, the borrower of certain information pertaining to loan program features, creditor policies, and requirements in or arising from the legal obligation or loan.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<LoanConsiderationDisclosureStatementBase> LoanConsiderationDisclosureStatementType;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanConsiderationDisclosureStatementType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanConsiderationDisclosureStatementType element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanConsiderationDisclosureStatementTypeSpecified
        {
            get { return this.LoanConsiderationDisclosureStatementType != null && this.LoanConsiderationDisclosureStatementType.enumValue != LoanConsiderationDisclosureStatementBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture additional information when Other is selected for Loan Consideration Disclosure Statement Type.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString LoanConsiderationDisclosureStatementTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanConsiderationDisclosureStatementTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanConsiderationDisclosureStatementTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanConsiderationDisclosureStatementTypeOtherDescriptionSpecified
        {
            get { return LoanConsiderationDisclosureStatementTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public OTHER_LOAN_CONSIDERATIONS_AND_DISCLOSURES_ITEM_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
