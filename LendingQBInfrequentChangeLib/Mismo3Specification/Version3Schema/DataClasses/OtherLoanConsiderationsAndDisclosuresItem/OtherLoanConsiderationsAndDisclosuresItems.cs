namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class OTHER_LOAN_CONSIDERATIONS_AND_DISCLOSURES_ITEMS
    {
        /// <summary>
        /// Gets a value indicating whether the OTHER_LOAN_CONSIDERATIONS_AND_DISCLOSURES_ITEMS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.OtherLoanConsiderationsAndDisclosuresItemSpecified;
            }
        }

        /// <summary>
        /// A collection of other loan considerations and disclosures items.
        /// </summary>
        [XmlElement("OTHER_LOAN_CONSIDERATIONS_AND_DISCLOSURES_ITEM", Order = 0)]
        public List<OTHER_LOAN_CONSIDERATIONS_AND_DISCLOSURES_ITEM> OtherLoanConsiderationsAndDisclosuresItem = new List<OTHER_LOAN_CONSIDERATIONS_AND_DISCLOSURES_ITEM>();

        /// <summary>
        /// Gets or sets a value indicating whether the OtherLoanConsiderationsAndDisclosuresItem element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OtherLoanConsiderationsAndDisclosuresItem element has been assigned a value.</value>
        [XmlIgnore]
        public bool OtherLoanConsiderationsAndDisclosuresItemSpecified
        {
            get { return this.OtherLoanConsiderationsAndDisclosuresItem != null && this.OtherLoanConsiderationsAndDisclosuresItem.Count(o => o != null && o.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public OTHER_LOAN_CONSIDERATIONS_AND_DISCLOSURES_ITEMS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
