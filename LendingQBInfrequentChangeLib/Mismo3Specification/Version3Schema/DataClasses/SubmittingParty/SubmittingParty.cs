namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class SUBMITTING_PARTY
    {
        /// <summary>
        /// Gets a value indicating whether the SUBMITTING_PARTY container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.SubmittingPartySequenceNumberSpecified
                    || this.SubmittingPartyTransactionIdentifierSpecified;
            }
        }

        /// <summary>
        /// Identifies the ordering of multiple submitting parties.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOSequenceNumber SubmittingPartySequenceNumber;

        /// <summary>
        /// Gets or sets a value indicating whether the SubmittingPartySequenceNumber element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SubmittingPartySequenceNumber element has been assigned a value.</value>
        [XmlIgnore]
        public bool SubmittingPartySequenceNumberSpecified
        {
            get { return SubmittingPartySequenceNumber != null; }
            set { }
        }

        /// <summary>
        /// The identifier created and used by the submitting party to track a transaction or associated interactions. The identifier is expected to be returned in the submitting party's transaction identifier attribute.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOIdentifier SubmittingPartyTransactionIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the SubmittingPartyTransactionIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SubmittingPartyTransactionIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool SubmittingPartyTransactionIdentifierSpecified
        {
            get { return SubmittingPartyTransactionIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public SUBMITTING_PARTY_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
