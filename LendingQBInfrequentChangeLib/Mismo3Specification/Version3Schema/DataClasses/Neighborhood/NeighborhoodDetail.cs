namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class NEIGHBORHOOD_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the NEIGHBORHOOD_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AdverseNeighborhoodInfluenceIndicatorSpecified
                    || this.ExtensionSpecified
                    || this.NeighborhoodAcceptabilityOfCooperativeOwnershipDescriptionSpecified
                    || this.NeighborhoodAcceptanceOfCooperativeOwnershipIndicatorSpecified
                    || this.NeighborhoodBoundariesDescriptionSpecified
                    || this.NeighborhoodBuiltUpPercentSpecified
                    || this.NeighborhoodDescriptionSpecified
                    || this.NeighborhoodGrowthPaceTypeSpecified
                    || this.NeighborhoodMarketabilityFactorsDescriptionSpecified
                    || this.NeighborhoodMarketConditionsDescriptionSpecified
                    || this.NeighborhoodNameSpecified
                    || this.NeighborhoodPopulationDensityDescriptionSpecified
                    || this.NeighborhoodPotentialManufacturedHomeParkDescriptionSpecified
                    || this.NeighborhoodPropertyLocationTypeOtherDescriptionSpecified
                    || this.NeighborhoodPropertyLocationTypeSpecified
                    || this.NeighborhoodStreetMeetsStandardsIndicatorSpecified;
            }
        }

        /// <summary>
        /// Indicates at least one adverse influence has been noted about the neighborhood.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIndicator AdverseNeighborhoodInfluenceIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the AdverseNeighborhoodInfluenceIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AdverseNeighborhoodInfluenceIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool AdverseNeighborhoodInfluenceIndicatorSpecified
        {
            get { return AdverseNeighborhoodInfluenceIndicator != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field describing how the neighborhood does accept cooperative property ownership.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString NeighborhoodAcceptabilityOfCooperativeOwnershipDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the NeighborhoodAcceptabilityOfCooperativeOwnershipDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NeighborhoodAcceptabilityOfCooperativeOwnershipDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool NeighborhoodAcceptabilityOfCooperativeOwnershipDescriptionSpecified
        {
            get { return NeighborhoodAcceptabilityOfCooperativeOwnershipDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the neighborhood does accept cooperative property ownership.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOIndicator NeighborhoodAcceptanceOfCooperativeOwnershipIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the NeighborhoodAcceptanceOfCooperativeOwnershipIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NeighborhoodAcceptanceOfCooperativeOwnershipIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool NeighborhoodAcceptanceOfCooperativeOwnershipIndicatorSpecified
        {
            get { return NeighborhoodAcceptanceOfCooperativeOwnershipIndicator != null; }
            set { }
        }

        /// <summary>
        /// The boundaries of the subject neighborhood.  These boundaries may include but are not limited to streets, legally recognized neighborhood boundaries, waterways, or other natural boundaries that define the separation of one neighborhood from another.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString NeighborhoodBoundariesDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the NeighborhoodBoundariesDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NeighborhoodBoundariesDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool NeighborhoodBoundariesDescriptionSpecified
        {
            get { return NeighborhoodBoundariesDescription != null; }
            set { }
        }

        /// <summary>
        /// The specific percentage of the built-up status of the neighborhood as observed in the valuation.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOPercent NeighborhoodBuiltUpPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the NeighborhoodBuiltUpPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NeighborhoodBuiltUpPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool NeighborhoodBuiltUpPercentSpecified
        {
            get { return NeighborhoodBuiltUpPercent != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field describing the neighborhood.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString NeighborhoodDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the NeighborhoodDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NeighborhoodDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool NeighborhoodDescriptionSpecified
        {
            get { return NeighborhoodDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the rate at which the neighborhood is being developed or if it has been fully developed.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOEnum<NeighborhoodGrowthPaceBase> NeighborhoodGrowthPaceType;

        /// <summary>
        /// Gets or sets a value indicating whether the NeighborhoodGrowthPaceType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NeighborhoodGrowthPaceType element has been assigned a value.</value>
        [XmlIgnore]
        public bool NeighborhoodGrowthPaceTypeSpecified
        {
            get { return this.NeighborhoodGrowthPaceType != null && this.NeighborhoodGrowthPaceType.enumValue != NeighborhoodGrowthPaceBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to discuss factors that affect the marketability of properties in the neighborhood such as proximity to employment, appeal, schools and shopping.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOString NeighborhoodMarketabilityFactorsDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the NeighborhoodMarketabilityFactorsDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NeighborhoodMarketabilityFactorsDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool NeighborhoodMarketabilityFactorsDescriptionSpecified
        {
            get { return NeighborhoodMarketabilityFactorsDescription != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to discuss market conditions in the neighborhood that may effect property values.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOString NeighborhoodMarketConditionsDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the NeighborhoodMarketConditionsDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NeighborhoodMarketConditionsDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool NeighborhoodMarketConditionsDescriptionSpecified
        {
            get { return NeighborhoodMarketConditionsDescription != null; }
            set { }
        }

        /// <summary>
        /// The name of the neighborhood.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOString NeighborhoodName;

        /// <summary>
        /// Gets or sets a value indicating whether the NeighborhoodName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NeighborhoodName element has been assigned a value.</value>
        [XmlIgnore]
        public bool NeighborhoodNameSpecified
        {
            get { return NeighborhoodName != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field to describe the population density as observed in the valuation.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOString NeighborhoodPopulationDensityDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the NeighborhoodPopulationDensityDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NeighborhoodPopulationDensityDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool NeighborhoodPopulationDensityDescriptionSpecified
        {
            get { return NeighborhoodPopulationDensityDescription != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to comment on the potential addition of manufactured home park units in the area.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOString NeighborhoodPotentialManufacturedHomeParkDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the NeighborhoodPotentialManufacturedHomeParkDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NeighborhoodPotentialManufacturedHomeParkDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool NeighborhoodPotentialManufacturedHomeParkDescriptionSpecified
        {
            get { return NeighborhoodPotentialManufacturedHomeParkDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the urban, suburban, or rural nature of the location of the subject property.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOEnum<NeighborhoodPropertyLocationBase> NeighborhoodPropertyLocationType;

        /// <summary>
        /// Gets or sets a value indicating whether the NeighborhoodPropertyLocationType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NeighborhoodPropertyLocationType element has been assigned a value.</value>
        [XmlIgnore]
        public bool NeighborhoodPropertyLocationTypeSpecified
        {
            get { return this.NeighborhoodPropertyLocationType != null && this.NeighborhoodPropertyLocationType.enumValue != NeighborhoodPropertyLocationBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used if NeighborhoodPropertyLocationType = Other.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOString NeighborhoodPropertyLocationTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the NeighborhoodPropertyLocationTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NeighborhoodPropertyLocationTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool NeighborhoodPropertyLocationTypeOtherDescriptionSpecified
        {
            get { return NeighborhoodPropertyLocationTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the property street meets the street standards for the neighborhood.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOIndicator NeighborhoodStreetMeetsStandardsIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the NeighborhoodStreetMeetsStandardsIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NeighborhoodStreetMeetsStandardsIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool NeighborhoodStreetMeetsStandardsIndicatorSpecified
        {
            get { return NeighborhoodStreetMeetsStandardsIndicator != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 15)]
        public NEIGHBORHOOD_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
