namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class NEIGHBORHOOD
    {
        /// <summary>
        /// Gets a value indicating whether the NEIGHBORHOOD container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BoundariesSpecified
                    || this.ExtensionSpecified
                    || this.HousingsSpecified
                    || this.NeighborhoodDetailSpecified
                    || this.NeighborhoodInfluencesSpecified
                    || this.PresentLandUsesSpecified
                    || this.SchoolsSpecified;
            }
        }

        /// <summary>
        /// Boundaries in the neighborhood.
        /// </summary>
        [XmlElement("BOUNDARIES", Order = 0)]
        public BOUNDARIES Boundaries;

        /// <summary>
        /// Gets or sets a value indicating whether the Boundaries element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Boundaries element has been assigned a value.</value>
        [XmlIgnore]
        public bool BoundariesSpecified
        {
            get { return this.Boundaries != null && this.Boundaries.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Housings in the neighborhood.
        /// </summary>
        [XmlElement("HOUSINGS", Order = 1)]
        public HOUSINGS Housings;

        /// <summary>
        /// Gets or sets a value indicating whether the Housings element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Housings element has been assigned a value.</value>
        [XmlIgnore]
        public bool HousingsSpecified
        {
            get { return this.Housings != null && this.Housings.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details on the neighborhood.
        /// </summary>
        [XmlElement("NEIGHBORHOOD_DETAIL", Order = 2)]
        public NEIGHBORHOOD_DETAIL NeighborhoodDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the NeighborhoodDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NeighborhoodDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool NeighborhoodDetailSpecified
        {
            get { return this.NeighborhoodDetail != null && this.NeighborhoodDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Influences on the neighborhood.
        /// </summary>
        [XmlElement("NEIGHBORHOOD_INFLUENCES", Order = 3)]
        public NEIGHBORHOOD_INFLUENCES NeighborhoodInfluences;

        /// <summary>
        /// Gets or sets a value indicating whether the NeighborhoodInfluences element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NeighborhoodInfluences element has been assigned a value.</value>
        [XmlIgnore]
        public bool NeighborhoodInfluencesSpecified
        {
            get { return this.NeighborhoodInfluences != null && this.NeighborhoodInfluences.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Present land uses of the neighborhood.
        /// </summary>
        [XmlElement("PRESENT_LAND_USES", Order = 4)]
        public PRESENT_LAND_USES PresentLandUses;

        /// <summary>
        /// Gets or sets a value indicating whether the PresentLandUses element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PresentLandUses element has been assigned a value.</value>
        [XmlIgnore]
        public bool PresentLandUsesSpecified
        {
            get { return this.PresentLandUses != null && this.PresentLandUses.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Schools in the neighborhood.
        /// </summary>
        [XmlElement("SCHOOLS", Order = 5)]
        public SCHOOLS Schools;

        /// <summary>
        /// Gets or sets a value indicating whether the Schools element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Schools element has been assigned a value.</value>
        [XmlIgnore]
        public bool SchoolsSpecified
        {
            get { return this.Schools != null && this.Schools.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 6)]
        public NEIGHBORHOOD_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
