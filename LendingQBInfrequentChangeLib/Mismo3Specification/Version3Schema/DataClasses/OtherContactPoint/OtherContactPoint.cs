namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class OTHER_CONTACT_POINT
    {
        /// <summary>
        /// Gets a value indicating whether the OTHER_CONTACT_POINT container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ContactPointOtherValueDescriptionSpecified
                    || this.ContactPointOtherValueSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// The contact information that is not email, phone or fax.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOValue ContactPointOtherValue;

        /// <summary>
        /// Gets or sets a value indicating whether the ContactPointOtherValue element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ContactPointOtherValue element has been assigned a value.</value>
        [XmlIgnore]
        public bool ContactPointOtherValueSpecified
        {
            get { return ContactPointOtherValue != null; }
            set { }
        }

        /// <summary>
        /// The description of the type on contact information being provided when Contact Point Other Value is populated.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString ContactPointOtherValueDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ContactPointOtherValueDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ContactPointOtherValueDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ContactPointOtherValueDescriptionSpecified
        {
            get { return ContactPointOtherValueDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public OTHER_CONTACT_POINT_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
