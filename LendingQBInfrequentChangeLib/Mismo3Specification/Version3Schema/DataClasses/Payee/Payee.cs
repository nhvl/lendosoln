namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PAYEE
    {
        /// <summary>
        /// Gets a value indicating whether the PAYEE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.PayeeRemittanceTypeOtherDescriptionSpecified
                    || this.PayeeRemittanceTypeSpecified
                    || this.PayeeTypeOtherDescriptionSpecified
                    || this.PayeeTypeSpecified;
            }
        }

        /// <summary>
        /// Indicates how the remittance is to be made to the payee.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<PayeeRemittanceBase> PayeeRemittanceType;

        /// <summary>
        /// Gets or sets a value indicating whether the PayeeRemittanceType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PayeeRemittanceType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PayeeRemittanceTypeSpecified
        {
            get { return this.PayeeRemittanceType != null && this.PayeeRemittanceType.enumValue != PayeeRemittanceBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture Payee Remittance Type if Other is selected.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString PayeeRemittanceTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PayeeRemittanceTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PayeeRemittanceTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PayeeRemittanceTypeOtherDescriptionSpecified
        {
            get { return PayeeRemittanceTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates how one would categorized this payee based on the type of entity that is being paid.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<PayeeBase> PayeeType;

        /// <summary>
        /// Gets or sets a value indicating whether the PayeeType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PayeeType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PayeeTypeSpecified
        {
            get { return this.PayeeType != null && this.PayeeType.enumValue != PayeeBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture the payee type if Other is selected as the payee type.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString PayeeTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PayeeTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PayeeTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PayeeTypeOtherDescriptionSpecified
        {
            get { return PayeeTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public PAYEE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
