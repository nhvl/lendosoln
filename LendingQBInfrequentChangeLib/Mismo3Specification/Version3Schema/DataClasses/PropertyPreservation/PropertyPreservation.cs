namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class PROPERTY_PRESERVATION
    {
        /// <summary>
        /// Gets a value indicating whether the PROPERTY_PRESERVATION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.PropertyPreservationActionCompletionDateSpecified
                    || this.PropertyPreservationActionTypeOtherDescriptionSpecified
                    || this.PropertyPreservationActionTypeSpecified
                    || this.PropertyPreservationStatusDateSpecified
                    || this.PropertyPreservationStatusTypeOtherDescriptionSpecified
                    || this.PropertyPreservationStatusTypeSpecified;
            }
        }

        /// <summary>
        /// The date the property preservation efforts were completed.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMODate PropertyPreservationActionCompletionDate;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyPreservationActionCompletionDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyPreservationActionCompletionDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyPreservationActionCompletionDateSpecified
        {
            get { return PropertyPreservationActionCompletionDate != null; }
            set { }
        }

        /// <summary>
        /// Specifies the action taken to preserve and protect the property.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<PropertyPreservationActionBase> PropertyPreservationActionType;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyPreservationActionType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyPreservationActionType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyPreservationActionTypeSpecified
        {
            get { return this.PropertyPreservationActionType != null && this.PropertyPreservationActionType.enumValue != PropertyPreservationActionBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Property Preservation Action Type.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString PropertyPreservationActionTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyPreservationActionTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyPreservationActionTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyPreservationActionTypeOtherDescriptionSpecified
        {
            get { return PropertyPreservationActionTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The date of the property preservation status.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMODate PropertyPreservationStatusDate;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyPreservationStatusDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyPreservationStatusDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyPreservationStatusDateSpecified
        {
            get { return PropertyPreservationStatusDate != null; }
            set { }
        }

        /// <summary>
        /// Specifies the status of the Property Preservation Action Type.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<PropertyPreservationStatusBase> PropertyPreservationStatusType;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyPreservationStatusType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyPreservationStatusType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyPreservationStatusTypeSpecified
        {
            get { return this.PropertyPreservationStatusType != null && this.PropertyPreservationStatusType.enumValue != PropertyPreservationStatusBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Property Preservation Status Type.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString PropertyPreservationStatusTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyPreservationStatusTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyPreservationStatusTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyPreservationStatusTypeOtherDescriptionSpecified
        {
            get { return PropertyPreservationStatusTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 6)]
        public PROPERTY_PRESERVATION_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
