namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PROPERTY_PRESERVATIONS
    {
        /// <summary>
        /// Gets a value indicating whether the PROPERTY_PRESERVATIONS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.PropertyPreservationSpecified;
            }
        }

        /// <summary>
        /// A collection of property preservations.
        /// </summary>
        [XmlElement("PROPERTY_PRESERVATION", Order = 0)]
		public List<PROPERTY_PRESERVATION> PropertyPreservation = new List<PROPERTY_PRESERVATION>();

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyPreservation element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyPreservation element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyPreservationSpecified
        {
            get { return this.PropertyPreservation != null && this.PropertyPreservation.Count(p => p != null && p.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public PROPERTY_PRESERVATIONS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
