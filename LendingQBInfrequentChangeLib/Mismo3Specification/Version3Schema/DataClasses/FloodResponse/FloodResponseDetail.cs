namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class FLOOD_RESPONSE_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the FLOOD_RESPONSE_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DisputeResolutionIndicatorSpecified
                    || this.ExtensionSpecified
                    || this.FEMAAdditionalLenderDescriptionSpecified
                    || this.FloodTransactionIdentifierSpecified;
            }
        }

        /// <summary>
        /// Indicates that the response is a result of a dispute.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIndicator DisputeResolutionIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the DisputeResolutionIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DisputeResolutionIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool DisputeResolutionIndicatorSpecified
        {
            get { return DisputeResolutionIndicator != null; }
            set { }
        }

        /// <summary>
        /// Additional information for the lender in Box 1 of the FEMA document.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString FEMAAdditionalLenderDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the FEMAAdditionalLenderDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FEMAAdditionalLenderDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool FEMAAdditionalLenderDescriptionSpecified
        {
            get { return FEMAAdditionalLenderDescription != null; }
            set { }
        }

        /// <summary>
        /// A unique tracking identifier assigned by the sender which is typically returned to the sender in the resulting response.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOIdentifier FloodTransactionIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the FloodTransactionIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FloodTransactionIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool FloodTransactionIdentifierSpecified
        {
            get { return FloodTransactionIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public FLOOD_RESPONSE_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
