namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class FLOOD_RESPONSE
    {
        /// <summary>
        /// Gets a value indicating whether the FLOOD_RESPONSE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.FloodDeterminationSpecified
                    || this.FloodResponseDetailSpecified
                    || this.LoansSpecified
                    || this.PartiesSpecified
                    || this.PropertySpecified;
            }
        }

        /// <summary>
        /// Determination of the flood status of the property.
        /// </summary>
        [XmlElement("FLOOD_DETERMINATION", Order = 0)]
        public FLOOD_DETERMINATION FloodDetermination;

        /// <summary>
        /// Gets or sets a value indicating whether the FloodDetermination element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FloodDetermination element has been assigned a value.</value>
        [XmlIgnore]
        public bool FloodDeterminationSpecified
        {
            get { return this.FloodDetermination != null && this.FloodDetermination.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details on the flood response.
        /// </summary>
        [XmlElement("FLOOD_RESPONSE_DETAIL", Order = 1)]
        public FLOOD_RESPONSE_DETAIL FloodResponseDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the FloodResponseDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FloodResponseDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool FloodResponseDetailSpecified
        {
            get { return this.FloodResponseDetail != null && this.FloodResponseDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The loans for which flood information is needed.
        /// </summary>
        [XmlElement("LOANS", Order = 2)]
        public LOANS Loans;

        /// <summary>
        /// Gets or sets a value indicating whether the Loans element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Loans element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoansSpecified
        {
            get { return this.Loans != null && this.Loans.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The parties who need flood information.
        /// </summary>
        [XmlElement("PARTIES", Order = 3)]
        public PARTIES Parties;

        /// <summary>
        /// Gets or sets a value indicating whether the Parties element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Parties element has been assigned a value.</value>
        [XmlIgnore]
        public bool PartiesSpecified
        {
            get { return this.Parties != null && this.Parties.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The property for which flood data is requested.
        /// </summary>
        [XmlElement("PROPERTY", Order = 4)]
        public PROPERTY Property;

        /// <summary>
        /// Gets or sets a value indicating whether the Property element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Property element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertySpecified
        {
            get { return this.Property != null && this.Property.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 5)]
        public FLOOD_RESPONSE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
