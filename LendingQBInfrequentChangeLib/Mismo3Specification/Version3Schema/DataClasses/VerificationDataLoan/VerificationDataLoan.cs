namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class VERIFICATION_DATA_LOAN
    {
        /// <summary>
        /// Gets a value indicating whether the VERIFICATION_DATA_LOAN container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BankruptcySuspenseBalanceAmountTotalAmountSpecified
                    || this.CurrentBuydownSubsidyAmountTotalAmountSpecified
                    || this.CurrentPrincipalAndInterestPaymentAmountTotalAmountSpecified
                    || this.CurrentTaxAndInsurancePaymentAmountTotalAmountSpecified
                    || this.CurrentTotalOptionalProductsPaymentAmountTotalAmountSpecified
                    || this.CurrentTotalPaymentAmountTotalAmountSpecified
                    || this.DeferredInterestBalanceAmountTotalAmountSpecified
                    || this.EscrowBalanceTotalAmountSpecified
                    || this.ExtensionSpecified
                    || this.HUD235SubsidyPaymentAmountSpecified
                    || this.LoanTotalCountSpecified
                    || this.RecoverableCorporateAdvanceFromBorrowerAmountTotalAmountSpecified
                    || this.RecoverableCorporateAdvanceFromThirdPartyAmountTotalAmountSpecified
                    || this.RemainingBuydownBalanceAmountTotalAmountSpecified
                    || this.RestrictedEscrowBalanceAmountTotalAmountSpecified
                    || this.SuspenseBalanceAmountTotalAmountSpecified
                    || this.UncollectedLateChargeBalanceAmountTotalAmountSpecified
                    || this.UPBTotalAmountSpecified;
            }
        }

        /// <summary>
        /// Provides an arithmetic total of the Bankruptcy Suspense Balance Amount data items within the Loan container in this data set.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount BankruptcySuspenseBalanceAmountTotalAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the BankruptcySuspenseBalanceAmountTotalAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BankruptcySuspenseBalanceAmountTotalAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool BankruptcySuspenseBalanceAmountTotalAmountSpecified
        {
            get { return BankruptcySuspenseBalanceAmountTotalAmount != null; }
            set { }
        }

        /// <summary>
        /// Provides an arithmetic total of the Current Buy down Subsidy Amount data items within the Loan container in this data set where Payment State Type equals Current.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOAmount CurrentBuydownSubsidyAmountTotalAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the CurrentBuyDownSubsidyAmountTotalAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CurrentBuyDownSubsidyAmountTotalAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool CurrentBuydownSubsidyAmountTotalAmountSpecified
        {
            get { return CurrentBuydownSubsidyAmountTotalAmount != null; }
            set { }
        }

        /// <summary>
        /// Provides an arithmetic total of the Current Principal And Interest Payment Amount data items within the Loan container in this data set from the PAYMENT_BREAKOUT container where Payment State Type equals Current.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOAmount CurrentPrincipalAndInterestPaymentAmountTotalAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the CurrentPrincipalAndInterestPaymentAmountTotalAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CurrentPrincipalAndInterestPaymentAmountTotalAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool CurrentPrincipalAndInterestPaymentAmountTotalAmountSpecified
        {
            get { return CurrentPrincipalAndInterestPaymentAmountTotalAmount != null; }
            set { }
        }

        /// <summary>
        /// Provides an arithmetic total of the Current Tax and Insurance Payment Amount data items within the Loan container in this data set from the PAYMENT_BREAKOUT container where Payment State Type equals Current.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOAmount CurrentTaxAndInsurancePaymentAmountTotalAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the CurrentTaxAndInsurancePaymentAmountTotalAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CurrentTaxAndInsurancePaymentAmountTotalAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool CurrentTaxAndInsurancePaymentAmountTotalAmountSpecified
        {
            get { return CurrentTaxAndInsurancePaymentAmountTotalAmount != null; }
            set { }
        }

        /// <summary>
        /// Provides an arithmetic total of the Current Total Optional Products Payment Amount data items within the Loan container in this data set from the PAYMENT_BREAKOUT container where Payment State Type equals Current.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOAmount CurrentTotalOptionalProductsPaymentAmountTotalAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the CurrentTotalOptionalProductsPaymentAmountTotalAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CurrentTotalOptionalProductsPaymentAmountTotalAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool CurrentTotalOptionalProductsPaymentAmountTotalAmountSpecified
        {
            get { return CurrentTotalOptionalProductsPaymentAmountTotalAmount != null; }
            set { }
        }

        /// <summary>
        /// Provides an arithmetic total of the Current Total Payment Amount data items within the Loan container in this data set from the PAYMENT_BREAKOUT container where Payment State Type equals Current.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOAmount CurrentTotalPaymentAmountTotalAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the CurrentTotalPaymentAmountTotalAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CurrentTotalPaymentAmountTotalAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool CurrentTotalPaymentAmountTotalAmountSpecified
        {
            get { return CurrentTotalPaymentAmountTotalAmount != null; }
            set { }
        }

        /// <summary>
        /// Provides a arithmetic total of the Deferred Interest Balance Amount data items within the Loan container in this data set.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOAmount DeferredInterestBalanceAmountTotalAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the DeferredInterestBalanceAmountTotalAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DeferredInterestBalanceAmountTotalAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool DeferredInterestBalanceAmountTotalAmountSpecified
        {
            get { return DeferredInterestBalanceAmountTotalAmount != null; }
            set { }
        }

        /// <summary>
        /// Provides an arithmetic total of the Escrow Balance data items within the Loan container in this data set.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOAmount EscrowBalanceTotalAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowBalanceTotalAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowBalanceTotalAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowBalanceTotalAmountSpecified
        {
            get { return EscrowBalanceTotalAmount != null; }
            set { }
        }

        /// <summary>
        /// The HUD 235 subsidy amount that is part of the total payment being reported.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOAmount HUD235SubsidyPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the HUD235SubsidyPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HUD235SubsidyPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool HUD235SubsidyPaymentAmountSpecified
        {
            get { return HUD235SubsidyPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// Provides a count of the Loan containers within this data set.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOCount LoanTotalCount;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanTotalCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanTotalCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanTotalCountSpecified
        {
            get { return LoanTotalCount != null; }
            set { }
        }

        /// <summary>
        /// Provides an arithmetic total of the Recoverable Corporate Advance from Borrower Amount data items within the Loan container in this data set.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOAmount RecoverableCorporateAdvanceFromBorrowerAmountTotalAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the RecoverableCorporateAdvanceFromBorrowerAmountTotalAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RecoverableCorporateAdvanceFromBorrowerAmountTotalAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool RecoverableCorporateAdvanceFromBorrowerAmountTotalAmountSpecified
        {
            get { return RecoverableCorporateAdvanceFromBorrowerAmountTotalAmount != null; }
            set { }
        }

        /// <summary>
        /// Provides an arithmetic total of the Recoverable Corporate Advance From Third Party Amount data items within the Loan container in this data set.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOAmount RecoverableCorporateAdvanceFromThirdPartyAmountTotalAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the RecoverableCorporateAdvanceFromThirdPartyAmountTotalAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RecoverableCorporateAdvanceFromThirdPartyAmountTotalAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool RecoverableCorporateAdvanceFromThirdPartyAmountTotalAmountSpecified
        {
            get { return RecoverableCorporateAdvanceFromThirdPartyAmountTotalAmount != null; }
            set { }
        }

        /// <summary>
        /// Provides an arithmetic total of the Remaining Buy down Balance Amount data items within the Loan container in this data set.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOAmount RemainingBuydownBalanceAmountTotalAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the RemainingBuyDownBalanceAmountTotalAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RemainingBuyDownBalanceAmountTotalAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool RemainingBuydownBalanceAmountTotalAmountSpecified
        {
            get { return RemainingBuydownBalanceAmountTotalAmount != null; }
            set { }
        }

        /// <summary>
        /// Provides an arithmetic total of the Restricted Escrow Balance Amount data items within the Loan container in this data set.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOAmount RestrictedEscrowBalanceAmountTotalAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the RestrictedEscrowBalanceAmountTotalAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RestrictedEscrowBalanceAmountTotalAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool RestrictedEscrowBalanceAmountTotalAmountSpecified
        {
            get { return RestrictedEscrowBalanceAmountTotalAmount != null; }
            set { }
        }

        /// <summary>
        /// Provides an arithmetic total of the Suspense Balance Amount data items within the Loan container in this data set.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOAmount SuspenseBalanceAmountTotalAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the SuspenseBalanceAmountTotalAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SuspenseBalanceAmountTotalAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool SuspenseBalanceAmountTotalAmountSpecified
        {
            get { return SuspenseBalanceAmountTotalAmount != null; }
            set { }
        }

        /// <summary>
        /// Provides an arithmetic total of the Uncollected Late Charge Balance Amount data items within the Loan container in this data set.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOAmount UncollectedLateChargeBalanceAmountTotalAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the UncollectedLateChargeBalanceAmountTotalAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the UncollectedLateChargeBalanceAmountTotalAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool UncollectedLateChargeBalanceAmountTotalAmountSpecified
        {
            get { return UncollectedLateChargeBalanceAmountTotalAmount != null; }
            set { }
        }

        /// <summary>
        /// Provides an arithmetic total of the Unpaid Principal Balance data items within the Loan container in this data set.
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMOAmount UPBTotalAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the UPBTotalAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the UPBTotalAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool UPBTotalAmountSpecified
        {
            get { return UPBTotalAmount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 17)]
        public VERIFICATION_DATA_LOAN_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
