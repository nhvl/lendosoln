namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class UNIT_CHARGE
    {
        /// <summary>
        /// Gets a value indicating whether the UNIT_CHARGE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.UnitChargeDetailSpecified
                    || this.UnitChargeUtilitiesSpecified;
            }
        }

        /// <summary>
        /// Details on a unit charge.
        /// </summary>
        [XmlElement("UNIT_CHARGE_DETAIL", Order = 0)]
        public UNIT_CHARGE_DETAIL UnitChargeDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the UnitChargeDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the UnitChargeDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool UnitChargeDetailSpecified
        {
            get { return this.UnitChargeDetail != null && this.UnitChargeDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A list of unit charges for utilities.
        /// </summary>
        [XmlElement("UNIT_CHARGE_UTILITIES", Order = 1)]
        public UNIT_CHARGE_UTILITIES UnitChargeUtilities;

        /// <summary>
        /// Gets or sets a value indicating whether the UnitChargeUtilities element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the UnitChargeUtilities element has been assigned a value.</value>
        [XmlIgnore]
        public bool UnitChargeUtilitiesSpecified
        {
            get { return this.UnitChargeUtilities != null && this.UnitChargeUtilities.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public UNIT_CHARGE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
