namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class UNIT_CHARGE_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the UNIT_CHARGE_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.UnitChargeAmountSpecified
                    || this.UnitChargePeriodTypeSpecified
                    || this.UnitChargePerSquareFootAmountSpecified
                    || this.UnitChargeRatingTypeOtherDescriptionSpecified
                    || this.UnitChargeRatingTypeSpecified
                    || this.UnitChargesIncludeUtilitiesIndicatorSpecified;
            }
        }

        /// <summary>
        /// The amount the unit is charged every period described by Period Type.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount UnitChargeAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the UnitChargeAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the UnitChargeAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool UnitChargeAmountSpecified
        {
            get { return UnitChargeAmount != null; }
            set { }
        }

        /// <summary>
        /// Specifies the period about which the fees per unit apply.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<UnitChargePeriodBase> UnitChargePeriodType;

        /// <summary>
        /// Gets or sets a value indicating whether the UnitChargePeriodType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the UnitChargePeriodType element has been assigned a value.</value>
        [XmlIgnore]
        public bool UnitChargePeriodTypeSpecified
        {
            get { return this.UnitChargePeriodType != null && this.UnitChargePeriodType.enumValue != UnitChargePeriodBase.Blank; }
            set { }
        }

        /// <summary>
        /// Assessment charge per square feet of gross living area of the unit. This is generally used for analysis of unit charges in a multiple unit project.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOAmount UnitChargePerSquareFootAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the UnitChargePerSquareFootAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the UnitChargePerSquareFootAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool UnitChargePerSquareFootAmountSpecified
        {
            get { return UnitChargePerSquareFootAmount != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether unit assessment charges in comparison with other units in comparable projects is high, low or typical.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOEnum<UnitChargeRatingBase> UnitChargeRatingType;

        /// <summary>
        /// Gets or sets a value indicating whether the UnitChargeRatingType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the UnitChargeRatingType element has been assigned a value.</value>
        [XmlIgnore]
        public bool UnitChargeRatingTypeSpecified
        {
            get { return this.UnitChargeRatingType != null && this.UnitChargeRatingType.enumValue != UnitChargeRatingBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe assessment charges in comparison to other units when Other is selected as the Unit Charge Rating Type.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString UnitChargeRatingTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the UnitChargeRatingTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the UnitChargeRatingTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool UnitChargeRatingTypeOtherDescriptionSpecified
        {
            get { return UnitChargeRatingTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the assessment charges for a unit includes one or more utilities charges.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOIndicator UnitChargesIncludeUtilitiesIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the UnitChargesIncludeUtilitiesIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the UnitChargesIncludeUtilitiesIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool UnitChargesIncludeUtilitiesIndicatorSpecified
        {
            get { return UnitChargesIncludeUtilitiesIndicator != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 6)]
        public UNIT_CHARGE_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
