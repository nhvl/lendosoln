namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CONSTRUCTION
    {
        /// <summary>
        /// Gets a value indicating whether the CONSTRUCTION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ConstructionImprovementCostsAmountSpecified
                    || this.ConstructionLoanEstimatedInterestCalculationMethodTypeSpecified
                    || this.ConstructionLoanInterestReserveAmountSpecified
                    || this.ConstructionLoanTotalTermMonthsCountSpecified
                    || this.ConstructionLoanTypeSpecified
                    || this.ConstructionPeriodInterestRatePercentSpecified
                    || this.ConstructionPeriodNumberOfMonthsCountSpecified
                    || this.ConstructionPhaseInterestPaymentFrequencyTypeOtherDescriptionSpecified
                    || this.ConstructionPhaseInterestPaymentFrequencyTypeSpecified
                    || this.ConstructionPhaseInterestPaymentMethodTypeOtherDescriptionSpecified
                    || this.ConstructionPhaseInterestPaymentMethodTypeSpecified
                    || this.ConstructionPhaseInterestPaymentTypeOtherDescriptionSpecified
                    || this.ConstructionPhaseInterestPaymentTypeSpecified
                    || this.ConstructionToPermanentClosingFeatureTypeOtherDescriptionSpecified
                    || this.ConstructionToPermanentClosingFeatureTypeSpecified
                    || this.ConstructionToPermanentClosingTypeOtherDescriptionSpecified
                    || this.ConstructionToPermanentClosingTypeSpecified
                    || this.ConstructionToPermanentFirstPaymentDueDateSpecified
                    || this.ConstructionToPermanentRecertificationDateSpecified
                    || this.ConstructionToPermanentRecertificationValueAmountSpecified
                    || this.ExtensionSpecified
                    || this.LandAppraisedValueAmountSpecified
                    || this.LandEstimatedValueAmountSpecified
                    || this.LandOriginalCostAmountSpecified;
            }
        }

        /// <summary>
        /// The total dollar amount of additions to raw land that normally increase its value (i.e., the building) as part of a construction loan. Collected on the URLA in Section II (Construction Loan - Cost of Improvements.).
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount ConstructionImprovementCostsAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ConstructionImprovementCostsAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConstructionImprovementCostsAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConstructionImprovementCostsAmountSpecified
        {
            get { return ConstructionImprovementCostsAmount != null; }
            set { }
        }

        /// <summary>
        /// The basis for calculating the amount of estimated interest on the construction loan.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<ConstructionLoanEstimatedInterestCalculationMethodBase> ConstructionLoanEstimatedInterestCalculationMethodType;

        /// <summary>
        /// Gets or sets a value indicating whether the ConstructionLoanEstimatedInterestCalculationMethodType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConstructionLoanEstimatedInterestCalculationMethodType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConstructionLoanEstimatedInterestCalculationMethodTypeSpecified
        {
            get { return this.ConstructionLoanEstimatedInterestCalculationMethodType != null && this.ConstructionLoanEstimatedInterestCalculationMethodType.enumValue != ConstructionLoanEstimatedInterestCalculationMethodBase.Blank; }
            set { }
        }

        /// <summary>
        /// The reserve amount required by the lender to pay interest on advance amount used for construction.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOAmount ConstructionLoanInterestReserveAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ConstructionLoanInterestReserveAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConstructionLoanInterestReserveAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConstructionLoanInterestReserveAmountSpecified
        {
            get { return ConstructionLoanInterestReserveAmount != null; }
            set { }
        }

        /// <summary>
        /// The total term of a construction loan, including the construction phase plus the fully amortizing phase.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOCount ConstructionLoanTotalTermMonthsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the ConstructionLoanTotalTermMonthsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConstructionLoanTotalTermMonthsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConstructionLoanTotalTermMonthsCountSpecified
        {
            get { return ConstructionLoanTotalTermMonthsCount != null; }
            set { }
        }

        /// <summary>
        /// Specifies the specific type of construction loan.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<ConstructionLoanBase> ConstructionLoanType;

        /// <summary>
        /// Gets or sets a value indicating whether the ConstructionLoanType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConstructionLoanType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConstructionLoanTypeSpecified
        {
            get { return this.ConstructionLoanType != null && this.ConstructionLoanType.enumValue != ConstructionLoanBase.Blank; }
            set { }
        }

        /// <summary>
        /// The interest rate to be used for calculations during construction period.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOPercent ConstructionPeriodInterestRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the ConstructionPeriodInterestRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConstructionPeriodInterestRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConstructionPeriodInterestRatePercentSpecified
        {
            get { return ConstructionPeriodInterestRatePercent != null; }
            set { }
        }

        /// <summary>
        /// The length of the construction period expressed in number of months.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOCount ConstructionPeriodNumberOfMonthsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the ConstructionPeriodNumberOfMonthsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConstructionPeriodNumberOfMonthsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConstructionPeriodNumberOfMonthsCountSpecified
        {
            get { return ConstructionPeriodNumberOfMonthsCount != null; }
            set { }
        }

        /// <summary>
        /// How frequently interest payments are due during the construction phase.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOEnum<ConstructionPhaseInterestPaymentFrequencyBase> ConstructionPhaseInterestPaymentFrequencyType;

        /// <summary>
        /// Gets or sets a value indicating whether the ConstructionPhaseInterestPaymentFrequencyType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConstructionPhaseInterestPaymentFrequencyType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConstructionPhaseInterestPaymentFrequencyTypeSpecified
        {
            get { return this.ConstructionPhaseInterestPaymentFrequencyType != null && this.ConstructionPhaseInterestPaymentFrequencyType.enumValue != ConstructionPhaseInterestPaymentFrequencyBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the construction phase interest payment frequency if Other is selected as the Construction Phase Interest Payment Frequency Type.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOString ConstructionPhaseInterestPaymentFrequencyTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ConstructionPhaseInterestPaymentFrequencyTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConstructionPhaseInterestPaymentFrequencyTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConstructionPhaseInterestPaymentFrequencyTypeOtherDescriptionSpecified
        {
            get { return ConstructionPhaseInterestPaymentFrequencyTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// How are the construction phase interest payments paid.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOEnum<ConstructionPhaseInterestPaymentMethodBase> ConstructionPhaseInterestPaymentMethodType;

        /// <summary>
        /// Gets or sets a value indicating whether the ConstructionPhaseInterestPaymentMethodType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConstructionPhaseInterestPaymentMethodType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConstructionPhaseInterestPaymentMethodTypeSpecified
        {
            get { return this.ConstructionPhaseInterestPaymentMethodType != null && this.ConstructionPhaseInterestPaymentMethodType.enumValue != ConstructionPhaseInterestPaymentMethodBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the construction phase interest payment method if Other is selected as the Construction Phase Interest Payment Method Type.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOString ConstructionPhaseInterestPaymentMethodTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ConstructionPhaseInterestPaymentMethodTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConstructionPhaseInterestPaymentMethodTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConstructionPhaseInterestPaymentMethodTypeOtherDescriptionSpecified
        {
            get { return ConstructionPhaseInterestPaymentMethodTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Represents whether interest is payable periodically or at the end of construction.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOEnum<ConstructionPhaseInterestPaymentBase> ConstructionPhaseInterestPaymentType;

        /// <summary>
        /// Gets or sets a value indicating whether the ConstructionPhaseInterestPaymentType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConstructionPhaseInterestPaymentType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConstructionPhaseInterestPaymentTypeSpecified
        {
            get { return this.ConstructionPhaseInterestPaymentType != null && this.ConstructionPhaseInterestPaymentType.enumValue != ConstructionPhaseInterestPaymentBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the construction phase interest payment if Other is selected as the Construction Phase Interest Payment Type.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOString ConstructionPhaseInterestPaymentTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ConstructionPhaseInterestPaymentTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConstructionPhaseInterestPaymentTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConstructionPhaseInterestPaymentTypeOtherDescriptionSpecified
        {
            get { return ConstructionPhaseInterestPaymentTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the type of feature associated with closing for the Construction To Permanent loan.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOEnum<ConstructionToPermanentClosingFeatureBase> ConstructionToPermanentClosingFeatureType;

        /// <summary>
        /// Gets or sets a value indicating whether the ConstructionToPermanentClosingFeatureType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConstructionToPermanentClosingFeatureType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConstructionToPermanentClosingFeatureTypeSpecified
        {
            get { return this.ConstructionToPermanentClosingFeatureType != null && this.ConstructionToPermanentClosingFeatureType.enumValue != ConstructionToPermanentClosingFeatureBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Construction To Permanent Closing Feature Type.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOString ConstructionToPermanentClosingFeatureTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ConstructionToPermanentClosingFeatureTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConstructionToPermanentClosingFeatureTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConstructionToPermanentClosingFeatureTypeOtherDescriptionSpecified
        {
            get { return ConstructionToPermanentClosingFeatureTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the type of closing for the Construction to Permanent loan.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOEnum<ConstructionToPermanentClosingBase> ConstructionToPermanentClosingType;

        /// <summary>
        /// Gets or sets a value indicating whether the ConstructionToPermanentClosingType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConstructionToPermanentClosingType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConstructionToPermanentClosingTypeSpecified
        {
            get { return this.ConstructionToPermanentClosingType != null && this.ConstructionToPermanentClosingType.enumValue != ConstructionToPermanentClosingBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Construction To Permanent Closing Type.
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMOString ConstructionToPermanentClosingTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ConstructionToPermanentClosingTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConstructionToPermanentClosingTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConstructionToPermanentClosingTypeOtherDescriptionSpecified
        {
            get { return ConstructionToPermanentClosingTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The due date of the first payment of the permanent mortgage phase of a construction to permanent loan.
        /// </summary>
        [XmlElement(Order = 17)]
        public MISMODate ConstructionToPermanentFirstPaymentDueDate;

        /// <summary>
        /// Gets or sets a value indicating whether the ConstructionToPermanentFirstPaymentDueDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConstructionToPermanentFirstPaymentDueDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConstructionToPermanentFirstPaymentDueDateSpecified
        {
            get { return ConstructionToPermanentFirstPaymentDueDate != null; }
            set { }
        }

        /// <summary>
        /// The date when the value of the property is reconfirmed or re-established upon completion of the construction as part of the conversion to permanent financing.
        /// </summary>
        [XmlElement(Order = 18)]
        public MISMODate ConstructionToPermanentRecertificationDate;

        /// <summary>
        /// Gets or sets a value indicating whether the ConstructionToPermanentRecertificationDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConstructionToPermanentRecertificationDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConstructionToPermanentRecertificationDateSpecified
        {
            get { return ConstructionToPermanentRecertificationDate != null; }
            set { }
        }

        /// <summary>
        /// The reconfirmed or re-established property value upon completion of the construction as part of the conversion to permanent financing.
        /// </summary>
        [XmlElement(Order = 19)]
        public MISMOAmount ConstructionToPermanentRecertificationValueAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ConstructionToPermanentRecertificationValueAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConstructionToPermanentRecertificationValueAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConstructionToPermanentRecertificationValueAmountSpecified
        {
            get { return ConstructionToPermanentRecertificationValueAmount != null; }
            set { }
        }

        /// <summary>
        /// The value of the land as stated in the appraisal.
        /// </summary>
        [XmlElement(Order = 20)]
        public MISMOAmount LandAppraisedValueAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the LandAppraisedValueAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LandAppraisedValueAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool LandAppraisedValueAmountSpecified
        {
            get { return LandAppraisedValueAmount != null; }
            set { }
        }

        /// <summary>
        /// This is the estimated present value of the land at the time of the loan application. It assumes that the valuation comes from the borrower, lender, or someone other than a professional appraiser. This value is used for construction loans.
        /// </summary>
        [XmlElement(Order = 21)]
        public MISMOAmount LandEstimatedValueAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the LandEstimatedValueAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LandEstimatedValueAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool LandEstimatedValueAmountSpecified
        {
            get { return LandEstimatedValueAmount != null; }
            set { }
        }

        /// <summary>
        /// The original cost of acquiring the land on which the home will be built. This is used for purchase, construction and refinance loans.
        /// </summary>
        [XmlElement(Order = 22)]
        public MISMOAmount LandOriginalCostAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the LandOriginalCostAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LandOriginalCostAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool LandOriginalCostAmountSpecified
        {
            get { return LandOriginalCostAmount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 23)]
        public CONSTRUCTION_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
