namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class FLOOD_REQUEST_DISPUTE
    {
        /// <summary>
        /// Gets a value indicating whether the FLOOD_REQUEST_DISPUTE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.FloodRequestDisputeDescriptionSpecified
                    || this.FloodRequestDisputeItemTypeOtherDescriptionSpecified
                    || this.FloodRequestDisputeItemTypeSpecified
                    || this.FloodRequestDisputeSupportingDocumentsDescriptionSpecified;
            }
        }

        /// <summary>
        /// Description of the reason for the flood dispute.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString FloodRequestDisputeDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the FloodRequestDisputeDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FloodRequestDisputeDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool FloodRequestDisputeDescriptionSpecified
        {
            get { return FloodRequestDisputeDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the field on the FEMA document which is being disputed.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<FloodRequestDisputeItemBase> FloodRequestDisputeItemType;

        /// <summary>
        /// Gets or sets a value indicating whether the FloodRequestDisputeItemType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FloodRequestDisputeItemType element has been assigned a value.</value>
        [XmlIgnore]
        public bool FloodRequestDisputeItemTypeSpecified
        {
            get { return this.FloodRequestDisputeItemType != null && this.FloodRequestDisputeItemType.enumValue != FloodRequestDisputeItemBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the Other option in the flood request dispute item.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString FloodRequestDisputeItemTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the FloodRequestDisputeItemTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FloodRequestDisputeItemTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool FloodRequestDisputeItemTypeOtherDescriptionSpecified
        {
            get { return FloodRequestDisputeItemTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A description of the document available that supports the flood dispute.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString FloodRequestDisputeSupportingDocumentsDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the FloodRequestDisputeSupportingDocumentsDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FloodRequestDisputeSupportingDocumentsDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool FloodRequestDisputeSupportingDocumentsDescriptionSpecified
        {
            get { return FloodRequestDisputeSupportingDocumentsDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public FLOOD_REQUEST_DISPUTE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
