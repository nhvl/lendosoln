namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CONTACT_POINT_TELEPHONE
    {
        /// <summary>
        /// Gets a value indicating whether the CONTACT_POINT_TELEPHONE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                if (Mismo3Utilities.ExceedsThreshold(1,
                    new List<bool> { 
                        this.ContactPointFaxExtensionValueSpecified || this.ContactPointFaxValueSpecified, 
                        this.ContactPointTelephoneExtensionValueSpecified || this.ContactPointTelephoneValueSpecified }))
                {
                    throw new System.Xml.Schema.XmlSchemaValidationException(
                        Mismo3Utilities.GetXSDChoiceExceptionMessage(
                        "CONTACT_POINT_TELEPHONE",
                        new List<string> { "ContactPointFaxExtensionValue and/or ContactPointFaxValue", "ContactPointTelephoneExtensionValue and/or ContactPointTelephoneValue" }));
                }

                return this.ContactPointFaxExtensionValueSpecified
                    || this.ContactPointFaxValueSpecified
                    || this.ContactPointTelephoneExtensionValueSpecified
                    || this.ContactPointTelephoneValueSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// The extension associated with the specified fax number. 
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMONumericString ContactPointFaxExtensionValue;

        /// <summary>
        /// Gets or sets a value indicating whether the ContactPointFaxExtensionValue element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ContactPointFaxExtensionValue element has been assigned a value.</value>
        [XmlIgnore]
        public bool ContactPointFaxExtensionValueSpecified
        {
            get { return ContactPointFaxExtensionValue != null; }
            set { }
        }

        /// <summary>
        /// The fax number for the contact.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMONumericString ContactPointFaxValue;

        /// <summary>
        /// Gets or sets a value indicating whether the ContactPointFaxValue element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ContactPointFaxValue element has been assigned a value.</value>
        [XmlIgnore]
        public bool ContactPointFaxValueSpecified
        {
            get { return ContactPointFaxValue != null; }
            set { }
        }

        /// <summary>
        /// The extension associated with the specified telephone number. 
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMONumericString ContactPointTelephoneExtensionValue;

        /// <summary>
        /// Gets or sets a value indicating whether the ContactPointTelephoneExtensionValue element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ContactPointTelephoneExtensionValue element has been assigned a value.</value>
        [XmlIgnore]
        public bool ContactPointTelephoneExtensionValueSpecified
        {
            get { return ContactPointTelephoneExtensionValue != null; }
            set { }
        }

        /// <summary>
        /// A telephone contact point.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMONumericString ContactPointTelephoneValue;

        /// <summary>
        /// Gets or sets a value indicating whether the ContactPointTelephoneValue element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ContactPointTelephoneValue element has been assigned a value.</value>
        [XmlIgnore]
        public bool ContactPointTelephoneValueSpecified
        {
            get { return ContactPointTelephoneValue != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public CONTACT_POINT_TELEPHONE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
