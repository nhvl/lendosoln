namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PAYMENT_SUMMARY
    {
        /// <summary>
        /// Gets a value indicating whether the PAYMENT_SUMMARY container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AggregateLoanCurtailmentAmountSpecified
                    || this.DefaultedFirstPaymentIndicatorSpecified
                    || this.ExtensionSpecified
                    || this.InterestBearingUPBAmountSpecified
                    || this.InterestPaidThroughDateSpecified
                    || this.LastPaidInstallmentDueDateSpecified
                    || this.LastPaymentReceivedDateSpecified
                    || this.NextPaymentDueDateSpecified
                    || this.NonInterestBearingUPBAmountSpecified
                    || this.PaymentHistoryPatternTextSpecified
                    || this.PostPetitionNextPaymentDueDateSpecified
                    || this.ScheduledUPBAmountSpecified
                    || this.ServicingTerminationUPBAmountSpecified
                    || this.TotalPITIMonthsCountSpecified
                    || this.TotalPITIReceivedAmountSpecified
                    || this.UPBAmountSpecified
                    || this.YearToDatePrincipalPaidAmountSpecified
                    || this.YearToDateTaxesPaidAmountSpecified
                    || this.YearToDateTotalInterestPaidAmountSpecified;
            }
        }

        /// <summary>
        /// The total amount of principal that has been paid from origination to date over and above the scheduled principal amount.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount AggregateLoanCurtailmentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the AggregateLoanCurtailmentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AggregateLoanCurtailmentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool AggregateLoanCurtailmentAmountSpecified
        {
            get { return AggregateLoanCurtailmentAmount != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether the loan experienced a first payment default. 
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOIndicator DefaultedFirstPaymentIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the DefaultedFirstPaymentIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DefaultedFirstPaymentIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool DefaultedFirstPaymentIndicatorSpecified
        {
            get { return DefaultedFirstPaymentIndicator != null; }
            set { }
        }

        /// <summary>
        /// The portion of the UPB on which interest is being collected.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOAmount InterestBearingUPBAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the InterestBearingUPBAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InterestBearingUPBAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool InterestBearingUPBAmountSpecified
        {
            get { return InterestBearingUPBAmount != null; }
            set { }
        }

        /// <summary>
        /// The date through which interest is paid with the current payment. This is the effective date from which interest will be calculated for the application of the next payment. (For example, used for daily simple interest loans.).
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMODate InterestPaidThroughDate;

        /// <summary>
        /// Gets or sets a value indicating whether the InterestPaidThroughDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InterestPaidThroughDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool InterestPaidThroughDateSpecified
        {
            get { return InterestPaidThroughDate != null; }
            set { }
        }

        /// <summary>
        /// The due date of last paid installment (DDLPI) that had been collected for the mortgage.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMODate LastPaidInstallmentDueDate;

        /// <summary>
        /// Gets or sets a value indicating whether the LastPaidInstallmentDueDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LastPaidInstallmentDueDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool LastPaidInstallmentDueDateSpecified
        {
            get { return LastPaidInstallmentDueDate != null; }
            set { }
        }

        /// <summary>
        /// The actual date the last payment by the borrower was received by the lender.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMODate LastPaymentReceivedDate;

        /// <summary>
        /// Gets or sets a value indicating whether the LastPaymentReceivedDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LastPaymentReceivedDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool LastPaymentReceivedDateSpecified
        {
            get { return LastPaymentReceivedDate != null; }
            set { }
        }

        /// <summary>
        /// Identifies the next payment due date.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMODate NextPaymentDueDate;

        /// <summary>
        /// Gets or sets a value indicating whether the NextPaymentDueDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NextPaymentDueDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool NextPaymentDueDateSpecified
        {
            get { return NextPaymentDueDate != null; }
            set { }
        }

        /// <summary>
        /// The portion of the UPB on which interest is no longer being collected.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOAmount NonInterestBearingUPBAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the NonInterestBearingUPBAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NonInterestBearingUPBAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool NonInterestBearingUPBAmountSpecified
        {
            get { return NonInterestBearingUPBAmount != null; }
            set { }
        }

        /// <summary>
        /// A representation of the history of last twelve payments based on a Twelve month calendar beginning with January.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOString PaymentHistoryPatternText;

        /// <summary>
        /// Gets or sets a value indicating whether the PaymentHistoryPatternText element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PaymentHistoryPatternText element has been assigned a value.</value>
        [XmlIgnore]
        public bool PaymentHistoryPatternTextSpecified
        {
            get { return PaymentHistoryPatternText != null; }
            set { }
        }

        /// <summary>
        /// Payment due date based on the bankruptcy plan confirmation.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMODate PostPetitionNextPaymentDueDate;

        /// <summary>
        /// Gets or sets a value indicating whether the PostPetitionNextPaymentDueDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PostPetitionNextPaymentDueDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool PostPetitionNextPaymentDueDateSpecified
        {
            get { return PostPetitionNextPaymentDueDate != null; }
            set { }
        }

        /// <summary>
        /// The unpaid principal balance on the loan based on an agreed upon schedule.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOAmount ScheduledUPBAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ScheduledUPBAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ScheduledUPBAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ScheduledUPBAmountSpecified
        {
            get { return ScheduledUPBAmount != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of the scheduled unpaid principal balance as of the termination date of the servicing agreement.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOAmount ServicingTerminationUPBAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ServicingTerminationUPBAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServicingTerminationUPBAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServicingTerminationUPBAmountSpecified
        {
            get { return ServicingTerminationUPBAmount != null; }
            set { }
        }

        /// <summary>
        /// The duration in months of the period for which the Total PITI Received Amount is provided.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOCount TotalPITIMonthsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the TotalPITIMonthsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TotalPITIMonthsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TotalPITIMonthsCountSpecified
        {
            get { return TotalPITIMonthsCount != null; }
            set { }
        }

        /// <summary>
        /// The total amount of PITI received within the number of months specified in Total PITI Months Count.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOAmount TotalPITIReceivedAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the TotalPITIReceivedAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TotalPITIReceivedAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TotalPITIReceivedAmountSpecified
        {
            get { return TotalPITIReceivedAmount != null; }
            set { }
        }

        /// <summary>
        /// The current unpaid principal balance on the loan.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOAmount UPBAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the UPBAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the UPBAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool UPBAmountSpecified
        {
            get { return UPBAmount != null; }
            set { }
        }

        /// <summary>
        /// Year To Date Total Principal Paid Balance.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOAmount YearToDatePrincipalPaidAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the YearToDatePrincipalPaidAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the YearToDatePrincipalPaidAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool YearToDatePrincipalPaidAmountSpecified
        {
            get { return YearToDatePrincipalPaidAmount != null; }
            set { }
        }

        /// <summary>
        /// Year To Date Total Taxes Paid Balance.
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMOAmount YearToDateTaxesPaidAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the YearToDateTaxesPaidAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the YearToDateTaxesPaidAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool YearToDateTaxesPaidAmountSpecified
        {
            get { return YearToDateTaxesPaidAmount != null; }
            set { }
        }

        /// <summary>
        /// Year To Date Total Interest Paid Amount.
        /// </summary>
        [XmlElement(Order = 17)]
        public MISMOAmount YearToDateTotalInterestPaidAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the YearToDateTotalInterestPaidAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the YearToDateTotalInterestPaidAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool YearToDateTotalInterestPaidAmountSpecified
        {
            get { return YearToDateTotalInterestPaidAmount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 18)]
        public PAYMENT_SUMMARY_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
