namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class TAXPAYER_IDENTIFIER
    {
        /// <summary>
        /// Gets a value indicating whether the TAXPAYER_IDENTIFIER container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CanadianSocialInsuranceNumberIdentifierSpecified
                    || this.ExtensionSpecified
                    || this.SSNCertificationIndicatorSpecified
                    || this.TaxpayerIdentifierTypeSpecified
                    || this.TaxpayerIdentifierValueSpecified;
            }
        }

        /// <summary>
        /// The Canadian Social Insurance Number associated with the party.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIdentifier CanadianSocialInsuranceNumberIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the CanadianSocialInsuranceNumberIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CanadianSocialInsuranceNumberIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool CanadianSocialInsuranceNumberIdentifierSpecified
        {
            get { return CanadianSocialInsuranceNumberIdentifier != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that the Social Security Number has been certified.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOIndicator SSNCertificationIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the SSNCertificationIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SSNCertificationIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool SSNCertificationIndicatorSpecified
        {
            get { return SSNCertificationIndicator != null; }
            set { }
        }

        /// <summary>
        /// Specifies the type of identification number used by the Internal Revenue Service (IRS) in the administration of tax laws. It is issued either by the Social Security Administration (SSA) or the IRS. A Social Security number (SSN) is issued by the SSA; all other taxpayer identification numbers are issued by the IRS.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<TaxpayerIdentifierBase> TaxpayerIdentifierType;

        /// <summary>
        /// Gets or sets a value indicating whether the TaxpayerIdentifierType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TaxpayerIdentifierType element has been assigned a value.</value>
        [XmlIgnore]
        public bool TaxpayerIdentifierTypeSpecified
        {
            get { return this.TaxpayerIdentifierType != null && this.TaxpayerIdentifierType.enumValue != TaxpayerIdentifierBase.Blank; }
            set { }
        }

        /// <summary>
        /// The value of the taxpayer identifier as assigned by the IRS to the individual or legal entity.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMONumericString TaxpayerIdentifierValue;

        /// <summary>
        /// Gets or sets a value indicating whether the TaxpayerIdentifierValue element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TaxpayerIdentifierValue element has been assigned a value.</value>
        [XmlIgnore]
        public bool TaxpayerIdentifierValueSpecified
        {
            get { return TaxpayerIdentifierValue != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public TAXPAYER_IDENTIFIER_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
