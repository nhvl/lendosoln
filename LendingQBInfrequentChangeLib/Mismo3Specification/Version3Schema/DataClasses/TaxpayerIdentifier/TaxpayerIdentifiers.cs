namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class TAXPAYER_IDENTIFIERS
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TAXPAYER_IDENTIFIERS" /> class.
        /// </summary>
        public TAXPAYER_IDENTIFIERS()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TAXPAYER_IDENTIFIERS" /> class. Adds a single identifier to the list of identifiers.
        /// </summary>
        /// <param name="taxpayerIdentifier">The taxpayer identifier to add to the list of identifiers.</param>
        public TAXPAYER_IDENTIFIERS(TAXPAYER_IDENTIFIER taxpayerIdentifier)
        {
            this.TaxpayerIdentifier.Add(taxpayerIdentifier);
        }

        /// <summary>
        /// Gets a value indicating whether the TAXPAYER_IDENTIFIERS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.TaxpayerIdentifierSpecified;
            }
        }

        /// <summary>
        /// A list of taxpayer identifiers.
        /// </summary>
        [XmlElement("TAXPAYER_IDENTIFIER", Order = 0)]
		public List<TAXPAYER_IDENTIFIER> TaxpayerIdentifier = new List<TAXPAYER_IDENTIFIER>();

        /// <summary>
        /// Gets or sets a value indicating whether the TaxpayerIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TaxpayerIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool TaxpayerIdentifierSpecified
        {
            get { return this.TaxpayerIdentifier != null && this.TaxpayerIdentifier.Count(t => t != null && t.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public TAXPAYER_IDENTIFIERS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
