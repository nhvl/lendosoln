namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class TRUST
    {
        /// <summary>
        /// Gets a value indicating whether the TRUST container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.TrustClassificationTypeOtherDescriptionSpecified
                    || this.TrustClassificationTypeSpecified
                    || this.TrustEstablishedDateSpecified
                    || this.TrustEstablishedStateNameSpecified;
            }
        }

        /// <summary>
        /// Defines the specific category or type of trust.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<TrustClassificationBase> TrustClassificationType;

        /// <summary>
        /// Gets or sets a value indicating whether the TrustClassificationType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TrustClassificationType element has been assigned a value.</value>
        [XmlIgnore]
        public bool TrustClassificationTypeSpecified
        {
            get { return this.TrustClassificationType != null && this.TrustClassificationType.enumValue != TrustClassificationBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when other is selected for Trust Classification Type.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString TrustClassificationTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the TrustClassificationTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TrustClassificationTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool TrustClassificationTypeOtherDescriptionSpecified
        {
            get { return TrustClassificationTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The date the Trust was established.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMODate TrustEstablishedDate;

        /// <summary>
        /// Gets or sets a value indicating whether the TrustEstablishedDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TrustEstablishedDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool TrustEstablishedDateSpecified
        {
            get { return TrustEstablishedDate != null; }
            set { }
        }

        /// <summary>
        /// The state in which the Trust was established.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString TrustEstablishedStateName;

        /// <summary>
        /// Gets or sets a value indicating whether the TrustEstablishedStateName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TrustEstablishedStateName element has been assigned a value.</value>
        [XmlIgnore]
        public bool TrustEstablishedStateNameSpecified
        {
            get { return TrustEstablishedStateName != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public TRUST_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
