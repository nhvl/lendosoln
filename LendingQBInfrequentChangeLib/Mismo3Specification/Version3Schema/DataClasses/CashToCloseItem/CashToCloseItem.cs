namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class CASH_TO_CLOSE_ITEM
    {
        /// <summary>
        /// Gets a value indicating whether the CASH_TO_CLOSE_ITEM container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.IntegratedDisclosureCashToCloseItemAmountChangedIndicatorSpecified
                    || this.IntegratedDisclosureCashToCloseItemChangeDescriptionSpecified
                    || this.IntegratedDisclosureCashToCloseItemEstimatedAmountSpecified
                    || this.IntegratedDisclosureCashToCloseItemFinalAmountSpecified
                    || this.IntegratedDisclosureCashToCloseItemPaymentTypeSpecified
                    || this.IntegratedDisclosureCashToCloseItemTypeSpecified;
            }
        }

        /// <summary>
        /// When true, indicates the estimated and final cash to close amounts for the specified Integrated Disclosure Cash To Close Item Type are unequal.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIndicator IntegratedDisclosureCashToCloseItemAmountChangedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the IntegratedDisclosureCashToCloseItemAmountChangedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IntegratedDisclosureCashToCloseItemAmountChangedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool IntegratedDisclosureCashToCloseItemAmountChangedIndicatorSpecified
        {
            get { return IntegratedDisclosureCashToCloseItemAmountChangedIndicator != null; }
            set { }
        }

        /// <summary>
        /// A statement providing information related to the difference between the estimated and final cash to close amounts for the specified Integrated Disclosure Cash To Close Item Type.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString IntegratedDisclosureCashToCloseItemChangeDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the IntegratedDisclosureCashToCloseItemChangeDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IntegratedDisclosureCashToCloseItemChangeDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool IntegratedDisclosureCashToCloseItemChangeDescriptionSpecified
        {
            get { return IntegratedDisclosureCashToCloseItemChangeDescription != null; }
            set { }
        }

        /// <summary>
        /// The estimated cash to close amount for the specified integrated Disclosure Cash To Close Item Type.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOAmount IntegratedDisclosureCashToCloseItemEstimatedAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the IntegratedDisclosureCashToCloseItemEstimatedAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IntegratedDisclosureCashToCloseItemEstimatedAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool IntegratedDisclosureCashToCloseItemEstimatedAmountSpecified
        {
            get { return IntegratedDisclosureCashToCloseItemEstimatedAmount != null; }
            set { }
        }

        /// <summary>
        /// The final cash to close amount for the specified Integrated Disclosure Cash To Close Item Type.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOAmount IntegratedDisclosureCashToCloseItemFinalAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the IntegratedDisclosureCashToCloseItemFinalAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IntegratedDisclosureCashToCloseItemFinalAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool IntegratedDisclosureCashToCloseItemFinalAmountSpecified
        {
            get { return IntegratedDisclosureCashToCloseItemFinalAmount != null; }
            set { }
        }

        /// <summary>
        /// Identifies whether the Cash To Close Item Amount is due to or from the borrower.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<IntegratedDisclosureCashToCloseItemPaymentBase> IntegratedDisclosureCashToCloseItemPaymentType;

        /// <summary>
        /// Gets or sets a value indicating whether the IntegratedDisclosureCashToCloseItemPaymentType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IntegratedDisclosureCashToCloseItemPaymentType element has been assigned a value.</value>
        [XmlIgnore]
        public bool IntegratedDisclosureCashToCloseItemPaymentTypeSpecified
        {
            get { return this.IntegratedDisclosureCashToCloseItemPaymentType != null && this.IntegratedDisclosureCashToCloseItemPaymentType.enumValue != IntegratedDisclosureCashToCloseItemPaymentBase.Blank; }
            set { }
        }

        /// <summary>
        /// Identifies the type of cash to close item as specified in the Calculating Cash to Close section of the integrated disclosure document.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOEnum<IntegratedDisclosureCashToCloseItemBase> IntegratedDisclosureCashToCloseItemType;

        /// <summary>
        /// Gets or sets a value indicating whether the IntegratedDisclosureCashToCloseItemType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IntegratedDisclosureCashToCloseItemType element has been assigned a value.</value>
        [XmlIgnore]
        public bool IntegratedDisclosureCashToCloseItemTypeSpecified
        {
            get { return this.IntegratedDisclosureCashToCloseItemType != null && this.IntegratedDisclosureCashToCloseItemType.enumValue != IntegratedDisclosureCashToCloseItemBase.Blank; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 6)]
        public CASH_TO_CLOSE_ITEM_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
