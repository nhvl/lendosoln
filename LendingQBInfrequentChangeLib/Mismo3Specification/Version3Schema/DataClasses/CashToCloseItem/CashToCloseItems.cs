namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CASH_TO_CLOSE_ITEMS
    {
        /// <summary>
        /// Gets a value indicating whether the CASH_TO_CLOSE_ITEMS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CashToCloseItemSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of cash to close items.
        /// </summary>
        [XmlElement("CASH_TO_CLOSE_ITEM", Order = 0)]
		public List<CASH_TO_CLOSE_ITEM> CashToCloseItem = new List<CASH_TO_CLOSE_ITEM>();

        /// <summary>
        /// Gets or sets a value indicating whether the CashToCloseItem element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CashToCloseItem element has been assigned a value.</value>
        [XmlIgnore]
        public bool CashToCloseItemSpecified
        {
            get { return this.CashToCloseItem != null && this.CashToCloseItem.Count(c => c != null && c.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public CASH_TO_CLOSE_ITEMS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null && Extension.ShouldSerialize; }
            set { }
        }
    }
}
