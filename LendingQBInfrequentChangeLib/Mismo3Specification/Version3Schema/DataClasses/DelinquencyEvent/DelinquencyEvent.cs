namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class DELINQUENCY_EVENT
    {
        /// <summary>
        /// Gets a value indicating whether the DELINQUENCY_EVENT container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.LoanDelinquencyEventDateSpecified
                    || this.LoanDelinquencyEventTypeOtherDescriptionSpecified
                    || this.LoanDelinquencyEventTypeSpecified;
            }
        }

        /// <summary>
        /// The date the loan delinquency event occurred.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMODate LoanDelinquencyEventDate;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanDelinquencyEventDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanDelinquencyEventDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanDelinquencyEventDateSpecified
        {
            get { return LoanDelinquencyEventDate != null; }
            set { }
        }

        /// <summary>
        /// Used to report an event on a delinquent loan.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<LoanDelinquencyEventBase> LoanDelinquencyEventType;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanDelinquencyEventType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanDelinquencyEventType element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanDelinquencyEventTypeSpecified
        {
            get { return this.LoanDelinquencyEventType != null && this.LoanDelinquencyEventType.enumValue != LoanDelinquencyEventBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture the Loan Delinquency Event Type if Other is selected.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString LoanDelinquencyEventTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanDelinquencyEventTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanDelinquencyEventTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanDelinquencyEventTypeOtherDescriptionSpecified
        {
            get { return LoanDelinquencyEventTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public DELINQUENCY_EVENT_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
