namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class DELINQUENCY_EVENTS
    {
        /// <summary>
        /// Gets a value indicating whether the DELINQUENCY_EVENTS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DelinquencyEventSpecified
                    || this.ExtensionSpecified;
            }
        }


        /// <summary>
        /// A collection of delinquency events.
        /// </summary>
        [XmlElement("DELINQUENCY_EVENT", Order = 0)]
		public List<DELINQUENCY_EVENT> DelinquencyEvent = new List<DELINQUENCY_EVENT>();

        /// <summary>
        /// Gets or sets a value indicating whether the DelinquencyEvent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DelinquencyEvent element has been assigned a value.</value>
        [XmlIgnore]
        public bool DelinquencyEventSpecified
        {
            get { return this.DelinquencyEvent != null && this.DelinquencyEvent.Count(d => d != null && d.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public DELINQUENCY_EVENTS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
