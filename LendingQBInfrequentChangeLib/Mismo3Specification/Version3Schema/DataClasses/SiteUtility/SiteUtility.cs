namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class SITE_UTILITY
    {
        /// <summary>
        /// Gets a value indicating whether the SITE_UTILITY container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.SiteUndergroundUtilitiesIndicatorSpecified
                    || this.SiteUtilityNonPublicOwnershipDescriptionSpecified
                    || this.SiteUtilityNotTypicalDescriptionSpecified
                    || this.SiteUtilityOwnershipTypeSpecified
                    || this.SiteUtilityTypeOtherDescriptionSpecified
                    || this.SiteUtilityTypeSpecified
                    || this.UtilityTypicalIndicatorSpecified;
            }
        }

        /// <summary>
        /// Indicates if the site has underground electrical and/or telephone cables.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIndicator SiteUndergroundUtilitiesIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the SiteUndergroundUtilitiesIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SiteUndergroundUtilitiesIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool SiteUndergroundUtilitiesIndicatorSpecified
        {
            get { return SiteUndergroundUtilitiesIndicator != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the ownership of the utility when Non Public is selected for Site Utility Ownership Type.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString SiteUtilityNonPublicOwnershipDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the SiteUtilityNonPublicOwnershipDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SiteUtilityNonPublicOwnershipDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool SiteUtilityNonPublicOwnershipDescriptionSpecified
        {
            get { return SiteUtilityNonPublicOwnershipDescription != null; }
            set { }
        }

        /// <summary>
        /// Describes how the site utilities differ from what is typical from the market area.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString SiteUtilityNotTypicalDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the SiteUtilityNotTypicalDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SiteUtilityNotTypicalDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool SiteUtilityNotTypicalDescriptionSpecified
        {
            get { return SiteUtilityNotTypicalDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies whether the utility is public utility or a non public utility.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOEnum<SiteUtilityOwnershipBase> SiteUtilityOwnershipType;

        /// <summary>
        /// Gets or sets a value indicating whether the SiteUtilityOwnershipType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SiteUtilityOwnershipType element has been assigned a value.</value>
        [XmlIgnore]
        public bool SiteUtilityOwnershipTypeSpecified
        {
            get { return this.SiteUtilityOwnershipType != null && this.SiteUtilityOwnershipType.enumValue != SiteUtilityOwnershipBase.Blank; }
            set { }
        }

        /// <summary>
        /// Specifies the utility used on the site that is being described. 
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<SiteUtilityBase> SiteUtilityType;

        /// <summary>
        /// Gets or sets a value indicating whether the SiteUtilityType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SiteUtilityType element has been assigned a value.</value>
        [XmlIgnore]
        public bool SiteUtilityTypeSpecified
        {
            get { return this.SiteUtilityType != null && this.SiteUtilityType.enumValue != SiteUtilityBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the utility if Other is selected as the Site Utility Type.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString SiteUtilityTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the SiteUtilityTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SiteUtilityTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool SiteUtilityTypeOtherDescriptionSpecified
        {
            get { return SiteUtilityTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the site utilities are typical for the market area.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOIndicator UtilityTypicalIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the UtilityTypicalIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the UtilityTypicalIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool UtilityTypicalIndicatorSpecified
        {
            get { return UtilityTypicalIndicator != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 7)]
        public SITE_UTILITY_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
