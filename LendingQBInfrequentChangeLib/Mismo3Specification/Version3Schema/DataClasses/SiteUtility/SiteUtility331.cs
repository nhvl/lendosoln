namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class SITE_UTILITY_3_3_1
    {
        /// <summary>
        /// Gets a value indicating whether the SITE_UTILITY_3_3_1 container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.SiteUtilityAlternativeEnergySourceTypeOtherDescriptionSpecified
                    || this.SiteUtilityAlternativeEnergySourceTypeSpecified;
            }
        }

        /// <summary>
        /// A collection of values that specify the alternative energy source that is used by a utility.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<SiteUtilityAlternativeEnergySourceBase> SiteUtilityAlternativeEnergySourceType;

        /// <summary>
        /// Gets or sets a value indicating whether the SiteUtilityAlternativeEnergySourceType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SiteUtilityAlternativeEnergySourceType element has been assigned a value.</value>
        [XmlIgnore]
        public bool SiteUtilityAlternativeEnergySourceTypeSpecified
        {
            get { return this.SiteUtilityAlternativeEnergySourceType != null && this.SiteUtilityAlternativeEnergySourceType.enumValue != SiteUtilityAlternativeEnergySourceBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field that specifies the enumeration when the value of Other is used for the Site Utility Alternative Energy Source Type.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString SiteUtilityAlternativeEnergySourceTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the SiteUtilityAlternativeEnergySourceTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SiteUtilityAlternativeEnergySourceTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool SiteUtilityAlternativeEnergySourceTypeOtherDescriptionSpecified
        {
            get { return SiteUtilityAlternativeEnergySourceTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public SITE_UTILITY_3_3_1_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
