namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class SITE_UTILITIES
    {
        /// <summary>
        /// Gets a value indicating whether the SITE_UTILITIES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.SiteUtilitySpecified;
            }
        }

        /// <summary>
        /// A collection of site utilities.
        /// </summary>
        [XmlElement("SITE_UTILITY", Order = 0)]
		public List<SITE_UTILITY> SiteUtility = new List<SITE_UTILITY>();

        /// <summary>
        /// Gets or sets a value indicating whether the SiteUtility element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SiteUtility element has been assigned a value.</value>
        [XmlIgnore]
        public bool SiteUtilitySpecified
        {
            get { return this.SiteUtility != null && this.SiteUtility.Count(s => s != null && s.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public SITE_UTILITIES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
