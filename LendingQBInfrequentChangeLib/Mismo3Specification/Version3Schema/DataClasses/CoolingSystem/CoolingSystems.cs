namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class COOLING_SYSTEMS
    {
        /// <summary>
        /// Gets a value indicating whether the COOLING_SYSTEMS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CoolingSystemSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of cooling systems.
        /// </summary>
        [XmlElement("COOLING_SYSTEM", Order = 0)]
		public List<COOLING_SYSTEM> CoolingSystem = new List<COOLING_SYSTEM>();

        /// <summary>
        /// Gets or sets a value indicating whether the CoolingSystem element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CoolingSystem element has been assigned a value.</value>
        [XmlIgnore]
        public bool CoolingSystemSpecified
        {
            get { return this.CoolingSystem != null && this.CoolingSystem.Count(c => c != null && c.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public COOLING_SYSTEMS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
