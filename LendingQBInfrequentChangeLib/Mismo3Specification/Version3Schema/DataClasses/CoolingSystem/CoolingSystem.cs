namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class COOLING_SYSTEM
    {
        /// <summary>
        /// Gets a value indicating whether the COOLING_SYSTEM container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AdequateIndicatorSpecified
                    || this.ComponentAdjustmentAmountSpecified
                    || this.ComponentClassificationTypeSpecified
                    || this.ConditionRatingDescriptionSpecified
                    || this.ConditionRatingTypeSpecified
                    || this.CoolingSystemDescriptionSpecified
                    || this.CoolingSystemPrimaryIndicatorSpecified
                    || this.CoolingSystemTypeOtherDescriptionSpecified
                    || this.CoolingSystemTypeSpecified
                    || this.ExtensionSpecified
                    || this.QualityRatingDescriptionSpecified
                    || this.QualityRatingTypeSpecified
                    || this.SystemPermanentIndicatorSpecified;
            }
        }

        /// <summary>
        /// Indicates that this feature is adequate for normal needs.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIndicator AdequateIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the AdequateIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AdequateIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool AdequateIndicatorSpecified
        {
            get { return AdequateIndicator != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount (either positive or negative) adjustment being made for a specific component of the property.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOAmount ComponentAdjustmentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ComponentAdjustmentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ComponentAdjustmentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ComponentAdjustmentAmountSpecified
        {
            get { return ComponentAdjustmentAmount != null; }
            set { }
        }

        /// <summary>
        /// Specifies whether the component is considered real or personal property.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<ComponentClassificationBase> ComponentClassificationType;

        /// <summary>
        /// Gets or sets a value indicating whether the ComponentClassificationType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ComponentClassificationType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ComponentClassificationTypeSpecified
        {
            get { return this.ComponentClassificationType != null && this.ComponentClassificationType.enumValue != ComponentClassificationBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe in detail the condition rating of the identified component.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString ConditionRatingDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ConditionRatingDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConditionRatingDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConditionRatingDescriptionSpecified
        {
            get { return ConditionRatingDescription != null; }
            set { }
        }

        /// <summary>
        /// A rating of the condition of the identified component.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<ConditionRatingBase> ConditionRatingType;

        /// <summary>
        /// Gets or sets a value indicating whether the ConditionRatingType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConditionRatingType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConditionRatingTypeSpecified
        {
            get { return this.ConditionRatingType != null && this.ConditionRatingType.enumValue != ConditionRatingBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe in detail the cooling system identified by the cooling system type.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString CoolingSystemDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the CoolingSystemDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CoolingSystemDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool CoolingSystemDescriptionSpecified
        {
            get { return CoolingSystemDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the cooling system is the primary unit, not a secondary or additional system.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOIndicator CoolingSystemPrimaryIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the CoolingSystemPrimaryIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CoolingSystemPrimaryIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool CoolingSystemPrimaryIndicatorSpecified
        {
            get { return CoolingSystemPrimaryIndicator != null; }
            set { }
        }

        /// <summary>
        /// Specifies the type of cooling system.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOEnum<CoolingSystemBase> CoolingSystemType;

        /// <summary>
        /// Gets or sets a value indicating whether the CoolingSystemType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CoolingSystemType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CoolingSystemTypeSpecified
        {
            get { return this.CoolingSystemType != null && this.CoolingSystemType.enumValue != CoolingSystemBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Cooling System Type. 
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOString CoolingSystemTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the CoolingSystemTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CoolingSystemTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool CoolingSystemTypeOtherDescriptionSpecified
        {
            get { return CoolingSystemTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe in detail the quality rating of the identified component.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOString QualityRatingDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the QualityRatingDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the QualityRatingDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool QualityRatingDescriptionSpecified
        {
            get { return QualityRatingDescription != null; }
            set { }
        }

        /// <summary>
        /// A rating of the quality of the identified component type.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOEnum<QualityRatingBase> QualityRatingType;

        /// <summary>
        /// Gets or sets a value indicating whether the QualityRatingType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the QualityRatingType element has been assigned a value.</value>
        [XmlIgnore]
        public bool QualityRatingTypeSpecified
        {
            get { return this.QualityRatingType != null && this.QualityRatingType.enumValue != QualityRatingBase.Blank; }
            set { }
        }

        /// <summary>
        /// When true indicates that the system specified is a permanent, attached system otherwise it is portable.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOIndicator SystemPermanentIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the SystemPermanentIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SystemPermanentIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool SystemPermanentIndicatorSpecified
        {
            get { return SystemPermanentIndicator != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 12)]
        public COOLING_SYSTEM_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
