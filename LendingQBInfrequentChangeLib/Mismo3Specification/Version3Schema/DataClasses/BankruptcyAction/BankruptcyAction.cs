namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class BANKRUPTCY_ACTION
    {
        /// <summary>
        /// Gets a value indicating whether the BANKRUPTCY_ACTION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BankruptcyActionDateSpecified
                    || this.BankruptcyActionTypeOtherDescriptionSpecified
                    || this.BankruptcyActionTypeSpecified
                    || this.BankruptcyAttorneyServiceReferralTypeOtherDescriptionSpecified
                    || this.BankruptcyAttorneyServiceReferralTypeSpecified
                    || this.BankruptcyPaymentChangeNoticeReasonTypeOtherDescriptionSpecified
                    || this.BankruptcyPaymentChangeNoticeReasonTypeSpecified
                    || this.BankruptcyPlanAmendedTypeSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// The date associated with the specified Bankruptcy Action Type.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMODate BankruptcyActionDate;

        /// <summary>
        /// Gets or sets a value indicating whether the BankruptcyActionDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BankruptcyActionDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool BankruptcyActionDateSpecified
        {
            get { return BankruptcyActionDate != null; }
            set { }
        }

        /// <summary>
        /// Specifies an action or step taken in the bankruptcy process.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<BankruptcyActionBase> BankruptcyActionType;

        /// <summary>
        /// Gets or sets a value indicating whether the BankruptcyActionType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BankruptcyActionType element has been assigned a value.</value>
        [XmlIgnore]
        public bool BankruptcyActionTypeSpecified
        {
            get { return this.BankruptcyActionType != null && this.BankruptcyActionType.enumValue != BankruptcyActionBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is  selected as the Bankruptcy Action Type.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString BankruptcyActionTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the BankruptcyActionTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BankruptcyActionTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool BankruptcyActionTypeOtherDescriptionSpecified
        {
            get { return BankruptcyActionTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates the type of instructions from the servicer to the bankruptcy attorney on the portion of the bankruptcy proceedings the attorney is to complete on the servicer's behalf.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOEnum<BankruptcyAttorneyServiceReferralBase> BankruptcyAttorneyServiceReferralType;

        /// <summary>
        /// Gets or sets a value indicating whether the BankruptcyAttorneyServiceReferralType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BankruptcyAttorneyServiceReferralType element has been assigned a value.</value>
        [XmlIgnore]
        public bool BankruptcyAttorneyServiceReferralTypeSpecified
        {
            get { return this.BankruptcyAttorneyServiceReferralType != null && this.BankruptcyAttorneyServiceReferralType.enumValue != BankruptcyAttorneyServiceReferralBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Bankruptcy Attorney Service Referral Type.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString BankruptcyAttorneyServiceReferralTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the BankruptcyAttorneyServiceReferralTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BankruptcyAttorneyServiceReferralTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool BankruptcyAttorneyServiceReferralTypeOtherDescriptionSpecified
        {
            get { return BankruptcyAttorneyServiceReferralTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates the reason that a Payment Change Notice was filed reflecting a change in the amount due to the servicer.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOEnum<BankruptcyPaymentChangeNoticeReasonBase> BankruptcyPaymentChangeNoticeReasonType;

        /// <summary>
        /// Gets or sets a value indicating whether the BankruptcyPaymentChangeNoticeReasonType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BankruptcyPaymentChangeNoticeReasonType element has been assigned a value.</value>
        [XmlIgnore]
        public bool BankruptcyPaymentChangeNoticeReasonTypeSpecified
        {
            get { return this.BankruptcyPaymentChangeNoticeReasonType != null && this.BankruptcyPaymentChangeNoticeReasonType.enumValue != BankruptcyPaymentChangeNoticeReasonBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture additional information when Other is selected forBankruptcy Payment Change Notice Reason Type.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOString BankruptcyPaymentChangeNoticeReasonTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the BankruptcyPaymentChangeNoticeReasonTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BankruptcyPaymentChangeNoticeReasonTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool BankruptcyPaymentChangeNoticeReasonTypeOtherDescriptionSpecified
        {
            get { return BankruptcyPaymentChangeNoticeReasonTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates the type of amendment being made to a bankruptcy plan.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOEnum<BankruptcyPlanAmendedBase> BankruptcyPlanAmendedType;

        /// <summary>
        /// Gets or sets a value indicating whether the BankruptcyPlanAmendedType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BankruptcyPlanAmendedType element has been assigned a value.</value>
        [XmlIgnore]
        public bool BankruptcyPlanAmendedTypeSpecified
        {
            get { return this.BankruptcyPlanAmendedType != null && this.BankruptcyPlanAmendedType.enumValue != BankruptcyPlanAmendedBase.Blank; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 8)]
        public BANKRUPTCY_ACTION_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
