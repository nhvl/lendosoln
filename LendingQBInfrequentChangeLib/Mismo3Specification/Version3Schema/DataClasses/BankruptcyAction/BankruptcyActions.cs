namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class BANKRUPTCY_ACTIONS
    {
        /// <summary>
        /// Gets a value indicating whether the BANKRUPTCY_ACTIONS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BankruptcyActionSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A set of bankruptcy actions.
        /// </summary>
        [XmlElement("BANKRUPTCY_ACTION", Order = 0)]
		public List<BANKRUPTCY_ACTION> BankruptcyAction = new List<BANKRUPTCY_ACTION>();

        /// <summary>
        /// Gets or sets a value indicating whether the BankruptcyAction element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BankruptcyAction element has been assigned a value.</value>
        [XmlIgnore]
        public bool BankruptcyActionSpecified
        {
            get { return this.BankruptcyAction != null && this.BankruptcyAction.Count(b => b != null && b.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public BANKRUPTCY_ACTIONS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
