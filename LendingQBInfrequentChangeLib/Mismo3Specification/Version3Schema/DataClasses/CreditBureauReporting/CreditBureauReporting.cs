namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CREDIT_BUREAU_REPORTING
    {
        /// <summary>
        /// Gets a value indicating whether the CREDIT_BUREAU_REPORTING container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditCommentCodeSpecified
                    || this.CreditCommentExpirationDateSpecified
                    || this.CreditCommentSourceTypeSpecified
                    || this.CreditLiabilityAccountStatusTypeSpecified
                    || this.CreditLiabilityCurrentRatingCodeSpecified
                    || this.CreditLiabilityCurrentRatingTypeSpecified
                    || this.CreditLiabilityFirstReportedDefaultDateSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// This is a code value that may be associated with Credit Comment Text data. This could be a repository bureau code (Equifax, Experian, Trans Union), or it could be a code provided by a lender, credit bureau or other source that is specified in the Credit Comment Source Type.  The code value should be exactly as shown in the source document without any other characters such as a prefix.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOCode CreditCommentCode;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditCommentCode element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditCommentCode element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditCommentCodeSpecified
        {
            get { return CreditCommentCode != null; }
            set { }
        }

        /// <summary>
        /// Date the comment expires.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMODate CreditCommentExpirationDate;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditCommentExpirationDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditCommentExpirationDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditCommentExpirationDateSpecified
        {
            get { return CreditCommentExpirationDate != null; }
            set { }
        }

        /// <summary>
        /// This data element provides an enumerated list of sources for the Credit Comment data.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<CreditCommentSourceBase> CreditCommentSourceType;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditCommentSourceType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditCommentSourceType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditCommentSourceTypeSpecified
        {
            get { return this.CreditCommentSourceType != null && this.CreditCommentSourceType.enumValue != CreditCommentSourceBase.Blank; }
            set { }
        }

        /// <summary>
        /// This data element is an enumerated status list for an account as of the Account Status Date.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOEnum<CreditLiabilityAccountStatusBase> CreditLiabilityAccountStatusType;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLiabilityAccountStatusType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLiabilityAccountStatusType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLiabilityAccountStatusTypeSpecified
        {
            get { return this.CreditLiabilityAccountStatusType != null && this.CreditLiabilityAccountStatusType.enumValue != CreditLiabilityAccountStatusBase.Blank; }
            set { }
        }

        /// <summary>
        /// The liability accounts current code (as of Date Reported) for how timely the borrower has been making payments on this account. This is also know as the Manner Of Payment (MOP). See the MISMO Implementation Guide: Credit Reporting for more information on MOP codes.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOCode CreditLiabilityCurrentRatingCode;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLiabilityCurrentRatingCode element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLiabilityCurrentRatingCode element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLiabilityCurrentRatingCodeSpecified
        {
            get { return CreditLiabilityCurrentRatingCode != null; }
            set { }
        }

        /// <summary>
        /// The liability accounts current rating (as of Date Reported) for how timely the borrower has been making payments on this account.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOEnum<CreditLiabilityCurrentRatingBase> CreditLiabilityCurrentRatingType;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLiabilityCurrentRatingType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLiabilityCurrentRatingType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLiabilityCurrentRatingTypeSpecified
        {
            get { return this.CreditLiabilityCurrentRatingType != null && this.CreditLiabilityCurrentRatingType.enumValue != CreditLiabilityCurrentRatingBase.Blank; }
            set { }
        }

        /// <summary>
        /// The date the loan was first reported to the credit bureau with a status code indicating bankruptcy or 30 day delinquency.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMODate CreditLiabilityFirstReportedDefaultDate;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLiabilityFirstReportedDefaultDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLiabilityFirstReportedDefaultDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLiabilityFirstReportedDefaultDateSpecified
        {
            get { return CreditLiabilityFirstReportedDefaultDate != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 7)]
        public CREDIT_BUREAU_REPORTING_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
