namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class ASSUMABILITY_RULE
    {
        /// <summary>
        /// Gets a value indicating whether the ASSUMABILITY_RULE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AssumabilityBeginDateSpecified
                    || this.AssumabilityTermMonthsCountSpecified
                    || this.AssumabilityTypeOtherDescriptionSpecified
                    || this.AssumabilityTypeSpecified
                    || this.ConditionsToAssumabilityIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// The date the loan becomes assumable.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMODate AssumabilityBeginDate;

        /// <summary>
        /// Gets or sets a value indicating whether the Assumption BeginDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Assumption BeginDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool AssumabilityBeginDateSpecified
        {
            get { return AssumabilityBeginDate != null; }
            set { }
        }

        /// <summary>
        /// The length of time the loan is assumable in months.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOCount AssumabilityTermMonthsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the Assumption Term Months Count element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Assumption Term Months Count element has been assigned a value.</value>
        [XmlIgnore]
        public bool AssumabilityTermMonthsCountSpecified
        {
            get { return AssumabilityTermMonthsCount != null; }
            set { }
        }

        /// <summary>
        /// Specifies when the loan assumption is permitted.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<AssumabilityBase> AssumabilityType;

        /// <summary>
        /// Gets or sets a value indicating whether the Assumable Type element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Assumable Type element has been assigned a value.</value>
        [XmlIgnore]
        public bool AssumabilityTypeSpecified
        {
            get { return this.AssumabilityType != null && this.AssumabilityType.enumValue != AssumabilityBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Assumable Type.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString AssumabilityTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the Assumable Type Other Description element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Assumable Type Other Description element has been assigned a value.</value>
        [XmlIgnore]
        public bool AssumabilityTypeOtherDescriptionSpecified
        {
            get { return AssumabilityTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that the loan may be assumable, subject to certain conditions.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOIndicator ConditionsToAssumabilityIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the Conditions To Assumable Indicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Conditions To Assumable Indicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConditionsToAssumabilityIndicatorSpecified
        {
            get { return ConditionsToAssumabilityIndicator != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 5)]
        public ASSUMABILITY_RULE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
