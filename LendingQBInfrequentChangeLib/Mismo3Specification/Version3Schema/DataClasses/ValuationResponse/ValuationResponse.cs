namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class VALUATION_RESPONSE
    {
        /// <summary>
        /// Gets a value indicating whether the VALUATION_RESPONSE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DataSourcesSpecified
                    || this.ExtensionSpecified
                    || this.PartiesSpecified
                    || this.PropertiesSpecified
                    || this.ValuationReportSpecified;
            }
        }

        /// <summary>
        /// Data sources for valuation.
        /// </summary>
        [XmlElement("DATA_SOURCES", Order = 0)]
        public DATA_SOURCES DataSources;

        /// <summary>
        /// Gets or sets a value indicating whether the DataSources element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DataSources element has been assigned a value.</value>
        [XmlIgnore]
        public bool DataSourcesSpecified
        {
            get { return this.DataSources != null && this.DataSources.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Parties associated with the valuation.
        /// </summary>
        [XmlElement("PARTIES", Order = 1)]
        public PARTIES Parties;

        /// <summary>
        /// Gets or sets a value indicating whether the Parties element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Parties element has been assigned a value.</value>
        [XmlIgnore]
        public bool PartiesSpecified
        {
            get { return this.Parties != null && this.Parties.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Properties receiving valuation.
        /// </summary>
        [XmlElement("PROPERTIES", Order = 2)]
        public PROPERTIES Properties;

        /// <summary>
        /// Gets or sets a value indicating whether the Properties element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Properties element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertiesSpecified
        {
            get { return this.Properties != null && this.Properties.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A report of valuation for a property.
        /// </summary>
        [XmlElement("VALUATION_REPORT", Order = 3)]
        public VALUATION_REPORT ValuationReport;

        /// <summary>
        /// Gets or sets a value indicating whether the ValuationReport element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ValuationReport element has been assigned a value.</value>
        [XmlIgnore]
        public bool ValuationReportSpecified
        {
            get { return this.ValuationReport != null && this.ValuationReport.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public VALUATION_RESPONSE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
