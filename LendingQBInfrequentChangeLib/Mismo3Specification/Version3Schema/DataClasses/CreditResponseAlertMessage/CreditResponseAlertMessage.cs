namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class CREDIT_RESPONSE_ALERT_MESSAGE
    {
        /// <summary>
        /// Gets a value indicating whether the CREDIT_RESPONSE_ALERT_MESSAGE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditRepositorySourceTypeOtherDescriptionSpecified
                    || this.CreditRepositorySourceTypeSpecified
                    || this.CreditResponseAlertMessageAdverseIndicatorSpecified
                    || this.CreditResponseAlertMessageCategoryTypeOtherDescriptionSpecified
                    || this.CreditResponseAlertMessageCategoryTypeSpecified
                    || this.CreditResponseAlertMessageCodeSourceTypeOtherDescriptionSpecified
                    || this.CreditResponseAlertMessageCodeSourceTypeSpecified
                    || this.CreditResponseAlertMessageCodeSpecified
                    || this.CreditResponseAlertMessageTextSpecified
                    || this.CreditResponseAlertMessageTypeOtherDescriptionSpecified
                    || this.CreditResponseAlertMessageTypeSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// This element describes the source of the credit file, Equifax, Experian, Trans Union or Unspecified if the specific sources are not specified.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<CreditRepositorySourceBase> CreditRepositorySourceType;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditRepositorySourceType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditRepositorySourceType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditRepositorySourceTypeSpecified
        {
            get { return this.CreditRepositorySourceType != null && this.CreditRepositorySourceType.enumValue != CreditRepositorySourceBase.Blank; }
            set { }
        }

        /// <summary>
        /// When Credit Repository Source Type is set to Other, enter its value in this data element.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString CreditRepositorySourceTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditRepositorySourceTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditRepositorySourceTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditRepositorySourceTypeOtherDescriptionSpecified
        {
            get { return CreditRepositorySourceTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// This indicator is set to Y when the Credit Response Alert Message contains information which may be considered adverse and may require further investigation or other action.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOIndicator CreditResponseAlertMessageAdverseIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditResponseAlertMessageAdverseIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditResponseAlertMessageAdverseIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditResponseAlertMessageAdverseIndicatorSpecified
        {
            get { return CreditResponseAlertMessageAdverseIndicator != null; }
            set { }
        }

        /// <summary>
        /// Describes the general category of the alert message. This allows for automated processing by systems reading the credit alert messages.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOEnum<CreditResponseAlertMessageCategoryBase> CreditResponseAlertMessageCategoryType;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditResponseAlertMessageCategoryType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditResponseAlertMessageCategoryType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditResponseAlertMessageCategoryTypeSpecified
        {
            get { return this.CreditResponseAlertMessageCategoryType != null && this.CreditResponseAlertMessageCategoryType.enumValue != CreditResponseAlertMessageCategoryBase.Blank; }
            set { }
        }

        /// <summary>
        /// When Credit Response Alert Message Category Type is set to Other, enter its value in this data element.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString CreditResponseAlertMessageCategoryTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditResponseAlertMessageCategoryTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditResponseAlertMessageCategoryTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditResponseAlertMessageCategoryTypeOtherDescriptionSpecified
        {
            get { return CreditResponseAlertMessageCategoryTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// This is the code value associated with the Credit Response Alert Message Text and/or Credit Response Alert Message Type.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOCode CreditResponseAlertMessageCode;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditResponseAlertMessageCode element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditResponseAlertMessageCode element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditResponseAlertMessageCodeSpecified
        {
            get { return CreditResponseAlertMessageCode != null; }
            set { }
        }

        /// <summary>
        /// This element identifies the name of the source for the code values used in the Credit Response Alert Message Code.  The enumeration definition identifies the source document and name of the table that contains the code values.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOEnum<CreditResponseAlertMessageCodeSourceBase> CreditResponseAlertMessageCodeSourceType;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditResponseAlertMessageCodeSourceType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditResponseAlertMessageCodeSourceType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditResponseAlertMessageCodeSourceTypeSpecified
        {
            get { return this.CreditResponseAlertMessageCodeSourceType != null && this.CreditResponseAlertMessageCodeSourceType.enumValue != CreditResponseAlertMessageCodeSourceBase.Blank; }
            set { }
        }

        /// <summary>
        /// When the Credit Response Alert Message Code Source Type has a value of Other, this element identifies the name of the source for the code values used in the Credit Response Alert Message Code.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOString CreditResponseAlertMessageCodeSourceTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditResponseAlertMessageCodeSourceTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditResponseAlertMessageCodeSourceTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditResponseAlertMessageCodeSourceTypeOtherDescriptionSpecified
        {
            get { return CreditResponseAlertMessageCodeSourceTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Text of the alert message.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOString CreditResponseAlertMessageText;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditResponseAlertMessageText element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditResponseAlertMessageText element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditResponseAlertMessageTextSpecified
        {
            get { return CreditResponseAlertMessageText != null; }
            set { }
        }

        /// <summary>
        /// Lists the name of the repository bureau service or other service that generated the alert message.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOEnum<CreditResponseAlertMessageBase> CreditResponseAlertMessageType;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditResponseAlertMessageType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditResponseAlertMessageType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditResponseAlertMessageTypeSpecified
        {
            get { return this.CreditResponseAlertMessageType != null && this.CreditResponseAlertMessageType.enumValue != CreditResponseAlertMessageBase.Blank; }
            set { }
        }

        /// <summary>
        /// When Credit Response Alert Message Type is set to Other, enter its value in this data element.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOString CreditResponseAlertMessageTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditResponseAlertMessageTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditResponseAlertMessageTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditResponseAlertMessageTypeOtherDescriptionSpecified
        {
            get { return CreditResponseAlertMessageTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 11)]
        public CREDIT_RESPONSE_ALERT_MESSAGE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
