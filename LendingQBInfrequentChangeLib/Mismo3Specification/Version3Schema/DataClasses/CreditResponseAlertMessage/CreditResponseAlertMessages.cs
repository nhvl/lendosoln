namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CREDIT_RESPONSE_ALERT_MESSAGES
    {
        /// <summary>
        /// Gets a value indicating whether the CREDIT_RESPONSE_ALERT_MESSAGES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditResponseAlertMessageSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of credit response alert messages.
        /// </summary>
        [XmlElement("CREDIT_RESPONSE_ALERT_MESSAGE", Order = 0)]
		public List<CREDIT_RESPONSE_ALERT_MESSAGE> CreditResponseAlertMessage = new List<CREDIT_RESPONSE_ALERT_MESSAGE>();

        /// <summary>
        /// Gets or sets a value indicating whether the CreditResponseAlertMessage element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditResponseAlertMessage element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditResponseAlertMessageSpecified
        {
            get { return this.CreditResponseAlertMessage != null && this.CreditResponseAlertMessage.Count(c => c != null && c.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public CREDIT_RESPONSE_ALERT_MESSAGES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
