namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class DRAW
    {
        /// <summary>
        /// Gets a value indicating whether the DRAW container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DrawActivitiesSpecified
                    || this.DrawRuleSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// Draws on a line of credit.
        /// </summary>
        [XmlElement("DRAW_ACTIVITIES", Order = 0)]
        public DRAW_ACTIVITIES DrawActivities;

        /// <summary>
        /// Gets or sets a value indicating whether the DrawActivities element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DrawActivities element has been assigned a value.</value>
        [XmlIgnore]
        public bool DrawActivitiesSpecified
        {
            get { return this.DrawActivities != null && this.DrawActivities.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A rule on the draw period of a line of credit.
        /// </summary>
        [XmlElement("DRAW_RULE", Order = 1)]
        public DRAW_RULE DrawRule;

        /// <summary>
        /// Gets or sets a value indicating whether the DrawRule element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DrawRule element has been assigned a value.</value>
        [XmlIgnore]
        public bool DrawRuleSpecified
        {
            get { return this.DrawRule != null && this.DrawRule.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public DRAW_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
