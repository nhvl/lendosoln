namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class REQUIRED_SERVICE_PROVIDER
    {
        /// <summary>
        /// Gets a value indicating whether the REQUIRED_SERVICE_PROVIDER container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                if (Mismo3Utilities.ExceedsThreshold(1,
                    new List<bool> { this.IndividualSpecified, this.LegalEntitySpecified }))
                {
                    throw new System.Xml.Schema.XmlSchemaValidationException(
                        Mismo3Utilities.GetXSDChoiceExceptionMessage(
                        "REQUIRED_SERVICE_PROVIDER",
                        new List<string> { "INDIVIDUAL", "LEGAL_ENTITY" }));
                }

                return this.AddressSpecified
                    || this.ExtensionSpecified
                    || this.IndividualSpecified
                    || this.LegalEntitySpecified
                    || this.RequiredServiceProviderDetailSpecified;
            }
        }

        /// <summary>
        /// An individual associated with the service provider.
        /// </summary>
        [XmlElement("INDIVIDUAL", Order = 0)]
        public INDIVIDUAL Individual;

        /// <summary>
        /// Gets or sets a value indicating whether the Individual element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Individual element has been assigned a value.</value>
        [XmlIgnore]
        public bool IndividualSpecified
        {
            get { return this.Individual != null && this.Individual.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Legal entity associated with the required service provider.
        /// </summary>
        [XmlElement("LEGAL_ENTITY", Order = 1)]
        public LEGAL_ENTITY LegalEntity;

        /// <summary>
        /// Gets or sets a value indicating whether the LegalEntity element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LegalEntity element has been assigned a value.</value>
        [XmlIgnore]
        public bool LegalEntitySpecified
        {
            get { return this.LegalEntity != null && this.LegalEntity.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Address of the required service provider.
        /// </summary>
        [XmlElement("ADDRESS", Order = 2)]
        public ADDRESS Address;

        /// <summary>
        /// Gets or sets a value indicating whether the Address element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Address element has been assigned a value.</value>
        [XmlIgnore]
        public bool AddressSpecified
        {
            get { return this.Address != null && this.Address.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details on the required service provider.
        /// </summary>
        [XmlElement("REQUIRED_SERVICE_PROVIDER_DETAIL", Order = 3)]
        public REQUIRED_SERVICE_PROVIDER_DETAIL RequiredServiceProviderDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the RequiredServiceProviderDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RequiredServiceProviderDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool RequiredServiceProviderDetailSpecified
        {
            get { return this.RequiredServiceProviderDetail != null && this.RequiredServiceProviderDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public REQUIRED_SERVICE_PROVIDER_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
