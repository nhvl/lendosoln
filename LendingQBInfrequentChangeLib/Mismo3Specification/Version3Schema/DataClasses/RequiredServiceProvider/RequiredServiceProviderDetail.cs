namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class REQUIRED_SERVICE_PROVIDER_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the REQUIRED_SERVICE_PROVIDER_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.RequiredServiceProviderNatureOfRelationshipDescriptionSpecified
                    || this.RequiredServiceProviderOwnershipInterestPercentSpecified;
            }
        }

        /// <summary>
        /// Describes the nature of any relationship between each required service provider and the lender. A free-form text field used to provide Plain English references to the relationship should be utilized such as Depositor of Lender.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString RequiredServiceProviderNatureOfRelationshipDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the RequiredServiceProviderNatureOfRelationshipDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RequiredServiceProviderNatureOfRelationshipDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool RequiredServiceProviderNatureOfRelationshipDescriptionSpecified
        {
            get { return RequiredServiceProviderNatureOfRelationshipDescription != null; }
            set { }
        }

        /// <summary>
        /// The lender's percentage of ownership interest in the required service provider organization.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOPercent RequiredServiceProviderOwnershipInterestPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the RequiredServiceProviderOwnershipInterestPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RequiredServiceProviderOwnershipInterestPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool RequiredServiceProviderOwnershipInterestPercentSpecified
        {
            get { return RequiredServiceProviderOwnershipInterestPercent != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public REQUIRED_SERVICE_PROVIDER_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
