namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class WORKOUT_EVALUATION_RESPONSE
    {
        /// <summary>
        /// Gets a value indicating whether the WORKOUT_EVALUATION_RESPONSE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DealSetSpecified
                    || this.ExtensionSpecified
                    || this.WorkoutEvaluationResponseDetailSpecified;
            }
        }

        /// <summary>
        /// A set of deals.
        /// </summary>
        [XmlElement("DEAL_SET", Order = 0)]
        public DEAL_SET DealSet;

        /// <summary>
        /// Gets or sets a value indicating whether the DealSet element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DealSet element has been assigned a value.</value>
        [XmlIgnore]
        public bool DealSetSpecified
        {
            get { return this.DealSet != null && this.DealSet.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details on a workout evaluation response.
        /// </summary>
        [XmlElement("WORKOUT_EVALUATION_RESPONSE_DETAIL", Order = 1)]
        public WORKOUT_EVALUATION_RESPONSE_DETAIL WorkoutEvaluationResponseDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the WorkoutEvaluationResponseDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the WorkoutEvaluationResponseDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool WorkoutEvaluationResponseDetailSpecified
        {
            get { return this.WorkoutEvaluationResponseDetail != null && this.WorkoutEvaluationResponseDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public WORKOUT_EVALUATION_RESPONSE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
