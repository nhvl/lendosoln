namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class WORKOUT_EVALUATION_RESPONSE_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the WORKOUT_EVALUATION_RESPONSE_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ProcessingSuccessIndicatorSpecified
                    || this.ResponseBatchIdentifierSpecified
                    || this.ServiceRequestReceiptDatetimeSpecified
                    || this.ServiceResponseDatetimeSpecified;
            }
        }

        /// <summary>
        /// When true, indicates the processing of the file submission has been successful.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIndicator ProcessingSuccessIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ProcessingSuccessIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProcessingSuccessIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProcessingSuccessIndicatorSpecified
        {
            get { return ProcessingSuccessIndicator != null; }
            set { }
        }

        /// <summary>
        /// A unique identifier assigned to a batch of loans for a response.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOIdentifier ResponseBatchIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the ResponseBatchIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ResponseBatchIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool ResponseBatchIdentifierSpecified
        {
            get { return ResponseBatchIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The date and time that a request submission was received.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMODatetime ServiceRequestReceiptDatetime;

        /// <summary>
        /// Gets or sets a value indicating whether the ServiceRequestReceiptDateTime element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServiceRequestReceiptDateTime element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServiceRequestReceiptDatetimeSpecified
        {
            get { return ServiceRequestReceiptDatetime != null; }
            set { }
        }

        /// <summary>
        /// The date and time that a response was provided.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMODatetime ServiceResponseDatetime;

        /// <summary>
        /// Gets or sets a value indicating whether the ServiceResponseDateTime element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServiceResponseDateTime element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServiceResponseDatetimeSpecified
        {
            get { return ServiceResponseDatetime != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public WORKOUT_EVALUATION_RESPONSE_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
