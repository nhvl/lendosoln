namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class DEAL_SET_INCOMES
    {
        /// <summary>
        /// Gets a value indicating whether the DEAL_SET_INCOMES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DealSetIncomeSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A list of incomes associated with a deal set.
        /// </summary>
        [XmlElement("DEAL_SET_INCOME", Order = 0)]
		public List<DEAL_SET_INCOME> DealSetIncome = new List<DEAL_SET_INCOME>();

        /// <summary>
        /// Gets or sets a value indicating whether the DealSetIncome element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DealSetIncome element has been assigned a value.</value>
        [XmlIgnore]
        public bool DealSetIncomeSpecified
        {
            get { return this.DealSetIncome != null && this.DealSetIncome.Count(d => d != null && d.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public DEAL_SET_INCOMES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
