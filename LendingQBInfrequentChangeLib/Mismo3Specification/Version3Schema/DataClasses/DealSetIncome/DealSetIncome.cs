namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class DEAL_SET_INCOME
    {
        /// <summary>
        /// Gets a value indicating whether the DEAL_SET_INCOME container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DealSetIncomeAmountSpecified
                    || this.DealSetIncomeTypeOtherDescriptionSpecified
                    || this.DealSetIncomeTypeSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// The dollar amount associated with the Deal Set Income Type.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount DealSetIncomeAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the DealSetIncomeAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DealSetIncomeAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool DealSetIncomeAmountSpecified
        {
            get { return DealSetIncomeAmount != null; }
            set { }
        }

        /// <summary>
        /// Specifies the type of income or fee that may be received for services rendered.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<DealSetIncomeBase> DealSetIncomeType;

        /// <summary>
        /// Gets or sets a value indicating whether the DealSetIncomeType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DealSetIncomeType element has been assigned a value.</value>
        [XmlIgnore]
        public bool DealSetIncomeTypeSpecified
        {
            get { return this.DealSetIncomeType != null && this.DealSetIncomeType.enumValue != DealSetIncomeBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture additional information when Other is selected for Deal Set Income Type.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString DealSetIncomeTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the DealSetIncomeTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DealSetIncomeTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool DealSetIncomeTypeOtherDescriptionSpecified
        {
            get { return DealSetIncomeTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public DEAL_SET_INCOME_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
