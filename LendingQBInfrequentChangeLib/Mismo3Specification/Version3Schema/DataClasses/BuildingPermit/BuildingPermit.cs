namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class BUILDING_PERMIT
    {
        /// <summary>
        /// Gets a value indicating whether the BUILDING_PERMIT container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BuildingPermitLevelTypeOtherDescriptionSpecified
                    || this.BuildingPermitLevelTypeSpecified
                    || this.BuildingPermitProposedUseDescriptionSpecified
                    || this.BuildingPermitReferenceIdentifierSpecified
                    || this.BuildingPermitStatusDateSpecified
                    || this.BuildingPermitStatusDescriptionSpecified
                    || this.BuildingPermitStatusTypeOtherDescriptionSpecified
                    || this.BuildingPermitStatusTypeSpecified
                    || this.BuildingPermitTradeDescriptionSpecified
                    || this.BuildingPermitUsageStandardDescriptionSpecified
                    || this.BuildingPermitUsageStandardTypeOtherDescriptionSpecified
                    || this.BuildingPermitUsageStandardTypeSpecified
                    || this.BuildingPermitWorkPurposeDescriptionSpecified
                    || this.BuildingPermitWorkPurposeTypeOtherDescriptionSpecified
                    || this.BuildingPermitWorkPurposeTypeSpecified
                    || this.BuildingPermitWorkTypeOtherDescriptionSpecified
                    || this.BuildingPermitWorkTypeSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// Indicates the level or classification of the instance of the Building Permit Work Type.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<BuildingPermitLevelBase> BuildingPermitLevelType;

        /// <summary>
        /// Gets or sets a value indicating whether the BuildingPermitLevelType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BuildingPermitLevelType element has been assigned a value.</value>
        [XmlIgnore]
        public bool BuildingPermitLevelTypeSpecified
        {
            get { return this.BuildingPermitLevelType != null && this.BuildingPermitLevelType.enumValue != BuildingPermitLevelBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the permit classification if Other is selected as the Building Permit Level Type.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString BuildingPermitLevelTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the BuildingPermitLevelTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BuildingPermitLevelTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool BuildingPermitLevelTypeOtherDescriptionSpecified
        {
            get { return BuildingPermitLevelTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Commentary about the building permit proposed usage provided by the permitting authority and is not categorical in nature.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString BuildingPermitProposedUseDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the BuildingPermitProposedUseDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BuildingPermitProposedUseDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool BuildingPermitProposedUseDescriptionSpecified
        {
            get { return BuildingPermitProposedUseDescription != null; }
            set { }
        }

        /// <summary>
        /// The identifier used by the permitting authority to designate this instance of a permit. Often prefixed by the 2- or 4-digit year and an abbreviation of the type and/or work class of permit or certificate.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOIdentifier BuildingPermitReferenceIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the BuildingPermitReferenceIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BuildingPermitReferenceIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool BuildingPermitReferenceIdentifierSpecified
        {
            get { return BuildingPermitReferenceIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The date corresponding to the status of the permit as indicated in the Permit Status Type as assigned by the authority for this permit.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMODate BuildingPermitStatusDate;

        /// <summary>
        /// Gets or sets a value indicating whether the BuildingPermitStatusDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BuildingPermitStatusDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool BuildingPermitStatusDateSpecified
        {
            get { return BuildingPermitStatusDate != null; }
            set { }
        }

        /// <summary>
        /// Commentary about the building permit status provided by the permitting authority and is not categorical in nature.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString BuildingPermitStatusDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the BuildingPermitStatusDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BuildingPermitStatusDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool BuildingPermitStatusDescriptionSpecified
        {
            get { return BuildingPermitStatusDescription != null; }
            set { }
        }

        /// <summary>
        /// The current status for this permit.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOEnum<BuildingPermitStatusBase> BuildingPermitStatusType;

        /// <summary>
        /// Gets or sets a value indicating whether the BuildingPermitStatusType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BuildingPermitStatusType element has been assigned a value.</value>
        [XmlIgnore]
        public bool BuildingPermitStatusTypeSpecified
        {
            get { return this.BuildingPermitStatusType != null && this.BuildingPermitStatusType.enumValue != BuildingPermitStatusBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the permit status if Other is selected as the Building Permit Status Type.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOString BuildingPermitStatusTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the BuildingPermitStatusTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BuildingPermitStatusTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool BuildingPermitStatusTypeOtherDescriptionSpecified
        {
            get { return BuildingPermitStatusTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Commentary about the building permit trade provided by the permitting authority and is not categorical in nature.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOString BuildingPermitTradeDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the BuildingPermitTradeDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BuildingPermitTradeDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool BuildingPermitTradeDescriptionSpecified
        {
            get { return BuildingPermitTradeDescription != null; }
            set { }
        }

        /// <summary>
        /// Commentary about the building usage permit provided by the permitting authority and is not categorical in nature.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOString BuildingPermitUsageStandardDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the BuildingPermitUsageStandardDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BuildingPermitUsageStandardDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool BuildingPermitUsageStandardDescriptionSpecified
        {
            get { return BuildingPermitUsageStandardDescription != null; }
            set { }
        }

        /// <summary>
        /// The usage standard designation assigned by the permitting authority for this permit.  
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOEnum<BuildingPermitUsageStandardBase> BuildingPermitUsageStandardType;

        /// <summary>
        /// Gets or sets a value indicating whether the BuildingPermitUsageStandardType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BuildingPermitUsageStandardType element has been assigned a value.</value>
        [XmlIgnore]
        public bool BuildingPermitUsageStandardTypeSpecified
        {
            get { return this.BuildingPermitUsageStandardType != null && this.BuildingPermitUsageStandardType.enumValue != BuildingPermitUsageStandardBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the permit usage standard type if Other is selected as the Building Permit Usage Standard Type. 
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOString BuildingPermitUsageStandardTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the BuildingPermitUsageStandardTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BuildingPermitUsageStandardTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool BuildingPermitUsageStandardTypeOtherDescriptionSpecified
        {
            get { return BuildingPermitUsageStandardTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Commentary about the building work purpose provided by the permitting authority and is not categorical in nature.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOString BuildingPermitWorkPurposeDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the BuildingPermitWorkPurposeDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BuildingPermitWorkPurposeDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool BuildingPermitWorkPurposeDescriptionSpecified
        {
            get { return BuildingPermitWorkPurposeDescription != null; }
            set { }
        }

        /// <summary>
        /// The class of work related to the current structure(s) (if any) on the property for this permit.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOEnum<BuildingPermitWorkPurposeBase> BuildingPermitWorkPurposeType;

        /// <summary>
        /// Gets or sets a value indicating whether the BuildingPermitWorkPurposeType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BuildingPermitWorkPurposeType element has been assigned a value.</value>
        [XmlIgnore]
        public bool BuildingPermitWorkPurposeTypeSpecified
        {
            get { return this.BuildingPermitWorkPurposeType != null && this.BuildingPermitWorkPurposeType.enumValue != BuildingPermitWorkPurposeBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the permit work class if Other is selected as the Building Permit Work Purpose Type.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOString BuildingPermitWorkPurposeTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the BuildingPermitWorkPurposeTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BuildingPermitWorkPurposeTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool BuildingPermitWorkPurposeTypeOtherDescriptionSpecified
        {
            get { return BuildingPermitWorkPurposeTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The primary type of trade work permissions on this permit and is directly related to the type of inspection for the component of the structure that would be required by the respective jurisdictional building code and conducted by the local permitting authority.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOEnum<BuildingPermitWorkBase> BuildingPermitWorkType;

        /// <summary>
        /// Gets or sets a value indicating whether the BuildingPermitWorkType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BuildingPermitWorkType element has been assigned a value.</value>
        [XmlIgnore]
        public bool BuildingPermitWorkTypeSpecified
        {
            get { return this.BuildingPermitWorkType != null && this.BuildingPermitWorkType.enumValue != BuildingPermitWorkBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the permitted work type if Other is selected as the Building Permit Work Type.
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMOString BuildingPermitWorkTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the BuildingPermitWorkTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BuildingPermitWorkTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool BuildingPermitWorkTypeOtherDescriptionSpecified
        {
            get { return BuildingPermitWorkTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 17)]
        public BUILDING_PERMIT_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
