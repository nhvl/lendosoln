namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class BUILDING_PERMITS
    {
        /// <summary>
        /// Gets a value indicating whether the BUILDING_PERMITS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BuildingPermitSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of building permits.
        /// </summary>
        [XmlElement("BUILDING_PERMIT", Order = 0)]
		public List<BUILDING_PERMIT> BuildingPermit = new List<BUILDING_PERMIT>();

        /// <summary>
        /// Gets or sets a value indicating whether the BuildingPermit element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BuildingPermit element has been assigned a value.</value>
        [XmlIgnore]
        public bool BuildingPermitSpecified
        {
            get { return this.BuildingPermit != null && this.BuildingPermit.Count(b => b != null && b.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public BUILDING_PERMITS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
