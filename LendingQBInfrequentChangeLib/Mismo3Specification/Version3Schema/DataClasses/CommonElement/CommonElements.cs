namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class COMMON_ELEMENTS
    {
        /// <summary>
        /// Gets a value indicating whether the COMMON_ELEMENTS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CommonElementSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of common elements.
        /// </summary>
        [XmlElement("COMMON_ELEMENT", Order = 0)]
		public List<COMMON_ELEMENT> CommonElement = new List<COMMON_ELEMENT>();

        /// <summary>
        /// Gets or sets a value indicating whether the CommonElement element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CommonElement element has been assigned a value.</value>
        [XmlIgnore]
        public bool CommonElementSpecified
        {
            get { return this.CommonElement != null && this.CommonElement.Count(c => c != null && c.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public COMMON_ELEMENTS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
