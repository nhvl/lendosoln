namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class SOLICITATION
    {
        /// <summary>
        /// Gets a value indicating whether the SOLICITATION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.SolicitationDateSpecified
                    || this.SolicitationMethodTypeOtherDescriptionSpecified
                    || this.SolicitationMethodTypeSpecified
                    || this.WorkoutSolicitationProgramIdentifierSpecified;
            }
        }

        /// <summary>
        /// The date on which the solicitation effort was made.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMODate SolicitationDate;

        /// <summary>
        /// Gets or sets a value indicating whether the SolicitationDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SolicitationDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool SolicitationDateSpecified
        {
            get { return SolicitationDate != null; }
            set { }
        }

        /// <summary>
        /// Specifies the method used in an attempt to make contact with the right party in a solicitation effort.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<SolicitationMethodBase> SolicitationMethodType;

        /// <summary>
        /// Gets or sets a value indicating whether the SolicitationMethodType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SolicitationMethodType element has been assigned a value.</value>
        [XmlIgnore]
        public bool SolicitationMethodTypeSpecified
        {
            get { return this.SolicitationMethodType != null && this.SolicitationMethodType.enumValue != SolicitationMethodBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture additional information when Other is selected for Solicitation Method Type.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString SolicitationMethodTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the SolicitationMethodTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SolicitationMethodTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool SolicitationMethodTypeOtherDescriptionSpecified
        {
            get { return SolicitationMethodTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A unique identifier of the workout solicitation program.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOIdentifier WorkoutSolicitationProgramIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the WorkoutSolicitationProgramIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the WorkoutSolicitationProgramIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool WorkoutSolicitationProgramIdentifierSpecified
        {
            get { return WorkoutSolicitationProgramIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public SOLICITATION_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
