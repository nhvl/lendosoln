namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class SOLICITATIONS
    {
        /// <summary>
        /// Gets a value indicating whether the SOLICITATIONS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.SolicitationSpecified;
            }
        }

        /// <summary>
        /// A collection of solicitations.
        /// </summary>
        [XmlElement("SOLICITATION", Order = 0)]
		public List<SOLICITATION> Solicitation = new List<SOLICITATION>();

        /// <summary>
        /// Gets or sets a value indicating whether the Solicitation element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Solicitation element has been assigned a value.</value>
        [XmlIgnore]
        public bool SolicitationSpecified
        {
            get { return this.Solicitation != null && this.Solicitation.Count(s => s != null && s.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public SOLICITATIONS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
