namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class INDIVIDUAL
    {
        /// <summary>
        /// Gets a value indicating whether the INDIVIDUAL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AliasesSpecified
                    || this.ContactPointsSpecified
                    || this.ExtensionSpecified
                    || this.IdentificationVerificationSpecified
                    || this.NameSpecified;
            }
        }

        /// <summary>
        /// Aliases for this individual.
        /// </summary>
        [XmlElement("ALIASES", Order = 0)]
        public ALIASES Aliases;

        /// <summary>
        /// Gets or sets a value indicating whether the Aliases element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Aliases element has been assigned a value.</value>
        [XmlIgnore]
        public bool AliasesSpecified
        {
            get { return this.Aliases != null && this.Aliases.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Points of contact for this individual.
        /// </summary>
        [XmlElement("CONTACT_POINTS", Order = 1)]
        public CONTACT_POINTS ContactPoints;

        /// <summary>
        /// Gets or sets a value indicating whether the ContactPoints element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ContactPoints element has been assigned a value.</value>
        [XmlIgnore]
        public bool ContactPointsSpecified
        {
            get { return this.ContactPoints != null && this.ContactPoints.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Identification verification for this individual.
        /// </summary>
        [XmlElement("IDENTIFICATION_VERIFICATION", Order = 2)]
        public IDENTIFICATION_VERIFICATION IdentificationVerification;

        /// <summary>
        /// Gets or sets a value indicating whether the IdentificationVerification element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IdentificationVerification element has been assigned a value.</value>
        [XmlIgnore]
        public bool IdentificationVerificationSpecified
        {
            get { return this.IdentificationVerification != null && this.IdentificationVerification.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A name for this individual.
        /// </summary>
        [XmlElement("NAME", Order = 3)]
        public NAME Name;

        /// <summary>
        /// Gets or sets a value indicating whether the Name element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Name element has been assigned a value.</value>
        [XmlIgnore]
        public bool NameSpecified
        {
            get { return this.Name != null && this.Name.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public INDIVIDUAL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
