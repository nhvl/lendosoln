namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CREDIT_SCORE_HISTOGRAM_INTERVALS
    {
        /// <summary>
        /// Gets a value indicating whether the CREDIT_SCORE_HISTOGRAM_INTERVALS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditScoreHistogramIntervalSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// Histogram interval of the credit score.
        /// </summary>
        [XmlElement("CREDIT_SCORE_HISTOGRAM_INTERVAL", Order = 0)]
		public List<CREDIT_SCORE_HISTOGRAM_INTERVAL> CreditScoreHistogramInterval = new List<CREDIT_SCORE_HISTOGRAM_INTERVAL>();

        /// <summary>
        /// Gets or sets a value indicating whether the CreditScoreHistogramInterval element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditScoreHistogramInterval element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditScoreHistogramIntervalSpecified
        {
            get { return this.CreditScoreHistogramInterval != null && this.CreditScoreHistogramInterval.Count(c => c != null && c.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public CREDIT_SCORE_HISTOGRAM_INTERVALS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
