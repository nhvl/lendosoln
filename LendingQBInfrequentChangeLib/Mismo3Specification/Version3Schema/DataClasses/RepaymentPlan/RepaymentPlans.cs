namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class REPAYMENT_PLANS
    {
        /// <summary>
        /// Gets a value indicating whether the REPAYMENT_PLANS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.RepaymentPlanSpecified;
            }
        }

        /// <summary>
        /// A collection of repayment plans.
        /// </summary>
        [XmlElement("REPAYMENT_PLAN", Order = 0)]
		public List<REPAYMENT_PLAN> RepaymentPlan = new List<REPAYMENT_PLAN>();

        /// <summary>
        /// Gets or sets a value indicating whether the RepaymentPlan element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RepaymentPlan element has been assigned a value.</value>
        [XmlIgnore]
        public bool RepaymentPlanSpecified
        {
            get { return this.RepaymentPlan != null && this.RepaymentPlan.Count(r => r != null && r.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public REPAYMENT_PLANS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
