namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class REPAYMENT_PLAN
    {
        /// <summary>
        /// Gets a value indicating whether the REPAYMENT_PLAN container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.PromiseToPayAmountSpecified
                    || this.PromiseToPayDueDateSpecified
                    || this.RepaymentPlanDownPaymentAmountSpecified
                    || this.RepaymentPlanEndDateSpecified
                    || this.RepaymentPlanMonthlyDueAmountSpecified
                    || this.RepaymentPlanNextDueDateSpecified
                    || this.RepaymentPlanStartDateSpecified
                    || this.RepaymentPlanTermMonthsCountSpecified;
            }
        }

        /// <summary>
        /// The dollar amount that the borrower and servicer agree will be paid by the borrower in order to cure a delinquency.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount PromiseToPayAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PromiseToPayAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PromiseToPayAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PromiseToPayAmountSpecified
        {
            get { return PromiseToPayAmount != null; }
            set { }
        }

        /// <summary>
        /// The date by which the borrower promised the funds would reach the servicer in order to cure a delinquency.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMODate PromiseToPayDueDate;

        /// <summary>
        /// Gets or sets a value indicating whether the PromiseToPayDueDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PromiseToPayDueDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool PromiseToPayDueDateSpecified
        {
            get { return PromiseToPayDueDate != null; }
            set { }
        }

        /// <summary>
        /// The initial lump sum amount paid prior to the first monthly payment for the Repayment Plan.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOAmount RepaymentPlanDownPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the RepaymentPlanDownPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RepaymentPlanDownPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool RepaymentPlanDownPaymentAmountSpecified
        {
            get { return RepaymentPlanDownPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// The scheduled end date of the repayment plan.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMODate RepaymentPlanEndDate;

        /// <summary>
        /// Gets or sets a value indicating whether the RepaymentPlanEndDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RepaymentPlanEndDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool RepaymentPlanEndDateSpecified
        {
            get { return RepaymentPlanEndDate != null; }
            set { }
        }

        /// <summary>
        /// The Repayment Plan monthly payment amount. The amount of repayment scheduled to be paid each month under the Repayment Plan.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOAmount RepaymentPlanMonthlyDueAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the RepaymentPlanMonthlyDueAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RepaymentPlanMonthlyDueAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool RepaymentPlanMonthlyDueAmountSpecified
        {
            get { return RepaymentPlanMonthlyDueAmount != null; }
            set { }
        }

        /// <summary>
        /// The date on which the next payment in the repayment plan is due. 
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMODate RepaymentPlanNextDueDate;

        /// <summary>
        /// Gets or sets a value indicating whether the RepaymentPlanNextDueDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RepaymentPlanNextDueDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool RepaymentPlanNextDueDateSpecified
        {
            get { return RepaymentPlanNextDueDate != null; }
            set { }
        }

        /// <summary>
        /// The date on which the repayment plan begins.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMODate RepaymentPlanStartDate;

        /// <summary>
        /// Gets or sets a value indicating whether the RepaymentPlanStartDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RepaymentPlanStartDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool RepaymentPlanStartDateSpecified
        {
            get { return RepaymentPlanStartDate != null; }
            set { }
        }

        /// <summary>
        /// The duration in months of a repayment plan agreement established with the borrower.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOCount RepaymentPlanTermMonthsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the RepaymentPlanTermMonthsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RepaymentPlanTermMonthsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool RepaymentPlanTermMonthsCountSpecified
        {
            get { return RepaymentPlanTermMonthsCount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 8)]
        public REPAYMENT_PLAN_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
