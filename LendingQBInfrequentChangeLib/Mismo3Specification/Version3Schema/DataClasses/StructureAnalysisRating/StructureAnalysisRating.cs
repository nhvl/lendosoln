namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class STRUCTURE_ANALYSIS_RATING
    {
        /// <summary>
        /// Gets a value indicating whether the STRUCTURE_ANALYSIS_RATING container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ConditionRatingTypeSpecified
                    || this.ExtensionSpecified
                    || this.StructureAnalysisRatingCommentDescriptionSpecified
                    || this.StructureAnalysisRatingItemTypeOtherDescriptionSpecified
                    || this.StructureAnalysisRatingItemTypeSpecified;
            }
        }

        /// <summary>
        /// A rating of the condition of the identified component.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<ConditionRatingBase> ConditionRatingType;

        /// <summary>
        /// Gets or sets a value indicating whether the ConditionRatingType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConditionRatingType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConditionRatingTypeSpecified
        {
            get { return this.ConditionRatingType != null && this.ConditionRatingType.enumValue != ConditionRatingBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field expanding on the Rating Condition Type.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString StructureAnalysisRatingCommentDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the StructureAnalysisRatingCommentDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the StructureAnalysisRatingCommentDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool StructureAnalysisRatingCommentDescriptionSpecified
        {
            get { return StructureAnalysisRatingCommentDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the characteristic of the structure being rated. The rating value of the characteristic is specified by Rating Condition Type.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<StructureAnalysisRatingItemBase> StructureAnalysisRatingItemType;

        /// <summary>
        /// Gets or sets a value indicating whether the StructureAnalysisRatingItemType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the StructureAnalysisRatingItemType element has been assigned a value.</value>
        [XmlIgnore]
        public bool StructureAnalysisRatingItemTypeSpecified
        {
            get { return this.StructureAnalysisRatingItemType != null && this.StructureAnalysisRatingItemType.enumValue != StructureAnalysisRatingItemBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the item of the structure being rated if Other is selected as the Structure Analysis Rating Item Type.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString StructureAnalysisRatingItemTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the StructureAnalysisRatingItemTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the StructureAnalysisRatingItemTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool StructureAnalysisRatingItemTypeOtherDescriptionSpecified
        {
            get { return StructureAnalysisRatingItemTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public STRUCTURE_ANALYSIS_RATING_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
