namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class STRUCTURE_ANALYSIS_RATINGS
    {
        /// <summary>
        /// Gets a value indicating whether the STRUCTURE_ANALYSIS_RATINGS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.StructureAnalysisRatingSpecified;
            }
        }

        /// <summary>
        /// A collection of structure analysis ratings.
        /// </summary>
        [XmlElement("STRUCTURE_ANALYSIS_RATING", Order = 0)]
		public List<STRUCTURE_ANALYSIS_RATING> StructureAnalysisRating = new List<STRUCTURE_ANALYSIS_RATING>();

        /// <summary>
        /// Gets or sets a value indicating whether the StructureAnalysisRating element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the StructureAnalysisRating element has been assigned a value.</value>
        [XmlIgnore]
        public bool StructureAnalysisRatingSpecified
        {
            get { return this.StructureAnalysisRating != null && this.StructureAnalysisRating.Count(s => s != null && s.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public STRUCTURE_ANALYSIS_RATINGS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
