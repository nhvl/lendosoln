namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class CREDIT_REPOSITORY
    {
        /// <summary>
        /// Gets a value indicating whether the CREDIT_REPOSITORY container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditRepositorySourceTypeOtherDescriptionSpecified
                    || this.CreditRepositorySourceTypeSpecified
                    || this.CreditRepositorySubscriberCodeSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// This element describes the source of the credit file, Equifax, Experian, Trans Union or Unspecified if the specific sources are not specified.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<CreditRepositorySourceBase> CreditRepositorySourceType;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditRepositorySourceType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditRepositorySourceType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditRepositorySourceTypeSpecified
        {
            get { return this.CreditRepositorySourceType != null && this.CreditRepositorySourceType.enumValue != CreditRepositorySourceBase.Blank; }
            set { }
        }

        /// <summary>
        /// When Credit Repository Source Type is set to Other, enter its value in this data element.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString CreditRepositorySourceTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditRepositorySourceTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditRepositorySourceTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditRepositorySourceTypeOtherDescriptionSpecified
        {
            get { return CreditRepositorySourceTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// This is the subscriber code (sometimes called member code) of the liability holder or provider of data to the credit repository bureau.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOCode CreditRepositorySubscriberCode;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditRepositorySubscriberCode element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditRepositorySubscriberCode element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditRepositorySubscriberCodeSpecified
        {
            get { return CreditRepositorySubscriberCode != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public CREDIT_REPOSITORY_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
