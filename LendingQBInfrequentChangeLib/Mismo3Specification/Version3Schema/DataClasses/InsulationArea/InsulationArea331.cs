namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class INSULATION_AREA_3_3_1
    {
        /// <summary>
        /// Gets a value indicating whether the INSULATION_AREA_3_3_1 container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.InsulationMaterialTypeOtherDescriptionSpecified
                    || this.InsulationMaterialTypeSpecified;
            }
        }

        /// <summary>
        /// A collection of values that specify the insulation material that is used in the specified Insulation Area Type.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<InsulationMaterialBase> InsulationMaterialType;

        /// <summary>
        /// Gets or sets a value indicating whether the InsulationMaterialType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InsulationMaterialType element has been assigned a value.</value>
        [XmlIgnore]
        public bool InsulationMaterialTypeSpecified
        {
            get { return this.InsulationMaterialType != null && this.InsulationMaterialType.enumValue != InsulationMaterialBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field that specifies the enumeration when the value of Other is used for the Insulation Material Type.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString InsulationMaterialTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the InsulationMaterialTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InsulationMaterialTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool InsulationMaterialTypeOtherDescriptionSpecified
        {
            get { return InsulationMaterialTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public INSULATION_AREA_3_3_1_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
