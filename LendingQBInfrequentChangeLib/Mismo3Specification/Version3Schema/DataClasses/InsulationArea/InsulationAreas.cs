namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class INSULATION_AREAS
    {
        /// <summary>
        /// Gets a value indicating whether the INSULATION_AREAS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.InsulationAreaSpecified;
            }
        }

        /// <summary>
        /// A collection of insulation areas.
        /// </summary>
        [XmlElement("INSULATION_AREA", Order = 0)]
		public List<INSULATION_AREA> InsulationArea = new List<INSULATION_AREA>();

        /// <summary>
        /// Gets or sets a value indicating whether the InsulationArea element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InsulationArea element has been assigned a value.</value>
        [XmlIgnore]
        public bool InsulationAreaSpecified
        {
            get { return this.InsulationArea != null && this.InsulationArea.Count(i => i != null && i.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public INSULATION_AREAS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
