namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class INSULATION_AREA
    {
        /// <summary>
        /// Gets a value indicating whether the INSULATION_AREA container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.InsulationAreaTypeOtherDescriptionSpecified
                    || this.InsulationAreaTypeSpecified
                    || this.InsulationCommentDescriptionSpecified
                    || this.InsulationPresenceTypeOtherDescriptionSpecified
                    || this.InsulationPresenceTypeSpecified
                    || this.InsulationRatingDescriptionSpecified;
            }
        }

        /// <summary>
        /// Indicates that the area specified by Insulation Area Type does have insulation.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<InsulationAreaBase> InsulationAreaType;

        /// <summary>
        /// Gets or sets a value indicating whether the InsulationAreaType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InsulationAreaType element has been assigned a value.</value>
        [XmlIgnore]
        public bool InsulationAreaTypeSpecified
        {
            get { return this.InsulationAreaType != null && this.InsulationAreaType.enumValue != InsulationAreaBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the location of the insulation if Other is selected as the Insulation Area Type.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString InsulationAreaTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the InsulationAreaTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InsulationAreaTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool InsulationAreaTypeOtherDescriptionSpecified
        {
            get { return InsulationAreaTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field for commenting on the presence or absence of insulation.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString InsulationCommentDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the InsulationCommentDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InsulationCommentDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool InsulationCommentDescriptionSpecified
        {
            get { return InsulationCommentDescription != null; }
            set { }
        }

        /// <summary>
        /// Identifies  the presence or absence of insulation in area as specified by the Insulation Area Type.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOEnum<InsulationPresenceBase> InsulationPresenceType;

        /// <summary>
        /// Gets or sets a value indicating whether the InsulationPresenceType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InsulationPresenceType element has been assigned a value.</value>
        [XmlIgnore]
        public bool InsulationPresenceTypeSpecified
        {
            get { return this.InsulationPresenceType != null && this.InsulationPresenceType.enumValue != InsulationPresenceBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Insulation Presence Type.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString InsulationPresenceTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the InsulationPresenceTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InsulationPresenceTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool InsulationPresenceTypeOtherDescriptionSpecified
        {
            get { return InsulationPresenceTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the rating (e.g. "R19") of the insulation.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString InsulationRatingDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the InsulationRatingDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InsulationRatingDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool InsulationRatingDescriptionSpecified
        {
            get { return InsulationRatingDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 6)]
        public INSULATION_AREA_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
