namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class RENTAL_FEATURE
    {
        /// <summary>
        /// Gets a value indicating whether the RENTAL_FEATURE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.RentalFeatureDescriptionSpecified
                    || this.RentalFeatureTypeOtherDescriptionSpecified
                    || this.RentalFeatureTypeSpecified;
            }
        }

        /// <summary>
        /// A free-form text field used to describe or comment on a particular feature used in determining the estimated market rent of rental unit or property.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString RentalFeatureDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the RentalFeatureDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RentalFeatureDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool RentalFeatureDescriptionSpecified
        {
            get { return RentalFeatureDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies a feature of a rental unit or property used in determining the estimated market rent of that unit or property.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<RentalFeatureBase> RentalFeatureType;

        /// <summary>
        /// Gets or sets a value indicating whether the RentalFeatureType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RentalFeatureType element has been assigned a value.</value>
        [XmlIgnore]
        public bool RentalFeatureTypeSpecified
        {
            get { return this.RentalFeatureType != null && this.RentalFeatureType.enumValue != RentalFeatureBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture additional information when Other is selected for Rental Feature Type.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString RentalFeatureTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the RentalFeatureTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RentalFeatureTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool RentalFeatureTypeOtherDescriptionSpecified
        {
            get { return RentalFeatureTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public RENTAL_FEATURE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
