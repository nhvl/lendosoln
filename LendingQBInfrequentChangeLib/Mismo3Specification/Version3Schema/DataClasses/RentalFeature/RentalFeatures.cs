namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class RENTAL_FEATURES
    {
        /// <summary>
        /// Gets a value indicating whether the RENTAL_FEATURES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.RentalFeatureSpecified;
            }
        }

        /// <summary>
        /// Container for various rental features.
        /// </summary>
        [XmlElement("RENTAL_FEATURE", Order = 0)]
		public List<RENTAL_FEATURE> RentalFeature = new List<RENTAL_FEATURE>();

        /// <summary>
        /// Gets or sets a value indicating whether the RentalFeature element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RentalFeature element has been assigned a value.</value>
        [XmlIgnore]
        public bool RentalFeatureSpecified
        {
            get { return this.RentalFeature != null && this.RentalFeature.Count(r => r != null && r.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public RENTAL_FEATURES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
