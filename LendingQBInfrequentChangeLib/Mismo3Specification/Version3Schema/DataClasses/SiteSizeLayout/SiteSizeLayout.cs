namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class SITE_SIZE_LAYOUT
    {
        /// <summary>
        /// Gets a value indicating whether the SITE_SIZE_LAYOUT container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ConditionRatingTypeSpecified
                    || this.ExtensionSpecified
                    || this.LengthFeetNumberSpecified
                    || this.LotSizeAreaNumberSpecified
                    || this.SiteDimensionsDescriptionSpecified
                    || this.SiteShapeDescriptionSpecified
                    || this.SiteSizeShapeTopographyAcceptableIndicatorSpecified
                    || this.SiteSizeShapeTopographyNotAcceptableDescriptionSpecified
                    || this.SiteTopographyDescriptionSpecified
                    || this.UnitOfMeasureTypeOtherDescriptionSpecified
                    || this.UnitOfMeasureTypeSpecified
                    || this.WidthFeetNumberSpecified;
            }
        }

        /// <summary>
        /// A rating of the condition of the identified component.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<ConditionRatingBase> ConditionRatingType;

        /// <summary>
        /// Gets or sets a value indicating whether the ConditionRatingType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConditionRatingType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConditionRatingTypeSpecified
        {
            get { return this.ConditionRatingType != null && this.ConditionRatingType.enumValue != ConditionRatingBase.Blank; }
            set { }
        }

        /// <summary>
        /// The length measured in linear feet.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMONumeric LengthFeetNumber;

        /// <summary>
        /// Gets or sets a value indicating whether the LengthFeetNumber element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LengthFeetNumber element has been assigned a value.</value>
        [XmlIgnore]
        public bool LengthFeetNumberSpecified
        {
            get { return LengthFeetNumber != null; }
            set { }
        }

        /// <summary>
        /// Identifies the total area of the site, inclusive of all parcels. Used in conjunction with Unit of Measure Type.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMONumeric LotSizeAreaNumber;

        /// <summary>
        /// Gets or sets a value indicating whether the LotSizeAreaNumber element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LotSizeAreaNumber element has been assigned a value.</value>
        [XmlIgnore]
        public bool LotSizeAreaNumberSpecified
        {
            get { return LotSizeAreaNumber != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the dimensions of the site such as its width and depth.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString SiteDimensionsDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the SiteDimensionsDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SiteDimensionsDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool SiteDimensionsDescriptionSpecified
        {
            get { return SiteDimensionsDescription != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the shape of the site.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString SiteShapeDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the SiteShapeDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SiteShapeDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool SiteShapeDescriptionSpecified
        {
            get { return SiteShapeDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates the  site size, shape, and topography are generally conforming to and acceptable to the market area.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOIndicator SiteSizeShapeTopographyAcceptableIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the SiteSizeShapeTopographyAcceptableIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SiteSizeShapeTopographyAcceptableIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool SiteSizeShapeTopographyAcceptableIndicatorSpecified
        {
            get { return SiteSizeShapeTopographyAcceptableIndicator != null; }
            set { }
        }

        /// <summary>
        /// A description of how the site size, shape and topography do not conform to what is acceptable in the market area. 
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOString SiteSizeShapeTopographyNotAcceptableDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the SiteSizeShapeTopographyNotAcceptableDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SiteSizeShapeTopographyNotAcceptableDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool SiteSizeShapeTopographyNotAcceptableDescriptionSpecified
        {
            get { return SiteSizeShapeTopographyNotAcceptableDescription != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the topography of the site.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOString SiteTopographyDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the SiteTopographyDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SiteTopographyDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool SiteTopographyDescriptionSpecified
        {
            get { return SiteTopographyDescription != null; }
            set { }
        }

        /// <summary>
        /// Identifies the type of measurement method applied.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOEnum<UnitOfMeasureBase> UnitOfMeasureType;

        /// <summary>
        /// Gets or sets a value indicating whether the UnitOfMeasureType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the UnitOfMeasureType element has been assigned a value.</value>
        [XmlIgnore]
        public bool UnitOfMeasureTypeSpecified
        {
            get { return this.UnitOfMeasureType != null && this.UnitOfMeasureType.enumValue != UnitOfMeasureBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Unit Of Measure Type.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOString UnitOfMeasureTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the UnitOfMeasureTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the UnitOfMeasureTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool UnitOfMeasureTypeOtherDescriptionSpecified
        {
            get { return UnitOfMeasureTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The width measured in linear feet.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMONumeric WidthFeetNumber;

        /// <summary>
        /// Gets or sets a value indicating whether the WidthFeetNumber element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the WidthFeetNumber element has been assigned a value.</value>
        [XmlIgnore]
        public bool WidthFeetNumberSpecified
        {
            get { return WidthFeetNumber != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 11)]
        public SITE_SIZE_LAYOUT_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
