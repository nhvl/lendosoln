namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CREDIT_LIABILITY_LATE_COUNT
    {
        /// <summary>
        /// Gets a value indicating whether the CREDIT_LIABILITY_LATE_COUNT container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditLiabilityLateCountDetailSpecified
                    || this.ExtensionSpecified
                    || this.PeriodicLateCountsSpecified;
            }
        }

        /// <summary>
        /// Details about late counts on a liability.
        /// </summary>
        [XmlElement("CREDIT_LIABILITY_LATE_COUNT_DETAIL", Order = 0)]
        public CREDIT_LIABILITY_LATE_COUNT_DETAIL CreditLiabilityLateCountDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLiabilityLateCountDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLiabilityLateCountDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLiabilityLateCountDetailSpecified
        {
            get { return this.CreditLiabilityLateCountDetail != null && this.CreditLiabilityLateCountDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Periodic late counts on a liability account.
        /// </summary>
        [XmlElement("PERIODIC_LATE_COUNTS", Order = 1)]
        public PERIODIC_LATE_COUNTS PeriodicLateCounts;

        /// <summary>
        /// Gets or sets a value indicating whether the PeriodicLateCounts element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PeriodicLateCounts element has been assigned a value.</value>
        [XmlIgnore]
        public bool PeriodicLateCountsSpecified
        {
            get { return this.PeriodicLateCounts != null && this.PeriodicLateCounts.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public CREDIT_LIABILITY_LATE_COUNT_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
