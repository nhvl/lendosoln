namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CREDIT_LIABILITY_LATE_COUNT_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the CREDIT_LIABILITY_LATE_COUNT_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditLiability120DaysLateCountSpecified
                    || this.CreditLiability30DaysLateCountSpecified
                    || this.CreditLiability60DaysLateCountSpecified
                    || this.CreditLiability90DaysLateCountSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// The number of times the liability account was reported 120 days late by the liability holder.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOCount CreditLiability120DaysLateCount;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLiability120DaysLateCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLiability120DaysLateCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLiability120DaysLateCountSpecified
        {
            get { return CreditLiability120DaysLateCount != null; }
            set { }
        }

        /// <summary>
        /// The number of times the liability account was reported 30 days late by the liability holder.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOCount CreditLiability30DaysLateCount;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLiability30DaysLateCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLiability30DaysLateCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLiability30DaysLateCountSpecified
        {
            get { return CreditLiability30DaysLateCount != null; }
            set { }
        }

        /// <summary>
        /// The number of times the liability account was reported 60 days late by the liability holder.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOCount CreditLiability60DaysLateCount;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLiability60DaysLateCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLiability60DaysLateCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLiability60DaysLateCountSpecified
        {
            get { return CreditLiability60DaysLateCount != null; }
            set { }
        }

        /// <summary>
        /// The number of times the liability account was reported 90 days late by the liability holder.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOCount CreditLiability90DaysLateCount;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLiability90DaysLateCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLiability90DaysLateCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLiability90DaysLateCountSpecified
        {
            get { return CreditLiability90DaysLateCount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public CREDIT_LIABILITY_LATE_COUNT_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
