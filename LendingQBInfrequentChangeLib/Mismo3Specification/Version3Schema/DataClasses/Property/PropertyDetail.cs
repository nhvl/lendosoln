namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PROPERTY_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the PROPERTY_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AssignmentOfRentsIndicatorSpecified
                    || this.AttachmentTypeSpecified
                    || this.BorrowerContinuesToOwnPropertyIndicatorSpecified
                    || this.CommunityLandTrustIndicatorSpecified
                    || this.CommunityReinvestmentActDelineatedCommunityIndicatorSpecified
                    || this.ConstructionMethodTypeOtherDescriptionSpecified
                    || this.ConstructionMethodTypeSpecified
                    || this.ConstructionStatusTypeOtherDescriptionSpecified
                    || this.ConstructionStatusTypeSpecified
                    || this.DeedRestrictionIndicatorSpecified
                    || this.DisasterIndicatorSpecified
                    || this.ExtensionSpecified
                    || this.FinancedUnitCountSpecified
                    || this.GrossLivingAreaSquareFeetNumberSpecified
                    || this.GroupHomeIndicatorSpecified
                    || this.InvestorREOPropertyIdentifierSpecified
                    || this.LandTrustTypeOtherDescriptionSpecified
                    || this.LandTrustTypeSpecified
                    || this.LandUseDescriptionSpecified
                    || this.LandUseTypeOtherDescriptionSpecified
                    || this.LandUseTypeSpecified
                    || this.LenderDesignatedDecliningMarketIdentifierSpecified
                    || this.NativeAmericanLandsTypeOtherDescriptionSpecified
                    || this.NativeAmericanLandsTypeSpecified
                    || this.PriorSaleInLastTwelveMonthsIndicatorSpecified
                    || this.PropertyAcquiredDateSpecified
                    || this.PropertyAcquiredYearSpecified
                    || this.PropertyAcreageNumberSpecified
                    || this.PropertyConditionDescriptionSpecified
                    || this.PropertyCurrentOccupancyTypeSpecified
                    || this.PropertyCurrentOccupantNameSpecified
                    || this.PropertyCurrentUsageTypeOtherDescriptionSpecified
                    || this.PropertyCurrentUsageTypeSpecified
                    || this.PropertyDispositionStatusTypeOtherDescriptionSpecified
                    || this.PropertyDispositionStatusTypeSpecified
                    || this.PropertyEarthquakeInsuranceIndicatorSpecified
                    || this.PropertyEstateTypeOtherDescriptionSpecified
                    || this.PropertyEstateTypeSpecified
                    || this.PropertyEstimatedValueAmountSpecified
                    || this.PropertyExistingLienAmountSpecified
                    || this.PropertyFloodInsuranceIndicatorSpecified
                    || this.PropertyGroundLeaseExpirationDateSpecified
                    || this.PropertyGroundLeasePerpetualIndicatorSpecified
                    || this.PropertyInclusionaryZoningIndicatorSpecified
                    || this.PropertyOriginalCostAmountSpecified
                    || this.PropertyPreviouslyOccupiedIndicatorSpecified
                    || this.PropertyStructureBuiltYearEstimatedIndicatorSpecified
                    || this.PropertyStructureBuiltYearSpecified
                    || this.PropertyStructureHabitableYearRoundIndicatorSpecified
                    || this.PropertyUsageTypeOtherDescriptionSpecified
                    || this.PropertyUsageTypeSpecified
                    || this.PropertyVacancyDateSpecified
                    || this.PUDIndicatorSpecified
                    || this.RuralUnderservedCountyIndicatorSpecified
                    || this.UniqueDwellingTypeOtherDescriptionSpecified
                    || this.UniqueDwellingTypeSpecified;
            }
        }

        /// <summary>
        /// When true, indicates that a signed Assignment of Rents associated with the property is in effect.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIndicator AssignmentOfRentsIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the AssignmentOfRentsIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AssignmentOfRentsIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool AssignmentOfRentsIndicatorSpecified
        {
            get { return AssignmentOfRentsIndicator != null; }
            set { }
        }

        /// <summary>
        /// Specifies the type of physical attachment, if any, between the dwelling unit and adjacent dwelling units.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<AttachmentBase> AttachmentType;

        /// <summary>
        /// Gets or sets a value indicating whether the AttachmentType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AttachmentType element has been assigned a value.</value>
        [XmlIgnore]
        public bool AttachmentTypeSpecified
        {
            get { return this.AttachmentType != null && this.AttachmentType.enumValue != AttachmentBase.Blank; }
            set { }
        }

        /// <summary>
        /// When true, indicates that the property associated with the loan is still owned by the borrower(s).
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOIndicator BorrowerContinuesToOwnPropertyIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the BorrowerContinuesToOwnPropertyIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BorrowerContinuesToOwnPropertyIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool BorrowerContinuesToOwnPropertyIndicatorSpecified
        {
            get { return BorrowerContinuesToOwnPropertyIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates the the property is located in a community land trust.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOIndicator CommunityLandTrustIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the CommunityLandTrustIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CommunityLandTrustIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool CommunityLandTrustIndicatorSpecified
        {
            get { return CommunityLandTrustIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the property is in an area defined by the Community Reinvestment Act.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOIndicator CommunityReinvestmentActDelineatedCommunityIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the CommunityReinvestmentActDelineatedCommunityIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CommunityReinvestmentActDelineatedCommunityIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool CommunityReinvestmentActDelineatedCommunityIndicatorSpecified
        {
            get { return CommunityReinvestmentActDelineatedCommunityIndicator != null; }
            set { }
        }

        /// <summary>
        /// Describes the construction process for the main dwelling unit of the subject property.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOEnum<ConstructionMethodBase> ConstructionMethodType;

        /// <summary>
        /// Gets or sets a value indicating whether the ConstructionMethodType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConstructionMethodType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConstructionMethodTypeSpecified
        {
            get { return this.ConstructionMethodType != null && this.ConstructionMethodType.enumValue != ConstructionMethodBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Construction Method Type.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOString ConstructionMethodTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ConstructionMethodTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConstructionMethodTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConstructionMethodTypeOtherDescriptionSpecified
        {
            get { return ConstructionMethodTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the physical status of the structure.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOEnum<ConstructionStatusBase> ConstructionStatusType;

        /// <summary>
        /// Gets or sets a value indicating whether the ConstructionStatusType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConstructionStatusType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConstructionStatusTypeSpecified
        {
            get { return this.ConstructionStatusType != null && this.ConstructionStatusType.enumValue != ConstructionStatusBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Construction Status Type.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOString ConstructionStatusTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ConstructionStatusTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConstructionStatusTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConstructionStatusTypeOtherDescriptionSpecified
        {
            get { return ConstructionStatusTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// If true, indicates that the deed has associated restrictions.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOIndicator DeedRestrictionIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the DeedRestrictionIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DeedRestrictionIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool DeedRestrictionIndicatorSpecified
        {
            get { return DeedRestrictionIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the property is damaged as a result of a natural disaster, such as a hurricane, tornado, or earthquake.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOIndicator DisasterIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the DisasterIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DisasterIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool DisasterIndicatorSpecified
        {
            get { return DisasterIndicator != null; }
            set { }
        }

        /// <summary>
        /// The number of individual family dwelling units being financed in the subject property.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOCount FinancedUnitCount;

        /// <summary>
        /// Gets or sets a value indicating whether the FinancedUnitCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FinancedUnitCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool FinancedUnitCountSpecified
        {
            get { return FinancedUnitCount != null; }
            set { }
        }

        /// <summary>
        /// The total area of all habitable rooms above grade measured contiguously.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMONumeric GrossLivingAreaSquareFeetNumber;

        /// <summary>
        /// Gets or sets a value indicating whether the GrossLivingAreaSquareFeetNumber element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GrossLivingAreaSquareFeetNumber element has been assigned a value.</value>
        [XmlIgnore]
        public bool GrossLivingAreaSquareFeetNumberSpecified
        {
            get { return GrossLivingAreaSquareFeetNumber != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates the property will be used as a group home. A group home is used for individuals with special needs, including the frail elderly, developmentally and/or physically disabled.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOIndicator GroupHomeIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the GroupHomeIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GroupHomeIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool GroupHomeIndicatorSpecified
        {
            get { return GroupHomeIndicator != null; }
            set { }
        }

        /// <summary>
        /// A unique identifier assigned by an investor to a property acquired as the result of delinquency.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOIdentifier InvestorREOPropertyIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the InvestorREOPropertyIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InvestorREOPropertyIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool InvestorREOPropertyIdentifierSpecified
        {
            get { return InvestorREOPropertyIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Specifies the type of land trust associated with the title.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOEnum<LandTrustBase> LandTrustType;

        /// <summary>
        /// Gets or sets a value indicating whether the LandTrustType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LandTrustType element has been assigned a value.</value>
        [XmlIgnore]
        public bool LandTrustTypeSpecified
        {
            get { return this.LandTrustType != null && this.LandTrustType.enumValue != LandTrustBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Land Trust Type.
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMOString LandTrustTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the LandTrustTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LandTrustTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool LandTrustTypeOtherDescriptionSpecified
        {
            get { return LandTrustTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to further describe the land use of the property as defined by Land Use Type.
        /// </summary>
        [XmlElement(Order = 17)]
        public MISMOString LandUseDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the LandUseDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LandUseDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool LandUseDescriptionSpecified
        {
            get { return LandUseDescription != null; }
            set { }
        }

        /// <summary>
        /// Identifies the designation of land use by a governmental authority described by Land Used Description.
        /// </summary>
        [XmlElement(Order = 18)]
        public MISMOEnum<LandUseBase> LandUseType;

        /// <summary>
        /// Gets or sets a value indicating whether the LandUseType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LandUseType element has been assigned a value.</value>
        [XmlIgnore]
        public bool LandUseTypeSpecified
        {
            get { return this.LandUseType != null && this.LandUseType.enumValue != LandUseBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the land use designation if Other is selected as the Property Land Use Type.
        /// </summary>
        [XmlElement(Order = 19)]
        public MISMOString LandUseTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the LandUseTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LandUseTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool LandUseTypeOtherDescriptionSpecified
        {
            get { return LandUseTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Identifies the type of declining market based on lender analysis.
        /// </summary>
        [XmlElement(Order = 20)]
        public MISMOIdentifier LenderDesignatedDecliningMarketIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the LenderDesignatedDecliningMarketIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LenderDesignatedDecliningMarketIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool LenderDesignatedDecliningMarketIdentifierSpecified
        {
            get { return LenderDesignatedDecliningMarketIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Any land granted to or acquired by a federally-recognized tribe. Title may be held in fee simple, or held by the federal government in trust through a leasehold for the benefit of the tribe.
        /// </summary>
        [XmlElement(Order = 21)]
        public MISMOEnum<NativeAmericanLandsBase> NativeAmericanLandsType;

        /// <summary>
        /// Gets or sets a value indicating whether the NativeAmericanLandsType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NativeAmericanLandsType element has been assigned a value.</value>
        [XmlIgnore]
        public bool NativeAmericanLandsTypeSpecified
        {
            get { return this.NativeAmericanLandsType != null && this.NativeAmericanLandsType.enumValue != NativeAmericanLandsBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Native American Lands Type.
        /// </summary>
        [XmlElement(Order = 22)]
        public MISMOString NativeAmericanLandsTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the NativeAmericanLandsTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NativeAmericanLandsTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool NativeAmericanLandsTypeOtherDescriptionSpecified
        {
            get { return NativeAmericanLandsTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates that property has been sold in the twelve months prior to origination of the delivery loan.
        /// </summary>
        [XmlElement(Order = 23)]
        public MISMOIndicator PriorSaleInLastTwelveMonthsIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PriorSaleInLastTwelveMonthsIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PriorSaleInLastTwelveMonthsIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PriorSaleInLastTwelveMonthsIndicatorSpecified
        {
            get { return PriorSaleInLastTwelveMonthsIndicator != null; }
            set { }
        }

        /// <summary>
        /// The date the property was acquired. 
        /// </summary>
        [XmlElement(Order = 24)]
        public MISMODate PropertyAcquiredDate;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyAcquiredDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyAcquiredDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyAcquiredDateSpecified
        {
            get { return PropertyAcquiredDate != null; }
            set { }
        }

        /// <summary>
        /// The year in which the land and buildings of the property were acquired by the borrower.
        /// </summary>
        [XmlElement(Order = 25)]
        public MISMOYear PropertyAcquiredYear;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyAcquiredYear element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyAcquiredYear element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyAcquiredYearSpecified
        {
            get { return PropertyAcquiredYear != null; }
            set { }
        }

        /// <summary>
        /// Number of acres of the subject property.
        /// </summary>
        [XmlElement(Order = 26)]
        public MISMONumeric PropertyAcreageNumber;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyAcreageNumber element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyAcreageNumber element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyAcreageNumberSpecified
        {
            get { return PropertyAcreageNumber != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field describing the condition of the property.
        /// </summary>
        [XmlElement(Order = 27)]
        public MISMOString PropertyConditionDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyConditionDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyConditionDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyConditionDescriptionSpecified
        {
            get { return PropertyConditionDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the property occupancy status of a subject property.
        /// </summary>
        [XmlElement(Order = 28)]
        public MISMOEnum<PropertyCurrentOccupancyBase> PropertyCurrentOccupancyType;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyCurrentOccupancyType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyCurrentOccupancyType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyCurrentOccupancyTypeSpecified
        {
            get { return this.PropertyCurrentOccupancyType != null && this.PropertyCurrentOccupancyType.enumValue != PropertyCurrentOccupancyBase.Blank; }
            set { }
        }

        /// <summary>
        /// The unparsed name of the current occupant of the property.
        /// </summary>
        [XmlElement(Order = 29)]
        public MISMOString PropertyCurrentOccupantName;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyCurrentOccupantName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyCurrentOccupantName element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyCurrentOccupantNameSpecified
        {
            get { return PropertyCurrentOccupantName != null; }
            set { }
        }

        /// <summary>
        /// Specifies the current use of the property.
        /// </summary>
        [XmlElement(Order = 30)]
        public MISMOEnum<PropertyCurrentUsageBase> PropertyCurrentUsageType;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyCurrentUsageType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyCurrentUsageType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyCurrentUsageTypeSpecified
        {
            get { return this.PropertyCurrentUsageType != null && this.PropertyCurrentUsageType.enumValue != PropertyCurrentUsageBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Property Current Usage Type.
        /// </summary>
        [XmlElement(Order = 31)]
        public MISMOString PropertyCurrentUsageTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyCurrentUsageTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyCurrentUsageTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyCurrentUsageTypeOtherDescriptionSpecified
        {
            get { return PropertyCurrentUsageTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The intended disposition of the property.
        /// </summary>
        [XmlElement(Order = 32)]
        public MISMOEnum<PropertyDispositionStatusBase> PropertyDispositionStatusType;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyDispositionStatusType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyDispositionStatusType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyDispositionStatusTypeSpecified
        {
            get { return this.PropertyDispositionStatusType != null && this.PropertyDispositionStatusType.enumValue != PropertyDispositionStatusBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Property Disposition Status Type.
        /// </summary>
        [XmlElement(Order = 33)]
        public MISMOString PropertyDispositionStatusTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyDispositionStatusTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyDispositionStatusTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyDispositionStatusTypeOtherDescriptionSpecified
        {
            get { return PropertyDispositionStatusTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// An indicator denoting whether the property securing the mortgage has earthquake insurance.
        /// </summary>
        [XmlElement(Order = 34)]
        public MISMOIndicator PropertyEarthquakeInsuranceIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyEarthquakeInsuranceIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyEarthquakeInsuranceIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyEarthquakeInsuranceIndicatorSpecified
        {
            get { return PropertyEarthquakeInsuranceIndicator != null; }
            set { }
        }

        /// <summary>
        /// Specifies the ownership interest in the property.
        /// </summary>
        [XmlElement(Order = 35)]
        public MISMOEnum<PropertyEstateBase> PropertyEstateType;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyEstateType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyEstateType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyEstateTypeSpecified
        {
            get { return this.PropertyEstateType != null && this.PropertyEstateType.enumValue != PropertyEstateBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Property Estate Type.
        /// </summary>
        [XmlElement(Order = 36)]
        public MISMOString PropertyEstateTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyEstateTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyEstateTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyEstateTypeOtherDescriptionSpecified
        {
            get { return PropertyEstateTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A statement of the estimated present market value of the property that is from the borrower or loan originator.
        /// </summary>
        [XmlElement(Order = 37)]
        public MISMOAmount PropertyEstimatedValueAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyEstimatedValueAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyEstimatedValueAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyEstimatedValueAmountSpecified
        {
            get { return PropertyEstimatedValueAmount != null; }
            set { }
        }

        /// <summary>
        /// The total amount of liens against the property at time of application for a loan. Used for Construction or Refinance loans.
        /// </summary>
        [XmlElement(Order = 38)]
        public MISMOAmount PropertyExistingLienAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyExistingLienAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyExistingLienAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyExistingLienAmountSpecified
        {
            get { return PropertyExistingLienAmount != null; }
            set { }
        }

        /// <summary>
        /// An indicator denoting whether the property securing the mortgage has flood insurance.
        /// </summary>
        [XmlElement(Order = 39)]
        public MISMOIndicator PropertyFloodInsuranceIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyFloodInsuranceIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyFloodInsuranceIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyFloodInsuranceIndicatorSpecified
        {
            get { return PropertyFloodInsuranceIndicator != null; }
            set { }
        }

        /// <summary>
        /// The final expiration date of the ground lease.
        /// </summary>
        [XmlElement(Order = 40)]
        public MISMODate PropertyGroundLeaseExpirationDate;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyGroundLeaseExpirationDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyGroundLeaseExpirationDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyGroundLeaseExpirationDateSpecified
        {
            get { return PropertyGroundLeaseExpirationDate != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the ground lease is renewable in perpetuity.
        /// </summary>
        [XmlElement(Order = 41)]
        public MISMOIndicator PropertyGroundLeasePerpetualIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyGroundLeasePerpetualIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyGroundLeasePerpetualIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyGroundLeasePerpetualIndicatorSpecified
        {
            get { return PropertyGroundLeasePerpetualIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the subject property is subject to Inclusionary zoning, a land-use concept in which builders include a certain amount of housing for eligible borrowers, usually low- and moderate-income households in exchange for considerations, such as low.
        /// </summary>
        [XmlElement(Order = 42)]
        public MISMOIndicator PropertyInclusionaryZoningIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyInclusionaryZoningIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyInclusionaryZoningIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyInclusionaryZoningIndicatorSpecified
        {
            get { return PropertyInclusionaryZoningIndicator != null; }
            set { }
        }

        /// <summary>
        /// The original cost of acquiring the entire property - land and structure. This is used for refinance loans.
        /// </summary>
        [XmlElement(Order = 43)]
        public MISMOAmount PropertyOriginalCostAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyOriginalCostAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyOriginalCostAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyOriginalCostAmountSpecified
        {
            get { return PropertyOriginalCostAmount != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the property has been previously occupied.
        /// </summary>
        [XmlElement(Order = 44)]
        public MISMOIndicator PropertyPreviouslyOccupiedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyPreviouslyOccupiedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyPreviouslyOccupiedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyPreviouslyOccupiedIndicatorSpecified
        {
            get { return PropertyPreviouslyOccupiedIndicator != null; }
            set { }
        }

        /// <summary>
        /// The year in which the dwelling on the property was completed.
        /// </summary>
        [XmlElement(Order = 45)]
        public MISMOYear PropertyStructureBuiltYear;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyStructureBuiltYear element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyStructureBuiltYear element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyStructureBuiltYearSpecified
        {
            get { return PropertyStructureBuiltYear != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the structure year built referenced is an estimate.
        /// </summary>
        [XmlElement(Order = 46)]
        public MISMOIndicator PropertyStructureBuiltYearEstimatedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyStructureBuiltYearEstimatedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyStructureBuiltYearEstimatedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyStructureBuiltYearEstimatedIndicatorSpecified
        {
            get { return PropertyStructureBuiltYearEstimatedIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the structure or improvement is habitable year round.
        /// </summary>
        [XmlElement(Order = 47)]
        public MISMOIndicator PropertyStructureHabitableYearRoundIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyStructureHabitableYearRoundIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyStructureHabitableYearRoundIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyStructureHabitableYearRoundIndicatorSpecified
        {
            get { return PropertyStructureHabitableYearRoundIndicator != null; }
            set { }
        }

        /// <summary>
        /// Specifies the usage intention of the borrower for the property.
        /// </summary>
        [XmlElement(Order = 48)]
        public MISMOEnum<PropertyUsageBase> PropertyUsageType;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyUsageType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyUsageType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyUsageTypeSpecified
        {
            get { return this.PropertyUsageType != null && this.PropertyUsageType.enumValue != PropertyUsageBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Property Usage Type.
        /// </summary>
        [XmlElement(Order = 49)]
        public MISMOString PropertyUsageTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyUsageTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyUsageTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyUsageTypeOtherDescriptionSpecified
        {
            get { return PropertyUsageTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The date the property was first determined to be vacant as determined by the date reported on an inspection report.
        /// </summary>
        [XmlElement(Order = 50)]
        public MISMODate PropertyVacancyDate;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyVacancyDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyVacancyDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyVacancyDateSpecified
        {
            get { return PropertyVacancyDate != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the project in which the subject property is located is a Planned Unit Development (PUD). A comprehensive development plan for a large land area. A PUD usually includes residences, roads, schools, recreational facilities, commercial, office and industrial areas. Also, a subdivision having lots or areas owned in common and reserved for the use of some or all of the owners of the separately owned lots. 
        /// </summary>
        [XmlElement(Order = 51)]
        public MISMOIndicator PUDIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PUDIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PUDIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PUDIndicatorSpecified
        {
            get { return PUDIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates the subject property county has been identified by the Bureau of Consumer Financial Protection as Rural or Underserved.
        /// </summary>
        [XmlElement(Order = 52)]
        public MISMOIndicator RuralUnderservedCountyIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the RuralUnderservedCountyIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RuralUnderservedCountyIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool RuralUnderservedCountyIndicatorSpecified
        {
            get { return RuralUnderservedCountyIndicator != null; }
            set { }
        }

        /// <summary>
        /// A dwelling that does not conform to a conventional building structure type or method.
        /// </summary>
        [XmlElement(Order = 53)]
        public MISMOEnum<UniqueDwellingBase> UniqueDwellingType;

        /// <summary>
        /// Gets or sets a value indicating whether the UniqueDwellingType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the UniqueDwellingType element has been assigned a value.</value>
        [XmlIgnore]
        public bool UniqueDwellingTypeSpecified
        {
            get { return this.UniqueDwellingType != null && this.UniqueDwellingType.enumValue != UniqueDwellingBase.Blank; }
            set { }
        }

        /// <summary>
        /// A text field used to describe the Unique Dwelling when "Other" is selected from the enumerated list.
        /// </summary>
        [XmlElement(Order = 54)]
        public MISMOString UniqueDwellingTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the UniqueDwellingTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the UniqueDwellingTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool UniqueDwellingTypeOtherDescriptionSpecified
        {
            get { return UniqueDwellingTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 55)]
        public PROPERTY_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
