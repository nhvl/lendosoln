namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets the Valuation Use Type.
    /// </summary>
    /// <value>A boolean indicating whether the ExpenseType element has been assigned a value.</value>
    public partial class PROPERTY
    {
        /// <summary>
        /// Gets a value indicating whether the PROPERTY container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AddressSpecified
                    || this.BuildingPermitsSpecified
                    || this.CertificatesSpecified
                    || this.ComparableSpecified
                    || this.DataSourcesSpecified
                    || this.DisastersSpecified
                    || this.EncumbrancesSpecified
                    || this.EnvironmentalConditionsSpecified
                    || this.ExtensionSpecified
                    || this.FloodDeterminationSpecified
                    || this.HazardInsurancesSpecified
                    || this.HomeownersAssociationSpecified
                    || this.ImprovementSpecified
                    || this.InspectionsSpecified
                    || this.LegalDescriptionsSpecified
                    || this.LicensesSpecified
                    || this.ListingInformationsSpecified
                    || this.LocationIdentifierSpecified
                    || this.ManufacturedHomeSpecified
                    || this.MarketSpecified
                    || this.NeighborhoodSpecified
                    || this.ProjectSpecified
                    || this.PropertyDetailSpecified
                    || this.PropertyTaxesSpecified
                    || this.PropertyTitleSpecified
                    || this.PropertyUnitsSpecified
                    || this.PropertyValuationsSpecified
                    || this.RepairSpecified
                    || this.SalesContractsSpecified
                    || this.SalesHistoriesSpecified
                    || this.ServitudesSpecified
                    || this.SiteSpecified;
            }
        }

        /// <summary>
        /// Address of the property.
        /// </summary>
        [XmlElement("ADDRESS", Order = 0)]
        public ADDRESS Address;

        /// <summary>
        /// Gets or sets a value indicating whether the Address element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Address element has been assigned a value.</value>
        [XmlIgnore]
        public bool AddressSpecified
        {
            get { return this.Address != null && this.Address.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Building permits associated with the property.
        /// </summary>
        [XmlElement("BUILDING_PERMITS", Order = 1)]
        public BUILDING_PERMITS BuildingPermits;

        /// <summary>
        /// Gets or sets a value indicating whether the BuildingPermits element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BuildingPermits element has been assigned a value.</value>
        [XmlIgnore]
        public bool BuildingPermitsSpecified
        {
            get { return this.BuildingPermits != null && this.BuildingPermits.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Certificates on the property.
        /// </summary>
        [XmlElement("CERTIFICATES", Order = 2)]
        public CERTIFICATES Certificates;

        /// <summary>
        /// Gets or sets a value indicating whether the Certificates element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Certificates element has been assigned a value.</value>
        [XmlIgnore]
        public bool CertificatesSpecified
        {
            get { return this.Certificates != null && this.Certificates.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Comparable on the property.
        /// </summary>
        [XmlElement("COMPARABLE", Order = 3)]
        public COMPARABLE Comparable;

        /// <summary>
        /// Gets or sets a value indicating whether the Comparable element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Comparable element has been assigned a value.</value>
        [XmlIgnore]
        public bool ComparableSpecified
        {
            get { return this.Comparable != null && this.Comparable.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Data sources for the property.
        /// </summary>
        [XmlElement("DATA_SOURCES", Order = 4)]
        public DATA_SOURCES DataSources;

        /// <summary>
        /// Gets or sets a value indicating whether the DataSources element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DataSources element has been assigned a value.</value>
        [XmlIgnore]
        public bool DataSourcesSpecified
        {
            get { return this.DataSources != null && this.DataSources.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Disasters that have affected the property.
        /// </summary>
        [XmlElement("DISASTERS", Order = 5)]
        public DISASTERS Disasters;

        /// <summary>
        /// Gets or sets a value indicating whether the Disasters element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Disasters element has been assigned a value.</value>
        [XmlIgnore]
        public bool DisastersSpecified
        {
            get { return this.Disasters != null && this.Disasters.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Encumbrances on the property.
        /// </summary>
        [XmlElement("ENCUMBRANCES", Order = 6)]
        public ENCUMBRANCES Encumbrances;

        /// <summary>
        /// Gets or sets a value indicating whether the Encumbrances element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Encumbrances element has been assigned a value.</value>
        [XmlIgnore]
        public bool EncumbrancesSpecified
        {
            get { return this.Encumbrances != null && this.Encumbrances.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Environmental conditions on the property.
        /// </summary>
        [XmlElement("ENVIRONMENTAL_CONDITIONS", Order = 7)]
        public ENVIRONMENTAL_CONDITIONS EnvironmentalConditions;

        /// <summary>
        /// Gets or sets a value indicating whether the EnvironmentalConditions element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EnvironmentalConditions element has been assigned a value.</value>
        [XmlIgnore]
        public bool EnvironmentalConditionsSpecified
        {
            get { return this.EnvironmentalConditions != null && this.EnvironmentalConditions.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Flood determination for the property.
        /// </summary>
        [XmlElement("FLOOD_DETERMINATION", Order = 8)]
        public FLOOD_DETERMINATION FloodDetermination;

        /// <summary>
        /// Gets or sets a value indicating whether the FloodDetermination element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FloodDetermination element has been assigned a value.</value>
        [XmlIgnore]
        public bool FloodDeterminationSpecified
        {
            get { return this.FloodDetermination != null && this.FloodDetermination.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Hazard insurances on the property.
        /// </summary>
        [XmlElement("HAZARD_INSURANCES", Order = 9)]
        public HAZARD_INSURANCES HazardInsurances;

        /// <summary>
        /// Gets or sets a value indicating whether the HazardInsurances element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HazardInsurances element has been assigned a value.</value>
        [XmlIgnore]
        public bool HazardInsurancesSpecified
        {
            get { return this.HazardInsurances != null && this.HazardInsurances.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The HOA governing the property.
        /// </summary>
        [XmlElement("HOMEOWNERS_ASSOCIATION", Order = 10)]
        public HOMEOWNERS_ASSOCIATION HomeownersAssociation;

        /// <summary>
        /// Gets or sets a value indicating whether the HomeownersAssociation element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HomeownersAssociation element has been assigned a value.</value>
        [XmlIgnore]
        public bool HomeownersAssociationSpecified
        {
            get { return this.HomeownersAssociation != null && this.HomeownersAssociation.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Improvements on the property.
        /// </summary>
        [XmlElement("IMPROVEMENT", Order = 11)]
        public IMPROVEMENT Improvement;

        /// <summary>
        /// Gets or sets a value indicating whether the Improvement element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Improvement element has been assigned a value.</value>
        [XmlIgnore]
        public bool ImprovementSpecified
        {
            get { return this.Improvement != null && this.Improvement.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Inspections on the property.
        /// </summary>
        [XmlElement("INSPECTIONS", Order = 12)]
        public INSPECTIONS Inspections;

        /// <summary>
        /// Gets or sets a value indicating whether the Inspections element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Inspections element has been assigned a value.</value>
        [XmlIgnore]
        public bool InspectionsSpecified
        {
            get { return this.Inspections != null && this.Inspections.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Legal descriptions of the property.
        /// </summary>
        [XmlElement("LEGAL_DESCRIPTIONS", Order = 13)]
        public LEGAL_DESCRIPTIONS LegalDescriptions;

        /// <summary>
        /// Gets or sets a value indicating whether the LegalDescriptions element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LegalDescriptions element has been assigned a value.</value>
        [XmlIgnore]
        public bool LegalDescriptionsSpecified
        {
            get { return this.LegalDescriptions != null && this.LegalDescriptions.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Property licenses.
        /// </summary>
        [XmlElement("LICENSES", Order = 14)]
        public LICENSES Licenses;

        /// <summary>
        /// Gets or sets a value indicating whether the Licenses element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Licenses element has been assigned a value.</value>
        [XmlIgnore]
        public bool LicensesSpecified
        {
            get { return this.Licenses != null && this.Licenses.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Property listing information.
        /// </summary>
        [XmlElement("LISTING_INFORMATIONS", Order = 15)]
        public LISTING_INFORMATIONS ListingInformations;

        /// <summary>
        /// Gets or sets a value indicating whether the ListingInformation element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ListingInformation element has been assigned a value.</value>
        [XmlIgnore]
        public bool ListingInformationsSpecified
        {
            get { return this.ListingInformations != null && this.ListingInformations.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Identifier for the location of the property.
        /// </summary>
        [XmlElement("LOCATION_IDENTIFIER", Order = 16)]
        public LOCATION_IDENTIFIER LocationIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the LocationIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LocationIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool LocationIdentifierSpecified
        {
            get { return this.LocationIdentifier != null && this.LocationIdentifier.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details if the property has a manufactured home.
        /// </summary>
        [XmlElement("MANUFACTURED_HOME", Order = 17)]
        public MANUFACTURED_HOME ManufacturedHome;

        /// <summary>
        /// Gets or sets a value indicating whether the ManufacturedHome element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ManufacturedHome element has been assigned a value.</value>
        [XmlIgnore]
        public bool ManufacturedHomeSpecified
        {
            get { return this.ManufacturedHome != null && this.ManufacturedHome.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The market associated with the property.
        /// </summary>
        [XmlElement("MARKET", Order = 18)]
        public MARKET Market;

        /// <summary>
        /// Gets or sets a value indicating whether the Market element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Market element has been assigned a value.</value>
        [XmlIgnore]
        public bool MarketSpecified
        {
            get { return this.Market != null && this.Market.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The neighborhood where the property is located.
        /// </summary>
        [XmlElement("NEIGHBORHOOD", Order = 19)]
        public NEIGHBORHOOD Neighborhood;

        /// <summary>
        /// Gets or sets a value indicating whether the Neighborhood element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Neighborhood element has been assigned a value.</value>
        [XmlIgnore]
        public bool NeighborhoodSpecified
        {
            get { return this.Neighborhood != null && this.Neighborhood.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Project related to the property.
        /// </summary>
        [XmlElement("PROJECT", Order = 20)]
        public PROJECT Project;

        /// <summary>
        /// Gets or sets a value indicating whether the Project element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Project element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectSpecified
        {
            get { return this.Project != null && this.Project.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details on the property.
        /// </summary>
        [XmlElement("PROPERTY_DETAIL", Order = 21)]
        public PROPERTY_DETAIL PropertyDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyDetailSpecified
        {
            get { return this.PropertyDetail != null && this.PropertyDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Taxes on the property.
        /// </summary>
        [XmlElement("PROPERTY_TAXES", Order = 22)]
        public PROPERTY_TAXES PropertyTaxes;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyTaxes element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyTaxes element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyTaxesSpecified
        {
            get { return this.PropertyTaxes != null && this.PropertyTaxes.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Title for the property.
        /// </summary>
        [XmlElement("PROPERTY_TITLE", Order = 23)]
        public PROPERTY_TITLE PropertyTitle;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyTitle element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyTitle element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyTitleSpecified
        {
            get { return this.PropertyTitle != null && this.PropertyTitle.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Units on the property.
        /// </summary>
        [XmlElement("PROPERTY_UNITS", Order = 24)]
        public PROPERTY_UNITS PropertyUnits;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyUnits element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyUnits element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyUnitsSpecified
        {
            get { return this.PropertyUnits != null && this.PropertyUnits.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Valuations of the property.
        /// </summary>
        [XmlElement("PROPERTY_VALUATIONS", Order = 25)]
        public PROPERTY_VALUATIONS PropertyValuations;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyValuations element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyValuations element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyValuationsSpecified
        {
            get { return this.PropertyValuations != null && this.PropertyValuations.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Repairs on the property.
        /// </summary>
        [XmlElement("REPAIR", Order = 26)]
        public REPAIR Repair;

        /// <summary>
        /// Gets or sets a value indicating whether the Repair element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Repair element has been assigned a value.</value>
        [XmlIgnore]
        public bool RepairSpecified
        {
            get { return this.Repair != null && this.Repair.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Sale contracts of the property.
        /// </summary>
        [XmlElement("SALES_CONTRACTS", Order = 27)]
        public SALES_CONTRACTS SalesContracts;

        /// <summary>
        /// Gets or sets a value indicating whether the SalesContracts element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SalesContracts element has been assigned a value.</value>
        [XmlIgnore]
        public bool SalesContractsSpecified
        {
            get { return this.SalesContracts != null && this.SalesContracts.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The sale history of the property.
        /// </summary>
        [XmlElement("SALES_HISTORIES", Order = 28)]
        public SALES_HISTORIES SalesHistories;

        /// <summary>
        /// Gets or sets a value indicating whether the SalesHistories element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SalesHistories element has been assigned a value.</value>
        [XmlIgnore]
        public bool SalesHistoriesSpecified
        {
            get { return this.SalesHistories != null && this.SalesHistories.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Servitudes on the property.
        /// </summary>
        [XmlElement("SERVITUDES", Order = 29)]
        public SERVITUDES Servitudes;

        /// <summary>
        /// Gets or sets a value indicating whether the Servitudes element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Servitudes element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServitudesSpecified
        {
            get { return this.Servitudes != null && this.Servitudes.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The site of the property.
        /// </summary>
        [XmlElement("SITE", Order = 30)]
        public SITE Site;

        /// <summary>
        /// Gets or sets a value indicating whether the Site element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Site element has been assigned a value.</value>
        [XmlIgnore]
        public bool SiteSpecified
        {
            get { return this.Site != null && this.Site.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 31)]
        public PROPERTY_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }

        /// <summary>
        /// Used as an attribute on PROPERTY to specify whether the referenced property is the Subject property or a property valuation comparable.
        /// </summary>
        [XmlIgnore]
        private ValuationUseBase valuationUseType;

        /// <summary>
        /// Gets or sets the Valuation Use Type.
        /// </summary>
        /// <value>A boolean indicating whether the ExpenseType element has been assigned a value.</value>
        [XmlIgnore]
        public ValuationUseBase ValuationUseType
        {
            get { return valuationUseType; }
            set
            {
                this.valuationUseType = value;
                this.IsValuationUseTypeSpecified = this.valuationUseType != ValuationUseBase.Blank;
            }
        }

        /// <summary>
        /// Indicates whether the Valuation Use Type has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool IsValuationUseTypeSpecified = false;

        /// <summary>
        /// Gets or sets the Valuation Use Type member as a string for serialization.
        /// </summary>
        /// <value>A boolean indicating whether the ExpenseType element has been assigned a value.</value>
        [XmlAttribute(AttributeName = "ValuationUseType")]
        public string ValuationUseTypeSerialized
        {
            get { return Mismo3Utilities.GetXmlEnumName(ValuationUseType); }
            set { }
        }

        /// <summary>
        /// Indicates whether the Valuation Use Type can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the Valuation Use Type can be serialized.</returns>
        public bool ShouldSerializeValuationUseTypeSerialized()
        {
            return IsValuationUseTypeSpecified;
        }
    }
}
