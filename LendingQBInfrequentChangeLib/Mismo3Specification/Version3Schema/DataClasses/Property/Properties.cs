namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PROPERTIES
    {
        /// <summary>
        /// Gets a value indicating whether the PROPERTIES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.PropertySpecified;
            }
        }

        /// <summary>
        /// A collection of properties.
        /// </summary>
        [XmlElement("PROPERTY", Order = 0)]
		public List<PROPERTY> Property = new List<PROPERTY>();

        /// <summary>
        /// Gets or sets a value indicating whether the Property element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Property element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertySpecified
        {
            get { return this.Property != null && this.Property.Count(p => p != null && p.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public PROPERTIES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
