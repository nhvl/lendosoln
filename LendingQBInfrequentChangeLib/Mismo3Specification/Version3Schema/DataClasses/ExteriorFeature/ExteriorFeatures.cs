namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class EXTERIOR_FEATURES
    {
        /// <summary>
        /// Gets a value indicating whether the EXTERIOR_FEATURES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ExteriorFeatureSpecified;
            }
        }

        /// <summary>
        /// A collection of exterior features.
        /// </summary>
        [XmlElement("EXTERIOR_FEATURE", Order = 0)]
		public List<EXTERIOR_FEATURE> ExteriorFeature = new List<EXTERIOR_FEATURE>();

        /// <summary>
        /// Gets or sets a value indicating whether the ExteriorFeature element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ExteriorFeature element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExteriorFeatureSpecified
        {
            get { return this.ExteriorFeature != null && this.ExteriorFeature.Count(e => e != null && e.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public EXTERIOR_FEATURES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
