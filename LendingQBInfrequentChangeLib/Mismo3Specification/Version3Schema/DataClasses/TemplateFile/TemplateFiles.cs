namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class TEMPLATE_FILES
    {
        /// <summary>
        /// Gets a value indicating whether the TEMPLATE_FILES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.TemplateFileSpecified;
            }
        }

        /// <summary>
        /// A collection of template files.
        /// </summary>
        [XmlElement("TEMPLATE_FILE", Order = 0)]
		public List<TEMPLATE_FILE> TemplateFile = new List<TEMPLATE_FILE>();

        /// <summary>
        /// Gets or sets a value indicating whether the TemplateFile element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TemplateFile element has been assigned a value.</value>
        [XmlIgnore]
        public bool TemplateFileSpecified
        {
            get { return this.TemplateFile != null && this.TemplateFile.Count(t => t != null && t.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public TEMPLATE_FILES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
