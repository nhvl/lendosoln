namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PROPERTY_PHOTO_INFORMATIONS
    {
        /// <summary>
        /// Gets a value indicating whether the PROPERTY_PHOTO_INFORMATIONS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.PropertyPhotoInformationSpecified;
            }
        }

        /// <summary>
        /// A collection of property photo information.
        /// </summary>
        [XmlElement("PROPERTY_PHOTO_INFORMATION", Order = 0)]
		public List<PROPERTY_PHOTO_INFORMATION> PropertyPhotoInformation = new List<PROPERTY_PHOTO_INFORMATION>();

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyPhotoInformation element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyPhotoInformation element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyPhotoInformationSpecified
        {
            get { return this.PropertyPhotoInformation != null && this.PropertyPhotoInformation.Count(p => p != null && p.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public PROPERTY_PHOTO_INFORMATIONS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
