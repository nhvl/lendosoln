namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class AVM
    {
        /// <summary>
        /// Gets a value indicating whether the container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AVMCascadePreferenceIdentifierSpecified
                    || this.AVMConfidenceLevelIdentifierSpecified
                    || this.AVMConfidenceScoreIndicatorSpecified
                    || this.AVMConfidenceScorenumValueSpecified
                    || this.AVMDateSpecified
                    || this.AVMHighValueRangeAmountSpecified
                    || this.AVMIndexTypeOtherDescriptionSpecified
                    || this.AVMIndexTypeSpecified
                    || this.AVMLowValueRangeAmountSpecified
                    || this.AVMMethodTypeOtherDescriptionSpecified
                    || this.AVMMethodTypeSpecified
                    || this.AVMModelEffectiveDateSpecified
                    || this.AVMModelNameTypeOtherDescriptionSpecified
                    || this.AVMModelNameTypeSpecified
                    || this.AVMOutcomeTypeOtherDescriptionSpecified
                    || this.AVMOutcomeTypeSpecified
                    || this.AVMServiceProviderNameSpecified
                    || this.AVMValueAmountSpecified
                    || this.ExtensionSpecified
                    || this.ForecastStandardDeviationScoreValueSpecified
                    || this.ForecastStandardDeviationScoringModelNameSpecified
                    || this.ValuationRequestCascadeRuleIdentifierSpecified
                    || this.ValuationRequestCascadingReturnTypeOtherDescriptionSpecified
                    || this.ValuationRequestCascadingReturnTypeSpecified;
            }
        }

        /// <summary>
        /// The name of the AVM preference table applied in this transaction.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIdentifier AVMCascadePreferenceIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the AVMCascadePreferenceIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AVMCascadePreferenceIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool AVMCascadePreferenceIdentifierSpecified
        {
            get { return AVMCascadePreferenceIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The level of the confidence score as expressed as a rating for a given AVM provider.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOIdentifier AVMConfidenceLevelIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the AVMConfidenceLevelIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AVMConfidenceLevelIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool AVMConfidenceLevelIdentifierSpecified
        {
            get { return AVMConfidenceLevelIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Indicator that the particular automated valuation model has the ability to generate a confidence level estimate of its results.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOIndicator AVMConfidenceScoreIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the AVMConfidenceScoreIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AVMConfidenceScoreIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool AVMConfidenceScoreIndicatorSpecified
        {
            get { return AVMConfidenceScoreIndicator != null; }
            set { }
        }

        /// <summary>
        /// Value of AVM confidence.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOValue AVMConfidenceScoreValue;

        /// <summary>
        /// Gets or sets a value indicating whether the AVMConfidenceScoreValue element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AVMConfidenceScoreValue element has been assigned a value.</value>
        [XmlIgnore]
        public bool AVMConfidenceScorenumValueSpecified
        {
            get { return AVMConfidenceScoreValue != null; }
            set { }
        }

        /// <summary>
        /// The date the automated valuation was calculated for the subject Property.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMODate AVMDate;

        /// <summary>
        /// Gets or sets a value indicating whether the AVMDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AVMDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool AVMDateSpecified
        {
            get { return AVMDate != null; }
            set { }
        }

        /// <summary>
        /// The upper end of the value range for the subject property resulting from the application of an automated valuation model.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOAmount AVMHighValueRangeAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the AVMHighValueRangeAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AVMHighValueRangeAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool AVMHighValueRangeAmountSpecified
        {
            get { return AVMHighValueRangeAmount != null; }
            set { }
        }

        /// <summary>
        /// The name of the published index used in the AVM.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOEnum<AVMIndexBase> AVMIndexType;

        /// <summary>
        /// Gets or sets a value indicating whether the AVMIndexType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AVMIndexType element has been assigned a value.</value>
        [XmlIgnore]
        public bool AVMIndexTypeSpecified
        {
            get { return this.AVMIndexType != null && this.AVMIndexType.enumValue != AVMIndexBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for AVM Index Type.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOString AVMIndexTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the AVMIndexTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AVMIndexTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool AVMIndexTypeOtherDescriptionSpecified
        {
            get { return AVMIndexTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The low end of the value range for the subject property resulting from the application of an automated valuation model.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOAmount AVMLowValueRangeAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the AVMLowValueRangeAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AVMLowValueRangeAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool AVMLowValueRangeAmountSpecified
        {
            get { return AVMLowValueRangeAmount != null; }
            set { }
        }

        /// <summary>
        /// Specifies the method used by the Automated Valuation system.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOEnum<AVMMethodBase> AVMMethodType;

        /// <summary>
        /// Gets or sets a value indicating whether the AVMMethodType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AVMMethodType element has been assigned a value.</value>
        [XmlIgnore]
        public bool AVMMethodTypeSpecified
        {
            get { return this.AVMMethodType != null && this.AVMMethodType.enumValue != AVMMethodBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Automated Valuation Method Type.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOString AVMMethodTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the AVMMethodTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AVMMethodTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool AVMMethodTypeOtherDescriptionSpecified
        {
            get { return AVMMethodTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The last update date for the AVM used in this transaction.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMODate AVMModelEffectiveDate;

        /// <summary>
        /// Gets or sets a value indicating whether the AVMModelEffectiveDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AVMModelEffectiveDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool AVMModelEffectiveDateSpecified
        {
            get { return AVMModelEffectiveDate != null; }
            set { }
        }

        /// <summary>
        /// The name of the AVM model used to calculate the valuation of the subject property.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOEnum<AVMModelNameBase> AVMModelNameType;

        /// <summary>
        /// Gets or sets a value indicating whether the AVMModelNameType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AVMModelNameType element has been assigned a value.</value>
        [XmlIgnore]
        public bool AVMModelNameTypeSpecified
        {
            get { return this.AVMModelNameType != null && this.AVMModelNameType.enumValue != AVMModelNameBase.Blank; }
            set { }
        }

        /// <summary>
        /// The name of the AVM model used to calculate the valuation of the subject property when Other is selected from the enumerated list.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOString AVMModelNameTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the AVMModelNameTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AVMModelNameTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool AVMModelNameTypeOtherDescriptionSpecified
        {
            get { return AVMModelNameTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The type of value computed by the AVM for presentation.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOEnum<AVMOutcomeBase> AVMOutcomeType;

        /// <summary>
        /// Gets or sets a value indicating whether the AVMOutcomeType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AVMOutcomeType element has been assigned a value.</value>
        [XmlIgnore]
        public bool AVMOutcomeTypeSpecified
        {
            get { return this.AVMOutcomeType != null && this.AVMOutcomeType.enumValue != AVMOutcomeBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for AVM Outcome Type.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOString AVMOutcomeTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the AVMOutcomeTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AVMOutcomeTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool AVMOutcomeTypeOtherDescriptionSpecified
        {
            get { return AVMOutcomeTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The vendor who provided an AVM evaluation for the subject property.
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMOString AVMServiceProviderName;

        /// <summary>
        /// Gets or sets a value indicating whether the AVMServiceProviderName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AVMServiceProviderName element has been assigned a value.</value>
        [XmlIgnore]
        public bool AVMServiceProviderNameSpecified
        {
            get { return AVMServiceProviderName != null; }
            set { }
        }

        /// <summary>
        /// The AVM computed value of the subject property resulting from the application of an automated valuation model.
        /// </summary>
        [XmlElement(Order = 17)]
        public MISMOAmount AVMValueAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the AVMValueAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AVMValueAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool AVMValueAmountSpecified
        {
            get { return AVMValueAmount != null; }
            set { }
        }

        /// <summary>
        /// Value of the forecast standard deviation.
        /// </summary>
        [XmlElement(Order = 18)]
        public MISMOValue ForecastStandardDeviationScoreValue;

        /// <summary>
        /// Gets or sets a value indicating whether the ForecastStandardDeviationScoreValue element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ForecastStandardDeviationScoreValue element has been assigned a value.</value>
        [XmlIgnore]
        public bool ForecastStandardDeviationScoreValueSpecified
        {
            get { return ForecastStandardDeviationScoreValue != null; }
            set { }
        }

        /// <summary>
        /// The name of the Forecast Standard Deviation scoring model presented by the selected AVM.
        /// </summary>
        [XmlElement(Order = 19)]
        public MISMOString ForecastStandardDeviationScoringModelName;

        /// <summary>
        /// Gets or sets a value indicating whether the ForecastStandardDeviationScoringModelName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ForecastStandardDeviationScoringModelName element has been assigned a value.</value>
        [XmlIgnore]
        public bool ForecastStandardDeviationScoringModelNameSpecified
        {
            get { return ForecastStandardDeviationScoringModelName != null; }
            set { }
        }

        /// <summary>
        /// Identifier used by the requesting party to specify user-selected rules, methods or strategies to combine and/or select from a group of valuation products or services.
        /// </summary>
        [XmlElement(Order = 20)]
        public MISMOIdentifier ValuationRequestCascadeRuleIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the ValuationRequestCascadeRuleIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ValuationRequestCascadeRuleIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool ValuationRequestCascadeRuleIdentifierSpecified
        {
            get { return ValuationRequestCascadeRuleIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Describes a specific type of valuation group method or valuation product returned to the requesting party based on the user-selected rules. 
        /// </summary>
        [XmlElement(Order = 21)]
        public MISMOEnum<ValuationRequestCascadingReturnBase> ValuationRequestCascadingReturnType;

        /// <summary>
        /// Gets or sets a value indicating whether the ValuationRequestCascadingReturnType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ValuationRequestCascadingReturnType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ValuationRequestCascadingReturnTypeSpecified
        {
            get { return this.ValuationRequestCascadingReturnType != null && this.ValuationRequestCascadingReturnType.enumValue != ValuationRequestCascadingReturnBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the Valuation Request Cascading Return Type if Other is selected.
        /// </summary>
        [XmlElement(Order = 22)]
        public MISMOString ValuationRequestCascadingReturnTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ValuationRequestCascadingReturnTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ValuationRequestCascadingReturnTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ValuationRequestCascadingReturnTypeOtherDescriptionSpecified
        {
            get { return ValuationRequestCascadingReturnTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 23)]
        public AVM_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
