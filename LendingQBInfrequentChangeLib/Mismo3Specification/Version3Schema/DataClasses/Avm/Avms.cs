namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class AVMS
    {
        /// <summary>
        /// Gets a value indicating whether the AVMS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AvmSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of AVMs.
        /// </summary>
        [XmlElement("AVM", Order = 0)]
		public List<AVM> Avm = new List<AVM>();

        /// <summary>
        /// Gets or sets a value indicating whether the AVM element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AVM element has been assigned a value.</value>
        [XmlIgnore]
        public bool AvmSpecified
        {
            get { return this.Avm != null && this.Avm.Count(a => a != null && a.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public AVMS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
