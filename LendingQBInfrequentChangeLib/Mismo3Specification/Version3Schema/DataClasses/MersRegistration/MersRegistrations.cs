namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class MERS_REGISTRATIONS
    {
        /// <summary>
        /// Gets a value indicating whether the MERS_REGISTRATIONS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.MersRegistrationSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of Mortgage Electronic Registration Systems registrations.
        /// </summary>
        [XmlElement("MERS_REGISTRATION", Order = 0)]
		public List<MERS_REGISTRATION> MersRegistration = new List<MERS_REGISTRATION>();

        /// <summary>
        /// Gets or sets a value indicating whether the Mortgage Electronic Registration Systems Registration element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Mortgage Electronic Registration Systems Registration element has been assigned a value.</value>
        [XmlIgnore]
        public bool MersRegistrationSpecified
        {
            get { return MersRegistration != null && MersRegistration.Count(m => m != null && m.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public MERS_REGISTRATIONS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
