namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class MERS_REGISTRATION
    {
        /// <summary>
        /// Gets a value indicating whether the MERS_REGISTRATION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.MERS_IRegistrationIndicatorSpecified
                    || this.MERSOriginalMortgageeOfRecordIndicatorSpecified
                    || this.MERSRegistrationDateSpecified
                    || this.MERSRegistrationStatusTypeOtherDescriptionSpecified
                    || this.MERSRegistrationStatusTypeSpecified
                    || this.MERSRegistrationTypeOtherDescriptionSpecified
                    || this.MERSRegistrationTypeSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// Indicates the loan is registered on the MERS System for information only. MERS is not the mortgagee.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIndicator MERS_IRegistrationIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the MERS_IRegistrationIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MERS_IRegistrationIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool MERS_IRegistrationIndicatorSpecified
        {
            get { return MERS_IRegistrationIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether the loan was originated with MERS as the original mortgagee of record (MOM).
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOIndicator MERSOriginalMortgageeOfRecordIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the MERSOriginalMortgageeOfRecordIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MERSOriginalMortgageeOfRecordIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool MERSOriginalMortgageeOfRecordIndicatorSpecified
        {
            get { return MERSOriginalMortgageeOfRecordIndicator != null; }
            set { }
        }

        /// <summary>
        /// The date the loan was registered with MERS.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMODate MERSRegistrationDate;

        /// <summary>
        /// Gets or sets a value indicating whether the MERSRegistrationDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MERSRegistrationDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool MERSRegistrationDateSpecified
        {
            get { return MERSRegistrationDate != null; }
            set { }
        }

        /// <summary>
        /// The status of the loans registration with MERS. A loan is registered one time with MERS with its Mortgage Identification Number (MIN). Various life of loan activities may alter the registration status  such as a deactivation transaction.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOEnum<MERSRegistrationStatusBase> MERSRegistrationStatusType;

        /// <summary>
        /// Gets or sets a value indicating whether the MERSRegistrationStatusType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MERSRegistrationStatusType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MERSRegistrationStatusTypeSpecified
        {
            get { return this.MERSRegistrationStatusType != null && this.MERSRegistrationStatusType.enumValue != MERSRegistrationStatusBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture the MERS Registration Status Type name if Other is selected as the MERS Registration Status Type.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString MERSRegistrationStatusTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MERSRegistrationStatusTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MERSRegistrationStatusTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MERSRegistrationStatusTypeOtherDescriptionSpecified
        {
            get { return MERSRegistrationStatusTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The system at MERS that the loan is registered on.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOEnum<MERSRegistrationBase> MERSRegistrationType;

        /// <summary>
        /// Gets or sets a value indicating whether the MERSRegistrationType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MERSRegistrationType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MERSRegistrationTypeSpecified
        {
            get { return this.MERSRegistrationType != null && this.MERSRegistrationType.enumValue != MERSRegistrationBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture additional information if Other is selected as MERS Registration Type.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOString MERSRegistrationTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MERSRegistrationTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MERSRegistrationTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MERSRegistrationTypeOtherDescriptionSpecified
        {
            get { return MERSRegistrationTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 7)]
        public MERS_REGISTRATION_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
