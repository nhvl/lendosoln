namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class MONETARY_EVENT_SUMMARY_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the MONETARY_EVENT_SUMMARY_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.SummaryMonetaryEventBorrowerIncentiveCurtailmentAmountSpecified
                    || this.SummaryMonetaryEventDeferredPrincipalCurtailmentAmountSpecified
                    || this.SummaryMonetaryEventDeferredUPBAmountSpecified
                    || this.SummaryMonetaryEventEscrowPaymentAmountSpecified
                    || this.SummaryMonetaryEventGrossInterestAmountSpecified
                    || this.SummaryMonetaryEventGrossPrincipalAmountSpecified
                    || this.SummaryMonetaryEventInterestBearingUPBAmountSpecified
                    || this.SummaryMonetaryEventInterestPaidThroughDateSpecified
                    || this.SummaryMonetaryEventInvestorSpecialHandlingDescriptionSpecified
                    || this.SummaryMonetaryEventLastPaidInstallmentAppliedDateSpecified
                    || this.SummaryMonetaryEventLastPaidInstallmentDueDateSpecified
                    || this.SummaryMonetaryEventNetInterestAmountSpecified
                    || this.SummaryMonetaryEventNetPrincipalAmountSpecified
                    || this.SummaryMonetaryEventOptionalProductsPaymentAmountSpecified
                    || this.SummaryMonetaryEventScheduledUPBAmountSpecified
                    || this.SummaryMonetaryEventUPBAmountSpecified
                    || this.SummaryMonetaryInvestorRemittanceAmountSpecified
                    || this.SummaryMonetaryInvestorRemittanceEffectiveDateSpecified
                    || this.SuspenseBalanceAmountSpecified
                    || this.UnremittedOptionalProductsPremiumBalanceAmountSpecified;
            }
        }

        /// <summary>
        /// The amount of any Borrower incentive payments applied to the unpaid principal balance of the Mortgage during the accounting cycle.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount SummaryMonetaryEventBorrowerIncentiveCurtailmentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the SummaryMonetaryEventBorrowerIncentiveCurtailmentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SummaryMonetaryEventBorrowerIncentiveCurtailmentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool SummaryMonetaryEventBorrowerIncentiveCurtailmentAmountSpecified
        {
            get { return SummaryMonetaryEventBorrowerIncentiveCurtailmentAmount != null; }
            set { }
        }

        /// <summary>
        /// The amount of any curtailment applied to the deferred UPB during the accounting cycle.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOAmount SummaryMonetaryEventDeferredPrincipalCurtailmentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the SummaryMonetaryEventDeferredPrincipalCurtailmentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SummaryMonetaryEventDeferredPrincipalCurtailmentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool SummaryMonetaryEventDeferredPrincipalCurtailmentAmountSpecified
        {
            get { return SummaryMonetaryEventDeferredPrincipalCurtailmentAmount != null; }
            set { }
        }

        /// <summary>
        /// For mortgages with a partial principal forbearance, the amount of the deferred (non-interest bearing) unpaid principal balance as of the accounting cycle cutoff.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOAmount SummaryMonetaryEventDeferredUPBAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the SummaryMonetaryEventDeferredUPBAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SummaryMonetaryEventDeferredUPBAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool SummaryMonetaryEventDeferredUPBAmountSpecified
        {
            get { return SummaryMonetaryEventDeferredUPBAmount != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of all payments by the borrower being reported in the Loan Monetary Event Summary applied to the escrow balance.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOAmount SummaryMonetaryEventEscrowPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the SummaryMonetaryEventEscrowPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SummaryMonetaryEventEscrowPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool SummaryMonetaryEventEscrowPaymentAmountSpecified
        {
            get { return SummaryMonetaryEventEscrowPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// The portion of all payments being reported in the Loan Monetary Event Summary allocated to interest based upon the current loan interest rate.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOAmount SummaryMonetaryEventGrossInterestAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the SummaryMonetaryEventGrossInterestAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SummaryMonetaryEventGrossInterestAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool SummaryMonetaryEventGrossInterestAmountSpecified
        {
            get { return SummaryMonetaryEventGrossInterestAmount != null; }
            set { }
        }

        /// <summary>
        /// The portion of all payments being reported in the Loan Monetary Event Summary that is applied to principal.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOAmount SummaryMonetaryEventGrossPrincipalAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the SummaryMonetaryEventGrossPrincipalAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SummaryMonetaryEventGrossPrincipalAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool SummaryMonetaryEventGrossPrincipalAmountSpecified
        {
            get { return SummaryMonetaryEventGrossPrincipalAmount != null; }
            set { }
        }

        /// <summary>
        /// For mortgages with a partial principal forbearance, the amount of the interest-bearing unpaid principal balance as of the accounting cycle cutoff. 
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOAmount SummaryMonetaryEventInterestBearingUPBAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the SummaryMonetaryEventInterestBearingUPBAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SummaryMonetaryEventInterestBearingUPBAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool SummaryMonetaryEventInterestBearingUPBAmountSpecified
        {
            get { return SummaryMonetaryEventInterestBearingUPBAmount != null; }
            set { }
        }

        /// <summary>
        /// The date through which interest is paid with the final activity being reported in the Loan Monetary Event Summary. This is the effective date from which interest will be calculated for the application of the next payment. (For example, used for Daily Simple Interest loans.).
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMODate SummaryMonetaryEventInterestPaidThroughDate;

        /// <summary>
        /// Gets or sets a value indicating whether the SummaryMonetaryEventInterestPaidThroughDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SummaryMonetaryEventInterestPaidThroughDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool SummaryMonetaryEventInterestPaidThroughDateSpecified
        {
            get { return SummaryMonetaryEventInterestPaidThroughDate != null; }
            set { }
        }

        /// <summary>
        /// Investor specific instructions associated with the Loan Monetary Event Summary.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOString SummaryMonetaryEventInvestorSpecialHandlingDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the SummaryMonetaryEventInvestorSpecialHandlingDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SummaryMonetaryEventInvestorSpecialHandlingDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool SummaryMonetaryEventInvestorSpecialHandlingDescriptionSpecified
        {
            get { return SummaryMonetaryEventInvestorSpecialHandlingDescription != null; }
            set { }
        }

        /// <summary>
        /// The date that the final activity being reported in the Loan Monetary Event Summary was actually applied.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMODate SummaryMonetaryEventLastPaidInstallmentAppliedDate;

        /// <summary>
        /// Gets or sets a value indicating whether the SummaryMonetaryEventLastPaidInstallmentAppliedDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SummaryMonetaryEventLastPaidInstallmentAppliedDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool SummaryMonetaryEventLastPaidInstallmentAppliedDateSpecified
        {
            get { return SummaryMonetaryEventLastPaidInstallmentAppliedDate != null; }
            set { }
        }

        /// <summary>
        /// The due date of last paid installment (DDLPI) being included in the summary reporting.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMODate SummaryMonetaryEventLastPaidInstallmentDueDate;

        /// <summary>
        /// Gets or sets a value indicating whether the SummaryMonetaryEventLastPaidInstallmentDueDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SummaryMonetaryEventLastPaidInstallmentDueDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool SummaryMonetaryEventLastPaidInstallmentDueDateSpecified
        {
            get { return SummaryMonetaryEventLastPaidInstallmentDueDate != null; }
            set { }
        }

        /// <summary>
        /// The portion of the Summary Loan Monetary Event Gross Interest Amount due to the Investor.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOAmount SummaryMonetaryEventNetInterestAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the SummaryMonetaryEventNetInterestAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SummaryMonetaryEventNetInterestAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool SummaryMonetaryEventNetInterestAmountSpecified
        {
            get { return SummaryMonetaryEventNetInterestAmount != null; }
            set { }
        }

        /// <summary>
        /// The portion of the Summary Loan Monetary Event Gross Principal Amount due to the Investor.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOAmount SummaryMonetaryEventNetPrincipalAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the SummaryMonetaryEventNetPrincipalAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SummaryMonetaryEventNetPrincipalAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool SummaryMonetaryEventNetPrincipalAmountSpecified
        {
            get { return SummaryMonetaryEventNetPrincipalAmount != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of all payments made by the borrower that are being reported in the Loan Monetary Event Summary applied to optional product features.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOAmount SummaryMonetaryEventOptionalProductsPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the SummaryMonetaryEventOptionalProductsPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SummaryMonetaryEventOptionalProductsPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool SummaryMonetaryEventOptionalProductsPaymentAmountSpecified
        {
            get { return SummaryMonetaryEventOptionalProductsPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// The current unpaid principal balance on the loan, based on an agreed upon schedule.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOAmount SummaryMonetaryEventScheduledUPBAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the SummaryMonetaryEventScheduledUPBAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SummaryMonetaryEventScheduledUPBAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool SummaryMonetaryEventScheduledUPBAmountSpecified
        {
            get { return SummaryMonetaryEventScheduledUPBAmount != null; }
            set { }
        }

        /// <summary>
        /// The unpaid principal balance on the loan after applying this monetary event.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOAmount SummaryMonetaryEventUPBAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the SummaryMonetaryEventUPBAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SummaryMonetaryEventUPBAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool SummaryMonetaryEventUPBAmountSpecified
        {
            get { return SummaryMonetaryEventUPBAmount != null; }
            set { }
        }

        /// <summary>
        /// The total remittance amount being reported to the Investor by the Servicer for this summary monetary event.
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMOAmount SummaryMonetaryInvestorRemittanceAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the SummaryMonetaryInvestorRemittanceAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SummaryMonetaryInvestorRemittanceAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool SummaryMonetaryInvestorRemittanceAmountSpecified
        {
            get { return SummaryMonetaryInvestorRemittanceAmount != null; }
            set { }
        }

        /// <summary>
        /// The date the investor should draft the remittance from the lenders bank account for this summary monetary event. This is typically the day after the remittance is reported to the investor.
        /// </summary>
        [XmlElement(Order = 17)]
        public MISMODate SummaryMonetaryInvestorRemittanceEffectiveDate;

        /// <summary>
        /// Gets or sets a value indicating whether the SummaryMonetaryInvestorRemittanceEffectiveDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SummaryMonetaryInvestorRemittanceEffectiveDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool SummaryMonetaryInvestorRemittanceEffectiveDateSpecified
        {
            get { return SummaryMonetaryInvestorRemittanceEffectiveDate != null; }
            set { }
        }

        /// <summary>
        /// Unapplied funds balance, excluding loans in bankruptcy (overpayments, underpayments, repayment plan funds, government funds i.e., 235, alien funds, etc).
        /// </summary>
        [XmlElement(Order = 18)]
        public MISMOAmount SuspenseBalanceAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the SuspenseBalanceAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SuspenseBalanceAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool SuspenseBalanceAmountSpecified
        {
            get { return SuspenseBalanceAmount != null; }
            set { }
        }

        /// <summary>
        /// Collected/Unremitted Optional Insurance Funds.
        /// </summary>
        [XmlElement(Order = 19)]
        public MISMOAmount UnremittedOptionalProductsPremiumBalanceAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the UnremittedOptionalProductsPremiumBalanceAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the UnremittedOptionalProductsPremiumBalanceAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool UnremittedOptionalProductsPremiumBalanceAmountSpecified
        {
            get { return UnremittedOptionalProductsPremiumBalanceAmount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 20)]
        public MONETARY_EVENT_SUMMARY_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
