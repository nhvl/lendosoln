namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class MONETARY_EVENT_SUMMARIES
    {
        /// <summary>
        /// Gets a value indicating whether the MONETARY_EVENT_SUMMARIES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.MonetaryEventSummarySpecified;
            }
        }

        /// <summary>
        /// A collection of monetary event summaries.
        /// </summary>
        [XmlElement("MONETARY_EVENT_SUMMARY", Order = 0)]
		public List<MONETARY_EVENT_SUMMARY> MonetaryEventSummary = new List<MONETARY_EVENT_SUMMARY>();

        /// <summary>
        /// Gets or sets a value indicating whether the MonetaryEventSummary element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MonetaryEventSummary element has been assigned a value.</value>
        [XmlIgnore]
        public bool MonetaryEventSummarySpecified
        {
            get { return this.MonetaryEventSummary != null && this.MonetaryEventSummary.Count(m => m != null && m.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public MONETARY_EVENT_SUMMARIES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
