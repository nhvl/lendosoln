namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class MONETARY_EVENT_SUMMARY
    {
        /// <summary>
        /// Gets a value indicating whether the MONETARY_EVENT_SUMMARY container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.InvestorReportingAdditionalChargesSpecified
                    || this.MonetaryEventSummaryDetailSpecified
                    || this.StatusChangeEventsSpecified;
            }
        }

        /// <summary>
        /// Additional charges related to this summary.
        /// </summary>
        [XmlElement("INVESTOR_REPORTING_ADDITIONAL_CHARGES", Order = 0)]
        public INVESTOR_REPORTING_ADDITIONAL_CHARGES InvestorReportingAdditionalCharges;

        /// <summary>
        /// Gets or sets a value indicating whether the InvestorReportingAdditionalCharges element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InvestorReportingAdditionalCharges element has been assigned a value.</value>
        [XmlIgnore]
        public bool InvestorReportingAdditionalChargesSpecified
        {
            get { return this.InvestorReportingAdditionalCharges != null && this.InvestorReportingAdditionalCharges.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details about a monetary event summary.
        /// </summary>
        [XmlElement("MONETARY_EVENT_SUMMARY_DETAIL", Order = 1)]
        public MONETARY_EVENT_SUMMARY_DETAIL MonetaryEventSummaryDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the MonetaryEventSummaryDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MonetaryEventSummaryDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool MonetaryEventSummaryDetailSpecified
        {
            get { return this.MonetaryEventSummaryDetail != null && this.MonetaryEventSummaryDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A list of status change events.
        /// </summary>
        [XmlElement("STATUS_CHANGE_EVENTS", Order = 2)]
        public STATUS_CHANGE_EVENTS StatusChangeEvents;

        /// <summary>
        /// Gets or sets a value indicating whether the StatusChangeEvents element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the StatusChangeEvents element has been assigned a value.</value>
        [XmlIgnore]
        public bool StatusChangeEventsSpecified
        {
            get { return this.StatusChangeEvents != null && this.StatusChangeEvents.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public MONETARY_EVENT_SUMMARY_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
