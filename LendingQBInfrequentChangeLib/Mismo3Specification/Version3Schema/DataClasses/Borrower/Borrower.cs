namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class BORROWER
    {
        /// <summary>
        /// Gets a value indicating whether the BORROWER container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BankruptciesSpecified
                    || this.BorrowerDetailSpecified
                    || this.CounselingSpecified
                    || this.CreditScoresSpecified
                    || this.CurrentIncomeSpecified
                    || this.DeclarationSpecified
                    || this.DependentsSpecified
                    || this.EmployersSpecified
                    || this.ExtensionSpecified
                    || this.GovernmentBorrowerSpecified
                    || this.GovernmentMonitoringSpecified
                    || this.HardshipDeclarationSpecified
                    || this.HousingExpensesSpecified
                    || this.LitigationsSpecified
                    || this.MilitaryServicesSpecified
                    || this.NearestLivingRelativeSpecified
                    || this.ResidencesSpecified
                    || this.SolicitationPreferenceSpecified
                    || this.SummariesSpecified;
            }
        }

        /// <summary>
        /// Bankruptcies undergone by the borrower.
        /// </summary>
        [XmlElement("BANKRUPTCIES", Order = 0)]
        public BANKRUPTCIES Bankruptcies;

        /// <summary>
        /// Gets or sets a value indicating whether the Bankruptcies element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Bankruptcies element has been assigned a value.</value>
        [XmlIgnore]
        public bool BankruptciesSpecified
        {
            get { return this.Bankruptcies != null && this.Bankruptcies.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details about the borrower.
        /// </summary>
        [XmlElement("BORROWER_DETAIL", Order = 1)]
        public BORROWER_DETAIL BorrowerDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the BorrowerDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BorrowerDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool BorrowerDetailSpecified
        {
            get { return this.BorrowerDetail != null && this.BorrowerDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Counseling undergone by the borrower.
        /// </summary>
        [XmlElement("COUNSELING", Order = 2)]
        public COUNSELING Counseling;

        /// <summary>
        /// Gets or sets a value indicating whether the Counseling element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Counseling element has been assigned a value.</value>
        [XmlIgnore]
        public bool CounselingSpecified
        {
            get { return this.Counseling != null && this.Counseling.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The borrower's credit scores.
        /// </summary>
        [XmlElement("CREDIT_SCORES", Order = 3)]
        public CREDIT_SCORES CreditScores;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditScores element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditScores element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditScoresSpecified
        {
            get { return this.CreditScores != null && this.CreditScores.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The borrower's current income.
        /// </summary>
        [XmlElement("CURRENT_INCOME", Order = 4)]
        public CURRENT_INCOME CurrentIncome;

        /// <summary>
        /// Gets or sets a value indicating whether the CurrentIncome element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CurrentIncome element has been assigned a value.</value>
        [XmlIgnore]
        public bool CurrentIncomeSpecified
        {
            get { return this.CurrentIncome != null && this.CurrentIncome.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The borrower's declaration.
        /// </summary>
        [XmlElement("DECLARATION", Order = 5)]
        public DECLARATION Declaration;

        /// <summary>
        /// Gets or sets a value indicating whether the Declaration element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Declaration element has been assigned a value.</value>
        [XmlIgnore]
        public bool DeclarationSpecified
        {
            get { return this.Declaration != null && this.Declaration.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Dependents of the borrower.
        /// </summary>
        [XmlElement("DEPENDENTS", Order = 6)]
        public DEPENDENTS Dependents;

        /// <summary>
        /// Gets or sets a value indicating whether the Dependents element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Dependents element has been assigned a value.</value>
        [XmlIgnore]
        public bool DependentsSpecified
        {
            get { return this.Dependents != null && this.Dependents.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Employers of the borrower.
        /// </summary>
        [XmlElement("EMPLOYERS", Order = 7)]
        public EMPLOYERS Employers;

        /// <summary>
        /// Gets or sets a value indicating whether the Employers element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Employers element has been assigned a value.</value>
        [XmlIgnore]
        public bool EmployersSpecified
        {
            get { return this.Employers != null && this.Employers.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Government borrower information.
        /// </summary>
        [XmlElement("GOVERNMENT_BORROWER", Order = 8)]
        public GOVERNMENT_BORROWER GovernmentBorrower;

        /// <summary>
        /// Gets or sets a value indicating whether the GovernmentBorrower element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GovernmentBorrower element has been assigned a value.</value>
        [XmlIgnore]
        public bool GovernmentBorrowerSpecified
        {
            get { return this.GovernmentBorrower != null && this.GovernmentBorrower.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Government monitoring related to the borrower.
        /// </summary>
        [XmlElement("GOVERNMENT_MONITORING", Order = 9)]
        public GOVERNMENT_MONITORING GovernmentMonitoring;

        /// <summary>
        /// Gets or sets a value indicating whether the GovernmentMonitoring element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GovernmentMonitoring element has been assigned a value.</value>
        [XmlIgnore]
        public bool GovernmentMonitoringSpecified
        {
            get { return this.GovernmentMonitoring != null && this.GovernmentMonitoring.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Declarations of hardship suffered by the borrower.
        /// </summary>
        [XmlElement("HARDSHIP_DECLARATION", Order = 10)]
        public HARDSHIP_DECLARATION HardshipDeclaration;

        /// <summary>
        /// Gets or sets a value indicating whether the HardshipDeclaration element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HardshipDeclaration element has been assigned a value.</value>
        [XmlIgnore]
        public bool HardshipDeclarationSpecified
        {
            get { return this.HardshipDeclaration != null && this.HardshipDeclaration.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Housing expenses of the borrower.
        /// </summary>
        [XmlElement("HOUSING_EXPENSES", Order = 11)]
        public HOUSING_EXPENSES HousingExpenses;

        /// <summary>
        /// Gets or sets a value indicating whether the HousingExpenses element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HousingExpenses element has been assigned a value.</value>
        [XmlIgnore]
        public bool HousingExpensesSpecified
        {
            get { return this.HousingExpenses != null && this.HousingExpenses.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Litigations the borrower has been involved in.
        /// </summary>
        [XmlElement("LITIGATIONS", Order = 12)]
        public LITIGATIONS Litigations;

        /// <summary>
        /// Gets or sets a value indicating whether the Litigations element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Litigations element has been assigned a value.</value>
        [XmlIgnore]
        public bool LitigationsSpecified
        {
            get { return this.Litigations != null && this.Litigations.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Information about the borrower's military background.
        /// </summary>
        [XmlElement("MILITARY_SERVICES", Order = 13)]
        public MILITARY_SERVICES MilitaryServices;

        /// <summary>
        /// Gets or sets a value indicating whether the MilitaryServices element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MilitaryServices element has been assigned a value.</value>
        [XmlIgnore]
        public bool MilitaryServicesSpecified
        {
            get { return this.MilitaryServices != null && this.MilitaryServices.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Nearest living relative of the borrower.
        /// </summary>
        [XmlElement("NEAREST_LIVING_RELATIVE", Order = 14)]
        public NEAREST_LIVING_RELATIVE NearestLivingRelative;

        /// <summary>
        /// Gets or sets a value indicating whether the NearestLivingRelative element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NearestLivingRelative element has been assigned a value.</value>
        [XmlIgnore]
        public bool NearestLivingRelativeSpecified
        {
            get { return this.NearestLivingRelative != null && this.NearestLivingRelative.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Residences owned by the borrower.
        /// </summary>
        [XmlElement("RESIDENCES", Order = 15)]
        public RESIDENCES Residences;

        /// <summary>
        /// Gets or sets a value indicating whether the Residences element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Residences element has been assigned a value.</value>
        [XmlIgnore]
        public bool ResidencesSpecified
        {
            get { return this.Residences != null && this.Residences.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Solicitation preferences of the borrower.
        /// </summary>
        [XmlElement("SOLICITATION_PREFERENCE", Order = 16)]
        public SOLICITATION_PREFERENCE SolicitationPreference;

        /// <summary>
        /// Gets or sets a value indicating whether the SolicitationPreference element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SolicitationPreference element has been assigned a value.</value>
        [XmlIgnore]
        public bool SolicitationPreferenceSpecified
        {
            get { return this.SolicitationPreference != null && this.SolicitationPreference.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Summaries of the borrowers.
        /// </summary>
        [XmlElement("SUMMARIES", Order = 17)]
        public SUMMARIES Summaries;

        /// <summary>
        /// Gets or sets a value indicating whether the Summaries element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Summaries element has been assigned a value.</value>
        [XmlIgnore]
        public bool SummariesSpecified
        {
            get { return this.Summaries != null && this.Summaries.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 18)]
        public BORROWER_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
