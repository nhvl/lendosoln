namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class BORROWER_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the BORROWER_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AutomobilesOwnedCountSpecified
                    || this.BorrowerAgeAtApplicationYearsCountSpecified
                    || this.BorrowerApplicationSignedDateSpecified
                    || this.BorrowerBankruptcyIndicatorSpecified
                    || this.BorrowerBirthDateSpecified
                    || this.BorrowerCharacteristicTypeOtherDescriptionSpecified
                    || this.BorrowerCharacteristicTypeSpecified
                    || this.BorrowerClassificationTypeSpecified
                    || this.BorrowerMailToAddressSameAsPropertyIndicatorSpecified
                    || this.BorrowerQualifyingIncomeAmountSpecified
                    || this.BorrowerRelationshipTitleTypeOtherDescriptionSpecified
                    || this.BorrowerRelationshipTitleTypeSpecified
                    || this.BorrowerSameAsBuilderIndicatorSpecified
                    || this.BorrowerTotalMortgagedPropertiesCountSpecified
                    || this.CreditFilerBorrowerAgeYearsCountSpecified
                    || this.CreditReportAuthorizationIndicatorSpecified
                    || this.CreditReportIdentifierSpecified
                    || this.DependentCountSpecified
                    || this.EmploymentStateTypeOtherDescriptionSpecified
                    || this.EmploymentStateTypeSpecified
                    || this.ExtensionSpecified
                    || this.JointAssetLiabilityReportingTypeSpecified
                    || this.MaritalStatusTypeSpecified
                    || this.SchoolingYearsCountSpecified
                    || this.SignedAuthorizationToRequestTaxRecordsIndicatorSpecified
                    || this.TaxRecordsObtainedIndicatorSpecified;

            }
        }

        /// <summary>
        /// The total number of automobiles owned by a borrower.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOCount AutomobilesOwnedCount;

        /// <summary>
        /// Gets or sets a value indicating whether the AutomobilesOwnedCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AutomobilesOwnedCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool AutomobilesOwnedCountSpecified
        {
            get { return AutomobilesOwnedCount != null; }
            set { }
        }

        /// <summary>
        /// The age of the borrower at the time of application in years.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOCount BorrowerAgeAtApplicationYearsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the BorrowerAgeAtApplicationYearsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BorrowerAgeAtApplicationYearsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool BorrowerAgeAtApplicationYearsCountSpecified
        {
            get { return BorrowerAgeAtApplicationYearsCount != null; }
            set { }
        }

        /// <summary>
        /// The date that the borrower signed the loan application. Collected on the URLA in Section IX (Borrowers Signature Date).
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMODate BorrowerApplicationSignedDate;

        /// <summary>
        /// Gets or sets a value indicating whether the BorrowerApplicationSignedDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BorrowerApplicationSignedDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool BorrowerApplicationSignedDateSpecified
        {
            get { return BorrowerApplicationSignedDate != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that the borrower has filed a bankruptcy petition.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOIndicator BorrowerBankruptcyIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the BorrowerBankruptcyIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BorrowerBankruptcyIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool BorrowerBankruptcyIndicatorSpecified
        {
            get { return BorrowerBankruptcyIndicator != null; }
            set { }
        }

        /// <summary>
        /// Borrowers date of birth. 
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMODate BorrowerBirthDate;

        /// <summary>
        /// Gets or sets a value indicating whether the BorrowerBirthDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BorrowerBirthDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool BorrowerBirthDateSpecified
        {
            get { return BorrowerBirthDate != null; }
            set { }
        }

        /// <summary>
        /// Specifies the classification applied to a borrower for the purposes of processing or reporting.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOEnum<BorrowerCharacteristicBase> BorrowerCharacteristicType;

        /// <summary>
        /// Gets or sets a value indicating whether the BorrowerCharacteristicType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BorrowerCharacteristicType element has been assigned a value.</value>
        [XmlIgnore]
        public bool BorrowerCharacteristicTypeSpecified
        {
            get { return this.BorrowerCharacteristicType != null && this.BorrowerCharacteristicType.enumValue != BorrowerCharacteristicBase.Blank; }
            set { }
        }

        /// <summary>
        /// The description of the Borrower Characteristic Type when Other is selected as the option from the enumerated list.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOString BorrowerCharacteristicTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the BorrowerCharacteristicTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BorrowerCharacteristicTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool BorrowerCharacteristicTypeOtherDescriptionSpecified
        {
            get { return BorrowerCharacteristicTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether the borrower is the primary or a secondary borrower.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOEnum<BorrowerClassificationBase> BorrowerClassificationType;

        /// <summary>
        /// Gets or sets a value indicating whether the BorrowerClassificationType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BorrowerClassificationType element has been assigned a value.</value>
        [XmlIgnore]
        public bool BorrowerClassificationTypeSpecified
        {
            get { return this.BorrowerClassificationType != null && this.BorrowerClassificationType.enumValue != BorrowerClassificationBase.Blank; }
            set { }
        }

        /// <summary>
        /// When true, indicates that the mailing address for the borrower is the same as the property address.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOIndicator BorrowerMailToAddressSameAsPropertyIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the BorrowerMailToAddressSameAsPropertyIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BorrowerMailToAddressSameAsPropertyIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool BorrowerMailToAddressSameAsPropertyIndicatorSpecified
        {
            get { return BorrowerMailToAddressSameAsPropertyIndicator != null; }
            set { }
        }

        /// <summary>
        /// The total monthly borrower income per lender or investor guidelines.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOAmount BorrowerQualifyingIncomeAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the BorrowerQualifyingIncomeAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BorrowerQualifyingIncomeAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool BorrowerQualifyingIncomeAmountSpecified
        {
            get { return BorrowerQualifyingIncomeAmount != null; }
            set { }
        }

        /// <summary>
        /// The description of the relationship of a borrower to (an) other borrower(s), if any, for Title purposes.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOEnum<BorrowerRelationshipTitleBase> BorrowerRelationshipTitleType;

        /// <summary>
        /// Gets or sets a value indicating whether the BorrowerRelationshipTitleType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BorrowerRelationshipTitleType element has been assigned a value.</value>
        [XmlIgnore]
        public bool BorrowerRelationshipTitleTypeSpecified
        {
            get { return this.BorrowerRelationshipTitleType != null && this.BorrowerRelationshipTitleType.enumValue != BorrowerRelationshipTitleBase.Blank; }
            set { }
        }

        /// <summary>
        /// When Other is specified in Borrower Relationship Title Type, this element contains the description.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOString BorrowerRelationshipTitleTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the BorrowerRelationshipTitleTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BorrowerRelationshipTitleTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool BorrowerRelationshipTitleTypeOtherDescriptionSpecified
        {
            get { return BorrowerRelationshipTitleTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that the Borrower is also the Builder on the loan transaction.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOIndicator BorrowerSameAsBuilderIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the BorrowerSameAsBuilderIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BorrowerSameAsBuilderIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool BorrowerSameAsBuilderIndicatorSpecified
        {
            get { return BorrowerSameAsBuilderIndicator != null; }
            set { }
        }

        /// <summary>
        /// The number of 1-4 unit properties that are financed and owned and/or obligated on by an individual borrower. 
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOCount BorrowerTotalMortgagedPropertiesCount;

        /// <summary>
        /// Gets or sets a value indicating whether the BorrowerTotalMortgagedPropertiesCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BorrowerTotalMortgagedPropertiesCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool BorrowerTotalMortgagedPropertiesCountSpecified
        {
            get { return BorrowerTotalMortgagedPropertiesCount != null; }
            set { }
        }

        /// <summary>
        /// The borrower's age in years.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOCount CreditFilerBorrowerAgeYearsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditFilerBorrowerAgeYearsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditFilerBorrowerAgeYearsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditFilerBorrowerAgeYearsCountSpecified
        {
            get { return CreditFilerBorrowerAgeYearsCount != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the borrower gave the lender consent to pull credit report.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOIndicator CreditReportAuthorizationIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditReportAuthorizationIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditReportAuthorizationIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditReportAuthorizationIndicatorSpecified
        {
            get { return CreditReportAuthorizationIndicator != null; }
            set { }
        }

        /// <summary>
        /// A reference number assigned by the credit bureau to a specific credit report. This report number is also referenced when a Reissue, Upgrade, or Status Query of an existing report is requested.
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMOIdentifier CreditReportIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditReportIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditReportIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditReportIdentifierSpecified
        {
            get { return CreditReportIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The number of dependents disclosed by the borrower. Collected on the URLA in Section III (Dependents No.).
        /// </summary>
        [XmlElement(Order = 17)]
        public MISMOCount DependentCount;

        /// <summary>
        /// Gets or sets a value indicating whether the DependentCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DependentCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool DependentCountSpecified
        {
            get { return DependentCount != null; }
            set { }
        }

        /// <summary>
        /// The current employment status of the borrower.
        /// </summary>
        [XmlElement(Order = 18)]
        public MISMOEnum<EmploymentStateBase> EmploymentStateType;

        /// <summary>
        /// Gets or sets a value indicating whether the EmploymentStateType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EmploymentStateType element has been assigned a value.</value>
        [XmlIgnore]
        public bool EmploymentStateTypeSpecified
        {
            get { return this.EmploymentStateType != null && this.EmploymentStateType.enumValue != EmploymentStateBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Employment State Type.
        /// </summary>
        [XmlElement(Order = 19)]
        public MISMOString EmploymentStateTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the EmploymentStateTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EmploymentStateTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool EmploymentStateTypeOtherDescriptionSpecified
        {
            get { return EmploymentStateTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A statement made by two borrowers on a single loan application indicating whether their reporting of assets and liabilities is being completed jointly or not jointly. Collected on the URLA in Section VI (Completed Jointly / Not Jointly - Check Box).
        /// </summary>
        [XmlElement(Order = 20)]
        public MISMOEnum<JointAssetLiabilityReportingBase> JointAssetLiabilityReportingType;

        /// <summary>
        /// Gets or sets a value indicating whether the JointAssetLiabilityReportingType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the JointAssetLiabilityReportingType element has been assigned a value.</value>
        [XmlIgnore]
        public bool JointAssetLiabilityReportingTypeSpecified
        {
            get { return this.JointAssetLiabilityReportingType != null && this.JointAssetLiabilityReportingType.enumValue != JointAssetLiabilityReportingBase.Blank; }
            set { }
        }

        /// <summary>
        /// The marital status of the party as disclosed by the party.
        /// </summary>
        [XmlElement(Order = 21)]
        public MISMOEnum<MaritalStatusBase> MaritalStatusType;

        /// <summary>
        /// Gets or sets a value indicating whether the MaritalStatusType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MaritalStatusType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MaritalStatusTypeSpecified
        {
            get { return this.MaritalStatusType != null && this.MaritalStatusType.enumValue != MaritalStatusBase.Blank; }
            set { }
        }

        /// <summary>
        /// The number of years the borrower has attended school. Collected on the URLA in Section III.
        /// </summary>
        [XmlElement(Order = 22)]
        public MISMOCount SchoolingYearsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the SchoolingYearsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SchoolingYearsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool SchoolingYearsCountSpecified
        {
            get { return SchoolingYearsCount != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the IRS form (4506 or 4506T) that gives the lender the authority to request copies of tax records is present.
        /// </summary>
        [XmlElement(Order = 23)]
        public MISMOIndicator SignedAuthorizationToRequestTaxRecordsIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the SignedAuthorizationToRequestTaxRecordsIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SignedAuthorizationToRequestTaxRecordsIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool SignedAuthorizationToRequestTaxRecordsIndicatorSpecified
        {
            get { return SignedAuthorizationToRequestTaxRecordsIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the IRS form (4506 or 4506T) that gives the lender the authority to request copies of tax records has been executed by the lender.
        /// </summary>
        [XmlElement(Order = 24)]
        public MISMOIndicator TaxRecordsObtainedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the TaxRecordsObtainedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TaxRecordsObtainedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool TaxRecordsObtainedIndicatorSpecified
        {
            get { return TaxRecordsObtainedIndicator != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 25)]
        public BORROWER_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension?.ShouldSerialize ?? false; }
            set { }
        }
    }
}
