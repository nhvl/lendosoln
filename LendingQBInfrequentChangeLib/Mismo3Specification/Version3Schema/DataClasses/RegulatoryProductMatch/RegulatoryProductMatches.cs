namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class REGULATORY_PRODUCT_MATCHES
    {
        /// <summary>
        /// Gets a value indicating whether the REGULATORY_PRODUCT_MATCHES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.RegulatoryProductMatchSpecified;
            }
        }

        /// <summary>
        /// A collection of regulatory product matches.
        /// </summary>
        [XmlElement("REGULATORY_PRODUCT_MATCH", Order = 0)]
		public List<REGULATORY_PRODUCT_MATCH> RegulatoryProductMatch = new List<REGULATORY_PRODUCT_MATCH>();

        /// <summary>
        /// Gets or sets a value indicating whether the RegulatoryProductMatch element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RegulatoryProductMatch element has been assigned a value.</value>
        [XmlIgnore]
        public bool RegulatoryProductMatchSpecified
        {
            get { return this.RegulatoryProductMatch != null && this.RegulatoryProductMatch.Count(r => r != null && r.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public REGULATORY_PRODUCT_MATCHES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
