namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class HEATING_SYSTEMS
    {
        /// <summary>
        /// Gets a value indicating whether the HEATING_SYSTEMS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.HeatingSystemSpecified;
            }
        }

        /// <summary>
        /// A collection of heating systems.
        /// </summary>
        [XmlElement("HEATING_SYSTEM", Order = 0)]
		public List<HEATING_SYSTEM> HeatingSystem = new List<HEATING_SYSTEM>();

        /// <summary>
        /// Gets or sets a value indicating whether the HeatingSystem element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HeatingSystem element has been assigned a value.</value>
        [XmlIgnore]
        public bool HeatingSystemSpecified
        {
            get { return this.HeatingSystem != null && this.HeatingSystem.Count(h => h != null && h.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public HEATING_SYSTEMS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
