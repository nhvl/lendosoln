namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class ROLE_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the ROLE_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.PartyRoleTypeOtherDescriptionSpecified
                    || this.PartyRoleTypeSpecified;
            }
        }

        /// <summary>
        /// Identifies the role that the party plays in the transaction. Parties may be either a person or legal entity. A party may play multiple roles in a transaction.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<PartyRoleBase> PartyRoleType;

        /// <summary>
        /// Gets or sets a value indicating whether the PartyRoleType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PartyRoleType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PartyRoleTypeSpecified
        {
            get { return this.PartyRoleType != null && this.PartyRoleType.enumValue != PartyRoleBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Party Role Type.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString PartyRoleTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PartyRoleTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PartyRoleTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PartyRoleTypeOtherDescriptionSpecified
        {
            get { return PartyRoleTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public ROLE_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null ? Extension.ShouldSerialize : false; }
            set { }
        }
    }
}
