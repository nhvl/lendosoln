namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class ROLE
    {
        /// <summary>
        /// Gets a value indicating whether the ROLE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                if (Mismo3Utilities.ExceedsThreshold(1,
                    new List<bool> { 
                        this.AppraiserSpecified,
                        this.AppraiserSupervisorSpecified,
                        this.AttorneyInFactSpecified,
                        this.AttorneySpecified,
                        this.BorrowerSpecified,
                        this.ClosingAgentSpecified,
                        this.FulfillmentPartySpecified,
                        this.LenderSpecified,
                        this.LienHolderSpecified,
                        this.LoanOriginatorSpecified,
                        this.LossPayeeSpecified,
                        this.NotarySpecified,
                        this.PayeeSpecified,
                        this.PropertyOwnerSpecified,
                        this.PropertySellerSpecified,
                        this.RealEstateAgentSpecified,
                        this.RegulatoryAgencySpecified,
                        this.RequestingPartySpecified,
                        this.RespondingPartySpecified,
                        this.ReturnToSpecified,
                        this.ReviewAppraiserSpecified,
                        this.ServicerSpecified,
                        this.ServicingTransferorSpecified,
                        this.SubmittingPartySpecified,
                        this.TrusteeSpecified,
                        this.TrustSpecified }))
                {
                    throw new System.Xml.Schema.XmlSchemaValidationException(
                        Mismo3Utilities.GetXSDChoiceExceptionMessage(
                        "ROLE",
                        new List<string> { 
                            "APPRAISER", 
                            "APPRAISER_SUPERVISOR",
                            "ATTORNEY",
                            "ATTORNEY_IN_FACT",
                            "BORROWER",
                            "CLOSING_AGENT",
                            "FULFILLMENT_PARTY",
                            "LENDER",
                            "LIEN_HOLDER",
                            "LOAN_ORIGINATOR",
                            "LOSS_PAYEE",
                            "NOTARY",
                            "PAYEE",
                            "PROPERTY_OWNER",
                            "PROPERTY_SELLER",
                            "REAL_ESTATE_AGENT",
                            "REGULATORY_AGENCY",
                            "REQUESTING_PARTY",
                            "RESPONDING_PARTY",
                            "RETURN_TO",
                            "REVIEW_APPRAISER",
                            "SERVICER",
                            "SERVICING_TRANSFEROR",
                            "SUBMITTING_PARTY",
                            "TRUST",
                            "TRUSTEE"}));
                }

                return this.AppraiserSpecified
                    || this.AppraiserSupervisorSpecified
                    || this.AttorneyInFactSpecified
                    || this.AttorneySpecified
                    || this.BorrowerSpecified
                    || this.ClosingAgentSpecified
                    || this.ExtensionSpecified
                    || this.FulfillmentPartySpecified
                    || this.LenderSpecified
                    || this.LicensesSpecified
                    || this.LienHolderSpecified
                    || this.LoanOriginatorSpecified
                    || this.LossPayeeSpecified
                    || this.NotarySpecified
                    || this.PartyRoleIdentifiersSpecified
                    || this.PayeeSpecified
                    || this.PropertyOwnerSpecified
                    || this.PropertySellerSpecified
                    || this.RealEstateAgentSpecified
                    || this.RegulatoryAgencySpecified
                    || this.RequestingPartySpecified
                    || this.RespondingPartySpecified
                    || this.ReturnToSpecified
                    || this.ReviewAppraiserSpecified
                    || this.RoleDetailSpecified
                    || this.ServicerSpecified
                    || this.ServicingTransferorSpecified
                    || this.SubmittingPartySpecified
                    || this.TrusteeSpecified
                    || this.TrustSpecified;
            }
        }

        /// <summary>
        /// The appraiser for this transaction.
        /// </summary>
        [XmlElement("APPRAISER", Order = 0)]
        public APPRAISER Appraiser;

        /// <summary>
        /// Gets or sets a value indicating whether the Appraiser element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Appraiser element has been assigned a value.</value>
        [XmlIgnore]
        public bool AppraiserSpecified
        {
            get { return this.Appraiser != null && this.Appraiser.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The appraiser supervisor for this transaction.
        /// </summary>
        [XmlElement("APPRAISER_SUPERVISOR", Order = 1)]
        public APPRAISER_SUPERVISOR AppraiserSupervisor;

        /// <summary>
        /// Gets or sets a value indicating whether the AppraiserSupervisor element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AppraiserSupervisor element has been assigned a value.</value>
        [XmlIgnore]
        public bool AppraiserSupervisorSpecified
        {
            get { return this.AppraiserSupervisor != null && this.AppraiserSupervisor.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The attorney for this transaction.
        /// </summary>
        [XmlElement("ATTORNEY", Order = 2)]
        public ATTORNEY Attorney;

        /// <summary>
        /// Gets or sets a value indicating whether the Attorney element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Attorney element has been assigned a value.</value>
        [XmlIgnore]
        public bool AttorneySpecified
        {
            get { return this.Attorney != null && this.Attorney.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The attorney-in-fact for this transaction.
        /// </summary>
        [XmlElement("ATTORNEY_IN_FACT", Order = 3)]
        public ATTORNEY_IN_FACT AttorneyInFact;

        /// <summary>
        /// Gets or sets a value indicating whether the AttorneyInFact element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AttorneyInFact element has been assigned a value.</value>
        [XmlIgnore]
        public bool AttorneyInFactSpecified
        {
            get { return this.AttorneyInFact != null && this.AttorneyInFact.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The borrower for this transaction.
        /// </summary>
        [XmlElement("BORROWER", Order = 4)]
        public BORROWER Borrower;

        /// <summary>
        /// Gets or sets a value indicating whether the Borrower element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Borrower element has been assigned a value.</value>
        [XmlIgnore]
        public bool BorrowerSpecified
        {
            get { return this.Borrower != null && this.Borrower.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The closing agent for this transaction.
        /// </summary>
        [XmlElement("CLOSING_AGENT", Order = 5)]
        public CLOSING_AGENT ClosingAgent;

        /// <summary>
        /// Gets or sets a value indicating whether the ClosingAgent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ClosingAgent element has been assigned a value.</value>
        [XmlIgnore]
        public bool ClosingAgentSpecified
        {
            get { return this.ClosingAgent != null && this.ClosingAgent.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The fulfillment party for this transaction.
        /// </summary>
        [XmlElement("FULFILLMENT_PARTY", Order = 6)]
        public FULFILLMENT_PARTY FulfillmentParty;

        /// <summary>
        /// Gets or sets a value indicating whether the FulfillmentParty element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FulfillmentParty element has been assigned a value.</value>
        [XmlIgnore]
        public bool FulfillmentPartySpecified
        {
            get { return this.FulfillmentParty != null && this.FulfillmentParty.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The lender for this transaction.
        /// </summary>
        [XmlElement("LENDER", Order = 7)]
        public LENDER Lender;

        /// <summary>
        /// Gets or sets a value indicating whether the Lender element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Lender element has been assigned a value.</value>
        [XmlIgnore]
        public bool LenderSpecified
        {
            get { return this.Lender != null && this.Lender.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The lien holder for this transaction.
        /// </summary>
        [XmlElement("LIEN_HOLDER", Order = 8)]
        public LIEN_HOLDER LienHolder;

        /// <summary>
        /// Gets or sets a value indicating whether the LienHolder element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LienHolder element has been assigned a value.</value>
        [XmlIgnore]
        public bool LienHolderSpecified
        {
            get { return this.LienHolder != null && this.LienHolder.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The loan originator for this transaction.
        /// </summary>
        [XmlElement("LOAN_ORIGINATOR", Order = 9)]
        public LOAN_ORIGINATOR LoanOriginator;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanOriginator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanOriginator element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanOriginatorSpecified
        {
            get { return this.LoanOriginator != null && this.LoanOriginator.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The loss payee in the transaction.
        /// </summary>
        [XmlElement("LOSS_PAYEE", Order = 10)]
        public LOSS_PAYEE LossPayee;

        /// <summary>
        /// Gets or sets a value indicating whether the LossPayee element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LossPayee element has been assigned a value.</value>
        [XmlIgnore]
        public bool LossPayeeSpecified
        {
            get { return this.LossPayee != null && this.LossPayee.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The notary in the transaction.
        /// </summary>
        [XmlElement("NOTARY", Order = 11)]
        public NOTARY Notary;

        /// <summary>
        /// Gets or sets a value indicating whether the Notary element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Notary element has been assigned a value.</value>
        [XmlIgnore]
        public bool NotarySpecified
        {
            get { return this.Notary != null && this.Notary.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The payee in the transaction.
        /// </summary>
        [XmlElement("PAYEE", Order = 12)]
        public PAYEE Payee;

        /// <summary>
        /// Gets or sets a value indicating whether the Payee element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Payee element has been assigned a value.</value>
        [XmlIgnore]
        public bool PayeeSpecified
        {
            get { return this.Payee != null && this.Payee.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The property owner in the transaction.
        /// </summary>
        [XmlElement("PROPERTY_OWNER", Order = 13)]
        public PROPERTY_OWNER PropertyOwner;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyOwner element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyOwner element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyOwnerSpecified
        {
            get { return this.PropertyOwner != null && this.PropertyOwner.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The property seller in the transaction.
        /// </summary>
        [XmlElement("PROPERTY_SELLER", Order = 14)]
        public PROPERTY_SELLER PropertySeller;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertySeller element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertySeller element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertySellerSpecified
        {
            get { return this.PropertySeller != null && this.PropertySeller.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The real estate agent in the transaction.
        /// </summary>
        [XmlElement("REAL_ESTATE_AGENT", Order = 15)]
        public REAL_ESTATE_AGENT RealEstateAgent;

        /// <summary>
        /// Gets or sets a value indicating whether the RealEstateAgent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RealEstateAgent element has been assigned a value.</value>
        [XmlIgnore]
        public bool RealEstateAgentSpecified
        {
            get { return this.RealEstateAgent != null && this.RealEstateAgent.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The regulatory agency involved in the transaction.
        /// </summary>
        [XmlElement("REGULATORY_AGENCY", Order = 16)]
        public REGULATORY_AGENCY RegulatoryAgency;

        /// <summary>
        /// Gets or sets a value indicating whether the RegulatoryAgency element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RegulatoryAgency element has been assigned a value.</value>
        [XmlIgnore]
        public bool RegulatoryAgencySpecified
        {
            get { return this.RegulatoryAgency != null && this.RegulatoryAgency.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The requesting party in the transaction.
        /// </summary>
        [XmlElement("REQUESTING_PARTY", Order = 17)]
        public REQUESTING_PARTY RequestingParty;

        /// <summary>
        /// Gets or sets a value indicating whether the RequestingParty element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RequestingParty element has been assigned a value.</value>
        [XmlIgnore]
        public bool RequestingPartySpecified
        {
            get { return this.RequestingParty != null && this.RequestingParty.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The responding party in the transaction.
        /// </summary>
        [XmlElement("RESPONDING_PARTY", Order = 18)]
        public RESPONDING_PARTY RespondingParty;

        /// <summary>
        /// Gets or sets a value indicating whether the RespondingParty element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RespondingParty element has been assigned a value.</value>
        [XmlIgnore]
        public bool RespondingPartySpecified
        {
            get { return this.RespondingParty != null && this.RespondingParty.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The "return to" in the transaction.
        /// </summary>
        [XmlElement("RETURN_TO", Order = 19)]
        public RETURN_TO ReturnTo;

        /// <summary>
        /// Gets or sets a value indicating whether the ReturnTo element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ReturnTo element has been assigned a value.</value>
        [XmlIgnore]
        public bool ReturnToSpecified
        {
            get { return this.ReturnTo != null && this.ReturnTo.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The review appraiser in the transaction.
        /// </summary>
        [XmlElement("REVIEW_APPRAISER", Order = 20)]
        public REVIEW_APPRAISER ReviewAppraiser;

        /// <summary>
        /// Gets or sets a value indicating whether the ReviewAppraiser element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ReviewAppraiser element has been assigned a value.</value>
        [XmlIgnore]
        public bool ReviewAppraiserSpecified
        {
            get { return this.ReviewAppraiser != null && this.ReviewAppraiser.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Servicer for this role.
        /// </summary>
        [XmlElement("SERVICER", Order = 21)]
        public SERVICER Servicer;

        /// <summary>
        /// Gets or sets a value indicating whether the Servicer element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Servicer element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServicerSpecified
        {
            get { return this.Servicer != null && this.Servicer.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Servicing transferor for this role.
        /// </summary>
        [XmlElement("SERVICING_TRANSFEROR", Order = 22)]
        public SERVICING_TRANSFEROR ServicingTransferor;

        /// <summary>
        /// Gets or sets a value indicating whether the ServicingTransferor element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServicingTransferor element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServicingTransferorSpecified
        {
            get { return this.ServicingTransferor != null && this.ServicingTransferor.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Submitting party related to this role.
        /// </summary>
        [XmlElement("SUBMITTING_PARTY", Order = 23)]
        public SUBMITTING_PARTY SubmittingParty;

        /// <summary>
        /// Gets or sets a value indicating whether the SubmittingParty element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SubmittingParty element has been assigned a value.</value>
        [XmlIgnore]
        public bool SubmittingPartySpecified
        {
            get { return this.SubmittingParty != null && this.SubmittingParty.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Trust related to this role.
        /// </summary>
        [XmlElement("TRUST", Order = 24)]
        public TRUST Trust;

        /// <summary>
        /// Gets or sets a value indicating whether the Trust element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Trust element has been assigned a value.</value>
        [XmlIgnore]
        public bool TrustSpecified
        {
            get { return this.Trust != null && this.Trust.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Trustee related to this role.
        /// </summary>
        [XmlElement("TRUSTEE", Order = 25)]
        public TRUSTEE Trustee;

        /// <summary>
        /// Gets or sets a value indicating whether the Trustee element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Trustee element has been assigned a value.</value>
        [XmlIgnore]
        public bool TrusteeSpecified
        {
            get { return this.Trustee != null && this.Trustee.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Licenses for this role.
        /// </summary>
        [XmlElement("LICENSES", Order = 26)]
        public LICENSES Licenses;

        /// <summary>
        /// Gets or sets a value indicating whether the Licenses element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Licenses element has been assigned a value.</value>
        [XmlIgnore]
        public bool LicensesSpecified
        {
            get { return this.Licenses != null && this.Licenses.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Identifiers for the party role.
        /// </summary>
        [XmlElement("PARTY_ROLE_IDENTIFIERS", Order = 27)]
        public PARTY_ROLE_IDENTIFIERS PartyRoleIdentifiers;

        /// <summary>
        /// Gets or sets a value indicating whether the PartyRoleIdentifiers element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PartyRoleIdentifiers element has been assigned a value.</value>
        [XmlIgnore]
        public bool PartyRoleIdentifiersSpecified
        {
            get { return this.PartyRoleIdentifiers != null && this.PartyRoleIdentifiers.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Information about the role.
        /// </summary>
        [XmlElement("ROLE_DETAIL", Order = 28)]
        public ROLE_DETAIL RoleDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the RoleDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RoleDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool RoleDetailSpecified
        {
            get { return this.RoleDetail != null && this.RoleDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 29)]
        public ROLE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// Gets or sets the $$xlink$$ label.
        /// </summary>
        [XmlAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified, Namespace = Mismo3Constants.XlinkNamespace)]
        public string label;

        /// <summary>
        /// Indicates whether the label can be serialized.
        /// </summary>
        /// <returns>A boolean indicating whether the label can be serialized.</returns>
        public bool ShouldSerializelabel()
        {
            return !string.IsNullOrEmpty(label);
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
