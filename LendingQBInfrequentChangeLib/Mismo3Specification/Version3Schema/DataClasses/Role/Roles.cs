namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class ROLES
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ROLES" /> class.
        /// </summary>
        public ROLES()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ROLES" /> class. Adds roles from the parameters to the list of roles.
        /// </summary>
        /// <param name="roles">The roles to add to the newly initialized list of roles.</param>
        public ROLES(params ROLE[] roles)
        {
            foreach (ROLE role in roles)
            {
                this.Role.Add(role);
            }
        }

        /// <summary>
        /// Gets a value indicating whether the ROLES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.RoleSpecified;
            }
        }

        /// <summary>
        /// A collection of roles in the transaction.
        /// </summary>
        [XmlElement("ROLE", Order = 0)]
		public List<ROLE> Role = new List<ROLE>();

        /// <summary>
        /// Gets or sets a value indicating whether the Role element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Role element has been assigned a value.</value>
        [XmlIgnore]
        public bool RoleSpecified
        {
            get { return this.Role != null && this.Role.Count(r => r != null && r.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public ROLES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
