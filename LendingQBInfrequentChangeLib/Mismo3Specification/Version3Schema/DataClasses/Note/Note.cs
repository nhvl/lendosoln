namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class NOTE
    {
        /// <summary>
        /// Gets a value indicating whether the NOTE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AllongeToNoteSpecified
                    || this.ExtensionSpecified
                    || this.NoteAddendumsSpecified
                    || this.NoteRidersSpecified;
            }
        }

        /// <summary>
        /// An addition to the note.
        /// </summary>
        [XmlElement("ALLONGE_TO_NOTE", Order = 0)]
        public ALLONGE_TO_NOTE AllongeToNote;

        /// <summary>
        /// Gets or sets a value indicating whether the ToNote element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ToNote element has been assigned a value.</value>
        [XmlIgnore]
        public bool AllongeToNoteSpecified
        {
            get { return this.AllongeToNote != null && this.AllongeToNote.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Addendums to the note.
        /// </summary>
        [XmlElement("NOTE_ADDENDUMS", Order = 1)]
        public NOTE_ADDENDUMS NoteAddendums;

        /// <summary>
        /// Gets or sets a value indicating whether the NoteAddendums element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NoteAddendums element has been assigned a value.</value>
        [XmlIgnore]
        public bool NoteAddendumsSpecified
        {
            get { return this.NoteAddendums != null && this.NoteAddendums.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A list of note riders.
        /// </summary>
        [XmlElement("NOTE_RIDERS", Order = 2)]
        public NOTE_RIDERS NoteRiders;

        /// <summary>
        /// Gets or sets a value indicating whether the NoteRiders element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NoteRiders element has been assigned a value.</value>
        [XmlIgnore]
        public bool NoteRidersSpecified
        {
            get { return this.NoteRiders != null && this.NoteRiders.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public NOTE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
