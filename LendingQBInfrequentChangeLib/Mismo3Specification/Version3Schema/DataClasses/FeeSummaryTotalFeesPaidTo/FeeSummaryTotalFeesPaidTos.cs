namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class FEE_SUMMARY_TOTAL_FEES_PAID_TOS
    {
        /// <summary>
        /// Gets a value indicating whether the FEE_SUMMARY_TOTAL_FEES_PAID_TOS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.FeeSummaryTotalFeesPaidToSpecified;
            }
        }

        /// <summary>
        /// A collection of fee summary total fees paid to.
        /// </summary>
        [XmlElement("FEE_SUMMARY_TOTAL_FEES_PAID_TO", Order = 0)]
        public List<FEE_SUMMARY_TOTAL_FEES_PAID_TO> FeeSummaryTotalFeesPaidTo = new List<FEE_SUMMARY_TOTAL_FEES_PAID_TO>();

        /// <summary>
        /// Gets or sets a value indicating whether the FeeSummaryTotalFeesPaidTo element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeeSummaryTotalFeesPaidTo element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeeSummaryTotalFeesPaidToSpecified
        {
            get { return this.FeeSummaryTotalFeesPaidTo != null && this.FeeSummaryTotalFeesPaidTo.Count(f => f != null && f.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public FEE_SUMMARY_TOTAL_FEES_PAID_TOS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
