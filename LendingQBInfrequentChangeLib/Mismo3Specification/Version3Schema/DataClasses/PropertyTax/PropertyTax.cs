namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class PROPERTY_TAX
    {
        /// <summary>
        /// Gets a value indicating whether the PROPERTY_TAX container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.PropertyTaxDetailSpecified
                    || this.PropertyTaxExemptionsSpecified
                    || this.PropertyTaxSpecialsSpecified;
            }
        }

        /// <summary>
        /// Details on a property tax.
        /// </summary>
        [XmlElement("PROPERTY_TAX_DETAIL", Order = 0)]
        public PROPERTY_TAX_DETAIL PropertyTaxDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyTaxDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyTaxDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyTaxDetailSpecified
        {
            get { return this.PropertyTaxDetail != null && this.PropertyTaxDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Exemptions from a property tax.
        /// </summary>
        [XmlElement("PROPERTY_TAX_EXEMPTIONS", Order = 1)]
        public PROPERTY_TAX_EXEMPTIONS PropertyTaxExemptions;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyTaxExemptions element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyTaxExemptions element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyTaxExemptionsSpecified
        {
            get { return this.PropertyTaxExemptions != null && this.PropertyTaxExemptions.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Special taxes on a property.
        /// </summary>
        [XmlElement("PROPERTY_TAX_SPECIALS", Order = 2)]
        public PROPERTY_TAX_SPECIALS PropertyTaxSpecials;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyTaxSpecials element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyTaxSpecials element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyTaxSpecialsSpecified
        {
            get { return this.PropertyTaxSpecials != null && this.PropertyTaxSpecials.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public PROPERTY_TAX_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
