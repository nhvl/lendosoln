namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PROPERTY_TAXES
    {
        /// <summary>
        /// Gets a value indicating whether the PROPERTY_TAXES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.PropertyTaxSpecified;
            }
        }

        /// <summary>
        /// A collection of property taxes.
        /// </summary>
        [XmlElement("PROPERTY_TAX", Order = 0)]
		public List<PROPERTY_TAX> PropertyTax = new List<PROPERTY_TAX>();

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyTax element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyTax element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyTaxSpecified
        {
            get { return this.PropertyTax != null && this.PropertyTax.Count(p => p != null && p.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public PROPERTY_TAXES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
