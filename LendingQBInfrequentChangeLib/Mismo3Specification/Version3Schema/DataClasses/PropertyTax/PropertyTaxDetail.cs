namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PROPERTY_TAX_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the PROPERTY_TAX_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.PropertyTaxAssessmentEndYearSpecified
                    || this.PropertyTaxAssessmentStartYearSpecified
                    || this.PropertyTaxAtypicalCommentDescriptionSpecified
                    || this.PropertyTaxCertificateDateSpecified
                    || this.PropertyTaxCountyRateAreaIdentifierSpecified
                    || this.PropertyTaxDelinquentIndicatorSpecified
                    || this.PropertyTaxEndYearSpecified
                    || this.PropertyTaxExemptionIndicatorSpecified
                    || this.PropertyTaxImprovementValueAmountSpecified
                    || this.PropertyTaxLandValueAmountSpecified
                    || this.PropertyTaxTotalAssessedValueAmountSpecified
                    || this.PropertyTaxTotalSpecialTaxAmountSpecified
                    || this.PropertyTaxTotalTaxableValueAmountSpecified
                    || this.PropertyTaxTotalTaxAmountSpecified
                    || this.PropertyTaxTotalTaxWithoutSpecialAssessmentsAmountSpecified
                    || this.PropertyTaxToValueRatePercentSpecified
                    || this.PropertyTaxTypicalTaxIndicatorSpecified
                    || this.PropertyTaxYearIdentifierSpecified
                    || this.TaxAuthorityAccountBillIdentifierSpecified
                    || this.TaxAuthorityTypeOtherDescriptionSpecified
                    || this.TaxAuthorityTypeSpecified
                    || this.TaxContractIdentifierSpecified
                    || this.TaxContractLifeOfLoanIndicatorSpecified
                    || this.TaxContractOrderDateSpecified
                    || this.TaxContractSuffixIdentifierSpecified
                    || this.TaxEscrowedIndicatorSpecified
                    || this.TaxItemExemptionIndicatorSpecified;
            }
        }

        /// <summary>
        /// The end of year of the assessment (tax authority valuation) used for this tax year.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOYear PropertyTaxAssessmentEndYear;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyTaxAssessmentEndYear element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyTaxAssessmentEndYear element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyTaxAssessmentEndYearSpecified
        {
            get { return PropertyTaxAssessmentEndYear != null; }
            set { }
        }

        /// <summary>
        /// The start year of the assessment (tax authority valuation) used for this tax year.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOYear PropertyTaxAssessmentStartYear;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyTaxAssessmentStartYear element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyTaxAssessmentStartYear element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyTaxAssessmentStartYearSpecified
        {
            get { return PropertyTaxAssessmentStartYear != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe atypical taxes or special assessments.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString PropertyTaxAtypicalCommentDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyTaxAtypicalCommentDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyTaxAtypicalCommentDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyTaxAtypicalCommentDescriptionSpecified
        {
            get { return PropertyTaxAtypicalCommentDescription != null; }
            set { }
        }

        /// <summary>
        /// This is the date the tax certificate was issued signifying the assessment cutoff date for the upcoming year, e.g. the cert date in CA is always the first of the year. Other states may vary.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMODate PropertyTaxCertificateDate;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyTaxCertificateDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyTaxCertificateDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyTaxCertificateDateSpecified
        {
            get { return PropertyTaxCertificateDate != null; }
            set { }
        }

        /// <summary>
        /// The tax area or region identifier used by the county tax authority for this tax year (Property Tax Year Identifier).
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOIdentifier PropertyTaxCountyRateAreaIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyTaxCountyRateAreaIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyTaxCountyRateAreaIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyTaxCountyRateAreaIdentifierSpecified
        {
            get { return PropertyTaxCountyRateAreaIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the owner is delinquent on taxes from a taxing authority or jurisdiction.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOIndicator PropertyTaxDelinquentIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyTaxDelinquentIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyTaxDelinquentIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyTaxDelinquentIndicatorSpecified
        {
            get { return PropertyTaxDelinquentIndicator != null; }
            set { }
        }

        /// <summary>
        /// The end year of the Subject property tax year for the information.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOYear PropertyTaxEndYear;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyTaxEndYear element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyTaxEndYear element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyTaxEndYearSpecified
        {
            get { return PropertyTaxEndYear != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the subject property has a tax exemption for this tax year (Property Tax Year Identifier). See Property Tax Exemption.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOIndicator PropertyTaxExemptionIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyTaxExemptionIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyTaxExemptionIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyTaxExemptionIndicatorSpecified
        {
            get { return PropertyTaxExemptionIndicator != null; }
            set { }
        }

        /// <summary>
        /// The taxable value of the structure or structures on the property as separate from the land itself for this tax year (Property Tax Year Identifier).
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOAmount PropertyTaxImprovementValueAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyTaxImprovementValueAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyTaxImprovementValueAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyTaxImprovementValueAmountSpecified
        {
            get { return PropertyTaxImprovementValueAmount != null; }
            set { }
        }

        /// <summary>
        /// The taxable value of the property land without improvements for this tax year (Property Tax Year Identifier).
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOAmount PropertyTaxLandValueAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyTaxLandValueAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyTaxLandValueAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyTaxLandValueAmountSpecified
        {
            get { return PropertyTaxLandValueAmount != null; }
            set { }
        }

        /// <summary>
        /// Combined value of the assessed land and improvements of the property (I.e. structures) for this tax year (Property Tax Year Identifier).
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOAmount PropertyTaxTotalAssessedValueAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyTaxTotalAssessedValueAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyTaxTotalAssessedValueAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyTaxTotalAssessedValueAmountSpecified
        {
            get { return PropertyTaxTotalAssessedValueAmount != null; }
            set { }
        }

        /// <summary>
        /// The total of all of the special real estate taxes of the property for this tax year (Property Tax Year Identifier) across all tax jurisdictions and authorities. This is the amount assessed for the tax year, not the amount paid. (see Property Tax Special).
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOAmount PropertyTaxTotalSpecialTaxAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyTaxTotalSpecialTaxAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyTaxTotalSpecialTaxAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyTaxTotalSpecialTaxAmountSpecified
        {
            get { return PropertyTaxTotalSpecialTaxAmount != null; }
            set { }
        }

        /// <summary>
        /// Value of the property tax total.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOAmount PropertyTaxTotalTaxableValueAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyTaxTotalTaxableValueAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyTaxTotalTaxableValueAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyTaxTotalTaxableValueAmountSpecified
        {
            get { return PropertyTaxTotalTaxableValueAmount != null; }
            set { }
        }

        /// <summary>
        /// The total of all of the real estate taxes of the property for this tax year (Property Tax Year Identifier) across all tax jurisdictions and authorities. This is the amount assessed for the tax year, not the amount paid. This amount includes all special taxes (see Property Tax Special).
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOAmount PropertyTaxTotalTaxAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyTaxTotalTaxAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyTaxTotalTaxAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyTaxTotalTaxAmountSpecified
        {
            get { return PropertyTaxTotalTaxAmount != null; }
            set { }
        }

        /// <summary>
        /// The total of all of the real estate taxes of the property for this tax year (Property Tax Year Identifier) across all tax jurisdictions and authorities. This is the amount assessed for the tax year, not the amount paid. This amount DOES NOT include special taxes (see Property Tax Special Amount).
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOAmount PropertyTaxTotalTaxWithoutSpecialAssessmentsAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyTaxTotalTaxWithoutSpecialAssessmentsAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyTaxTotalTaxWithoutSpecialAssessmentsAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyTaxTotalTaxWithoutSpecialAssessmentsAmountSpecified
        {
            get { return PropertyTaxTotalTaxWithoutSpecialAssessmentsAmount != null; }
            set { }
        }

        /// <summary>
        /// The ratio of the property value (Property Tax Total Taxable Value Amount) to this year tax amounts (Property Tax Real Estate Total Tax Amount). For example, a $500,000 property paying $2,500.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOPercent PropertyTaxToValueRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyTaxToValueRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyTaxToValueRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyTaxToValueRatePercentSpecified
        {
            get { return PropertyTaxToValueRatePercent != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the real estate taxes of the subject property are typical for the area and type of property.
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMOIndicator PropertyTaxTypicalTaxIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyTaxTypicalTaxIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyTaxTypicalTaxIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyTaxTypicalTaxIndicatorSpecified
        {
            get { return PropertyTaxTypicalTaxIndicator != null; }
            set { }
        }

        /// <summary>
        /// The tax year of the subject property associated with the information provided.
        /// </summary>
        [XmlElement(Order = 17)]
        public MISMOIdentifier PropertyTaxYearIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyTaxYearIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyTaxYearIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyTaxYearIdentifierSpecified
        {
            get { return PropertyTaxYearIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A number assigned by the taxing authority to identify the tax bill (parcel number, account number, etc.) This may not be the same as the City, State or County Assessor Parcel Identifier.
        /// </summary>
        [XmlElement(Order = 18)]
        public MISMOIdentifier TaxAuthorityAccountBillIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the TaxAuthorityAccountBillIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TaxAuthorityAccountBillIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool TaxAuthorityAccountBillIdentifierSpecified
        {
            get { return TaxAuthorityAccountBillIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Identifies the type of tax to be paid.
        /// </summary>
        [XmlElement(Order = 19)]
        public MISMOEnum<TaxAuthorityBase> TaxAuthorityType;

        /// <summary>
        /// Gets or sets a value indicating whether the TaxAuthorityType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TaxAuthorityType element has been assigned a value.</value>
        [XmlIgnore]
        public bool TaxAuthorityTypeSpecified
        {
            get { return this.TaxAuthorityType != null && this.TaxAuthorityType.enumValue != TaxAuthorityBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture the Tax Authority Type if Other is selected as the Tax Authority Type.
        /// </summary>
        [XmlElement(Order = 20)]
        public MISMOString TaxAuthorityTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the TaxAuthorityTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TaxAuthorityTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool TaxAuthorityTypeOtherDescriptionSpecified
        {
            get { return TaxAuthorityTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The contract number assigned to the loan by the tax service.
        /// </summary>
        [XmlElement(Order = 21)]
        public MISMOIdentifier TaxContractIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the TaxContractIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TaxContractIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool TaxContractIdentifierSpecified
        {
            get { return TaxContractIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Indicates if the tax contract is transferable with the loan.
        /// </summary>
        [XmlElement(Order = 22)]
        public MISMOIndicator TaxContractLifeOfLoanIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the TaxContractLifeOfLoanIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TaxContractLifeOfLoanIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool TaxContractLifeOfLoanIndicatorSpecified
        {
            get { return TaxContractLifeOfLoanIndicator != null; }
            set { }
        }

        /// <summary>
        /// The date the request for a tax contract was submitted.
        /// </summary>
        [XmlElement(Order = 23)]
        public MISMODate TaxContractOrderDate;

        /// <summary>
        /// Gets or sets a value indicating whether the TaxContractOrderDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TaxContractOrderDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool TaxContractOrderDateSpecified
        {
            get { return TaxContractOrderDate != null; }
            set { }
        }

        /// <summary>
        /// Sub identification number to identify each parcel associated with the property under contract.
        /// </summary>
        [XmlElement(Order = 24)]
        public MISMOIdentifier TaxContractSuffixIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the TaxContractSuffixIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TaxContractSuffixIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool TaxContractSuffixIdentifierSpecified
        {
            get { return TaxContractSuffixIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Indicates if monies for this tax item are escrowed.
        /// </summary>
        [XmlElement(Order = 25)]
        public MISMOIndicator TaxEscrowedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the TaxEscrowedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TaxEscrowedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool TaxEscrowedIndicatorSpecified
        {
            get { return TaxEscrowedIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether the mortgagor has an exemption for payment of this tax item.
        /// </summary>
        [XmlElement(Order = 26)]
        public MISMOIndicator TaxItemExemptionIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the TaxItemExemptionIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TaxItemExemptionIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool TaxItemExemptionIndicatorSpecified
        {
            get { return TaxItemExemptionIndicator != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 27)]
        public PROPERTY_TAX_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
