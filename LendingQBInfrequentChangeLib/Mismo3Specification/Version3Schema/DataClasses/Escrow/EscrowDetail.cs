namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class ESCROW_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the ESCROW_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.EscrowAccountInitialBalanceAmountSpecified
                    || this.EscrowAccountMinimumBalanceAmountSpecified
                    || this.EscrowAccountShortageAmountSpecified
                    || this.EscrowAccountShortageRepaymentPeriodYearsCountSpecified
                    || this.EscrowAggregateAccountingAdjustmentAmountSpecified
                    || this.EscrowBalanceAmountSpecified
                    || this.EscrowCushionNumberOfMonthsCountSpecified
                    || this.ExtensionSpecified
                    || this.GFEDisclosedInitialEscrowBalanceAmountSpecified
                    || this.InitialEscrowDepositIncludesAllInsuranceIndicatorSpecified
                    || this.InitialEscrowDepositIncludesAllPropertyTaxesIndicatorSpecified
                    || this.InitialEscrowDepositIncludesOtherDescriptionSpecified
                    || this.InitialEscrowProjectionStatementDateSpecified
                    || this.InterestOnEscrowAccruedAmountSpecified
                    || this.InterestOnEscrowAccruedThroughDateSpecified
                    || this.InterestOnEscrowBackupWithholdingIndicatorSpecified
                    || this.InterestOnEscrowIndicatorSpecified
                    || this.InterestOnEscrowYearToDatePostedAmountSpecified
                    || this.InterestOnRestrictedEscrowIndicatorSpecified
                    || this.LastEscrowAnalysisDateSpecified
                    || this.LastInterestOnEscrowPostedDateSpecified
                    || this.MIEscrowIncludedInAggregateIndicatorSpecified
                    || this.RestrictedEscrowBalanceAmountSpecified;
            }
        }

        /// <summary>
        /// The initial total of all escrow amounts collected to establish the escrow account.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount EscrowAccountInitialBalanceAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowAccountInitialBalanceAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowAccountInitialBalanceAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowAccountInitialBalanceAmountSpecified
        {
            get { return EscrowAccountInitialBalanceAmount != null; }
            set { }
        }

        /// <summary>
        /// The lowest escrow balance target amount determined by the allowable cushion.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOAmount EscrowAccountMinimumBalanceAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowAccountMinimumBalanceAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowAccountMinimumBalanceAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowAccountMinimumBalanceAmountSpecified
        {
            get { return EscrowAccountMinimumBalanceAmount != null; }
            set { }
        }

        /// <summary>
        /// Amount the escrow account is or is anticipated to be short. (As determined by the originator of the modification.).
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOAmount EscrowAccountShortageAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowAccountShortageAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowAccountShortageAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowAccountShortageAmountSpecified
        {
            get { return EscrowAccountShortageAmount != null; }
            set { }
        }

        /// <summary>
        /// The number of years over which the Escrow Account Shortage Amount may be repaid.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOCount EscrowAccountShortageRepaymentPeriodYearsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowAccountShortageRepaymentPeriodYearsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowAccountShortageRepaymentPeriodYearsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowAccountShortageRepaymentPeriodYearsCountSpecified
        {
            get { return EscrowAccountShortageRepaymentPeriodYearsCount != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of any adjustment made to total escrow account based on Regulation X requirements. This must be a negative number or 0.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOAmount EscrowAggregateAccountingAdjustmentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowAggregateAccountingAdjustmentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowAggregateAccountingAdjustmentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowAggregateAccountingAdjustmentAmountSpecified
        {
            get { return EscrowAggregateAccountingAdjustmentAmount != null; }
            set { }
        }

        /// <summary>
        /// Escrow balance on the loan net of any escrow advances. May be a negative amount.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOAmount EscrowBalanceAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowBalanceAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowBalanceAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowBalanceAmountSpecified
        {
            get { return EscrowBalanceAmount != null; }
            set { }
        }

        /// <summary>
        /// Denotes the maximum number of monthly escrow collections allowed to be held as the projected lowest balance in the escrow account for the projection year. Escrow cushion months will have a value of 0, 1 or 2.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOCount EscrowCushionNumberOfMonthsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowCushionNumberOfMonthsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowCushionNumberOfMonthsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowCushionNumberOfMonthsCountSpecified
        {
            get { return EscrowCushionNumberOfMonthsCount != null; }
            set { }
        }

        /// <summary>
        /// The initial escrow account deposit amount, as reflected on a single instance of the GFE.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOAmount GFEDisclosedInitialEscrowBalanceAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the GFEDisclosedInitialEscrowBalanceAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GFEDisclosedInitialEscrowBalanceAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool GFEDisclosedInitialEscrowBalanceAmountSpecified
        {
            get { return GFEDisclosedInitialEscrowBalanceAmount != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that the initial escrow account deposit amount includes amounts deposited for the purpose of paying future recurring charges for all types of insurances required on the subject property, as reflected on a single instance of the GFE. For example, the total amount needed to cover the individual payments of hazard insurance, and wind insurance, and earthquake insurance.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOIndicator InitialEscrowDepositIncludesAllInsuranceIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the InitialEscrowDepositIncludesAllInsuranceIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InitialEscrowDepositIncludesAllInsuranceIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool InitialEscrowDepositIncludesAllInsuranceIndicatorSpecified
        {
            get { return InitialEscrowDepositIncludesAllInsuranceIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that the initial escrow account deposit amount includes amounts deposited for the purpose of paying future recurring charges for all types of property taxes required on the subject property, as reflected on a single instance of the GFE. For example, the total amount needed to cover the individual payments of county property taxes, and city property taxes, and school taxes.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOIndicator InitialEscrowDepositIncludesAllPropertyTaxesIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the InitialEscrowDepositIncludesAllPropertyTaxesIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InitialEscrowDepositIncludesAllPropertyTaxesIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool InitialEscrowDepositIncludesAllPropertyTaxesIndicatorSpecified
        {
            get { return InitialEscrowDepositIncludesAllPropertyTaxesIndicator != null; }
            set { }
        }

        /// <summary>
        /// Specifies the other escrow items for which payment will be covered by the initial escrow account deposit amount, as reflected on a single instance of the GFE.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOString InitialEscrowDepositIncludesOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the InitialEscrowDepositIncludesOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InitialEscrowDepositIncludesOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool InitialEscrowDepositIncludesOtherDescriptionSpecified
        {
            get { return InitialEscrowDepositIncludesOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Identifies the date that the initial escrow statement was given to the borrower.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMODate InitialEscrowProjectionStatementDate;

        /// <summary>
        /// Gets or sets a value indicating whether the InitialEscrowProjectionStatementDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InitialEscrowProjectionStatementDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool InitialEscrowProjectionStatementDateSpecified
        {
            get { return InitialEscrowProjectionStatementDate != null; }
            set { }
        }

        /// <summary>
        /// Identifies Interest on Escrow accrued not yet posted amount.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOAmount InterestOnEscrowAccruedAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the InterestOnEscrowAccruedAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InterestOnEscrowAccruedAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool InterestOnEscrowAccruedAmountSpecified
        {
            get { return InterestOnEscrowAccruedAmount != null; }
            set { }
        }

        /// <summary>
        /// Identifies date thru which interest on escrow has accrued.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMODate InterestOnEscrowAccruedThroughDate;

        /// <summary>
        /// Gets or sets a value indicating whether the InterestOnEscrowAccruedThroughDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InterestOnEscrowAccruedThroughDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool InterestOnEscrowAccruedThroughDateSpecified
        {
            get { return InterestOnEscrowAccruedThroughDate != null; }
            set { }
        }

        /// <summary>
        /// Flag to indicate if interest on escrow s/b withheld due to IRS notification.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOIndicator InterestOnEscrowBackupWithholdingIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the InterestOnEscrowBackupWithholdingIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InterestOnEscrowBackupWithholdingIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool InterestOnEscrowBackupWithholdingIndicatorSpecified
        {
            get { return InterestOnEscrowBackupWithholdingIndicator != null; }
            set { }
        }

        /// <summary>
        /// Identifies if interest is paid on for an Escrow Account of the borrower.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOIndicator InterestOnEscrowIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the InterestOnEscrowIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InterestOnEscrowIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool InterestOnEscrowIndicatorSpecified
        {
            get { return InterestOnEscrowIndicator != null; }
            set { }
        }

        /// <summary>
        /// Identifies the dollar amount of interest on escrow posted for the current year.
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMOAmount InterestOnEscrowYearToDatePostedAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the InterestOnEscrowYearToDatePostedAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InterestOnEscrowYearToDatePostedAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool InterestOnEscrowYearToDatePostedAmountSpecified
        {
            get { return InterestOnEscrowYearToDatePostedAmount != null; }
            set { }
        }

        /// <summary>
        /// Identifies if interest is paid on restricted escrow.
        /// </summary>
        [XmlElement(Order = 17)]
        public MISMOIndicator InterestOnRestrictedEscrowIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the InterestOnRestrictedEscrowIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InterestOnRestrictedEscrowIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool InterestOnRestrictedEscrowIndicatorSpecified
        {
            get { return InterestOnRestrictedEscrowIndicator != null; }
            set { }
        }

        /// <summary>
        /// Identifies the date of last Escrow Analysis.
        /// </summary>
        [XmlElement(Order = 18)]
        public MISMODate LastEscrowAnalysisDate;

        /// <summary>
        /// Gets or sets a value indicating whether the LastEscrowAnalysisDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LastEscrowAnalysisDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool LastEscrowAnalysisDateSpecified
        {
            get { return LastEscrowAnalysisDate != null; }
            set { }
        }

        /// <summary>
        /// Identifies the date for which interest on escrow was last posted to the account.
        /// </summary>
        [XmlElement(Order = 19)]
        public MISMODate LastInterestOnEscrowPostedDate;

        /// <summary>
        /// Gets or sets a value indicating whether the LastInterestOnEscrowPostedDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LastInterestOnEscrowPostedDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool LastInterestOnEscrowPostedDateSpecified
        {
            get { return LastInterestOnEscrowPostedDate != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether Private Mortgage Insurance Escrow is to be included in the cushion for Aggregate Accounting.
        /// </summary>
        [XmlElement(Order = 20)]
        public MISMOIndicator MIEscrowIncludedInAggregateIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the MIEscrowIncludedInAggregateIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIEscrowIncludedInAggregateIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIEscrowIncludedInAggregateIndicatorSpecified
        {
            get { return MIEscrowIncludedInAggregateIndicator != null; }
            set { }
        }

        /// <summary>
        /// Loss Draft Funds Balance.
        /// </summary>
        [XmlElement(Order = 21)]
        public MISMOAmount RestrictedEscrowBalanceAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the RestrictedEscrowBalanceAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RestrictedEscrowBalanceAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool RestrictedEscrowBalanceAmountSpecified
        {
            get { return RestrictedEscrowBalanceAmount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 22)]
        public ESCROW_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
