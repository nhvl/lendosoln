namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class ESCROW
    {
        /// <summary>
        /// Gets a value indicating whether the ESCROW container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.EscrowDetailSpecified
                    || this.EscrowDisclosuresSpecified
                    || this.EscrowItemsSpecified
                    || this.ExtensionSpecified
                    || this.HoldbackItemsSpecified;
            }
        }

        /// <summary>
        /// Details on the escrow.
        /// </summary>
        [XmlElement("ESCROW_DETAIL", Order = 0)]
        public ESCROW_DETAIL EscrowDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowDetailSpecified
        {
            get { return this.EscrowDetail != null && this.EscrowDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Disclosures related to the escrow.
        /// </summary>
        [XmlElement("ESCROW_DISCLOSURES", Order = 1)]
        public ESCROW_DISCLOSURES EscrowDisclosures;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowDisclosures element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowDisclosures element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowDisclosuresSpecified
        {
            get { return this.EscrowDisclosures != null && this.EscrowDisclosures.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A list of escrow items.
        /// </summary>
        [XmlElement("ESCROW_ITEMS", Order = 2)]
        public ESCROW_ITEMS EscrowItems;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowItems element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowItems element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowItemsSpecified
        {
            get { return this.EscrowItems != null && this.EscrowItems.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Holdback items on the escrow.
        /// </summary>
        [XmlElement("HOLDBACK_ITEMS", Order = 3)]
        public HOLDBACK_ITEMS HoldbackItems;

        /// <summary>
        /// Gets or sets a value indicating whether the HoldbackItems element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HoldbackItems element has been assigned a value.</value>
        [XmlIgnore]
        public bool HoldbackItemsSpecified
        {
            get { return this.HoldbackItems != null && this.HoldbackItems.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public ESCROW_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
