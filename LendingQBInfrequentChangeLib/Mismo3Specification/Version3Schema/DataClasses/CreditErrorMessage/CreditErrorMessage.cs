namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class CREDIT_ERROR_MESSAGE
    {
        /// <summary>
        /// Gets a value indicating whether the CREDIT_ERROR_MESSAGE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditErrorMessageCodeSpecified
                    || this.CreditErrorMessageSourceTypeSpecified
                    || this.CreditErrorMessageTextSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// This is a code value associated with a Credit Error Message Text element. The Credit Error Message Source Type describes the provider of the code, which could be the credit bureau, lender or the repository bureau.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOCode CreditErrorMessageCode;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditErrorMessageCode element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditErrorMessageCode element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditErrorMessageCodeSpecified
        {
            get { return CreditErrorMessageCode != null; }
            set { }
        }

        /// <summary>
        /// Specifies the source of an error message. Messages can come either from the credit data repository, lender or the credit bureau.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<CreditErrorMessageSourceBase> CreditErrorMessageSourceType;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditErrorMessageSourceType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditErrorMessageSourceType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditErrorMessageSourceTypeSpecified
        {
            get { return this.CreditErrorMessageSourceType != null && this.CreditErrorMessageSourceType.enumValue != CreditErrorMessageSourceBase.Blank; }
            set { }
        }

        /// <summary>
        /// This is error message text that appears in a credit response transaction. It may be accompanied by a Credit Error Message Source Type that describes the source of the error message text. It may also be accompanied by a Credit Error Message Code value that refers to the error message text.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString CreditErrorMessageText;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditErrorMessageText element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditErrorMessageText element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditErrorMessageTextSpecified
        {
            get { return CreditErrorMessageText != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public CREDIT_ERROR_MESSAGE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
