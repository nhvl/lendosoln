namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CREDIT_ERROR_MESSAGES
    {
        /// <summary>
        /// Gets a value indicating whether the CREDIT_ERROR_MESSAGES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditErrorMessageSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of credit error messages.
        /// </summary>
        [XmlElement("CREDIT_ERROR_MESSAGE", Order = 0)]
		public List<CREDIT_ERROR_MESSAGE> CreditErrorMessage = new List<CREDIT_ERROR_MESSAGE>();

        /// <summary>
        /// Gets or sets a value indicating whether the CreditErrorMessage element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditErrorMessage element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditErrorMessageSpecified
        {
            get { return this.CreditErrorMessage != null && this.CreditErrorMessage.Count(c => c != null && c.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public CREDIT_ERROR_MESSAGES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
