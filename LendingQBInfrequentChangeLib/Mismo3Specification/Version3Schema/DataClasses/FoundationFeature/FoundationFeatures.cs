namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class FOUNDATION_FEATURES
    {
        /// <summary>
        /// Gets a value indicating whether the FOUNDATION_FEATURES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.FoundationFeatureSpecified;
            }
        }

        /// <summary>
        /// A collection of foundation features.
        /// </summary>
        [XmlElement("FOUNDATION_FEATURE", Order = 0)]
		public List<FOUNDATION_FEATURE> FoundationFeature = new List<FOUNDATION_FEATURE>();

        /// <summary>
        /// Gets or sets a value indicating whether the FoundationFeature element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FoundationFeature element has been assigned a value.</value>
        [XmlIgnore]
        public bool FoundationFeatureSpecified
        {
            get { return this.FoundationFeature != null && this.FoundationFeature.Count(f => f != null && f.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public FOUNDATION_FEATURES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
