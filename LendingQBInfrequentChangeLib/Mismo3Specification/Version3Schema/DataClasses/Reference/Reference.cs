﻿namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// REFERENCE is a pointer to an XML structure that satisfies the XML type required within this context.
    /// The referenced structure may exist internally or externally to the document.
    /// A REFERENCE is resolved by replacing its parent container with the referenced XML structure.
    /// </summary>
    public partial class REFERENCE
    {
        /// <summary>
        /// Gets a value indicating whether the REFERENCE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.LocationLabelValueSpecified
                    || this.LocationURLSpecified
                    || this.OriginalCreatorDigestValueSpecified
                    || this.ReferenceSigningTypeSpecified;
            }
        }

        /// <summary>
        /// The URL where the resource may be found when the location is external to the current XML document.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOURL LocationURL;

        /// <summary>
        /// Gets or sets a value indicating whether the LocationURL element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LocationURL element has been assigned a value.</value>
        [XmlIgnore]
        public bool LocationURLSpecified
        {
            get { return LocationURL != null; }
            set { }
        }

        /// <summary>
        /// Location of the resource within the external resource.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOValue LocationLabelValue;

        /// <summary>
        /// Gets or sets a value indicating whether the LocationLabelValue element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LocationLabelValue element has been assigned a value.</value>
        [XmlIgnore]
        public bool LocationLabelValueSpecified
        {
            get { return LocationLabelValue != null; }
            set { }
        }

        /// <summary>
        /// Optional placeholder for storing the original digest of the resource at the time of its creation.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOValue OriginalCreatorDigestValue;

        /// <summary>
        /// Gets or sets a value indicating whether the OriginalCreatorDigestValue element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OriginalCreatorDigestValue element has been assigned a value.</value>
        [XmlIgnore]
        public bool OriginalCreatorDigestValueSpecified
        {
            get { return OriginalCreatorDigestValue != null; }
            set { }
        }

        /// <summary>
        /// This attribute describes how the content of a REFERENCE or ObjectURL element is signed using XML digital signatures. This instructs systems about which transformation, if any, needs to be applied to the REFERENCE or ObjectURL element before validating existing SYSTEM_SIGNATURES or applying new ones and what transformations are prohibited.
        /// </summary>
        [XmlIgnore]
        private Version3Schema.ReferenceSigningBase referenceSigningType;

        /// <summary>
        /// Gets or sets the Reference Signing Type.
        /// </summary>
        /// <value>A selected value from the Reference Signing enumeration.</value>
        [XmlIgnore]
        public Version3Schema.ReferenceSigningBase ReferenceSigningType
        {
            get { return referenceSigningType; }
            set { this.referenceSigningType = value; this.ReferenceSigningTypeSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the Reference Signing Type has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool ReferenceSigningTypeSpecified = false;

        /// <summary>
        /// Gets or sets the Reference Signing Type member as a string for serialization.
        /// </summary>
        /// <value>Gets the enumerated value as a string for serialization.</value>
        [XmlAttribute(AttributeName = "ReferenceSigningType")]
        public string ReferenceSigningTypeSerialized
        {
            get { return referenceSigningType.ToString("G"); }
            set { }
        }

        /// <summary>
        /// Indicates whether the Reference Signing Type can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the Reference Signing Type can be serialized.</returns>
        public bool ShouldSerializeReferenceSigningTypeSerialized()
        {
            return ReferenceSigningTypeSpecified;
        }
    }
}
