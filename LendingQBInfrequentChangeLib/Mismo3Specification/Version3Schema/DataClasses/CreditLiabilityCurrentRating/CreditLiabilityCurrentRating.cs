namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CREDIT_LIABILITY_CURRENT_RATING
    {
        /// <summary>
        /// Gets a value indicating whether the CREDIT_LIABILITY_CURRENT_RATING container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditLiabilityCurrentRatingCodeSpecified
                    || this.CreditLiabilityCurrentRatingTypeSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// The liability accounts current code (as of Date Reported) for how timely the borrower has been making payments on this account. This is also know as the Manner Of Payment (MOP). See the MISMO Implementation Guide: Credit Reporting for more information on MOP codes.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOCode CreditLiabilityCurrentRatingCode;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLiabilityCurrentRatingCode element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLiabilityCurrentRatingCode element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLiabilityCurrentRatingCodeSpecified
        {
            get { return CreditLiabilityCurrentRatingCode != null; }
            set { }
        }

        /// <summary>
        /// The liability accounts current rating (as of Date Reported) for how timely the borrower has been making payments on this account.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<CreditLiabilityCurrentRatingBase> CreditLiabilityCurrentRatingType;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLiabilityCurrentRatingType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLiabilityCurrentRatingType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLiabilityCurrentRatingTypeSpecified
        {
            get { return this.CreditLiabilityCurrentRatingType != null && this.CreditLiabilityCurrentRatingType.enumValue != CreditLiabilityCurrentRatingBase.Blank; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public CREDIT_LIABILITY_CURRENT_RATING_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
