namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class GOVERNMENT_MONITORING
    {
        /// <summary>
        /// Gets a value indicating whether the GOVERNMENT_MONITORING container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.GovernmentMonitoringDetailSpecified
                    || this.HmdaRacesSpecified;
            }
        }

        /// <summary>
        /// Details on government monitoring.
        /// </summary>
        [XmlElement("GOVERNMENT_MONITORING_DETAIL", Order = 0)]
        public GOVERNMENT_MONITORING_DETAIL GovernmentMonitoringDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the GovernmentMonitoringDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GovernmentMonitoringDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool GovernmentMonitoringDetailSpecified
        {
            get { return this.GovernmentMonitoringDetail != null && this.GovernmentMonitoringDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A list of races.
        /// </summary>
        [XmlElement("HMDA_RACES", Order = 1)]
        public HMDA_RACES HmdaRaces;

        /// <summary>
        /// Gets or sets a value indicating whether the HMDARaces element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HMDARaces element has been assigned a value.</value>
        [XmlIgnore]
        public bool HmdaRacesSpecified
        {
            get { return this.HmdaRaces != null && this.HmdaRaces.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public GOVERNMENT_MONITORING_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
