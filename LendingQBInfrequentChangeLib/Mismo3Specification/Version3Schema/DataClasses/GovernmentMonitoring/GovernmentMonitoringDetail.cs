namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class GOVERNMENT_MONITORING_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the GOVERNMENT_MONITORING_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.GenderTypeSpecified
                    || this.HMDAEthnicityTypeSpecified
                    || this.HMDARefusalIndicatorSpecified
                    || this.RaceNationalOriginRefusalIndicatorSpecified;
            }
        }

        /// <summary>
        /// The borrowers or interviewers statement about the borrowers gender.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<GenderBase> GenderType;

        /// <summary>
        /// Gets or sets a value indicating whether the GenderType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GenderType element has been assigned a value.</value>
        [XmlIgnore]
        public bool GenderTypeSpecified
        {
            get { return this.GenderType != null && this.GenderType.enumValue != GenderBase.Blank; }
            set { }
        }

        /// <summary>
        /// The borrowers or interviewers statement about the borrowers ethnicity as defined in the Home Mortgage Disclosure Act (HMDA).
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<HMDAEthnicityBase> HMDAEthnicityType;

        /// <summary>
        /// Gets or sets a value indicating whether the HMDAEthnicityType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HMDAEthnicityType element has been assigned a value.</value>
        [XmlIgnore]
        public bool HMDAEthnicityTypeSpecified
        {
            get { return this.HMDAEthnicityType != null && this.HMDAEthnicityType.enumValue != HMDAEthnicityBase.Blank; }
            set { }
        }

        /// <summary>
        /// Indicates that the borrower has refused to provide the requested HMDA information.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOIndicator HMDARefusalIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the HMDARefusalIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HMDARefusalIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool HMDARefusalIndicatorSpecified
        {
            get { return HMDARefusalIndicator != null; }
            set { }
        }

        /// <summary>
        /// Borrowers answer to the statement: I do not wish to furnish the HMDA) information. Collected on the URLA in Section X.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOIndicator RaceNationalOriginRefusalIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the RaceNationalOriginRefusalIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RaceNationalOriginRefusalIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool RaceNationalOriginRefusalIndicatorSpecified
        {
            get { return RaceNationalOriginRefusalIndicator != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public GOVERNMENT_MONITORING_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
