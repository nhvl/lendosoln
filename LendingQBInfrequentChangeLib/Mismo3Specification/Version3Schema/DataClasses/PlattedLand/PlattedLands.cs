namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PLATTED_LANDS
    {
        /// <summary>
        /// Gets a value indicating whether the PLATTED_LANDS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.PlattedLandSpecified;
            }
        }

        /// <summary>
        /// A collection of platted lands.
        /// </summary>
        [XmlElement("PLATTED_LAND", Order = 0)]
		public List<PLATTED_LAND> PlattedLand = new List<PLATTED_LAND>();

        /// <summary>
        /// Gets or sets a value indicating whether the PlattedLand element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PlattedLand element has been assigned a value.</value>
        [XmlIgnore]
        public bool PlattedLandSpecified
        {
            get { return this.PlattedLand != null && this.PlattedLand.Count(p => p != null && p.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public PLATTED_LANDS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
