namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class PLATTED_LAND
    {
        /// <summary>
        /// Gets a value indicating whether the PLATTED_LAND container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AppurtenanceDescriptionSpecified
                    || this.AppurtenanceIdentifierSpecified
                    || this.AppurtenanceTypeOtherDescriptionSpecified
                    || this.AppurtenanceTypeSpecified
                    || this.ExtensionSpecified
                    || this.PlatBlockIdentifierSpecified
                    || this.PlatBookIdentifierSpecified
                    || this.PlatBuildingIdentifierSpecified
                    || this.PlatIdentifierSpecified
                    || this.PlatInstrumentIdentifierSpecified
                    || this.PlatLotIdentifierSpecified
                    || this.PlatNameSpecified
                    || this.PlatPageIdentifierSpecified
                    || this.PlatTypeOtherDescriptionSpecified
                    || this.PlatTypeSpecified
                    || this.PlatUnitIdentifierSpecified;
            }
        }

        /// <summary>
        ///  A text field used to capture a description of items that are sold as part of a condominium unit (e.g.: parking spaces, boat docks, storage bins, etc.).
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString AppurtenanceDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the AppurtenanceDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AppurtenanceDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool AppurtenanceDescriptionSpecified
        {
            get { return AppurtenanceDescription != null; }
            set { }
        }

        /// <summary>
        /// An alpha or numeric value that specifies which appurtenant feature is being referenced.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOIdentifier AppurtenanceIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the AppurtenanceIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AppurtenanceIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool AppurtenanceIdentifierSpecified
        {
            get { return AppurtenanceIdentifier != null; }
            set { }
        }

        /// <summary>
        ///  A feature that is included as part of a parcel of property (usually in a condominium) but may not be located in physical proximity to the parcel and is not constructed as an improvement on the parcel.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<AppurtenanceBase> AppurtenanceType;

        /// <summary>
        /// Gets or sets a value indicating whether the AppurtenanceType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AppurtenanceType element has been assigned a value.</value>
        [XmlIgnore]
        public bool AppurtenanceTypeSpecified
        {
            get { return this.AppurtenanceType != null && this.AppurtenanceType.enumValue != AppurtenanceBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Platted Land Type.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString AppurtenanceTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the AppurtenanceTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AppurtenanceTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool AppurtenanceTypeOtherDescriptionSpecified
        {
            get { return AppurtenanceTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The block number assigned to a parcel of real property on a subdivision plat.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOIdentifier PlatBlockIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the PlatBlockIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PlatBlockIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool PlatBlockIdentifierSpecified
        {
            get { return PlatBlockIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The number of the plat book where the plat map is stored in the office of the county recorder.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOIdentifier PlatBookIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the PlatBookIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PlatBookIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool PlatBookIdentifierSpecified
        {
            get { return PlatBookIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A unique number assigned to each building on a condominium plat.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOIdentifier PlatBuildingIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the PlatBuildingIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PlatBuildingIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool PlatBuildingIdentifierSpecified
        {
            get { return PlatBuildingIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A computer index code assigned by the county to identify the plat in the tract index database.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOIdentifier PlatIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the PlatIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PlatIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool PlatIdentifierSpecified
        {
            get { return PlatIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The unique number assigned to a plat map by a county recorder at the time of recording.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOIdentifier PlatInstrumentIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the PlatInstrumentIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PlatInstrumentIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool PlatInstrumentIdentifierSpecified
        {
            get { return PlatInstrumentIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The lot number assigned to a parcel of real property on a subdivision plat.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOIdentifier PlatLotIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the PlatLotIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PlatLotIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool PlatLotIdentifierSpecified
        {
            get { return PlatLotIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The name used to identify the plat in the legal description of the property (assigned to the plat by the party platting the land).
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOString PlatName;

        /// <summary>
        /// Gets or sets a value indicating whether the PlatName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PlatName element has been assigned a value.</value>
        [XmlIgnore]
        public bool PlatNameSpecified
        {
            get { return PlatName != null; }
            set { }
        }

        /// <summary>
        /// The page number within the plat book where the plat map is stored in the office of the county recorder.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOIdentifier PlatPageIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the PlatPageIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PlatPageIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool PlatPageIdentifierSpecified
        {
            get { return PlatPageIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Identifies the type of plat (subdivision, condominium, certified survey map).
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOEnum<PlatBase> PlatType;

        /// <summary>
        /// Gets or sets a value indicating whether the PlatType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PlatType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PlatTypeSpecified
        {
            get { return this.PlatType != null && this.PlatType.enumValue != PlatBase.Blank; }
            set { }
        }

        /// <summary>
        /// A text field used to specify the Plat Type when "Other" is selected from the enumerated list.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOString PlatTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PlatTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PlatTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PlatTypeOtherDescriptionSpecified
        {
            get { return PlatTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A unique number assigned to each dwelling unit on a condominium plat.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOIdentifier PlatUnitIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the PlatUnitIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PlatUnitIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool PlatUnitIdentifierSpecified
        {
            get { return PlatUnitIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 15)]
        public PLATTED_LAND_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
