namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a collection of XML namespaces.
    /// </summary>
    /// <value>Declares the $$xlink$$ namespace required for MISMO relationship elements.</value>
    public partial class MESSAGE
    {
        /// <summary>
        /// Meta-data about the message.
        /// </summary>
        [XmlElement("ABOUT_VERSIONS", Order = 0)]
        public ABOUT_VERSIONS AboutVersions;

        /// <summary>
        /// Gets or sets a value indicating whether the AboutVersions element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AboutVersions element has been assigned a value.</value>
        [XmlIgnore]
        public bool AboutVersionsSpecified
        {
            get { return this.AboutVersions != null && this.AboutVersions.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Deal sets associated with the message.
        /// </summary>
        [XmlElement("DEAL_SETS", Order = 1)]
        public DEAL_SETS DealSets;

        /// <summary>
        /// Gets or sets a value indicating whether the DealSets element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DealSets element has been assigned a value.</value>
        [XmlIgnore]
        public bool DealSetsSpecified
        {
            get { return this.DealSets != null && this.DealSets.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Document sets associated with the message.
        /// </summary>
        [XmlElement("DOCUMENT_SETS", Order = 2)]
        public DOCUMENT_SETS DocumentSets;

        /// <summary>
        /// Gets or sets a value indicating whether the DocumentSets element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DocumentSets element has been assigned a value.</value>
        [XmlIgnore]
        public bool DocumentSetsSpecified
        {
            get { return this.DocumentSets != null && this.DocumentSets.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Relationships associated with the message.
        /// </summary>
        [XmlElement("RELATIONSHIPS", Order = 3)]
        public RELATIONSHIPS Relationships;

        /// <summary>
        /// Gets or sets a value indicating whether the Relationships element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Relationships element has been assigned a value.</value>
        [XmlIgnore]
        public bool RelationshipsSpecified
        {
            get { return this.Relationships != null && this.Relationships.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// System signatures associated with the message.
        /// </summary>
        [XmlElement("SYSTEM_SIGNATURES", Order = 4)]
        public SYSTEM_SIGNATURES SystemSignatures;

        /// <summary>
        /// Gets or sets a value indicating whether the SystemSignatures element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SystemSignatures element has been assigned a value.</value>
        [XmlIgnore]
        public bool SystemSignaturesSpecified
        {
            get { return this.SystemSignatures != null && this.SystemSignatures.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 5)]
        public MESSAGE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null; }
            set { }
        }

        /// <summary>
        /// The Mortgage Industry Standards Maintenance Organization Logical Data Dictionary Identifier is a unique value that represents the version of the Mortgage Industry Standards Maintenance Organization LDD section of the reference model to which the containing XML instance document complies.
        /// </summary>
        //// Pattern: 3\.\d*((\.\d*)?(\.\d*)?(\[[B]\d*(\-\d*)?\])?)?
        [XmlAttribute]
        public virtual string MISMOLogicalDataDictionaryIdentifier { get; set; } = "3.3.1[B306]";

        /// <summary>
        /// Indicates whether the Mortgage Industry Standards Maintenance Organization logical data dictionary identifier can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the Mortgage Industry Standards Maintenance Organization logical data dictionary identifier can be serialized.</returns>
        public bool ShouldSerializeMISMOLogicalDataDictionaryIdentifier()
        {
            return !string.IsNullOrEmpty(this.MISMOLogicalDataDictionaryIdentifier);
        }

        /// <summary>
        /// The Mortgage Industry Standards Maintenance Organization Reference Model Identifier is a unique value that represents the version of the Mortgage Industry Standards Maintenance Organization reference model to which the containing XML instance document complies.
        /// </summary>
        //// Pattern: 3\.\d*((\.\d*)?(\[[B]\d*(\-\d*)?\])?)?
        [XmlAttribute]
        public virtual string MISMOReferenceModelIdentifier { get; set; } = "3.3.1[B306]";

        /// <summary>
        /// Indicates whether the Mortgage Industry Standards Maintenance Organization reference model identifier can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the Mortgage Industry Standards Maintenance Organization reference model identifier can be serialized.</returns>
        public bool ShouldSerializeMISMOReferenceModelIdentifier()
        {
            return !string.IsNullOrEmpty(this.MISMOReferenceModelIdentifier);
        }

        /// <summary>
        /// A set of namespaces for declaration at the MISMO root.
        /// </summary>
        private XmlSerializerNamespaces xmlns;

        /// <summary>
        /// Gets or sets a collection of XML namespaces.
        /// </summary>
        /// <value>Declares the $$xlink$$ namespace required for MISMO relationship elements.</value>
        [XmlNamespaceDeclarations]
        public virtual XmlSerializerNamespaces Xmlns
        {
            get
            {
                if (this.xmlns == null)
                {
                    this.xmlns = new XmlSerializerNamespaces();
                    this.xmlns.Add("xlink", Mismo3Constants.XlinkNamespace);
                    this.xmlns.Add("lendingqb", "http://www.lendingqb.com");
                }

                return this.xmlns;
            }

            set
            {
                this.xmlns = value;
            }
        }
    }
}
