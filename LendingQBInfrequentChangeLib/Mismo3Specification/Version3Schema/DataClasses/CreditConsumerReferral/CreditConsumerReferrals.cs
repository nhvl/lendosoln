namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CREDIT_CONSUMER_REFERRALS
    {
        /// <summary>
        /// Gets a value indicating whether the CREDIT_CONSUMER_REFERRALS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditConsumerReferralSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of credit consumer referrals.
        /// </summary>
        [XmlElement("CREDIT_CONSUMER_REFERRAL", Order = 0)]
		public List<CREDIT_CONSUMER_REFERRAL> CreditConsumerReferral = new List<CREDIT_CONSUMER_REFERRAL>();

        /// <summary>
        /// Gets or sets a value indicating whether the CreditConsumerReferral element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditConsumerReferral element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditConsumerReferralSpecified
        {
            get { return this.CreditConsumerReferral != null && this.CreditConsumerReferral.Count(c => c != null && c.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public CREDIT_CONSUMER_REFERRALS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
