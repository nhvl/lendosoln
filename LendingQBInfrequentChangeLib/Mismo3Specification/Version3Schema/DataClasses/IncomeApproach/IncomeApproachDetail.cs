namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class INCOME_APPROACH_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the INCOME_APPROACH_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.EstimatedMarketMonthlyRentAmountSpecified
                    || this.ExtensionSpecified
                    || this.GrossRentMultiplierFactorPercentSpecified
                    || this.IncomeAnalysisCommentDescriptionSpecified
                    || this.ValueIndicatedByIncomeApproachAmountSpecified;
            }
        }

        /// <summary>
        /// The estimated dollar value of the market monthly rent for a living unit or residence.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount EstimatedMarketMonthlyRentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the EstimatedMarketMonthlyRentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EstimatedMarketMonthlyRentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool EstimatedMarketMonthlyRentAmountSpecified
        {
            get { return EstimatedMarketMonthlyRentAmount != null; }
            set { }
        }

        /// <summary>
        /// Specifies the Gross Rent Multiplier for the subject property.  (e.g. Actual Sales Price divided by Actual Gross Monthly Rent = Gross Rent Multiplier (GRM)).
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOPercent GrossRentMultiplierFactorPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the GrossRentMultiplierFactorPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GrossRentMultiplierFactorPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool GrossRentMultiplierFactorPercentSpecified
        {
            get { return GrossRentMultiplierFactorPercent != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe or comment on the use of the Income Approach in valuing a property.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString IncomeAnalysisCommentDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the IncomeAnalysisCommentDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IncomeAnalysisCommentDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool IncomeAnalysisCommentDescriptionSpecified
        {
            get { return IncomeAnalysisCommentDescription != null; }
            set { }
        }

        /// <summary>
        /// The dollar value of the subject property indicated by the Income Approach method of property valuation.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOAmount ValueIndicatedByIncomeApproachAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ValueIndicatedByIncomeApproachAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ValueIndicatedByIncomeApproachAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ValueIndicatedByIncomeApproachAmountSpecified
        {
            get { return ValueIndicatedByIncomeApproachAmount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public INCOME_APPROACH_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
