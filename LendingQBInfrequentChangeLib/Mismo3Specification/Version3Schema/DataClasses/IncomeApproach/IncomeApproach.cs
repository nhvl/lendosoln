namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class INCOME_APPROACH
    {
        /// <summary>
        /// Gets a value indicating whether the INCOME_APPROACH container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.IncomeApproachDetailSpecified
                    || this.MultifamilyRentalsSpecified
                    || this.MultifamilyRentScheduleSpecified
                    || this.ResidentialRentScheduleSpecified;
            }
        }

        /// <summary>
        /// Detail about the income producing aspects of the property.
        /// </summary>
        [XmlElement("INCOME_APPROACH_DETAIL", Order = 0)]
        public INCOME_APPROACH_DETAIL IncomeApproachDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the IncomeApproachDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IncomeApproachDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool IncomeApproachDetailSpecified
        {
            get { return IncomeApproachDetail != null && IncomeApproachDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Rent schedule for any multifamily rentals.
        /// </summary>
        [XmlElement("MULTIFAMILY_RENT_SCHEDULE", Order = 1)]
        public MULTIFAMILY_RENT_SCHEDULE MultifamilyRentSchedule;

        /// <summary>
        /// Gets or sets a value indicating whether the MultifamilyRentSchedule element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MultifamilyRentSchedule element has been assigned a value.</value>
        [XmlIgnore]
        public bool MultifamilyRentScheduleSpecified
        {
            get { return MultifamilyRentSchedule != null && MultifamilyRentSchedule.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Information on any multifamily rentals.
        /// </summary>
        [XmlElement("MULTIFAMILY_RENTALS", Order = 2)]
        public MULTIFAMILY_RENTALS MultifamilyRentals;

        /// <summary>
        /// Gets or sets a value indicating whether the MultifamilyRentals element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MultifamilyRentals element has been assigned a value.</value>
        [XmlIgnore]
        public bool MultifamilyRentalsSpecified
        {
            get { return MultifamilyRentals != null && MultifamilyRentals.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Rent schedule for the property.
        /// </summary>
        [XmlElement("RESIDENTIAL_RENT_SCHEDULE", Order = 3)]
        public RESIDENTIAL_RENT_SCHEDULE ResidentialRentSchedule;

        /// <summary>
        /// Gets or sets a value indicating whether the ResidentialRentSchedule element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ResidentialRentSchedule element has been assigned a value.</value>
        [XmlIgnore]
        public bool ResidentialRentScheduleSpecified
        {
            get { return ResidentialRentSchedule != null && ResidentialRentSchedule.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public INCOME_APPROACH_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
