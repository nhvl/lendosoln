namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PRODUCT_COMPONENTS
    {
        /// <summary>
        /// Gets a value indicating whether the PRODUCT_COMPONENTS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ProductComponentSpecified;
            }
        }

        /// <summary>
        /// A collection of product components.
        /// </summary>
        [XmlElement("PRODUCT_COMPONENT", Order = 0)]
		public List<PRODUCT_COMPONENT> ProductComponent = new List<PRODUCT_COMPONENT>();

        /// <summary>
        /// Gets or sets a value indicating whether the ProductComponent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProductComponent element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProductComponentSpecified
        {
            get { return this.ProductComponent != null && this.ProductComponent.Count(p => p != null && p.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public PRODUCT_COMPONENTS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
