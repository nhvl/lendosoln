namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class INSPECTION_SERVICE_RESPONSE
    {
        /// <summary>
        /// Gets a value indicating whether the INSPECTION_SERVICE_RESPONSE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.InspectionServiceResponseDetailSpecified
                    || this.PropertiesSpecified;
            }
        }

        /// <summary>
        /// Details on the inspection service response.
        /// </summary>
        [XmlElement("INSPECTION_SERVICE_RESPONSE_DETAIL", Order = 0)]
        public INSPECTION_SERVICE_RESPONSE_DETAIL InspectionServiceResponseDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the InspectionServiceResponseDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InspectionServiceResponseDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool InspectionServiceResponseDetailSpecified
        {
            get { return this.InspectionServiceResponseDetail != null && this.InspectionServiceResponseDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Properties to be inspected.
        /// </summary>
        [XmlElement("PROPERTIES", Order = 1)]
        public PROPERTIES Properties;

        /// <summary>
        /// Gets or sets a value indicating whether the Properties element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Properties element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertiesSpecified
        {
            get { return this.Properties != null && this.Properties.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public INSPECTION_SERVICE_RESPONSE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
