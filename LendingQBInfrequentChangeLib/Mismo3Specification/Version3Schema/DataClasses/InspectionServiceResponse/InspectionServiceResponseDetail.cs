namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class INSPECTION_SERVICE_RESPONSE_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the INSPECTION_SERVICE_RESPONSE_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.InspectorFileIdentifierSpecified;
            }
        }

        /// <summary>
        /// An identifier or number used by the inspecting professional to identify the report.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIdentifier InspectorFileIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the InspectorFileIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InspectorFileIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool InspectorFileIdentifierSpecified
        {
            get { return InspectorFileIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public INSPECTION_SERVICE_RESPONSE_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
