namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class REGULATORY_PRODUCTS
    {
        /// <summary>
        /// Gets a value indicating whether the REGULATORY_PRODUCTS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.RegulatoryProductSpecified;
            }
        }

        /// <summary>
        /// A collection of regulatory products.
        /// </summary>
        [XmlElement("REGULATORY_PRODUCT", Order = 0)]
		public List<REGULATORY_PRODUCT> RegulatoryProduct = new List<REGULATORY_PRODUCT>();

        /// <summary>
        /// Gets or sets a value indicating whether the RegulatoryProduct element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RegulatoryProduct element has been assigned a value.</value>
        [XmlIgnore]
        public bool RegulatoryProductSpecified
        {
            get { return this.RegulatoryProduct != null && this.RegulatoryProduct.Count(r => r != null && r.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public REGULATORY_PRODUCTS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
