namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class REGULATORY_PRODUCT_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the REGULATORY_PRODUCT_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditRepositorySourceTypeOtherDescriptionSpecified
                    || this.CreditRepositorySourceTypeSpecified
                    || this.ExtensionSpecified
                    || this.RegulatoryProductDisclaimerTextSpecified
                    || this.RegulatoryProductProviderDescriptionSpecified
                    || this.RegulatoryProductResultStatusTypeOtherDescriptionSpecified
                    || this.RegulatoryProductResultStatusTypeSpecified
                    || this.RegulatoryProductResultTextSpecified
                    || this.RegulatoryProductSourceTypeOtherDescriptionSpecified
                    || this.RegulatoryProductSourceTypeSpecified;
            }
        }

        /// <summary>
        /// This element describes the source of the credit file, Equifax, Experian, Trans Union or Unspecified if the specific sources are not specified.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<CreditRepositorySourceBase> CreditRepositorySourceType;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditRepositorySourceType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditRepositorySourceType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditRepositorySourceTypeSpecified
        {
            get { return this.CreditRepositorySourceType != null && this.CreditRepositorySourceType.enumValue != CreditRepositorySourceBase.Blank; }
            set { }
        }

        /// <summary>
        /// When Credit Repository Source Type is set to Other, enter its value in this data element.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString CreditRepositorySourceTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditRepositorySourceTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditRepositorySourceTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditRepositorySourceTypeOtherDescriptionSpecified
        {
            get { return CreditRepositorySourceTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Contains disclaimer text associated with the product.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString RegulatoryProductDisclaimerText;

        /// <summary>
        /// Gets or sets a value indicating whether the RegulatoryProductDisclaimerText element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RegulatoryProductDisclaimerText element has been assigned a value.</value>
        [XmlIgnore]
        public bool RegulatoryProductDisclaimerTextSpecified
        {
            get { return RegulatoryProductDisclaimerText != null; }
            set { }
        }

        /// <summary>
        /// Either the branded name or description of the product provider of the regulatory product.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString RegulatoryProductProviderDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the RegulatoryProductProviderDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RegulatoryProductProviderDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool RegulatoryProductProviderDescriptionSpecified
        {
            get { return RegulatoryProductProviderDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates the result of the current product request.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<RegulatoryProductResultStatusBase> RegulatoryProductResultStatusType;

        /// <summary>
        /// Gets or sets a value indicating whether the RegulatoryProductResultStatusType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RegulatoryProductResultStatusType element has been assigned a value.</value>
        [XmlIgnore]
        public bool RegulatoryProductResultStatusTypeSpecified
        {
            get { return this.RegulatoryProductResultStatusType != null && this.RegulatoryProductResultStatusType.enumValue != RegulatoryProductResultStatusBase.Blank ; }
            set { }
        }

        /// <summary>
        /// Contains the regulatory status when the Regulatory Product Status Type is "Other".
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString RegulatoryProductResultStatusTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the RegulatoryProductResultStatusTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RegulatoryProductResultStatusTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool RegulatoryProductResultStatusTypeOtherDescriptionSpecified
        {
            get { return RegulatoryProductResultStatusTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Contains text information result associated with the product.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOString RegulatoryProductResultText;

        /// <summary>
        /// Gets or sets a value indicating whether the RegulatoryProductResultText element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RegulatoryProductResultText element has been assigned a value.</value>
        [XmlIgnore]
        public bool RegulatoryProductResultTextSpecified
        {
            get { return RegulatoryProductResultText != null; }
            set { }
        }

        /// <summary>
        /// Identifies the regulatory product provided by the federal government. These products are used to screen borrower names to see if they match any names listed in the regulatory product.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOEnum<RegulatoryProductSourceBase> RegulatoryProductSourceType;

        /// <summary>
        /// Gets or sets a value indicating whether the RegulatoryProductSourceType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RegulatoryProductSourceType element has been assigned a value.</value>
        [XmlIgnore]
        public bool RegulatoryProductSourceTypeSpecified
        {
            get { return this.RegulatoryProductSourceType != null && this.RegulatoryProductSourceType.enumValue != RegulatoryProductSourceBase.Blank; }
            set { }
        }

        /// <summary>
        /// Contains the text description of the product when the Regulatory Product Source Type is "Other".
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOString RegulatoryProductSourceTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the RegulatoryProductSourceTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RegulatoryProductSourceTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool RegulatoryProductSourceTypeOtherDescriptionSpecified
        {
            get { return RegulatoryProductSourceTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 9)]
        public REGULATORY_PRODUCT_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
