namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class PROJECT_BLANKET_FINANCING
    {
        /// <summary>
        /// Gets a value indicating whether the PROJECT_BLANKET_FINANCING container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AmortizationTypeOtherDescriptionSpecified
                    || this.AmortizationTypeSpecified
                    || this.BalloonIndicatorSpecified
                    || this.CurrentInterestRatePercentSpecified
                    || this.ExtensionSpecified
                    || this.LienDescriptionSpecified
                    || this.LienPriorityTypeOtherDescriptionSpecified
                    || this.LienPriorityTypeSpecified
                    || this.LoanRemainingMaturityTermMonthsCountSpecified
                    || this.ProjectLienHolderNameSpecified
                    || this.TotalPaymentAmountSpecified
                    || this.UPBAmountSpecified;
            }
        }

        /// <summary>
        /// A classification or description of a loan or a group of loans generally based on the changeability of the rate or payment over time.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<AmortizationBase> AmortizationType;

        /// <summary>
        /// Gets or sets a value indicating whether the AmortizationType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AmortizationType element has been assigned a value.</value>
        [XmlIgnore]
        public bool AmortizationTypeSpecified
        {
            get { return this.AmortizationType != null && this.AmortizationType.enumValue != AmortizationBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information or a description of the amortization type when Other is selected.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString AmortizationTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the AmortizationTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AmortizationTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool AmortizationTypeOtherDescriptionSpecified
        {
            get { return AmortizationTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether or not a final balloon payment is required under the terms of the loan repayment schedule to fully pay off the loan.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOIndicator BalloonIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the BalloonIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BalloonIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool BalloonIndicatorSpecified
        {
            get { return BalloonIndicator != null; }
            set { }
        }

        /// <summary>
        /// The current interest rate, expressed as a percent, for this loan.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOPercent CurrentInterestRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the CurrentInterestRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CurrentInterestRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool CurrentInterestRatePercentSpecified
        {
            get { return CurrentInterestRatePercent != null; }
            set { }
        }

        /// <summary>
        /// Describes the lien on the property.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString LienDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the LienDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LienDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool LienDescriptionSpecified
        {
            get { return LienDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the priority of the lien against the subject property.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOEnum<LienPriorityBase> LienPriorityType;

        /// <summary>
        /// Gets or sets a value indicating whether the LienPriorityType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LienPriorityType element has been assigned a value.</value>
        [XmlIgnore]
        public bool LienPriorityTypeSpecified
        {
            get { return this.LienPriorityType != null && this.LienPriorityType.enumValue != LienPriorityBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture the lien priority type if Other is selected as the lien priority type.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOString LienPriorityTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the LienPriorityTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LienPriorityTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool LienPriorityTypeOtherDescriptionSpecified
        {
            get { return LienPriorityTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The number of months remaining until the loan matures.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOCount LoanRemainingMaturityTermMonthsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanRemainingMaturityTermMonthsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanRemainingMaturityTermMonthsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanRemainingMaturityTermMonthsCountSpecified
        {
            get { return LoanRemainingMaturityTermMonthsCount != null; }
            set { }
        }

        /// <summary>
        /// The name of the lien holder against the Project.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOString ProjectLienHolderName;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectLienHolderName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectLienHolderName element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectLienHolderNameSpecified
        {
            get { return ProjectLienHolderName != null; }
            set { }
        }

        /// <summary>
        /// The total payment amount that is reported.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOAmount TotalPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the TotalPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TotalPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TotalPaymentAmountSpecified
        {
            get { return TotalPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// The current unpaid principal balance on the loan.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOAmount UPBAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the UPBAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the UPBAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool UPBAmountSpecified
        {
            get { return UPBAmount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 11)]
        public PROJECT_BLANKET_FINANCING_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
