namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PROJECT_BLANKET_FINANCINGS
    {
        /// <summary>
        /// Gets a value indicating whether the PROJECT_BLANKET_FINANCINGS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ProjectBlanketFinancingSpecified;
            }
        }

        /// <summary>
        /// A collection of project blanket financings.
        /// </summary>
        [XmlElement("PROJECT_BLANKET_FINANCING", Order = 0)]
		public List<PROJECT_BLANKET_FINANCING> ProjectBlanketFinancing = new List<PROJECT_BLANKET_FINANCING>();

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectBlanketFinancing element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectBlanketFinancing element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectBlanketFinancingSpecified
        {
            get { return this.ProjectBlanketFinancing != null && this.ProjectBlanketFinancing.Count(p => p != null && p.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public PROJECT_BLANKET_FINANCINGS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
