namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class VERIFICATION_DATA_POOL
    {
        /// <summary>
        /// Gets a value indicating whether the VERIFICATION_DATA_POOL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.PoolTotalCountSpecified;
            }
        }

        /// <summary>
        /// Provides a count of the Pool containers within this data set.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOCount PoolTotalCount;

        /// <summary>
        /// Gets or sets a value indicating whether the PoolTotalCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PoolTotalCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PoolTotalCountSpecified
        {
            get { return PoolTotalCount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public VERIFICATION_DATA_POOL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
