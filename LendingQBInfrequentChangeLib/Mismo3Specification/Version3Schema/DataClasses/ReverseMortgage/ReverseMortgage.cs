namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class REVERSE_MORTGAGE
    {
        /// <summary>
        /// Gets a value indicating whether the REVERSE_MORTGAGE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ReverseGrossLineOfCreditReserveAmountSpecified
                    || this.ReverseMortgageDisclosedMonthlyServicingFeeAmountSpecified
                    || this.ReverseNetLineOfCreditAmountSpecified
                    || this.ReverseNetPrincipalLimitAmountSpecified
                    || this.ReversePaymentPlanTypeOtherDescriptionSpecified
                    || this.ReversePaymentPlanTypeSpecified
                    || this.ReversePaymentTermMonthsCountSpecified
                    || this.ReverseScheduledPaymentAmountSpecified
                    || this.ReverseSetAsideFirstYearPropertyChargesAmountSpecified
                    || this.ReverseSetAsideTaxesAndInsuranceWithholdingAmountSpecified
                    || this.ReverseSetAsideTaxesAndInsuranceWithholdingEndDateSpecified
                    || this.ReverseSetAsideTaxesAndInsuranceWithholdingRatePercentSpecified
                    || this.ReverseSetAsideTaxesAndInsuranceWithholdingStartDateSpecified
                    || this.ReverseTotalAvailableAmountSpecified
                    || this.UPBAmountSpecified;
            }
        }

        /// <summary>
        /// The dollar amount of the gross line of credit including remaining set asides.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount ReverseGrossLineOfCreditReserveAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ReverseGrossLineOfCreditReserveAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ReverseGrossLineOfCreditReserveAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ReverseGrossLineOfCreditReserveAmountSpecified
        {
            get { return ReverseGrossLineOfCreditReserveAmount != null; }
            set { }
        }

        /// <summary>
        /// The disclosed amount of monthly servicing fees on the reverse mortgage.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOAmount ReverseMortgageDisclosedMonthlyServicingFeeAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ReverseMortgageDisclosedMonthlyServicingFeeAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ReverseMortgageDisclosedMonthlyServicingFeeAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ReverseMortgageDisclosedMonthlyServicingFeeAmountSpecified
        {
            get { return ReverseMortgageDisclosedMonthlyServicingFeeAmount != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of the remaining line of credit as of the current reporting period for a reverse mortgage.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOAmount ReverseNetLineOfCreditAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ReverseNetLineOfCreditAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ReverseNetLineOfCreditAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ReverseNetLineOfCreditAmountSpecified
        {
            get { return ReverseNetLineOfCreditAmount != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of the net principal limit as of the current reporting period for a reverse mortgage.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOAmount ReverseNetPrincipalLimitAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ReverseNetPrincipalLimitAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ReverseNetPrincipalLimitAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ReverseNetPrincipalLimitAmountSpecified
        {
            get { return ReverseNetPrincipalLimitAmount != null; }
            set { }
        }

        /// <summary>
        /// Specifies the manner in which loan proceeds are paid out to the borrower.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<ReversePaymentPlanBase> ReversePaymentPlanType;

        /// <summary>
        /// Gets or sets a value indicating whether the ReversePaymentPlanType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ReversePaymentPlanType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ReversePaymentPlanTypeSpecified
        {
            get { return this.ReversePaymentPlanType != null && this.ReversePaymentPlanType.enumValue != ReversePaymentPlanBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is  selected as the Reverse Payment Plan Type.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString ReversePaymentPlanTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ReversePaymentPlanTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ReversePaymentPlanTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ReversePaymentPlanTypeOtherDescriptionSpecified
        {
            get { return ReversePaymentPlanTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The term expressed in months over which the borrower is to receive payments in a reverse mortgage.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOCount ReversePaymentTermMonthsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the ReversePaymentTermMonthsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ReversePaymentTermMonthsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ReversePaymentTermMonthsCountSpecified
        {
            get { return ReversePaymentTermMonthsCount != null; }
            set { }
        }

        /// <summary>
        /// The scheduled payment amount that was determined by the borrower at origination or after a change of the payment plan.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOAmount ReverseScheduledPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ReverseScheduledPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ReverseScheduledPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ReverseScheduledPaymentAmountSpecified
        {
            get { return ReverseScheduledPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount that is set aside to cover property expenses during the first year of the loan.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOAmount ReverseSetAsideFirstYearPropertyChargesAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ReverseSetAsideFirstYearPropertyChargesAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ReverseSetAsideFirstYearPropertyChargesAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ReverseSetAsideFirstYearPropertyChargesAmountSpecified
        {
            get { return ReverseSetAsideFirstYearPropertyChargesAmount != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount to be subtracted from the scheduled payment and placed in a tax and insurance set aside.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOAmount ReverseSetAsideTaxesAndInsuranceWithholdingAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ReverseSetAsideTaxesAndInsuranceWithholdingAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ReverseSetAsideTaxesAndInsuranceWithholdingAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ReverseSetAsideTaxesAndInsuranceWithholdingAmountSpecified
        {
            get { return ReverseSetAsideTaxesAndInsuranceWithholdingAmount != null; }
            set { }
        }

        /// <summary>
        /// The date selected by the borrower to end withholding the tax and insurance set aside.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMODate ReverseSetAsideTaxesAndInsuranceWithholdingEndDate;

        /// <summary>
        /// Gets or sets a value indicating whether the ReverseSetAsideTaxesAndInsuranceWithholdingEndDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ReverseSetAsideTaxesAndInsuranceWithholdingEndDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ReverseSetAsideTaxesAndInsuranceWithholdingEndDateSpecified
        {
            get { return ReverseSetAsideTaxesAndInsuranceWithholdingEndDate != null; }
            set { }
        }

        /// <summary>
        /// The percentage of the scheduled payment amount to be subtracted and placed in the tax and insurance set aside.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOPercent ReverseSetAsideTaxesAndInsuranceWithholdingRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the ReverseSetAsideTaxesAndInsuranceWithholdingRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ReverseSetAsideTaxesAndInsuranceWithholdingRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool ReverseSetAsideTaxesAndInsuranceWithholdingRatePercentSpecified
        {
            get { return ReverseSetAsideTaxesAndInsuranceWithholdingRatePercent != null; }
            set { }
        }

        /// <summary>
        /// The date selected by the borrower to begin withholding the tax and insurance set aside.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMODate ReverseSetAsideTaxesAndInsuranceWithholdingStartDate;

        /// <summary>
        /// Gets or sets a value indicating whether the ReverseSetAsideTaxesAndInsuranceWithholdingStartDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ReverseSetAsideTaxesAndInsuranceWithholdingStartDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ReverseSetAsideTaxesAndInsuranceWithholdingStartDateSpecified
        {
            get { return ReverseSetAsideTaxesAndInsuranceWithholdingStartDate != null; }
            set { }
        }

        /// <summary>
        /// The total amount available to the borrower under a specific reverse mortgage plan.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOAmount ReverseTotalAvailableAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ReverseTotalAvailableAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ReverseTotalAvailableAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ReverseTotalAvailableAmountSpecified
        {
            get { return ReverseTotalAvailableAmount != null; }
            set { }
        }

        /// <summary>
        /// The current unpaid principal balance on the loan.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOAmount UPBAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the UPBAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the UPBAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool UPBAmountSpecified
        {
            get { return UPBAmount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 15)]
        public REVERSE_MORTGAGE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
