namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class FLOOD_DETERMINATION
    {
        /// <summary>
        /// Gets a value indicating whether the FLOOD_DETERMINATION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.FloodDeterminationDetailSpecified
                    || this.FloodRiskFactorSpecified;
            }
        }

        /// <summary>
        /// Details on a flood determination.
        /// </summary>
        [XmlElement("FLOOD_DETERMINATION_DETAIL", Order = 0)]
        public FLOOD_DETERMINATION_DETAIL FloodDeterminationDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the FloodDeterminationDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FloodDeterminationDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool FloodDeterminationDetailSpecified
        {
            get { return this.FloodDeterminationDetail != null && this.FloodDeterminationDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A risk factor for flooding.
        /// </summary>
        [XmlElement("FLOOD_RISK_FACTOR", Order = 1)]
        public FLOOD_RISK_FACTOR FloodRiskFactor;

        /// <summary>
        /// Gets or sets a value indicating whether the FloodRiskFactor element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FloodRiskFactor element has been assigned a value.</value>
        [XmlIgnore]
        public bool FloodRiskFactorSpecified
        {
            get { return this.FloodRiskFactor != null && this.FloodRiskFactor.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public FLOOD_DETERMINATION_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
