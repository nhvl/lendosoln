namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class FLOOD_DETERMINATION_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the FLOOD_DETERMINATION_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.FloodCertificationIdentifierSpecified
                    || this.FloodDeterminationLifeOfLoanIndicatorSpecified
                    || this.FloodDiscrepancyGrandfatheredIndicatorSpecified
                    || this.FloodDiscrepancyIndicatorSpecified
                    || this.FloodDiscrepancyResolvedIndicatorSpecified
                    || this.FloodPartialIndicatorSpecified
                    || this.FloodProductCertifyDateSpecified
                    || this.NFIPCommunityFIRMDateSpecified
                    || this.NFIPCommunityIdentifierSpecified
                    || this.NFIPCommunityNameSpecified
                    || this.NFIPCommunityParticipationStartDateSpecified
                    || this.NFIPCommunityParticipationStatusTypeOtherDescriptionSpecified
                    || this.NFIPCommunityParticipationStatusTypeSpecified
                    || this.NFIPCountyNameSpecified
                    || this.NFIPFloodDataRevisionTypeSpecified
                    || this.NFIPFloodZoneIdentifierSpecified
                    || this.NFIPLetterOfMapDateSpecified
                    || this.NFIPMapIdentifierSpecified
                    || this.NFIPMapIndicatorSpecified
                    || this.NFIPMapPanelDateSpecified
                    || this.NFIPMapPanelIdentifierSpecified
                    || this.NFIPMapPanelSuffixIdentifierSpecified
                    || this.NFIPStateCodeSpecified
                    || this.ProtectedAreaDesignationDateSpecified
                    || this.ProtectedAreaIndicatorSpecified
                    || this.SpecialFloodHazardAreaIndicatorSpecified;
            }
        }

        /// <summary>
        /// The certification number assigned by the compliance company when an order is received from the submitting / requesting party.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIdentifier FloodCertificationIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the FloodCertificationIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FloodCertificationIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool FloodCertificationIdentifierSpecified
        {
            get { return FloodCertificationIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Indicates if the flood contract with the compliance company is for the Life of the Loan.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOIndicator FloodDeterminationLifeOfLoanIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the FloodDeterminationLifeOfLoanIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FloodDeterminationLifeOfLoanIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool FloodDeterminationLifeOfLoanIndicatorSpecified
        {
            get { return FloodDeterminationLifeOfLoanIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that a discrepancy between the property location flood zone and the policy flood zone is acceptable  because the property location in which the flood zone code is grandfathered.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOIndicator FloodDiscrepancyGrandfatheredIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the FloodDiscrepancyGrandfatheredIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FloodDiscrepancyGrandfatheredIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool FloodDiscrepancyGrandfatheredIndicatorSpecified
        {
            get { return FloodDiscrepancyGrandfatheredIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates  there is a discrepancy between the property location flood zone code and the flood zone specified in the loan's flood insurance policy. 
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOIndicator FloodDiscrepancyIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the FloodDiscrepancyIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FloodDiscrepancyIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool FloodDiscrepancyIndicatorSpecified
        {
            get { return FloodDiscrepancyIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates there was a discrepancy between the property location flood zone code and the flood zone specified in the loan's flood insurance policy but the discrepancy has been resolved.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOIndicator FloodDiscrepancyResolvedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the FloodDiscrepancyResolvedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FloodDiscrepancyResolvedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool FloodDiscrepancyResolvedIndicatorSpecified
        {
            get { return FloodDiscrepancyResolvedIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates that a portion of the subject property is in an identified Special Flood Hazard Area (SFHA).
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOIndicator FloodPartialIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the FloodPartialIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FloodPartialIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool FloodPartialIndicatorSpecified
        {
            get { return FloodPartialIndicator != null; }
            set { }
        }

        /// <summary>
        /// The date on which the flood determination was assessed by the certification provider.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMODate FloodProductCertifyDate;

        /// <summary>
        /// Gets or sets a value indicating whether the FloodProductCertifyDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FloodProductCertifyDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool FloodProductCertifyDateSpecified
        {
            get { return FloodProductCertifyDate != null; }
            set { }
        }

        /// <summary>
        /// The date on which a community received its first Flood Insurance Rate Map. Used by Insurance companies to determine flood rates as to whether the structure is pre or post FIRM.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMODate NFIPCommunityFIRMDate;

        /// <summary>
        /// Gets or sets a value indicating whether the NFIPCommunityFIRMDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NFIPCommunityFIRMDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool NFIPCommunityFIRMDateSpecified
        {
            get { return NFIPCommunityFIRMDate != null; }
            set { }
        }

        /// <summary>
        /// The 6 digit NFIP community number.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOIdentifier NFIPCommunityIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the NFIPCommunityIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NFIPCommunityIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool NFIPCommunityIdentifierSpecified
        {
            get { return NFIPCommunityIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The complete name of the community in which the building or mobile home is located.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOString NFIPCommunityName;

        /// <summary>
        /// Gets or sets a value indicating whether the NFIPCommunityName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NFIPCommunityName element has been assigned a value.</value>
        [XmlIgnore]
        public bool NFIPCommunityNameSpecified
        {
            get { return NFIPCommunityName != null; }
            set { }
        }

        /// <summary>
        /// The date on which the community entered the Regular Program of the NFIP.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMODate NFIPCommunityParticipationStartDate;

        /// <summary>
        /// Gets or sets a value indicating whether the NFIPCommunityParticipationStartDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NFIPCommunityParticipationStartDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool NFIPCommunityParticipationStartDateSpecified
        {
            get { return NFIPCommunityParticipationStartDate != null; }
            set { }
        }

        /// <summary>
        /// A code indicating the type of flood program.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOEnum<NFIPCommunityParticipationStatusBase> NFIPCommunityParticipationStatusType;

        /// <summary>
        /// Gets or sets a value indicating whether the NFIPCommunityParticipationStatusType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NFIPCommunityParticipationStatusType element has been assigned a value.</value>
        [XmlIgnore]
        public bool NFIPCommunityParticipationStatusTypeSpecified
        {
            get { return this.NFIPCommunityParticipationStatusType != null && this.NFIPCommunityParticipationStatusType.enumValue != NFIPCommunityParticipationStatusBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture the NFIP Community Participation Status Type Other Description if Other is selected.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOString NFIPCommunityParticipationStatusTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the NFIPCommunityParticipationStatusTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NFIPCommunityParticipationStatusTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool NFIPCommunityParticipationStatusTypeOtherDescriptionSpecified
        {
            get { return NFIPCommunityParticipationStatusTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The name of the county or counties in which the community is located. In unincorporated areas of a county, this would be classified as: unincorporated areas. For independent cities, this would be classified as: independent city.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOString NFIPCountyName;

        /// <summary>
        /// Gets or sets a value indicating whether the NFIPCountyName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NFIPCountyName element has been assigned a value.</value>
        [XmlIgnore]
        public bool NFIPCountyNameSpecified
        {
            get { return NFIPCountyName != null; }
            set { }
        }

        /// <summary>
        /// Letter of Map Amendment or Revision has been issued by FEMA.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOEnum<NFIPFloodDataRevisionBase> NFIPFloodDataRevisionType;

        /// <summary>
        /// Gets or sets a value indicating whether the NFIPFloodDataRevisionType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NFIPFloodDataRevisionType element has been assigned a value.</value>
        [XmlIgnore]
        public bool NFIPFloodDataRevisionTypeSpecified
        {
            get { return this.NFIPFloodDataRevisionType != null && this.NFIPFloodDataRevisionType.enumValue != NFIPFloodDataRevisionBase.Blank; }
            set { }
        }

        /// <summary>
        /// The flood zone covering the building or mobile home.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOIdentifier NFIPFloodZoneIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the NFIPFloodZoneIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NFIPFloodZoneIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool NFIPFloodZoneIdentifierSpecified
        {
            get { return NFIPFloodZoneIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The date the letter (LOMA /LOMR) was issued.
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMODate NFIPLetterOfMapDate;

        /// <summary>
        /// Gets or sets a value indicating whether the NFIPLetterOfMapDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NFIPLetterOfMapDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool NFIPLetterOfMapDateSpecified
        {
            get { return NFIPLetterOfMapDate != null; }
            set { }
        }

        /// <summary>
        /// The 11-digit number shown on the NFIP map that covers the building or mobile home. (Full concatenated version of the National Flood Community Identifier, NFIP Map Panel Identifier, NFIP Map Panel Suffix Identifier).
        /// </summary>
        [XmlElement(Order = 17)]
        public MISMOIdentifier NFIPMapIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the NFIPMapIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NFIPMapIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool NFIPMapIdentifierSpecified
        {
            get { return NFIPMapIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Indicates if the NFIP map covers the area where the building or mobile home is located.
        /// </summary>
        [XmlElement(Order = 18)]
        public MISMOIndicator NFIPMapIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the NFIPMapIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NFIPMapIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool NFIPMapIndicatorSpecified
        {
            get { return NFIPMapIndicator != null; }
            set { }
        }

        /// <summary>
        /// The map effective date or the map revised date shown on the NFIP map.
        /// </summary>
        [XmlElement(Order = 19)]
        public MISMODate NFIPMapPanelDate;

        /// <summary>
        /// Gets or sets a value indicating whether the NFIPMapPanelDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NFIPMapPanelDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool NFIPMapPanelDateSpecified
        {
            get { return NFIPMapPanelDate != null; }
            set { }
        }

        /// <summary>
        /// The 4-digit number that describes the panel in which the property is located.
        /// </summary>
        [XmlElement(Order = 20)]
        public MISMOIdentifier NFIPMapPanelIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the NFIPMapPanelIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NFIPMapPanelIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool NFIPMapPanelIdentifierSpecified
        {
            get { return NFIPMapPanelIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The letter that indicates the current map revision.
        /// </summary>
        [XmlElement(Order = 21)]
        public MISMOIdentifier NFIPMapPanelSuffixIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the NFIPMapPanelSuffixIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NFIPMapPanelSuffixIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool NFIPMapPanelSuffixIdentifierSpecified
        {
            get { return NFIPMapPanelSuffixIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The state in which the community is located, the two-digit state abbreviation.
        /// </summary>
        [XmlElement(Order = 22)]
        public MISMOCode NFIPStateCode;

        /// <summary>
        /// Gets or sets a value indicating whether the NFIPStateCode element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NFIPStateCode element has been assigned a value.</value>
        [XmlIgnore]
        public bool NFIPStateCodeSpecified
        {
            get { return NFIPStateCode != null; }
            set { }
        }

        /// <summary>
        /// The date of the Coastal Barrier Resources Area (CBRA) or Otherwise Protected Area (OPA).
        /// </summary>
        [XmlElement(Order = 23)]
        public MISMODate ProtectedAreaDesignationDate;

        /// <summary>
        /// Gets or sets a value indicating whether the ProtectedAreaDesignationDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProtectedAreaDesignationDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProtectedAreaDesignationDateSpecified
        {
            get { return ProtectedAreaDesignationDate != null; }
            set { }
        }

        /// <summary>
        /// Indicates if the building/mobile home is within a Coastal Barrier Resources Area (CBRA) or Otherwise Protected Area (OPA).
        /// </summary>
        [XmlElement(Order = 24)]
        public MISMOIndicator ProtectedAreaIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ProtectedAreaIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProtectedAreaIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProtectedAreaIndicatorSpecified
        {
            get { return ProtectedAreaIndicator != null; }
            set { }
        }

        /// <summary>
        /// Flag that indicates if any portion of the building/mobile home is in an identified Special Flood Hazard Area (SFHA) and that flood insurance is required.
        /// </summary>
        [XmlElement(Order = 25)]
        public MISMOIndicator SpecialFloodHazardAreaIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the SpecialFloodHazardAreaIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SpecialFloodHazardAreaIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool SpecialFloodHazardAreaIndicatorSpecified
        {
            get { return SpecialFloodHazardAreaIndicator != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 26)]
        public FLOOD_DETERMINATION_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
