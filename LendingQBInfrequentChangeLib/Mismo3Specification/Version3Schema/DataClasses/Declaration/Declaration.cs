namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class DECLARATION
    {
        /// <summary>
        /// Gets a value indicating whether the DECLARATION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DeclarationDetailSpecified
                    || this.DeclarationExplanationsSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// Details on a declaration.
        /// </summary>
        [XmlElement("DECLARATION_DETAIL", Order = 0)]
        public DECLARATION_DETAIL DeclarationDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the DeclarationDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DeclarationDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool DeclarationDetailSpecified
        {
            get { return this.DeclarationDetail != null && this.DeclarationDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Explanation of a declaration.
        /// </summary>
        [XmlElement("DECLARATION_EXPLANATIONS", Order = 1)]
        public DECLARATION_EXPLANATIONS DeclarationExplanations;

        /// <summary>
        /// Gets or sets a value indicating whether the DeclarationExplanations element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DeclarationExplanations element has been assigned a value.</value>
        [XmlIgnore]
        public bool DeclarationExplanationsSpecified
        {
            get { return this.DeclarationExplanations != null && this.DeclarationExplanations.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public DECLARATION_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
