namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class DECLARATION_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the DECLARATION_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AlimonyChildSupportObligationIndicatorSpecified
                    || this.BankruptcyIndicatorSpecified
                    || this.BorrowedDownPaymentIndicatorSpecified
                    || this.BorrowerFirstTimeHomebuyerIndicatorSpecified
                    || this.CitizenshipResidencyTypeSpecified
                    || this.CoMakerEndorserOfNoteIndicatorSpecified
                    || this.ExtensionSpecified
                    || this.HomeownerPastThreeYearsTypeSpecified
                    || this.IntentToOccupyTypeSpecified
                    || this.LoanForeclosureOrJudgmentIndicatorSpecified
                    || this.OutstandingJudgmentsIndicatorSpecified
                    || this.PartyToLawsuitIndicatorSpecified
                    || this.PresentlyDelinquentIndicatorSpecified
                    || this.PriorPropertyTitleTypeSpecified
                    || this.PriorPropertyUsageTypeSpecified
                    || this.PropertyForeclosedPastSevenYearsIndicatorSpecified;
            }
        }

        /// <summary>
        /// Borrowers declaration regarding obligations for alimony, child support, etc. Collected on the URLA in Section VIII line g.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIndicator AlimonyChildSupportObligationIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the AlimonyChildSupportObligationIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AlimonyChildSupportObligationIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool AlimonyChildSupportObligationIndicatorSpecified
        {
            get { return AlimonyChildSupportObligationIndicator != null; }
            set { }
        }

        /// <summary>
        /// Borrowers answer to the question: Have you been declared bankrupt within the past 7 years? Collected on the URLA in Section VIII line b.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOIndicator BankruptcyIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the BankruptcyIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BankruptcyIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool BankruptcyIndicatorSpecified
        {
            get { return BankruptcyIndicator != null; }
            set { }
        }

        /// <summary>
        /// Borrowers answer to the question: Is any of the down payment borrowed? Collected on the URLA in Section VIII line h.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOIndicator BorrowedDownPaymentIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the BorrowedDownPaymentIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BorrowedDownPaymentIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool BorrowedDownPaymentIndicatorSpecified
        {
            get { return BorrowedDownPaymentIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the borrower qualifies as a first time homebuyer as determined by the lender and/or the investor. (Note: The 3 year rule used in the Declarations alone is not sufficient.).
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOIndicator BorrowerFirstTimeHomebuyerIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the BorrowerFirstTimeHomebuyerIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BorrowerFirstTimeHomebuyerIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool BorrowerFirstTimeHomebuyerIndicatorSpecified
        {
            get { return BorrowerFirstTimeHomebuyerIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates the U.S. citizenship or alien status of the borrower, as collected on the URLA (Section VIII, lines j. and k.).
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<CitizenshipResidencyBase> CitizenshipResidencyType;

        /// <summary>
        /// Gets or sets a value indicating whether the CitizenshipResidencyType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CitizenshipResidencyType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CitizenshipResidencyTypeSpecified
        {
            get { return this.CitizenshipResidencyType != null && this.CitizenshipResidencyType.enumValue != CitizenshipResidencyBase.Blank; }
            set { }
        }

        /// <summary>
        /// Borrowers answer to the question: Are you a co-maker or endorser on a note? Collected on the URLA in Section VIII line i.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOIndicator CoMakerEndorserOfNoteIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the CoMakerEndorserOfNoteIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CoMakerEndorserOfNoteIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool CoMakerEndorserOfNoteIndicatorSpecified
        {
            get { return CoMakerEndorserOfNoteIndicator != null; }
            set { }
        }

        /// <summary>
        /// Borrowers answer to the question: Have you had an ownership interest in a property in the last three years?.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOEnum<HomeownerPastThreeYearsBase> HomeownerPastThreeYearsType;

        /// <summary>
        /// Gets or sets a value indicating whether the HomeownerPastThreeYearsType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HomeownerPastThreeYearsType element has been assigned a value.</value>
        [XmlIgnore]
        public bool HomeownerPastThreeYearsTypeSpecified
        {
            get { return this.HomeownerPastThreeYearsType != null && this.HomeownerPastThreeYearsType.enumValue != HomeownerPastThreeYearsBase.Blank; }
            set { }
        }

        /// <summary>
        /// Borrowers answer to the question: Do you intend to occupy the property as your primary residence?.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOEnum<IntentToOccupyBase> IntentToOccupyType;

        /// <summary>
        /// Gets or sets a value indicating whether the IntentToOccupyType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IntentToOccupyType element has been assigned a value.</value>
        [XmlIgnore]
        public bool IntentToOccupyTypeSpecified
        {
            get { return this.IntentToOccupyType != null && this.IntentToOccupyType.enumValue != IntentToOccupyBase.Blank; }
            set { }
        }

        /// <summary>
        /// Borrowers answer to the question: Have you directly or indirectly been obligated on any loan that resulted in foreclosure, transfer of title in lieu of foreclosure, or judgment? Collected on the URLA in Section VIII line e.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOIndicator LoanForeclosureOrJudgmentIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanForeclosureOrJudgmentIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanForeclosureOrJudgmentIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanForeclosureOrJudgmentIndicatorSpecified
        {
            get { return LoanForeclosureOrJudgmentIndicator != null; }
            set { }
        }

        /// <summary>
        /// Borrowers answer to the question: Are there any outstanding judgments against you? Collected on the URLA in Section VIII line a.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOIndicator OutstandingJudgmentsIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the OutstandingJudgmentsIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OutstandingJudgmentsIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool OutstandingJudgmentsIndicatorSpecified
        {
            get { return OutstandingJudgmentsIndicator != null; }
            set { }
        }

        /// <summary>
        /// Borrowers answer to the question: Are you a party to a lawsuit? Collected on the URLA in Section VIII line d.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOIndicator PartyToLawsuitIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PartyToLawsuitIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PartyToLawsuitIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PartyToLawsuitIndicatorSpecified
        {
            get { return PartyToLawsuitIndicator != null; }
            set { }
        }

        /// <summary>
        /// Borrowers answer to the question: Are you presently delinquent or in default on any Federal debt or any other loan mortgage, financial obligation, bond or loan guarantee? Collected on the URLA in Section VIII line f.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOIndicator PresentlyDelinquentIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PresentlyDelinquentIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PresentlyDelinquentIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PresentlyDelinquentIndicatorSpecified
        {
            get { return PresentlyDelinquentIndicator != null; }
            set { }
        }

        /// <summary>
        /// Borrowers answer to the question: How did you hold title to the home solely by yourself, jointly with your spouse, or jointly with another person? Collected on the URLA in Section VIII line m2.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOEnum<PriorPropertyTitleBase> PriorPropertyTitleType;

        /// <summary>
        /// Gets or sets a value indicating whether the PriorPropertyTitleType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PriorPropertyTitleType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PriorPropertyTitleTypeSpecified
        {
            get { return this.PriorPropertyTitleType != null && this.PriorPropertyTitleType.enumValue != PriorPropertyTitleBase.Blank; }
            set { }
        }

        /// <summary>
        /// Borrowers answer to the question: What type of property did you own principal residence, second home, or investment property? Collected on the URLA in Section VIII line m1.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOEnum<PriorPropertyUsageBase> PriorPropertyUsageType;

        /// <summary>
        /// Gets or sets a value indicating whether the PriorPropertyUsageType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PriorPropertyUsageType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PriorPropertyUsageTypeSpecified
        {
            get { return this.PriorPropertyUsageType != null && this.PriorPropertyUsageType.enumValue != PriorPropertyUsageBase.Blank; }
            set { }
        }

        /// <summary>
        /// Borrowers answer to the question: Have you had property foreclosed upon or given title or deed in lieu thereof in the last seven years? Collected on the URLA in Section VIII line c.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOIndicator PropertyForeclosedPastSevenYearsIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyForeclosedPastSevenYearsIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyForeclosedPastSevenYearsIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyForeclosedPastSevenYearsIndicatorSpecified
        {
            get { return PropertyForeclosedPastSevenYearsIndicator != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 15)]
        public DECLARATION_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
