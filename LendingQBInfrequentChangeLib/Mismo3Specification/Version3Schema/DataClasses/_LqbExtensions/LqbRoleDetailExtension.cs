namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the IncludedInDisclosure element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the IncludedInDisclosure element has a value to serialize.</value>
    public class LQB_ROLE_DETAIL_EXTENSION
    {
        /// <summary>
        /// Gets a value indicating whether the LQB_ROLE_DETAIL_EXTENSION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.IncludedInDisclosureSpecified;
            }
        }

        /// <summary>
        /// Indicates whether a party is listed on the Loan Estimate or Closing Disclosure.
        /// </summary>
        [XmlElement("IncludedInDisclosure", Order = 0, Namespace = "http://www.lendingqb.com")]
        public MISMOIndicator IncludedInDisclosure;

        /// <summary>
        /// Gets or sets a value indicating whether the IncludedInDisclosure element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IncludedInDisclosure element has a value to serialize.</value>
        [XmlIgnore]
        public bool IncludedInDisclosureSpecified
        {
            get
            {
                return this.IncludedInDisclosure != null;
            }
        }
    }
}
