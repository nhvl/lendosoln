namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the ChangeOfCircumstanceOccurrence element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the ChangeOfCircumstanceOccurrence element has been assigned a value.</value>
    public class LQB_CHANGE_OF_CIRCUMSTANCE_OCCURRENCES
    {
        /// <summary>
        /// Gets a value indicating whether the LQB_CHANGE_OF_CIRCUMSTANCE_OCCURRENCES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ChangeOfCircumstanceOccurrenceSpecified;
            }
        }

        /// <summary>
        /// A collection of change of circumstance occurrences.
        /// </summary>
        [XmlElement("CHANGE_OF_CIRCUMSTANCE_OCCURRENCE", Order = 0, Namespace = "http://www.lendingqb.com")]
        public List<LQB_CHANGE_OF_CIRCUMSTANCE_OCCURRENCE> ChangeOfCircumstanceOccurrence = new List<LQB_CHANGE_OF_CIRCUMSTANCE_OCCURRENCE>();

        /// <summary>
        /// Gets or sets a value indicating whether the ChangeOfCircumstanceOccurrence element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ChangeOfCircumstanceOccurrence element has been assigned a value.</value>
        [XmlIgnore]
        public bool ChangeOfCircumstanceOccurrenceSpecified
        {
            get { return this.ChangeOfCircumstanceOccurrence != null && this.ChangeOfCircumstanceOccurrence.Count(c => c != null && c.ShouldSerialize) > 0; }
            set { }
        }
    }
}
