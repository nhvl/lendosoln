﻿namespace Mismo3Specification.Version3Schema
{
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// A proprietary extension to the CONSTRUCTION element.
    /// </summary>
    public class LQB_CONSTRUCTION_EXTENSION
    {
        /// <summary>
        /// Gets a value indicating whether the element should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ConstructionPurposeTypeSpecified
                    || this.ConstructionLoanAmortizationTypeSpecified
                    || this.ConstructionInitialAdvanceAmountSpecified
                    || this.ConstructionRequiredInterestReserveIndicatorSpecified
                    || this.ConstructionPhaseInterestAccrualTypeSpecified
                    || this.ConstructionLoanDateSpecified
                    || this.ConstructionPeriodEndDateSpecified
                    || this.ConstructionFirstPaymentDueDateSpecified;
            }
        }

        /// <summary>
        /// The purpose of a construction loan.
        /// </summary>
        [XmlElement("ConstructionPurposeType", Order = 0, Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<LqbConstructionPurposeBase> ConstructionPurposeType;

        /// <summary>
        /// Gets a value indicating whether the <see cref="ConstructionPurposeType"/> element has data to serialize.
        /// </summary>
        [XmlIgnore]
        public bool ConstructionPurposeTypeSpecified
        {
            get
            {
                return this.ConstructionPurposeType != null;
            }
        }

        /// <summary>
        /// The amortization type of a construction loan.
        /// </summary>
        [XmlElement("ConstructionLoanAmortizationType", Order = 1, Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<LqbConstructionLoanAmortizationBase> ConstructionLoanAmortizationType;

        /// <summary>
        /// Gets a value indicating whether the <see cref="ConstructionLoanAmortizationType"/> element has data to serialize.
        /// </summary>
        [XmlIgnore]
        public bool ConstructionLoanAmortizationTypeSpecified
        {
            get
            {
                return this.ConstructionLoanAmortizationType != null;
            }
        }

        /// <summary>
        /// The initial advance amount for a construction loan.
        /// </summary>
        [XmlElement("ConstructionInitialAdvanceAmount", Order = 2, Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOAmount ConstructionInitialAdvanceAmount;

        /// <summary>
        /// Gets a value indicating whether the <see cref="ConstructionInitialAdvanceAmount"/> element has data to serialize.
        /// </summary>
        [XmlIgnore]
        public bool ConstructionInitialAdvanceAmountSpecified
        {
            get
            {
                return this.ConstructionInitialAdvanceAmount != null;
            }
        }

        /// <summary>
        /// Indicates whether the construction loan requires an interest reserve.
        /// </summary>
        [XmlElement("ConstructionRequiredInterestReserveIndicator", Order = 3, Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOIndicator ConstructionRequiredInterestReserveIndicator;

        /// <summary>
        /// Gets a value indicating whether the <see cref="ConstructionRequiredInterestReserveIndicator"/> element has data to serialize.
        /// </summary>
        [XmlIgnore]
        public bool ConstructionRequiredInterestReserveIndicatorSpecified
        {
            get
            {
                return this.ConstructionRequiredInterestReserveIndicator != null;
            }
        }

        /// <summary>
        /// The interest accrual type during the construction phase.
        /// </summary>
        [XmlElement("ConstructionPhaseInterestAccrualType", Order = 4, Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<LqbConstructionPhaseInterestAccrualBase> ConstructionPhaseInterestAccrualType;

        /// <summary>
        /// Gets a value indicating whether the <see cref="ConstructionPhaseInterestAccrualType"/> element has data to serialize.
        /// </summary>
        [XmlIgnore]
        public bool ConstructionPhaseInterestAccrualTypeSpecified
        {
            get
            {
                return this.ConstructionPhaseInterestAccrualType != null;
            }
        }

        /// <summary>
        /// The construction loan date.
        /// </summary>
        [XmlElement("ConstructionLoanDate", Order = 5, Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMODate ConstructionLoanDate;

        /// <summary>
        /// Gets a value indicating whether the <see cref="ConstructionLoanDate"/> element has data to serialize.
        /// </summary>
        [XmlIgnore]
        public bool ConstructionLoanDateSpecified
        {
            get
            {
                return this.ConstructionLoanDate != null;
            }
        }

        /// <summary>
        /// The end date of the construction phase.
        /// </summary>
        [XmlElement("ConstructionPeriodEndDate", Order = 6, Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMODate ConstructionPeriodEndDate;

        /// <summary>
        /// Gets a value indicating whether the <see cref="ConstructionPeriodEndDate"/> element has data to serialize.
        /// </summary>
        [XmlIgnore]
        public bool ConstructionPeriodEndDateSpecified
        {
            get
            {
                return this.ConstructionPeriodEndDate != null;
            }
        }

        /// <summary>
        /// The due date of the first construction loan payment.
        /// </summary>
        [XmlElement("ConstructionFirstPaymentDueDate", Order = 7, Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMODate ConstructionFirstPaymentDueDate;

        /// <summary>
        /// Gets a value indicating whether the <see cref="ConstructionFirstPaymentDueDate"/> element has data to serialize.
        /// </summary>
        [XmlIgnore]
        public bool ConstructionFirstPaymentDueDateSpecified
        {
            get
            {
                return this.ConstructionFirstPaymentDueDate != null;
            }
        }

        /// <summary>
        /// The original cost of the land.
        /// </summary>
        [XmlElement("LandCostAmount", Order = 8, Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOAmount LandCostAmount;

        /// <summary>
        /// Gets a value indicating whether the <see cref="LandCostAmount"/> element has data to serialize.
        /// </summary>
        [XmlIgnore]
        public bool LandCostAmountSpecified
        {
            get
            {
                return this.LandCostAmount != null;
            }
        }

        /// <summary>
        /// The value of the lot.
        /// </summary>
        [XmlElement("LotValueAmount", Order = 9, Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOAmount LotValueAmount;

        /// <summary>
        /// Gets a value indicating whether the <see cref="LotValueAmount"/> element has data to serialize.
        /// </summary>
        [XmlIgnore]
        public bool LotValueAmountSpecified
        {
            get
            {
                return this.LotValueAmount != null;
            }
        }

        /// <summary>
        /// The date the lot was acquired.
        /// </summary>
        [XmlElement("LotAcquiredDate", Order = 10, Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMODate LotAcquiredDate;

        /// <summary>
        /// Gets a value indicating whether the <see cref="LotAcquiredDate"/> element has data to serialize.
        /// </summary>
        [XmlIgnore]
        public bool LotAcquiredDateSpecified
        {
            get
            {
                return this.LotAcquiredDate != null;
            }
        }

        /// <summary>
        /// The owner of the lot.
        /// </summary>
        [XmlElement("LotOwnerType", Order = 11, Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<LqbLotOwnerBase> LotOwnerType;

        /// <summary>
        /// Gets a value indicating whether the <see cref="LotOwnerType"/> element has data to serialize.
        /// </summary>
        [XmlIgnore]
        public bool LotOwnerTypeSpecified
        {
            get
            {
                return this.LotOwnerType != null;
            }
        }

        /// <summary>
        /// The subsequently paid finance charge amount.
        /// </summary>
        [XmlElement("SubsequentlyPaidFinanceChargeAmount", Order = 12, Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOAmount SubsequentlyPaidFinanceChargeAmount;

        /// <summary>
        /// Gets a value indicating whether the <see cref="SubsequentlyPaidFinanceChargeAmount"/> element has data to serialize.
        /// </summary>
        [XmlIgnore]
        public bool SubsequentlyPaidFinanceChargeAmountSpecified
        {
            get
            {
                return this.SubsequentlyPaidFinanceChargeAmount != null;
            }
        }

        /// <summary>
        /// The amount of construction interest to be paid.
        /// </summary>
        [XmlElement("ConstructionInterestAmount", Order = 13, Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOAmount ConstructionInterestAmount;

        /// <summary>
        /// Gets a value indicating whether the <see cref="ConstructionInterestAmount"/> element has data to serialize.
        /// </summary>
        [XmlIgnore]
        public bool ConstructionInterestAmountSpecified
        {
            get
            {
                return this.ConstructionInterestAmount != null;
            }
        }

        /// <summary>
        /// The date construction interest will accrue.
        /// </summary>
        [XmlElement("ConstructionInterestAccrualDate", Order = 14, Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMODate ConstructionInterestAccrualDate;

        /// <summary>
        /// Gets a value indicating whether the <see cref="ConstructionInterestAccrualDate"/> element has data to serialize.
        /// </summary>
        [XmlIgnore]
        public bool ConstructionInterestAccrualDateSpecified
        {
            get
            {
                return this.ConstructionInterestAccrualDate != null;
            }
        }

        /// <summary>
        /// The construction disclosure type.
        /// </summary>
        [XmlElement("ConstructionDisclosureType", Order = 15, Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<LqbConstructionDisclosureBase> ConstructionDisclosureType;

        /// <summary>
        /// Gets a value indicating whether the <see cref="ConstructionDisclosureType"/> element has data to serialize.
        /// </summary>
        [XmlIgnore]
        public bool ConstructionDisclosureTypeSpecified
        {
            get
            {
                return this.ConstructionDisclosureType != null;
            }
        }
    }
}
