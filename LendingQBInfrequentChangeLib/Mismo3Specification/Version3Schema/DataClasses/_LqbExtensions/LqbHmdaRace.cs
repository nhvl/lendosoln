﻿namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    public partial class LQB_HMDA_RACE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.HMDARaceDetailSpecified || this.HMDARaceDesignationsSpecified;
            }
        }

        [XmlElement("HMDA_RACE_DETAIL", Order = 0, Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public LQB_HMDA_RACE_DETAIL HMDARaceDetail;

        [XmlIgnore]
        public bool HMDARaceDetailSpecified
        {
            get { return this.HMDARaceDetail != null; }
            set { }
        }

        [XmlElement("HMDA_RACE_DESIGNATIONS", Order = 1, Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public LQB_HMDA_RACE_DESIGNATIONS HMDARaceDesignations;

        [XmlIgnore]
        public bool HMDARaceDesignationsSpecified
        {
            get { return this.HMDARaceDesignations != null; }
            set { }
        }
    }
}
