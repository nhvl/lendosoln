namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the InterestOnlyIndicator element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the InterestOnlyIndicator element has a value to serialize.</value>
    public class LQB_PROJECTED_PAYMENT_EXTENSION
    {
        /// <summary>
        /// Gets a value indicating whether the LQB_PROJECTED_PAYMENT_EXTENSION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.InterestOnlyIndicatorSpecified;
            }
        }

        /// <summary>
        /// Indicates whether the projected payment includes an interest-only payment.
        /// </summary>
        [XmlElement("InterestOnlyIndicator", Order = 0, Namespace = "http://www.lendingqb.com")]
        public MISMOIndicator InterestOnlyIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the InterestOnlyIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InterestOnlyIndicator element has a value to serialize.</value>
        [XmlIgnore]
        public bool InterestOnlyIndicatorSpecified
        {
            get
            {
                return this.InterestOnlyIndicator != null;
            }
        }
    }
}
