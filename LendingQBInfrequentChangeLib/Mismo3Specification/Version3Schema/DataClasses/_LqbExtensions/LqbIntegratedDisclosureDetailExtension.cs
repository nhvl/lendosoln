namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the LandOriginalCostAmount element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the LandOriginalCostAmount element has been assigned a value.</value>
    public class LQB_INTEGRATED_DISCLOSURE_DETAIL_EXTENSION
    {
        /// <summary>
        /// Gets a value indicating whether the LQB_INTEGRATED_DISCLOSURE_DETAIL_EXTENSION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CurrentLoanSpecified 
                    || this.LastDisclosedSpecified 
                    || this.DeliveryMethodTypeSpecified 
                    || this.ReceivedDateSpecified
                    || this.ClosingDateSpecified
                    || this.RefinanceIncludingDebtsToBePaidOffAmountSpecified
                    || this.BorrowerRequestedLoanAmountSpecified
                    || this.AlterationsImprovementsAndRepairsAmountSpecified
                    || this.LandOriginalCostAmountSpecified
                    || this.CdEscrowFirstYearAnalysisStartFromTypeSpecified;
            }
        }

        /// <summary>
        /// Indicates whether the associated integrated disclosure data-set reflects the current loan file, i.e. the data to-be-disclosed.
        /// </summary>
        [XmlElement("CurrentLoan", Order = 0, Namespace = "http://www.lendingqb.com")]
        public MISMOIndicator CurrentLoan;

        /// <summary>
        /// Gets or sets a value indicating whether the CurrentLoan element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CurrentLoan element has a value to serialize.</value>
        [XmlIgnore]
        public bool CurrentLoanSpecified
        {
            get
            {
                return this.CurrentLoan != null;
            }
        }

        /// <summary>
        /// The method used to deliver the integrated disclosure to the borrower, e.g. email, fax, {snail} mail, etc.
        /// </summary>
        [XmlElement("DeliveryMethodType", Order = 1, Namespace = "http://www.lendingqb.com")]
        public MISMOString DeliveryMethodType;

        /// <summary>
        /// Gets or sets a value indicating whether the DeliveryMethodType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DeliveryMethodType element has a value to serialize.</value>
        [XmlIgnore]
        public bool DeliveryMethodTypeSpecified
        {
            get
            {
                return this.DeliveryMethodType != null;
            }
            set { }
        }

        /// <summary>
        /// Indicates whether this Loan Estimate or Closing Disclosure was the last disclosed.
        /// </summary>
        [XmlElement("LastDisclosed", Order = 2, Namespace = "http://www.lendingqb.com")]
        public MISMOIndicator LastDisclosed;

        /// <summary>
        /// Gets or sets a value indicating whether the LastDisclosed element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LastDisclosed element has a value to serialize.</value>
        [XmlIgnore]
        public bool LastDisclosedSpecified
        {
            get
            {
                return this.LastDisclosed != null;
            }
        }

        /// <summary>
        /// The date the integrated disclosure was received by the borrower.
        /// </summary>
        [XmlElement("ReceivedDate", Order = 3, Namespace = "http://www.lendingqb.com")]
        public MISMODate ReceivedDate;

        /// <summary>
        /// Gets or sets a value indicating whether the ReceivedDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ReceivedDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ReceivedDateSpecified
        {
            get { return this.ReceivedDate != null && !string.IsNullOrEmpty(this.ReceivedDate.Value); }
            set { }
        }

        /// <summary>
        /// The archived closing date of the loan.
        /// </summary>
        [XmlElement("ClosingDate", Order = 4, Namespace = "http://www.lendingqb.com")]
        public MISMODate ClosingDate;

        /// <summary>
        /// Gets or sets a value indicating whether the ClosingDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ClosingDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ClosingDateSpecified
        {
            get { return this.ClosingDate != null && !string.IsNullOrEmpty(this.ClosingDate.Value); }
            set { }
        }

        /// <summary>
        /// The refinance amount, including debts to be paid off.
        /// </summary>
        [XmlElement("RefinanceIncludingDebtsToBePaidOffAmount", Order = 5, Namespace = "http://www.lendingqb.com")]
        public MISMOAmount RefinanceIncludingDebtsToBePaidOffAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the RefinanceIncludingDebtsToBePaidOffAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RefinanceIncludingDebtsToBePaidOffAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool RefinanceIncludingDebtsToBePaidOffAmountSpecified
        {
            get { return this.RefinanceIncludingDebtsToBePaidOffAmount != null && !string.IsNullOrEmpty(this.RefinanceIncludingDebtsToBePaidOffAmount.Value); }
        }

        /// <summary>
        /// The loan amount requested by the borrower.
        /// </summary>
        [XmlElement("BorrowerRequestedLoanAmount", Order = 6, Namespace = "http://www.lendingqb.com")]
        public MISMOAmount BorrowerRequestedLoanAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the BorrowerRequestedLoanAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BorrowerRequestedLoanAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool BorrowerRequestedLoanAmountSpecified
        {
            get { return this.BorrowerRequestedLoanAmount != null && !string.IsNullOrEmpty(this.BorrowerRequestedLoanAmount.Value); }
        }

        /// <summary>
        /// The amount of any alterations, improvements, and repairs.
        /// </summary>
        [XmlElement("AlterationsImprovementsAndRepairsAmount", Order = 7, Namespace = "http://www.lendingqb.com")]
        public MISMOAmount AlterationsImprovementsAndRepairsAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the AlterationsImprovementsAndRepairsAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AlterationsImprovementsAndRepairsAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool AlterationsImprovementsAndRepairsAmountSpecified
        {
            get { return this.AlterationsImprovementsAndRepairsAmount != null && !string.IsNullOrEmpty(this.AlterationsImprovementsAndRepairsAmount.Value); }
        }

        /// <summary>
        /// The original cost of the land.
        /// </summary>
        [XmlElement("LandOriginalCostAmount", Order = 8, Namespace = "http://www.lendingqb.com")]
        public MISMOAmount LandOriginalCostAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the LandOriginalCostAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LandOriginalCostAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool LandOriginalCostAmountSpecified
        {
            get { return this.LandOriginalCostAmount != null && !string.IsNullOrEmpty(this.LandOriginalCostAmount.Value); }
        }

        /// <summary>
        /// The starting point for first-year escrow analysis.
        /// </summary>
        [XmlElement("CDEscrowFirstYearAnalysisStartFromType", Order = 9, Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<LqbCdEscrowFirstYearAnalysisStartFromBase> CdEscrowFirstYearAnalysisStartFromType;

        /// <summary>
        /// Gets or sets a value indicating whether the CdEscrowFirstYearAnalysisStartFromType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CdEscrowFirstYearAnalysCdEscrowFirstYearAnalysisStartFromTypeisStartFrom element has been assigned a value.</value>
        [XmlIgnore]
        public bool CdEscrowFirstYearAnalysisStartFromTypeSpecified
        {
            get { return this.CdEscrowFirstYearAnalysisStartFromType != null; }
            set { }
        }
    }
}
