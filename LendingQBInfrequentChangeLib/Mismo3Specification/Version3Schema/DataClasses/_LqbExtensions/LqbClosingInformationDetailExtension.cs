namespace Mismo3Specification.Version3Schema
{
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the EstimatedPrepaidDaysCount element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the EstimatedPrepaidDaysCount element has a value to serialize.</value>
    public class LQB_CLOSING_INFORMATION_DETAIL_EXTENSION
    {
        /// <summary>
        /// Gets a value indicating whether the LQB_ESCROW_ITEM_DETAIL_EXTENSION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.EstimatedPrepaidDaysCountSpecified;
            }
        }

        /// <summary>
        /// The number of days for which per-diem interest is paid.
        /// </summary>
        [XmlElement("EstimatedPrepaidDaysCount", Order = 0, Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOString EstimatedPrepaidDaysCount;

        /// <summary>
        /// Gets or sets a value indicating whether the EstimatedPrepaidDaysCount element has a value to serialize.
        /// </summary>
        [XmlIgnore]
        public bool EstimatedPrepaidDaysCountSpecified
        {
            get { return this.EstimatedPrepaidDaysCount != null; }
        }

        /// <summary>
        /// The number of additional interest days requiring consent.
        /// </summary>
        [XmlElement("AdditionalInterestDaysRequiringConsentCount", Order = 1, Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOCount AdditionalInterestDaysRequiringConsentCount;

        /// <summary>
        /// Gets or sets a value indicating whether the AdditionalInterestDaysRequiringConsentCount element has a value to serialize.
        /// </summary>
        [XmlIgnore]
        public bool AdditionalInterestDaysRequiringConsentCountSpecified
        {
            get { return this.AdditionalInterestDaysRequiringConsentCount != null; }
        }
    }
}
