﻿namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// LQB extension for the CLOSING_COST_FUND container.
    /// </summary>
    public class LQB_CLOSING_COST_FUND_EXTENSION
    {
        /// <summary>
        /// Gets a value indicating whether the element should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExcludeFromLECDForThisLienSpecified;
            }
        }

        /// <summary>
        /// Indicates whether the closing cost fund should be excluded from the LE/CD.
        /// </summary>
        [XmlElement("ExcludeFromLECDForThisLien", Order = 0, Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOIndicator ExcludeFromLECDForThisLien;

        /// <summary>
        /// Gets or sets a value indicating whether the ExcludeFromLECDForThisLien element has a value to serialize.
        /// </summary>
        [XmlIgnore]
        public bool ExcludeFromLECDForThisLienSpecified
        {
            get
            {
                return this.ExcludeFromLECDForThisLien != null;
            }
        }
    }
}
