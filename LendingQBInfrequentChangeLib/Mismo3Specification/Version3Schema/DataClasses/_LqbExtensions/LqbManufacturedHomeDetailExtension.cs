namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the CertificateOfTitleType element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the CertificateOfTitleType element has been assigned a value.</value>
    public class LQB_MANUFACTURED_HOME_DETAIL_EXTENSION
    {
        /// <summary>
        /// Gets a value indicating whether the LQB_MANUFACTURED_HOME_DETAIL_EXTENSION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CertificateOfTitleTypeSpecified;
            }
        }

        /// <summary>
        /// An enumeration indicating the certificate of title type for a manufactured home.
        /// </summary>
        [XmlElement("CertificateOfTitleType")]
        public MISMOEnum<CertificateOfTitleBase> CertificateOfTitleType;

        /// <summary>
        /// Gets or sets a value indicating whether the CertificateOfTitleType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CertificateOfTitleType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CertificateOfTitleTypeSpecified
        {
            get { return this.CertificateOfTitleType != null; }
            set { }
        }
    }
}
