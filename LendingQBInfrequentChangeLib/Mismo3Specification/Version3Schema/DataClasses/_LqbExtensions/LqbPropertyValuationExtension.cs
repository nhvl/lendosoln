﻿namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// An LQB-branded extension to the <see cref="PROPERTY_VALUATION"/> MISMO element.
    /// </summary>
    public class LQB_PROPERTY_VALUATION_EXTENSION
    {
        /// <summary>
        /// Gets a value indicating whether the <see cref="LqbPropertyValuationExtension"/> container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CuRiskScoreSpecified;
            }
        }

        /// <summary>
        /// The Collateral Underwriter (CU) risk score.
        /// </summary>
        [XmlElement("CURiskScore", Order = 0, Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOString CuRiskScore;

        /// <summary>
        /// Gets or sets a value indicating whether the CU risk score element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CU risk score element has been assigned a value.</value>
        [XmlIgnore]
        public bool CuRiskScoreSpecified
        {
            get { return this.CuRiskScore != null; }
            set { }
        }
    }
}
