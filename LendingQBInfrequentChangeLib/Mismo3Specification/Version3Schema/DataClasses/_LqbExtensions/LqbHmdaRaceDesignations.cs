﻿namespace Mismo3Specification.Version3Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public partial class LQB_HMDA_RACE_DESIGNATIONS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.HMDARaceDesignationSpecified;
            }
        }

        [XmlElement("HMDA_RACE_DESIGNATION", Order = 0, Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public List<LQB_HMDA_RACE_DESIGNATION> HMDARaceDesignation = new List<LQB_HMDA_RACE_DESIGNATION>();

        [XmlIgnore]
        public bool HMDARaceDesignationSpecified
        {
            get { return this.HMDARaceDesignation != null && this.HMDARaceDesignation.Any(hmda => hmda.ShouldSerialize); }
            set { }
        }
    }
}
