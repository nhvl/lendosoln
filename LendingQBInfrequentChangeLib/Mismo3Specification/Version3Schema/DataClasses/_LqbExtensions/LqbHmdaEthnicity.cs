﻿namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    public partial class LQB_HMDA_ETHNICITY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get { return this.HMDAEthnicityTypeSpecified; }
        }

        [XmlElement(nameof(HMDAEthnicityType), Order = 0, Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<HMDAEthnicityBase> HMDAEthnicityType;

        [XmlIgnore]
        public bool HMDAEthnicityTypeSpecified
        {
            get { return this.HMDAEthnicityType != null; }
            set { }
        }
    }
}
