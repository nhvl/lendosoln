namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public class LQB_CHANGE_OF_CIRCUMSTANCE_OCCURRENCE
    {
        /// <summary>
        /// Gets a value indicating whether the LQB_CHANGE_OF_CIRCUMSTANCE_OCCURRENCE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AssociatedFeeChangesSpecified
                    || this.AssociatedLoanValueChangesSpecified
                    || this.ChangeOfCircumstanceOccurrenceDetailSpecified;
            }
        }

        /// <summary>
        /// Fees associated with a change of circumstance.
        /// </summary>
        [XmlElement("ASSOCIATED_FEE_CHANGES", Order = 0, Namespace = "http://www.lendingqb.com")]
        public LQB_ASSOCIATED_FEE_CHANGES AssociatedFeeChanges;

        /// <summary>
        /// Gets or sets a value indicating whether the AssociatedFeeChanges element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AssociatedFeeChanges element has been assigned a value.</value>
        [XmlIgnore]
        public bool AssociatedFeeChangesSpecified
        {
            get { return this.AssociatedFeeChanges != null && this.AssociatedFeeChanges.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Loan values associated with a change of circumstance.
        /// </summary>
        [XmlElement("ASSOCIATED_LOAN_VALUE_CHANGES", Order = 1, Namespace = "http://www.lendingqb.com")]
        public LQB_ASSOCIATED_LOAN_VALUE_CHANGES AssociatedLoanValueChanges;

        /// <summary>
        /// Gets or sets a value indicating whether the AssociatedLoanValueChanges element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AssociatedLoanValueChanges element has been assigned a value.</value>
        [XmlIgnore]
        public bool AssociatedLoanValueChangesSpecified
        {
            get { return this.AssociatedLoanValueChanges != null && this.AssociatedLoanValueChanges.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Occurrences of changes of circumstance.
        /// </summary>
        [XmlElement("CHANGE_OF_CIRCUMSTANCE_OCCURRENCE_DETAIL", Order = 2, Namespace = "http://www.lendingqb.com")]
        public LQB_CHANGE_OF_CIRCUMSTANCE_OCCURRENCE_DETAIL ChangeOfCircumstanceOccurrenceDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the ChangeOfCircumstanceOccurrenceDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ChangeOfCircumstanceOccurrenceDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool ChangeOfCircumstanceOccurrenceDetailSpecified
        {
            get { return this.ChangeOfCircumstanceOccurrenceDetail != null && this.ChangeOfCircumstanceOccurrenceDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
