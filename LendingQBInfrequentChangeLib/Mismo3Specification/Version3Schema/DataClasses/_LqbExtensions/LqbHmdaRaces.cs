﻿namespace Mismo3Specification.Version3Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public partial class LQB_HMDA_RACES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.HMDARaceSpecified;
            }
        }

        [XmlElement("HMDA_RACE", Order = 0, Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public List<LQB_HMDA_RACE> HMDARace = new List<LQB_HMDA_RACE>();

        [XmlIgnore]
        public bool HMDARaceSpecified
        {
            get { return this.HMDARace != null && this.HMDARace.Any(hmda => hmda.ShouldSerialize); }
            set { }
        }
    }
}
