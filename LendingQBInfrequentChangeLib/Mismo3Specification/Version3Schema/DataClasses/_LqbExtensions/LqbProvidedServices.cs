namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the ProvidedService list contains any serializable data.
    /// </summary>
    public class LQB_PROVIDED_SERVICES
    {
        /// <summary>
        /// Indicates whether the LQB_PROVIDED_SERVICES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get { return ProvidedServiceSpecified; }
        }

        /// <summary>
        /// A collection of services offered by a service provider.
        /// </summary>
        [XmlElement("PROVIDED_SERVICE", Order = 0, Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public List<LQB_PROVIDED_SERVICE> ProvidedService = new List<LQB_PROVIDED_SERVICE>();

        /// <summary>
        /// Indicates whether the ProvidedService list contains any serializable data.
        /// </summary>
        [XmlIgnore]
        public bool ProvidedServiceSpecified
        {
            get { return this.ProvidedService != null && this.ProvidedService.Count(s => s != null && s.ShouldSerialize) > 0; }
            set { }
        }
    }
}
