namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the ClosingCostFeeTypeID element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the ClosingCostFeeTypeID element has been assigned a value.</value>
    public partial class LQB_ESCROW_ITEM_DETAIL_EXTENSION
    {
        /// <summary>
        /// Gets a value indicating whether the LQB_ESCROW_ITEM_DETAIL_EXTENSION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return  ClosingCostFeeTypeIDSpecified;
            }
        }

        /// <summary>
        /// The unique closing cost type ID of the fee.
        /// </summary>
        [XmlElement("ClosingCostFeeTypeID", Order = 0, Namespace = "http://www.lendingqb.com")]
        public MISMOString ClosingCostFeeTypeID;

        /// <summary>
        /// Gets or sets a value indicating whether the ClosingCostFeeTypeID element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ClosingCostFeeTypeID element has been assigned a value.</value>
        [XmlIgnore]
        public bool ClosingCostFeeTypeIDSpecified
        {
            get { return this.ClosingCostFeeTypeID != null; }
            set { }
        }
    }
}
