﻿namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;
    
    /// <summary>
    /// An extension holding supplementary data on a closing adjustment item.
    /// </summary>
    public class LQB_CLOSING_ADJUSTMENT_ITEM_DETAIL_EXTENSION
    {
        /// <summary>
        /// Gets a value indicating whether the element should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AdjustmentIsPrincipalReductionSpecified
                    || this.PrincipalReductionForToleranceCureSpecified
                    || this.ExcludeFromLECDForThisLienSpecified;
            }
        }

        /// <summary>
        /// Indicates whether the closing adjustment item is a principal reduction.
        /// </summary>
        [XmlElement("AdjustmentIsPrincipalReduction", Order = 0, Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOIndicator AdjustmentIsPrincipalReduction;

        /// <summary>
        /// Gets or sets a value indicating whether the AdjustmentIsPrincipalReduction element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AdjustmentIsPrincipalReduction element has a value to serialize.</value>
        [XmlIgnore]
        public bool AdjustmentIsPrincipalReductionSpecified
        {
            get
            {
                return this.AdjustmentIsPrincipalReduction != null;
            }
        }

        /// <summary>
        /// Indicates whether the closing adjustment item is a principal reduction for a tolerance cure.
        /// </summary>
        [XmlElement("PrincipalReductionForToleranceCure", Order = 1, Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOIndicator PrincipalReductionForToleranceCure;

        /// <summary>
        /// Gets or sets a value indicating whether the PrincipalReductionForToleranceCure element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrincipalReductionForToleranceCure element has a value to serialize.</value>
        [XmlIgnore]
        public bool PrincipalReductionForToleranceCureSpecified
        {
            get
            {
                return this.PrincipalReductionForToleranceCure != null;
            }
        }

        /// <summary>
        /// Indicates whether the closing adjustment should be excluded from the LE/CD.
        /// </summary>
        [XmlElement("ExcludeFromLECDForThisLien", Order = 2, Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOIndicator ExcludeFromLECDForThisLien;

        /// <summary>
        /// Gets or sets a value indicating whether the ExcludeFromLECDForThisLien element has a value to serialize.
        /// </summary>
        [XmlIgnore]
        public bool ExcludeFromLECDForThisLienSpecified
        {
            get { return this.ExcludeFromLECDForThisLien != null; }
        }
    }
}
