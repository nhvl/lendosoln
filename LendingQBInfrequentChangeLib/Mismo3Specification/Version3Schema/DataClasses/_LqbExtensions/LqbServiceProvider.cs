namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the ProvidedServicesSpecified datapoint has a serializable value.
    /// </summary>
    public class LQB_SERVICE_PROVIDER
    {
        /// <summary>
        /// Indicates whether the LQB_SERVICE_PROVIDER container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get { return ProvidedServicesSpecified; }
        }

        /// <summary>
        /// A set of services offered by a service provider.
        /// </summary>
        [XmlElement("PROVIDED_SERVICES", Order = 0, Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public LQB_PROVIDED_SERVICES ProvidedServices;

        /// <summary>
        /// Indicates whether the ProvidedServicesSpecified datapoint has a serializable value.
        /// </summary>
        [XmlIgnore]
        public bool ProvidedServicesSpecified
        {
            get { return ProvidedServices != null; }
            set { }
        }
    }
}
