﻿namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    public partial class LQB_HMDA_RACE_DESIGNATION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.HMDARaceDesignationTypeSpecified
                    || this.HMDARaceDesignationTypeOtherAsianDescriptionSpecified
                    || this.HMDARaceDesignationTypeOtherPacificIslanderDescriptionSpecified;
            }
        }

        [XmlElement(nameof(HMDARaceDesignationType), Order = 0, Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<HMDARaceDesignationBase> HMDARaceDesignationType;

        [XmlIgnore]
        public bool HMDARaceDesignationTypeSpecified
        {
            get { return this.HMDARaceDesignationType != null; }
            set { }
        }

        [XmlElement(nameof(HMDARaceDesignationTypeOtherAsianDescription), Order = 1, Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOString HMDARaceDesignationTypeOtherAsianDescription;

        [XmlIgnore]
        public bool HMDARaceDesignationTypeOtherAsianDescriptionSpecified
        {
            get { return this.HMDARaceDesignationTypeOtherAsianDescription != null; }
            set { }
        }

        [XmlElement(nameof(HMDARaceDesignationTypeOtherPacificIslanderDescription), Order = 2, Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOString HMDARaceDesignationTypeOtherPacificIslanderDescription;

        [XmlIgnore]
        public bool HMDARaceDesignationTypeOtherPacificIslanderDescriptionSpecified
        {
            get { return this.HMDARaceDesignationTypeOtherPacificIslanderDescription != null; }
            set { }
        }
    }
}
