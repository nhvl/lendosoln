﻿namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    public partial class LQB_GOVERNMENT_MONITORING_EXTENSION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.GenderTypeSpecified
                    || this.HMDAGenderRefusalIndicatorSpecified
                    || this.HMDAEthnicitiesSpecified
                    || this.HMDAEthnicityOriginsSpecified
                    || this.HMDAEthnicityRefusalIndicatorSpecified
                    || this.HMDARacesSpecified
                    || this.HMDARaceRefusalIndicatorSpecified
                    || this.HMDAEthnicityCollectedBasedOnVisualObservationOrSurnameIndicatorSpecified
                    || this.HMDAGenderCollectedBasedOnVisualObservationOrNameIndicatorSpecified
                    || this.HMDARaceCollectedBasedOnVisualObservationOrSurnameIndicatorSpecified
                    || this.ApplicationTakenMethodTypeSpecified
                    || this.DecisionCreditScoreSpecified;
                }
        }

        [XmlElement(nameof(GenderType), Order = 0, Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<HMDAGenderBase> GenderType;

        [XmlIgnore]
        public bool GenderTypeSpecified
        {
            get { return this.GenderType != null; }
            set { }
        }

        [XmlElement(nameof(HMDAGenderRefusalIndicator), Order = 1, Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOIndicator HMDAGenderRefusalIndicator;

        [XmlIgnore]
        public bool HMDAGenderRefusalIndicatorSpecified
        {
            get { return this.HMDAGenderRefusalIndicator != null; }
            set { }
        }

        [XmlElement("HMDA_ETHNICITIES", Order = 2, Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public LQB_HMDA_ETHNICITIES HMDAEthnicities;

        [XmlIgnore]
        public bool HMDAEthnicitiesSpecified
        {
            get { return this.HMDAEthnicities != null && this.HMDAEthnicities.ShouldSerialize; }
            set { }
        }

        [XmlElement("HMDA_ETHNICITY_ORIGINS", Order = 3, Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public LQB_HMDA_ETHNICITY_ORIGINS HMDAEthnicityOrigins;

        [XmlIgnore]
        public bool HMDAEthnicityOriginsSpecified
        {
            get { return this.HMDAEthnicityOrigins != null; }
            set { }
        }

        [XmlElement(nameof(HMDAEthnicityRefusalIndicator), Order = 4, Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOIndicator HMDAEthnicityRefusalIndicator;

        [XmlIgnore]
        public bool HMDAEthnicityRefusalIndicatorSpecified
        {
            get { return this.HMDAEthnicityRefusalIndicator != null; }
            set { }
        }

        [XmlElement("HMDA_RACES", Order = 5, Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public LQB_HMDA_RACES HMDARaces;

        [XmlIgnore]
        public bool HMDARacesSpecified
        {
            get { return this.HMDARaces != null; }
            set { }
        }

        [XmlElement(nameof(HMDARaceRefusalIndicator), Order = 6, Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOIndicator HMDARaceRefusalIndicator;

        [XmlIgnore]
        public bool HMDARaceRefusalIndicatorSpecified
        {
            get { return this.HMDARaceRefusalIndicator != null; }
            set { }
        }

        [XmlElement(nameof(HMDAEthnicityCollectedBasedOnVisualObservationOrSurnameIndicator), Order = 7, Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOIndicator HMDAEthnicityCollectedBasedOnVisualObservationOrSurnameIndicator;

        [XmlIgnore]
        public bool HMDAEthnicityCollectedBasedOnVisualObservationOrSurnameIndicatorSpecified
        {
            get { return this.HMDAEthnicityCollectedBasedOnVisualObservationOrSurnameIndicator != null; }
            set { }
        }

        [XmlElement(nameof(HMDAGenderCollectedBasedOnVisualObservationOrNameIndicator), Order = 8, Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOIndicator HMDAGenderCollectedBasedOnVisualObservationOrNameIndicator;

        [XmlIgnore]
        public bool HMDAGenderCollectedBasedOnVisualObservationOrNameIndicatorSpecified
        {
            get { return this.HMDAGenderCollectedBasedOnVisualObservationOrNameIndicator != null; }
            set { }
        }

        [XmlElement(nameof(HMDARaceCollectedBasedOnVisualObservationOrSurnameIndicator), Order = 9, Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOIndicator HMDARaceCollectedBasedOnVisualObservationOrSurnameIndicator;

        [XmlIgnore]
        public bool HMDARaceCollectedBasedOnVisualObservationOrSurnameIndicatorSpecified
        {
            get { return this.HMDARaceCollectedBasedOnVisualObservationOrSurnameIndicator != null; }
            set { }
        }

        [XmlElement(nameof(ApplicationTakenMethodType), Order = 10, Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<ApplicationTakenMethodBase> ApplicationTakenMethodType;

        [XmlIgnore]
        public bool ApplicationTakenMethodTypeSpecified
        {
            get { return this.ApplicationTakenMethodType != null; }
            set { }
        }

        /// <summary>
        /// Contains the borrower's decision credit score.
        /// </summary>
        [XmlElement("DECISION_CREDIT_SCORE", Order = 11, Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public LQB_DECISION_CREDIT_SCORE DecisionCreditScore;

        /// <summary>
        /// Indicates whether the <see cref="DecisionCreditScore"/> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="DecisionCreditScore"/> element has a value to serialize.</value>
        [XmlIgnore]
        public bool DecisionCreditScoreSpecified
        {
            get { return this.DecisionCreditScore != null; }
            set { }
        }
    }
}
