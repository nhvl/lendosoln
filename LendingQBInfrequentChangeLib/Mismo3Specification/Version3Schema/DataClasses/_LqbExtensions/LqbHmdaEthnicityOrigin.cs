﻿namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    public partial class LQB_HMDA_ETHNICITY_ORIGIN
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.HMDAEthnicityOriginTypeSpecified
                    || this.HMDAEthnicityOriginTypeOtherDescriptionSpecified;
            }
        }

        [XmlElement(nameof(HMDAEthnicityOriginType), Order = 0, Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<HMDAEthnicityOriginBase> HMDAEthnicityOriginType;

        [XmlIgnore]
        public bool HMDAEthnicityOriginTypeSpecified
        {
            get { return this.HMDAEthnicityOriginType != null; }
            set { }
        }

        [XmlElement(nameof(HMDAEthnicityOriginTypeOtherDescription), Order = 1, Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOString HMDAEthnicityOriginTypeOtherDescription;

        [XmlIgnore]
        public bool HMDAEthnicityOriginTypeOtherDescriptionSpecified
        {
            get { return this.HMDAEthnicityOriginTypeOtherDescription != null; }
            set { }
        }
    }
}
