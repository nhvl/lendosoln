namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the ChangeOfCircumstanceOccurrences element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the ChangeOfCircumstanceOccurrences element has been assigned a value.</value>
    public class LQB_LOAN_EXTENSION
    {
        /// <summary>
        /// Gets a value indicating whether the LQB_LOAN_EXTENSION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ChangeOfCircumstanceOccurrencesSpecified;
            }
        }

        /// <summary>
        /// Occurrences of changes of circumstance.
        /// </summary>
        [XmlElement("CHANGE_OF_CIRCUMSTANCE_OCCURRENCES", Order = 0, Namespace = "http://www.lendingqb.com")]
        public LQB_CHANGE_OF_CIRCUMSTANCE_OCCURRENCES ChangeOfCircumstanceOccurrences;

        /// <summary>
        /// Gets or sets a value indicating whether the ChangeOfCircumstanceOccurrences element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ChangeOfCircumstanceOccurrences element has been assigned a value.</value>
        [XmlIgnore]
        public bool ChangeOfCircumstanceOccurrencesSpecified
        {
            get { return this.ChangeOfCircumstanceOccurrences != null && this.ChangeOfCircumstanceOccurrences.ShouldSerialize; }
            set { }
        }
    }
}
