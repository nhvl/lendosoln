namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// A container for details about the collection of cash to close items.
    /// </summary>
    public partial class LQB_CASH_TO_CLOSE_ITEMS_EXTENSION
    {
        /// <summary>
        /// Gets a value indicating whether the LQB_CASH_TO_CLOSE_ITEMS_EXTENSION container should be serialized.
        /// </summary>
        public bool ShouldSerialize
        {
            get { return LqbCashToCloseDetail != null && LqbCashToCloseDetail.ShouldSerialize; }
        }

        /// <summary>
        /// A container for details about the collection of cash to close items.
        /// </summary>
        [XmlElement("CASH_TO_CLOSE_DETAIL", Order = 0, Namespace = "http://www.lendingqb.com")]
        public LQB_CASH_TO_CLOSE_DETAIL LqbCashToCloseDetail;
    }
}
