namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets a value indicating whether MannerInWhichTitleHeld has a value set.
    /// </summary>
    public class LQB_URLA_DETAIL_EXTENSION
    {
        /// <summary>
        /// Gets a value indicaiting whether the LQB_URLA_DETAIL_EXTENSION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return MannerInWhichTitleHeldSpecified || LandIfAcquiredSeparatelyAmountSpecified;
            }
        }

        /// <summary>
        /// A string describing how the title will be held.
        /// </summary>
        [XmlElement("MannerInWhichTitleHeld", Order = 0, Namespace = "http://www.lendingqb.com")]
        public MISMOString MannerInWhichTitleHeld;

        /// <summary>
        /// Gets a value indicating whether MannerInWhichTitleHeld has a value set.
        /// </summary>
        [XmlIgnore]
        public bool MannerInWhichTitleHeldSpecified
        {
            get
            {
                return MannerInWhichTitleHeld != null;
            }
        }

        /// <summary>
        /// Land if acquired separately amount.
        /// </summary>
        [XmlElement("LandIfAcquiredSeparatelyAmount", Order = 1, Namespace = "http://www.lendingqb.com")]
        public MISMOAmount LandIfAcquiredSeparatelyAmount;

        /// <summary>
        /// Gets a value indicating whether LandIfAcquiredSeparatelyAmount is specified.
        /// </summary>
        [XmlIgnore]
        public bool LandIfAcquiredSeparatelyAmountSpecified
        {
            get
            {
                return LandIfAcquiredSeparatelyAmount != null;
            }
        }
    }
}
