﻿namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    public class LQB_BORROWER_DETAIL_EXTENSION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.FinancialInstitutionMemberIdentifierSpecified;
            }
        }

        [XmlElement("FinancialInstitutionMemberIdentifier", Order = 0, Namespace = "http://www.lendingqb.com")]
        public MISMOIdentifier FinancialInstitutionMemberIdentifier;

        [XmlIgnore]
        public bool FinancialInstitutionMemberIdentifierSpecified
        {
            get
            {
                return this.FinancialInstitutionMemberIdentifier != null;
            }
        }
    }
}
