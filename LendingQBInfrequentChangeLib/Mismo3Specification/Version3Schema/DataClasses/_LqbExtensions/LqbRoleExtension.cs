namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the ServiceProvider datapoint has a serializable value.
    /// </summary>
    public class LQB_ROLE_EXTENSION
    {
        /// <summary>
        /// Indicates whether the LQB_ROLE_EXTENSION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get { return ServiceProviderSpecified; }
        }

        /// <summary>
        /// Holds information on a service provider.
        /// </summary>
        [XmlElement("SERVICE_PROVIDER", Order = 0, Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public LQB_SERVICE_PROVIDER ServiceProvider;

        /// <summary>
        /// Indicates whether the ServiceProvider datapoint has a serializable value.
        /// </summary>
        [XmlIgnore]
        public bool ServiceProviderSpecified
        {
            get { return ServiceProvider != null; }
            set { }
        }
    }
}
