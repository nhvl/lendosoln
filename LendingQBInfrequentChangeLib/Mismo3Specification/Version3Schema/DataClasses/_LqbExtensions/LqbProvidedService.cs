namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether ProvidedServiceEstimatedCostAmount has a serializable value.
    /// </summary>
    public class LQB_PROVIDED_SERVICE
    {
        /// <summary>
        /// Indicates whether the LQB_PROVIDED_SERVICE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return ProvidedServiceCanShopForIndicatorSpecified
                    || ProvidedServiceDescriptionSpecified
                    || ProvidedServiceEstimatedCostAmountSpecified;
            }
        }

        /// <summary>
        /// Indicates whether the service can be shopped for.
        /// </summary>
        [XmlElement("ProvidedServiceCanShopForIndicator", Order = 0, Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOIndicator ProvidedServiceCanShopForIndicator;

        /// <summary>
        /// Indicates whether ProvidedServiceCanShopForIndicator has a serializable value.
        /// </summary>
        [XmlIgnore]
        public bool ProvidedServiceCanShopForIndicatorSpecified
        {
            get { return ProvidedServiceCanShopForIndicator != null; }
            set { }
        }

        /// <summary>
        /// A description of the provided service.
        /// </summary>
        [XmlElement("ProvidedServiceDescription", Order = 1, Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOString ProvidedServiceDescription;

        /// <summary>
        /// Indicates whether ProvidedServiceDescription has a serializable value.
        /// </summary>
        [XmlIgnore]
        public bool ProvidedServiceDescriptionSpecified
        {
            get { return ProvidedServiceDescription != null; }
            set { }
        }

        /// <summary>
        /// The cost amount for the provided service.
        /// </summary>
        [XmlElement("ProvidedServiceEstimatedCostAmount", Order = 2, Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOAmount ProvidedServiceEstimatedCostAmount;

        /// <summary>
        /// Indicates whether ProvidedServiceEstimatedCostAmount has a serializable value.
        /// </summary>
        [XmlIgnore]
        public bool ProvidedServiceEstimatedCostAmountSpecified
        {
            get { return ProvidedServiceEstimatedCostAmount != null; }
            set { }
        }
    }
}
