namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the PrepaidItemPerDiemUnroundedAmount element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the PrepaidItemPerDiemUnroundedAmount element has been assigned a value.</value>
    public partial class LQB_PREPAID_ITEM_DETAIL_EXTENSION
    {
        /// <summary>
        /// Gets a value indicating whether the LQB_PREPAID_ITEM_DETAIL_EXTENSION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.PrepaidItemSpecifiedHUD1LineNumberValueSpecified
                    || this.ClosingCostFeeTypeIDSpecified
                    || this.PrepaidItemPerDiemUnroundedAmountSpecified;
            }
        }

        /// <summary>
        /// The corresponding HUD line number for this prepaid item.
        /// </summary>
        [XmlElement("PrepaidItemSpecifiedHUD1LineNumberValue", Order = 0, Namespace = "http://www.lendingqb.com")]
        public MISMOValue PrepaidItemSpecifiedHUD1LineNumberValue;

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaidItemSpecifiedHUD1LineNumberValue has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaidItemSpecifiedHUD1LineNumberValue has been assigned a value.</value>
        public bool PrepaidItemSpecifiedHUD1LineNumberValueSpecified
        {
            get
            {
                return this.PrepaidItemSpecifiedHUD1LineNumberValue != null;
            }
        }

        /// <summary>
        /// The unique closing cost type ID of the fee.
        /// </summary>
        [XmlElement("ClosingCostFeeTypeID", Order = 1, Namespace = "http://www.lendingqb.com")]
        public MISMOString ClosingCostFeeTypeID;

        /// <summary>
        /// Gets or sets a value indicating whether the ClosingCostFeeTypeID element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ClosingCostFeeTypeID element has been assigned a value.</value>
        [XmlIgnore]
        public bool ClosingCostFeeTypeIDSpecified
        {
            get { return this.ClosingCostFeeTypeID != null; }
            set { }
        }

        /// <summary>
        /// The unrounded per diem amount. Some vendors want the full unrounded amount to six digits
        /// of precision, but a <see cref="MISMOAmount"/> can only have two-digit precision, so we need
        /// to send this as an extension.
        /// </summary>
        [XmlElement(Order = 2, Namespace = "http://www.lendingqb.com")]
        public MISMONumeric PrepaidItemPerDiemUnroundedAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaidItemPerDiemUnroundedAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaidItemPerDiemUnroundedAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaidItemPerDiemUnroundedAmountSpecified
        {
            get { return this.PrepaidItemPerDiemUnroundedAmount != null; }
            set { }
        }
    }
}
