namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the RedisclosureDate element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the RedisclosureDate element has a value to serialize.</value>
    public class LQB_CHANGE_OF_CIRCUMSTANCE_OCCURRENCE_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the LQB_CHANGE_OF_CIRCUMSTANCE_OCCURRENCE_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ChangeExplanationTextSpecified
                    || this.CircumstanceChangeDateSpecified
                    || this.COCArchivedDatetimeSpecified
                    || this.ComplianceCOCCategorySpecified
                    || this.RedisclosureDateSpecified;
            }
        }

        /// <summary>
        /// An explanation for the change of circumstance.
        /// </summary>
        [XmlElement("ChangeExplanationText", Order = 0, Namespace = "http://www.lendingqb.com")]
        public MISMOString ChangeExplanationText;

        /// <summary>
        /// Gets or sets a value indicating whether the ChangeExplanationText element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ChangeExplanationText element has been assigned a value.</value>
        [XmlIgnore]
        public bool ChangeExplanationTextSpecified
        {
            get { return ChangeExplanationText != null; }
            set { }
        }

        /// <summary>
        /// The date of the change of circumstance.
        /// </summary>
        [XmlElement("CircumstanceChangeDate", Order = 1, Namespace = "http://www.lendingqb.com")]
        public MISMODate CircumstanceChangeDate;

        /// <summary>
        /// Gets or sets a value indicating whether the CircumstanceChangeDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CircumstanceChangeDate element has a value to serialize.</value>
        [XmlIgnore]
        public bool CircumstanceChangeDateSpecified
        {
            get
            {
                return this.CircumstanceChangeDate != null;
            }
        }

        /// <summary>
        /// The date of the change of circumstance.
        /// </summary>
        [XmlElement("COCArchivedDatetime", Order = 2, Namespace = "http://www.lendingqb.com")]
        public MISMODatetime COCArchivedDatetime;

        /// <summary>
        /// Gets or sets a value indicating whether the COCArchivedDatetime element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the COCArchivedDatetime element has a value to serialize.</value>
        [XmlIgnore]
        public bool COCArchivedDatetimeSpecified
        {
            get
            {
                return this.COCArchivedDatetime != null;
            }
        }

        /// <summary>
        /// The category of the change of circumstance. For use with compliance vendors.
        /// </summary>
        [XmlElement("ComplianceCOCCategory", Order = 3, Namespace = "http://www.lendingqb.com")]
        public MISMOString ComplianceCOCCategory;

        /// <summary>
        /// Gets or sets a value indicating whether the ComplianceCOCCategory element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ComplianceCOCCategory element has been assigned a value.</value>
        [XmlIgnore]
        public bool ComplianceCOCCategorySpecified
        {
            get { return ComplianceCOCCategory != null; }
            set { }
        }

        /// <summary>
        /// The date of the re-disclosure.
        /// </summary>
        [XmlElement("RedisclosureDate", Order = 4, Namespace = "http://www.lendingqb.com")]
        public MISMODate RedisclosureDate;

        /// <summary>
        /// Gets or sets a value indicating whether the RedisclosureDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RedisclosureDate element has a value to serialize.</value>
        [XmlIgnore]
        public bool RedisclosureDateSpecified
        {
            get
            {
                return this.RedisclosureDate != null;
            }
        }
    }
}
