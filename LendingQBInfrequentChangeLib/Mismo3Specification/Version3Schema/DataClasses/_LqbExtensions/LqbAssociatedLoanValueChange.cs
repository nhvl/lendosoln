namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public class LQB_ASSOCIATED_LOAN_VALUE_CHANGE
    {
        /// <summary>
        /// Gets a value indicating whether the LQB_ASSOCIATED_LOAN_VALUE_CHANGE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.FieldDescriptionSpecified
                    || this.FieldNameSpecified
                    || this.ValueSpecified;
            }
        }

        /// <summary>
        /// A description of the field.
        /// </summary>
        [XmlElement("FieldDescription", Order = 0, Namespace = "http://www.lendingqb.com")]
        public MISMOString FieldDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the FieldDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FieldDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool FieldDescriptionSpecified
        {
            get { return FieldDescription != null; }
            set { }
        }

        /// <summary>
        /// The unique identifier for field.
        /// </summary>
        [XmlElement("FieldName", Order = 1, Namespace = "http://www.lendingqb.com")]
        public MISMOString FieldName;

        /// <summary>
        /// Gets or sets a value indicating whether the FieldName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FieldName element has been assigned a value.</value>
        [XmlIgnore]
        public bool FieldNameSpecified
        {
            get { return FieldName != null; }
            set { }
        }

        /// <summary>
        /// The decimal value of the field.
        /// </summary>
        [XmlElement("Value", Order = 2, Namespace = "http://www.lendingqb.com")]
        public MISMONumeric Value;

        /// <summary>
        /// Gets or sets a value indicating whether the Value element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Value element has been assigned a value.</value>
        [XmlIgnore]
        public bool ValueSpecified
        {
            get { return Value != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
