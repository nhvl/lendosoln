namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Ticket element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Ticket element has been assigned a value.</value>
    public class LQB_MESSAGE_EXTENSION
    {
        /// <summary>
        /// A container for the generic framework authentication ticket. The namespace reference ensures that the $$lendingqb$$ prefix will be applied to the element name on serialization. 
        /// </summary>
        [XmlElement("AuthenticationTicket", Order = 0, Namespace = "http://www.lendingqb.com")]
        public AuthenticationTicket Ticket;

        /// <summary>
        /// Gets or sets a value indicating whether the Ticket element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Ticket element has been assigned a value.</value>
        [XmlIgnore]
        public bool TicketSpecified
        {
            get { return this.Ticket != null; }
            set { }
        }

        /// <summary>
        /// A container for the generic framework authentication ticket's webservice domain. The namespace reference ensures that the $$lendingqb$$ prefix will be applied to the element name on serialization. 
        /// </summary>
        [XmlElement("TicketServiceDomain", Order = 1, Namespace = "http://www.lendingqb.com")]
        public MISMOURL TicketServiceDomain;

        /// <summary>
        /// Gets or sets a value indicating whether the TicketServiceDomain element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TicketTicketServiceDomain element has been assigned a value.</value>
        [XmlIgnore]
        public bool TicketServiceDomainSpecified
        {
            get { return this.TicketServiceDomain != null; }
            set { }
        }
    }
}
