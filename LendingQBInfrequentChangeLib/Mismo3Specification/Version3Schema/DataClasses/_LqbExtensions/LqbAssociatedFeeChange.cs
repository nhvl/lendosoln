namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public class LQB_ASSOCIATED_FEE_CHANGE
    {
        /// <summary>
        /// Gets a value indicating whether the LQB_CHANGE_OF_CIRCUMSTANCE_OCCURRENCE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.FeeAmountSpecified
                    || this.FeeDescriptionSpecified
                    || this.FeeIdentifierSpecified;
            }
        }

        /// <summary>
        /// The dollar amount of the fee.
        /// </summary>
        [XmlElement("FeeAmount", Order = 0, Namespace = "http://www.lendingqb.com")]
        public MISMOAmount FeeAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the FeeAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeeAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeeAmountSpecified
        {
            get { return FeeAmount != null; }
            set { }
        }

        /// <summary>
        /// A description of the fee.
        /// </summary>
        [XmlElement("FeeDescription", Order = 1, Namespace = "http://www.lendingqb.com")]
        public MISMOString FeeDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the FeeDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeeDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeeDescriptionSpecified
        {
            get { return FeeDescription != null; }
            set { }
        }

        /// <summary>
        /// The unique identifier for fee.
        /// </summary>
        [XmlElement("FeeIdentifier", Order = 2, Namespace = "http://www.lendingqb.com")]
        public MISMOIdentifier FeeIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the FeeIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeeIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeeIdentifierSpecified
        {
            get { return FeeIdentifier != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
