﻿namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    public partial class LQB_HMDA_RACE_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.HMDARaceTypeSpecified
                    || this.HMDARaceTypeAdditionalDescriptionSpecified;
            }
        }

        [XmlElement(nameof(HMDARaceType), Order = 0, Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<HMDARaceBase> HMDARaceType;

        [XmlIgnore]
        public bool HMDARaceTypeSpecified
        {
            get { return this.HMDARaceType != null; }
            set { }
        }

        [XmlElement(nameof(HMDARaceTypeAdditionalDescription), Order = 1, Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOString HMDARaceTypeAdditionalDescription;

        [XmlIgnore]
        public bool HMDARaceTypeAdditionalDescriptionSpecified
        {
            get { return this.HMDARaceTypeAdditionalDescription != null; }
            set { }
        }
    }
}
