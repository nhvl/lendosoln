﻿namespace Mismo3Specification.Version3Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A proprietary extension to the FEE_SUMMARY_DETAIL container.
    /// </summary>
    public class LQB_FEE_SUMMARY_DETAIL_EXTENSION
    {
        /// <summary>
        /// Gets a value indicating whether the container has data to serialize.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get { return this.LenderCreditCanOffsetAprIndicatorSpecified; }
        }

        /// <summary>
        /// Indicates whether the lender credit can offset the APR indicator.
        /// </summary>
        [XmlElement("LenderCreditCanOffsetAPRIndicator")]
        public MISMOIndicator LenderCreditCanOffsetAprIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="LenderCreditCanOffsetAprIndicator"/>
        /// has a value to serialize.
        /// </summary>
        [XmlIgnore]
        public bool LenderCreditCanOffsetAprIndicatorSpecified
        {
            get { return this.LenderCreditCanOffsetAprIndicator != null; }
            set { }
        }
    }
}
