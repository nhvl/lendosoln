namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the AssociatedFeeChange element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the AssociatedFeeChange element has been assigned a value.</value>
    public class LQB_ASSOCIATED_FEE_CHANGES
    {
        /// <summary>
        /// Gets a value indicating whether the LQB_ASSOCIATED_FEE_CHANGES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AssociatedFeeChangeSpecified;
            }
        }

        /// <summary>
        /// A collection of associated fee changes.
        /// </summary>
        [XmlElement("ASSOCIATED_FEE_CHANGE", Order = 0, Namespace = "http://www.lendingqb.com")]
        public List<LQB_ASSOCIATED_FEE_CHANGE> AssociatedFeeChange = new List<LQB_ASSOCIATED_FEE_CHANGE>();

        /// <summary>
        /// Gets or sets a value indicating whether the AssociatedFeeChange element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AssociatedFeeChange element has been assigned a value.</value>
        [XmlIgnore]
        public bool AssociatedFeeChangeSpecified
        {
            get { return this.AssociatedFeeChange != null && this.AssociatedFeeChange.Count(c => c != null && c.ShouldSerialize) > 0; }
            set { }
        }
    }
}
