﻿namespace Mismo3Specification.Version3Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public partial class LQB_HMDA_ETHNICITY_ORIGINS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.HMDAEthnicityOriginSpecified;
            }
        }

        [XmlElement("HMDA_ETHNICITY_ORIGIN", Order = 0, Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public List<LQB_HMDA_ETHNICITY_ORIGIN> HMDAEthnicityOrigin = new List<LQB_HMDA_ETHNICITY_ORIGIN>();

        [XmlIgnore]
        public bool HMDAEthnicityOriginSpecified
        {
            get { return this.HMDAEthnicityOrigin != null && this.HMDAEthnicityOrigin.Any(hmda => hmda.ShouldSerialize); }
            set { }
        }
    }
}
