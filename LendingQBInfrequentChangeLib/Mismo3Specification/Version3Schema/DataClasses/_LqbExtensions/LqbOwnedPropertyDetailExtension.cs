namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the GsePropertyType element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the GsePropertyType element has a value to serialize.</value>
    public class LQB_OWNED_PROPERTY_DETAIL_EXTENSION
    {
        /// <summary>
        /// Gets a value indicating whether the LQB_ESCROW_ITEM_DETAIL_EXTENSION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.GsePropertyTypeSpecified;
            }
        }

        /// <summary>
        /// Indicates the property type according to the GSEs.
        /// </summary>
        [XmlElement("GsePropertyType", Order = 0, Namespace = "http://www.lendingqb.com")]
        public MISMOEnum<GsePropertyBase> GsePropertyType;

        /// <summary>
        /// Gets or sets a value indicating whether the GsePropertyType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GsePropertyType element has a value to serialize.</value>
        [XmlIgnore]
        public bool GsePropertyTypeSpecified
        {
            get
            {
                return GsePropertyType != null;
            }
        }
    }
}
