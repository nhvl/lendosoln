namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the AnnualAmount element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the AnnualAmount element has been assigned a value.</value>
    public partial class LQB_ESTIMATED_PROPERTY_COST_COMPONENT_EXTENSION
    {
        /// <summary>
        /// A container for description of the expense. The namespace reference ensures that the $$lendingqb$$ prefix will be applied to the element name on serialization.
        /// </summary>
        [XmlElement("ExpenseDescription", Order = 0, Namespace = "http://www.lendingqb.com")]
        public MISMOString ExpenseDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ExpenseDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ExpenseDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExpenseDescriptionSpecified
        {
            get { return this.ExpenseDescription != null; }
            set { }
        }

        /// <summary>
        /// A container for the annual payment amount of the expense. The namespace reference ensures that the $$lendingqb$$ prefix will be applied to the element name on serialization.
        /// </summary>
        [XmlElement("AnnualAmount", Order = 1, Namespace = "http://www.lendingqb.com")]
        public MISMOAmount AnnualAmount = null;

        /// <summary>
        /// Gets or sets a value indicating whether the AnnualAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AnnualAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool AnnualAmountSpecified
        {
            get { return this.AnnualAmount != null; }
            set { }
        }


    }
}
