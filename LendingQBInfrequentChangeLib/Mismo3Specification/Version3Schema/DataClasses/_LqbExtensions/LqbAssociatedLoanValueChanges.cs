namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the AssociatedLoanValueChange element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the AssociatedLoanValueChange element has been assigned a value.</value>
    public class LQB_ASSOCIATED_LOAN_VALUE_CHANGES
    {
        /// <summary>
        /// Gets a value indicating whether the LQB_ASSOCIATED_LOAN_VALUE_CHANGES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AssociatedLoanValueChangeSpecified;
            }
        }

        /// <summary>
        /// A collection of associated fee changes.
        /// </summary>
        [XmlElement("ASSOCIATED_LOAN_VALUE_CHANGE", Order = 0, Namespace = "http://www.lendingqb.com")]
        public List<LQB_ASSOCIATED_LOAN_VALUE_CHANGE> AssociatedLoanValueChange = new List<LQB_ASSOCIATED_LOAN_VALUE_CHANGE>();

        /// <summary>
        /// Gets or sets a value indicating whether the AssociatedLoanValueChange element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AssociatedLoanValueChange element has been assigned a value.</value>
        [XmlIgnore]
        public bool AssociatedLoanValueChangeSpecified
        {
            get { return this.AssociatedLoanValueChange != null && this.AssociatedLoanValueChange.Count(c => c != null && c.ShouldSerialize) > 0; }
            set { }
        }
    }
}
