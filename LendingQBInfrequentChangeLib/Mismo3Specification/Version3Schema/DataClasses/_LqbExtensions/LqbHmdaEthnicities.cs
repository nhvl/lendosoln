﻿namespace Mismo3Specification.Version3Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public partial class LQB_HMDA_ETHNICITIES
    {
        public bool ShouldSerialize
        {
            get { return this.HmdaEthnicitySpecified; }
        }

        [XmlElement("HMDA_ETHNICITY", Order = 0, Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public List<LQB_HMDA_ETHNICITY> HmdaEthnicity = new List<LQB_HMDA_ETHNICITY>();

        [XmlIgnore]
        public bool HmdaEthnicitySpecified
        {
            get { return this.HmdaEthnicity != null && this.HmdaEthnicity.Any(hmda => hmda.ShouldSerialize); }
            set { }
        }
    }
}
