namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class ORIGINATION_SYSTEM
    {
        /// <summary>
        /// Gets a value indicating whether the ORIGINATION_SYSTEM container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.LoanOriginationSystemLoanIdentifierSpecified
                    || this.LoanOriginationSystemNameSpecified
                    || this.LoanOriginationSystemVendorIdentifierSpecified
                    || this.LoanOriginationSystemVersionIdentifierSpecified;
            }
        }

        /// <summary>
        /// A control number (i.e. key) for the loan in the sending loan origination system.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIdentifier LoanOriginationSystemLoanIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanOriginationSystemLoanIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanOriginationSystemLoanIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanOriginationSystemLoanIdentifierSpecified
        {
            get { return LoanOriginationSystemLoanIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A descriptive name of the loan origination system as determined by the vendor. For example, EasyLender.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString LoanOriginationSystemName;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanOriginationSystemName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanOriginationSystemName element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanOriginationSystemNameSpecified
        {
            get { return LoanOriginationSystemName != null; }
            set { }
        }

        /// <summary>
        /// A unique identifier agreed upon by the parties to the transaction to identify the vendor of the loan origination system used to process the loan.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOIdentifier LoanOriginationSystemVendorIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanOriginationSystemVendorIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanOriginationSystemVendorIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanOriginationSystemVendorIdentifierSpecified
        {
            get { return LoanOriginationSystemVendorIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Specifies the version of the loan origination system used to process the loan.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOIdentifier LoanOriginationSystemVersionIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanOriginationSystemVersionIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanOriginationSystemVersionIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanOriginationSystemVersionIdentifierSpecified
        {
            get { return LoanOriginationSystemVersionIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public ORIGINATION_SYSTEM_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
