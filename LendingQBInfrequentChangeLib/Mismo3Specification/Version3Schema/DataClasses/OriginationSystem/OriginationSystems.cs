namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class ORIGINATION_SYSTEMS
    {
        /// <summary>
        /// Gets a value indicating whether the ORIGINATION_SYSTEMS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.OriginationSystemSpecified;
            }
        }

        /// <summary>
        /// A collection of origination systems.
        /// </summary>
        [XmlElement("ORIGINATION_SYSTEM", Order = 0)]
		public List<ORIGINATION_SYSTEM> OriginationSystem = new List<ORIGINATION_SYSTEM>();

        /// <summary>
        /// Gets or sets a value indicating whether the OriginationSystem element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OriginationSystem element has been assigned a value.</value>
        [XmlIgnore]
        public bool OriginationSystemSpecified
        {
            get { return this.OriginationSystem != null && this.OriginationSystem.Count(o => o != null && o.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public ORIGINATION_SYSTEMS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
