namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class VIEW_PAGES
    {
        /// <summary>
        /// Gets a value indicating whether the VIEW_PAGES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ViewPageSpecified;
            }
        }

        /// <summary>
        /// A collection of view pages.
        /// </summary>
        [XmlElement("VIEW_PAGE", Order = 0)]
		public List<VIEW_PAGE> ViewPage = new List<VIEW_PAGE>();

        /// <summary>
        /// Gets or sets a value indicating whether the ViewPage element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ViewPage element has been assigned a value.</value>
        [XmlIgnore]
        public bool ViewPageSpecified
        {
            get { return this.ViewPage != null && this.ViewPage.Count(v => v != null && v.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public VIEW_PAGES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
