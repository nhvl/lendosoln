namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class DEED_RESTRICTIONS
    {
        /// <summary>
        /// Gets a value indicating whether the DEED_RESTRICTIONS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DeedRestrictionSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of deed restrictions.
        /// </summary>
        [XmlElement("DEED_RESTRICTION", Order = 0)]
		public List<DEED_RESTRICTION> DeedRestriction = new List<DEED_RESTRICTION>();

        /// <summary>
        /// Gets or sets a value indicating whether the DeedRestriction element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DeedRestriction element has been assigned a value.</value>
        [XmlIgnore]
        public bool DeedRestrictionSpecified
        {
            get { return this.DeedRestriction != null && this.DeedRestriction.Count(d => d != null && d.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public DEED_RESTRICTIONS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
