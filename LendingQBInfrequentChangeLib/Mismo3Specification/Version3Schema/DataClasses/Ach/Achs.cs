namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether this element should be serialized.
    /// </summary>
    /// <returns>A boolean value indicating whether this element should be serialized.</returns>
    public class ACHS
    {
        /// <summary>
        /// A list of ACH elements within this container.
        /// </summary>
        [XmlElement("ACH")]
        public List<ACH> ACH = new List<ACH>();

        /// <summary>
        /// Indicates whether this element should be serialized.
        /// </summary>
        /// <returns>A boolean value indicating whether this element should be serialized.</returns>
        public bool ShouldSerialize()
        {
            return ACH.Exists(ach => ach.ShouldSerialize);
        }
    }
}
