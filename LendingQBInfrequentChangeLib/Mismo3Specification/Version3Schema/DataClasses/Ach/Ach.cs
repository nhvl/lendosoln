namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class ACH
    {
        /// <summary>
        /// Gets a value indicating whether the ACH container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ACH_ABARoutingAndTransitIdentifierSpecified
                    || this.ACHAccountTypeSpecified
                    || this.ACHAdditionalEscrowAmountSpecified
                    || this.ACHAdditionalPrincipalAmountSpecified
                    || this.ACHBankAccountIdentifierSpecified
                    || this.ACHBankAccountPurposeTypeOtherDescriptionSpecified
                    || this.ACHBankAccountPurposeTypeSpecified
                    || this.ACHDraftAfterDueDateDayCountSpecified
                    || this.ACHDraftFrequencyTypeSpecified
                    || this.ACHDraftingPartyNameSpecified
                    || this.ACHInstitutionTelegraphicAbbreviationNameSpecified
                    || this.ACHPendingDraftEffectiveDateSpecified
                    || this.ACHReceiverSubaccountNameSpecified
                    || this.ACHTypeSpecified
                    || this.ACHWireAmountSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// 9 digit routing and transit number for the bank from which the payment is drafted.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIdentifier ACH_ABARoutingAndTransitIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the ACH_ABARoutingAndTransitIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ACH_ABARoutingAndTransitIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool ACH_ABARoutingAndTransitIdentifierSpecified
        {
            get { return ACH_ABARoutingAndTransitIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The account type from which the payment is drafted.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<ACHAccountBase> ACHAccountType;

        /// <summary>
        /// Gets or sets a value indicating whether the ACHAccountType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ACHAccountType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ACHAccountTypeSpecified
        {
            get { return this.ACHAccountType != null && this.ACHAccountType.enumValue != ACHAccountBase.Blank; }
            set { }
        }

        /// <summary>
        /// Dollar amount beyond the normal PITI that is be drafted and applied to the escrow account.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOAmount ACHAdditionalEscrowAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ACHAdditionalEscrowAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ACHAdditionalEscrowAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ACHAdditionalEscrowAmountSpecified
        {
            get { return ACHAdditionalEscrowAmount != null; }
            set { }
        }

        /// <summary>
        /// Dollar amount beyond the normal PITI that is be drafted and applied to reduce the principal of the loan.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOAmount ACHAdditionalPrincipalAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ACHAdditionalPrincipalAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ACHAdditionalPrincipalAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ACHAdditionalPrincipalAmountSpecified
        {
            get { return ACHAdditionalPrincipalAmount != null; }
            set { }
        }

        /// <summary>
        /// The number identifying the account from which the payment is drafted.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOIdentifier ACHBankAccountIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the ACHBankAccountIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ACHBankAccountIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool ACHBankAccountIdentifierSpecified
        {
            get { return ACHBankAccountIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Identifies the purpose of the account where the payment or refund is drafted.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOEnum<ACHBankAccountPurposeBase> ACHBankAccountPurposeType;

        /// <summary>
        /// Gets or sets a value indicating whether the ACHBankAccountPurposeType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ACHBankAccountPurposeType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ACHBankAccountPurposeTypeSpecified
        {
            get { return this.ACHBankAccountPurposeType != null && this.ACHBankAccountPurposeType.enumValue != ACHBankAccountPurposeBase.Blank; }
            set { }
        }

        /// <summary>
        ///  A free-form text field used to collect additional information when Other is selected for ACH Bank Account Purpose Type.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOString ACHBankAccountPurposeTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ACHBankAccountPurposeTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ACHBankAccountPurposeTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ACHBankAccountPurposeTypeOtherDescriptionSpecified
        {
            get { return ACHBankAccountPurposeTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The number of days past the payment due date that payment drafting is scheduled to occur. Payment due date plus ACH Draft Days after Due Date equals draft date.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOCount ACHDraftAfterDueDateDayCount;

        /// <summary>
        /// Gets or sets a value indicating whether the ACHDraftAfterDueDateDayCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ACHDraftAfterDueDateDayCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ACHDraftAfterDueDateDayCountSpecified
        {
            get { return ACHDraftAfterDueDateDayCount != null; }
            set { }
        }

        /// <summary>
        /// Indicates how frequently the servicer drafts the scheduled mortgage payment from the account of the borrower .
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOEnum<ACHDraftFrequencyBase> ACHDraftFrequencyType;

        /// <summary>
        /// Gets or sets a value indicating whether the ACHDraftFrequencyType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ACHDraftFrequencyType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ACHDraftFrequencyTypeSpecified
        {
            get { return this.ACHDraftFrequencyType != null && this.ACHDraftFrequencyType.enumValue != ACHDraftFrequencyBase.Blank; }
            set { }
        }

        /// <summary>
        /// Identifies who is drafting the payment on the loan.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOString ACHDraftingPartyName;

        /// <summary>
        /// Gets or sets a value indicating whether the ACHDraftingPartyName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ACHDraftingPartyName element has been assigned a value.</value>
        [XmlIgnore]
        public bool ACHDraftingPartyNameSpecified
        {
            get { return ACHDraftingPartyName != null; }
            set { }
        }

        /// <summary>
        /// The abbreviated name of the depository institution assigned by the Federal Reserve.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOString ACHInstitutionTelegraphicAbbreviationName;

        /// <summary>
        /// Gets or sets a value indicating whether the ACHInstitutionTelegraphicAbbreviationName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ACHInstitutionTelegraphicAbbreviationName element has been assigned a value.</value>
        [XmlIgnore]
        public bool ACHInstitutionTelegraphicAbbreviationNameSpecified
        {
            get { return ACHInstitutionTelegraphicAbbreviationName != null; }
            set { }
        }

        /// <summary>
        /// Identifies the date on which automated drafting is scheduled to begin.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMODate ACHPendingDraftEffectiveDate;

        /// <summary>
        /// Gets or sets a value indicating whether the ACHPendingDraftEffectiveDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ACHPendingDraftEffectiveDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ACHPendingDraftEffectiveDateSpecified
        {
            get { return ACHPendingDraftEffectiveDate != null; }
            set { }
        }

        /// <summary>
        /// The subaccount name for the receiver of the wire.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOString ACHReceiverSubaccountName;

        /// <summary>
        /// Gets or sets a value indicating whether the ACHReceiverSubaccountName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ACHReceiverSubaccountName element has been assigned a value.</value>
        [XmlIgnore]
        public bool ACHReceiverSubaccountNameSpecified
        {
            get { return ACHReceiverSubaccountName != null; }
            set { }
        }

        /// <summary>
        /// Indicates if the ACH information is current or pending.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOEnum<ACHBase> ACHType;

        /// <summary>
        /// Gets or sets a value indicating whether the ACHType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ACHType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ACHTypeSpecified
        {
            get { return this.ACHType != null && this.ACHType.enumValue != ACHBase.Blank; }
            set { }
        }

        /// <summary>
        /// The dollar amount of funds being transferred by the wire.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOAmount ACHWireAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ACHWireAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ACHWireAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ACHWireAmountSpecified
        {
            get { return ACHWireAmount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 15)]
        public ACH_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null && Extension.ShouldSerialize(); }
            set { }
        }
    }
}
