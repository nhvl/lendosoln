namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class FORECLOSURE_EXPENSE_SUMMARY
    {
        /// <summary>
        /// Gets a value indicating whether the FORECLOSURE_EXPENSE_SUMMARY container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.TotalForeclosureExpenseAmountSpecified;
            }
        }

        /// <summary>
        /// Total dollar amount of all liquidation or foreclosure expenses.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount TotalForeclosureExpenseAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the TotalForeclosureExpenseAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TotalForeclosureExpenseAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TotalForeclosureExpenseAmountSpecified
        {
            get { return TotalForeclosureExpenseAmount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public FORECLOSURE_EXPENSE_SUMMARY_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
