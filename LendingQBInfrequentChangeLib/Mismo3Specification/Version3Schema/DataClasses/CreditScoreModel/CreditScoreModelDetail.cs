namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CREDIT_SCORE_MODEL_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the CREDIT_SCORE_MODEL_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditScoreCategoryTypeOtherDescriptionSpecified
                    || this.CreditScoreCategoryTypeSpecified
                    || this.CreditScoreMaximumValueSpecified
                    || this.CreditScoreMinimumValueSpecified
                    || this.CreditScoreModelNameTypeOtherDescriptionSpecified
                    || this.CreditScoreModelNameTypeSpecified
                    || this.CreditScoreModelRangeOfScoresDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// An enumerated list that describes the general type or purpose of a credit score model. This allows LOS systems to extract scores by a more general category, without having to know the specific score model names.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<CreditScoreCategoryBase> CreditScoreCategoryType;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditScoreCategoryType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditScoreCategoryType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditScoreCategoryTypeSpecified
        {
            get { return this.CreditScoreCategoryType != null && this.CreditScoreCategoryType.enumValue != CreditScoreCategoryBase.Blank; }
            set { }
        }

        /// <summary>
        /// When Credit Score Category Type is set to "Other", enter its value in this data element.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString CreditScoreCategoryTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditScoreCategoryTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditScoreCategoryTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditScoreCategoryTypeOtherDescriptionSpecified
        {
            get { return CreditScoreCategoryTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// This is the upper range of possible score values for this Credit Score Model Name Type.  The range of credit score values is required to be part of the FACTA disclosure.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOValue CreditScoreMaximumValue;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditScoreMaximumValue element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditScoreMaximumValue element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditScoreMaximumValueSpecified
        {
            get { return CreditScoreMaximumValue != null; }
            set { }
        }

        /// <summary>
        /// This is the lower range of possible score values for this Credit Score Model Name Type.  The range of credit score values is required to be part of the FACTA disclosure.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOValue CreditScoreMinimumValue;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditScoreMinimumValue element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditScoreMinimumValue element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditScoreMinimumValueSpecified
        {
            get { return CreditScoreMinimumValue != null; }
            set { }
        }

        /// <summary>
        /// Identifies the score algorithm model name used to produce the referenced credit risk score.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<CreditScoreModelNameBase> CreditScoreModelNameType;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditScoreModelNameType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditScoreModelNameType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditScoreModelNameTypeSpecified
        {
            get { return this.CreditScoreModelNameType != null && this.CreditScoreModelNameType.enumValue != CreditScoreModelNameBase.Blank; }
            set { }
        }

        /// <summary>
        /// When the Credit Score Model Name Type is set to Other, this element holds the description.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString CreditScoreModelNameTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditScoreModelNameTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditScoreModelNameTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditScoreModelNameTypeOtherDescriptionSpecified
        {
            get { return CreditScoreModelNameTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The range of scores possible under the credit score model.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOString CreditScoreModelRangeOfScoresDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditScoreModelRangeOfScoresDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditScoreModelRangeOfScoresDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditScoreModelRangeOfScoresDescriptionSpecified
        {
            get { return CreditScoreModelRangeOfScoresDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 7)]
        public CREDIT_SCORE_MODEL_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
