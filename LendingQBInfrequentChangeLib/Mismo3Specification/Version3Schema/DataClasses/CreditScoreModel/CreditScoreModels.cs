namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CREDIT_SCORE_MODELS
    {
        /// <summary>
        /// Gets a value indicating whether the CREDIT_SCORE_MODELS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditScoreModelSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of credit scoring models.
        /// </summary>
        [XmlElement("CREDIT_SCORE_MODEL", Order = 0)]
		public List<CREDIT_SCORE_MODEL> CreditScoreModel = new List<CREDIT_SCORE_MODEL>();

        /// <summary>
        /// Gets or sets a value indicating whether the CreditScoreModel element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditScoreModel element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditScoreModelSpecified
        {
            get { return this.CreditScoreModel != null && this.CreditScoreModel.Count(c => c != null && c.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public CREDIT_SCORE_MODELS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
