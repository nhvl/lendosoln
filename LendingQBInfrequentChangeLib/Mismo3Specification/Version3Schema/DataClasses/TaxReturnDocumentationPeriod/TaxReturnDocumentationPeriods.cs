namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class TAX_RETURN_DOCUMENTATION_PERIODS
    {
        /// <summary>
        /// Gets a value indicating whether the TAX_RETURN_DOCUMENTATION_PERIODS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.TaxReturnDocumentationPeriodSpecified;
            }
        }

        /// <summary>
        /// A collection of tax return documentation periods.
        /// </summary>
        [XmlElement("TAX_RETURN_DOCUMENTATION_PERIOD", Order = 0)]
		public List<TAX_RETURN_DOCUMENTATION_PERIOD> TaxReturnDocumentationPeriod = new List<TAX_RETURN_DOCUMENTATION_PERIOD>();

        /// <summary>
        /// Gets or sets a value indicating whether the TaxReturnDocumentationPeriod element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TaxReturnDocumentationPeriod element has been assigned a value.</value>
        [XmlIgnore]
        public bool TaxReturnDocumentationPeriodSpecified
        {
            get { return this.TaxReturnDocumentationPeriod != null && this.TaxReturnDocumentationPeriod.Count(t => t != null && t.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public TAX_RETURN_DOCUMENTATION_PERIODS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
