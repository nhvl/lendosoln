namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CONTACT_POINT_EMAIL
    {
        /// <summary>
        /// Gets a value indicating whether the CONTACT_POINT_EMAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ContactPointEmailValueSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// The email address for the contact.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOValue ContactPointEmailValue;

        /// <summary>
        /// Gets or sets a value indicating whether the ContactPointEmailValue element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ContactPointEmailValue element has been assigned a value.</value>
        [XmlIgnore]
        public bool ContactPointEmailValueSpecified
        {
            get { return ContactPointEmailValue != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public CONTACT_POINT_EMAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
