namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class MODIFICATION_ASPECT
    {
        /// <summary>
        /// Gets a value indicating whether the MODIFICATION_ASPECT container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.LoanModificationTypeOtherDescriptionSpecified
                    || this.LoanModificationTypeSpecified;
            }
        }

        /// <summary>
        /// Identifies the note term(s) that changed as a result of a loan modification.  More than one term could change as the result of one loan modification.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<LoanModificationBase> LoanModificationType;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanModificationType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanModificationType element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanModificationTypeSpecified
        {
            get { return this.LoanModificationType != null && this.LoanModificationType.enumValue != LoanModificationBase.Blank; }
            set { }
        }

        /// <summary>
        /// Used to collect additional information when Other is selected for Loan Modification Type.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString LoanModificationTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanModificationTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanModificationTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanModificationTypeOtherDescriptionSpecified
        {
            get { return LoanModificationTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public MODIFICATION_ASPECT_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
