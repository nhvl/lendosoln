namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class MODIFICATION_ASPECTS
    {
        /// <summary>
        /// Gets a value indicating whether the MODIFICATION_ASPECTS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ModificationAspectSpecified;
            }
        }

        /// <summary>
        /// A collection of loan modification aspects.
        /// </summary>
        [XmlElement("MODIFICATION_ASPECT", Order = 0)]
		public List<MODIFICATION_ASPECT> ModificationAspect = new List<MODIFICATION_ASPECT>();

        /// <summary>
        /// Gets or sets a value indicating whether the ModificationAspect element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ModificationAspect element has been assigned a value.</value>
        [XmlIgnore]
        public bool ModificationAspectSpecified
        {
            get { return this.ModificationAspect != null && this.ModificationAspect.Count(m => m != null && m.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public MODIFICATION_ASPECTS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
