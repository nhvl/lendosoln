namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CREDIT_BUREAU_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the CREDIT_BUREAU_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditBureauDisclaimerTextSpecified
                    || this.CreditBureauNameSpecified
                    || this.CreditBureauTechnologyProviderNameSpecified
                    || this.ExtensionSpecified
                    || this.FNMCreditBureauIdentifierSpecified;
            }
        }

        /// <summary>
        /// Contains the legal disclaimer text provided by the credit bureau.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString CreditBureauDisclaimerText;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditBureauDisclaimerText element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditBureauDisclaimerText element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditBureauDisclaimerTextSpecified
        {
            get { return CreditBureauDisclaimerText != null; }
            set { }
        }

        /// <summary>
        /// The name of the credit bureau that  merges data from the credit repositories and produces credit reports for end users. The credit bureau is NOT the same as the Credit Repository (Equifax, Experian, Trans Union) that houses the raw credit files.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString CreditBureauName;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditBureauName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditBureauName element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditBureauNameSpecified
        {
            get { return CreditBureauName != null; }
            set { }
        }

        /// <summary>
        /// Identifies  the software technology provider for a credit bureau, if the provider is an outside vendor.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString CreditBureauTechnologyProviderName;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditBureauTechnologyProviderName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditBureauTechnologyProviderName element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditBureauTechnologyProviderNameSpecified
        {
            get { return CreditBureauTechnologyProviderName != null; }
            set { }
        }

        /// <summary>
        /// This is the numeric identifier assigned by Fannie Mae to the Credit Bureau providing the credit report.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOIdentifier FNMCreditBureauIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the FNMCreditBureauIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FNMCreditBureauIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool FNMCreditBureauIdentifierSpecified
        {
            get { return FNMCreditBureauIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public CREDIT_BUREAU_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
