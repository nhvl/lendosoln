namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class SYSTEM_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the SYSTEM_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.HeatingCoolingMeteredSeparatelyDescriptionSpecified
                    || this.HeatingCoolingMeteredSeparatelyIndicatorSpecified;
            }
        }

        /// <summary>
        /// A free-form text field used to detail how the Heating and Cooling utility use is metered separately per unit.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString HeatingCoolingMeteredSeparatelyDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the HeatingCoolingMeteredSeparatelyDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HeatingCoolingMeteredSeparatelyDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool HeatingCoolingMeteredSeparatelyDescriptionSpecified
        {
            get { return HeatingCoolingMeteredSeparatelyDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates that individual units are capable of measuring and billing their heating and cooling utility use separately.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOIndicator HeatingCoolingMeteredSeparatelyIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the HeatingCoolingMeteredSeparatelyIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HeatingCoolingMeteredSeparatelyIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool HeatingCoolingMeteredSeparatelyIndicatorSpecified
        {
            get { return HeatingCoolingMeteredSeparatelyIndicator != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public SYSTEM_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
