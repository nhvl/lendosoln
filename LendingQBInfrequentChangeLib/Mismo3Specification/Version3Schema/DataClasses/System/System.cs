namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class SYSTEM
    {
        /// <summary>
        /// Gets a value indicating whether the SYSTEM container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CoolingSystemsSpecified
                    || this.ExtensionSpecified
                    || this.HeatingSystemsSpecified
                    || this.SystemDetailSpecified;
            }
        }

        /// <summary>
        /// A list of cooling systems.
        /// </summary>
        [XmlElement("COOLING_SYSTEMS", Order = 0)]
        public COOLING_SYSTEMS CoolingSystems;

        /// <summary>
        /// Gets or sets a value indicating whether the CoolingSystems element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CoolingSystems element has been assigned a value.</value>
        [XmlIgnore]
        public bool CoolingSystemsSpecified
        {
            get { return this.CoolingSystems != null && this.CoolingSystems.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A list of heating systems.
        /// </summary>
        [XmlElement("HEATING_SYSTEMS", Order = 1)]
        public HEATING_SYSTEMS HeatingSystems;

        /// <summary>
        /// Gets or sets a value indicating whether the HeatingSystems element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HeatingSystems element has been assigned a value.</value>
        [XmlIgnore]
        public bool HeatingSystemsSpecified
        {
            get { return this.HeatingSystems != null && this.HeatingSystems.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details on a system.
        /// </summary>
        [XmlElement("SYSTEM_DETAIL", Order = 2)]
        public SYSTEM_DETAIL SystemDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the SystemDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SystemDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool SystemDetailSpecified
        {
            get { return this.SystemDetail != null && this.SystemDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public SYSTEM_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
