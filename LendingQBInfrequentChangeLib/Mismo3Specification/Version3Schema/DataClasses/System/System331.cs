namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class SYSTEM_3_3_1
    {
        /// <summary>
        /// Gets a value indicating whether the SYSTEM_3_3_1 container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.SecuritySystemsSpecified
                    || this.WaterSystemsSpecified;
            }
        }

        /// <summary>
        /// A list of security systems.
        /// </summary>
        [XmlElement("SECURITY_SYSTEMS", Order = 0)]
        public SECURITY_SYSTEMS SecuritySystems;

        /// <summary>
        /// Gets or sets a value indicating whether the SecuritySystems element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SecuritySystems element has been assigned a value.</value>
        [XmlIgnore]
        public bool SecuritySystemsSpecified
        {
            get { return this.SecuritySystems != null && this.SecuritySystems.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A list of water systems.
        /// </summary>
        [XmlElement("WATER_SYSTEMS", Order = 1)]
        public WATER_SYSTEMS WaterSystems;

        /// <summary>
        /// Gets or sets a value indicating whether the WaterSystems element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the WaterSystems element has been assigned a value.</value>
        [XmlIgnore]
        public bool WaterSystemsSpecified
        {
            get { return this.WaterSystems != null && this.WaterSystems.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public SYSTEM_3_3_1_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
