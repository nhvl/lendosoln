namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class UNIQUE_PROPERTY_IDENTIFICATIONS
    {
        /// <summary>
        /// Gets a value indicating whether the UNIQUE_PROPERTY_IDENTIFICATIONS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.UniquePropertyIdentificationSpecified;
            }
        }

        /// <summary>
        /// A collection of unique property identifications.
        /// </summary>
        [XmlElement("UNIQUE_PROPERTY_IDENTIFICATION", Order = 0)]
		public List<UNIQUE_PROPERTY_IDENTIFICATION> UniquePropertyIdentification = new List<UNIQUE_PROPERTY_IDENTIFICATION>();

        /// <summary>
        /// Gets or sets a value indicating whether the UniquePropertyIdentification element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the UniquePropertyIdentification element has been assigned a value.</value>
        [XmlIgnore]
        public bool UniquePropertyIdentificationSpecified
        {
            get { return this.UniquePropertyIdentification != null && this.UniquePropertyIdentification.Count(u => u != null && u.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public UNIQUE_PROPERTY_IDENTIFICATIONS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
