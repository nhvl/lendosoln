namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class INSPECTION_SERVICE
    {
        /// <summary>
        /// Gets a value indicating whether the INSPECTION_SERVICE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.InspectionServiceRequestSpecified
                    || this.InspectionServiceResponseSpecified;
            }
        }

        /// <summary>
        /// An inspection service request.
        /// </summary>
        [XmlElement("INSPECTION_SERVICE_REQUEST", Order = 0)]
        public INSPECTION_SERVICE_REQUEST InspectionServiceRequest;

        /// <summary>
        /// Gets or sets a value indicating whether the InspectionServiceRequest element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InspectionServiceRequest element has been assigned a value.</value>
        [XmlIgnore]
        public bool InspectionServiceRequestSpecified
        {
            get { return this.InspectionServiceRequest != null && this.InspectionServiceRequest.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Response to an inspection service request.
        /// </summary>
        [XmlElement("INSPECTION_SERVICE_RESPONSE", Order = 1)]
        public INSPECTION_SERVICE_RESPONSE InspectionServiceResponse;

        /// <summary>
        /// Gets or sets a value indicating whether the InspectionServiceResponse element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InspectionServiceResponse element has been assigned a value.</value>
        [XmlIgnore]
        public bool InspectionServiceResponseSpecified
        {
            get { return this.InspectionServiceResponse != null && this.InspectionServiceResponse.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public INSPECTION_SERVICE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
