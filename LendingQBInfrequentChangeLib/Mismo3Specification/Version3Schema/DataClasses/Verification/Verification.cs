namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class VERIFICATION
    {
        /// <summary>
        /// Gets a value indicating whether the VERIFICATION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.VerificationByNameSpecified
                    || this.VerificationCommentDescriptionSpecified
                    || this.VerificationDateSpecified
                    || this.VerificationMethodTypeOtherDescriptionSpecified
                    || this.VerificationMethodTypeSpecified
                    || this.VerificationStatusTypeSpecified;
            }
        }

        /// <summary>
        /// The name or initials of the person who verified information in the record.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString VerificationByName;

        /// <summary>
        /// Gets or sets a value indicating whether the VerificationByName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the VerificationByName element has been assigned a value.</value>
        [XmlIgnore]
        public bool VerificationByNameSpecified
        {
            get { return VerificationByName != null; }
            set { }
        }

        /// <summary>
        /// Any remarks by the person specified in the Verification By Name about the data in the record.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString VerificationCommentDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the VerificationCommentDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the VerificationCommentDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool VerificationCommentDescriptionSpecified
        {
            get { return VerificationCommentDescription != null; }
            set { }
        }

        /// <summary>
        /// The date that the information in the record was verified.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMODate VerificationDate;

        /// <summary>
        /// Gets or sets a value indicating whether the VerificationDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the VerificationDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool VerificationDateSpecified
        {
            get { return VerificationDate != null; }
            set { }
        }

        /// <summary>
        /// Method used to verify the particular item.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOEnum<VerificationMethodBase> VerificationMethodType;

        /// <summary>
        /// Gets or sets a value indicating whether the VerificationMethodType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the VerificationMethodType element has been assigned a value.</value>
        [XmlIgnore]
        public bool VerificationMethodTypeSpecified
        {
            get { return this.VerificationMethodType != null && this.VerificationMethodType.enumValue != VerificationMethodBase.Blank; }
            set { }
        }

        /// <summary>
        /// The description of the Verification Method Type when Other is selected as the option from the enumerated list.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString VerificationMethodTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the VerificationMethodTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the VerificationMethodTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool VerificationMethodTypeOtherDescriptionSpecified
        {
            get { return VerificationMethodTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// This attribute indicates the status of the verification - verified, not verified, or to be verified.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOEnum<VerificationStatusBase> VerificationStatusType;

        /// <summary>
        /// Gets or sets a value indicating whether the VerificationStatusType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the VerificationStatusType element has been assigned a value.</value>
        [XmlIgnore]
        public bool VerificationStatusTypeSpecified
        {
            get { return this.VerificationStatusType != null && this.VerificationStatusType.enumValue != VerificationStatusBase.Blank; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 6)]
        public VERIFICATION_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
