namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class DATA_MODIFICATION
    {
        /// <summary>
        /// Gets a value indicating whether the DATA_MODIFICATION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DataModificationRequestSpecified
                    || this.DataModificationResponseSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A request for data modification.
        /// </summary>
        [XmlElement("DATA_MODIFICATION_REQUEST", Order = 0)]
        public DATA_MODIFICATION_REQUEST DataModificationRequest;

        /// <summary>
        /// Gets or sets a value indicating whether the DataModificationRequest element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DataModificationRequest element has been assigned a value.</value>
        [XmlIgnore]
        public bool DataModificationRequestSpecified
        {
            get { return this.DataModificationRequest != null && this.DataModificationRequest.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A response to a request for data modification.
        /// </summary>
        [XmlElement("DATA_MODIFICATION_RESPONSE", Order = 1)]
        public DATA_MODIFICATION_RESPONSE DataModificationResponse;

        /// <summary>
        /// Gets or sets a value indicating whether the DataModificationResponse element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DataModificationResponse element has been assigned a value.</value>
        [XmlIgnore]
        public bool DataModificationResponseSpecified
        {
            get { return this.DataModificationResponse != null && this.DataModificationResponse.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public DATA_MODIFICATION_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
