namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class RESIDENCE
    {
        /// <summary>
        /// Gets a value indicating whether the RESIDENCE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AddressSpecified
                    || this.ExtensionSpecified
                    || this.LandlordSpecified
                    || this.ResidenceDetailSpecified;
            }
        }

        /// <summary>
        /// Address of the residence.
        /// </summary>
        [XmlElement("ADDRESS", Order = 0)]
        public ADDRESS Address;

        /// <summary>
        /// Gets or sets a value indicating whether the Address element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Address element has been assigned a value.</value>
        [XmlIgnore]
        public bool AddressSpecified
        {
            get { return this.Address != null && this.Address.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Landlord of the residence.
        /// </summary>
        [XmlElement("LANDLORD", Order = 1)]
        public LANDLORD Landlord;

        /// <summary>
        /// Gets or sets a value indicating whether the Landlord element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Landlord element has been assigned a value.</value>
        [XmlIgnore]
        public bool LandlordSpecified
        {
            get { return this.Landlord != null && this.Landlord.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details about a residence.
        /// </summary>
        [XmlElement("RESIDENCE_DETAIL", Order = 2)]
        public RESIDENCE_DETAIL ResidenceDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the ResidenceDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ResidenceDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool ResidenceDetailSpecified
        {
            get { return this.ResidenceDetail != null && this.ResidenceDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public RESIDENCE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
