namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class RESIDENCE_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the RESIDENCE_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BorrowerResidencyBasisTypeSpecified
                    || this.BorrowerResidencyDurationMonthsCountSpecified
                    || this.BorrowerResidencyDurationYearsCountSpecified
                    || this.BorrowerResidencyEndDateSpecified
                    || this.BorrowerResidencyReportedDateSpecified
                    || this.BorrowerResidencyStartDateSpecified
                    || this.BorrowerResidencyTypeSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// The basis on which borrower lives/lived at the indicated address. Could be either current or prior address. Collected on the URLA in Section III (Present Address or Former Address). It may also be reported on a Credit Report.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<BorrowerResidencyBasisBase> BorrowerResidencyBasisType;

        /// <summary>
        /// Gets or sets a value indicating whether the BorrowerResidencyBasisType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BorrowerResidencyBasisType element has been assigned a value.</value>
        [XmlIgnore]
        public bool BorrowerResidencyBasisTypeSpecified
        {
            get { return this.BorrowerResidencyBasisType != null && this.BorrowerResidencyBasisType.enumValue != BorrowerResidencyBasisBase.Blank; }
            set { }
        }

        /// <summary>
        /// The number of months the borrower resided at the indicated address. Could be either current or prior address. Collected on the URLA in Section III (Present Address or Former Address). It may also be reported on a Credit Report.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOCount BorrowerResidencyDurationMonthsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the BorrowerResidencyDurationMonthsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BorrowerResidencyDurationMonthsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool BorrowerResidencyDurationMonthsCountSpecified
        {
            get { return BorrowerResidencyDurationMonthsCount != null; }
            set { }
        }

        /// <summary>
        /// The number of years the borrower resided at the indicated address. Could be either current or prior address. Collected on the URLA in Section III (Present Address or Former Address). It may also be reported on a Credit Report.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOCount BorrowerResidencyDurationYearsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the BorrowerResidencyDurationYearsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BorrowerResidencyDurationYearsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool BorrowerResidencyDurationYearsCountSpecified
        {
            get { return BorrowerResidencyDurationYearsCount != null; }
            set { }
        }

        /// <summary>
        /// The date the borrower actually moved out of the indicated address. 
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMODate BorrowerResidencyEndDate;

        /// <summary>
        /// Gets or sets a value indicating whether the BorrowerResidencyEndDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BorrowerResidencyEndDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool BorrowerResidencyEndDateSpecified
        {
            get { return BorrowerResidencyEndDate != null; }
            set { }
        }

        /// <summary>
        /// The date it was reported that the borrower moved to the indicated address. 
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMODate BorrowerResidencyReportedDate;

        /// <summary>
        /// Gets or sets a value indicating whether the BorrowerResidencyReportedDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BorrowerResidencyReportedDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool BorrowerResidencyReportedDateSpecified
        {
            get { return BorrowerResidencyReportedDate != null; }
            set { }
        }

        /// <summary>
        /// The date the borrower actually moved to the indicated address. 
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMODate BorrowerResidencyStartDate;

        /// <summary>
        /// Gets or sets a value indicating whether the BorrowerResidencyStartDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BorrowerResidencyStartDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool BorrowerResidencyStartDateSpecified
        {
            get { return BorrowerResidencyStartDate != null; }
            set { }
        }

        /// <summary>
        /// This attribute, accompanying borrower residence data, qualifies it as either the current or prior borrowers residence.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOEnum<BorrowerResidencyBase> BorrowerResidencyType;

        /// <summary>
        /// Gets or sets a value indicating whether the BorrowerResidencyType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BorrowerResidencyType element has been assigned a value.</value>
        [XmlIgnore]
        public bool BorrowerResidencyTypeSpecified
        {
            get { return this.BorrowerResidencyType != null && this.BorrowerResidencyType.enumValue != BorrowerResidencyBase.Blank; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 7)]
        public RESIDENCE_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
