namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class MI
    {
        /// <summary>
        /// Gets a value indicating whether the MI container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.MIApplicationRequestSpecified
                    || this.MIApplicationResponseSpecified
                    || this.MIRateQuoteResponseSpecified
                    || this.MiValidationRequestSpecified
                    || this.MiValidationResponseSpecified;
            }
        }

        /// <summary>
        /// An application request for mortgage insurance.
        /// </summary>
        [XmlElement("MI_APPLICATION_REQUEST", Order = 0)]
        public MI_APPLICATION_REQUEST MIApplicationRequest;

        /// <summary>
        /// Gets or sets a value indicating whether the Mortgage Insurance Application Request element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Mortgage Insurance Application Request element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIApplicationRequestSpecified
        {
            get { return this.MIApplicationRequest != null && this.MIApplicationRequest.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A response to a mortgage insurance application.
        /// </summary>
        [XmlElement("MI_APPLICATION_RESPONSE", Order = 1)]
        public MI_APPLICATION_RESPONSE MIApplicationResponse;

        /// <summary>
        /// Gets or sets a value indicating whether the Mortgage Insurance Application Response element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Mortgage Insurance Application Response element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIApplicationResponseSpecified
        {
            get { return this.MIApplicationResponse != null && this.MIApplicationResponse.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A response containing a rate quote.
        /// </summary>
        [XmlElement("MI_RATE_QUOTE_RESPONSE", Order = 2)]
        public MI_RATE_QUOTE_RESPONSE MIRateQuoteResponse;

        /// <summary>
        /// Gets or sets a value indicating whether the Mortgage Insurance Rate Quote Response element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Mortgage Insurance Rate Quote Response element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIRateQuoteResponseSpecified
        {
            get { return this.MIRateQuoteResponse != null && this.MIRateQuoteResponse.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Request for mortgage insurance validation.
        /// </summary>
        [XmlElement("MI_VALIDATION_REQUEST", Order = 3)]
        public MI_VALIDATION_REQUEST MiValidationRequest;

        /// <summary>
        /// Gets or sets a value indicating whether the Mortgage Insurance Validation Request element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Mortgage Insurance Validation Request element has been assigned a value.</value>
        [XmlIgnore]
        public bool MiValidationRequestSpecified
        {
            get { return this.MiValidationRequest != null && this.MiValidationRequest.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Validation of the mortgage insurance.
        /// </summary>
        [XmlElement("MI_VALIDATION_RESPONSE", Order = 4)]
        public MI_VALIDATION_RESPONSE MiValidationResponse;

        /// <summary>
        /// Gets or sets a value indicating whether the Mortgage Insurance ValidationResponse element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Mortgage Insurance Validation Response element has been assigned a value.</value>
        [XmlIgnore]
        public bool MiValidationResponseSpecified
        {
            get { return this.MiValidationResponse != null && this.MiValidationResponse.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 5)]
        public MI_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
