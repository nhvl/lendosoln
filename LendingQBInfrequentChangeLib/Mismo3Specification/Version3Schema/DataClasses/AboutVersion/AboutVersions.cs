namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class ABOUT_VERSIONS
    {
        /// <summary>
        /// Gets a value indicating whether the ABOUT_VERSIONS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AboutVersionSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A container for the various meta-data elements.
        /// </summary>
        [XmlElement("ABOUT_VERSION", Order = 0)]
		public List<ABOUT_VERSION> AboutVersion = new List<ABOUT_VERSION>();

        /// <summary>
        /// Gets or sets a value indicating whether the AboutVersion element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AboutVersion element has been assigned a value.</value>
        [XmlIgnore]
        public bool AboutVersionSpecified
        {
            get { return this.AboutVersion != null && this.AboutVersion.Count(a => a != null && a.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public ABOUT_VERSIONS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
