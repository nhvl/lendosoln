namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class SERVICING_TRANSFER
    {
        /// <summary>
        /// Gets a value indicating whether the SERVICING_TRANSFER container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ServicingTransferRequestSpecified
                    || this.ServicingTransferResponseSpecified;
            }
        }

        /// <summary>
        /// A request for transferring service.
        /// </summary>
        [XmlElement("SERVICING_TRANSFER_REQUEST", Order = 0)]
        public SERVICING_TRANSFER_REQUEST ServicingTransferRequest;

        /// <summary>
        /// Gets or sets a value indicating whether the ServicingTransferRequest element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServicingTransferRequest element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServicingTransferRequestSpecified
        {
            get { return this.ServicingTransferRequest != null && this.ServicingTransferRequest.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A response to a servicing transfer request.
        /// </summary>
        [XmlElement("SERVICING_TRANSFER_RESPONSE", Order = 1)]
        public SERVICING_TRANSFER_RESPONSE ServicingTransferResponse;

        /// <summary>
        /// Gets or sets a value indicating whether the ServicingTransferResponse element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServicingTransferResponse element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServicingTransferResponseSpecified
        {
            get { return this.ServicingTransferResponse != null && this.ServicingTransferResponse.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public SERVICING_TRANSFER_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
