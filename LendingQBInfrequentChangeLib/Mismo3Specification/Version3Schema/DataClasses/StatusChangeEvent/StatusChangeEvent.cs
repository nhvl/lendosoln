namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class STATUS_CHANGE_EVENT
    {
        /// <summary>
        /// Gets a value indicating whether the STATUS_CHANGE_EVENT container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.InvestorReportingActionDateSpecified
                    || this.InvestorReportingActionDescriptionSpecified
                    || this.InvestorReportingActionReversalIndicatorSpecified
                    || this.InvestorReportingActionTypeOtherDescriptionSpecified
                    || this.InvestorReportingActionTypeSpecified
                    || this.ReverseMortgageReportingActionDescriptionSpecified
                    || this.ReverseMortgageReportingActionTypeOtherDescriptionSpecified
                    || this.ReverseMortgageReportingActionTypeSpecified;
            }
        }

        /// <summary>
        /// The actual date associated with the reported Investor Reporting Action Type.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMODate InvestorReportingActionDate;

        /// <summary>
        /// Gets or sets a value indicating whether the InvestorReportingActionDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InvestorReportingActionDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool InvestorReportingActionDateSpecified
        {
            get { return InvestorReportingActionDate != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information in conjunction with Investor Reporting Action Type.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString InvestorReportingActionDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the InvestorReportingActionDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InvestorReportingActionDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool InvestorReportingActionDescriptionSpecified
        {
            get { return InvestorReportingActionDescription != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that the previously reported investor reporting action is being reversed.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOIndicator InvestorReportingActionReversalIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the InvestorReportingActionReversalIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InvestorReportingActionReversalIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool InvestorReportingActionReversalIndicatorSpecified
        {
            get { return InvestorReportingActionReversalIndicator != null; }
            set { }
        }

        /// <summary>
        /// The Reporting System status of the loan as reported to the investor.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOEnum<InvestorReportingActionBase> InvestorReportingActionType;

        /// <summary>
        /// Gets or sets a value indicating whether the InvestorReportingActionType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InvestorReportingActionType element has been assigned a value.</value>
        [XmlIgnore]
        public bool InvestorReportingActionTypeSpecified
        {
            get { return this.InvestorReportingActionType != null && this.InvestorReportingActionType.enumValue != InvestorReportingActionBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture the Investor Reporting Action type if Other is selected as the Investor Reporting Action type.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString InvestorReportingActionTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the InvestorReportingActionTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InvestorReportingActionTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool InvestorReportingActionTypeOtherDescriptionSpecified
        {
            get { return InvestorReportingActionTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information in conjunction with Reverse Mortgage Reporting Action Types.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString ReverseMortgageReportingActionDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ReverseMortgageReportingActionDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ReverseMortgageReportingActionDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ReverseMortgageReportingActionDescriptionSpecified
        {
            get { return ReverseMortgageReportingActionDescription != null; }
            set { }
        }

        /// <summary>
        /// The reporting system status of the reverse mortgage as reported to the investor.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOEnum<ReverseMortgageReportingActionBase> ReverseMortgageReportingActionType;

        /// <summary>
        /// Gets or sets a value indicating whether the ReverseMortgageReportingActionType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ReverseMortgageReportingActionType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ReverseMortgageReportingActionTypeSpecified
        {
            get { return this.ReverseMortgageReportingActionType != null && this.ReverseMortgageReportingActionType.enumValue != ReverseMortgageReportingActionBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is  selected as the Reverse Mortgage Reporting Action Type.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOString ReverseMortgageReportingActionTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ReverseMortgageReportingActionTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ReverseMortgageReportingActionTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ReverseMortgageReportingActionTypeOtherDescriptionSpecified
        {
            get { return ReverseMortgageReportingActionTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 8)]
        public STATUS_CHANGE_EVENT_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
