namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class STATUS_CHANGE_EVENTS
    {
        /// <summary>
        /// Gets a value indicating whether the STATUS_CHANGE_EVENTS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.StatusChangeEventSpecified;
            }
        }

        /// <summary>
        /// A collection of status change events.
        /// </summary>
        [XmlElement("STATUS_CHANGE_EVENT", Order = 0)]
		public List<STATUS_CHANGE_EVENT> StatusChangeEvent = new List<STATUS_CHANGE_EVENT>();

        /// <summary>
        /// Gets or sets a value indicating whether the StatusChangeEvent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the StatusChangeEvent element has been assigned a value.</value>
        [XmlIgnore]
        public bool StatusChangeEventSpecified
        {
            get { return this.StatusChangeEvent != null && this.StatusChangeEvent.Count(s => s != null && s.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public STATUS_CHANGE_EVENTS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
