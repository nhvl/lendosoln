namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CREDIT_LIABILITY_MOST_RECENT_ADVERSE_RATING
    {
        /// <summary>
        /// Gets a value indicating whether the CREDIT_LIABILITY_MOST_RECENT_ADVERSE_RATING container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditLiabilityMostRecentAdverseRatingAmountSpecified
                    || this.CreditLiabilityMostRecentAdverseRatingCodeSpecified
                    || this.CreditLiabilityMostRecentAdverseRatingDateSpecified
                    || this.CreditLiabilityMostRecentAdverseRatingTypeSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// The most recent amount that the borrower was reported delinquent by the liability holder.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount CreditLiabilityMostRecentAdverseRatingAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLiabilityMostRecentAdverseRatingAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLiabilityMostRecentAdverseRatingAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLiabilityMostRecentAdverseRatingAmountSpecified
        {
            get { return CreditLiabilityMostRecentAdverseRatingAmount != null; }
            set { }
        }

        /// <summary>
        /// The liability accounts most recent delinquency rating code for how timely the borrower has been making payments on this account. See the MISMO Implementation Guide: Credit Reporting for more information on MOP codes.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOCode CreditLiabilityMostRecentAdverseRatingCode;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLiabilityMostRecentAdverseRatingCode element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLiabilityMostRecentAdverseRatingCode element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLiabilityMostRecentAdverseRatingCodeSpecified
        {
            get { return CreditLiabilityMostRecentAdverseRatingCode != null; }
            set { }
        }

        /// <summary>
        /// The date of the most recent delinquency reported by the liability holder.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMODate CreditLiabilityMostRecentAdverseRatingDate;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLiabilityMostRecentAdverseRatingDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLiabilityMostRecentAdverseRatingDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLiabilityMostRecentAdverseRatingDateSpecified
        {
            get { return CreditLiabilityMostRecentAdverseRatingDate != null; }
            set { }
        }

        /// <summary>
        /// The liability accounts most recent delinquency rating type for how timely the borrower has been making payments on this account.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOEnum<CreditLiabilityMostRecentAdverseRatingBase> CreditLiabilityMostRecentAdverseRatingType;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLiabilityMostRecentAdverseRatingType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLiabilityMostRecentAdverseRatingType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLiabilityMostRecentAdverseRatingTypeSpecified
        {
            get { return this.CreditLiabilityMostRecentAdverseRatingType != null && this.CreditLiabilityMostRecentAdverseRatingType.enumValue != CreditLiabilityMostRecentAdverseRatingBase.Blank; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public CREDIT_LIABILITY_MOST_RECENT_ADVERSE_RATING_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
