namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class RESIDENTIAL_RENT_SCHEDULE
    {
        /// <summary>
        /// Gets a value indicating whether the RESIDENTIAL_RENT_SCHEDULE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ResidentialRentalSpecified
                    || this.ResidentialRentScheduleDetailSpecified;
            }
        }

        /// <summary>
        /// Contains details about the rent schedule as it relates to the rental market.
        /// </summary>
        [XmlElement("RESIDENTIAL_RENT_SCHEDULE_DETAIL", Order = 0)]
        public RESIDENTIAL_RENT_SCHEDULE_DETAIL ResidentialRentScheduleDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the ResidentialRentScheduleDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ResidentialRentScheduleDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool ResidentialRentScheduleDetailSpecified
        {
            get { return this.ResidentialRentScheduleDetail != null && this.ResidentialRentScheduleDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Contains details about the rental.
        /// </summary>
        [XmlElement("RESIDENTIAL_RENTAL", Order = 1)]
        public RESIDENTIAL_RENTAL ResidentialRental;

        /// <summary>
        /// Gets or sets a value indicating whether the ResidentialRental element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ResidentialRental element has been assigned a value.</value>
        [XmlIgnore]
        public bool ResidentialRentalSpecified
        {
            get { return this.ResidentialRental != null && this.ResidentialRental.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public RESIDENTIAL_RENT_SCHEDULE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
