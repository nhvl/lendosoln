namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class RESIDENTIAL_RENT_SCHEDULE_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the RESIDENTIAL_RENT_SCHEDULE_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.EstimatedMarketMonthlyRentAmountSpecified
                    || this.EstimatedMarketMonthlyRentEffectiveDateSpecified
                    || this.ExtensionSpecified
                    || this.MarketRentalDataCommentDescriptionSpecified
                    || this.MarketRentReconciliationCommentDescriptionSpecified;
            }
        }

        /// <summary>
        /// The estimated dollar value of the market monthly rent for a living unit or residence.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount EstimatedMarketMonthlyRentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the EstimatedMarketMonthlyRentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EstimatedMarketMonthlyRentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool EstimatedMarketMonthlyRentAmountSpecified
        {
            get { return EstimatedMarketMonthlyRentAmount != null; }
            set { }
        }

        /// <summary>
        /// Specifies the effective date of the estimated market monthly rent.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMODate EstimatedMarketMonthlyRentEffectiveDate;

        /// <summary>
        /// Gets or sets a value indicating whether the EstimatedMarketMonthlyRentEffectiveDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EstimatedMarketMonthlyRentEffectiveDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool EstimatedMarketMonthlyRentEffectiveDateSpecified
        {
            get { return EstimatedMarketMonthlyRentEffectiveDate != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe or comment on the market rental data used in the determination of the Estimated Market Monthly Rent Amount for a living unit or property.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString MarketRentalDataCommentDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MarketRentalDataCommentDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MarketRentalDataCommentDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MarketRentalDataCommentDescriptionSpecified
        {
            get { return MarketRentalDataCommentDescription != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe or comment on the reconciliation of the final value for Estimated Market Monthly Rent Amount for a living unit or property.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString MarketRentReconciliationCommentDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MarketRentReconciliationCommentDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MarketRentReconciliationCommentDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MarketRentReconciliationCommentDescriptionSpecified
        {
            get { return MarketRentReconciliationCommentDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public RESIDENTIAL_RENT_SCHEDULE_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
