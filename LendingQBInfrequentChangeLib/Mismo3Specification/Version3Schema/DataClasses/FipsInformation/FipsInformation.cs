namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class FIPS_INFORMATION
    {
        /// <summary>
        /// Gets a value indicating whether the FIPS_INFORMATION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.FIPSCountryCodeSpecified
                    || this.FIPSCountyCodeSpecified
                    || this.FIPSCountySubdivisionCodeSpecified
                    || this.FIPSCountySubdivisionNameSpecified
                    || this.FIPSCountySubdivisionTypeSpecified
                    || this.FIPSPlaceCodeSpecified
                    || this.FIPSPlaceNameSpecified
                    || this.FIPSStateAlphaCodeSpecified
                    || this.FIPSStateNumericCodeSpecified;
            }
        }

        /// <summary>
        /// A unique two letter alpha code to identify each country.  These Country codes are part of the Federal Information Processing Standards (FIPS), an official coding system developed by the National Institute of Standards and Technology (NIST, formerly the National Bureau of Standards), U.S. Department of Commerce, and maintained by the U.S. Geological Survey (USGS).
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOCode FIPSCountryCode;

        /// <summary>
        /// Gets or sets a value indicating whether the FIPSCountryCode element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FIPSCountryCode element has been assigned a value.</value>
        [XmlIgnore]
        public bool FIPSCountryCodeSpecified
        {
            get { return FIPSCountryCode != null; }
            set { }
        }

        /// <summary>
        /// Code identifying the county. (Designator Code based on Federal Information Processing Standard Publication 6-4).
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOCode FIPSCountyCode;

        /// <summary>
        /// Gets or sets a value indicating whether the FIPSCountyCode element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FIPSCountyCode element has been assigned a value.</value>
        [XmlIgnore]
        public bool FIPSCountyCodeSpecified
        {
            get { return FIPSCountyCode != null; }
            set { }
        }

        /// <summary>
        /// The Federal Information Processing Standards (FIPS) County Subdivision Code identifies all county subdivisions in the 50 states, District of Columbia and Puerto Rico. 
        /// The Minor Civil Division (MCD) and Census County Division (CCD) are geocodes that are part of the FIPS 55 system, developed by the National Institute of Standards and Technology (NIST) and now maintained by the U.S. Geological Survey. The FIPS 55 system identifies named entities with a unique five-digit numeric locality code. The FIPS 55 locality codes identify governmentally functioning MCDs within a numeric range from 00001 to 89999. The codes in this range also represent incorporated places, Census Designated Places (CDP), and Alaska Native and American Indian areas, together with other entities not included in the tabulations of the decennial census, such as named localities, military installations, and National Parks. All these entity names are combined and listed in a single alphabetic sequence. The FIPS code range, 90000 to 98999, is reserved for CCDs and nonfunctioning MCDs where they cover whole States, whole counties, or their statistically equivalent entities. 
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOCode FIPSCountySubdivisionCode;

        /// <summary>
        /// Gets or sets a value indicating whether the FIPSCountySubdivisionCode element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FIPSCountySubdivisionCode element has been assigned a value.</value>
        [XmlIgnore]
        public bool FIPSCountySubdivisionCodeSpecified
        {
            get { return FIPSCountySubdivisionCode != null; }
            set { }
        }

        /// <summary>
        /// The formal name or description for a county subdivision that is referenced by the FIPS County Subdivision Code. For example,  Jones Township.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString FIPSCountySubdivisionName;

        /// <summary>
        /// Gets or sets a value indicating whether the FIPSCountySubdivisionName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FIPSCountySubdivisionName element has been assigned a value.</value>
        [XmlIgnore]
        public bool FIPSCountySubdivisionNameSpecified
        {
            get { return FIPSCountySubdivisionName != null; }
            set { }
        }

        /// <summary>
        /// A legal or statistical division of a county recognized as part of the Federal Information Processing Standards (FIPS) 55 system. The two major types of county subdivisions are census county divisions and minor civil divisions. This information is used by the US Census Bureau.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<FIPSCountySubdivisionBase> FIPSCountySubdivisionType;

        /// <summary>
        /// Gets or sets a value indicating whether the FIPSCountySubdivisionType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FIPSCountySubdivisionType element has been assigned a value.</value>
        [XmlIgnore]
        public bool FIPSCountySubdivisionTypeSpecified
        {
            get { return this.FIPSCountySubdivisionType != null && this.FIPSCountySubdivisionType.enumValue != FIPSCountySubdivisionBase.Blank; }
            set { }
        }

        /// <summary>
        /// A five-digit numeric code assigned by the  National Institute of Standards and Technology (NIST), and maintained by the U.S. Geological Survey (USGS), to identify populated places, primary county divisions, and other locational entities within a State. The NIST assigns the codes based on the alphabetic sequence of the entity names; it documents the codes in FIPS PUB 55. This information is used by the US Census Bureau.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOCode FIPSPlaceCode;

        /// <summary>
        /// Gets or sets a value indicating whether the FIPSPlaceCode element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FIPSPlaceCode element has been assigned a value.</value>
        [XmlIgnore]
        public bool FIPSPlaceCodeSpecified
        {
            get { return FIPSPlaceCode != null; }
            set { }
        }

        /// <summary>
        /// The formal name of a statistical entity as defined by the by the  National Institute of Standards and Technology (NIST), and maintained by the U.S. Geological Survey (USGS), that is referenced by the FIPS Place Code.  Place includes Census Bureau designated places, consolidated cities, and incorporated places. This is also represented by the 5-digit FIPS Place Code. This information is used by the US Census Bureau.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOString FIPSPlaceName;

        /// <summary>
        /// Gets or sets a value indicating whether the FIPSPlaceName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FIPSPlaceName element has been assigned a value.</value>
        [XmlIgnore]
        public bool FIPSPlaceNameSpecified
        {
            get { return FIPSPlaceName != null; }
            set { }
        }

        /// <summary>
        /// A unique two letter alpha code to identify each State and State equivalent entity.  These State codes are part of the Federal Information Processing Standards (FIPS), an official coding system developed by the National Institute of Standards and Technology (NIST, formerly the National Bureau of Standards), U.S. Department of Commerce, and maintained by the U.S. Geological Survey (USGS). The FIPS state alpha code for each U.S. states and the District of Columbia is identical to the postal abbreviations by the United States Postal Service. Since September 3, 1987, the same has been true of the alpha code for each of the outlying areas, with the exception of U.S. Minor Outlying Islands (UM) as the USPS routes mail for these islands indirectly.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOCode FIPSStateAlphaCode;

        /// <summary>
        /// Gets or sets a value indicating whether the FIPSStateAlphaCode element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FIPSStateAlphaCode element has been assigned a value.</value>
        [XmlIgnore]
        public bool FIPSStateAlphaCodeSpecified
        {
            get { return FIPSStateAlphaCode != null; }
            set { }
        }

        /// <summary>
        /// A unique two digit numeric code to identify each State and State equivalent entity.  These State codes are part of the Federal Information Processing Standards (FIPS), an official coding system developed by the National Institute of Standards and Technology (NIST, formerly the National Bureau of Standards), U.S. Department of Commerce, and maintained by the U.S. Geological Survey (USGS).
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOCode FIPSStateNumericCode;

        /// <summary>
        /// Gets or sets a value indicating whether the FIPSStateNumericCode element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FIPSStateNumericCode element has been assigned a value.</value>
        [XmlIgnore]
        public bool FIPSStateNumericCodeSpecified
        {
            get { return FIPSStateNumericCode != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 9)]
        public FIPS_INFORMATION_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
