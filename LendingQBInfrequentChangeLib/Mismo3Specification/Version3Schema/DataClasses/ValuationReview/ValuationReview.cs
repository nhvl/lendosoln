namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class VALUATION_REVIEW
    {
        /// <summary>
        /// Gets a value indicating whether the VALUATION_REVIEW container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ValuationReviewCategoriesSpecified
                    || this.ValuationReviewDetailSpecified;
            }
        }

        /// <summary>
        /// Categories of a valuation review.
        /// </summary>
        [XmlElement("VALUATION_REVIEW_CATEGORIES", Order = 0)]
        public VALUATION_REVIEW_CATEGORIES ValuationReviewCategories;

        /// <summary>
        /// Gets or sets a value indicating whether the ValuationReviewCategories element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ValuationReviewCategories element has been assigned a value.</value>
        [XmlIgnore]
        public bool ValuationReviewCategoriesSpecified
        {
            get { return this.ValuationReviewCategories != null && this.ValuationReviewCategories.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details on a valuation review.
        /// </summary>
        [XmlElement("VALUATION_REVIEW_DETAIL", Order = 1)]
        public VALUATION_REVIEW_DETAIL ValuationReviewDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the ValuationReviewDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ValuationReviewDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool ValuationReviewDetailSpecified
        {
            get { return this.ValuationReviewDetail != null && this.ValuationReviewDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public VALUATION_REVIEW_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
