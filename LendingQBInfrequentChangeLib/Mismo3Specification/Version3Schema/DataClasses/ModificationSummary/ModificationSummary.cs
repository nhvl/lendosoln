namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class MODIFICATION_SUMMARY
    {
        /// <summary>
        /// Gets a value indicating whether the MODIFICATION_SUMMARY container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.LifeOfLoanTotalForgivenInterestAmountSpecified
                    || this.LifeOfLoanTotalForgivenPrincipalAmountSpecified;
            }
        }

        /// <summary>
        /// Cumulative total amount of interest forgiven over the life of the loan.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount LifeOfLoanTotalForgivenInterestAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the LifeOfLoanTotalForgivenInterestAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LifeOfLoanTotalForgivenInterestAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool LifeOfLoanTotalForgivenInterestAmountSpecified
        {
            get { return LifeOfLoanTotalForgivenInterestAmount != null; }
            set { }
        }

        /// <summary>
        /// Cumulative total amount of principal forgiven over the life of the loan.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOAmount LifeOfLoanTotalForgivenPrincipalAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the LifeOfLoanTotalForgivenPrincipalAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LifeOfLoanTotalForgivenPrincipalAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool LifeOfLoanTotalForgivenPrincipalAmountSpecified
        {
            get { return LifeOfLoanTotalForgivenPrincipalAmount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public MODIFICATION_SUMMARY_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
