namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class ROOM_FEATURES
    {
        /// <summary>
        /// Gets a value indicating whether the ROOM_FEATURES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.RoomFeatureSpecified;
            }
        }

        /// <summary>
        /// A collection of room features.
        /// </summary>
        [XmlElement("ROOM_FEATURE", Order = 0)]
		public List<ROOM_FEATURE> RoomFeature = new List<ROOM_FEATURE>();

        /// <summary>
        /// Gets or sets a value indicating whether the RoomFeature element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RoomFeature element has been assigned a value.</value>
        [XmlIgnore]
        public bool RoomFeatureSpecified
        {
            get { return this.RoomFeature != null && this.RoomFeature.Count(r => r != null && r.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public ROOM_FEATURES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
