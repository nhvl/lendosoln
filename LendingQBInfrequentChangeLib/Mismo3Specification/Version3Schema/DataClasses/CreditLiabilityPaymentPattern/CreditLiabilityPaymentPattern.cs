namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CREDIT_LIABILITY_PAYMENT_PATTERN
    {
        /// <summary>
        /// Gets a value indicating whether the CREDIT_LIABILITY_PAYMENT_PATTERN container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditLiabilityPaymentPatternDataTextSpecified
                    || this.CreditLiabilityPaymentPatternStartDateSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// This is a set of previous monthly payment ratings beginning with the Payment Pattern Start Date. C=Current, 1-6=# Months Late, 7=Wage Earner Plan, 8=Repossession, 9=Collection, N=No Activity, X=No Data.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString CreditLiabilityPaymentPatternDataText;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLiabilityPaymentPatternDataText element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLiabilityPaymentPatternDataText element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLiabilityPaymentPatternDataTextSpecified
        {
            get { return CreditLiabilityPaymentPatternDataText != null; }
            set { }
        }

        /// <summary>
        /// The date of the beginning of the data in the Credit Liability Payment Pattern Data.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMODate CreditLiabilityPaymentPatternStartDate;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLiabilityPaymentPatternStartDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLiabilityPaymentPatternStartDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLiabilityPaymentPatternStartDateSpecified
        {
            get { return CreditLiabilityPaymentPatternStartDate != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public CREDIT_LIABILITY_PAYMENT_PATTERN_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
