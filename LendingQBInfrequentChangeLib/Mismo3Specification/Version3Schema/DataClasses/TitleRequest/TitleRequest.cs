namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class TITLE_REQUEST
    {
        /// <summary>
        /// Gets a value indicating whether the TITLE_REQUEST container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExecutionsSpecified
                    || this.ExtensionSpecified
                    || this.TitleRequestDetailSpecified;
            }
        }

        /// <summary>
        /// Execution of a title request.
        /// </summary>
        [XmlElement("EXECUTIONS", Order = 0)]
        public EXECUTIONS Executions;

        /// <summary>
        /// Gets or sets a value indicating whether the Executions element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Executions element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExecutionsSpecified
        {
            get { return this.Executions != null && this.Executions.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details about a title request.
        /// </summary>
        [XmlElement("TITLE_REQUEST_DETAIL", Order = 1)]
        public TITLE_REQUEST_DETAIL TitleRequestDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the TitleRequestDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TitleRequestDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool TitleRequestDetailSpecified
        {
            get { return this.TitleRequestDetail != null && this.TitleRequestDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public TITLE_REQUEST_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
