namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class TITLE_REQUEST_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the TITLE_REQUEST_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.InsuredNameSpecified
                    || this.NAICTitlePolicyClassificationTypeSpecified
                    || this.NamedInsuredTypeSpecified
                    || this.ProcessorIdentifierSpecified
                    || this.RequestedClosingDateSpecified
                    || this.RequestedClosingTimeSpecified
                    || this.TitleAgentValidationReasonTypeOtherDescriptionSpecified
                    || this.TitleAgentValidationReasonTypeSpecified
                    || this.TitleAssociationTypeOtherDescriptionSpecified
                    || this.TitleAssociationTypeSpecified
                    || this.TitleOfficeIdentifierSpecified
                    || this.TitleOwnershipTypeOtherDescriptionSpecified
                    || this.TitleOwnershipTypeSpecified
                    || this.TitleRequestActionTypeSpecified
                    || this.TitleRequestCommentDescriptionSpecified
                    || this.TitleRequestProposedTitleInsuranceCoverageAmountSpecified
                    || this.VendorOrderIdentifierSpecified
                    || this.VendorTransactionIdentifierSpecified;
            }
        }

        /// <summary>
        /// Identifies the individual or corporation who will be named as the insured party on the policy.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString InsuredName;

        /// <summary>
        /// Gets or sets a value indicating whether the InsuredName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InsuredName element has been assigned a value.</value>
        [XmlIgnore]
        public bool InsuredNameSpecified
        {
            get { return InsuredName != null; }
            set { }
        }

        /// <summary>
        /// NAIC-defined classification for policies insuring title to real property as either residential or nonresidential. 
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<NAICTitlePolicyClassificationBase> NAICTitlePolicyClassificationType;

        /// <summary>
        /// Gets or sets a value indicating whether the NAICTitlePolicyClassificationType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NAICTitlePolicyClassificationType element has been assigned a value.</value>
        [XmlIgnore]
        public bool NAICTitlePolicyClassificationTypeSpecified
        {
            get { return this.NAICTitlePolicyClassificationType != null && this.NAICTitlePolicyClassificationType.enumValue != NAICTitlePolicyClassificationBase.Blank; }
            set { }
        }

        /// <summary>
        /// Designates the type of entity that will be the named insured on the Title Insurance Policy document.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<NamedInsuredBase> NamedInsuredType;

        /// <summary>
        /// Gets or sets a value indicating whether the NamedInsuredType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NamedInsuredType element has been assigned a value.</value>
        [XmlIgnore]
        public bool NamedInsuredTypeSpecified
        {
            get { return this.NamedInsuredType != null && this.NamedInsuredType.enumValue != NamedInsuredBase.Blank; }
            set { }
        }

        /// <summary>
        /// A string that uniquely identifies the specific person that will be processing the order as agreed upon by the parties to the transaction.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOIdentifier ProcessorIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the ProcessorIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProcessorIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProcessorIdentifierSpecified
        {
            get { return ProcessorIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The requested date for when the loan will be closed with the borrower.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMODate RequestedClosingDate;

        /// <summary>
        /// Gets or sets a value indicating whether the RequestedClosingDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RequestedClosingDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool RequestedClosingDateSpecified
        {
            get { return RequestedClosingDate != null; }
            set { }
        }

        /// <summary>
        /// The requested time on the the requested date for when the loan will be closed with the borrower.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOTime RequestedClosingTime;

        /// <summary>
        /// Gets or sets a value indicating whether the RequestedClosingTime element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RequestedClosingTime element has been assigned a value.</value>
        [XmlIgnore]
        public bool RequestedClosingTimeSpecified
        {
            get { return RequestedClosingTime != null; }
            set { }
        }

        /// <summary>
        /// This is typically referred to as the Reason Code and is the element that further qualifies the validation response received.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOEnum<TitleAgentValidationReasonBase> TitleAgentValidationReasonType;

        /// <summary>
        /// Gets or sets a value indicating whether the TitleAgentValidationReasonType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TitleAgentValidationReasonType element has been assigned a value.</value>
        [XmlIgnore]
        public bool TitleAgentValidationReasonTypeSpecified
        {
            get { return this.TitleAgentValidationReasonType != null && this.TitleAgentValidationReasonType.enumValue != TitleAgentValidationReasonBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Other is selected for Title Agent Validation Reason Type.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOString TitleAgentValidationReasonTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the TitleAgentValidationReasonTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TitleAgentValidationReasonTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool TitleAgentValidationReasonTypeOtherDescriptionSpecified
        {
            get { return TitleAgentValidationReasonTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Identifies the national title association.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOEnum<TitleAssociationBase> TitleAssociationType;

        /// <summary>
        /// Gets or sets a value indicating whether the TitleAssociationType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TitleAssociationType element has been assigned a value.</value>
        [XmlIgnore]
        public bool TitleAssociationTypeSpecified
        {
            get { return this.TitleAssociationType != null && this.TitleAssociationType.enumValue != TitleAssociationBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the Title Association if Other is selected as the Title Association Type.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOString TitleAssociationTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the TitleAssociationTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TitleAssociationTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool TitleAssociationTypeOtherDescriptionSpecified
        {
            get { return TitleAssociationTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Identifies the Title Office intended to fulfill the title request.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOIdentifier TitleOfficeIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the TitleOfficeIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TitleOfficeIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool TitleOfficeIdentifierSpecified
        {
            get { return TitleOfficeIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Specifies how ownership for the Title Insurance Policy document will be held.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOEnum<TitleOwnershipBase> TitleOwnershipType;

        /// <summary>
        /// Gets or sets a value indicating whether the TitleOwnershipType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TitleOwnershipType element has been assigned a value.</value>
        [XmlIgnore]
        public bool TitleOwnershipTypeSpecified
        {
            get { return this.TitleOwnershipType != null && this.TitleOwnershipType.enumValue != TitleOwnershipBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture additional information when Other is selected for Title Ownership Type.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOString TitleOwnershipTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the TitleOwnershipTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TitleOwnershipTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool TitleOwnershipTypeOtherDescriptionSpecified
        {
            get { return TitleOwnershipTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Describes the action requested for a particular title order.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOEnum<TitleRequestActionBase> TitleRequestActionType;

        /// <summary>
        /// Gets or sets a value indicating whether the TitleRequestActionType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TitleRequestActionType element has been assigned a value.</value>
        [XmlIgnore]
        public bool TitleRequestActionTypeSpecified
        {
            get { return this.TitleRequestActionType != null && this.TitleRequestActionType.enumValue != TitleRequestActionBase.Blank; }
            set { }
        }

        /// <summary>
        /// Any text comments sent by the ordering party in a title request.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOString TitleRequestCommentDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the TitleRequestCommentDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TitleRequestCommentDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool TitleRequestCommentDescriptionSpecified
        {
            get { return TitleRequestCommentDescription != null; }
            set { }
        }

        /// <summary>
        /// The amount of coverage proposed for the requested insurance product by the requestor based on the sales price or loan amount.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOAmount TitleRequestProposedTitleInsuranceCoverageAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the TitleRequestProposedTitleInsuranceCoverageAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TitleRequestProposedTitleInsuranceCoverageAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TitleRequestProposedTitleInsuranceCoverageAmountSpecified
        {
            get { return TitleRequestProposedTitleInsuranceCoverageAmount != null; }
            set { }
        }

        /// <summary>
        /// A string that uniquely identifies a specific order for a vendor set by the vendor.
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMOIdentifier VendorOrderIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the VendorOrderIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the VendorOrderIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool VendorOrderIdentifierSpecified
        {
            get { return VendorOrderIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A string that uniquely identifies a specific transaction/file for a vendor set by the vendor.
        /// </summary>
        [XmlElement(Order = 17)]
        public MISMOIdentifier VendorTransactionIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the VendorTransactionIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the VendorTransactionIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool VendorTransactionIdentifierSpecified
        {
            get { return VendorTransactionIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 18)]
        public TITLE_REQUEST_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
