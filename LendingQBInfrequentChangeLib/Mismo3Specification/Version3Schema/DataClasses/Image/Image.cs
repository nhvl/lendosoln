namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class IMAGE
    {
        /// <summary>
        /// Gets a value indicating whether the IMAGE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ImageCaptionCommentDescriptionSpecified
                    || this.ImageDateSpecified
                    || this.ImageGroupNameSpecified
                    || this.ImageGroupSequenceNumberSpecified
                    || this.ImageIdentifierSpecified
                    || this.ImageNameSpecified;
            }
        }

        /// <summary>
        /// A free-form text field used to further describe or comment on the image.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString ImageCaptionCommentDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ImageCaptionCommentDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ImageCaptionCommentDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ImageCaptionCommentDescriptionSpecified
        {
            get { return ImageCaptionCommentDescription != null; }
            set { }
        }

        /// <summary>
        /// The date when the image was taken.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMODate ImageDate;

        /// <summary>
        /// Gets or sets a value indicating whether the ImageDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ImageDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ImageDateSpecified
        {
            get { return ImageDate != null; }
            set { }
        }

        /// <summary>
        /// Name of group represented by the images.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString ImageGroupName;

        /// <summary>
        /// Gets or sets a value indicating whether the ImageGroupName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ImageGroupName element has been assigned a value.</value>
        [XmlIgnore]
        public bool ImageGroupNameSpecified
        {
            get { return ImageGroupName != null; }
            set { }
        }

        /// <summary>
        /// Uniquely sequence the images within the named group.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOSequenceNumber ImageGroupSequenceNumber;

        /// <summary>
        /// Gets or sets a value indicating whether the ImageGroupSequenceNumber element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ImageGroupSequenceNumber element has been assigned a value.</value>
        [XmlIgnore]
        public bool ImageGroupSequenceNumberSpecified
        {
            get { return ImageGroupSequenceNumber != null; }
            set { }
        }

        /// <summary>
        /// An identifier that may be used to uniquely identify an image.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOIdentifier ImageIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the ImageIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ImageIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool ImageIdentifierSpecified
        {
            get { return ImageIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Indicates the name or title of the image (e.g., Subject Photo).
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString ImageName;

        /// <summary>
        /// Gets or sets a value indicating whether the ImageName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ImageName element has been assigned a value.</value>
        [XmlIgnore]
        public bool ImageNameSpecified
        {
            get { return ImageName != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 6)]
        public IMAGE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
