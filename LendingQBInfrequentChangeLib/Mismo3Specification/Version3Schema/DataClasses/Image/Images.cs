namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class IMAGES
    {
        /// <summary>
        /// Gets a value indicating whether the IMAGES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ImageSpecified;
            }
        }

        /// <summary>
        /// A collection of images.
        /// </summary>
        [XmlElement("IMAGE", Order = 0)]
		public List<IMAGE> Image = new List<IMAGE>();

        /// <summary>
        /// Gets or sets a value indicating whether the Image element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Image element has been assigned a value.</value>
        [XmlIgnore]
        public bool ImageSpecified
        {
            get { return this.Image != null && this.Image.Count(i => i != null && i.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public IMAGES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
