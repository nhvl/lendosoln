namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class VERIFICATION_DATA_PAYEE
    {
        /// <summary>
        /// Gets a value indicating whether the VERIFICATION_DATA_PAYEE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.PayeeTotalCountSpecified;
            }
        }

        /// <summary>
        /// Provides a count of the Payee containers within this data set.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOCount PayeeTotalCount;

        /// <summary>
        /// Gets or sets a value indicating whether the PayeeTotalCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PayeeTotalCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PayeeTotalCountSpecified
        {
            get { return PayeeTotalCount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public VERIFICATION_DATA_PAYEE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
