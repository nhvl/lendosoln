namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class SKIP_PAYMENT
    {
        /// <summary>
        /// Gets a value indicating whether the SKIP_PAYMENT container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.SkipPaymentActionTypeOtherDescriptionSpecified
                    || this.SkipPaymentActionTypeSpecified
                    || this.SkipPaymentInitialRestrictionTermMonthsCountSpecified
                    || this.SkippedPaymentCountSpecified;
            }
        }

        /// <summary>
        /// Specifies the action to be taken when a skip payment feature is exercised.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<SkipPaymentActionBase> SkipPaymentActionType;

        /// <summary>
        /// Gets or sets a value indicating whether the SkipPaymentActionType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SkipPaymentActionType element has been assigned a value.</value>
        [XmlIgnore]
        public bool SkipPaymentActionTypeSpecified
        {
            get { return this.SkipPaymentActionType != null && this.SkipPaymentActionType.enumValue != SkipPaymentActionBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Skip Payment Action Type.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString SkipPaymentActionTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the SkipPaymentActionTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SkipPaymentActionTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool SkipPaymentActionTypeOtherDescriptionSpecified
        {
            get { return SkipPaymentActionTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The number of months from origination during which the skip payment feature may not be exercised.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOCount SkipPaymentInitialRestrictionTermMonthsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the SkipPaymentInitialRestrictionTermMonthsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SkipPaymentInitialRestrictionTermMonthsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool SkipPaymentInitialRestrictionTermMonthsCountSpecified
        {
            get { return SkipPaymentInitialRestrictionTermMonthsCount != null; }
            set { }
        }

        /// <summary>
        /// The total number of loan payments that a borrower has skipped since loan origination.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOCount SkippedPaymentCount;

        /// <summary>
        /// Gets or sets a value indicating whether the SkippedPaymentCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SkippedPaymentCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool SkippedPaymentCountSpecified
        {
            get { return SkippedPaymentCount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public SKIP_PAYMENT_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
