namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PRINCIPAL_AND_INTEREST_PAYMENT_LIFETIME_ADJUSTMENT_RULE
    {
        /// <summary>
        /// Gets a value indicating whether the PRINCIPAL_AND_INTEREST_PAYMENT_LIFETIME_ADJUSTMENT_RULE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.FinalPrincipalAndInterestPaymentChangeDateSpecified
                    || this.FirstPrincipalAndInterestPaymentChangeDateSpecified
                    || this.FirstPrincipalAndInterestPaymentChangeMonthsCountSpecified
                    || this.GEMPayoffYearsCountSpecified
                    || this.GPMAndGEMPrincipalAndInterestPaymentChangesCountSpecified
                    || this.GPMMultiplierRatePercentSpecified
                    || this.InitialPaymentDiscountPercentSpecified
                    || this.PaymentAdjustmentLifetimeCapAmountSpecified
                    || this.PaymentAdjustmentLifetimeCapPercentSpecified
                    || this.PaymentsBetweenPrincipalAndInterestPaymentChangesCountSpecified
                    || this.PrincipalAndInterestCalculationPaymentPeriodTypeOtherDescriptionSpecified
                    || this.PrincipalAndInterestCalculationPaymentPeriodTypeSpecified
                    || this.PrincipalAndInterestPaymentDecreaseCapTypeSpecified
                    || this.PrincipalAndInterestPaymentFinalRecastTypeOtherDescriptionSpecified
                    || this.PrincipalAndInterestPaymentFinalRecastTypeSpecified
                    || this.PrincipalAndInterestPaymentIncreaseCapTypeSpecified
                    || this.PrincipalAndInterestPaymentMaximumAmountEarliestEffectiveMonthsCountSpecified
                    || this.PrincipalAndInterestPaymentMaximumAmountSpecified
                    || this.PrincipalAndInterestPaymentMaximumDecreaseRatePercentSpecified
                    || this.PrincipalAndInterestPaymentMaximumExtensionCountSpecified
                    || this.PrincipalAndInterestPaymentMinimumAmountSpecified
                    || this.PrincipalBalanceCalculationMethodTypeSpecified;
            }
        }

        /// <summary>
        /// The last date that a scheduled principal and interest payment change will occur. 
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMODate FinalPrincipalAndInterestPaymentChangeDate;

        /// <summary>
        /// Gets or sets a value indicating whether the FinalPrincipalAndInterestPaymentChangeDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FinalPrincipalAndInterestPaymentChangeDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool FinalPrincipalAndInterestPaymentChangeDateSpecified
        {
            get { return FinalPrincipalAndInterestPaymentChangeDate != null; }
            set { }
        }

        /// <summary>
        /// The date of the first scheduled principal and interest payment change. This date typically occurs in an ARM transaction.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMODate FirstPrincipalAndInterestPaymentChangeDate;

        /// <summary>
        /// Gets or sets a value indicating whether the FirstPrincipalAndInterestPaymentChangeDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FirstPrincipalAndInterestPaymentChangeDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool FirstPrincipalAndInterestPaymentChangeDateSpecified
        {
            get { return FirstPrincipalAndInterestPaymentChangeDate != null; }
            set { }
        }

        /// <summary>
        /// The number of months after origination in which the first payment adjustment occurs.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOCount FirstPrincipalAndInterestPaymentChangeMonthsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the FirstPrincipalAndInterestPaymentChangeMonthsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FirstPrincipalAndInterestPaymentChangeMonthsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool FirstPrincipalAndInterestPaymentChangeMonthsCountSpecified
        {
            get { return FirstPrincipalAndInterestPaymentChangeMonthsCount != null; }
            set { }
        }

        /// <summary>
        /// The number of years in which a Growing Equity Mortgage (GEM) is calculated to be fully repaid using the original payment amount.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOCount GEMPayoffYearsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the GEMPayoffYearsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GEMPayoffYearsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool GEMPayoffYearsCountSpecified
        {
            get { return GEMPayoffYearsCount != null; }
            set { }
        }

        /// <summary>
        /// The number of principal and interest payment changes dictated on the loan document for a Graduated Payment Mortgage (GPM) or Growth Equity Mortgage (GEM) loan.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOCount GPMAndGEMPrincipalAndInterestPaymentChangesCount;

        /// <summary>
        /// Gets or sets a value indicating whether the GPMAndGEMPrincipalAndInterestPaymentChangesCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GPMAndGEMPrincipalAndInterestPaymentChangesCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool GPMAndGEMPrincipalAndInterestPaymentChangesCountSpecified
        {
            get { return GPMAndGEMPrincipalAndInterestPaymentChangesCount != null; }
            set { }
        }

        /// <summary>
        /// The percent used to calculate the new monthly Graduated Payment Amount.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOPercent GPMMultiplierRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the GPMMultiplierRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GPMMultiplierRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool GPMMultiplierRatePercentSpecified
        {
            get { return GPMMultiplierRatePercent != null; }
            set { }
        }

        /// <summary>
        /// Percent value used to calculate the discounted initial payment amount when it is not the fully-amortized amount.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOPercent InitialPaymentDiscountPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the InitialPaymentDiscountPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InitialPaymentDiscountPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool InitialPaymentDiscountPercentSpecified
        {
            get { return InitialPaymentDiscountPercent != null; }
            set { }
        }

        /// <summary>
        /// The maximum dollar amount increase to the original principal and interest payment allowed during the life of the loan.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOAmount PaymentAdjustmentLifetimeCapAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PaymentAdjustmentLifetimeCapAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PaymentAdjustmentLifetimeCapAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PaymentAdjustmentLifetimeCapAmountSpecified
        {
            get { return PaymentAdjustmentLifetimeCapAmount != null; }
            set { }
        }

        /// <summary>
        /// The maximum percentage of the payment increase allowed during the life of the loan. Expressed as percentage of the payment amount.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOPercent PaymentAdjustmentLifetimeCapPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the PaymentAdjustmentLifetimeCapPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PaymentAdjustmentLifetimeCapPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool PaymentAdjustmentLifetimeCapPercentSpecified
        {
            get { return PaymentAdjustmentLifetimeCapPercent != null; }
            set { }
        }

        /// <summary>
        /// The number of payments between scheduled principal and interest payment changes.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOCount PaymentsBetweenPrincipalAndInterestPaymentChangesCount;

        /// <summary>
        /// Gets or sets a value indicating whether the PaymentsBetweenPrincipalAndInterestPaymentChangesCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PaymentsBetweenPrincipalAndInterestPaymentChangesCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PaymentsBetweenPrincipalAndInterestPaymentChangesCountSpecified
        {
            get { return PaymentsBetweenPrincipalAndInterestPaymentChangesCount != null; }
            set { }
        }

        /// <summary>
        /// Specifies the payment period used to calculate the principal and interest payment.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOEnum<PrincipalAndInterestCalculationPaymentPeriodBase> PrincipalAndInterestCalculationPaymentPeriodType;

        /// <summary>
        /// Gets or sets a value indicating whether the PrincipalAndInterestCalculationPaymentPeriodType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrincipalAndInterestCalculationPaymentPeriodType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrincipalAndInterestCalculationPaymentPeriodTypeSpecified
        {
            get { return this.PrincipalAndInterestCalculationPaymentPeriodType != null && this.PrincipalAndInterestCalculationPaymentPeriodType.enumValue != PrincipalAndInterestCalculationPaymentPeriodBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Principal And Interest Calculation Payment Period Type.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOString PrincipalAndInterestCalculationPaymentPeriodTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PrincipalAndInterestCalculationPaymentPeriodTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrincipalAndInterestCalculationPaymentPeriodTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrincipalAndInterestCalculationPaymentPeriodTypeOtherDescriptionSpecified
        {
            get { return PrincipalAndInterestCalculationPaymentPeriodTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Defines the optionality of a principal and interest payment cap when the principal and interest payment is adjusting downward.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOEnum<PrincipalAndInterestPaymentDecreaseCapBase> PrincipalAndInterestPaymentDecreaseCapType;

        /// <summary>
        /// Gets or sets a value indicating whether the PrincipalAndInterestPaymentDecreaseCapType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrincipalAndInterestPaymentDecreaseCapType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrincipalAndInterestPaymentDecreaseCapTypeSpecified
        {
            get { return this.PrincipalAndInterestPaymentDecreaseCapType != null && this.PrincipalAndInterestPaymentDecreaseCapType.enumValue != PrincipalAndInterestPaymentDecreaseCapBase.Blank; }
            set { }
        }

        /// <summary>
        /// Describes the principal and interest recast behavior at the end of the loan term.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOEnum<PrincipalAndInterestPaymentFinalRecastBase> PrincipalAndInterestPaymentFinalRecastType;

        /// <summary>
        /// Gets or sets a value indicating whether the PrincipalAndInterestPaymentFinalRecastType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrincipalAndInterestPaymentFinalRecastType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrincipalAndInterestPaymentFinalRecastTypeSpecified
        {
            get { return this.PrincipalAndInterestPaymentFinalRecastType != null && this.PrincipalAndInterestPaymentFinalRecastType.enumValue != PrincipalAndInterestPaymentFinalRecastBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Principal and Interest Payment Final Recast Type.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOString PrincipalAndInterestPaymentFinalRecastTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PrincipalAndInterestPaymentFinalRecastTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrincipalAndInterestPaymentFinalRecastTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrincipalAndInterestPaymentFinalRecastTypeOtherDescriptionSpecified
        {
            get { return PrincipalAndInterestPaymentFinalRecastTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Defines the optionality of a principal and interest payment cap when the principal and interest payment is adjusting upward.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOEnum<PrincipalAndInterestPaymentIncreaseCapBase> PrincipalAndInterestPaymentIncreaseCapType;

        /// <summary>
        /// Gets or sets a value indicating whether the PrincipalAndInterestPaymentIncreaseCapType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrincipalAndInterestPaymentIncreaseCapType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrincipalAndInterestPaymentIncreaseCapTypeSpecified
        {
            get { return this.PrincipalAndInterestPaymentIncreaseCapType != null && this.PrincipalAndInterestPaymentIncreaseCapType.enumValue != PrincipalAndInterestPaymentIncreaseCapBase.Blank; }
            set { }
        }

        /// <summary>
        /// The stated maximum principal and interest payment amount allowed over the life of the loan.
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMOAmount PrincipalAndInterestPaymentMaximumAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PrincipalAndInterestPaymentMaximumAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrincipalAndInterestPaymentMaximumAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrincipalAndInterestPaymentMaximumAmountSpecified
        {
            get { return PrincipalAndInterestPaymentMaximumAmount != null; }
            set { }
        }

        /// <summary>
        /// The minimum number of months before the maximum payment amount to which the principal and interest payment can increase over the life of the loan could be reached.
        /// </summary>
        [XmlElement(Order = 17)]
        public MISMOCount PrincipalAndInterestPaymentMaximumAmountEarliestEffectiveMonthsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the PrincipalAndInterestPaymentMaximumAmountEarliestEffectiveMonthsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrincipalAndInterestPaymentMaximumAmountEarliestEffectiveMonthsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrincipalAndInterestPaymentMaximumAmountEarliestEffectiveMonthsCountSpecified
        {
            get { return PrincipalAndInterestPaymentMaximumAmountEarliestEffectiveMonthsCount != null; }
            set { }
        }

        /// <summary>
        /// The maximum number of percentage points by which the principal and interest payment can decrease from the original principal and interest payment over the life of the loan.
        /// </summary>
        [XmlElement(Order = 18)]
        public MISMOPercent PrincipalAndInterestPaymentMaximumDecreaseRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the PrincipalAndInterestPaymentMaximumDecreaseRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrincipalAndInterestPaymentMaximumDecreaseRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrincipalAndInterestPaymentMaximumDecreaseRatePercentSpecified
        {
            get { return PrincipalAndInterestPaymentMaximumDecreaseRatePercent != null; }
            set { }
        }

        /// <summary>
        /// The maximum number of payments by which the loan term can be extended for the purposes of keeping the Principle and Interest payment constant as long as possible.
        /// </summary>
        [XmlElement(Order = 19)]
        public MISMOCount PrincipalAndInterestPaymentMaximumExtensionCount;

        /// <summary>
        /// Gets or sets a value indicating whether the PrincipalAndInterestPaymentMaximumExtensionCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrincipalAndInterestPaymentMaximumExtensionCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrincipalAndInterestPaymentMaximumExtensionCountSpecified
        {
            get { return PrincipalAndInterestPaymentMaximumExtensionCount != null; }
            set { }
        }

        /// <summary>
        /// The stated lowest principal and interest payment amount allowed over the life of the loan.
        /// </summary>
        [XmlElement(Order = 20)]
        public MISMOAmount PrincipalAndInterestPaymentMinimumAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PrincipalAndInterestPaymentMinimumAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrincipalAndInterestPaymentMinimumAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrincipalAndInterestPaymentMinimumAmountSpecified
        {
            get { return PrincipalAndInterestPaymentMinimumAmount != null; }
            set { }
        }

        /// <summary>
        /// Indicates the method used to determine the principal balance to be used to compute the new principal and interest payment.
        /// </summary>
        [XmlElement(Order = 21)]
        public MISMOEnum<PrincipalBalanceCalculationMethodBase> PrincipalBalanceCalculationMethodType;

        /// <summary>
        /// Gets or sets a value indicating whether the PrincipalBalanceCalculationMethodType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrincipalBalanceCalculationMethodType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrincipalBalanceCalculationMethodTypeSpecified
        {
            get { return this.PrincipalBalanceCalculationMethodType != null && this.PrincipalBalanceCalculationMethodType.enumValue != PrincipalBalanceCalculationMethodBase.Blank; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 22)]
        public PRINCIPAL_AND_INTEREST_PAYMENT_LIFETIME_ADJUSTMENT_RULE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
