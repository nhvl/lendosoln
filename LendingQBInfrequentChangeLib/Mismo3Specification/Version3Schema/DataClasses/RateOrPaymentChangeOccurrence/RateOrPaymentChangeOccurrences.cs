namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class RATE_OR_PAYMENT_CHANGE_OCCURRENCES
    {
        /// <summary>
        /// Gets a value indicating whether the RATE_OR_PAYMENT_CHANGE_OCCURRENCES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.RateOrPaymentChangeOccurrenceSpecified;
            }
        }

        /// <summary>
        /// A collection of rate or payment change occurrences.
        /// </summary>
        [XmlElement("RATE_OR_PAYMENT_CHANGE_OCCURRENCE", Order = 0)]
		public List<RATE_OR_PAYMENT_CHANGE_OCCURRENCE> RateOrPaymentChangeOccurrence = new List<RATE_OR_PAYMENT_CHANGE_OCCURRENCE>();

        /// <summary>
        /// Gets or sets a value indicating whether the RateOrPaymentChangeOccurrence element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RateOrPaymentChangeOccurrence element has been assigned a value.</value>
        [XmlIgnore]
        public bool RateOrPaymentChangeOccurrenceSpecified
        {
            get { return this.RateOrPaymentChangeOccurrence != null && this.RateOrPaymentChangeOccurrence.Count(r => r != null && r.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public RATE_OR_PAYMENT_CHANGE_OCCURRENCES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
