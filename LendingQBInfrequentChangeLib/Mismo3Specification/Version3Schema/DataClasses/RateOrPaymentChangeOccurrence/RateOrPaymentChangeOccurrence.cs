namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class RATE_OR_PAYMENT_CHANGE_OCCURRENCE
    {
        /// <summary>
        /// Gets a value indicating whether the RATE_OR_PAYMENT_CHANGE_OCCURRENCE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AdjustmentChangeEffectiveDueDateSpecified
                    || this.AdjustmentChangeIndexRatePercentSpecified
                    || this.AdjustmentChangeInterestRatePercentSpecified
                    || this.AdjustmentChangeOccurrenceExtendedTermMonthsCountSpecified
                    || this.AdjustmentChangeOccurrencePassThroughRatePercentSpecified
                    || this.AdjustmentChangeOccurrenceTypeOtherDescriptionSpecified
                    || this.AdjustmentChangeOccurrenceTypeSpecified
                    || this.AdjustmentChangePrincipalAndInterestPaymentAmountSpecified
                    || this.AdjustmentChangeProjectedPrincipalBalanceAmountSpecified
                    || this.AdjustmentChangeServiceFeeAmountSpecified
                    || this.AdjustmentChangeServiceFeeRatePercentSpecified
                    || this.AdjustmentRuleTypeSpecified
                    || this.BalloonResetDateSpecified
                    || this.CarryoverRatePercentSpecified
                    || this.ChangeOccurrenceExtendedTermMonthsCountSpecified
                    || this.ConvertibleStatusTypeSpecified
                    || this.ExtensionSpecified
                    || this.LastRateChangeNotificationDateSpecified
                    || this.LatestConversionEffectiveDateSpecified
                    || this.NextConversionOptionEffectiveDateSpecified
                    || this.NextConversionOptionNoticeDateSpecified
                    || this.NextDemandConversionOptionNoticeDateSpecified
                    || this.NextIgnorePrincipalAndInterestPaymentAdjustmentCapsDateSpecified
                    || this.NextIgnoreRateAdjustmentCapsDateSpecified
                    || this.NextPrincipalAndInterestPaymentChangeEffectiveDateSpecified
                    || this.NextRateAdjustmentEffectiveDateSpecified
                    || this.NextRateAdjustmentEffectiveNoticeDateSpecified
                    || this.RateAdjustmentPercentSpecified
                    || this.ServicerARMPlanIdentifierSpecified;
            }
        }

        /// <summary>
        /// The effective payment due date of the Interest Rate, principal and interest Payment or Service Fee Change.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMODate AdjustmentChangeEffectiveDueDate;

        /// <summary>
        /// Gets or sets a value indicating whether the AdjustmentChangeEffectiveDueDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AdjustmentChangeEffectiveDueDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool AdjustmentChangeEffectiveDueDateSpecified
        {
            get { return AdjustmentChangeEffectiveDueDate != null; }
            set { }
        }

        /// <summary>
        /// The index value used to calculate the interest rate percent for this adjustment.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOPercent AdjustmentChangeIndexRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the AdjustmentChangeIndexRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AdjustmentChangeIndexRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool AdjustmentChangeIndexRatePercentSpecified
        {
            get { return AdjustmentChangeIndexRatePercent != null; }
            set { }
        }

        /// <summary>
        /// The calculated or pre-determined interest rate percent that will be effective on the Adjustment Change Effective Due Date.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOPercent AdjustmentChangeInterestRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the AdjustmentChangeInterestRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AdjustmentChangeInterestRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool AdjustmentChangeInterestRatePercentSpecified
        {
            get { return AdjustmentChangeInterestRatePercent != null; }
            set { }
        }

        /// <summary>
        /// For loans where the term of the loan can be extended rather than increasing the principal and interest payment, this is the new term of the loan including any extension.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOCount AdjustmentChangeOccurrenceExtendedTermMonthsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the AdjustmentChangeOccurrenceExtendedTermMonthsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AdjustmentChangeOccurrenceExtendedTermMonthsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool AdjustmentChangeOccurrenceExtendedTermMonthsCountSpecified
        {
            get { return AdjustmentChangeOccurrenceExtendedTermMonthsCount != null; }
            set { }
        }

        /// <summary>
        /// The new rate, expressed as a percent, at which the Servicer passes through interest on the loan to the investor. It can be computed using either a top-down formula: Loan Interest Rate - Servicing Fee Rate - Excess Yield Rate -Lender Guaranty Fee Rate, or a bottom-up formula: Index Rate + Net (a.k.a. Required) Margin Rate + Investor Guaranty Fee Rate + Yield Excess/Deficit Rate, and then applying any caps that are in effect.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOPercent AdjustmentChangeOccurrencePassThroughRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the AdjustmentChangeOccurrencePassThroughRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AdjustmentChangeOccurrencePassThroughRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool AdjustmentChangeOccurrencePassThroughRatePercentSpecified
        {
            get { return AdjustmentChangeOccurrencePassThroughRatePercent != null; }
            set { }
        }

        /// <summary>
        /// Indicates the reason for the Interest Rate, principal and interest Payment or Service Fee change on an existing mortgage loan.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOEnum<AdjustmentChangeOccurrenceBase> AdjustmentChangeOccurrenceType;

        /// <summary>
        /// Gets or sets a value indicating whether the AdjustmentChangeOccurrenceType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AdjustmentChangeOccurrenceType element has been assigned a value.</value>
        [XmlIgnore]
        public bool AdjustmentChangeOccurrenceTypeSpecified
        {
            get { return this.AdjustmentChangeOccurrenceType != null && this.AdjustmentChangeOccurrenceType.enumValue != AdjustmentChangeOccurrenceBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture the Adjustment Change Occurrence type if Other is selected.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOString AdjustmentChangeOccurrenceTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the AdjustmentChangeOccurrenceTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AdjustmentChangeOccurrenceTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool AdjustmentChangeOccurrenceTypeOtherDescriptionSpecified
        {
            get { return AdjustmentChangeOccurrenceTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of the principal and interest payment associated with the Adjustment Change Effective Due Date.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOAmount AdjustmentChangePrincipalAndInterestPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the AdjustmentChangePrincipalAndInterestPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AdjustmentChangePrincipalAndInterestPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool AdjustmentChangePrincipalAndInterestPaymentAmountSpecified
        {
            get { return AdjustmentChangePrincipalAndInterestPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// The projected principal balance amortized to the date for which interest rate, principal and interest payment, or convertible calculations were performed.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOAmount AdjustmentChangeProjectedPrincipalBalanceAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the AdjustmentChangeProjectedPrincipalBalanceAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AdjustmentChangeProjectedPrincipalBalanceAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool AdjustmentChangeProjectedPrincipalBalanceAmountSpecified
        {
            get { return AdjustmentChangeProjectedPrincipalBalanceAmount != null; }
            set { }
        }

        /// <summary>
        /// The recalculated fixed dollar amount that is paid to the servicer by the investor for servicing the mortgage if a service fee percent is not expressed.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOAmount AdjustmentChangeServiceFeeAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the AdjustmentChangeServiceFeeAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AdjustmentChangeServiceFeeAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool AdjustmentChangeServiceFeeAmountSpecified
        {
            get { return AdjustmentChangeServiceFeeAmount != null; }
            set { }
        }

        /// <summary>
        /// The recalculated percentage of interest collected that is paid to the servicer by the investor for servicing the mortgage (gross).
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOPercent AdjustmentChangeServiceFeeRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the AdjustmentChangeServiceFeeRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AdjustmentChangeServiceFeeRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool AdjustmentChangeServiceFeeRatePercentSpecified
        {
            get { return AdjustmentChangeServiceFeeRatePercent != null; }
            set { }
        }

        /// <summary>
        /// Specifies whether the occurrence of the adjustment is the first change or a subsequent change.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOEnum<AdjustmentRuleBase> AdjustmentRuleType;

        /// <summary>
        /// Gets or sets a value indicating whether the AdjustmentRuleType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AdjustmentRuleType element has been assigned a value.</value>
        [XmlIgnore]
        public bool AdjustmentRuleTypeSpecified
        {
            get { return this.AdjustmentRuleType != null && this.AdjustmentRuleType.enumValue != AdjustmentRuleBase.Blank; }
            set { }
        }

        /// <summary>
        /// The date the balloon loan was reset to the current market rate for the remainder of the amortization period.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMODate BalloonResetDate;

        /// <summary>
        /// Gets or sets a value indicating whether the BalloonResetDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BalloonResetDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool BalloonResetDateSpecified
        {
            get { return BalloonResetDate != null; }
            set { }
        }

        /// <summary>
        /// The cumulative unapplied difference between the calculated interest rate and the capped Interest Rate that can be used to modify the calculated interest rate at future interest rate change dates. Carryover Rate Percent is only present if Carryover Rate Indicator is true.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOPercent CarryoverRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the CarryoverRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CarryoverRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool CarryoverRatePercentSpecified
        {
            get { return CarryoverRatePercent != null; }
            set { }
        }

        /// <summary>
        /// For loans where the term of the loan can be extended rather than increasing the principal and interest payment, this is the new term of the loan including any extension.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOCount ChangeOccurrenceExtendedTermMonthsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the ChangeOccurrenceExtendedTermMonthsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ChangeOccurrenceExtendedTermMonthsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ChangeOccurrenceExtendedTermMonthsCountSpecified
        {
            get { return ChangeOccurrenceExtendedTermMonthsCount != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether the mortgagor has exercised the option to convert the ARM loan to a fixed rate loan.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOEnum<ConvertibleStatusBase> ConvertibleStatusType;

        /// <summary>
        /// Gets or sets a value indicating whether the ConvertibleStatusType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConvertibleStatusType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConvertibleStatusTypeSpecified
        {
            get { return this.ConvertibleStatusType != null && this.ConvertibleStatusType.enumValue != ConvertibleStatusBase.Blank; }
            set { }
        }

        /// <summary>
        /// Date of last ARM change notification mailed to Mortgagor.
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMODate LastRateChangeNotificationDate;

        /// <summary>
        /// Gets or sets a value indicating whether the LastRateChangeNotificationDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LastRateChangeNotificationDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool LastRateChangeNotificationDateSpecified
        {
            get { return LastRateChangeNotificationDate != null; }
            set { }
        }

        /// <summary>
        /// The most recent date on which a change in the terms of the loan, as described in the note, became effective.
        /// </summary>
        [XmlElement(Order = 17)]
        public MISMODate LatestConversionEffectiveDate;

        /// <summary>
        /// Gets or sets a value indicating whether the LatestConversionEffectiveDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LatestConversionEffectiveDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool LatestConversionEffectiveDateSpecified
        {
            get { return LatestConversionEffectiveDate != null; }
            set { }
        }

        /// <summary>
        /// The next payment due date for which the mortgage has the option to convert to a fixed rate loan.
        /// </summary>
        [XmlElement(Order = 18)]
        public MISMODate NextConversionOptionEffectiveDate;

        /// <summary>
        /// Gets or sets a value indicating whether the NextConversionOptionEffectiveDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NextConversionOptionEffectiveDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool NextConversionOptionEffectiveDateSpecified
        {
            get { return NextConversionOptionEffectiveDate != null; }
            set { }
        }

        /// <summary>
        /// The date the next conversion option notice is due to be sent.
        /// </summary>
        [XmlElement(Order = 19)]
        public MISMODate NextConversionOptionNoticeDate;

        /// <summary>
        /// Gets or sets a value indicating whether the NextConversionOptionNoticeDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NextConversionOptionNoticeDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool NextConversionOptionNoticeDateSpecified
        {
            get { return NextConversionOptionNoticeDate != null; }
            set { }
        }

        /// <summary>
        /// The next payment due date on which a conversion option notice will be sent as a courtesy for a demand convertible loan.
        /// </summary>
        [XmlElement(Order = 20)]
        public MISMODate NextDemandConversionOptionNoticeDate;

        /// <summary>
        /// Gets or sets a value indicating whether the NextDemandConversionOptionNoticeDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NextDemandConversionOptionNoticeDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool NextDemandConversionOptionNoticeDateSpecified
        {
            get { return NextDemandConversionOptionNoticeDate != null; }
            set { }
        }

        /// <summary>
        /// The due date of the next payment for which the new principal and interest payment is calculated without regard to per change or lifetime caps.
        /// </summary>
        [XmlElement(Order = 21)]
        public MISMODate NextIgnorePrincipalAndInterestPaymentAdjustmentCapsDate;

        /// <summary>
        /// Gets or sets a value indicating whether the NextIgnorePrincipalAndInterestPaymentAdjustmentCapsDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NextIgnorePrincipalAndInterestPaymentAdjustmentCapsDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool NextIgnorePrincipalAndInterestPaymentAdjustmentCapsDateSpecified
        {
            get { return NextIgnorePrincipalAndInterestPaymentAdjustmentCapsDate != null; }
            set { }
        }

        /// <summary>
        /// The due date of the next payment for which the new interest rate is calculated without regard to periodic, per change or lifetime caps.
        /// </summary>
        [XmlElement(Order = 22)]
        public MISMODate NextIgnoreRateAdjustmentCapsDate;

        /// <summary>
        /// Gets or sets a value indicating whether the NextIgnoreRateAdjustmentCapsDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NextIgnoreRateAdjustmentCapsDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool NextIgnoreRateAdjustmentCapsDateSpecified
        {
            get { return NextIgnoreRateAdjustmentCapsDate != null; }
            set { }
        }

        /// <summary>
        /// The payment due date of the next principal and interest payment to be calculated.
        /// </summary>
        [XmlElement(Order = 23)]
        public MISMODate NextPrincipalAndInterestPaymentChangeEffectiveDate;

        /// <summary>
        /// Gets or sets a value indicating whether the NextPrincipalAndInterestPaymentChangeEffectiveDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NextPrincipalAndInterestPaymentChangeEffectiveDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool NextPrincipalAndInterestPaymentChangeEffectiveDateSpecified
        {
            get { return NextPrincipalAndInterestPaymentChangeEffectiveDate != null; }
            set { }
        }

        /// <summary>
        /// The date on which the next interest rate adjustment goes into effect.
        /// </summary>
        [XmlElement(Order = 24)]
        public MISMODate NextRateAdjustmentEffectiveDate;

        /// <summary>
        /// Gets or sets a value indicating whether the NextRateAdjustmentEffectiveDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NextRateAdjustmentEffectiveDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool NextRateAdjustmentEffectiveDateSpecified
        {
            get { return NextRateAdjustmentEffectiveDate != null; }
            set { }
        }

        /// <summary>
        /// The interest rate change effective date for which the next interest rate change notice is to be sent, if notices are not sent to the mortgagor for every change.
        /// </summary>
        [XmlElement(Order = 25)]
        public MISMODate NextRateAdjustmentEffectiveNoticeDate;

        /// <summary>
        /// Gets or sets a value indicating whether the NextRateAdjustmentEffectiveNoticeDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NextRateAdjustmentEffectiveNoticeDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool NextRateAdjustmentEffectiveNoticeDateSpecified
        {
            get { return NextRateAdjustmentEffectiveNoticeDate != null; }
            set { }
        }

        /// <summary>
        /// Percentage to be added to current or original interest rate, if the interest rate on the subject loan can change.
        /// </summary>
        [XmlElement(Order = 26)]
        public MISMOPercent RateAdjustmentPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the RateAdjustmentPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RateAdjustmentPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool RateAdjustmentPercentSpecified
        {
            get { return RateAdjustmentPercent != null; }
            set { }
        }

        /// <summary>
        /// Identifies the servicers plan of the varying loan payment and/or rate change characteristics.
        /// </summary>
        [XmlElement(Order = 27)]
        public MISMOIdentifier ServicerARMPlanIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the ServicerARMPlanIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServicerARMPlanIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServicerARMPlanIdentifierSpecified
        {
            get { return ServicerARMPlanIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 28)]
        public RATE_OR_PAYMENT_CHANGE_OCCURRENCE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
