namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class TRANSFORM_FILES
    {
        /// <summary>
        /// Gets a value indicating whether the TRANSFORM_FILES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.TransformFileSpecified;
            }
        }

        /// <summary>
        /// A collection of transform files.
        /// </summary>
        [XmlElement("TRANSFORM_FILE", Order = 0)]
		public List<TRANSFORM_FILE> TransformFile = new List<TRANSFORM_FILE>();

        /// <summary>
        /// Gets or sets a value indicating whether the TransformFile element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TransformFile element has been assigned a value.</value>
        [XmlIgnore]
        public bool TransformFileSpecified
        {
            get { return this.TransformFile != null && this.TransformFile.Count(t => t != null && t.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public TRANSFORM_FILES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
