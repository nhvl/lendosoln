namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CREDIT_LIABILITY_PRIOR_ADVERSE_RATINGS
    {
        /// <summary>
        /// Gets a value indicating whether the CREDIT_LIABILITY_PRIOR_ADVERSE_RATINGS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditLiabilityPriorAdverseRatingSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of prior adverse ratings of credit liability.
        /// </summary>
        [XmlElement("CREDIT_LIABILITY_PRIOR_ADVERSE_RATING", Order = 0)]
		public List<CREDIT_LIABILITY_PRIOR_ADVERSE_RATING> CreditLiabilityPriorAdverseRating = new List<CREDIT_LIABILITY_PRIOR_ADVERSE_RATING>();

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLiabilityPriorAdverseRating element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLiabilityPriorAdverseRating element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLiabilityPriorAdverseRatingSpecified
        {
            get { return this.CreditLiabilityPriorAdverseRating != null && this.CreditLiabilityPriorAdverseRating.Count(c => c != null && c.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public CREDIT_LIABILITY_PRIOR_ADVERSE_RATINGS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
