namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class CREDIT_LIABILITY_PRIOR_ADVERSE_RATING
    {
        /// <summary>
        /// Gets a value indicating whether the CREDIT_LIABILITY_PRIOR_ADVERSE_RATING container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditLiabilityPriorAdverseRatingCodeSpecified
                    || this.CreditLiabilityPriorAdverseRatingDateSpecified
                    || this.CreditLiabilityPriorAdverseRatingTypeSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// This is the accounts Manner Of Payment (MOP) rating code for the associated Prior Adverse Rating Date. See the MISMO Implementation Guide: Credit Reporting for more information on MOP codes.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOCode CreditLiabilityPriorAdverseRatingCode;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLiabilityPriorAdverseRatingCode element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLiabilityPriorAdverseRatingCode element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLiabilityPriorAdverseRatingCodeSpecified
        {
            get { return CreditLiabilityPriorAdverseRatingCode != null; }
            set { }
        }

        /// <summary>
        /// Lists the date (month and year) that the borrower was late on payments. The associated Prior Adverse Rating Rating element gives the rating for that date.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMODate CreditLiabilityPriorAdverseRatingDate;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLiabilityPriorAdverseRatingDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLiabilityPriorAdverseRatingDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLiabilityPriorAdverseRatingDateSpecified
        {
            get { return CreditLiabilityPriorAdverseRatingDate != null; }
            set { }
        }

        /// <summary>
        /// This is the accounts manner Of payment rating type for the associated Prior Adverse Rating Date.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<CreditLiabilityPriorAdverseRatingBase> CreditLiabilityPriorAdverseRatingType;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLiabilityPriorAdverseRatingType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLiabilityPriorAdverseRatingType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLiabilityPriorAdverseRatingTypeSpecified
        {
            get { return this.CreditLiabilityPriorAdverseRatingType != null && this.CreditLiabilityPriorAdverseRatingType.enumValue != CreditLiabilityPriorAdverseRatingBase.Blank; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public CREDIT_LIABILITY_PRIOR_ADVERSE_RATING_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
