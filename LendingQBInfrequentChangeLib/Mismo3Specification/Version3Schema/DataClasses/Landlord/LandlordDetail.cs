namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class LANDLORD_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the LANDLORD_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.MonthlyRentAmountSpecified
                    || this.MonthlyRentCurrentRatingTypeSpecified;
            }
        }

        /// <summary>
        /// The amount paid by the borrower or co-borrower for the rental of a residence property. This information is normally provided during interview with the borrower or obtained from a loan application, and is sometimes verified with the residence property landlord.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount MonthlyRentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the MonthlyRentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MonthlyRentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool MonthlyRentAmountSpecified
        {
            get { return MonthlyRentAmount != null; }
            set { }
        }

        /// <summary>
        /// This is the landlord evaluation of the current payment status of monthly rent by a borrower or co-borrower.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<MonthlyRentCurrentRatingBase> MonthlyRentCurrentRatingType;

        /// <summary>
        /// Gets or sets a value indicating whether the MonthlyRentCurrentRatingType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MonthlyRentCurrentRatingType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MonthlyRentCurrentRatingTypeSpecified
        {
            get { return this.MonthlyRentCurrentRatingType != null && this.MonthlyRentCurrentRatingType.enumValue != MonthlyRentCurrentRatingBase.Blank; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public LANDLORD_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
