namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class LANDLORD
    {
        /// <summary>
        /// Gets a value indicating whether the LANDLORD container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AddressSpecified
                    || this.ContactsSpecified
                    || this.CreditCommentsSpecified
                    || this.ExtensionSpecified
                    || this.LandlordDetailSpecified
                    || this.NameSpecified
                    || this.VerificationSpecified;
            }
        }

        /// <summary>
        /// Address of the landlord.
        /// </summary>
        [XmlElement("ADDRESS", Order = 0)]
        public ADDRESS Address;

        /// <summary>
        /// Gets or sets a value indicating whether the Address element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Address element has been assigned a value.</value>
        [XmlIgnore]
        public bool AddressSpecified
        {
            get { return this.Address != null && this.Address.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Contacts of the landlord.
        /// </summary>
        [XmlElement("CONTACTS", Order = 1)]
        public CONTACTS Contacts;

        /// <summary>
        /// Gets or sets a value indicating whether the Contacts element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Contacts element has been assigned a value.</value>
        [XmlIgnore]
        public bool ContactsSpecified
        {
            get { return this.Contacts != null && this.Contacts.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Credit comments of the landlord.
        /// </summary>
        [XmlElement("CREDIT_COMMENTS", Order = 2)]
        public CREDIT_COMMENTS CreditComments;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditComments element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditComments element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditCommentsSpecified
        {
            get { return this.CreditComments != null && this.CreditComments.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Detail about the landlord.
        /// </summary>
        [XmlElement("LANDLORD_DETAIL", Order = 3)]
        public LANDLORD_DETAIL LandlordDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the LandlordDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LandlordDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool LandlordDetailSpecified
        {
            get { return this.LandlordDetail != null && this.LandlordDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Name of the landlord.
        /// </summary>
        [XmlElement("NAME", Order = 4)]
        public NAME Name;

        /// <summary>
        /// Gets or sets a value indicating whether the Name element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Name element has been assigned a value.</value>
        [XmlIgnore]
        public bool NameSpecified
        {
            get { return this.Name != null && this.Name.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Verification of a landlord.
        /// </summary>
        [XmlElement("VERIFICATION", Order = 5)]
        public VERIFICATION Verification;

        /// <summary>
        /// Gets or sets a value indicating whether the Verification element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Verification element has been assigned a value.</value>
        [XmlIgnore]
        public bool VerificationSpecified
        {
            get { return this.Verification != null && this.Verification.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 6)]
        public LANDLORD_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
