namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class LITIGATIONS
    {
        /// <summary>
        /// Gets a value indicating whether the LITIGATIONS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.LitigationSpecified;
            }
        }

        /// <summary>
        /// A collection of litigation.
        /// </summary>
        [XmlElement("LITIGATION", Order = 0)]
		public List<LITIGATION> Litigation = new List<LITIGATION>();

        /// <summary>
        /// Gets or sets a value indicating whether the Litigation element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Litigation element has been assigned a value.</value>
        [XmlIgnore]
        public bool LitigationSpecified
        {
            get { return this.Litigation != null && this.Litigation.Count(l => l != null && l.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public LITIGATIONS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
