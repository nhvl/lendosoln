namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class RENT_ADJUSTMENTS
    {
        /// <summary>
        /// Gets a value indicating whether the RENT_ADJUSTMENTS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.RentAdjustmentSpecified;
            }
        }

        /// <summary>
        /// Contains various rent adjustment elements.
        /// </summary>
        [XmlElement("RENT_ADJUSTMENT", Order = 0)]
		public List<RENT_ADJUSTMENT> RentAdjustment = new List<RENT_ADJUSTMENT>();

        /// <summary>
        /// Gets or sets a value indicating whether the RentAdjustment element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RentAdjustment element has been assigned a value.</value>
        [XmlIgnore]
        public bool RentAdjustmentSpecified
        {
            get { return this.RentAdjustment != null && this.RentAdjustment.Count(r => r != null && r.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public RENT_ADJUSTMENTS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
