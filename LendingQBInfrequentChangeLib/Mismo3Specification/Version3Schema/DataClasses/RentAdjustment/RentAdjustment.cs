namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class RENT_ADJUSTMENT
    {
        /// <summary>
        /// Gets a value indicating whether the RENT_ADJUSTMENT container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.RentAdjustmentAmountSpecified
                    || this.RentAdjustmentDescriptionSpecified
                    || this.RentAdjustmentTypeOtherDescriptionSpecified
                    || this.RentAdjustmentTypeSpecified;
            }
        }

        /// <summary>
        /// The dollar amount of the sales price adjustment. 
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount RentAdjustmentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the RentAdjustmentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RentAdjustmentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool RentAdjustmentAmountSpecified
        {
            get { return RentAdjustmentAmount != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe a property feature which is being used during the application of the Rent Schedule Comparison approach to estimate the market rent.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString RentAdjustmentDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the RentAdjustmentDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RentAdjustmentDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool RentAdjustmentDescriptionSpecified
        {
            get { return RentAdjustmentDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the type of rent adjustment that is made when using the Rent Schedule Comparison approach to estimate the market rent of a property. Each type corresponds to a property feature that may affect the market rent.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<RentAdjustmentBase> RentAdjustmentType;

        /// <summary>
        /// Gets or sets a value indicating whether the RentAdjustmentType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RentAdjustmentType element has been assigned a value.</value>
        [XmlIgnore]
        public bool RentAdjustmentTypeSpecified
        {
            get { return this.RentAdjustmentType != null && this.RentAdjustmentType.enumValue != RentAdjustmentBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture additional information when Other is selected for Rent Adjustment Type.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString RentAdjustmentTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the RentAdjustmentTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RentAdjustmentTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool RentAdjustmentTypeOtherDescriptionSpecified
        {
            get { return RentAdjustmentTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public RENT_ADJUSTMENT_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
