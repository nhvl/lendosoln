namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class MI_VALIDATION_RESPONSE
    {
        /// <summary>
        /// Gets a value indicating whether the MI_VALIDATION_RESPONSE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.LoanIdentifiersSpecified
                    || this.MIProductSpecified
                    || this.MIValidationResponseDetailSpecified
                    || this.PartiesSpecified
                    || this.PropertiesSpecified;
            }
        }

        /// <summary>
        /// Identifiers for loans needing insurance.
        /// </summary>
        [XmlElement("LOAN_IDENTIFIERS", Order = 0)]
        public LOAN_IDENTIFIERS LoanIdentifiers;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanIdentifiers element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanIdentifiers element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanIdentifiersSpecified
        {
            get { return this.LoanIdentifiers != null && this.LoanIdentifiers.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A mortgage insurance product.
        /// </summary>
        [XmlElement("MI_PRODUCT", Order = 1)]
        public MI_PRODUCT MIProduct;

        /// <summary>
        /// Gets or sets a value indicating whether the Mortgage Insurance Product element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Mortgage Insurance Product element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIProductSpecified
        {
            get { return this.MIProduct != null && this.MIProduct.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details on a mortgage insurance validation response.
        /// </summary>
        [XmlElement("MI_VALIDATION_RESPONSE_DETAIL", Order = 2)]
        public MI_VALIDATION_RESPONSE_DETAIL MIValidationResponseDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the Mortgage InsuranceValidationResponseDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Mortgage Insurance ValidationResponseDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIValidationResponseDetailSpecified
        {
            get { return this.MIValidationResponseDetail != null && this.MIValidationResponseDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Parties involved in a mortgage insurance validation.
        /// </summary>
        [XmlElement("PARTIES", Order = 3)]
        public PARTIES Parties;

        /// <summary>
        /// Gets or sets a value indicating whether the Parties element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Parties element has been assigned a value.</value>
        [XmlIgnore]
        public bool PartiesSpecified
        {
            get { return this.Parties != null && this.Parties.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Properties of a mortgage insurance validation.
        /// </summary>
        [XmlElement("PROPERTIES", Order = 4)]
        public PROPERTIES Properties;

        /// <summary>
        /// Gets or sets a value indicating whether the Properties element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Properties element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertiesSpecified
        {
            get { return this.Properties != null && this.Properties.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 5)]
        public MI_VALIDATION_RESPONSE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
