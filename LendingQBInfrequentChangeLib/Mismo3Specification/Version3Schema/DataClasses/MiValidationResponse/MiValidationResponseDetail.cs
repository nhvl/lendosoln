namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class MI_VALIDATION_RESPONSE_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the MI_VALIDATION_RESPONSE_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.MICancellationDateSpecified
                    || this.MICertificateIdentifierSpecified
                    || this.MIPolicyStatusTypeOtherDescriptionSpecified
                    || this.MIPolicyStatusTypeSpecified
                    || this.MIPremiumFinancedAmountSpecified
                    || this.ReasonForMIPolicyCancellationTypeOtherDescriptionSpecified
                    || this.ReasonForMIPolicyCancellationTypeSpecified;
            }
        }

        /// <summary>
        /// Actual cancellation date of mortgage insurance coverage.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMODate MICancellationDate;

        /// <summary>
        /// Gets or sets a value indicating whether the MICancellationDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MICancellationDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool MICancellationDateSpecified
        {
            get { return MICancellationDate != null; }
            set { }
        }

        /// <summary>
        /// The number assigned by the private mortgage insurance company to track a loan.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOIdentifier MICertificateIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the MICertificateIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MICertificateIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool MICertificateIdentifierSpecified
        {
            get { return MICertificateIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Conveys the current status of the MI policy.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<MIPolicyStatusBase> MIPolicyStatusType;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPolicyStatusType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPolicyStatusType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPolicyStatusTypeSpecified
        {
            get { return this.MIPolicyStatusType != null && this.MIPolicyStatusType.enumValue != MIPolicyStatusBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for MI Policy Status Type.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString MIPolicyStatusTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPolicyStatusTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPolicyStatusTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPolicyStatusTypeOtherDescriptionSpecified
        {
            get { return MIPolicyStatusTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The amount of the up-front premium that is financed.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOAmount MIPremiumFinancedAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPremiumFinancedAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPremiumFinancedAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumFinancedAmountSpecified
        {
            get { return MIPremiumFinancedAmount != null; }
            set { }
        }

        /// <summary>
        /// The reason for the cancellation of Active/In Force MI Coverage.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOEnum<ReasonForMIPolicyCancellationBase> ReasonForMIPolicyCancellationType;

        /// <summary>
        /// Gets or sets a value indicating whether the ReasonForMIPolicyCancellationType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ReasonForMIPolicyCancellationType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ReasonForMIPolicyCancellationTypeSpecified
        {
            get { return this.ReasonForMIPolicyCancellationType != null && this.ReasonForMIPolicyCancellationType.enumValue != ReasonForMIPolicyCancellationBase.Blank; }
            set { }
        }

        /// <summary>
        /// Used to further define ReasonForCancellation when Other is selected.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOString ReasonForMIPolicyCancellationTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ReasonForMIPolicyCancellationTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ReasonForMIPolicyCancellationTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ReasonForMIPolicyCancellationTypeOtherDescriptionSpecified
        {
            get { return ReasonForMIPolicyCancellationTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 7)]
        public MI_VALIDATION_RESPONSE_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
