namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class SCHOOL
    {
        /// <summary>
        /// Gets a value indicating whether the SCHOOL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.SchoolDescriptionSpecified
                    || this.SchoolDistrictNameSpecified
                    || this.SchoolDistrictURLSpecified
                    || this.SchoolMaximumGradeTypeSpecified
                    || this.SchoolMinimumGradeTypeSpecified
                    || this.SchoolNameSpecified
                    || this.SchoolTypeGradeCountSpecified
                    || this.SchoolTypeOtherDescriptionSpecified
                    || this.SchoolTypeSpecified
                    || this.SchoolURLSpecified;
            }
        }

        /// <summary>
        /// Free-form text description of the school in the area for the specified type.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString SchoolDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the SchoolDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SchoolDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool SchoolDescriptionSpecified
        {
            get { return SchoolDescription != null; }
            set { }
        }

        /// <summary>
        /// The name of the school district in which the property is located.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString SchoolDistrictName;

        /// <summary>
        /// Gets or sets a value indicating whether the SchoolDistrictName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SchoolDistrictName element has been assigned a value.</value>
        [XmlIgnore]
        public bool SchoolDistrictNameSpecified
        {
            get { return SchoolDistrictName != null; }
            set { }
        }

        /// <summary>
        /// The Uniform Resource Locator (web address) for the school district.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOURL SchoolDistrictURL;

        /// <summary>
        /// Gets or sets a value indicating whether the SchoolDistrictURL element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SchoolDistrictURL element has been assigned a value.</value>
        [XmlIgnore]
        public bool SchoolDistrictURLSpecified
        {
            get { return SchoolDistrictURL != null; }
            set { }
        }

        /// <summary>
        /// The highest grade offered by the school.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOEnum<SchoolGradeBase> SchoolMaximumGradeType;

        /// <summary>
        /// Gets or sets a value indicating whether the SchoolMaximumGradeType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SchoolMaximumGradeType element has been assigned a value.</value>
        [XmlIgnore]
        public bool SchoolMaximumGradeTypeSpecified
        {
            get { return this.SchoolMaximumGradeType != null && this.SchoolMaximumGradeType.enumValue != SchoolGradeBase.Blank; }
            set { }
        }

        /// <summary>
        /// The lowest grade offered by the school.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<SchoolGradeBase> SchoolMinimumGradeType;

        /// <summary>
        /// Gets or sets a value indicating whether the SchoolMinimumGradeType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SchoolMinimumGradeType element has been assigned a value.</value>
        [XmlIgnore]
        public bool SchoolMinimumGradeTypeSpecified
        {
            get { return this.SchoolMinimumGradeType != null && this.SchoolMinimumGradeType.enumValue != SchoolGradeBase.Blank; }
            set { }
        }

        /// <summary>
        /// The full name of the specified school.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString SchoolName;

        /// <summary>
        /// Gets or sets a value indicating whether the SchoolName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SchoolName element has been assigned a value.</value>
        [XmlIgnore]
        public bool SchoolNameSpecified
        {
            get { return SchoolName != null; }
            set { }
        }

        /// <summary>
        /// The public school type as categorized by the common names for grades supported by the indicated school.  Each state determines what grade range constitutes elementary education.   According to its length, elementary education may be followed (or not) by a number of years of middle school education (generally three years).  Secondary education takes place in grades 7-12, depending upon the laws and policies of states and local school districts. There is no national structure, curriculum or governing law; all laws and policies are set and enforced by the 50 state governments and the over 14,000 local school districts. All states and school districts have set the secondary school graduation level as the completion of grade 12, and the common name for the secondary graduation qualification is the High School Diploma. This diploma name covers a variety of awards for different curricula and standards.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOEnum<SchoolBase> SchoolType;

        /// <summary>
        /// Gets or sets a value indicating whether the SchoolType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SchoolType element has been assigned a value.</value>
        [XmlIgnore]
        public bool SchoolTypeSpecified
        {
            get { return this.SchoolType != null && this.SchoolType.enumValue != SchoolBase.Blank; }
            set { }
        }

        /// <summary>
        /// The number of grades offered by the indicated school type.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOCount SchoolTypeGradeCount;

        /// <summary>
        /// Gets or sets a value indicating whether the SchoolTypeGradeCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SchoolTypeGradeCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool SchoolTypeGradeCountSpecified
        {
            get { return SchoolTypeGradeCount != null; }
            set { }
        }

        /// <summary>
        /// Free-form description of the school if School Type Other is selected.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOString SchoolTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the SchoolTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SchoolTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool SchoolTypeOtherDescriptionSpecified
        {
            get { return SchoolTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The Uniform Resource Locator (web address) for the school.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOURL SchoolURL;

        /// <summary>
        /// Gets or sets a value indicating whether the SchoolURL element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SchoolURL element has been assigned a value.</value>
        [XmlIgnore]
        public bool SchoolURLSpecified
        {
            get { return SchoolURL != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 10)]
        public SCHOOL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
