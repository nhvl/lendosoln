namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class SITE_ZONING
    {
        /// <summary>
        /// Gets a value indicating whether the SITE_ZONING container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.PropertyZoningCategoryTypeOtherDescriptionSpecified
                    || this.PropertyZoningCategoryTypeSpecified
                    || this.SiteZoningClassificationDescriptionSpecified
                    || this.SiteZoningClassificationIdentifierSpecified
                    || this.SiteZoningComplianceDescriptionSpecified
                    || this.SiteZoningComplianceTypeSpecified
                    || this.ZoningPermitRebuildToCurrentDensityIndicatorSpecified;
            }
        }

        /// <summary>
        /// Specifies the property zoning category. The zoning category is not the official legal description.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<PropertyZoningCategoryBase> PropertyZoningCategoryType;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyZoningCategoryType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyZoningCategoryType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyZoningCategoryTypeSpecified
        {
            get { return this.PropertyZoningCategoryType != null && this.PropertyZoningCategoryType.enumValue != PropertyZoningCategoryBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Property Zoning Category Type.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString PropertyZoningCategoryTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyZoningCategoryTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyZoningCategoryTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyZoningCategoryTypeOtherDescriptionSpecified
        {
            get { return PropertyZoningCategoryTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the zoning classification of the property site.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString SiteZoningClassificationDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the SiteZoningClassificationDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SiteZoningClassificationDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool SiteZoningClassificationDescriptionSpecified
        {
            get { return SiteZoningClassificationDescription != null; }
            set { }
        }

        /// <summary>
        /// Identifies the zoning classification applied to the site.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOIdentifier SiteZoningClassificationIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the SiteZoningClassificationIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SiteZoningClassificationIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool SiteZoningClassificationIdentifierSpecified
        {
            get { return SiteZoningClassificationIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field describing any issues concerning how the site complies with zoning regulations including limitations on future development.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString SiteZoningComplianceDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the SiteZoningComplianceDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SiteZoningComplianceDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool SiteZoningComplianceDescriptionSpecified
        {
            get { return SiteZoningComplianceDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates the level of compliance of the subject Site to zoning regulations.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOEnum<SiteZoningComplianceBase> SiteZoningComplianceType;

        /// <summary>
        /// Gets or sets a value indicating whether the SiteZoningComplianceType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SiteZoningComplianceType element has been assigned a value.</value>
        [XmlIgnore]
        public bool SiteZoningComplianceTypeSpecified
        {
            get { return this.SiteZoningComplianceType != null && this.SiteZoningComplianceType.enumValue != SiteZoningComplianceBase.Blank; }
            set { }
        }

        /// <summary>
        /// Indicates that the zoning regulations allow the property to rebuild to the current density.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOIndicator ZoningPermitRebuildToCurrentDensityIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ZoningPermitRebuildToCurrentDensityIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ZoningPermitRebuildToCurrentDensityIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ZoningPermitRebuildToCurrentDensityIndicatorSpecified
        {
            get { return ZoningPermitRebuildToCurrentDensityIndicator != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 7)]
        public SITE_ZONING_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
