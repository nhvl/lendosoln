namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class NAME
    {
        /// <summary>
        /// Gets a value indicating whether the NAME container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.EducationalAchievementsDescriptionSpecified
                    || this.ExtensionSpecified
                    || this.FirstNameSpecified
                    || this.FullNameSpecified
                    || this.FunctionalTitleDescriptionSpecified
                    || this.IndividualTitleDescriptionSpecified
                    || this.LastNameSpecified
                    || this.MiddleNameSpecified
                    || this.PrefixNameSpecified
                    || this.SuffixNameSpecified;
            }
        }

        /// <summary>
        /// One or more advanced degrees that may be important to an establishment such as an educational institution. (MD, JD, Ed.D.).
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString EducationalAchievementsDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the EducationalAchievementsDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EducationalAchievementsDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool EducationalAchievementsDescriptionSpecified
        {
            get { return EducationalAchievementsDescription != null; }
            set { }
        }

        /// <summary>
        /// The first name of the individual represented by the parent object.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString FirstName;

        /// <summary>
        /// Gets or sets a value indicating whether the FirstName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FirstName element has been assigned a value.</value>
        [XmlIgnore]
        public bool FirstNameSpecified
        {
            get { return FirstName != null; }
            set { }
        }

        /// <summary>
        /// The unparsed name of either an individual or a legal entity.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString FullName;

        /// <summary>
        /// Gets or sets a value indicating whether the FullName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FullName element has been assigned a value.</value>
        [XmlIgnore]
        public bool FullNameSpecified
        {
            get { return FullName != null; }
            set { }
        }

        /// <summary>
        /// An explanation of the role of a person , in relation to a group, department or division of a company.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString FunctionalTitleDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the FunctionalTitleDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FunctionalTitleDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool FunctionalTitleDescriptionSpecified
        {
            get { return FunctionalTitleDescription != null; }
            set { }
        }

        /// <summary>
        /// A professional or job classification held by a person in a business.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString IndividualTitleDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the IndividualTitleDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IndividualTitleDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool IndividualTitleDescriptionSpecified
        {
            get { return IndividualTitleDescription != null; }
            set { }
        }

        /// <summary>
        /// The last name of the individual represented by the parent object.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString LastName;

        /// <summary>
        /// Gets or sets a value indicating whether the LastName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LastName element has been assigned a value.</value>
        [XmlIgnore]
        public bool LastNameSpecified
        {
            get { return LastName != null; }
            set { }
        }

        /// <summary>
        /// The middle name of the individual represented by the parent object.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOString MiddleName;

        /// <summary>
        /// Gets or sets a value indicating whether the MiddleName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MiddleName element has been assigned a value.</value>
        [XmlIgnore]
        public bool MiddleNameSpecified
        {
            get { return MiddleName != null; }
            set { }
        }

        /// <summary>
        /// Title preceding the name of an individual (Judge, Mister, Colonel).
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOString PrefixName;

        /// <summary>
        /// Gets or sets a value indicating whether the PrefixName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrefixName element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrefixNameSpecified
        {
            get { return PrefixName != null; }
            set { }
        }

        /// <summary>
        /// The name suffix of the individual represented by the parent object. (E.g., JR = Junior, SR = Senior, etc.).
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOString SuffixName;

        /// <summary>
        /// Gets or sets a value indicating whether the SuffixName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SuffixName element has been assigned a value.</value>
        [XmlIgnore]
        public bool SuffixNameSpecified
        {
            get { return SuffixName != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 9)]
        public NAME_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
