namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class DEPRECIATION
    {
        /// <summary>
        /// Gets a value indicating whether the DEPRECIATION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DepreciationExteriorAmountSpecified
                    || this.DepreciationExteriorPercentSpecified
                    || this.DepreciationFunctionalAmountSpecified
                    || this.DepreciationFunctionalPercentSpecified
                    || this.DepreciationPhysicalAmountSpecified
                    || this.DepreciationPhysicalPercentSpecified
                    || this.DepreciationTotalAmountSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// This element specifies the Exterior depreciation amount of the subject property.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount DepreciationExteriorAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the DepreciationExteriorAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DepreciationExteriorAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool DepreciationExteriorAmountSpecified
        {
            get { return DepreciationExteriorAmount != null; }
            set { }
        }

        /// <summary>
        /// This element specifies the percent of depreciation applied to exterior depreciation of the subject property.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOPercent DepreciationExteriorPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the DepreciationExteriorPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DepreciationExteriorPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool DepreciationExteriorPercentSpecified
        {
            get { return DepreciationExteriorPercent != null; }
            set { }
        }

        /// <summary>
        /// This element specifies the Functional depreciation amount of the subject property.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOAmount DepreciationFunctionalAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the DepreciationFunctionalAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DepreciationFunctionalAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool DepreciationFunctionalAmountSpecified
        {
            get { return DepreciationFunctionalAmount != null; }
            set { }
        }

        /// <summary>
        /// This element specifies the percent of depreciation applied to function depreciation of the subject property.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOPercent DepreciationFunctionalPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the DepreciationFunctionalPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DepreciationFunctionalPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool DepreciationFunctionalPercentSpecified
        {
            get { return DepreciationFunctionalPercent != null; }
            set { }
        }

        /// <summary>
        /// This element specifies the Physical depreciation amount of the subject property.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOAmount DepreciationPhysicalAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the DepreciationPhysicalAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DepreciationPhysicalAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool DepreciationPhysicalAmountSpecified
        {
            get { return DepreciationPhysicalAmount != null; }
            set { }
        }

        /// <summary>
        /// This element specifies the percent of depreciation applied to physical depreciation of the subject property.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOPercent DepreciationPhysicalPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the DepreciationPhysicalPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DepreciationPhysicalPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool DepreciationPhysicalPercentSpecified
        {
            get { return DepreciationPhysicalPercent != null; }
            set { }
        }

        /// <summary>
        /// Specifies the total amount of depreciation for the property improvements. This amount includes physical, functional and external depreciation.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOAmount DepreciationTotalAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the DepreciationTotalAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DepreciationTotalAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool DepreciationTotalAmountSpecified
        {
            get { return DepreciationTotalAmount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 7)]
        public DEPRECIATION_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
