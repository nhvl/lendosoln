namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class LTV
    {
        /// <summary>
        /// Gets a value indicating whether the LTV container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BaseLTVRatioPercentSpecified
                || this.LTVCalculationDateSpecified
                || this.LTVRatioPercentSpecified
                || this.PostCapitalizationMarkToMarketLTVRatioPercentSpecified
                || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// TThe result of dividing the unpaid principal balance (UPB) of the  mortgage (not including any financed mortgage insurance premium) by the value of the subject property.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOPercent BaseLTVRatioPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the BaseLTVRatioPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BaseLTVRatioPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool BaseLTVRatioPercentSpecified
        {
            get { return BaseLTVRatioPercent != null; }
            set { }
        }

        /// <summary>
        /// The date the LTV was calculated.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMODate LTVCalculationDate;

        /// <summary>
        /// Gets or sets a value indicating whether the LTVCalculationDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LTVCalculationDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool LTVCalculationDateSpecified
        {
            get { return LTVCalculationDate != null; }
            set { }
        }

        /// <summary>
        /// The ratio of the (outstanding) loan amount to the appraised value, estimated value or purchase price of the property.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOPercent LTVRatioPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the LTVRatioPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LTVRatioPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool LTVRatioPercentSpecified
        {
            get { return LTVRatioPercent != null; }
            set { }
        }

        /// <summary>
        /// Post capitalized UPB, including forbearance amounts, divided by current property value.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOPercent PostCapitalizationMarkToMarketLTVRatioPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the PostCapitalizationMarkToMarketLTVRatioPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PostCapitalizationMarkToMarketLTVRatioPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool PostCapitalizationMarkToMarketLTVRatioPercentSpecified
        {
            get { return PostCapitalizationMarkToMarketLTVRatioPercent != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public LTV_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
