namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PROJECT_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the PROJECT_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AdditionalProjectConsiderationsTypeOtherDescriptionSpecified
                    || this.AdditionalProjectConsiderationsTypeSpecified
                    || this.CondominiumProjectStatusTypeSpecified
                    || this.CondominiumPUDDeclarationsDescriptionSpecified
                    || this.ExtensionSpecified
                    || this.FNMCondominiumProjectManagerProjectIdentifierSpecified
                    || this.ProjectAmenityImprovementsCompletedIndicatorSpecified
                    || this.ProjectAttachmentTypeSpecified
                    || this.ProjectClassificationIdentifierSpecified
                    || this.ProjectContainsMultipleDwellingUnitsDataSourceDescriptionSpecified
                    || this.ProjectContainsMultipleDwellingUnitsIndicatorSpecified
                    || this.ProjectDesignTypeOtherDescriptionSpecified
                    || this.ProjectDesignTypeSpecified
                    || this.ProjectDwellingUnitCountSpecified
                    || this.ProjectDwellingUnitsSoldCountSpecified
                    || this.ProjectEligibilityDeterminationTypeSpecified
                    || this.ProjectLegalStructureTypeSpecified
                    || this.ProjectNameSpecified
                    || this.ProjectPrimaryUsageTypeSpecified
                    || this.PUDIndicatorSpecified
                    || this.TwoToFourUnitCondominiumProjectIndicatorSpecified;
            }
        }

        /// <summary>
        /// Describes the specific ownership or use of the project.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<AdditionalProjectConsiderationsBase> AdditionalProjectConsiderationsType;

        /// <summary>
        /// Gets or sets a value indicating whether the AdditionalProjectConsiderationsType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AdditionalProjectConsiderationsType element has been assigned a value.</value>
        [XmlIgnore]
        public bool AdditionalProjectConsiderationsTypeSpecified
        {
            get { return this.AdditionalProjectConsiderationsType != null && this.AdditionalProjectConsiderationsType.enumValue != AdditionalProjectConsiderationsBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Additional Project Considerations Type.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString AdditionalProjectConsiderationsTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the AdditionalProjectConsiderationsTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AdditionalProjectConsiderationsTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool AdditionalProjectConsiderationsTypeOtherDescriptionSpecified
        {
            get { return AdditionalProjectConsiderationsTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the current state of the condominium project.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<CondominiumProjectStatusBase> CondominiumProjectStatusType;

        /// <summary>
        /// Gets or sets a value indicating whether the CondominiumProjectStatusType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CondominiumProjectStatusType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CondominiumProjectStatusTypeSpecified
        {
            get { return this.CondominiumProjectStatusType != null && this.CondominiumProjectStatusType.enumValue != CondominiumProjectStatusBase.Blank; }
            set { }
        }

        /// <summary>
        /// The full text description of the Condo or PUD declarations.
        /// Replaced by NoteRiderType, SecurityInstrumentRiderType.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString CondominiumPUDDeclarationsDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the CondominiumPUDDeclarationsDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CondominiumPUDDeclarationsDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool CondominiumPUDDeclarationsDescriptionSpecified
        {
            get { return CondominiumPUDDeclarationsDescription != null; }
            set { }
        }

        /// <summary>
        /// The unique identifier of a property development project to which individual properties belong assigned by the Fannie Mae Condo Project Manager (CPM) system.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOIdentifier FNMCondominiumProjectManagerProjectIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the FNMCondominiumProjectManagerProjectIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FNMCondominiumProjectManagerProjectIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool FNMCondominiumProjectManagerProjectIdentifierSpecified
        {
            get { return FNMCondominiumProjectManagerProjectIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether construction on the project amenities such as pool and clubhouse have been completed.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOIndicator ProjectAmenityImprovementsCompletedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectAmenityImprovementsCompletedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectAmenityImprovementsCompletedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectAmenityImprovementsCompletedIndicatorSpecified
        {
            get { return ProjectAmenityImprovementsCompletedIndicator != null; }
            set { }
        }

        /// <summary>
        /// Specifies the type of physical attachment, if any, between the dwelling units in the project.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOEnum<ProjectAttachmentBase> ProjectAttachmentType;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectAttachmentType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectAttachmentType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectAttachmentTypeSpecified
        {
            get { return this.ProjectAttachmentType != null && this.ProjectAttachmentType.enumValue != ProjectAttachmentBase.Blank; }
            set { }
        }

        /// <summary>
        /// Identifies the type of project or condominium classification for the subject property and its associated review.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOIdentifier ProjectClassificationIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectClassificationIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectClassificationIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectClassificationIdentifierSpecified
        {
            get { return ProjectClassificationIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the source of the information used to set the Project Contains Multiple Dwelling Units Indicator.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOString ProjectContainsMultipleDwellingUnitsDataSourceDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectContainsMultipleDwellingUnitsDataSourceDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectContainsMultipleDwellingUnitsDataSourceDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectContainsMultipleDwellingUnitsDataSourceDescriptionSpecified
        {
            get { return ProjectContainsMultipleDwellingUnitsDataSourceDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the project contains multiple dwelling-units.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOIndicator ProjectContainsMultipleDwellingUnitsIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectContainsMultipleDwellingUnitsIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectContainsMultipleDwellingUnitsIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectContainsMultipleDwellingUnitsIndicatorSpecified
        {
            get { return ProjectContainsMultipleDwellingUnitsIndicator != null; }
            set { }
        }

        /// <summary>
        /// This field specifies the type of design for the multiple unit buildings in a project.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOEnum<ProjectDesignBase> ProjectDesignType;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectDesignType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectDesignType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectDesignTypeSpecified
        {
            get { return this.ProjectDesignType != null && this.ProjectDesignType.enumValue != ProjectDesignBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the design if Other is selected as the Project Design Type.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOString ProjectDesignTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectDesignTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectDesignTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectDesignTypeOtherDescriptionSpecified
        {
            get { return ProjectDesignTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Total number of individual dwelling units in the project.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOCount ProjectDwellingUnitCount;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectDwellingUnitCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectDwellingUnitCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectDwellingUnitCountSpecified
        {
            get { return ProjectDwellingUnitCount != null; }
            set { }
        }

        /// <summary>
        /// The number of units in a building, project or development that have been sold to date.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOCount ProjectDwellingUnitsSoldCount;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectDwellingUnitsSoldCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectDwellingUnitsSoldCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectDwellingUnitsSoldCountSpecified
        {
            get { return ProjectDwellingUnitsSoldCount != null; }
            set { }
        }

        /// <summary>
        /// Specifies the level of review performed to determine that a project meets eligibility guidelines for purchase by the investor.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOEnum<ProjectEligibilityDeterminationBase> ProjectEligibilityDeterminationType;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectEligibilityDeterminationType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectEligibilityDeterminationType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectEligibilityDeterminationTypeSpecified
        {
            get { return this.ProjectEligibilityDeterminationType != null && this.ProjectEligibilityDeterminationType.enumValue != ProjectEligibilityDeterminationBase.Blank; }
            set { }
        }

        /// <summary>
        /// Specifies the form of ownership that defines the quality and quantity of ownership and rights to the individual unit owner.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOEnum<ProjectLegalStructureBase> ProjectLegalStructureType;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectLegalStructureType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectLegalStructureType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectLegalStructureTypeSpecified
        {
            get { return this.ProjectLegalStructureType != null && this.ProjectLegalStructureType.enumValue != ProjectLegalStructureBase.Blank; }
            set { }
        }

        /// <summary>
        /// The name of the project in which subject property is located (e.g., the name of the condominium or cooperative).
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMOString ProjectName;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectName element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectNameSpecified
        {
            get { return ProjectName != null; }
            set { }
        }

        /// <summary>
        /// Specifies the primary type of occupancy of the Project.
        /// </summary>
        [XmlElement(Order = 17)]
        public MISMOEnum<ProjectPrimaryUsageBase> ProjectPrimaryUsageType;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectPrimaryUsageType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectPrimaryUsageType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectPrimaryUsageTypeSpecified
        {
            get { return this.ProjectPrimaryUsageType != null && this.ProjectPrimaryUsageType.enumValue != ProjectPrimaryUsageBase.Blank; }
            set { }
        }

        /// <summary>
        /// Indicates that the project in which the subject property is located is a Planned Unit Development (PUD). A comprehensive development plan for a large land area. A PUD usually includes residences, roads, schools, recreational facilities, commercial, office and industrial areas. Also, a subdivision having lots or areas owned in common and reserved for the use of some or all of the owners of the separately owned lots. 
        /// </summary>
        [XmlElement(Order = 18)]
        public MISMOIndicator PUDIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PUDIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PUDIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PUDIndicatorSpecified
        {
            get { return PUDIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that the condominium project comprised of at least 2 but no more than 4 one unit dwellings.
        /// </summary>
        [XmlElement(Order = 19)]
        public MISMOIndicator TwoToFourUnitCondominiumProjectIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the TwoToFourUnitCondominiumProjectIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TwoToFourUnitCondominiumProjectIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool TwoToFourUnitCondominiumProjectIndicatorSpecified
        {
            get { return TwoToFourUnitCondominiumProjectIndicator != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 20)]
        public PROJECT_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
