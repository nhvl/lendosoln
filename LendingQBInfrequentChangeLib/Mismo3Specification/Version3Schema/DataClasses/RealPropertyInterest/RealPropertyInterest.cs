namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class REAL_PROPERTY_INTEREST
    {
        /// <summary>
        /// Gets a value indicating whether the REAL_PROPERTY_INTEREST container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AppraisedIndicatorSpecified
                    || this.ExtensionSpecified
                    || this.PropertyInterestRightsAppraisedTypeOtherDescriptionSpecified
                    || this.PropertyInterestRightsAppraisedTypeSpecified
                    || this.PropertyPartialInterestTypeOtherDescriptionSpecified
                    || this.PropertyPartialInterestTypeSpecified
                    || this.StructureUnitSharesCountSpecified
                    || this.UnitIdentifierSpecified;
            }
        }

        /// <summary>
        ///  Indicates whether the named property right has been included in the scope of the appraisal.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIndicator AppraisedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the AppraisedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AppraisedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool AppraisedIndicatorSpecified
        {
            get { return AppraisedIndicator != null; }
            set { }
        }

        /// <summary>
        /// Identifies the components of the bundle of rights inherent in the ownership of real estate is what is bought and sold in a real property transaction.  In an appraisal, the rights being appraised must be stated, because any limitation on the rights of ownership may affect property value.  This observation is at the property level versus the party level (individual or entity) named as the owner of such right. This is different than the ownership rights that may be recorded with the lien transaction.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<PropertyInterestRightsAppraisedBase> PropertyInterestRightsAppraisedType;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyInterestRightsAppraisedType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyInterestRightsAppraisedType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyInterestRightsAppraisedTypeSpecified
        {
            get { return this.PropertyInterestRightsAppraisedType != null && this.PropertyInterestRightsAppraisedType.enumValue != PropertyInterestRightsAppraisedBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Property Interest Rights Appraised Type.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString PropertyInterestRightsAppraisedTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyInterestRightsAppraisedTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyInterestRightsAppraisedTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyInterestRightsAppraisedTypeOtherDescriptionSpecified
        {
            get { return PropertyInterestRightsAppraisedTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        ///  Indicates the specific type of Partial Ownership Interest on the property as evaluated in the appraisal process.  May also be referred to as a fractional ownership interest.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOEnum<PropertyPartialInterestBase> PropertyPartialInterestType;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyPartialInterestType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyPartialInterestType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyPartialInterestTypeSpecified
        {
            get { return this.PropertyPartialInterestType != null && this.PropertyPartialInterestType.enumValue != PropertyPartialInterestBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Property Partial Interest Type.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString PropertyPartialInterestTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyPartialInterestTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyPartialInterestTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyPartialInterestTypeOtherDescriptionSpecified
        {
            get { return PropertyPartialInterestTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Reports the number of shares (e.g. of cooperative ownership stock) the subject unit controls.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOCount StructureUnitSharesCount;

        /// <summary>
        /// Gets or sets a value indicating whether the StructureUnitSharesCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the StructureUnitSharesCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool StructureUnitSharesCountSpecified
        {
            get { return StructureUnitSharesCount != null; }
            set { }
        }

        /// <summary>
        /// The identification of the unit.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOIdentifier UnitIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the UnitIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the UnitIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool UnitIdentifierSpecified
        {
            get { return UnitIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 7)]
        public REAL_PROPERTY_INTEREST_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
