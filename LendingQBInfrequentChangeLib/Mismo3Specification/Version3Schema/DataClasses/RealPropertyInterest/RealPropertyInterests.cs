namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class REAL_PROPERTY_INTERESTS
    {
        /// <summary>
        /// Gets a value indicating whether the REAL_PROPERTY_INTERESTS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.RealPropertyInterestSpecified;
            }
        }

        /// <summary>
        /// A collection of real property interests.
        /// </summary>
        [XmlElement("REAL_PROPERTY_INTEREST", Order = 0)]
		public List<REAL_PROPERTY_INTEREST> RealPropertyInterest = new List<REAL_PROPERTY_INTEREST>();

        /// <summary>
        /// Gets or sets a value indicating whether the RealPropertyInterest element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RealPropertyInterest element has been assigned a value.</value>
        [XmlIgnore]
        public bool RealPropertyInterestSpecified
        {
            get { return this.RealPropertyInterest != null && this.RealPropertyInterest.Count(r => r != null && r.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public REAL_PROPERTY_INTERESTS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
