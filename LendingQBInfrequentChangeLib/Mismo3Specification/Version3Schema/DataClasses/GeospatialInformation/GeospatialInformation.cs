namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class GEOSPATIAL_INFORMATION
    {
        /// <summary>
        /// Gets a value indicating whether the GEOSPATIAL_INFORMATION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.GeospatialPropertyIdentifierSpecified
                    || this.LatitudeIdentifierSpecified
                    || this.LongitudeIdentifierSpecified
                    || this.LotBoundaryIdentifierSpecified
                    || this.NaturalLotLocationIdentifierSpecified
                    || this.NaturalUnitSpaceEntryIdentifierSpecified;
            }
        }

        /// <summary>
        /// A geospatially-based identification that uses location components and is defined by the URI owner.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIdentifier GeospatialPropertyIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the GeospatialPropertyIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GeospatialPropertyIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool GeospatialPropertyIdentifierSpecified
        {
            get { return GeospatialPropertyIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Gives the location of a place on Earth  north or south of the equator. Lines of Latitude are the horizontal lines shown running east-to-west on maps (particularly so in the Mercator projection). Technically, latitude is an angular measurement in degrees ranging from 0 degrees at the equator (low latitude) to 90 degree at the poles (90 degree N or +90 degree for the North Pole and 90 degree S or −90 degree for the South Pole). 
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOIdentifier LatitudeIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the LatitudeIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LatitudeIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool LatitudeIdentifierSpecified
        {
            get { return LatitudeIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The geographic coordinate most commonly used in cartography and global navigation for east-west measurement. It is the angular distance measured east or west and usually expressed in degrees (or hours), minutes, and seconds, from the prime meridian, defined to be at the Royal Observatory, Greenwich, in England, to the meridian passing through another position on the surface of the earth. 
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOIdentifier LongitudeIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the LongitudeIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LongitudeIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool LongitudeIdentifierSpecified
        {
            get { return LongitudeIdentifier != null; }
            set { }
        }

        /// <summary>
        /// An identifier created through the application of a reversible algorithm to a real estate parcel and in which the source of parcel identity is defined by the URI owner.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOIdentifier LotBoundaryIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the LotBoundaryIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LotBoundaryIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool LotBoundaryIdentifierSpecified
        {
            get { return LotBoundaryIdentifier != null; }
            set { }
        }

        /// <summary>
        /// An identifier that represents the coordinates of a lot or parcel and in which the URI owner definition specifies the source of the measurements.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOIdentifier NaturalLotLocationIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the NaturalLotLocationIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NaturalLotLocationIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool NaturalLotLocationIdentifierSpecified
        {
            get { return NaturalLotLocationIdentifier != null; }
            set { }
        }

        /// <summary>
        /// An identifier created through the application of a reversible algorithm to the position and elevation of the midpoint of the plane representing the primary point of entry to a legally delineated unit space within a building.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOIdentifier NaturalUnitSpaceEntryIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the NaturalUnitSpaceEntryIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NaturalUnitSpaceEntryIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool NaturalUnitSpaceEntryIdentifierSpecified
        {
            get { return NaturalUnitSpaceEntryIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 6)]
        public GEOSPATIAL_INFORMATION_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
