namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class DRAW_RULE
    {
        /// <summary>
        /// Gets a value indicating whether the DRAW_RULE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.LoanDrawExpirationDateSpecified
                    || this.LoanDrawExtensionTermMonthsCountSpecified
                    || this.LoanDrawMaximumAmountSpecified
                    || this.LoanDrawMaximumTermMonthsCountSpecified
                    || this.LoanDrawMinimumAmountSpecified
                    || this.LoanDrawMinimumInitialDrawAmountSpecified
                    || this.LoanDrawPeriodMaximumDrawCountSpecified
                    || this.LoanDrawStartPeriodMonthsCountSpecified
                    || this.LoanDrawTermMonthsCountSpecified;
            }
        }

        /// <summary>
        /// The last date on which the borrower is able to draw funds on a line of credit.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMODate LoanDrawExpirationDate;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanDrawExpirationDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanDrawExpirationDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanDrawExpirationDateSpecified
        {
            get { return LoanDrawExpirationDate != null; }
            set { }
        }

        /// <summary>
        /// The number of months by which the draw term can be extended in a single transaction.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOCount LoanDrawExtensionTermMonthsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanDrawExtensionTermMonthsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanDrawExtensionTermMonthsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanDrawExtensionTermMonthsCountSpecified
        {
            get { return LoanDrawExtensionTermMonthsCount != null; }
            set { }
        }

        /// <summary>
        /// The maximum dollar amount a borrower can withdraw in each draw transaction against the loan.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOAmount LoanDrawMaximumAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanDrawMaximumAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanDrawMaximumAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanDrawMaximumAmountSpecified
        {
            get { return LoanDrawMaximumAmount != null; }
            set { }
        }

        /// <summary>
        /// The maximum number of months to which the draw term can be extended.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOCount LoanDrawMaximumTermMonthsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanDrawMaximumTermMonthsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanDrawMaximumTermMonthsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanDrawMaximumTermMonthsCountSpecified
        {
            get { return LoanDrawMaximumTermMonthsCount != null; }
            set { }
        }

        /// <summary>
        /// The minimum dollar amount a borrower can withdraw in each draw transaction against the loan.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOAmount LoanDrawMinimumAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanDrawMinimumAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanDrawMinimumAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanDrawMinimumAmountSpecified
        {
            get { return LoanDrawMinimumAmount != null; }
            set { }
        }

        /// <summary>
        /// The minimum dollar amount the borrower is required to withdraw in the first draw transaction against the loan.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOAmount LoanDrawMinimumInitialDrawAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanDrawMinimumInitialDrawAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanDrawMinimumInitialDrawAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanDrawMinimumInitialDrawAmountSpecified
        {
            get { return LoanDrawMinimumInitialDrawAmount != null; }
            set { }
        }

        /// <summary>
        /// The maximum number of times a borrower can withdraw funds against the loan during a specific period.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOCount LoanDrawPeriodMaximumDrawCount;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanDrawPeriodMaximumDrawCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanDrawPeriodMaximumDrawCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanDrawPeriodMaximumDrawCountSpecified
        {
            get { return LoanDrawPeriodMaximumDrawCount != null; }
            set { }
        }

        /// <summary>
        /// The minimum number of months from closing after which the borrower may begin to withdraw funds from the loan.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOCount LoanDrawStartPeriodMonthsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanDrawStartPeriodMonthsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanDrawStartPeriodMonthsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanDrawStartPeriodMonthsCountSpecified
        {
            get { return LoanDrawStartPeriodMonthsCount != null; }
            set { }
        }

        /// <summary>
        /// The total number of months during which draws are allowed.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOCount LoanDrawTermMonthsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanDrawTermMonthsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanDrawTermMonthsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanDrawTermMonthsCountSpecified
        {
            get { return LoanDrawTermMonthsCount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 9)]
        public DRAW_RULE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
