namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CLAIM
    {
        /// <summary>
        /// Gets a value indicating whether the CLAIM container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ClaimRequestSpecified
                    || this.ClaimResponseSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A claim request.
        /// </summary>
        [XmlElement("CLAIM_REQUEST", Order = 0)]
        public CLAIM_REQUEST ClaimRequest;

        /// <summary>
        /// Gets or sets a value indicating whether the ClaimRequest element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ClaimRequest element has been assigned a value.</value>
        [XmlIgnore]
        public bool ClaimRequestSpecified
        {
            get { return this.ClaimRequest != null && this.ClaimRequest.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A response to a claim.
        /// </summary>
        [XmlElement("CLAIM_RESPONSE", Order = 1)]
        public CLAIM_RESPONSE ClaimResponse;

        /// <summary>
        /// Gets or sets a value indicating whether the ClaimResponse element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ClaimResponse element has been assigned a value.</value>
        [XmlIgnore]
        public bool ClaimResponseSpecified
        {
            get { return this.ClaimResponse != null && this.ClaimResponse.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public CLAIM_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
