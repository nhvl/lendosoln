namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class HOLDBACK_ITEMS
    {
        /// <summary>
        /// Gets a value indicating whether the HOLDBACK_ITEMS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.HoldbackItemSpecified;
            }
        }

        /// <summary>
        /// A collection of holdback items.
        /// </summary>
        [XmlElement("HOLDBACK_ITEM", Order = 0)]
        public List<HOLDBACK_ITEM> HoldbackItem = new List<HOLDBACK_ITEM>();

        /// <summary>
        /// Gets or sets a value indicating whether the HoldbackItem element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HoldbackItem element has been assigned a value.</value>
        [XmlIgnore]
        public bool HoldbackItemSpecified
        {
            get { return this.HoldbackItem != null && this.HoldbackItem.Count(h => h != null && h.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public HOLDBACK_ITEMS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
