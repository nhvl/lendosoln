namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class HOLDBACK_ITEM
    {
        /// <summary>
        /// Gets a value indicating whether the HOLDBACK_ITEM container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.EscrowHoldbackAmountSpecified
                    || this.EscrowHoldbackTypeOtherDescriptionSpecified
                    || this.EscrowHoldbackTypeSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// Amount held back at closing by the lender or escrow agent until a particular condition has been met.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount EscrowHoldbackAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowHoldbackAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowHoldbackAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowHoldbackAmountSpecified
        {
            get { return EscrowHoldbackAmount != null; }
            set { }
        }

        /// <summary>
        /// An enumerated list of values that describe the purpose of the escrow funds being held back.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<EscrowHoldbackBase> EscrowHoldbackType;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowHoldbackType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowHoldbackType element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowHoldbackTypeSpecified
        {
            get { return this.EscrowHoldbackType != null && this.EscrowHoldbackType.enumValue != EscrowHoldbackBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture the Escrow Holdback Type if Other is selected.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString EscrowHoldbackTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowHoldbackTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowHoldbackTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowHoldbackTypeOtherDescriptionSpecified
        {
            get { return EscrowHoldbackTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public HOLDBACK_ITEM_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
