namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class INTERIOR_ROOM_SUMMARIES
    {
        /// <summary>
        /// Gets a value indicating whether the INTERIOR_ROOM_SUMMARIES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.InteriorRoomSummarySpecified;
            }
        }

        /// <summary>
        /// A collection of interior room summaries.
        /// </summary>
        [XmlElement("INTERIOR_ROOM_SUMMARY", Order = 0)]
		public List<INTERIOR_ROOM_SUMMARY> InteriorRoomSummary = new List<INTERIOR_ROOM_SUMMARY>();

        /// <summary>
        /// Gets or sets a value indicating whether the InteriorRoomSummary element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InteriorRoomSummary element has been assigned a value.</value>
        [XmlIgnore]
        public bool InteriorRoomSummarySpecified
        {
            get { return this.InteriorRoomSummary != null && this.InteriorRoomSummary.Count(i => i != null && i.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public INTERIOR_ROOM_SUMMARIES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
