namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class INTERIOR_ROOM_SUMMARY
    {
        /// <summary>
        /// Gets a value indicating whether the INTERIOR_ROOM_SUMMARY container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AboveGradeIndicatorSpecified
                    || this.ExtensionSpecified
                    || this.RoomTypeOtherDescriptionSpecified
                    || this.RoomTypeSpecified
                    || this.TotalNumberOfRoomsAboveGradeCountSpecified
                    || this.TotalNumberOfRoomsBelowGradeCountSpecified
                    || this.TotalRoomCountSpecified;
            }
        }

        /// <summary>
        /// Indicates the room is above grade and can be included in GLA.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIndicator AboveGradeIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the AboveGradeIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AboveGradeIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool AboveGradeIndicatorSpecified
        {
            get { return AboveGradeIndicator != null; }
            set { }
        }

        /// <summary>
        /// Specifies a type of room normally found in a living unit or residence .
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<RoomBase> RoomType;

        /// <summary>
        /// Gets or sets a value indicating whether the RoomType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RoomType element has been assigned a value.</value>
        [XmlIgnore]
        public bool RoomTypeSpecified
        {
            get { return this.RoomType != null && this.RoomType.enumValue != RoomBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the room if Other is selected as the Room Type.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString RoomTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the RoomTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RoomTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool RoomTypeOtherDescriptionSpecified
        {
            get { return RoomTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The total number of rooms above grade.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOCount TotalNumberOfRoomsAboveGradeCount;

        /// <summary>
        /// Gets or sets a value indicating whether the TotalNumberOfRoomsAboveGradeCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TotalNumberOfRoomsAboveGradeCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TotalNumberOfRoomsAboveGradeCountSpecified
        {
            get { return TotalNumberOfRoomsAboveGradeCount != null; }
            set { }
        }

        /// <summary>
        /// The total number of rooms below grade.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOCount TotalNumberOfRoomsBelowGradeCount;

        /// <summary>
        /// Gets or sets a value indicating whether the TotalNumberOfRoomsBelowGradeCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TotalNumberOfRoomsBelowGradeCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TotalNumberOfRoomsBelowGradeCountSpecified
        {
            get { return TotalNumberOfRoomsBelowGradeCount != null; }
            set { }
        }

        /// <summary>
        /// Specifies the total number of livable rooms.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOCount TotalRoomCount;

        /// <summary>
        /// Gets or sets a value indicating whether the TotalRoomCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TotalRoomCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TotalRoomCountSpecified
        {
            get { return TotalRoomCount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 6)]
        public INTERIOR_ROOM_SUMMARY_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
