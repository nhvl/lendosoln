namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a CDATA object.
    /// </summary>
    /// <value>A CDATA block populated with the string Content.</value>
    public class AuthenticationTicket
    {
        /// <summary>
        /// Gets or sets the string content for the CDATA block.
        /// </summary>
        /// <value>The string contents for the CDATA block.</value>
        [XmlIgnore]
        public string Content { get; set; }

        /// <summary>
        /// Gets or sets a CDATA object.
        /// </summary>
        /// <value>A CDATA block populated with the string Content.</value>
        [XmlText]
        public XmlNode[] CDataContent
        {
            get
            {
                XmlDocument cdataDoc = new XmlDocument();
                return new XmlNode[] { cdataDoc.CreateCDataSection(this.Content) };
            }
            
            set
            {
                if (value == null || value.Length != 1)
                {
                    this.Content = string.Empty;
                    return;
                }

                this.Content = value[0].Value;
            }
        }
    }
}
