namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class MI_PREMIUM_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the MI_PREMIUM_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.MIPremiumAnnualPaymentAmountSpecified
                    || this.MIPremiumCalculationTypeOtherDescriptionSpecified
                    || this.MIPremiumCalculationTypeSpecified
                    || this.MIPremiumMonthlyPaymentAmountSpecified
                    || this.MIPremiumMonthlyPaymentRoundingTypeOtherDescriptionSpecified
                    || this.MIPremiumMonthlyPaymentRoundingTypeSpecified
                    || this.MIPremiumPeriodTypeOtherDescriptionSpecified
                    || this.MIPremiumPeriodTypeSpecified
                    || this.MIPremiumRateDurationMonthsCountSpecified
                    || this.MIPremiumRatePercentSpecified
                    || this.MIPremiumSequenceTypeSpecified
                    || this.MIPremiumUpfrontPercentSpecified;
            }
        }

        /// <summary>
        /// An annualized amount for the mortgage insurance premium.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount MIPremiumAnnualPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPremiumAnnualPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPremiumAnnualPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumAnnualPaymentAmountSpecified
        {
            get { return MIPremiumAnnualPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// Method by which new PMI premium is calculated.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<MIPremiumCalculationBase> MIPremiumCalculationType;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPremiumCalculationType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPremiumCalculationType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumCalculationTypeSpecified
        {
            get { return this.MIPremiumCalculationType != null && this.MIPremiumCalculationType.enumValue != MIPremiumCalculationBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for MI Premium Calculation Type Other Description.  
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString MIPremiumCalculationTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPremiumCalculationTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPremiumCalculationTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumCalculationTypeOtherDescriptionSpecified
        {
            get { return MIPremiumCalculationTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Monthly payment amount for the mortgage insurance premium.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOAmount MIPremiumMonthlyPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPremiumMonthlyPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPremiumMonthlyPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumMonthlyPaymentAmountSpecified
        {
            get { return MIPremiumMonthlyPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// The rounding method applied to the monthly payment amount.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<MIPremiumMonthlyPaymentRoundingBase> MIPremiumMonthlyPaymentRoundingType;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPremiumMonthlyPaymentRoundingType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPremiumMonthlyPaymentRoundingType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumMonthlyPaymentRoundingTypeSpecified
        {
            get { return this.MIPremiumMonthlyPaymentRoundingType != null && this.MIPremiumMonthlyPaymentRoundingType.enumValue != MIPremiumMonthlyPaymentRoundingBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text filed used to collect additional information when Other is selected for MI Premium Monthly Payment Rounding Type.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString MIPremiumMonthlyPaymentRoundingTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPremiumMonthlyPaymentRoundingTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPremiumMonthlyPaymentRoundingTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumMonthlyPaymentRoundingTypeOtherDescriptionSpecified
        {
            get { return MIPremiumMonthlyPaymentRoundingTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Defines the type of premium being paid during different stages of the mortgage insurance policy.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOEnum<MIPremiumPeriodBase> MIPremiumPeriodType;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPremiumPeriodType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPremiumPeriodType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumPeriodTypeSpecified
        {
            get { return this.MIPremiumPeriodType != null && this.MIPremiumPeriodType.enumValue != MIPremiumPeriodBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text filed used to collect additional information when Other is selected for MI Premium Period Type.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOString MIPremiumPeriodTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPremiumPeriodTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPremiumPeriodTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumPeriodTypeOtherDescriptionSpecified
        {
            get { return MIPremiumPeriodTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The number of months for which a given occurrence of a MI premium rate applies.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOCount MIPremiumRateDurationMonthsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPremiumRateDurationMonthsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPremiumRateDurationMonthsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumRateDurationMonthsCountSpecified
        {
            get { return MIPremiumRateDurationMonthsCount != null; }
            set { }
        }

        /// <summary>
        /// Rate percent used to calculate the premium amount.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOPercent MIPremiumRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPremiumRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPremiumRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumRatePercentSpecified
        {
            get { return MIPremiumRatePercent != null; }
            set { }
        }

        /// <summary>
        /// Specifies the order in which the MI premium rate and amount applies.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOEnum<MIPremiumSequenceBase> MIPremiumSequenceType;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPremiumSequenceType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPremiumSequenceType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumSequenceTypeSpecified
        {
            get { return this.MIPremiumSequenceType != null && this.MIPremiumSequenceType.enumValue != MIPremiumSequenceBase.Blank; }
            set { }
        }

        /// <summary>
        /// Reflects the actual up-front MI Premium percent associated with the plan.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOPercent MIPremiumUpfrontPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPremiumUpfrontPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPremiumUpfrontPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumUpfrontPercentSpecified
        {
            get { return MIPremiumUpfrontPercent != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 12)]
        public MI_PREMIUM_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
