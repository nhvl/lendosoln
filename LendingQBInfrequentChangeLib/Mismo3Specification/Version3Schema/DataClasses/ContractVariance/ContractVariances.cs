namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CONTRACT_VARIANCES
    {
        /// <summary>
        /// Gets a value indicating whether the CONTRACT_VARIANCES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ContractVarianceSpecified
                    || this.ExtensionSpecified;
            }
        }


        /// <summary>
        /// A collection of contract variances.
        /// </summary>
        [XmlElement("CONTRACT_VARIANCE", Order = 0)]
		public List<CONTRACT_VARIANCE> ContractVariance = new List<CONTRACT_VARIANCE>();

        /// <summary>
        /// Gets or sets a value indicating whether the ContractVariance element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ContractVariance element has been assigned a value.</value>
        [XmlIgnore]
        public bool ContractVarianceSpecified
        {
            get { return this.ContractVariance != null && this.ContractVariance.Count(c => c != null && c.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public CONTRACT_VARIANCES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
