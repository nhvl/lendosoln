namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class CONTRACT_VARIANCE
    {
        /// <summary>
        /// Gets a value indicating whether the CONTRACT_VARIANCE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ContractVarianceCodeIssuerTypeOtherDescriptionSpecified
                    || this.ContractVarianceCodeIssuerTypeSpecified
                    || this.ContractVarianceCodeSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A code assigned by the contracting system identifying a variance from the master contract.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOCode ContractVarianceCode;

        /// <summary>
        /// Gets or sets a value indicating whether the ContractVarianceCode element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ContractVarianceCode element has been assigned a value.</value>
        [XmlIgnore]
        public bool ContractVarianceCodeSpecified
        {
            get { return ContractVarianceCode != null; }
            set { }
        }

        /// <summary>
        /// Identifying the party who issued the variance code.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<ContractVarianceCodeIssuerBase> ContractVarianceCodeIssuerType;

        /// <summary>
        /// Gets or sets a value indicating whether the ContractVarianceCodeIssuerType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ContractVarianceCodeIssuerType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ContractVarianceCodeIssuerTypeSpecified
        {
            get { return this.ContractVarianceCodeIssuerType != null && this.ContractVarianceCodeIssuerType.enumValue != ContractVarianceCodeIssuerBase.Blank; }
            set { }
        }

        /// <summary>
        /// The description of the Contract Variance Code Issuer Type when Other is selected as the option from the enumerated list.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString ContractVarianceCodeIssuerTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ContractVarianceCodeIssuerTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ContractVarianceCodeIssuerTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ContractVarianceCodeIssuerTypeOtherDescriptionSpecified
        {
            get { return ContractVarianceCodeIssuerTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public CONTRACT_VARIANCE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
