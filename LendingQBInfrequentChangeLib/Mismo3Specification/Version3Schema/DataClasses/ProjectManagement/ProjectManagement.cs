namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PROJECT_MANAGEMENT
    {
        /// <summary>
        /// Gets a value indicating whether the PROJECT_MANAGEMENT container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ProjectManagementTypeOtherDescriptionSpecified
                    || this.ProjectManagementTypeSpecified
                    || this.ProjectPartOfMasterAssociationDescriptionSpecified
                    || this.ProjectPartOfMasterAssociationIndicatorSpecified;
            }
        }

        /// <summary>
        /// Identifies type of management associated with the project.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<ProjectManagementBase> ProjectManagementType;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectManagementType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectManagementType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectManagementTypeSpecified
        {
            get { return this.ProjectManagementType != null && this.ProjectManagementType.enumValue != ProjectManagementBase.Blank; }
            set { }
        }

        /// <summary>
        /// Identifies type of management associated with the project.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString ProjectManagementTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectManagementTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectManagementTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectManagementTypeOtherDescriptionSpecified
        {
            get { return ProjectManagementTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field describing the relationship of the project to a master association.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString ProjectPartOfMasterAssociationDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectPartOfMasterAssociationDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectPartOfMasterAssociationDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectPartOfMasterAssociationDescriptionSpecified
        {
            get { return ProjectPartOfMasterAssociationDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the project is part of a master association.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOIndicator ProjectPartOfMasterAssociationIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectPartOfMasterAssociationIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectPartOfMasterAssociationIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectPartOfMasterAssociationIndicatorSpecified
        {
            get { return ProjectPartOfMasterAssociationIndicator != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public PROJECT_MANAGEMENT_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
