namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class STRUCTURE_ANALYSIS_3_3_1
    {
        /// <summary>
        /// Gets a value indicating whether the STRUCTURE_ANALYSIS_3_3_1 container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.StructureAnalysisSummarySpecified;
            }
        }

        /// <summary>
        /// A summary of the structure analysis.
        /// </summary>
        [XmlElement("STRUCTURE_ANALYSIS_SUMMARY", Order = 0)]
        public STRUCTURE_ANALYSIS_SUMMARY StructureAnalysisSummary;

        /// <summary>
        /// Gets or sets a value indicating whether the StructureAnalysisSummary element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the StructureAnalysisSummary element has been assigned a value.</value>
        [XmlIgnore]
        public bool StructureAnalysisSummarySpecified
        {
            get { return this.StructureAnalysisSummary != null && this.StructureAnalysisSummary.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public STRUCTURE_ANALYSIS_3_3_1_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
