namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class STRUCTURE_ANALYSIS_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the STRUCTURE_ANALYSIS_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ComponentStatusTimeframeTypeOtherDescriptionSpecified
                    || this.ComponentStatusTimeframeTypeSpecified
                    || this.ComponentStatusTypeOtherDescriptionSpecified
                    || this.ComponentStatusTypeSpecified
                    || this.EffectiveAgeRangeHighYearsCountSpecified
                    || this.EffectiveAgeRangeLowYearsCountSpecified
                    || this.EffectiveAgeYearsCountSpecified
                    || this.ExtensionSpecified
                    || this.MaterialUpdateAfterOriginalConstructionIndicatorSpecified
                    || this.MaterialUpdateTimeFrameRangeHighYearsCountSpecified
                    || this.MaterialUpdateTimeFrameRangeLowYearsCountSpecified
                    || this.OverallConditionRatingIdentifierSpecified
                    || this.OverallQualityRatingIdentifierSpecified
                    || this.PropertyCondemnedDateSpecified
                    || this.PropertyDamageNotificationDateSpecified
                    || this.RemainingEconomicLifeRangeHighYearsCountSpecified
                    || this.RemainingEconomicLifeRangeLowYearsCountSpecified
                    || this.StructureAppealDescriptionSpecified;
            }
        }

        /// <summary>
        /// The estimated time period associated with the updating or remodeling of the property described by the Component Status Type.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<ComponentStatusTimeframeBase> ComponentStatusTimeframeType;

        /// <summary>
        /// Gets or sets a value indicating whether the ComponentStatusTimeframeType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ComponentStatusTimeframeType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ComponentStatusTimeframeTypeSpecified
        {
            get { return this.ComponentStatusTimeframeType != null && this.ComponentStatusTimeframeType.enumValue != ComponentStatusTimeframeBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free form text field used to collect additional information when Other is selected for Component Status Timeframe Type.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString ComponentStatusTimeframeTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ComponentStatusTimeframeTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ComponentStatusTimeframeTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ComponentStatusTimeframeTypeOtherDescriptionSpecified
        {
            get { return ComponentStatusTimeframeTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// An opinion on the level of update or improvement completed on the structure subsequent to its original construction.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<ComponentStatusBase> ComponentStatusType;

        /// <summary>
        /// Gets or sets a value indicating whether the ComponentStatusType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ComponentStatusType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ComponentStatusTypeSpecified
        {
            get { return this.ComponentStatusType != null && this.ComponentStatusType.enumValue != ComponentStatusBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free form text field used to collect additional information when Other is selected for Component Status Type.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString ComponentStatusTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ComponentStatusTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ComponentStatusTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ComponentStatusTypeOtherDescriptionSpecified
        {
            get { return ComponentStatusTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// High end of the range of the estimated effective age reflecting condition and deterioration.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOCount EffectiveAgeRangeHighYearsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the EffectiveAgeRangeHighYearsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EffectiveAgeRangeHighYearsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool EffectiveAgeRangeHighYearsCountSpecified
        {
            get { return EffectiveAgeRangeHighYearsCount != null; }
            set { }
        }

        /// <summary>
        /// Low end of the range of the estimated effective age reflecting condition and deterioration.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOCount EffectiveAgeRangeLowYearsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the EffectiveAgeRangeLowYearsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EffectiveAgeRangeLowYearsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool EffectiveAgeRangeLowYearsCountSpecified
        {
            get { return EffectiveAgeRangeLowYearsCount != null; }
            set { }
        }

        /// <summary>
        /// The estimated effective age reflecting condition and deterioration.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOCount EffectiveAgeYearsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the EffectiveAgeYearsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EffectiveAgeYearsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool EffectiveAgeYearsCountSpecified
        {
            get { return EffectiveAgeYearsCount != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether updates in any area of the subject improvement are observed to have taken place since its original construction.  
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOIndicator MaterialUpdateAfterOriginalConstructionIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the MaterialUpdateAfterOriginalConstructionIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MaterialUpdateAfterOriginalConstructionIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool MaterialUpdateAfterOriginalConstructionIndicatorSpecified
        {
            get { return MaterialUpdateAfterOriginalConstructionIndicator != null; }
            set { }
        }

        /// <summary>
        /// The high end of the timeframe range associated with the update or improvement of the property component described by the Component Status Type.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOCount MaterialUpdateTimeFrameRangeHighYearsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the MaterialUpdateTimeFrameRangeHighYearsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MaterialUpdateTimeFrameRangeHighYearsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool MaterialUpdateTimeFrameRangeHighYearsCountSpecified
        {
            get { return MaterialUpdateTimeFrameRangeHighYearsCount != null; }
            set { }
        }

        /// <summary>
        /// The low end of the timeframe range associated with the update or improvement of the property component described by the Component Status Type.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOCount MaterialUpdateTimeFrameRangeLowYearsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the MaterialUpdateTimeFrameRangeLowYearsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MaterialUpdateTimeFrameRangeLowYearsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool MaterialUpdateTimeFrameRangeLowYearsCountSpecified
        {
            get { return MaterialUpdateTimeFrameRangeLowYearsCount != null; }
            set { }
        }

        /// <summary>
        /// The overall condition rating of the improvements of property. The party assigning the identifier can be provided using the IdentifierOwnerURI associated with a MISMO identifier.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOIdentifier OverallConditionRatingIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the OverallConditionRatingIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OverallConditionRatingIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool OverallConditionRatingIdentifierSpecified
        {
            get { return OverallConditionRatingIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The overall quality rating of the improvements of property. The party assigning the identifier can be provided using the IdentifierOwnerURI associated with a MISMO identifier.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOIdentifier OverallQualityRatingIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the OverallQualityRatingIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OverallQualityRatingIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool OverallQualityRatingIdentifierSpecified
        {
            get { return OverallQualityRatingIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The date the property condemnation was issued.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMODate PropertyCondemnedDate;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyCondemnedDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyCondemnedDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyCondemnedDateSpecified
        {
            get { return PropertyCondemnedDate != null; }
            set { }
        }

        /// <summary>
        /// Date when notification of property damage is received.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMODate PropertyDamageNotificationDate;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyDamageNotificationDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyDamageNotificationDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyDamageNotificationDateSpecified
        {
            get { return PropertyDamageNotificationDate != null; }
            set { }
        }

        /// <summary>
        /// High end of the range of the estimated remaining economic life.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOCount RemainingEconomicLifeRangeHighYearsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the RemainingEconomicLifeRangeHighYearsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RemainingEconomicLifeRangeHighYearsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool RemainingEconomicLifeRangeHighYearsCountSpecified
        {
            get { return RemainingEconomicLifeRangeHighYearsCount != null; }
            set { }
        }

        /// <summary>
        /// Low end of the range of the estimated remaining economic life.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOCount RemainingEconomicLifeRangeLowYearsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the RemainingEconomicLifeRangeLowYearsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RemainingEconomicLifeRangeLowYearsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool RemainingEconomicLifeRangeLowYearsCountSpecified
        {
            get { return RemainingEconomicLifeRangeLowYearsCount != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe aspects of the structure that add or detract from its appeal.
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMOString StructureAppealDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the StructureAppealDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the StructureAppealDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool StructureAppealDescriptionSpecified
        {
            get { return StructureAppealDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 17)]
        public STRUCTURE_ANALYSIS_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
