namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class STRUCTURE_ANALYSES
    {
        /// <summary>
        /// Gets a value indicating whether the STRUCTURE_ANALYSES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.StructureAnalysisSpecified;
            }
        }

        /// <summary>
        /// A collection of structure analyses.
        /// </summary>
        [XmlElement("STRUCTURE_ANALYSIS", Order = 0)]
		public List<STRUCTURE_ANALYSIS> StructureAnalysis = new List<STRUCTURE_ANALYSIS>();

        /// <summary>
        /// Gets or sets a value indicating whether the StructureAnalysis element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the StructureAnalysis element has been assigned a value.</value>
        [XmlIgnore]
        public bool StructureAnalysisSpecified
        {
            get { return this.StructureAnalysis != null && this.StructureAnalysis.Count(s => s != null && s.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public STRUCTURE_ANALYSES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
