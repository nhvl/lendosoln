namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class STRUCTURE_ANALYSIS_DETAIL_3_3_1
    {
        /// <summary>
        /// Gets a value indicating whether the STRUCTURE_ANALYSIS_DETAIL_3_3_1 container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AnalysisComponentActualCostAmountSpecified
                    || this.AnalysisComponentEstimatedCostAmountSpecified
                    || this.AnalysisComponentTypeOtherDescriptionSpecified
                    || this.AnalysisComponentTypeSpecified
                    || this.CostOfAnalysisComponentStatusTypeSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// The actual amount of the cost of a structural component that is being analyzed as specified by the Analysis Component Type.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount AnalysisComponentActualCostAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the AnalysisComponentActualCostAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AnalysisComponentActualCostAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool AnalysisComponentActualCostAmountSpecified
        {
            get { return AnalysisComponentActualCostAmount != null; }
            set { }
        }

        /// <summary>
        /// The estimated amount of the cost of a structural component that is being analyzed as specified by the Analysis Component Type.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOAmount AnalysisComponentEstimatedCostAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the AnalysisComponentEstimatedCostAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AnalysisComponentEstimatedCostAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool AnalysisComponentEstimatedCostAmountSpecified
        {
            get { return AnalysisComponentEstimatedCostAmount != null; }
            set { }
        }

        /// <summary>
        /// A collection of values that specify the component, room or structural element being analyzed.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<AnalysisComponentBase> AnalysisComponentType;

        /// <summary>
        /// Gets or sets a value indicating whether the AnalysisComponentType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AnalysisComponentType element has been assigned a value.</value>
        [XmlIgnore]
        public bool AnalysisComponentTypeSpecified
        {
            get { return this.AnalysisComponentType != null && this.AnalysisComponentType.enumValue != AnalysisComponentBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field that specifies the enumeration when the value of Other is used for the Analysis Component Type. 
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString AnalysisComponentTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the AnalysisComponentTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AnalysisComponentTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool AnalysisComponentTypeOtherDescriptionSpecified
        {
            get { return AnalysisComponentTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A collection of values that specify the level of completeness of the specified Analysis Component Type for which a cost is being provided.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<CostOfAnalysisComponentStatusBase> CostOfAnalysisComponentStatusType;

        /// <summary>
        /// Gets or sets a value indicating whether the CostOfAnalysisComponentStatusType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CostOfAnalysisComponentStatusType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CostOfAnalysisComponentStatusTypeSpecified
        {
            get { return this.CostOfAnalysisComponentStatusType != null && this.CostOfAnalysisComponentStatusType.enumValue != CostOfAnalysisComponentStatusBase.Blank; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 5)]
        public STRUCTURE_ANALYSIS_DETAIL_3_3_1_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
