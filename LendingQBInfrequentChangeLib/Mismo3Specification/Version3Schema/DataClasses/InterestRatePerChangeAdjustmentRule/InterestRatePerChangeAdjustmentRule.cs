namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class INTEREST_RATE_PER_CHANGE_ADJUSTMENT_RULE
    {
        /// <summary>
        /// Gets a value indicating whether the INTEREST_RATE_PER_CHANGE_ADJUSTMENT_RULE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AdjustmentRuleTypeSpecified
                    || this.ExtensionSpecified
                    || this.NoInterestRateCapsFeatureDescriptionSpecified
                    || this.PaymentsBetweenRateChangeNoticesCountSpecified
                    || this.PaymentsBetweenRateChangesCountSpecified
                    || this.PaymentsToFirstInterestRatenumValueCountSpecified
                    || this.PerChangeCeilingRatePercentSpecified
                    || this.PerChangeFactorOfIndexPercentSpecified
                    || this.PerChangeFloorRatePercentSpecified
                    || this.PerChangeInterestRateRoundingTimingTypeSpecified
                    || this.PerChangeMaximumDecreaseRatePercentSpecified
                    || this.PerChangeMaximumIncreaseRatePercentSpecified
                    || this.PerChangeMinimumDecreaseRatePercentSpecified
                    || this.PerChangeMinimumIncreaseRatePercentSpecified
                    || this.PerChangeRateAdjustmentCalculationMethodDescriptionSpecified
                    || this.PerChangeRateAdjustmentEffectiveDateSpecified
                    || this.PerChangeRateAdjustmentEffectiveMonthsCountSpecified
                    || this.PerChangeRateAdjustmentFrequencyMonthsCountSpecified
                    || this.PerChangeRateAdjustmentPaymentsCountSpecified;
            }
        }

        /// <summary>
        /// Specifies whether the occurrence of the adjustment is the first change or a subsequent change.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<AdjustmentRuleBase> AdjustmentRuleType;

        /// <summary>
        /// Gets or sets a value indicating whether the AdjustmentRuleType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AdjustmentRuleType element has been assigned a value.</value>
        [XmlIgnore]
        public bool AdjustmentRuleTypeSpecified
        {
            get { return this.AdjustmentRuleType != null && this.AdjustmentRuleType.enumValue != AdjustmentRuleBase.Blank; }
            set { }
        }

        /// <summary>
        /// A description of any interest rate calculation period that ignores per change, periodic or lifetime interest rate limits.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString NoInterestRateCapsFeatureDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the NoInterestRateCapsFeatureDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NoInterestRateCapsFeatureDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool NoInterestRateCapsFeatureDescriptionSpecified
        {
            get { return NoInterestRateCapsFeatureDescription != null; }
            set { }
        }

        /// <summary>
        /// The number of payment periods between interest rate change dates that notices are generated, if notices are not sent to the mortgagor for every change. If a loans interest rate changes every month but notices are only mailed once a year, 12 will be entered in this field.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOCount PaymentsBetweenRateChangeNoticesCount;

        /// <summary>
        /// Gets or sets a value indicating whether the PaymentsBetweenRateChangeNoticesCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PaymentsBetweenRateChangeNoticesCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PaymentsBetweenRateChangeNoticesCountSpecified
        {
            get { return PaymentsBetweenRateChangeNoticesCount != null; }
            set { }
        }

        /// <summary>
        /// The number of payments between rate changes.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOCount PaymentsBetweenRateChangesCount;

        /// <summary>
        /// Gets or sets a value indicating whether the PaymentsBetweenRateChangesCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PaymentsBetweenRateChangesCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PaymentsBetweenRateChangesCountSpecified
        {
            get { return PaymentsBetweenRateChangesCount != null; }
            set { }
        }

        /// <summary>
        /// Value of payments to first interest rate.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOCount PaymentsToFirstInterestRatenumValueCount;

        /// <summary>
        /// Gets or sets a value indicating whether the PaymentsToFirstInterestRateNumValueCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PaymentsToFirstInterestRateNumValueCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PaymentsToFirstInterestRatenumValueCountSpecified
        {
            get { return PaymentsToFirstInterestRatenumValueCount != null; }
            set { }
        }

        /// <summary>
        /// The stated maximum, expressed as a percent, to which the interest rate can increase to in this adjustment period.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOPercent PerChangeCeilingRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the PerChangeCeilingRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PerChangeCeilingRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool PerChangeCeilingRatePercentSpecified
        {
            get { return PerChangeCeilingRatePercent != null; }
            set { }
        }

        /// <summary>
        /// The percentage by which the new index is multiplied to arrive at a new interest rate. Most commonly used for Fannie Mae STABLE ARM products.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOPercent PerChangeFactorOfIndexPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the PerChangeFactorOfIndexPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PerChangeFactorOfIndexPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool PerChangeFactorOfIndexPercentSpecified
        {
            get { return PerChangeFactorOfIndexPercent != null; }
            set { }
        }

        /// <summary>
        /// The stated minimum, expressed as a percent, to which the interest rate can decrease to in this adjustment period.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOPercent PerChangeFloorRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the PerChangeFloorRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PerChangeFloorRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool PerChangeFloorRatePercentSpecified
        {
            get { return PerChangeFloorRatePercent != null; }
            set { }
        }

        /// <summary>
        /// Describes when the rate is rounded relative to checking other rules.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOEnum<PerChangeInterestRateRoundingTimingBase> PerChangeInterestRateRoundingTimingType;

        /// <summary>
        /// Gets or sets a value indicating whether the PerChangeInterestRateRoundingTimingType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PerChangeInterestRateRoundingTimingType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PerChangeInterestRateRoundingTimingTypeSpecified
        {
            get { return this.PerChangeInterestRateRoundingTimingType != null && this.PerChangeInterestRateRoundingTimingType.enumValue != PerChangeInterestRateRoundingTimingBase.Blank; }
            set { }
        }

        /// <summary>
        /// The maximum number of percentage points by which the rate can decrease from the previous interest rate.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOPercent PerChangeMaximumDecreaseRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the PerChangeMaximumDecreaseRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PerChangeMaximumDecreaseRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool PerChangeMaximumDecreaseRatePercentSpecified
        {
            get { return PerChangeMaximumDecreaseRatePercent != null; }
            set { }
        }

        /// <summary>
        /// The maximum number of percentage points by which the rate can increase from the previous interest rate.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOPercent PerChangeMaximumIncreaseRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the PerChangeMaximumIncreaseRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PerChangeMaximumIncreaseRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool PerChangeMaximumIncreaseRatePercentSpecified
        {
            get { return PerChangeMaximumIncreaseRatePercent != null; }
            set { }
        }

        /// <summary>
        /// The smallest calculated interest rate decrease in a single adjustment period that requires an interest rate change. If the decrease is less than this percentage, then the interest rate will not change at the time of calculation.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOPercent PerChangeMinimumDecreaseRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the PerChangeMinimumDecreaseRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PerChangeMinimumDecreaseRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool PerChangeMinimumDecreaseRatePercentSpecified
        {
            get { return PerChangeMinimumDecreaseRatePercent != null; }
            set { }
        }

        /// <summary>
        /// The smallest calculated interest rate increase in a single adjustment period that requires an interest rate change. If the increase is less than this percentage, then the interest rate will not change at the time of calculation.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOPercent PerChangeMinimumIncreaseRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the PerChangeMinimumIncreaseRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PerChangeMinimumIncreaseRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool PerChangeMinimumIncreaseRatePercentSpecified
        {
            get { return PerChangeMinimumIncreaseRatePercent != null; }
            set { }
        }

        /// <summary>
        /// Indicates how interest rate changes are to be calculated.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOString PerChangeRateAdjustmentCalculationMethodDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PerChangeRateAdjustmentCalculationMethodDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PerChangeRateAdjustmentCalculationMethodDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PerChangeRateAdjustmentCalculationMethodDescriptionSpecified
        {
            get { return PerChangeRateAdjustmentCalculationMethodDescription != null; }
            set { }
        }

        /// <summary>
        /// The date when the interest rate per change adjustment rule first becomes applicable. The per change rule remains in effect unless another per change rule with a later date is present on the loan.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMODate PerChangeRateAdjustmentEffectiveDate;

        /// <summary>
        /// Gets or sets a value indicating whether the PerChangeRateAdjustmentEffectiveDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PerChangeRateAdjustmentEffectiveDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool PerChangeRateAdjustmentEffectiveDateSpecified
        {
            get { return PerChangeRateAdjustmentEffectiveDate != null; }
            set { }
        }

        /// <summary>
        /// The period of time, expressed in months, that the Interest Rate Per Change Adjustment Rule is in effect.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOCount PerChangeRateAdjustmentEffectiveMonthsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the PerChangeRateAdjustmentEffectiveMonthsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PerChangeRateAdjustmentEffectiveMonthsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PerChangeRateAdjustmentEffectiveMonthsCountSpecified
        {
            get { return PerChangeRateAdjustmentEffectiveMonthsCount != null; }
            set { }
        }

        /// <summary>
        /// The number of months between rate adjustments, if the interest rate on the subject loan can change.
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMOCount PerChangeRateAdjustmentFrequencyMonthsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the PerChangeRateAdjustmentFrequencyMonthsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PerChangeRateAdjustmentFrequencyMonthsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PerChangeRateAdjustmentFrequencyMonthsCountSpecified
        {
            get { return PerChangeRateAdjustmentFrequencyMonthsCount != null; }
            set { }
        }

        /// <summary>
        /// The number of payments for which the Interest Rate Per Change Adjustment Rule is applicable.
        /// </summary>
        [XmlElement(Order = 17)]
        public MISMOCount PerChangeRateAdjustmentPaymentsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the PerChangeRateAdjustmentPaymentsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PerChangeRateAdjustmentPaymentsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PerChangeRateAdjustmentPaymentsCountSpecified
        {
            get { return PerChangeRateAdjustmentPaymentsCount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 18)]
        public INTEREST_RATE_PER_CHANGE_ADJUSTMENT_RULE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
