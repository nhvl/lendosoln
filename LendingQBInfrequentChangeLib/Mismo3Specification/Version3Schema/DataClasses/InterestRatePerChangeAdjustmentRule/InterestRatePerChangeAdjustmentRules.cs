namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class INTEREST_RATE_PER_CHANGE_ADJUSTMENT_RULES
    {
        /// <summary>
        /// Gets a value indicating whether the INTEREST_RATE_PER_CHANGE_ADJUSTMENT_RULES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.InterestRatePerChangeAdjustmentRuleSpecified;
            }
        }

        /// <summary>
        /// A collection of interest rate per change adjustment rules.
        /// </summary>
        [XmlElement("INTEREST_RATE_PER_CHANGE_ADJUSTMENT_RULE", Order = 0)]
		public List<INTEREST_RATE_PER_CHANGE_ADJUSTMENT_RULE> InterestRatePerChangeAdjustmentRule = new List<INTEREST_RATE_PER_CHANGE_ADJUSTMENT_RULE>();

        /// <summary>
        /// Gets or sets a value indicating whether the InterestRatePerChangeAdjustmentRule element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InterestRatePerChangeAdjustmentRule element has been assigned a value.</value>
        [XmlIgnore]
        public bool InterestRatePerChangeAdjustmentRuleSpecified
        {
            get { return this.InterestRatePerChangeAdjustmentRule != null && this.InterestRatePerChangeAdjustmentRule.Count(i => i != null && i.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public INTEREST_RATE_PER_CHANGE_ADJUSTMENT_RULES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
