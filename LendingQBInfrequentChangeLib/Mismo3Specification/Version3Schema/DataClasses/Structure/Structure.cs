namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class STRUCTURE
    {
        /// <summary>
        /// Gets a value indicating whether the STRUCTURE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ArchitecturalDesignSpecified
                    || this.ExtensionSpecified
                    || this.ExteriorFeaturesSpecified
                    || this.ExteriorWallsSpecified
                    || this.FoundationsSpecified
                    || this.InsulationSpecified
                    || this.RoofSpecified
                    || this.StructureAnalysesSpecified
                    || this.StructureDetailSpecified
                    || this.WindowsSpecified;
            }
        }

        /// <summary>
        /// The design of the structure.
        /// </summary>
        [XmlElement("ARCHITECTURAL_DESIGN", Order = 0)]
        public ARCHITECTURAL_DESIGN ArchitecturalDesign;

        /// <summary>
        /// Gets or sets a value indicating whether the ArchitecturalDesign element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ArchitecturalDesign element has been assigned a value.</value>
        [XmlIgnore]
        public bool ArchitecturalDesignSpecified
        {
            get { return this.ArchitecturalDesign != null && this.ArchitecturalDesign.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Describes the exterior features.
        /// </summary>
        [XmlElement("EXTERIOR_FEATURES", Order = 1)]
        public EXTERIOR_FEATURES ExteriorFeatures;

        /// <summary>
        /// Gets or sets a value indicating whether the ExteriorFeatures element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ExteriorFeatures element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExteriorFeaturesSpecified
        {
            get { return this.ExteriorFeatures != null && this.ExteriorFeatures.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Describes the exterior walls.
        /// </summary>
        [XmlElement("EXTERIOR_WALLS", Order = 2)]
        public EXTERIOR_WALLS ExteriorWalls;

        /// <summary>
        /// Gets or sets a value indicating whether the ExteriorWalls element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ExteriorWalls element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExteriorWallsSpecified
        {
            get { return this.ExteriorWalls != null && this.ExteriorWalls.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Describes the foundations.
        /// </summary>
        [XmlElement("FOUNDATIONS", Order = 3)]
        public FOUNDATIONS Foundations;

        /// <summary>
        /// Gets or sets a value indicating whether the Foundations element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Foundations element has been assigned a value.</value>
        [XmlIgnore]
        public bool FoundationsSpecified
        {
            get { return this.Foundations != null && this.Foundations.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Describes the insulation.
        /// </summary>
        [XmlElement("INSULATION", Order = 4)]
        public INSULATION Insulation;

        /// <summary>
        /// Gets or sets a value indicating whether the Insulation element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Insulation element has been assigned a value.</value>
        [XmlIgnore]
        public bool InsulationSpecified
        {
            get { return this.Insulation != null && this.Insulation.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Describes the roof.
        /// </summary>
        [XmlElement("ROOF", Order = 5)]
        public ROOF Roof;

        /// <summary>
        /// Gets or sets a value indicating whether the Roof element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Roof element has been assigned a value.</value>
        [XmlIgnore]
        public bool RoofSpecified
        {
            get { return this.Roof != null && this.Roof.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Analyses of the structure.
        /// </summary>
        [XmlElement("STRUCTURE_ANALYSES", Order = 6)]
        public STRUCTURE_ANALYSES StructureAnalyses;

        /// <summary>
        /// Gets or sets a value indicating whether the StructureAnalyses element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the StructureAnalyses element has been assigned a value.</value>
        [XmlIgnore]
        public bool StructureAnalysesSpecified
        {
            get { return this.StructureAnalyses != null && this.StructureAnalyses.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details of the structure.
        /// </summary>
        [XmlElement("STRUCTURE_DETAIL", Order = 7)]
        public STRUCTURE_DETAIL StructureDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the StructureDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the StructureDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool StructureDetailSpecified
        {
            get { return this.StructureDetail != null && this.StructureDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The windows in the structure.
        /// </summary>
        [XmlElement("WINDOWS", Order = 8)]
        public WINDOWS Windows;

        /// <summary>
        /// Gets or sets a value indicating whether the Windows element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Windows element has been assigned a value.</value>
        [XmlIgnore]
        public bool WindowsSpecified
        {
            get { return this.Windows != null && this.Windows.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 9)]
        public STRUCTURE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
