namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class ADJUSTMENT
    {
        /// <summary>
        /// Gets a value indicating whether the ADJUSTMENT container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ConversionAdjustmentSpecified
                    || this.ExtensionSpecified
                    || this.InterestRateAdjustmentSpecified
                    || this.PrincipalAndInterestPaymentAdjustmentSpecified
                    || this.RateOrPaymentChangeOccurrencesSpecified;
            }
        }

        /// <summary>
        /// Adjustments to the conversion option.
        /// </summary>
        [XmlElement("CONVERSION_ADJUSTMENT", Order = 0)]
        public CONVERSION_ADJUSTMENT ConversionAdjustment;

        /// <summary>
        /// Gets or sets a value indicating whether the ConversionAdjustment element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConversionAdjustment element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConversionAdjustmentSpecified
        {
            get { return this.ConversionAdjustment != null && this.ConversionAdjustment.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Adjustments to the interest rate.
        /// </summary>
        [XmlElement("INTEREST_RATE_ADJUSTMENT", Order = 1)]
        public INTEREST_RATE_ADJUSTMENT InterestRateAdjustment;

        /// <summary>
        /// Gets or sets a value indicating whether the InterestRateAdjustment element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InterestRateAdjustment element has been assigned a value.</value>
        [XmlIgnore]
        public bool InterestRateAdjustmentSpecified
        {
            get { return this.InterestRateAdjustment != null && this.InterestRateAdjustment.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Adjustments to the principal and interest payment.
        /// </summary>
        [XmlElement("PRINCIPAL_AND_INTEREST_PAYMENT_ADJUSTMENT", Order = 2)]
        public PRINCIPAL_AND_INTEREST_PAYMENT_ADJUSTMENT PrincipalAndInterestPaymentAdjustment;

        /// <summary>
        /// Gets or sets a value indicating whether the PrincipalAndInterestPaymentAdjustment element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrincipalAndInterestPaymentAdjustment element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrincipalAndInterestPaymentAdjustmentSpecified
        {
            get { return this.PrincipalAndInterestPaymentAdjustment != null && this.PrincipalAndInterestPaymentAdjustment.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Occurrences of a rate/payment change.
        /// </summary>
        [XmlElement("RATE_OR_PAYMENT_CHANGE_OCCURRENCES", Order = 3)]
        public RATE_OR_PAYMENT_CHANGE_OCCURRENCES RateOrPaymentChangeOccurrences;

        /// <summary>
        /// Gets or sets a value indicating whether the RateOrPaymentChangeOccurrences element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RateOrPaymentChangeOccurrences element has been assigned a value.</value>
        [XmlIgnore]
        public bool RateOrPaymentChangeOccurrencesSpecified
        {
            get { return this.RateOrPaymentChangeOccurrences != null && this.RateOrPaymentChangeOccurrences.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public ADJUSTMENT_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
