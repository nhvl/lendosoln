namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class SITE_LOCATIONS
    {
        /// <summary>
        /// Gets a value indicating whether the SITE_LOCATIONS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.SiteLocationSpecified;
            }
        }

        /// <summary>
        /// A collection of site locations.
        /// </summary>
        [XmlElement("SITE_LOCATION", Order = 0)]
		public List<SITE_LOCATION> SiteLocation = new List<SITE_LOCATION>();

        /// <summary>
        /// Gets or sets a value indicating whether the SiteLocation element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SiteLocation element has been assigned a value.</value>
        [XmlIgnore]
        public bool SiteLocationSpecified
        {
            get { return this.SiteLocation != null && this.SiteLocation.Count(s => s != null && s.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public SITE_LOCATIONS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
