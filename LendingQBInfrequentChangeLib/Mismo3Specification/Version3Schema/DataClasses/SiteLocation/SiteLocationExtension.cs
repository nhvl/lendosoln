namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Other element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Other element has been assigned a value.</value>
    public partial class SITE_LOCATION_EXTENSION
    {
        /// <summary>
        /// A generic Mortgage Industry Standards Maintenance Organization element for allowing custom extension.
        /// </summary>
        [XmlElement("MISMO", Order = 0)]
        public SITE_LOCATION_3_3_1 Mismo;

        /// <summary>
        /// Gets or sets a value indicating whether the Mortgage Industry Standards Maintenance Organization element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Mortgage Industry Standards Maintenance Organization element has been assigned a value.</value>
        [XmlIgnore]
        public bool MismoSpecified
        {
            get { return Mismo != null; }
            set { }
        }

        /// <summary>
        /// A generic miscellaneous element for allowing custom extension.
        /// </summary>
        [XmlElement("OTHER", Order = 1)]
        public OTHER_BASE Other;

        /// <summary>
        /// Gets or sets a value indicating whether the Other element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Other element has been assigned a value.</value>
        [XmlIgnore]
        public bool OtherSpecified
        {
            get { return Other != null; }
            set { }
        }
    }
}
