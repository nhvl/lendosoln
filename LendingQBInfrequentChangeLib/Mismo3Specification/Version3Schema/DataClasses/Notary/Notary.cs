namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class NOTARY
    {
        /// <summary>
        /// Gets a value indicating whether the NOTARY container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.NotaryCommissionBondNumberIdentifierSpecified
                    || this.NotaryCommissionCityNameSpecified
                    || this.NotaryCommissionCountyNameSpecified
                    || this.NotaryCommissionExpirationDateSpecified
                    || this.NotaryCommissionNumberIdentifierSpecified
                    || this.NotaryCommissionStateNameSpecified;
            }
        }

        /// <summary>
        /// The identifying commission bond number of the notary.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIdentifier NotaryCommissionBondNumberIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the NotaryCommissionBondNumberIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NotaryCommissionBondNumberIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool NotaryCommissionBondNumberIdentifierSpecified
        {
            get { return NotaryCommissionBondNumberIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The city where the notary is commissioned.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString NotaryCommissionCityName;

        /// <summary>
        /// Gets or sets a value indicating whether the NotaryCommissionCityName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NotaryCommissionCityName element has been assigned a value.</value>
        [XmlIgnore]
        public bool NotaryCommissionCityNameSpecified
        {
            get { return NotaryCommissionCityName != null; }
            set { }
        }

        /// <summary>
        /// The county in which the notary is commissioned.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString NotaryCommissionCountyName;

        /// <summary>
        /// Gets or sets a value indicating whether the NotaryCommissionCountyName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NotaryCommissionCountyName element has been assigned a value.</value>
        [XmlIgnore]
        public bool NotaryCommissionCountyNameSpecified
        {
            get { return NotaryCommissionCountyName != null; }
            set { }
        }

        /// <summary>
        /// The expiration date of the commission of the notary .
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMODate NotaryCommissionExpirationDate;

        /// <summary>
        /// Gets or sets a value indicating whether the NotaryCommissionExpirationDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NotaryCommissionExpirationDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool NotaryCommissionExpirationDateSpecified
        {
            get { return NotaryCommissionExpirationDate != null; }
            set { }
        }

        /// <summary>
        /// The identifying commission number assigned to the notary.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOIdentifier NotaryCommissionNumberIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the NotaryCommissionNumberIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NotaryCommissionNumberIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool NotaryCommissionNumberIdentifierSpecified
        {
            get { return NotaryCommissionNumberIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The state in which notary is commissioned.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString NotaryCommissionStateName;

        /// <summary>
        /// Gets or sets a value indicating whether the NotaryCommissionStateName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NotaryCommissionStateName element has been assigned a value.</value>
        [XmlIgnore]
        public bool NotaryCommissionStateNameSpecified
        {
            get { return NotaryCommissionStateName != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 6)]
        public NOTARY_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
