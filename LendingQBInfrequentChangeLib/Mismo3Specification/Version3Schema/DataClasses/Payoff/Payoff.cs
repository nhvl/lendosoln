namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PAYOFF
    {
        /// <summary>
        /// Gets a value indicating whether the PAYOFF container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.IntegratedDisclosureSectionTypeOtherDescriptionSpecified
                    || this.IntegratedDisclosureSectionTypeSpecified
                    || this.PayoffAccountNumberIdentifierSpecified
                    || this.PayoffActionTypeOtherDescriptionSpecified
                    || this.PayoffActionTypeSpecified
                    || this.PayoffAmountSpecified
                    || this.PayoffApplicationSequenceTypeSpecified
                    || this.PayoffPerDiemAmountSpecified
                    || this.PayoffPrepaymentPenaltyAmountSpecified
                    || this.PayoffSpecifiedHUD1LineNumberValueSpecified
                    || this.PayoffThroughDateSpecified;
            }
        }

        /// <summary>
        /// The title or description used to identify a primary section of the integrated disclosure document.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<IntegratedDisclosureSectionBase> IntegratedDisclosureSectionType;

        /// <summary>
        /// Gets or sets a value indicating whether the IntegratedDisclosureSectionType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IntegratedDisclosureSectionType element has been assigned a value.</value>
        [XmlIgnore]
        public bool IntegratedDisclosureSectionTypeSpecified
        {
            get { return this.IntegratedDisclosureSectionType != null && this.IntegratedDisclosureSectionType.enumValue != IntegratedDisclosureSectionBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture additional information when Other is selected for Integrated Disclosure Section Type.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString IntegratedDisclosureSectionTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the IntegratedDisclosureSectionTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IntegratedDisclosureSectionTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool IntegratedDisclosureSectionTypeOtherDescriptionSpecified
        {
            get { return IntegratedDisclosureSectionTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The account number associated with the debt that is being paid off as part of the closing transaction.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOIdentifier PayoffAccountNumberIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the PayoffAccountNumberIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PayoffAccountNumberIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool PayoffAccountNumberIdentifierSpecified
        {
            get { return PayoffAccountNumberIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Specifies the type of action related to the liability payoff.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOEnum<PayoffActionBase> PayoffActionType;

        /// <summary>
        /// Gets or sets a value indicating whether the PayoffActionType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PayoffActionType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PayoffActionTypeSpecified
        {
            get { return this.PayoffActionType != null && this.PayoffActionType.enumValue != PayoffActionBase.Blank; }
            set { }
        }

        /// <summary>
        /// Specifies the other type of action related to the liability payoff when Other is selected.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString PayoffActionTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PayoffActionTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PayoffActionTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PayoffActionTypeOtherDescriptionSpecified
        {
            get { return PayoffActionTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount for the debt that is being paid off as part of the closing transaction.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOAmount PayoffAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PayoffAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PayoffAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PayoffAmountSpecified
        {
            get { return PayoffAmount != null; }
            set { }
        }

        /// <summary>
        /// Specifies the order in which the payoff is applied relative to the scheduled payment.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOEnum<PayoffApplicationSequenceBase> PayoffApplicationSequenceType;

        /// <summary>
        /// Gets or sets a value indicating whether the PayoffApplicationSequenceType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PayoffApplicationSequenceType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PayoffApplicationSequenceTypeSpecified
        {
            get { return this.PayoffApplicationSequenceType != null && this.PayoffApplicationSequenceType.enumValue != PayoffApplicationSequenceBase.Blank; }
            set { }
        }

        /// <summary>
        /// The payoff amount as a per diem (daily) dollar amount.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOAmount PayoffPerDiemAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PayoffPerDiemAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PayoffPerDiemAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PayoffPerDiemAmountSpecified
        {
            get { return PayoffPerDiemAmount != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of the Prepayment Penalty charged on the account being paid in full.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOAmount PayoffPrepaymentPenaltyAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PayoffPrepaymentPenaltyAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PayoffPrepaymentPenaltyAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PayoffPrepaymentPenaltyAmountSpecified
        {
            get { return PayoffPrepaymentPenaltyAmount != null; }
            set { }
        }

        /// <summary>
        /// The line number on the HUD-1 where the Payoff is disclosed.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOValue PayoffSpecifiedHUD1LineNumberValue;

        /// <summary>
        /// Gets or sets a value indicating whether the PayoffSpecifiedHUD1LineNumberValue element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PayoffSpecifiedHUD1LineNumberValue element has been assigned a value.</value>
        [XmlIgnore]
        public bool PayoffSpecifiedHUD1LineNumberValueSpecified
        {
            get { return PayoffSpecifiedHUD1LineNumberValue != null; }
            set { }
        }

        /// <summary>
        /// The last valid date that is covered in the payoff amount.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMODate PayoffThroughDate;

        /// <summary>
        /// Gets or sets a value indicating whether the PayoffThroughDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PayoffThroughDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool PayoffThroughDateSpecified
        {
            get { return PayoffThroughDate != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 11)]
        public PAYOFF_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
