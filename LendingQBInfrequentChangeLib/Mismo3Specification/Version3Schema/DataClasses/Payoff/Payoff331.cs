namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PAYOFF_3_3_1
    {
        /// <summary>
        /// Gets a value indicating whether the PAYOFF_3_3_1 container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.PayoffOrderedDatetimeSpecified
                    || this.PayoffPartialIndicatorSpecified
                    || this.PayoffRequestedByTypeOtherDescriptionSpecified
                    || this.PayoffRequestedByTypeSpecified
                    || this.PayoffRequestedDateSpecified;
            }
        }

        /// <summary>
        /// The date and time that a payoff order was submitted to the existing lien holder.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMODatetime PayoffOrderedDatetime;

        /// <summary>
        /// Gets or sets a value indicating whether the Payoff Ordered date/time element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Payoff Ordered date/time element has been assigned a value.</value>
        [XmlIgnore]
        public bool PayoffOrderedDatetimeSpecified
        {
            get { return PayoffOrderedDatetime != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that the amount being paid does not fully satisfy the liability.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOIndicator PayoffPartialIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PayoffPartialIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PayoffPartialIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PayoffPartialIndicatorSpecified
        {
            get { return PayoffPartialIndicator != null; }
            set { }
        }

        /// <summary>
        /// A collection of values that specify the party making the request for payoff of the loan.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<PayoffRequestedByBase> PayoffRequestedByType;

        /// <summary>
        /// Gets or sets a value indicating whether the PayoffRequestedByType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PayoffRequestedByType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PayoffRequestedByTypeSpecified
        {
            get { return this.PayoffRequestedByType != null && this.PayoffRequestedByType.enumValue != PayoffRequestedByBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field that specifies the enumeration when the value of Other is used for the Payoff Requested By Type.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString PayoffRequestedByTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PayoffRequestedByTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PayoffRequestedByTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PayoffRequestedByTypeOtherDescriptionSpecified
        {
            get { return PayoffRequestedByTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The date that the payoff was requested.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMODate PayoffRequestedDate;

        /// <summary>
        /// Gets or sets a value indicating whether the PayoffRequestedDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PayoffRequestedDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool PayoffRequestedDateSpecified
        {
            get { return PayoffRequestedDate != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 5)]
        public PAYOFF_3_3_1_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
