namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class EXPENSE_CLAIM_ITEM_OCCURRENCE
    {
        /// <summary>
        /// Gets a value indicating whether the EXPENSE_CLAIM_ITEM_OCCURRENCE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExpenseClaimItemCommentTextSpecified
                    || this.ExpenseClaimItemCompletedDateSpecified
                    || this.ExpenseClaimItemPaidDateSpecified
                    || this.ExpenseClaimItemRequestedAmountSpecified
                    || this.ExpenseClaimItemUnitPriceAmountSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A free-form text field used to describe or comment on some aspect of the expense claim item.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString ExpenseClaimItemCommentText;

        /// <summary>
        /// Gets or sets a value indicating whether the ExpenseClaimItemCommentText element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ExpenseClaimItemCommentText element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExpenseClaimItemCommentTextSpecified
        {
            get { return ExpenseClaimItemCommentText != null; }
            set { }
        }

        /// <summary>
        /// For maintenance, service, or activities performed over a period of time, the date that it was completed.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMODate ExpenseClaimItemCompletedDate;

        /// <summary>
        /// Gets or sets a value indicating whether the ExpenseClaimItemCompletedDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ExpenseClaimItemCompletedDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExpenseClaimItemCompletedDateSpecified
        {
            get { return ExpenseClaimItemCompletedDate != null; }
            set { }
        }

        /// <summary>
        /// The date the expense claim item was paid by the submitter to the service provider.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMODate ExpenseClaimItemPaidDate;

        /// <summary>
        /// Gets or sets a value indicating whether the ExpenseClaimItemPaidDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ExpenseClaimItemPaidDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExpenseClaimItemPaidDateSpecified
        {
            get { return ExpenseClaimItemPaidDate != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount requested per occurrence of the service or activity. 
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOAmount ExpenseClaimItemRequestedAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ExpenseClaimItemRequestedAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ExpenseClaimItemRequestedAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExpenseClaimItemRequestedAmountSpecified
        {
            get { return ExpenseClaimItemRequestedAmount != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount that is allowed per occurrence of the service or activity. 
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOAmount ExpenseClaimItemUnitPriceAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ExpenseClaimItemUnitPriceAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ExpenseClaimItemUnitPriceAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExpenseClaimItemUnitPriceAmountSpecified
        {
            get { return ExpenseClaimItemUnitPriceAmount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 5)]
        public EXPENSE_CLAIM_ITEM_OCCURRENCE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
