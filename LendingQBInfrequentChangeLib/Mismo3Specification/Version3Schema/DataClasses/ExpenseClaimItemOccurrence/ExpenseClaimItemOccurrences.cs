namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class EXPENSE_CLAIM_ITEM_OCCURRENCES
    {
        /// <summary>
        /// Gets a value indicating whether the EXPENSE_CLAIM_ITEM_OCCURRENCES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExpenseClaimItemOccurrenceSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// Occurrences of a particular expense claim item.
        /// </summary>
        [XmlElement("EXPENSE_CLAIM_ITEM_OCCURRENCE", Order = 0)]
		public List<EXPENSE_CLAIM_ITEM_OCCURRENCE> ExpenseClaimItemOccurrence = new List<EXPENSE_CLAIM_ITEM_OCCURRENCE>();

        /// <summary>
        /// Gets or sets a value indicating whether the ExpenseClaimItemOccurrence element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ExpenseClaimItemOccurrence element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExpenseClaimItemOccurrenceSpecified
        {
            get { return this.ExpenseClaimItemOccurrence != null && this.ExpenseClaimItemOccurrence.Count(e => e != null && e.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public EXPENSE_CLAIM_ITEM_OCCURRENCES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
