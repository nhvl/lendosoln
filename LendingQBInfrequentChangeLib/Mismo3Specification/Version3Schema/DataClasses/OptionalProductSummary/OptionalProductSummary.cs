namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class OPTIONAL_PRODUCT_SUMMARY
    {
        /// <summary>
        /// Gets a value indicating whether the OPTIONAL_PRODUCT_SUMMARY container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.OptionalProductPremiumsTotalAmountSpecified;
            }
        }

        /// <summary>
        /// The sum of all Optional Product Premium Amounts for all selected Optional Product Plan Types.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount OptionalProductPremiumsTotalAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the OptionalProductPremiumsTotalAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OptionalProductPremiumsTotalAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool OptionalProductPremiumsTotalAmountSpecified
        {
            get { return OptionalProductPremiumsTotalAmount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public OPTIONAL_PRODUCT_SUMMARY_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
