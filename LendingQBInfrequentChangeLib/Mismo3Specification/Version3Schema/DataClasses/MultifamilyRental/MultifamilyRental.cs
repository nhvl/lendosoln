namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class MULTIFAMILY_RENTAL
    {
        /// <summary>
        /// Gets a value indicating whether the MULTIFAMILY_RENTAL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.MultifamilyRentalDetailSpecified
                    || this.RentalFeaturesSpecified
                    || this.RentalUnitsSpecified
                    || this.RentIncludesUtilitiesSpecified;
            }
        }

        /// <summary>
        /// Details about the multifamily rental.
        /// </summary>
        [XmlElement("MULTIFAMILY_RENTAL_DETAIL", Order = 0)]
        public MULTIFAMILY_RENTAL_DETAIL MultifamilyRentalDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the MultifamilyRentalDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MultifamilyRentalDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool MultifamilyRentalDetailSpecified
        {
            get { return this.MultifamilyRentalDetail != null && this.MultifamilyRentalDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Value of the rent including utilities.
        /// </summary>
        [XmlElement("RENT_INCLUDES_UTILITIES", Order = 1)]
        public RENT_INCLUDES_UTILITIES RentIncludesUtilities;

        /// <summary>
        /// Gets or sets a value indicating whether the RentIncludesUtilities element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RentIncludesUtilities element has been assigned a value.</value>
        [XmlIgnore]
        public bool RentIncludesUtilitiesSpecified
        {
            get { return this.RentIncludesUtilities != null && this.RentIncludesUtilities.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Information on features of the rental agreement.
        /// </summary>
        [XmlElement("RENTAL_FEATURES", Order = 2)]
        public RENTAL_FEATURES RentalFeatures;

        /// <summary>
        /// Gets or sets a value indicating whether the RentalFeatures element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RentalFeatures element has been assigned a value.</value>
        [XmlIgnore]
        public bool RentalFeaturesSpecified
        {
            get { return this.RentalFeatures != null && this.RentalFeatures.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Information on various rental units.
        /// </summary>
        [XmlElement("RENTAL_UNITS", Order = 3)]
        public RENTAL_UNITS RentalUnits;

        /// <summary>
        /// Gets or sets a value indicating whether the RentalUnits element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RentalUnits element has been assigned a value.</value>
        [XmlIgnore]
        public bool RentalUnitsSpecified
        {
            get { return this.RentalUnits != null && this.RentalUnits.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public MULTIFAMILY_RENTAL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
