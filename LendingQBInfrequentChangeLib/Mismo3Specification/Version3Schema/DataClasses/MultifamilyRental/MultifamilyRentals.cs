namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class MULTIFAMILY_RENTALS
    {
        /// <summary>
        /// Gets a value indicating whether the MULTIFAMILY_RENTALS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.MultifamilyRentalSpecified;
            }
        }

        /// <summary>
        /// Container for multifamily rental information.
        /// </summary>
        [XmlElement("MULTIFAMILY_RENTAL", Order = 0)]
		public List<MULTIFAMILY_RENTAL> MultifamilyRental = new List<MULTIFAMILY_RENTAL>();

        /// <summary>
        /// Gets or sets a value indicating whether the MultifamilyRental element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MultifamilyRental element has been assigned a value.</value>
        [XmlIgnore]
        public bool MultifamilyRentalSpecified
        {
            get { return this.MultifamilyRental != null && this.MultifamilyRental.Count(m => m != null && m.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public MULTIFAMILY_RENTALS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
