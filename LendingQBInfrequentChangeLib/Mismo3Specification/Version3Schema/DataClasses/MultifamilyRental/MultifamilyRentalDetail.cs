namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class MULTIFAMILY_RENTAL_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the MULTIFAMILY_RENTAL_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DataSourceDescriptionSpecified
                    || this.ExtensionSpecified
                    || this.GrossBuildingAreaSquareFeetNumberSpecified
                    || this.MonthlyRentAmountSpecified
                    || this.RentalDataAnalysisCommentDescriptionSpecified
                    || this.RentControlStatusTypeSpecified
                    || this.RentPerGrossBuildingAreaAmountSpecified;
            }
        }

        /// <summary>
        /// A free-form text field used to describe the source of the information. 
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString DataSourceDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the DataSourceDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DataSourceDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool DataSourceDescriptionSpecified
        {
            get { return DataSourceDescription != null; }
            set { }
        }

        /// <summary>
        /// The total area of the structure not limited to living areas.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMONumeric GrossBuildingAreaSquareFeetNumber;

        /// <summary>
        /// Gets or sets a value indicating whether the GrossBuildingAreaSquareFeetNumber element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GrossBuildingAreaSquareFeetNumber element has been assigned a value.</value>
        [XmlIgnore]
        public bool GrossBuildingAreaSquareFeetNumberSpecified
        {
            get { return GrossBuildingAreaSquareFeetNumber != null; }
            set { }
        }

        /// <summary>
        /// The amount paid by the borrower or co-borrower for the rental of a residence property. This information is normally provided during interview with the borrower or obtained from a loan application, and is sometimes verified with the residence property landlord.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOAmount MonthlyRentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the MonthlyRentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MonthlyRentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool MonthlyRentAmountSpecified
        {
            get { return MonthlyRentAmount != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe or comment on the lease schedule of a rental property or unit.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString RentalDataAnalysisCommentDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the RentalDataAnalysisCommentDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RentalDataAnalysisCommentDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool RentalDataAnalysisCommentDescriptionSpecified
        {
            get { return RentalDataAnalysisCommentDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the existence or likelihood of rent controls such as controls on businesses and apartment complexes.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<RentControlStatusBase> RentControlStatusType;

        /// <summary>
        /// Gets or sets a value indicating whether the RentControlStatusType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RentControlStatusType element has been assigned a value.</value>
        [XmlIgnore]
        public bool RentControlStatusTypeSpecified
        {
            get { return this.RentControlStatusType != null && this.RentControlStatusType.enumValue != RentControlStatusBase.Blank; }
            set { }
        }

        /// <summary>
        /// The dollar value of the rent per square foot of gross living area determined by dividing the Gross Monthly Rent by the Gross Building Area of the property.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOAmount RentPerGrossBuildingAreaAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the RentPerGrossBuildingAreaAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RentPerGrossBuildingAreaAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool RentPerGrossBuildingAreaAmountSpecified
        {
            get { return RentPerGrossBuildingAreaAmount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 6)]
        public MULTIFAMILY_RENTAL_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
