namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PROPERTY_SELLER
    {
        /// <summary>
        /// Gets a value indicating whether the PROPERTY_SELLER container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.MaritalStatusTypeSpecified;
            }
        }

        /// <summary>
        /// The marital status of the party as disclosed by the party.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<MaritalStatusBase> MaritalStatusType;

        /// <summary>
        /// Gets or sets a value indicating whether the MaritalStatusType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MaritalStatusType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MaritalStatusTypeSpecified
        {
            get { return this.MaritalStatusType != null && this.MaritalStatusType.enumValue != MaritalStatusBase.Blank; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public PROPERTY_SELLER_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
