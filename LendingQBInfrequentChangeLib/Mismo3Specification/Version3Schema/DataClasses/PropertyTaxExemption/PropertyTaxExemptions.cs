namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PROPERTY_TAX_EXEMPTIONS
    {
        /// <summary>
        /// Gets a value indicating whether the PROPERTY_TAX_EXEMPTIONS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.PropertyTaxExemptionSpecified;
            }
        }

        /// <summary>
        /// A collection of property tax exemptions.
        /// </summary>
        [XmlElement("PROPERTY_TAX_EXEMPTION", Order = 0)]
		public List<PROPERTY_TAX_EXEMPTION> PropertyTaxExemption = new List<PROPERTY_TAX_EXEMPTION>();

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyTaxExemption element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyTaxExemption element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyTaxExemptionSpecified
        {
            get { return this.PropertyTaxExemption != null && this.PropertyTaxExemption.Count(p => p != null && p.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public PROPERTY_TAX_EXEMPTIONS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
