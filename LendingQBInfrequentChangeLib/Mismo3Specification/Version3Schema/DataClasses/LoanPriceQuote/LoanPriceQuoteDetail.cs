namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class LOAN_PRICE_QUOTE_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the LOAN_PRICE_QUOTE_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.FinancingStructureDescriptionSpecified
                    || this.LoanAllInPricePercentSpecified
                    || this.LoanBasePricePercentSpecified
                    || this.LoanPricePercentFormatTypeSpecified
                    || this.LoanPriceQuoteDatetimeSpecified
                    || this.LoanPriceQuoteExpirationDatetimeSpecified
                    || this.LoanPriceQuoteIdentifierSpecified
                    || this.LoanPriceQuoteInterestRatePercentSpecified
                    || this.LoanPriceQuoteTypeOtherDescriptionSpecified
                    || this.LoanPriceQuoteTypeSpecified
                    || this.RateLockTypeSpecified
                    || this.ServicingReleasePremiumPercentSpecified;
            }
        }

        /// <summary>
        /// Describes the structure of the combined financing and down payment as their relative percentages of the real estate purchase expense. For example, 80-10-10 means an 80% LTV first mortgage, a 10% second mortgage, and 10% down payment.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString FinancingStructureDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the FinancingStructureDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FinancingStructureDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool FinancingStructureDescriptionSpecified
        {
            get { return FinancingStructureDescription != null; }
            set { }
        }

        /// <summary>
        /// The price of the loan including all fees and adjustments, represented as a percent.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOPercent LoanAllInPricePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanAllInPricePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanAllInPricePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanAllInPricePercentSpecified
        {
            get { return LoanAllInPricePercent != null; }
            set { }
        }

        /// <summary>
        /// The price of the loan, represented as a percent, prior to applying fees and adjustments.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOPercent LoanBasePricePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanBasePricePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanBasePricePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanBasePricePercentSpecified
        {
            get { return LoanBasePricePercent != null; }
            set { }
        }

        /// <summary>
        /// Describes the format of the price as being relative to par (0-Base) or absolute (100-base).
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOEnum<LoanPricePercentFormatBase> LoanPricePercentFormatType;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanPricePercentFormatType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanPricePercentFormatType element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanPricePercentFormatTypeSpecified
        {
            get { return this.LoanPricePercentFormatType != null && this.LoanPricePercentFormatType.enumValue != LoanPricePercentFormatBase.Blank; }
            set { }
        }

        /// <summary>
        /// The date and time at which a Price Quote was created.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMODatetime LoanPriceQuoteDatetime;

        /// <summary>
        /// Gets or sets a value indicating whether the Loan Price Quote date/time element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Loan Price Quote date/time element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanPriceQuoteDatetimeSpecified
        {
            get { return LoanPriceQuoteDatetime != null; }
            set { }
        }

        /// <summary>
        /// The date and time at which a Price Quote expires.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMODatetime LoanPriceQuoteExpirationDatetime;

        /// <summary>
        /// Gets or sets a value indicating whether the Loan Price Quote Expiration date/time element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Loan Price Quote Expiration date/time element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanPriceQuoteExpirationDatetimeSpecified
        {
            get { return LoanPriceQuoteExpirationDatetime != null; }
            set { }
        }

        /// <summary>
        /// A unique identifier for a loan price quote, assigned by the party that makes the price quote for tracking purposes.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOIdentifier LoanPriceQuoteIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanPriceQuoteIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanPriceQuoteIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanPriceQuoteIdentifierSpecified
        {
            get { return LoanPriceQuoteIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The mortgage loan interest rate for which the price quote is calculated.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOPercent LoanPriceQuoteInterestRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanPriceQuoteInterestRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanPriceQuoteInterestRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanPriceQuoteInterestRatePercentSpecified
        {
            get { return LoanPriceQuoteInterestRatePercent != null; }
            set { }
        }

        /// <summary>
        /// The supply chain of loans, both before and after a loan closing involves funds to purchase the asset and expected or real income from the sale of the asset.  In the supply chain one organizations sale price is the purchase price of another organization. 
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOEnum<LoanPriceQuoteBase> LoanPriceQuoteType;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanPriceQuoteType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanPriceQuoteType element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanPriceQuoteTypeSpecified
        {
            get { return this.LoanPriceQuoteType != null && this.LoanPriceQuoteType.enumValue != LoanPriceQuoteBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information or a description of the Loan Price Quote Type when Other is selected as the value.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOString LoanPriceQuoteTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanPriceQuoteTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanPriceQuoteTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanPriceQuoteTypeOtherDescriptionSpecified
        {
            get { return LoanPriceQuoteTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the method by which the rate lock period will be determined.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOEnum<RateLockBase> RateLockType;

        /// <summary>
        /// Gets or sets a value indicating whether the RateLockType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RateLockType element has been assigned a value.</value>
        [XmlIgnore]
        public bool RateLockTypeSpecified
        {
            get { return this.RateLockType != null && this.RateLockType.enumValue != RateLockBase.Blank; }
            set { }
        }

        /// <summary>
        /// The price, expressed as a rate, paid for the right to service the loan and receive its servicing compensation. The rate is applied to the unpaid principal balance of the loan (UPB) to determine the dollar price.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOPercent ServicingReleasePremiumPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the ServicingReleasePremiumPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServicingReleasePremiumPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServicingReleasePremiumPercentSpecified
        {
            get { return ServicingReleasePremiumPercent != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 12)]
        public LOAN_PRICE_QUOTE_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
