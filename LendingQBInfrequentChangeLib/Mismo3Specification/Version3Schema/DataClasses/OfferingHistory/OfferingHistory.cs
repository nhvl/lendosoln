namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class OFFERING_HISTORY
    {
        /// <summary>
        /// Gets a value indicating whether the OFFERING_HISTORY container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.OfferingHistoryAmountSpecified
                    || this.OfferingHistoryDateSpecified
                    || this.OfferingHistoryTypeOtherDescriptionSpecified
                    || this.OfferingHistoryTypeSpecified;
            }
        }

        /// <summary>
        /// The amount related to the Offering History Type.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount OfferingHistoryAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the OfferingHistoryAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OfferingHistoryAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool OfferingHistoryAmountSpecified
        {
            get { return OfferingHistoryAmount != null; }
            set { }
        }

        /// <summary>
        /// The date related to the Offering History Type.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMODate OfferingHistoryDate;

        /// <summary>
        /// Gets or sets a value indicating whether the OfferingHistoryDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OfferingHistoryDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool OfferingHistoryDateSpecified
        {
            get { return OfferingHistoryDate != null; }
            set { }
        }

        /// <summary>
        /// The type of events within the history of a specific offering. 
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<OfferingHistoryBase> OfferingHistoryType;

        /// <summary>
        /// Gets or sets a value indicating whether the OfferingHistoryType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OfferingHistoryType element has been assigned a value.</value>
        [XmlIgnore]
        public bool OfferingHistoryTypeSpecified
        {
            get { return this.OfferingHistoryType != null && this.OfferingHistoryType.enumValue != OfferingHistoryBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Offering History Type.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString OfferingHistoryTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the OfferingHistoryTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OfferingHistoryTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool OfferingHistoryTypeOtherDescriptionSpecified
        {
            get { return OfferingHistoryTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public OFFERING_HISTORY_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
