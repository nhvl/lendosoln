namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class OFFERING_HISTORIES
    {
        /// <summary>
        /// Gets a value indicating whether the OFFERING_HISTORIES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.OfferingHistorySpecified;
            }
        }

        /// <summary>
        /// A collection of offering histories.
        /// </summary>
        [XmlElement("OFFERING_HISTORY", Order = 0)]
		public List<OFFERING_HISTORY> OfferingHistory = new List<OFFERING_HISTORY>();

        /// <summary>
        /// Gets or sets a value indicating whether the OfferingHistory element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OfferingHistory element has been assigned a value.</value>
        [XmlIgnore]
        public bool OfferingHistorySpecified
        {
            get { return this.OfferingHistory != null && this.OfferingHistory.Count(o => o != null && o.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public OFFERING_HISTORIES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
