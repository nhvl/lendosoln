namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class ESCROW_ITEMS
    {
        /// <summary>
        /// Gets a value indicating whether the ESCROW_ITEMS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.EscrowItemSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of escrow items.
        /// </summary>
        [XmlElement("ESCROW_ITEM", Order = 0)]
        public List<ESCROW_ITEM> EscrowItem = new List<ESCROW_ITEM>();

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowItem element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowItem element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowItemSpecified
        {
            get { return this.EscrowItem != null && this.EscrowItem.Count(e => e != null && e.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public ESCROW_ITEMS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
