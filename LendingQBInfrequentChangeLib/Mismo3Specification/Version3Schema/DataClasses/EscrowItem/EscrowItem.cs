namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class ESCROW_ITEM
    {
        /// <summary>
        /// Gets a value indicating whether the ESCROW_ITEM container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.EscrowItemDetailSpecified
                    || this.EscrowItemDisbursementsSpecified
                    || this.EscrowItemPaymentsSpecified
                    || this.EscrowPaidToSpecified
                    || this.ExtensionSpecified
                    || this.IdentifiedServiceProvidersSpecified
                    || this.RequiredServiceProviderSpecified;
            }
        }

        /// <summary>
        /// Details on escrow items.
        /// </summary>
        [XmlElement("ESCROW_ITEM_DETAIL", Order = 0)]
        public ESCROW_ITEM_DETAIL EscrowItemDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowItemDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowItemDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowItemDetailSpecified
        {
            get { return this.EscrowItemDetail != null && this.EscrowItemDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Disbursements on escrow items.
        /// </summary>
        [XmlElement("ESCROW_ITEM_DISBURSEMENTS", Order = 1)]
        public ESCROW_ITEM_DISBURSEMENTS EscrowItemDisbursements;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowItemDisbursements element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowItemDisbursements element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowItemDisbursementsSpecified
        {
            get { return this.EscrowItemDisbursements != null && this.EscrowItemDisbursements.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Payments on the escrow account.
        /// </summary>
        [XmlElement("ESCROW_ITEM_PAYMENTS", Order = 2)]
        public ESCROW_ITEM_PAYMENTS EscrowItemPayments;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowItemPayments element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowItemPayments element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowItemPaymentsSpecified
        {
            get { return this.EscrowItemPayments != null && this.EscrowItemPayments.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The entity to whom escrow payments are made.
        /// </summary>
        [XmlElement("ESCROW_PAID_TO", Order = 3)]
        public PAID_TO EscrowPaidTo;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowPaidTo element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowPaidTo element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowPaidToSpecified
        {
            get { return this.EscrowPaidTo != null && this.EscrowPaidTo.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Identified service providers for the escrow.
        /// </summary>
        [XmlElement("IDENTIFIED_SERVICE_PROVIDERS", Order = 4)]
        public IDENTIFIED_SERVICE_PROVIDERS IdentifiedServiceProviders;

        /// <summary>
        /// Gets or sets a value indicating whether the IdentifiedServiceProviders element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IdentifiedServiceProviders element has been assigned a value.</value>
        [XmlIgnore]
        public bool IdentifiedServiceProvidersSpecified
        {
            get { return this.IdentifiedServiceProviders != null && this.IdentifiedServiceProviders.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Required service provider for the escrow.
        /// </summary>
        [XmlElement("REQUIRED_SERVICE_PROVIDER", Order = 5)]
        public REQUIRED_SERVICE_PROVIDER RequiredServiceProvider;

        /// <summary>
        /// Gets or sets a value indicating whether the RequiredServiceProvider element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RequiredServiceProvider element has been assigned a value.</value>
        [XmlIgnore]
        public bool RequiredServiceProviderSpecified
        {
            get { return this.RequiredServiceProvider != null && this.RequiredServiceProvider.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 6)]
        public ESCROW_ITEM_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
