namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class ESCROW_ITEM_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the ESCROW_ITEM_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BorrowerChosenProviderIndicatorSpecified
                    || this.EscrowAnnualPaymentAmountSpecified
                    || this.EscrowCollectedNumberOfMonthsCountSpecified
                    || this.EscrowInsurancePolicyIdentifierSpecified
                    || this.EscrowItemActualTotalAmountSpecified
                    || this.EscrowItemCategoryTypeOtherDescriptionSpecified
                    || this.EscrowItemCategoryTypeSpecified
                    || this.EscrowItemEstimatedTotalAmountSpecified
                    || this.EscrowItemLastDisbursementDateSpecified
                    || this.EscrowItemNextDisbursementDueDateSpecified
                    || this.EscrowItemNextProjectedDisbursementAmountSpecified
                    || this.EscrowItemTotalReserveCollectedAtClosingAmountSpecified
                    || this.EscrowItemTypeOtherDescriptionSpecified
                    || this.EscrowItemTypeSpecified
                    || this.EscrowMonthlyPaymentAmountSpecified
                    || this.EscrowMonthlyPaymentRoundingTypeSpecified
                    || this.EscrowPaidByTypeSpecified
                    || this.EscrowPaymentFrequencyTypeOtherDescriptionSpecified
                    || this.EscrowPaymentFrequencyTypeSpecified
                    || this.EscrowPremiumAmountSpecified
                    || this.EscrowPremiumDurationMonthsCountSpecified
                    || this.EscrowPremiumPaidByTypeSpecified
                    || this.EscrowPremiumPaymentTypeSpecified
                    || this.EscrowPremiumRatePercentBasisTypeOtherDescriptionSpecified
                    || this.EscrowPremiumRatePercentBasisTypeSpecified
                    || this.EscrowPremiumRatePercentSpecified
                    || this.EscrowSpecifiedHUD1LineNumberValueSpecified
                    || this.EscrowTotalDisbursementAmountSpecified
                    || this.ExtensionSpecified
                    || this.FeePaidToTypeOtherDescriptionSpecified
                    || this.FeePaidToTypeSpecified
                    || this.GFEDisclosedEscrowPremiumAmountSpecified
                    || this.IntegratedDisclosureLineNumberValueSpecified
                    || this.IntegratedDisclosureSectionTypeOtherDescriptionSpecified
                    || this.IntegratedDisclosureSectionTypeSpecified
                    || this.RegulationZPointsAndFeesIndicatorSpecified
                    || this.RequiredProviderOfServiceIndicatorSpecified;
            }
        }

        /// <summary>
        /// When true, indicates the provider of the service for which the particular fee or payment is charged was selected by the borrower independent of the lender or lender-provided list.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIndicator BorrowerChosenProviderIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the BorrowerChosenProviderIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BorrowerChosenProviderIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool BorrowerChosenProviderIndicatorSpecified
        {
            get { return BorrowerChosenProviderIndicator != null; }
            set { }
        }

        /// <summary>
        /// The amount of the annual payment for the Escrow Item Type.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOAmount EscrowAnnualPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowAnnualPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowAnnualPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowAnnualPaymentAmountSpecified
        {
            get { return EscrowAnnualPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// The number of months escrow to be collected at closing for the escrow item.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOCount EscrowCollectedNumberOfMonthsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowCollectedNumberOfMonthsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowCollectedNumberOfMonthsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowCollectedNumberOfMonthsCountSpecified
        {
            get { return EscrowCollectedNumberOfMonthsCount != null; }
            set { }
        }

        /// <summary>
        /// The identifier assigned to the insurance policy by the issuing company.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOIdentifier EscrowInsurancePolicyIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowInsurancePolicyIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowInsurancePolicyIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowInsurancePolicyIdentifierSpecified
        {
            get { return EscrowInsurancePolicyIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The actual total dollar amount paid for the escrow item by all parties.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOAmount EscrowItemActualTotalAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowItemActualTotalAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowItemActualTotalAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowItemActualTotalAmountSpecified
        {
            get { return EscrowItemActualTotalAmount != null; }
            set { }
        }

        /// <summary>
        /// Identifies the category of the escrow item type.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOEnum<EscrowItemCategoryBase> EscrowItemCategoryType;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowItemCategoryType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowItemCategoryType element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowItemCategoryTypeSpecified
        {
            get { return this.EscrowItemCategoryType != null && this.EscrowItemCategoryType.enumValue != EscrowItemCategoryBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Escrow Item Category Type.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOString EscrowItemCategoryTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowItemCategoryTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowItemCategoryTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowItemCategoryTypeOtherDescriptionSpecified
        {
            get { return EscrowItemCategoryTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The total estimated dollar amount to be paid for the escrow item by all parties.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOAmount EscrowItemEstimatedTotalAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowItemEstimatedTotalAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowItemEstimatedTotalAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowItemEstimatedTotalAmountSpecified
        {
            get { return EscrowItemEstimatedTotalAmount != null; }
            set { }
        }

        /// <summary>
        /// Specifies the last disbursement date for the selected escrow item.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMODate EscrowItemLastDisbursementDate;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowItemLastDisbursementDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowItemLastDisbursementDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowItemLastDisbursementDateSpecified
        {
            get { return EscrowItemLastDisbursementDate != null; }
            set { }
        }

        /// <summary>
        /// Specifies the due date of the next disbursement for the selected escrow item.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMODate EscrowItemNextDisbursementDueDate;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowItemNextDisbursementDueDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowItemNextDisbursementDueDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowItemNextDisbursementDueDateSpecified
        {
            get { return EscrowItemNextDisbursementDueDate != null; }
            set { }
        }

        /// <summary>
        /// Specifies the projected amount of the next disbursement due on this escrow item.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOAmount EscrowItemNextProjectedDisbursementAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowItemNextProjectedDisbursementAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowItemNextProjectedDisbursementAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowItemNextProjectedDisbursementAmountSpecified
        {
            get { return EscrowItemNextProjectedDisbursementAmount != null; }
            set { }
        }

        /// <summary>
        /// The total amount collected at closing for escrow account reserves for the specified escrow item.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOAmount EscrowItemTotalReserveCollectedAtClosingAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowItemTotalReserveCollectedAtClosingAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowItemTotalReserveCollectedAtClosingAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowItemTotalReserveCollectedAtClosingAmountSpecified
        {
            get { return EscrowItemTotalReserveCollectedAtClosingAmount != null; }
            set { }
        }

        /// <summary>
        /// Specifies the type of Escrow Item.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOEnum<EscrowItemBase> EscrowItemType;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowItemType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowItemType element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowItemTypeSpecified
        {
            get { return this.EscrowItemType != null && this.EscrowItemType.enumValue != EscrowItemBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field to collect additional information when Other is selected for Escrow Item Type.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOString EscrowItemTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowItemTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowItemTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowItemTypeOtherDescriptionSpecified
        {
            get { return EscrowItemTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The monthly payment amount for the escrow item.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOAmount EscrowMonthlyPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowMonthlyPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowMonthlyPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowMonthlyPaymentAmountSpecified
        {
            get { return EscrowMonthlyPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// Defines the rounding method applied to the escrow payment.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOEnum<EscrowMonthlyPaymentRoundingBase> EscrowMonthlyPaymentRoundingType;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowMonthlyPaymentRoundingType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowMonthlyPaymentRoundingType element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowMonthlyPaymentRoundingTypeSpecified
        {
            get { return this.EscrowMonthlyPaymentRoundingType != null && this.EscrowMonthlyPaymentRoundingType.enumValue != EscrowMonthlyPaymentRoundingBase.Blank; }
            set { }
        }

        /// <summary>
        /// The type of Party responsible for payment of the escrow item.
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMOEnum<EscrowPaidByBase> EscrowPaidByType;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowPaidByType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowPaidByType element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowPaidByTypeSpecified
        {
            get { return this.EscrowPaidByType != null && this.EscrowPaidByType.enumValue != EscrowPaidByBase.Blank; }
            set { }
        }

        /// <summary>
        /// Payment period covered by escrow item. If item were paid once a year the payment frequency type would be annual.
        /// </summary>
        [XmlElement(Order = 17)]
        public MISMOEnum<EscrowPaymentFrequencyBase> EscrowPaymentFrequencyType;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowPaymentFrequencyType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowPaymentFrequencyType element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowPaymentFrequencyTypeSpecified
        {
            get { return this.EscrowPaymentFrequencyType != null && this.EscrowPaymentFrequencyType.enumValue != EscrowPaymentFrequencyBase.Blank; }
            set { }
        }

        /// <summary>
        /// When Other is specified as the Payment Frequency Type, this data element contains the description.
        /// </summary>
        [XmlElement(Order = 18)]
        public MISMOString EscrowPaymentFrequencyTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowPaymentFrequencyTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowPaymentFrequencyTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowPaymentFrequencyTypeOtherDescriptionSpecified
        {
            get { return EscrowPaymentFrequencyTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of the escrow premium for the escrow item type.
        /// </summary>
        [XmlElement(Order = 19)]
        public MISMOAmount EscrowPremiumAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowPremiumAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowPremiumAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowPremiumAmountSpecified
        {
            get { return EscrowPremiumAmount != null; }
            set { }
        }

        /// <summary>
        /// The number of months covered by the related premium.
        /// </summary>
        [XmlElement(Order = 20)]
        public MISMOCount EscrowPremiumDurationMonthsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowPremiumDurationMonthsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowPremiumDurationMonthsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowPremiumDurationMonthsCountSpecified
        {
            get { return EscrowPremiumDurationMonthsCount != null; }
            set { }
        }

        /// <summary>
        /// The type of party responsible for the payment of the escrow premium amount.
        /// </summary>
        [XmlElement(Order = 21)]
        public MISMOEnum<EscrowPremiumPaidByBase> EscrowPremiumPaidByType;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowPremiumPaidByType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowPremiumPaidByType element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowPremiumPaidByTypeSpecified
        {
            get { return this.EscrowPremiumPaidByType != null && this.EscrowPremiumPaidByType.enumValue != EscrowPremiumPaidByBase.Blank; }
            set { }
        }

        /// <summary>
        /// The payment method of the premium for the escrow item. Paid Outside of Closing (POC), Waived or Collect at Closing.
        /// </summary>
        [XmlElement(Order = 22)]
        public MISMOEnum<EscrowPremiumPaymentBase> EscrowPremiumPaymentType;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowPremiumPaymentType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowPremiumPaymentType element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowPremiumPaymentTypeSpecified
        {
            get { return this.EscrowPremiumPaymentType != null && this.EscrowPremiumPaymentType.enumValue != EscrowPremiumPaymentBase.Blank; }
            set { }
        }

        /// <summary>
        /// The percentage rate used to calculate the amount of the Escrow Premium. This is used in conjunction with the Escrow Premium Percent Basis Type to determine a total dollar amount of the Escrow Premium.
        /// </summary>
        [XmlElement(Order = 23)]
        public MISMOPercent EscrowPremiumRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowPremiumRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowPremiumRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowPremiumRatePercentSpecified
        {
            get { return EscrowPremiumRatePercent != null; }
            set { }
        }

        /// <summary>
        /// Defines the amount upon which the Escrow Premium Percent is calculated.
        /// </summary>
        [XmlElement(Order = 24)]
        public MISMOEnum<EscrowPremiumRatePercentBasisBase> EscrowPremiumRatePercentBasisType;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowPremiumRatePercentBasisType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowPremiumRatePercentBasisType element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowPremiumRatePercentBasisTypeSpecified
        {
            get { return this.EscrowPremiumRatePercentBasisType != null && this.EscrowPremiumRatePercentBasisType.enumValue != EscrowPremiumRatePercentBasisBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Escrow Premium Percent Basis Type.
        /// </summary>
        [XmlElement(Order = 25)]
        public MISMOString EscrowPremiumRatePercentBasisTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowPremiumRatePercentBasisTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowPremiumRatePercentBasisTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowPremiumRatePercentBasisTypeOtherDescriptionSpecified
        {
            get { return EscrowPremiumRatePercentBasisTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The line number on the HUD-1 where the Escrow Item is to be disclosed. 
        /// </summary>
        [XmlElement(Order = 26)]
        public MISMOValue EscrowSpecifiedHUD1LineNumberValue;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowSpecifiedHUD1LineNumberValue element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowSpecifiedHUD1LineNumberValue element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowSpecifiedHUD1LineNumberValueSpecified
        {
            get { return EscrowSpecifiedHUD1LineNumberValue != null; }
            set { }
        }

        /// <summary>
        /// Total amount disbursed for the escrow item from closing to the current date.
        /// </summary>
        [XmlElement(Order = 27)]
        public MISMOAmount EscrowTotalDisbursementAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowTotalDisbursementAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowTotalDisbursementAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool EscrowTotalDisbursementAmountSpecified
        {
            get { return EscrowTotalDisbursementAmount != null; }
            set { }
        }

        /// <summary>
        /// Identifies the category or type of payee to which the fee or payment will be paid.
        /// </summary>
        [XmlElement(Order = 28)]
        public MISMOEnum<FeePaidToBase> FeePaidToType;

        /// <summary>
        /// Gets or sets a value indicating whether the FeePaidToType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeePaidToType element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeePaidToTypeSpecified
        {
            get { return this.FeePaidToType != null && this.FeePaidToType.enumValue != FeePaidToBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field to capture the description when Other is selected as Fee Paid To Type.
        /// </summary>
        [XmlElement(Order = 29)]
        public MISMOString FeePaidToTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the FeePaidToTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeePaidToTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeePaidToTypeOtherDescriptionSpecified
        {
            get { return FeePaidToTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of the premium for an escrow item type, as reflected on a single instance of the GFE.
        /// </summary>
        [XmlElement(Order = 30)]
        public MISMOAmount GFEDisclosedEscrowPremiumAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the GFEDisclosedEscrowPremiumAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GFEDisclosedEscrowPremiumAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool GFEDisclosedEscrowPremiumAmountSpecified
        {
            get { return GFEDisclosedEscrowPremiumAmount != null; }
            set { }
        }

        /// <summary>
        /// The line identifier for the item as represented in a section of the Integrated Disclosure form.
        /// </summary>
        [XmlElement(Order = 31)]
        public MISMOValue IntegratedDisclosureLineNumberValue;

        /// <summary>
        /// Gets or sets a value indicating whether the IntegratedDisclosureLineNumberValue element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IntegratedDisclosureLineNumberValue element has been assigned a value.</value>
        [XmlIgnore]
        public bool IntegratedDisclosureLineNumberValueSpecified
        {
            get { return IntegratedDisclosureLineNumberValue != null; }
            set { }
        }

        /// <summary>
        /// The title or description used to identify a primary section of the integrated disclosure document.
        /// </summary>
        [XmlElement(Order = 32)]
        public MISMOEnum<IntegratedDisclosureSectionBase> IntegratedDisclosureSectionType;

        /// <summary>
        /// Gets or sets a value indicating whether the IntegratedDisclosureSectionType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IntegratedDisclosureSectionType element has been assigned a value.</value>
        [XmlIgnore]
        public bool IntegratedDisclosureSectionTypeSpecified
        {
            get { return this.IntegratedDisclosureSectionType != null && this.IntegratedDisclosureSectionType.enumValue != IntegratedDisclosureSectionBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture additional information when Other is selected for Integrated Disclosure Section Type.
        /// </summary>
        [XmlElement(Order = 33)]
        public MISMOString IntegratedDisclosureSectionTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the IntegratedDisclosureSectionTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IntegratedDisclosureSectionTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool IntegratedDisclosureSectionTypeOtherDescriptionSpecified
        {
            get { return IntegratedDisclosureSectionTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that item is included in the Regulation Z Points and Fees calculation.
        /// </summary>
        [XmlElement(Order = 34)]
        public MISMOIndicator RegulationZPointsAndFeesIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the RegulationZPointsAndFeesIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RegulationZPointsAndFeesIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool RegulationZPointsAndFeesIndicatorSpecified
        {
            get { return RegulationZPointsAndFeesIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates a required Provider of Service is associated with this fee or payment.
        /// </summary>
        [XmlElement(Order = 35)]
        public MISMOIndicator RequiredProviderOfServiceIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the RequiredProviderOfServiceIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RequiredProviderOfServiceIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool RequiredProviderOfServiceIndicatorSpecified
        {
            get { return RequiredProviderOfServiceIndicator != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 36)]
        public ESCROW_ITEM_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null && Extension.ShouldSerialize; }
            set { }
        }
    }
}
