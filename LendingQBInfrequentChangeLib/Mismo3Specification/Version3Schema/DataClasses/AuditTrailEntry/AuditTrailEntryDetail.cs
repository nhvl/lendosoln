namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class AUDIT_TRAIL_ENTRY_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the AUDIT_TRAIL_ENTRY_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.EntryDatetimeSpecified
                    || this.EntryDescriptionSpecified
                    || this.EventTypeOtherDescriptionSpecified
                    || this.EventTypeSpecified
                    || this.ExtensionSpecified
                    || this.PerformedByOrganizationNameSpecified
                    || this.PerformedBySystemEntryIdentifierSpecified
                    || this.PerformedBySystemNameSpecified;
            }
        }

        /// <summary>
        /// The date and time the action occurred.  The time must always contain the proper time zone designator.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMODatetime EntryDatetime;

        /// <summary>
        /// Gets or sets a value indicating whether a date/time exists to serialize.
        /// </summary>
        /// <value>A boolean indicating whether a date/time has been assigned.</value>
        [XmlIgnore]
        public bool EntryDatetimeSpecified
        {
            get { return EntryDatetime != null; }
            set { }
        }

        /// <summary>
        /// Contains additional information or details about the action performed.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString EntryDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the EntryDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EntryDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool EntryDescriptionSpecified
        {
            get { return EntryDescription != null; }
            set { }
        }

        /// <summary>
        /// For a step in a process, contains the type of action or state.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<EventBase> EventType;

        /// <summary>
        /// Gets or sets a value indicating whether the EventType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EventType element has been assigned a value.</value>
        [XmlIgnore]
        public bool EventTypeSpecified
        {
            get { return this.EventType != null && this.EventType.enumValue != EventBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the type of action or state if Other is selected as the Event Type.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString EventTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the EventTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EventTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool EventTypeOtherDescriptionSpecified
        {
            get { return EventTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Contains the name of the organization that performed the action.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString PerformedByOrganizationName;

        /// <summary>
        /// Gets or sets a value indicating whether the PerformedByOrganizationName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PerformedByOrganizationName element has been assigned a value.</value>
        [XmlIgnore]
        public bool PerformedByOrganizationNameSpecified
        {
            get { return PerformedByOrganizationName != null; }
            set { }
        }

        /// <summary>
        /// Contains an identifier that was assigned by the system that performed the action.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOIdentifier PerformedBySystemEntryIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the PerformedBySystemEntryIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PerformedBySystemEntryIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool PerformedBySystemEntryIdentifierSpecified
        {
            get { return PerformedBySystemEntryIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Contains the name of the system used for performing the action.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOString PerformedBySystemName;

        /// <summary>
        /// Gets or sets a value indicating whether the PerformedBySystemName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PerformedBySystemName element has been assigned a value.</value>
        [XmlIgnore]
        public bool PerformedBySystemNameSpecified
        {
            get { return PerformedBySystemName != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 7)]
        public AUDIT_TRAIL_ENTRY_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
