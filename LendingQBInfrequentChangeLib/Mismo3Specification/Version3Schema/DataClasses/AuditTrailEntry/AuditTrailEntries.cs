namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class AUDIT_TRAIL_ENTRIES
    {
        /// <summary>
        /// Gets a value indicating whether the AUDIT_TRAIL_ENTRIES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AuditTrailEntrySpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A container for past actions performed on the document.
        /// </summary>
        [XmlElement("AUDIT_TRAIL_ENTRY", Order = 0)]
		public List<AUDIT_TRAIL_ENTRY> AuditTrailEntry = new List<AUDIT_TRAIL_ENTRY>();

        /// <summary>
        /// Gets or sets a value indicating whether the AuditTrailEntry element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AuditTrailEntry element has been assigned a value.</value>
        [XmlIgnore]
        public bool AuditTrailEntrySpecified
        {
            get { return this.AuditTrailEntry != null && this.AuditTrailEntry.Count(a => a != null && a.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public AUDIT_TRAIL_ENTRIES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
