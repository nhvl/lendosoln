namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class AUDIT_TRAIL_ENTRY
    {
        /// <summary>
        /// Gets a value indicating whether the AUDIT_TRAIL_ENTRY container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AuditTrailEntryDetailSpecified
                    || this.AuditTrailEntryEvidenceSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// Details about a past action performed on a document.
        /// </summary>
        [XmlElement("AUDIT_TRAIL_ENTRY_DETAIL", Order = 0)]
        public AUDIT_TRAIL_ENTRY_DETAIL AuditTrailEntryDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the AuditTrailEntryDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AuditTrailEntryDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool AuditTrailEntryDetailSpecified
        {
            get { return this.AuditTrailEntryDetail != null && this.AuditTrailEntryDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Evidence about a past action performed on a document.
        /// </summary>
        [XmlElement("AUDIT_TRAIL_ENTRY_EVIDENCE", Order = 1)]
        public AUDIT_TRAIL_ENTRY_EVIDENCE AuditTrailEntryEvidence;

        /// <summary>
        /// Gets or sets a value indicating whether the AuditTrailEntryEvidence element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AuditTrailEntryEvidence element has been assigned a value.</value>
        [XmlIgnore]
        public bool AuditTrailEntryEvidenceSpecified
        {
            get { return this.AuditTrailEntryEvidence != null && this.AuditTrailEntryEvidence.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public AUDIT_TRAIL_ENTRY_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
