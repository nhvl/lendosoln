namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class DESIGNATIONS
    {
        /// <summary>
        /// Gets a value indicating whether the DESIGNATIONS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DesignationSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// Container for various designations.
        /// </summary>
        [XmlElement("DESIGNATION", Order = 0)]
		public List<DESIGNATION> Designation = new List<DESIGNATION>();

        /// <summary>
        /// Gets or sets a value indicating whether the Designation element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Designation element has been assigned a value.</value>
        [XmlIgnore]
        public bool DesignationSpecified
        {
            get { return this.Designation != null && this.Designation.Count(d => d != null && d.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public DESIGNATIONS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
