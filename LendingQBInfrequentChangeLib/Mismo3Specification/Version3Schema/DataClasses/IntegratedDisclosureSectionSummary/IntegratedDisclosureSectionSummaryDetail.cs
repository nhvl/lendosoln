namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class INTEGRATED_DISCLOSURE_SECTION_SUMMARY_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the INTEGRATED_DISCLOSURE_SECTION_SUMMARY_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.IntegratedDisclosureSectionTotalAmountSpecified
                    || this.IntegratedDisclosureSectionTypeOtherDescriptionSpecified
                    || this.IntegratedDisclosureSectionTypeSpecified
                    || this.IntegratedDisclosureSubsectionTotalAmountSpecified
                    || this.IntegratedDisclosureSubsectionTypeOtherDescriptionSpecified
                    || this.IntegratedDisclosureSubsectionTypeSpecified
                    || this.LenderCreditToleranceCureAmountSpecified;
            }
        }

        /// <summary>
        /// The total amount of itemized charges and credits within a specified section of the integrated disclosure document.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount IntegratedDisclosureSectionTotalAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the IntegratedDisclosureSectionTotalAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IntegratedDisclosureSectionTotalAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool IntegratedDisclosureSectionTotalAmountSpecified
        {
            get { return IntegratedDisclosureSectionTotalAmount != null; }
            set { }
        }

        /// <summary>
        /// The title or description used to identify a primary section of the integrated disclosure document.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<IntegratedDisclosureSectionBase> IntegratedDisclosureSectionType;

        /// <summary>
        /// Gets or sets a value indicating whether the IntegratedDisclosureSectionType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IntegratedDisclosureSectionType element has been assigned a value.</value>
        [XmlIgnore]
        public bool IntegratedDisclosureSectionTypeSpecified
        {
            get { return this.IntegratedDisclosureSectionType != null && this.IntegratedDisclosureSectionType.enumValue != IntegratedDisclosureSectionBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture additional information when Other is selected for Integrated Disclosure Section Type.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString IntegratedDisclosureSectionTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the IntegratedDisclosureSectionTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IntegratedDisclosureSectionTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool IntegratedDisclosureSectionTypeOtherDescriptionSpecified
        {
            get { return IntegratedDisclosureSectionTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The total amount of itemized charges and credits within a specified subsection of the integrated disclosure document.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOAmount IntegratedDisclosureSubsectionTotalAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the IntegratedDisclosureSubsectionTotalAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IntegratedDisclosureSubsectionTotalAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool IntegratedDisclosureSubsectionTotalAmountSpecified
        {
            get { return IntegratedDisclosureSubsectionTotalAmount != null; }
            set { }
        }

        /// <summary>
        /// The title or description used to identify a secondary or subsection of the integrated disclosure document.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<IntegratedDisclosureSubsectionBase> IntegratedDisclosureSubsectionType;

        /// <summary>
        /// Gets or sets a value indicating whether the IntegratedDisclosureSubsectionType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IntegratedDisclosureSubsectionType element has been assigned a value.</value>
        [XmlIgnore]
        public bool IntegratedDisclosureSubsectionTypeSpecified
        {
            get { return this.IntegratedDisclosureSubsectionType != null && this.IntegratedDisclosureSubsectionType.enumValue != IntegratedDisclosureSubsectionBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture additional information when Other is selected for Integrated Disclosure Subsection Type.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString IntegratedDisclosureSubsectionTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the IntegratedDisclosureSubsectionTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IntegratedDisclosureSubsectionTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool IntegratedDisclosureSubsectionTypeOtherDescriptionSpecified
        {
            get { return IntegratedDisclosureSubsectionTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The amount necessary to bring the final actual costs in line with the amount initially disclosed  to the borrower.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOAmount LenderCreditToleranceCureAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the LenderCreditToleranceCureAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LenderCreditToleranceCureAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool LenderCreditToleranceCureAmountSpecified
        {
            get { return LenderCreditToleranceCureAmount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 7)]
        public INTEGRATED_DISCLOSURE_SECTION_SUMMARY_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
