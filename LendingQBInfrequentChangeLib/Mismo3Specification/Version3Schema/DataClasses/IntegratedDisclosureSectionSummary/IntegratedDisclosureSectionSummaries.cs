namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class INTEGRATED_DISCLOSURE_SECTION_SUMMARIES
    {
        /// <summary>
        /// Gets a value indicating whether the INTEGRATED_DISCLOSURE_SECTION_SUMMARIES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.IntegratedDisclosureSectionSummarySpecified;
            }
        }

        /// <summary>
        /// A collection of integrated disclosure section summaries.
        /// </summary>
        [XmlElement("INTEGRATED_DISCLOSURE_SECTION_SUMMARY", Order = 0)]
		public List<INTEGRATED_DISCLOSURE_SECTION_SUMMARY> IntegratedDisclosureSectionSummary = new List<INTEGRATED_DISCLOSURE_SECTION_SUMMARY>();

        /// <summary>
        /// Gets or sets a value indicating whether the IntegratedDisclosureSectionSummary element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IntegratedDisclosureSectionSummary element has been assigned a value.</value>
        [XmlIgnore]
        public bool IntegratedDisclosureSectionSummarySpecified
        {
            get { return this.IntegratedDisclosureSectionSummary != null && this.IntegratedDisclosureSectionSummary.Count(i => i != null && i.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public INTEGRATED_DISCLOSURE_SECTION_SUMMARIES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
