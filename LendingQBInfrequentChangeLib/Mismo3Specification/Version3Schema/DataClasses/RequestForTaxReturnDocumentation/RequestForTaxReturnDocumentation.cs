namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class REQUEST_FOR_TAX_RETURN_DOCUMENTATION
    {
        /// <summary>
        /// Gets a value indicating whether the REQUEST_FOR_TAX_RETURN_DOCUMENTATION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AddressesSpecified
                    || this.ExtensionSpecified
                    || this.RequestForTaxReturnDocumentationDetailSpecified
                    || this.TaxReturnDocumentationsSpecified;
            }
        }

        /// <summary>
        /// Addresses related to a request for tax return documentation.
        /// </summary>
        [XmlElement("ADDRESSES", Order = 0)]
        public ADDRESSES Addresses;

        /// <summary>
        /// Gets or sets a value indicating whether the Addresses element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Addresses element has been assigned a value.</value>
        [XmlIgnore]
        public bool AddressesSpecified
        {
            get { return this.Addresses != null && this.Addresses.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details on a request for tax return documentation.
        /// </summary>
        [XmlElement("REQUEST_FOR_TAX_RETURN_DOCUMENTATION_DETAIL", Order = 1)]
        public REQUEST_FOR_TAX_RETURN_DOCUMENTATION_DETAIL RequestForTaxReturnDocumentationDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the RequestForTaxReturnDocumentationDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RequestForTaxReturnDocumentationDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool RequestForTaxReturnDocumentationDetailSpecified
        {
            get { return this.RequestForTaxReturnDocumentationDetail != null && this.RequestForTaxReturnDocumentationDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Any tax return documentation.
        /// </summary>
        [XmlElement("TAX_RETURN_DOCUMENTATIONS", Order = 2)]
        public TAX_RETURN_DOCUMENTATIONS TaxReturnDocumentations;

        /// <summary>
        /// Gets or sets a value indicating whether the TaxReturnDocumentations element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TaxReturnDocumentations element has been assigned a value.</value>
        [XmlIgnore]
        public bool TaxReturnDocumentationsSpecified
        {
            get { return this.TaxReturnDocumentations != null && this.TaxReturnDocumentations.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public REQUEST_FOR_TAX_RETURN_DOCUMENTATION_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
