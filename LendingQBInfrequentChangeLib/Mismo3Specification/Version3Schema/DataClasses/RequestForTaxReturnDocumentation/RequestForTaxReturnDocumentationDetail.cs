namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class REQUEST_FOR_TAX_RETURN_DOCUMENTATION_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the REQUEST_FOR_TAX_RETURN_DOCUMENTATION_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.NumberOfReturnsCountSpecified
                    || this.RefundToThirdPartyIndicatorSpecified
                    || this.TaxpayerIdentityTheftIndicatorSpecified
                    || this.TotalCostForReturnsAmountSpecified
                    || this.TranscriptCertifiedCopyIndicatorSpecified
                    || this.VerificationServicerDescriptionSpecified;
            }
        }

        /// <summary>
        /// The number of copies of tax returns requested on a single instance of the IRS Form 4506.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOCount NumberOfReturnsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the NumberOfReturnsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NumberOfReturnsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool NumberOfReturnsCountSpecified
        {
            get { return NumberOfReturnsCount != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates the refund of any fees related to the request for copy or transcript of tax return, will be made to the third party identified within the related IRS Form 4506.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOIndicator RefundToThirdPartyIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the RefundToThirdPartyIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RefundToThirdPartyIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool RefundToThirdPartyIndicatorSpecified
        {
            get { return RefundToThirdPartyIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that taxpayer has been a victim of identify theft.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOIndicator TaxpayerIdentityTheftIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the TaxpayerIdentityTheftIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TaxpayerIdentityTheftIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool TaxpayerIdentityTheftIndicatorSpecified
        {
            get { return TaxpayerIdentityTheftIndicator != null; }
            set { }
        }

        /// <summary>
        /// The total cost for all returns requested on a single instance of the IRS Form 4506.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOAmount TotalCostForReturnsAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the TotalCostForReturnsAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TotalCostForReturnsAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TotalCostForReturnsAmountSpecified
        {
            get { return TotalCostForReturnsAmount != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that the user requested a certified copy for court or administrative proceeding. Used on the IRS Form 4506.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOIndicator TranscriptCertifiedCopyIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the TranscriptCertifiedCopyIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TranscriptCertifiedCopyIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool TranscriptCertifiedCopyIndicatorSpecified
        {
            get { return TranscriptCertifiedCopyIndicator != null; }
            set { }
        }

        /// <summary>
        /// A description of the entities requesting and fulfilling the service, as prescribed by the IRS. Used on the IRS Form 4506 and 4506T in Line 5.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString VerificationServicerDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the VerificationServicerDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the VerificationServicerDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool VerificationServicerDescriptionSpecified
        {
            get { return VerificationServicerDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 6)]
        public REQUEST_FOR_TAX_RETURN_DOCUMENTATION_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
