namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class ASSISTANCES
    {
        /// <summary>
        /// Gets a value indicating whether the ASSISTANCES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AssistanceSpecified
                    || this.AssistanceSummarySpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of assistances.
        /// </summary>
        [XmlElement("ASSISTANCE", Order = 0)]
		public List<ASSISTANCE> Assistance = new List<ASSISTANCE>();

        /// <summary>
        /// Gets or sets a value indicating whether the Assistance element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Assistance element has been assigned a value.</value>
        [XmlIgnore]
        public bool AssistanceSpecified
        {
            get { return this.Assistance != null && this.Assistance.Count(a => a != null && a.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A summary of assistance.
        /// </summary>
        [XmlElement("ASSISTANCE_SUMMARY", Order = 1)]
        public ASSISTANCE_SUMMARY AssistanceSummary;

        /// <summary>
        /// Gets or sets a value indicating whether the AssistanceSummary element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AssistanceSummary element has been assigned a value.</value>
        [XmlIgnore]
        public bool AssistanceSummarySpecified
        {
            get { return this.AssistanceSummary != null && this.AssistanceSummary.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public ASSISTANCES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
