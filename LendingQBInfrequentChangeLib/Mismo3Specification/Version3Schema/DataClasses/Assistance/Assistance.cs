namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class ASSISTANCE
    {
        /// <summary>
        /// Gets a value indicating whether the ASSISTANCE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AssistanceActualDisbursementDateSpecified
                    || this.AssistanceAmountSpecified
                    || this.AssistanceMaximumAmountSpecified
                    || this.AssistancePeriodMonthsCountSpecified
                    || this.AssistanceRecipientTypeOtherDescriptionSpecified
                    || this.AssistanceRecipientTypeSpecified
                    || this.AssistanceScheduledDisbursementDateSpecified
                    || this.AssistanceSourceTypeOtherDescriptionSpecified
                    || this.AssistanceSourceTypeSpecified
                    || this.AssistanceTypeOtherDescriptionSpecified
                    || this.AssistanceTypeSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// The date on which the assistance is disbursed to the recipient in connection with a workout.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMODate AssistanceActualDisbursementDate;

        /// <summary>
        /// Gets or sets a value indicating whether the AssistanceActualDisbursementDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AssistanceActualDisbursementDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool AssistanceActualDisbursementDateSpecified
        {
            get { return AssistanceActualDisbursementDate != null; }
            set { }
        }

        /// <summary>
        /// The actual dollar amount of assistance offered with a workout.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOAmount AssistanceAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the AssistanceAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AssistanceAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool AssistanceAmountSpecified
        {
            get { return AssistanceAmount != null; }
            set { }
        }

        /// <summary>
        /// The maximum dollar amount  that a recipient is eligible for after a completed workout.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOAmount AssistanceMaximumAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the AssistanceMaximumAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AssistanceMaximumAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool AssistanceMaximumAmountSpecified
        {
            get { return AssistanceMaximumAmount != null; }
            set { }
        }

        /// <summary>
        /// The number of months over which the assistance with a workout will be in effect.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOCount AssistancePeriodMonthsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the AssistancePeriodMonthsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AssistancePeriodMonthsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool AssistancePeriodMonthsCountSpecified
        {
            get { return AssistancePeriodMonthsCount != null; }
            set { }
        }

        /// <summary>
        /// The type of party that is the recipient of the assistance with a workout.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<AssistanceRecipientBase> AssistanceRecipientType;

        /// <summary>
        /// Gets or sets a value indicating whether the AssistanceRecipientType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AssistanceRecipientType element has been assigned a value.</value>
        [XmlIgnore]
        public bool AssistanceRecipientTypeSpecified
        {
            get { return this.AssistanceRecipientType != null && this.AssistanceRecipientType.enumValue != AssistanceRecipientBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture additional information when Other is selected for Assistance Recipient Type.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString AssistanceRecipientTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the AssistanceRecipientTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AssistanceRecipientTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool AssistanceRecipientTypeOtherDescriptionSpecified
        {
            get { return AssistanceRecipientTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The date when the assistance is scheduled to be disbursed to the recipient in connection with a workout.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMODate AssistanceScheduledDisbursementDate;

        /// <summary>
        /// Gets or sets a value indicating whether the AssistanceScheduledDisbursementDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AssistanceScheduledDisbursementDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool AssistanceScheduledDisbursementDateSpecified
        {
            get { return AssistanceScheduledDisbursementDate != null; }
            set { }
        }

        /// <summary>
        /// The type of party that is providing the assistance with a workout.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOEnum<AssistanceSourceBase> AssistanceSourceType;

        /// <summary>
        /// Gets or sets a value indicating whether the AssistanceSourceType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AssistanceSourceType element has been assigned a value.</value>
        [XmlIgnore]
        public bool AssistanceSourceTypeSpecified
        {
            get { return this.AssistanceSourceType != null && this.AssistanceSourceType.enumValue != AssistanceSourceBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture additional information when Other is selected for Assistance Source Type.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOString AssistanceSourceTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the AssistanceSourceTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AssistanceSourceTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool AssistanceSourceTypeOtherDescriptionSpecified
        {
            get { return AssistanceSourceTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The type of assistance offered for a completed workout.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOEnum<AssistanceBase> AssistanceType;

        /// <summary>
        /// Gets or sets a value indicating whether the AssistanceType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AssistanceType element has been assigned a value.</value>
        [XmlIgnore]
        public bool AssistanceTypeSpecified
        {
            get { return this.AssistanceType != null && this.AssistanceType.enumValue != AssistanceBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Assistance Type.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOString AssistanceTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the AssistanceTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AssistanceTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool AssistanceTypeOtherDescriptionSpecified
        {
            get { return AssistanceTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 11)]
        public ASSISTANCE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
