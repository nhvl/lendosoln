namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class DOWN_PAYMENTS
    {
        /// <summary>
        /// Gets a value indicating whether the DOWN_PAYMENTS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DownPaymentSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of down payments.
        /// </summary>
        [XmlElement("DOWN_PAYMENT", Order = 0)]
		public List<DOWN_PAYMENT> DownPayment = new List<DOWN_PAYMENT>();

        /// <summary>
        /// Gets or sets a value indicating whether the DownPayment element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DownPayment element has been assigned a value.</value>
        [XmlIgnore]
        public bool DownPaymentSpecified
        {
            get { return this.DownPayment != null && this.DownPayment.Count(d => d != null && d.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public DOWN_PAYMENTS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
