namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class REQUESTING_PARTY
    {
        /// <summary>
        /// Gets a value indicating whether the REQUESTING_PARTY container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.InternalAccountIdentifierSpecified
                    || this.RequestedByNameSpecified
                    || this.RequestingPartyBranchIdentifierSpecified
                    || this.RequestingPartySequenceNumberSpecified;
            }
        }

        /// <summary>
        /// The account number or identifier assigned by a service provider to a service requestor. The Internal Account Identifier is supplied with request transactions to allow the service provider to identify the billing account to be used.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIdentifier InternalAccountIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the InternalAccountIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InternalAccountIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool InternalAccountIdentifierSpecified
        {
            get { return InternalAccountIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The name of the end user requesting the service.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString RequestedByName;

        /// <summary>
        /// Gets or sets a value indicating whether the RequestedByName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RequestedByName element has been assigned a value.</value>
        [XmlIgnore]
        public bool RequestedByNameSpecified
        {
            get { return RequestedByName != null; }
            set { }
        }

        /// <summary>
        /// The identification code of the requesting party for the branch number associated with the loan.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOIdentifier RequestingPartyBranchIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the RequestingPartyBranchIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RequestingPartyBranchIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool RequestingPartyBranchIdentifierSpecified
        {
            get { return RequestingPartyBranchIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Identifies the ordering of multiple requesting parties.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOSequenceNumber RequestingPartySequenceNumber;

        /// <summary>
        /// Gets or sets a value indicating whether the RequestingPartySequenceNumber element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RequestingPartySequenceNumber element has been assigned a value.</value>
        [XmlIgnore]
        public bool RequestingPartySequenceNumberSpecified
        {
            get { return RequestingPartySequenceNumber != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public REQUESTING_PARTY_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
