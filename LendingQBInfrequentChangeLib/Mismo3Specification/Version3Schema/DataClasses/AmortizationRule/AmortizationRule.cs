namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class AMORTIZATION_RULE
    {
        /// <summary>
        /// Gets a value indicating whether the AMORTIZATION_RULE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AmortizationTypeOtherDescriptionSpecified
                    || this.AmortizationTypeSpecified
                    || this.ExtensionSpecified
                    || this.LoanAmortizationMaximumTermMonthsCountSpecified
                    || this.LoanAmortizationPeriodCountSpecified
                    || this.LoanAmortizationPeriodTypeSpecified;
            }
        }

        /// <summary>
        /// A classification or description of a loan or a group of loans generally based on the changeability of the rate or payment over time.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<AmortizationBase> AmortizationType;

        /// <summary>
        /// Gets or sets a value indicating whether the AmortizationType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AmortizationType element has been assigned a value.</value>
        [XmlIgnore]
        public bool AmortizationTypeSpecified
        {
            get { return this.AmortizationType != null && this.AmortizationType.enumValue != AmortizationBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information or a description of the amortization type when Other is selected.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString AmortizationTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the AmortizationTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AmortizationTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool AmortizationTypeOtherDescriptionSpecified
        {
            get { return AmortizationTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The maximum number of months over which an extendable mortgage may be amortized.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOCount LoanAmortizationMaximumTermMonthsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanAmortizationMaximumTermMonthsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanAmortizationMaximumTermMonthsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanAmortizationMaximumTermMonthsCountSpecified
        {
            get { return LoanAmortizationMaximumTermMonthsCount != null; }
            set { }
        }

        /// <summary>
        /// The number periods (as defined by the Loan Amortization Period Type) over which the scheduled loan payments of principal and/or interest are calculated to retire the obligation.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOCount LoanAmortizationPeriodCount;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanAmortizationPeriodCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanAmortizationPeriodCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanAmortizationPeriodCountSpecified
        {
            get { return LoanAmortizationPeriodCount != null; }
            set { }
        }

        /// <summary>
        /// The duration of time used to define the period over which the loan is amortized.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<LoanAmortizationPeriodBase> LoanAmortizationPeriodType;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanAmortizationPeriodType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanAmortizationPeriodType element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanAmortizationPeriodTypeSpecified
        {
            get { return this.LoanAmortizationPeriodType != null && this.LoanAmortizationPeriodType.enumValue != LoanAmortizationPeriodBase.Blank; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 5)]
        public AMORTIZATION_RULE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
