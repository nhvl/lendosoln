namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class AMENITIES
    {
        /// <summary>
        /// Gets a value indicating whether the AMENITIES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AmenitySpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of amenities.
        /// </summary>
        [XmlElement("AMENITY", Order = 0)]
		public List<AMENITY> Amenity = new List<AMENITY>();

        /// <summary>
        /// Gets or sets a value indicating whether the Amenity element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Amenity element has been assigned a value.</value>
        [XmlIgnore]
        public bool AmenitySpecified
        {
            get { return this.Amenity != null && this.Amenity.Count(a => a != null && a.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public AMENITIES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
