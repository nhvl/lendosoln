namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PAYMENT_RULE
    {
        /// <summary>
        /// Gets a value indicating whether the PAYMENT_RULE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CurtailmentApplicationSequenceTypeSpecified
                    || this.ExtensionSpecified
                    || this.FinalPaymentAmountSpecified
                    || this.FirstPrincipalReductionDateSpecified
                    || this.FullyIndexedInitialPrincipalAndInterestPaymentAmountSpecified
                    || this.InitialInterestOnlyPaymentAmountSpecified
                    || this.InitialPaymentRatePercentSpecified
                    || this.InitialPITIPaymentAmountSpecified
                    || this.InitialPrincipalAndInterestAndMIMonthlyPaymentAmountSpecified
                    || this.InitialPrincipalAndInterestPaymentAmountSpecified
                    || this.InitialTaxAndInsurancePaymentAmountSpecified
                    || this.LoanPaymentAccumulationAmountSpecified
                    || this.PartialPaymentAllowedIndicatorSpecified
                    || this.PartialPaymentPenaltyIndicatorSpecified
                    || this.PaymentBillingMethodTypeSpecified
                    || this.PaymentBillingStatementFrequencyTypeSpecified
                    || this.PaymentBillingStatementLeadDaysCountSpecified
                    || this.PaymentFrequencyTypeOtherDescriptionSpecified
                    || this.PaymentFrequencyTypeSpecified
                    || this.PaymentOptionIndicatorSpecified
                    || this.PaymentRemittanceDaySpecified
                    || this.ScheduledAnnualPaymentCountSpecified
                    || this.ScheduledFirstPaymentDateSpecified
                    || this.ScheduledTotalPaymentCountSpecified
                    || this.SeasonalPaymentPeriodEndMonthSpecified
                    || this.SeasonalPaymentPeriodStartMonthSpecified
                    || this.ThirdPartyInvestorFeePassThroughPercentSpecified
                    || this.ThirdPartyInvestorInterestPassThroughPercentSpecified
                    || this.ThirdPartyInvestorPrincipalPassThroughPercentSpecified;
            }
        }

        /// <summary>
        /// Specifies the order in which unscheduled principal payments are applied relative to the scheduled payment.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<CurtailmentApplicationSequenceBase> CurtailmentApplicationSequenceType;

        /// <summary>
        /// Gets or sets a value indicating whether the CurtailmentApplicationSequenceType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CurtailmentApplicationSequenceType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CurtailmentApplicationSequenceTypeSpecified
        {
            get { return this.CurtailmentApplicationSequenceType != null && this.CurtailmentApplicationSequenceType.enumValue != CurtailmentApplicationSequenceBase.Blank; }
            set { }
        }

        /// <summary>
        /// The amount of the final scheduled payment. (This is a calculated results field.).
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOAmount FinalPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the FinalPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FinalPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool FinalPaymentAmountSpecified
        {
            get { return FinalPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// The date of the first scheduled payment that will reduce the UPB.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMODate FirstPrincipalReductionDate;

        /// <summary>
        /// Gets or sets a value indicating whether the FirstPrincipalReductionDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FirstPrincipalReductionDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool FirstPrincipalReductionDateSpecified
        {
            get { return FirstPrincipalReductionDate != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of the principal and interest payment when calculated using the loan amount and fully-indexed interest rate (in place of Note Interest Rate) to arrive at full amortization during the loan term.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOAmount FullyIndexedInitialPrincipalAndInterestPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the FullyIndexedInitialPrincipalAndInterestPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FullyIndexedInitialPrincipalAndInterestPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool FullyIndexedInitialPrincipalAndInterestPaymentAmountSpecified
        {
            get { return FullyIndexedInitialPrincipalAndInterestPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of the the initial interest payment for interest only loans.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOAmount InitialInterestOnlyPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the InitialInterestOnlyPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InitialInterestOnlyPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool InitialInterestOnlyPaymentAmountSpecified
        {
            get { return InitialInterestOnlyPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// The initial effective payment rate at which the borrower is making payments, even if it is temporary.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOPercent InitialPaymentRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the InitialPaymentRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InitialPaymentRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool InitialPaymentRatePercentSpecified
        {
            get { return InitialPaymentRatePercent != null; }
            set { }
        }

        /// <summary>
        /// The total monthly amount of the loan payment to be made by the borrower as disclosed on the Initial Escrow Account Disclosure, including principal and interest and all monthly escrow payment amounts. 
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOAmount InitialPITIPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the InitialPITIPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InitialPITIPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool InitialPITIPaymentAmountSpecified
        {
            get { return InitialPITIPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of the initial monthly payment including principal, interest, and mortgage insurance. For example, the amount identified as the initial monthly payment amount on the GFE.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOAmount InitialPrincipalAndInterestAndMIMonthlyPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the InitialPrincipalAndInterestAndMIMonthlyPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InitialPrincipalAndInterestAndMIMonthlyPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool InitialPrincipalAndInterestAndMIMonthlyPaymentAmountSpecified
        {
            get { return InitialPrincipalAndInterestAndMIMonthlyPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of the principal and interest payment as stated on the Note. The principal and interest payment is usually obtained using the loan amount and interest rate to arrive at full amortization during the loan term.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOAmount InitialPrincipalAndInterestPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the InitialPrincipalAndInterestPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InitialPrincipalAndInterestPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool InitialPrincipalAndInterestPaymentAmountSpecified
        {
            get { return InitialPrincipalAndInterestPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// Net taxes and insurance Payment (excluding Optional Insurance) to be collected on a monthly basis. This includes any Mortgage Insurance, all taxes and property casualty that are escrowed.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOAmount InitialTaxAndInsurancePaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the InitialTaxAndInsurancePaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InitialTaxAndInsurancePaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool InitialTaxAndInsurancePaymentAmountSpecified
        {
            get { return InitialTaxAndInsurancePaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// The minimum dollar amount that must be applied in order to advance the last paid installment date. This may be applicable to Title I daily simple interest loans.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOAmount LoanPaymentAccumulationAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanPaymentAccumulationAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanPaymentAccumulationAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanPaymentAccumulationAmountSpecified
        {
            get { return LoanPaymentAccumulationAmount != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that the terms of the loan allow principal and interest payments that are less than the amount due.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOIndicator PartialPaymentAllowedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PartialPaymentAllowedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PartialPaymentAllowedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PartialPaymentAllowedIndicatorSpecified
        {
            get { return PartialPaymentAllowedIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that penalties will be assessed if the borrower submits a partial payment.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOIndicator PartialPaymentPenaltyIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PartialPaymentPenaltyIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PartialPaymentPenaltyIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PartialPaymentPenaltyIndicatorSpecified
        {
            get { return PartialPaymentPenaltyIndicator != null; }
            set { }
        }

        /// <summary>
        /// Identifies the method used by the servicer to notify the mortgagor to remit the mortgage payment.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOEnum<PaymentBillingMethodBase> PaymentBillingMethodType;

        /// <summary>
        /// Gets or sets a value indicating whether the PaymentBillingMethodType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PaymentBillingMethodType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PaymentBillingMethodTypeSpecified
        {
            get { return this.PaymentBillingMethodType != null && this.PaymentBillingMethodType.enumValue != PaymentBillingMethodBase.Blank; }
            set { }
        }

        /// <summary>
        /// Identifies the frequency that statements are sent to the borrower.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOEnum<PaymentBillingStatementFrequencyBase> PaymentBillingStatementFrequencyType;

        /// <summary>
        /// Gets or sets a value indicating whether the PaymentBillingStatementFrequencyType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PaymentBillingStatementFrequencyType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PaymentBillingStatementFrequencyTypeSpecified
        {
            get { return this.PaymentBillingStatementFrequencyType != null && this.PaymentBillingStatementFrequencyType.enumValue != PaymentBillingStatementFrequencyBase.Blank; }
            set { }
        }

        /// <summary>
        /// The number of days between the billing statement date and the payment due date.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOCount PaymentBillingStatementLeadDaysCount;

        /// <summary>
        /// Gets or sets a value indicating whether the PaymentBillingStatementLeadDaysCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PaymentBillingStatementLeadDaysCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PaymentBillingStatementLeadDaysCountSpecified
        {
            get { return PaymentBillingStatementLeadDaysCount != null; }
            set { }
        }

        /// <summary>
        /// Specifies the frequency of the mortgage payment.
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMOEnum<PaymentFrequencyBase> PaymentFrequencyType;

        /// <summary>
        /// Gets or sets a value indicating whether the PaymentFrequencyType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PaymentFrequencyType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PaymentFrequencyTypeSpecified
        {
            get { return this.PaymentFrequencyType != null && this.PaymentFrequencyType.enumValue != PaymentFrequencyBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture the payment frequency type if Other is selected as the payment frequency.
        /// </summary>
        [XmlElement(Order = 17)]
        public MISMOString PaymentFrequencyTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PaymentFrequencyTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PaymentFrequencyTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PaymentFrequencyTypeOtherDescriptionSpecified
        {
            get { return PaymentFrequencyTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies that the loan instrument allows the borrower(s) to determine and change on a month-to-month basis the amount of any payment to be paid on the loan for a specified period of time.
        /// </summary>
        [XmlElement(Order = 18)]
        public MISMOIndicator PaymentOptionIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PaymentOptionIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PaymentOptionIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PaymentOptionIndicatorSpecified
        {
            get { return PaymentOptionIndicator != null; }
            set { }
        }

        /// <summary>
        /// The day of the month on which the loan payment is to be remitted under the terms of the Mortgage.
        /// </summary>
        [XmlElement(Order = 19)]
        public MISMODay PaymentRemittanceDay;

        /// <summary>
        /// Gets or sets a value indicating whether the PaymentRemittanceDay element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PaymentRemittanceDay element has been assigned a value.</value>
        [XmlIgnore]
        public bool PaymentRemittanceDaySpecified
        {
            get { return PaymentRemittanceDay != null; }
            set { }
        }

        /// <summary>
        /// The number of payments the borrower is expected to make per year.
        /// </summary>
        [XmlElement(Order = 20)]
        public MISMOCount ScheduledAnnualPaymentCount;

        /// <summary>
        /// Gets or sets a value indicating whether the ScheduledAnnualPaymentCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ScheduledAnnualPaymentCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ScheduledAnnualPaymentCountSpecified
        {
            get { return ScheduledAnnualPaymentCount != null; }
            set { }
        }

        /// <summary>
        /// The date of the first scheduled mortgage payment to be made by the borrower under the terms of the mortgage.
        /// </summary>
        [XmlElement(Order = 21)]
        public MISMODate ScheduledFirstPaymentDate;

        /// <summary>
        /// Gets or sets a value indicating whether the ScheduledFirstPaymentDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ScheduledFirstPaymentDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ScheduledFirstPaymentDateSpecified
        {
            get { return ScheduledFirstPaymentDate != null; }
            set { }
        }

        /// <summary>
        /// The total number of payments the borrower is expected to make over the course of the loan.
        /// </summary>
        [XmlElement(Order = 22)]
        public MISMOCount ScheduledTotalPaymentCount;

        /// <summary>
        /// Gets or sets a value indicating whether the ScheduledTotalPaymentCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ScheduledTotalPaymentCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ScheduledTotalPaymentCountSpecified
        {
            get { return ScheduledTotalPaymentCount != null; }
            set { }
        }

        /// <summary>
        /// The month that the period during which payments are not scheduled ends.
        /// </summary>
        [XmlElement(Order = 23)]
        public MISMOMonth SeasonalPaymentPeriodEndMonth;

        /// <summary>
        /// Gets or sets a value indicating whether the SeasonalPaymentPeriodEndMonth element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SeasonalPaymentPeriodEndMonth element has been assigned a value.</value>
        [XmlIgnore]
        public bool SeasonalPaymentPeriodEndMonthSpecified
        {
            get { return SeasonalPaymentPeriodEndMonth != null; }
            set { }
        }

        /// <summary>
        /// The month that the period during which payments are not scheduled begins.
        /// </summary>
        [XmlElement(Order = 24)]
        public MISMOMonth SeasonalPaymentPeriodStartMonth;

        /// <summary>
        /// Gets or sets a value indicating whether the SeasonalPaymentPeriodStartMonth element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SeasonalPaymentPeriodStartMonth element has been assigned a value.</value>
        [XmlIgnore]
        public bool SeasonalPaymentPeriodStartMonthSpecified
        {
            get { return SeasonalPaymentPeriodStartMonth != null; }
            set { }
        }

        /// <summary>
        /// The percentage of fees paid by the borrower that are passed through to a third party investor.
        /// </summary>
        [XmlElement(Order = 25)]
        public MISMOPercent ThirdPartyInvestorFeePassThroughPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the ThirdPartyInvestorFeePassThroughPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ThirdPartyInvestorFeePassThroughPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool ThirdPartyInvestorFeePassThroughPercentSpecified
        {
            get { return ThirdPartyInvestorFeePassThroughPercent != null; }
            set { }
        }

        /// <summary>
        /// The percentage of interest payments made by the borrower that are passed through to a third party investor.
        /// </summary>
        [XmlElement(Order = 26)]
        public MISMOPercent ThirdPartyInvestorInterestPassThroughPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the ThirdPartyInvestorInterestPassThroughPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ThirdPartyInvestorInterestPassThroughPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool ThirdPartyInvestorInterestPassThroughPercentSpecified
        {
            get { return ThirdPartyInvestorInterestPassThroughPercent != null; }
            set { }
        }

        /// <summary>
        /// The percentage of principal payments made by the borrower that are passed through to a third party investor.
        /// </summary>
        [XmlElement(Order = 27)]
        public MISMOPercent ThirdPartyInvestorPrincipalPassThroughPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the ThirdPartyInvestorPrincipalPassThroughPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ThirdPartyInvestorPrincipalPassThroughPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool ThirdPartyInvestorPrincipalPassThroughPercentSpecified
        {
            get { return ThirdPartyInvestorPrincipalPassThroughPercent != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 28)]
        public PAYMENT_RULE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
