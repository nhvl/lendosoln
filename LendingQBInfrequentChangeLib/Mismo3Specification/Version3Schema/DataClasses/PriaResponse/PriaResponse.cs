namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PRIA_RESPONSE
    {
        /// <summary>
        /// Gets a value indicating whether the PRIA_RESPONSE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.OriginatingRecordingRequestSpecified
                    || this.PriaResponseDetailSpecified
                    || this.RecordingEndorsementSpecified
                    || this.RecordingErrorsSpecified
                    || this.RecordingTransactionIdentifierSpecified;
            }
        }

        /// <summary>
        /// The originating recording request for this response.
        /// </summary>
        [XmlElement("ORIGINATING_RECORDING_REQUEST", Order = 0)]
        public ORIGINATING_RECORDING_REQUEST OriginatingRecordingRequest;

        /// <summary>
        /// Gets or sets a value indicating whether the OriginatingRecordingRequest element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OriginatingRecordingRequest element has been assigned a value.</value>
        [XmlIgnore]
        public bool OriginatingRecordingRequestSpecified
        {
            get { return this.OriginatingRecordingRequest != null && this.OriginatingRecordingRequest.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details about a PRIA response.
        /// </summary>
        [XmlElement("PRIA_RESPONSE_DETAIL", Order = 1)]
        public PRIA_RESPONSE_DETAIL PriaResponseDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the Response Detail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Response Detail element has been assigned a value.</value>
        [XmlIgnore]
        public bool PriaResponseDetailSpecified
        {
            get { return this.PriaResponseDetail != null && this.PriaResponseDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// An endorsement for the recording.
        /// </summary>
        [XmlElement("RECORDING_ENDORSEMENT", Order = 2)]
        public RECORDING_ENDORSEMENT RecordingEndorsement;

        /// <summary>
        /// Gets or sets a value indicating whether the RecordingEndorsement element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RecordingEndorsement element has been assigned a value.</value>
        [XmlIgnore]
        public bool RecordingEndorsementSpecified
        {
            get { return this.RecordingEndorsement != null && this.RecordingEndorsement.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Errors related to the recording.
        /// </summary>
        [XmlElement("RECORDING_ERRORS", Order = 3)]
        public RECORDING_ERRORS RecordingErrors;

        /// <summary>
        /// Gets or sets a value indicating whether the RecordingErrors element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RecordingErrors element has been assigned a value.</value>
        [XmlIgnore]
        public bool RecordingErrorsSpecified
        {
            get { return this.RecordingErrors != null && this.RecordingErrors.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Identifier for a recording transaction.
        /// </summary>
        [XmlElement("RECORDING_TRANSACTION_IDENTIFIER", Order = 4)]
        public RECORDING_TRANSACTION_IDENTIFIER RecordingTransactionIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the RecordingTransactionIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RecordingTransactionIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool RecordingTransactionIdentifierSpecified
        {
            get { return this.RecordingTransactionIdentifier != null && this.RecordingTransactionIdentifier.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 5)]
        public PRIA_RESPONSE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
