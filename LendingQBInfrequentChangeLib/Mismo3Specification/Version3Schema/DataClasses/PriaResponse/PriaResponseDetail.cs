namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PRIA_RESPONSE_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the PRIA_RESPONSE_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.PRIAResponseRelatedDocumentsIndicatorSpecified
                    || this.PRIAResponseTypeOtherDescriptionSpecified
                    || this.PRIAResponseTypeSpecified;
            }
        }

        /// <summary>
        /// Indicates whether the documents sent are related to each other.  If "yes", all documents must pass recording tests for any to be recorded.  If "no", one document can fail recording without preventing the remaining documents from being recorded.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIndicator PRIAResponseRelatedDocumentsIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PRIAResponseRelatedDocumentsIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PRIAResponseRelatedDocumentsIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PRIAResponseRelatedDocumentsIndicatorSpecified
        {
            get { return PRIAResponseRelatedDocumentsIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates the type of request this response relates to.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<PRIAResponseBase> PRIAResponseType;

        /// <summary>
        /// Gets or sets a value indicating whether the PRIAResponseType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PRIAResponseType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PRIAResponseTypeSpecified
        {
            get { return this.PRIAResponseType != null && this.PRIAResponseType.enumValue != PRIAResponseBase.Blank; }
            set { }
        }

        /// <summary>
        /// A description of the request this response relates to that is not included in the enumerated list.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString PRIAResponseTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PRIAResponseTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PRIAResponseTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PRIAResponseTypeOtherDescriptionSpecified
        {
            get { return PRIAResponseTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public PRIA_RESPONSE_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
