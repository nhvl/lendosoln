namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class CERTIFICATE
    {
        /// <summary>
        /// Gets a value indicating whether the CERTIFICATE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CertificateApplicationDateSpecified
                    || this.CertificateAuthorityTypeOtherDescriptionSpecified
                    || this.CertificateAuthorityTypeSpecified
                    || this.CertificateExpirationDateSpecified
                    || this.CertificateIssuedDateSpecified
                    || this.CertificateStatusDateSpecified
                    || this.CertificateStatusTypeOtherDescriptionSpecified
                    || this.CertificateStatusTypeSpecified
                    || this.CertificateTypeOtherDescriptionSpecified
                    || this.CertificateTypeSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// The date of the certificate application as recorded by the local authority.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMODate CertificateApplicationDate;

        /// <summary>
        /// Gets or sets a value indicating whether the CertificateApplicationDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CertificateApplicationDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool CertificateApplicationDateSpecified
        {
            get { return CertificateApplicationDate != null; }
            set { }
        }

        /// <summary>
        /// The type of authority which issued the permission for the property.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<CertificateAuthorityBase> CertificateAuthorityType;

        /// <summary>
        /// Gets or sets a value indicating whether the CertificateAuthorityType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CertificateAuthorityType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CertificateAuthorityTypeSpecified
        {
            get { return this.CertificateAuthorityType != null && this.CertificateAuthorityType.enumValue != CertificateAuthorityBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the status of a given certificate issued by a local authority if Other is selected as the Certificate Authority Type.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString CertificateAuthorityTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the CertificateAuthorityTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CertificateAuthorityTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool CertificateAuthorityTypeOtherDescriptionSpecified
        {
            get { return CertificateAuthorityTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The date of the certificate expiration as recorded by the local authority.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMODate CertificateExpirationDate;

        /// <summary>
        /// Gets or sets a value indicating whether the CertificateExpirationDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CertificateExpirationDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool CertificateExpirationDateSpecified
        {
            get { return CertificateExpirationDate != null; }
            set { }
        }

        /// <summary>
        /// The date of the certificate issuance as recorded by the local authority.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMODate CertificateIssuedDate;

        /// <summary>
        /// Gets or sets a value indicating whether the CertificateIssuedDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CertificateIssuedDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool CertificateIssuedDateSpecified
        {
            get { return CertificateIssuedDate != null; }
            set { }
        }

        /// <summary>
        /// The date corresponding to the status of the certificate as indicated in the Certificate Status Type as assigned by the authority for this permit.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMODate CertificateStatusDate;

        /// <summary>
        /// Gets or sets a value indicating whether the CertificateStatusDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CertificateStatusDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool CertificateStatusDateSpecified
        {
            get { return CertificateStatusDate != null; }
            set { }
        }

        /// <summary>
        /// The status for the certificate.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOEnum<CertificateStatusBase> CertificateStatusType;

        /// <summary>
        /// Gets or sets a value indicating whether the CertificateStatusType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CertificateStatusType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CertificateStatusTypeSpecified
        {
            get { return this.CertificateStatusType != null && this.CertificateStatusType.enumValue != CertificateStatusBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the status of a given certificate issued by a local authority if Other is selected as the Certificate Status Type.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOString CertificateStatusTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the CertificateStatusTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CertificateStatusTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool CertificateStatusTypeOtherDescriptionSpecified
        {
            get { return CertificateStatusTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The type of certificate issued by a local authority for the property.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOEnum<CertificateBase> CertificateType;

        /// <summary>
        /// Gets or sets a value indicating whether the CertificateType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CertificateType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CertificateTypeSpecified
        {
            get { return this.CertificateType != null && this.CertificateType.enumValue != CertificateBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the certificate if Other is selected as the Certificate Type.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOString CertificateTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the CertificateTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CertificateTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool CertificateTypeOtherDescriptionSpecified
        {
            get { return CertificateTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 10)]
        public CERTIFICATE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
