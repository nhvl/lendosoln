namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CERTIFICATES
    {
        /// <summary>
        /// Gets a value indicating whether the CERTIFICATES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CertificateSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of certificates.
        /// </summary>
        [XmlElement("CERTIFICATE", Order = 0)]
		public List<CERTIFICATE> Certificate = new List<CERTIFICATE>();

        /// <summary>
        /// Gets or sets a value indicating whether the Certificate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Certificate element has been assigned a value.</value>
        [XmlIgnore]
        public bool CertificateSpecified
        {
            get { return this.Certificate != null && this.Certificate.Count(c => c != null && c.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public CERTIFICATES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
