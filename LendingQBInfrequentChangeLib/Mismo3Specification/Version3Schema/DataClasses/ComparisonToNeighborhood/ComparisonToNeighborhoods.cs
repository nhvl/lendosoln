namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class COMPARISON_TO_NEIGHBORHOODS
    {
        /// <summary>
        /// Gets a value indicating whether the COMPARISON_TO_NEIGHBORHOODS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ComparisonToNeighborhoodSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of comparisons to neighborhood.
        /// </summary>
        [XmlElement("COMPARISON_TO_NEIGHBORHOOD", Order = 0)]
		public List<COMPARISON_TO_NEIGHBORHOOD> ComparisonToNeighborhood = new List<COMPARISON_TO_NEIGHBORHOOD>();

        /// <summary>
        /// Gets or sets a value indicating whether the ComparisonToNeighborhood element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ComparisonToNeighborhood element has been assigned a value.</value>
        [XmlIgnore]
        public bool ComparisonToNeighborhoodSpecified
        {
            get { return this.ComparisonToNeighborhood != null && this.ComparisonToNeighborhood.Count(c => c != null && c.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public COMPARISON_TO_NEIGHBORHOODS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
