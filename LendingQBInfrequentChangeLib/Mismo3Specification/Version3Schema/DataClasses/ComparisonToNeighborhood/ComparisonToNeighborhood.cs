namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class COMPARISON_TO_NEIGHBORHOOD
    {
        /// <summary>
        /// Gets a value indicating whether the COMPARISON_TO_NEIGHBORHOOD container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.LivingUnitCountSpecified
                    || this.RentControlDescriptionSpecified
                    || this.RentControlStatusTypeSpecified
                    || this.StoriesCountSpecified
                    || this.StructureFeatureTypeOtherDescriptionSpecified
                    || this.StructureFeatureTypeSpecified
                    || this.StructureTypicalToNeighborhoodAtypicalDescriptionSpecified
                    || this.StructureTypicalToNeighborhoodExistsIndicatorSpecified
                    || this.TypicalApartmentDescriptionSpecified
                    || this.TypicalApartmentEstimatedVacancyPercentSpecified
                    || this.TypicalApartmentRentHighPriceAmountSpecified
                    || this.TypicalApartmentRentLowPriceAmountSpecified
                    || this.TypicalApartmentRentTrendTypeSpecified
                    || this.TypicalApartmentVacancyTrendTypeSpecified;
            }
        }

        /// <summary>
        /// Number of separate living units (i.e. in a structure such as an apartment or duplex).
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOCount LivingUnitCount;

        /// <summary>
        /// Gets or sets a value indicating whether the LivingUnitCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LivingUnitCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool LivingUnitCountSpecified
        {
            get { return LivingUnitCount != null; }
            set { }
        }

        /// <summary>
        /// Description of the existing or proposed rent controls for a property.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString RentControlDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the RentControlDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RentControlDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool RentControlDescriptionSpecified
        {
            get { return RentControlDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the existence or likelihood of rent controls such as controls on businesses and apartment complexes.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<RentControlStatusBase> RentControlStatusType;

        /// <summary>
        /// Gets or sets a value indicating whether the RentControlStatusType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RentControlStatusType element has been assigned a value.</value>
        [XmlIgnore]
        public bool RentControlStatusTypeSpecified
        {
            get { return this.RentControlStatusType != null && this.RentControlStatusType.enumValue != RentControlStatusBase.Blank; }
            set { }
        }

        /// <summary>
        /// Specifies the number of habitable levels above grade in a structure such as in a 2 story house, a cape cod with 1.5  stories or a 2 story condominium unit in a multi-level building.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOCount StoriesCount;

        /// <summary>
        /// Gets or sets a value indicating whether the StoriesCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the StoriesCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool StoriesCountSpecified
        {
            get { return StoriesCount != null; }
            set { }
        }

        /// <summary>
        /// Specifies an aspect of the structure that is typical to its neighborhood.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<StructureFeatureBase> StructureFeatureType;

        /// <summary>
        /// Gets or sets a value indicating whether the StructureFeatureType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the StructureFeatureType element has been assigned a value.</value>
        [XmlIgnore]
        public bool StructureFeatureTypeSpecified
        {
            get { return this.StructureFeatureType != null && this.StructureFeatureType.enumValue != StructureFeatureBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe a structure feature if Other is selected as the Structure Feature Type.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString StructureFeatureTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the StructureFeatureTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the StructureFeatureTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool StructureFeatureTypeOtherDescriptionSpecified
        {
            get { return StructureFeatureTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field describing how the structure differs from the typical neighborhood structure.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOString StructureTypicalToNeighborhoodAtypicalDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the StructureTypicalToNeighborhoodAtypicalDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the StructureTypicalToNeighborhoodAtypicalDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool StructureTypicalToNeighborhoodAtypicalDescriptionSpecified
        {
            get { return StructureTypicalToNeighborhoodAtypicalDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the aspect specified by Structure Typical To Neighborhood is present in the structure.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOIndicator StructureTypicalToNeighborhoodExistsIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the StructureTypicalToNeighborhoodExistsIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the StructureTypicalToNeighborhoodExistsIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool StructureTypicalToNeighborhoodExistsIndicatorSpecified
        {
            get { return StructureTypicalToNeighborhoodExistsIndicator != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the typical comparable buildings in the property neighborhood.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOString TypicalApartmentDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the TypicalApartmentDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TypicalApartmentDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool TypicalApartmentDescriptionSpecified
        {
            get { return TypicalApartmentDescription != null; }
            set { }
        }

        /// <summary>
        /// The estimated vacancy percentage for a typical two-to-four family building the property neighborhood.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOPercent TypicalApartmentEstimatedVacancyPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the TypicalApartmentEstimatedVacancyPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TypicalApartmentEstimatedVacancyPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool TypicalApartmentEstimatedVacancyPercentSpecified
        {
            get { return TypicalApartmentEstimatedVacancyPercent != null; }
            set { }
        }

        /// <summary>
        /// The high end of the range of typical market rents in the neighborhood.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOAmount TypicalApartmentRentHighPriceAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the TypicalApartmentRentHighPriceAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TypicalApartmentRentHighPriceAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TypicalApartmentRentHighPriceAmountSpecified
        {
            get { return TypicalApartmentRentHighPriceAmount != null; }
            set { }
        }

        /// <summary>
        /// The low end of the range of typical market rents in the neighborhood.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOAmount TypicalApartmentRentLowPriceAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the TypicalApartmentRentLowPriceAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TypicalApartmentRentLowPriceAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TypicalApartmentRentLowPriceAmountSpecified
        {
            get { return TypicalApartmentRentLowPriceAmount != null; }
            set { }
        }

        /// <summary>
        /// Specifies the current trend in rent prices for the typical apartment in the neighborhood.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOEnum<TypicalApartmentRentTrendBase> TypicalApartmentRentTrendType;

        /// <summary>
        /// Gets or sets a value indicating whether the TypicalApartmentRentTrendType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TypicalApartmentRentTrendType element has been assigned a value.</value>
        [XmlIgnore]
        public bool TypicalApartmentRentTrendTypeSpecified
        {
            get { return this.TypicalApartmentRentTrendType != null && this.TypicalApartmentRentTrendType.enumValue != TypicalApartmentRentTrendBase.Blank; }
            set { }
        }

        /// <summary>
        /// Specifies the current trend in vacancy rates for the typical apartment in the neighborhood.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOEnum<TypicalApartmentVacancyTrendBase> TypicalApartmentVacancyTrendType;

        /// <summary>
        /// Gets or sets a value indicating whether the TypicalApartmentVacancyTrendType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TypicalApartmentVacancyTrendType element has been assigned a value.</value>
        [XmlIgnore]
        public bool TypicalApartmentVacancyTrendTypeSpecified
        {
            get { return this.TypicalApartmentVacancyTrendType != null && this.TypicalApartmentVacancyTrendType.enumValue != TypicalApartmentVacancyTrendBase.Blank; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 14)]
        public COMPARISON_TO_NEIGHBORHOOD_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
