namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class VALUATION_UPDATE
    {
        /// <summary>
        /// Gets a value indicating whether the VALUATION_UPDATE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AppraisalCompletionCertificateCommentDescriptionSpecified
                    || this.AppraisalCompletionCertificateIndicatorSpecified
                    || this.AppraisalReviewOriginalAppraisalEffectiveDateSpecified
                    || this.AppraisalReviewOriginalAppraisedValueAmountSpecified
                    || this.AppraisalUpdateCommentDescriptionSpecified
                    || this.AppraisalUpdateIndicatorSpecified
                    || this.AppraisalUpdateOriginalAppraiserCompanyNameSpecified
                    || this.AppraisalUpdateOriginalAppraiserUnparsedNameSpecified
                    || this.AppraisalUpdateOriginalLenderAddressLineTextSpecified
                    || this.AppraisalUpdateOriginalLenderUnparsedNameSpecified
                    || this.ExtensionSpecified
                    || this.PropertyImprovementsCompletedIndicatorSpecified
                    || this.PropertyMarketValueDecreasedIndicatorSpecified;
            }
        }

        /// <summary>
        /// A free-form text field used to describe or comment on the Certification of Completion of property improvements that a property valuation was dependent upon.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString AppraisalCompletionCertificateCommentDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the AppraisalCompletionCertificateCommentDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AppraisalCompletionCertificateCommentDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool AppraisalCompletionCertificateCommentDescriptionSpecified
        {
            get { return AppraisalCompletionCertificateCommentDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether the appraisal report is a Certification of Completion of improvements upon which a previous appraisal report is dependent.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOIndicator AppraisalCompletionCertificateIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the AppraisalCompletionCertificateIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AppraisalCompletionCertificateIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool AppraisalCompletionCertificateIndicatorSpecified
        {
            get { return AppraisalCompletionCertificateIndicator != null; }
            set { }
        }

        /// <summary>
        /// Specifies the original appraisal effective date and distinguishes it from all other effective dates during an appraisal review or appraisal update.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMODate AppraisalReviewOriginalAppraisalEffectiveDate;

        /// <summary>
        /// Gets or sets a value indicating whether the AppraisalReviewOriginalAppraisalEffectiveDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AppraisalReviewOriginalAppraisalEffectiveDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool AppraisalReviewOriginalAppraisalEffectiveDateSpecified
        {
            get { return AppraisalReviewOriginalAppraisalEffectiveDate != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of the original property valuation. This field distinguishes it from a new valuation amount determined during an appraisal review or appraisal update.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOAmount AppraisalReviewOriginalAppraisedValueAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the AppraisalReviewOriginalAppraisedValueAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AppraisalReviewOriginalAppraisedValueAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool AppraisalReviewOriginalAppraisedValueAmountSpecified
        {
            get { return AppraisalReviewOriginalAppraisedValueAmount != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe or comment on the Appraisal Update Report.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString AppraisalUpdateCommentDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the AppraisalUpdateCommentDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AppraisalUpdateCommentDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool AppraisalUpdateCommentDescriptionSpecified
        {
            get { return AppraisalUpdateCommentDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether the appraisal report is an update to an existing appraisal report.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOIndicator AppraisalUpdateIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the AppraisalUpdateIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AppraisalUpdateIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool AppraisalUpdateIndicatorSpecified
        {
            get { return AppraisalUpdateIndicator != null; }
            set { }
        }

        /// <summary>
        /// Specifies the company name of the original appraiser whose appraisal report is being reviewed or updated.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOString AppraisalUpdateOriginalAppraiserCompanyName;

        /// <summary>
        /// Gets or sets a value indicating whether the AppraisalUpdateOriginalAppraiserCompanyName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AppraisalUpdateOriginalAppraiserCompanyName element has been assigned a value.</value>
        [XmlIgnore]
        public bool AppraisalUpdateOriginalAppraiserCompanyNameSpecified
        {
            get { return AppraisalUpdateOriginalAppraiserCompanyName != null; }
            set { }
        }

        /// <summary>
        /// Specifies the unparsed name of the original appraiser whose appraisal report is being reviewed or updated.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOString AppraisalUpdateOriginalAppraiserUnparsedName;

        /// <summary>
        /// Gets or sets a value indicating whether the AppraisalUpdateOriginalAppraiserUnparsedName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AppraisalUpdateOriginalAppraiserUnparsedName element has been assigned a value.</value>
        [XmlIgnore]
        public bool AppraisalUpdateOriginalAppraiserUnparsedNameSpecified
        {
            get { return AppraisalUpdateOriginalAppraiserUnparsedName != null; }
            set { }
        }

        /// <summary>
        /// Specifies the unparsed address of the original lender on an appraisal report that is being reviewed or updated.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOString AppraisalUpdateOriginalLenderAddressLineText;

        /// <summary>
        /// Gets or sets a value indicating whether the AppraisalUpdateOriginalLenderAddressLineText element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AppraisalUpdateOriginalLenderAddressLineText element has been assigned a value.</value>
        [XmlIgnore]
        public bool AppraisalUpdateOriginalLenderAddressLineTextSpecified
        {
            get { return AppraisalUpdateOriginalLenderAddressLineText != null; }
            set { }
        }

        /// <summary>
        /// Specifies the unparsed name of the original lender on an appraisal report that is being reviewed or updated.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOString AppraisalUpdateOriginalLenderUnparsedName;

        /// <summary>
        /// Gets or sets a value indicating whether the AppraisalUpdateOriginalLenderUnparsedName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AppraisalUpdateOriginalLenderUnparsedName element has been assigned a value.</value>
        [XmlIgnore]
        public bool AppraisalUpdateOriginalLenderUnparsedNameSpecified
        {
            get { return AppraisalUpdateOriginalLenderUnparsedName != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether the improvements to the property have been completed. These are improvements generally in accordance with the requirements stated in an appraisal report.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOIndicator PropertyImprovementsCompletedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyImprovementsCompletedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyImprovementsCompletedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyImprovementsCompletedIndicatorSpecified
        {
            get { return PropertyImprovementsCompletedIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether the market value of a property has decreased. Generally used when updating a previous appraisal on the same property.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOIndicator PropertyMarketValueDecreasedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyMarketValueDecreasedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyMarketValueDecreasedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyMarketValueDecreasedIndicatorSpecified
        {
            get { return PropertyMarketValueDecreasedIndicator != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 12)]
        public VALUATION_UPDATE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
