namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class SITE_FEATURE
    {
        /// <summary>
        /// Gets a value indicating whether the SITE_FEATURE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.IrrigationSystemSourceTypeOtherDescriptionSpecified
                    || this.IrrigationSystemSourceTypeSpecified
                    || this.LandscapeLightingPowerSourceTypeOtherDescriptionSpecified
                    || this.LandscapeLightingPowerSourceTypeSpecified
                    || this.LandscapingTypeOtherDescriptionSpecified
                    || this.LandscapingTypeSpecified;
            }
        }

        /// <summary>
        /// A collection of values that specify the source of water for an irrigation system.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<IrrigationSystemSourceBase> IrrigationSystemSourceType;

        /// <summary>
        /// Gets or sets a value indicating whether the IrrigationSystemSourceType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IrrigationSystemSourceType element has been assigned a value.</value>
        [XmlIgnore]
        public bool IrrigationSystemSourceTypeSpecified
        {
            get { return this.IrrigationSystemSourceType != null && this.IrrigationSystemSourceType.enumValue != IrrigationSystemSourceBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field that specifies the enumeration when the value of Other is used for the Irrigation System Source Type.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString IrrigationSystemSourceTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the IrrigationSystemSourceTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IrrigationSystemSourceTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool IrrigationSystemSourceTypeOtherDescriptionSpecified
        {
            get { return IrrigationSystemSourceTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A collection of values that describe the source of power for a landscape lighting system.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<LandscapeLightingPowerSourceBase> LandscapeLightingPowerSourceType;

        /// <summary>
        /// Gets or sets a value indicating whether the LandscapeLightingPowerSourceType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LandscapeLightingPowerSourceType element has been assigned a value.</value>
        [XmlIgnore]
        public bool LandscapeLightingPowerSourceTypeSpecified
        {
            get { return this.LandscapeLightingPowerSourceType != null && this.LandscapeLightingPowerSourceType.enumValue != LandscapeLightingPowerSourceBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field that specifies the enumeration when the value of Other is used for the Landscape Lighting Power Source Type.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString LandscapeLightingPowerSourceTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the LandscapeLightingPowerSourceTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LandscapeLightingPowerSourceTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool LandscapeLightingPowerSourceTypeOtherDescriptionSpecified
        {
            get { return LandscapeLightingPowerSourceTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A collection of values that describe materials or methods used in landscaping a property.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<LandscapingBase> LandscapingType;

        /// <summary>
        /// Gets or sets a value indicating whether the LandscapingType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LandscapingType element has been assigned a value.</value>
        [XmlIgnore]
        public bool LandscapingTypeSpecified
        {
            get { return this.LandscapingType != null && this.LandscapingType.enumValue != LandscapingBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field that specifies the enumeration when the value of Other is used for the Landscaping Type.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString LandscapingTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the LandscapingTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LandscapingTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool LandscapingTypeOtherDescriptionSpecified
        {
            get { return LandscapingTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 6)]
        public SITE_FEATURE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
