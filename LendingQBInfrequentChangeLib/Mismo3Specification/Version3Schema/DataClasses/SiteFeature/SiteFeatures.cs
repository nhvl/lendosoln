namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class SITE_FEATURES
    {
        /// <summary>
        /// Gets a value indicating whether the SITE_FEATURES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.SiteFeatureSpecified;
            }
        }

        /// <summary>
        /// A collection of site features.
        /// </summary>
        [XmlElement("SITE_FEATURE", Order = 0)]
		public List<SITE_FEATURE> SiteFeature = new List<SITE_FEATURE>();

        /// <summary>
        /// Gets or sets a value indicating whether the SiteFeature element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SiteFeature element has been assigned a value.</value>
        [XmlIgnore]
        public bool SiteFeatureSpecified
        {
            get { return this.SiteFeature != null && this.SiteFeature.Count(s => s != null && s.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public SITE_FEATURES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
