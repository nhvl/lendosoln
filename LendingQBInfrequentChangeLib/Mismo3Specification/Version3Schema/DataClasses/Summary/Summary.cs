namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class SUMMARY
    {
        /// <summary>
        /// Gets a value indicating whether the SUMMARY container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.SummaryAmountSpecified
                    || this.SummaryAmountTypeOtherDescriptionSpecified
                    || this.SummaryAmountTypeSpecified;
            }
        }

        /// <summary>
        /// The summary amount expressed in dollars.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount SummaryAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the SummaryAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SummaryAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool SummaryAmountSpecified
        {
            get { return SummaryAmount != null; }
            set { }
        }

        /// <summary>
        /// Identifies the type of summary amount.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<SummaryAmountBase> SummaryAmountType;

        /// <summary>
        /// Gets or sets a value indicating whether the SummaryAmountType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SummaryAmountType element has been assigned a value.</value>
        [XmlIgnore]
        public bool SummaryAmountTypeSpecified
        {
            get { return this.SummaryAmountType != null && this.SummaryAmountType.enumValue != SummaryAmountBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture the payment frequency type if Other is selected as the Summary Type.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString SummaryAmountTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the SummaryAmountTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SummaryAmountTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool SummaryAmountTypeOtherDescriptionSpecified
        {
            get { return SummaryAmountTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public SUMMARY_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
