namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class SUMMARIES
    {
        /// <summary>
        /// Gets a value indicating whether the SUMMARIES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.SummarySpecified;
            }
        }

        /// <summary>
        /// A collection of summaries.
        /// </summary>
        [XmlElement("SUMMARY", Order = 0)]
		public List<SUMMARY> Summary = new List<SUMMARY>();

        /// <summary>
        /// Gets or sets a value indicating whether the Summary element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Summary element has been assigned a value.</value>
        [XmlIgnore]
        public bool SummarySpecified
        {
            get { return this.Summary != null && this.Summary.Count(s => s != null && s.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public SUMMARIES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
