namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class SERVICING_COMMENTS
    {
        /// <summary>
        /// Gets a value indicating whether the SERVICING_COMMENTS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ServicingCommentSpecified;
            }
        }

        /// <summary>
        /// A collection of servicing comments.
        /// </summary>
        [XmlElement("SERVICING_COMMENT", Order = 0)]
		public List<SERVICING_COMMENT> ServicingComment = new List<SERVICING_COMMENT>();

        /// <summary>
        /// Gets or sets a value indicating whether the ServicingComment element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServicingComment element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServicingCommentSpecified
        {
            get { return this.ServicingComment != null && this.ServicingComment.Count(s => s != null && s.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public SERVICING_COMMENTS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
