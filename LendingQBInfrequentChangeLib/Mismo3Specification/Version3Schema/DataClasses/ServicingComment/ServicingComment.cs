namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class SERVICING_COMMENT
    {
        /// <summary>
        /// Gets a value indicating whether the SERVICING_COMMENT container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ServicingCommentDatetimeSpecified
                    || this.ServicingCommentSourceDescriptionSpecified
                    || this.ServicingCommentTextSpecified
                    || this.ServicingCommentTypeOtherDescriptionSpecified
                    || this.ServicingCommentTypeSpecified;
            }
        }

        /// <summary>
        /// The date and time of the servicing comment.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMODatetime ServicingCommentDatetime;

        /// <summary>
        /// Gets or sets a value indicating whether the ServicingCommentDateTime element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServicingCommentDateTime element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServicingCommentDatetimeSpecified
        {
            get { return ServicingCommentDatetime != null; }
            set { }
        }

        /// <summary>
        /// A system specific string indicating the source of the servicing comment.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString ServicingCommentSourceDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ServicingCommentSourceDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServicingCommentSourceDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServicingCommentSourceDescriptionSpecified
        {
            get { return ServicingCommentSourceDescription != null; }
            set { }
        }

        /// <summary>
        /// The text of the servicing comment.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString ServicingCommentText;

        /// <summary>
        /// Gets or sets a value indicating whether the ServicingCommentText element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServicingCommentText element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServicingCommentTextSpecified
        {
            get { return ServicingCommentText != null; }
            set { }
        }

        /// <summary>
        /// Indicates the type of servicing comment.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOEnum<ServicingCommentBase> ServicingCommentType;

        /// <summary>
        /// Gets or sets a value indicating whether the ServicingCommentType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServicingCommentType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServicingCommentTypeSpecified
        {
            get { return this.ServicingCommentType != null && this.ServicingCommentType.enumValue != ServicingCommentBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture the Servicing Comment Type when Other is selected.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString ServicingCommentTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ServicingCommentTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServicingCommentTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServicingCommentTypeOtherDescriptionSpecified
        {
            get { return ServicingCommentTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 5)]
        public SERVICING_COMMENT_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
