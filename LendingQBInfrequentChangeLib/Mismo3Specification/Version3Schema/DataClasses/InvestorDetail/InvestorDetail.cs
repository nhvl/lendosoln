namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// The investor's original subscription amount in the security.
    /// </summary>
    public class INVESTOR_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether this INVESTOR_DETAIL container should be serialized.
        /// </summary>
        /// <returns>A boolean: whether this container should be serialized.</returns>
        public bool ShouldSerialize()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The investor's original subscription amount in the security.
        /// </summary>
        [XmlElement("SecurityOriginalSubscriptionAmount", Order = 0)]
        public MISMOAmount SecurityOriginalSubscriptionAmount;

        [XmlIgnore]
        public bool SecurityOriginalSubscriptionAmountSpecified
        {
            get { return SecurityOriginalSubscriptionAmount != null && !string.IsNullOrEmpty(SecurityOriginalSubscriptionAmount.Value); }
        }
    }
}
