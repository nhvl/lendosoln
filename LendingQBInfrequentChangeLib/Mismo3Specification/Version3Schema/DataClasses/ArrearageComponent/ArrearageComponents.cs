namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class ARREARAGE_COMPONENTS
    {
        /// <summary>
        /// Gets a value indicating whether the ARREARAGE_COMPONENTS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ArrearageComponentSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of arrearage components.
        /// </summary>
        [XmlElement("ARREARAGE_COMPONENT", Order = 0)]
		public List<ARREARAGE_COMPONENT> ArrearageComponent = new List<ARREARAGE_COMPONENT>();

        /// <summary>
        /// Gets or sets a value indicating whether the ArrearageComponent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ArrearageComponent element has been assigned a value.</value>
        [XmlIgnore]
        public bool ArrearageComponentSpecified
        {
            get { return this.ArrearageComponent != null && this.ArrearageComponent.Count(a => a != null && a.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public ARREARAGE_COMPONENTS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
