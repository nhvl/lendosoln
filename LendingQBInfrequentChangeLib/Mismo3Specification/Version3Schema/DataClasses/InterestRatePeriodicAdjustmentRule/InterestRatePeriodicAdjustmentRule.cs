namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class INTEREST_RATE_PERIODIC_ADJUSTMENT_RULE
    {
        /// <summary>
        /// Gets a value indicating whether the INTEREST_RATE_PERIODIC_ADJUSTMENT_RULE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.PeriodicBaseRatePercentSpecified
                    || this.PeriodicEffectiveDateSpecified
                    || this.PeriodicMaximumDecreaseRatePercentSpecified
                    || this.PeriodicMaximumIncreaseRatePercentSpecified
                    || this.PeriodicNextBaseRateSelectionDateSpecified
                    || this.PeriodicPaymentsBetweenBaseRatesCountSpecified;
            }
        }

        /// <summary>
        /// The base rate used to determine the periodic interest rate caps. All calculated interest rates are compared to this base rate to determine whether a limit has been reached for the interim period.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOPercent PeriodicBaseRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the PeriodicBaseRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PeriodicBaseRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool PeriodicBaseRatePercentSpecified
        {
            get { return PeriodicBaseRatePercent != null; }
            set { }
        }

        /// <summary>
        /// The payment due date when the periodic adjustment parameters begin.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMODate PeriodicEffectiveDate;

        /// <summary>
        /// Gets or sets a value indicating whether the PeriodicEffectiveDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PeriodicEffectiveDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool PeriodicEffectiveDateSpecified
        {
            get { return PeriodicEffectiveDate != null; }
            set { }
        }

        /// <summary>
        /// The maximum number of percentage points by which the interest rate can decrease from the Periodic Base Rate until the next base rate selection date.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOPercent PeriodicMaximumDecreaseRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the PeriodicMaximumDecreaseRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PeriodicMaximumDecreaseRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool PeriodicMaximumDecreaseRatePercentSpecified
        {
            get { return PeriodicMaximumDecreaseRatePercent != null; }
            set { }
        }

        /// <summary>
        /// The maximum number of percentage points by which the interest rate can increase from the Periodic Base Rate until the next base rate selection date.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOPercent PeriodicMaximumIncreaseRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the PeriodicMaximumIncreaseRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PeriodicMaximumIncreaseRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool PeriodicMaximumIncreaseRatePercentSpecified
        {
            get { return PeriodicMaximumIncreaseRatePercent != null; }
            set { }
        }

        /// <summary>
        /// The date on which a new base rate is to be determined for future periodic interest rate calculations.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMODate PeriodicNextBaseRateSelectionDate;

        /// <summary>
        /// Gets or sets a value indicating whether the PeriodicNextBaseRateSelectionDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PeriodicNextBaseRateSelectionDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool PeriodicNextBaseRateSelectionDateSpecified
        {
            get { return PeriodicNextBaseRateSelectionDate != null; }
            set { }
        }

        /// <summary>
        /// The number of payments between interest rate changes for which the periodic cap is in effect. At the end of the period a new base rate is established.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOCount PeriodicPaymentsBetweenBaseRatesCount;

        /// <summary>
        /// Gets or sets a value indicating whether the PeriodicPaymentsBetweenBaseRatesCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PeriodicPaymentsBetweenBaseRatesCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PeriodicPaymentsBetweenBaseRatesCountSpecified
        {
            get { return PeriodicPaymentsBetweenBaseRatesCount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 6)]
        public INTEREST_RATE_PERIODIC_ADJUSTMENT_RULE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
