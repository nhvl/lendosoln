namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class INTEREST_RATE_PERIODIC_ADJUSTMENT_RULES
    {
        /// <summary>
        /// Gets a value indicating whether the INTEREST_RATE_PERIODIC_ADJUSTMENT_RULES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.InterestRatePeriodicAdjustmentRuleSpecified;
            }
        }

        /// <summary>
        /// A collection of interest rate periodic adjustment rules.
        /// </summary>
        [XmlElement("INTEREST_RATE_PERIODIC_ADJUSTMENT_RULE", Order = 0)]
		public List<INTEREST_RATE_PERIODIC_ADJUSTMENT_RULE> InterestRatePeriodicAdjustmentRule = new List<INTEREST_RATE_PERIODIC_ADJUSTMENT_RULE>();

        /// <summary>
        /// Gets or sets a value indicating whether the InterestRatePeriodicAdjustmentRule element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InterestRatePeriodicAdjustmentRule element has been assigned a value.</value>
        [XmlIgnore]
        public bool InterestRatePeriodicAdjustmentRuleSpecified
        {
            get { return this.InterestRatePeriodicAdjustmentRule != null && this.InterestRatePeriodicAdjustmentRule.Count(i => i != null && i.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public INTEREST_RATE_PERIODIC_ADJUSTMENT_RULES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
