namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class INVESTOR_FEATURE
    {
        /// <summary>
        /// Gets a value indicating whether the INVESTOR_FEATURE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.InvestorFeatureCategoryNameSpecified
                    || this.InvestorFeatureDescriptionSpecified
                    || this.InvestorFeatureIdentifierSpecified
                    || this.InvestorFeatureNameSpecified;
            }
        }

        /// <summary>
        /// An investor-specified name for a collection of features.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString InvestorFeatureCategoryName;

        /// <summary>
        /// Gets or sets a value indicating whether the InvestorFeatureCategoryName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InvestorFeatureCategoryName element has been assigned a value.</value>
        [XmlIgnore]
        public bool InvestorFeatureCategoryNameSpecified
        {
            get { return InvestorFeatureCategoryName != null; }
            set { }
        }

        /// <summary>
        /// A text description of a loan feature not defined by other attributes.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString InvestorFeatureDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the InvestorFeatureDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InvestorFeatureDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool InvestorFeatureDescriptionSpecified
        {
            get { return InvestorFeatureDescription != null; }
            set { }
        }

        /// <summary>
        /// An investor-specified identifier used to identify a loan feature not defined by other attributes.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOIdentifier InvestorFeatureIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the InvestorFeatureIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InvestorFeatureIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool InvestorFeatureIdentifierSpecified
        {
            get { return InvestorFeatureIdentifier != null; }
            set { }
        }

        /// <summary>
        /// An investor-specified name used to describe a loan feature not defined by other attributes.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString InvestorFeatureName;

        /// <summary>
        /// Gets or sets a value indicating whether the InvestorFeatureName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InvestorFeatureName element has been assigned a value.</value>
        [XmlIgnore]
        public bool InvestorFeatureNameSpecified
        {
            get { return InvestorFeatureName != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public INVESTOR_FEATURE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
