namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class INVESTOR_FEATURES
    {
        /// <summary>
        /// Gets a value indicating whether the INVESTOR_FEATURES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.InvestorFeatureSpecified;
            }
        }

        /// <summary>
        /// A collection of investor features.
        /// </summary>
        [XmlElement("INVESTOR_FEATURE", Order = 0)]
		public List<INVESTOR_FEATURE> InvestorFeature = new List<INVESTOR_FEATURE>();

        /// <summary>
        /// Gets or sets a value indicating whether the InvestorFeature element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InvestorFeature element has been assigned a value.</value>
        [XmlIgnore]
        public bool InvestorFeatureSpecified
        {
            get { return this.InvestorFeature != null && this.InvestorFeature.Count(i => i != null && i.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public INVESTOR_FEATURES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
