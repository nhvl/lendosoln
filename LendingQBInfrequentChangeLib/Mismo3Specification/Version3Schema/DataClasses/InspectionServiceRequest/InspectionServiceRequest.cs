namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class INSPECTION_SERVICE_REQUEST
    {
        /// <summary>
        /// Gets a value indicating whether the INSPECTION_SERVICE_REQUEST container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.InspectionServiceRequestDetailSpecified
                    || this.PropertiesSpecified;
            }
        }

        /// <summary>
        /// Details on an inspection service request.
        /// </summary>
        [XmlElement("INSPECTION_SERVICE_REQUEST_DETAIL", Order = 0)]
        public INSPECTION_SERVICE_REQUEST_DETAIL InspectionServiceRequestDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the InspectionServiceRequestDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InspectionServiceRequestDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool InspectionServiceRequestDetailSpecified
        {
            get { return this.InspectionServiceRequestDetail != null && this.InspectionServiceRequestDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A list of properties.
        /// </summary>
        [XmlElement("PROPERTIES", Order = 1)]
        public PROPERTIES Properties;

        /// <summary>
        /// Gets or sets a value indicating whether the Properties element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Properties element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertiesSpecified
        {
            get { return this.Properties != null && this.Properties.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public INSPECTION_SERVICE_REQUEST_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
