namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class INSPECTION_SERVICE_REQUEST_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the INSPECTION_SERVICE_REQUEST_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.InspectionReportFormatDescriptionSpecified
                    || this.InspectionRequestActionTypeOtherDescriptionSpecified
                    || this.InspectionRequestActionTypeSpecified
                    || this.InspectorFileIdentifierSpecified
                    || this.OrderingSystemNameSpecified;
            }
        }

        /// <summary>
        /// A free-form description of the inspection report format and content.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString InspectionReportFormatDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the InspectionReportFormatDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InspectionReportFormatDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool InspectionReportFormatDescriptionSpecified
        {
            get { return InspectionReportFormatDescription != null; }
            set { }
        }

        /// <summary>
        /// Defines the inspection service requested.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<InspectionRequestActionBase> InspectionRequestActionType;

        /// <summary>
        /// Gets or sets a value indicating whether the InspectionRequestActionType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InspectionRequestActionType element has been assigned a value.</value>
        [XmlIgnore]
        public bool InspectionRequestActionTypeSpecified
        {
            get { return this.InspectionRequestActionType != null && this.InspectionRequestActionType.enumValue != InspectionRequestActionBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the request action type if Other is selected as the Inspection Request Action Type.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString InspectionRequestActionTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the InspectionRequestActionTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InspectionRequestActionTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool InspectionRequestActionTypeOtherDescriptionSpecified
        {
            get { return InspectionRequestActionTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// An identifier or number used by the inspecting professional to identify the report.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOIdentifier InspectorFileIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the InspectorFileIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InspectorFileIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool InspectorFileIdentifierSpecified
        {
            get { return InspectorFileIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The name of the system that originated the order.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString OrderingSystemName;

        /// <summary>
        /// Gets or sets a value indicating whether the OrderingSystemName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OrderingSystemName element has been assigned a value.</value>
        [XmlIgnore]
        public bool OrderingSystemNameSpecified
        {
            get { return OrderingSystemName != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 5)]
        public INSPECTION_SERVICE_REQUEST_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
