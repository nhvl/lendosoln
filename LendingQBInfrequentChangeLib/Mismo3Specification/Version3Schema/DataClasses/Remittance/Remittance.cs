namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class REMITTANCE
    {
        /// <summary>
        /// Gets a value indicating whether the REMITTANCE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AgentPortionAmountSpecified
                    || this.BasisOfInsuranceDiscountAmountSpecified
                    || this.ExtensionSpecified
                    || this.GrossPremiumAmountSpecified
                    || this.InsuredNameSpecified
                    || this.NAICTitlePolicyClassificationTypeSpecified
                    || this.OtherBasisOfInsuranceDiscountAmountSpecified
                    || this.PriorPolicyEffectiveDateSpecified
                    || this.PriorPolicyIdentifierSpecified
                    || this.RemittancePolicyDescriptionSpecified
                    || this.RemittancePolicyEffectiveDateSpecified
                    || this.RemittancePolicyPrefixCodeSpecified
                    || this.RemittancePolicySuffixIdentifierSpecified
                    || this.RemittanceRecordTransactionCodeDescriptionSpecified
                    || this.RemittanceRecordTransactionCodeSpecified
                    || this.RemittanceRecordTypeOtherDescriptionSpecified
                    || this.RemittanceRecordTypeSpecified
                    || this.RemittanceTitleInsuranceAmountSpecified
                    || this.UnderwriterPortionAmountSpecified;
            }
        }

        /// <summary>
        /// The dollar amount of the gross premium that is retained by the agent issuing policy.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount AgentPortionAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the AgentPortionAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AgentPortionAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool AgentPortionAmountSpecified
        {
            get { return AgentPortionAmount != null; }
            set { }
        }

        /// <summary>
        /// The amount that is the basis for discounting the policy, if prior policy, loan payoff amount, or a simultaneous policy exists. Should a loan policy exceed the owners insurance amount to determine if an additional premium is needed.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOAmount BasisOfInsuranceDiscountAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the BasisOfInsuranceDiscountAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BasisOfInsuranceDiscountAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool BasisOfInsuranceDiscountAmountSpecified
        {
            get { return BasisOfInsuranceDiscountAmount != null; }
            set { }
        }

        /// <summary>
        /// The total dollar amount collected for the policy, fee, or endorsements being issued or remitted.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOAmount GrossPremiumAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the GrossPremiumAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GrossPremiumAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool GrossPremiumAmountSpecified
        {
            get { return GrossPremiumAmount != null; }
            set { }
        }

        /// <summary>
        /// Identifies the individual or corporation who will be named as the insured party on the policy.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString InsuredName;

        /// <summary>
        /// Gets or sets a value indicating whether the InsuredName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InsuredName element has been assigned a value.</value>
        [XmlIgnore]
        public bool InsuredNameSpecified
        {
            get { return InsuredName != null; }
            set { }
        }

        /// <summary>
        /// NAIC-defined classification for policies insuring title to real property as either residential or nonresidential. 
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<NAICTitlePolicyClassificationBase> NAICTitlePolicyClassificationType;

        /// <summary>
        /// Gets or sets a value indicating whether the NAICTitlePolicyClassificationType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NAICTitlePolicyClassificationType element has been assigned a value.</value>
        [XmlIgnore]
        public bool NAICTitlePolicyClassificationTypeSpecified
        {
            get { return this.NAICTitlePolicyClassificationType != null && this.NAICTitlePolicyClassificationType.enumValue != NAICTitlePolicyClassificationBase.Blank; }
            set { }
        }

        /// <summary>
        /// The amount that is used for discounting the policy, should there be an additional basis for discounting the policy based on a simultaneous policy.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOAmount OtherBasisOfInsuranceDiscountAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the OtherBasisOfInsuranceDiscountAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OtherBasisOfInsuranceDiscountAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool OtherBasisOfInsuranceDiscountAmountSpecified
        {
            get { return OtherBasisOfInsuranceDiscountAmount != null; }
            set { }
        }

        /// <summary>
        /// The date that the title insurance policy coverage begins / becomes effective of the prior policy.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMODate PriorPolicyEffectiveDate;

        /// <summary>
        /// Gets or sets a value indicating whether the PriorPolicyEffectiveDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PriorPolicyEffectiveDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool PriorPolicyEffectiveDateSpecified
        {
            get { return PriorPolicyEffectiveDate != null; }
            set { }
        }

        /// <summary>
        /// This is typically referred to as the "Prior Policy Number" associated with the policy which is also referred to as the Jacket Number of the previous transaction should this be a "ReIssue".
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOIdentifier PriorPolicyIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the PriorPolicyIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PriorPolicyIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool PriorPolicyIdentifierSpecified
        {
            get { return PriorPolicyIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The description of the remittance policy product being reported.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOString RemittancePolicyDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the RemittancePolicyDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RemittancePolicyDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool RemittancePolicyDescriptionSpecified
        {
            get { return RemittancePolicyDescription != null; }
            set { }
        }

        /// <summary>
        /// The date that the title insurance policy coverage begins or becomes effective for the remittance record.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMODate RemittancePolicyEffectiveDate;

        /// <summary>
        /// Gets or sets a value indicating whether the RemittancePolicyEffectiveDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RemittancePolicyEffectiveDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool RemittancePolicyEffectiveDateSpecified
        {
            get { return RemittancePolicyEffectiveDate != null; }
            set { }
        }

        /// <summary>
        /// This is typically referred to as the Policy Number associated with the policy which is also referred to as the Jacket Number. This number is in two parts this data element is for the Prefix part of that number which is commonly used as the form identification (ex. OP - Owners Policy, MP - Mortgage Policy, etc).
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOCode RemittancePolicyPrefixCode;

        /// <summary>
        /// Gets or sets a value indicating whether the RemittancePolicyPrefixCode element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RemittancePolicyPrefixCode element has been assigned a value.</value>
        [XmlIgnore]
        public bool RemittancePolicyPrefixCodeSpecified
        {
            get { return RemittancePolicyPrefixCode != null; }
            set { }
        }

        /// <summary>
        /// This is typically referred to as the Policy Number associated with the policy which is also referred to as the Jacket Number. This number is in two parts this data element is for the Suffix part of that number which is commonly used as the serial number / unique identifier to that policy.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOIdentifier RemittancePolicySuffixIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the RemittancePolicySuffixIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RemittancePolicySuffixIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool RemittancePolicySuffixIdentifierSpecified
        {
            get { return RemittancePolicySuffixIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Remittance record transaction code is the code used in calculation to further qualify the rate being used in the calculation. The party assigning the code can be provided using the CodeOwnerURI associated with a MISMOCode.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOCode RemittanceRecordTransactionCode;

        /// <summary>
        /// Gets or sets a value indicating whether the RemittanceRecordTransactionCode element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RemittanceRecordTransactionCode element has been assigned a value.</value>
        [XmlIgnore]
        public bool RemittanceRecordTransactionCodeSpecified
        {
            get { return RemittanceRecordTransactionCode != null; }
            set { }
        }

        /// <summary>
        ///  A description of the remittance record transaction code.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOString RemittanceRecordTransactionCodeDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the RemittanceRecordTransactionCodeDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RemittanceRecordTransactionCodeDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool RemittanceRecordTransactionCodeDescriptionSpecified
        {
            get { return RemittanceRecordTransactionCodeDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies a type of remittance record.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOEnum<RemittanceRecordBase> RemittanceRecordType;

        /// <summary>
        /// Gets or sets a value indicating whether the RemittanceRecordType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RemittanceRecordType element has been assigned a value.</value>
        [XmlIgnore]
        public bool RemittanceRecordTypeSpecified
        {
            get { return this.RemittanceRecordType != null && this.RemittanceRecordType.enumValue != RemittanceRecordBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the remittance record type if Other is selected as the Remittance Record Type.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOString RemittanceRecordTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the RemittanceRecordTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RemittanceRecordTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool RemittanceRecordTypeOtherDescriptionSpecified
        {
            get { return RemittanceRecordTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of the policy coverage for the referenced remittance record if there is coverage (e.g. Loan Amount for loan policy, sales price for owners policy, none for Endorsements).
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMOAmount RemittanceTitleInsuranceAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the RemittanceTitleInsuranceAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RemittanceTitleInsuranceAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool RemittanceTitleInsuranceAmountSpecified
        {
            get { return RemittanceTitleInsuranceAmount != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of the gross premium that is attributed to the underwriter.
        /// </summary>
        [XmlElement(Order = 17)]
        public MISMOAmount UnderwriterPortionAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the UnderwriterPortionAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the UnderwriterPortionAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool UnderwriterPortionAmountSpecified
        {
            get { return UnderwriterPortionAmount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 18)]
        public REMITTANCE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
