namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class REMITTANCES
    {
        /// <summary>
        /// Gets a value indicating whether the REMITTANCES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.RemittanceSpecified;
            }
        }

        /// <summary>
        /// A collection of remittances.
        /// </summary>
        [XmlElement("REMITTANCE", Order = 0)]
		public List<REMITTANCE> Remittance = new List<REMITTANCE>();

        /// <summary>
        /// Gets or sets a value indicating whether the Remittance element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Remittance element has been assigned a value.</value>
        [XmlIgnore]
        public bool RemittanceSpecified
        {
            get { return this.Remittance != null && this.Remittance.Count(r => r != null && r.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public REMITTANCES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
