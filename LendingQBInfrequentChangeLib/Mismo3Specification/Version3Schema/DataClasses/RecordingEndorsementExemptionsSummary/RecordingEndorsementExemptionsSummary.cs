namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class RECORDING_ENDORSEMENT_EXEMPTIONS_SUMMARY
    {
        /// <summary>
        /// Gets a value indicating whether the RECORDING_ENDORSEMENT_EXEMPTIONS_SUMMARY container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.RecordingEndorsementExemptionsTotalAmountSpecified;
            }
        }

        /// <summary>
        /// The total amount of all fees exempted from remittance to the county recorder for recording a single document.  (Depending on state laws and local ordinances there may not be a direct relationship between Recording Endorsement Exemptions and RESPA Fees.).
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount RecordingEndorsementExemptionsTotalAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the RecordingEndorsementExemptionsTotalAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RecordingEndorsementExemptionsTotalAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool RecordingEndorsementExemptionsTotalAmountSpecified
        {
            get { return RecordingEndorsementExemptionsTotalAmount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public RECORDING_ENDORSEMENT_EXEMPTIONS_SUMMARY_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
