namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class PRINCIPAL_AND_INTEREST_PAYMENT_PERIODIC_ADJUSTMENT_RULE
    {
        /// <summary>
        /// Gets a value indicating whether the PRINCIPAL_AND_INTEREST_PAYMENT_PERIODIC_ADJUSTMENT_RULE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AdjustmentRuleTypeSpecified
                    || this.ExtensionSpecified
                    || this.PeriodicEffectiveDateSpecified
                    || this.PeriodicEffectiveMonthsCountSpecified
                    || this.PrincipalAndInterestPaymentAnnualCapAmountSpecified
                    || this.PrincipalAndInterestRecastMonthsCountSpecified;
            }
        }

        /// <summary>
        /// Specifies whether the occurrence of the adjustment is the first change or a subsequent change.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<AdjustmentRuleBase> AdjustmentRuleType;

        /// <summary>
        /// Gets or sets a value indicating whether the AdjustmentRuleType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AdjustmentRuleType element has been assigned a value.</value>
        [XmlIgnore]
        public bool AdjustmentRuleTypeSpecified
        {
            get { return this.AdjustmentRuleType != null && this.AdjustmentRuleType.enumValue != AdjustmentRuleBase.Blank; }
            set { }
        }

        /// <summary>
        /// The payment due date when the periodic adjustment parameters begin.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMODate PeriodicEffectiveDate;

        /// <summary>
        /// Gets or sets a value indicating whether the PeriodicEffectiveDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PeriodicEffectiveDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool PeriodicEffectiveDateSpecified
        {
            get { return PeriodicEffectiveDate != null; }
            set { }
        }

        /// <summary>
        /// The period of time, expressed in months, that the Adjustment Rule is in effect.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOCount PeriodicEffectiveMonthsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the PeriodicEffectiveMonthsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PeriodicEffectiveMonthsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PeriodicEffectiveMonthsCountSpecified
        {
            get { return PeriodicEffectiveMonthsCount != null; }
            set { }
        }

        /// <summary>
        /// The maximum dollar amount by which the principal and interest payment can increase in one year.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOAmount PrincipalAndInterestPaymentAnnualCapAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PrincipalAndInterestPaymentAnnualCapAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrincipalAndInterestPaymentAnnualCapAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrincipalAndInterestPaymentAnnualCapAmountSpecified
        {
            get { return PrincipalAndInterestPaymentAnnualCapAmount != null; }
            set { }
        }

        /// <summary>
        /// The frequency, expressed as a number of months, that defines when a payment recalculation occurs for a negatively amortized loan. The recast occurs in order for the loan to become fully amortizing.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOCount PrincipalAndInterestRecastMonthsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the PrincipalAndInterestRecastMonthsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrincipalAndInterestRecastMonthsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrincipalAndInterestRecastMonthsCountSpecified
        {
            get { return PrincipalAndInterestRecastMonthsCount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 5)]
        public PRINCIPAL_AND_INTEREST_PAYMENT_PERIODIC_ADJUSTMENT_RULE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
