namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PRINCIPAL_AND_INTEREST_PAYMENT_PERIODIC_ADJUSTMENT_RULES
    {
        /// <summary>
        /// Gets a value indicating whether the PRINCIPAL_AND_INTEREST_PAYMENT_PERIODIC_ADJUSTMENT_RULES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.PrincipalAndInterestPaymentPeriodicAdjustmentRuleSpecified;
            }
        }

        /// <summary>
        /// A collection of principal and interest payment periodic adjustment rules.
        /// </summary>
        [XmlElement("PRINCIPAL_AND_INTEREST_PAYMENT_PERIODIC_ADJUSTMENT_RULE", Order = 0)]
		public List<PRINCIPAL_AND_INTEREST_PAYMENT_PERIODIC_ADJUSTMENT_RULE> PrincipalAndInterestPaymentPeriodicAdjustmentRule = new List<PRINCIPAL_AND_INTEREST_PAYMENT_PERIODIC_ADJUSTMENT_RULE>();

        /// <summary>
        /// Gets or sets a value indicating whether the PrincipalAndInterestPaymentPeriodicAdjustmentRule element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrincipalAndInterestPaymentPeriodicAdjustmentRule element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrincipalAndInterestPaymentPeriodicAdjustmentRuleSpecified
        {
            get { return this.PrincipalAndInterestPaymentPeriodicAdjustmentRule != null && this.PrincipalAndInterestPaymentPeriodicAdjustmentRule.Count(p => p != null && p.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public PRINCIPAL_AND_INTEREST_PAYMENT_PERIODIC_ADJUSTMENT_RULES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
