namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class BATH_EQUIPMENTS
    {
        /// <summary>
        /// Gets a value indicating whether the BATH_EQUIPMENTS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BathEquipmentSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of bath equipment.
        /// </summary>
        [XmlElement("BATH_EQUIPMENT", Order = 0)]
		public List<BATH_EQUIPMENT> BathEquipment = new List<BATH_EQUIPMENT>();

        /// <summary>
        /// Gets or sets a value indicating whether the BathEquipment element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BathEquipment element has been assigned a value.</value>
        [XmlIgnore]
        public bool BathEquipmentSpecified
        {
            get { return this.BathEquipment != null && this.BathEquipment.Count(b => b != null && b.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public BATH_EQUIPMENTS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
