namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class BATH_EQUIPMENT
    {
        /// <summary>
        /// Gets a value indicating whether the BATH_EQUIPMENT container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BathEquipmentCountSpecified
                    || this.BathEquipmentDescriptionSpecified
                    || this.BathEquipmentTypeOtherDescriptionSpecified
                    || this.BathEquipmentTypeSpecified
                    || this.ComponentAdjustmentAmountSpecified
                    || this.ComponentClassificationTypeSpecified
                    || this.ConditionRatingDescriptionSpecified
                    || this.ConditionRatingTypeSpecified
                    || this.ExtensionSpecified
                    || this.QualityRatingDescriptionSpecified
                    || this.QualityRatingTypeSpecified;
            }
        }

        /// <summary>
        /// Holds the number of bath equipment or fixtures specified by Bath Equipment Type.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOCount BathEquipmentCount;

        /// <summary>
        /// Gets or sets a value indicating whether the BathEquipmentCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BathEquipmentCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool BathEquipmentCountSpecified
        {
            get { return BathEquipmentCount != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe in detail the identified bath equipment.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString BathEquipmentDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the BathEquipmentDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BathEquipmentDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool BathEquipmentDescriptionSpecified
        {
            get { return BathEquipmentDescription != null; }
            set { }
        }

        /// <summary>
        /// A collection of values that specify the bath equipment normally found in a living unit or residence.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<BathEquipmentBase> BathEquipmentType;

        /// <summary>
        /// Gets or sets a value indicating whether the BathEquipmentType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BathEquipmentType element has been assigned a value.</value>
        [XmlIgnore]
        public bool BathEquipmentTypeSpecified
        {
            get { return this.BathEquipmentType != null && this.BathEquipmentType.enumValue != BathEquipmentBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to provide additional information when Other is selected as the Bath Equipment Type.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString BathEquipmentTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the BathEquipmentTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BathEquipmentTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool BathEquipmentTypeOtherDescriptionSpecified
        {
            get { return BathEquipmentTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount (either positive or negative) adjustment being made for a specific component of the property.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOAmount ComponentAdjustmentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ComponentAdjustmentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ComponentAdjustmentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ComponentAdjustmentAmountSpecified
        {
            get { return ComponentAdjustmentAmount != null; }
            set { }
        }

        /// <summary>
        /// Specifies whether the component is considered real or personal property.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOEnum<ComponentClassificationBase> ComponentClassificationType;

        /// <summary>
        /// Gets or sets a value indicating whether the ComponentClassificationType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ComponentClassificationType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ComponentClassificationTypeSpecified
        {
            get { return this.ComponentClassificationType != null && this.ComponentClassificationType.enumValue != ComponentClassificationBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe in detail the condition rating of the identified component.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOString ConditionRatingDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ConditionRatingDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConditionRatingDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConditionRatingDescriptionSpecified
        {
            get { return ConditionRatingDescription != null; }
            set { }
        }

        /// <summary>
        /// A rating of the condition of the identified component.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOEnum<ConditionRatingBase> ConditionRatingType;

        /// <summary>
        /// Gets or sets a value indicating whether the ConditionRatingType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConditionRatingType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConditionRatingTypeSpecified
        {
            get { return this.ConditionRatingType != null && this.ConditionRatingType.enumValue != ConditionRatingBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe in detail the quality rating of the identified component.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOString QualityRatingDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the QualityRatingDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the QualityRatingDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool QualityRatingDescriptionSpecified
        {
            get { return QualityRatingDescription != null; }
            set { }
        }

        /// <summary>
        /// A rating of the quality of the identified component type.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOEnum<QualityRatingBase> QualityRatingType;

        /// <summary>
        /// Gets or sets a value indicating whether the QualityRatingType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the QualityRatingType element has been assigned a value.</value>
        [XmlIgnore]
        public bool QualityRatingTypeSpecified
        {
            get { return this.QualityRatingType != null && this.QualityRatingType.enumValue != QualityRatingBase.Blank; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 10)]
        public BATH_EQUIPMENT_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
