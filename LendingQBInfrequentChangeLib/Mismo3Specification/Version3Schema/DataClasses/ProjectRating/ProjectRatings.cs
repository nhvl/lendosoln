namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PROJECT_RATINGS
    {
        /// <summary>
        /// Gets a value indicating whether the PROJECT_RATINGS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ProjectRatingSpecified;
            }
        }

        /// <summary>
        /// A collection of project ratings.
        /// </summary>
        [XmlElement("PROJECT_RATING", Order = 0)]
		public List<PROJECT_RATING> ProjectRating = new List<PROJECT_RATING>();

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectRating element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectRating element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectRatingSpecified
        {
            get { return this.ProjectRating != null && this.ProjectRating.Count(p => p != null && p.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public PROJECT_RATINGS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
