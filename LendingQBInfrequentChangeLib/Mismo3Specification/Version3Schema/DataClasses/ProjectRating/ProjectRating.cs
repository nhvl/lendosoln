namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class PROJECT_RATING
    {
        /// <summary>
        /// Gets a value indicating whether the PROJECT_RATING container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ConditionRatingTypeSpecified
                    || this.ExtensionSpecified
                    || this.ProjectAspectTypeOtherDescriptionSpecified
                    || this.ProjectAspectTypeSpecified
                    || this.ProjectRatingCommentDescriptionSpecified;
            }
        }

        /// <summary>
        /// A rating of the condition of the identified component.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<ConditionRatingBase> ConditionRatingType;

        /// <summary>
        /// Gets or sets a value indicating whether the ConditionRatingType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConditionRatingType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConditionRatingTypeSpecified
        {
            get { return this.ConditionRatingType != null && this.ConditionRatingType.enumValue != ConditionRatingBase.Blank; }
            set { }
        }

        /// <summary>
        /// Specifies an aspect of the project, PUD or mobile home park that will be rated using Rating Condition Type.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<ProjectAspectBase> ProjectAspectType;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectAspectType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectAspectType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectAspectTypeSpecified
        {
            get { return this.ProjectAspectType != null && this.ProjectAspectType.enumValue != ProjectAspectBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the project aspect being rated if Other is selected as the Project Aspect Type.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString ProjectAspectTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectAspectTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectAspectTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectAspectTypeOtherDescriptionSpecified
        {
            get { return ProjectAspectTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to expand on the Rating Condition Type.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString ProjectRatingCommentDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectRatingCommentDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectRatingCommentDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectRatingCommentDescriptionSpecified
        {
            get { return ProjectRatingCommentDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public PROJECT_RATING_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
