namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PROPERTY_OWNER
    {
        /// <summary>
        /// Gets a value indicating whether the PROPERTY_OWNER container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.OwnershipPercentSpecified
                    || this.RelationshipVestingTypeOtherDescriptionSpecified
                    || this.RelationshipVestingTypeSpecified;
            }
        }

        /// <summary>
        /// Specifies the ownership percentage of the party in the property.  If this data is absent/undeclared it is assumed to be an equal, undivided interest.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOPercent OwnershipPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the OwnershipPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OwnershipPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool OwnershipPercentSpecified
        {
            get { return OwnershipPercent != null; }
            set { }
        }

        /// <summary>
        /// Specifies a type of property ownership by one or more parties. This may define the rights of the parties to sell their interest in the property to others, to will the property, or to sever joint ownership of the property.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<RelationshipVestingBase> RelationshipVestingType;

        /// <summary>
        /// Gets or sets a value indicating whether the RelationshipVestingType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RelationshipVestingType element has been assigned a value.</value>
        [XmlIgnore]
        public bool RelationshipVestingTypeSpecified
        {
            get { return this.RelationshipVestingType != null && this.RelationshipVestingType.enumValue != RelationshipVestingBase.Blank; }
            set { }
        }

        /// <summary>
        /// The description of the Relationship Vesting Type when Other is selected as the option from the enumerated list.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString RelationshipVestingTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the RelationshipVestingTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RelationshipVestingTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool RelationshipVestingTypeOtherDescriptionSpecified
        {
            get { return RelationshipVestingTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public PROPERTY_OWNER_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
