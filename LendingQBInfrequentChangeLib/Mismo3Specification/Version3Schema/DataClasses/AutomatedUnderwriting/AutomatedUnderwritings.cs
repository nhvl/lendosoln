namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class AUTOMATED_UNDERWRITINGS
    {
        /// <summary>
        /// Gets a value indicating whether the AUTOMATED_UNDERWRITINGS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AutomatedUnderwritingSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of automated underwriting cases.
        /// </summary>
        [XmlElement("AUTOMATED_UNDERWRITING", Order = 0)]
		public List<AUTOMATED_UNDERWRITING> AutomatedUnderwriting = new List<AUTOMATED_UNDERWRITING>();

        /// <summary>
        /// Gets or sets a value indicating whether the AutomatedUnderwriting element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AutomatedUnderwriting element has been assigned a value.</value>
        [XmlIgnore]
        public bool AutomatedUnderwritingSpecified
        {
            get { return this.AutomatedUnderwriting != null && this.AutomatedUnderwriting.Count(a => a != null && a.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public AUTOMATED_UNDERWRITINGS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
