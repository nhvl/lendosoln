namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class AUTOMATED_UNDERWRITING
    {
        /// <summary>
        /// Gets a value indicating whether the AUTOMATED_UNDERWRITING container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AutomatedUnderwritingCaseIdentifierSpecified
                    || this.AutomatedUnderwritingDecisionDatetimeSpecified
                    || this.AutomatedUnderwritingEvaluationStatusDescriptionSpecified
                    || this.AutomatedUnderwritingMethodVersionIdentifierSpecified
                    || this.AutomatedUnderwritingProcessDescriptionSpecified
                    || this.AutomatedUnderwritingRecommendationDescriptionSpecified
                    || this.AutomatedUnderwritingSystemDocumentWaiverIndicatorSpecified
                    || this.AutomatedUnderwritingSystemResultValueSpecified
                    || this.AutomatedUnderwritingSystemTypeOtherDescriptionSpecified
                    || this.AutomatedUnderwritingSystemTypeSpecified
                    || this.DesktopUnderwriterRecommendationTypeSpecified
                    || this.ExtensionSpecified
                    || this.FHAPreReviewResultsValueSpecified
                    || this.FREPurchaseEligibilityTypeSpecified
                    || this.LoanProspectorCreditRiskClassificationDescriptionSpecified
                    || this.LoanProspectorDocumentationClassificationDescriptionSpecified;
            }
        }

        /// <summary>
        /// A unique identifier assigned by the underwriting system to the underwriting case for a specific loan application.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIdentifier AutomatedUnderwritingCaseIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the AutomatedUnderwritingCaseIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AutomatedUnderwritingCaseIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool AutomatedUnderwritingCaseIdentifierSpecified
        {
            get { return AutomatedUnderwritingCaseIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The date and time of the underwriting decision.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMODatetime AutomatedUnderwritingDecisionDatetime;

        /// <summary>
        /// Gets or sets a value indicating whether the date/time element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the date/time element has been assigned a value.</value>
        [XmlIgnore]
        public bool AutomatedUnderwritingDecisionDatetimeSpecified
        {
            get { return AutomatedUnderwritingDecisionDatetime != null; }
            set { }
        }

        /// <summary>
        /// The status of the Automated Underwriting Certificate Request.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString AutomatedUnderwritingEvaluationStatusDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the AutomatedUnderwritingEvaluationStatusDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AutomatedUnderwritingEvaluationStatusDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool AutomatedUnderwritingEvaluationStatusDescriptionSpecified
        {
            get { return AutomatedUnderwritingEvaluationStatusDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the version of the automated underwriting system used to underwrite the loan.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOIdentifier AutomatedUnderwritingMethodVersionIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the AutomatedUnderwritingMethodVersionIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AutomatedUnderwritingMethodVersionIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool AutomatedUnderwritingMethodVersionIdentifierSpecified
        {
            get { return AutomatedUnderwritingMethodVersionIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A free-form text description of the automated underwriting process or procedure used on the loan.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString AutomatedUnderwritingProcessDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the AutomatedUnderwritingProcessDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AutomatedUnderwritingProcessDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool AutomatedUnderwritingProcessDescriptionSpecified
        {
            get { return AutomatedUnderwritingProcessDescription != null; }
            set { }
        }

        /// <summary>
        /// The loan approval recommendation determined by the Automated Underwriting system.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString AutomatedUnderwritingRecommendationDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the AutomatedUnderwritingRecommendationDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AutomatedUnderwritingRecommendationDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool AutomatedUnderwritingRecommendationDescriptionSpecified
        {
            get { return AutomatedUnderwritingRecommendationDescription != null; }
            set { }
        }

        /// <summary>
        /// When a returned AU system recommendation indicates that some or all of the loan information is not required to be formally documented in the loan submission package, e.g., no requirement to provide a wage statement, tax returns, etc.  A loan of this type would not be considered an Alt A product because the borrower is not applying for a limited/reduced documentation loan.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOIndicator AutomatedUnderwritingSystemDocumentWaiverIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the AutomatedUnderwritingSystemDocumentWaiverIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AutomatedUnderwritingSystemDocumentWaiverIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool AutomatedUnderwritingSystemDocumentWaiverIndicatorSpecified
        {
            get { return AutomatedUnderwritingSystemDocumentWaiverIndicator != null; }
            set { }
        }

        /// <summary>
        /// The result of the private automated underwriting evaluation system.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOValue AutomatedUnderwritingSystemResultValue;

        /// <summary>
        /// Gets or sets a value indicating whether the AutomatedUnderwritingSystemResultValue element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AutomatedUnderwritingSystemResultValue element has been assigned a value.</value>
        [XmlIgnore]
        public bool AutomatedUnderwritingSystemResultValueSpecified
        {
            get { return AutomatedUnderwritingSystemResultValue != null; }
            set { }
        }

        /// <summary>
        /// The type of automated underwriting system used to evaluate the loan.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOEnum<AutomatedUnderwritingSystemBase> AutomatedUnderwritingSystemType;

        /// <summary>
        /// Gets or sets a value indicating whether the AutomatedUnderwritingSystemType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AutomatedUnderwritingSystemType element has been assigned a value.</value>
        [XmlIgnore]
        public bool AutomatedUnderwritingSystemTypeSpecified
        {
            get { return this.AutomatedUnderwritingSystemType != null && this.AutomatedUnderwritingSystemType.enumValue != AutomatedUnderwritingSystemBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Automated Underwriting System Type.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOString AutomatedUnderwritingSystemTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the AutomatedUnderwritingSystemTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AutomatedUnderwritingSystemTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool AutomatedUnderwritingSystemTypeOtherDescriptionSpecified
        {
            get { return AutomatedUnderwritingSystemTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// To convey the DU recommendation.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOEnum<DesktopUnderwriterRecommendationBase> DesktopUnderwriterRecommendationType;

        /// <summary>
        /// Gets or sets a value indicating whether the DesktopUnderwriterRecommendationType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DesktopUnderwriterRecommendationType element has been assigned a value.</value>
        [XmlIgnore]
        public bool DesktopUnderwriterRecommendationTypeSpecified
        {
            get { return this.DesktopUnderwriterRecommendationType != null && this.DesktopUnderwriterRecommendationType.enumValue != DesktopUnderwriterRecommendationBase.Blank; }
            set { }
        }

        /// <summary>
        /// The credit risk decision issued by the FHA TOTAL Mortgage Scorecard based solely on the algorithm score. (Does not take into account review rules or credit overrides.).
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOString FHAPreReviewResultsValue;

        /// <summary>
        /// Gets or sets a value indicating whether the FHAPreReviewResultsValue element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FHAPreReviewResultsValue element has been assigned a value.</value>
        [XmlIgnore]
        public bool FHAPreReviewResultsValueSpecified
        {
            get { return FHAPreReviewResultsValue != null; }
            set { }
        }

        /// <summary>
        /// To convey the Freddie Mac purchase eligibility.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOEnum<FREPurchaseEligibilityBase> FREPurchaseEligibilityType;

        /// <summary>
        /// Gets or sets a value indicating whether the FREPurchaseEligibilityType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FREPurchaseEligibilityType element has been assigned a value.</value>
        [XmlIgnore]
        public bool FREPurchaseEligibilityTypeSpecified
        {
            get { return this.FREPurchaseEligibilityType != null && this.FREPurchaseEligibilityType.enumValue != FREPurchaseEligibilityBase.Blank; }
            set { }
        }

        /// <summary>
        /// The Loan Prospector credit risk classification for a loan.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOString LoanProspectorCreditRiskClassificationDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanProspectorCreditRiskClassificationDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanProspectorCreditRiskClassificationDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanProspectorCreditRiskClassificationDescriptionSpecified
        {
            get { return LoanProspectorCreditRiskClassificationDescription != null; }
            set { }
        }

        /// <summary>
        /// The Loan Prospector documentation classification for the loan.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOString LoanProspectorDocumentationClassificationDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanProspectorDocumentationClassificationDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanProspectorDocumentationClassificationDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanProspectorDocumentationClassificationDescriptionSpecified
        {
            get { return LoanProspectorDocumentationClassificationDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 15)]
        public AUTOMATED_UNDERWRITING_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
