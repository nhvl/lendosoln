namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class FEES_SUMMARY
    {
        /// <summary>
        /// Gets a value indicating whether the FEES_SUMMARY container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.FeeSummaryDetailSpecified
                    || this.FeeSummaryTotalFeesPaidBysSpecified
                    || this.FeeSummaryTotalFeesPaidTosSpecified;
            }
        }

        /// <summary>
        /// Details on the fee summary.
        /// </summary>
        [XmlElement("FEE_SUMMARY_DETAIL", Order = 0)]
        public FEE_SUMMARY_DETAIL FeeSummaryDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the FeeSummaryDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeeSummaryDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeeSummaryDetailSpecified
        {
            get { return this.FeeSummaryDetail != null && this.FeeSummaryDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A list of who fees were paid by.
        /// </summary>
        [XmlElement("FEE_SUMMARY_TOTAL_FEES_PAID_BYS", Order = 1)]
        public FEE_SUMMARY_TOTAL_FEES_PAID_BYS FeeSummaryTotalFeesPaidBys;

        /// <summary>
        /// Gets or sets a value indicating whether the FeeSummaryTotalFeesPaidBy element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeeSummaryTotalFeesPaidBy element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeeSummaryTotalFeesPaidBysSpecified
        {
            get { return this.FeeSummaryTotalFeesPaidBys != null && this.FeeSummaryTotalFeesPaidBys.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A list of who fees were paid to.
        /// </summary>
        [XmlElement("FEE_SUMMARY_TOTAL_FEES_PAID_TOS", Order = 2)]
        public FEE_SUMMARY_TOTAL_FEES_PAID_TOS FeeSummaryTotalFeesPaidTos;

        /// <summary>
        /// Gets or sets a value indicating whether the FeeSummaryTotalFeesPaidTo element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeeSummaryTotalFeesPaidTo element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeeSummaryTotalFeesPaidTosSpecified
        {
            get { return this.FeeSummaryTotalFeesPaidTos != null && this.FeeSummaryTotalFeesPaidTos.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public FEES_SUMMARY_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
