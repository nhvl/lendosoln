namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class FEE_SUMMARY_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the FEE_SUMMARY_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.APRPercentSpecified
                    || this.ExtensionSpecified
                    || this.FeeSummaryApproximateCostOfFundsPercentSpecified
                    || this.FeeSummaryDisclosedTotalSalesPriceAmountSpecified
                    || this.FeeSummaryTotalAmountFinancedAmountSpecified
                    || this.FeeSummaryTotalAPRFeesAmountSpecified
                    || this.FeeSummaryTotalDepositedReservesAmountSpecified
                    || this.FeeSummaryTotalFeesAmountSpecified
                    || this.FeeSummaryTotalFilingRecordingFeeAmountSpecified
                    || this.FeeSummaryTotalFinanceChargeAmountSpecified
                    || this.FeeSummaryTotalInterestPercentSpecified
                    || this.FeeSummaryTotalLoanOriginationFeesAmountSpecified
                    || this.FeeSummaryTotalNetBorrowerFeesAmountSpecified
                    || this.FeeSummaryTotalNetProceedsForFundingAmountSpecified
                    || this.FeeSummaryTotalNetSellerFeesAmountSpecified
                    || this.FeeSummaryTotalNonAPRFeesAmountSpecified
                    || this.FeeSummaryTotalOfAllPaymentsAmountSpecified
                    || this.FeeSummaryTotalPaidToOthersAmountSpecified
                    || this.FeeSummaryTotalPrepaidFinanceChargeAmountSpecified
                    || this.HOEPA_APRPercentSpecified;
            }
        }

        /// <summary>
        /// The annual percentage rate for the loan based on the accepted industry standard defined by Regulation Z (Section 12 C.F.R. 1026.18(e)) used for Qualified Mortgages and general disclosure purposes.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOPercent APRPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the APRPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the APRPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool APRPercentSpecified
        {
            get { return APRPercent != null; }
            set { }
        }

        /// <summary>
        /// The approximate cost of the funds used to make the loan transaction. Either the most recent ten-year Treasury constant maturity rate or the creditor's actual cost of borrowing the funds used to extend the credit, at the creditor's option.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOPercent FeeSummaryApproximateCostOfFundsPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the FeeSummaryApproximateCostOfFundsPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeeSummaryApproximateCostOfFundsPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeeSummaryApproximateCostOfFundsPercentSpecified
        {
            get { return FeeSummaryApproximateCostOfFundsPercent != null; }
            set { }
        }

        /// <summary>
        /// Total amount required as part of the TIL Disclosure document for REO sales. (Fifth box of the disclosure section.).
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOAmount FeeSummaryDisclosedTotalSalesPriceAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the FeeSummaryDisclosedTotalSalesPriceAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeeSummaryDisclosedTotalSalesPriceAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeeSummaryDisclosedTotalSalesPriceAmountSpecified
        {
            get { return FeeSummaryDisclosedTotalSalesPriceAmount != null; }
            set { }
        }

        /// <summary>
        /// The amount of the Total Amount Financed as disclosed based on the accepted industry standard defined by Regulation Z.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOAmount FeeSummaryTotalAmountFinancedAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the FeeSummaryTotalAmountFinancedAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeeSummaryTotalAmountFinancedAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeeSummaryTotalAmountFinancedAmountSpecified
        {
            get { return FeeSummaryTotalAmountFinancedAmount != null; }
            set { }
        }

        /// <summary>
        /// The total amount of all fees charged included in the APR calculation.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOAmount FeeSummaryTotalAPRFeesAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the FeeSummaryTotalAPRFeesAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeeSummaryTotalAPRFeesAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeeSummaryTotalAPRFeesAmountSpecified
        {
            get { return FeeSummaryTotalAPRFeesAmount != null; }
            set { }
        }

        /// <summary>
        /// The total amount of escrow reserves to be deposited. 
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOAmount FeeSummaryTotalDepositedReservesAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the FeeSummaryTotalDepositedReservesAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeeSummaryTotalDepositedReservesAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeeSummaryTotalDepositedReservesAmountSpecified
        {
            get { return FeeSummaryTotalDepositedReservesAmount != null; }
            set { }
        }

        /// <summary>
        /// The total dollar amount of fees that have been charged across all payers and payees.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOAmount FeeSummaryTotalFeesAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the FeeSummaryTotalFeesAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeeSummaryTotalFeesAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeeSummaryTotalFeesAmountSpecified
        {
            get { return FeeSummaryTotalFeesAmount != null; }
            set { }
        }

        /// <summary>
        /// The total amount of filing fees, or recording fees, as required to be disclosed on the TIL Disclosure document. 
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOAmount FeeSummaryTotalFilingRecordingFeeAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the FeeSummaryTotalFilingRecordingFeeAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeeSummaryTotalFilingRecordingFeeAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeeSummaryTotalFilingRecordingFeeAmountSpecified
        {
            get { return FeeSummaryTotalFilingRecordingFeeAmount != null; }
            set { }
        }

        /// <summary>
        /// The amount of the Total Finance Charges as disclosed based on the accepted industry standard defined by Regulation Z.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOAmount FeeSummaryTotalFinanceChargeAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the FeeSummaryTotalFinanceChargeAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeeSummaryTotalFinanceChargeAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeeSummaryTotalFinanceChargeAmountSpecified
        {
            get { return FeeSummaryTotalFinanceChargeAmount != null; }
            set { }
        }

        /// <summary>
        /// The total amount of interest paid by the borrower over the term of the loan as a percentage of the loan amount.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOPercent FeeSummaryTotalInterestPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the FeeSummaryTotalInterestPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeeSummaryTotalInterestPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeeSummaryTotalInterestPercentSpecified
        {
            get { return FeeSummaryTotalInterestPercent != null; }
            set { }
        }

        /// <summary>
        /// The total dollar amount of fees, for both the seller and the buyer, that have been charged on the loan at the time of origination. It is the total of the 800 series of lines on the HUD1.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOAmount FeeSummaryTotalLoanOriginationFeesAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the FeeSummaryTotalLoanOriginationFeesAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeeSummaryTotalLoanOriginationFeesAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeeSummaryTotalLoanOriginationFeesAmountSpecified
        {
            get { return FeeSummaryTotalLoanOriginationFeesAmount != null; }
            set { }
        }

        /// <summary>
        /// The total dollar amount of fees still due from the borrower after the deduction of amounts paid by or on behalf of the borrower toward fees charged to the borrower. 
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOAmount FeeSummaryTotalNetBorrowerFeesAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the FeeSummaryTotalNetBorrowerFeesAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeeSummaryTotalNetBorrowerFeesAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeeSummaryTotalNetBorrowerFeesAmountSpecified
        {
            get { return FeeSummaryTotalNetBorrowerFeesAmount != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of proceeds required for funding after subtracting lender defined fees or charges. 
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOAmount FeeSummaryTotalNetProceedsForFundingAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the FeeSummaryTotalNetProceedsForFundingAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeeSummaryTotalNetProceedsForFundingAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeeSummaryTotalNetProceedsForFundingAmountSpecified
        {
            get { return FeeSummaryTotalNetProceedsForFundingAmount != null; }
            set { }
        }

        /// <summary>
        /// The total dollar amount of fees still due from the seller after the deduction of amounts paid by or on behalf of the seller toward fees charged to the seller. 
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOAmount FeeSummaryTotalNetSellerFeesAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the FeeSummaryTotalNetSellerFeesAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeeSummaryTotalNetSellerFeesAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeeSummaryTotalNetSellerFeesAmountSpecified
        {
            get { return FeeSummaryTotalNetSellerFeesAmount != null; }
            set { }
        }

        /// <summary>
        /// The total amount of all fees charged not included in the APR calculation. 
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOAmount FeeSummaryTotalNonAPRFeesAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the FeeSummaryTotalNonAPRFeesAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeeSummaryTotalNonAPRFeesAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeeSummaryTotalNonAPRFeesAmountSpecified
        {
            get { return FeeSummaryTotalNonAPRFeesAmount != null; }
            set { }
        }

        /// <summary>
        /// The amount of the Total of All Payments as disclosed based on the accepted industry standard defined by Regulation Z.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOAmount FeeSummaryTotalOfAllPaymentsAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the FeeSummaryTotalOfAllPaymentsAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeeSummaryTotalOfAllPaymentsAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeeSummaryTotalOfAllPaymentsAmountSpecified
        {
            get { return FeeSummaryTotalOfAllPaymentsAmount != null; }
            set { }
        }

        /// <summary>
        /// The amount determined by Regulation Z as the Total Paid To Others as disclosed on the Itemization of Amount Financed. 
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMOAmount FeeSummaryTotalPaidToOthersAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the FeeSummaryTotalPaidToOthersAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeeSummaryTotalPaidToOthersAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeeSummaryTotalPaidToOthersAmountSpecified
        {
            get { return FeeSummaryTotalPaidToOthersAmount != null; }
            set { }
        }

        /// <summary>
        /// The total dollar amount of all fees considered as prepaid finance charges as defined by Regulation Z. 
        /// </summary>
        [XmlElement(Order = 17)]
        public MISMOAmount FeeSummaryTotalPrepaidFinanceChargeAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the FeeSummaryTotalPrepaidFinanceChargeAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeeSummaryTotalPrepaidFinanceChargeAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeeSummaryTotalPrepaidFinanceChargeAmountSpecified
        {
            get { return FeeSummaryTotalPrepaidFinanceChargeAmount != null; }
            set { }
        }

        /// <summary>
        /// The annual percentage rate for the loan calculated according to the high-cost mortgage requirements of Regulation Z (Section 12 C.F.R. 1026.32(a)(3)) used exclusively for HOEPA loans. 
        /// </summary>
        [XmlElement(Order = 18)]
        public MISMOPercent HOEPA_APRPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the HOEPA_APRPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HOEPA_APRPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool HOEPA_APRPercentSpecified
        {
            get { return HOEPA_APRPercent != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 19)]
        public FEE_SUMMARY_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
