namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class URLA_TOTAL_HOUSING_EXPENSES
    {
        /// <summary>
        /// Gets a value indicating whether the URLA_TOTAL_HOUSING_EXPENSES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.UrlaTotalHousingExpenseSpecified;
            }
        }

        /// <summary>
        /// A collection of URLA total housing expenses.
        /// </summary>
        [XmlElement("URLA_TOTAL_HOUSING_EXPENSE", Order = 0)]
		public List<URLA_TOTAL_HOUSING_EXPENSE> UrlaTotalHousingExpense = new List<URLA_TOTAL_HOUSING_EXPENSE>();

        /// <summary>
        /// Gets or sets a value indicating whether the URLATotalHousingExpense element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the URLATotalHousingExpense element has been assigned a value.</value>
        [XmlIgnore]
        public bool UrlaTotalHousingExpenseSpecified
        {
            get { return this.UrlaTotalHousingExpense != null && this.UrlaTotalHousingExpense.Count(u => u != null && u.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public URLA_TOTAL_HOUSING_EXPENSES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
