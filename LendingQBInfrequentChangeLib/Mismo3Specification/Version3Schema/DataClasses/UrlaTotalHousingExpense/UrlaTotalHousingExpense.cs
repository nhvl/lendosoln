namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class URLA_TOTAL_HOUSING_EXPENSE
    {
        /// <summary>
        /// Gets a value indicating whether the URLA_TOTAL_HOUSING_EXPENSE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.HousingExpenseTypeOtherDescriptionSpecified
                    || this.HousingExpenseTypeSpecified
                    || this.URLATotalHousingExpensePaymentAmountSpecified;
            }
        }

        /// <summary>
        /// Specifies the general names (types) of housing expense items commonly listed as by the borrower(s) in a mortgage loan transaction.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<HousingExpenseBase> HousingExpenseType;

        /// <summary>
        /// Gets or sets a value indicating whether the HousingExpenseType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HousingExpenseType element has been assigned a value.</value>
        [XmlIgnore]
        public bool HousingExpenseTypeSpecified
        {
            get { return this.HousingExpenseType != null && this.HousingExpenseType.enumValue != HousingExpenseBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Housing Expense Type.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString HousingExpenseTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the HousingExpenseTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HousingExpenseTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool HousingExpenseTypeOtherDescriptionSpecified
        {
            get { return HousingExpenseTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount per month of the associated housing expense type.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOAmount URLATotalHousingExpensePaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the URLATotalHousingExpensePaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the URLATotalHousingExpensePaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool URLATotalHousingExpensePaymentAmountSpecified
        {
            get { return URLATotalHousingExpensePaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public URLA_TOTAL_HOUSING_EXPENSE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
