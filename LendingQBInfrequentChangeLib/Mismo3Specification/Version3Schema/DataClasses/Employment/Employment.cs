namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class EMPLOYMENT
    {
        /// <summary>
        /// Gets a value indicating whether the EMPLOYMENT container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.EmployedAbroadIndicatorSpecified
                    || this.EmploymentBorrowerHomeOfficeIndicatorSpecified
                    || this.EmploymentBorrowerSelfEmployedIndicatorSpecified
                    || this.EmploymentClassificationTypeSpecified
                    || this.EmploymentEndDateSpecified
                    || this.EmploymentMonthlyIncomeAmountSpecified
                    || this.EmploymentMonthsOnJobCountSpecified
                    || this.EmploymentPositionDescriptionSpecified
                    || this.EmploymentReportedDateSpecified
                    || this.EmploymentStartDateSpecified
                    || this.EmploymentStatusTypeSpecified
                    || this.EmploymentTimeInLineOfWorkMonthsCountSpecified
                    || this.EmploymentTimeInLineOfWorkYearsCountSpecified
                    || this.EmploymentYearsOnJobCountSpecified
                    || this.ExtensionSpecified
                    || this.SpecialBorrowerEmployerRelationshipTypeOtherDescriptionSpecified
                    || this.SpecialBorrowerEmployerRelationshipTypeSpecified;
            }
        }

        /// <summary>
        /// Indicates if the physical place of employment of the  borrower is located outside of the United States. If the employer of the borrower is a foreign entity and the place of employment of the borrower  is located inside the United States this value would be false.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIndicator EmployedAbroadIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the EmployedAbroadIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EmployedAbroadIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool EmployedAbroadIndicatorSpecified
        {
            get { return EmployedAbroadIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates that in the referenced employment, the office of the borrower is within the primary residence.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOIndicator EmploymentBorrowerHomeOfficeIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the EmploymentBorrowerHomeOfficeIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EmploymentBorrowerHomeOfficeIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool EmploymentBorrowerHomeOfficeIndicatorSpecified
        {
            get { return EmploymentBorrowerHomeOfficeIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates that in the referenced employment the borrower is self-employed.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOIndicator EmploymentBorrowerSelfEmployedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the EmploymentBorrowerSelfEmployedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EmploymentBorrowerSelfEmployedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool EmploymentBorrowerSelfEmployedIndicatorSpecified
        {
            get { return EmploymentBorrowerSelfEmployedIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether the employment for the borrower is primary or secondary.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOEnum<EmploymentClassificationBase> EmploymentClassificationType;

        /// <summary>
        /// Gets or sets a value indicating whether the EmploymentClassificationType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EmploymentClassificationType element has been assigned a value.</value>
        [XmlIgnore]
        public bool EmploymentClassificationTypeSpecified
        {
            get { return this.EmploymentClassificationType != null && this.EmploymentClassificationType.enumValue != EmploymentClassificationBase.Blank; }
            set { }
        }

        /// <summary>
        /// The date that the borrower ended the employment position with the employer. Current employment would have no end data. One source of this data is the URLA in Section IV (Dated To).
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMODate EmploymentEndDate;

        /// <summary>
        /// Gets or sets a value indicating whether the EmploymentEndDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EmploymentEndDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool EmploymentEndDateSpecified
        {
            get { return EmploymentEndDate != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount per month of Income associated with the borrowers employment. Note: For current secondary or previous primary or previous secondary income.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOAmount EmploymentMonthlyIncomeAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the EmploymentMonthlyIncomeAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EmploymentMonthlyIncomeAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool EmploymentMonthlyIncomeAmountSpecified
        {
            get { return EmploymentMonthlyIncomeAmount != null; }
            set { }
        }

        /// <summary>
        /// The number of complete months of service to an employer from the start of employment until the date on which the employment is reported.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOCount EmploymentMonthsOnJobCount;

        /// <summary>
        /// Gets or sets a value indicating whether the EmploymentMonthsOnJobCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EmploymentMonthsOnJobCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool EmploymentMonthsOnJobCountSpecified
        {
            get { return EmploymentMonthsOnJobCount != null; }
            set { }
        }

        /// <summary>
        /// An expanded description of the borrowers employment position. Collected on the URLA in Section IV.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOString EmploymentPositionDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the EmploymentPositionDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EmploymentPositionDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool EmploymentPositionDescriptionSpecified
        {
            get { return EmploymentPositionDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates the date when the employment information was reported to the repository bureau.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMODate EmploymentReportedDate;

        /// <summary>
        /// Gets or sets a value indicating whether the EmploymentReportedDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EmploymentReportedDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool EmploymentReportedDateSpecified
        {
            get { return EmploymentReportedDate != null; }
            set { }
        }

        /// <summary>
        /// The date that the borrower started the employment position with the employer.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMODate EmploymentStartDate;

        /// <summary>
        /// Gets or sets a value indicating whether the EmploymentStartDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EmploymentStartDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool EmploymentStartDateSpecified
        {
            get { return EmploymentStartDate != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether the employment for the borrower is current or previous.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOEnum<EmploymentStatusBase> EmploymentStatusType;

        /// <summary>
        /// Gets or sets a value indicating whether the EmploymentStatusType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EmploymentStatusType element has been assigned a value.</value>
        [XmlIgnore]
        public bool EmploymentStatusTypeSpecified
        {
            get { return this.EmploymentStatusType != null && this.EmploymentStatusType.enumValue != EmploymentStatusBase.Blank; }
            set { }
        }

        /// <summary>
        /// The number of complete months the borrower has been employed in the reported occupation.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOCount EmploymentTimeInLineOfWorkMonthsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the EmploymentTimeInLineOfWorkMonthsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EmploymentTimeInLineOfWorkMonthsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool EmploymentTimeInLineOfWorkMonthsCountSpecified
        {
            get { return EmploymentTimeInLineOfWorkMonthsCount != null; }
            set { }
        }

        /// <summary>
        /// The total number of years the borrower has been employed in the reported occupation. One source of this data is the URLA Section IV (Years Employed In This Line Of Work.).
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOCount EmploymentTimeInLineOfWorkYearsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the EmploymentTimeInLineOfWorkYearsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EmploymentTimeInLineOfWorkYearsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool EmploymentTimeInLineOfWorkYearsCountSpecified
        {
            get { return EmploymentTimeInLineOfWorkYearsCount != null; }
            set { }
        }

        /// <summary>
        /// The number of complete years of service to an employer from the start of employment until the date on which the employment is reported.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOCount EmploymentYearsOnJobCount;

        /// <summary>
        /// Gets or sets a value indicating whether the EmploymentYearsOnJobCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EmploymentYearsOnJobCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool EmploymentYearsOnJobCountSpecified
        {
            get { return EmploymentYearsOnJobCount != null; }
            set { }
        }

        /// <summary>
        /// Specifies a unique category of employer which is the source of income for the borrower.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOEnum<SpecialBorrowerEmployerRelationshipBase> SpecialBorrowerEmployerRelationshipType;

        /// <summary>
        /// Gets or sets a value indicating whether the SpecialBorrowerEmployerRelationshipType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SpecialBorrowerEmployerRelationshipType element has been assigned a value.</value>
        [XmlIgnore]
        public bool SpecialBorrowerEmployerRelationshipTypeSpecified
        {
            get { return this.SpecialBorrowerEmployerRelationshipType != null && this.SpecialBorrowerEmployerRelationshipType.enumValue != SpecialBorrowerEmployerRelationshipBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Borrower Employer Relationship Type.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOString SpecialBorrowerEmployerRelationshipTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the SpecialBorrowerEmployerRelationshipTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SpecialBorrowerEmployerRelationshipTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool SpecialBorrowerEmployerRelationshipTypeOtherDescriptionSpecified
        {
            get { return SpecialBorrowerEmployerRelationshipTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 16)]
        public EMPLOYMENT_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
