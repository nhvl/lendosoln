namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class POOL
    {
        /// <summary>
        /// Gets a value indicating whether the POOL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.PoolCertificateSpecified
                    || this.PoolDetailSpecified;
            }
        }

        /// <summary>
        /// A certificate for a pool.
        /// </summary>
        [XmlElement("POOL_CERTIFICATE", Order = 0)]
        public POOL_CERTIFICATE PoolCertificate;

        /// <summary>
        /// Gets or sets a value indicating whether the PoolCertificate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PoolCertificate element has been assigned a value.</value>
        [XmlIgnore]
        public bool PoolCertificateSpecified
        {
            get { return this.PoolCertificate != null && this.PoolCertificate.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details on a pool.
        /// </summary>
        [XmlElement("POOL_DETAIL", Order = 1)]
        public POOL_DETAIL PoolDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the PoolDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PoolDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool PoolDetailSpecified
        {
            get { return this.PoolDetail != null && this.PoolDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public POOL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
