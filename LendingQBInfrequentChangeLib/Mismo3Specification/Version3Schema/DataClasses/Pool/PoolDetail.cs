namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class POOL_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the POOL_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AmortizationTypeOtherDescriptionSpecified
                    || this.AmortizationTypeSpecified
                    || this.CUSIPIdentifierSpecified
                    || this.ExtensionSpecified
                    || this.GovernmentBondFinanceIndicatorSpecified
                    || this.GovernmentBondFinancingProgramTypeOtherDescriptionSpecified
                    || this.GovernmentBondFinancingProgramTypeSpecified
                    || this.PoolAccrualRateStructureTypeSpecified
                    || this.PoolAssumabilityIndicatorSpecified
                    || this.PoolBalloonIndicatorSpecified
                    || this.PoolClassTypeOtherDescriptionSpecified
                    || this.PoolClassTypeSpecified
                    || this.PoolConcurrentTransferIndicatorSpecified
                    || this.PoolCurrentLoanCountSpecified
                    || this.PoolCurrentPrincipalBalanceAmountSpecified
                    || this.PoolFixedServicingFeePercentSpecified
                    || this.PoolGuarantyFeeRatePercentSpecified
                    || this.PoolIdentifierSpecified
                    || this.PoolingMethodTypeOtherDescriptionSpecified
                    || this.PoolingMethodTypeSpecified
                    || this.PoolInterestAdjustmentEffectiveDateSpecified
                    || this.PoolInterestAdjustmentIndexLeadDaysCountSpecified
                    || this.PoolInterestAndPaymentAdjustmentIndexLeadDaysCountSpecified
                    || this.PoolInterestOnlyIndicatorSpecified
                    || this.PoolInterestRateRoundingPercentSpecified
                    || this.PoolInterestRateRoundingTypeSpecified
                    || this.PoolInterestRateTruncatedDigitsCountSpecified
                    || this.PoolInvestorProductPlanIdentifierSpecified
                    || this.PoolIssueDateSpecified
                    || this.PoolIssuerIdentifierSpecified
                    || this.PoolMarginRatePercentSpecified
                    || this.PoolMaturityDateSpecified
                    || this.PoolMaximumAccrualRatePercentSpecified
                    || this.PoolMinimumAccrualRatePercentSpecified
                    || this.PoolMortgageRatePercentSpecified
                    || this.PoolMortgageTypeOtherDescriptionSpecified
                    || this.PoolMortgageTypeSpecified
                    || this.PoolOriginalLoanCountSpecified
                    || this.PoolOriginalPrincipalBalanceAmountSpecified
                    || this.PoolOwnershipPercentSpecified
                    || this.PoolPrefixIdentifierSpecified
                    || this.PoolPriorPeriodPrincipalBalanceAmountSpecified
                    || this.PoolScheduledPrincipalAndInterestPaymentAmountSpecified
                    || this.PoolScheduledPrincipalBalanceAmountSpecified
                    || this.PoolScheduledRemittancePaymentDaySpecified
                    || this.PoolSecurityInterestRatePercentSpecified
                    || this.PoolSecurityIssueDateInterestRatePercentSpecified
                    || this.PoolServiceFeeRatePercentSpecified
                    || this.PoolStructureTypeOtherDescriptionSpecified
                    || this.PoolStructureTypeSpecified
                    || this.PoolSuffixIdentifierSpecified
                    || this.PoolUnscheduledPrincipalAmountSpecified
                    || this.PoolUnscheduledPrincipalPaymentDaySpecified
                    || this.SecurityTradeBookEntryDateSpecified
                    || this.SecurityTradeCustomerAccountIdentifierSpecified;
            }
        }

        /// <summary>
        /// A classification or description of a loan or a group of loans generally based on the changeability of the rate or payment over time.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<AmortizationBase> AmortizationType;

        /// <summary>
        /// Gets or sets a value indicating whether the AmortizationType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AmortizationType element has been assigned a value.</value>
        [XmlIgnore]
        public bool AmortizationTypeSpecified
        {
            get { return this.AmortizationType != null && this.AmortizationType.enumValue != AmortizationBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information or a description of the amortization type when Other is selected.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString AmortizationTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the AmortizationTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AmortizationTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool AmortizationTypeOtherDescriptionSpecified
        {
            get { return AmortizationTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Unique identifier according to the standards set by Committee on Uniform Security Identification Practices. The CUSIP is registered with the SEC and the Federal Reserve.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOIdentifier CUSIPIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the CUSIPIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CUSIPIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool CUSIPIdentifierSpecified
        {
            get { return CUSIPIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the pool is to back securities for use as collateral for a state or local housing bond financing program (BFP).
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOIndicator GovernmentBondFinanceIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the GovernmentBondFinanceIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GovernmentBondFinanceIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool GovernmentBondFinanceIndicatorSpecified
        {
            get { return GovernmentBondFinanceIndicator != null; }
            set { }
        }

        /// <summary>
        /// Type of state or local housing bond financing program (BFP).
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<GovernmentBondFinancingProgramBase> GovernmentBondFinancingProgramType;

        /// <summary>
        /// Gets or sets a value indicating whether the GovernmentBondFinancingProgramType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GovernmentBondFinancingProgramType element has been assigned a value.</value>
        [XmlIgnore]
        public bool GovernmentBondFinancingProgramTypeSpecified
        {
            get { return this.GovernmentBondFinancingProgramType != null && this.GovernmentBondFinancingProgramType.enumValue != GovernmentBondFinancingProgramBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information or a description of the mortgage type when Other is selected for Government Bond Financing Program Type.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString GovernmentBondFinancingProgramTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the GovernmentBondFinancingProgramTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GovernmentBondFinancingProgramTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool GovernmentBondFinancingProgramTypeOtherDescriptionSpecified
        {
            get { return GovernmentBondFinancingProgramTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the structure used to determine the accrual interest rate for the pool.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOEnum<PoolAccrualRateStructureBase> PoolAccrualRateStructureType;

        /// <summary>
        /// Gets or sets a value indicating whether the PoolAccrualRateStructureType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PoolAccrualRateStructureType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PoolAccrualRateStructureTypeSpecified
        {
            get { return this.PoolAccrualRateStructureType != null && this.PoolAccrualRateStructureType.enumValue != PoolAccrualRateStructureBase.Blank; }
            set { }
        }

        /// <summary>
        /// Indicates whether the pool is backed by loans that are assumable by another borrower.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOIndicator PoolAssumabilityIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the Pool Assumption Indicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Pool Assumption Indicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PoolAssumabilityIndicatorSpecified
        {
            get { return PoolAssumabilityIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether or not the pool is backed by loans where a final balloon payment is required under the terms of the loan repayment schedule to fully pay off the loan.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOIndicator PoolBalloonIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PoolBalloonIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PoolBalloonIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PoolBalloonIndicatorSpecified
        {
            get { return PoolBalloonIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates the type of pool and its guarantor (if any).
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOEnum<PoolClassBase> PoolClassType;

        /// <summary>
        /// Gets or sets a value indicating whether the PoolClassType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PoolClassType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PoolClassTypeSpecified
        {
            get { return this.PoolClassType != null && this.PoolClassType.enumValue != PoolClassBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture the Pool Class if Other is selected as the Pool Class Type.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOString PoolClassTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PoolClassTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PoolClassTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PoolClassTypeOtherDescriptionSpecified
        {
            get { return PoolClassTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that the pool will be immediately transferred upon pool issuance.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOIndicator PoolConcurrentTransferIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PoolConcurrentTransferIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PoolConcurrentTransferIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PoolConcurrentTransferIndicatorSpecified
        {
            get { return PoolConcurrentTransferIndicator != null; }
            set { }
        }

        /// <summary>
        /// The current number of mortgages in the pool with principal balances as of the last cutoff.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOCount PoolCurrentLoanCount;

        /// <summary>
        /// Gets or sets a value indicating whether the PoolCurrentLoanCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PoolCurrentLoanCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PoolCurrentLoanCountSpecified
        {
            get { return PoolCurrentLoanCount != null; }
            set { }
        }

        /// <summary>
        /// Total dollar amount of the remaining unpaid principal balance for a pool of mortgages as of the last cutoff.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOAmount PoolCurrentPrincipalBalanceAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PoolCurrentPrincipalBalanceAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PoolCurrentPrincipalBalanceAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PoolCurrentPrincipalBalanceAmountSpecified
        {
            get { return PoolCurrentPrincipalBalanceAmount != null; }
            set { }
        }

        /// <summary>
        /// The servicing fee to be used in determining the rate of interest that accrues on an mortgage backed security (MBS) pool that has a weighted-average structure based on a fixed servicing fee.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOPercent PoolFixedServicingFeePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the PoolFixedServicingFeePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PoolFixedServicingFeePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool PoolFixedServicingFeePercentSpecified
        {
            get { return PoolFixedServicingFeePercent != null; }
            set { }
        }

        /// <summary>
        /// The percentage of the guaranty fee rate that applies to loans in the pool that do not have a rate specified at the individual loan level.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOPercent PoolGuarantyFeeRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the PoolGuarantyFeeRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PoolGuarantyFeeRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool PoolGuarantyFeeRatePercentSpecified
        {
            get { return PoolGuarantyFeeRatePercent != null; }
            set { }
        }

        /// <summary>
        /// The unique identifier for a group or pool of loans.  May include relevant prefix and suffix when not parsed into applicable fields.  See Pool Prefix Identifier or Pool Suffix Identifier.
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMOIdentifier PoolIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the PoolIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PoolIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool PoolIdentifierSpecified
        {
            get { return PoolIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Indicates the timing of the remittance of the scheduled principal and interest payment from the servicer to the certificate holders of a particular pool.
        /// </summary>
        [XmlElement(Order = 17)]
        public MISMOEnum<PoolingMethodBase> PoolingMethodType;

        /// <summary>
        /// Gets or sets a value indicating whether the PoolingMethodType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PoolingMethodType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PoolingMethodTypeSpecified
        {
            get { return this.PoolingMethodType != null && this.PoolingMethodType.enumValue != PoolingMethodBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture the Pooling Method Type if Other is selected.
        /// </summary>
        [XmlElement(Order = 18)]
        public MISMOString PoolingMethodTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PoolingMethodTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PoolingMethodTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PoolingMethodTypeOtherDescriptionSpecified
        {
            get { return PoolingMethodTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The date the pool interest rate changes.
        /// </summary>
        [XmlElement(Order = 19)]
        public MISMODate PoolInterestAdjustmentEffectiveDate;

        /// <summary>
        /// Gets or sets a value indicating whether the PoolInterestAdjustmentEffectiveDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PoolInterestAdjustmentEffectiveDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool PoolInterestAdjustmentEffectiveDateSpecified
        {
            get { return PoolInterestAdjustmentEffectiveDate != null; }
            set { }
        }

        /// <summary>
        /// The number of days prior to an interest rate effective date used to determine the date for the index value when calculating a new interest rate for a pool of loans. 
        /// </summary>
        [XmlElement(Order = 20)]
        public MISMOCount PoolInterestAdjustmentIndexLeadDaysCount;

        /// <summary>
        /// Gets or sets a value indicating whether the PoolInterestAdjustmentIndexLeadDaysCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PoolInterestAdjustmentIndexLeadDaysCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PoolInterestAdjustmentIndexLeadDaysCountSpecified
        {
            get { return PoolInterestAdjustmentIndexLeadDaysCount != null; }
            set { }
        }

        /// <summary>
        /// The number of days prior to an interest rate effective date used to determine the date for the index value when calculating both a new interest rate and a principal and interest payment for a pool of loans.
        /// </summary>
        [XmlElement(Order = 21)]
        public MISMOCount PoolInterestAndPaymentAdjustmentIndexLeadDaysCount;

        /// <summary>
        /// Gets or sets a value indicating whether the PoolInterestAndPaymentAdjustmentIndexLeadDaysCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PoolInterestAndPaymentAdjustmentIndexLeadDaysCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PoolInterestAndPaymentAdjustmentIndexLeadDaysCountSpecified
        {
            get { return PoolInterestAndPaymentAdjustmentIndexLeadDaysCount != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether a pool is backed by loans with interest only payments.
        /// </summary>
        [XmlElement(Order = 22)]
        public MISMOIndicator PoolInterestOnlyIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PoolInterestOnlyIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PoolInterestOnlyIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PoolInterestOnlyIndicatorSpecified
        {
            get { return PoolInterestOnlyIndicator != null; }
            set { }
        }

        /// <summary>
        /// The percentage to which the interest rate is rounded when a new interest rate is calculated for a pool.  This field is used in conjunction with Pool Interest Rate Rounding Type which indicates how the rounding should occur.
        /// </summary>
        [XmlElement(Order = 23)]
        public MISMOPercent PoolInterestRateRoundingPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the PoolInterestRateRoundingPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PoolInterestRateRoundingPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool PoolInterestRateRoundingPercentSpecified
        {
            get { return PoolInterestRateRoundingPercent != null; }
            set { }
        }

        /// <summary>
        /// Indicates how the interest rate is rounded when a new interest rate is calculated for a pool of loans.
        /// </summary>
        [XmlElement(Order = 24)]
        public MISMOEnum<InterestRateRoundingBase> PoolInterestRateRoundingType;

        /// <summary>
        /// Gets or sets a value indicating whether the PoolInterestRateRoundingType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PoolInterestRateRoundingType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PoolInterestRateRoundingTypeSpecified
        {
            get { return this.PoolInterestRateRoundingType != null && this.PoolInterestRateRoundingType.enumValue != InterestRateRoundingBase.Blank; }
            set { }
        }

        /// <summary>
        ///  Indicates the number of digits the interest rate should be truncated for a pool.
        /// </summary>
        [XmlElement(Order = 25)]
        public MISMOCount PoolInterestRateTruncatedDigitsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the PoolInterestRateTruncatedDigitsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PoolInterestRateTruncatedDigitsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PoolInterestRateTruncatedDigitsCountSpecified
        {
            get { return PoolInterestRateTruncatedDigitsCount != null; }
            set { }
        }

        /// <summary>
        /// Identifies the plan of the varying loan payment and/or rate change characteristics for a pool of loans issued by the servicer.
        /// </summary>
        [XmlElement(Order = 26)]
        public MISMOIdentifier PoolInvestorProductPlanIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the PoolInvestorProductPlanIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PoolInvestorProductPlanIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool PoolInvestorProductPlanIdentifierSpecified
        {
            get { return PoolInvestorProductPlanIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The date a mortgage backed security is issued to investors.
        /// </summary>
        [XmlElement(Order = 27)]
        public MISMODate PoolIssueDate;

        /// <summary>
        /// Gets or sets a value indicating whether the PoolIssueDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PoolIssueDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool PoolIssueDateSpecified
        {
            get { return PoolIssueDate != null; }
            set { }
        }

        /// <summary>
        /// Identification number assigned by pool guarantor to the Issuer or current servicer of the pool.
        /// </summary>
        [XmlElement(Order = 28)]
        public MISMOIdentifier PoolIssuerIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the PoolIssuerIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PoolIssuerIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool PoolIssuerIdentifierSpecified
        {
            get { return PoolIssuerIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The factor that is added to the ARM index value to calculate the pool accrual rate.
        /// </summary>
        [XmlElement(Order = 29)]
        public MISMOPercent PoolMarginRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the PoolMarginRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PoolMarginRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool PoolMarginRatePercentSpecified
        {
            get { return PoolMarginRatePercent != null; }
            set { }
        }

        /// <summary>
        /// The date the final payment is due on the pool. The pool maturity date is typically 15 to 45 days after the maturity date of the latest maturing mortgage in the mortgage-backed security.
        /// </summary>
        [XmlElement(Order = 30)]
        public MISMODate PoolMaturityDate;

        /// <summary>
        /// Gets or sets a value indicating whether the PoolMaturityDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PoolMaturityDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool PoolMaturityDateSpecified
        {
            get { return PoolMaturityDate != null; }
            set { }
        }

        /// <summary>
        /// The maximum rate, expressed as a percent, at which interest can accrue on a pool.
        /// </summary>
        [XmlElement(Order = 31)]
        public MISMOPercent PoolMaximumAccrualRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the PoolMaximumAccrualRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PoolMaximumAccrualRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool PoolMaximumAccrualRatePercentSpecified
        {
            get { return PoolMaximumAccrualRatePercent != null; }
            set { }
        }

        /// <summary>
        /// The minimum rate, expressed as a percent, at which interest can accrue on a pool.
        /// </summary>
        [XmlElement(Order = 32)]
        public MISMOPercent PoolMinimumAccrualRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the PoolMinimumAccrualRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PoolMinimumAccrualRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool PoolMinimumAccrualRatePercentSpecified
        {
            get { return PoolMinimumAccrualRatePercent != null; }
            set { }
        }

        /// <summary>
        /// The annual interest rate, expressed as a percent, of the mortgages in a pool as of last cutoff date.
        /// </summary>
        [XmlElement(Order = 33)]
        public MISMOPercent PoolMortgageRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the PoolMortgageRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PoolMortgageRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool PoolMortgageRatePercentSpecified
        {
            get { return PoolMortgageRatePercent != null; }
            set { }
        }

        /// <summary>
        /// Specifies mortgage type of the loans for the pool.
        /// </summary>
        [XmlElement(Order = 34)]
        public MISMOEnum<MortgageBase> PoolMortgageType;

        /// <summary>
        /// Gets or sets a value indicating whether the PoolMortgageType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PoolMortgageType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PoolMortgageTypeSpecified
        {
            get { return this.PoolMortgageType != null && this.PoolMortgageType.enumValue != MortgageBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information or a description of the mortgage type when Other is selected for Pool Mortgage Type.
        /// </summary>
        [XmlElement(Order = 35)]
        public MISMOString PoolMortgageTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PoolMortgageTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PoolMortgageTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PoolMortgageTypeOtherDescriptionSpecified
        {
            get { return PoolMortgageTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The total number of loans in the pool when the pool was set up.
        /// </summary>
        [XmlElement(Order = 36)]
        public MISMOCount PoolOriginalLoanCount;

        /// <summary>
        /// Gets or sets a value indicating whether the PoolOriginalLoanCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PoolOriginalLoanCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PoolOriginalLoanCountSpecified
        {
            get { return PoolOriginalLoanCount != null; }
            set { }
        }

        /// <summary>
        /// The total of the original principal balance of all the loans in the pool. For participations the principal balance contributed for an individual loan is adjusted based upon the participation percent.
        /// </summary>
        [XmlElement(Order = 37)]
        public MISMOAmount PoolOriginalPrincipalBalanceAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PoolOriginalPrincipalBalanceAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PoolOriginalPrincipalBalanceAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PoolOriginalPrincipalBalanceAmountSpecified
        {
            get { return PoolOriginalPrincipalBalanceAmount != null; }
            set { }
        }

        /// <summary>
        /// Identifies the percentage amount of the pool owned by the investor.
        /// </summary>
        [XmlElement(Order = 38)]
        public MISMOPercent PoolOwnershipPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the PoolOwnershipPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PoolOwnershipPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool PoolOwnershipPercentSpecified
        {
            get { return PoolOwnershipPercent != null; }
            set { }
        }

        /// <summary>
        /// The prefix associated with the pool identifier.
        /// </summary>
        [XmlElement(Order = 39)]
        public MISMOIdentifier PoolPrefixIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the PoolPrefixIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PoolPrefixIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool PoolPrefixIdentifierSpecified
        {
            get { return PoolPrefixIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of the outstanding principal  balance, reported in the prior period, for the loans in the pool.
        /// </summary>
        [XmlElement(Order = 40)]
        public MISMOAmount PoolPriorPeriodPrincipalBalanceAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PoolPriorPeriodPrincipalBalanceAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PoolPriorPeriodPrincipalBalanceAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PoolPriorPeriodPrincipalBalanceAmountSpecified
        {
            get { return PoolPriorPeriodPrincipalBalanceAmount != null; }
            set { }
        }

        /// <summary>
        /// Total scheduled dollar amount of the principal and interest payments of the underlying loans as of the last cutoff date.
        /// </summary>
        [XmlElement(Order = 41)]
        public MISMOAmount PoolScheduledPrincipalAndInterestPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PoolScheduledPrincipalAndInterestPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PoolScheduledPrincipalAndInterestPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PoolScheduledPrincipalAndInterestPaymentAmountSpecified
        {
            get { return PoolScheduledPrincipalAndInterestPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of the scheduled principal balance of the loans in the pool amortized through the date when the activity report is made.
        /// </summary>
        [XmlElement(Order = 42)]
        public MISMOAmount PoolScheduledPrincipalBalanceAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PoolScheduledPrincipalBalanceAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PoolScheduledPrincipalBalanceAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PoolScheduledPrincipalBalanceAmountSpecified
        {
            get { return PoolScheduledPrincipalBalanceAmount != null; }
            set { }
        }

        /// <summary>
        /// Indicates the day of the month the servicer must remit the scheduled pass through payment to the certificate holders of a particular pool.
        /// </summary>
        [XmlElement(Order = 43)]
        public MISMODay PoolScheduledRemittancePaymentDay;

        /// <summary>
        /// Gets or sets a value indicating whether the PoolScheduledRemittancePaymentDay element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PoolScheduledRemittancePaymentDay element has been assigned a value.</value>
        [XmlIgnore]
        public bool PoolScheduledRemittancePaymentDaySpecified
        {
            get { return PoolScheduledRemittancePaymentDay != null; }
            set { }
        }

        /// <summary>
        /// The security interest rate, expressed as a percent, for the pool as of last cutoff date.
        /// </summary>
        [XmlElement(Order = 44)]
        public MISMOPercent PoolSecurityInterestRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the PoolSecurityInterestRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PoolSecurityInterestRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool PoolSecurityInterestRatePercentSpecified
        {
            get { return PoolSecurityInterestRatePercent != null; }
            set { }
        }

        /// <summary>
        /// The security interest rate, expressed as a percent, for the pool as of the security issue date.
        /// </summary>
        [XmlElement(Order = 45)]
        public MISMOPercent PoolSecurityIssueDateInterestRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the PoolSecurityIssueDateInterestRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PoolSecurityIssueDateInterestRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool PoolSecurityIssueDateInterestRatePercentSpecified
        {
            get { return PoolSecurityIssueDateInterestRatePercent != null; }
            set { }
        }

        /// <summary>
        /// The annualized rate, expressed as a percent, used to compute the fee paid to the servicer as of the last cutoff date.
        /// </summary>
        [XmlElement(Order = 46)]
        public MISMOPercent PoolServiceFeeRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the PoolServiceFeeRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PoolServiceFeeRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool PoolServiceFeeRatePercentSpecified
        {
            get { return PoolServiceFeeRatePercent != null; }
            set { }
        }

        /// <summary>
        /// Denotes the type of mortgage backed security structure.
        /// </summary>
        [XmlElement(Order = 47)]
        public MISMOEnum<PoolStructureBase> PoolStructureType;

        /// <summary>
        /// Gets or sets a value indicating whether the PoolStructureType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PoolStructureType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PoolStructureTypeSpecified
        {
            get { return this.PoolStructureType != null && this.PoolStructureType.enumValue != PoolStructureBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Pool Structure Type.
        /// </summary>
        [XmlElement(Order = 48)]
        public MISMOString PoolStructureTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PoolStructureTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PoolStructureTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PoolStructureTypeOtherDescriptionSpecified
        {
            get { return PoolStructureTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Suffix associated with the pool identifier. 
        /// </summary>
        [XmlElement(Order = 49)]
        public MISMOIdentifier PoolSuffixIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the PoolSuffixIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PoolSuffixIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool PoolSuffixIdentifierSpecified
        {
            get { return PoolSuffixIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of the unscheduled principal paid, for the current reporting period, of the loans in the pool.
        /// </summary>
        [XmlElement(Order = 50)]
        public MISMOAmount PoolUnscheduledPrincipalAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PoolUnscheduledPrincipalAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PoolUnscheduledPrincipalAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PoolUnscheduledPrincipalAmountSpecified
        {
            get { return PoolUnscheduledPrincipalAmount != null; }
            set { }
        }

        /// <summary>
        /// Indicates the day of the month the servicer must remit unscheduled principal payments to the certificate holders of a particular pool if they are to be remitted sooner than the scheduled remittance day.
        /// </summary>
        [XmlElement(Order = 51)]
        public MISMODay PoolUnscheduledPrincipalPaymentDay;

        /// <summary>
        /// Gets or sets a value indicating whether the PoolUnscheduledPrincipalPaymentDay element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PoolUnscheduledPrincipalPaymentDay element has been assigned a value.</value>
        [XmlIgnore]
        public bool PoolUnscheduledPrincipalPaymentDaySpecified
        {
            get { return PoolUnscheduledPrincipalPaymentDay != null; }
            set { }
        }

        /// <summary>
        /// The date that the security will be delivered to the designated book entry account.
        /// </summary>
        [XmlElement(Order = 52)]
        public MISMODate SecurityTradeBookEntryDate;

        /// <summary>
        /// Gets or sets a value indicating whether the SecurityTradeBookEntryDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SecurityTradeBookEntryDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool SecurityTradeBookEntryDateSpecified
        {
            get { return SecurityTradeBookEntryDate != null; }
            set { }
        }

        /// <summary>
        /// The securities trading account identifier assigned to a customer by an investor.
        /// </summary>
        [XmlElement(Order = 53)]
        public MISMOIdentifier SecurityTradeCustomerAccountIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the SecurityTradeCustomerAccountIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SecurityTradeCustomerAccountIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool SecurityTradeCustomerAccountIdentifierSpecified
        {
            get { return SecurityTradeCustomerAccountIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 54)]
        public POOL_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
