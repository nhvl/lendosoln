namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class BANKRUPTCIES
    {
        /// <summary>
        /// Gets a value indicating whether the BANKRUPTCIES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BankruptcySpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of occurrences of bankruptcy.
        /// </summary>
        [XmlElement("BANKRUPTCY", Order = 0)]
		public List<BANKRUPTCY> Bankruptcy = new List<BANKRUPTCY>();

        /// <summary>
        /// Gets or sets a value indicating whether the Bankruptcy element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Bankruptcy element has been assigned a value.</value>
        [XmlIgnore]
        public bool BankruptcySpecified
        {
            get { return this.Bankruptcy != null && this.Bankruptcy.Count(b => b != null && b.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public BANKRUPTCIES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
