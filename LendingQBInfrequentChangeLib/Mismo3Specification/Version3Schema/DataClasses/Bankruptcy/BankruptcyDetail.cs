namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class BANKRUPTCY_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the BANKRUPTCY_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BankruptcyAdjustmentPercentSpecified
                    || this.BankruptcyAssetsAmountSpecified
                    || this.BankruptcyBarDateSpecified
                    || this.BankruptcyCaseIdentifierSpecified
                    || this.BankruptcyChapterTypeOtherDescriptionSpecified
                    || this.BankruptcyChapterTypeSpecified
                    || this.BankruptcyDistrictNameSpecified
                    || this.BankruptcyDivisionNameSpecified
                    || this.BankruptcyExemptAmountSpecified
                    || this.BankruptcyFeeAmountSpecified
                    || this.BankruptcyFiledStateNameSpecified
                    || this.BankruptcyLiabilitiesAmountSpecified
                    || this.BankruptcyRepaymentPercentSpecified
                    || this.BankruptcyTypeSpecified
                    || this.BankruptcyVoluntaryIndicatorSpecified
                    || this.ExtensionSpecified
                    || this.MaterialChangeInLoanTermsRequestedIndicatorSpecified
                    || this.MotionForReliefNotFiledReasonDescriptionSpecified
                    || this.PostPetitionPaymentAmountSpecified
                    || this.PostPetitionPayToTrusteeIndicatorSpecified
                    || this.PostPetititonArrearageIndicatorSpecified
                    || this.PrePetitionArrearageIndicatorSpecified
                    || this.PreviousChapterSevenBankruptcyDischargeIndicatorSpecified
                    || this.PrincipalReductionOfUnsecuredAmountSpecified;
            }
        }

        /// <summary>
        /// The percentage of the debt paid by the consumer. This amount is negotiated and assigned by the courts.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOPercent BankruptcyAdjustmentPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the BankruptcyAdjustmentPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BankruptcyAdjustmentPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool BankruptcyAdjustmentPercentSpecified
        {
            get { return BankruptcyAdjustmentPercent != null; }
            set { }
        }

        /// <summary>
        /// The amount of the defendants declared assets at the time of bankruptcy.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOAmount BankruptcyAssetsAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the BankruptcyAssetsAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BankruptcyAssetsAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool BankruptcyAssetsAmountSpecified
        {
            get { return BankruptcyAssetsAmount != null; }
            set { }
        }

        /// <summary>
        /// The date by which proofs of claim must be filed in a bankruptcy proceeding.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMODate BankruptcyBarDate;

        /// <summary>
        /// Gets or sets a value indicating whether the BankruptcyBarDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BankruptcyBarDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool BankruptcyBarDateSpecified
        {
            get { return BankruptcyBarDate != null; }
            set { }
        }

        /// <summary>
        /// The court-issued case number associated with the bankruptcy filing being reported.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOIdentifier BankruptcyCaseIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the BankruptcyCaseIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BankruptcyCaseIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool BankruptcyCaseIdentifierSpecified
        {
            get { return BankruptcyCaseIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Chapter under which bankruptcy has been filed. 
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<BankruptcyChapterBase> BankruptcyChapterType;

        /// <summary>
        /// Gets or sets a value indicating whether the BankruptcyChapterType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BankruptcyChapterType element has been assigned a value.</value>
        [XmlIgnore]
        public bool BankruptcyChapterTypeSpecified
        {
            get { return this.BankruptcyChapterType != null && this.BankruptcyChapterType.enumValue != BankruptcyChapterBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture the Bankruptcy Chapter Type if Other is selected as the Bankruptcy Chapter Type.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString BankruptcyChapterTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the BankruptcyChapterTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BankruptcyChapterTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool BankruptcyChapterTypeOtherDescriptionSpecified
        {
            get { return BankruptcyChapterTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The name of the district in which the bankruptcy petition will be processed.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOString BankruptcyDistrictName;

        /// <summary>
        /// Gets or sets a value indicating whether the BankruptcyDistrictName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BankruptcyDistrictName element has been assigned a value.</value>
        [XmlIgnore]
        public bool BankruptcyDistrictNameSpecified
        {
            get { return BankruptcyDistrictName != null; }
            set { }
        }

        /// <summary>
        /// The name of the court in which the bankruptcy petition will be processed.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOString BankruptcyDivisionName;

        /// <summary>
        /// Gets or sets a value indicating whether the BankruptcyDivisionName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BankruptcyDivisionName element has been assigned a value.</value>
        [XmlIgnore]
        public bool BankruptcyDivisionNameSpecified
        {
            get { return BankruptcyDivisionName != null; }
            set { }
        }

        /// <summary>
        /// The value of exempt assets that the bankruptcy court allows the debtor to retain.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOAmount BankruptcyExemptAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the BankruptcyExemptAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BankruptcyExemptAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool BankruptcyExemptAmountSpecified
        {
            get { return BankruptcyExemptAmount != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of fees and other costs associated with bankruptcy based on the borrower's time in bankruptcy and the average amount per the state (United States) where the property is located as published by the investor.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOAmount BankruptcyFeeAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the BankruptcyFeeAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BankruptcyFeeAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool BankruptcyFeeAmountSpecified
        {
            get { return BankruptcyFeeAmount != null; }
            set { }
        }

        /// <summary>
        /// The name of the state in which the bankruptcy petition was filed.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOString BankruptcyFiledStateName;

        /// <summary>
        /// Gets or sets a value indicating whether the BankruptcyFiledStateName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BankruptcyFiledStateName element has been assigned a value.</value>
        [XmlIgnore]
        public bool BankruptcyFiledStateNameSpecified
        {
            get { return BankruptcyFiledStateName != null; }
            set { }
        }

        /// <summary>
        /// The amount of the defendants declared liabilities at the time of bankruptcy.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOAmount BankruptcyLiabilitiesAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the BankruptcyLiabilitiesAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BankruptcyLiabilitiesAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool BankruptcyLiabilitiesAmountSpecified
        {
            get { return BankruptcyLiabilitiesAmount != null; }
            set { }
        }

        /// <summary>
        /// The percentage of the total bankruptcy amount that will be paid to the creditors.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOPercent BankruptcyRepaymentPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the BankruptcyRepaymentPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BankruptcyRepaymentPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool BankruptcyRepaymentPercentSpecified
        {
            get { return BankruptcyRepaymentPercent != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether the borrower has file a bankruptcy related to their personal assets or whether the bankruptcy is related to a business in which the borrower is a principal.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOEnum<BankruptcyBase> BankruptcyType;

        /// <summary>
        /// Gets or sets a value indicating whether the BankruptcyType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BankruptcyType element has been assigned a value.</value>
        [XmlIgnore]
        public bool BankruptcyTypeSpecified
        {
            get { return this.BankruptcyType != null && this.BankruptcyType.enumValue != BankruptcyBase.Blank; }
            set { }
        }

        /// <summary>
        /// Indicates whether the bankruptcy was voluntary or involuntary.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOIndicator BankruptcyVoluntaryIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the BankruptcyVoluntaryIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BankruptcyVoluntaryIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool BankruptcyVoluntaryIndicatorSpecified
        {
            get { return BankruptcyVoluntaryIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates the debtor is requesting a material change in the terms of the loan as a part of the proceedings.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOIndicator MaterialChangeInLoanTermsRequestedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the MaterialChangeInLoanTermsRequestedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MaterialChangeInLoanTermsRequestedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool MaterialChangeInLoanTermsRequestedIndicatorSpecified
        {
            get { return MaterialChangeInLoanTermsRequestedIndicator != null; }
            set { }
        }

        /// <summary>
        /// A free form text field to capture the reason that a motion for relief has not been filed.
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMOString MotionForReliefNotFiledReasonDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MotionForReliefNotFiledReasonDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MotionForReliefNotFiledReasonDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MotionForReliefNotFiledReasonDescriptionSpecified
        {
            get { return MotionForReliefNotFiledReasonDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether this bankruptcy involves a post petition arrearage.
        /// </summary>
        [XmlElement(Order = 17)]
        public MISMOIndicator PostPetititonArrearageIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PostPetitionArrearageIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PostPetitionArrearageIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PostPetititonArrearageIndicatorSpecified
        {
            get { return PostPetititonArrearageIndicator != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of total principal, interest, taxes, insurance and association dues payment agreed upon in a confirmed attorney or stipulation agreement. 
        /// </summary>
        [XmlElement(Order = 18)]
        public MISMOAmount PostPetitionPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PostPetitionPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PostPetitionPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PostPetitionPaymentAmountSpecified
        {
            get { return PostPetitionPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that the post-petition payments are sent to the bankruptcy trustee by the borrower and then forwarded to the Servicer by the Trustee.
        /// </summary>
        [XmlElement(Order = 19)]
        public MISMOIndicator PostPetitionPayToTrusteeIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PostPetitionPayToTrusteeIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PostPetitionPayToTrusteeIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PostPetitionPayToTrusteeIndicatorSpecified
        {
            get { return PostPetitionPayToTrusteeIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that the borrower has fallen behind on their mortgage payments before the  bankruptcy petition was filed.
        /// </summary>
        [XmlElement(Order = 20)]
        public MISMOIndicator PrePetitionArrearageIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PrePetitionArrearageIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrePetitionArrearageIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrePetitionArrearageIndicatorSpecified
        {
            get { return PrePetitionArrearageIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates the borrower has previously received a Chapter 7 bankruptcy discharge.
        /// </summary>
        [XmlElement(Order = 21)]
        public MISMOIndicator PreviousChapterSevenBankruptcyDischargeIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PreviousChapterSevenBankruptcyDischargeIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PreviousChapterSevenBankruptcyDischargeIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PreviousChapterSevenBankruptcyDischargeIndicatorSpecified
        {
            get { return PreviousChapterSevenBankruptcyDischargeIndicator != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of principal reduction as result of a bankruptcy due to the bifurcation of the claim.
        /// </summary>
        [XmlElement(Order = 22)]
        public MISMOAmount PrincipalReductionOfUnsecuredAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PrincipalReductionOfUnsecuredAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrincipalReductionOfUnsecuredAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrincipalReductionOfUnsecuredAmountSpecified
        {
            get { return PrincipalReductionOfUnsecuredAmount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 23)]
        public BANKRUPTCY_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
