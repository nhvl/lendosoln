namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class BANKRUPTCY
    {
        /// <summary>
        /// Gets a value indicating whether the BANKRUPTCY container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BankruptcyActionsSpecified
                    || this.BankruptcyDetailSpecified
                    || this.BankruptcyDispositionsSpecified
                    || this.BankruptcyResultClassesSpecified
                    || this.BankruptcyStatusesSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// Actions related to this bankruptcy.
        /// </summary>
        [XmlElement("BANKRUPTCY_ACTIONS", Order = 0)]
        public BANKRUPTCY_ACTIONS BankruptcyActions;

        /// <summary>
        /// Gets or sets a value indicating whether the BankruptcyActions element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BankruptcyActions element has been assigned a value.</value>
        [XmlIgnore]
        public bool BankruptcyActionsSpecified
        {
            get { return this.BankruptcyActions != null && this.BankruptcyActions.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details on this bankruptcy.
        /// </summary>
        [XmlElement("BANKRUPTCY_DETAIL", Order = 1)]
        public BANKRUPTCY_DETAIL BankruptcyDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the BankruptcyDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BankruptcyDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool BankruptcyDetailSpecified
        {
            get { return this.BankruptcyDetail != null && this.BankruptcyDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Dispositions related to this bankruptcy.
        /// </summary>
        [XmlElement("BANKRUPTCY_DISPOSITIONS", Order = 2)]
        public BANKRUPTCY_DISPOSITIONS BankruptcyDispositions;

        /// <summary>
        /// Gets or sets a value indicating whether the BankruptcyDispositions element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BankruptcyDispositions element has been assigned a value.</value>
        [XmlIgnore]
        public bool BankruptcyDispositionsSpecified
        {
            get { return this.BankruptcyDispositions != null && this.BankruptcyDispositions.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Result classes of this bankruptcy.
        /// </summary>
        [XmlElement("BANKRUPTCY_RESULT_CLASSES", Order = 3)]
        public BANKRUPTCY_RESULT_CLASSES BankruptcyResultClasses;

        /// <summary>
        /// Gets or sets a value indicating whether the BankruptcyResultClasses element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BankruptcyResultClasses element has been assigned a value.</value>
        [XmlIgnore]
        public bool BankruptcyResultClassesSpecified
        {
            get { return this.BankruptcyResultClasses != null && this.BankruptcyResultClasses.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Status of this bankruptcy.
        /// </summary>
        [XmlElement("BANKRUPTCY_STATUSES", Order = 4)]
        public BANKRUPTCY_STATUSES BankruptcyStatuses;

        /// <summary>
        /// Gets or sets a value indicating whether the BankruptcyStatuses element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BankruptcyStatuses element has been assigned a value.</value>
        [XmlIgnore]
        public bool BankruptcyStatusesSpecified
        {
            get { return this.BankruptcyStatuses != null && this.BankruptcyStatuses.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 5)]
        public BANKRUPTCY_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
