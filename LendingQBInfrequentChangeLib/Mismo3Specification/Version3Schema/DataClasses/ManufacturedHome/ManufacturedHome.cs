namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class MANUFACTURED_HOME
    {
        /// <summary>
        /// Gets a value indicating whether the MANUFACTURED_HOME container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ManufacturedHomeDetailSpecified
                    || this.ManufacturedHomeIdentificationSpecified
                    || this.ManufacturedHomeSectionsSpecified;
            }
        }

        /// <summary>
        /// Details on a manufactured home.
        /// </summary>
        [XmlElement("MANUFACTURED_HOME_DETAIL", Order = 0)]
        public MANUFACTURED_HOME_DETAIL ManufacturedHomeDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the ManufacturedHomeDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ManufacturedHomeDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool ManufacturedHomeDetailSpecified
        {
            get { return this.ManufacturedHomeDetail != null && this.ManufacturedHomeDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Identification information for a manufactured home.
        /// </summary>
        [XmlElement("MANUFACTURED_HOME_IDENTIFICATION", Order = 1)]
        public MANUFACTURED_HOME_IDENTIFICATION ManufacturedHomeIdentification;

        /// <summary>
        /// Gets or sets a value indicating whether the ManufacturedHomeIdentification element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ManufacturedHomeIdentification element has been assigned a value.</value>
        [XmlIgnore]
        public bool ManufacturedHomeIdentificationSpecified
        {
            get { return this.ManufacturedHomeIdentification != null && this.ManufacturedHomeIdentification.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Various sections of a manufactured home.
        /// </summary>
        [XmlElement("MANUFACTURED_HOME_SECTIONS", Order = 2)]
        public MANUFACTURED_HOME_SECTIONS ManufacturedHomeSections;

        /// <summary>
        /// Gets or sets a value indicating whether the ManufacturedHomeSections element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ManufacturedHomeSections element has been assigned a value.</value>
        [XmlIgnore]
        public bool ManufacturedHomeSectionsSpecified
        {
            get { return this.ManufacturedHomeSections != null && this.ManufacturedHomeSections.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public MANUFACTURED_HOME_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
