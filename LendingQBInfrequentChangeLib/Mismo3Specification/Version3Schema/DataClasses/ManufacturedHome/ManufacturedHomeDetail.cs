namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class MANUFACTURED_HOME_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the MANUFACTURED_HOME_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.LengthFeetNumberSpecified
                || this.ManufacturedHomeAttachedToFoundationDescriptionSpecified
                || this.ManufacturedHomeAttachedToFoundationIndicatorSpecified
                || this.ManufacturedHomeConditionDescriptionTypeSpecified
                || this.ManufacturedHomeConnectedToUtilitiesIndicatorSpecified
                || this.ManufacturedHomeConnectionToUtilitiesDescriptionSpecified
                || this.ManufacturedHomeConstructionQualityRatingTypeSpecified
                || this.ManufacturedHomeConstructionQualitySourceDescriptionSpecified
                || this.ManufacturedHomeDeckDescriptionSpecified
                || this.ManufacturedHomeHUDCertificationLabelAttachedIndicatorSpecified
                || this.ManufacturedHomeHUDCertificationLabelDataSourceDescriptionSpecified
                || this.ManufacturedHomeHUDDataPlateAttachedIndicatorSpecified
                || this.ManufacturedHomeHUDDataPlateDataSourceDescriptionSpecified
                || this.ManufacturedHomeHUDDataPlateIdentifierSpecified
                || this.ManufacturedHomeInstalledDateSpecified
                || this.ManufacturedHomeInstallerNameSpecified
                || this.ManufacturedHomeInteriorSpaceAcceptabilityToMarketDescriptionSpecified
                || this.ManufacturedHomeInteriorSpaceAcceptableToMarketIndicatorSpecified
                || this.ManufacturedHomeInvoiceReviewDescriptionSpecified
                || this.ManufacturedHomeInvoiceReviewedIndicatorSpecified
                || this.ManufacturedHomeManufacturerNameSpecified
                || this.ManufacturedHomeManufactureYearSpecified
                || this.ManufacturedHomeMeetsHUDRequirementsForLocationDescriptionSpecified
                || this.ManufacturedHomeMeetsHUDRequirementsForLocationIndicatorSpecified
                || this.ManufacturedHomeModelIdentifierSpecified
                || this.ManufacturedHomeModelYearSpecified
                || this.ManufacturedHomeModificationsDescriptionSpecified
                || this.ManufacturedHomePorchDescriptionSpecified
                || this.ManufacturedHomeRetailerNameSpecified
                || this.ManufacturedHomeSectionCountSpecified
                || this.ManufacturedHomeSerialNumberIdentifierSpecified
                || this.ManufacturedHomeTipOutDescriptionSpecified
                || this.ManufacturedHomeWaniganDescriptionSpecified
                || this.ManufacturedHomeWheelsDescriptionSpecified
                || this.ManufacturedHomeWheelsRemovedIndicatorSpecified
                || this.ManufacturedHomeWidthTypeSpecified
                || this.MobileHomeParkIndicatorSpecified
                || this.SquareFeetNumberSpecified
                || this.WidthFeetNumberSpecified
                || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// The length measured in linear feet.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMONumeric LengthFeetNumber;

        /// <summary>
        /// Gets or sets a value indicating whether the LengthFeetNumber element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LengthFeetNumber element has been assigned a value.</value>
        [XmlIgnore]
        public bool LengthFeetNumberSpecified
        {
            get { return LengthFeetNumber != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field describing how the manufactured home is attached to a permanent foundation.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString ManufacturedHomeAttachedToFoundationDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ManufacturedHomeAttachedToFoundationDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ManufacturedHomeAttachedToFoundationDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ManufacturedHomeAttachedToFoundationDescriptionSpecified
        {
            get { return ManufacturedHomeAttachedToFoundationDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the manufactured home is attached to its foundation.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOIndicator ManufacturedHomeAttachedToFoundationIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ManufacturedHomeAttachedToFoundationIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ManufacturedHomeAttachedToFoundationIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ManufacturedHomeAttachedToFoundationIndicatorSpecified
        {
            get { return ManufacturedHomeAttachedToFoundationIndicator != null; }
            set { }
        }

        /// <summary>
        /// The description of the condition of the manufactured home.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOEnum<ManufacturedHomeConditionDescriptionBase> ManufacturedHomeConditionDescriptionType;

        /// <summary>
        /// Gets or sets a value indicating whether the ManufacturedHomeConditionDescriptionType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ManufacturedHomeConditionDescriptionType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ManufacturedHomeConditionDescriptionTypeSpecified
        {
            get { return this.ManufacturedHomeConditionDescriptionType != null && this.ManufacturedHomeConditionDescriptionType.enumValue != ManufacturedHomeConditionDescriptionBase.Blank; }
            set { }
        }

        /// <summary>
        /// Indicates that the manufactured home is attached to utilities (e.g. electricity, gas).
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOIndicator ManufacturedHomeConnectedToUtilitiesIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ManufacturedHomeConnectedToUtilitiesIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ManufacturedHomeConnectedToUtilitiesIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ManufacturedHomeConnectedToUtilitiesIndicatorSpecified
        {
            get { return ManufacturedHomeConnectedToUtilitiesIndicator != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field describing the nature of the attachments the manufactured home has to utilities (e.g. electricity, gas).
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString ManufacturedHomeConnectionToUtilitiesDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ManufacturedHomeConnectionToUtilitiesDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ManufacturedHomeConnectionToUtilitiesDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ManufacturedHomeConnectionToUtilitiesDescriptionSpecified
        {
            get { return ManufacturedHomeConnectionToUtilitiesDescription != null; }
            set { }
        }

        /// <summary>
        /// A rating of the quality of construction of the manufactured home based on objective criteria such as NADA Manufactured Housing Appraisal Guide, Marshall and Swift Residential Cost Handbook or other published cost source.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOEnum<ManufacturedHomeConstructionQualityRatingBase> ManufacturedHomeConstructionQualityRatingType;

        /// <summary>
        /// Gets or sets a value indicating whether the ManufacturedHomeConstructionQualityRatingType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ManufacturedHomeConstructionQualityRatingType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ManufacturedHomeConstructionQualityRatingTypeSpecified
        {
            get { return this.ManufacturedHomeConstructionQualityRatingType != null && this.ManufacturedHomeConstructionQualityRatingType.enumValue != ManufacturedHomeConstructionQualityRatingBase.Blank; }
            set { }
        }

        /// <summary>
        /// Identifies the source used for the Manufactured Home Construction Quality Rating Type.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOString ManufacturedHomeConstructionQualitySourceDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ManufacturedHomeConstructionQualitySourceDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ManufacturedHomeConstructionQualitySourceDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ManufacturedHomeConstructionQualitySourceDescriptionSpecified
        {
            get { return ManufacturedHomeConstructionQualitySourceDescription != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the deck of the Manufactured home.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOString ManufacturedHomeDeckDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ManufacturedHomeDeckDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ManufacturedHomeDeckDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ManufacturedHomeDeckDescriptionSpecified
        {
            get { return ManufacturedHomeDeckDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the manufactured home has a HUD certification label attached to every segment.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOIndicator ManufacturedHomeHUDCertificationLabelAttachedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ManufacturedHomeHUDCertificationLabelAttachedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ManufacturedHomeHUDCertificationLabelAttachedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ManufacturedHomeHUDCertificationLabelAttachedIndicatorSpecified
        {
            get { return ManufacturedHomeHUDCertificationLabelAttachedIndicator != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field describing the source of the information about HUD certification label if it is not attached to the manufactured home.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOString ManufacturedHomeHUDCertificationLabelDataSourceDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ManufacturedHomeHUDCertificationLabelDataSourceDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ManufacturedHomeHUDCertificationLabelDataSourceDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ManufacturedHomeHUDCertificationLabelDataSourceDescriptionSpecified
        {
            get { return ManufacturedHomeHUDCertificationLabelDataSourceDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the manufactured home has a HUD data plate attached.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOIndicator ManufacturedHomeHUDDataPlateAttachedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ManufacturedHomeHUDDataPlateAttachedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ManufacturedHomeHUDDataPlateAttachedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ManufacturedHomeHUDDataPlateAttachedIndicatorSpecified
        {
            get { return ManufacturedHomeHUDDataPlateAttachedIndicator != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field describing the source of information about the HUD data plate if it is not attached to the manufactured home. 
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOString ManufacturedHomeHUDDataPlateDataSourceDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ManufacturedHomeHUDDataPlateDataSourceDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ManufacturedHomeHUDDataPlateDataSourceDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ManufacturedHomeHUDDataPlateDataSourceDescriptionSpecified
        {
            get { return ManufacturedHomeHUDDataPlateDataSourceDescription != null; }
            set { }
        }

        /// <summary>
        /// An identifier that is assigned by the manufacturer to an individual manufactured home unit. It may be found on the HUD Data Plate, which is affixed to the interior of the manufactured home.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOIdentifier ManufacturedHomeHUDDataPlateIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the ManufacturedHomeHUDDataPlateIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ManufacturedHomeHUDDataPlateIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool ManufacturedHomeHUDDataPlateIdentifierSpecified
        {
            get { return ManufacturedHomeHUDDataPlateIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Reports the date a Certificate of Occupancy or its equivalent was issued for the development of a lot and placement of a manufactured home on a permanent foundation.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMODate ManufacturedHomeInstalledDate;

        /// <summary>
        /// Gets or sets a value indicating whether the ManufacturedHomeInstalledDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ManufacturedHomeInstalledDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ManufacturedHomeInstalledDateSpecified
        {
            get { return ManufacturedHomeInstalledDate != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field holding the name of the installer.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOString ManufacturedHomeInstallerName;

        /// <summary>
        /// Gets or sets a value indicating whether the ManufacturedHomeInstallerName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ManufacturedHomeInstallerName element has been assigned a value.</value>
        [XmlIgnore]
        public bool ManufacturedHomeInstallerNameSpecified
        {
            get { return ManufacturedHomeInstallerName != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field describing the acceptability of the interior space of the manufactured home to the market.
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMOString ManufacturedHomeInteriorSpaceAcceptabilityToMarketDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ManufacturedHomeInteriorSpaceAcceptabilityToMarketDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ManufacturedHomeInteriorSpaceAcceptabilityToMarketDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ManufacturedHomeInteriorSpaceAcceptabilityToMarketDescriptionSpecified
        {
            get { return ManufacturedHomeInteriorSpaceAcceptabilityToMarketDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the interior space of the manufactured home is acceptable to the market.
        /// </summary>
        [XmlElement(Order = 17)]
        public MISMOIndicator ManufacturedHomeInteriorSpaceAcceptableToMarketIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ManufacturedHomeInteriorSpaceAcceptableToMarketIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ManufacturedHomeInteriorSpaceAcceptableToMarketIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ManufacturedHomeInteriorSpaceAcceptableToMarketIndicatorSpecified
        {
            get { return ManufacturedHomeInteriorSpaceAcceptableToMarketIndicator != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field describing the result of the review and analysis of the invoice of the manufacturer of the manufactured home.
        /// </summary>
        [XmlElement(Order = 18)]
        public MISMOString ManufacturedHomeInvoiceReviewDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ManufacturedHomeInvoiceReviewDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ManufacturedHomeInvoiceReviewDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ManufacturedHomeInvoiceReviewDescriptionSpecified
        {
            get { return ManufacturedHomeInvoiceReviewDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the appraiser reviewed and analyzed the invoice of the manufacturer for the manufactured home. 
        /// </summary>
        [XmlElement(Order = 19)]
        public MISMOIndicator ManufacturedHomeInvoiceReviewedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ManufacturedHomeInvoiceReviewedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ManufacturedHomeInvoiceReviewedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ManufacturedHomeInvoiceReviewedIndicatorSpecified
        {
            get { return ManufacturedHomeInvoiceReviewedIndicator != null; }
            set { }
        }

        /// <summary>
        /// The name of the manufacturer (maker) of the Manufactured home.
        /// </summary>
        [XmlElement(Order = 20)]
        public MISMOString ManufacturedHomeManufacturerName;

        /// <summary>
        /// Gets or sets a value indicating whether the ManufacturedHomeManufacturerName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ManufacturedHomeManufacturerName element has been assigned a value.</value>
        [XmlIgnore]
        public bool ManufacturedHomeManufacturerNameSpecified
        {
            get { return ManufacturedHomeManufacturerName != null; }
            set { }
        }

        /// <summary>
        /// Reports the year the manufactured home was built.
        /// </summary>
        [XmlElement(Order = 21)]
        public MISMOYear ManufacturedHomeManufactureYear;

        /// <summary>
        /// Gets or sets a value indicating whether the ManufacturedHomeManufactureYear element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ManufacturedHomeManufactureYear element has been assigned a value.</value>
        [XmlIgnore]
        public bool ManufacturedHomeManufactureYearSpecified
        {
            get { return ManufacturedHomeManufactureYear != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field describing how the manufactured home meets meet the HUD Wind Road Load and Thermal Zone requirements for its location.
        /// </summary>
        [XmlElement(Order = 22)]
        public MISMOString ManufacturedHomeMeetsHUDRequirementsForLocationDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ManufacturedHomeMeetsHUDRequirementsForLocationDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ManufacturedHomeMeetsHUDRequirementsForLocationDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ManufacturedHomeMeetsHUDRequirementsForLocationDescriptionSpecified
        {
            get { return ManufacturedHomeMeetsHUDRequirementsForLocationDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the manufactured home meets meet the HUD Wind Road Load and Thermal Zone requirements for its location.
        /// </summary>
        [XmlElement(Order = 23)]
        public MISMOIndicator ManufacturedHomeMeetsHUDRequirementsForLocationIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ManufacturedHomeMeetsHUDRequirementsForLocationIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ManufacturedHomeMeetsHUDRequirementsForLocationIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ManufacturedHomeMeetsHUDRequirementsForLocationIndicatorSpecified
        {
            get { return ManufacturedHomeMeetsHUDRequirementsForLocationIndicator != null; }
            set { }
        }

        /// <summary>
        /// The model designation of the Manufactured home (specific to the manufacturer).
        /// </summary>
        [XmlElement(Order = 24)]
        public MISMOIdentifier ManufacturedHomeModelIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the ManufacturedHomeModelIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ManufacturedHomeModelIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool ManufacturedHomeModelIdentifierSpecified
        {
            get { return ManufacturedHomeModelIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Reports the model year of the manufactured home.
        /// </summary>
        [XmlElement(Order = 25)]
        public MISMOYear ManufacturedHomeModelYear;

        /// <summary>
        /// Gets or sets a value indicating whether the ManufacturedHomeModelYear element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ManufacturedHomeModelYear element has been assigned a value.</value>
        [XmlIgnore]
        public bool ManufacturedHomeModelYearSpecified
        {
            get { return ManufacturedHomeModelYear != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field describing the modifications made to the manufactured home.
        /// </summary>
        [XmlElement(Order = 26)]
        public MISMOString ManufacturedHomeModificationsDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ManufacturedHomeModificationsDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ManufacturedHomeModificationsDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ManufacturedHomeModificationsDescriptionSpecified
        {
            get { return ManufacturedHomeModificationsDescription != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the porch area of a Manufactured home.
        /// </summary>
        [XmlElement(Order = 27)]
        public MISMOString ManufacturedHomePorchDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ManufacturedHomePorchDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ManufacturedHomePorchDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ManufacturedHomePorchDescriptionSpecified
        {
            get { return ManufacturedHomePorchDescription != null; }
            set { }
        }

        /// <summary>
        /// The name of the retailer seller of the manufactured home.
        /// </summary>
        [XmlElement(Order = 28)]
        public MISMOString ManufacturedHomeRetailerName;

        /// <summary>
        /// Gets or sets a value indicating whether the ManufacturedHomeRetailerName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ManufacturedHomeRetailerName element has been assigned a value.</value>
        [XmlIgnore]
        public bool ManufacturedHomeRetailerNameSpecified
        {
            get { return ManufacturedHomeRetailerName != null; }
            set { }
        }

        /// <summary>
        /// The number of sections of the manufactured home.
        /// </summary>
        [XmlElement(Order = 29)]
        public MISMOCount ManufacturedHomeSectionCount;

        /// <summary>
        /// Gets or sets a value indicating whether the ManufacturedHomeSectionCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ManufacturedHomeSectionCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ManufacturedHomeSectionCountSpecified
        {
            get { return ManufacturedHomeSectionCount != null; }
            set { }
        }

        /// <summary>
        /// The serial number of the manufactured home identifies the manufacturer and the state in which the manufactured home is manufactured. This may be also be called a VIN (Vehicle Identification Number).
        /// </summary>
        [XmlElement(Order = 30)]
        public MISMOIdentifier ManufacturedHomeSerialNumberIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the ManufacturedHomeSerialNumberIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ManufacturedHomeSerialNumberIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool ManufacturedHomeSerialNumberIdentifierSpecified
        {
            get { return ManufacturedHomeSerialNumberIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the tip out (extendable) portion of the Manufactured home.
        /// </summary>
        [XmlElement(Order = 31)]
        public MISMOString ManufacturedHomeTipOutDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ManufacturedHomeTipOutDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ManufacturedHomeTipOutDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ManufacturedHomeTipOutDescriptionSpecified
        {
            get { return ManufacturedHomeTipOutDescription != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe an addition built onto a trailer house for extra living or storage space.
        /// </summary>
        [XmlElement(Order = 32)]
        public MISMOString ManufacturedHomeWaniganDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the Manufactured Home Description element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Manufactured Home Description element has been assigned a value.</value>
        [XmlIgnore]
        public bool ManufacturedHomeWaniganDescriptionSpecified
        {
            get { return ManufacturedHomeWaniganDescription != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field describing the status and usability of the wheels, axels, and tires of the manufactured home. This description can report on the immobility of the home for classification purposes.
        /// </summary>
        [XmlElement(Order = 33)]
        public MISMOString ManufacturedHomeWheelsDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ManufacturedHomeWheelsDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ManufacturedHomeWheelsDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ManufacturedHomeWheelsDescriptionSpecified
        {
            get { return ManufacturedHomeWheelsDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the wheels, axels, and tires have been removed from the manufactured home .
        /// </summary>
        [XmlElement(Order = 34)]
        public MISMOIndicator ManufacturedHomeWheelsRemovedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ManufacturedHomeWheelsRemovedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ManufacturedHomeWheelsRemovedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ManufacturedHomeWheelsRemovedIndicatorSpecified
        {
            get { return ManufacturedHomeWheelsRemovedIndicator != null; }
            set { }
        }

        /// <summary>
        /// Specifies the common size (width) designation of a Manufactured home.
        /// </summary>
        [XmlElement(Order = 35)]
        public MISMOEnum<ManufacturedHomeWidthBase> ManufacturedHomeWidthType;

        /// <summary>
        /// Gets or sets a value indicating whether the ManufacturedHomeWidthType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ManufacturedHomeWidthType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ManufacturedHomeWidthTypeSpecified
        {
            get { return this.ManufacturedHomeWidthType != null && this.ManufacturedHomeWidthType.enumValue != ManufacturedHomeWidthBase.Blank; }
            set { }
        }

        /// <summary>
        /// Indicates whether the manufactured home is part of a mobile home park.
        /// </summary>
        [XmlElement(Order = 36)]
        public MISMOIndicator MobileHomeParkIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the MobileHomeParkIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MobileHomeParkIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool MobileHomeParkIndicatorSpecified
        {
            get { return MobileHomeParkIndicator != null; }
            set { }
        }

        /// <summary>
        /// Identifies the total area measured in square feet.
        /// </summary>
        [XmlElement(Order = 37)]
        public MISMONumeric SquareFeetNumber;

        /// <summary>
        /// Gets or sets a value indicating whether the SquareFeetNumber element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SquareFeetNumber element has been assigned a value.</value>
        [XmlIgnore]
        public bool SquareFeetNumberSpecified
        {
            get { return SquareFeetNumber != null; }
            set { }
        }

        /// <summary>
        /// The width measured in linear feet.
        /// </summary>
        [XmlElement(Order = 38)]
        public MISMONumeric WidthFeetNumber;

        /// <summary>
        /// Gets or sets a value indicating whether the WidthFeetNumber element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the WidthFeetNumber element has been assigned a value.</value>
        [XmlIgnore]
        public bool WidthFeetNumberSpecified
        {
            get { return WidthFeetNumber != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 39)]
        public MANUFACTURED_HOME_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null && Extension.ShouldSerialize; }
        }
    }
}
