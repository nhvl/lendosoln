namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class NEAREST_LIVING_RELATIVE_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the NEAREST_LIVING_RELATIVE_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BorrowerNearestLivingRelativeRelationshipDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// Describes the type of relationship of the nearest living relative to the Borrower.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString BorrowerNearestLivingRelativeRelationshipDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the BorrowerNearestLivingRelativeRelationshipDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BorrowerNearestLivingRelativeRelationshipDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool BorrowerNearestLivingRelativeRelationshipDescriptionSpecified
        {
            get { return BorrowerNearestLivingRelativeRelationshipDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public NEAREST_LIVING_RELATIVE_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
