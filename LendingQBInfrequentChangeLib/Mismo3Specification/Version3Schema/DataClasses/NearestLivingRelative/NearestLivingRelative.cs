namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class NEAREST_LIVING_RELATIVE
    {
        /// <summary>
        /// Gets a value indicating whether the NEAREST_LIVING_RELATIVE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AddressSpecified
                    || this.ContactPointsSpecified
                    || this.ExtensionSpecified
                    || this.NameSpecified
                    || this.NearestLivingRelativeDetailSpecified;
            }
        }

        /// <summary>
        /// Address of the nearest living relative.
        /// </summary>
        [XmlElement("ADDRESS", Order = 0)]
        public ADDRESS Address;

        /// <summary>
        /// Gets or sets a value indicating whether the Address element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Address element has been assigned a value.</value>
        [XmlIgnore]
        public bool AddressSpecified
        {
            get { return this.Address != null && this.Address.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Contact points for the nearest living relative.
        /// </summary>
        [XmlElement("CONTACT_POINTS", Order = 1)]
        public CONTACT_POINTS ContactPoints;

        /// <summary>
        /// Gets or sets a value indicating whether the ContactPoints element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ContactPoints element has been assigned a value.</value>
        [XmlIgnore]
        public bool ContactPointsSpecified
        {
            get { return this.ContactPoints != null && this.ContactPoints.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Name of the nearest living relative.
        /// </summary>
        [XmlElement("NAME", Order = 2)]
        public NAME Name;

        /// <summary>
        /// Gets or sets a value indicating whether the Name element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Name element has been assigned a value.</value>
        [XmlIgnore]
        public bool NameSpecified
        {
            get { return this.Name != null && this.Name.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details on the nearest living relative.
        /// </summary>
        [XmlElement("NEAREST_LIVING_RELATIVE_DETAIL", Order = 3)]
        public NEAREST_LIVING_RELATIVE_DETAIL NearestLivingRelativeDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the NearestLivingRelativeDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NearestLivingRelativeDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool NearestLivingRelativeDetailSpecified
        {
            get { return this.NearestLivingRelativeDetail != null && this.NearestLivingRelativeDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public NEAREST_LIVING_RELATIVE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
