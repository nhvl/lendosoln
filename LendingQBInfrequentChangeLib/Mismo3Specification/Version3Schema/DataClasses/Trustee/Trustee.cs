namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class TRUSTEE
    {
        /// <summary>
        /// Gets a value indicating whether the TRUSTEE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DeedOfTrustTrusteeIndicatorSpecified
                    || this.ExtensionSpecified
                    || this.TrusteeTypeOtherDescriptionSpecified
                    || this.TrusteeTypeSpecified;
            }
        }

        /// <summary>
        /// When true, indicates the party is a Trustee as identified on a Deed of Trust.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIndicator DeedOfTrustTrusteeIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the DeedOfTrustTrusteeIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DeedOfTrustTrusteeIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool DeedOfTrustTrusteeIndicatorSpecified
        {
            get { return DeedOfTrustTrusteeIndicator != null; }
            set { }
        }

        /// <summary>
        /// Specifies the type of trustee associated with the mortgage loan.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<TrusteeBase> TrusteeType;

        /// <summary>
        /// Gets or sets a value indicating whether the TrusteeType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TrusteeType element has been assigned a value.</value>
        [XmlIgnore]
        public bool TrusteeTypeSpecified
        {
            get { return this.TrusteeType != null && this.TrusteeType.enumValue != TrusteeBase.Blank; }
            set { }
        }

        /// <summary>
        /// When Other is specified in Trustee Type, this element contains the description.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString TrusteeTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the TrusteeTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TrusteeTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool TrusteeTypeOtherDescriptionSpecified
        {
            get { return TrusteeTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public TRUSTEE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
