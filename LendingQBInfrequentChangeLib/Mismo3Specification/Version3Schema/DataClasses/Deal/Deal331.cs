namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class DEAL_3_3_1
    {
        /// <summary>
        /// Gets a value indicating whether the DEAL_3_3_1 container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DealDetailSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// Details on a deal.
        /// </summary>
        [XmlElement("DEAL_DETAIL", Order = 0)]
        public DEAL_DETAIL DealDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the DealDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DealDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool DealDetailSpecified
        {
            get { return this.DealDetail != null && this.DealDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public DEAL_3_3_1_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
