namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class DEALS
    {
        /// <summary>
        /// Gets a value indicating whether the DEALS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DealSpecified
                    || this.ExtensionSpecified
                    || this.PartiesSpecified;
            }
        }

        /// <summary>
        /// A list of deals.
        /// </summary>
        [XmlElement("DEAL", Order = 0)]
		public List<DEAL> Deal = new List<DEAL>();

        /// <summary>
        /// Gets or sets a value indicating whether the Deal element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Deal element has been assigned a value.</value>
        [XmlIgnore]
        public bool DealSpecified
        {
            get { return this.Deal != null && this.Deal.Count(d => d != null && d.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A list of parties associated with this deal.
        /// </summary>
        [XmlElement("PARTIES", Order = 1)]
        public PARTIES Parties;

        /// <summary>
        /// Gets or sets a value indicating whether the Parties element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Parties element has been assigned a value.</value>
        [XmlIgnore]
        public bool PartiesSpecified
        {
            get { return this.Parties != null && this.Parties.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public DEALS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
