namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class DEAL_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the DEAL_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.MortgageOriginatorFirstTimeHomeBuyerEligibilityIndicatorSpecified;
            }
        }

        /// <summary>
        /// When true, indicates that the mortgage originator has determined that this deal is eligible to be considered a "First Time Home Buyer".
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIndicator MortgageOriginatorFirstTimeHomeBuyerEligibilityIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the MortgageOriginatorFirstTimeHomeBuyerEligibilityIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MortgageOriginatorFirstTimeHomeBuyerEligibilityIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool MortgageOriginatorFirstTimeHomeBuyerEligibilityIndicatorSpecified
        {
            get { return MortgageOriginatorFirstTimeHomeBuyerEligibilityIndicator != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public DEAL_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
