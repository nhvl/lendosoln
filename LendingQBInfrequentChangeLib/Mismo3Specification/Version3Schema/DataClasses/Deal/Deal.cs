namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the Mortgage Industry Standards Maintenance Organization reference model identifier can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the Mortgage Industry Standards Maintenance Organization reference model identifier can be serialized.</returns>
    public partial class DEAL
    {
        /// <summary>
        /// Gets a value indicating whether the DEAL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                if (Mismo3Utilities.ExceedsThreshold(1,
                    new List<bool> { 
                        this.ReferenceSpecified, 
                        this.AboutVersionsSpecified 
                        || this.AssetsSpecified 
                        || this.CollateralsSpecified 
                        || this.ExpensesSpecified 
                        || this.ExtensionSpecified 
                        || this.LiabilitiesSpecified
                        || this.LoansSpecified
                        || this.PartiesSpecified
                        || this.RelationshipsSpecified
                        || this.ServicesSpecified }))
                {
                    throw new System.Xml.Schema.XmlSchemaValidationException(
                        Mismo3Utilities.GetXSDChoiceExceptionMessage(
                        "DEAL",
                        new List<string> { "REFERENCE", "Any other child element of the DEAL" }));
                }

                return this.AboutVersionsSpecified
                    || this.AssetsSpecified
                    || this.CollateralsSpecified
                    || this.ExpensesSpecified
                    || this.ExtensionSpecified
                    || this.LiabilitiesSpecified
                    || this.LoansSpecified
                    || this.PartiesSpecified
                    || this.ReferenceSpecified
                    || this.RelationshipsSpecified
                    || this.ServicesSpecified;
            }
        }

        /// <summary>
        /// Holds meta-data about the deal.
        /// </summary>
        [XmlElement("ABOUT_VERSIONS", Order = 0)]
        public ABOUT_VERSIONS AboutVersions;

        /// <summary>
        /// Gets or sets a value indicating whether the AboutVersions element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AboutVersions element has been assigned a value.</value>
        [XmlIgnore]
        public bool AboutVersionsSpecified
        {
            get { return this.AboutVersions != null && this.AboutVersions.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Container element for the assets involved in the deal.
        /// </summary>
        [XmlElement("ASSETS", Order = 1)]
        public ASSETS Assets;

        /// <summary>
        /// Gets or sets a value indicating whether the Assets element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Assets element has been assigned a value.</value>
        [XmlIgnore]
        public bool AssetsSpecified
        {
            get { return Assets != null && Assets.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Container element for the collaterals involved in the deal.
        /// </summary>
        [XmlElement("COLLATERALS", Order = 2)]
        public COLLATERALS Collaterals;

        /// <summary>
        /// Gets or sets a value indicating whether the Collaterals element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Collaterals element has been assigned a value.</value>
        [XmlIgnore]
        public bool CollateralsSpecified
        {
            get { return this.Collaterals != null && this.Collaterals.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Container element for the expenses involved in the deal.
        /// </summary>
        [XmlElement("EXPENSES", Order = 3)]
        public EXPENSES Expenses;

        /// <summary>
        /// Gets or sets a value indicating whether the Expenses element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Expenses element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExpensesSpecified
        {
            get { return this.Expenses != null && this.Expenses.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public DEAL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// Container element for the liabilities involved in the deal.
        /// </summary>
        [XmlElement("LIABILITIES", Order = 5)]
        public LIABILITIES Liabilities;

        /// <summary>
        /// Gets or sets a value indicating whether the Liabilities element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Liabilities element has been assigned a value.</value>
        [XmlIgnore]
        public bool LiabilitiesSpecified
        {
            get { return this.Liabilities != null && this.Liabilities.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Container element for the loans involved in the deal.
        /// </summary>
        [XmlElement("LOANS", Order = 6)]
        public LOANS Loans;

        /// <summary>
        /// Gets or sets a value indicating whether the Loans element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Loans element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoansSpecified
        {
            get { return this.Loans != null && this.Loans.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Container element for the parties involved in the deal.
        /// </summary>
        [XmlElement("PARTIES", Order = 7)]
        public PARTIES Parties;

        /// <summary>
        /// Gets or sets a value indicating whether the Parties element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Parties element has been assigned a value.</value>
        [XmlIgnore]
        public bool PartiesSpecified
        {
            get { return this.Parties != null && this.Parties.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Container element for any references involved in the deal.
        /// </summary>
        [XmlElement("REFERENCE", Order = 8)]
        public REFERENCE Reference;

        /// <summary>
        /// Gets or sets a value indicating whether the Reference element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Reference element has been assigned a value.</value>
        [XmlIgnore]
        public bool ReferenceSpecified
        {
            get { return this.Reference != null && this.Reference.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Container element for any relationships involved in the deal.
        /// </summary>
        [XmlElement("RELATIONSHIPS", Order = 9)]
        public RELATIONSHIPS Relationships;

        /// <summary>
        /// Gets or sets a value indicating whether the Relationships element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Relationships element has been assigned a value.</value>
        [XmlIgnore]
        public bool RelationshipsSpecified
        {
            get { return this.Relationships != null && this.Relationships.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Container element for any services involved in the deal.
        /// </summary>
        [XmlElement("SERVICES", Order = 10)]
        public SERVICES Services;

        /// <summary>
        /// Gets or sets a value indicating whether the Services element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Services element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServicesSpecified
        {
            get { return this.Services != null && this.Services.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }

        /// <summary>
        /// The Mortgage Industry Standards Maintenance Organization Logical Data Dictionary Identifier is a unique value that represents the version of the Mortgage Industry Standards Maintenance Organization LDD section of the reference model to which the containing XML instance document complies.
        /// </summary>
        //// Pattern: 3\.\d*((\.\d*)?(\.\d*)?(\[[B]\d*(\-\d*)?\])?)?
        [XmlAttribute]
        public string MISMOLogicalDataDictionaryIdentifier;

        /// <summary>
        /// Indicates whether the Mortgage Industry Standards Maintenance Organization logical data dictionary identifier can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the Mortgage Industry Standards Maintenance Organization logical data dictionary identifier can be serialized.</returns>
        public bool ShouldSerializeMISMOLogicalDataDictionaryIdentifier()
        {
            return !string.IsNullOrEmpty(MISMOLogicalDataDictionaryIdentifier);
        }

        /// <summary>
        /// The Mortgage Industry Standards Maintenance Organization Reference Model Identifier is a unique value that represents the version of the Mortgage Industry Standards Maintenance Organization reference model to which the containing XML instance document complies.
        /// </summary>
        //// Pattern: 3\.\d*((\.\d*)?(\[[B]\d*(\-\d*)?\])?)?
        [XmlAttribute]
        public string MISMOReferenceModelIdentifier;

        /// <summary>
        /// Indicates whether the Mortgage Industry Standards Maintenance Organization reference model identifier can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the Mortgage Industry Standards Maintenance Organization reference model identifier can be serialized.</returns>
        public bool ShouldSerializeMISMOReferenceModelIdentifier()
        {
            return !string.IsNullOrEmpty(MISMOReferenceModelIdentifier);
        }
    }
}
