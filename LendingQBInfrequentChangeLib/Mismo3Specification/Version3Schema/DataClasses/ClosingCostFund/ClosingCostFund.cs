namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class CLOSING_COST_FUND
    {
        /// <summary>
        /// Gets a value indicating whether the CLOSING_COST_FUND container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ClosingCostFundAmountSpecified
                    || this.FundsSourceTypeOtherDescriptionSpecified
                    || this.FundsSourceTypeSpecified
                    || this.FundsTypeOtherDescriptionSpecified
                    || this.FundsTypeSpecified
                    || this.IntegratedDisclosureLineNumberValueSpecified
                    || this.IntegratedDisclosureSectionTypeOtherDescriptionSpecified
                    || this.IntegratedDisclosureSectionTypeSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// The dollar amount of funds supplied by a specified source at closing.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount ClosingCostFundAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ClosingCostFundAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ClosingCostFundAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ClosingCostFundAmountSpecified
        {
            get { return ClosingCostFundAmount != null; }
            set { }
        }

        /// <summary>
        /// The party providing the associated funds.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<FundsSourceBase> FundsSourceType;

        /// <summary>
        /// Gets or sets a value indicating whether the FundsSourceType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FundsSourceType element has been assigned a value.</value>
        [XmlIgnore]
        public bool FundsSourceTypeSpecified
        {
            get { return this.FundsSourceType != null && this.FundsSourceType.enumValue != FundsSourceBase.Blank; }
            set { }
        }

        /// <summary>
        /// Used to collect additional information when Other is selected for Funds Source Type.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString FundsSourceTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the FundsSourceTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FundsSourceTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool FundsSourceTypeOtherDescriptionSpecified
        {
            get { return FundsSourceTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A monetary source commonly used to pay obligations in a mortgage loan transaction.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOEnum<FundsBase> FundsType;

        /// <summary>
        /// Gets or sets a value indicating whether the FundsType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FundsType element has been assigned a value.</value>
        [XmlIgnore]
        public bool FundsTypeSpecified
        {
            get { return this.FundsType != null && this.FundsType.enumValue != FundsBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Funds Type.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString FundsTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the FundsTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FundsTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool FundsTypeOtherDescriptionSpecified
        {
            get { return FundsTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The line identifier for the item as represented in a section of the Integrated Disclosure form.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOValue IntegratedDisclosureLineNumberValue;

        /// <summary>
        /// Gets or sets a value indicating whether the IntegratedDisclosureLineNumberValue element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IntegratedDisclosureLineNumberValue element has been assigned a value.</value>
        [XmlIgnore]
        public bool IntegratedDisclosureLineNumberValueSpecified
        {
            get { return IntegratedDisclosureLineNumberValue != null; }
            set { }
        }

        /// <summary>
        /// The title or description used to identify a primary section of the integrated disclosure document.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOEnum<IntegratedDisclosureSectionBase> IntegratedDisclosureSectionType;

        /// <summary>
        /// Gets or sets a value indicating whether the IntegratedDisclosureSectionType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IntegratedDisclosureSectionType element has been assigned a value.</value>
        [XmlIgnore]
        public bool IntegratedDisclosureSectionTypeSpecified
        {
            get { return this.IntegratedDisclosureSectionType != null && this.IntegratedDisclosureSectionType.enumValue != IntegratedDisclosureSectionBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture additional information when Other is selected for Integrated Disclosure Section Type.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOString IntegratedDisclosureSectionTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the IntegratedDisclosureSectionTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IntegratedDisclosureSectionTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool IntegratedDisclosureSectionTypeOtherDescriptionSpecified
        {
            get { return IntegratedDisclosureSectionTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 8)]
        public CLOSING_COST_FUND_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
