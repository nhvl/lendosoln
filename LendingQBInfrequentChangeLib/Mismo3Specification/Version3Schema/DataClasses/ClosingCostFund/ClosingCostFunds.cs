namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CLOSING_COST_FUNDS
    {
        /// <summary>
        /// Gets a value indicating whether the CLOSING_COST_FUNDS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ClosingCostFundSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of closing cost funds.
        /// </summary>
        [XmlElement("CLOSING_COST_FUND", Order = 0)]
        public List<CLOSING_COST_FUND> ClosingCostFund = new List<CLOSING_COST_FUND>();

        /// <summary>
        /// Gets or sets a value indicating whether the ClosingCostFund element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ClosingCostFund element has been assigned a value.</value>
        [XmlIgnore]
        public bool ClosingCostFundSpecified
        {
            get { return ClosingCostFund != null && ClosingCostFund.Count(c => c != null && c.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public CLOSING_COST_FUNDS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
