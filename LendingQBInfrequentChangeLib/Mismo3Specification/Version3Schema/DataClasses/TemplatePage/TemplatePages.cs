namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class TEMPLATE_PAGES
    {
        /// <summary>
        /// Gets a value indicating whether the TEMPLATE_PAGES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.TemplatePageSpecified;
            }
        }

        /// <summary>
        /// A collection of template pages.
        /// </summary>
        [XmlElement("TEMPLATE_PAGE", Order = 0)]
		public List<TEMPLATE_PAGE> TemplatePage = new List<TEMPLATE_PAGE>();

        /// <summary>
        /// Gets or sets a value indicating whether the TemplatePage element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TemplatePage element has been assigned a value.</value>
        [XmlIgnore]
        public bool TemplatePageSpecified
        {
            get { return this.TemplatePage != null && this.TemplatePage.Count(t => t != null && t.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public TEMPLATE_PAGES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
