namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class STAKEHOLDER_SIGNATURE_FIELD
    {
        /// <summary>
        /// Gets a value indicating whether the STAKEHOLDER_SIGNATURE_FIELD container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.SignatureAboveLineFieldReferenceSpecified
                    || this.SignatureAreaFieldReferenceSpecified
                    || this.SignatureBelowLineFieldReferenceSpecified
                    || this.SignaturePresentationFieldReferenceSpecified
                    || this.StakeholderSignatureFieldDetailSpecified;
            }
        }

        /// <summary>
        /// Field reference for a signature above line.
        /// </summary>
        [XmlElement("SIGNATURE_ABOVE_LINE_FIELD_REFERENCE", Order = 0)]
        public FIELD_REFERENCE SignatureAboveLineFieldReference;

        /// <summary>
        /// Gets or sets a value indicating whether the SignatureAboveLineFieldReference element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SignatureAboveLineFieldReference element has been assigned a value.</value>
        [XmlIgnore]
        public bool SignatureAboveLineFieldReferenceSpecified
        {
            get { return this.SignatureAboveLineFieldReference != null && this.SignatureAboveLineFieldReference.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Field reference for a signature area.
        /// </summary>
        [XmlElement("SIGNATURE_AREA_FIELD_REFERENCE", Order = 1)]
        public FIELD_REFERENCE SignatureAreaFieldReference;

        /// <summary>
        /// Gets or sets a value indicating whether the SignatureAreaFieldReference element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SignatureAreaFieldReference element has been assigned a value.</value>
        [XmlIgnore]
        public bool SignatureAreaFieldReferenceSpecified
        {
            get { return this.SignatureAreaFieldReference != null && this.SignatureAreaFieldReference.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Field reference for a signature below line.
        /// </summary>
        [XmlElement("SIGNATURE_BELOW_LINE_FIELD_REFERENCE", Order = 2)]
        public FIELD_REFERENCE SignatureBelowLineFieldReference;

        /// <summary>
        /// Gets or sets a value indicating whether the SignatureBelowLineFieldReference element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SignatureBelowLineFieldReference element has been assigned a value.</value>
        [XmlIgnore]
        public bool SignatureBelowLineFieldReferenceSpecified
        {
            get { return this.SignatureBelowLineFieldReference != null && this.SignatureBelowLineFieldReference.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Field reference for a signature presentation.
        /// </summary>
        [XmlElement("SIGNATURE_PRESENTATION_FIELD_REFERENCE", Order = 3)]
        public FIELD_REFERENCE SignaturePresentationFieldReference;

        /// <summary>
        /// Gets or sets a value indicating whether the SignaturePresentationFieldReference element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SignaturePresentationFieldReference element has been assigned a value.</value>
        [XmlIgnore]
        public bool SignaturePresentationFieldReferenceSpecified
        {
            get { return this.SignaturePresentationFieldReference != null && this.SignaturePresentationFieldReference.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details on a stakeholder signature.
        /// </summary>
        [XmlElement("STAKEHOLDER_SIGNATURE_FIELD_DETAIL", Order = 4)]
        public STAKEHOLDER_SIGNATURE_FIELD_DETAIL StakeholderSignatureFieldDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the StakeholderSignatureFieldDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the StakeholderSignatureFieldDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool StakeholderSignatureFieldDetailSpecified
        {
            get { return this.StakeholderSignatureFieldDetail != null && this.StakeholderSignatureFieldDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 5)]
        public STAKEHOLDER_SIGNATURE_FIELD_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
