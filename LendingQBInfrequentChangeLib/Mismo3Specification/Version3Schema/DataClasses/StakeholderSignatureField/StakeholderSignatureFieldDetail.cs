namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class STAKEHOLDER_SIGNATURE_FIELD_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the STAKEHOLDER_SIGNATURE_FIELD_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.MutuallyExclusiveSignatureGroupNameSpecified
                    || this.SignatureFieldMarkTypeSpecified
                    || this.SignatureFieldRequiredIndicatorSpecified
                    || this.SignatureTypeOtherDescriptionSpecified
                    || this.SignatureTypeSpecified;
            }
        }

        /// <summary>
        /// Specifies the name of the mutually exclusive group this signature field belongs to. A mutually exclusive signature group is the set of signature fields that share the same value for this element. At any given time only one of the signatures in the group can be signed. This feature is used to define signature fields, including initials marks, that are used to express a choice of the signer with regards to specific provisions offered in the document.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString MutuallyExclusiveSignatureGroupName;

        /// <summary>
        /// Gets or sets a value indicating whether the MutuallyExclusiveSignatureGroupName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MutuallyExclusiveSignatureGroupName element has been assigned a value.</value>
        [XmlIgnore]
        public bool MutuallyExclusiveSignatureGroupNameSpecified
        {
            get { return MutuallyExclusiveSignatureGroupName != null; }
            set { }
        }

        /// <summary>
        /// Specifies whether the signature field is for a full signature or for an initials mark.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<SignatureFieldMarkBase> SignatureFieldMarkType;

        /// <summary>
        /// Gets or sets a value indicating whether the SignatureFieldMarkType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SignatureFieldMarkType element has been assigned a value.</value>
        [XmlIgnore]
        public bool SignatureFieldMarkTypeSpecified
        {
            get { return this.SignatureFieldMarkType != null && this.SignatureFieldMarkType.enumValue != SignatureFieldMarkBase.Blank; }
            set { }
        }

        /// <summary>
        /// Specifies whether the signature is required or optional. A value of true indicates that the signature must be executed in order for the document to be considered fully executed. A value of false indicates that the signature is optional and does not need to be executed in certain circumstances.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOIndicator SignatureFieldRequiredIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the SignatureFieldRequiredIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SignatureFieldRequiredIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool SignatureFieldRequiredIndicatorSpecified
        {
            get { return SignatureFieldRequiredIndicator != null; }
            set { }
        }

        /// <summary>
        /// Contains the signature type proposed by the document creator. The actual type of the signature applied to the signature field may be different (please see audit trail).
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOEnum<SignatureBase> SignatureType;

        /// <summary>
        /// Gets or sets a value indicating whether the SignatureType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SignatureType element has been assigned a value.</value>
        [XmlIgnore]
        public bool SignatureTypeSpecified
        {
            get { return this.SignatureType != null && this.SignatureType.enumValue != SignatureBase.Blank; }
            set { }
        }

        /// <summary>
        /// Contains a description of the signature type when the value of Other is used for the SignatureType.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString SignatureTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the SignatureTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SignatureTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool SignatureTypeOtherDescriptionSpecified
        {
            get { return SignatureTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 5)]
        public STAKEHOLDER_SIGNATURE_FIELD_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
