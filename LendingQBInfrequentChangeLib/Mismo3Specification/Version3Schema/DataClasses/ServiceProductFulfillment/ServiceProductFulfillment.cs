namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class SERVICE_PRODUCT_FULFILLMENT
    {
        /// <summary>
        /// Gets a value indicating whether the SERVICE_PRODUCT_FULFILLMENT container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ContactPointsSpecified
                    || this.ExtensionSpecified
                    || this.ServiceProductFulfillmentDetailSpecified;
            }
        }

        /// <summary>
        /// Contact points for a service product.
        /// </summary>
        [XmlElement("CONTACT_POINTS", Order = 0)]
        public CONTACT_POINTS ContactPoints;

        /// <summary>
        /// Gets or sets a value indicating whether the ContactPoints element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ContactPoints element has been assigned a value.</value>
        [XmlIgnore]
        public bool ContactPointsSpecified
        {
            get { return this.ContactPoints != null && this.ContactPoints.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details on a service product fulfillment.
        /// </summary>
        [XmlElement("SERVICE_PRODUCT_FULFILLMENT_DETAIL", Order = 1)]
        public SERVICE_PRODUCT_FULFILLMENT_DETAIL ServiceProductFulfillmentDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the ServiceProductFulfillmentDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServiceProductFulfillmentDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServiceProductFulfillmentDetailSpecified
        {
            get { return this.ServiceProductFulfillmentDetail != null && this.ServiceProductFulfillmentDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public SERVICE_PRODUCT_FULFILLMENT_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
