namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class SERVICE_PRODUCT_FULFILLMENT_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the SERVICE_PRODUCT_FULFILLMENT_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ServiceActualCompletionDateSpecified
                    || this.ServiceActualPriceAmountSpecified
                    || this.ServiceCancellationPriceAmountSpecified
                    || this.ServiceEstimatedCompletionDateSpecified
                    || this.ServiceEstimatedOffHoldDateSpecified
                    || this.ServiceEstimatedPriceAmountSpecified
                    || this.ServiceRangeEndDateSpecified
                    || this.ServiceRangeStartDateSpecified
                    || this.ServiceRequestedCompletionDateSpecified
                    || this.VendorOrderIdentifierSpecified
                    || this.VendorTransactionIdentifierSpecified;
            }
        }

        /// <summary>
        /// The actual date of completion of the product/service being fulfilled.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMODate ServiceActualCompletionDate;

        /// <summary>
        /// Gets or sets a value indicating whether the ServiceActualCompletionDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServiceActualCompletionDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServiceActualCompletionDateSpecified
        {
            get { return ServiceActualCompletionDate != null; }
            set { }
        }

        /// <summary>
        /// The actual price of the product/service being fulfilled.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOAmount ServiceActualPriceAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ServiceActualPriceAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServiceActualPriceAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServiceActualPriceAmountSpecified
        {
            get { return ServiceActualPriceAmount != null; }
            set { }
        }

        /// <summary>
        /// The price that the service provider charged for the cancellation of the order.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOAmount ServiceCancellationPriceAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ServiceCancellationPriceAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServiceCancellationPriceAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServiceCancellationPriceAmountSpecified
        {
            get { return ServiceCancellationPriceAmount != null; }
            set { }
        }

        /// <summary>
        /// The estimated date of completion of the product/service being fulfilled.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMODate ServiceEstimatedCompletionDate;

        /// <summary>
        /// Gets or sets a value indicating whether the ServiceEstimatedCompletionDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServiceEstimatedCompletionDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServiceEstimatedCompletionDateSpecified
        {
            get { return ServiceEstimatedCompletionDate != null; }
            set { }
        }

        /// <summary>
        /// The estimated date that resolution to the current issue/problem will be resolved.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMODate ServiceEstimatedOffHoldDate;

        /// <summary>
        /// Gets or sets a value indicating whether the ServiceEstimatedOffHoldDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServiceEstimatedOffHoldDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServiceEstimatedOffHoldDateSpecified
        {
            get { return ServiceEstimatedOffHoldDate != null; }
            set { }
        }

        /// <summary>
        /// The estimated price of the product/service being fulfilled.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOAmount ServiceEstimatedPriceAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ServiceEstimatedPriceAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServiceEstimatedPriceAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServiceEstimatedPriceAmountSpecified
        {
            get { return ServiceEstimatedPriceAmount != null; }
            set { }
        }

        /// <summary>
        /// For the request/response of products that contain a range of dates it is needed to identify the date range that that product delivery satisfies.   This element identifies the start of the period being identified.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMODate ServiceRangeEndDate;

        /// <summary>
        /// Gets or sets a value indicating whether the ServiceRangeEndDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServiceRangeEndDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServiceRangeEndDateSpecified
        {
            get { return ServiceRangeEndDate != null; }
            set { }
        }

        /// <summary>
        /// For the request/response of products that contain a range of dates it is needed to identify the date range that that product delivery satisfies.   This element identifies the end of the period being identified.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMODate ServiceRangeStartDate;

        /// <summary>
        /// Gets or sets a value indicating whether the ServiceRangeStartDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServiceRangeStartDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServiceRangeStartDateSpecified
        {
            get { return ServiceRangeStartDate != null; }
            set { }
        }

        /// <summary>
        /// The date that the service requestor  would like the execution of the the service to be completed and the results of the service provided.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMODate ServiceRequestedCompletionDate;

        /// <summary>
        /// Gets or sets a value indicating whether the ServiceRequestedCompletionDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServiceRequestedCompletionDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServiceRequestedCompletionDateSpecified
        {
            get { return ServiceRequestedCompletionDate != null; }
            set { }
        }

        /// <summary>
        /// A string that uniquely identifies a specific order for a vendor set by the vendor.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOIdentifier VendorOrderIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the VendorOrderIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the VendorOrderIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool VendorOrderIdentifierSpecified
        {
            get { return VendorOrderIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A string that uniquely identifies a specific transaction/file for a vendor set by the vendor.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOIdentifier VendorTransactionIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the VendorTransactionIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the VendorTransactionIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool VendorTransactionIdentifierSpecified
        {
            get { return VendorTransactionIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 11)]
        public SERVICE_PRODUCT_FULFILLMENT_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
