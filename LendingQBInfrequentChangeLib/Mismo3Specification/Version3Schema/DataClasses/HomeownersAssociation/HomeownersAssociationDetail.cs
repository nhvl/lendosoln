namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class HOMEOWNERS_ASSOCIATION_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the HOMEOWNERS_ASSOCIATION_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.HomeownersAssociationAccountIdentifierSpecified;
            }
        }

        /// <summary>
        /// A unique alphanumeric string identifying the account associated with a property as assigned by a homeowners association.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIdentifier HomeownersAssociationAccountIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the HomeownersAssociationAccountIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HomeownersAssociationAccountIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool HomeownersAssociationAccountIdentifierSpecified
        {
            get { return HomeownersAssociationAccountIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public HOMEOWNERS_ASSOCIATION_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
