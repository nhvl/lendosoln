namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class HOMEOWNERS_ASSOCIATION_3_3_1
    {
        /// <summary>
        /// Gets a value indicating whether the HOMEOWNERS_ASSOCIATION_3_3_1 container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.HomeownersAssociationDetailSpecified
                    || this.VerificationSpecified;
            }
        }

        /// <summary>
        /// Details on the HOA.
        /// </summary>
        [XmlElement("HOMEOWNERS_ASSOCIATION_DETAIL", Order = 0)]
        public HOMEOWNERS_ASSOCIATION_DETAIL HomeownersAssociationDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the HomeownersAssociationDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HomeownersAssociationDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool HomeownersAssociationDetailSpecified
        {
            get { return this.HomeownersAssociationDetail != null && this.HomeownersAssociationDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// HOA verification.
        /// </summary>
        [XmlElement("VERIFICATION", Order = 1)]
        public VERIFICATION Verification;

        /// <summary>
        /// Gets or sets a value indicating whether the Verification element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Verification element has been assigned a value.</value>
        [XmlIgnore]
        public bool VerificationSpecified
        {
            get { return this.Verification != null && this.Verification.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public HOMEOWNERS_ASSOCIATION_3_3_1_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
