namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class HOMEOWNERS_ASSOCIATION
    {
        /// <summary>
        /// Gets a value indicating whether the HOMEOWNERS_ASSOCIATION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AddressSpecified
                    || this.ExtensionSpecified
                    || this.LegalEntitySpecified
                    || this.ManagementAgentSpecified;
            }
        }

        /// <summary>
        /// The address of the HOA.
        /// </summary>
        [XmlElement("ADDRESS", Order = 0)]
        public ADDRESS Address;

        /// <summary>
        /// Gets or sets a value indicating whether the Address element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Address element has been assigned a value.</value>
        [XmlIgnore]
        public bool AddressSpecified
        {
            get { return this.Address != null && this.Address.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The legal entity of the HOA.
        /// </summary>
        [XmlElement("LEGAL_ENTITY", Order = 1)]
        public LEGAL_ENTITY LegalEntity;

        /// <summary>
        /// Gets or sets a value indicating whether the LegalEntity element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LegalEntity element has been assigned a value.</value>
        [XmlIgnore]
        public bool LegalEntitySpecified
        {
            get { return this.LegalEntity != null && this.LegalEntity.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The HOA management agent.
        /// </summary>
        [XmlElement("MANAGEMENT_AGENT", Order = 2)]
        public MANAGEMENT_AGENT ManagementAgent;

        /// <summary>
        /// Gets or sets a value indicating whether the ManagementAgent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ManagementAgent element has been assigned a value.</value>
        [XmlIgnore]
        public bool ManagementAgentSpecified
        {
            get { return this.ManagementAgent != null && this.ManagementAgent.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public HOMEOWNERS_ASSOCIATION_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
