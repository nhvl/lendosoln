namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class MI_RATE_QUOTE_PRODUCTS
    {
        /// <summary>
        /// Gets a value indicating whether the MI_RATE_QUOTE_PRODUCTS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.MIRateQuoteProductSpecified;
            }
        }

        /// <summary>
        /// Rate quote on a mortgage insurance product.
        /// </summary>
        [XmlElement("MI_RATE_QUOTE_PRODUCT", Order = 0)]
		public List<MI_RATE_QUOTE_PRODUCT> MIRateQuoteProduct = new List<MI_RATE_QUOTE_PRODUCT>();

        /// <summary>
        /// Gets or sets a value indicating whether the Mortgage Insurance Rate Quote Product element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Mortgage Insurance Rate Quote Product element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIRateQuoteProductSpecified
        {
            get { return this.MIRateQuoteProduct != null && this.MIRateQuoteProduct.Count(m => m != null && m.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public MI_RATE_QUOTE_PRODUCTS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
