namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class MI_RATE_QUOTE_PRODUCT_3_3_1
    {
        /// <summary>
        /// Gets a value indicating whether the MI_RATE_QUOTE_PRODUCT_3_3_1 container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.LoanIdentifiersSpecified;
            }
        }

        /// <summary>
        /// A list of loan identifiers.
        /// </summary>
        [XmlElement("LOAN_IDENTIFIERS", Order = 0)]
        public LOAN_IDENTIFIERS LoanIdentifiers;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanIdentifiers element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanIdentifiers element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanIdentifiersSpecified
        {
            get { return this.LoanIdentifiers != null && this.LoanIdentifiers.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public MI_RATE_QUOTE_PRODUCT_3_3_1_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
