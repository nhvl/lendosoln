namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class MI_RATE_QUOTE_PRODUCT_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the MI_RATE_QUOTE_PRODUCT_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BaseLoanAmountSpecified
                    || this.ExtensionSpecified
                    || this.FiveYearCostComparisonAmountSpecified
                    || this.HousingCostOverFiveYearsAmountSpecified
                    || this.MIDurationTypeOtherDescriptionSpecified
                    || this.MIDurationTypeSpecified
                    || this.MIEligibleIndicatorSpecified
                    || this.MIPremiumFinancedAmountSpecified
                    || this.MIPremiumFinancedIndicatorSpecified
                    || this.MIPremiumRefundableTypeOtherDescriptionSpecified
                    || this.MIPremiumRefundableTypeSpecified
                    || this.MIPremiumSourceTypeOtherDescriptionSpecified
                    || this.MIPremiumSourceTypeSpecified
                    || this.MIPremiumUpfrontAmountSpecified
                    || this.MIPremiumUpfrontPercentSpecified
                    || this.MIProductDescriptionSpecified
                    || this.MIRateFoundIndicatorSpecified
                    || this.MIRenewalCalculationTypeOtherDescriptionSpecified
                    || this.MIRenewalCalculationTypeSpecified;
            }
        }

        /// <summary>
        /// The base loan amount to be loaned to the borrower not including PMI, MIP, or Funding Fee.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount BaseLoanAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the BaseLoanAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BaseLoanAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool BaseLoanAmountSpecified
        {
            get { return BaseLoanAmount != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount difference between multiple options when comparing the costs of different products, may be negative or positive. Based on the total housing cost summed up over a five year period. 
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOAmount FiveYearCostComparisonAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the FiveYearCostComparisonAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FiveYearCostComparisonAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool FiveYearCostComparisonAmountSpecified
        {
            get { return FiveYearCostComparisonAmount != null; }
            set { }
        }

        /// <summary>
        /// The total housing cost summed up over a five year period.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOAmount HousingCostOverFiveYearsAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the HousingCostOverFiveYearsAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HousingCostOverFiveYearsAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool HousingCostOverFiveYearsAmountSpecified
        {
            get { return HousingCostOverFiveYearsAmount != null; }
            set { }
        }

        /// <summary>
        /// Specifies the period of time for which mortgage insurance coverage has been obtained/paid.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOEnum<MIDurationBase> MIDurationType;

        /// <summary>
        /// Gets or sets a value indicating whether the MIDurationType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIDurationType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIDurationTypeSpecified
        {
            get { return this.MIDurationType != null && this.MIDurationType.enumValue != MIDurationBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture the description of the type of MI Duration if Other is selected as the MIDurationType.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString MIDurationTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MIDurationTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIDurationTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIDurationTypeOtherDescriptionSpecified
        {
            get { return MIDurationTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the loan is eligible for MI based upon the MI Product and the data provided.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOIndicator MIEligibleIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the MIEligibleIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIEligibleIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIEligibleIndicatorSpecified
        {
            get { return MIEligibleIndicator != null; }
            set { }
        }

        /// <summary>
        /// The amount of the up-front premium that is financed.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOAmount MIPremiumFinancedAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPremiumFinancedAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPremiumFinancedAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumFinancedAmountSpecified
        {
            get { return MIPremiumFinancedAmount != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether mortgage insurance premium has been added to loan amount.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOIndicator MIPremiumFinancedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPremiumFinancedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPremiumFinancedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumFinancedIndicatorSpecified
        {
            get { return MIPremiumFinancedIndicator != null; }
            set { }
        }

        /// <summary>
        /// Specifies how the unearned portion of any private mortgage insurance premiums will be treated if the private mortgage insurance coverage is canceled.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOEnum<MIPremiumRefundableBase> MIPremiumRefundableType;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPremiumRefundableType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPremiumRefundableType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumRefundableTypeSpecified
        {
            get { return this.MIPremiumRefundableType != null && this.MIPremiumRefundableType.enumValue != MIPremiumRefundableBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture the description of the type of MI Premium Refundable if Other is selected as the MIPremiumRefundableType.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOString MIPremiumRefundableTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPremiumRefundableTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPremiumRefundableTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumRefundableTypeOtherDescriptionSpecified
        {
            get { return MIPremiumRefundableTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Defines the source of the MI premium payment.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOEnum<MIPremiumSourceBase> MIPremiumSourceType;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPremiumSourceType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPremiumSourceType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumSourceTypeSpecified
        {
            get { return this.MIPremiumSourceType != null && this.MIPremiumSourceType.enumValue != MIPremiumSourceBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field to collect MI premium source when Other is selected for MI Premium Source Type.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOString MIPremiumSourceTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPremiumSourceTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPremiumSourceTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumSourceTypeOtherDescriptionSpecified
        {
            get { return MIPremiumSourceTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Reflects the actual up-front MI Premium percent associated with the plan.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOAmount MIPremiumUpfrontAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPremiumUpfrontAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPremiumUpfrontAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumUpfrontAmountSpecified
        {
            get { return MIPremiumUpfrontAmount != null; }
            set { }
        }

        /// <summary>
        /// Reflects the actual up-front MI Premium percent associated with the plan.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOPercent MIPremiumUpfrontPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPremiumUpfrontPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPremiumUpfrontPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPremiumUpfrontPercentSpecified
        {
            get { return MIPremiumUpfrontPercent != null; }
            set { }
        }

        /// <summary>
        /// The description of a particular mortgage insurance product.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOString MIProductDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MIProductDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIProductDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIProductDescriptionSpecified
        {
            get { return MIProductDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates that an MI Rate was found based on the MI Product and the data provided.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOIndicator MIRateFoundIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the MIRateFoundIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIRateFoundIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIRateFoundIndicatorSpecified
        {
            get { return MIRateFoundIndicator != null; }
            set { }
        }

        /// <summary>
        /// Specifies the way in which the renewal premiums will be calculated.
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMOEnum<MIRenewalCalculationBase> MIRenewalCalculationType;

        /// <summary>
        /// Gets or sets a value indicating whether the MIRenewalCalculationType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIRenewalCalculationType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIRenewalCalculationTypeSpecified
        {
            get { return this.MIRenewalCalculationType != null && this.MIRenewalCalculationType.enumValue != MIRenewalCalculationBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for MI Renewal Calculation Type.
        /// </summary>
        [XmlElement(Order = 17)]
        public MISMOString MIRenewalCalculationTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MIRenewalCalculationTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIRenewalCalculationTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIRenewalCalculationTypeOtherDescriptionSpecified
        {
            get { return MIRenewalCalculationTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 18)]
        public MI_RATE_QUOTE_PRODUCT_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
