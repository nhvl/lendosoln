namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class HOUSING
    {
        /// <summary>
        /// Gets a value indicating whether the HOUSING container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.NeighborhoodBuiltUpRangeTypeSpecified
                    || this.NeighborhoodDemandSupplyTypeSpecified
                    || this.NeighborhoodHousingHighPriceAmountSpecified
                    || this.NeighborhoodHousingLowPriceAmountSpecified
                    || this.NeighborhoodHousingNewestYearsCountSpecified
                    || this.NeighborhoodHousingOldestYearsCountSpecified
                    || this.NeighborhoodHousingPredominantAgeYearsCountSpecified
                    || this.NeighborhoodHousingPredominantOccupancyTypeSpecified
                    || this.NeighborhoodHousingPredominantPriceAmountSpecified
                    || this.NeighborhoodHousingTypeOtherDescriptionSpecified
                    || this.NeighborhoodHousingTypeSpecified
                    || this.NeighborhoodOwnerOccupancyPercentSpecified
                    || this.NeighborhoodPropertyValueTrendTypeSpecified
                    || this.NeighborhoodTypicalMarketingDaysDurationCountSpecified
                    || this.NeighborhoodTypicalMarketingMonthsDurationTypeSpecified;
            }
        }

        /// <summary>
        /// Specifies an estimated percentage range of available land in the neighborhood that has been improved.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<NeighborhoodBuiltUpRangeBase> NeighborhoodBuiltUpRangeType;

        /// <summary>
        /// Gets or sets a value indicating whether the NeighborhoodBuiltUpRangeType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NeighborhoodBuiltUpRangeType element has been assigned a value.</value>
        [XmlIgnore]
        public bool NeighborhoodBuiltUpRangeTypeSpecified
        {
            get { return this.NeighborhoodBuiltUpRangeType != null && this.NeighborhoodBuiltUpRangeType.enumValue != NeighborhoodBuiltUpRangeBase.Blank; }
            set { }
        }

        /// <summary>
        /// Specifies the state of market demand versus the supply of properties in the neighborhood.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<NeighborhoodDemandSupplyBase> NeighborhoodDemandSupplyType;

        /// <summary>
        /// Gets or sets a value indicating whether the NeighborhoodDemandSupplyType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NeighborhoodDemandSupplyType element has been assigned a value.</value>
        [XmlIgnore]
        public bool NeighborhoodDemandSupplyTypeSpecified
        {
            get { return this.NeighborhoodDemandSupplyType != null && this.NeighborhoodDemandSupplyType.enumValue != NeighborhoodDemandSupplyBase.Blank; }
            set { }
        }

        /// <summary>
        /// The high price of the neighborhood housing specified in Neighborhood Housing Type.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOAmount NeighborhoodHousingHighPriceAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the NeighborhoodHousingHighPriceAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NeighborhoodHousingHighPriceAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool NeighborhoodHousingHighPriceAmountSpecified
        {
            get { return NeighborhoodHousingHighPriceAmount != null; }
            set { }
        }

        /// <summary>
        /// The low price of the neighborhood housing specified in Neighborhood Housing Type.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOAmount NeighborhoodHousingLowPriceAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the NeighborhoodHousingLowPriceAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NeighborhoodHousingLowPriceAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool NeighborhoodHousingLowPriceAmountSpecified
        {
            get { return NeighborhoodHousingLowPriceAmount != null; }
            set { }
        }

        /// <summary>
        /// The age, in years, of the newest neighborhood housing specified in Neighborhood Housing Type.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOCount NeighborhoodHousingNewestYearsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the NeighborhoodHousingNewestYearsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NeighborhoodHousingNewestYearsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool NeighborhoodHousingNewestYearsCountSpecified
        {
            get { return NeighborhoodHousingNewestYearsCount != null; }
            set { }
        }

        /// <summary>
        /// The age, in years, of the oldest neighborhood housing specified in Neighborhood Housing Type.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOCount NeighborhoodHousingOldestYearsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the NeighborhoodHousingOldestYearsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NeighborhoodHousingOldestYearsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool NeighborhoodHousingOldestYearsCountSpecified
        {
            get { return NeighborhoodHousingOldestYearsCount != null; }
            set { }
        }

        /// <summary>
        /// The predominate age, in years, of the neighborhood housing specified by Neighborhood Housing Type.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOCount NeighborhoodHousingPredominantAgeYearsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the NeighborhoodHousingPredominantAgeYearsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NeighborhoodHousingPredominantAgeYearsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool NeighborhoodHousingPredominantAgeYearsCountSpecified
        {
            get { return NeighborhoodHousingPredominantAgeYearsCount != null; }
            set { }
        }

        /// <summary>
        /// The predominant occupancy for the specified Neighborhood Housing Type.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOEnum<NeighborhoodHousingPredominantOccupancyBase> NeighborhoodHousingPredominantOccupancyType;

        /// <summary>
        /// Gets or sets a value indicating whether the NeighborhoodHousingPredominantOccupancyType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NeighborhoodHousingPredominantOccupancyType element has been assigned a value.</value>
        [XmlIgnore]
        public bool NeighborhoodHousingPredominantOccupancyTypeSpecified
        {
            get { return this.NeighborhoodHousingPredominantOccupancyType != null && this.NeighborhoodHousingPredominantOccupancyType.enumValue != NeighborhoodHousingPredominantOccupancyBase.Blank; }
            set { }
        }

        /// <summary>
        /// The predominate price of the neighborhood housing specified by Neighborhood Housing Type.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOAmount NeighborhoodHousingPredominantPriceAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the NeighborhoodHousingPredominantPriceAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NeighborhoodHousingPredominantPriceAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool NeighborhoodHousingPredominantPriceAmountSpecified
        {
            get { return NeighborhoodHousingPredominantPriceAmount != null; }
            set { }
        }

        /// <summary>
        /// Specifies a type of housing found in the neighborhood.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOEnum<NeighborhoodHousingBase> NeighborhoodHousingType;

        /// <summary>
        /// Gets or sets a value indicating whether the NeighborhoodHousingType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NeighborhoodHousingType element has been assigned a value.</value>
        [XmlIgnore]
        public bool NeighborhoodHousingTypeSpecified
        {
            get { return this.NeighborhoodHousingType != null && this.NeighborhoodHousingType.enumValue != NeighborhoodHousingBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the neighborhood housing type if Other is selected as the Neighborhood Housing Type.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOString NeighborhoodHousingTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the NeighborhoodHousingTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NeighborhoodHousingTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool NeighborhoodHousingTypeOtherDescriptionSpecified
        {
            get { return NeighborhoodHousingTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The percentage of owner occupied in the specified neighborhood.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOPercent NeighborhoodOwnerOccupancyPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the NeighborhoodOwnerOccupancyPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NeighborhoodOwnerOccupancyPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool NeighborhoodOwnerOccupancyPercentSpecified
        {
            get { return NeighborhoodOwnerOccupancyPercent != null; }
            set { }
        }

        /// <summary>
        /// Specifies the current trend of property values in the neighborhood.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOEnum<NeighborhoodPropertyValueTrendBase> NeighborhoodPropertyValueTrendType;

        /// <summary>
        /// Gets or sets a value indicating whether the NeighborhoodPropertyValueTrendType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NeighborhoodPropertyValueTrendType element has been assigned a value.</value>
        [XmlIgnore]
        public bool NeighborhoodPropertyValueTrendTypeSpecified
        {
            get { return this.NeighborhoodPropertyValueTrendType != null && this.NeighborhoodPropertyValueTrendType.enumValue != NeighborhoodPropertyValueTrendBase.Blank; }
            set { }
        }

        /// <summary>
        /// Specifies the length of time in terms of days that a property typical to the neighborhood would stay on the market before being sold.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOCount NeighborhoodTypicalMarketingDaysDurationCount;

        /// <summary>
        /// Gets or sets a value indicating whether the NeighborhoodTypicalMarketingDaysDurationCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NeighborhoodTypicalMarketingDaysDurationCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool NeighborhoodTypicalMarketingDaysDurationCountSpecified
        {
            get { return NeighborhoodTypicalMarketingDaysDurationCount != null; }
            set { }
        }

        /// <summary>
        /// Specifies the length of time in terms of months that a property typical to the neighborhood would stay on the market before being sold.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOEnum<NeighborhoodTypicalMarketingMonthsDurationBase> NeighborhoodTypicalMarketingMonthsDurationType;

        /// <summary>
        /// Gets or sets a value indicating whether the NeighborhoodTypicalMarketingMonthsDurationType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NeighborhoodTypicalMarketingMonthsDurationType element has been assigned a value.</value>
        [XmlIgnore]
        public bool NeighborhoodTypicalMarketingMonthsDurationTypeSpecified
        {
            get { return this.NeighborhoodTypicalMarketingMonthsDurationType != null && this.NeighborhoodTypicalMarketingMonthsDurationType.enumValue != NeighborhoodTypicalMarketingMonthsDurationBase.Blank; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 15)]
        public HOUSING_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
