namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class FEE_INFORMATION
    {
        /// <summary>
        /// Gets a value indicating whether the FEE_INFORMATION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.FeesSpecified
                    || this.FeesSummarySpecified;
            }
        }

        /// <summary>
        /// A list of fees.
        /// </summary>
        [XmlElement("FEES", Order = 0)]
        public FEES Fees;

        /// <summary>
        /// Gets or sets a value indicating whether the Fees element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Fees element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeesSpecified
        {
            get { return this.Fees != null && this.Fees.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A summary of associated fees.
        /// </summary>
        [XmlElement("FEES_SUMMARY", Order = 1)]
        public FEES_SUMMARY FeesSummary;

        /// <summary>
        /// Gets or sets a value indicating whether the FeesSummary element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FeesSummary element has been assigned a value.</value>
        [XmlIgnore]
        public bool FeesSummarySpecified
        {
            get { return this.FeesSummary != null && this.FeesSummary.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public FEE_INFORMATION_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
