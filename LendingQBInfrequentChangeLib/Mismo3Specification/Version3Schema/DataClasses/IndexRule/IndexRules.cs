namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class INDEX_RULES
    {
        /// <summary>
        /// Gets a value indicating whether the INDEX_RULES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.IndexRuleSpecified;
            }
        }

        /// <summary>
        /// A collection of index rules.
        /// </summary>
        [XmlElement("INDEX_RULE", Order = 0)]
		public List<INDEX_RULE> IndexRule = new List<INDEX_RULE>();

        /// <summary>
        /// Gets or sets a value indicating whether the IndexRule element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IndexRule element has been assigned a value.</value>
        [XmlIgnore]
        public bool IndexRuleSpecified
        {
            get { return this.IndexRule != null && this.IndexRule.Count(i => i != null && i.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public INDEX_RULES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
