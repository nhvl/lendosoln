namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class INDEX_RULE
    {
        /// <summary>
        /// Gets a value indicating whether the INDEX_RULE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.IndexAveragingIndicatorSpecified
                    || this.IndexAveragingTypeSpecified
                    || this.IndexAveragingValueCountSpecified
                    || this.IndexCalculationMethodTypeSpecified
                    || this.IndexDesignationTypeSpecified
                    || this.IndexLeadMonthsCountSpecified
                    || this.IndexLookbackTypeOtherDescriptionSpecified
                    || this.IndexLookbackTypeSpecified
                    || this.IndexRoundingPercentSpecified
                    || this.IndexRoundingTimingTypeSpecified
                    || this.IndexRoundingTypeSpecified
                    || this.IndexSourceTypeOtherDescriptionSpecified
                    || this.IndexSourceTypeSpecified
                    || this.IndexTypeOtherDescriptionSpecified
                    || this.IndexTypeSpecified
                    || this.InterestAdjustmentIndexLeadDaysCountSpecified
                    || this.InterestAndPaymentAdjustmentIndexLeadDaysCountSpecified
                    || this.MinimumIndexMovementRatePercentSpecified
                    || this.PaymentsBetweenIndexValuesCountSpecified
                    || this.PaymentsToFirstIndexValueCountSpecified;
            }
        }

        /// <summary>
        /// Indicates that several interest rates are to be averaged to calculate the new interest rate for an ARM change. The average interest rate value, if rate averaging is indicated, is used when the interest rate calculation method indicates some use of a specified rate.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIndicator IndexAveragingIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the IndexAveragingIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IndexAveragingIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool IndexAveragingIndicatorSpecified
        {
            get { return IndexAveragingIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates if and how multiple index values are to be averaged to arrive at the value to be used in the interest rate calculation.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<IndexAveragingBase> IndexAveragingType;

        /// <summary>
        /// Gets or sets a value indicating whether the IndexAveragingType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IndexAveragingType element has been assigned a value.</value>
        [XmlIgnore]
        public bool IndexAveragingTypeSpecified
        {
            get { return this.IndexAveragingType != null && this.IndexAveragingType.enumValue != IndexAveragingBase.Blank; }
            set { }
        }

        /// <summary>
        /// The number of index values that should be averaged to arrive at the value to be used in the interest rate calculation. Used in conjunction with the Per Change Index Averaging Type which indicates how to arrive at the values to be counted and used for averaging.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOCount IndexAveragingValueCount;

        /// <summary>
        /// Gets or sets a value indicating whether the IndexAveragingValueCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IndexAveragingValueCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool IndexAveragingValueCountSpecified
        {
            get { return IndexAveragingValueCount != null; }
            set { }
        }

        /// <summary>
        /// Indicates the method used to determine the index rate used to calculate the new interest rate.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOEnum<IndexCalculationMethodBase> IndexCalculationMethodType;

        /// <summary>
        /// Gets or sets a value indicating whether the IndexCalculationMethodType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IndexCalculationMethodType element has been assigned a value.</value>
        [XmlIgnore]
        public bool IndexCalculationMethodTypeSpecified
        {
            get { return this.IndexCalculationMethodType != null && this.IndexCalculationMethodType.enumValue != IndexCalculationMethodBase.Blank; }
            set { }
        }

        /// <summary>
        /// Specifies whether the index is the primary or secondary.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<IndexDesignationBase> IndexDesignationType;

        /// <summary>
        /// Gets or sets a value indicating whether the IndexDesignationType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IndexDesignationType element has been assigned a value.</value>
        [XmlIgnore]
        public bool IndexDesignationTypeSpecified
        {
            get { return this.IndexDesignationType != null && this.IndexDesignationType.enumValue != IndexDesignationBase.Blank; }
            set { }
        }

        /// <summary>
        /// The number of months prior to an interest rate effective date used to determine the date for the index value when calculating a new interest rate on a loan. Combined with Per Change Index Lead Days Count, with number of Months utilized first to arrive at the total lead time. The first of two possible sets of index data if index averaging is used.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOCount IndexLeadMonthsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the IndexLeadMonthsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IndexLeadMonthsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool IndexLeadMonthsCountSpecified
        {
            get { return IndexLeadMonthsCount != null; }
            set { }
        }

        /// <summary>
        /// Specifies the point in time prior to the effective date of the payment adjustment at which date the effective value of the published index is taken to compute the new payment.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOEnum<IndexLookbackBase> IndexLookbackType;

        /// <summary>
        /// Gets or sets a value indicating whether the IndexLookBackType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IndexLookBackType element has been assigned a value.</value>
        [XmlIgnore]
        public bool IndexLookbackTypeSpecified
        {
            get { return this.IndexLookbackType != null && this.IndexLookbackType.enumValue != IndexLookbackBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Loan Index Look back Type.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOString IndexLookbackTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the IndexLookBackTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IndexLookBackTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool IndexLookbackTypeOtherDescriptionSpecified
        {
            get { return IndexLookbackTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The percentage to which the index is rounded prior to use in the calculation of the new interest rate. This field is used in conjunction with the Index Rounding Type which indicates how rounding should occur.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOPercent IndexRoundingPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the IndexRoundingPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IndexRoundingPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool IndexRoundingPercentSpecified
        {
            get { return IndexRoundingPercent != null; }
            set { }
        }

        /// <summary>
        /// Describes when rounding occurs relative to the minimum index movement rule.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOEnum<IndexRoundingTimingBase> IndexRoundingTimingType;

        /// <summary>
        /// Gets or sets a value indicating whether the IndexRoundingTimingType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IndexRoundingTimingType element has been assigned a value.</value>
        [XmlIgnore]
        public bool IndexRoundingTimingTypeSpecified
        {
            get { return this.IndexRoundingTimingType != null && this.IndexRoundingTimingType.enumValue != IndexRoundingTimingBase.Blank; }
            set { }
        }

        /// <summary>
        /// Indicates how the index is rounded prior to use in the calculation of the new interest rate. The index can be rounded up, down or to the nearest factor. This field is used in conjunction with Index Rounding Factor which indicates the percentage to which the rounding occurs.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOEnum<IndexRoundingBase> IndexRoundingType;

        /// <summary>
        /// Gets or sets a value indicating whether the IndexRoundingType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IndexRoundingType element has been assigned a value.</value>
        [XmlIgnore]
        public bool IndexRoundingTypeSpecified
        {
            get { return this.IndexRoundingType != null && this.IndexRoundingType.enumValue != IndexRoundingBase.Blank; }
            set { }
        }

        /// <summary>
        /// Specifies the type and source of index to be used to determine the interest rate at each adjustment.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOEnum<IndexSourceBase> IndexSourceType;

        /// <summary>
        /// Gets or sets a value indicating whether the IndexSourceType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IndexSourceType element has been assigned a value.</value>
        [XmlIgnore]
        public bool IndexSourceTypeSpecified
        {
            get { return this.IndexSourceType != null && this.IndexSourceType.enumValue != IndexSourceBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information or a description of the index, used when Other is selected for the attribute Index Source Type.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOString IndexSourceTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the IndexSourceTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IndexSourceTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool IndexSourceTypeOtherDescriptionSpecified
        {
            get { return IndexSourceTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the general category of mortgage index upon which adjustments will be based.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOEnum<IndexBase> IndexType;

        /// <summary>
        /// Gets or sets a value indicating whether the IndexType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IndexType element has been assigned a value.</value>
        [XmlIgnore]
        public bool IndexTypeSpecified
        {
            get { return this.IndexType != null && this.IndexType.enumValue != IndexBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field to collect additional information when Other is selected for Index Type.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOString IndexTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the IndexTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IndexTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool IndexTypeOtherDescriptionSpecified
        {
            get { return IndexTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The number of days prior to an interest rate effective date used to determine the date for the index value when calculating a new interest rate on a loan.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOCount InterestAdjustmentIndexLeadDaysCount;

        /// <summary>
        /// Gets or sets a value indicating whether the InterestAdjustmentIndexLeadDaysCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InterestAdjustmentIndexLeadDaysCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool InterestAdjustmentIndexLeadDaysCountSpecified
        {
            get { return InterestAdjustmentIndexLeadDaysCount != null; }
            set { }
        }

        /// <summary>
        /// The number of days prior to an interest rate effective date used to determine the date for the index value when calculating both a new interest rate and a principal and interest payment.
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMOCount InterestAndPaymentAdjustmentIndexLeadDaysCount;

        /// <summary>
        /// Gets or sets a value indicating whether the InterestAndPaymentAdjustmentIndexLeadDaysCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InterestAndPaymentAdjustmentIndexLeadDaysCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool InterestAndPaymentAdjustmentIndexLeadDaysCountSpecified
        {
            get { return InterestAndPaymentAdjustmentIndexLeadDaysCount != null; }
            set { }
        }

        /// <summary>
        /// The smallest financial index movement that requires an interest rate change. If the movement is less than this percent, then the interest rate will not change at the time of a scheduled adjustment.
        /// </summary>
        [XmlElement(Order = 17)]
        public MISMOPercent MinimumIndexMovementRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the MinimumIndexMovementRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MinimumIndexMovementRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool MinimumIndexMovementRatePercentSpecified
        {
            get { return MinimumIndexMovementRatePercent != null; }
            set { }
        }

        /// <summary>
        /// The number of payments between the index values that should be used for index averaging.
        /// </summary>
        [XmlElement(Order = 18)]
        public MISMOCount PaymentsBetweenIndexValuesCount;

        /// <summary>
        /// Gets or sets a value indicating whether the PaymentsBetweenIndexValuesCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PaymentsBetweenIndexValuesCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PaymentsBetweenIndexValuesCountSpecified
        {
            get { return PaymentsBetweenIndexValuesCount != null; }
            set { }
        }

        /// <summary>
        /// The number of payments from the interest rate change payment due date to the date of the first index rate to be used in index averaging.
        /// </summary>
        [XmlElement(Order = 19)]
        public MISMOCount PaymentsToFirstIndexValueCount;

        /// <summary>
        /// Gets or sets a value indicating whether the PaymentsToFirstIndexValueCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PaymentsToFirstIndexValueCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PaymentsToFirstIndexValueCountSpecified
        {
            get { return PaymentsToFirstIndexValueCount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 20)]
        public INDEX_RULE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
