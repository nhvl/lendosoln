namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class TIL_PAYMENT_SUMMARY_ITEMS
    {
        /// <summary>
        /// Gets a value indicating whether the TIL_PAYMENT_SUMMARY_ITEMS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.TilPaymentSummaryItemSpecified;
            }
        }

        /// <summary>
        /// A collection of TIL payment summary items.
        /// </summary>
        [XmlElement("TIL_PAYMENT_SUMMARY_ITEM", Order = 0)]
		public List<TIL_PAYMENT_SUMMARY_ITEM> TilPaymentSummaryItem = new List<TIL_PAYMENT_SUMMARY_ITEM>();

        /// <summary>
        /// Gets or sets a value indicating whether the TILPaymentSummaryItem element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TILPaymentSummaryItem element has been assigned a value.</value>
        [XmlIgnore]
        public bool TilPaymentSummaryItemSpecified
        {
            get { return this.TilPaymentSummaryItem != null && this.TilPaymentSummaryItem.Count(t => t != null && t.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public TIL_PAYMENT_SUMMARY_ITEMS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
