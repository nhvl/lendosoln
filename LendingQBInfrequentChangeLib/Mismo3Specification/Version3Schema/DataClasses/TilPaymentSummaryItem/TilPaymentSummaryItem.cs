namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class TIL_PAYMENT_SUMMARY_ITEM
    {
        /// <summary>
        /// Gets a value indicating whether the TIL_PAYMENT_SUMMARY_ITEM container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.TILEstimatedTotalPaymentAmountSpecified
                    || this.TILInterestPaymentAmountSpecified
                    || this.TILInterestRatePercentSpecified
                    || this.TILNegativeAmortizationFullPrincipalPaymentAmountSpecified
                    || this.TILNegativeAmortizationMinimumPrincipalPaymentAmountSpecified
                    || this.TILPaymentAdjustmentDateSpecified
                    || this.TILPaymentSummaryItemTypeOtherDescriptionSpecified
                    || this.TILPaymentSummaryItemTypeSpecified
                    || this.TILPeriodDurationCountSpecified
                    || this.TILPeriodDurationTypeSpecified
                    || this.TILPrincipalPaymentAmountSpecified
                    || this.TILTaxesAndInsuranceEscrowPaymentAmountSpecified;
            }
        }

        /// <summary>
        /// The estimated total amount of the payment that is applied for the payment period specified on the TIL Disclosure document.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount TILEstimatedTotalPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the TILEstimatedTotalPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TILEstimatedTotalPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TILEstimatedTotalPaymentAmountSpecified
        {
            get { return TILEstimatedTotalPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// The amount of the payment that is applied toward interest for the payment period specified on the TIL Disclosure document.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOAmount TILInterestPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the TILInterestPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TILInterestPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TILInterestPaymentAmountSpecified
        {
            get { return TILInterestPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// The interest rate in effect during a specified payment period specified on the TIL Disclosure document.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOPercent TILInterestRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the TILInterestRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TILInterestRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool TILInterestRatePercentSpecified
        {
            get { return TILInterestRatePercent != null; }
            set { }
        }

        /// <summary>
        /// For a negative amortization loan, the full principal payment amount as reflected for the payment period as disclosed on the TIL Disclosure document.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOAmount TILNegativeAmortizationFullPrincipalPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the TILNegativeAmortizationFullPrincipalPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TILNegativeAmortizationFullPrincipalPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TILNegativeAmortizationFullPrincipalPaymentAmountSpecified
        {
            get { return TILNegativeAmortizationFullPrincipalPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// For a negative amortization loan, the minimum principal payment amount as reflected for the payment summary period as disclosed on the TIL Disclosure document.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOAmount TILNegativeAmortizationMinimumPrincipalPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the TILNegativeAmortizationMinimumPrincipalPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TILNegativeAmortizationMinimumPrincipalPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TILNegativeAmortizationMinimumPrincipalPaymentAmountSpecified
        {
            get { return TILNegativeAmortizationMinimumPrincipalPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// The date of the payment adjustment for this TIL Payment Summary Item as disclosed on the TIL Disclosure document.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMODate TILPaymentAdjustmentDate;

        /// <summary>
        /// Gets or sets a value indicating whether the TILPaymentAdjustmentDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TILPaymentAdjustmentDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool TILPaymentAdjustmentDateSpecified
        {
            get { return TILPaymentAdjustmentDate != null; }
            set { }
        }

        /// <summary>
        /// Specifies the type of this Payment Summary Item.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOEnum<TILPaymentSummaryItemBase> TILPaymentSummaryItemType;

        /// <summary>
        /// Gets or sets a value indicating whether the TILPaymentSummaryItemType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TILPaymentSummaryItemType element has been assigned a value.</value>
        [XmlIgnore]
        public bool TILPaymentSummaryItemTypeSpecified
        {
            get { return this.TILPaymentSummaryItemType != null && this.TILPaymentSummaryItemType.enumValue != TILPaymentSummaryItemBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for TIL Payment Summary Item Type.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOString TILPaymentSummaryItemTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the TILPaymentSummaryItemTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TILPaymentSummaryItemTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool TILPaymentSummaryItemTypeOtherDescriptionSpecified
        {
            get { return TILPaymentSummaryItemTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The length of the payment period as it relates to the TIL Period Duration Type for the TIL Payment Summary Item as disclosed on the TIL Disclosure document.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOCount TILPeriodDurationCount;

        /// <summary>
        /// Gets or sets a value indicating whether the TILPeriodDurationCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TILPeriodDurationCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TILPeriodDurationCountSpecified
        {
            get { return TILPeriodDurationCount != null; }
            set { }
        }

        /// <summary>
        /// The type of payment period applicable to the TIL Payment Summary Item as disclosed on the TIL Disclosure document.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOEnum<TILPeriodDurationBase> TILPeriodDurationType;

        /// <summary>
        /// Gets or sets a value indicating whether the TILPeriodDurationType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TILPeriodDurationType element has been assigned a value.</value>
        [XmlIgnore]
        public bool TILPeriodDurationTypeSpecified
        {
            get { return this.TILPeriodDurationType != null && this.TILPeriodDurationType.enumValue != TILPeriodDurationBase.Blank; }
            set { }
        }

        /// <summary>
        /// The amount of the payment that is applied toward principal for the payment period specified on the TIL Disclosure document.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOAmount TILPrincipalPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the TILPrincipalPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TILPrincipalPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TILPrincipalPaymentAmountSpecified
        {
            get { return TILPrincipalPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// The amount of the payment that is applied toward payment of taxes and/or insurance for the payment period specified on the TIL Disclosure document.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOAmount TILTaxesAndInsuranceEscrowPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the TILTaxesAndInsuranceEscrowPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TILTaxesAndInsuranceEscrowPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TILTaxesAndInsuranceEscrowPaymentAmountSpecified
        {
            get { return TILTaxesAndInsuranceEscrowPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 12)]
        public TIL_PAYMENT_SUMMARY_ITEM_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
