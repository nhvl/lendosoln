namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class MORTGAGE_SCORES
    {
        /// <summary>
        /// Gets a value indicating whether the MORTGAGE_SCORES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.MortgageScoreSpecified;
            }
        }

        /// <summary>
        /// A collection of mortgage scores.
        /// </summary>
        [XmlElement("MORTGAGE_SCORE", Order = 0)]
		public List<MORTGAGE_SCORE> MortgageScore = new List<MORTGAGE_SCORE>();

        /// <summary>
        /// Gets or sets a value indicating whether the MortgageScore element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MortgageScore element has been assigned a value.</value>
        [XmlIgnore]
        public bool MortgageScoreSpecified
        {
            get { return this.MortgageScore != null && this.MortgageScore.Count(m => m != null && m.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public MORTGAGE_SCORES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
