namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class MORTGAGE_SCORE
    {
        /// <summary>
        /// Gets a value indicating whether the MORTGAGE_SCORE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.MortgageScoreDateSpecified
                    || this.MortgageScoreValueSpecified
                    || this.MortgageScoreTypeOtherDescriptionSpecified
                    || this.MortgageScoreTypeSpecified;
            }
        }

        /// <summary>
        /// Date that the mortgage score was received from the Mortgage Scoring engine.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMODate MortgageScoreDate;

        /// <summary>
        /// Gets or sets a value indicating whether the MortgageScoreDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MortgageScoreDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool MortgageScoreDateSpecified
        {
            get { return MortgageScoreDate != null; }
            set { }
        }

        /// <summary>
        /// Identifies the type of proprietary mortgage score being reported.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<MortgageScoreBase> MortgageScoreType;

        /// <summary>
        /// Gets or sets a value indicating whether the MortgageScoreType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MortgageScoreType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MortgageScoreTypeSpecified
        {
            get { return this.MortgageScoreType != null && this.MortgageScoreType.enumValue != MortgageScoreBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture the description of the type of Mortgage Score if Other is selected as the Mortgage Score Type.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString MortgageScoreTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MortgageScoreTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MortgageScoreTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MortgageScoreTypeOtherDescriptionSpecified
        {
            get { return MortgageScoreTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The mortgage score.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOValue MortgageScoreValue;

        /// <summary>
        /// Gets or sets a value indicating whether the Mortgage Score Value element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Mortgage Score Value element has been assigned a value.</value>
        [XmlIgnore]
        public bool MortgageScoreValueSpecified
        {
            get { return MortgageScoreValue != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public MORTGAGE_SCORE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
