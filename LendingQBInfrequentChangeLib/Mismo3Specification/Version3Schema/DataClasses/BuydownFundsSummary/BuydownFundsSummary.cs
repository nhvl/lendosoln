namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class BUYDOWN_FUNDS_SUMMARY
    {
        /// <summary>
        /// Gets a value indicating whether the BUYDOWN_FUNDS_SUMMARY container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BuydownTotalSubsidyAmountSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// The total dollar amount of funds held for the buy down subsidy portion of the monthly principal and interest payment, to be discharged according to the buy down schedule.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount BuydownTotalSubsidyAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the BuyDownTotalSubsidyAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BuyDownTotalSubsidyAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool BuydownTotalSubsidyAmountSpecified
        {
            get { return BuydownTotalSubsidyAmount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public BUYDOWN_FUNDS_SUMMARY_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
