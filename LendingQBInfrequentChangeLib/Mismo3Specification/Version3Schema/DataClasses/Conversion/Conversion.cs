namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CONVERSION
    {
        /// <summary>
        /// Gets a value indicating whether the CONVERSION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ProjectConversionDateSpecified
                    || this.ProjectConversionIndicatorSpecified
                    || this.ProjectConversionOriginalUseDescriptionSpecified;
            }
        }


        /// <summary>
        /// Specifies the date the project was converted into the current Project Ownership Type.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMODate ProjectConversionDate;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectConversionDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectConversionDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectConversionDateSpecified
        {
            get { return ProjectConversionDate != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the project was converted into the current Project Ownership Type.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOIndicator ProjectConversionIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectConversionIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectConversionIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectConversionIndicatorSpecified
        {
            get { return ProjectConversionIndicator != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the original use before the project was converted to its current Project Ownership Type.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString ProjectConversionOriginalUseDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectConversionOriginalUseDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectConversionOriginalUseDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectConversionOriginalUseDescriptionSpecified
        {
            get { return ProjectConversionOriginalUseDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public CONVERSION_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
