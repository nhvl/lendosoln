namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class NEIGHBORHOOD_INFLUENCES
    {
        /// <summary>
        /// Gets a value indicating whether the NEIGHBORHOOD_INFLUENCES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.NeighborhoodInfluenceSpecified;
            }
        }

        /// <summary>
        /// A collection of neighborhood influences.
        /// </summary>
        [XmlElement("NEIGHBORHOOD_INFLUENCE", Order = 0)]
		public List<NEIGHBORHOOD_INFLUENCE> NeighborhoodInfluence = new List<NEIGHBORHOOD_INFLUENCE>();

        /// <summary>
        /// Gets or sets a value indicating whether the NeighborhoodInfluence element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NeighborhoodInfluence element has been assigned a value.</value>
        [XmlIgnore]
        public bool NeighborhoodInfluenceSpecified
        {
            get { return this.NeighborhoodInfluence != null && this.NeighborhoodInfluence.Count(n => n != null && n.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public NEIGHBORHOOD_INFLUENCES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
