namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class NEIGHBORHOOD_INFLUENCE
    {
        /// <summary>
        /// Gets a value indicating whether the NEIGHBORHOOD_INFLUENCE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ConditionRatingTypeSpecified
                    || this.ExtensionSpecified
                    || this.InfluenceImpactTypeSpecified
                    || this.NeighborhoodInfluenceCommentDescriptionSpecified
                    || this.NeighborhoodInfluenceEstimatedDistanceTextSpecified
                    || this.NeighborhoodInfluenceTypeOtherDescriptionSpecified
                    || this.NeighborhoodInfluenceTypeSpecified;
            }
        }

        /// <summary>
        /// A rating of the condition of the identified component.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<ConditionRatingBase> ConditionRatingType;

        /// <summary>
        /// Gets or sets a value indicating whether the ConditionRatingType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConditionRatingType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConditionRatingTypeSpecified
        {
            get { return this.ConditionRatingType != null && this.ConditionRatingType.enumValue != ConditionRatingBase.Blank; }
            set { }
        }

        /// <summary>
        /// Identifies the impact of the indicated Influence on the subject property as used in the appraiser's analysis.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<InfluenceImpactBase> InfluenceImpactType;

        /// <summary>
        /// Gets or sets a value indicating whether the InfluenceImpactType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InfluenceImpactType element has been assigned a value.</value>
        [XmlIgnore]
        public bool InfluenceImpactTypeSpecified
        {
            get { return this.InfluenceImpactType != null && this.InfluenceImpactType.enumValue != InfluenceImpactBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field holding any comments on the adverse feature specified by Neighborhood Adverse Feature Type.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString NeighborhoodInfluenceCommentDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the NeighborhoodInfluenceCommentDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NeighborhoodInfluenceCommentDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool NeighborhoodInfluenceCommentDescriptionSpecified
        {
            get { return NeighborhoodInfluenceCommentDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the estimated distance from the neighborhood to a convenience feature specified in Neighborhood Feature Type. (i.e. such as the distance to public transportation or shopping malls).
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString NeighborhoodInfluenceEstimatedDistanceText;

        /// <summary>
        /// Gets or sets a value indicating whether the NeighborhoodInfluenceEstimatedDistanceText element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NeighborhoodInfluenceEstimatedDistanceText element has been assigned a value.</value>
        [XmlIgnore]
        public bool NeighborhoodInfluenceEstimatedDistanceTextSpecified
        {
            get { return NeighborhoodInfluenceEstimatedDistanceText != null; }
            set { }
        }

        /// <summary>
        /// Specifies a feature of the neighborhood in which the property is located.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<NeighborhoodInfluenceBase> NeighborhoodInfluenceType;

        /// <summary>
        /// Gets or sets a value indicating whether the NeighborhoodInfluenceType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NeighborhoodInfluenceType element has been assigned a value.</value>
        [XmlIgnore]
        public bool NeighborhoodInfluenceTypeSpecified
        {
            get { return this.NeighborhoodInfluenceType != null && this.NeighborhoodInfluenceType.enumValue != NeighborhoodInfluenceBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the neighborhood feature if Other is selected as the Neighborhood Influence Type.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString NeighborhoodInfluenceTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the NeighborhoodInfluenceTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NeighborhoodInfluenceTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool NeighborhoodInfluenceTypeOtherDescriptionSpecified
        {
            get { return NeighborhoodInfluenceTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 6)]
        public NEIGHBORHOOD_INFLUENCE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
