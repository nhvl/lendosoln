namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class RENTAL_UNIT
    {
        /// <summary>
        /// Gets a value indicating whether the RENTAL_UNIT container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.MonthlyRentAmountSpecified
                    || this.SquareFeetNumberSpecified
                    || this.TotalBathroomCountSpecified
                    || this.TotalBedroomCountSpecified
                    || this.TotalRoomCountSpecified;
            }
        }

        /// <summary>
        /// The amount paid by the borrower or co-borrower for the rental of a residence property. This information is normally provided during interview with the borrower or obtained from a loan application, and is sometimes verified with the residence property landlord.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount MonthlyRentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the MonthlyRentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MonthlyRentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool MonthlyRentAmountSpecified
        {
            get { return MonthlyRentAmount != null; }
            set { }
        }

        /// <summary>
        /// Identifies the total area measured in square feet.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMONumeric SquareFeetNumber;

        /// <summary>
        /// Gets or sets a value indicating whether the SquareFeetNumber element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SquareFeetNumber element has been assigned a value.</value>
        [XmlIgnore]
        public bool SquareFeetNumberSpecified
        {
            get { return SquareFeetNumber != null; }
            set { }
        }

        /// <summary>
        /// Specifies the total number of bathrooms.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOCount TotalBathroomCount;

        /// <summary>
        /// Gets or sets a value indicating whether the TotalBathroomCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TotalBathroomCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TotalBathroomCountSpecified
        {
            get { return TotalBathroomCount != null; }
            set { }
        }

        /// <summary>
        /// Specifies the total number of bedrooms.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOCount TotalBedroomCount;

        /// <summary>
        /// Gets or sets a value indicating whether the TotalBedroomCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TotalBedroomCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TotalBedroomCountSpecified
        {
            get { return TotalBedroomCount != null; }
            set { }
        }

        /// <summary>
        /// Specifies the total number of livable rooms.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOCount TotalRoomCount;

        /// <summary>
        /// Gets or sets a value indicating whether the TotalRoomCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TotalRoomCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TotalRoomCountSpecified
        {
            get { return TotalRoomCount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 5)]
        public RENTAL_UNIT_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
