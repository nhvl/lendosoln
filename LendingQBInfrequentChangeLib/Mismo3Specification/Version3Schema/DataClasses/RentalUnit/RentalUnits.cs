namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class RENTAL_UNITS
    {
        /// <summary>
        /// Gets a value indicating whether the RENTAL_UNITS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.RentalUnitSpecified;
            }
        }

        /// <summary>
        /// Contains multiple rental unit elements.
        /// </summary>
        [XmlElement("RENTAL_UNIT", Order = 0)]
		public List<RENTAL_UNIT> RentalUnit = new List<RENTAL_UNIT>();

        /// <summary>
        /// Gets or sets a value indicating whether the RentalUnit element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RentalUnit element has been assigned a value.</value>
        [XmlIgnore]
        public bool RentalUnitSpecified
        {
            get { return this.RentalUnit != null && this.RentalUnit.Count(r => r != null && r.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public RENTAL_UNITS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
