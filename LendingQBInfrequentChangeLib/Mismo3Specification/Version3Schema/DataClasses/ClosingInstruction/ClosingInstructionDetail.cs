namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CLOSING_INSTRUCTION_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the CLOSING_INSTRUCTION_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ClosingInstructionsConsolidatedClosingConditionsDescriptionSpecified
                    || this.ClosingInstructionsPropertyTaxMessageDescriptionSpecified
                    || this.ClosingInstructionsTermiteReportRequiredIndicatorSpecified
                    || this.ExtensionSpecified
                    || this.FundingCutoffTimeSpecified
                    || this.HoursDocumentsNeededPriorToDisbursementCountSpecified
                    || this.LeadBasedPaintCertificationRequiredIndicatorSpecified;
            }
        }

        /// <summary>
        /// Consolidated description of all closing instructions. Use this field when the individual closing conditions are not represented in separate entries.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString ClosingInstructionsConsolidatedClosingConditionsDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ClosingInstructionsConsolidatedClosingConditionsDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ClosingInstructionsConsolidatedClosingConditionsDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ClosingInstructionsConsolidatedClosingConditionsDescriptionSpecified
        {
            get { return ClosingInstructionsConsolidatedClosingConditionsDescription != null; }
            set { }
        }

        /// <summary>
        /// The text message to the closing agent regarding the status of tax payments for the subject property.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString ClosingInstructionsPropertyTaxMessageDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ClosingInstructionsPropertyTaxMessageDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ClosingInstructionsPropertyTaxMessageDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ClosingInstructionsPropertyTaxMessageDescriptionSpecified
        {
            get { return ClosingInstructionsPropertyTaxMessageDescription != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that a Termite Report is a requirement of closing.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOIndicator ClosingInstructionsTermiteReportRequiredIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ClosingInstructionsTermiteReportRequiredIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ClosingInstructionsTermiteReportRequiredIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ClosingInstructionsTermiteReportRequiredIndicatorSpecified
        {
            get { return ClosingInstructionsTermiteReportRequiredIndicator != null; }
            set { }
        }

        /// <summary>
        /// The time of day by which the executed loan documents must be received for funding to occur. 
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOTime FundingCutoffTime;

        /// <summary>
        /// Gets or sets a value indicating whether the FundingCutoffTime element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FundingCutoffTime element has been assigned a value.</value>
        [XmlIgnore]
        public bool FundingCutoffTimeSpecified
        {
            get { return FundingCutoffTime != null; }
            set { }
        }

        /// <summary>
        /// The number of hours by which the executed loan documents must be received prior to funds disbursement. This field is used on the Closing Instructions to identify funding lenders requirements in terms of when funds disbursement will occur in relation to the lenders receipt of the fully executed loan documents. 
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOCount HoursDocumentsNeededPriorToDisbursementCount;

        /// <summary>
        /// Gets or sets a value indicating whether the HoursDocumentsNeededPriorToDisbursementCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HoursDocumentsNeededPriorToDisbursementCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool HoursDocumentsNeededPriorToDisbursementCountSpecified
        {
            get { return HoursDocumentsNeededPriorToDisbursementCount != null; }
            set { }
        }

        /// <summary>
        /// Indicates that a Lead Base Certification is a requirement of closing.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOIndicator LeadBasedPaintCertificationRequiredIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the LeadBasedPaintCertificationRequiredIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LeadBasedPaintCertificationRequiredIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool LeadBasedPaintCertificationRequiredIndicatorSpecified
        {
            get { return LeadBasedPaintCertificationRequiredIndicator != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 6)]
        public CLOSING_INSTRUCTION_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
