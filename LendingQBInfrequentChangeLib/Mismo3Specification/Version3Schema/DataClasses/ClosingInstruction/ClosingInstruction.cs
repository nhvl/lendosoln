namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CLOSING_INSTRUCTION
    {
        /// <summary>
        /// Gets a value indicating whether the CLOSING_INSTRUCTION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ClosingInstructionDetailSpecified
                    || this.ConditionsSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// Details on closing instructions.
        /// </summary>
        [XmlElement("CLOSING_INSTRUCTION_DETAIL", Order = 0)]
        public CLOSING_INSTRUCTION_DETAIL ClosingInstructionDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the ClosingInstructionDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ClosingInstructionDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool ClosingInstructionDetailSpecified
        {
            get { return this.ClosingInstructionDetail != null && this.ClosingInstructionDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Conditions of closing instructions.
        /// </summary>
        [XmlElement("CONDITIONS", Order = 1)]
        public CONDITIONS Conditions;

        /// <summary>
        /// Gets or sets a value indicating whether the Conditions element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Conditions element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConditionsSpecified
        {
            get { return Conditions != null && Conditions.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public CLOSING_INSTRUCTION_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
