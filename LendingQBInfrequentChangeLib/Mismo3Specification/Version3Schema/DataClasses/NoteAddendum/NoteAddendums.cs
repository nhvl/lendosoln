namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class NOTE_ADDENDUMS
    {
        /// <summary>
        /// Gets a value indicating whether the NOTE_ADDENDUMS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.NoteAddendumSpecified;
            }
        }

        /// <summary>
        /// A collection of note addendums.
        /// </summary>
        [XmlElement("NOTE_ADDENDUM", Order = 0)]
		public List<NOTE_ADDENDUM> NoteAddendum = new List<NOTE_ADDENDUM>();

        /// <summary>
        /// Gets or sets a value indicating whether the NoteAddendum element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NoteAddendum element has been assigned a value.</value>
        [XmlIgnore]
        public bool NoteAddendumSpecified
        {
            get { return this.NoteAddendum != null && this.NoteAddendum.Count(n => n != null && n.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public NOTE_ADDENDUMS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
