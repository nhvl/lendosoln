namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PROJECT_OWNERSHIP
    {
        /// <summary>
        /// Gets a value indicating whether the PROJECT_OWNERSHIP container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ProjectConcentratedOwnershipDescriptionSpecified
                    || this.ProjectConcentratedOwnershipIndicatorSpecified;
            }
        }

        /// <summary>
        /// A free-form text field describing the one person or entity that holds more than ten (10) percent ownership of the project.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString ProjectConcentratedOwnershipDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectConcentratedOwnershipDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectConcentratedOwnershipDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectConcentratedOwnershipDescriptionSpecified
        {
            get { return ProjectConcentratedOwnershipDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates that one person or entity holds more than ten (10) percent ownership of the project.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOIndicator ProjectConcentratedOwnershipIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectConcentratedOwnershipIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectConcentratedOwnershipIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectConcentratedOwnershipIndicatorSpecified
        {
            get { return ProjectConcentratedOwnershipIndicator != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public PROJECT_OWNERSHIP_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
