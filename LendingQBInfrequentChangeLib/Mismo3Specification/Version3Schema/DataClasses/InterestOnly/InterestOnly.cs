namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class INTEREST_ONLY
    {
        /// <summary>
        /// Gets a value indicating whether the INTEREST_ONLY container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.InterestOnlyEndDateSpecified
                    || this.InterestOnlyMonthlyPaymentAmountSpecified
                    || this.InterestOnlyTermMonthsCountSpecified
                    || this.InterestOnlyTermPaymentsCountSpecified;
            }
        }

        /// <summary>
        /// The date on which the interest only period on the loan ends.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMODate InterestOnlyEndDate;

        /// <summary>
        /// Gets or sets a value indicating whether the InterestOnlyEndDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InterestOnlyEndDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool InterestOnlyEndDateSpecified
        {
            get { return InterestOnlyEndDate != null; }
            set { }
        }

        /// <summary>
        /// The amount of a monthly payment when the borrower is only paying the interest.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOAmount InterestOnlyMonthlyPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the InterestOnlyMonthlyPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InterestOnlyMonthlyPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool InterestOnlyMonthlyPaymentAmountSpecified
        {
            get { return InterestOnlyMonthlyPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// Then number of months the loan remains interest only.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOCount InterestOnlyTermMonthsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the InterestOnlyTermMonthsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InterestOnlyTermMonthsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool InterestOnlyTermMonthsCountSpecified
        {
            get { return InterestOnlyTermMonthsCount != null; }
            set { }
        }

        /// <summary>
        /// The number of payments the loan remains interest only.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOCount InterestOnlyTermPaymentsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the InterestOnlyTermPaymentsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InterestOnlyTermPaymentsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool InterestOnlyTermPaymentsCountSpecified
        {
            get { return InterestOnlyTermPaymentsCount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public INTEREST_ONLY_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
