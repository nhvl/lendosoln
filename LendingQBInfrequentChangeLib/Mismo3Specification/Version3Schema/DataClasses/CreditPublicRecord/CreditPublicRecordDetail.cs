namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CREDIT_PUBLIC_RECORD_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the CREDIT_PUBLIC_RECORD_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditPublicRecordAccountOwnershipTypeSpecified
                    || this.CreditPublicRecordAttorneyNameSpecified
                    || this.CreditPublicRecordConsumerDisputeIndicatorSpecified
                    || this.CreditPublicRecordCourtNameSpecified
                    || this.CreditPublicRecordDefendantNameSpecified
                    || this.CreditPublicRecordDerogatoryDataIndicatorSpecified
                    || this.CreditPublicRecordDispositionDateSpecified
                    || this.CreditPublicRecordDispositionTypeOtherDescriptionSpecified
                    || this.CreditPublicRecordDispositionTypeSpecified
                    || this.CreditPublicRecordDocketIdentifierSpecified
                    || this.CreditPublicRecordFiledDateSpecified
                    || this.CreditPublicRecordLegalObligationAmountSpecified
                    || this.CreditPublicRecordManualUpdateIndicatorSpecified
                    || this.CreditPublicRecordPaidDateSpecified
                    || this.CreditPublicRecordPaymentFrequencyTypeOtherDescriptionSpecified
                    || this.CreditPublicRecordPaymentFrequencyTypeSpecified
                    || this.CreditPublicRecordPlaintiffNameSpecified
                    || this.CreditPublicRecordReportedDateSpecified
                    || this.CreditPublicRecordSettledDateSpecified
                    || this.CreditPublicRecordTypeOtherDescriptionSpecified
                    || this.CreditPublicRecordTypeSpecified
                    || this.DuplicateGroupIdentifierSpecified
                    || this.ExtensionSpecified
                    || this.PrimaryRecordIndicatorSpecified;
            }
        }

        /// <summary>
        /// Describes the ownership relation of the borrower(s) to the public record (i.e. are they individually responsible, jointly responsible, terminated).
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<CreditPublicRecordAccountOwnershipBase> CreditPublicRecordAccountOwnershipType;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditPublicRecordAccountOwnershipType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditPublicRecordAccountOwnershipType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditPublicRecordAccountOwnershipTypeSpecified
        {
            get { return this.CreditPublicRecordAccountOwnershipType != null && this.CreditPublicRecordAccountOwnershipType.enumValue != CreditPublicRecordAccountOwnershipBase.Blank; }
            set { }
        }

        /// <summary>
        /// The name of the attorney (normally the plaintiff) reported in the public record.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString CreditPublicRecordAttorneyName;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditPublicRecordAttorneyName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditPublicRecordAttorneyName element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditPublicRecordAttorneyNameSpecified
        {
            get { return CreditPublicRecordAttorneyName != null; }
            set { }
        }

        /// <summary>
        /// This element indicates whether or not the consumer (borrower) disputes information reported for this public record and has filed a FCRA dispute document.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOIndicator CreditPublicRecordConsumerDisputeIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditPublicRecordConsumerDisputeIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditPublicRecordConsumerDisputeIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditPublicRecordConsumerDisputeIndicatorSpecified
        {
            get { return CreditPublicRecordConsumerDisputeIndicator != null; }
            set { }
        }

        /// <summary>
        /// The name of the court reporting the public record.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString CreditPublicRecordCourtName;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditPublicRecordCourtName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditPublicRecordCourtName element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditPublicRecordCourtNameSpecified
        {
            get { return CreditPublicRecordCourtName != null; }
            set { }
        }

        /// <summary>
        /// The name of the defendant in a public record.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString CreditPublicRecordDefendantName;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditPublicRecordDefendantName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditPublicRecordDefendantName element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditPublicRecordDefendantNameSpecified
        {
            get { return CreditPublicRecordDefendantName != null; }
            set { }
        }

        /// <summary>
        /// This element indicates whether or not the public record contains adverse or derogatory information.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOIndicator CreditPublicRecordDerogatoryDataIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditPublicRecordDerogatoryDataIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditPublicRecordDerogatoryDataIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditPublicRecordDerogatoryDataIndicatorSpecified
        {
            get { return CreditPublicRecordDerogatoryDataIndicator != null; }
            set { }
        }

        /// <summary>
        /// This element provides the date that the Public Record Disposition Type first became effective.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMODate CreditPublicRecordDispositionDate;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditPublicRecordDispositionDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditPublicRecordDispositionDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditPublicRecordDispositionDateSpecified
        {
            get { return CreditPublicRecordDispositionDate != null; }
            set { }
        }

        /// <summary>
        /// This is the enumerated disposition or the public record as of the Disposition Date. If the disposition does not fit one of the enumerated categories, select Other, and enter it in the Credit Public Record Disposition Type Other Description element.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOEnum<CreditPublicRecordDispositionBase> CreditPublicRecordDispositionType;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditPublicRecordDispositionType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditPublicRecordDispositionType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditPublicRecordDispositionTypeSpecified
        {
            get { return this.CreditPublicRecordDispositionType != null && this.CreditPublicRecordDispositionType.enumValue != CreditPublicRecordDispositionBase.Blank; }
            set { }
        }

        /// <summary>
        /// If the Credit Public Record Disposition Type is set to Other, enter the disposition in this element.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOString CreditPublicRecordDispositionTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditPublicRecordDispositionTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditPublicRecordDispositionTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditPublicRecordDispositionTypeOtherDescriptionSpecified
        {
            get { return CreditPublicRecordDispositionTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The Docket Identifier can be a Case Number, Docket Number, Public Record Book, Page or Sequence Number.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOIdentifier CreditPublicRecordDocketIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditPublicRecordDocketIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditPublicRecordDocketIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditPublicRecordDocketIdentifierSpecified
        {
            get { return CreditPublicRecordDocketIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The date that the public record was filed by the court.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMODate CreditPublicRecordFiledDate;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditPublicRecordFiledDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditPublicRecordFiledDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditPublicRecordFiledDateSpecified
        {
            get { return CreditPublicRecordFiledDate != null; }
            set { }
        }

        /// <summary>
        /// This is the amount listed in the public record that the defendant is obligated to pay.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOAmount CreditPublicRecordLegalObligationAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditPublicRecordLegalObligationAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditPublicRecordLegalObligationAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditPublicRecordLegalObligationAmountSpecified
        {
            get { return CreditPublicRecordLegalObligationAmount != null; }
            set { }
        }

        /// <summary>
        /// This element indicates whether or not the data in the public record has been manually updated by the credit bureau based on an investigation.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOIndicator CreditPublicRecordManualUpdateIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditPublicRecordManualUpdateIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditPublicRecordManualUpdateIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditPublicRecordManualUpdateIndicatorSpecified
        {
            get { return CreditPublicRecordManualUpdateIndicator != null; }
            set { }
        }

        /// <summary>
        /// The date that the Legal Obligation Amount was paid to zero.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMODate CreditPublicRecordPaidDate;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditPublicRecordPaidDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditPublicRecordPaidDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditPublicRecordPaidDateSpecified
        {
            get { return CreditPublicRecordPaidDate != null; }
            set { }
        }

        /// <summary>
        /// This element indicates the frequency of payments allowed for the Legal Obligation Amount.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOEnum<CreditPublicRecordPaymentFrequencyBase> CreditPublicRecordPaymentFrequencyType;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditPublicRecordPaymentFrequencyType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditPublicRecordPaymentFrequencyType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditPublicRecordPaymentFrequencyTypeSpecified
        {
            get { return this.CreditPublicRecordPaymentFrequencyType != null && this.CreditPublicRecordPaymentFrequencyType.enumValue != CreditPublicRecordPaymentFrequencyBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Credit Public Record Payment Frequency Type. 
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOString CreditPublicRecordPaymentFrequencyTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditPublicRecordPaymentFrequencyTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditPublicRecordPaymentFrequencyTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditPublicRecordPaymentFrequencyTypeOtherDescriptionSpecified
        {
            get { return CreditPublicRecordPaymentFrequencyTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The name of the plaintiff in a public record.
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMOString CreditPublicRecordPlaintiffName;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditPublicRecordPlaintiffName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditPublicRecordPlaintiffName element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditPublicRecordPlaintiffNameSpecified
        {
            get { return CreditPublicRecordPlaintiffName != null; }
            set { }
        }

        /// <summary>
        /// The date that the public record data was last reported by the court to the credit repository bureau.
        /// </summary>
        [XmlElement(Order = 17)]
        public MISMODate CreditPublicRecordReportedDate;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditPublicRecordReportedDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditPublicRecordReportedDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditPublicRecordReportedDateSpecified
        {
            get { return CreditPublicRecordReportedDate != null; }
            set { }
        }

        /// <summary>
        /// The date that the public record case was settled.
        /// </summary>
        [XmlElement(Order = 18)]
        public MISMODate CreditPublicRecordSettledDate;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditPublicRecordSettledDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditPublicRecordSettledDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditPublicRecordSettledDateSpecified
        {
            get { return CreditPublicRecordSettledDate != null; }
            set { }
        }

        /// <summary>
        /// This is the enumerated type of public record reported by the credit bureau. If the type does not fit one of the enumerated categories, enter the type in the Credit Public Record Type Other Description element.
        /// </summary>
        [XmlElement(Order = 19)]
        public MISMOEnum<CreditPublicRecordBase> CreditPublicRecordType;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditPublicRecordType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditPublicRecordType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditPublicRecordTypeSpecified
        {
            get { return this.CreditPublicRecordType != null && this.CreditPublicRecordType.enumValue != CreditPublicRecordBase.Blank; }
            set { }
        }

        /// <summary>
        /// If the Public Record Type is set to Other, enter the type in this element.
        /// </summary>
        [XmlElement(Order = 20)]
        public MISMOString CreditPublicRecordTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditPublicRecordTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditPublicRecordTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditPublicRecordTypeOtherDescriptionSpecified
        {
            get { return CreditPublicRecordTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Data records with the same Duplicate Group Identifier value have been identified as being duplicate records from different sources.  Within a set of records that have the same Duplicate Group Identifier, one record is marked with a Primary Record Indicator set to "True", indicating that this record has been identified as the record out of the group that should be used for evaluation of credit.  The remaining records in the group should have a Primary Record Indicator set to "False".
        /// </summary>
        [XmlElement(Order = 21)]
        public MISMOIdentifier DuplicateGroupIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the DuplicateGroupIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DuplicateGroupIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool DuplicateGroupIdentifierSpecified
        {
            get { return DuplicateGroupIdentifier != null; }
            set { }
        }

        /// <summary>
        /// If this indicator is missing or if this indicator is set to "True", the data element is the primary record to be used for output and evaluation.  Most mortgage credit report data is collected from multiple sources (i.e. Equifax, Experian and Trans Union), and combined into a single data set that does not contain duplicate data.  In most credit reports, only the primary, merged data is returned.  For these reports, the Primary Record Indicator can be left out completely or if it is provided is should be set to "True".   Some credit reports are provided that contain both the primary, merged data, plus the original records that were considered duplicates of each other that were used to derive the primary record.  These duplicates that contained the original, unmerged data will have a Primary Record Indicator value of "False".
        /// </summary>
        [XmlElement(Order = 22)]
        public MISMOIndicator PrimaryRecordIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PrimaryRecordIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrimaryRecordIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrimaryRecordIndicatorSpecified
        {
            get { return PrimaryRecordIndicator != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 23)]
        public CREDIT_PUBLIC_RECORD_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
