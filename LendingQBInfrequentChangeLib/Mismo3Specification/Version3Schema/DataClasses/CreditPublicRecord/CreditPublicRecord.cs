namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class CREDIT_PUBLIC_RECORD
    {
        /// <summary>
        /// Gets a value indicating whether the CREDIT_PUBLIC_RECORD container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BankruptcySpecified
                    || this.ContactsSpecified
                    || this.CreditCommentsSpecified
                    || this.CreditPublicRecordDetailSpecified
                    || this.CreditRepositoriesSpecified
                    || this.ExtensionSpecified
                    || this.VerificationSpecified;
            }
        }

        /// <summary>
        /// Bankruptcy on the public record.
        /// </summary>
        [XmlElement("BANKRUPTCY", Order = 0)]
        public BANKRUPTCY Bankruptcy;

        /// <summary>
        /// Gets or sets a value indicating whether the Bankruptcy element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Bankruptcy element has been assigned a value.</value>
        [XmlIgnore]
        public bool BankruptcySpecified
        {
            get { return this.Bankruptcy != null && this.Bankruptcy.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Contacts related to the public record.
        /// </summary>
        [XmlElement("CONTACTS", Order = 1)]
        public CONTACTS Contacts;

        /// <summary>
        /// Gets or sets a value indicating whether the Contacts element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Contacts element has been assigned a value.</value>
        [XmlIgnore]
        public bool ContactsSpecified
        {
            get { return this.Contacts != null && this.Contacts.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Comments about credit.
        /// </summary>
        [XmlElement("CREDIT_COMMENTS", Order = 2)]
        public CREDIT_COMMENTS CreditComments;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditComments element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditComments element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditCommentsSpecified
        {
            get { return this.CreditComments != null && this.CreditComments.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details on the credit public record.
        /// </summary>
        [XmlElement("CREDIT_PUBLIC_RECORD_DETAIL", Order = 3)]
        public CREDIT_PUBLIC_RECORD_DETAIL CreditPublicRecordDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditPublicRecordDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditPublicRecordDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditPublicRecordDetailSpecified
        {
            get { return this.CreditPublicRecordDetail != null && this.CreditPublicRecordDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Repository of the credit data.
        /// </summary>
        [XmlElement("CREDIT_REPOSITORIES", Order = 4)]
        public CREDIT_REPOSITORIES CreditRepositories;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditRepositories element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditRepositories element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditRepositoriesSpecified
        {
            get { return this.CreditRepositories != null && this.CreditRepositories.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Verification of the public record.
        /// </summary>
        [XmlElement("VERIFICATION", Order = 5)]
        public VERIFICATION Verification;

        /// <summary>
        /// Gets or sets a value indicating whether the Verification element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Verification element has been assigned a value.</value>
        [XmlIgnore]
        public bool VerificationSpecified
        {
            get { return this.Verification != null && this.Verification.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 6)]
        public CREDIT_PUBLIC_RECORD_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
