namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CREDIT_PUBLIC_RECORDS
    {
        /// <summary>
        /// Gets a value indicating whether the CREDIT_PUBLIC_RECORDS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditPublicRecordSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of credit public records.
        /// </summary>
        [XmlElement("CREDIT_PUBLIC_RECORD", Order = 0)]
		public List<CREDIT_PUBLIC_RECORD> CreditPublicRecord = new List<CREDIT_PUBLIC_RECORD>();

        /// <summary>
        /// Gets or sets a value indicating whether the CreditPublicRecord element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditPublicRecord element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditPublicRecordSpecified
        {
            get { return this.CreditPublicRecord != null && this.CreditPublicRecord.Count(c => c != null && c.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public CREDIT_PUBLIC_RECORDS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
