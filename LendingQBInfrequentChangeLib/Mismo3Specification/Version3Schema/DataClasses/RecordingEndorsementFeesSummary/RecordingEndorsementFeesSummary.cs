namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class RECORDING_ENDORSEMENT_FEES_SUMMARY
    {
        /// <summary>
        /// Gets a value indicating whether the RECORDING_ENDORSEMENT_FEES_SUMMARY container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.RecordingEndorsementFeesTotalAmountSpecified;
            }
        }

        /// <summary>
        /// The total amount of all fees required to be remitted to the county recorder for recording a single document.  (Depending on state laws and local ordinances there may not be a direct relationship between Recording Endorsement Fees and RESPA Fees.).
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount RecordingEndorsementFeesTotalAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the RecordingEndorsementFeesTotalAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RecordingEndorsementFeesTotalAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool RecordingEndorsementFeesTotalAmountSpecified
        {
            get { return RecordingEndorsementFeesTotalAmount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public RECORDING_ENDORSEMENT_FEES_SUMMARY_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
