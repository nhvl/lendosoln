namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class VALUATION_INTENDED_USE
    {
        /// <summary>
        /// Gets a value indicating whether the VALUATION_INTENDED_USE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ValuationIntendedUseDescriptionSpecified
                    || this.ValuationIntendedUseTypeOtherDescriptionSpecified
                    || this.ValuationIntendedUseTypeSpecified;
            }
        }

        /// <summary>
        /// Detail or commentary from the appraiser regarding the Intended Use of valuation product or service declared by the appraiser and for which he is obligated.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString ValuationIntendedUseDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ValuationIntendedUseDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ValuationIntendedUseDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ValuationIntendedUseDescriptionSpecified
        {
            get { return ValuationIntendedUseDescription != null; }
            set { }
        }

        /// <summary>
        /// Specific indications of the types of use authorized by the appraiser for a given valuation product.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<ValuationIntendedUseBase> ValuationIntendedUseType;

        /// <summary>
        /// Gets or sets a value indicating whether the ValuationIntendedUseType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ValuationIntendedUseType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ValuationIntendedUseTypeSpecified
        {
            get { return this.ValuationIntendedUseType != null && this.ValuationIntendedUseType.enumValue != ValuationIntendedUseBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Valuation Intended Use Type.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString ValuationIntendedUseTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ValuationIntendedUseTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ValuationIntendedUseTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ValuationIntendedUseTypeOtherDescriptionSpecified
        {
            get { return ValuationIntendedUseTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public VALUATION_INTENDED_USE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
