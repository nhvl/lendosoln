namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CONDITIONS
    {
        /// <summary>
        /// Gets a value indicating whether the CONDITIONS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ConditionSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of conditions.
        /// </summary>
        [XmlElement("CONDITION", Order = 0)]
		public List<CONDITION> Condition = new List<CONDITION>();

        /// <summary>
        /// Gets or sets a value indicating whether the Condition element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Condition element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConditionSpecified
        {
            get { return Condition != null && Condition.Count(c => c != null && c.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public CONDITIONS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
