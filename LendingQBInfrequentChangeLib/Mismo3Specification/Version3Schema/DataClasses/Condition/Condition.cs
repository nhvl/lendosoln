namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class CONDITION
    {
        /// <summary>
        /// Gets a value indicating whether the CONDITION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ConditionDescriptionSpecified
                    || this.ConditionMetIndicatorSpecified
                    || this.ConditionSatisfactionApprovedByNameSpecified
                    || this.ConditionSatisfactionDateSpecified
                    || this.ConditionSatisfactionDueDateSpecified
                    || this.ConditionSatisfactionResponsiblePartyTypeOtherDescriptionSpecified
                    || this.ConditionSatisfactionResponsiblePartyTypeSpecified
                    || this.ConditionSatisfactionTimeframeTypeOtherDescriptionSpecified
                    || this.ConditionSatisfactionTimeframeTypeSpecified
                    || this.ConditionWaivedIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// The closing instruction text description.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString ConditionDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ConditionDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConditionDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConditionDescriptionSpecified
        {
            get { return ConditionDescription != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that the condition has been satisfied.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOIndicator ConditionMetIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ConditionMetIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConditionMetIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConditionMetIndicatorSpecified
        {
            get { return ConditionMetIndicator != null; }
            set { }
        }

        /// <summary>
        /// The name of the person who signs off that the condition has been met.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString ConditionSatisfactionApprovedByName;

        /// <summary>
        /// Gets or sets a value indicating whether the ConditionSatisfactionApprovedByName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConditionSatisfactionApprovedByName element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConditionSatisfactionApprovedByNameSpecified
        {
            get { return ConditionSatisfactionApprovedByName != null; }
            set { }
        }

        /// <summary>
        /// The date the condition was met/satisfied.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMODate ConditionSatisfactionDate;

        /// <summary>
        /// Gets or sets a value indicating whether the ConditionSatisfactionDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConditionSatisfactionDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConditionSatisfactionDateSpecified
        {
            get { return ConditionSatisfactionDate != null; }
            set { }
        }

        /// <summary>
        /// The date the condition must be satisfied before the loan can proceed.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMODate ConditionSatisfactionDueDate;

        /// <summary>
        /// Gets or sets a value indicating whether the ConditionSatisfactionDueDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConditionSatisfactionDueDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConditionSatisfactionDueDateSpecified
        {
            get { return ConditionSatisfactionDueDate != null; }
            set { }
        }

        /// <summary>
        /// The party responsible for obtaining the necessary information to satisfy the condition.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOEnum<ConditionSatisfactionResponsiblePartyBase> ConditionSatisfactionResponsiblePartyType;

        /// <summary>
        /// Gets or sets a value indicating whether the ConditionSatisfactionResponsiblePartyType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConditionSatisfactionResponsiblePartyType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConditionSatisfactionResponsiblePartyTypeSpecified
        {
            get { return this.ConditionSatisfactionResponsiblePartyType != null && this.ConditionSatisfactionResponsiblePartyType.enumValue != ConditionSatisfactionResponsiblePartyBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Condition Satisfaction Responsible Party Type.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOString ConditionSatisfactionResponsiblePartyTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ConditionSatisfactionResponsiblePartyTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConditionSatisfactionResponsiblePartyTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConditionSatisfactionResponsiblePartyTypeOtherDescriptionSpecified
        {
            get { return ConditionSatisfactionResponsiblePartyTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The point in the Loan Process where this condition must be satisfied before proceeding to the next stage.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOEnum<ConditionSatisfactionTimeframeBase> ConditionSatisfactionTimeframeType;

        /// <summary>
        /// Gets or sets a value indicating whether the ConditionSatisfactionTimeframeType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConditionSatisfactionTimeframeType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConditionSatisfactionTimeframeTypeSpecified
        {
            get { return this.ConditionSatisfactionTimeframeType != null && this.ConditionSatisfactionTimeframeType.enumValue != ConditionSatisfactionTimeframeBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Condition Satisfaction Timeframe Type.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOString ConditionSatisfactionTimeframeTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ConditionSatisfactionTimeframeTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConditionSatisfactionTimeframeTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConditionSatisfactionTimeframeTypeOtherDescriptionSpecified
        {
            get { return ConditionSatisfactionTimeframeTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that the condition has been waived.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOIndicator ConditionWaivedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ConditionWaivedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConditionWaivedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConditionWaivedIndicatorSpecified
        {
            get { return ConditionWaivedIndicator != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 10)]
        public CONDITION_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
