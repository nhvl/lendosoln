namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class VERIFICATION_OF_INCOME
    {
        /// <summary>
        /// Gets a value indicating whether the VERIFICATION_OF_INCOME container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.VerificationOfIncomeRequestSpecified
                    || this.VerificationOfIncomeResponseSpecified;
            }
        }

        /// <summary>
        /// An income verification request.
        /// </summary>
        [XmlElement("VERIFICATION_OF_INCOME_REQUEST", Order = 0)]
        public VERIFICATION_OF_INCOME_REQUEST VerificationOfIncomeRequest;

        /// <summary>
        /// Gets or sets a value indicating whether the VerificationOfIncomeRequest element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the VerificationOfIncomeRequest element has been assigned a value.</value>
        [XmlIgnore]
        public bool VerificationOfIncomeRequestSpecified
        {
            get { return this.VerificationOfIncomeRequest != null && this.VerificationOfIncomeRequest.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// An income verification response.
        /// </summary>
        [XmlElement("VERIFICATION_OF_INCOME_RESPONSE", Order = 1)]
        public VERIFICATION_OF_INCOME_RESPONSE VerificationOfIncomeResponse;

        /// <summary>
        /// Gets or sets a value indicating whether the VerificationOfIncomeResponse element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the VerificationOfIncomeResponse element has been assigned a value.</value>
        [XmlIgnore]
        public bool VerificationOfIncomeResponseSpecified
        {
            get { return this.VerificationOfIncomeResponse != null && this.VerificationOfIncomeResponse.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public VERIFICATION_OF_INCOME_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
