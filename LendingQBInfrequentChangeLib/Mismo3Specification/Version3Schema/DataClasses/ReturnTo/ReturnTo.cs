namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class RETURN_TO
    {
        /// <summary>
        /// Gets a value indicating whether the RETURN_TO container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.PreferredResponsesSpecified;
            }
        }

        /// <summary>
        /// A list of preferred responses.
        /// </summary>
        [XmlElement("PREFERRED_RESPONSES", Order = 0)]
        public PREFERRED_RESPONSES PreferredResponses;

        /// <summary>
        /// Gets or sets a value indicating whether the PreferredResponses element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PreferredResponses element has been assigned a value.</value>
        [XmlIgnore]
        public bool PreferredResponsesSpecified
        {
            get { return this.PreferredResponses != null && this.PreferredResponses.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public RETURN_TO_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
