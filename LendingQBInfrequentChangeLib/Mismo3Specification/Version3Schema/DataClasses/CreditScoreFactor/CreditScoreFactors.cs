namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CREDIT_SCORE_FACTORS
    {
        /// <summary>
        /// Gets a value indicating whether the CREDIT_SCORE_FACTORS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditScoreFactorSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of credit score factors.
        /// </summary>
        [XmlElement("CREDIT_SCORE_FACTOR", Order = 0)]
		public List<CREDIT_SCORE_FACTOR> CreditScoreFactor = new List<CREDIT_SCORE_FACTOR>();

        /// <summary>
        /// Gets or sets a value indicating whether the CreditScoreFactor element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditScoreFactor element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditScoreFactorSpecified
        {
            get { return this.CreditScoreFactor != null && this.CreditScoreFactor.Count(c => c != null && c.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public CREDIT_SCORE_FACTORS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
