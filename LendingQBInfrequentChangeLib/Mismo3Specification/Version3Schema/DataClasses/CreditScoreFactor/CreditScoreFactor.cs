namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class CREDIT_SCORE_FACTOR
    {
        /// <summary>
        /// Gets a value indicating whether the CREDIT_SCORE_FACTOR container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditScoreFactorCodeSpecified
                    || this.CreditScoreFactorTextSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// Factors of the borrowers credit which give context to the referenced credit score. The factor is normally a numeric code. The Credit Score Factor Text contains the text for the factor code.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOCode CreditScoreFactorCode;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditScoreFactorCode element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditScoreFactorCode element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditScoreFactorCodeSpecified
        {
            get { return CreditScoreFactorCode != null; }
            set { }
        }

        /// <summary>
        /// The text remarks associated with a Credit Score Factor.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString CreditScoreFactorText;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditScoreFactorText element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditScoreFactorText element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditScoreFactorTextSpecified
        {
            get { return CreditScoreFactorText != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public CREDIT_SCORE_FACTOR_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
