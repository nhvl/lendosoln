namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class BASEMENT_FEATURES
    {
        /// <summary>
        /// Gets a value indicating whether the BASEMENT_FEATURES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BasementFeatureSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of basement features.
        /// </summary>
        [XmlElement("BASEMENT_FEATURE", Order = 0)]
		public List<BASEMENT_FEATURE> BasementFeature = new List<BASEMENT_FEATURE>();

        /// <summary>
        /// Gets or sets a value indicating whether the BasementFeature element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BasementFeature element has been assigned a value.</value>
        [XmlIgnore]
        public bool BasementFeatureSpecified
        {
            get { return this.BasementFeature != null && this.BasementFeature.Count(b => b != null && b.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public BASEMENT_FEATURES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
