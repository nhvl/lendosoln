namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class BASEMENT_FEATURE
    {
        /// <summary>
        /// Gets a value indicating whether the BASEMENT_FEATURE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BasementFeatureDescriptionSpecified
                    || this.BasementFeatureTypeOtherDescriptionSpecified
                    || this.BasementFeatureTypeSpecified
                    || this.ConditionRatingTypeSpecified
                    || this.ExtensionSpecified
                    || this.MaterialDescriptionSpecified
                    || this.QualityRatingDescriptionSpecified
                    || this.QualityRatingTypeSpecified;
            }
        }

        /// <summary>
        /// A free-form text field used to describe or comment on the feature specified in the Basement Feature Type.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString BasementFeatureDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the BasementFeatureDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BasementFeatureDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool BasementFeatureDescriptionSpecified
        {
            get { return BasementFeatureDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the basement feature that is present in the basement.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<BasementFeatureBase> BasementFeatureType;

        /// <summary>
        /// Gets or sets a value indicating whether the BasementFeatureType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BasementFeatureType element has been assigned a value.</value>
        [XmlIgnore]
        public bool BasementFeatureTypeSpecified
        {
            get { return this.BasementFeatureType != null && this.BasementFeatureType.enumValue != BasementFeatureBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the Basement Feature if Other is selected as the Basement Feature Type.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString BasementFeatureTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the BasementFeatureTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BasementFeatureTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool BasementFeatureTypeOtherDescriptionSpecified
        {
            get { return BasementFeatureTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A rating of the condition of the identified component.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOEnum<ConditionRatingBase> ConditionRatingType;

        /// <summary>
        /// Gets or sets a value indicating whether the ConditionRatingType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConditionRatingType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConditionRatingTypeSpecified
        {
            get { return this.ConditionRatingType != null && this.ConditionRatingType.enumValue != ConditionRatingBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe in detail the material used in the identified component.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString MaterialDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MaterialDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MaterialDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MaterialDescriptionSpecified
        {
            get { return MaterialDescription != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe in detail the quality rating of the identified component.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString QualityRatingDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the QualityRatingDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the QualityRatingDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool QualityRatingDescriptionSpecified
        {
            get { return QualityRatingDescription != null; }
            set { }
        }

        /// <summary>
        /// A rating of the quality of the identified component type.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOEnum<QualityRatingBase> QualityRatingType;

        /// <summary>
        /// Gets or sets a value indicating whether the QualityRatingType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the QualityRatingType element has been assigned a value.</value>
        [XmlIgnore]
        public bool QualityRatingTypeSpecified
        {
            get { return this.QualityRatingType != null && this.QualityRatingType.enumValue != QualityRatingBase.Blank; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 7)]
        public BASEMENT_FEATURE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
