namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class DELINQUENCY
    {
        /// <summary>
        /// Gets a value indicating whether the DELINQUENCY container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DelinquencyDetailSpecified
                    || this.DelinquencyEventsSpecified
                    || this.DelinquencyHardshipsSpecified
                    || this.DelinquencyStatusesSpecified
                    || this.ExtensionSpecified;
            }
        }


        /// <summary>
        /// Details about the delinquency.
        /// </summary>
        [XmlElement("DELINQUENCY_DETAIL", Order = 0)]
        public DELINQUENCY_DETAIL DelinquencyDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the DelinquencyDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DelinquencyDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool DelinquencyDetailSpecified
        {
            get { return this.DelinquencyDetail != null && this.DelinquencyDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Events associated with the delinquency.
        /// </summary>
        [XmlElement("DELINQUENCY_EVENTS", Order = 1)]
        public DELINQUENCY_EVENTS DelinquencyEvents;

        /// <summary>
        /// Gets or sets a value indicating whether the DelinquencyEvents element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DelinquencyEvents element has been assigned a value.</value>
        [XmlIgnore]
        public bool DelinquencyEventsSpecified
        {
            get { return this.DelinquencyEvents != null && this.DelinquencyEvents.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Hardships associated with the delinquency.
        /// </summary>
        [XmlElement("DELINQUENCY_HARDSHIPS", Order = 2)]
        public DELINQUENCY_HARDSHIPS DelinquencyHardships;

        /// <summary>
        /// Gets or sets a value indicating whether the DelinquencyHardships element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DelinquencyHardships element has been assigned a value.</value>
        [XmlIgnore]
        public bool DelinquencyHardshipsSpecified
        {
            get { return this.DelinquencyHardships != null && this.DelinquencyHardships.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Statuses of the delinquency.
        /// </summary>
        [XmlElement("DELINQUENCY_STATUSES", Order = 3)]
        public DELINQUENCY_STATUSES DelinquencyStatuses;

        /// <summary>
        /// Gets or sets a value indicating whether the DelinquencyStatuses element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DelinquencyStatuses element has been assigned a value.</value>
        [XmlIgnore]
        public bool DelinquencyStatusesSpecified
        {
            get { return this.DelinquencyStatuses != null && this.DelinquencyStatuses.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public DELINQUENCY_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
