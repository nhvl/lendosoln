namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class DELINQUENCIES
    {
        /// <summary>
        /// Gets a value indicating whether the DELINQUENCIES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DelinquencySpecified
                    || this.DelinquencySummarySpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of delinquencies.
        /// </summary>
        [XmlElement("DELINQUENCY", Order = 0)]
		public List<DELINQUENCY> Delinquency = new List<DELINQUENCY>();

        /// <summary>
        /// Gets or sets a value indicating whether the Delinquency element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Delinquency element has been assigned a value.</value>
        [XmlIgnore]
        public bool DelinquencySpecified
        {
            get { return this.Delinquency != null && this.Delinquency.Count(d => d != null && d.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// Summary of the delinquency collection.
        /// </summary>
        [XmlElement("DELINQUENCY_SUMMARY", Order = 1)]
        public DELINQUENCY_SUMMARY DelinquencySummary;

        /// <summary>
        /// Gets or sets a value indicating whether the DelinquencySummary element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DelinquencySummary element has been assigned a value.</value>
        [XmlIgnore]
        public bool DelinquencySummarySpecified
        {
            get { return this.DelinquencySummary != null && this.DelinquencySummary.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public DELINQUENCIES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
