namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class DELINQUENCY_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the DELINQUENCY_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BankruptcySuspenseBalanceAmountSpecified
                    || this.BorrowerSolicitationExpirationDateSpecified
                    || this.BorrowerVacancyReasonTypeOtherDescriptionSpecified
                    || this.BorrowerVacancyReasonTypeSpecified
                    || this.BreachLetterExpirationDateSpecified
                    || this.DelinquencyEffectiveDateSpecified
                    || this.DelinquencyReportingPropertyOverallConditionTypeOtherDescriptionSpecified
                    || this.DelinquencyReportingPropertyOverallConditionTypeSpecified
                    || this.EmailAttemptsCountSpecified
                    || this.ExtensionSpecified
                    || this.FaceToFaceInterviewsCountSpecified
                    || this.FormLettersCountSpecified
                    || this.LienRecommendationTypeOtherDescriptionSpecified
                    || this.LienRecommendationTypeSpecified
                    || this.NumberOfCallAttemptsCountSpecified
                    || this.OutgoingCallMethodTypeSpecified
                    || this.PropertyCurrentOccupancyTypeSpecified
                    || this.RightPartyContactCountSpecified
                    || this.SubordinateLienHolderPaymentAmountSpecified
                    || this.TaxAdvancesOnAccountsNotEscrowedIndicatorSpecified;
            }
        }

        /// <summary>
        /// Unapplied funds related to a loan that is in bankruptcy.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount BankruptcySuspenseBalanceAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the BankruptcySuspenseBalanceAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BankruptcySuspenseBalanceAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool BankruptcySuspenseBalanceAmountSpecified
        {
            get { return BankruptcySuspenseBalanceAmount != null; }
            set { }
        }

        /// <summary>
        /// The expiration date of the last solicitation offered to the borrower.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMODate BorrowerSolicitationExpirationDate;

        /// <summary>
        /// Gets or sets a value indicating whether the BorrowerSolicitationExpirationDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BorrowerSolicitationExpirationDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool BorrowerSolicitationExpirationDateSpecified
        {
            get { return BorrowerSolicitationExpirationDate != null; }
            set { }
        }

        /// <summary>
        /// Specifies the reason that the borrower does not occupy the delinquent property.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<BorrowerVacancyReasonBase> BorrowerVacancyReasonType;

        /// <summary>
        /// Gets or sets a value indicating whether the BorrowerVacancyReasonType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BorrowerVacancyReasonType element has been assigned a value.</value>
        [XmlIgnore]
        public bool BorrowerVacancyReasonTypeSpecified
        {
            get { return this.BorrowerVacancyReasonType != null && this.BorrowerVacancyReasonType.enumValue != BorrowerVacancyReasonBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Borrower Vacancy Reason Type.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString BorrowerVacancyReasonTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the BorrowerVacancyReasonTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BorrowerVacancyReasonTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool BorrowerVacancyReasonTypeOtherDescriptionSpecified
        {
            get { return BorrowerVacancyReasonTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The date the Servicer intends on commencing the foreclosure process as stated in the written notification to the borrower that they have failed to make payments, or arrangements to make repayment.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMODate BreachLetterExpirationDate;

        /// <summary>
        /// Gets or sets a value indicating whether the BreachLetterExpirationDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BreachLetterExpirationDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool BreachLetterExpirationDateSpecified
        {
            get { return BreachLetterExpirationDate != null; }
            set { }
        }

        /// <summary>
        /// The date that the delinquency of the loan started, could be at day 16 or day 30 depending on the Investor requirements.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMODate DelinquencyEffectiveDate;

        /// <summary>
        /// Gets or sets a value indicating whether the DelinquencyEffectiveDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DelinquencyEffectiveDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool DelinquencyEffectiveDateSpecified
        {
            get { return DelinquencyEffectiveDate != null; }
            set { }
        }

        /// <summary>
        /// Used to report the overall condition of the property associated with this loan as identified through a property inspection or through direct contact with Borrower, Neighbors, Real Estate Agent, etc.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOEnum<DelinquencyReportingPropertyOverallConditionBase> DelinquencyReportingPropertyOverallConditionType;

        /// <summary>
        /// Gets or sets a value indicating whether the DelinquencyReportingPropertyOverallConditionType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DelinquencyReportingPropertyOverallConditionType element has been assigned a value.</value>
        [XmlIgnore]
        public bool DelinquencyReportingPropertyOverallConditionTypeSpecified
        {
            get { return this.DelinquencyReportingPropertyOverallConditionType != null && this.DelinquencyReportingPropertyOverallConditionType.enumValue != DelinquencyReportingPropertyOverallConditionBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the data source type if Other is selected as the Delinquency Reporting Property Overall Condition Type.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOString DelinquencyReportingPropertyOverallConditionTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the DelinquencyReportingPropertyOverallConditionTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DelinquencyReportingPropertyOverallConditionTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool DelinquencyReportingPropertyOverallConditionTypeOtherDescriptionSpecified
        {
            get { return DelinquencyReportingPropertyOverallConditionTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The number of email attempts to contact the borrower in the current month.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOCount EmailAttemptsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the EmailAttemptsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EmailAttemptsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool EmailAttemptsCountSpecified
        {
            get { return EmailAttemptsCount != null; }
            set { }
        }

        /// <summary>
        /// The number of  face to face interviews with the borrower in current month.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOCount FaceToFaceInterviewsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the FaceToFaceInterviewsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FaceToFaceInterviewsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool FaceToFaceInterviewsCountSpecified
        {
            get { return FaceToFaceInterviewsCount != null; }
            set { }
        }

        /// <summary>
        /// The number of form letters (30, 60, 90) sent to the borrower in current month.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOCount FormLettersCount;

        /// <summary>
        /// Gets or sets a value indicating whether the FormLettersCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FormLettersCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool FormLettersCountSpecified
        {
            get { return FormLettersCount != null; }
            set { }
        }

        /// <summary>
        /// Specifies the recommendation to the investor from the servicer on loan disposition of the senior lien when the subject loan is a subordinate lien.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOEnum<LienRecommendationBase> LienRecommendationType;

        /// <summary>
        /// Gets or sets a value indicating whether the LienRecommendationType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LienRecommendationType element has been assigned a value.</value>
        [XmlIgnore]
        public bool LienRecommendationTypeSpecified
        {
            get { return this.LienRecommendationType != null && this.LienRecommendationType.enumValue != LienRecommendationBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected as the Lien Recommendation Type.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOString LienRecommendationTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the LienRecommendationTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LienRecommendationTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool LienRecommendationTypeOtherDescriptionSpecified
        {
            get { return LienRecommendationTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The number of attempts to call the borrower in the reporting period.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOCount NumberOfCallAttemptsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the NumberOfCallAttemptsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NumberOfCallAttemptsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool NumberOfCallAttemptsCountSpecified
        {
            get { return NumberOfCallAttemptsCount != null; }
            set { }
        }

        /// <summary>
        /// Specifies the method of placing the most recent call to borrower, whether manually or using a dialer.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOEnum<OutgoingCallMethodBase> OutgoingCallMethodType;

        /// <summary>
        /// Gets or sets a value indicating whether the OutgoingCallMethodType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OutgoingCallMethodType element has been assigned a value.</value>
        [XmlIgnore]
        public bool OutgoingCallMethodTypeSpecified
        {
            get { return this.OutgoingCallMethodType != null && this.OutgoingCallMethodType.enumValue != OutgoingCallMethodBase.Blank; }
            set { }
        }

        /// <summary>
        /// Specifies the property occupancy status of a subject property.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOEnum<PropertyCurrentOccupancyBase> PropertyCurrentOccupancyType;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyCurrentOccupancyType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyCurrentOccupancyType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyCurrentOccupancyTypeSpecified
        {
            get { return this.PropertyCurrentOccupancyType != null && this.PropertyCurrentOccupancyType.enumValue != PropertyCurrentOccupancyBase.Blank; }
            set { }
        }

        /// <summary>
        /// The number of contacts made by the Servicer or the agent of the Servicer in the reporting period with the Borrower, co-Borrower, a representative of the estate or anyone that is legally responsible for representing any of these entities.
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMOCount RightPartyContactCount;

        /// <summary>
        /// Gets or sets a value indicating whether the RightPartyContactCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RightPartyContactCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool RightPartyContactCountSpecified
        {
            get { return RightPartyContactCount != null; }
            set { }
        }

        /// <summary>
        /// The aggregate payment amount permitted to be made to subordinate lien holders in exchange for lien releases.
        /// </summary>
        [XmlElement(Order = 17)]
        public MISMOAmount SubordinateLienHolderPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the SubordinateLienHolderPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SubordinateLienHolderPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool SubordinateLienHolderPaymentAmountSpecified
        {
            get { return SubordinateLienHolderPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the Servicer has incurred advances to pay delinquent taxes on a non-escrowed account.
        /// </summary>
        [XmlElement(Order = 18)]
        public MISMOIndicator TaxAdvancesOnAccountsNotEscrowedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the TaxAdvancesOnAccountsNotEscrowedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TaxAdvancesOnAccountsNotEscrowedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool TaxAdvancesOnAccountsNotEscrowedIndicatorSpecified
        {
            get { return TaxAdvancesOnAccountsNotEscrowedIndicator != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 19)]
        public DELINQUENCY_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
