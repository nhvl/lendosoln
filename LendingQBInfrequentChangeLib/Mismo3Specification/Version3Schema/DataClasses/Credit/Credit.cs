namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CREDIT
    {
        /// <summary>
        /// Gets a value indicating whether the CREDIT container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditRequestSpecified
                    || this.CreditResponseSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A request for a credit report.
        /// </summary>
        [XmlElement("CREDIT_REQUEST", Order = 0)]
        public CREDIT_REQUEST CreditRequest;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditRequest element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditRequest element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditRequestSpecified
        {
            get { return this.CreditRequest != null && this.CreditRequest.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A response to a credit request.
        /// </summary>
        [XmlElement("CREDIT_RESPONSE", Order = 1)]
        public CREDIT_RESPONSE CreditResponse;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditResponse element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditResponse element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditResponseSpecified
        {
            get { return this.CreditResponse != null && this.CreditResponse.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public CREDIT_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
