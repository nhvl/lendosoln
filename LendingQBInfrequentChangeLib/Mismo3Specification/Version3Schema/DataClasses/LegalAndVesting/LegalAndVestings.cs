namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class LEGAL_AND_VESTINGS
    {
        /// <summary>
        /// Gets a value indicating whether the LEGAL_AND_VESTINGS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.LegalAndVestingSpecified;
            }
        }

        /// <summary>
        /// A collection of legal and vesting.
        /// </summary>
        [XmlElement("LEGAL_AND_VESTING", Order = 0)]
		public List<LEGAL_AND_VESTING> LegalAndVesting = new List<LEGAL_AND_VESTING>();

        /// <summary>
        /// Gets or sets a value indicating whether the LegalAndVesting element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LegalAndVesting element has been assigned a value.</value>
        [XmlIgnore]
        public bool LegalAndVestingSpecified
        {
            get { return this.LegalAndVesting != null && this.LegalAndVesting.Count(l => l != null && l.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public LEGAL_AND_VESTINGS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
