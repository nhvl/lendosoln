namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class DRAW_ACTIVITY
    {
        /// <summary>
        /// Gets a value indicating whether the DRAW_ACTIVITY container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.LoanDrawAmountSpecified
                    || this.LoanDrawDateSpecified
                    || this.LoanTotalDrawAmountSpecified;
            }
        }

        /// <summary>
        /// The dollar amount borrowed against the loan for the draw transaction.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount LoanDrawAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanDrawAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanDrawAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanDrawAmountSpecified
        {
            get { return LoanDrawAmount != null; }
            set { }
        }

        /// <summary>
        /// The date on which the draw transaction was executed.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMODate LoanDrawDate;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanDrawDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanDrawDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanDrawDateSpecified
        {
            get { return LoanDrawDate != null; }
            set { }
        }

        /// <summary>
        /// The total amount of all draws that the borrower has made as of the date reported.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOAmount LoanTotalDrawAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanTotalDrawAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanTotalDrawAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanTotalDrawAmountSpecified
        {
            get { return LoanTotalDrawAmount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public DRAW_ACTIVITY_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
