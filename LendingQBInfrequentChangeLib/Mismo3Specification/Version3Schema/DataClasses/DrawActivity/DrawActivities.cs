namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class DRAW_ACTIVITIES
    {
        /// <summary>
        /// Gets a value indicating whether the DRAW_ACTIVITIES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DrawActivitySpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of draw activities.
        /// </summary>
        [XmlElement("DRAW_ACTIVITY", Order = 0)]
		public List<DRAW_ACTIVITY> DrawActivity = new List<DRAW_ACTIVITY>();

        /// <summary>
        /// Gets or sets a value indicating whether the DrawActivity element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DrawActivity element has been assigned a value.</value>
        [XmlIgnore]
        public bool DrawActivitySpecified
        {
            get { return this.DrawActivity != null && this.DrawActivity.Count(d => d != null && d.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public DRAW_ACTIVITIES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
