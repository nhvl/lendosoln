namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class INTEGRATED_DISCLOSURE_DETAIL_3_3_1
    {
        /// <summary>
        /// Gets a value indicating whether the INTEGRATED_DISCLOSURE_DETAIL_3_3_1 container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.IncludeSignatureLinesIndicatorSpecified;
            }
        }

        /// <summary>
        /// When true, indicates the creditor wants to include consumer signature lines on the integrated disclosure.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIndicator IncludeSignatureLinesIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the IncludeSignatureLinesIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IncludeSignatureLinesIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool IncludeSignatureLinesIndicatorSpecified
        {
            get { return IncludeSignatureLinesIndicator != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public INTEGRATED_DISCLOSURE_DETAIL_3_3_1_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
