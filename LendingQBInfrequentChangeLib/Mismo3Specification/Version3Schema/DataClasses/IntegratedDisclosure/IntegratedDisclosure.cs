namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class INTEGRATED_DISCLOSURE
    {
        /// <summary>
        /// Gets a value indicating whether the INTEGRATED_DISCLOSURE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CashToCloseItemsSpecified
                    || this.EstimatedPropertyCostSpecified
                    || this.ExtensionSpecified
                    || this.IntegratedDisclosureDetailSpecified
                    || this.IntegratedDisclosureSectionSummariesSpecified
                    || this.OtherLoanConsiderationsAndDisclosuresItemsSpecified
                    || this.ProjectedPaymentsSpecified;
            }
        }

        /// <summary>
        /// Items related to cash to close.
        /// </summary>
        [XmlElement("CASH_TO_CLOSE_ITEMS", Order = 0)]
        public CASH_TO_CLOSE_ITEMS CashToCloseItems;

        /// <summary>
        /// Gets or sets a value indicating whether the CashToCloseItems element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CashToCloseItems element has been assigned a value.</value>
        [XmlIgnore]
        public bool CashToCloseItemsSpecified
        {
            get { return this.CashToCloseItems != null && this.CashToCloseItems.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The estimated cost of the property.
        /// </summary>
        [XmlElement("ESTIMATED_PROPERTY_COST", Order = 1)]
        public ESTIMATED_PROPERTY_COST EstimatedPropertyCost;

        /// <summary>
        /// Gets or sets a value indicating whether the EstimatedPropertyCost element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EstimatedPropertyCost element has been assigned a value.</value>
        [XmlIgnore]
        public bool EstimatedPropertyCostSpecified
        {
            get { return this.EstimatedPropertyCost != null && this.EstimatedPropertyCost.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details on the integrated disclosure.
        /// </summary>
        [XmlElement("INTEGRATED_DISCLOSURE_DETAIL", Order = 2)]
        public INTEGRATED_DISCLOSURE_DETAIL IntegratedDisclosureDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the IntegratedDisclosureDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IntegratedDisclosureDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool IntegratedDisclosureDetailSpecified
        {
            get { return this.IntegratedDisclosureDetail != null && this.IntegratedDisclosureDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Summaries of integrated disclosure sections.
        /// </summary>
        [XmlElement("INTEGRATED_DISCLOSURE_SECTION_SUMMARIES", Order = 3)]
        public INTEGRATED_DISCLOSURE_SECTION_SUMMARIES IntegratedDisclosureSectionSummaries;

        /// <summary>
        /// Gets or sets a value indicating whether the IntegratedDisclosureSectionSummaries element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IntegratedDisclosureSectionSummaries element has been assigned a value.</value>
        [XmlIgnore]
        public bool IntegratedDisclosureSectionSummariesSpecified
        {
            get { return this.IntegratedDisclosureSectionSummaries != null && this.IntegratedDisclosureSectionSummaries.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Other items related to loan considerations and disclosures.
        /// </summary>
        [XmlElement("OTHER_LOAN_CONSIDERATIONS_AND_DISCLOSURES_ITEMS", Order = 4)]
        public OTHER_LOAN_CONSIDERATIONS_AND_DISCLOSURES_ITEMS OtherLoanConsiderationsAndDisclosuresItems;

        /// <summary>
        /// Gets or sets a value indicating whether the OtherLoanConsiderationsAndDisclosuresItems element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OtherLoanConsiderationsAndDisclosuresItems element has been assigned a value.</value>
        [XmlIgnore]
        public bool OtherLoanConsiderationsAndDisclosuresItemsSpecified
        {
            get { return this.OtherLoanConsiderationsAndDisclosuresItems != null && this.OtherLoanConsiderationsAndDisclosuresItems.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Projected payments on the integrated disclosure.
        /// </summary>
        [XmlElement("PROJECTED_PAYMENTS", Order = 5)]
        public PROJECTED_PAYMENTS ProjectedPayments;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectedPayments element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectedPayments element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectedPaymentsSpecified
        {
            get { return this.ProjectedPayments != null && this.ProjectedPayments.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 6)]
        public INTEGRATED_DISCLOSURE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
