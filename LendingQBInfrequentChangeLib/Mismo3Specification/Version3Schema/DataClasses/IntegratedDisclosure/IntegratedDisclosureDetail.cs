namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class INTEGRATED_DISCLOSURE_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the INTEGRATED_DISCLOSURE_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.FirstYearTotalEscrowPaymentAmountSpecified
                    || this.FirstYearTotalEscrowPaymentDescriptionSpecified
                    || this.FirstYearTotalNonEscrowPaymentAmountSpecified
                    || this.FirstYearTotalNonEscrowPaymentDescriptionSpecified
                    || this.FiveYearPrincipalReductionComparisonAmountSpecified
                    || this.FiveYearTotalOfPaymentsComparisonAmountSpecified
                    || this.IntegratedDisclosureDocumentTypeOtherDescriptionSpecified
                    || this.IntegratedDisclosureDocumentTypeSpecified
                    || this.IntegratedDisclosureEstimatedClosingCostsExpirationDatetimeSpecified
                    || this.IntegratedDisclosureHomeEquityLoanIndicatorSpecified
                    || this.IntegratedDisclosureInitialPrincipalAndInterestPaymentAmountSpecified
                    || this.IntegratedDisclosureIssuedDateSpecified
                    || this.IntegratedDisclosureLoanProductDescriptionSpecified;
            }
        }

        /// <summary>
        /// Estimated total amount of one year of escrowed property costs.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount FirstYearTotalEscrowPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the FirstYearTotalEscrowPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FirstYearTotalEscrowPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool FirstYearTotalEscrowPaymentAmountSpecified
        {
            get { return FirstYearTotalEscrowPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the estimated total amount of one year of escrowed property costs.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString FirstYearTotalEscrowPaymentDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the FirstYearTotalEscrowPaymentDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FirstYearTotalEscrowPaymentDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool FirstYearTotalEscrowPaymentDescriptionSpecified
        {
            get { return FirstYearTotalEscrowPaymentDescription != null; }
            set { }
        }

        /// <summary>
        /// Estimated total amount of one year of non-escrowed property costs.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOAmount FirstYearTotalNonEscrowPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the FirstYearTotalNonEscrowPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FirstYearTotalNonEscrowPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool FirstYearTotalNonEscrowPaymentAmountSpecified
        {
            get { return FirstYearTotalNonEscrowPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// A free form text field used to describe the estimated total amount of one year of escrowed property costs.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString FirstYearTotalNonEscrowPaymentDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the FirstYearTotalNonEscrowPaymentDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FirstYearTotalNonEscrowPaymentDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool FirstYearTotalNonEscrowPaymentDescriptionSpecified
        {
            get { return FirstYearTotalNonEscrowPaymentDescription != null; }
            set { }
        }

        /// <summary>
        /// The total amount of principal paid by the borrower in the first five years of the loan term as disclosed on the Loan Estimate for comparison purposes. 
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOAmount FiveYearPrincipalReductionComparisonAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the FiveYearPrincipalReductionComparisonAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FiveYearPrincipalReductionComparisonAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool FiveYearPrincipalReductionComparisonAmountSpecified
        {
            get { return FiveYearPrincipalReductionComparisonAmount != null; }
            set { }
        }

        /// <summary>
        /// The total paid in principal, interest, mortgage insurance, and loan costs by the borrower in the first five years of the loan term as disclosed on the Loan Estimate for comparison purposes.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOAmount FiveYearTotalOfPaymentsComparisonAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the FiveYearTotalOfPaymentsComparisonAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FiveYearTotalOfPaymentsComparisonAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool FiveYearTotalOfPaymentsComparisonAmountSpecified
        {
            get { return FiveYearTotalOfPaymentsComparisonAmount != null; }
            set { }
        }

        /// <summary>
        /// Specifies the type of integrated disclosure document.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOEnum<IntegratedDisclosureDocumentBase> IntegratedDisclosureDocumentType;

        /// <summary>
        /// Gets or sets a value indicating whether the IntegratedDisclosureDocumentType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IntegratedDisclosureDocumentType element has been assigned a value.</value>
        [XmlIgnore]
        public bool IntegratedDisclosureDocumentTypeSpecified
        {
            get { return this.IntegratedDisclosureDocumentType != null && this.IntegratedDisclosureDocumentType.enumValue != IntegratedDisclosureDocumentBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture additional information when Other is selected for Integrated Disclosure Document Type.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOString IntegratedDisclosureDocumentTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the IntegratedDisclosureDocumentTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IntegratedDisclosureDocumentTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool IntegratedDisclosureDocumentTypeOtherDescriptionSpecified
        {
            get { return IntegratedDisclosureDocumentTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The date and time that the estimate of the loan closing costs amount expires as reflected on the integrated disclosure document.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMODatetime IntegratedDisclosureEstimatedClosingCostsExpirationDatetime;

        /// <summary>
        /// Gets or sets a value indicating whether the IntegratedDisclosureEstimatedClosingCostsExpirationDateTime element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IntegratedDisclosureEstimatedClosingCostsExpirationDateTime element has been assigned a value.</value>
        [XmlIgnore]
        public bool IntegratedDisclosureEstimatedClosingCostsExpirationDatetimeSpecified
        {
            get { return IntegratedDisclosureEstimatedClosingCostsExpirationDatetime != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates the loan is an extension of credit that does not involve the purchase of real property or the initial construction of a dwelling and will not be used to refinance an existing obligation.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOIndicator IntegratedDisclosureHomeEquityLoanIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the IntegratedDisclosureHomeEquityLoanIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IntegratedDisclosureHomeEquityLoanIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool IntegratedDisclosureHomeEquityLoanIndicatorSpecified
        {
            get { return IntegratedDisclosureHomeEquityLoanIndicator != null; }
            set { }
        }

        /// <summary>
        /// The initial principal and interest payment amount as calculated and disclosed on a single instance of the integrated disclosure document.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOAmount IntegratedDisclosureInitialPrincipalAndInterestPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the IntegratedDisclosureInitialPrincipalAndInterestPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IntegratedDisclosureInitialPrincipalAndInterestPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool IntegratedDisclosureInitialPrincipalAndInterestPaymentAmountSpecified
        {
            get { return IntegratedDisclosureInitialPrincipalAndInterestPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// The date the integrated disclosure was mailed or delivered to the borrower as reflected on a single instance of the integrated disclosure document.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMODate IntegratedDisclosureIssuedDate;

        /// <summary>
        /// Gets or sets a value indicating whether the IntegratedDisclosureIssuedDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IntegratedDisclosureIssuedDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool IntegratedDisclosureIssuedDateSpecified
        {
            get { return IntegratedDisclosureIssuedDate != null && !string.IsNullOrEmpty(IntegratedDisclosureIssuedDate.Value) ; }
            set { }
        }

        /// <summary>
        /// A description of the loan product which includes, when applicable, certain loan features added to the loan product that may change the periodic loan payment.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOString IntegratedDisclosureLoanProductDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the IntegratedDisclosureLoanProductDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IntegratedDisclosureLoanProductDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool IntegratedDisclosureLoanProductDescriptionSpecified
        {
            get { return IntegratedDisclosureLoanProductDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 13)]
        public INTEGRATED_DISCLOSURE_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null ? Extension.ShouldSerialize : false; }
            set { }
        }
    }
}
