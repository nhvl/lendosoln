namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class MI_POOL_INSURANCE
    {
        /// <summary>
        /// Gets a value indicating whether the MI_POOL_INSURANCE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.MICompanyNameTypeOtherDescriptionSpecified
                    || this.MICompanyNameTypeSpecified
                    || this.MIPoolInsuranceCertificateIdentifierSpecified
                    || this.MIPoolInsuranceMasterCommitmentIdentifierSpecified;
            }
        }

        /// <summary>
        /// To convey the private MI company short/common name from whom the private mortgage insurance coverage was obtained.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<MICompanyNameBase> MICompanyNameType;

        /// <summary>
        /// Gets or sets a value indicating whether the MICompanyNameType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MICompanyNameType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MICompanyNameTypeSpecified
        {
            get { return this.MICompanyNameType != null && this.MICompanyNameType.enumValue != MICompanyNameBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the MI company name when Other is selected for the MI Company Name Type.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString MICompanyNameTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MICompanyNameTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MICompanyNameTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MICompanyNameTypeOtherDescriptionSpecified
        {
            get { return MICompanyNameTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Policy number issued by the provider.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOIdentifier MIPoolInsuranceCertificateIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPoolInsuranceCertificateIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPoolInsuranceCertificateIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPoolInsuranceCertificateIdentifierSpecified
        {
            get { return MIPoolInsuranceCertificateIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Corporate master commitment number.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOIdentifier MIPoolInsuranceMasterCommitmentIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the MIPoolInsuranceMasterCommitmentIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MIPoolInsuranceMasterCommitmentIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool MIPoolInsuranceMasterCommitmentIdentifierSpecified
        {
            get { return MIPoolInsuranceMasterCommitmentIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public MI_POOL_INSURANCE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
