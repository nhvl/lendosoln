namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class INCOME_DOCUMENTATIONS
    {
        /// <summary>
        /// Gets a value indicating whether the INCOME_DOCUMENTATIONS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.IncomeDocumentationSpecified;
            }
        }

        /// <summary>
        /// A collection of income documentation.
        /// </summary>
        [XmlElement("INCOME_DOCUMENTATION", Order = 0)]
		public List<INCOME_DOCUMENTATION> IncomeDocumentation = new List<INCOME_DOCUMENTATION>();

        /// <summary>
        /// Gets or sets a value indicating whether the IncomeDocumentation element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IncomeDocumentation element has been assigned a value.</value>
        [XmlIgnore]
        public bool IncomeDocumentationSpecified
        {
            get { return this.IncomeDocumentation != null && this.IncomeDocumentation.Count(i => i != null && i.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public INCOME_DOCUMENTATIONS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
