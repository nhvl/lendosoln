namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class MONETARY_EVENT_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the MONETARY_EVENT_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.LoanPartialPrepaymentAmountSpecified
                    || this.MonetaryEventAppliedDateSpecified
                    || this.MonetaryEventBorrowerIncentiveCurtailmentAmountSpecified
                    || this.MonetaryEventDeferredInterestAmountSpecified
                    || this.MonetaryEventDeferredPrincipalCurtailmentAmountSpecified
                    || this.MonetaryEventDeferredUPBAmountSpecified
                    || this.MonetaryEventDueDateSpecified
                    || this.MonetaryEventEscrowPaymentAmountSpecified
                    || this.MonetaryEventExceptionInterestAmountSpecified
                    || this.MonetaryEventGrossInterestAmountSpecified
                    || this.MonetaryEventGrossPrincipalAmountSpecified
                    || this.MonetaryEventInterestBearingUPBAmountSpecified
                    || this.MonetaryEventInterestPaidThroughDateSpecified
                    || this.MonetaryEventInvestorRemittanceAmountSpecified
                    || this.MonetaryEventInvestorRemittanceEffectiveDateSpecified
                    || this.MonetaryEventNetInterestAmountSpecified
                    || this.MonetaryEventNetPrincipalAmountSpecified
                    || this.MonetaryEventOptionalProductsPaymentAmountSpecified
                    || this.MonetaryEventPaymentAmountSpecified
                    || this.MonetaryEventPaymentIdentifierSpecified
                    || this.MonetaryEventReversalIndicatorSpecified
                    || this.MonetaryEventScheduledUPBAmountSpecified
                    || this.MonetaryEventSubsequentRecoveryAmountSpecified
                    || this.MonetaryEventTotalPaymentAmountSpecified
                    || this.MonetaryEventTypeOtherDescriptionSpecified
                    || this.MonetaryEventTypeSpecified
                    || this.MonetaryEventUPBAmountSpecified;
            }
        }

        /// <summary>
        /// The amount the borrower has sent in advance of the scheduled payment date when that amount is not enough to cover the entire amount of the next payment.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount LoanPartialPrepaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanPartialPrepaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanPartialPrepaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanPartialPrepaymentAmountSpecified
        {
            get { return LoanPartialPrepaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// The date that the loan monetary activity being reported was applied or the effective date for a back-dated activity.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMODate MonetaryEventAppliedDate;

        /// <summary>
        /// Gets or sets a value indicating whether the MonetaryEventAppliedDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MonetaryEventAppliedDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool MonetaryEventAppliedDateSpecified
        {
            get { return MonetaryEventAppliedDate != null; }
            set { }
        }

        /// <summary>
        /// The amount of any Borrower incentive payments applied to the unpaid principal balance of the Mortgage for monetary event being reported.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOAmount MonetaryEventBorrowerIncentiveCurtailmentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the MonetaryEventBorrowerIncentiveCurtailmentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MonetaryEventBorrowerIncentiveCurtailmentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool MonetaryEventBorrowerIncentiveCurtailmentAmountSpecified
        {
            get { return MonetaryEventBorrowerIncentiveCurtailmentAmount != null; }
            set { }
        }

        /// <summary>
        /// The portion of the payment allocated to repayment of deferred interest due to the investor.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOAmount MonetaryEventDeferredInterestAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the MonetaryEventDeferredInterestAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MonetaryEventDeferredInterestAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool MonetaryEventDeferredInterestAmountSpecified
        {
            get { return MonetaryEventDeferredInterestAmount != null; }
            set { }
        }

        /// <summary>
        /// The amount of any curtailment applied to the deferred UPB for the monetary event being reported.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOAmount MonetaryEventDeferredPrincipalCurtailmentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the MonetaryEventDeferredPrincipalCurtailmentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MonetaryEventDeferredPrincipalCurtailmentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool MonetaryEventDeferredPrincipalCurtailmentAmountSpecified
        {
            get { return MonetaryEventDeferredPrincipalCurtailmentAmount != null; }
            set { }
        }

        /// <summary>
        /// For Mortgages with a partial principal forbearance, the amount of the deferred (non-interest bearing) unpaid principal balance as of the monetary event being reported.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOAmount MonetaryEventDeferredUPBAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the MonetaryEventDeferredUPBAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MonetaryEventDeferredUPBAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool MonetaryEventDeferredUPBAmountSpecified
        {
            get { return MonetaryEventDeferredUPBAmount != null; }
            set { }
        }

        /// <summary>
        /// The due date of the loan monetary activity being reported.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMODate MonetaryEventDueDate;

        /// <summary>
        /// Gets or sets a value indicating whether the MonetaryEventDueDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MonetaryEventDueDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool MonetaryEventDueDateSpecified
        {
            get { return MonetaryEventDueDate != null; }
            set { }
        }

        /// <summary>
        /// That portion of the payment applied to the escrow balance. This reflects all escrow items (e.g. hazard and taxes) associated with the loan.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOAmount MonetaryEventEscrowPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the MonetaryEventEscrowPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MonetaryEventEscrowPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool MonetaryEventEscrowPaymentAmountSpecified
        {
            get { return MonetaryEventEscrowPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// An interest adjustment caused by an exception event such as a payoff, REO, or foreclosure sale.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOAmount MonetaryEventExceptionInterestAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the MonetaryEventExceptionInterestAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MonetaryEventExceptionInterestAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool MonetaryEventExceptionInterestAmountSpecified
        {
            get { return MonetaryEventExceptionInterestAmount != null; }
            set { }
        }

        /// <summary>
        /// The portion of the payment allocated to interest, based upon the current loan interest rate, for a loan monetary event.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOAmount MonetaryEventGrossInterestAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the MonetaryEventGrossInterestAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MonetaryEventGrossInterestAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool MonetaryEventGrossInterestAmountSpecified
        {
            get { return MonetaryEventGrossInterestAmount != null; }
            set { }
        }

        /// <summary>
        /// The portion of the payment being reported for a loan monetary event that is applied to principal.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOAmount MonetaryEventGrossPrincipalAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the MonetaryEventGrossPrincipalAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MonetaryEventGrossPrincipalAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool MonetaryEventGrossPrincipalAmountSpecified
        {
            get { return MonetaryEventGrossPrincipalAmount != null; }
            set { }
        }

        /// <summary>
        ///  For Mortgages with a partial principal forbearance, the amount of the interest-bearing unpaid principal balance as of the monetary event being reported.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOAmount MonetaryEventInterestBearingUPBAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the MonetaryEventInterestBearingUPBAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MonetaryEventInterestBearingUPBAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool MonetaryEventInterestBearingUPBAmountSpecified
        {
            get { return MonetaryEventInterestBearingUPBAmount != null; }
            set { }
        }

        /// <summary>
        /// The date through which interest is paid with the current payment in the Monetary Event structure. This is the effective date from which interest will be calculated for the application of the next payment. (For example, used for Daily Simple Interest Loans.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMODate MonetaryEventInterestPaidThroughDate;

        /// <summary>
        /// Gets or sets a value indicating whether the MonetaryEventInterestPaidThroughDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MonetaryEventInterestPaidThroughDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool MonetaryEventInterestPaidThroughDateSpecified
        {
            get { return MonetaryEventInterestPaidThroughDate != null; }
            set { }
        }

        /// <summary>
        /// The total remittance amount being reported to the Investor by the Servicer for this monetary event.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOAmount MonetaryEventInvestorRemittanceAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the MonetaryEventInvestorRemittanceAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MonetaryEventInvestorRemittanceAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool MonetaryEventInvestorRemittanceAmountSpecified
        {
            get { return MonetaryEventInvestorRemittanceAmount != null; }
            set { }
        }

        /// <summary>
        /// The date the investor should draft the remittance from the lenders bank account for this monetary event. This is typically the day after the remittance is reported to the investor.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMODate MonetaryEventInvestorRemittanceEffectiveDate;

        /// <summary>
        /// Gets or sets a value indicating whether the MonetaryEventInvestorRemittanceEffectiveDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MonetaryEventInvestorRemittanceEffectiveDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool MonetaryEventInvestorRemittanceEffectiveDateSpecified
        {
            get { return MonetaryEventInvestorRemittanceEffectiveDate != null; }
            set { }
        }

        /// <summary>
        /// The portion of the Gross Interest Amount, associated with a loan monetary event, due to the Investor.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOAmount MonetaryEventNetInterestAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the MonetaryEventNetInterestAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MonetaryEventNetInterestAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool MonetaryEventNetInterestAmountSpecified
        {
            get { return MonetaryEventNetInterestAmount != null; }
            set { }
        }

        /// <summary>
        /// The portion of the Loan Monetary Event Gross Principal Amount due to the Investor.
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMOAmount MonetaryEventNetPrincipalAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the MonetaryEventNetPrincipalAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MonetaryEventNetPrincipalAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool MonetaryEventNetPrincipalAmountSpecified
        {
            get { return MonetaryEventNetPrincipalAmount != null; }
            set { }
        }

        /// <summary>
        /// That portion of the payment applied to optional product features. This reflects a summary of all optional Life and Accident and Health Insurance premiums and other optional product payments required to be reported to the investor.
        /// </summary>
        [XmlElement(Order = 17)]
        public MISMOAmount MonetaryEventOptionalProductsPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the MonetaryEventOptionalProductsPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MonetaryEventOptionalProductsPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool MonetaryEventOptionalProductsPaymentAmountSpecified
        {
            get { return MonetaryEventOptionalProductsPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// The amount received from the borrower associated with this monetary event.
        /// </summary>
        [XmlElement(Order = 18)]
        public MISMOAmount MonetaryEventPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the MonetaryEventPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MonetaryEventPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool MonetaryEventPaymentAmountSpecified
        {
            get { return MonetaryEventPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// A unique identification for a specific monetary event payment. Identifier owner may be referenced using OwnerIdentifierURI.
        /// </summary>
        [XmlElement(Order = 19)]
        public MISMOIdentifier MonetaryEventPaymentIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the MonetaryEventPaymentIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MonetaryEventPaymentIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool MonetaryEventPaymentIdentifierSpecified
        {
            get { return MonetaryEventPaymentIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the loan monetary activity is being reversed.
        /// </summary>
        [XmlElement(Order = 20)]
        public MISMOIndicator MonetaryEventReversalIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the MonetaryEventReversalIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MonetaryEventReversalIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool MonetaryEventReversalIndicatorSpecified
        {
            get { return MonetaryEventReversalIndicator != null; }
            set { }
        }

        /// <summary>
        /// The current unpaid principal balance on the loan as of the date of the loan monetary event, based on an agreed upon schedule.
        /// </summary>
        [XmlElement(Order = 21)]
        public MISMOAmount MonetaryEventScheduledUPBAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the MonetaryEventScheduledUPBAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MonetaryEventScheduledUPBAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool MonetaryEventScheduledUPBAmountSpecified
        {
            get { return MonetaryEventScheduledUPBAmount != null; }
            set { }
        }

        /// <summary>
        /// Any recovery of funds from the borrower after a short sale, foreclosure, REO sale, etc.  
        /// </summary>
        [XmlElement(Order = 22)]
        public MISMOAmount MonetaryEventSubsequentRecoveryAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the MonetaryEventSubsequentRecoveryAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MonetaryEventSubsequentRecoveryAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool MonetaryEventSubsequentRecoveryAmountSpecified
        {
            get { return MonetaryEventSubsequentRecoveryAmount != null; }
            set { }
        }

        /// <summary>
        /// The total dollar amount of the payment received as part of a specified monetary event.
        /// </summary>
        [XmlElement(Order = 23)]
        public MISMOAmount MonetaryEventTotalPaymentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the MonetaryEventTotalPaymentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MonetaryEventTotalPaymentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool MonetaryEventTotalPaymentAmountSpecified
        {
            get { return MonetaryEventTotalPaymentAmount != null; }
            set { }
        }

        /// <summary>
        /// Identifies type of monetary event that affects balances associated with a loan.
        /// </summary>
        [XmlElement(Order = 24)]
        public MISMOEnum<MonetaryEventBase> MonetaryEventType;

        /// <summary>
        /// Gets or sets a value indicating whether the MonetaryEventType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MonetaryEventType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MonetaryEventTypeSpecified
        {
            get { return this.MonetaryEventType != null && this.MonetaryEventType.enumValue != MonetaryEventBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture the Loan Monetary Event Type if Other is selected.
        /// </summary>
        [XmlElement(Order = 25)]
        public MISMOString MonetaryEventTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MonetaryEventTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MonetaryEventTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MonetaryEventTypeOtherDescriptionSpecified
        {
            get { return MonetaryEventTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The unpaid principal balance on the loan after applying this monetary event.
        /// </summary>
        [XmlElement(Order = 26)]
        public MISMOAmount MonetaryEventUPBAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the MonetaryEventUPBAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MonetaryEventUPBAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool MonetaryEventUPBAmountSpecified
        {
            get { return MonetaryEventUPBAmount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 27)]
        public MONETARY_EVENT_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
