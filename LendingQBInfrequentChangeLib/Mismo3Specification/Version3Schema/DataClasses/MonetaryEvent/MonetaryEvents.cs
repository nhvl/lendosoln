namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class MONETARY_EVENTS
    {
        /// <summary>
        /// Gets a value indicating whether the MONETARY_EVENTS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.MonetaryEventSpecified;
            }
        }

        /// <summary>
        /// A collection of monetary events.
        /// </summary>
        [XmlElement("MONETARY_EVENT", Order = 0)]
		public List<MONETARY_EVENT> MonetaryEvent = new List<MONETARY_EVENT>();

        /// <summary>
        /// Gets or sets a value indicating whether the MonetaryEvent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MonetaryEvent element has been assigned a value.</value>
        [XmlIgnore]
        public bool MonetaryEventSpecified
        {
            get { return this.MonetaryEvent != null && this.MonetaryEvent.Count(m => m != null && m.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public MONETARY_EVENTS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
