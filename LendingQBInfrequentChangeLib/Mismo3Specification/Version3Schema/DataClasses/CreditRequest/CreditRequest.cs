namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CREDIT_REQUEST
    {
        /// <summary>
        /// Gets a value indicating whether the CREDIT_REQUEST container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditInquiriesSpecified
                    || this.CreditLiabilitiesSpecified
                    || this.CreditPublicRecordsSpecified
                    || this.CreditRequestDataInformationSpecified
                    || this.CreditRequestDatasSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// Request for credit inquiries.
        /// </summary>
        [XmlElement("CREDIT_INQUIRIES", Order = 0)]
        public CREDIT_INQUIRIES CreditInquiries;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditInquiries element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditInquiries element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditInquiriesSpecified
        {
            get { return this.CreditInquiries != null && this.CreditInquiries.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Liabilities of a credit request.
        /// </summary>
        [XmlElement("CREDIT_LIABILITIES", Order = 1)]
        public CREDIT_LIABILITIES CreditLiabilities;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditLiabilities element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditLiabilities element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditLiabilitiesSpecified
        {
            get { return this.CreditLiabilities != null && this.CreditLiabilities.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Public records related to credit.
        /// </summary>
        [XmlElement("CREDIT_PUBLIC_RECORDS", Order = 2)]
        public CREDIT_PUBLIC_RECORDS CreditPublicRecords;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditPublicRecords element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditPublicRecords element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditPublicRecordsSpecified
        {
            get { return this.CreditPublicRecords != null && this.CreditPublicRecords.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Information about credit request data.
        /// </summary>
        [XmlElement("CREDIT_REQUEST_DATA_INFORMATION", Order = 3)]
        public CREDIT_REQUEST_DATA_INFORMATION CreditRequestDataInformation;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditRequestDataInformation element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditRequestDataInformation element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditRequestDataInformationSpecified
        {
            get { return this.CreditRequestDataInformation != null && this.CreditRequestDataInformation.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Data about a credit request.
        /// </summary>
        [XmlElement("CREDIT_REQUEST_DATAS", Order = 4)]
        public CREDIT_REQUEST_DATAS CreditRequestDatas;

        /// <summary>
        /// Gets or sets a value indicating whether the Credit Request Data element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Credit Request Data element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditRequestDatasSpecified
        {
            get { return this.CreditRequestDatas != null && this.CreditRequestDatas.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 5)]
        public CREDIT_REQUEST_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
