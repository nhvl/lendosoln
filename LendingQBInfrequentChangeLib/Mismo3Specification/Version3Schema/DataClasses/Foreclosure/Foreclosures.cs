namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class FORECLOSURES
    {
        /// <summary>
        /// Gets a value indicating whether the FORECLOSURES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ForeclosureSpecified;
            }
        }

        /// <summary>
        /// A collection of foreclosures.
        /// </summary>
        [XmlElement("FORECLOSURE", Order = 0)]
		public List<FORECLOSURE> Foreclosure = new List<FORECLOSURE>();

        /// <summary>
        /// Gets or sets a value indicating whether the Foreclosure element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Foreclosure element has been assigned a value.</value>
        [XmlIgnore]
        public bool ForeclosureSpecified
        {
            get { return this.Foreclosure != null && this.Foreclosure.Count(f => f != null && f.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public FORECLOSURES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
