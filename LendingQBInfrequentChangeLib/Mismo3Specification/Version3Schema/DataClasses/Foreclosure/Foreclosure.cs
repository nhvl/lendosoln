namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class FORECLOSURE
    {
        /// <summary>
        /// Gets a value indicating whether the FORECLOSURE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ForeclosureDelaysSpecified
                    || this.ForeclosureDetailSpecified
                    || this.ForeclosureExpensesSpecified;
            }
        }

        /// <summary>
        /// Delays on a foreclosure.
        /// </summary>
        [XmlElement("FORECLOSURE_DELAYS", Order = 0)]
        public FORECLOSURE_DELAYS ForeclosureDelays;

        /// <summary>
        /// Gets or sets a value indicating whether the ForeclosureDelays element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ForeclosureDelays element has been assigned a value.</value>
        [XmlIgnore]
        public bool ForeclosureDelaysSpecified
        {
            get { return this.ForeclosureDelays != null && this.ForeclosureDelays.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details on a foreclosure.
        /// </summary>
        [XmlElement("FORECLOSURE_DETAIL", Order = 1)]
        public FORECLOSURE_DETAIL ForeclosureDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the ForeclosureDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ForeclosureDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool ForeclosureDetailSpecified
        {
            get { return this.ForeclosureDetail != null && this.ForeclosureDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Expenses associated with a foreclosure.
        /// </summary>
        [XmlElement("FORECLOSURE_EXPENSES", Order = 2)]
        public FORECLOSURE_EXPENSES ForeclosureExpenses;

        /// <summary>
        /// Gets or sets a value indicating whether the ForeclosureExpenses element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ForeclosureExpenses element has been assigned a value.</value>
        [XmlIgnore]
        public bool ForeclosureExpensesSpecified
        {
            get { return this.ForeclosureExpenses != null && this.ForeclosureExpenses.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public FORECLOSURE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
