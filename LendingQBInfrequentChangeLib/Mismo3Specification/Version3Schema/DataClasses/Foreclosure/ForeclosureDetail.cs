namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class FORECLOSURE_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the FORECLOSURE_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ActiveForeclosureIndicatorSpecified
                    || this.BidActualAmountSpecified
                    || this.BidMinimumAmountSpecified
                    || this.BidOpenAmountSpecified
                    || this.DaysToForeclosureSaleCountSpecified
                    || this.DeficiencyRightsPreservedIndicatorSpecified
                    || this.EvictionApprovedDateSpecified
                    || this.EvictionStartDateSpecified
                    || this.ExtensionSpecified
                    || this.ForeclosureCaseDismissedReasonTypeOtherDescriptionSpecified
                    || this.ForeclosureCaseDismissedReasonTypeSpecified
                    || this.ForeclosureCaseIdentifierSpecified
                    || this.ForeclosureFeeAmountSpecified
                    || this.ForeclosureMethodTypeOtherDescriptionSpecified
                    || this.ForeclosureMethodTypeSpecified
                    || this.ForeclosurePerformanceMaximumDaysCountSpecified
                    || this.ForeclosureProcedureEndedDateSpecified
                    || this.ForeclosureProcedureEndedReasonTypeOtherDescriptionSpecified
                    || this.ForeclosureProcedureEndedReasonTypeSpecified
                    || this.ForeclosureSaleCancellationReasonTypeOtherDescriptionSpecified
                    || this.ForeclosureSaleCancellationReasonTypeSpecified
                    || this.ForeclosureSalePostponementReasonDateSpecified
                    || this.ForeclosureSalePostponementReasonTypeOtherDescriptionSpecified
                    || this.ForeclosureSalePostponementReasonTypeSpecified
                    || this.ForeclosureSaleProceedsAmountSpecified
                    || this.ForeclosureSaleProjectedDateSpecified
                    || this.ForeclosureSaleRescindedReasonTypeOtherDescriptionSpecified
                    || this.ForeclosureSaleRescindedReasonTypeSpecified
                    || this.ForeclosureSaleScheduledDateSpecified
                    || this.ForeclosureSaleTotalSubordinateLienholderPayoutAmountSpecified
                    || this.ForeclosureStatusTypeSpecified
                    || this.IRSLienNotificationIndicatorSpecified
                    || this.RedemptionExercisedIndicatorSpecified
                    || this.RedemptionPeriodExpirationDateSpecified
                    || this.RedemptionPeriodTermMonthsCountSpecified;
            }
        }

        /// <summary>
        /// When true, indicates that the loan is actively in the foreclosure process.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIndicator ActiveForeclosureIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ActiveForeclosureIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ActiveForeclosureIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ActiveForeclosureIndicatorSpecified
        {
            get { return ActiveForeclosureIndicator != null; }
            set { }
        }

        /// <summary>
        /// The final amount bid for the property at a foreclosure sale. The sales price for a foreclosed property.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOAmount BidActualAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the BidActualAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BidActualAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool BidActualAmountSpecified
        {
            get { return BidActualAmount != null; }
            set { }
        }

        /// <summary>
        /// The minimum bid for a foreclosure sale that would be accepted  by an investor.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOAmount BidMinimumAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the BidMinimumAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BidMinimumAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool BidMinimumAmountSpecified
        {
            get { return BidMinimumAmount != null; }
            set { }
        }

        /// <summary>
        /// The initial opening bid amount presented at the foreclosure sale for a property.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOAmount BidOpenAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the BidOpenAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BidOpenAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool BidOpenAmountSpecified
        {
            get { return BidOpenAmount != null; }
            set { }
        }

        /// <summary>
        /// The number of days between the current date and the estimated or scheduled foreclosure sale day.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOCount DaysToForeclosureSaleCount;

        /// <summary>
        /// Gets or sets a value indicating whether the DaysToForeclosureSaleCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DaysToForeclosureSaleCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool DaysToForeclosureSaleCountSpecified
        {
            get { return DaysToForeclosureSaleCount != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that upon the foreclosure sale completion deficiency rights against the borrowers are preserved.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOIndicator DeficiencyRightsPreservedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the DeficiencyRightsPreservedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DeficiencyRightsPreservedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool DeficiencyRightsPreservedIndicatorSpecified
        {
            get { return DeficiencyRightsPreservedIndicator != null; }
            set { }
        }

        /// <summary>
        /// Date the court revokes legal possession of the property from the borrower.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMODate EvictionApprovedDate;

        /// <summary>
        /// Gets or sets a value indicating whether the EvictionApprovedDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EvictionApprovedDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool EvictionApprovedDateSpecified
        {
            get { return EvictionApprovedDate != null; }
            set { }
        }

        /// <summary>
        /// Date the servicer initiates eviction of the borrower.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMODate EvictionStartDate;

        /// <summary>
        /// Gets or sets a value indicating whether the EvictionStartDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EvictionStartDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool EvictionStartDateSpecified
        {
            get { return EvictionStartDate != null; }
            set { }
        }

        /// <summary>
        /// The reason that a foreclosure case was dismissed by the processing party.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOEnum<ForeclosureCaseDismissedReasonBase> ForeclosureCaseDismissedReasonType;

        /// <summary>
        /// Gets or sets a value indicating whether the ForeclosureCaseDismissedReasonType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ForeclosureCaseDismissedReasonType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ForeclosureCaseDismissedReasonTypeSpecified
        {
            get { return this.ForeclosureCaseDismissedReasonType != null && this.ForeclosureCaseDismissedReasonType.enumValue != ForeclosureCaseDismissedReasonBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Foreclosure Case Dismissed Reason Type.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOString ForeclosureCaseDismissedReasonTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ForeclosureCaseDismissedReasonTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ForeclosureCaseDismissedReasonTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ForeclosureCaseDismissedReasonTypeOtherDescriptionSpecified
        {
            get { return ForeclosureCaseDismissedReasonTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A unique identifier assigned to a foreclosure case by the processing party. The party assigning the identifier can be specified using the IdentifierOwnerURI attribute.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOIdentifier ForeclosureCaseIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the ForeclosureCaseIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ForeclosureCaseIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool ForeclosureCaseIdentifierSpecified
        {
            get { return ForeclosureCaseIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of fees and other costs associated with foreclosure based on both the mortgage's time in foreclosure and the average amounts per the state (United States) where the property is located.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOAmount ForeclosureFeeAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ForeclosureFeeAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ForeclosureFeeAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ForeclosureFeeAmountSpecified
        {
            get { return ForeclosureFeeAmount != null; }
            set { }
        }

        /// <summary>
        /// The type of foreclosure method based on the property's geographic location.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOEnum<ForeclosureMethodBase> ForeclosureMethodType;

        /// <summary>
        /// Gets or sets a value indicating whether the ForeclosureMethodType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ForeclosureMethodType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ForeclosureMethodTypeSpecified
        {
            get { return this.ForeclosureMethodType != null && this.ForeclosureMethodType.enumValue != ForeclosureMethodBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Foreclosure Method Type.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOString ForeclosureMethodTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ForeclosureMethodTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ForeclosureMethodTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ForeclosureMethodTypeOtherDescriptionSpecified
        {
            get { return ForeclosureMethodTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The maximum number of allowable days between referral to attorney (or trustee) and foreclosure sale date, based on the property's geographic location and investor guidelines.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOCount ForeclosurePerformanceMaximumDaysCount;

        /// <summary>
        /// Gets or sets a value indicating whether the ForeclosurePerformanceMaximumDaysCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ForeclosurePerformanceMaximumDaysCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ForeclosurePerformanceMaximumDaysCountSpecified
        {
            get { return ForeclosurePerformanceMaximumDaysCount != null; }
            set { }
        }

        /// <summary>
        /// The date the foreclosure proceedings ended.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMODate ForeclosureProcedureEndedDate;

        /// <summary>
        /// Gets or sets a value indicating whether the ForeclosureProcedureEndedDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ForeclosureProcedureEndedDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ForeclosureProcedureEndedDateSpecified
        {
            get { return ForeclosureProcedureEndedDate != null; }
            set { }
        }

        /// <summary>
        /// Indicates the reason foreclosure proceedings ended.
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMOEnum<ForeclosureProcedureEndedReasonBase> ForeclosureProcedureEndedReasonType;

        /// <summary>
        /// Gets or sets a value indicating whether the ForeclosureProcedureEndedReasonType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ForeclosureProcedureEndedReasonType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ForeclosureProcedureEndedReasonTypeSpecified
        {
            get { return this.ForeclosureProcedureEndedReasonType != null && this.ForeclosureProcedureEndedReasonType.enumValue != ForeclosureProcedureEndedReasonBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected as the Foreclosure Procedure Ended Reason Type.
        /// </summary>
        [XmlElement(Order = 17)]
        public MISMOString ForeclosureProcedureEndedReasonTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ForeclosureProcedureEndedReasonTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ForeclosureProcedureEndedReasonTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ForeclosureProcedureEndedReasonTypeOtherDescriptionSpecified
        {
            get { return ForeclosureProcedureEndedReasonTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The reason that a foreclosure sale was cancelled.
        /// </summary>
        [XmlElement(Order = 18)]
        public MISMOEnum<ForeclosureSaleCancellationReasonBase> ForeclosureSaleCancellationReasonType;

        /// <summary>
        /// Gets or sets a value indicating whether the ForeclosureSaleCancellationReasonType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ForeclosureSaleCancellationReasonType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ForeclosureSaleCancellationReasonTypeSpecified
        {
            get { return this.ForeclosureSaleCancellationReasonType != null && this.ForeclosureSaleCancellationReasonType.enumValue != ForeclosureSaleCancellationReasonBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for the Foreclosure Sale Cancellation Reason Type.
        /// </summary>
        [XmlElement(Order = 19)]
        public MISMOString ForeclosureSaleCancellationReasonTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ForeclosureSaleCancellationReasonTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ForeclosureSaleCancellationReasonTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ForeclosureSaleCancellationReasonTypeOtherDescriptionSpecified
        {
            get { return ForeclosureSaleCancellationReasonTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Date associated with the Foreclosure Sale Postponement Reason Type.
        /// </summary>
        [XmlElement(Order = 20)]
        public MISMODate ForeclosureSalePostponementReasonDate;

        /// <summary>
        /// Gets or sets a value indicating whether the ForeclosureSalePostponementReasonDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ForeclosureSalePostponementReasonDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ForeclosureSalePostponementReasonDateSpecified
        {
            get { return ForeclosureSalePostponementReasonDate != null; }
            set { }
        }

        /// <summary>
        /// Specifies the reason for a postponement of the foreclosure sale.
        /// </summary>
        [XmlElement(Order = 21)]
        public MISMOEnum<ForeclosureSalePostponementReasonBase> ForeclosureSalePostponementReasonType;

        /// <summary>
        /// Gets or sets a value indicating whether the ForeclosureSalePostponementReasonType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ForeclosureSalePostponementReasonType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ForeclosureSalePostponementReasonTypeSpecified
        {
            get { return this.ForeclosureSalePostponementReasonType != null && this.ForeclosureSalePostponementReasonType.enumValue != ForeclosureSalePostponementReasonBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected as the Foreclosure Sale Postponement Reason Type.
        /// </summary>
        [XmlElement(Order = 22)]
        public MISMOString ForeclosureSalePostponementReasonTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ForeclosureSalePostponementReasonTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ForeclosureSalePostponementReasonTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ForeclosureSalePostponementReasonTypeOtherDescriptionSpecified
        {
            get { return ForeclosureSalePostponementReasonTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The gross sales price less court approved fees and costs allowed by statute or law for the foreclosure sale.
        /// </summary>
        [XmlElement(Order = 23)]
        public MISMOAmount ForeclosureSaleProceedsAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ForeclosureSaleProceedsAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ForeclosureSaleProceedsAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ForeclosureSaleProceedsAmountSpecified
        {
            get { return ForeclosureSaleProceedsAmount != null; }
            set { }
        }

        /// <summary>
        /// Date the foreclosure sale is projected to occur.
        /// </summary>
        [XmlElement(Order = 24)]
        public MISMODate ForeclosureSaleProjectedDate;

        /// <summary>
        /// Gets or sets a value indicating whether the ForeclosureSaleProjectedDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ForeclosureSaleProjectedDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ForeclosureSaleProjectedDateSpecified
        {
            get { return ForeclosureSaleProjectedDate != null; }
            set { }
        }

        /// <summary>
        /// The reason that a foreclosure sale was rescinded.
        /// </summary>
        [XmlElement(Order = 25)]
        public MISMOEnum<ForeclosureSaleRescindedReasonBase> ForeclosureSaleRescindedReasonType;

        /// <summary>
        /// Gets or sets a value indicating whether the ForeclosureSaleRescindedReasonType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ForeclosureSaleRescindedReasonType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ForeclosureSaleRescindedReasonTypeSpecified
        {
            get { return this.ForeclosureSaleRescindedReasonType != null && this.ForeclosureSaleRescindedReasonType.enumValue != ForeclosureSaleRescindedReasonBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for the Foreclosure Sale Rescinded Reason Type.
        /// </summary>
        [XmlElement(Order = 26)]
        public MISMOString ForeclosureSaleRescindedReasonTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ForeclosureSaleRescindedReasonTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ForeclosureSaleRescindedReasonTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ForeclosureSaleRescindedReasonTypeOtherDescriptionSpecified
        {
            get { return ForeclosureSaleRescindedReasonTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Date the foreclosure sale has been scheduled to occur.
        /// </summary>
        [XmlElement(Order = 27)]
        public MISMODate ForeclosureSaleScheduledDate;

        /// <summary>
        /// Gets or sets a value indicating whether the ForeclosureSaleScheduledDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ForeclosureSaleScheduledDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ForeclosureSaleScheduledDateSpecified
        {
            get { return ForeclosureSaleScheduledDate != null; }
            set { }
        }

        /// <summary>
        /// The total amount paid to all subordinate lien holders from the final foreclosure sale proceeds amount.
        /// </summary>
        [XmlElement(Order = 28)]
        public MISMOAmount ForeclosureSaleTotalSubordinateLienholderPayoutAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ForeclosureSaleTotalSubordinateLienholderPayoutAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ForeclosureSaleTotalSubordinateLienholderPayoutAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ForeclosureSaleTotalSubordinateLienholderPayoutAmountSpecified
        {
            get { return ForeclosureSaleTotalSubordinateLienholderPayoutAmount != null; }
            set { }
        }

        /// <summary>
        /// Indicates the most recent step in the foreclosure proceedings.
        /// </summary>
        [XmlElement(Order = 29)]
        public MISMOEnum<ForeclosureStatusBase> ForeclosureStatusType;

        /// <summary>
        /// Gets or sets a value indicating whether the ForeclosureStatusType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ForeclosureStatusType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ForeclosureStatusTypeSpecified
        {
            get { return this.ForeclosureStatusType != null && this.ForeclosureStatusType.enumValue != ForeclosureStatusBase.Blank; }
            set { }
        }

        /// <summary>
        /// When true, indicates that the Servicer has notified the IRS that a real property with an IRS Lien attachment has been foreclosed upon.
        /// </summary>
        [XmlElement(Order = 30)]
        public MISMOIndicator IRSLienNotificationIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the IRSLienNotificationIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IRSLienNotificationIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool IRSLienNotificationIndicatorSpecified
        {
            get { return IRSLienNotificationIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates the borrowers right to redeem the property has been exercised.
        /// </summary>
        [XmlElement(Order = 31)]
        public MISMOIndicator RedemptionExercisedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the RedemptionExercisedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RedemptionExercisedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool RedemptionExercisedIndicatorSpecified
        {
            get { return RedemptionExercisedIndicator != null; }
            set { }
        }

        /// <summary>
        /// The date the redemption period expires in applicable states.
        /// </summary>
        [XmlElement(Order = 32)]
        public MISMODate RedemptionPeriodExpirationDate;

        /// <summary>
        /// Gets or sets a value indicating whether the RedemptionPeriodExpirationDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RedemptionPeriodExpirationDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool RedemptionPeriodExpirationDateSpecified
        {
            get { return RedemptionPeriodExpirationDate != null; }
            set { }
        }

        /// <summary>
        /// The number of months the borrower can redeem the property after a foreclosure sale has occurred.  The redemption period is based on the property's geographic location.
        /// </summary>
        [XmlElement(Order = 33)]
        public MISMOCount RedemptionPeriodTermMonthsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the RedemptionPeriodTermMonthsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RedemptionPeriodTermMonthsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool RedemptionPeriodTermMonthsCountSpecified
        {
            get { return RedemptionPeriodTermMonthsCount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 34)]
        public FORECLOSURE_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
