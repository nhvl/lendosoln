namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class REHABILITATION
    {
        /// <summary>
        /// Gets a value indicating whether the REHABILITATION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.RehabilitationCompletionDateSpecified
                    || this.RehabilitationContingencyPercentSpecified
                    || this.RehabilitationEstimatedPropertyValueAmountSpecified
                    || this.RehabilitationLoanFundsAmountSpecified;
            }
        }

        /// <summary>
        /// The date the rehabilitation should be completed (six months from 203K authorization date as specified on FHA 203K worksheet).
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMODate RehabilitationCompletionDate;

        /// <summary>
        /// Gets or sets a value indicating whether the RehabilitationCompletionDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RehabilitationCompletionDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool RehabilitationCompletionDateSpecified
        {
            get { return RehabilitationCompletionDate != null; }
            set { }
        }

        /// <summary>
        /// Percentage of loan amount that has been designated as a reserve to cover possible overages in repair costs.  Applies to 203K and HomeStyle loans.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOPercent RehabilitationContingencyPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the RehabilitationContingencyPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RehabilitationContingencyPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool RehabilitationContingencyPercentSpecified
        {
            get { return RehabilitationContingencyPercent != null; }
            set { }
        }

        /// <summary>
        /// The estimated value of the property when rehabilitation is complete.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOAmount RehabilitationEstimatedPropertyValueAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the RehabilitationEstimatedPropertyValueAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RehabilitationEstimatedPropertyValueAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool RehabilitationEstimatedPropertyValueAmountSpecified
        {
            get { return RehabilitationEstimatedPropertyValueAmount != null; }
            set { }
        }

        /// <summary>
        /// Total amount of rehabilitation funds included in the loan.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOAmount RehabilitationLoanFundsAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the RehabilitationLoanFundsAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RehabilitationLoanFundsAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool RehabilitationLoanFundsAmountSpecified
        {
            get { return RehabilitationLoanFundsAmount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public REHABILITATION_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
