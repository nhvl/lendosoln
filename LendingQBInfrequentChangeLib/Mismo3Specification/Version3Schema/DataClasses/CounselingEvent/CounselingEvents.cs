namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class COUNSELING_EVENTS
    {
        /// <summary>
        /// Gets a value indicating whether the COUNSELING_EVENTS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CounselingEventSpecified
                    || this.ExtensionSpecified;
            }
        }


        /// <summary>
        /// A collection of counseling events.
        /// </summary>
        [XmlElement("COUNSELING_EVENT", Order = 0)]
		public List<COUNSELING_EVENT> CounselingEvent = new List<COUNSELING_EVENT>();

        /// <summary>
        /// Gets or sets a value indicating whether the CounselingEvent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CounselingEvent element has been assigned a value.</value>
        [XmlIgnore]
        public bool CounselingEventSpecified
        {
            get { return this.CounselingEvent != null && this.CounselingEvent.Count(c => c != null && c.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public COUNSELING_EVENTS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
