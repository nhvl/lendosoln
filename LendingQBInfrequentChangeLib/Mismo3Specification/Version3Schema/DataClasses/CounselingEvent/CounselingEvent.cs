namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class COUNSELING_EVENT
    {
        /// <summary>
        /// Gets a value indicating whether the COUNSELING_EVENT container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CounselingDeliveredDateSpecified
                    || this.CounselingFormatTypeOtherDescriptionSpecified
                    || this.CounselingFormatTypeSpecified
                    || this.CounselingProviderTypeOtherDescriptionSpecified
                    || this.CounselingProviderTypeSpecified
                    || this.CounselingTypeOtherDescriptionSpecified
                    || this.CounselingTypeSpecified
                    || this.ExtensionSpecified;
            }
        }


        /// <summary>
        /// The date the mortgage counseling or education program was delivered to the borrower.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMODate CounselingDeliveredDate;

        /// <summary>
        /// Gets or sets a value indicating whether the CounselingDeliveredDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CounselingDeliveredDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool CounselingDeliveredDateSpecified
        {
            get { return CounselingDeliveredDate != null; }
            set { }
        }

        /// <summary>
        /// Identifies the educational setting of the formal borrower homeownership program in which the borrower participated as a requirement of a special mortgage program.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<CounselingFormatBase> CounselingFormatType;

        /// <summary>
        /// Gets or sets a value indicating whether the CounselingFormatType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CounselingFormatType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CounselingFormatTypeSpecified
        {
            get { return this.CounselingFormatType != null && this.CounselingFormatType.enumValue != CounselingFormatBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Counseling Format Type.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString CounselingFormatTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the CounselingFormatTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CounselingFormatTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool CounselingFormatTypeOtherDescriptionSpecified
        {
            get { return CounselingFormatTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the provider of mortgage counseling or education program attended by one or more of the borrowers.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOEnum<CounselingProviderBase> CounselingProviderType;

        /// <summary>
        /// Gets or sets a value indicating whether the CounselingProviderType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CounselingProviderType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CounselingProviderTypeSpecified
        {
            get { return this.CounselingProviderType != null && this.CounselingProviderType.enumValue != CounselingProviderBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Counseling Provider Type.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString CounselingProviderTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the CounselingProviderTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CounselingProviderTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool CounselingProviderTypeOtherDescriptionSpecified
        {
            get { return CounselingProviderTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the type of mortgage counseling or education program.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOEnum<CounselingBase> CounselingType;

        /// <summary>
        /// Gets or sets a value indicating whether the CounselingType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CounselingType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CounselingTypeSpecified
        {
            get { return this.CounselingType != null && this.CounselingType.enumValue != CounselingBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Counseling Type.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOString CounselingTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the CounselingTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CounselingTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool CounselingTypeOtherDescriptionSpecified
        {
            get { return CounselingTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 7)]
        public COUNSELING_EVENT_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
