namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class INVESTOR_REPORTING_ADDITIONAL_CHARGE_SUMMARY
    {
        /// <summary>
        /// Gets a value indicating whether the INVESTOR_REPORTING_ADDITIONAL_CHARGE_SUMMARY container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.TotalInvestorReportingAdditionalChargesAmountSpecified;
            }
        }

        /// <summary>
        /// The sum of all additional charges reported on the loan.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount TotalInvestorReportingAdditionalChargesAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the TotalInvestorReportingAdditionalChargesAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TotalInvestorReportingAdditionalChargesAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TotalInvestorReportingAdditionalChargesAmountSpecified
        {
            get { return TotalInvestorReportingAdditionalChargesAmount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public INVESTOR_REPORTING_ADDITIONAL_CHARGE_SUMMARY_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
