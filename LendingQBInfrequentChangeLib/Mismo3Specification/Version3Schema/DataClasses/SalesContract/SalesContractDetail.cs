namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class SALES_CONTRACT_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the SALES_CONTRACT_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ArmsLengthIndicatorSpecified
                    || this.BuyerSellerSameAgentIndicatorSpecified
                    || this.ExtensionSpecified
                    || this.NetSaleProceedsAmountSpecified
                    || this.PersonalPropertyAmountSpecified
                    || this.PersonalPropertyIncludedIndicatorSpecified
                    || this.PreForeclosureSaleClosingDateSpecified
                    || this.PreForeclosureSaleIndicatorSpecified
                    || this.ProjectedClosingDateSpecified
                    || this.ProposedFinancingDescriptionSpecified
                    || this.RealEstateAgentCountSpecified
                    || this.RealPropertyAmountSpecified
                    || this.SalesConcessionDescriptionSpecified
                    || this.SalesConcessionIndicatorSpecified
                    || this.SalesConditionsOfSaleDescriptionSpecified
                    || this.SalesContractAcceptedIndicatorSpecified
                    || this.SalesContractAmountSpecified
                    || this.SalesContractAnalysisDescriptionSpecified
                    || this.SalesContractDateSpecified
                    || this.SalesContractInvoiceRevewCommentDescriptionSpecified
                    || this.SalesContractInvoiceReviewedIndicatorSpecified
                    || this.SalesContractRetailerNameSpecified
                    || this.SalesContractReviewCommentDescriptionSpecified
                    || this.SalesContractReviewDescriptionSpecified
                    || this.SalesContractReviewedIndicatorSpecified
                    || this.SalesContractTermsConsistentWithMarketIndicatorSpecified
                    || this.SaleTypeOtherDescriptionSpecified
                    || this.SaleTypeSpecified
                    || this.SellerIsOwnerIndicatorSpecified
                    || this.TotalSalesConcessionAmountSpecified;
            }
        }

        /// <summary>
        /// Indicates that this is an Arms Length Transaction. An Arms length transaction is between a willing buyer and a willing seller with no undue influence on either party and there is no relationship between the parties except that of the specific transaction.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIndicator ArmsLengthIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ArmsLengthIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ArmsLengthIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ArmsLengthIndicatorSpecified
        {
            get { return ArmsLengthIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates the buyer's and the seller's agents are the same party.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOIndicator BuyerSellerSameAgentIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the BuyerSellerSameAgentIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BuyerSellerSameAgentIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool BuyerSellerSameAgentIndicatorSpecified
        {
            get { return BuyerSellerSameAgentIndicator != null; }
            set { }
        }

        /// <summary>
        /// The proceeds available from the sale after all closing costs (excluding any maintenance costs). Gross purchase price less any closing costs and cash to or from borrower.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOAmount NetSaleProceedsAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the NetSaleProceedsAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NetSaleProceedsAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool NetSaleProceedsAmountSpecified
        {
            get { return NetSaleProceedsAmount != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of personal property included in the Sales Contract Amount. 
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOAmount PersonalPropertyAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PersonalPropertyAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PersonalPropertyAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PersonalPropertyAmountSpecified
        {
            get { return PersonalPropertyAmount != null; }
            set { }
        }

        /// <summary>
        /// Indicates if there is personal property included in the contract for sale. 
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOIndicator PersonalPropertyIncludedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PersonalPropertyIncludedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PersonalPropertyIncludedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PersonalPropertyIncludedIndicatorSpecified
        {
            get { return PersonalPropertyIncludedIndicator != null; }
            set { }
        }

        /// <summary>
        /// The date the pre foreclosure sale for the subject property is scheduled to close.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMODate PreForeclosureSaleClosingDate;

        /// <summary>
        /// Gets or sets a value indicating whether the PreForeclosureSaleClosingDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PreForeclosureSaleClosingDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool PreForeclosureSaleClosingDateSpecified
        {
            get { return PreForeclosureSaleClosingDate != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that the property was in foreclosure and the sale took place prior to a foreclosure being finalized.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOIndicator PreForeclosureSaleIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the PreForeclosureSaleIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PreForeclosureSaleIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool PreForeclosureSaleIndicatorSpecified
        {
            get { return PreForeclosureSaleIndicator != null; }
            set { }
        }

        /// <summary>
        /// The projected closing date as stated on the sales contract.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMODate ProjectedClosingDate;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectedClosingDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectedClosingDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectedClosingDateSpecified
        {
            get { return ProjectedClosingDate != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field describing the terms of the loan or financing proposed in the sales transaction as determined in the analysis of the Sales Contract or transaction details by the appraiser.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOString ProposedFinancingDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ProposedFinancingDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProposedFinancingDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProposedFinancingDescriptionSpecified
        {
            get { return ProposedFinancingDescription != null; }
            set { }
        }

        /// <summary>
        /// A count of the number of real estate agents that are involved in the transaction based on the sales contract.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOCount RealEstateAgentCount;

        /// <summary>
        /// Gets or sets a value indicating whether the RealEstateAgentCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RealEstateAgentCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool RealEstateAgentCountSpecified
        {
            get { return RealEstateAgentCount != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of real property included in the Sales Contract Amount.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOAmount RealPropertyAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the RealPropertyAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RealPropertyAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool RealPropertyAmountSpecified
        {
            get { return RealPropertyAmount != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe sales concessions granted by an interested party.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOString SalesConcessionDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the SalesConcessionDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SalesConcessionDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool SalesConcessionDescriptionSpecified
        {
            get { return SalesConcessionDescription != null; }
            set { }
        }

        /// <summary>
        /// When true indicates the presence of a concession in the sales contract. 
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOIndicator SalesConcessionIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the SalesConcessionIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SalesConcessionIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool SalesConcessionIndicatorSpecified
        {
            get { return SalesConcessionIndicator != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field describing the conditions of the sale.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOString SalesConditionsOfSaleDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the SalesConditionsOfSaleDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SalesConditionsOfSaleDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool SalesConditionsOfSaleDescriptionSpecified
        {
            get { return SalesConditionsOfSaleDescription != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates that the sales contract has been reviewed and accepted by both parties.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOIndicator SalesContractAcceptedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the SalesContractAcceptedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SalesContractAcceptedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool SalesContractAcceptedIndicatorSpecified
        {
            get { return SalesContractAcceptedIndicator != null; }
            set { }
        }

        /// <summary>
        /// The amount of money the contract is for.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOAmount SalesContractAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the SalesContractAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SalesContractAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool SalesContractAmountSpecified
        {
            get { return SalesContractAmount != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used for the analysis of the terms and conditions of the property sales contract.
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMOString SalesContractAnalysisDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the SalesContractAnalysisDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SalesContractAnalysisDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool SalesContractAnalysisDescriptionSpecified
        {
            get { return SalesContractAnalysisDescription != null; }
            set { }
        }

        /// <summary>
        /// The date the sales contract was fully executed.
        /// </summary>
        [XmlElement(Order = 17)]
        public MISMODate SalesContractDate;

        /// <summary>
        /// Gets or sets a value indicating whether the SalesContractDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SalesContractDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool SalesContractDateSpecified
        {
            get { return SalesContractDate != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field reporting any comments about the sales contract or the reason it was not reviewed.
        /// </summary>
        [XmlElement(Order = 18)]
        public MISMOString SalesContractInvoiceRevewCommentDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the SalesContractInvoiceReviewCommentDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SalesContractInvoiceReviewCommentDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool SalesContractInvoiceRevewCommentDescriptionSpecified
        {
            get { return SalesContractInvoiceRevewCommentDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the appraiser did review the sales contract.
        /// </summary>
        [XmlElement(Order = 19)]
        public MISMOIndicator SalesContractInvoiceReviewedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the SalesContractInvoiceReviewedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SalesContractInvoiceReviewedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool SalesContractInvoiceReviewedIndicatorSpecified
        {
            get { return SalesContractInvoiceReviewedIndicator != null; }
            set { }
        }

        /// <summary>
        /// The name of the vendor of the contract.
        /// </summary>
        [XmlElement(Order = 20)]
        public MISMOString SalesContractRetailerName;

        /// <summary>
        /// Gets or sets a value indicating whether the SalesContractRetailerName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SalesContractRetailerName element has been assigned a value.</value>
        [XmlIgnore]
        public bool SalesContractRetailerNameSpecified
        {
            get { return SalesContractRetailerName != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field describing the sales contract.
        /// </summary>
        [XmlElement(Order = 21)]
        public MISMOString SalesContractReviewCommentDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the SalesContractReviewCommentDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SalesContractReviewCommentDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool SalesContractReviewCommentDescriptionSpecified
        {
            get { return SalesContractReviewCommentDescription != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field describing the result of the review and analysis of the contract for sale or why analysis was not performed.
        /// </summary>
        [XmlElement(Order = 22)]
        public MISMOString SalesContractReviewDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the SalesContractReviewDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SalesContractReviewDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool SalesContractReviewDescriptionSpecified
        {
            get { return SalesContractReviewDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicated that the appraiser reviewed and analyzed the contract for sale for the subject purchase transaction.
        /// </summary>
        [XmlElement(Order = 23)]
        public MISMOIndicator SalesContractReviewedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the SalesContractReviewedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SalesContractReviewedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool SalesContractReviewedIndicatorSpecified
        {
            get { return SalesContractReviewedIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the terms and conditions of the sales contract are consistent with the local market.
        /// </summary>
        [XmlElement(Order = 24)]
        public MISMOIndicator SalesContractTermsConsistentWithMarketIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the SalesContractTermsConsistentWithMarketIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SalesContractTermsConsistentWithMarketIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool SalesContractTermsConsistentWithMarketIndicatorSpecified
        {
            get { return SalesContractTermsConsistentWithMarketIndicator != null; }
            set { }
        }

        /// <summary>
        /// Quantifies the type of sales transaction for the property as indicated from the analysis of the sales contract.
        /// </summary>
        [XmlElement(Order = 25)]
        public MISMOEnum<SaleBase> SaleType;

        /// <summary>
        /// Gets or sets a value indicating whether the SaleType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SaleType element has been assigned a value.</value>
        [XmlIgnore]
        public bool SaleTypeSpecified
        {
            get { return this.SaleType != null && this.SaleType.enumValue != SaleBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for the ListingStatusType.
        /// </summary>
        [XmlElement(Order = 26)]
        public MISMOString SaleTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the SaleTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SaleTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool SaleTypeOtherDescriptionSpecified
        {
            get { return SaleTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the property seller is the owner of public record for the subject property.
        /// </summary>
        [XmlElement(Order = 27)]
        public MISMOIndicator SellerIsOwnerIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the SellerIsOwnerIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SellerIsOwnerIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool SellerIsOwnerIndicatorSpecified
        {
            get { return SellerIsOwnerIndicator != null; }
            set { }
        }

        /// <summary>
        /// The sum total dollar amount of the value of all sales concessions granted by an interested party including such items as furniture, carpeting, decorator allowances, automobiles, vacations, securities, giveaways or other sales incentives.
        /// </summary>
        [XmlElement(Order = 28)]
        public MISMOAmount TotalSalesConcessionAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the TotalSalesConcessionAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the TotalSalesConcessionAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool TotalSalesConcessionAmountSpecified
        {
            get { return TotalSalesConcessionAmount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 29)]
        public SALES_CONTRACT_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
