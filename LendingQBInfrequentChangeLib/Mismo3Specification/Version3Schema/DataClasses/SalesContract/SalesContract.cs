namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class SALES_CONTRACT
    {
        /// <summary>
        /// Gets a value indicating whether the SALES_CONTRACT container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.SalesConcessionsSpecified
                    || this.SalesContingenciesSpecified
                    || this.SalesContractDetailSpecified;
            }
        }

        /// <summary>
        /// Concessions related to a sale.
        /// </summary>
        [XmlElement("SALES_CONCESSIONS", Order = 0)]
        public SALES_CONCESSIONS SalesConcessions;

        /// <summary>
        /// Gets or sets a value indicating whether the SalesConcessions element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SalesConcessions element has been assigned a value.</value>
        [XmlIgnore]
        public bool SalesConcessionsSpecified
        {
            get { return this.SalesConcessions != null && this.SalesConcessions.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Contingencies related to a sale.
        /// </summary>
        [XmlElement("SALES_CONTINGENCIES", Order = 1)]
        public SALES_CONTINGENCIES SalesContingencies;

        /// <summary>
        /// Gets or sets a value indicating whether the SalesContingencies element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SalesContingencies element has been assigned a value.</value>
        [XmlIgnore]
        public bool SalesContingenciesSpecified
        {
            get { return this.SalesContingencies != null && this.SalesContingencies.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details on a sales contract.
        /// </summary>
        [XmlElement("SALES_CONTRACT_DETAIL", Order = 2)]
        public SALES_CONTRACT_DETAIL SalesContractDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the SalesContractDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SalesContractDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool SalesContractDetailSpecified
        {
            get { return this.SalesContractDetail != null && this.SalesContractDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public SALES_CONTRACT_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
