namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class SALES_CONTRACTS
    {
        /// <summary>
        /// Gets a value indicating whether the SALES_CONTRACTS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.SalesContractSpecified;
            }
        }

        /// <summary>
        /// A collection of sales contracts.
        /// </summary>
        [XmlElement("SALES_CONTRACT", Order = 0)]
		public List<SALES_CONTRACT> SalesContract = new List<SALES_CONTRACT>();

        /// <summary>
        /// Gets or sets a value indicating whether the SalesContract element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SalesContract element has been assigned a value.</value>
        [XmlIgnore]
        public bool SalesContractSpecified
        {
            get { return this.SalesContract != null && this.SalesContract.Count(s => s != null && s.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public SALES_CONTRACTS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
