namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class SECURITY_SYSTEMS
    {
        /// <summary>
        /// Gets a value indicating whether the SECURITY_SYSTEMS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.SecuritySystemSpecified;
            }
        }

        /// <summary>
        /// A collection of security systems.
        /// </summary>
        [XmlElement("SECURITY_SYSTEM", Order = 0)]
		public List<SECURITY_SYSTEM> SecuritySystem = new List<SECURITY_SYSTEM>();

        /// <summary>
        /// Gets or sets a value indicating whether the SecuritySystem element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SecuritySystem element has been assigned a value.</value>
        [XmlIgnore]
        public bool SecuritySystemSpecified
        {
            get { return this.SecuritySystem != null && this.SecuritySystem.Count(s => s != null && s.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public SECURITY_SYSTEMS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
