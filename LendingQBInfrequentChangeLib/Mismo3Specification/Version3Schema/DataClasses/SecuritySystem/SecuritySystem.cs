namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class SECURITY_SYSTEM
    {
        /// <summary>
        /// Gets a value indicating whether the SECURITY_SYSTEM container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.SecuritySystemTypeOtherDescriptionSpecified
                    || this.SecuritySystemTypeSpecified;
            }
        }

        /// <summary>
        /// A collection of values that describe the type or level of monitoring of a security system that is installed in a home.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<SecuritySystemBase> SecuritySystemType;

        /// <summary>
        /// Gets or sets a value indicating whether the SecuritySystemType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SecuritySystemType element has been assigned a value.</value>
        [XmlIgnore]
        public bool SecuritySystemTypeSpecified
        {
            get { return this.SecuritySystemType != null && this.SecuritySystemType.enumValue != SecuritySystemBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field that specifies the enumeration when the value of Other is used for the Security System Type.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString SecuritySystemTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the SecuritySystemTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SecuritySystemTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool SecuritySystemTypeOtherDescriptionSpecified
        {
            get { return SecuritySystemTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public SECURITY_SYSTEM_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
