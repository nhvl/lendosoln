namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class AUTOMATED_UNDERWRITING_SYSTEM_RESPONSE
    {
        /// <summary>
        /// Gets a value indicating whether the AUTOMATED_UNDERWRITING_SYSTEM_RESPONSE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AutomatedUnderwritingSystemResponseMessagesSpecified
                    || this.ExtensionSpecified
                    || this.LoansSpecified;
            }
        }

        /// <summary>
        /// Response messages from the AUS.
        /// </summary>
        [XmlElement("AUTOMATED_UNDERWRITING_SYSTEM_RESPONSE_MESSAGES", Order = 0)]
        public AUTOMATED_UNDERWRITING_SYSTEM_RESPONSE_MESSAGES AutomatedUnderwritingSystemResponseMessages;

        /// <summary>
        /// Gets or sets a value indicating whether the AutomatedUnderwritingSystemResponseMessages element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AutomatedUnderwritingSystemResponseMessages element has been assigned a value.</value>
        [XmlIgnore]
        public bool AutomatedUnderwritingSystemResponseMessagesSpecified
        {
            get { return this.AutomatedUnderwritingSystemResponseMessages != null && this.AutomatedUnderwritingSystemResponseMessages.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A list of loans.
        /// </summary>
        [XmlElement("LOANS", Order = 1)]
        public LOANS Loans;

        /// <summary>
        /// Gets or sets a value indicating whether the Loans element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Loans element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoansSpecified
        {
            get { return this.Loans != null && this.Loans.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public AUTOMATED_UNDERWRITING_SYSTEM_RESPONSE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
