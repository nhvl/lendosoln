namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class FOREIGN_SYSTEM_SIGNATURE_FIELD
    {
        /// <summary>
        /// Gets a value indicating whether the FOREIGN_SYSTEM_SIGNATURE_FIELD container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ForeignSystemSignatureFieldDetailSpecified
                    || this.SignatureFieldReferenceSpecified;
            }
        }

        /// <summary>
        /// Details on a foreign system signature field.
        /// </summary>
        [XmlElement("FOREIGN_SYSTEM_SIGNATURE_FIELD_DETAIL", Order = 0)]
        public FOREIGN_SYSTEM_SIGNATURE_FIELD_DETAIL ForeignSystemSignatureFieldDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the ForeignSystemSignatureFieldDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ForeignSystemSignatureFieldDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool ForeignSystemSignatureFieldDetailSpecified
        {
            get { return this.ForeignSystemSignatureFieldDetail != null && this.ForeignSystemSignatureFieldDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Reference for a signature field.
        /// </summary>
        [XmlElement("SIGNATURE_FIELD_REFERENCE", Order = 1)]
        public FIELD_REFERENCE SignatureFieldReference;

        /// <summary>
        /// Gets or sets a value indicating whether the SignatureFieldReference element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SignatureFieldReference element has been assigned a value.</value>
        [XmlIgnore]
        public bool SignatureFieldReferenceSpecified
        {
            get { return this.SignatureFieldReference != null && this.SignatureFieldReference.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public FOREIGN_SYSTEM_SIGNATURE_FIELD_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
