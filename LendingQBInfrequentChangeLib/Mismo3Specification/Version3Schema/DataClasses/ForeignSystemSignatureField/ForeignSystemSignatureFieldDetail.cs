namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class FOREIGN_SYSTEM_SIGNATURE_FIELD_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the FOREIGN_SYSTEM_SIGNATURE_FIELD_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.EventTypeOtherDescriptionSpecified
                    || this.EventTypeSpecified
                    || this.ExtensionSpecified
                    || this.SignatureRequiredIndicatorSpecified;
            }
        }

        /// <summary>
        /// For a step in a process, contains the type of action or state.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<EventBase> EventType;

        /// <summary>
        /// Gets or sets a value indicating whether the EventType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EventType element has been assigned a value.</value>
        [XmlIgnore]
        public bool EventTypeSpecified
        {
            get { return this.EventType != null && this.EventType.enumValue != EventBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the type of action or state if Other is selected as the Event Type.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString EventTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the EventTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EventTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool EventTypeOtherDescriptionSpecified
        {
            get { return EventTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Denotes whether the foreign system signature is required or not.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOIndicator SignatureRequiredIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the SignatureRequiredIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SignatureRequiredIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool SignatureRequiredIndicatorSpecified
        {
            get { return SignatureRequiredIndicator != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public FOREIGN_SYSTEM_SIGNATURE_FIELD_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
