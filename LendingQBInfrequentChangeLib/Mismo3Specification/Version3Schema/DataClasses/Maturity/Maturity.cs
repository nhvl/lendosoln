namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class MATURITY
    {
        /// <summary>
        /// Gets a value indicating whether the MATURITY container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.MaturityOccurrencesSpecified
                    || this.MaturityRuleSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A list of maturity occurrences.
        /// </summary>
        [XmlElement("MATURITY_OCCURRENCES", Order = 0)]
        public MATURITY_OCCURRENCES MaturityOccurrences;

        /// <summary>
        /// Gets or sets a value indicating whether the MaturityOccurrences element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MaturityOccurrences element has been assigned a value.</value>
        [XmlIgnore]
        public bool MaturityOccurrencesSpecified
        {
            get { return this.MaturityOccurrences != null && this.MaturityOccurrences.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A rule about maturity.
        /// </summary>
        [XmlElement("MATURITY_RULE", Order = 1)]
        public MATURITY_RULE MaturityRule;

        /// <summary>
        /// Gets or sets a value indicating whether the MaturityRule element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MaturityRule element has been assigned a value.</value>
        [XmlIgnore]
        public bool MaturityRuleSpecified
        {
            get { return MaturityRule != null && MaturityRule.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public MATURITY_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
