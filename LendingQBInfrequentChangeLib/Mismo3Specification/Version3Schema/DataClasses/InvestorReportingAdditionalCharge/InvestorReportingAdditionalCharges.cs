namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class INVESTOR_REPORTING_ADDITIONAL_CHARGES
    {
        /// <summary>
        /// Gets a value indicating whether the INVESTOR_REPORTING_ADDITIONAL_CHARGES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.InvestorReportingAdditionalChargeSpecified
                    || this.InvestorReportingAdditionalChargeSummarySpecified;
            }
        }

        /// <summary>
        /// A collection of investor reporting additional charges.
        /// </summary>
        [XmlElement("INVESTOR_REPORTING_ADDITIONAL_CHARGE", Order = 0)]
		public List<INVESTOR_REPORTING_ADDITIONAL_CHARGE> InvestorReportingAdditionalCharge = new List<INVESTOR_REPORTING_ADDITIONAL_CHARGE>();

        /// <summary>
        /// Gets or sets a value indicating whether the InvestorReportingAdditionalCharge element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InvestorReportingAdditionalCharge element has been assigned a value.</value>
        [XmlIgnore]
        public bool InvestorReportingAdditionalChargeSpecified
        {
            get { return this.InvestorReportingAdditionalCharge != null && this.InvestorReportingAdditionalCharge.Count(i => i != null && i.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// Summary of investor reporting additional charges.
        /// </summary>
        [XmlElement("INVESTOR_REPORTING_ADDITIONAL_CHARGE_SUMMARY", Order = 1)]
        public INVESTOR_REPORTING_ADDITIONAL_CHARGE_SUMMARY InvestorReportingAdditionalChargeSummary;

        /// <summary>
        /// Gets or sets a value indicating whether the InvestorReportingAdditionalChargeSummary element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InvestorReportingAdditionalChargeSummary element has been assigned a value.</value>
        [XmlIgnore]
        public bool InvestorReportingAdditionalChargeSummarySpecified
        {
            get { return this.InvestorReportingAdditionalChargeSummary != null && this.InvestorReportingAdditionalChargeSummary.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public INVESTOR_REPORTING_ADDITIONAL_CHARGES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
