namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class INVESTOR_REPORTING_ADDITIONAL_CHARGE
    {
        /// <summary>
        /// Gets a value indicating whether the INVESTOR_REPORTING_ADDITIONAL_CHARGE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.InvestorReportingAdditionalChargeAmountSpecified
                    || this.InvestorReportingAdditionalChargeAssessedToPartyTypeOtherDescriptionSpecified
                    || this.InvestorReportingAdditionalChargeAssessedToPartyTypeSpecified
                    || this.InvestorReportingAdditionalChargeDateSpecified
                    || this.InvestorReportingAdditionalChargeReversalIndicatorSpecified
                    || this.InvestorReportingAdditionalChargeTypeOtherDescriptionSpecified
                    || this.InvestorReportingAdditionalChargeTypeSpecified;
            }
        }

        /// <summary>
        /// The monetary amount of a charge, an adjustment, a loss, a recovery or an advance associated with a loan. An additional charge is a charge, an adjustment, a loss, a recovery or an advance associated with a loan that may be part of an activity or occur separately that is required to be reported to an investor.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount InvestorReportingAdditionalChargeAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the InvestorReportingAdditionalChargeAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InvestorReportingAdditionalChargeAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool InvestorReportingAdditionalChargeAmountSpecified
        {
            get { return InvestorReportingAdditionalChargeAmount != null; }
            set { }
        }

        /// <summary>
        /// The party that has responsibility for payment of the additional charge. An additional charge is a charge, an adjustment, a loss, a recovery or an advance associated with a loan that may be part of an activity or occur separately that is required to be reported to an investor.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<InvestorReportingAdditionalChargeAssessedToPartyBase> InvestorReportingAdditionalChargeAssessedToPartyType;

        /// <summary>
        /// Gets or sets a value indicating whether the InvestorReportingAdditionalChargeAssessedToPartyType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InvestorReportingAdditionalChargeAssessedToPartyType element has been assigned a value.</value>
        [XmlIgnore]
        public bool InvestorReportingAdditionalChargeAssessedToPartyTypeSpecified
        {
            get { return this.InvestorReportingAdditionalChargeAssessedToPartyType != null && this.InvestorReportingAdditionalChargeAssessedToPartyType.enumValue != InvestorReportingAdditionalChargeAssessedToPartyBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture the Investor Reporting Additional Charge Assessed to Party Type if Other is selected.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString InvestorReportingAdditionalChargeAssessedToPartyTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the InvestorReportingAdditionalChargeAssessedToPartyTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InvestorReportingAdditionalChargeAssessedToPartyTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool InvestorReportingAdditionalChargeAssessedToPartyTypeOtherDescriptionSpecified
        {
            get { return InvestorReportingAdditionalChargeAssessedToPartyTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The date of the additional charge being reported to the Investor. An additional charge is a charge, an adjustment, a loss, a recovery or an advance associated with a loan that may be part of an activity or occur separately that is required to be reported to an investor.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMODate InvestorReportingAdditionalChargeDate;

        /// <summary>
        /// Gets or sets a value indicating whether the InvestorReportingAdditionalChargeDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InvestorReportingAdditionalChargeDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool InvestorReportingAdditionalChargeDateSpecified
        {
            get { return InvestorReportingAdditionalChargeDate != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the Additional Charge assessment is being reversed. An additional charge is a charge, an adjustment, a loss, a recovery or an advance associated with a loan that may be part of an activity or occur separately that is required to be reported to an investor.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOIndicator InvestorReportingAdditionalChargeReversalIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the InvestorReportingAdditionalChargeReversalIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InvestorReportingAdditionalChargeReversalIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool InvestorReportingAdditionalChargeReversalIndicatorSpecified
        {
            get { return InvestorReportingAdditionalChargeReversalIndicator != null; }
            set { }
        }

        /// <summary>
        /// A description of a charge, an adjustment, a loss, a recovery, or an advance associated with a loan that may be part of an activity or occur separately that is required to be reported to an investor. An additional charge is a charge, an adjustment, a loss, a recovery, or an advance associated with a loan that may be part of an activity or occur separately that is required to be reported to an investor.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOEnum<InvestorReportingAdditionalChargeBase> InvestorReportingAdditionalChargeType;

        /// <summary>
        /// Gets or sets a value indicating whether the InvestorReportingAdditionalChargeType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InvestorReportingAdditionalChargeType element has been assigned a value.</value>
        [XmlIgnore]
        public bool InvestorReportingAdditionalChargeTypeSpecified
        {
            get { return this.InvestorReportingAdditionalChargeType != null && this.InvestorReportingAdditionalChargeType.enumValue != InvestorReportingAdditionalChargeBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture the Investor Reporting Additional Charge type if Other is selected. An additional charge is a charge, an adjustment, a loss, a recovery or an advance associated with a loan that may be part of an activity or occur separately that is required to be reported to an investor.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOString InvestorReportingAdditionalChargeTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the InvestorReportingAdditionalChargeTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InvestorReportingAdditionalChargeTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool InvestorReportingAdditionalChargeTypeOtherDescriptionSpecified
        {
            get { return InvestorReportingAdditionalChargeTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 7)]
        public INVESTOR_REPORTING_ADDITIONAL_CHARGE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
