namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class REVIEW_APPRAISER
    {
        /// <summary>
        /// Gets a value indicating whether the REVIEW_APPRAISER container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ReviewAppraiserCompanyNameSpecified;
            }
        }

        /// <summary>
        /// Name of the company the Review Appraiser is associated with.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString ReviewAppraiserCompanyName;

        /// <summary>
        /// Gets or sets a value indicating whether the ReviewAppraiserCompanyName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ReviewAppraiserCompanyName element has been assigned a value.</value>
        [XmlIgnore]
        public bool ReviewAppraiserCompanyNameSpecified
        {
            get { return ReviewAppraiserCompanyName != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public REVIEW_APPRAISER_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
