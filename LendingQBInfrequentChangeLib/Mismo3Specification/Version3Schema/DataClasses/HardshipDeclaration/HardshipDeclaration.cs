namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class HARDSHIP_DECLARATION
    {
        /// <summary>
        /// Gets a value indicating whether the HARDSHIP_DECLARATION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CashReservesInsufficientIndicatorSpecified
                    || this.ExpensesIncreasedIndicatorSpecified
                    || this.ExtensionSpecified
                    || this.HardshipExplanationDescriptionSpecified
                    || this.HouseholdFinancialCircumstancesChangedIndicatorSpecified
                    || this.IncomeReducedOrLostIndicatorSpecified
                    || this.MonthlyDebtPaymentsExcessiveIndicatorSpecified
                    || this.OtherHardshipReasonsIndicatorSpecified;
            }
        }

        /// <summary>
        /// When true, indicates the borrower cannot make mortgage payments due to insufficient cash reserves.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIndicator CashReservesInsufficientIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the CashReservesInsufficientIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CashReservesInsufficientIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool CashReservesInsufficientIndicatorSpecified
        {
            get { return CashReservesInsufficientIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates the borrower cannot make mortgage payments due to an increase in expenses.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOIndicator ExpensesIncreasedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the ExpensesIncreasedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ExpensesIncreasedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExpensesIncreasedIndicatorSpecified
        {
            get { return ExpensesIncreasedIndicator != null; }
            set { }
        }

        /// <summary>
        /// Description of hardship event(s) related to the inability of the borrowers  to make their loan payment(s).
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString HardshipExplanationDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the HardshipExplanationDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HardshipExplanationDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool HardshipExplanationDescriptionSpecified
        {
            get { return HardshipExplanationDescription != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates the borrower cannot make mortgage payments due to a change in household financial circumstances.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOIndicator HouseholdFinancialCircumstancesChangedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the HouseholdFinancialCircumstancesChangedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HouseholdFinancialCircumstancesChangedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool HouseholdFinancialCircumstancesChangedIndicatorSpecified
        {
            get { return HouseholdFinancialCircumstancesChangedIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates the borrower cannot make mortgage payments due to income reduction or loss.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOIndicator IncomeReducedOrLostIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the IncomeReducedOrLostIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IncomeReducedOrLostIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool IncomeReducedOrLostIndicatorSpecified
        {
            get { return IncomeReducedOrLostIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates the borrower cannot make mortgage payments because they are excessive.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOIndicator MonthlyDebtPaymentsExcessiveIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the MonthlyDebtPaymentsExcessiveIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MonthlyDebtPaymentsExcessiveIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool MonthlyDebtPaymentsExcessiveIndicatorSpecified
        {
            get { return MonthlyDebtPaymentsExcessiveIndicator != null; }
            set { }
        }

        /// <summary>
        /// When true, indicates the borrower cannot make mortgage payments due to yet to be specified reason(s).
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOIndicator OtherHardshipReasonsIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the OtherHardshipReasonsIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OtherHardshipReasonsIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool OtherHardshipReasonsIndicatorSpecified
        {
            get { return OtherHardshipReasonsIndicator != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 7)]
        public HARDSHIP_DECLARATION_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
