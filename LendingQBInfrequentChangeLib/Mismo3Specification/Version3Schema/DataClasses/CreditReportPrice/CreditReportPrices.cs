namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CREDIT_REPORT_PRICES
    {
        /// <summary>
        /// Gets a value indicating whether the CREDIT_REPORT_PRICES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditReportPriceSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// The price of a credit report.
        /// </summary>
        [XmlElement("CREDIT_REPORT_PRICE", Order = 0)]
		public List<CREDIT_REPORT_PRICE> CreditReportPrice = new List<CREDIT_REPORT_PRICE>();

        /// <summary>
        /// Gets or sets a value indicating whether the CreditReportPrice element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditReportPrice element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditReportPriceSpecified
        {
            get { return this.CreditReportPrice != null && this.CreditReportPrice.Count(c => c != null && c.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public CREDIT_REPORT_PRICES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
