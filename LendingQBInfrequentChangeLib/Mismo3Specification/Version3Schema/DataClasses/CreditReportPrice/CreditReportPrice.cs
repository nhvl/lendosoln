namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class CREDIT_REPORT_PRICE
    {
        /// <summary>
        /// Gets a value indicating whether the CREDIT_REPORT_PRICE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditReportPriceAmountSpecified
                    || this.CreditReportPriceTypeOtherDescriptionSpecified
                    || this.CreditReportPriceTypeSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// The price of the credit report produced on the borrower(s).
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount CreditReportPriceAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditReportPriceAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditReportPriceAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditReportPriceAmountSpecified
        {
            get { return CreditReportPriceAmount != null; }
            set { }
        }

        /// <summary>
        /// This describes the type of the Credit Report Price (Net, Tax, Total or Other). If Other is selected enter the description into the Credit Report Price Type Other Description element.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<CreditReportPriceBase> CreditReportPriceType;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditReportPriceType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditReportPriceType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditReportPriceTypeSpecified
        {
            get { return this.CreditReportPriceType != null && this.CreditReportPriceType.enumValue != CreditReportPriceBase.Blank; }
            set { }
        }

        /// <summary>
        /// When Other is specified in Price Type, this element contains its description.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString CreditReportPriceTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the CreditReportPriceTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditReportPriceTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditReportPriceTypeOtherDescriptionSpecified
        {
            get { return CreditReportPriceTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public CREDIT_REPORT_PRICE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
