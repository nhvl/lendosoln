namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class INTEREST_CALCULATION_RULES
    {
        /// <summary>
        /// Gets a value indicating whether the INTEREST_CALCULATION_RULES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.InterestCalculationRuleSpecified;
            }
        }

        /// <summary>
        /// A collection of interest calculation rules.
        /// </summary>
        [XmlElement("INTEREST_CALCULATION_RULE", Order = 0)]
		public List<INTEREST_CALCULATION_RULE> InterestCalculationRule = new List<INTEREST_CALCULATION_RULE>();

        /// <summary>
        /// Gets or sets a value indicating whether the InterestCalculationRule element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InterestCalculationRule element has been assigned a value.</value>
        [XmlIgnore]
        public bool InterestCalculationRuleSpecified
        {
            get { return this.InterestCalculationRule != null && this.InterestCalculationRule.Count(i => i != null && i.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public INTEREST_CALCULATION_RULES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
