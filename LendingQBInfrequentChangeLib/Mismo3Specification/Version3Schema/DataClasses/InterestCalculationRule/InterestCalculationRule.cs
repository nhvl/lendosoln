namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class INTEREST_CALCULATION_RULE
    {
        /// <summary>
        /// Gets a value indicating whether the INTEREST_CALCULATION_RULE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.InitialUnearnedInterestAmountSpecified
                    || this.InterestAccrualTypeSpecified
                    || this.InterestCalculationBasisDaysInPeriodTypeOtherDescriptionSpecified
                    || this.InterestCalculationBasisDaysInPeriodTypeSpecified
                    || this.InterestCalculationBasisDaysInYearCountTypeSpecified
                    || this.InterestCalculationBasisTypeOtherDescriptionSpecified
                    || this.InterestCalculationBasisTypeSpecified
                    || this.InterestCalculationEffectiveDateSpecified
                    || this.InterestCalculationEffectiveMonthsCountSpecified
                    || this.InterestCalculationExpirationDateSpecified
                    || this.InterestCalculationPeriodAdjustmentIndicatorSpecified
                    || this.InterestCalculationPeriodTypeSpecified
                    || this.InterestCalculationPurposeTypeOtherDescriptionSpecified
                    || this.InterestCalculationPurposeTypeSpecified
                    || this.InterestCalculationTypeOtherDescriptionSpecified
                    || this.InterestCalculationTypeSpecified
                    || this.InterestInAdvanceIndicatorSpecified
                    || this.LoanInterestAccrualStartDateSpecified;
            }
        }

        /// <summary>
        /// The amount of all interest scheduled to be paid on a Rule of 78s loan.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount InitialUnearnedInterestAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the InitialUnearnedInterestAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InitialUnearnedInterestAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool InitialUnearnedInterestAmountSpecified
        {
            get { return InitialUnearnedInterestAmount != null; }
            set { }
        }

        /// <summary>
        /// Describes the formula used to calculate interest accrued since the previous payment.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<InterestAccrualBase> InterestAccrualType;

        /// <summary>
        /// Gets or sets a value indicating whether the InterestAccrualType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InterestAccrualType element has been assigned a value.</value>
        [XmlIgnore]
        public bool InterestAccrualTypeSpecified
        {
            get { return this.InterestAccrualType != null && this.InterestAccrualType.enumValue != InterestAccrualBase.Blank; }
            set { }
        }

        /// <summary>
        /// The length of the payment period used to calculate interest.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<InterestCalculationBasisDaysInPeriodBase> InterestCalculationBasisDaysInPeriodType;

        /// <summary>
        /// Gets or sets a value indicating whether the InterestCalculationBasisDaysInPeriodType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InterestCalculationBasisDaysInPeriodType element has been assigned a value.</value>
        [XmlIgnore]
        public bool InterestCalculationBasisDaysInPeriodTypeSpecified
        {
            get { return this.InterestCalculationBasisDaysInPeriodType != null && this.InterestCalculationBasisDaysInPeriodType.enumValue != InterestCalculationBasisDaysInPeriodBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for the Interest Calculation Basis Days In Period Type.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString InterestCalculationBasisDaysInPeriodTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the InterestCalculationBasisDaysInPeriodTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InterestCalculationBasisDaysInPeriodTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool InterestCalculationBasisDaysInPeriodTypeOtherDescriptionSpecified
        {
            get { return InterestCalculationBasisDaysInPeriodTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The number of days in a year to be used for a loans interest calculation. Commonly used for Daily Simple Interest and other loans where interest due is calculated monthly.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOEnum<InterestCalculationBasisDaysInYearCountBase> InterestCalculationBasisDaysInYearCountType;

        /// <summary>
        /// Gets or sets a value indicating whether the InterestCalculationBasisDaysInYearCountType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InterestCalculationBasisDaysInYearCountType element has been assigned a value.</value>
        [XmlIgnore]
        public bool InterestCalculationBasisDaysInYearCountTypeSpecified
        {
            get { return this.InterestCalculationBasisDaysInYearCountType != null && this.InterestCalculationBasisDaysInYearCountType.enumValue != InterestCalculationBasisDaysInYearCountBase.Blank; }
            set { }
        }

        /// <summary>
        /// Defines the loan balance upon which the interest is calculated.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOEnum<InterestCalculationBasisBase> InterestCalculationBasisType;

        /// <summary>
        /// Gets or sets a value indicating whether the InterestCalculationBasisType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InterestCalculationBasisType element has been assigned a value.</value>
        [XmlIgnore]
        public bool InterestCalculationBasisTypeSpecified
        {
            get { return this.InterestCalculationBasisType != null && this.InterestCalculationBasisType.enumValue != InterestCalculationBasisBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for the Interest Calculation Basis Type.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOString InterestCalculationBasisTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the InterestCalculationBasisTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InterestCalculationBasisTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool InterestCalculationBasisTypeOtherDescriptionSpecified
        {
            get { return InterestCalculationBasisTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The date on which the interest calculation formula goes into effect.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMODate InterestCalculationEffectiveDate;

        /// <summary>
        /// Gets or sets a value indicating whether the InterestCalculationEffectiveDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InterestCalculationEffectiveDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool InterestCalculationEffectiveDateSpecified
        {
            get { return InterestCalculationEffectiveDate != null; }
            set { }
        }

        /// <summary>
        /// The number of months that the interest determination method is in effect.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOCount InterestCalculationEffectiveMonthsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the InterestCalculationEffectiveMonthsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InterestCalculationEffectiveMonthsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool InterestCalculationEffectiveMonthsCountSpecified
        {
            get { return InterestCalculationEffectiveMonthsCount != null; }
            set { }
        }

        /// <summary>
        /// The last date on which the interest calculation formula is in effect.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMODate InterestCalculationExpirationDate;

        /// <summary>
        /// Gets or sets a value indicating whether the InterestCalculationExpirationDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InterestCalculationExpirationDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool InterestCalculationExpirationDateSpecified
        {
            get { return InterestCalculationExpirationDate != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the interest calculation period is adjusted.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOIndicator InterestCalculationPeriodAdjustmentIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the InterestCalculationPeriodAdjustmentIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InterestCalculationPeriodAdjustmentIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool InterestCalculationPeriodAdjustmentIndicatorSpecified
        {
            get { return InterestCalculationPeriodAdjustmentIndicator != null; }
            set { }
        }

        /// <summary>
        /// Describes the length of the interest accrual period.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOEnum<InterestCalculationPeriodBase> InterestCalculationPeriodType;

        /// <summary>
        /// Gets or sets a value indicating whether the InterestCalculationPeriodType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InterestCalculationPeriodType element has been assigned a value.</value>
        [XmlIgnore]
        public bool InterestCalculationPeriodTypeSpecified
        {
            get { return this.InterestCalculationPeriodType != null && this.InterestCalculationPeriodType.enumValue != InterestCalculationPeriodBase.Blank; }
            set { }
        }

        /// <summary>
        /// Specifies the purpose of the interest calculation.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOEnum<InterestCalculationPurposeBase> InterestCalculationPurposeType;

        /// <summary>
        /// Gets or sets a value indicating whether the InterestCalculationPurposeType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InterestCalculationPurposeType element has been assigned a value.</value>
        [XmlIgnore]
        public bool InterestCalculationPurposeTypeSpecified
        {
            get { return this.InterestCalculationPurposeType != null && this.InterestCalculationPurposeType.enumValue != InterestCalculationPurposeBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for the Interest Calculation Purpose Type.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOString InterestCalculationPurposeTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the InterestCalculationPurposeTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InterestCalculationPurposeTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool InterestCalculationPurposeTypeOtherDescriptionSpecified
        {
            get { return InterestCalculationPurposeTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Defines the method used to calculate the interest on the loan.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOEnum<InterestCalculationBase> InterestCalculationType;

        /// <summary>
        /// Gets or sets a value indicating whether the InterestCalculationType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InterestCalculationType element has been assigned a value.</value>
        [XmlIgnore]
        public bool InterestCalculationTypeSpecified
        {
            get { return this.InterestCalculationType != null && this.InterestCalculationType.enumValue != InterestCalculationBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for the Interest Calculation Type.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOString InterestCalculationTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the InterestCalculationTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InterestCalculationTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool InterestCalculationTypeOtherDescriptionSpecified
        {
            get { return InterestCalculationTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates that interest is accrued in advance. A value of false indicates interest in arrears.
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMOIndicator InterestInAdvanceIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the InterestInAdvanceIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InterestInAdvanceIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool InterestInAdvanceIndicatorSpecified
        {
            get { return InterestInAdvanceIndicator != null; }
            set { }
        }

        /// <summary>
        /// The date that interest begins to accrue for a loan.
        /// </summary>
        [XmlElement(Order = 17)]
        public MISMODate LoanInterestAccrualStartDate;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanInterestAccrualStartDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanInterestAccrualStartDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanInterestAccrualStartDateSpecified
        {
            get { return LoanInterestAccrualStartDate != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 18)]
        public INTEREST_CALCULATION_RULE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
