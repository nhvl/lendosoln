namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class COST_APPROACH
    {
        /// <summary>
        /// Gets a value indicating whether the container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CostApproachDetailSpecified
                    || this.DepreciationSpecified
                    || this.ExtensionSpecified
                    || this.NewImprovementsSpecified;
            }
        }

        /// <summary>
        /// Details on the cost approach.
        /// </summary>
        [XmlElement("COST_APPROACH_DETAIL", Order = 0)]
        public COST_APPROACH_DETAIL CostApproachDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the CostApproachDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CostApproachDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool CostApproachDetailSpecified
        {
            get { return CostApproachDetail != null && CostApproachDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Depreciation of the property value.
        /// </summary>
        [XmlElement("DEPRECIATION", Order = 1)]
        public DEPRECIATION Depreciation;

        /// <summary>
        /// Gets or sets a value indicating whether the Depreciation element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Depreciation element has been assigned a value.</value>
        [XmlIgnore]
        public bool DepreciationSpecified
        {
            get { return Depreciation != null && Depreciation.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// New improvements contributing to the cost.
        /// </summary>
        [XmlElement("NEW_IMPROVEMENTS", Order = 2)]
        public NEW_IMPROVEMENTS NewImprovements;

        /// <summary>
        /// Gets or sets a value indicating whether the NewImprovements element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NewImprovements element has been assigned a value.</value>
        [XmlIgnore]
        public bool NewImprovementsSpecified
        {
            get { return NewImprovements != null && NewImprovements.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 3)]
        public COST_APPROACH_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
