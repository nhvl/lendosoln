namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class COST_APPROACH_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CostAnalysisCommentDescriptionSpecified
                    || this.CostAnalysisTypeOtherDescriptionSpecified
                    || this.CostAnalysisTypeSpecified
                    || this.CostServiceQualityRatingDescriptionSpecified
                    || this.EstimatedRemainingEconomicLifeYearsCountSpecified
                    || this.ExtensionSpecified
                    || this.ManufacturedHomeInstallationCostAmountSpecified
                    || this.NewImprovementCostLocalizedAmountSpecified
                    || this.NewImprovementCostMultiplierPercentSpecified
                    || this.NewImprovementDepreciatedCostAmountSpecified
                    || this.NewImprovementTotalCostAmountSpecified
                    || this.SiteEstimatedValueAmountSpecified
                    || this.SiteEstimatedValueCommentDescriptionSpecified
                    || this.SiteOtherImprovementsAsIsAmountSpecified
                    || this.ValueIndicatedByCostApproachAmountSpecified;
            }
        }

        /// <summary>
        /// A free-form text field used to further describe the application of the Cost Approach valuation method to the subject property.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString CostAnalysisCommentDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the CostAnalysisCommentDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CostAnalysisCommentDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool CostAnalysisCommentDescriptionSpecified
        {
            get { return CostAnalysisCommentDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the type cost analysis performed. (e.g. whether it was for Replacement Costs or Reproduction Costs).
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<CostAnalysisBase> CostAnalysisType;

        /// <summary>
        /// Gets or sets a value indicating whether the CostAnalysisType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CostAnalysisType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CostAnalysisTypeSpecified
        {
            get { return CostAnalysisType != null && CostAnalysisType.enumValue != CostAnalysisBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture additional information when Other is selected for Cost Analysis Type.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString CostAnalysisTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the CostAnalysisTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CostAnalysisTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool CostAnalysisTypeOtherDescriptionSpecified
        {
            get { return CostAnalysisTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the quality rating of the itemized building cost information provided by the cost estimating service.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString CostServiceQualityRatingDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the CostServiceQualityRatingDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CostServiceQualityRatingDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool CostServiceQualityRatingDescriptionSpecified
        {
            get { return CostServiceQualityRatingDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the estimated remaining economic life of property improvements. This value is used in calculating the depreciation of the property improvements.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOCount EstimatedRemainingEconomicLifeYearsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the EstimatedRemainingEconomicLifeYearsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EstimatedRemainingEconomicLifeYearsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool EstimatedRemainingEconomicLifeYearsCountSpecified
        {
            get { return EstimatedRemainingEconomicLifeYearsCount != null; }
            set { }
        }

        /// <summary>
        /// The dollar value of the costs allocated for installation and setup for a new Manufactured Home.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOAmount ManufacturedHomeInstallationCostAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ManufacturedHomeInstallationCostAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ManufacturedHomeInstallationCostAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ManufacturedHomeInstallationCostAmountSpecified
        {
            get { return ManufacturedHomeInstallationCostAmount != null; }
            set { }
        }

        /// <summary>
        /// The dollar value of the total costs to reproduce new the improvements on the subject property after the New Improvement Cost Multiplier Factor has been applied. 
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOAmount NewImprovementCostLocalizedAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the NewImprovementCostLocalizedAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NewImprovementCostLocalizedAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool NewImprovementCostLocalizedAmountSpecified
        {
            get { return NewImprovementCostLocalizedAmount != null; }
            set { }
        }

        /// <summary>
        /// Specifies the cost multiplier used to adjust the New Improvement Building Costs to a particular local or region.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOPercent NewImprovementCostMultiplierPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the NewImprovementCostMultiplierPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NewImprovementCostMultiplierPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool NewImprovementCostMultiplierPercentSpecified
        {
            get { return NewImprovementCostMultiplierPercent != null; }
            set { }
        }

        /// <summary>
        /// Specifies the reproduction costs of the improvements after depreciation has been taken into account.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOAmount NewImprovementDepreciatedCostAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the NewImprovementDepreciatedCostAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NewImprovementDepreciatedCostAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool NewImprovementDepreciatedCostAmountSpecified
        {
            get { return NewImprovementDepreciatedCostAmount != null; }
            set { }
        }

        /// <summary>
        /// The dollar value of the total costs to reproduce new the improvements on the subject property.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOAmount NewImprovementTotalCostAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the NewImprovementTotalCostAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NewImprovementTotalCostAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool NewImprovementTotalCostAmountSpecified
        {
            get { return NewImprovementTotalCostAmount != null; }
            set { }
        }

        /// <summary>
        /// The dollar value estimated for the property site (e.g. land that is improved so that it is ready to be used for a specific purpose.).
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOAmount SiteEstimatedValueAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the SiteEstimatedValueAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SiteEstimatedValueAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool SiteEstimatedValueAmountSpecified
        {
            get { return SiteEstimatedValueAmount != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to further describe the estimated value provided in Site Estimated Value Amount.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOString SiteEstimatedValueCommentDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the SiteEstimatedValueCommentDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SiteEstimatedValueCommentDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool SiteEstimatedValueCommentDescriptionSpecified
        {
            get { return SiteEstimatedValueCommentDescription != null; }
            set { }
        }

        /// <summary>
        /// The dollar value contributed by other "as is" improvements located on the subject property site.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOAmount SiteOtherImprovementsAsIsAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the SiteOtherImprovementsAsIsAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SiteOtherImprovementsAsIsAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool SiteOtherImprovementsAsIsAmountSpecified
        {
            get { return SiteOtherImprovementsAsIsAmount != null; }
            set { }
        }

        /// <summary>
        /// The dollar value of the subject property indicated by the Cost Approach method of property valuation.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOAmount ValueIndicatedByCostApproachAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ValueIndicatedByCostApproachAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ValueIndicatedByCostApproachAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ValueIndicatedByCostApproachAmountSpecified
        {
            get { return ValueIndicatedByCostApproachAmount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 14)]
        public COST_APPROACH_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
