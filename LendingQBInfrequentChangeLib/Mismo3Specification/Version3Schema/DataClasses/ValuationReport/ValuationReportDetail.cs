namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class VALUATION_REPORT_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the VALUATION_REPORT_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AppraisalReportEffectiveDateSpecified
                    || this.AppraiserAdditionalFileIdentifierSpecified
                    || this.AppraiserFileAdditionalIdentifierDescriptionSpecified
                    || this.AppraiserFileIdentifierDescriptionSpecified
                    || this.AppraiserFileIdentifierSpecified
                    || this.AppraiserReportSignedDateSpecified
                    || this.ExtensionSpecified
                    || this.LoanOriginationSystemLoanIdentifierSpecified
                    || this.SupervisorReportSignedDateSpecified
                    || this.ValuationAddendumTextSpecified
                    || this.ValuationReportContentDescriptionSpecified
                    || this.ValuationReportContentIdentifierSpecified
                    || this.ValuationReportContentNameSpecified
                    || this.ValuationReportContentTypeOtherDescriptionSpecified
                    || this.ValuationReportContentTypeSpecified;
            }
        }

        /// <summary>
        /// Effective date of the appraisal report on the subject property.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMODate AppraisalReportEffectiveDate;

        /// <summary>
        /// Gets or sets a value indicating whether the AppraisalReportEffectiveDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AppraisalReportEffectiveDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool AppraisalReportEffectiveDateSpecified
        {
            get { return AppraisalReportEffectiveDate != null; }
            set { }
        }

        /// <summary>
        /// Secondary report identifier used by the appraiser to identify appraisal reports. This field may carry the FHA number, case number, loan number or some other identifier.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOIdentifier AppraiserAdditionalFileIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the AppraiserAdditionalFileIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AppraiserAdditionalFileIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool AppraiserAdditionalFileIdentifierSpecified
        {
            get { return AppraiserAdditionalFileIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Used to describe by name the type of identifier represented by Appraiser File Additional Identifier. (such as FHA Case Number).
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString AppraiserFileAdditionalIdentifierDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the AppraiserFileAdditionalIdentifierDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AppraiserFileAdditionalIdentifierDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool AppraiserFileAdditionalIdentifierDescriptionSpecified
        {
            get { return AppraiserFileAdditionalIdentifierDescription != null; }
            set { }
        }

        /// <summary>
        /// This is an identifier or number used by the appraiser to identify their reports. It is generally specific to the appraiser.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOIdentifier AppraiserFileIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the AppraiserFileIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AppraiserFileIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool AppraiserFileIdentifierSpecified
        {
            get { return AppraiserFileIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Used to describe by name the type of identifier represented by Appraiser File Identifier.  (such as Client Order Number).
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString AppraiserFileIdentifierDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the AppraiserFileIdentifierDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AppraiserFileIdentifierDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool AppraiserFileIdentifierDescriptionSpecified
        {
            get { return AppraiserFileIdentifierDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates the date the appraiser signed the appraisal report.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMODate AppraiserReportSignedDate;

        /// <summary>
        /// Gets or sets a value indicating whether the AppraiserReportSignedDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AppraiserReportSignedDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool AppraiserReportSignedDateSpecified
        {
            get { return AppraiserReportSignedDate != null; }
            set { }
        }

        /// <summary>
        /// A control number (i.e. key) for the loan in the sending loan origination system.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOIdentifier LoanOriginationSystemLoanIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanOriginationSystemLoanIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanOriginationSystemLoanIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanOriginationSystemLoanIdentifierSpecified
        {
            get { return LoanOriginationSystemLoanIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Indicates the date the supervisor signed the report.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMODate SupervisorReportSignedDate;

        /// <summary>
        /// Gets or sets a value indicating whether the SupervisorReportSignedDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SupervisorReportSignedDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool SupervisorReportSignedDateSpecified
        {
            get { return SupervisorReportSignedDate != null; }
            set { }
        }

        /// <summary>
        /// Used to hold the additional comments found on a Comment Addendum form within the valuation. 
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOString ValuationAddendumText;

        /// <summary>
        /// Gets or sets a value indicating whether the ValuationAddendumText element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ValuationAddendumText element has been assigned a value.</value>
        [XmlIgnore]
        public bool ValuationAddendumTextSpecified
        {
            get { return ValuationAddendumText != null; }
            set { }
        }

        /// <summary>
        /// Used to describe the purpose, function or contents of a valuation report or addendum. (e.g., Subject Interior Photos).
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOString ValuationReportContentDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ValuationReportContentDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ValuationReportContentDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ValuationReportContentDescriptionSpecified
        {
            get { return ValuationReportContentDescription != null; }
            set { }
        }

        /// <summary>
        /// An additional identifier that is used for uniquely identifying valuation report or addendums. This may be an internal systems identifier.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOIdentifier ValuationReportContentIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the ValuationReportContentIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ValuationReportContentIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool ValuationReportContentIdentifierSpecified
        {
            get { return ValuationReportContentIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Indicates the name or title of an valuation report form or addendum. (e.g., Extra Comparable 4-5-6).
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOString ValuationReportContentName;

        /// <summary>
        /// Gets or sets a value indicating whether the ValuationReportContentName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ValuationReportContentName element has been assigned a value.</value>
        [XmlIgnore]
        public bool ValuationReportContentNameSpecified
        {
            get { return ValuationReportContentName != null; }
            set { }
        }

        /// <summary>
        /// Used to specify the types of forms or addendum pages that make up a complete valuation report.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOEnum<ValuationReportContentBase> ValuationReportContentType;

        /// <summary>
        /// Gets or sets a value indicating whether the ValuationReportContentType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ValuationReportContentType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ValuationReportContentTypeSpecified
        {
            get { return this.ValuationReportContentType != null && this.ValuationReportContentType.enumValue != ValuationReportContentBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture additional information when Other is selected for Valuation Report Content Type.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMOString ValuationReportContentTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ValuationReportContentTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ValuationReportContentTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ValuationReportContentTypeOtherDescriptionSpecified
        {
            get { return ValuationReportContentTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 14)]
        public VALUATION_REPORT_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
