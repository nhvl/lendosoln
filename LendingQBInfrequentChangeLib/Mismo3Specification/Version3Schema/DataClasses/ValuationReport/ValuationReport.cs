namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class VALUATION_REPORT
    {
        /// <summary>
        /// Gets a value indicating whether the VALUATION_REPORT container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ApproachToValueSpecified
                    || this.ExtensionSpecified
                    || this.ScopeOfWorkSpecified
                    || this.ValuationFormsSpecified
                    || this.ValuationReconciliationSpecified
                    || this.ValuationReportDetailSpecified
                    || this.ValuationReviewSpecified
                    || this.ValuationUpdateSpecified;
            }
        }

        /// <summary>
        /// The approach to value for the valuation.
        /// </summary>
        [XmlElement("APPROACH_TO_VALUE", Order = 0)]
        public APPROACH_TO_VALUE ApproachToValue;

        /// <summary>
        /// Gets or sets a value indicating whether the ApproachToValue element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ApproachToValue element has been assigned a value.</value>
        [XmlIgnore]
        public bool ApproachToValueSpecified
        {
            get { return this.ApproachToValue != null && this.ApproachToValue.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The scope of work on the valuation.
        /// </summary>
        [XmlElement("SCOPE_OF_WORK", Order = 1)]
        public SCOPE_OF_WORK ScopeOfWork;

        /// <summary>
        /// Gets or sets a value indicating whether the ScopeOfWork element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ScopeOfWork element has been assigned a value.</value>
        [XmlIgnore]
        public bool ScopeOfWorkSpecified
        {
            get { return this.ScopeOfWork != null && this.ScopeOfWork.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The forms associated with the valuation.
        /// </summary>
        [XmlElement("VALUATION_FORMS", Order = 2)]
        public VALUATION_FORMS ValuationForms;

        /// <summary>
        /// Gets or sets a value indicating whether the ValuationForms element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ValuationForms element has been assigned a value.</value>
        [XmlIgnore]
        public bool ValuationFormsSpecified
        {
            get { return this.ValuationForms != null && this.ValuationForms.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Reconciliation of the valuation.
        /// </summary>
        [XmlElement("VALUATION_RECONCILIATION", Order = 3)]
        public VALUATION_RECONCILIATION ValuationReconciliation;

        /// <summary>
        /// Gets or sets a value indicating whether the ValuationReconciliation element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ValuationReconciliation element has been assigned a value.</value>
        [XmlIgnore]
        public bool ValuationReconciliationSpecified
        {
            get { return this.ValuationReconciliation != null && this.ValuationReconciliation.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details on the valuation report.
        /// </summary>
        [XmlElement("VALUATION_REPORT_DETAIL", Order = 4)]
        public VALUATION_REPORT_DETAIL ValuationReportDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the ValuationReportDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ValuationReportDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool ValuationReportDetailSpecified
        {
            get { return this.ValuationReportDetail != null && this.ValuationReportDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Review of the valuation.
        /// </summary>
        [XmlElement("VALUATION_REVIEW", Order = 5)]
        public VALUATION_REVIEW ValuationReview;

        /// <summary>
        /// Gets or sets a value indicating whether the ValuationReview element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ValuationReview element has been assigned a value.</value>
        [XmlIgnore]
        public bool ValuationReviewSpecified
        {
            get { return this.ValuationReview != null && this.ValuationReview.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Updates on the valuation.
        /// </summary>
        [XmlElement("VALUATION_UPDATE", Order = 6)]
        public VALUATION_UPDATE ValuationUpdate;

        /// <summary>
        /// Gets or sets a value indicating whether the ValuationUpdate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ValuationUpdate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ValuationUpdateSpecified
        {
            get { return this.ValuationUpdate != null && this.ValuationUpdate.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 7)]
        public VALUATION_REPORT_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
