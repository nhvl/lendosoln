namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class IDENTIFICATION_VERIFICATION
    {
        /// <summary>
        /// Gets a value indicating whether the IDENTIFICATION_VERIFICATION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BirthInformationSpecified
                    || this.ExtensionSpecified
                    || this.IdentificationVerificationDetailSpecified
                    || this.IdentityDocumentationsSpecified
                    || this.SharedSecretsSpecified
                    || this.SpouseInformationSpecified;
            }
        }

        /// <summary>
        /// Information about birth.
        /// </summary>
        [XmlElement("BIRTH_INFORMATION", Order = 0)]
        public BIRTH_INFORMATION BirthInformation;

        /// <summary>
        /// Gets or sets a value indicating whether the BirthInformation element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BirthInformation element has been assigned a value.</value>
        [XmlIgnore]
        public bool BirthInformationSpecified
        {
            get { return this.BirthInformation != null && this.BirthInformation.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Information about identification verification.
        /// </summary>
        [XmlElement("IDENTIFICATION_VERIFICATION_DETAIL", Order = 1)]
        public IDENTIFICATION_VERIFICATION_DETAIL IdentificationVerificationDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the IdentificationVerificationDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IdentificationVerificationDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool IdentificationVerificationDetailSpecified
        {
            get { return this.IdentificationVerificationDetail != null && this.IdentificationVerificationDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// List of identity documentation.
        /// </summary>
        [XmlElement("IDENTITY_DOCUMENTATIONS", Order = 2)]
        public IDENTITY_DOCUMENTATIONS IdentityDocumentations;

        /// <summary>
        /// Gets or sets a value indicating whether the IdentityDocumentations element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IdentityDocumentations element has been assigned a value.</value>
        [XmlIgnore]
        public bool IdentityDocumentationsSpecified
        {
            get { return this.IdentityDocumentations != null && this.IdentityDocumentations.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Secrets shared to ascertain identity.
        /// </summary>
        [XmlElement("SHARED_SECRETS", Order = 3)]
        public SHARED_SECRETS SharedSecrets;

        /// <summary>
        /// Gets or sets a value indicating whether the SharedSecrets element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SharedSecrets element has been assigned a value.</value>
        [XmlIgnore]
        public bool SharedSecretsSpecified
        {
            get { return this.SharedSecrets != null && this.SharedSecrets.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Information about a spouse.
        /// </summary>
        [XmlElement("SPOUSE_INFORMATION", Order = 4)]
        public SPOUSE_INFORMATION SpouseInformation;

        /// <summary>
        /// Gets or sets a value indicating whether the SpouseInformation element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SpouseInformation element has been assigned a value.</value>
        [XmlIgnore]
        public bool SpouseInformationSpecified
        {
            get { return this.SpouseInformation != null && this.SpouseInformation.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 5)]
        public IDENTIFICATION_VERIFICATION_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
