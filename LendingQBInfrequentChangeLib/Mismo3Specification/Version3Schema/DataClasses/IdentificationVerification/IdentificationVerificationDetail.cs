namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class IDENTIFICATION_VERIFICATION_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the IDENTIFICATION_VERIFICATION_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CountryOfCitizenshipNameSpecified
                    || this.ExtensionSpecified
                    || this.SSNAssociationsCountSpecified;
            }
        }

        /// <summary>
        /// The name of the country of citizenship of the individual.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString CountryOfCitizenshipName;

        /// <summary>
        /// Gets or sets a value indicating whether the CountryOfCitizenshipName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CountryOfCitizenshipName element has been assigned a value.</value>
        [XmlIgnore]
        public bool CountryOfCitizenshipNameSpecified
        {
            get { return CountryOfCitizenshipName != null; }
            set { }
        }

        /// <summary>
        /// The count of the associations between name and SSN for this individual.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOCount SSNAssociationsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the SSNAssociationsCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SSNAssociationsCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool SSNAssociationsCountSpecified
        {
            get { return SSNAssociationsCount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public IDENTIFICATION_VERIFICATION_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
