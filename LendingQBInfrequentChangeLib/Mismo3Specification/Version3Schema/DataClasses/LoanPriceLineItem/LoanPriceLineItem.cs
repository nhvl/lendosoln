namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class LOAN_PRICE_LINE_ITEM
    {
        /// <summary>
        /// Gets a value indicating whether the LOAN_PRICE_LINE_ITEM container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.LoanPriceLineItemAmountSpecified
                    || this.LoanPriceLineItemCategoryDescriptionSpecified
                    || this.LoanPriceLineItemPercentSpecified
                    || this.LoanPriceLineItemTypeOtherDescriptionSpecified
                    || this.LoanPriceLineItemTypeSpecified;
            }
        }

        /// <summary>
        /// A line item component of a price quote, expressed as a dollar amount.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount LoanPriceLineItemAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanPriceLineItemAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanPriceLineItemAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanPriceLineItemAmountSpecified
        {
            get { return LoanPriceLineItemAmount != null; }
            set { }
        }

        /// <summary>
        /// Defines the category of the Price Line Item. Categories such as Fee, Adjustment, or Charge may reflect tax or accounting implications for the line item.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString LoanPriceLineItemCategoryDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanPriceLineItemCategoryDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanPriceLineItemCategoryDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanPriceLineItemCategoryDescriptionSpecified
        {
            get { return LoanPriceLineItemCategoryDescription != null; }
            set { }
        }

        /// <summary>
        /// A line item component of a price quote, expressed as a percent of the loan amount.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOPercent LoanPriceLineItemPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanPriceLineItemPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanPriceLineItemPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanPriceLineItemPercentSpecified
        {
            get { return LoanPriceLineItemPercent != null; }
            set { }
        }

        /// <summary>
        /// Denotes a type of fee, price adjustment, or other charge that is provided as detail information with a price quote.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOEnum<LoanPriceLineItemBase> LoanPriceLineItemType;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanPriceLineItemType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanPriceLineItemType element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanPriceLineItemTypeSpecified
        {
            get { return this.LoanPriceLineItemType != null && this.LoanPriceLineItemType.enumValue != LoanPriceLineItemBase.Blank; }
            set { }
        }

        /// <summary>
        /// A text description of the type of pricing line item, used when the value of Other is chosen for the attribute Price Line Item Type.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString LoanPriceLineItemTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanPriceLineItemTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanPriceLineItemTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanPriceLineItemTypeOtherDescriptionSpecified
        {
            get { return LoanPriceLineItemTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 5)]
        public LOAN_PRICE_LINE_ITEM_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
