namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class LOAN_PRICE_LINE_ITEMS
    {
        /// <summary>
        /// Gets a value indicating whether the LOAN_PRICE_LINE_ITEMS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.LoanPriceLineItemSpecified;
            }
        }

        /// <summary>
        /// A collection of loan price line items.
        /// </summary>
        [XmlElement("LOAN_PRICE_LINE_ITEM", Order = 0)]
        public List<LOAN_PRICE_LINE_ITEM> LoanPriceLineItem = new List<LOAN_PRICE_LINE_ITEM>();

        /// <summary>
        /// Gets or sets a value indicating whether the LoanPriceLineItem element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanPriceLineItem element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanPriceLineItemSpecified
        {
            get { return this.LoanPriceLineItem != null && this.LoanPriceLineItem.Count(l => l != null && l.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public LOAN_PRICE_LINE_ITEMS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
