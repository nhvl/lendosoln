namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class ORIGINATION_FUND
    {
        /// <summary>
        /// Gets a value indicating whether the ORIGINATION_FUND container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.FundsSourceTypeOtherDescriptionSpecified
                    || this.FundsSourceTypeSpecified
                    || this.FundsTypeOtherDescriptionSpecified
                    || this.FundsTypeSpecified
                    || this.OriginationFundsAmountSpecified;
            }
        }

        /// <summary>
        /// The party providing the associated funds.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<FundsSourceBase> FundsSourceType;

        /// <summary>
        /// Gets or sets a value indicating whether the FundsSourceType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FundsSourceType element has been assigned a value.</value>
        [XmlIgnore]
        public bool FundsSourceTypeSpecified
        {
            get { return this.FundsSourceType != null && this.FundsSourceType.enumValue != FundsSourceBase.Blank; }
            set { }
        }

        /// <summary>
        /// Used to collect additional information when Other is selected for Funds Source Type.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString FundsSourceTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the FundsSourceTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FundsSourceTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool FundsSourceTypeOtherDescriptionSpecified
        {
            get { return FundsSourceTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A monetary source commonly used to pay obligations in a mortgage loan transaction.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<FundsBase> FundsType;

        /// <summary>
        /// Gets or sets a value indicating whether the FundsType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FundsType element has been assigned a value.</value>
        [XmlIgnore]
        public bool FundsTypeSpecified
        {
            get { return this.FundsType != null && this.FundsType.enumValue != FundsBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Funds Type.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString FundsTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the FundsTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FundsTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool FundsTypeOtherDescriptionSpecified
        {
            get { return FundsTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The dollar amount of funds supplied by a specified source at loan origination.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOAmount OriginationFundsAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the OriginationFundsAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OriginationFundsAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool OriginationFundsAmountSpecified
        {
            get { return OriginationFundsAmount != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 5)]
        public ORIGINATION_FUND_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
