namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class ORIGINATION_FUNDS
    {
        /// <summary>
        /// Gets a value indicating whether the ORIGINATION_FUNDS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.OriginationFundSpecified;
            }
        }

        /// <summary>
        /// A collection of origination funds.
        /// </summary>
        [XmlElement("ORIGINATION_FUND", Order = 0)]
		public List<ORIGINATION_FUND> OriginationFund = new List<ORIGINATION_FUND>();

        /// <summary>
        /// Gets or sets a value indicating whether the OriginationFund element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OriginationFund element has been assigned a value.</value>
        [XmlIgnore]
        public bool OriginationFundSpecified
        {
            get { return this.OriginationFund != null && this.OriginationFund.Count(o => o != null && o.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public ORIGINATION_FUNDS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
