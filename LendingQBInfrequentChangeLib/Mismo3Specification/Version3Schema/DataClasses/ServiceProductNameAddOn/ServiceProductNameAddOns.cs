namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class SERVICE_PRODUCT_NAME_ADD_ONS
    {
        /// <summary>
        /// Gets a value indicating whether the SERVICE_PRODUCT_NAME_ADD_ONS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ServiceProductNameAddOnSpecified;
            }
        }

        /// <summary>
        /// A collection of service product name add on.
        /// </summary>
        [XmlElement("SERVICE_PRODUCT_NAME_ADD_ON", Order = 0)]
		public List<SERVICE_PRODUCT_NAME_ADD_ON> ServiceProductNameAddOn = new List<SERVICE_PRODUCT_NAME_ADD_ON>();

        /// <summary>
        /// Gets or sets a value indicating whether the ServiceProductNameAddOn element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServiceProductNameAddOn element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServiceProductNameAddOnSpecified
        {
            get { return this.ServiceProductNameAddOn != null && this.ServiceProductNameAddOn.Count(s => s != null && s.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public SERVICE_PRODUCT_NAME_ADD_ONS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
