namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class SERVICE_PRODUCT_NAME_ADD_ON
    {
        /// <summary>
        /// Gets a value indicating whether the SERVICE_PRODUCT_NAME_ADD_ON container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ServiceProductNameAddOnDescriptionSpecified
                    || this.ServiceProductNameAddOnIdentifierSpecified;
            }
        }

        /// <summary>
        /// A brief description of the the service or product option being added to a primary service or product.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString ServiceProductNameAddOnDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ServiceProductNameAddOnDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServiceProductNameAddOnDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServiceProductNameAddOnDescriptionSpecified
        {
            get { return ServiceProductNameAddOnDescription != null; }
            set { }
        }

        /// <summary>
        /// Some unique identifier that correlates to a specific sub-part of the product; usually a number.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOIdentifier ServiceProductNameAddOnIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the ServiceProductNameAddOnIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ServiceProductNameAddOnIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool ServiceProductNameAddOnIdentifierSpecified
        {
            get { return ServiceProductNameAddOnIdentifier != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public SERVICE_PRODUCT_NAME_ADD_ON_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
