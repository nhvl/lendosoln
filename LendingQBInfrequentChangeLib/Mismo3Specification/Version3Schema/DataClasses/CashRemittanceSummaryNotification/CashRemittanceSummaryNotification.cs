namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CASH_REMITTANCE_SUMMARY_NOTIFICATION
    {
        /// <summary>
        /// Gets a value indicating whether the CASH_REMITTANCE_SUMMARY_NOTIFICATION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.InvestorRemittanceAmountSpecified
                    || this.InvestorRemittanceEffectiveDateSpecified
                    || this.InvestorRemittanceTypeOtherDescriptionSpecified
                    || this.InvestorRemittanceTypeSpecified;
            }
        }

        /// <summary>
        /// The total remittance amount being reported to the Investor by the Servicer.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount InvestorRemittanceAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the InvestorRemittanceAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InvestorRemittanceAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool InvestorRemittanceAmountSpecified
        {
            get { return InvestorRemittanceAmount != null; }
            set { }
        }

        /// <summary>
        /// The date the investor should draft the remittance from the lenders bank account. This is typically the day after the remittance is reported to the investor.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMODate InvestorRemittanceEffectiveDate;

        /// <summary>
        /// Gets or sets a value indicating whether the InvestorRemittanceEffectiveDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InvestorRemittanceEffectiveDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool InvestorRemittanceEffectiveDateSpecified
        {
            get { return InvestorRemittanceEffectiveDate != null; }
            set { }
        }

        /// <summary>
        /// This describes the contractual accounting method used to calculate the funds received by the Servicer from the borrower that are due to the investor.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<InvestorRemittanceBase> InvestorRemittanceType;

        /// <summary>
        /// Gets or sets a value indicating whether the InvestorRemittanceType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InvestorRemittanceType element has been assigned a value.</value>
        [XmlIgnore]
        public bool InvestorRemittanceTypeSpecified
        {
            get { return this.InvestorRemittanceType != null && this.InvestorRemittanceType.enumValue != InvestorRemittanceBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture the Investor Remittance Type if Other is selected.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString InvestorRemittanceTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the InvestorRemittanceTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InvestorRemittanceTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool InvestorRemittanceTypeOtherDescriptionSpecified
        {
            get { return InvestorRemittanceTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public CASH_REMITTANCE_SUMMARY_NOTIFICATION_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
