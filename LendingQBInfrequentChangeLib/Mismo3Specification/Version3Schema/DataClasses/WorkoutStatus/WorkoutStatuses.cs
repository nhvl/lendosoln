namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class WORKOUT_STATUSES
    {
        /// <summary>
        /// Gets a value indicating whether the WORKOUT_STATUSES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.WorkoutStatusSpecified;
            }
        }

        /// <summary>
        /// A collection of workout statuses.
        /// </summary>
        [XmlElement("WORKOUT_STATUS", Order = 0)]
		public List<WORKOUT_STATUS> WorkoutStatus = new List<WORKOUT_STATUS>();

        /// <summary>
        /// Gets or sets a value indicating whether the WorkoutStatus element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the WorkoutStatus element has been assigned a value.</value>
        [XmlIgnore]
        public bool WorkoutStatusSpecified
        {
            get { return this.WorkoutStatus != null && this.WorkoutStatus.Count(w => w != null && w.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public WORKOUT_STATUSES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
