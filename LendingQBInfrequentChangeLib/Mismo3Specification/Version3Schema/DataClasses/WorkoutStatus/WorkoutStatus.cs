namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class WORKOUT_STATUS
    {
        /// <summary>
        /// Gets a value indicating whether the WORKOUT_STATUS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.WorkoutProcessStatusTypeSpecified
                    || this.WorkoutStatusEndDateSpecified
                    || this.WorkoutStatusOccurredDateSpecified
                    || this.WorkoutStatusTypeOtherDescriptionSpecified
                    || this.WorkoutStatusTypeSpecified;
            }
        }

        /// <summary>
        /// Categorizes the process status of the workout as defined by investor policy guidelines.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<WorkoutProcessStatusBase> WorkoutProcessStatusType;

        /// <summary>
        /// Gets or sets a value indicating whether the WorkoutProcessStatusType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the WorkoutProcessStatusType element has been assigned a value.</value>
        [XmlIgnore]
        public bool WorkoutProcessStatusTypeSpecified
        {
            get { return this.WorkoutProcessStatusType != null && this.WorkoutProcessStatusType.enumValue != WorkoutProcessStatusBase.Blank; }
            set { }
        }

        /// <summary>
        /// The date on which the associated Workout Status Type ended.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMODate WorkoutStatusEndDate;

        /// <summary>
        /// Gets or sets a value indicating whether the WorkoutStatusEndDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the WorkoutStatusEndDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool WorkoutStatusEndDateSpecified
        {
            get { return WorkoutStatusEndDate != null; }
            set { }
        }

        /// <summary>
        /// The date on which the associated Workout Status Type occurred. 
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMODate WorkoutStatusOccurredDate;

        /// <summary>
        /// Gets or sets a value indicating whether the WorkoutStatusOccurredDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the WorkoutStatusOccurredDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool WorkoutStatusOccurredDateSpecified
        {
            get { return WorkoutStatusOccurredDate != null; }
            set { }
        }

        /// <summary>
        /// The status of the corresponding Workout Type.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOEnum<WorkoutStatusBase> WorkoutStatusType;

        /// <summary>
        /// Gets or sets a value indicating whether the WorkoutStatusType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the WorkoutStatusType element has been assigned a value.</value>
        [XmlIgnore]
        public bool WorkoutStatusTypeSpecified
        {
            get { return this.WorkoutStatusType != null && this.WorkoutStatusType.enumValue != WorkoutStatusBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Workout Status Type.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString WorkoutStatusTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the WorkoutStatusTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the WorkoutStatusTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool WorkoutStatusTypeOtherDescriptionSpecified
        {
            get { return WorkoutStatusTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 5)]
        public WORKOUT_STATUS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
