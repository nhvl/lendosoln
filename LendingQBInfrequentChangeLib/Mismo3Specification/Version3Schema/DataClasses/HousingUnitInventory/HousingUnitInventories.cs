namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class HOUSING_UNIT_INVENTORIES
    {
        /// <summary>
        /// Gets a value indicating whether the HOUSING_UNIT_INVENTORIES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.HousingUnitInventorySpecified;
            }
        }

        /// <summary>
        /// A collection of housing unit inventories.
        /// </summary>
        [XmlElement("HOUSING_UNIT_INVENTORY", Order = 0)]
		public List<HOUSING_UNIT_INVENTORY> HousingUnitInventory = new List<HOUSING_UNIT_INVENTORY>();

        /// <summary>
        /// Gets or sets a value indicating whether the HousingUnitInventory element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HousingUnitInventory element has been assigned a value.</value>
        [XmlIgnore]
        public bool HousingUnitInventorySpecified
        {
            get { return this.HousingUnitInventory != null && this.HousingUnitInventory.Count(h => h != null && h.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public HOUSING_UNIT_INVENTORIES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
