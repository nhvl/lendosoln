namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class HOUSING_UNIT_INVENTORY
    {
        /// <summary>
        /// Gets a value indicating whether the HOUSING_UNIT_INVENTORY container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ConstructionStatusTypeOtherDescriptionSpecified
                    || this.ConstructionStatusTypeSpecified
                    || this.DevelopmentStageDescriptionSpecified
                    || this.DevelopmentStageTotalPhasesCountSpecified
                    || this.ExtensionSpecified
                    || this.LivingUnitCountSpecified
                    || this.LivingUnitPercentSpecified
                    || this.ProjectConstructionStatusDescriptionSpecified
                    || this.ProjectTypeSpecified
                    || this.ProjectUnitDensityPerAcreNumberSpecified
                    || this.UnitOccupancyTypeSpecified
                    || this.UnitOwnedByTypeSpecified
                    || this.UnitRentRateTypeSpecified
                    || this.UnitSaleRentalStatusTypeSpecified;
            }
        }

        /// <summary>
        /// Specifies the physical status of the structure.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<ConstructionStatusBase> ConstructionStatusType;

        /// <summary>
        /// Gets or sets a value indicating whether the ConstructionStatusType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConstructionStatusType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConstructionStatusTypeSpecified
        {
            get { return this.ConstructionStatusType != null && this.ConstructionStatusType.enumValue != ConstructionStatusBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Construction Status Type.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString ConstructionStatusTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ConstructionStatusTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConstructionStatusTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConstructionStatusTypeOtherDescriptionSpecified
        {
            get { return ConstructionStatusTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the phase or completion or incomplete status of the project.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString DevelopmentStageDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the DevelopmentStageDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DevelopmentStageDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool DevelopmentStageDescriptionSpecified
        {
            get { return DevelopmentStageDescription != null; }
            set { }
        }

        /// <summary>
        /// Number of phases that exist in the project.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOCount DevelopmentStageTotalPhasesCount;

        /// <summary>
        /// Gets or sets a value indicating whether the DevelopmentStageTotalPhasesCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DevelopmentStageTotalPhasesCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool DevelopmentStageTotalPhasesCountSpecified
        {
            get { return DevelopmentStageTotalPhasesCount != null; }
            set { }
        }

        /// <summary>
        /// Number of separate living units (i.e. in a structure such as an apartment or duplex).
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOCount LivingUnitCount;

        /// <summary>
        /// Gets or sets a value indicating whether the LivingUnitCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LivingUnitCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool LivingUnitCountSpecified
        {
            get { return LivingUnitCount != null; }
            set { }
        }

        /// <summary>
        /// The percentage of the living units defined in the manner specified in Housing Unit Inventory to the total number of living units.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOPercent LivingUnitPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the LivingUnitPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LivingUnitPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool LivingUnitPercentSpecified
        {
            get { return LivingUnitPercent != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the current status (e.g. Existing, Completed, Planned) of the project.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOString ProjectConstructionStatusDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectConstructionStatusDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectConstructionStatusDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectConstructionStatusDescriptionSpecified
        {
            get { return ProjectConstructionStatusDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies whether the referenced project is associated with the subject or other property. 
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOEnum<ProjectBase> ProjectType;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectTypeSpecified
        {
            get { return this.ProjectType != null && this.ProjectType.enumValue != ProjectBase.Blank; }
            set { }
        }

        /// <summary>
        /// The number of living units per acre.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMONumeric ProjectUnitDensityPerAcreNumber;

        /// <summary>
        /// Gets or sets a value indicating whether the ProjectUnitDensityPerAcreNumber element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ProjectUnitDensityPerAcreNumber element has been assigned a value.</value>
        [XmlIgnore]
        public bool ProjectUnitDensityPerAcreNumberSpecified
        {
            get { return ProjectUnitDensityPerAcreNumber != null; }
            set { }
        }

        /// <summary>
        /// Specifies the current occupancy status of the specified unit within a property or an individual unit or a group of units within project.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOEnum<UnitOccupancyBase> UnitOccupancyType;

        /// <summary>
        /// Gets or sets a value indicating whether the UnitOccupancyType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the UnitOccupancyType element has been assigned a value.</value>
        [XmlIgnore]
        public bool UnitOccupancyTypeSpecified
        {
            get { return this.UnitOccupancyType != null && this.UnitOccupancyType.enumValue != UnitOccupancyBase.Blank; }
            set { }
        }

        /// <summary>
        /// Specifies the type of individual or entity that owns the unit. 
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOEnum<UnitOwnedByBase> UnitOwnedByType;

        /// <summary>
        /// Gets or sets a value indicating whether the UnitOwnedByType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the UnitOwnedByType element has been assigned a value.</value>
        [XmlIgnore]
        public bool UnitOwnedByTypeSpecified
        {
            get { return this.UnitOwnedByType != null && this.UnitOwnedByType.enumValue != UnitOwnedByBase.Blank; }
            set { }
        }

        /// <summary>
        /// Specifies whether or not the rent rate for the unit is set by market conditions or is regulated.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOEnum<UnitRentRateBase> UnitRentRateType;

        /// <summary>
        /// Gets or sets a value indicating whether the UnitRentRateType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the UnitRentRateType element has been assigned a value.</value>
        [XmlIgnore]
        public bool UnitRentRateTypeSpecified
        {
            get { return this.UnitRentRateType != null && this.UnitRentRateType.enumValue != UnitRentRateBase.Blank; }
            set { }
        }

        /// <summary>
        /// Specifies the status of the unit with respect to its sale or rental.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOEnum<UnitSaleRentalStatusBase> UnitSaleRentalStatusType;

        /// <summary>
        /// Gets or sets a value indicating whether the UnitSaleRentalStatusType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the UnitSaleRentalStatusType element has been assigned a value.</value>
        [XmlIgnore]
        public bool UnitSaleRentalStatusTypeSpecified
        {
            get { return this.UnitSaleRentalStatusType != null && this.UnitSaleRentalStatusType.enumValue != UnitSaleRentalStatusBase.Blank; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 13)]
        public HOUSING_UNIT_INVENTORY_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
