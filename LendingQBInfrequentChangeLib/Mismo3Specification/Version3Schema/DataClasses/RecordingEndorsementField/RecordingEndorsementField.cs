namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class RECORDING_ENDORSEMENT_FIELD
    {
        /// <summary>
        /// Gets a value indicating whether the RECORDING_ENDORSEMENT_FIELD container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.RecordingEndorsementFieldReferenceSpecified;
            }
        }

        /// <summary>
        /// A field reference for the recording endorsement.
        /// </summary>
        [XmlElement("RECORDING_ENDORSEMENT_FIELD_REFERENCE", Order = 0)]
        public FIELD_REFERENCE RecordingEndorsementFieldReference;

        /// <summary>
        /// Gets or sets a value indicating whether the RecordingEndorsementFieldReference element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RecordingEndorsementFieldReference element has been assigned a value.</value>
        [XmlIgnore]
        public bool RecordingEndorsementFieldReferenceSpecified
        {
            get { return this.RecordingEndorsementFieldReference != null && this.RecordingEndorsementFieldReference.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public RECORDING_ENDORSEMENT_FIELD_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
