﻿namespace Mismo3Specification.Version3Schema
{
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// A generic class for allowing custom proprietary extensions.
    /// </summary>
    public partial class OTHER_BASE
    {
        /// <summary>
        /// Gets a value indicating whether the element should be serialized.
        /// </summary>
        /// <remarks>Always returns false as this placeholder element should never be exported.</remarks>
        [XmlIgnore]
        public virtual bool ShouldSerialize
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets or sets the child elements of this extension.
        /// </summary>
        [XmlAnyElement]
        public XmlElement[] Elements { get; set; }
    }
}
