namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class TERMS_OF_LOAN
    {
        /// <summary>
        /// Gets a value indicating whether the TERMS_OF_LOAN container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AssumedLoanAmountSpecified
                    || this.BaseLoanAmountSpecified
                    || this.DisclosedFullyIndexedRatePercentSpecified
                    || this.DisclosedIndexRatePercentSpecified
                    || this.DisclosedMarginRatePercentSpecified
                    || this.ExtensionSpecified
                    || this.LienPriorityTypeOtherDescriptionSpecified
                    || this.LienPriorityTypeSpecified
                    || this.LoanPurposeTypeOtherDescriptionSpecified
                    || this.LoanPurposeTypeSpecified
                    || this.MortgageTypeOtherDescriptionSpecified
                    || this.MortgageTypeSpecified
                    || this.NoteAmountSpecified
                    || this.NoteCityNameSpecified
                    || this.NoteDateSpecified
                    || this.NoteRatePercentSpecified
                    || this.NoteStateNameSpecified
                    || this.OriginalInterestRateDiscountPercentSpecified
                    || this.SharedAppreciationCapAmountSpecified
                    || this.SharedAppreciationRatePercentSpecified
                    || this.SupplementalMortgageTypeOtherDescriptionSpecified
                    || this.SupplementalMortgageTypeSpecified
                    || this.UPBChangeFrequencyTypeSpecified
                    || this.WeightedAverageInterestRatePercentSpecified;
            }
        }

        /// <summary>
        /// The dollar amount of an existing loan assumed by the borrower.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount AssumedLoanAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the AssumedLoanAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AssumedLoanAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool AssumedLoanAmountSpecified
        {
            get { return AssumedLoanAmount != null; }
            set { }
        }

        /// <summary>
        /// The base loan amount to be loaned to the borrower not including PMI, MIP, or Funding Fee.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOAmount BaseLoanAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the BaseLoanAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the BaseLoanAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool BaseLoanAmountSpecified
        {
            get { return BaseLoanAmount != null; }
            set { }
        }

        /// <summary>
        /// The value of the fully indexed interest rate, expressed as a percent, that must be disclosed to the borrower for adjustable rate mortgages.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOPercent DisclosedFullyIndexedRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the DisclosedFullyIndexedRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DisclosedFullyIndexedRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool DisclosedFullyIndexedRatePercentSpecified
        {
            get { return DisclosedFullyIndexedRatePercent != null; }
            set { }
        }

        /// <summary>
        /// The value of the financial index, expressed as a percent, used to calculate the Disclosed Fully Indexed Interest Rate that must be disclosed to the borrower for adjustable rate mortgages.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOPercent DisclosedIndexRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the DisclosedIndexRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DisclosedIndexRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool DisclosedIndexRatePercentSpecified
        {
            get { return DisclosedIndexRatePercent != null; }
            set { }
        }

        /// <summary>
        /// The number of percentage points to be added to the Disclosed Index Rate Percent to arrive at the Disclosed Fully Indexed Interest Rate that must be disclosed to the borrower for adjustable rate mortgages. 
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOPercent DisclosedMarginRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the DisclosedMarginRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DisclosedMarginRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool DisclosedMarginRatePercentSpecified
        {
            get { return DisclosedMarginRatePercent != null; }
            set { }
        }

        /// <summary>
        /// Specifies the priority of the lien against the subject property.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOEnum<LienPriorityBase> LienPriorityType;

        /// <summary>
        /// Gets or sets a value indicating whether the LienPriorityType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LienPriorityType element has been assigned a value.</value>
        [XmlIgnore]
        public bool LienPriorityTypeSpecified
        {
            get { return this.LienPriorityType != null && this.LienPriorityType.enumValue != LienPriorityBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture the lien priority type if Other is selected as the lien priority type.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOString LienPriorityTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the LienPriorityTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LienPriorityTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool LienPriorityTypeOtherDescriptionSpecified
        {
            get { return LienPriorityTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the purpose for which the loan proceeds will be used.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOEnum<LoanPurposeBase> LoanPurposeType;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanPurposeType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanPurposeType element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanPurposeTypeSpecified
        {
            get { return this.LoanPurposeType != null && this.LoanPurposeType.enumValue != LoanPurposeBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture the purpose of the loan when Other is indicated for Loan Purpose.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOString LoanPurposeTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanPurposeTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanPurposeTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanPurposeTypeOtherDescriptionSpecified
        {
            get { return LoanPurposeTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Identifies the highest level private or public sector entity under whose guidelines the mortgage is originated.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOEnum<MortgageBase> MortgageType;

        /// <summary>
        /// Gets or sets a value indicating whether the MortgageType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MortgageType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MortgageTypeSpecified
        {
            get { return this.MortgageType != null && this.MortgageType.enumValue != MortgageBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information or a description of the mortgage type when Other is selected.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOString MortgageTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the MortgageTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MortgageTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool MortgageTypeOtherDescriptionSpecified
        {
            get { return MortgageTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The amount to be repaid as disclosed on the note.
        /// </summary>
        [XmlElement(Order = 11)]
        public MISMOAmount NoteAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the NoteAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NoteAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool NoteAmountSpecified
        {
            get { return NoteAmount != null; }
            set { }
        }

        /// <summary>
        /// The city referenced at the top of the promissory note.
        /// </summary>
        [XmlElement(Order = 12)]
        public MISMOString NoteCityName;

        /// <summary>
        /// Gets or sets a value indicating whether the NoteCityName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NoteCityName element has been assigned a value.</value>
        [XmlIgnore]
        public bool NoteCityNameSpecified
        {
            get { return NoteCityName != null; }
            set { }
        }

        /// <summary>
        /// The date on the note.
        /// </summary>
        [XmlElement(Order = 13)]
        public MISMODate NoteDate;

        /// <summary>
        /// Gets or sets a value indicating whether the NoteDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NoteDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool NoteDateSpecified
        {
            get { return NoteDate != null; }
            set { }
        }

        /// <summary>
        /// The actual interest rate as disclosed on the note.
        /// </summary>
        [XmlElement(Order = 14)]
        public MISMOPercent NoteRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the NoteRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NoteRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool NoteRatePercentSpecified
        {
            get { return NoteRatePercent != null; }
            set { }
        }

        /// <summary>
        /// The state referenced at the top of the promissory note.
        /// </summary>
        [XmlElement(Order = 15)]
        public MISMOString NoteStateName;

        /// <summary>
        /// Gets or sets a value indicating whether the NoteStateName element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the NoteStateName element has been assigned a value.</value>
        [XmlIgnore]
        public bool NoteStateNameSpecified
        {
            get { return NoteStateName != null; }
            set { }
        }

        /// <summary>
        /// For mortgages with an initial discounted rate (teaser rate), the difference in percentage points between the initial discounted rate and the fully indexed rate. 
        /// </summary>
        [XmlElement(Order = 16)]
        public MISMOPercent OriginalInterestRateDiscountPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the OriginalInterestRateDiscountPercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OriginalInterestRateDiscountPercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool OriginalInterestRateDiscountPercentSpecified
        {
            get { return OriginalInterestRateDiscountPercent != null; }
            set { }
        }

        /// <summary>
        /// The maximum dollar amount that may be claimed as a share of the increase in value of the property.
        /// </summary>
        [XmlElement(Order = 17)]
        public MISMOAmount SharedAppreciationCapAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the SharedAppreciationCapAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SharedAppreciationCapAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool SharedAppreciationCapAmountSpecified
        {
            get { return SharedAppreciationCapAmount != null; }
            set { }
        }

        /// <summary>
        /// The percentage of the increase in property value that accrues when the appreciation is shared.
        /// </summary>
        [XmlElement(Order = 18)]
        public MISMOPercent SharedAppreciationRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the SharedAppreciationRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SharedAppreciationRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool SharedAppreciationRatePercentSpecified
        {
            get { return SharedAppreciationRatePercent != null; }
            set { }
        }

        /// <summary>
        /// Identifies another lower-level private or public sector agency under whose guidelines the mortgage is originated.
        /// </summary>
        [XmlElement(Order = 19)]
        public MISMOEnum<MortgageBase> SupplementalMortgageType;

        /// <summary>
        /// Gets or sets a value indicating whether the SupplementalMortgageType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SupplementalMortgageType element has been assigned a value.</value>
        [XmlIgnore]
        public bool SupplementalMortgageTypeSpecified
        {
            get { return this.SupplementalMortgageType != null && this.SupplementalMortgageType.enumValue != MortgageBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information or a description of the Supplemental Mortgage Type when Other is selected.
        /// </summary>
        [XmlElement(Order = 20)]
        public MISMOString SupplementalMortgageTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the SupplementalMortgageTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SupplementalMortgageTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool SupplementalMortgageTypeOtherDescriptionSpecified
        {
            get { return SupplementalMortgageTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Frequency of changes to the unpaid principal balance.
        /// </summary>
        [XmlElement(Order = 21)]
        public MISMOEnum<UPBChangeFrequencyBase> UPBChangeFrequencyType;

        /// <summary>
        /// Gets or sets a value indicating whether the UPBChangeFrequencyType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the UPBChangeFrequencyType element has been assigned a value.</value>
        [XmlIgnore]
        public bool UPBChangeFrequencyTypeSpecified
        {
            get { return this.UPBChangeFrequencyType != null && this.UPBChangeFrequencyType.enumValue != UPBChangeFrequencyBase.Blank; }
            set { }
        }

        /// <summary>
        /// An average of the different interest rates applicable to the transaction, based on the portions of the amount to which each interest rate applies, when multiple interest rates are applied to different portions of a loan's principal balance in a previously computed transaction.
        /// </summary>
        [XmlElement(Order = 22)]
        public MISMOPercent WeightedAverageInterestRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the WeightedAverageInterestRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the WeightedAverageInterestRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool WeightedAverageInterestRatePercentSpecified
        {
            get { return WeightedAverageInterestRatePercent != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 23)]
        public TERMS_OF_LOAN_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
