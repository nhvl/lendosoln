namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class RECORDING_ENDORSEMENT_EXEMPTIONS
    {
        /// <summary>
        /// Gets a value indicating whether the RECORDING_ENDORSEMENT_EXEMPTIONS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.RecordingEndorsementExemptionSpecified
                    || this.RecordingEndorsementExemptionsSummarySpecified;
            }
        }

        /// <summary>
        /// A collection of recording endorsement exemptions.
        /// </summary>
        [XmlElement("RECORDING_ENDORSEMENT_EXEMPTION", Order = 0)]
		public List<RECORDING_ENDORSEMENT_EXEMPTION> RecordingEndorsementExemption = new List<RECORDING_ENDORSEMENT_EXEMPTION>();

        /// <summary>
        /// Gets or sets a value indicating whether the RecordingEndorsementExemption element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RecordingEndorsementExemption element has been assigned a value.</value>
        [XmlIgnore]
        public bool RecordingEndorsementExemptionSpecified
        {
            get { return this.RecordingEndorsementExemption != null && this.RecordingEndorsementExemption.Count(r => r != null && r.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// Summary of recording endorsement exemptions.
        /// </summary>
        [XmlElement("RECORDING_ENDORSEMENT_EXEMPTIONS_SUMMARY", Order = 1)]
        public RECORDING_ENDORSEMENT_EXEMPTIONS_SUMMARY RecordingEndorsementExemptionsSummary;

        /// <summary>
        /// Gets or sets a value indicating whether the RecordingEndorsementExemptionsSummary element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RecordingEndorsementExemptionsSummary element has been assigned a value.</value>
        [XmlIgnore]
        public bool RecordingEndorsementExemptionsSummarySpecified
        {
            get { return this.RecordingEndorsementExemptionsSummary != null && this.RecordingEndorsementExemptionsSummary.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public RECORDING_ENDORSEMENT_EXEMPTIONS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
