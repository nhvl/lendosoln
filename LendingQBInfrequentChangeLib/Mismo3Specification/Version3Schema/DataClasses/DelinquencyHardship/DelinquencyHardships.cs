namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class DELINQUENCY_HARDSHIPS
    {
        /// <summary>
        /// Gets a value indicating whether the DELINQUENCY_HARDSHIPS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DelinquencyHardshipSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of delinquency hardships.
        /// </summary>
        [XmlElement("DELINQUENCY_HARDSHIP", Order = 0)]
		public List<DELINQUENCY_HARDSHIP> DelinquencyHardship = new List<DELINQUENCY_HARDSHIP>();

        /// <summary>
        /// Gets or sets a value indicating whether the DelinquencyHardship element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the DelinquencyHardship element has been assigned a value.</value>
        [XmlIgnore]
        public bool DelinquencyHardshipSpecified
        {
            get { return this.DelinquencyHardship != null && this.DelinquencyHardship.Count(d => d != null && d.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public DELINQUENCY_HARDSHIPS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
