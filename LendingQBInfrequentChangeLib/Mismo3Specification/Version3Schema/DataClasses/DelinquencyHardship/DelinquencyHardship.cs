namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class DELINQUENCY_HARDSHIP
    {
        /// <summary>
        /// Gets a value indicating whether the DELINQUENCY_HARDSHIP container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.HardshipContactDateSpecified
                    || this.HardshipDurationTypeOtherDescriptionSpecified
                    || this.HardshipDurationTypeSpecified
                    || this.HardshipEndDateSpecified
                    || this.HardshipReasonCommentTextSpecified
                    || this.HardshipResolvedIndicatorSpecified
                    || this.HardshipStartDateSpecified
                    || this.LoanDelinquencyReasonTypeOtherDescriptionSpecified
                    || this.LoanDelinquencyReasonTypeSpecified;
            }
        }

        /// <summary>
        /// The latest contact date when the servicer received an update to the reason for default or hardship from the borrower.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMODate HardshipContactDate;

        /// <summary>
        /// Gets or sets a value indicating whether the HardshipContactDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HardshipContactDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool HardshipContactDateSpecified
        {
            get { return HardshipContactDate != null; }
            set { }
        }

        /// <summary>
        /// Indicates the duration of the borrower's primary hardship. Duration is determined based on trading party policy guidelines.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<HardshipDurationBase> HardshipDurationType;

        /// <summary>
        /// Gets or sets a value indicating whether the HardshipDurationType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HardshipDurationType element has been assigned a value.</value>
        [XmlIgnore]
        public bool HardshipDurationTypeSpecified
        {
            get { return this.HardshipDurationType != null && this.HardshipDurationType.enumValue != HardshipDurationBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Hardship Duration Type.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString HardshipDurationTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the HardshipDurationTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HardshipDurationTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool HardshipDurationTypeOtherDescriptionSpecified
        {
            get { return HardshipDurationTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Identifies the end date of the borrower's hardship, on their mortgage payment obligations.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMODate HardshipEndDate;

        /// <summary>
        /// Gets or sets a value indicating whether the HardshipEndDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HardshipEndDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool HardshipEndDateSpecified
        {
            get { return HardshipEndDate != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field with comments concerning the hardship reason.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString HardshipReasonCommentText;

        /// <summary>
        /// Gets or sets a value indicating whether the HardshipReasonCommentText element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HardshipReasonCommentText element has been assigned a value.</value>
        [XmlIgnore]
        public bool HardshipReasonCommentTextSpecified
        {
            get { return HardshipReasonCommentText != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether borrower has resolved all previous hardships.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOIndicator HardshipResolvedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the HardshipResolvedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HardshipResolvedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool HardshipResolvedIndicatorSpecified
        {
            get { return HardshipResolvedIndicator != null; }
            set { }
        }

        /// <summary>
        /// Identifies the start date of the borrower's hardship, on their mortgage payment obligations.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMODate HardshipStartDate;

        /// <summary>
        /// Gets or sets a value indicating whether the HardshipStartDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HardshipStartDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool HardshipStartDateSpecified
        {
            get { return HardshipStartDate != null; }
            set { }
        }

        /// <summary>
        /// The reason why the Borrower has become delinquent on the loan.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOEnum<LoanDelinquencyReasonBase> LoanDelinquencyReasonType;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanDelinquencyReasonType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanDelinquencyReasonType element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanDelinquencyReasonTypeSpecified
        {
            get { return this.LoanDelinquencyReasonType != null && this.LoanDelinquencyReasonType.enumValue != LoanDelinquencyReasonBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture the Loan Delinquency Reason type if Other is selected.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOString LoanDelinquencyReasonTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the LoanDelinquencyReasonTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LoanDelinquencyReasonTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool LoanDelinquencyReasonTypeOtherDescriptionSpecified
        {
            get { return LoanDelinquencyReasonTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 9)]
        public DELINQUENCY_HARDSHIP_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
