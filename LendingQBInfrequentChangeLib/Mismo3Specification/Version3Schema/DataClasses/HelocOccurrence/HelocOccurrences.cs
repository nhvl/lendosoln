namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class HELOC_OCCURRENCES
    {
        /// <summary>
        /// Gets a value indicating whether the HELOC_OCCURRENCES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.HELOCOccurrenceSpecified;
            }
        }

        /// <summary>
        /// A collection of HELOC occurrences.
        /// </summary>
        [XmlElement("HELOC_OCCURRENCE", Order = 0)]
		public List<HELOC_OCCURRENCE> HELOCOccurrence = new List<HELOC_OCCURRENCE>();

        /// <summary>
        /// Gets or sets a value indicating whether the Home Equity Line Of Credit Occurrence element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Home Equity Line Of Credit Occurrence element has been assigned a value.</value>
        [XmlIgnore]
        public bool HELOCOccurrenceSpecified
        {
            get { return this.HELOCOccurrence != null && this.HELOCOccurrence.Count(h => h != null && h.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public HELOC_OCCURRENCES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
