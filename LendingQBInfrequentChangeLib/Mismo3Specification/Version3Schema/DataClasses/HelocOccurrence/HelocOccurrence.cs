namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class HELOC_OCCURRENCE
    {
        /// <summary>
        /// Gets a value indicating whether the HELOC_OCCURRENCE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CurrentHELOCMaximumBalanceAmountSpecified
                    || this.ExtensionSpecified
                    || this.HELOCBalanceAmountSpecified
                    || this.HELOCDailyPeriodicInterestRatePercentSpecified
                    || this.HELOCTeaserTermEndDateSpecified;
            }
        }

        /// <summary>
        /// The total dollar amount of the line of credit as of the date reported.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount CurrentHELOCMaximumBalanceAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the CurrentHELOCMaximumBalanceAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CurrentHELOCMaximumBalanceAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool CurrentHELOCMaximumBalanceAmountSpecified
        {
            get { return CurrentHELOCMaximumBalanceAmount != null; }
            set { }
        }

        /// <summary>
        /// The outstanding balance of the home equity line of credit (HELOC).
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOAmount HELOCBalanceAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the HELOCBalanceAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HELOCBalanceAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool HELOCBalanceAmountSpecified
        {
            get { return HELOCBalanceAmount != null; }
            set { }
        }

        /// <summary>
        /// The interest rate percent used to calculate the dollar amount of the daily periodic interest charged in the HELOC loan transaction.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOPercent HELOCDailyPeriodicInterestRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the HELOCDailyPeriodicInterestRatePercent element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HELOCDailyPeriodicInterestRatePercent element has been assigned a value.</value>
        [XmlIgnore]
        public bool HELOCDailyPeriodicInterestRatePercentSpecified
        {
            get { return HELOCDailyPeriodicInterestRatePercent != null; }
            set { }
        }

        /// <summary>
        /// The date when the discounted interest rate is no longer used and the interest rate is determined using the index and margin.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMODate HELOCTeaserTermEndDate;

        /// <summary>
        /// Gets or sets a value indicating whether the HELOCTeaserTermEndDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HELOCTeaserTermEndDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool HELOCTeaserTermEndDateSpecified
        {
            get { return HELOCTeaserTermEndDate != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public HELOC_OCCURRENCE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
