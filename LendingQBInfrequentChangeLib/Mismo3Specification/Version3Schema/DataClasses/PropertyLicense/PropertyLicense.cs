namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class PROPERTY_LICENSE
    {
        /// <summary>
        /// Gets a value indicating whether the PROPERTY_LICENSE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.PropertyLicenseTypeOtherDescriptionSpecified
                    || this.PropertyLicenseTypeSpecified;
            }
        }

        /// <summary>
        /// The type of license or certificate to operate as a right of use issued by a local jurisdiction for the property.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<PropertyLicenseBase> PropertyLicenseType;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyLicenseType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyLicenseType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyLicenseTypeSpecified
        {
            get { return this.PropertyLicenseType != null && this.PropertyLicenseType.enumValue != PropertyLicenseBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Property License Type.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString PropertyLicenseTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PropertyLicenseTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PropertyLicenseTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PropertyLicenseTypeOtherDescriptionSpecified
        {
            get { return PropertyLicenseTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public PROPERTY_LICENSE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
