namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class FIELD_REFERENCE
    {
        /// <summary>
        /// Gets a value indicating whether the FIELD_REFERENCE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.FieldHeightNumberSpecified
                    || this.FieldPlaceholderIdentifierSpecified
                    || this.FieldWidthNumberSpecified
                    || this.MeasurementUnitTypeSpecified
                    || this.OffsetFromLeftNumberSpecified
                    || this.OffsetFromTopNumberSpecified
                    || this.PageNumberValueSpecified;
            }
        }

        /// <summary>
        /// The height of the view field area, in measurement units.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMONumeric FieldHeightNumber;

        /// <summary>
        /// Gets or sets a value indicating whether the FieldHeightNumber element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FieldHeightNumber element has been assigned a value.</value>
        [XmlIgnore]
        public bool FieldHeightNumberSpecified
        {
            get { return FieldHeightNumber != null; }
            set { }
        }

        /// <summary>
        /// The identifier of the object in the view that represents the view field area. The identification method is dependent upon the view file format. For example, in an XHTML view, this is the id of the span or div tag that constitutes the view field. In a PDF view, this is the PDF field name.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOIdentifier FieldPlaceholderIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the FieldPlaceholderIdentifier element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FieldPlaceholderIdentifier element has been assigned a value.</value>
        [XmlIgnore]
        public bool FieldPlaceholderIdentifierSpecified
        {
            get { return FieldPlaceholderIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The width of the view field area, in measurement units.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMONumeric FieldWidthNumber;

        /// <summary>
        /// Gets or sets a value indicating whether the FieldWidthNumber element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FieldWidthNumber element has been assigned a value.</value>
        [XmlIgnore]
        public bool FieldWidthNumberSpecified
        {
            get { return FieldWidthNumber != null; }
            set { }
        }

        /// <summary>
        /// The type of measurement unit used to express the top offset, the left offset, the width and the height of the view field area. Available options are: Inches, Centimeters, Points and Pixels.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOEnum<MeasurementUnitBase> MeasurementUnitType;

        /// <summary>
        /// Gets or sets a value indicating whether the MeasurementUnitType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the MeasurementUnitType element has been assigned a value.</value>
        [XmlIgnore]
        public bool MeasurementUnitTypeSpecified
        {
            get { return this.MeasurementUnitType != null && this.MeasurementUnitType.enumValue != MeasurementUnitBase.Blank; }
            set { }
        }

        /// <summary>
        /// The distance, in measurement units, from the left edge of the page to the left edge of the view field area.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMONumeric OffsetFromLeftNumber;

        /// <summary>
        /// Gets or sets a value indicating whether the OffsetFromLeftNumber element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OffsetFromLeftNumber element has been assigned a value.</value>
        [XmlIgnore]
        public bool OffsetFromLeftNumberSpecified
        {
            get { return OffsetFromLeftNumber != null; }
            set { }
        }

        /// <summary>
        /// The distance, in measurement units, from the top edge of the page to the top edge of the view field area.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMONumeric OffsetFromTopNumber;

        /// <summary>
        /// Gets or sets a value indicating whether the OffsetFromTopNumber element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OffsetFromTopNumber element has been assigned a value.</value>
        [XmlIgnore]
        public bool OffsetFromTopNumberSpecified
        {
            get { return OffsetFromTopNumber != null; }
            set { }
        }

        /// <summary>
        /// The page number of the page that contains this field reference.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOValue PageNumberValue;

        /// <summary>
        /// Gets or sets a value indicating whether the PageNumberValue element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PageNumberValue element has been assigned a value.</value>
        [XmlIgnore]
        public bool PageNumberValueSpecified
        {
            get { return PageNumberValue != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 7)]
        public FIELD_REFERENCE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
