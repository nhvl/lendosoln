namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class SOLICITATION_PREFERENCE
    {
        /// <summary>
        /// Gets a value indicating whether the SOLICITATION_PREFERENCE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CorporateAffiliateSharingAllowedIndicatorSpecified
                    || this.ExtensionSpecified
                    || this.SolicitationByEmailAllowedIndicatorSpecified
                    || this.SolicitationByTelephoneAllowedIndicatorSpecified
                    || this.SolicitationByUSMailAllowedIndicatorSpecified;
            }
        }

        /// <summary>
        /// Indicates that the lender is authorized to share loan information with corporate affiliates.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOIndicator CorporateAffiliateSharingAllowedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the CorporateAffiliateSharingAllowedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CorporateAffiliateSharingAllowedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool CorporateAffiliateSharingAllowedIndicatorSpecified
        {
            get { return CorporateAffiliateSharingAllowedIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether the borrower wishes to receive Email solicitation from the lender.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOIndicator SolicitationByEmailAllowedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the SolicitationByEmailAllowedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SolicitationByEmailAllowedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool SolicitationByEmailAllowedIndicatorSpecified
        {
            get { return SolicitationByEmailAllowedIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether the borrower wishes to receive telephone solicitation from the lender.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOIndicator SolicitationByTelephoneAllowedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the SolicitationByTelephoneAllowedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SolicitationByTelephoneAllowedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool SolicitationByTelephoneAllowedIndicatorSpecified
        {
            get { return SolicitationByTelephoneAllowedIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether the borrower wishes to receive US Mail solicitation from the lender.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOIndicator SolicitationByUSMailAllowedIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the SolicitationByUSMailAllowedIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SolicitationByUSMailAllowedIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool SolicitationByUSMailAllowedIndicatorSpecified
        {
            get { return SolicitationByUSMailAllowedIndicator != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public SOLICITATION_PREFERENCE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
