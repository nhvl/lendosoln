namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CAPITALIZATIONS
    {
        /// <summary>
        /// Gets a value indicating whether the CAPITALIZATIONS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CapitalizationSpecified
                    || this.CapitalizationSummarySpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of capitalizations.
        /// </summary>
        [XmlElement("CAPITALIZATION", Order = 0)]
		public List<CAPITALIZATION> Capitalization = new List<CAPITALIZATION>();

        /// <summary>
        /// Gets or sets a value indicating whether the Capitalization element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Capitalization element has been assigned a value.</value>
        [XmlIgnore]
        public bool CapitalizationSpecified
        {
            get { return this.Capitalization != null && this.Capitalization.Count(c => c != null && c.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// Summary of capitalization.
        /// </summary>
        [XmlElement("CAPITALIZATION_SUMMARY", Order = 1)]
        public CAPITALIZATION_SUMMARY CapitalizationSummary;

        /// <summary>
        /// Gets or sets a value indicating whether the CapitalizationSummary element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CapitalizationSummary element has been assigned a value.</value>
        [XmlIgnore]
        public bool CapitalizationSummarySpecified
        {
            get { return this.CapitalizationSummary != null && this.CapitalizationSummary.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public CAPITALIZATIONS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
