namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class CAPITALIZATION
    {
        /// <summary>
        /// Gets a value indicating whether the CAPITALIZATION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CapitalizationAmountSpecified
                    || this.CapitalizationChargeTypeOtherDescriptionSpecified
                    || this.CapitalizationChargeTypeSpecified
                    || this.CapitalizationCommentTextSpecified
                    || this.CapitalizationDateSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// The dollar amount of the Capitalization Charge Type.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount CapitalizationAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the CapitalizationAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CapitalizationAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool CapitalizationAmountSpecified
        {
            get { return CapitalizationAmount != null; }
            set { }
        }

        /// <summary>
        /// Specifies the type of fee or charge that is capitalized into the principal balance.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<CapitalizationChargeBase> CapitalizationChargeType;

        /// <summary>
        /// Gets or sets a value indicating whether the CapitalizationChargeType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CapitalizationChargeType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CapitalizationChargeTypeSpecified
        {
            get { return this.CapitalizationChargeType != null && this.CapitalizationChargeType.enumValue != CapitalizationChargeBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is  selected as the Capitalization Charge Type.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString CapitalizationChargeTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the CapitalizationChargeTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CapitalizationChargeTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool CapitalizationChargeTypeOtherDescriptionSpecified
        {
            get { return CapitalizationChargeTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A free form comment or description associated with the capitalization charge.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString CapitalizationCommentText;

        /// <summary>
        /// Gets or sets a value indicating whether the CapitalizationCommentText element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CapitalizationCommentText element has been assigned a value.</value>
        [XmlIgnore]
        public bool CapitalizationCommentTextSpecified
        {
            get { return CapitalizationCommentText != null; }
            set { }
        }

        /// <summary>
        /// The date on which the fee or charge was capitalized into the principal balance.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMODate CapitalizationDate;

        /// <summary>
        /// Gets or sets a value indicating whether the CapitalizationDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CapitalizationDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool CapitalizationDateSpecified
        {
            get { return CapitalizationDate != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 5)]
        public CAPITALIZATION_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
