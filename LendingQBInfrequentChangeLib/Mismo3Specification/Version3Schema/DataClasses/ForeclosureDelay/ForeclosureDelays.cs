namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class FORECLOSURE_DELAYS
    {
        /// <summary>
        /// Gets a value indicating whether the FORECLOSURE_DELAYS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ForeclosureDelaySpecified;
            }
        }

        /// <summary>
        /// A collection of foreclosure delays.
        /// </summary>
        [XmlElement("FORECLOSURE_DELAY", Order = 0)]
		public List<FORECLOSURE_DELAY> ForeclosureDelay = new List<FORECLOSURE_DELAY>();

        /// <summary>
        /// Gets or sets a value indicating whether the ForeclosureDelay element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ForeclosureDelay element has been assigned a value.</value>
        [XmlIgnore]
        public bool ForeclosureDelaySpecified
        {
            get { return this.ForeclosureDelay != null && this.ForeclosureDelay.Count(f => f != null && f.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public FORECLOSURE_DELAYS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
