namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class FORECLOSURE_DELAY
    {
        /// <summary>
        /// Gets a value indicating whether the FORECLOSURE_DELAY container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ForeclosureDelayActualEndDateSpecified
                    || this.ForeclosureDelayCategoryAdditionalDescriptionSpecified
                    || this.ForeclosureDelayCategoryTypeOtherDescriptionSpecified
                    || this.ForeclosureDelayCategoryTypeSpecified
                    || this.ForeclosureDelayProjectedEndDateSpecified
                    || this.ForeclosureDelayStartDateSpecified;
            }
        }

        /// <summary>
        /// The actual date the foreclosure delay event ended.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMODate ForeclosureDelayActualEndDate;

        /// <summary>
        /// Gets or sets a value indicating whether the ForeclosureDelayActualEndDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ForeclosureDelayActualEndDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ForeclosureDelayActualEndDateSpecified
        {
            get { return ForeclosureDelayActualEndDate != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to provide a supplemental comment or remark to an individual foreclosure delay category.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString ForeclosureDelayCategoryAdditionalDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ForeclosureDelayCategoryAdditionalDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ForeclosureDelayCategoryAdditionalDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ForeclosureDelayCategoryAdditionalDescriptionSpecified
        {
            get { return ForeclosureDelayCategoryAdditionalDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies a general grouping for activities that may create a processing delay.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOEnum<ForeclosureDelayCategoryBase> ForeclosureDelayCategoryType;

        /// <summary>
        /// Gets or sets a value indicating whether the ForeclosureDelayCategoryType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ForeclosureDelayCategoryType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ForeclosureDelayCategoryTypeSpecified
        {
            get { return this.ForeclosureDelayCategoryType != null && this.ForeclosureDelayCategoryType.enumValue != ForeclosureDelayCategoryBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected as the Foreclosure Delay Category Type.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOString ForeclosureDelayCategoryTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ForeclosureDelayCategoryTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ForeclosureDelayCategoryTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ForeclosureDelayCategoryTypeOtherDescriptionSpecified
        {
            get { return ForeclosureDelayCategoryTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The date that a foreclosure delay is projected to end.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMODate ForeclosureDelayProjectedEndDate;

        /// <summary>
        /// Gets or sets a value indicating whether the ForeclosureDelayProjectedEndDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ForeclosureDelayProjectedEndDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ForeclosureDelayProjectedEndDateSpecified
        {
            get { return ForeclosureDelayProjectedEndDate != null; }
            set { }
        }

        /// <summary>
        /// The actual date the foreclosure delay event starts.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMODate ForeclosureDelayStartDate;

        /// <summary>
        /// Gets or sets a value indicating whether the ForeclosureDelayStartDate element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ForeclosureDelayStartDate element has been assigned a value.</value>
        [XmlIgnore]
        public bool ForeclosureDelayStartDateSpecified
        {
            get { return ForeclosureDelayStartDate != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 6)]
        public FORECLOSURE_DELAY_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
