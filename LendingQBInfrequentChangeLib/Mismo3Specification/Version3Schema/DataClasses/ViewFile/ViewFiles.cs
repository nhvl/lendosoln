namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class VIEW_FILES
    {
        /// <summary>
        /// Gets a value indicating whether the VIEW_FILES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.ViewFileSpecified;
            }
        }

        /// <summary>
        /// A collection of view files.
        /// </summary>
        [XmlElement("VIEW_FILE", Order = 0)]
		public List<VIEW_FILE> ViewFile = new List<VIEW_FILE>();

        /// <summary>
        /// Gets or sets a value indicating whether the ViewFile element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ViewFile element has been assigned a value.</value>
        [XmlIgnore]
        public bool ViewFileSpecified
        {
            get { return this.ViewFile != null && this.ViewFile.Count(v => v != null && v.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public VIEW_FILES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
