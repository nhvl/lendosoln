namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class RECORDING_ENDORSEMENT_FEES
    {
        /// <summary>
        /// Gets a value indicating whether the RECORDING_ENDORSEMENT_FEES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.RecordingEndorsementFeeSpecified
                    || this.RecordingEndorsementFeesSummarySpecified;
            }
        }

        /// <summary>
        /// A collection of recording endorsement fees.
        /// </summary>
        [XmlElement("RECORDING_ENDORSEMENT_FEE", Order = 0)]
		public List<RECORDING_ENDORSEMENT_FEE> RecordingEndorsementFee = new List<RECORDING_ENDORSEMENT_FEE>();

        /// <summary>
        /// Gets or sets a value indicating whether the RecordingEndorsementFee element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RecordingEndorsementFee element has been assigned a value.</value>
        [XmlIgnore]
        public bool RecordingEndorsementFeeSpecified
        {
            get { return this.RecordingEndorsementFee != null && this.RecordingEndorsementFee.Count(r => r != null && r.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// Summary of recording endorsement fees.
        /// </summary>
        [XmlElement("RECORDING_ENDORSEMENT_FEES_SUMMARY", Order = 1)]
        public RECORDING_ENDORSEMENT_FEES_SUMMARY RecordingEndorsementFeesSummary;

        /// <summary>
        /// Gets or sets a value indicating whether the RecordingEndorsementFeesSummary element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the RecordingEndorsementFeesSummary element has been assigned a value.</value>
        [XmlIgnore]
        public bool RecordingEndorsementFeesSummarySpecified
        {
            get { return this.RecordingEndorsementFeesSummary != null && this.RecordingEndorsementFeesSummary.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public RECORDING_ENDORSEMENT_FEES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
