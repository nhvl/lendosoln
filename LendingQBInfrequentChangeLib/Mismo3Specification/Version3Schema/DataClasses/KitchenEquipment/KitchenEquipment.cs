namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class KITCHEN_EQUIPMENT
    {
        /// <summary>
        /// Gets a value indicating whether the KITCHEN_EQUIPMENT container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ComponentAdjustmentAmountSpecified
                    || this.ComponentClassificationTypeSpecified
                    || this.ConditionRatingDescriptionSpecified
                    || this.ConditionRatingTypeSpecified
                    || this.ExtensionSpecified
                    || this.KitchenEquipmentCountSpecified
                    || this.KitchenEquipmentDescriptionSpecified
                    || this.KitchenEquipmentMakeModelDescriptionSpecified
                    || this.KitchenEquipmentTypeOtherDescriptionSpecified
                    || this.KitchenEquipmentTypeSpecified
                    || this.QualityRatingDescriptionSpecified
                    || this.QualityRatingTypeSpecified;
            }
        }

        /// <summary>
        /// The dollar amount (either positive or negative) adjustment being made for a specific component of the property.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOAmount ComponentAdjustmentAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the ComponentAdjustmentAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ComponentAdjustmentAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool ComponentAdjustmentAmountSpecified
        {
            get { return ComponentAdjustmentAmount != null; }
            set { }
        }

        /// <summary>
        /// Specifies whether the component is considered real or personal property.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<ComponentClassificationBase> ComponentClassificationType;

        /// <summary>
        /// Gets or sets a value indicating whether the ComponentClassificationType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ComponentClassificationType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ComponentClassificationTypeSpecified
        {
            get { return this.ComponentClassificationType != null && this.ComponentClassificationType.enumValue != ComponentClassificationBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe in detail the condition rating of the identified component.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString ConditionRatingDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ConditionRatingDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConditionRatingDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConditionRatingDescriptionSpecified
        {
            get { return ConditionRatingDescription != null; }
            set { }
        }

        /// <summary>
        /// A rating of the condition of the identified component.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOEnum<ConditionRatingBase> ConditionRatingType;

        /// <summary>
        /// Gets or sets a value indicating whether the ConditionRatingType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConditionRatingType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConditionRatingTypeSpecified
        {
            get { return this.ConditionRatingType != null && this.ConditionRatingType.enumValue != ConditionRatingBase.Blank; }
            set { }
        }

        /// <summary>
        /// Holds the number of kitchen items specified by Kitchen Equipment Type.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOCount KitchenEquipmentCount;

        /// <summary>
        /// Gets or sets a value indicating whether the KitchenEquipmentCount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the KitchenEquipmentCount element has been assigned a value.</value>
        [XmlIgnore]
        public bool KitchenEquipmentCountSpecified
        {
            get { return KitchenEquipmentCount != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe in detail the identified kitchen equipment.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOString KitchenEquipmentDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the KitchenEquipmentDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the KitchenEquipmentDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool KitchenEquipmentDescriptionSpecified
        {
            get { return KitchenEquipmentDescription != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to capture the make and model of the kitchen equipment.
        /// </summary>
        [XmlElement(Order = 6)]
        public MISMOString KitchenEquipmentMakeModelDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the KitchenEquipmentMakeModelDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the KitchenEquipmentMakeModelDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool KitchenEquipmentMakeModelDescriptionSpecified
        {
            get { return KitchenEquipmentMakeModelDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the kitchen equipment normally found in a living unit or residence.
        /// </summary>
        [XmlElement(Order = 7)]
        public MISMOEnum<KitchenEquipmentBase> KitchenEquipmentType;

        /// <summary>
        /// Gets or sets a value indicating whether the KitchenEquipmentType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the KitchenEquipmentType element has been assigned a value.</value>
        [XmlIgnore]
        public bool KitchenEquipmentTypeSpecified
        {
            get { return this.KitchenEquipmentType != null && this.KitchenEquipmentType.enumValue != KitchenEquipmentBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe the kitchen equipment if Other is selected as the Kitchen Equipment Type.
        /// </summary>
        [XmlElement(Order = 8)]
        public MISMOString KitchenEquipmentTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the KitchenEquipmentTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the KitchenEquipmentTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool KitchenEquipmentTypeOtherDescriptionSpecified
        {
            get { return KitchenEquipmentTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to describe in detail the quality rating of the identified component.
        /// </summary>
        [XmlElement(Order = 9)]
        public MISMOString QualityRatingDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the QualityRatingDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the QualityRatingDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool QualityRatingDescriptionSpecified
        {
            get { return QualityRatingDescription != null; }
            set { }
        }

        /// <summary>
        /// A rating of the quality of the identified component type.
        /// </summary>
        [XmlElement(Order = 10)]
        public MISMOEnum<QualityRatingBase> QualityRatingType;

        /// <summary>
        /// Gets or sets a value indicating whether the QualityRatingType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the QualityRatingType element has been assigned a value.</value>
        [XmlIgnore]
        public bool QualityRatingTypeSpecified
        {
            get { return this.QualityRatingType != null && this.QualityRatingType.enumValue != QualityRatingBase.Blank; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 11)]
        public KITCHEN_EQUIPMENT_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }
}
