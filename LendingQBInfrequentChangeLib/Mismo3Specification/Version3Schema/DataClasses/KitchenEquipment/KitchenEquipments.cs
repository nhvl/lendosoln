namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class KITCHEN_EQUIPMENTS
    {
        /// <summary>
        /// Gets a value indicating whether the KITCHEN_EQUIPMENTS container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.KitchenEquipmentSpecified;
            }
        }

        /// <summary>
        /// A collection of kitchen equipment.
        /// </summary>
        [XmlElement("KITCHEN_EQUIPMENT", Order = 0)]
		public List<KITCHEN_EQUIPMENT> KitchenEquipment = new List<KITCHEN_EQUIPMENT>();

        /// <summary>
        /// Gets or sets a value indicating whether the KitchenEquipment element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the KitchenEquipment element has been assigned a value.</value>
        [XmlIgnore]
        public bool KitchenEquipmentSpecified
        {
            get { return this.KitchenEquipment != null && this.KitchenEquipment.Count(k => k != null && k.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public KITCHEN_EQUIPMENTS_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
