namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class CREDIT_TRADE_REFERENCES
    {
        /// <summary>
        /// Gets a value indicating whether the CREDIT_TRADE_REFERENCES container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditTradeReferenceSpecified
                    || this.ExtensionSpecified;
            }
        }

        /// <summary>
        /// A collection of credit trade references.
        /// </summary>
        [XmlElement("CREDIT_TRADE_REFERENCE", Order = 0)]
		public List<CREDIT_TRADE_REFERENCE> CreditTradeReference = new List<CREDIT_TRADE_REFERENCE>();

        /// <summary>
        /// Gets or sets a value indicating whether the CreditTradeReference element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CreditTradeReference element has been assigned a value.</value>
        [XmlIgnore]
        public bool CreditTradeReferenceSpecified
        {
            get { return this.CreditTradeReference != null && this.CreditTradeReference.Count(c => c != null && c.ShouldSerialize) > 0; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public CREDIT_TRADE_REFERENCES_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
