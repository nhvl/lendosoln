namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
    /// </summary>
    /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
    public partial class PARTIAL_PAYMENT
    {
        /// <summary>
        /// Gets a value indicating whether the PARTIAL_PAYMENT container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.PartialPaymentApplicationItemTypeSpecified
                    || this.PartialPaymentApplicationMethodTypeOtherDescriptionSpecified
                    || this.PartialPaymentApplicationMethodTypeSpecified
                    || this.PartialPaymentApplicationOrderTypeSpecified;
            }
        }

        /// <summary>
        /// Specifies the item to which a partial payment is to be applied.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOEnum<PartialPaymentApplicationItemBase> PartialPaymentApplicationItemType;

        /// <summary>
        /// Gets or sets a value indicating whether the PartialPaymentApplicationItemType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PartialPaymentApplicationItemType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PartialPaymentApplicationItemTypeSpecified
        {
            get { return this.PartialPaymentApplicationItemType != null && this.PartialPaymentApplicationItemType.enumValue != PartialPaymentApplicationItemBase.Blank; }
            set { }
        }

        /// <summary>
        /// The method used for applying a partial payment to the loan.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<PartialPaymentApplicationMethodBase> PartialPaymentApplicationMethodType;

        /// <summary>
        /// Gets or sets a value indicating whether the PartialPaymentApplicationMethodType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PartialPaymentApplicationMethodType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PartialPaymentApplicationMethodTypeSpecified
        {
            get { return this.PartialPaymentApplicationMethodType != null && this.PartialPaymentApplicationMethodType.enumValue != PartialPaymentApplicationMethodBase.Blank; }
            set { }
        }

        /// <summary>
        /// A free-form text field used to collect additional information when Other is selected for Partial Payment Application Method Type.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOString PartialPaymentApplicationMethodTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the PartialPaymentApplicationMethodTypeOtherDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PartialPaymentApplicationMethodTypeOtherDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool PartialPaymentApplicationMethodTypeOtherDescriptionSpecified
        {
            get { return PartialPaymentApplicationMethodTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Specifies the item to which a partial payment is to be applied.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOEnum<PartialPaymentApplicationOrderBase> PartialPaymentApplicationOrderType;

        /// <summary>
        /// Gets or sets a value indicating whether the PartialPaymentApplicationOrderType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PartialPaymentApplicationOrderType element has been assigned a value.</value>
        [XmlIgnore]
        public bool PartialPaymentApplicationOrderTypeSpecified
        {
            get { return this.PartialPaymentApplicationOrderType != null && this.PartialPaymentApplicationOrderType.enumValue != PartialPaymentApplicationOrderBase.Blank; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 4)]
        public PARTIAL_PAYMENT_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }

        /// <summary>
        /// An integer value used to provide an order to multi-instance sibling elements. The value must be unique for each sibling element.
        /// </summary>
        [XmlIgnore]
        private int sequenceNumber;

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>An integer indicating the order for multi-instance sibling elements.</value>
        [XmlIgnore]
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { this.sequenceNumber = value; this.SequenceNumberSpecified = true; }
        }

        /// <summary>
        /// Indicates whether the sequence number has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool SequenceNumberSpecified = false;

        /// <summary>
        /// Gets or sets the sequence number as a string for serialization.
        /// </summary>
        /// <value>Gets the value of the SequenceNumber property as a string for serialization.</value>
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized
        {
            get { return SequenceNumber.ToString(); }
            set { }
        }

        /// <summary>
        /// Indicates whether the sequence number can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the sequence number can be serialized.</returns>
        public bool ShouldSerializeSequenceNumberSerialized()
        {
            return SequenceNumberSpecified;
        }
    }    
}
