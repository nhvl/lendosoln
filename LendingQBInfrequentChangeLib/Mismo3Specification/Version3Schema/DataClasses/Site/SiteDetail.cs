namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class SITE_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the SITE_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CharacteristicsAffectMarketabilityDescriptionSpecified
                    || this.ConditionRatingTypeSpecified
                    || this.CornerLotIndicatorSpecified
                    || this.ExtensionSpecified
                    || this.SiteAdverseConditionsIndicatorSpecified
                    || this.SiteHighestBestUseDescriptionSpecified
                    || this.SiteHighestBestUseIndicatorSpecified;
            }
        }

        /// <summary>
        /// A free-form text field describing the characteristics that affect marketability.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString CharacteristicsAffectMarketabilityDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the CharacteristicsAffectMarketabilityDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CharacteristicsAffectMarketabilityDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool CharacteristicsAffectMarketabilityDescriptionSpecified
        {
            get { return CharacteristicsAffectMarketabilityDescription != null; }
            set { }
        }

        /// <summary>
        /// A rating of the condition of the identified component.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOEnum<ConditionRatingBase> ConditionRatingType;

        /// <summary>
        /// Gets or sets a value indicating whether the ConditionRatingType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ConditionRatingType element has been assigned a value.</value>
        [XmlIgnore]
        public bool ConditionRatingTypeSpecified
        {
            get { return this.ConditionRatingType != null && this.ConditionRatingType.enumValue != ConditionRatingBase.Blank; }
            set { }
        }

        /// <summary>
        /// Indicates that the property lot or site is situated on a corner.
        /// </summary>
        [XmlElement(Order = 2)]
        public MISMOIndicator CornerLotIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the CornerLotIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CornerLotIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool CornerLotIndicatorSpecified
        {
            get { return CornerLotIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates that there adverse site conditions or external factors.
        /// </summary>
        [XmlElement(Order = 3)]
        public MISMOIndicator SiteAdverseConditionsIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the SiteAdverseConditionsIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SiteAdverseConditionsIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool SiteAdverseConditionsIndicatorSpecified
        {
            get { return SiteAdverseConditionsIndicator != null; }
            set { }
        }

        /// <summary>
        /// Specifies a use, other than the present use, that would constitute a higher and best use of the site.
        /// </summary>
        [XmlElement(Order = 4)]
        public MISMOString SiteHighestBestUseDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the SiteHighestBestUseDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SiteHighestBestUseDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool SiteHighestBestUseDescriptionSpecified
        {
            get { return SiteHighestBestUseDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates that the present use is the highest and best of the site as improved.
        /// </summary>
        [XmlElement(Order = 5)]
        public MISMOIndicator SiteHighestBestUseIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the SiteHighestBestUseIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SiteHighestBestUseIndicator element has been assigned a value.</value>
        [XmlIgnore]
        public bool SiteHighestBestUseIndicatorSpecified
        {
            get { return SiteHighestBestUseIndicator != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 6)]
        public SITE_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
