namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class SITE_3_3_1
    {
        /// <summary>
        /// Gets a value indicating whether the SITE_3_3_1 container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.SiteFeaturesSpecified;
            }
        }

        /// <summary>
        /// A list of site features.
        /// </summary>
        [XmlElement("SITE_FEATURES", Order = 0)]
        public SITE_FEATURES SiteFeatures;

        /// <summary>
        /// Gets or sets a value indicating whether the SiteFeatures element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SiteFeatures element has been assigned a value.</value>
        [XmlIgnore]
        public bool SiteFeaturesSpecified
        {
            get { return this.SiteFeatures != null && this.SiteFeatures.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 1)]
        public SITE_3_3_1_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
