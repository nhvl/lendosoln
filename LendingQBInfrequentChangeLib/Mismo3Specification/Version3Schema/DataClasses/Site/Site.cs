namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class SITE
    {
        /// <summary>
        /// Gets a value indicating whether the SITE container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.OffSiteImprovementsSpecified
                    || this.SiteDetailSpecified
                    || this.SiteInfluencesSpecified
                    || this.SiteLocationsSpecified
                    || this.SiteSizeLayoutSpecified
                    || this.SiteUtilitiesSpecified
                    || this.SiteViewsSpecified
                    || this.SiteZoningSpecified;
            }
        }

        /// <summary>
        /// Off site improvements associated with the property.
        /// </summary>
        [XmlElement("OFF_SITE_IMPROVEMENTS", Order = 0)]
        public OFF_SITE_IMPROVEMENTS OffSiteImprovements;

        /// <summary>
        /// Gets or sets a value indicating whether the OffSiteImprovements element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OffSiteImprovements element has been assigned a value.</value>
        [XmlIgnore]
        public bool OffSiteImprovementsSpecified
        {
            get { return this.OffSiteImprovements != null && this.OffSiteImprovements.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details on the property.
        /// </summary>
        [XmlElement("SITE_DETAIL", Order = 1)]
        public SITE_DETAIL SiteDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the SiteDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SiteDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool SiteDetailSpecified
        {
            get { return this.SiteDetail != null && this.SiteDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Influences on the property.
        /// </summary>
        [XmlElement("SITE_INFLUENCES", Order = 2)]
        public SITE_INFLUENCES SiteInfluences;

        /// <summary>
        /// Gets or sets a value indicating whether the SiteInfluences element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SiteInfluences element has been assigned a value.</value>
        [XmlIgnore]
        public bool SiteInfluencesSpecified
        {
            get { return this.SiteInfluences != null && this.SiteInfluences.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Locations associated with the property.
        /// </summary>
        [XmlElement("SITE_LOCATIONS", Order = 3)]
        public SITE_LOCATIONS SiteLocations;

        /// <summary>
        /// Gets or sets a value indicating whether the SiteLocations element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SiteLocations element has been assigned a value.</value>
        [XmlIgnore]
        public bool SiteLocationsSpecified
        {
            get { return this.SiteLocations != null && this.SiteLocations.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Size of the property.
        /// </summary>
        [XmlElement("SITE_SIZE_LAYOUT", Order = 4)]
        public SITE_SIZE_LAYOUT SiteSizeLayout;

        /// <summary>
        /// Gets or sets a value indicating whether the SiteSizeLayout element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SiteSizeLayout element has been assigned a value.</value>
        [XmlIgnore]
        public bool SiteSizeLayoutSpecified
        {
            get { return this.SiteSizeLayout != null && this.SiteSizeLayout.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Utilities on the property.
        /// </summary>
        [XmlElement("SITE_UTILITIES", Order = 5)]
        public SITE_UTILITIES SiteUtilities;

        /// <summary>
        /// Gets or sets a value indicating whether the SiteUtilities element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SiteUtilities element has been assigned a value.</value>
        [XmlIgnore]
        public bool SiteUtilitiesSpecified
        {
            get { return this.SiteUtilities != null && this.SiteUtilities.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Views on the property.
        /// </summary>
        [XmlElement("SITE_VIEWS", Order = 6)]
        public SITE_VIEWS SiteViews;

        /// <summary>
        /// Gets or sets a value indicating whether the SiteViews element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SiteViews element has been assigned a value.</value>
        [XmlIgnore]
        public bool SiteViewsSpecified
        {
            get { return this.SiteViews != null && this.SiteViews.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Information on the zoning category.
        /// </summary>
        [XmlElement("SITE_ZONING", Order = 7)]
        public SITE_ZONING SiteZoning;

        /// <summary>
        /// Gets or sets a value indicating whether the SiteZoning element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the SiteZoning element has been assigned a value.</value>
        [XmlIgnore]
        public bool SiteZoningSpecified
        {
            get { return this.SiteZoning != null && this.SiteZoning.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 8)]
        public SITE_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
