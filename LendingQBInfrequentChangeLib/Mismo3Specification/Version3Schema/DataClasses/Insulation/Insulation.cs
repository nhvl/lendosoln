namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class INSULATION
    {
        /// <summary>
        /// Gets a value indicating whether the INSULATION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.InsulationAreasSpecified
                    || this.InsulationDetailSpecified;
            }
        }

        /// <summary>
        /// A list of insulation areas.
        /// </summary>
        [XmlElement("INSULATION_AREAS", Order = 0)]
        public INSULATION_AREAS InsulationAreas;

        /// <summary>
        /// Gets or sets a value indicating whether the InsulationAreas element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InsulationAreas element has been assigned a value.</value>
        [XmlIgnore]
        public bool InsulationAreasSpecified
        {
            get { return this.InsulationAreas != null && this.InsulationAreas.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// Details on the insulation.
        /// </summary>
        [XmlElement("INSULATION_DETAIL", Order = 1)]
        public INSULATION_DETAIL InsulationDetail;

        /// <summary>
        /// Gets or sets a value indicating whether the InsulationDetail element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InsulationDetail element has been assigned a value.</value>
        [XmlIgnore]
        public bool InsulationDetailSpecified
        {
            get { return this.InsulationDetail != null && this.InsulationDetail.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public INSULATION_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
