namespace Mismo3Specification.Version3Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the Extension element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
    public partial class INSULATION_DETAIL
    {
        /// <summary>
        /// Gets a value indicating whether the INSULATION_DETAIL container should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.EnergyEfficientItemsDescriptionSpecified
                    || this.ExtensionSpecified
                    || this.InsulationCommentDescriptionSpecified;
            }
        }

        /// <summary>
        /// A free-form text field used to describe energy efficient items.
        /// </summary>
        [XmlElement(Order = 0)]
        public MISMOString EnergyEfficientItemsDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the EnergyEfficientItemsDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EnergyEfficientItemsDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool EnergyEfficientItemsDescriptionSpecified
        {
            get { return EnergyEfficientItemsDescription != null; }
            set { }
        }

        /// <summary>
        /// A free-form text field for commenting on the presence or absence of insulation.
        /// </summary>
        [XmlElement(Order = 1)]
        public MISMOString InsulationCommentDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the InsulationCommentDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InsulationCommentDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool InsulationCommentDescriptionSpecified
        {
            get { return InsulationCommentDescription != null; }
            set { }
        }

        /// <summary>
        /// A placeholder allowing for a custom addition to this element.
        /// </summary>
        [XmlElement("EXTENSION", Order = 2)]
        public INSULATION_DETAIL_EXTENSION Extension;

        /// <summary>
        /// Gets or sets a value indicating whether the Extension element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the Extension element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return Extension != null; }
            set { }
        }
    }
}
