﻿using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
using DocMagic.Common;
namespace DocMagic.DsiDocResponse
{
    public class DocRetrieval : AbstractXmlSerializable
    {
        #region Private Member Variables
        private E_DocRetrievalPackageType m_packageType = E_DocRetrievalPackageType.Undefined;
        private string m_docCode = string.Empty;
        private string m_expirationDate = string.Empty;
        private string m_requestDate = string.Empty;
        private string m_requestTime = string.Empty;
        #endregion

        #region Public Properties
        public E_DocRetrievalPackageType PackageType
        {
            get { return m_packageType; }
            set { m_packageType = value; }
        }
        public string DocCode
        {
            get { return m_docCode; }
            set { m_docCode = value; }
        }
        public string ExpirationDate
        {
            get { return m_expirationDate; }
            set { m_expirationDate = value; }
        }
        public string RequestDate
        {
            get { return m_requestDate; }
            set { m_requestDate = value; }
        }
        public string RequestTime
        {
            get { return m_requestTime; }
            set { m_requestTime = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "DocRetrieval")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_packageType = (E_DocRetrievalPackageType)ReadAttribute(reader, "packageType", EnumMappings.E_DocRetrievalPackageType);
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "DocRetrieval") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                switch (reader.Name)
                {
                    case "DocCode":
                        DocCode = reader.ReadString();
                        break;
                    case "ExpirationDate":
                        ExpirationDate = reader.ReadString();
                        break;
                    case "RequestDate":
                        RequestDate = reader.ReadString();
                        break;
                    case "RequestTime":
                        RequestTime = reader.ReadString();
                        break;
                }
                #endregion
            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("DocRetrieval");
            WriteEnumAttribute(writer, "packageType", EnumMappings.E_DocRetrievalPackageType[(int)m_packageType]);
            WriteElementString(writer, "DocCode", m_docCode);
            WriteElementString(writer, "ExpirationDate", m_expirationDate);
            WriteElementString(writer, "RequestDate", m_requestDate);
            WriteElementString(writer, "RequestTime", m_requestTime);
            writer.WriteEndElement();
        }
    }

    public enum E_DocRetrievalPackageType
    {
        Undefined,
        FormList,
        Closing,
        Underwriting,
        Predisclosure,
        PointOfSale,
        Preclosing,
        Prequalification,
        FloodCertification,
        Redisclosure,
        ServicingTransfer,
        Adverse,
        PostClosing,
        Processing,
        LoanApplication,
        LoanModification
    }

    static partial class EnumMappings
    {
        public static readonly string[] E_DocRetrievalPackageType = {
                        ""
                         , "FormList"
                         , "Closing"
                         , "Underwriting"
                         , "Predisclosure"
                         , "PointOfSale"
                         , "Preclosing"
                         , "Prequalification"
                         , "FloodCertification"
                         , "Redisclosure"
                         , "ServicingTransfer"
                         , "Adverse"
                         , "PostClosing"
                         , "Processing"
                         , "LoanApplication"
                         , "LoanModification"
                         };
    }
}

