// Generated by CodeMonkey on 2/7/2009 12:12:42 PM
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace DocMagic.DsiDocResponse
{
    public class BuyDownPeriod : AbstractXmlSerializable
    {
        #region Private Member Variables
        private string m_term = string.Empty;
        private string m_rate = string.Empty;
        #endregion

        #region Public Properties
        public string Term
        {
            get { return m_term; }
            set { m_term = value; }
        }
        public string Rate
        {
            get { return m_rate; }
            set { m_rate = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "BuyDownPeriod")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "BuyDownPeriod") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                switch (reader.Name)
                {
                    case "Term":
                        Term = reader.ReadString();
                        break;
                    case "Rate":
                        Rate = reader.ReadString();
                        break;
                }
                #endregion
            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("BuyDownPeriod");
            WriteElementString(writer, "Term", m_term);
            WriteElementString(writer, "Rate", m_rate);
            writer.WriteEndElement();
        }
    }
}
