﻿using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
using DocMagic.Common;
namespace DocMagic.DsiDocResponse
{
    public class DocSet : AbstractXmlSerializable
    {
        #region Private Member Variables
        private DocRetrieval m_docRetrieval = null;
        private List<Document> m_documentList = null;
        private E_DocSetType m_type = E_DocSetType.Undefined;
        private E_DocSetCompressionType m_compressionType = E_DocSetCompressionType.Undefined;
        private E_DocSetBundleType m_bundleType = E_DocSetBundleType.Undefined;
        private E_YNIndicator m_includeInResponse = E_YNIndicator.Undefined;
        #endregion

        #region Public Properties
        public DocRetrieval DocRetrieval
        {
            get
            {
                if (null == m_docRetrieval) m_docRetrieval = new DocRetrieval();
                return m_docRetrieval;
            }
            set
            {
                m_docRetrieval = value;
            }
        }
        public List<Document> DocumentList
        {
            get
            {
                if (null == m_documentList) m_documentList = new List<Document>();
                return m_documentList;
            }
        }
        public E_DocSetType Type
        {
            get { return m_type; }
            set { m_type = value; }
        }
        public E_DocSetCompressionType CompressionType
        {
            get { return m_compressionType; }
            set { m_compressionType = value; }
        }
        public E_DocSetBundleType BundleType
        {
            get { return m_bundleType; }
            set { m_bundleType = value; }
        }
        public E_YNIndicator IncludeInResponse
        {
            get { return m_includeInResponse; }
            set { m_includeInResponse = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "DocSet")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_type = (E_DocSetType)ReadAttribute(reader, "type", EnumMappings.E_DocSetType);
            m_compressionType = (E_DocSetCompressionType)ReadAttribute(reader, "compressionType", EnumMappings.E_DocSetCompressionType);
            m_bundleType = (E_DocSetBundleType)ReadAttribute(reader, "bundleType", EnumMappings.E_DocSetBundleType);
            m_includeInResponse = (E_YNIndicator)ReadAttribute(reader, "includeInResponse", XmlSerializableCommon.EnumMappings.E_YNIndicator);
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "DocSet") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                switch (reader.Name)
                {
                    case "DocRetrieval":
                        ReadElement(reader, DocRetrieval);
                        break;
                    case "Document":
                        ReadElement(reader, DocumentList);
                        break;
                }
                #endregion
            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("DocSet");
            WriteEnumAttribute(writer, "type", EnumMappings.E_DocSetType[(int)m_type]);
            WriteEnumAttribute(writer, "compressionType", EnumMappings.E_DocSetCompressionType[(int)m_compressionType]);
            WriteEnumAttribute(writer, "bundleType", EnumMappings.E_DocSetBundleType[(int)m_bundleType]);
            WriteEnumAttribute(writer, "includeInResponse", XmlSerializableCommon.EnumMappings.E_YNIndicator[(int)m_includeInResponse]);
            WriteElement(writer, m_docRetrieval);
            WriteElement(writer, m_documentList);
            writer.WriteEndElement();
        }
    }

    public enum E_DocSetType
    {
        Undefined,
        PDF,
        DBK,
        PCL
    }

    public enum E_DocSetCompressionType
    {
        Undefined,
        Zip
    }

    public enum E_DocSetBundleType
    {
        Undefined,
        Set,
        Form
    }

    static partial class EnumMappings
    {
        public static readonly string[] E_DocSetType = {
                        "",
                        "PDF",
                        "DBK",
                        "PCL"
                        };

        public static readonly string[] E_DocSetCompressionType = {
                        "",
                        "Zip"
                        };

        public static readonly string[] E_DocSetBundleType = {
                        "",
                        "Set",
                        "Form"
                        };
    }

}
