// Generated by CodeMonkey on 2/7/2009 12:12:42 PM
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace DocMagic.DsiDocResponse
{
    public class Closing : AbstractXmlSerializable
    {
        #region Private Member Variables
        private E_ClosingLoanProceedsTo m_loanProceedsTo = E_ClosingLoanProceedsTo.Undefined;
        private string m_closingCounty = string.Empty;
        private string m_closingState = string.Empty;
        private List<ClosingConditions> m_closingConditionsList = null;
        #endregion

        #region Public Properties
        public E_ClosingLoanProceedsTo LoanProceedsTo
        {
            get { return m_loanProceedsTo; }
            set { m_loanProceedsTo = value; }
        }
        public string ClosingCounty
        {
            get { return m_closingCounty; }
            set { m_closingCounty = value; }
        }
        public string ClosingState
        {
            get { return m_closingState; }
            set { m_closingState = value; }
        }
        public List<ClosingConditions> ClosingConditionsList
        {
            get
            {
                if (null == m_closingConditionsList) m_closingConditionsList = new List<ClosingConditions>();
                return m_closingConditionsList;
            }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "Closing")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_loanProceedsTo = (E_ClosingLoanProceedsTo) ReadAttribute(reader, "LoanProceedsTo", EnumMappings.E_ClosingLoanProceedsTo);
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "Closing") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                switch (reader.Name)
                {
                    case "ClosingCounty":
                        ClosingCounty = reader.ReadString();
                        break;
                    case "ClosingState":
                        ClosingState = reader.ReadString();
                        break;
                    case "ClosingConditions":
                        ReadElement(reader, ClosingConditionsList);
                        break;
                }
                #endregion
            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("Closing");
            WriteEnumAttribute(writer, "LoanProceedsTo", EnumMappings.E_ClosingLoanProceedsTo[(int) m_loanProceedsTo]);
            WriteElementString(writer, "ClosingCounty", m_closingCounty);
            WriteElementString(writer, "ClosingState", m_closingState);
            WriteElement(writer, m_closingConditionsList);
            writer.WriteEndElement();
        }
    }

    public enum E_ClosingLoanProceedsTo
    {
        Undefined
        , Borrower
        , ClosingCompany
        , TitleCompany
    }

    static partial class EnumMappings
    {
        public static readonly string[] E_ClosingLoanProceedsTo = {
                        ""
                         , "Borrower"
                         , "ClosingCompany"
                         , "TitleCompany"
                         };
    }
}
