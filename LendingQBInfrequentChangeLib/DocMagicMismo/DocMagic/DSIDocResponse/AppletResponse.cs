// Generated by CodeMonkey on 2/7/2009 12:12:42 PM
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace DocMagic.DsiDocResponse
{
    public class AppletResponse : AbstractXmlSerializable
    {
        #region Private Member Variables
        private string m_appletHtml = string.Empty;
        private WebsheetInfo m_websheetInfo = null;
        #endregion

        #region Public Properties
        public string AppletHtml
        {
            get { return m_appletHtml; }
            set { m_appletHtml = value; }
        }
        public WebsheetInfo WebsheetInfo
        {
            get
            {
                if (null == m_websheetInfo) m_websheetInfo = new WebsheetInfo();
                return m_websheetInfo;
            }
            set { m_websheetInfo = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "AppletResponse")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "AppletResponse") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                switch (reader.Name)
                {
                    case "AppletHTML":
                        AppletHtml = reader.ReadString();
                        break;
                    case "WebsheetInfo":
                        ReadElement(reader, WebsheetInfo);
                        break;
                }
                #endregion
            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("AppletResponse");
            WriteElementString(writer, "AppletHTML", m_appletHtml);
            WriteElement(writer, m_websheetInfo);
            writer.WriteEndElement();
        }
    }
}
