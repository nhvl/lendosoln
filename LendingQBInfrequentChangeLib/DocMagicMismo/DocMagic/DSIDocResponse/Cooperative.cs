// Generated by CodeMonkey on 2/7/2009 12:12:42 PM
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace DocMagic.DsiDocResponse
{
    public class Cooperative : AbstractXmlSerializable
    {
        #region Private Member Variables
        private E_CooperativeFormOfOwnership m_formOfOwnership = E_CooperativeFormOfOwnership.Undefined;
        private string m_numberOfShares = string.Empty;
        private string m_leaseDate = string.Empty;
        private string m_leaseAssignDate = string.Empty;
        private string m_lienSearchDate = string.Empty;
        private string m_lienSearchNumber = string.Empty;
        private string m_stockCertificateNumber = string.Empty;
        #endregion

        #region Public Properties
        public E_CooperativeFormOfOwnership FormOfOwnership
        {
            get { return m_formOfOwnership; }
            set { m_formOfOwnership = value; }
        }
        public string NumberOfShares
        {
            get { return m_numberOfShares; }
            set { m_numberOfShares = value; }
        }
        public string LeaseDate
        {
            get { return m_leaseDate; }
            set { m_leaseDate = value; }
        }
        public string LeaseAssignDate
        {
            get { return m_leaseAssignDate; }
            set { m_leaseAssignDate = value; }
        }
        public string LienSearchDate
        {
            get { return m_lienSearchDate; }
            set { m_lienSearchDate = value; }
        }
        public string LienSearchNumber
        {
            get { return m_lienSearchNumber; }
            set { m_lienSearchNumber = value; }
        }
        public string StockCertificateNumber
        {
            get { return m_stockCertificateNumber; }
            set { m_stockCertificateNumber = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "Cooperative")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_formOfOwnership = (E_CooperativeFormOfOwnership) ReadAttribute(reader, "FormOfOwnership", EnumMappings.E_CooperativeFormOfOwnership);
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "Cooperative") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                switch (reader.Name)
                {
                    case "NumberOfShares":
                        NumberOfShares = reader.ReadString();
                        break;
                    case "LeaseDate":
                        LeaseDate = reader.ReadString();
                        break;
                    case "LeaseAssignDate":
                        LeaseAssignDate = reader.ReadString();
                        break;
                    case "LienSearchDate":
                        LienSearchDate = reader.ReadString();
                        break;
                    case "LienSearchNumber":
                        LienSearchNumber = reader.ReadString();
                        break;
                    case "StockCertificateNumber":
                        StockCertificateNumber = reader.ReadString();
                        break;
                }
                #endregion
            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("Cooperative");
            WriteEnumAttribute(writer, "FormOfOwnership", EnumMappings.E_CooperativeFormOfOwnership[(int) m_formOfOwnership]);
            WriteElementString(writer, "NumberOfShares", m_numberOfShares);
            WriteElementString(writer, "LeaseDate", m_leaseDate);
            WriteElementString(writer, "LeaseAssignDate", m_leaseAssignDate);
            WriteElementString(writer, "LienSearchDate", m_lienSearchDate);
            WriteElementString(writer, "LienSearchNumber", m_lienSearchNumber);
            WriteElementString(writer, "StockCertificateNumber", m_stockCertificateNumber);
            writer.WriteEndElement();
        }
    }

    public enum E_CooperativeFormOfOwnership
    {
        Undefined
        , OwnerInFee
        , GroundTenant
    }

    static partial class EnumMappings
    {
        public static readonly string[] E_CooperativeFormOfOwnership = {
                        ""
                         , "OwnerInFee"
                         , "GroundTenant"
                         };
    }
}
