﻿namespace DocMagic.DsiDocResponse
{
    using System;
    using System.Xml;
    using DocMagic.Common;
    using XmlSerializableCommon;

    public class EmbeddedFile : AbstractXmlSerializable
    {
        private E_EncodingType encodingType;

        public E_EncodingType EncodingType
        {
            get { return this.encodingType; }
            set { this.encodingType = value;}
        }

        public string Description { get; set; }

        public string EmbeddedFileContent { get; set; }

        public string FileName { get; set; }

        public string FileTypeIdentifier { get; set; }

        public string MIMETypeIdentifier { get; set; }

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != nameof(EmbeddedFile))
            {
                reader.Read();

                if (reader.EOF)
                {
                    return;
                }
            }

            this.encodingType = (E_EncodingType)this.ReadAttribute(reader, nameof(this.encodingType), EnumMappings.E_EncodingType);

            if (reader.IsEmptyElement)
            {
                return;
            }

            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == nameof(EmbeddedFile))
                {
                    return;
                }

                if (reader.NodeType != XmlNodeType.Element && reader.NodeType != XmlNodeType.CDATA)
                {
                    continue;
                }

                switch (reader.Name)
                {
                    case nameof(this.Description):
                        this.Description = reader.ReadString();
                        break;
                    case nameof(this.EmbeddedFileContent):
                        this.EmbeddedFileContent = reader.ReadString();
                        break;
                    case nameof(this.FileName):
                        this.FileName = reader.ReadString();
                        break;
                    case nameof(this.FileTypeIdentifier):
                        this.FileTypeIdentifier = reader.ReadString();
                        break;
                    case nameof(this.MIMETypeIdentifier):
                        this.MIMETypeIdentifier = reader.ReadString();
                        break;
                    default:
                        throw new ArgumentException($"Element {reader.Name} is not handled.");
                }
            }
        }

        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement(nameof(EmbeddedFile));
            this.WriteAttribute(writer, nameof(this.encodingType), EnumMappings.E_EncodingType[(int)this.EncodingType]);
            this.WriteElementString(writer, nameof(this.Description), this.Description);
            this.WriteElementString(writer, nameof(this.EmbeddedFileContent), this.EmbeddedFileContent);
            this.WriteElementString(writer, nameof(this.FileName), this.FileName);
            this.WriteElementString(writer, nameof(this.FileTypeIdentifier), this.FileTypeIdentifier);
            this.WriteElementString(writer, nameof(this.MIMETypeIdentifier), this.MIMETypeIdentifier);
            writer.WriteEndElement();
        }
    }

    static partial class EnumMappings
    {
        public static readonly string[] E_EncodingType ={
            string.Empty,
            "None",
            "Base64"
        };
    }
}
