// Generated by CodeMonkey on 2/7/2009 12:12:42 PM
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace DocMagic.DsiDocResponse
{
    public class AlternateDelivery : AbstractXmlSerializable
    {
        #region Private Member Variables
        private string m_name = string.Empty;
        private string m_address = string.Empty;
        private string m_city = string.Empty;
        private string m_state = string.Empty;
        private string m_zip = string.Empty;
        private string m_attention = string.Empty;
        #endregion

        #region Public Properties
        public string Name
        {
            get { return m_name; }
            set { m_name = value; }
        }
        public string Address
        {
            get { return m_address; }
            set { m_address = value; }
        }
        public string City
        {
            get { return m_city; }
            set { m_city = value; }
        }
        public string State
        {
            get { return m_state; }
            set { m_state = value; }
        }
        public string Zip
        {
            get { return m_zip; }
            set { m_zip = value; }
        }
        public string Attention
        {
            get { return m_attention; }
            set { m_attention = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "AlternateDelivery")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "AlternateDelivery") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                switch (reader.Name)
                {
                    case "Name":
                        Name = reader.ReadString();
                        break;
                    case "Address":
                        Address = reader.ReadString();
                        break;
                    case "City":
                        City = reader.ReadString();
                        break;
                    case "State":
                        State = reader.ReadString();
                        break;
                    case "Zip":
                        Zip = reader.ReadString();
                        break;
                    case "Attention":
                        Attention = reader.ReadString();
                        break;
                }
                #endregion
            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("AlternateDelivery");
            WriteElementString(writer, "Name", m_name);
            WriteElementString(writer, "Address", m_address);
            WriteElementString(writer, "City", m_city);
            WriteElementString(writer, "State", m_state);
            WriteElementString(writer, "Zip", m_zip);
            WriteElementString(writer, "Attention", m_attention);
            writer.WriteEndElement();
        }
    }
}
