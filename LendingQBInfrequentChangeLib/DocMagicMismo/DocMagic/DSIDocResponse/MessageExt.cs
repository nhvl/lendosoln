﻿
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace DocMagic.DsiDocResponse
{
    public class MessageExt : AbstractXmlSerializable
    {
        #region Private Member Variables
        private string m_id = string.Empty;
        private AuditInfo m_auditInfo = null;
        #endregion

        #region Public Properties
        public string Id
        {
            get { return m_id; }
            set { m_id = value; }
        }
        public AuditInfo AuditInfo
        {
            get
            {
                if (null == m_auditInfo) m_auditInfo = new AuditInfo();
                return m_auditInfo;
            }
            set { m_auditInfo = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "MessageExt")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_id = ReadAttribute(reader, "id");
            #endregion

            if (reader.IsEmptyElement) return;

            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "MessageExt") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                switch (reader.Name)
                {
                    case "AuditInfo":
                        ReadElement(reader, AuditInfo);
                        break;
                }
                #endregion
            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("MessageExt");
            WriteAttribute(writer, "id", m_id);
            WriteElement(writer, m_auditInfo);
            writer.WriteEndElement();
        }
    }
}
