﻿using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace DocMagic.DsiDocResponse
{
    public class EmbeddedContent : AbstractXmlSerializable
    {
        #region Private Member Variables
        private string m_chkSum = string.Empty;
        private string m_innerText = string.Empty;
        #endregion

        #region Public Properties
        public string chkSum
        {
            get { return m_chkSum; }
            set { m_chkSum = value; }
        }
        public string InnerText
        {
            get { return m_innerText; }
            set { m_innerText = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "EmbeddedContent")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_chkSum = ReadAttribute(reader, "chkSum");
            #endregion

            if (reader.IsEmptyElement) return;
            m_innerText = reader.ReadString();
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("EmbeddedContent");
            WriteAttribute(writer, "chkSum", m_chkSum);
            WriteString(writer, m_innerText);
            writer.WriteEndElement();
        }
    }
}
