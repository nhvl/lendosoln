﻿namespace DocMagic.DsiDocResponse
{
    using System;
    using System.Xml;
    using XmlSerializableCommon;

    public class DataServiceResponse : AbstractXmlSerializable
    {
        private DataServicePackage dataServicePackage;

        private string docCode;

        public DataServicePackage DataServicePackage
        {
            get
            {
                if (this.dataServicePackage == null)
                {
                    this.dataServicePackage = new DataServicePackage();
                }

                return this.dataServicePackage;
            }

            set
            {
                this.dataServicePackage = value;
            }
        }

        public string DocCode
        {
            get { return this.docCode; }
            set { this.docCode = value; }
        }

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != nameof(DataServiceResponse))
            {
                reader.Read();

                if (reader.EOF)
                {
                    return;
                }
            }

            if (reader.IsEmptyElement)
            {
                return;
            }

            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == nameof(DataServiceResponse))
                {
                    return;
                }

                if (reader.NodeType != XmlNodeType.Element)
                {
                    continue;
                }

                switch (reader.Name)
                {
                    case nameof(this.DataServicePackage):
                        this.ReadElement(reader, this.DataServicePackage);
                        break;
                    case nameof(this.DocCode):
                        this.DocCode = reader.ReadString();
                        break;
                    default:
                        throw new ArgumentException($"Element {reader.Name} is not handled.");
                }
            }
        }

        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement(nameof(DataServiceResponse));
            this.WriteElement(writer, this.DataServicePackage);
            this.WriteElementString(writer, nameof(this.DocCode), this.DocCode);
            writer.WriteEndElement();
        }
    }
}
