﻿using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;

namespace DocMagic.DsiDocResponse
{
    public class Format : AbstractXmlSerializable
    {
        #region Private Member Variables
        private Form m_form = null;
        private string m_description = string.Empty;
        #endregion

        #region Public Properties
        public Form Form
        {
            get
            {
                if (null == m_form) m_form = new Form();
                return m_form;
            }
            set { m_form = value; }
        }
        public string Description
        {
            get { return m_description; }
            set { m_description = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "Format")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "Format") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                switch (reader.Name)
                {
                    case "Form":
                        ReadElement(reader, Form);
                        break;
                    case "Description":
                        Description = reader.ReadString();
                        break;
                }
                #endregion
            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("FormRequest");
            WriteElement(writer, m_form);
            WriteElementString(writer, "Description", m_description);
            writer.WriteEndElement();
        }
    }
}
