// Generated by CodeMonkey on 2/7/2009 12:12:42 PM
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace DocMagic.DsiDocResponse
{
    public class ServiceProviderResponse : AbstractXmlSerializable
    {
        #region Private Member Variables
        private E_ServiceProviderResponseType m_type = E_ServiceProviderResponseType.Undefined;
        private List<ServiceProvider> m_serviceProviderList = null;
        #endregion

        #region Public Properties
        public E_ServiceProviderResponseType Type
        {
            get { return m_type; }
            set { m_type = value; }
        }
        public List<ServiceProvider> ServiceProviderList
        {
            get
            {
                if (null == m_serviceProviderList) m_serviceProviderList = new List<ServiceProvider>();
                return m_serviceProviderList;
            }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "ServiceProviderResponse")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_type = (E_ServiceProviderResponseType) ReadAttribute(reader, "type", EnumMappings.E_ServiceProviderResponseType);
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "ServiceProviderResponse") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                switch (reader.Name)
                {
                    case "ServiceProvider":
                        ReadElement(reader, ServiceProviderList);
                        break;
                }
                #endregion
            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("ServiceProviderResponse");
            WriteEnumAttribute(writer, "type", EnumMappings.E_ServiceProviderResponseType[(int) m_type]);
            WriteElement(writer, m_serviceProviderList);
            writer.WriteEndElement();
        }
    }

    public enum E_ServiceProviderResponseType
    {
        Undefined
        , Load
        , Save
        , Delete
    }

    static partial class EnumMappings
    {
        public static readonly string[] E_ServiceProviderResponseType = {
                        ""
                         , "Load"
                         , "Save"
                         , "Delete"
                         };
    }
}
