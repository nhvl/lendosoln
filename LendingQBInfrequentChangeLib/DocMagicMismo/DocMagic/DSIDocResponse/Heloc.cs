// Generated by CodeMonkey on 2/7/2009 12:12:42 PM
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace DocMagic.DsiDocResponse
{
    public class Heloc : AbstractXmlSerializable
    {
        #region Private Member Variables
        private string m_minPayment = string.Empty;
        #endregion

        #region Public Properties
        public string MinPayment
        {
            get { return m_minPayment; }
            set { m_minPayment = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "Heloc")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "Heloc") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                switch (reader.Name)
                {
                    case "MinPayment":
                        MinPayment = reader.ReadString();
                        break;
                }
                #endregion
            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("Heloc");
            WriteElementString(writer, "MinPayment", m_minPayment);
            writer.WriteEndElement();
        }
    }
}
