﻿using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;

namespace DocMagic.DsiDocResponse
{
    public class Form : AbstractXmlSerializable
    {
        #region Private Member Variables
        private string m_borrowerNumber = string.Empty;
        private string m_innerText = string.Empty;
        #endregion

        #region Public Properties
        public string BorrowerNumber
        {
            get { return m_borrowerNumber; }
            set { m_borrowerNumber = value; }
        }
        public string InnerText
        {
            get { return m_innerText; }
            set { m_innerText = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "Form")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_borrowerNumber = ReadAttribute(reader, "BorrowerNumber");
            #endregion

            if (reader.IsEmptyElement) return;
            m_innerText = reader.ReadString();
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("FormRequest");
            WriteAttribute(writer, "BorrowerNumber", m_borrowerNumber);
            WriteString(writer, m_innerText);
            writer.WriteEndElement();
        }
    }
}
