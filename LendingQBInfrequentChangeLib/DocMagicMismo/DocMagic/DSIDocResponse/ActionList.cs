// Generated by CodeMonkey on 2/7/2009 12:12:42 PM
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace DocMagic.DsiDocResponse
{
    public class ActionList : AbstractXmlSerializable
    {
        #region Private Member Variables
        private List<Action> m__ActionList = null;
        #endregion

        #region Public Properties
        public List<Action> _ActionList
        {
            get
            {
                if (null == m__ActionList) m__ActionList = new List<Action>();
                return m__ActionList;
            }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "ActionList")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "ActionList") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                switch (reader.Name)
                {
                    case "Action":
                        ReadElement(reader, _ActionList);
                        break;
                }
                #endregion
            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("ActionList");
            WriteElement(writer, m__ActionList);
            writer.WriteEndElement();
        }
    }
}
