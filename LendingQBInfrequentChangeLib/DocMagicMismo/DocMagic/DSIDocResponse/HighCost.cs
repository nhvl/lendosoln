// Generated by CodeMonkey on 2/7/2009 12:12:42 PM
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace DocMagic.DsiDocResponse
{
    public class HighCost : AbstractXmlSerializable
    {
        #region Private Member Variables
        private E_YesNo m_usePriorPrepaymentAmount = E_YesNo.Undefined;
        private string m_effectiveDate = string.Empty;
        #endregion

        #region Public Properties
        public E_YesNo UsePriorPrepaymentAmount
        {
            get { return m_usePriorPrepaymentAmount; }
            set { m_usePriorPrepaymentAmount = value; }
        }
        public string EffectiveDate
        {
            get { return m_effectiveDate; }
            set { m_effectiveDate = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "HighCost")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_usePriorPrepaymentAmount = (E_YesNo) ReadAttribute(reader, "UsePriorPrepaymentAmount", XmlSerializableCommon.EnumMappings.E_YesNo);
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "HighCost") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                switch (reader.Name)
                {
                    case "EffectiveDate":
                        EffectiveDate = reader.ReadString();
                        break;
                }
                #endregion
            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("HighCost");
            WriteEnumAttribute(writer, "UsePriorPrepaymentAmount", XmlSerializableCommon.EnumMappings.E_YesNo[(int) m_usePriorPrepaymentAmount]);
            WriteElementString(writer, "EffectiveDate", m_effectiveDate);
            writer.WriteEndElement();
        }
    }
}
