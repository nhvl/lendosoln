// Generated by CodeMonkey on 2/7/2009 12:12:42 PM
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace DocMagic.DsiDocResponse
{
    public class Prelim : AbstractXmlSerializable
    {
        #region Private Member Variables
        private E_PrelimLegalDescriptionAttached m_legalDescriptionAttached = E_PrelimLegalDescriptionAttached.Undefined;
        private List<string> m_legalDescriptionList = null;
        private List<string> m_mineralRightsDescriptionList = null;
        private string m_titleReportDate = string.Empty;
        private string m_parcelNumber = string.Empty;
        private string m_taxMessage = string.Empty;
        private string m_specialEndorsements = string.Empty;
        private string m_approvedItems = string.Empty;
        #endregion

        #region Public Properties
        public E_PrelimLegalDescriptionAttached LegalDescriptionAttached
        {
            get { return m_legalDescriptionAttached; }
            set { m_legalDescriptionAttached = value; }
        }
        public List<string> LegalDescriptionList
        {
            get
            {
                if (null == m_legalDescriptionList) m_legalDescriptionList = new List<string>();
                return m_legalDescriptionList;
            }
        }
        public List<string> MineralRightsDescriptionList
        {
            get
            {
                if (null == m_mineralRightsDescriptionList) m_mineralRightsDescriptionList = new List<string>();
                return m_mineralRightsDescriptionList;
            }
        }
        public string TitleReportDate
        {
            get { return m_titleReportDate; }
            set { m_titleReportDate = value; }
        }
        public string ParcelNumber
        {
            get { return m_parcelNumber; }
            set { m_parcelNumber = value; }
        }
        public string TaxMessage
        {
            get { return m_taxMessage; }
            set { m_taxMessage = value; }
        }
        public string SpecialEndorsements
        {
            get { return m_specialEndorsements; }
            set { m_specialEndorsements = value; }
        }
        public string ApprovedItems
        {
            get { return m_approvedItems; }
            set { m_approvedItems = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "Prelim")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_legalDescriptionAttached = (E_PrelimLegalDescriptionAttached) ReadAttribute(reader, "LegalDescriptionAttached", EnumMappings.E_PrelimLegalDescriptionAttached);
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "Prelim") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                switch (reader.Name)
                {
                    case "LegalDescription":
                        LegalDescriptionList.Add(reader.ReadString());
                        break;
                    case "MineralRightsDescription":
                        MineralRightsDescriptionList.Add(reader.ReadString());
                        break;
                    case "TitleReportDate":
                        TitleReportDate = reader.ReadString();
                        break;
                    case "ParcelNumber":
                        ParcelNumber = reader.ReadString();
                        break;
                    case "TaxMessage":
                        TaxMessage = reader.ReadString();
                        break;
                    case "SpecialEndorsements":
                        SpecialEndorsements = reader.ReadString();
                        break;
                    case "ApprovedItems":
                        ApprovedItems = reader.ReadString();
                        break;
                }
                #endregion
            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("Prelim");
            WriteEnumAttribute(writer, "LegalDescriptionAttached", EnumMappings.E_PrelimLegalDescriptionAttached[(int) m_legalDescriptionAttached]);
            WriteElementString(writer, "LegalDescription", m_legalDescriptionList);
            WriteElementString(writer, "MineralRightsDescription", m_mineralRightsDescriptionList);
            WriteElementString(writer, "TitleReportDate", m_titleReportDate);
            WriteElementString(writer, "ParcelNumber", m_parcelNumber);
            WriteElementString(writer, "TaxMessage", m_taxMessage);
            WriteElementString(writer, "SpecialEndorsements", m_specialEndorsements);
            WriteElementString(writer, "ApprovedItems", m_approvedItems);
            writer.WriteEndElement();
        }
    }

    public enum E_PrelimLegalDescriptionAttached
    {
        Undefined
        , No
        , Yes
    }

    static partial class EnumMappings
    {
        public static readonly string[] E_PrelimLegalDescriptionAttached = {
                        ""
                         , "No"
                         , "Yes"
                         };
    }
}
