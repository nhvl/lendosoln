﻿using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace DocMagic.DsiDocResponse
{
    public class AuditInfo : AbstractXmlSerializable
    {
        #region Private Member Variables
        private string m_url = string.Empty;
        private string m_class = string.Empty;
        private List<string> m_auditSubClassList = null;
        #endregion

        #region Public Properties
        public string Url
        {
            get { return m_url; }
            set { m_url = value; }
        }
        public string Class
        {
            get { return m_class; }
            set { m_class = value; }
        }
        public List<string> AuditSubClassList
        {
            get
            {
                if (null == m_auditSubClassList) m_auditSubClassList = new List<string>();
                return m_auditSubClassList;
            }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "AuditInfo")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_url = ReadAttribute(reader, "URL");
            m_class = ReadAttribute(reader, "class");
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "AuditInfo") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                switch (reader.Name)
                {
                    case "AuditSubClass":
                        AuditSubClassList.Add(reader.ReadString());
                        break;
                }
                #endregion
            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("AuditInfo");
            WriteAttribute(writer, "URL", m_url);
            WriteAttribute(writer, "class", m_class);
            foreach (string auditSubClass in m_auditSubClassList)
                WriteElementString(writer, "AuditSubClass", auditSubClass);
            writer.WriteEndElement();
        }
    }
}
