﻿namespace DocMagic.DsiDocResponse
{
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using XmlSerializableCommon;

    public class DataServicePackage : AbstractXmlSerializable
    {
        private List<EmbeddedFile> embeddedFileList;

        public List<EmbeddedFile> EmbeddedFileList
        {
            get
            {
                if (this.embeddedFileList == null)
                {
                    this.embeddedFileList = new List<EmbeddedFile>();
                }

                return this.embeddedFileList;
            }

            set
            {
                this.embeddedFileList = value;
            }
        }

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != nameof(DataServicePackage))
            {
                reader.Read();

                if (reader.EOF)
                {
                    return;
                }
            }

            if (reader.IsEmptyElement)
            {
                return;
            }

            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == nameof(DataServicePackage))
                {
                    return;
                }

                if (reader.NodeType != XmlNodeType.Element)
                {
                    continue;
                }

                switch (reader.Name)
                {
                    case nameof(EmbeddedFile):
                        this.ReadElement(reader, this.EmbeddedFileList);
                        break;
                    default:
                        throw new ArgumentException($"Element {reader.Name} is not handled.");
                }
            }
        }

        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement(nameof(DataServicePackage));
            this.WriteElement(writer, this.EmbeddedFileList);
            writer.WriteEndElement();
        }
    }
}
