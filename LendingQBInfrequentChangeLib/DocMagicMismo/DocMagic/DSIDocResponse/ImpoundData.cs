// Generated by CodeMonkey on 2/7/2009 12:12:42 PM
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace DocMagic.DsiDocResponse
{
    public class ImpoundData : AbstractXmlSerializable
    {
        #region Private Member Variables
        private E_ImpoundDataImpoundsPaidBy m_impoundsPaidBy = E_ImpoundDataImpoundsPaidBy.Undefined;
        private E_ImpoundDataPmiPaidBy m_pmiPaidBy = E_ImpoundDataPmiPaidBy.Borrower;
        private string m_pmi1stYearPremiumRate = string.Empty;
        private string m_pmi1stYearPremium = string.Empty;
        private string m_miRenewalRate1 = string.Empty;
        private string m_miRenewalTerm1 = string.Empty;
        private string m_miRenewalRate2 = string.Empty;
        private string m_miRenewalTerm2 = string.Empty;
        private string m_pmimmiMonthlyAmount = string.Empty;
        private string m_pmimmiDueDate = string.Empty;
        private string m_pmimmiMonths = string.Empty;
        private string m_cushionAmount = string.Empty;
        private string m_cushionMonthsCount = string.Empty;
        private string m_aggregateAdjustment = string.Empty;
        #endregion

        #region Public Properties
        public E_ImpoundDataImpoundsPaidBy ImpoundsPaidBy
        {
            get { return m_impoundsPaidBy; }
            set { m_impoundsPaidBy = value; }
        }
        public E_ImpoundDataPmiPaidBy PmiPaidBy
        {
            get { return m_pmiPaidBy; }
            set { m_pmiPaidBy = value; }
        }
        public string Pmi1stYearPremiumRate
        {
            get { return m_pmi1stYearPremiumRate; }
            set { m_pmi1stYearPremiumRate = value; }
        }
        public string Pmi1stYearPremium
        {
            get { return m_pmi1stYearPremium; }
            set { m_pmi1stYearPremium = value; }
        }
        public string MiRenewalRate1
        {
            get { return m_miRenewalRate1; }
            set { m_miRenewalRate1 = value; }
        }
        public string MiRenewalTerm1
        {
            get { return m_miRenewalTerm1; }
            set { m_miRenewalTerm1 = value; }
        }
        public string MiRenewalRate2
        {
            get { return m_miRenewalRate2; }
            set { m_miRenewalRate2 = value; }
        }
        public string MiRenewalTerm2
        {
            get { return m_miRenewalTerm2; }
            set { m_miRenewalTerm2 = value; }
        }
        public string PmimmiMonthlyAmount
        {
            get { return m_pmimmiMonthlyAmount; }
            set { m_pmimmiMonthlyAmount = value; }
        }
        public string PmimmiDueDate
        {
            get { return m_pmimmiDueDate; }
            set { m_pmimmiDueDate = value; }
        }
        public string PmimmiMonths
        {
            get { return m_pmimmiMonths; }
            set { m_pmimmiMonths = value; }
        }
        public string CushionAmount
        {
            get { return m_cushionAmount; }
            set { m_cushionAmount = value; }
        }
        public string CushionMonthsCount
        {
            get { return m_cushionMonthsCount; }
            set { m_cushionMonthsCount = value; }
        }
        public string AggregateAdjustment
        {
            get { return m_aggregateAdjustment; }
            set { m_aggregateAdjustment = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "ImpoundData")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_impoundsPaidBy = (E_ImpoundDataImpoundsPaidBy) ReadAttribute(reader, "ImpoundsPaidBy", EnumMappings.E_ImpoundDataImpoundsPaidBy);
            m_pmiPaidBy = (E_ImpoundDataPmiPaidBy) ReadAttribute(reader, "PMIPaidBy", EnumMappings.E_ImpoundDataPmiPaidBy);
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "ImpoundData") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                switch (reader.Name)
                {
                    case "PMI1stYearPremiumRate":
                        Pmi1stYearPremiumRate = reader.ReadString();
                        break;
                    case "PMI1stYearPremium":
                        Pmi1stYearPremium = reader.ReadString();
                        break;
                    case "MIRenewalRate1":
                        MiRenewalRate1 = reader.ReadString();
                        break;
                    case "MIRenewalTerm1":
                        MiRenewalTerm1 = reader.ReadString();
                        break;
                    case "MIRenewalRate2":
                        MiRenewalRate2 = reader.ReadString();
                        break;
                    case "MIRenewalTerm2":
                        MiRenewalTerm2 = reader.ReadString();
                        break;
                    case "PMIMMIMonthlyAmount":
                        PmimmiMonthlyAmount = reader.ReadString();
                        break;
                    case "PMIMMIDueDate":
                        PmimmiDueDate = reader.ReadString();
                        break;
                    case "PMIMMIMonths":
                        PmimmiMonths = reader.ReadString();
                        break;
                    case "CushionAmount":
                        CushionAmount = reader.ReadString();
                        break;
                    case "CushionMonthsCount":
                        CushionMonthsCount = reader.ReadString();
                        break;
                    case "AggregateAdjustment":
                        AggregateAdjustment = reader.ReadString();
                        break;
                }
                #endregion
            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("ImpoundData");
            WriteEnumAttribute(writer, "ImpoundsPaidBy", EnumMappings.E_ImpoundDataImpoundsPaidBy[(int) m_impoundsPaidBy]);
            WriteEnumAttribute(writer, "PMIPaidBy", EnumMappings.E_ImpoundDataPmiPaidBy[(int) m_pmiPaidBy]);
            WriteElementString(writer, "PMI1stYearPremiumRate", m_pmi1stYearPremiumRate);
            WriteElementString(writer, "PMI1stYearPremium", m_pmi1stYearPremium);
            WriteElementString(writer, "MIRenewalRate1", m_miRenewalRate1);
            WriteElementString(writer, "MIRenewalTerm1", m_miRenewalTerm1);
            WriteElementString(writer, "MIRenewalRate2", m_miRenewalRate2);
            WriteElementString(writer, "MIRenewalTerm2", m_miRenewalTerm2);
            WriteElementString(writer, "PMIMMIMonthlyAmount", m_pmimmiMonthlyAmount);
            WriteElementString(writer, "PMIMMIDueDate", m_pmimmiDueDate);
            WriteElementString(writer, "PMIMMIMonths", m_pmimmiMonths);
            WriteElementString(writer, "CushionAmount", m_cushionAmount);
            WriteElementString(writer, "CushionMonthsCount", m_cushionMonthsCount);
            WriteElementString(writer, "AggregateAdjustment", m_aggregateAdjustment);
            writer.WriteEndElement();
        }
    }

    public enum E_ImpoundDataImpoundsPaidBy
    {
        Undefined
        , Borrower
        , Seller
        , Broker
        , Lender
        , Investor
        , Other
    }

    public enum E_ImpoundDataPmiPaidBy
    {
        Undefined
        , Borrower
        , Seller
        , Broker
        , Lender
        , Investor
        , Other
    }

    static partial class EnumMappings
    {
        public static readonly string[] E_ImpoundDataImpoundsPaidBy = {
                        ""
                         , "Borrower"
                         , "Seller"
                         , "Broker"
                         , "Lender"
                         , "Investor"
                         , "Other"
                         };
        public static readonly string[] E_ImpoundDataPmiPaidBy = {
                        ""
                         , "Borrower"
                         , "Seller"
                         , "Broker"
                         , "Lender"
                         , "Investor"
                         , "Other"
                         };
    }
}
