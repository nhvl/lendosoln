﻿using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;

namespace DocMagic.DsiDocResponse
{
    public class FormResponse : AbstractXmlSerializable
    {
        #region Private Member Variables
        private E_FormResponseType m_type = E_FormResponseType.Undefined;
        private List<Format> m_formatList = null;
        private string m_websheetNumber = string.Empty;
        #endregion

        #region Public Properties
        public E_FormResponseType Type
        {
            get { return m_type; }
            set { m_type = value; }
        }
        public List<Format> FormatList
        {
            get
            {
                if (null == m_formatList) m_formatList = new List<Format>();
                return m_formatList;
            }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "FormResponse")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_type = (E_FormResponseType)ReadAttribute(reader, "type", EnumMappings.E_FormResponseType);
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "FormResponse") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                switch (reader.Name)
                {
                    case "Format":
                        ReadElement(reader, FormatList);
                        break;
                }
                #endregion
            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("FormRequest");
            WriteEnumAttribute(writer, "type", EnumMappings.E_FormResponseType[(int)m_type]);
            WriteElement(writer, m_formatList);
            writer.WriteEndElement();
        }
    }

    public enum E_FormResponseType
    {
        Undefined
        , List
    }

    static partial class EnumMappings
    {
        public static readonly string[] E_FormResponseType = {
            ""
            , "List"
            };
    }
}
