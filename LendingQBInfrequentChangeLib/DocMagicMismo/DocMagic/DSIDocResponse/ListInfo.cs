// Generated by CodeMonkey on 2/7/2009 12:12:42 PM
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace DocMagic.DsiDocResponse
{
    public class ListInfo : AbstractXmlSerializable
    {
        #region Private Member Variables
        private E_ListInfoType m_type = E_ListInfoType.Undefined;
        private List<string> m_valueList = null;
        #endregion

        #region Public Properties
        public E_ListInfoType Type
        {
            get { return m_type; }
            set { m_type = value; }
        }
        public List<string> ValueList
        {
            get
            {
                if (null == m_valueList) m_valueList = new List<string>();
                return m_valueList;
            }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "ListInfo")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_type = (E_ListInfoType) ReadAttribute(reader, "type", EnumMappings.E_ListInfoType);
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "ListInfo") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                switch (reader.Name)
                {
                    case "Value":
                        ValueList.Add(reader.ReadString());
                        break;
                }
                #endregion
            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("ListInfo");
            WriteEnumAttribute(writer, "type", EnumMappings.E_ListInfoType[(int) m_type]);
            WriteElementString(writer, "Value", m_valueList);
            writer.WriteEndElement();
        }
    }

    public enum E_ListInfoType
    {
        Undefined
        , Count
    }

    static partial class EnumMappings
    {
        public static readonly string[] E_ListInfoType = {
                        ""
                         , "Count"
                         };
    }
}
