// Generated by CodeMonkey on 2/7/2009 12:12:42 PM
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace DocMagic.DsiDocResponse
{
    public class Disbursement : AbstractXmlSerializable
    {
        #region Private Member Variables
        private string m_amount = string.Empty;
        private string m_dueDate = string.Empty;
        #endregion

        #region Public Properties
        public string Amount
        {
            get { return m_amount; }
            set { m_amount = value; }
        }
        public string DueDate
        {
            get { return m_dueDate; }
            set { m_dueDate = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "Disbursement")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "Disbursement") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                switch (reader.Name)
                {
                    case "Amount":
                        Amount = reader.ReadString();
                        break;
                    case "DueDate":
                        DueDate = reader.ReadString();
                        break;
                }
                #endregion
            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("Disbursement");
            WriteElementString(writer, "Amount", m_amount);
            WriteElementString(writer, "DueDate", m_dueDate);
            writer.WriteEndElement();
        }
    }
}
