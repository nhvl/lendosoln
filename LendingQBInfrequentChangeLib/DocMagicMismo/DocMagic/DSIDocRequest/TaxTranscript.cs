﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XmlSerializableCommon;
using System.Xml;

namespace DocMagic.DsiDocRequest
{
    public class TaxTranscript : AbstractXmlSerializable
    {
        public string BorrowerID { get; set; }
        public E_YNIndicator ReturnTranscriptIndicator { get; set; }
        public E_YNIndicator AccountTranscriptIndicator { get; set; }
        public E_YNIndicator RecordOfAccountIndicator { get; set; }
        public E_YNIndicator VerificationOfNonfilingIndicator { get; set; }
        public E_YNIndicator FormSeriesTranscriptIndicator { get; set; }
        public E_YNIndicator IdentityTheftIndicator { get; set; }

        public string TaxFormId { get; set; }

        private List<string> m_taxPeriodDateList = null;
        private List<PreviousFilingAddress> m_previousFilingAddressList = null;

        public List<string> TaxPeriodDateList
        {
            get
            {
                if (null == m_taxPeriodDateList)
                {
                    m_taxPeriodDateList = new List<string>();
                }
                return m_taxPeriodDateList;
            }
        }
        public List<PreviousFilingAddress> PreviousFilingAddressList
        {
            get
            {
                if (null == m_previousFilingAddressList)
                {
                    m_previousFilingAddressList = new List<PreviousFilingAddress>();
                }
                return m_previousFilingAddressList;
            }
        }
        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "TaxTranscript")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            BorrowerID = ReadAttribute(reader, "BorrowerID");
            ReturnTranscriptIndicator = (E_YNIndicator)ReadAttribute(reader, "ReturnTranscriptIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator);
            AccountTranscriptIndicator = (E_YNIndicator)ReadAttribute(reader, "AccountTranscriptIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator);
            RecordOfAccountIndicator = (E_YNIndicator)ReadAttribute(reader, "RecordOfAccountIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator);
            VerificationOfNonfilingIndicator = (E_YNIndicator)ReadAttribute(reader, "VerificationOfNonfilingIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator);
            FormSeriesTranscriptIndicator = (E_YNIndicator)ReadAttribute(reader, "FormSeriesTranscriptIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator);
            IdentityTheftIndicator = (E_YNIndicator)ReadAttribute(reader, "IdentityTheftIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator);

            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "TaxTranscript") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                switch (reader.Name)
                {
                    case "TaxFormId":
                        TaxFormId = reader.ReadString();
                        break;
                    case "TaxPeriodDate":
                        TaxPeriodDateList.Add(reader.ReadString());
                        break;
                    case "PreviousFilingAddress":
                        ReadElement(reader, PreviousFilingAddressList);
                        break;
                }
                #endregion
            }
        }

        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("TaxTranscript");
            WriteAttribute(writer, "BorrowerID", BorrowerID);
            WriteEnumAttribute(writer, "ReturnTranscriptIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator[(int)ReturnTranscriptIndicator]);
            WriteEnumAttribute(writer, "AccountTranscriptIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator[(int)AccountTranscriptIndicator]);
            WriteEnumAttribute(writer, "RecordOfAccountIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator[(int)RecordOfAccountIndicator]);
            WriteEnumAttribute(writer, "VerificationOfNonfilingIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator[(int)VerificationOfNonfilingIndicator]);
            WriteEnumAttribute(writer, "FormSeriesTranscriptIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator[(int)FormSeriesTranscriptIndicator]);
            WriteEnumAttribute(writer, "IdentityTheftIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator[(int)IdentityTheftIndicator]);
            WriteElementString(writer, "TaxFormId", TaxFormId);
            WriteElementString(writer, "TaxPeriodDate", m_taxPeriodDateList);
            WriteElement(writer, m_previousFilingAddressList);
            writer.WriteEndElement();
        }
    }
}
