﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using XmlSerializableCommon;
namespace DocMagic.DsiDocRequest
{
    public class LoanOptionsDisclosureDetail : AbstractXmlSerializable
    {
        public string CompensationPlanMinimumAmount { get; set; }
        public string CompensationPlanMaximumAmount { get; set; }

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "LoanOptionsDisclosureDetail")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "LoanOptionsDisclosureDetail") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                switch (reader.Name)
                {
                    case "CompensationPlanMinimumAmount":
                        CompensationPlanMinimumAmount = reader.ReadString();
                        break;
                    case "CompensationPlanMaximumAmount":
                        CompensationPlanMaximumAmount = reader.ReadString();
                        break;

                }
                #endregion
            }
        }

        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("LoanOptionsDisclosureDetail");
            WriteElementString(writer, "CompensationPlanMinimumAmount", CompensationPlanMinimumAmount);
            WriteElementString(writer, "CompensationPlanMaximumAmount", CompensationPlanMaximumAmount);
            writer.WriteEndElement();
        }
    }
}
