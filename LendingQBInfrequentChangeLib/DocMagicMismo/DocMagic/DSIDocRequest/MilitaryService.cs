﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XmlSerializableCommon;
using System.Xml;

namespace DocMagic.DsiDocRequest
{
    public class MilitaryService : AbstractXmlSerializable
    {
        public string MilitaryServiceID { get; set; }
        public string MilitaryServiceType { get; set; }

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "MilitaryService")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            MilitaryServiceID = ReadAttribute(reader, "MilitaryServiceID");
            MilitaryServiceType = ReadAttribute(reader, "MilitaryServiceType");
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "MilitaryService") return;
            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("MilitaryService");
            WriteAttribute(writer, "MilitaryServiceID", MilitaryServiceID);
            if(!string.IsNullOrEmpty(MilitaryServiceType))
                writer.WriteAttributeString("MilitaryServiceType", MilitaryServiceType);
            writer.WriteEndElement();
        }
    }
}
