﻿namespace DocMagic.DsiDocRequest
{
    /// <summary>
    /// Indicates a loan investor.
    /// </summary>
    public enum E_InvestorType
    {
        /// <summary>
        /// No answer selected.
        /// </summary>
        LeaveBlank,

        /// <summary>
        /// Indicates no investor.
        /// </summary>
        None,

        /// <summary>
        /// Indicates Fannie Mae.
        /// </summary>
        FNM,

        /// <summary>
        /// Indicates Freddie Mac.
        /// </summary>
        FRE
    }
}
