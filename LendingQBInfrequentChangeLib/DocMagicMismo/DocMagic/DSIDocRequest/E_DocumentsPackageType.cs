﻿// <copyright file="E_DocumentsPackageType.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Justin Lara
//    Date:   July 6, 2015
// </summary>

namespace DocMagic.DsiDocRequest
{
    public enum E_DocumentsPackageType
    {
        Initial = 0,
        Closing = 1,
        Other = 2
    }
}