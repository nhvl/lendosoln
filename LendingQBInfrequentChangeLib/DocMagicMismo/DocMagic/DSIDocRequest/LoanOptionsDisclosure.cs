﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using XmlSerializableCommon;
namespace DocMagic.DsiDocRequest
{
    public class LoanOptionsDisclosure : AbstractXmlSerializable
    {
        private LoanOptionsDisclosureDetail m_loanOptionsDisclosureDetail = null;
        private List<LoanOptionComparisonItem> m_loanOptionComparisonItemList = null;

        public LoanOptionsDisclosureDetail LoanOptionsDisclosureDetail
        {
            get
            {
                if (null == m_loanOptionsDisclosureDetail)
                {
                    m_loanOptionsDisclosureDetail = new LoanOptionsDisclosureDetail();
                }
                return m_loanOptionsDisclosureDetail;
            }
            set { m_loanOptionsDisclosureDetail = value; }
        }

        public List<LoanOptionComparisonItem> LoanOptionComparisonItemList
        {
            get
            {
                if (null == m_loanOptionComparisonItemList)
                {
                    m_loanOptionComparisonItemList = new List<LoanOptionComparisonItem>();
                }
                return m_loanOptionComparisonItemList;
            }
        }
        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "LoanOptionsDisclosure")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "LoanOptionsDisclosure") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                switch (reader.Name)
                {
                    case "LoanOptionsDisclosureDetail":
                        ReadElement(reader, LoanOptionsDisclosureDetail);
                        break;
                    case "LoanOptionComparisonItem":
                        ReadElement(reader, LoanOptionComparisonItemList);
                        break;
                }
                #endregion
            }
        }

        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("LoanOptionsDisclosure");
            WriteElement(writer, m_loanOptionsDisclosureDetail);
            WriteElement(writer, m_loanOptionComparisonItemList);
            writer.WriteEndElement();
        }
    }
}
