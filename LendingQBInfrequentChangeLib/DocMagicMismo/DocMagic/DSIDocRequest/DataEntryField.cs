// Generated by CodeMonkey on 2/7/2009 11:54:44 AM
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace DocMagic.DsiDocRequest
{
    public class DataEntryField : AbstractXmlSerializable
    {
        #region Private Member Variables
        private string m_id = string.Empty;
        private E_DataEntryFieldState m_state = E_DataEntryFieldState.On;
        #endregion

        #region Public Properties
        public string Id
        {
            get { return m_id; }
            set { m_id = value; }
        }
        public E_DataEntryFieldState State
        {
            get { return m_state; }
            set { m_state = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "DataEntryField")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_id = ReadAttribute(reader, "id");
            m_state = (E_DataEntryFieldState) ReadAttribute(reader, "state", EnumMappings.E_DataEntryFieldState);
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "DataEntryField") return;

            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("DataEntryField");
            WriteAttribute(writer, "id", m_id);
            WriteEnumAttribute(writer, "state", EnumMappings.E_DataEntryFieldState[(int) m_state]);
            writer.WriteEndElement();
        }
    }

    public enum E_DataEntryFieldState
    {
        Undefined
        , On
        , Off
    }

    static partial class EnumMappings
    {
        public static readonly string[] E_DataEntryFieldState = {
                        ""
                         , "On"
                         , "Off"
                         };
    }
}
