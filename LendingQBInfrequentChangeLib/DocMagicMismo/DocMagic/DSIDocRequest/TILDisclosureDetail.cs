﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XmlSerializableCommon;
using System.Xml;

namespace DocMagic.DsiDocRequest
{
    public class TILDisclosureDetail :AbstractXmlSerializable
    {
        public string TILDisclosureDate { get; set; }

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "TILDisclosureDetail")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "TILDisclosureDetail") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                switch (reader.Name)
                {
                    case "TILDisclosureDate":
                        TILDisclosureDate = reader.ReadString();
                        break;

                }
                #endregion
            }
        }

        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("TILDisclosureDetail");
            WriteElementString(writer, "TILDisclosureDate", TILDisclosureDate);
            writer.WriteEndElement();
        }
    }
}
