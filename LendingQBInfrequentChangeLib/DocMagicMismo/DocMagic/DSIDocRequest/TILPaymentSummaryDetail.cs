﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Xml;
using XmlSerializableCommon;

namespace DocMagic.DsiDocRequest
{
    public class TILPaymentSummaryDetail : AbstractXmlSerializable
    {
        public string MinimumPaymentOnlyAdditionalBorrowedAmount { get; set; }
        public string MinimumPaymentOnlyAdditionalBorrowedDate { get; set; }
        public E_YNIndicator NonPrincipalAndInterestPaymentIncludesMortgageInsuranceIndicator { get; set; }
        public E_YNIndicator NonPrincipalAndInterestPaymentIncludesPrivateMortgageInsuranceIndicator { get; set; }
        public E_YNIndicator NonPrincipalAndInterestPaymentIncludesTaxesAndInsuranceEscrowsIndicator { get; set; }

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "TILPaymentSummaryDetail")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            NonPrincipalAndInterestPaymentIncludesMortgageInsuranceIndicator = (E_YNIndicator)ReadAttribute(reader, "NonPrincipalAndInterestPaymentIncludesMortgageInsuranceIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator);
            NonPrincipalAndInterestPaymentIncludesPrivateMortgageInsuranceIndicator = (E_YNIndicator)ReadAttribute(reader, "NonPrincipalAndInterestPaymentIncludesPrivateMortgageInsuranceIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator);
            NonPrincipalAndInterestPaymentIncludesTaxesAndInsuranceEscrowsIndicator = (E_YNIndicator)ReadAttribute(reader, "NonPrincipalAndInterestPaymentIncludesTaxesAndInsuranceEscrowsIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator);

            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "TILPaymentSummaryDetail") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                switch (reader.Name)
                {
                    case "MinimumPaymentOnlyAdditionalBorrowedAmount":
                        MinimumPaymentOnlyAdditionalBorrowedAmount = reader.ReadString();
                        break;
                    case "MinimumPaymentOnlyAdditionalBorrowedDate":
                        MinimumPaymentOnlyAdditionalBorrowedDate = reader.ReadString();
                        break;
                }
                #endregion
            }

        }

        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("TILPaymentSummaryDetail");
            WriteEnumAttribute(writer, "NonPrincipalAndInterestPaymentIncludesMortgageInsuranceIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator[(int)NonPrincipalAndInterestPaymentIncludesMortgageInsuranceIndicator]);
            WriteEnumAttribute(writer, "NonPrincipalAndInterestPaymentIncludesPrivateMortgageInsuranceIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator[(int)NonPrincipalAndInterestPaymentIncludesPrivateMortgageInsuranceIndicator]);
            WriteEnumAttribute(writer, "NonPrincipalAndInterestPaymentIncludesTaxesAndInsuranceEscrowsIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator[(int)NonPrincipalAndInterestPaymentIncludesTaxesAndInsuranceEscrowsIndicator]);
            WriteElementString(writer, "MinimumPaymentOnlyAdditionalBorrowedAmount", MinimumPaymentOnlyAdditionalBorrowedAmount);
            WriteElementString(writer, "MinimumPaymentOnlyAdditionalBorrowedDate", MinimumPaymentOnlyAdditionalBorrowedDate);

            writer.WriteEndElement();

        }
    }
}
