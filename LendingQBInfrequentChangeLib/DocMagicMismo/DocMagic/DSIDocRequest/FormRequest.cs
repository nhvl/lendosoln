﻿using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace DocMagic.DsiDocRequest
{
    public class FormRequest : AbstractXmlSerializable
    {
        #region Private Member Variables
        private E_FormRequestType m_type = E_FormRequestType.Undefined;
        private E_FormRequestPackageType m_packageType = E_FormRequestPackageType.Undefined;
        private string m_websheetNumber = string.Empty;
        #endregion

        #region Public Properties
        public E_FormRequestType Type
        {
            get { return m_type; }
            set { m_type = value; }
        }
        public E_FormRequestPackageType PackageType
        {
            get { return m_packageType; }
            set { m_packageType = value; }
        }
        public string WebsheetNumber
        {
            get { return m_websheetNumber; }
            set { m_websheetNumber = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "FormRequest")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_type = (E_FormRequestType)ReadAttribute(reader, "type", EnumMappings.E_FormRequestType);
            m_packageType = (E_FormRequestPackageType)ReadAttribute(reader, "PackageType", EnumMappings.E_FormRequestPackageType);
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "FormRequest") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                switch (reader.Name)
                {
                    case "WebsheetNumber":
                        WebsheetNumber = reader.ReadString();
                        break;
                }
                #endregion
            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("FormRequest");
            WriteEnumAttribute(writer, "type", EnumMappings.E_FormRequestType[(int)m_type]);
            WriteEnumAttribute(writer, "packageType", EnumMappings.E_FormRequestPackageType[(int)m_packageType]); // yes lowercase 'p'
            WriteElementString(writer, "WebsheetNumber", m_websheetNumber);
            writer.WriteEndElement();
        }
    }

    public enum E_FormRequestType
    {
        Undefined
        , List
    }

    public enum E_FormRequestPackageType
    {
        Undefined
        , Closing
        , Predisclosure
        , FormList
    }

    static partial class EnumMappings
    {
        public static readonly string[] E_FormRequestType = {
            ""
            , "List"
            };

        public static readonly string[] E_FormRequestPackageType = {
            ""
            , "Closing"
            , "Predisclosure"
            , "FormList"
            };
    }
}
