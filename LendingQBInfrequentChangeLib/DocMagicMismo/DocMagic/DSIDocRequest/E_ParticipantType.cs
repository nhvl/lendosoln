﻿namespace DocMagic.DsiDocRequest
{
    /// <summary>
    /// Indicates the lender's relationship to an investor.
    /// </summary>
    public enum E_ParticipantType
    {
        /// <summary>
        /// A blank option.
        /// </summary>
        LeaveBlank,

        /// <summary>
        /// Indicates a seller to the investor.
        /// </summary>
        Seller,

        /// <summary>
        /// Indicates a correspondent of the investor.
        /// </summary>
        Correspondent
    }
}
