// Generated by CodeMonkey on 2/7/2009 11:54:44 AM
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace DocMagic.DsiDocRequest
{
    public class MismoClosing : AbstractXmlSerializable
    {
        #region Private Member Variables
        private string m_planCode = string.Empty;
        private string m_altLenderCode = string.Empty;
        private Mismo.Closing2_6.Loan m_loan = null;
        #endregion

        #region Public Properties
        public string PlanCode
        {
            get { return m_planCode; }
            set { m_planCode = value; }
        }
        public string AltLenderCode
        {
            get { return m_altLenderCode; }
            set { m_altLenderCode = value; }
        }
        public Mismo.Closing2_6.Loan Loan
        {
            get 
            {
                if (null == m_loan) m_loan = new Mismo.Closing2_6.Loan();
                return m_loan; 
            }
            set { m_loan = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "MISMOClosing")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "MISMOClosing") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                switch (reader.Name)
                {
                    case "PlanCode":
                        PlanCode = reader.ReadString();
                        break;
                    case "AltLenderCode":
                        AltLenderCode = reader.ReadString();
                        break;
                    case "LOAN":
                        ReadElement(reader, Loan);
                        break;
                }
                #endregion
            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("MISMOClosing");
            WriteElementString(writer, "PlanCode", m_planCode);
            WriteElementString(writer, "AltLenderCode", m_altLenderCode);
            WriteElement(writer, m_loan);
            writer.WriteEndElement();
        }
    }
}
