﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XmlSerializableCommon;
using System.Xml;

namespace DocMagic.DsiDocRequest
{
    public class DisclosureDetail : AbstractXmlSerializable
    {
        #region Private Member Variables
        private E_DisclosureDetailRedisclosureMethod m_reDisclosureMethod = E_DisclosureDetailRedisclosureMethod.Undefined;
        private string m_changedCircumstanceDate = string.Empty;
        private string m_reDisclosureSendDate = string.Empty;
        private string m_reDisclosureReceivedDate = string.Empty;
        private string m_disclosedBalloonMaturityMonths = string.Empty;
        private string m_disclosedBalloonPaymentAmount = string.Empty;
        private string m_disclosedFirstRateAdjustmentMonths = string.Empty;
        private string m_disclosedLoanAmount = string.Empty;
        private string m_disclosedLoanMaturityTermMonths = string.Empty;
        private string m_DisclosedInterestRatePercent = string.Empty;
        private string m_DisclosedMaximumInterestRatePercent = string.Empty;
        private string m_DisclosedMonthlyPaymentAmount = string.Empty;
        private string m_DisclosedNegativeAmortizationMaximumLoanBalanceAmount = string.Empty;
        private string m_DisclosureSendDate = string.Empty;
        private string m_LastDisclosedAPR = string.Empty;
        #endregion

        #region Public Properties
        public E_DisclosureDetailRedisclosureMethod ReDisclosureMethod
        {
            get { return m_reDisclosureMethod; }
            set { m_reDisclosureMethod = value; }
        }
        public string ChangedCircumstanceDate
        {
            get;
            set;
        }
        public string ReDisclosureSendDate
        {
            get;
            set;
        }
        public string ReDisclosureReceivedDate
        {
            get;
            set;
        }
        public string DisclosedBalloonMaturityMonths
        {
            get;
            set;
        }
        public string DisclosedBalloonPaymentAmount
        {
            get;
            set;
        }
        public string DisclosedFirstRateAdjustmentMonths
        {
            get;
            set;
        }
        public string DisclosedLoanAmount
        {
            get;
            set;
        }
        public string DisclosedLoanMaturityTermMonths
        {
            get;
            set;
        }
        public string DisclosedInterestRatePercent
        {
            get;
            set;
        }
        public string DisclosedMaximumInterestRatePercent
        {
            get;
            set;
        }
        public string DisclosedMonthlyPaymentAmount
        {
            get;
            set;
        }
        public string DisclosedNegativeAmortizationMaximumLoanBalanceAmount
        {
            get;
            set;
        }
        public string DisclosureSendDate
        {
            get;
            set;
        }
        public string LastDisclosedAPR
        {
            get;
            set;
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "DisclosureDetail")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_reDisclosureMethod = (E_DisclosureDetailRedisclosureMethod)ReadAttribute(reader, "ReDisclosureMethod", EnumMappings.E_DisclosureDetailRedisclosureMethod);
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "DisclosureDetail") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                switch (reader.Name)
                {
                    case "ChangedCircumstanceDate":
                        ChangedCircumstanceDate = reader.ReadString();
                        break;
                    case "ReDisclosureSendDate":
                        ReDisclosureSendDate = reader.ReadString();
                        break;
                    case "ReDisclosureReceivedDate":
                        ReDisclosureReceivedDate = reader.ReadString();
                        break;
                    case "DisclosedBalloonMaturityMonths":
                        DisclosedBalloonMaturityMonths = reader.ReadString();
                        break;
                    case "DisclosedBalloonPaymentAmount":
                        DisclosedBalloonPaymentAmount = reader.ReadString();
                        break;
                    case "DisclosedFirstRateAdjustmentMonths":
                        DisclosedFirstRateAdjustmentMonths = reader.ReadString();
                        break;
                    case "DisclosedLoanAmount":
                        DisclosedLoanAmount = reader.ReadString();
                        break;
                    case "DisclosedLoanMaturityTermMonths":
                        DisclosedLoanMaturityTermMonths = reader.ReadString();
                        break;
                    case "DisclosedInterestRatePercent":
                        DisclosedInterestRatePercent = reader.ReadString();
                        break;
                    case "DisclosedMaximumInterestRatePercent":
                        DisclosedMaximumInterestRatePercent = reader.ReadString();
                        break;
                    case "DisclosedMonthlyPaymentAmount":
                        DisclosedMonthlyPaymentAmount = reader.ReadString();
                        break;
                    case "DisclosedNegativeAmortizationMaximumLoanBalanceAmount":
                        DisclosedNegativeAmortizationMaximumLoanBalanceAmount = reader.ReadString();
                        break;
                    case "DisclosureSendDate":
                        DisclosureSendDate = reader.ReadString();
                        break;
                    case "LastDisclosedAPR":
                        LastDisclosedAPR = reader.ReadString();
                        break;
                }
                #endregion
            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("DisclosureDetail");
            WriteEnumAttribute(writer, "ReDisclosureMethod", EnumMappings.E_DisclosureDetailRedisclosureMethod[(int)m_reDisclosureMethod]);

            WriteElementString(writer, "DisclosureSendDate", DisclosureSendDate);
            WriteElementString(writer, "ReDisclosureSendDate", ReDisclosureSendDate);
            WriteElementString(writer, "ReDisclosureReceivedDate", ReDisclosureReceivedDate);
            WriteElementString(writer, "LastDisclosedAPR", LastDisclosedAPR);
            WriteElementString(writer, "DisclosedBalloonMaturityMonths", DisclosedBalloonMaturityMonths);
            WriteElementString(writer, "DisclosedBalloonPaymentAmount", DisclosedBalloonPaymentAmount);
            WriteElementString(writer, "DisclosedFirstRateAdjustmentMonths", DisclosedFirstRateAdjustmentMonths);
            WriteElementString(writer, "DisclosedLoanAmount", DisclosedLoanAmount);
            WriteElementString(writer, "DisclosedLoanMaturityTermMonths", DisclosedLoanMaturityTermMonths);
            WriteElementString(writer, "DisclosedInterestRatePercent", DisclosedInterestRatePercent);
            WriteElementString(writer, "DisclosedMaximumInterestRatePercent", DisclosedMaximumInterestRatePercent);
            WriteElementString(writer, "DisclosedMonthlyPaymentAmount", DisclosedMonthlyPaymentAmount);
            WriteElementString(writer, "DisclosedNegativeAmortizationMaximumLoanBalanceAmount", DisclosedNegativeAmortizationMaximumLoanBalanceAmount);
            // OtherAmortizationTypeDescription
            WriteElementString(writer, "ChangedCircumstanceDate", ChangedCircumstanceDate);
            writer.WriteEndElement();
        }
    }

    public enum E_DisclosureDetailRedisclosureMethod
    {
        Undefined
        , Email
        , Fax
        , InPerson
        , Mail
        , Overnight
    }

    static partial class EnumMappings
    {
        public static readonly string[] E_DisclosureDetailRedisclosureMethod = {
                        ""
                         , "Email"
                         , "Fax"
                         , "InPerson"
                         , "Mail"
                         , "Overnight"
                         };
    }
}
