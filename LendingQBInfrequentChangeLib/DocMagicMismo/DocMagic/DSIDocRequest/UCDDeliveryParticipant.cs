﻿namespace DocMagic.DsiDocRequest
{
    using System;
    using System.Xml;
    using XmlSerializableCommon;

    public class UCDDeliveryParticipant : AbstractXmlSerializable
    {
        private E_ParticipantType participantType;

        private string participantIdentifier;

        public E_ParticipantType ParticipantType
        {
            get { return this.participantType; }
            set { this.participantType = value; }
        }

        public string ParticipantIdentifier
        {
            get { return this.participantIdentifier; }
            set { this.participantIdentifier = value; }
        }

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != nameof(UCDDeliveryParticipant))
            {
                reader.Read();
                if (reader.EOF)
                {
                    return;
                }
            }

            this.ParticipantType = (E_ParticipantType)this.ReadAttribute(reader, nameof(this.participantType), EnumMappings.E_ParticipantType);

            if (reader.IsEmptyElement)
            {
                return;
            }

            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == nameof(UCDDeliveryParticipant))
                {
                    return;
                }

                if (reader.NodeType != XmlNodeType.Element)
                {
                    continue;
                }

                switch (reader.Name)
                {
                    case nameof(this.ParticipantIdentifier):
                        this.ParticipantIdentifier = reader.ReadString();
                        break;
                    default:
                        throw new ArgumentException($"Element {reader.Name} is not handled.");
                }
            }
        }

        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement(nameof(UCDDeliveryParticipant));
            this.WriteEnumAttribute(writer, nameof(this.participantType), EnumMappings.E_ParticipantType[(int)this.ParticipantType]);
            this.WriteElementString(writer, nameof(this.ParticipantIdentifier), this.ParticipantIdentifier);
            writer.WriteEndElement();
        }
    }

    static partial class EnumMappings
    {
        public static readonly string[] E_ParticipantType = {
            string.Empty,
            "Seller",
            "Correspondent"
        };
    }
}
