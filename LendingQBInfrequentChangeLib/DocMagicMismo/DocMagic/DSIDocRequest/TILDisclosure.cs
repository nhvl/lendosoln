﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XmlSerializableCommon;
using System.Xml;

namespace DocMagic.DsiDocRequest
{
    public class TILDisclosure : AbstractXmlSerializable
    {
        private TILDisclosureDetail m_tilDisclosureDetail = null;

        public TILDisclosureDetail TILDisclosureDetail
        {
            get
            {
                if (null == m_tilDisclosureDetail)
                {
                    m_tilDisclosureDetail = new TILDisclosureDetail();
                }
                return m_tilDisclosureDetail;
            }
            set { m_tilDisclosureDetail = value; }
        }

        private TILPaymentSummary m_tilPaymentSummary = null;

        public TILPaymentSummary TILPaymentSummary
        {
            get
            {
                if (null == m_tilPaymentSummary)
                {
                    m_tilPaymentSummary = new TILPaymentSummary();
                }
                return m_tilPaymentSummary;
            }
            set { m_tilPaymentSummary = value; }
        }
        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "TILDisclosure")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "TILDisclosure") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                switch (reader.Name)
                {
                    case "TILDisclosureDetail":
                        ReadElement(reader, TILDisclosureDetail);
                        break;
                    case "TILPaymentSummary":
                        ReadElement(reader, TILPaymentSummary);
                        break;
                }
                #endregion
            }
        }

        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("TILDisclosure");
            WriteElement(writer, m_tilDisclosureDetail);
            WriteElement(writer, m_tilPaymentSummary);
            writer.WriteEndElement();
        }
    }
}
