﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XmlSerializableCommon;
using System.Xml;

namespace DocMagic.DsiDocRequest
{
    public class RespaFee : AbstractXmlSerializable
    {
        #region Private Member Variables
        private string m_respaFeeID = string.Empty;
        private E_YNIndicator m_associatedWithChangedCircumstanceIndicator = E_YNIndicator.Undefined;
        private E_YNIndicator m_paidToAffiliateIndicator = E_YNIndicator.Undefined;
        private E_YNIndicator m_requiredServiceIndicator = E_YNIndicator.Undefined;
        private string m_withheldFromWireAmount = string.Empty;
        #endregion

        #region Public Properties
        public string RespaFeeID
        {
            get { return m_respaFeeID; }
            set { m_respaFeeID = value; }
        }
        public E_YNIndicator AssociatedWithChangedCircumstanceIndicator
        {
            get { return m_associatedWithChangedCircumstanceIndicator; }
            set { m_associatedWithChangedCircumstanceIndicator = value; }
        }
        public E_YNIndicator PaidToAffiliateIndicator
        {
            get { return m_paidToAffiliateIndicator; }
            set { m_paidToAffiliateIndicator = value; }
        }
        public E_YNIndicator RequiredServiceIndicator
        {
            get { return m_requiredServiceIndicator; }
            set { m_requiredServiceIndicator = value; }
        }
        public string WithheldFromWireAmount
        {
            get { return m_withheldFromWireAmount; }
            set { m_withheldFromWireAmount = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "RespaFee")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_respaFeeID = ReadAttribute(reader, "RespaFeeID");
            m_associatedWithChangedCircumstanceIndicator = (E_YNIndicator)ReadAttribute(reader, "AssociatedWithChangedCircumstanceIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator);
            m_paidToAffiliateIndicator = (E_YNIndicator)ReadAttribute(reader, "PaidToAffiliateIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator);
            m_requiredServiceIndicator = (E_YNIndicator)ReadAttribute(reader, "RequiredServiceIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator);
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "RespaFee") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                switch (reader.Name)
                {
                    case "WithheldFromWireAmount":
                        m_withheldFromWireAmount = reader.ReadString();
                        break;
                }
                #endregion
            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("RespaFee");
            WriteAttribute(writer, "RespaFeeID", m_respaFeeID);
            WriteEnumAttribute(writer, "AssociatedWithChangedCircumstanceIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator[(int)m_associatedWithChangedCircumstanceIndicator]);
            WriteEnumAttribute(writer, "PaidToAffiliateIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator[(int)m_paidToAffiliateIndicator]);
            WriteEnumAttribute(writer, "RequiredServiceIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator[(int)m_requiredServiceIndicator]);
            WriteElementString(writer, "WithheldFromWireAmount", m_withheldFromWireAmount);
            writer.WriteEndElement();
        }
    }
}
