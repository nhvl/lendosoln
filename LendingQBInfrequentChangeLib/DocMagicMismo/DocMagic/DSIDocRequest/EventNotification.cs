
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace DocMagic.DsiDocRequest
{
    public class EventNotification : AbstractXmlSerializable
    {
        #region Private Member Variables
        private List<EmailPerson> m_emailPersonList = null;
        #endregion

        #region Public Properties
        public List<EmailPerson> EmailPersonList
        {
            get
            {
                if (null == m_emailPersonList) m_emailPersonList = new List<EmailPerson>();
                return m_emailPersonList;
            }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "EventNotification")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "EventNotification") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                switch (reader.Name)
                {
                    case "EmailPerson":
                        ReadElement(reader, EmailPersonList);
                        break;
                }
                #endregion
            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("EventNotification");
            WriteElement(writer, m_emailPersonList);
            writer.WriteEndElement();
        }
    }
}
