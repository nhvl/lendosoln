﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using XmlSerializableCommon;

namespace DocMagic.DsiDocRequest
{
    public class TILPaymentSummary : AbstractXmlSerializable
    {
        private TILPaymentSummaryDetail m_tilPaymentSummaryDetail = null;
        private List<TILPaymentSummaryItem> m_tilPaymentSummaryItemList = null;

        public TILPaymentSummaryDetail TILPaymentSummaryDetail
        {
            get
            {
                if (null == m_tilPaymentSummaryDetail)
                {
                    m_tilPaymentSummaryDetail = new TILPaymentSummaryDetail();
                }
                return m_tilPaymentSummaryDetail;
            }
            set { m_tilPaymentSummaryDetail = value; }
        }

        public List<TILPaymentSummaryItem> TILPaymentSummaryItemList
        {
            get
            {
                if (null == m_tilPaymentSummaryItemList)
                {
                    m_tilPaymentSummaryItemList = new List<TILPaymentSummaryItem>();
                }
                return m_tilPaymentSummaryItemList;
            }
        }

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "TILPaymentSummary")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "TILPaymentSummary") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                switch (reader.Name)
                {
                    case "TILPaymentSummaryDetail":
                        ReadElement(reader, TILPaymentSummaryDetail);
                        break;
                    case "TILPaymentSummaryItem":
                        ReadElement(reader, TILPaymentSummaryItemList);
                        break;
                }
                #endregion
            }

        }

        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("TILPaymentSummary");
            WriteElement(writer, m_tilPaymentSummaryDetail);
            WriteElement(writer, m_tilPaymentSummaryItemList);
            writer.WriteEndElement();
        }
    }
}
