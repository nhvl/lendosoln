﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XmlSerializableCommon;
using System.Xml;

namespace DocMagic.DsiDocRequest
{
    public class PreviousFilingAddress : AbstractXmlSerializable
    {
        #region Public Properties
        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string County { get; set; }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "PreviousFilingAddress")
            {
                reader.Read();
                if (reader.EOF) return;
            }

            #region Read Attributes
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "PreviousFilingAddress") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                switch (reader.Name)
                {
                    case "Street":
                        Street = reader.ReadString();
                        break;
                    case "City":
                        City = reader.ReadString();
                        break;
                    case "State":
                        State = reader.ReadString();
                        break;
                    case "Zip":
                        Zip = reader.ReadString();
                        break;
                    case "County":
                        County = reader.ReadString();
                        break;
                }
                #endregion
            }
        }

        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("PreviousFilingAddress");
            WriteElementString(writer, "Street", Street);
            WriteElementString(writer, "City", City);
            WriteElementString(writer, "State", State);
            WriteElementString(writer, "Zip", Zip);
            WriteElementString(writer, "County", County);
            writer.WriteEndElement();
        }
    }
}
