// Generated by CodeMonkey on 2/7/2009 11:54:44 AM
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace DocMagic.DsiDocRequest
{
    public class ProgramFeature : AbstractXmlSerializable
    {
        #region Private Member Variables
        private E_ProgramFeatureType m_type = E_ProgramFeatureType.Undefined;
        #endregion

        #region Public Properties
        public E_ProgramFeatureType Type
        {
            get { return m_type; }
            set { m_type = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "ProgramFeature")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_type = (E_ProgramFeatureType) ReadAttribute(reader, "Type", EnumMappings.E_ProgramFeatureType);
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "ProgramFeature") return;

            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("ProgramFeature");
            WriteEnumAttribute(writer, "Type", EnumMappings.E_ProgramFeatureType[(int) m_type]);
            writer.WriteEndElement();
        }
    }

    public enum E_ProgramFeatureType
    {
        Undefined
        , _100PercentLTV
        , AllReadyHomeRefi
        , AssociateDiscount
        , BondTaxProviderExempt
        , CoverLetter
        , DisasterReliefProgram
        , FloatDown
        , MortgageRewards
        , MortgageRewardsPlus
        , MortgageRewards3
        , MortgageRewardsAlternativeVendor
        , POSClosing
        , PreferredRate
        , PrivateBanking
        , TrustCertification
    }

    static partial class EnumMappings
    {
        public static readonly string[] E_ProgramFeatureType = {
                        ""
                         , "100PercentLTV"
                         , "AllReadyHomeRefi"
                         , "AssociateDiscount"
                         , "BondTaxProviderExempt"
                         , "CoverLetter"
                         , "DisasterReliefProgram"
                         , "FloatDown"
                         , "MortgageRewards"
                         , "MortgageRewardsPlus"
                         , "MortgageRewards3"
                         , "MortgageRewardsAlternativeVendor"
                         , "POSClosing"
                         , "PreferredRate"
                         , "PrivateBanking"
                         , "TrustCertification"
                         };
    }
}
