﻿namespace DocMagic.DsiDocRequest
{
    using System;
    using System.Xml;
    using XmlSerializableCommon;

    public class InvestorDelivery : AbstractXmlSerializable
    {
        private E_YNIndicator includeDeliveryContentInResponseIndicator;

        private Credentials credentials;

        private UCDDeliveryParticipants ucdDeliveryParticipants;

        public E_YNIndicator IncludeDeliveryContentInResponseIndicator
        {
            get { return this.includeDeliveryContentInResponseIndicator; }
            set { this.includeDeliveryContentInResponseIndicator = value; }
        }

        public Credentials Credentials
        {
            get
            {
                if (this.credentials == null)
                {
                    this.credentials = new Credentials();
                }

                return this.credentials;
            }

            set
            {
                this.credentials = value;
            }
        }

        public UCDDeliveryParticipants UCDDeliveryParticipants
        {
            get
            {
                if (this.ucdDeliveryParticipants == null)
                {
                    this.ucdDeliveryParticipants = new UCDDeliveryParticipants();
                }

                return this.ucdDeliveryParticipants;
            }

            set
            {
                this.ucdDeliveryParticipants = value;
            }
        }

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != nameof(InvestorDelivery))
            {
                reader.Read();
                if (reader.EOF)
                {
                    return;
                }
            }

            this.IncludeDeliveryContentInResponseIndicator = (E_YNIndicator)this.ReadAttribute(reader, nameof(this.includeDeliveryContentInResponseIndicator), XmlSerializableCommon.EnumMappings.E_YNIndicator);

            if (reader.IsEmptyElement)
            {
                return;
            }

            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == nameof(InvestorDelivery))
                {
                    return;
                }

                if (reader.NodeType != XmlNodeType.Element)
                {
                    continue;
                }

                switch (reader.Name)
                {
                    case nameof(Credentials):
                        this.ReadElement(reader, this.Credentials);
                        break;
                    case nameof(UCDDeliveryParticipants):
                        this.ReadElement(reader, this.UCDDeliveryParticipants);
                        break;
                    default:
                        throw new ArgumentException($"Element {reader.Name} is not handled.");
                }
            }
        }

        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement(nameof(InvestorDelivery));
            this.WriteEnumAttribute(writer, nameof(this.includeDeliveryContentInResponseIndicator), XmlSerializableCommon.EnumMappings.E_YNIndicator[(int)this.IncludeDeliveryContentInResponseIndicator]);
            this.WriteElement(writer, this.Credentials);
            this.WriteElement(writer, this.UCDDeliveryParticipants);
            writer.WriteEndElement();
        }
    }
}
