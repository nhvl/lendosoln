﻿namespace DocMagic.DsiDocRequest
{
    /// <summary>
    /// Indicates a DocMagic Data Service web method.
    /// </summary>
    public enum E_DataServiceActionType
    {
        /// <summary>
        /// A default blank option.
        /// </summary>
        LeaveBlank,

        /// <summary>
        /// Creates a UCD file.
        /// </summary>
        Create,

        /// <summary>
        /// Creates a UCD file and delivers to the investor.
        /// </summary>
        CreateAndDeliver
    }
}
