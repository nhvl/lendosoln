﻿// <copyright file="PartyContacts.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Justin Lara
//  Date:   July 22, 2015
// </summary>

namespace DocMagic.DsiDocRequest
{
    using System.Xml;
    using XmlSerializableCommon;
    using System.Collections.Generic;

    public class PartyContacts : AbstractXmlSerializable
    {
        private List<PartyContact> partyContactList = null;

        public List<PartyContact> PartyContactList
        {
            get
            {
                if (partyContactList == null)
                {
                    partyContactList = new List<PartyContact>();
                }

                return partyContactList;
            }
        }

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "Party")
            {
                reader.Read();
                if (reader.EOF) return;
            }

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "Party") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                switch (reader.Name)
                {
                    case "PartyContact":
                        ReadElement(reader, PartyContactList);
                        break;
                }
            }
        }

        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("PartyContacts");
            WriteElement(writer, partyContactList);
            writer.WriteEndElement();
        }
    }
}