// Generated by CodeMonkey on 2/7/2009 11:54:44 AM
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace DocMagic.DsiDocRequest
{
    public class ServiceProviderRequest : AbstractXmlSerializable
    {
        #region Private Member Variables
        private E_ServiceProviderRequestType m_type = E_ServiceProviderRequestType.Undefined;
        private List<ServiceProvider> m_serviceProviderList = null;
        #endregion

        #region Public Properties
        public E_ServiceProviderRequestType Type
        {
            get { return m_type; }
            set { m_type = value; }
        }
        public List<ServiceProvider> ServiceProviderList
        {
            get
            {
                if (null == m_serviceProviderList) m_serviceProviderList = new List<ServiceProvider>();
                return m_serviceProviderList;
            }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "ServiceProviderRequest")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_type = (E_ServiceProviderRequestType) ReadAttribute(reader, "type", EnumMappings.E_ServiceProviderRequestType);
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "ServiceProviderRequest") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                switch (reader.Name)
                {
                    case "ServiceProvider":
                        ReadElement(reader, ServiceProviderList);
                        break;
                }
                #endregion
            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("ServiceProviderRequest");
            WriteEnumAttribute(writer, "type", EnumMappings.E_ServiceProviderRequestType[(int) m_type]);
            WriteElement(writer, m_serviceProviderList);
            writer.WriteEndElement();
        }
    }

    public enum E_ServiceProviderRequestType
    {
        Undefined
        , Load
        , Save
        , Delete
    }

    static partial class EnumMappings
    {
        public static readonly string[] E_ServiceProviderRequestType = {
                        ""
                         , "Load"
                         , "Save"
                         , "Delete"
                         };
    }
}
