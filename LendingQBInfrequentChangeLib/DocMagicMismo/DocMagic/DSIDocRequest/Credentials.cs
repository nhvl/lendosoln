﻿namespace DocMagic.DsiDocRequest
{
    using System;
    using System.Xml;
    using XmlSerializableCommon;

    public class Credentials : AbstractXmlSerializable
    {
        public string LoginAccountIdentifier { get; set; }

        public string LoginAccountPassword { get; set; }

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != nameof(Credentials))
            {
                reader.Read();
                if (reader.EOF)
                {
                    return;
                }
            }

            if (reader.IsEmptyElement)
            {
                return;
            }

            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == nameof(Credentials))
                {
                    return;
                }

                if (reader.NodeType != XmlNodeType.Element)
                {
                    continue;
                }

                switch (reader.Name)
                {
                    case nameof(LoginAccountIdentifier):
                        this.LoginAccountIdentifier = reader.ReadString();
                        break;
                    case nameof(LoginAccountPassword):
                        this.LoginAccountPassword = reader.ReadString();
                        break;
                    default:
                        throw new ArgumentException($"Element {reader.Name} is not handled.");
                }
            }
        }

        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement(nameof(Credentials));
            this.WriteElementString(writer, nameof(LoginAccountIdentifier), this.LoginAccountIdentifier, true);
            this.WriteElementString(writer, nameof(LoginAccountPassword), this.LoginAccountPassword, true);
            writer.WriteEndElement();
        }
    }
}
