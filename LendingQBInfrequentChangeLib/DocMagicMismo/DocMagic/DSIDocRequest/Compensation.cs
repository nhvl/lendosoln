﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XmlSerializableCommon;
using System.Xml;

namespace DocMagic.DsiDocRequest
{
    public class Compensation : AbstractXmlSerializable
    {
        #region Private Member Variables
        private string m_compensationID = string.Empty;
        private E_YNIndicator m_associatedWithChangedCircumstanceIndicator = E_YNIndicator.Undefined;
        #endregion

        #region Public Properties
        public string CompensationID
        {
            get { return m_compensationID; }
            set { m_compensationID = value; }
        }
        public E_YNIndicator AssociatedWithChangedCircumstanceIndicator
        {
            get { return m_associatedWithChangedCircumstanceIndicator; }
            set { m_associatedWithChangedCircumstanceIndicator = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "Compensation")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_compensationID = ReadAttribute(reader, "CompensationID");
            m_associatedWithChangedCircumstanceIndicator = (E_YNIndicator)ReadAttribute(reader, "AssociatedWithChangedCircumstanceIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator);
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "Compensation") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                #endregion
            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("Compensation");
            WriteAttribute(writer, "CompensationID", m_compensationID);
            WriteEnumAttribute(writer, "AssociatedWithChangedCircumstanceIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator[(int)m_associatedWithChangedCircumstanceIndicator]);
            writer.WriteEndElement();
        }
    }
}
