﻿using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace DocMagic.DsiDocRequest
{
    public class VALoan : AbstractXmlSerializable
    {
        #region Private Member Variables
        private E_FHAVALoanPurposeType m_FHAVALoanPurposeType = E_FHAVALoanPurposeType.Undefined;
        #endregion

        #region Public Properties
        public E_FHAVALoanPurposeType FHAVALoanPurposeType
        {
            get { return m_FHAVALoanPurposeType; }
            set { m_FHAVALoanPurposeType = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "VALoan")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_FHAVALoanPurposeType = (E_FHAVALoanPurposeType) ReadAttribute(reader, "FHAVALoanPurposeType", EnumMappings.E_FHAVALoanPurposeType);
            #endregion
            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "VALoan") return;
            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("VALoan");
            WriteEnumAttribute(writer, "FHAVALoanPurposeType", EnumMappings.E_FHAVALoanPurposeType[(int) m_FHAVALoanPurposeType]);
            writer.WriteEndElement();
        }
    }
    public enum E_FHAVALoanPurposeType
    {
        Undefined
        , PurchaseExistingHomePreviouslyOccupied
        , FinanceImprovementsToExistingProperty
        , Refinance
        , PurchaseNewCondominiumUnit
        , PurchaseExistingCondominiumUnit
        , PurchaseExistingHomeNotPreviouslyOccupied
        , ConstructHome
        , FinanceCooperativePurchase
        , PurchasePermanentlySitedManufacturedHome
        , PurchasePermanentlySitedManufacturedHomeAndLot
        , RefinancePermanentlySitedManufacturedHomeToBuyLot
        , RefinancePermanentlySitedManufacturedHomeLotLoan
    }
    static partial class EnumMappings
    {
        public static readonly string[] E_FHAVALoanPurposeType = 
                {         ""
                          , "PurchaseExistingHomePreviouslyOccupied"
                          , "FinanceImprovementsToExistingProperty"
                          , "Refinance"
                          , "PurchaseNewCondominiumUnit"
                          , "PurchaseExistingCondominiumUnit"
                          , "PurchaseExistingHomeNotPreviouslyOccupied"
                          , "ConstructHome"
                          , "FinanceCooperativePurchase"
                          , "PurchasePermanentlySitedManufacturedHome"
                          , "PurchasePermanentlySitedManufacturedHomeAndLot"
                          , "RefinancePermanentlySitedManufacturedHomeToBuyLot"
                          , "RefinancePermanentlySitedManufacturedHomeLotLoan"
                };
    }
}
