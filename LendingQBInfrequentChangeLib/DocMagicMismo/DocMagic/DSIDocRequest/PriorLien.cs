// Generated by CodeMonkey on 2/7/2009 11:54:44 AM
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace DocMagic.DsiDocRequest
{
    public class PriorLien : AbstractXmlSerializable
    {
        #region Private Member Variables
        private string m_relatedLoanId = string.Empty;
        private E_YNIndicator m_assumabilityIndicator = E_YNIndicator.Undefined;
        private E_YNIndicator m_inDefaultIndicator = E_YNIndicator.Undefined;
        private E_PriorLienLienPriority m_lienPriority = E_PriorLienLienPriority.Undefined;
        private E_PriorLienLoanPurposeType m_loanPurposeType = E_PriorLienLoanPurposeType.Undefined;
        private E_YNIndicator m_prepaymentPenaltyIndicator = E_YNIndicator.Undefined;
        private E_PriorLienRelationshipToSubjectLoanType m_relationshipToSubjectLoanType = E_PriorLienRelationshipToSubjectLoanType.Undefined;
        private string m_mortgageDate = string.Empty;
        private string m_borrowerVesting = string.Empty;
        private string m_principalAmount = string.Empty;
        private string m_interestRate = string.Empty;
        private string m_paymentAmount = string.Empty;
        private string m_pitiPaymentAmount = string.Empty;
        private string m_maturityDate = string.Empty;
        private string m_balloonPaymentAmount = string.Empty;
        private string m_currentBalance = string.Empty;
        private string m_trusteeName = string.Empty;
        private string m_noteDate = string.Empty;
        private string m_lenderName = string.Empty;
        private string m_assignedDate = string.Empty;
        private string m_assignedToName = string.Empty;
        private string m_retainedByName = string.Empty;
        private List<PriorRecording> m_priorRecordingList = null;
        private string m_investorLoanIdentifier = string.Empty;
        private string m_monthlyMIPaymentAmount = string.Empty;
        private string m_assignmentRecordingDescription = string.Empty;
        private string m_lienPriorityOtherDescription = string.Empty;
        private string m_borrowerName = string.Empty;
        #endregion

        #region Public Properties
        public string RelatedLoanId
        {
            get { return m_relatedLoanId; }
            set { m_relatedLoanId = value; }
        }
        public E_YNIndicator AssumabilityIndicator
        {
            get { return m_assumabilityIndicator; }
            set { m_assumabilityIndicator = value; }
        }
        public E_YNIndicator InDefaultIndicator
        {
            get { return m_inDefaultIndicator; }
            set { m_inDefaultIndicator = value; }
        }
        public E_PriorLienLienPriority LienPriority
        {
            get { return m_lienPriority; }
            set { m_lienPriority = value; }
        }
        public E_PriorLienLoanPurposeType LoanPurposeType
        {
            get { return m_loanPurposeType; }
            set { m_loanPurposeType = value; }
        }
        public E_YNIndicator PrepaymentPenaltyIndicator
        {
            get { return m_prepaymentPenaltyIndicator; }
            set { m_prepaymentPenaltyIndicator = value; }
        }
        public E_PriorLienRelationshipToSubjectLoanType RelationshipToSubjectLoanType
        {
            get { return m_relationshipToSubjectLoanType; }
            set { m_relationshipToSubjectLoanType = value; }
        }
        public string MortgageDate
        {
            get { return m_mortgageDate; }
            set { m_mortgageDate = value; }
        }
        public string BorrowerVesting
        {
            get { return m_borrowerVesting; }
            set { m_borrowerVesting = value; }
        }
        public string PrincipalAmount
        {
            get { return m_principalAmount; }
            set { m_principalAmount = value; }
        }
        public string InterestRate
        {
            get { return m_interestRate; }
            set { m_interestRate = value; }
        }
        public string PaymentAmount
        {
            get { return m_paymentAmount; }
            set { m_paymentAmount = value; }
        }
        public string PITIPaymentAmount
        {
            get { return m_pitiPaymentAmount; }
            set { m_pitiPaymentAmount = value; }
        }
        public string MaturityDate
        {
            get { return m_maturityDate; }
            set { m_maturityDate = value; }
        }
        public string BalloonPaymentAmount
        {
            get { return m_balloonPaymentAmount; }
            set { m_balloonPaymentAmount = value; }
        }
        public string CurrentBalance
        {
            get { return m_currentBalance; }
            set { m_currentBalance = value; }
        }
        public string TrusteeName
        {
            get { return m_trusteeName; }
            set { m_trusteeName = value; }
        }
        public string NoteDate
        {
            get { return m_noteDate; }
            set { m_noteDate = value; }
        }
        public string LenderName
        {
            get { return m_lenderName; }
            set { m_lenderName = value; }
        }
        public string AssignedDate
        {
            get { return m_assignedDate; }
            set { m_assignedDate = value; }
        }
        public string AssignedToName
        {
            get { return m_assignedToName; }
            set { m_assignedToName = value; }
        }
        public string RetainedByName
        {
            get { return m_retainedByName; }
            set { m_retainedByName = value; }
        }
        public List<PriorRecording> PriorRecordingList
        {
            get
            {
                if (null == m_priorRecordingList) m_priorRecordingList = new List<PriorRecording>();
                return m_priorRecordingList;
            }
        }
        public string InvestorLoanIdentifier
        {
            get { return m_investorLoanIdentifier; }
            set { m_investorLoanIdentifier = value; }
        }
        public string MonthlyMIPaymentAmount
        {
            get { return m_monthlyMIPaymentAmount; }
            set { m_monthlyMIPaymentAmount = value; }
        }
        public string AssignmentRecordingDescription
        {
            get { return m_assignmentRecordingDescription; }
            set { m_assignmentRecordingDescription = value; }
        }
        public string LienPriorityOtherDescription
        {
            get { return m_lienPriorityOtherDescription; }
            set { m_lienPriorityOtherDescription = value; }
        }
        public string BorrowerName
        {
            get { return m_borrowerName; }
            set { m_borrowerName = value; }
        }
        #endregion


        public void Set_LienPriority(string priority)
        {
            m_lienPriority = (E_PriorLienLienPriority)GetEnumValue(priority, "LienPriority", EnumMappings.E_PriorLienLienPriority);
        }

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "PriorLien")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_relatedLoanId = ReadAttribute(reader, "RelatedLoanID");
            m_assumabilityIndicator = (E_YNIndicator)ReadAttribute(reader, "AssumabilityIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator);
            m_inDefaultIndicator = (E_YNIndicator)ReadAttribute(reader, "InDefaultIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator);
            m_lienPriority = (E_PriorLienLienPriority) ReadAttribute(reader, "LienPriority", EnumMappings.E_PriorLienLienPriority);
            m_loanPurposeType = (E_PriorLienLoanPurposeType)ReadAttribute(reader, "LoanPurposeType", EnumMappings.E_PriorLienLoanPurposeType);
            m_prepaymentPenaltyIndicator = (E_YNIndicator)ReadAttribute(reader, "PrepaymentPenaltyIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator);
            m_relationshipToSubjectLoanType = (E_PriorLienRelationshipToSubjectLoanType)ReadAttribute(reader, "RelationshipToSubjectLoanType", EnumMappings.E_PriorLienRelationshipToSubjectLoanType);
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "PriorLien") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                switch (reader.Name)
                {
                    case "MortgageDate":
                        MortgageDate = reader.ReadString();
                        break;
                    case "BorrowerVesting":
                        BorrowerVesting = reader.ReadString();
                        break;
                    case "PrincipalAmount":
                        PrincipalAmount = reader.ReadString();
                        break;
                    case "InterestRate":
                        InterestRate = reader.ReadString();
                        break;
                    case "PaymentAmount":
                        PaymentAmount = reader.ReadString();
                        break;
                    case "PITIPaymentAmount":
                        PITIPaymentAmount = reader.ReadString();
                        break;
                    case "MaturityDate":
                        MaturityDate = reader.ReadString();
                        break;
                    case "BalloonPaymentAmount":
                        BalloonPaymentAmount = reader.ReadString();
                        break;
                    case "CurrentBalance":
                        CurrentBalance = reader.ReadString();
                        break;
                    case "TrusteeName":
                        TrusteeName = reader.ReadString();
                        break;
                    case "NoteDate":
                        NoteDate = reader.ReadString();
                        break;
                    case "LenderName":
                        LenderName = reader.ReadString();
                        break;
                    case "AssignedDate":
                        AssignedDate = reader.ReadString();
                        break;
                    case "AssignedToName":
                        AssignedToName = reader.ReadString();
                        break;
                    case "RetainedByName":
                        RetainedByName = reader.ReadString();
                        break;
                    case "PriorRecording":
                        ReadElement(reader, PriorRecordingList);
                        break;
                    case "InvestorLoanIdentifier":
                        InvestorLoanIdentifier = reader.ReadString();
                        break;
                    case "MonthlyMIPaymentAmount":
                        MonthlyMIPaymentAmount = reader.ReadString();
                        break;
                    case "AssignmentRecordingDescription":
                        AssignmentRecordingDescription = reader.ReadString();
                        break;
                    case "LienPriorityOtherDescription":
                        LienPriorityOtherDescription = reader.ReadString();
                        break;
                    case "BorrowerName":
                        BorrowerName = reader.ReadString();
                        break;
                }
                #endregion
            }
        }
        /// <summary>
        /// This won't export if the principal is 0.  Per JJ for opm 84022 
        /// </summary>
        public override void WriteXml(XmlWriter writer)
        {

            #region ******NOTE******* Don't export if principal is 0 per JJ for opm 84022
            decimal principal;
            if (string.IsNullOrEmpty(PrincipalAmount))
            {
                return;
            }
            if(Decimal.TryParse(PrincipalAmount, out principal))
            {
                if (principal == 0.0M)
                {
                    return;
                }
            }
            #endregion 

            writer.WriteStartElement("PriorLien");
            WriteAttribute(writer, "RelatedLoanID", m_relatedLoanId);
            WriteEnumAttribute(writer, "AssumabilityIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator[(int)m_assumabilityIndicator]);
            WriteEnumAttribute(writer, "InDefaultIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator[(int)m_inDefaultIndicator]);
            WriteEnumAttribute(writer, "LienPriority", EnumMappings.E_PriorLienLienPriority[(int) m_lienPriority]);
            WriteEnumAttribute(writer, "LoanPurposeType", EnumMappings.E_PriorLienLoanPurposeType[(int)m_loanPurposeType]);
            WriteEnumAttribute(writer, "PrepaymentPenaltyIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator[(int)m_prepaymentPenaltyIndicator]);
            WriteEnumAttribute(writer, "RelationshipToSubjectLoanType", EnumMappings.E_PriorLienRelationshipToSubjectLoanType[(int)m_relationshipToSubjectLoanType]);
            WriteElementString(writer, "MortgageDate", m_mortgageDate);
            WriteElementString(writer, "BorrowerVesting", m_borrowerVesting);
            WriteElementString(writer, "PrincipalAmount", m_principalAmount);
            WriteElementString(writer, "InterestRate", m_interestRate);
            WriteElementString(writer, "PaymentAmount", m_paymentAmount);
            WriteElementString(writer, "PITIPaymentAmount", m_pitiPaymentAmount);
            WriteElementString(writer, "MaturityDate", m_maturityDate);
            WriteElementString(writer, "BalloonPaymentAmount", m_balloonPaymentAmount);
            WriteElementString(writer, "CurrentBalance", m_currentBalance);
            WriteElementString(writer, "TrusteeName", m_trusteeName);
            WriteElementString(writer, "NoteDate", m_noteDate);
            WriteElementString(writer, "LenderName", m_lenderName);
            WriteElementString(writer, "AssignedDate", m_assignedDate);
            WriteElementString(writer, "AssignedToName", m_assignedToName);
            WriteElementString(writer, "RetainedByName", m_retainedByName);
            WriteElement(writer, m_priorRecordingList);
            WriteElementString(writer, "InvestorLoanIdentifier", m_investorLoanIdentifier);
            WriteElementString(writer, "MonthlyMIPaymentAmount", m_monthlyMIPaymentAmount);
            WriteElementString(writer, "AssignmentRecordingDescription", m_assignmentRecordingDescription);
            WriteElementString(writer, "LienPriorityOtherDescription", m_lienPriorityOtherDescription);
            WriteElementString(writer, "BorrowerName", m_borrowerName);
            writer.WriteEndElement();
        }
    }

    public enum E_PriorLienLienPriority
    {
        Undefined
        , _1st
        , _2nd
        , _3rd
        , _4th
    }

    public enum E_PriorLienLoanPurposeType
    {
        Undefined
        , ConstructionOnly
        , ConstructionToPermanent
        , Other
        , MortgageModification
        , Purchase
        , Refinance
        , Unknown
    }

    public enum E_PriorLienRelationshipToSubjectLoanType
    {
        Undefined
        , ConvertedIntoSubjectLoan
        , ModifiedIntoSubjectLoan
        , Other
        , PriorREODisposition
        , RefinancedBySubjectLoan
        , SeniorLien
        , SubordinateLien
        , Unknown
    }

    static partial class EnumMappings
    {
        public static readonly string[] E_PriorLienLienPriority = {
                        ""
                         , "1st"
                         , "2nd"
                         , "3rd"
                         , "4th"
                         };

        public static readonly string[] E_PriorLienLoanPurposeType = {
                        ""
                        , "ConstructionOnly"
                        , "ConstructionToPermanent"
                        , "Other"
                        , "MortgageModification"
                        , "Purchase"
                        , "Refinance"
                        , "Unknown"
                        };

        public static readonly string[] E_PriorLienRelationshipToSubjectLoanType = {
                        ""
                        , "ConvertedIntoSubjectLoan"
                        , "ModifiedIntoSubjectLoan"
                        , "Other"
                        , "PriorREODisposition"
                        , "RefinancedBySubjectLoan"
                        , "SeniorLien"
                        , "SubordinateLien"
                        , "Unknown"
                        };
    }
}
