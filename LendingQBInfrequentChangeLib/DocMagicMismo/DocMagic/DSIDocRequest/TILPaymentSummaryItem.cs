﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using XmlSerializableCommon;

namespace DocMagic.DsiDocRequest
{
    public class TILPaymentSummaryItem : AbstractXmlSerializable
    {
        public string TILEstimatedTotalPaymentAmount { get; set; }
        public string TILInterestPaymentAmount { get; set; }
        public string TILInterestRatePercent { get; set; }
        public string TILNegativeAmortizationFullPrincipalPaymentAmount { get; set; }
        public string TILNegativeAmortizationMinimumPrincipalPaymentAmount { get; set; }
        public string TILPaymentAdjustmentDate { get; set; }
        public string TILPaymentSummaryTypeOtherDescription { get; set; }
        public string TILPeriodDurationCount { get; set; }
        public string TILPrincipalPaymentAmount { get; set; }
        public string TILTaxesAndInsuranceEscrowPaymentAmount { get; set; }

        public string SequenceNumber { get; set; }
        public E_TILPaymentSummaryItemType TILPaymentSummaryItemType { get; set; }
        public E_TILPaymentSummaryItemPeriodDurationType TILPeriodDurationType { get; set; }


        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "TILPaymentSummaryItem")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            SequenceNumber = ReadAttribute(reader, "SequenceNumber");
            TILPaymentSummaryItemType = (E_TILPaymentSummaryItemType)ReadAttribute(reader, "TILPaymentSummaryItemType", EnumMappings.E_TILPaymentSummaryItemType);
            TILPeriodDurationType = (E_TILPaymentSummaryItemPeriodDurationType)ReadAttribute(reader, "TILPeriodDurationType", EnumMappings.E_TILPaymentSummaryItemPeriodDurationType);

            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "TILPaymentSummaryItem") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                switch (reader.Name)
                {
                    case "TILEstimatedTotalPaymentAmount":
                        TILEstimatedTotalPaymentAmount = reader.ReadString();
                        break;
                    case "TILInterestPaymentAmount":
                        TILInterestPaymentAmount = reader.ReadString();
                        break;
                    case "TILInterestRatePercent":
                        TILInterestRatePercent = reader.ReadString();
                        break;
                    case "TILNegativeAmortizationFullPrincipalPaymentAmount":
                        TILNegativeAmortizationFullPrincipalPaymentAmount = reader.ReadString();
                        break;
                    case "TILNegativeAmortizationMinimumPrincipalPaymentAmount":
                        TILNegativeAmortizationMinimumPrincipalPaymentAmount = reader.ReadString();
                        break;
                    case "TILPaymentAdjustmentDate":
                        TILPaymentAdjustmentDate = reader.ReadString();
                        break;
                    case "TILPaymentSummaryTypeOtherDescription":
                        TILPaymentSummaryTypeOtherDescription = reader.ReadString();
                        break;
                    case "TILPeriodDurationCount":
                        TILPeriodDurationCount = reader.ReadString();
                        break;
                    case "TILPrincipalPaymentAmount":
                        TILPrincipalPaymentAmount = reader.ReadString();
                        break;

                    case "TILTaxesAndInsuranceEscrowPaymentAmount":
                        TILTaxesAndInsuranceEscrowPaymentAmount = reader.ReadString();
                        break;

                }
                #endregion
            }
        }

        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("TILPaymentSummaryItem");
            WriteAttribute(writer, "SequenceNumber", SequenceNumber);
            WriteEnumAttribute(writer, "TILPaymentSummaryItemType", EnumMappings.E_TILPaymentSummaryItemType[(int)TILPaymentSummaryItemType]);
            WriteEnumAttribute(writer, "TILPeriodDurationType", EnumMappings.E_TILPaymentSummaryItemPeriodDurationType[(int)TILPeriodDurationType]);

            WriteElementString(writer, "TILEstimatedTotalPaymentAmount", TILEstimatedTotalPaymentAmount);
            WriteElementString(writer, "TILInterestPaymentAmount", TILInterestPaymentAmount);
            WriteElementString(writer, "TILInterestRatePercent", TILInterestRatePercent);
            WriteElementString(writer, "TILNegativeAmortizationFullPrincipalPaymentAmount", TILNegativeAmortizationFullPrincipalPaymentAmount);
            WriteElementString(writer, "TILNegativeAmortizationMinimumPrincipalPaymentAmount", TILNegativeAmortizationMinimumPrincipalPaymentAmount);
            WriteElementString(writer, "TILPaymentAdjustmentDate", TILPaymentAdjustmentDate);
            WriteElementString(writer, "TILPaymentSummaryTypeOtherDescription", TILPaymentSummaryTypeOtherDescription);
            WriteElementString(writer, "TILPeriodDurationCount", TILPeriodDurationCount);
            WriteElementString(writer, "TILPrincipalPaymentAmount", TILPrincipalPaymentAmount);
            WriteElementString(writer, "TILTaxesAndInsuranceEscrowPaymentAmount", TILTaxesAndInsuranceEscrowPaymentAmount);

            writer.WriteEndElement();
        }
    }

    public enum E_TILPaymentSummaryItemType
    {
        Undefined,
        AtFirstAdjustment,
        AtSecondAdjustment,
        Introductory,
        MaximumEver,
        MaximumFirstFiveYears
    }

    public enum E_TILPaymentSummaryItemPeriodDurationType
    {
        Undefined
        , Biweekly
        , Monthly
        , Yearly
    }

    static partial class EnumMappings
    {
        public static readonly string[] E_TILPaymentSummaryItemType = {
                                                                          ""
                                                                          ,"AtFirstAdjustment"
                                                                          ,"AtSecondAdjustment"
                                                                          ,"Introductory"
                                                                          ,"MaximumEver"
                                                                          ,"MaximumFirstFiveYears"
                                                                      };
        public static readonly string[] E_TILPaymentSummaryItemPeriodDurationType = {
                                                                                        ""
                                                                                        ,"Biweekly"
                                                                                        ,"Monthly"
                                                                                        ,"Yearly"
                                                                                    };
    }

}
