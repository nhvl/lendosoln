// Generated by CodeMonkey on 2/7/2009 11:54:44 AM
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace DocMagic.DsiDocRequest
{
    public class UserService : AbstractXmlSerializable
    {
        #region Private Member Variables
        private E_UserServiceType m_type = E_UserServiceType.Undefined;
        private E_YesNo m_enable = E_YesNo.Undefined;
        #endregion

        #region Public Properties
        public E_UserServiceType Type
        {
            get { return m_type; }
            set { m_type = value; }
        }
        public E_YesNo Enable
        {
            get { return m_enable; }
            set { m_enable = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "UserService")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_type = (E_UserServiceType) ReadAttribute(reader, "type", EnumMappings.E_UserServiceType);
            m_enable = (E_YesNo) ReadAttribute(reader, "enable", XmlSerializableCommon.EnumMappings.E_YesNo);
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "UserService") return;

            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("UserService");
            WriteEnumAttribute(writer, "type", EnumMappings.E_UserServiceType[(int) m_type]);
            WriteEnumAttribute(writer, "enable", XmlSerializableCommon.EnumMappings.E_YesNo[(int) m_enable]);
            writer.WriteEndElement();
        }
    }

    public enum E_UserServiceType
    {
        Undefined
        , docmagic
        , docmagic_process
    }

    static partial class EnumMappings
    {
        public static readonly string[] E_UserServiceType = {
                        ""
                         , "docmagic"
                         , "docmagic_process"
                         };
    }
}
