// Generated by CodeMonkey on 2/7/2009 11:54:44 AM
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace DocMagic.DsiDocRequest
{
    public class Flood : AbstractXmlSerializable
    {
        #region Private Member Variables
        private E_FloodNfipCommunityParticipationStatusType m_nfipCommunityParticipationStatusType = E_FloodNfipCommunityParticipationStatusType.Undefined;
        private string m_nfipCommunityName = string.Empty;
        #endregion

        #region Public Properties
        public E_FloodNfipCommunityParticipationStatusType NfipCommunityParticipationStatusType
        {
            get { return m_nfipCommunityParticipationStatusType; }
            set { m_nfipCommunityParticipationStatusType = value; }
        }
        public string NfipCommunityName
        {
            get { return m_nfipCommunityName; }
            set { m_nfipCommunityName = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "Flood")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_nfipCommunityParticipationStatusType = (E_FloodNfipCommunityParticipationStatusType) ReadAttribute(reader, "NFIPCommunityParticipationStatusType", EnumMappings.E_FloodNfipCommunityParticipationStatusType);
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "Flood") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                switch (reader.Name)
                {
                    case "NFIPCommunityName":
                        NfipCommunityName = reader.ReadString();
                        break;
                }
                #endregion
            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("Flood");
            WriteEnumAttribute(writer, "NFIPCommunityParticipationStatusType", EnumMappings.E_FloodNfipCommunityParticipationStatusType[(int) m_nfipCommunityParticipationStatusType]);
            WriteElementString(writer, "NFIPCommunityName", m_nfipCommunityName);
            writer.WriteEndElement();
        }
    }

    public enum E_FloodNfipCommunityParticipationStatusType
    {
        Undefined
        , Emergency
        , _Non_Participating
        , Probation
        , Regular
        , Suspended
        , Unknown
    }

    static partial class EnumMappings
    {
        public static readonly string[] E_FloodNfipCommunityParticipationStatusType = {
                        ""
                         , "Emergency"
                         , "Non-Participating"
                         , "Probation"
                         , "Regular"
                         , "Suspended"
                         , "Unknown"
                         };
    }
}
