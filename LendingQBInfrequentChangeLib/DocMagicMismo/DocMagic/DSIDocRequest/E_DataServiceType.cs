﻿namespace DocMagic.DsiDocRequest
{
    /// <summary>
    /// Indicates what Data Service feature is being used.
    /// </summary>
    public enum E_DataServiceType
    {
        /// <summary>
        /// A blank option.
        /// </summary>
        LeaveBlank,

        /// <summary>
        /// Indicates a call to UCD Data Service methods.
        /// </summary>
        UCD,
    }
}
