﻿namespace DocMagic.DsiDocRequest
{
    using System;
    using System.Xml;
    using XmlSerializableCommon;

    public class DataServiceRequest : AbstractXmlSerializable
    {
        private E_DataServiceType dataServiceType;

        private DataServiceUCD dataServiceUcd;

        public E_DataServiceType DataServiceType
        {
            get { return this.dataServiceType; }
            set { this.dataServiceType = value; }
        }

        public DataServiceUCD DataServiceUcd
        {
            get
            {
                if (this.dataServiceUcd == null)
                {
                    this.dataServiceUcd = new DataServiceUCD();
                }

                return this.dataServiceUcd;
            }

            set
            {
                this.dataServiceUcd = value;
            }
        }

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != nameof(DataServiceRequest))
            {
                reader.Read();
                if (reader.EOF)
                {
                    return;
                }
            }

            this.DataServiceType = (E_DataServiceType)this.ReadAttribute(reader, nameof(dataServiceType), EnumMappings.E_DataServiceType);

            if (reader.IsEmptyElement)
            {
                return;
            }

            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == nameof(DataServiceRequest))
                {
                    return;
                }

                if (reader.NodeType != XmlNodeType.Element)
                {
                    continue;
                }

                switch (reader.Name)
                {
                    case nameof(DataServiceUCD):
                        this.ReadElement(reader, this.DataServiceUcd);
                        break;
                    default:
                        throw new ArgumentException($"Element name {reader.Name} is not handled.");
                }
            }
        }

        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement(nameof(DataServiceRequest));
            this.WriteAttribute(writer, nameof(dataServiceType), EnumMappings.E_DataServiceType[(int)this.DataServiceType]);
            this.WriteElement(writer, this.DataServiceUcd);
            writer.WriteEndElement();
        }
    }

    static partial class EnumMappings
    {
        public static readonly string[] E_DataServiceType =
        {
            string.Empty,
            "UCD"
        };
    }
}
