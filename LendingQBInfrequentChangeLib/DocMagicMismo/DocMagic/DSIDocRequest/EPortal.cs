
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace DocMagic.DsiDocRequest
{
    public class EPortal : AbstractXmlSerializable
    {
        #region Private Member Variables
        private List<EventNotification> m_eventNotificationList = null;
        private E_YNIndicator m_clickSign = E_YNIndicator.Undefined;
        private E_YNIndicator m_borrowerMobileService = E_YNIndicator.Undefined;
        private E_YNIndicator m_disableSigningInvitationIndicator = E_YNIndicator.Undefined;
        private E_YNIndicator m_enablePreviewIndicator = E_YNIndicator.Undefined;
        #endregion

        #region Public Properties
        public List<EventNotification> EventNotificationList
        {
            get
            {
                if (null == m_eventNotificationList) m_eventNotificationList = new List<EventNotification>();
                return m_eventNotificationList;
            }
        }
        public E_YNIndicator ClickSign
        {
            get { return m_clickSign; }
            set { m_clickSign = value; }
        }
        public E_YNIndicator BorrowerMobileService
        {
            get { return m_borrowerMobileService; }
            set { m_borrowerMobileService = value; }
        }
        public E_YNIndicator DisableSigningInvitationIndicator
        {
            get { return m_disableSigningInvitationIndicator; }
            set { m_disableSigningInvitationIndicator = value; }
        }
        public E_YNIndicator EnablePreviewIndicator
        {
            get { return m_enablePreviewIndicator; }
            set { m_enablePreviewIndicator = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "EPortal")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_clickSign = (E_YNIndicator)ReadAttribute(reader, "clickSign", XmlSerializableCommon.EnumMappings.E_YNIndicator);
            m_borrowerMobileService = (E_YNIndicator)ReadAttribute(reader, "borrowerMobileService", XmlSerializableCommon.EnumMappings.E_YNIndicator);
            m_disableSigningInvitationIndicator = (E_YNIndicator)ReadAttribute(reader, "disableSigningInvitationIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator);
            m_enablePreviewIndicator = (E_YNIndicator)ReadAttribute(reader, "enablePreviewIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator);
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "EPortal") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                switch (reader.Name)
                {
                    case "EventNotification":
                        ReadElement(reader, EventNotificationList);
                        break;
                }
                #endregion
            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("EPortal");
            WriteEnumAttribute(writer, "clickSign", XmlSerializableCommon.EnumMappings.E_YNIndicator[(int)m_clickSign]);
            WriteEnumAttribute(writer, "borrowerMobileService", XmlSerializableCommon.EnumMappings.E_YNIndicator[(int)m_borrowerMobileService]);
            WriteEnumAttribute(writer, "disableSigningInvitationIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator[(int)m_disableSigningInvitationIndicator]);
            WriteEnumAttribute(writer, "enablePreviewIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator[(int)m_enablePreviewIndicator]);
            WriteElement(writer, m_eventNotificationList);
            writer.WriteEndElement();
        }
    }
}
