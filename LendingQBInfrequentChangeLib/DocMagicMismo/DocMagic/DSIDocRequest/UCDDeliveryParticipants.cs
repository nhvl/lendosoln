﻿namespace DocMagic.DsiDocRequest
{
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using XmlSerializableCommon;

    public class UCDDeliveryParticipants : AbstractXmlSerializable
    {
        private List<UCDDeliveryParticipant> ucdDeliveryParticipantList;

        public List<UCDDeliveryParticipant> UCDDeliveryParticipantList
        {
            get
            {
                if (this.ucdDeliveryParticipantList == null)
                {
                    this.ucdDeliveryParticipantList = new List<UCDDeliveryParticipant>();
                }

                return this.ucdDeliveryParticipantList;
            }

            set
            {
                this.ucdDeliveryParticipantList = value;
            }
        }

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != nameof(UCDDeliveryParticipants))
            {
                reader.Read();
                if (reader.EOF)
                {
                    return;
                }
            }

            if (reader.IsEmptyElement)
            {
                return;
            }

            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == nameof(UCDDeliveryParticipants))
                {
                    return;
                }

                if (reader.NodeType != XmlNodeType.Element)
                {
                    continue;
                }

                switch (reader.Name)
                {
                    case nameof(UCDDeliveryParticipant):
                        this.ReadElement(reader, this.UCDDeliveryParticipantList);
                        break;
                    default:
                        throw new ArgumentException($"Element {reader.Name} is not handled.");
                }
            }
        }

        public override void WriteXml(XmlWriter writer)
        {
            if (this.UCDDeliveryParticipantList != null && this.UCDDeliveryParticipantList.Count > 0)
            {
                writer.WriteStartElement(nameof(UCDDeliveryParticipants));
                this.WriteElement(writer, this.UCDDeliveryParticipantList);
                writer.WriteEndElement();
            }
        }
    }
}
