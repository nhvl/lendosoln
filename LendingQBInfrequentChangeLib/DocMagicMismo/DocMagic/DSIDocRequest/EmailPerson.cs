
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace DocMagic.DsiDocRequest
{
    public class EmailPerson : AbstractXmlSerializable
    {
        #region Private Member Variables
        private List<string> m_emailAddressList = null;
        private E_EmailPersonType m_type = E_EmailPersonType.To;
        private string m_name = string.Empty;
        #endregion

        #region Public Properties
        public List<string> EmailAddressList
        {
            get
            {
                if (null == m_emailAddressList) m_emailAddressList = new List<string>();
                return m_emailAddressList;
            }
        }
        public E_EmailPersonType Type
        {
            get { return m_type; }
            set { m_type = value; }
        }
        public string Name
        {
            get { return m_name; }
            set { m_name = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "EmailPerson")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_type = (E_EmailPersonType)ReadAttribute(reader, "type", EnumMappings.E_EmailPersonType);
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "EmailPerson") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                switch (reader.Name)
                {
                    case "EmailAddress":
                        EmailAddressList.Add(reader.ReadString());
                        break;
                    case "Name":
                        Name = reader.ReadString();
                        break;
                }
                #endregion
            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("EmailPerson");
            WriteEnumAttribute(writer, "type", EnumMappings.E_EmailPersonType[(int)m_type]);
            WriteElementString(writer, "EmailAddress", m_emailAddressList);
            WriteElementString(writer, "Name", m_name);
            writer.WriteEndElement();
        }
    }

    public enum E_EmailPersonType
    {
        Undefined
        , To
        , CC
    }

    static partial class EnumMappings
    {
        public static readonly string[] E_EmailPersonType = {
                        ""
                         , "To"
                         , "CC"
                         };
    }
}
