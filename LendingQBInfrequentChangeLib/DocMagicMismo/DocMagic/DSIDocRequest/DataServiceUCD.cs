﻿namespace DocMagic.DsiDocRequest
{
    using System;
    using System.Xml;
    using DocMagic.Common;
    using XmlSerializableCommon;

    public class DataServiceUCD : AbstractXmlSerializable
    {
        private E_DataServiceActionType dataServiceActionType;

        private E_InvestorType investorType;

        private E_EncodingType requestedContentEncodingType;

        private string docCode;

        private InvestorDelivery investorDelivery;

        public E_DataServiceActionType DataServiceActionType
        {
            get { return this.dataServiceActionType; }
            set { this.dataServiceActionType = value; }
        }

        public E_InvestorType InvestorType
        {
            get { return this.investorType; }
            set { this.investorType = value; }
        }

        public E_EncodingType RequestedContentEncodingType
        {
            get { return this.requestedContentEncodingType; }
            set { this.requestedContentEncodingType = value; }
        }

        public string DocCode
        {
            get { return this.docCode; }
            set { this.docCode = value; }
        }

        public InvestorDelivery InvestorDelivery
        {
            get
            {
                if (this.investorDelivery == null)
                {
                    this.investorDelivery = new InvestorDelivery();
                }

                return this.investorDelivery;
            }

            set
            {
                this.investorDelivery = value;
            }
        }

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != nameof(DataServiceUCD))
            {
                reader.Read();
                if (reader.EOF)
                {
                    return;
                }
            }

            this.DataServiceActionType = (E_DataServiceActionType)this.ReadAttribute(reader, nameof(this.dataServiceActionType), EnumMappings.E_DataServiceActionType);
            this.InvestorType = (E_InvestorType)this.ReadAttribute(reader, nameof(this.investorType), EnumMappings.E_InvestorType);
            this.RequestedContentEncodingType = (E_EncodingType)this.ReadAttribute(reader, nameof(this.requestedContentEncodingType), EnumMappings.E_EncodingType);
            
            if (reader.IsEmptyElement)
            {
                return;
            }

            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == nameof(DataServiceUCD))
                {
                    return;
                }

                if (reader.NodeType != XmlNodeType.Element)
                {
                    continue;
                }

                switch (reader.Name)
                {
                    case nameof(DocCode):
                        this.DocCode = reader.ReadString();
                        break;
                    case nameof(InvestorDelivery):
                        this.ReadElement(reader, this.InvestorDelivery);
                        break;
                    default:
                        throw new ArgumentException($"Element {reader.Name} is not handled.");
                }
            }
        }

        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement(nameof(DataServiceUCD));
            this.WriteEnumAttribute(writer, nameof(dataServiceActionType), EnumMappings.E_DataServiceActionType[(int)this.DataServiceActionType]);
            this.WriteEnumAttribute(writer, nameof(investorType), EnumMappings.E_InvestorType[(int)this.InvestorType]);
            this.WriteEnumAttribute(writer, nameof(requestedContentEncodingType), EnumMappings.E_EncodingType[(int)this.RequestedContentEncodingType]);
            this.WriteElementString(writer, nameof(this.DocCode), this.DocCode, true);
            this.WriteElement(writer, this.investorDelivery);
            writer.WriteEndElement();
        }
    }

    static partial class EnumMappings
    {
        public static readonly string[] E_DataServiceActionType = {
            string.Empty,
            "Create",
            "CreateAndDeliver"
        };

        public static readonly string[] E_InvestorType = {
            string.Empty,
            "None",
            "FNM",
            "FRE"
        };

        public static readonly string[] E_EncodingType = {
            string.Empty,
            "None",
            "Base64"
        };
    }
}
