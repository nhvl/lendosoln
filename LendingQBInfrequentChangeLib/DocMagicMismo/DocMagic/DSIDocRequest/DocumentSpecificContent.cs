﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using XmlSerializableCommon;
namespace DocMagic.DsiDocRequest
{
    public class DocumentSpecificContent : AbstractXmlSerializable
    {
        private LoanOptionsDisclosure m_loanOptionsDisclosure = null;
        public LoanOptionsDisclosure LoanOptionsDisclosure
        {
            get
            {
                if (m_loanOptionsDisclosure == null)
                {
                    m_loanOptionsDisclosure = new LoanOptionsDisclosure();
                }
                return m_loanOptionsDisclosure;
            }
        }

        private TILDisclosure m_tilDisclosure = null;
        public TILDisclosure TILDisclosure
        {
            get
            {
                if (m_tilDisclosure == null)
                {
                    m_tilDisclosure = new TILDisclosure();
                }
                return m_tilDisclosure;
            }
        }
        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "DocumentSpecificContent")
            {
                reader.Read();
                if (reader.EOF) return;
            }

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "DocumentSpecificContent") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                switch (reader.Name)
                {
                    case "LoanOptionsDisclosure":
                        ReadElement(reader, LoanOptionsDisclosure);
                        break;
                    case "TILDisclosure":
                        ReadElement(reader, TILDisclosure);
                        break;
                }
                #endregion
            }
        }

        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("DocumentSpecificContent");
            WriteElement(writer, m_tilDisclosure);
            WriteElement(writer, m_loanOptionsDisclosure);
            writer.WriteEndElement();
        }
    }
}
