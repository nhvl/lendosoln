// Generated by CodeMonkey on 2/7/2009 11:54:44 AM
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace DocMagic.DsiDocRequest
{
    public class FloodInfo : AbstractXmlSerializable
    {
        #region Private Member Variables
        private E_YesNo m_lifeOfLoan = E_YesNo.Yes;
        private string m_vendor = string.Empty;
        #endregion

        #region Public Properties
        public E_YesNo LifeOfLoan
        {
            get { return m_lifeOfLoan; }
            set { m_lifeOfLoan = value; }
        }
        public string Vendor
        {
            get { return m_vendor; }
            set { m_vendor = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "FloodInfo")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_lifeOfLoan = (E_YesNo) ReadAttribute(reader, "lifeOfLoan", XmlSerializableCommon.EnumMappings.E_YesNo);
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "FloodInfo") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                switch (reader.Name)
                {
                    case "Vendor":
                        Vendor = reader.ReadString();
                        break;
                }
                #endregion
            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("FloodInfo");
            WriteEnumAttribute(writer, "lifeOfLoan", XmlSerializableCommon.EnumMappings.E_YesNo[(int) m_lifeOfLoan]);
            WriteElementString(writer, "Vendor", m_vendor);
            writer.WriteEndElement();
        }
    }
}
