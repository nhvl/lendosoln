﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using XmlSerializableCommon;

namespace DocMagic.DsiDocRequest
{
    public class VaBorrower : AbstractXmlSerializable
    {
        #region Private Member Variables
        private List<MilitaryService> m_militaryServiceList = null;
        private E_YNIndicator m_VaBenefitRelatedIndebtednessIndicator = E_YNIndicator.Undefined;
        private E_YNIndicator m_VaDisabilityBenefitClaimIndicator = E_YNIndicator.Undefined;
        private E_YNIndicator m_PreviousVaHomeLoanIndicator = E_YNIndicator.Undefined;
        #endregion

        #region Public Properties
        public List<MilitaryService> MilitaryServiceList
        {
            get
            {
                if (null == m_militaryServiceList) m_militaryServiceList = new List<MilitaryService>();
                return m_militaryServiceList;
            }
        }
        public string ActiveDutyOnDayFollowingDateOfSeparationIndicator { get; set; }
        public string SeparatedFromMilitaryDueToDisabilityIndicator { get; set; }

        public E_YNIndicator VaBenefitRelatedIndebtednessIndicator
        {
            get { return m_VaBenefitRelatedIndebtednessIndicator; }
            set { m_VaBenefitRelatedIndebtednessIndicator = value; }
        }
        public E_YNIndicator VaDisabilityBenefitClaimIndicator
        {
            get { return m_VaDisabilityBenefitClaimIndicator; }
            set { m_VaDisabilityBenefitClaimIndicator = value; }
        }
        public E_YNIndicator PreviousVaHomeLoanIndicator
        {
            get { return m_PreviousVaHomeLoanIndicator; }
            set { m_PreviousVaHomeLoanIndicator = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "VABorrower")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            ActiveDutyOnDayFollowingDateOfSeparationIndicator = ReadAttribute(reader, "ActiveDutyOnDayFollowingDateOfSeparationIndicator");
            SeparatedFromMilitaryDueToDisabilityIndicator = ReadAttribute(reader, "SeparatedFromMilitaryDueToDisabilityIndicator");
            m_VaBenefitRelatedIndebtednessIndicator = (E_YNIndicator)ReadAttribute(reader, "VABenefitRelatedIndebtednessIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator);
            m_VaDisabilityBenefitClaimIndicator = (E_YNIndicator)ReadAttribute(reader, "VADisabilityBenefitClaimIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator);
            m_PreviousVaHomeLoanIndicator = (E_YNIndicator)ReadAttribute(reader, "PreviousVAHomeLoanIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator);
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "VABorrower") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                switch (reader.Name)
                {
                    case "MilitaryService":
                        ReadElement(reader, MilitaryServiceList);
                        break;
                }
                #endregion
            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("VABorrower");
            WriteAttribute(writer, "ActiveDutyOnDayFollowingDateOfSeparationIndicator", ActiveDutyOnDayFollowingDateOfSeparationIndicator);
            WriteAttribute(writer, "SeparatedFromMilitaryDueToDisabilityIndicator", SeparatedFromMilitaryDueToDisabilityIndicator);
            WriteEnumAttribute(writer, "VABenefitRelatedIndebtednessIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator[(int)m_VaBenefitRelatedIndebtednessIndicator]);
            WriteEnumAttribute(writer, "VADisabilityBenefitClaimIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator[(int)m_VaDisabilityBenefitClaimIndicator]);
            WriteEnumAttribute(writer, "PreviousVAHomeLoanIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator[(int)m_PreviousVaHomeLoanIndicator]);
            WriteElement(writer, MilitaryServiceList);
            writer.WriteEndElement();
        }


    }
}
