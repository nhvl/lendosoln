﻿// <copyright file="PartyContact.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Justin Lara
//  Date:   July 21, 2015
// </summary>

namespace DocMagic.DsiDocRequest
{
    using System.Xml;
    using XmlSerializableCommon;

    public class PartyContact : AbstractXmlSerializable
    {
        private string contactName = string.Empty;
        private ContactPoint contactPoint = null;
        private string licenseInfo = string.Empty;

        public string ContactName
        {
            get { return contactName; }
            set { contactName = value; }
        }

        public ContactPoint ContactPoint
        {
            get { return contactPoint; }
            set { contactPoint = value; }
        }

        public string LicenseInfo
        {
            get { return licenseInfo; }
            set { licenseInfo = value; }
        }


        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "PartyRepresentative")
            {
                reader.Read();
                if (reader.EOF) return;
            }

            if (reader.IsEmptyElement) return;

            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "PartyRepresentative")
                {
                    return;
                }
                else if (reader.NodeType != XmlNodeType.Element)
                {
                    continue;
                }
                
                switch (reader.Name)
                {
                    case "ContactName":
                        ContactName = reader.ReadString();
                        break;
                    case "ContactPoint":
                        ReadElement(reader, ContactPoint);
                        break;
                    case "LicenseInfo":
                        LicenseInfo = reader.ReadString();
                        break;
                }
            }
        }

        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("PartyContact");
            WriteElementString(writer, "ContactName", contactName);
            WriteElement(writer, ContactPoint);
            WriteElementString(writer, "LicenseInfo", licenseInfo);
            writer.WriteEndElement();
        }
    }
}