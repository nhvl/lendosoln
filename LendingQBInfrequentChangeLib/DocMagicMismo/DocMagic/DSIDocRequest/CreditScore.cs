﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using XmlSerializableCommon;

namespace DocMagic.DsiDocRequest
{
    public class CreditScore : AbstractXmlSerializable
    {
        public string CreditScoreId { get; set; }

        public string CreditScoreRankPercent { get; set; }

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "CreditScore")
            {
                reader.Read();
                if (reader.EOF) return;
            }

            #region Read Attributes
            CreditScoreId = ReadAttribute(reader, "CreditScoreID");
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "CreditScore") return;
                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                switch (reader.Name)
                {
                    case "CreditScoreRankPercent":
                        CreditScoreRankPercent = reader.ReadElementString();
                        break;
                }
                #endregion
            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("CreditScore");
            WriteAttribute(writer, "CreditScoreID", CreditScoreId);
            if (string.IsNullOrEmpty(CreditScoreRankPercent) == false)
            {
                writer.WriteElementString("CreditScoreRankPercent", CreditScoreRankPercent);
            }
            writer.WriteEndElement();
        }
    }
}
