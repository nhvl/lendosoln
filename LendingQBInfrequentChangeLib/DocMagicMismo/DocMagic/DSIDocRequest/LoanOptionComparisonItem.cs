﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using XmlSerializableCommon;
namespace DocMagic.DsiDocRequest
{
    public class LoanOptionComparisonItem : AbstractXmlSerializable
    {
        public string LenderName { get; set; }
        public string LoanProgramName { get; set; }
        public string InterestRatePercent { get; set; }
        public string OriginationFeeAmount { get; set; }
        public string DiscountPointsPercent { get; set; }
        public string PrepaymentFeeAmount { get; set; }
        public string NegativeAmortization { get; set; }
        public string BalloonTermMonths { get; set; }

        public E_LoanOptionComparisonItemComparisonType ComparisonType { get; set; }
        public E_YNIndicator DemandFeatureIndicator { get; set; }
        public E_YNIndicator SharedAppreciationFeatureIndicator { get; set; }
        public E_YNIndicator SharedEquityFeatureIndicator { get; set; }

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "LoanOptionComparisonItem")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            ComparisonType = (E_LoanOptionComparisonItemComparisonType)ReadAttribute(reader, "ComparisonType", EnumMappings.E_LoanOptionComparisonItemComparisonType);
            DemandFeatureIndicator = (E_YNIndicator)ReadAttribute(reader, "DemandFeatureIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator);
            SharedAppreciationFeatureIndicator = (E_YNIndicator)ReadAttribute(reader, "SharedAppreciationFeatureIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator);
            SharedEquityFeatureIndicator = (E_YNIndicator)ReadAttribute(reader, "SharedEquityFeatureIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator);

            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "LoanOptionComparisonItem") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                switch (reader.Name)
                {
                    case "LenderName":
                        LenderName = reader.ReadString();
                        break;
                    case "LoanProgramName":
                        LoanProgramName = reader.ReadString();
                        break;
                    case "InterestRatePercent":
                        InterestRatePercent = reader.ReadString();
                        break;
                    case "OriginationFeeAmount":
                        OriginationFeeAmount = reader.ReadString();
                        break;
                    case "DiscountPointsPercent":
                        DiscountPointsPercent = reader.ReadString();
                        break;
                    case "PrepaymentFeeAmount":
                        PrepaymentFeeAmount = reader.ReadString();
                        break;
                    case "NegativeAmortization":
                        NegativeAmortization = reader.ReadString();
                        break;
                    case "BalloonTermMonths":
                        BalloonTermMonths = reader.ReadString();
                        break;
                }
                #endregion
            }
        }

        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("LoanOptionComparisonItem");
            WriteEnumAttribute(writer, "ComparisonType", EnumMappings.E_LoanOptionComparisonItemComparisonType[(int)ComparisonType]);
            WriteEnumAttribute(writer, "DemandFeatureIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator[(int)DemandFeatureIndicator]);
            WriteEnumAttribute(writer, "SharedAppreciationFeatureIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator[(int)SharedAppreciationFeatureIndicator]);
            WriteEnumAttribute(writer, "SharedEquityFeatureIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator[(int)SharedEquityFeatureIndicator]);
            WriteElementString(writer, "LenderName", LenderName);
            WriteElementString(writer, "LoanProgramName", LoanProgramName);
            WriteElementString(writer, "InterestRatePercent", InterestRatePercent);
            WriteElementString(writer, "OriginationFeeAmount", OriginationFeeAmount);
            WriteElementString(writer, "DiscountPointsPercent", DiscountPointsPercent);
            WriteElementString(writer, "PrepaymentFeeAmount", PrepaymentFeeAmount);
            WriteElementString(writer, "NegativeAmortization", NegativeAmortization);
            WriteElementString(writer, "BalloonTermMonths", BalloonTermMonths);

            writer.WriteEndElement();

        }

    }
    public enum E_LoanOptionComparisonItemComparisonType
    {
        Undefined
        , LowestInterestRate
        , LowestFees
        , LowestInterestRateWithNoRiskyFeatures
    }
    static partial class EnumMappings
    {
        public static readonly string[] E_LoanOptionComparisonItemComparisonType = {
                        ""
                         , "LowestInterestRate"
                         , "LowestFees"
                         , "LowestInterestRateWithNoRiskyFeatures"
                         };
    }

}
