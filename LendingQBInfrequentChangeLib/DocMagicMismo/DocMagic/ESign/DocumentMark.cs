﻿using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;

namespace DocMagic.ESign
{
    public class DocumentMark : AbstractXmlSerializable
    {
        #region Private Member Variables

        private string m_signerId = "";
        private E_DocumentMarkType m_documentMarkType = E_DocumentMarkType.Initial;
        private string m_OffsetMeasurementUnitType = "Pixels";
        private int m_OffsetFromLeftNumber = 0;
        private int m_OffsetFromTopNumber = 0;
        private int m_PageNumberCount = 0;


        
        #endregion

        #region Public Member Variables

        public string SignerId
        {
            get { return m_signerId; }
            set { m_signerId = value; }
        }

        public E_DocumentMarkType DocumentMarkType
        {
            get { return m_documentMarkType; }
            set { m_documentMarkType = value; }
        }

        public string OffsetMeasurementUnitType  
        {
            get { return m_OffsetMeasurementUnitType; }
            set { m_OffsetMeasurementUnitType = value; }
        }

        public int OffsetFromLeftNumber
        {
            get { return m_OffsetFromLeftNumber; }
            set { m_OffsetFromLeftNumber = value; }
        }

        public int OffsetFromTopNumber
        {
            get { return m_OffsetFromTopNumber; }
            set { m_OffsetFromTopNumber = value; }
        }

        public int PageNumberCount
        {
            get { return m_PageNumberCount; }
            set { m_PageNumberCount = value; }
        }


        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "DocumentMark")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            SignerId = ReadAttribute(reader, "signerId");
            DocumentMarkType = (E_DocumentMarkType)ReadAttribute(reader, "documentMarkType", EnumMappings.E_DocumentMarkType);
            OffsetMeasurementUnitType = ReadAttribute(reader, "offsetMeasurementUnitType");
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "DocumentMark") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                switch (reader.Name)
                {
                    case "OffsetFromLeftNumber":
                        OffsetFromLeftNumber = int.Parse(reader.Value); 
                        break;
                    case "OffsetFromTopNumber":
                        OffsetFromTopNumber = int.Parse(reader.Value);
                        break;
                    case "PageNumberCount":
                        PageNumberCount = int.Parse(reader.Value);
                        break;
                }
                #endregion
            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("DocumentMark");
            WriteAttribute(writer, "signerID", SignerId);
            WriteEnumAttribute(writer, "documentMarkType", EnumMappings.E_DocumentMarkType[(int)DocumentMarkType]);
            WriteAttribute(writer, "offsetMeasurementUnitType", OffsetMeasurementUnitType);
            WriteElementString(writer, "OffsetFromLeftNumber", OffsetFromLeftNumber.ToString());
            WriteElementString(writer, "OffsetFromTopNumber", OffsetFromTopNumber.ToString());
            WriteElementString(writer, "PageNumberCount", PageNumberCount.ToString());
            writer.WriteEndElement();
        }

    }


}
