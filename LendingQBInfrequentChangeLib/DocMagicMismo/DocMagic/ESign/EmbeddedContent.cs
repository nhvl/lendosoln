﻿using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace DocMagic.ESign
{
    public class EmbeddedContent : AbstractXmlSerializable
    {
        #region Private Member Variables
        private E_ContentEncodingType m_ContentEncodingType = E_ContentEncodingType.Base64;
        private string m_Content;  
        #endregion

        #region Public Properties
        public E_ContentEncodingType ContentEncodingType
        {
            get { return m_ContentEncodingType; }
            set { m_ContentEncodingType = value; }
        }

        public string Content
        {
            get { return m_Content; }
            set { m_Content = value; }
        }
        #endregion


        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "EmbeddedContent")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            ContentEncodingType = (E_ContentEncodingType)Enum.Parse(typeof(E_ContentEncodingType), ReadAttribute(reader, "contentEncodingType")); 
            #endregion

            if (reader.IsEmptyElement) return;

            Content = reader.Value;
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("EmbeddedContent");
            WriteAttribute(writer, "contentEncodingType", ContentEncodingType.ToString());
            writer.WriteCData(Content);
            writer.WriteEndElement();
        }
    }
}
