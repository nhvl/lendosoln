﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocMagic.ESign
{
    public enum E_PackageType
    {
        Adverse,
        Closing,
        FloodCertification,
        FormList,
        LoanApplication,
        LoanModification,
        PostClosing,
        Preclosing,
        Predisclosure,
        Prequalification,
        Processing,
        Redisclosure,
        ServicingTransfer,
        Underwriting
    }

    public enum E_ContentEncodingType
    {
        Base64
    }

    public enum E_SignatureType
    {
        ClickSign,
        InkSign,
        None
    }

    public enum E_MIMEType
    {
        ApplicationPdf
    }

    public enum E_DocumentMarkType
    {
        Initial,
        Signature
    }

    public enum E_PackageStatusType
    {
        Created,
        Cancelled,
        Completed,
        Consented,
        Declined,
        InProgress,
        New,
        Printed,
        Reviewed,
        Signed
    }

    public enum E_CategoryType
    {
        Audit,
        Debug,
        Server
    }
    public enum E_MessageType
    {
        Fatal,
        Info,
        Warning
    }

    public enum E_RESPAStatusType
    {
        Unknown,
        Consented,
        DSIPrintedandMailed,
        NotApplicable,
        RequestedClienttoFulfill,
        Waiting,
    }

    static partial class EnumMappings
    {
        public static readonly string[] E_DocumentMarkType = {
                            "Initial",
                            "Signature"
                         };
        public static readonly string[] E_CategoryType = {
                         "Audit",                                          
                         "Debug",
                        };
        public static readonly string[] E_MessageType = {
                        "Fatal",
                        "Info",
                        "Warning"
                                                        };

        public static readonly string[] E_RESPAStatusType = {
                    "Unknown",
                    "Consented",
                    "DSIPrintedandMailed",
                 "NotApplicable",
                  "RequestedClienttoFulfill",
                "Waiting" };
    }
}