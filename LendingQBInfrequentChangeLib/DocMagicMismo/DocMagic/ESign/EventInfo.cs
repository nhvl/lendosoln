﻿using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace DocMagic.ESign
{
    public class EventInfo : AbstractXmlSerializable
    {
        #region Private Member Variables
        private List<Event> m_Event;
        #endregion

        #region Public Properties
        public List<Event> Event
        {
            get
            {
                if (m_Event == null)
                {
                    m_Event = new List<Event>();
                }
                return m_Event;
            }
            set { Event = value; }
        }

        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "EventInfo")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            if (reader.IsEmptyElement) return;

            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "EventInfo") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                switch (reader.Name)
                {
                    case "Event":
                        Event e = new Event();
                        e.ReadXml(reader);
                        Event.Add(e);
                        break;
                }
                #endregion
            }

        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("EventInfo");
            WriteElement(writer, Event);
            writer.WriteEndElement();
        }
    }

}
