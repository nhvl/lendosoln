﻿using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace DocMagic.ESign
{
    public class Document : AbstractXmlSerializable
    {
        #region Private Member Variables
        private string m_DocumentDescription = "";
        private EmbeddedContent m_Content;
        private int m_TotalNumberOfPagesCount = 0;
        private E_SignatureType m_signatureType;
        private E_MIMEType m_MIMEType = E_MIMEType.ApplicationPdf;
        private List<DocumentMark> m_DocumentMarks;
        private string m_TemplateName;
        
        #endregion

        #region Public Properties
        
        public string DocumentDescription
        {
            get { return m_DocumentDescription; }
            set { m_DocumentDescription = value; }
        }

        public EmbeddedContent Content
        {
            get
            {
                if (m_Content == null)
                {
                    m_Content = new EmbeddedContent();
                }
                return m_Content;
            }

            set { m_Content = value; }
        }

        public int TotalNumberOfPages
        {
            get { return m_TotalNumberOfPagesCount; }
            set { m_TotalNumberOfPagesCount = value; }
        }

        public E_SignatureType SignatureTupe
        {
            get { return m_signatureType; }
            set { m_signatureType = value; }
        }

        public E_MIMEType MIMEType
        {
            get { return m_MIMEType; }
            set { m_MIMEType = value; }
        }

        public List<DocumentMark> DocumentMarks
        {
            get
            {
                if (m_DocumentMarks == null)
                {
                    m_DocumentMarks = new List<DocumentMark>();
                }
                return m_DocumentMarks;
            }
            set { m_DocumentMarks = value; }
        }

        public string TemplateName
        {
            get { return m_TemplateName; }
            set { m_TemplateName = value; }
        }

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "Document")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_signatureType = (E_SignatureType)ReadAttribute(reader, "signatureType", EnumMappings.E_SignatureType); 
            m_MIMEType = (E_MIMEType) ReadAttribute(reader, "MIMEType", EnumMappings.E_MIMEType); 
            #endregion

            if (reader.IsEmptyElement) return;

            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "Document") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements

                switch (reader.Name)
                {
                    case "DocumentDescription":
                        m_DocumentDescription = reader.Value;
                        break;
                    case "EmbeddedContent":
                        ReadElement(reader, Content);
                        break;
                    case "TotalNumberOfPagesCount":
                        m_TotalNumberOfPagesCount = int.Parse(reader.Value);
                        break;
                    case "DocumentMarks":
                        ReadElement(reader, DocumentMarks);
                        break;
                    case "TemplateName":
                        m_TemplateName = reader.Value;
                        break;
                }

                #endregion

            }

        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("Document");
            WriteEnumAttribute(writer, "signatureType", EnumMappings.E_SignatureType[(int)SignatureTupe]);
            WriteEnumAttribute(writer, "MIMEType", EnumMappings.E_MIMEType[(int)MIMEType]); 
            WriteElementString(writer, "DocumentDescription", DocumentDescription);
            WriteElement(writer, Content);
            WriteElementString(writer, "TotalNumberOfPagesCount", TotalNumberOfPages.ToString());
            if (DocumentMarks.Count > 0)
            {
                writer.WriteStartElement("DocumentMarks");
                WriteElement(writer, DocumentMarks);
                writer.WriteEndElement();
            }
            else if (false == string.IsNullOrEmpty(TemplateName))
            {
                WriteElementString(writer, "TemplateName", TemplateName);
            }
            writer.WriteEndElement();
        }

        #endregion

    }

    static partial class EnumMappings
    {
        public static readonly string[] E_MIMEType = {
                        "application/pdf"
                         };
        public static readonly string[] E_SignatureType = {
                        "application/pdf"
                         };
    }
}
