// Generated by CodeMonkey on 4/5/2012 2:02:17 PM
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace DocMagic.ESign
{
    public class Destination : AbstractXmlSerializable
    {
        #region Private Member Variables
        private List<string> m_EmailAddressList = null;
        #endregion

        #region Public Properties
        public List<string> EmailAddressList
        {
            get
            {
                if (null == m_EmailAddressList) m_EmailAddressList = new List<string>();
                return m_EmailAddressList;
            }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "Destination")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "Destination") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                switch (reader.Name)
                {
                    case "EmailAddress":
                        EmailAddressList.Add(reader.ReadString());
                        break;
                }
                #endregion
            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("Destination");
            WriteElementString(writer, "EmailAddress", m_EmailAddressList);
            writer.WriteEndElement();
        }
    }
}
