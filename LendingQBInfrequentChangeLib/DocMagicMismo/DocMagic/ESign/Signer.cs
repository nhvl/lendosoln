﻿// Generated by CodeMonkey on 4/5/2012 2:02:17 PM
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace DocMagic.ESign
{
    public class Signer : AbstractXmlSerializable
    {
        #region Private Member Variables
        private string m_AuthenticationCodeValue;
        private string m_EmailValue;
        private string m_FullName;
        private string m_TokenIdentifier;
        private string m_SignerID;
        #endregion

        #region Public Properties
        public string AuthenticationCodeValue
        {
            get { return m_AuthenticationCodeValue; }
            set { m_AuthenticationCodeValue = value; }
        }
        public string EmailValue
        {
            get { return m_EmailValue; }
            set { m_EmailValue = value; }
        }
        public string FullName
        {
            get { return m_FullName; }
            set { m_FullName = value; }
        }
        public string TokenIdentifier
        {
            get { return m_TokenIdentifier; }
            set { m_TokenIdentifier = value; }
        }
        public string SignerID
        {
            get { return m_SignerID; }
            set { m_SignerID = value; }
        }

        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "Signer")
            {
                reader.Read();
                if (reader.EOF) return;
            }

            #region Read Attributes
            SignerID = ReadAttribute(reader, "signerId");
            #endregion

            if (reader.IsEmptyElement) return;

            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "Signer") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                switch (reader.Name)
                {
                    case "AuthenticationCodeValue":
                        AuthenticationCodeValue = reader.Value;
                        break;
                    case "EmailValue":
                        EmailValue = reader.Value;
                        break;
                    case "FullName":
                        FullName = reader.Value;
                        break;
                    case "TokenIdentifier":
                        TokenIdentifier = reader.Value;
                        break;
                }
                #endregion
            }
        }

        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("Signer");
            WriteAttribute(writer, "signerID", SignerID); 
            WriteElementString(writer,"AuthenticationCodeValue", AuthenticationCodeValue);
            WriteElementString(writer, "EmailValue", EmailValue);
            WriteElementString(writer, "FullName", FullName);
            WriteElementString(writer, "TokenIdentifier", TokenIdentifier);
            writer.WriteEndElement();
        }


    }
}
