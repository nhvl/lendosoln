﻿using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace DocMagic.ESign
{
    public class Event : AbstractXmlSerializable
    {
        #region Private Member Variables
        private DateTime m_EventDate;
        private string m_NoteDescription;
        private string m_eventType;
        #endregion

        #region Public Properties
        public DateTime EventDate
        {
            get { return m_EventDate; }
            set { m_EventDate = value; }
        }
        public string NoteDescription
        {
            get { return m_NoteDescription; }
            set { m_NoteDescription = value; }
        }
        public string eventType
        {
            get { return m_eventType; }
            set { m_eventType = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "Event")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            eventType = ReadAttribute(reader, "eventType");
            #endregion

            if (reader.IsEmptyElement) return;

            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "Event") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                switch (reader.Name)
                {
                    case "EventDate":
                        EventDate = DateTime.Parse(reader.ReadString());
                        break;
                    case "NoteDescription":
                        NoteDescription = reader.ReadString();
                        break;
                }
                #endregion
            }

        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("Event");
            WriteAttribute(writer, "eventType", eventType);
            WriteElementString(writer, "EventDate", EventDate.ToString());
            WriteElementString(writer, "NoteDescription", NoteDescription);
            writer.WriteEndElement();
        }
    }
}
