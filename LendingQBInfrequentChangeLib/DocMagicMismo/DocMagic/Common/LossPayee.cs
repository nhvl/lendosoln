// Generated by CodeMonkey on 2/7/2009 4:24:23 AM
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace DocMagic.Common
{
    public class LossPayee : AbstractXmlSerializable
    {
        #region Private Member Variables
        private string m_lossPayeeName = string.Empty;
        private string m_lossPayeeStreet = string.Empty;
        private string m_lossPayeeCity = string.Empty;
        private string m_lossPayeeState = string.Empty;
        private string m_lossPayeeZip = string.Empty;
        private string m_lossPayeeAssignee = string.Empty;
        #endregion

        #region Public Properties
        public string LossPayeeName
        {
            get { return m_lossPayeeName; }
            set { m_lossPayeeName = value; }
        }
        public string LossPayeeStreet
        {
            get { return m_lossPayeeStreet; }
            set { m_lossPayeeStreet = value; }
        }
        public string LossPayeeCity
        {
            get { return m_lossPayeeCity; }
            set { m_lossPayeeCity = value; }
        }
        public string LossPayeeState
        {
            get { return m_lossPayeeState; }
            set { m_lossPayeeState = value; }
        }
        public string LossPayeeZip
        {
            get { return m_lossPayeeZip; }
            set { m_lossPayeeZip = value; }
        }
        public string LossPayeeAssignee
        {
            get { return m_lossPayeeAssignee; }
            set { m_lossPayeeAssignee = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "LossPayee")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "LossPayee") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                switch (reader.Name)
                {
                    case "LossPayeeName":
                        LossPayeeName = reader.ReadString();
                        break;
                    case "LossPayeeStreet":
                        LossPayeeStreet = reader.ReadString();
                        break;
                    case "LossPayeeCity":
                        LossPayeeCity = reader.ReadString();
                        break;
                    case "LossPayeeState":
                        LossPayeeState = reader.ReadString();
                        break;
                    case "LossPayeeZip":
                        LossPayeeZip = reader.ReadString();
                        break;
                    case "LossPayeeAssignee":
                        LossPayeeAssignee = reader.ReadString();
                        break;
                }
                #endregion
            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("LossPayee");
            WriteElementString(writer, "LossPayeeName", m_lossPayeeName);
            WriteElementString(writer, "LossPayeeStreet", m_lossPayeeStreet);
            WriteElementString(writer, "LossPayeeCity", m_lossPayeeCity);
            WriteElementString(writer, "LossPayeeState", m_lossPayeeState);
            WriteElementString(writer, "LossPayeeZip", m_lossPayeeZip);
            WriteElementString(writer, "LossPayeeAssignee", m_lossPayeeAssignee);
            writer.WriteEndElement();
        }
    }
}
