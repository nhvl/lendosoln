﻿namespace DocMagic.Common
{
    /// <summary>
    /// Indicates an encoding type for the response content.
    /// </summary>
    public enum E_EncodingType
    {
        /// <summary>
        /// A blank option.
        /// </summary>
        LeaveBlank,

        /// <summary>
        /// No encoding.
        /// </summary>
        None,

        /// <summary>
        /// Base64 encoding.
        /// </summary>
        Base64
    }
}
