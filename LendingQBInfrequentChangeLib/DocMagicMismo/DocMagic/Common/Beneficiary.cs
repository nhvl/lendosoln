// Generated by CodeMonkey on 2/7/2009 4:24:23 AM
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace DocMagic.Common
{
    public class Beneficiary : AbstractXmlSerializable
    {
        #region Private Member Variables
        private string m_beneficiaryName = string.Empty;
        private string m_beneficiaryStreet = string.Empty;
        private string m_beneficiaryCity = string.Empty;
        private string m_beneficiaryState = string.Empty;
        private string m_beneficiaryZip = string.Empty;
        private string m_beneficiaryStateOfOrigin = string.Empty;
        private string m_beneficiaryOrganizationType = string.Empty;
        #endregion

        #region Public Properties
        public string BeneficiaryName
        {
            get { return m_beneficiaryName; }
            set { m_beneficiaryName = value; }
        }
        public string BeneficiaryStreet
        {
            get { return m_beneficiaryStreet; }
            set { m_beneficiaryStreet = value; }
        }
        public string BeneficiaryCity
        {
            get { return m_beneficiaryCity; }
            set { m_beneficiaryCity = value; }
        }
        public string BeneficiaryState
        {
            get { return m_beneficiaryState; }
            set { m_beneficiaryState = value; }
        }
        public string BeneficiaryZip
        {
            get { return m_beneficiaryZip; }
            set { m_beneficiaryZip = value; }
        }
        public string BeneficiaryStateOfOrigin
        {
            get { return m_beneficiaryStateOfOrigin; }
            set { m_beneficiaryStateOfOrigin = value; }
        }
        public string BeneficiaryOrganizationType
        {
            get { return m_beneficiaryOrganizationType; }
            set { m_beneficiaryOrganizationType = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "Beneficiary")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "Beneficiary") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                switch (reader.Name)
                {
                    case "BeneficiaryName":
                        BeneficiaryName = reader.ReadString();
                        break;
                    case "BeneficiaryStreet":
                        BeneficiaryStreet = reader.ReadString();
                        break;
                    case "BeneficiaryCity":
                        BeneficiaryCity = reader.ReadString();
                        break;
                    case "BeneficiaryState":
                        BeneficiaryState = reader.ReadString();
                        break;
                    case "BeneficiaryZip":
                        BeneficiaryZip = reader.ReadString();
                        break;
                    case "BeneficiaryStateOfOrigin":
                        BeneficiaryStateOfOrigin = reader.ReadString();
                        break;
                    case "BeneficiaryOrganizationType":
                        BeneficiaryOrganizationType = reader.ReadString();
                        break;
                }
                #endregion
            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("Beneficiary");
            WriteElementString(writer, "BeneficiaryName", m_beneficiaryName);
            WriteElementString(writer, "BeneficiaryStreet", m_beneficiaryStreet);
            WriteElementString(writer, "BeneficiaryCity", m_beneficiaryCity);
            WriteElementString(writer, "BeneficiaryState", m_beneficiaryState);
            WriteElementString(writer, "BeneficiaryZip", m_beneficiaryZip);
            WriteElementString(writer, "BeneficiaryStateOfOrigin", m_beneficiaryStateOfOrigin);
            WriteElementString(writer, "BeneficiaryOrganizationType", m_beneficiaryOrganizationType);
            writer.WriteEndElement();
        }
    }
}
