// Generated by CodeMonkey on 2/7/2009 4:08:11 AM
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace DocMagic.Common
{
    public class QueryGroup : AbstractXmlSerializable
    {
        #region Private Member Variables
        private E_QueryGroupCondition m_condition = E_QueryGroupCondition.Or;
        private List<QueryItem> m_queryItemList = null;
        private List<QueryGroup> m_queryGroupList = null;
        #endregion

        #region Public Properties
        public E_QueryGroupCondition Condition
        {
            get { return m_condition; }
            set { m_condition = value; }
        }
        public List<QueryItem> QueryItemList
        {
            get
            {
                if (null == m_queryItemList) m_queryItemList = new List<QueryItem>();
                return m_queryItemList;
            }
        }
        public List<QueryGroup> QueryGroupList
        {
            get
            {
                if (null == m_queryGroupList) m_queryGroupList = new List<QueryGroup>();
                return m_queryGroupList;
            }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "QueryGroup")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_condition = (E_QueryGroupCondition) ReadAttribute(reader, "condition", EnumMappings.E_QueryGroupCondition);
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "QueryGroup") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                switch (reader.Name)
                {
                    case "QueryItem":
                        ReadElement(reader, QueryItemList);
                        break;
                    case "QueryGroup":
                        ReadElement(reader, QueryGroupList);
                        break;
                }
                #endregion
            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("QueryGroup");
            WriteEnumAttribute(writer, "condition", EnumMappings.E_QueryGroupCondition[(int) m_condition]);
            WriteElement(writer, m_queryItemList);
            WriteElement(writer, m_queryGroupList);
            writer.WriteEndElement();
        }
    }

    public enum E_QueryGroupCondition
    {
        Undefined
        , And
        , Or
    }

    static partial class EnumMappings
    {
        public static readonly string[] E_QueryGroupCondition = {
                        ""
                         , "And"
                         , "Or"
                         };
    }
}
