// Generated by CodeMonkey on 2/7/2009 4:08:11 AM
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace DocMagic.Common
{
    public class QueryItem : AbstractXmlSerializable
    {
        #region Private Member Variables
        private string m_field = string.Empty;
        private E_QueryItemOperator m_operator = E_QueryItemOperator.EQ;
        private E_QueryItemCondition m_condition = E_QueryItemCondition.Or;
        private string m_innerText = string.Empty;
        #endregion

        #region Public Properties
        public string Field
        {
            get { return m_field; }
            set { m_field = value; }
        }
        public E_QueryItemOperator Operator
        {
            get { return m_operator; }
            set { m_operator = value; }
        }
        public E_QueryItemCondition Condition
        {
            get { return m_condition; }
            set { m_condition = value; }
        }
        public string InnerText
        {
            get { return m_innerText; }
            set { m_innerText = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "QueryItem")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_field = ReadAttribute(reader, "field");
            m_operator = (E_QueryItemOperator) ReadAttribute(reader, "operator", EnumMappings.E_QueryItemOperator);
            m_condition = (E_QueryItemCondition) ReadAttribute(reader, "condition", EnumMappings.E_QueryItemCondition);
            #endregion

            if (reader.IsEmptyElement) return;
            m_innerText = reader.ReadString();
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("QueryItem");
            WriteAttribute(writer, "field", m_field);
            WriteEnumAttribute(writer, "operator", EnumMappings.E_QueryItemOperator[(int) m_operator]);
            WriteEnumAttribute(writer, "condition", EnumMappings.E_QueryItemCondition[(int) m_condition]);
            WriteString(writer, m_innerText);
            writer.WriteEndElement();
        }
    }

    public enum E_QueryItemOperator
    {
        Undefined
        , EQ
        , NOT
        , GT
        , GE
        , LT
        , LE
    }

    public enum E_QueryItemCondition
    {
        Undefined
        , And
        , Or
    }

    static partial class EnumMappings
    {
        public static readonly string[] E_QueryItemOperator = {
                        ""
                         , "EQ"
                         , "NOT"
                         , "GT"
                         , "GE"
                         , "LT"
                         , "LE"
                         };
        public static readonly string[] E_QueryItemCondition = {
                        ""
                         , "And"
                         , "Or"
                         };
    }
}
