// Generated by CodeMonkey on 2/7/2009 2:23:24 AM
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace Mismo.Closing2_4
{
    public class GovernmentMonitoring : AbstractXmlSerializable
    {
        #region Private Member Variables
        private string m_id = string.Empty;
        private E_GovernmentMonitoringGenderType m_genderType = E_GovernmentMonitoringGenderType.Undefined;
        private E_GovernmentMonitoringHmdaEthnicityType m_hmdaEthnicityType = E_GovernmentMonitoringHmdaEthnicityType.Undefined;
        private string m_otherRaceNationalOriginDescription = string.Empty;
        private E_YNIndicator m_raceNationalOriginRefusalIndicator = E_YNIndicator.Undefined;
        private E_GovernmentMonitoringRaceNationalOriginType m_raceNationalOriginType = E_GovernmentMonitoringRaceNationalOriginType.Undefined;
        private List<HmdaRace> m_hmdaRaceList = null;
        #endregion

        #region Public Properties
        public string Id
        {
            get { return m_id; }
            set { m_id = value; }
        }
        public E_GovernmentMonitoringGenderType GenderType
        {
            get { return m_genderType; }
            set { m_genderType = value; }
        }
        public E_GovernmentMonitoringHmdaEthnicityType HmdaEthnicityType
        {
            get { return m_hmdaEthnicityType; }
            set { m_hmdaEthnicityType = value; }
        }
        public string OtherRaceNationalOriginDescription
        {
            get { return m_otherRaceNationalOriginDescription; }
            set { m_otherRaceNationalOriginDescription = value; }
        }
        public E_YNIndicator RaceNationalOriginRefusalIndicator
        {
            get { return m_raceNationalOriginRefusalIndicator; }
            set { m_raceNationalOriginRefusalIndicator = value; }
        }
        public E_GovernmentMonitoringRaceNationalOriginType RaceNationalOriginType
        {
            get { return m_raceNationalOriginType; }
            set { m_raceNationalOriginType = value; }
        }
        public List<HmdaRace> HmdaRaceList
        {
            get
            {
                if (null == m_hmdaRaceList) m_hmdaRaceList = new List<HmdaRace>();
                return m_hmdaRaceList;
            }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "GOVERNMENT_MONITORING")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_id = ReadAttribute(reader, "_ID");
            m_genderType = (E_GovernmentMonitoringGenderType) ReadAttribute(reader, "GenderType", EnumMappings.E_GovernmentMonitoringGenderType);
            m_hmdaEthnicityType = (E_GovernmentMonitoringHmdaEthnicityType) ReadAttribute(reader, "HMDAEthnicityType", EnumMappings.E_GovernmentMonitoringHmdaEthnicityType);
            m_otherRaceNationalOriginDescription = ReadAttribute(reader, "OtherRaceNationalOriginDescription");
            m_raceNationalOriginRefusalIndicator = (E_YNIndicator) ReadAttribute(reader, "RaceNationalOriginRefusalIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator);
            m_raceNationalOriginType = (E_GovernmentMonitoringRaceNationalOriginType) ReadAttribute(reader, "RaceNationalOriginType", EnumMappings.E_GovernmentMonitoringRaceNationalOriginType);
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "GOVERNMENT_MONITORING") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                switch (reader.Name)
                {
                    case "HMDA_RACE":
                        ReadElement(reader, HmdaRaceList);
                        break;
                }
                #endregion
            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("GOVERNMENT_MONITORING");
            WriteAttribute(writer, "_ID", m_id);
            WriteEnumAttribute(writer, "GenderType", EnumMappings.E_GovernmentMonitoringGenderType[(int) m_genderType]);
            WriteEnumAttribute(writer, "HMDAEthnicityType", EnumMappings.E_GovernmentMonitoringHmdaEthnicityType[(int) m_hmdaEthnicityType]);
            WriteAttribute(writer, "OtherRaceNationalOriginDescription", m_otherRaceNationalOriginDescription);
            WriteEnumAttribute(writer, "RaceNationalOriginRefusalIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator[(int) m_raceNationalOriginRefusalIndicator]);
            WriteEnumAttribute(writer, "RaceNationalOriginType", EnumMappings.E_GovernmentMonitoringRaceNationalOriginType[(int) m_raceNationalOriginType]);
            WriteElement(writer, m_hmdaRaceList);
            writer.WriteEndElement();
        }
    }

    public enum E_GovernmentMonitoringGenderType
    {
        Undefined
        , Female
        , InformationNotProvidedUnknown
        , Male
        , NotApplicable
    }

    public enum E_GovernmentMonitoringHmdaEthnicityType
    {
        Undefined
        , HispanicOrLatino
        , NotHispanicOrLatino
        , InformationNotProvidedByApplicantInMailInternetOrTelephoneApplication
        , NotApplicable
    }

    public enum E_GovernmentMonitoringRaceNationalOriginType
    {
        Undefined
        , AmericanIndianOrAlaskanNative
        , AsianOrPacificIslander
        , BlackNotOfHispanicOrigin
        , Hispanic
        , InformationNotProvided
        , Other
        , WhiteNotOfHispanicOrigin
        , NotApplicable
    }

    static partial class EnumMappings
    {
        public static readonly string[] E_GovernmentMonitoringGenderType = {
                        ""
                         , "Female"
                         , "InformationNotProvidedUnknown"
                         , "Male"
                         , "NotApplicable"
                         };
        public static readonly string[] E_GovernmentMonitoringHmdaEthnicityType = {
                        ""
                         , "HispanicOrLatino"
                         , "NotHispanicOrLatino"
                         , "InformationNotProvidedByApplicantInMailInternetOrTelephoneApplication"
                         , "NotApplicable"
                         };
        public static readonly string[] E_GovernmentMonitoringRaceNationalOriginType = {
                        ""
                         , "AmericanIndianOrAlaskanNative"
                         , "AsianOrPacificIslander"
                         , "BlackNotOfHispanicOrigin"
                         , "Hispanic"
                         , "InformationNotProvided"
                         , "Other"
                         , "WhiteNotOfHispanicOrigin"
                         , "NotApplicable"
                         };
    }
}
