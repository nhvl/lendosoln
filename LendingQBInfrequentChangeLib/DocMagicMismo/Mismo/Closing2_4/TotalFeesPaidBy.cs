// Generated by CodeMonkey on 2/7/2009 2:23:24 AM
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace Mismo.Closing2_4
{
    public class TotalFeesPaidBy : AbstractXmlSerializable
    {
        #region Private Member Variables
        private string m_id = string.Empty;
        private E_TotalFeesPaidByType m_type = E_TotalFeesPaidByType.Undefined;
        private string m_typeAmount = string.Empty;
        private string m_typeOtherDescription = string.Empty;
        #endregion

        #region Public Properties
        public string Id
        {
            get { return m_id; }
            set { m_id = value; }
        }
        public E_TotalFeesPaidByType Type
        {
            get { return m_type; }
            set { m_type = value; }
        }
        public string TypeAmount
        {
            get { return m_typeAmount; }
            set { m_typeAmount = value; }
        }
        public string TypeOtherDescription
        {
            get { return m_typeOtherDescription; }
            set { m_typeOtherDescription = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "_TOTAL_FEES_PAID_BY")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_id = ReadAttribute(reader, "_ID");
            m_type = (E_TotalFeesPaidByType) ReadAttribute(reader, "_Type", EnumMappings.E_TotalFeesPaidByType);
            m_typeAmount = ReadAttribute(reader, "_TypeAmount");
            m_typeOtherDescription = ReadAttribute(reader, "_TypeOtherDescription");
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "_TOTAL_FEES_PAID_BY") return;

            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("_TOTAL_FEES_PAID_BY");
            WriteAttribute(writer, "_ID", m_id);
            WriteEnumAttribute(writer, "_Type", EnumMappings.E_TotalFeesPaidByType[(int) m_type]);
            WriteAttribute(writer, "_TypeAmount", m_typeAmount);
            WriteAttribute(writer, "_TypeOtherDescription", m_typeOtherDescription);
            writer.WriteEndElement();
        }
    }

    public enum E_TotalFeesPaidByType
    {
        Undefined
        , Broker
        , Buyer
        , Investor
        , Lender
        , Seller
        , Other
    }

    static partial class EnumMappings
    {
        public static readonly string[] E_TotalFeesPaidByType = {
                        ""
                         , "Broker"
                         , "Buyer"
                         , "Investor"
                         , "Lender"
                         , "Seller"
                         , "Other"
                         };
    }
}
