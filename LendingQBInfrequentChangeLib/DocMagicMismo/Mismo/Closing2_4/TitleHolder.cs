// Generated by CodeMonkey on 2/7/2009 2:23:24 AM
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace Mismo.Closing2_4
{
    public class TitleHolder : AbstractXmlSerializable
    {
        #region Private Member Variables
        private string m_id = string.Empty;
        private E_TitleHolderLandTrustType m_landTrustType = E_TitleHolderLandTrustType.Undefined;
        private string m_landTrustTypeOtherDescription = string.Empty;
        private string m_name = string.Empty;
        #endregion

        #region Public Properties
        public string Id
        {
            get { return m_id; }
            set { m_id = value; }
        }
        public E_TitleHolderLandTrustType LandTrustType
        {
            get { return m_landTrustType; }
            set { m_landTrustType = value; }
        }
        public string LandTrustTypeOtherDescription
        {
            get { return m_landTrustTypeOtherDescription; }
            set { m_landTrustTypeOtherDescription = value; }
        }
        public string Name
        {
            get { return m_name; }
            set { m_name = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "TITLE_HOLDER")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_id = ReadAttribute(reader, "_ID");
            m_landTrustType = (E_TitleHolderLandTrustType) ReadAttribute(reader, "LandTrustType", EnumMappings.E_TitleHolderLandTrustType);
            m_landTrustTypeOtherDescription = ReadAttribute(reader, "LandTrustTypeOtherDescription");
            m_name = ReadAttribute(reader, "_Name");
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "TITLE_HOLDER") return;

            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("TITLE_HOLDER");
            WriteAttribute(writer, "_ID", m_id);
            WriteEnumAttribute(writer, "LandTrustType", EnumMappings.E_TitleHolderLandTrustType[(int) m_landTrustType]);
            WriteAttribute(writer, "LandTrustTypeOtherDescription", m_landTrustTypeOtherDescription);
            WriteAttribute(writer, "_Name", m_name);
            writer.WriteEndElement();
        }
    }

    public enum E_TitleHolderLandTrustType
    {
        Undefined
        , IllinoisLandTrust
        , Other
    }

    static partial class EnumMappings
    {
        public static readonly string[] E_TitleHolderLandTrustType = {
                        ""
                         , "IllinoisLandTrust"
                         , "Other"
                         };
    }
}
