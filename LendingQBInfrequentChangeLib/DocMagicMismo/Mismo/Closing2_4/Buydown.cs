// Generated by CodeMonkey on 2/7/2009 2:23:23 AM
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace Mismo.Closing2_4
{
    public class Buydown : AbstractXmlSerializable
    {
        #region Private Member Variables
        private string m_id = string.Empty;
        private E_BuydownBaseDateType m_baseDateType = E_BuydownBaseDateType.Undefined;
        private string m_baseDateTypeOtherDescription = string.Empty;
        private string m_changeFrequencyMonths = string.Empty;
        private E_BuydownContributorType m_contributorType = E_BuydownContributorType.Undefined;
        private string m_contributorTypeOtherDescription = string.Empty;
        private string m_durationMonths = string.Empty;
        private string m_increaseRatePercent = string.Empty;
        private E_YNIndicator m_lenderFundingIndicator = E_YNIndicator.Undefined;
        private string m_originalBalanceAmount = string.Empty;
        private E_YNIndicator m_permanentIndicator = E_YNIndicator.Undefined;
        private E_BuydownSubsidyCalculationType m_subsidyCalculationType = E_BuydownSubsidyCalculationType.Undefined;
        private string m_totalSubsidyAmount = string.Empty;
        private List<Contributor> m_contributorList = null;
        private List<SubsidySchedule> m_subsidyScheduleList = null;
        #endregion

        #region Public Properties
        public string Id
        {
            get { return m_id; }
            set { m_id = value; }
        }
        public E_BuydownBaseDateType BaseDateType
        {
            get { return m_baseDateType; }
            set { m_baseDateType = value; }
        }
        public string BaseDateTypeOtherDescription
        {
            get { return m_baseDateTypeOtherDescription; }
            set { m_baseDateTypeOtherDescription = value; }
        }
        public string ChangeFrequencyMonths
        {
            get { return m_changeFrequencyMonths; }
            set { m_changeFrequencyMonths = value; }
        }
        public E_BuydownContributorType ContributorType
        {
            get { return m_contributorType; }
            set { m_contributorType = value; }
        }
        public string ContributorTypeOtherDescription
        {
            get { return m_contributorTypeOtherDescription; }
            set { m_contributorTypeOtherDescription = value; }
        }
        public string DurationMonths
        {
            get { return m_durationMonths; }
            set { m_durationMonths = value; }
        }
        public string IncreaseRatePercent
        {
            get { return m_increaseRatePercent; }
            set { m_increaseRatePercent = value; }
        }
        public E_YNIndicator LenderFundingIndicator
        {
            get { return m_lenderFundingIndicator; }
            set { m_lenderFundingIndicator = value; }
        }
        public string OriginalBalanceAmount
        {
            get { return m_originalBalanceAmount; }
            set { m_originalBalanceAmount = value; }
        }
        public E_YNIndicator PermanentIndicator
        {
            get { return m_permanentIndicator; }
            set { m_permanentIndicator = value; }
        }
        public E_BuydownSubsidyCalculationType SubsidyCalculationType
        {
            get { return m_subsidyCalculationType; }
            set { m_subsidyCalculationType = value; }
        }
        public string TotalSubsidyAmount
        {
            get { return m_totalSubsidyAmount; }
            set { m_totalSubsidyAmount = value; }
        }
        public List<Contributor> ContributorList
        {
            get
            {
                if (null == m_contributorList) m_contributorList = new List<Contributor>();
                return m_contributorList;
            }
        }
        public List<SubsidySchedule> SubsidyScheduleList
        {
            get
            {
                if (null == m_subsidyScheduleList) m_subsidyScheduleList = new List<SubsidySchedule>();
                return m_subsidyScheduleList;
            }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "BUYDOWN")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_id = ReadAttribute(reader, "_ID");
            m_baseDateType = (E_BuydownBaseDateType) ReadAttribute(reader, "_BaseDateType", EnumMappings.E_BuydownBaseDateType);
            m_baseDateTypeOtherDescription = ReadAttribute(reader, "_BaseDateTypeOtherDescription");
            m_changeFrequencyMonths = ReadAttribute(reader, "_ChangeFrequencyMonths");
            m_contributorType = (E_BuydownContributorType) ReadAttribute(reader, "_ContributorType", EnumMappings.E_BuydownContributorType);
            m_contributorTypeOtherDescription = ReadAttribute(reader, "_ContributorTypeOtherDescription");
            m_durationMonths = ReadAttribute(reader, "_DurationMonths");
            m_increaseRatePercent = ReadAttribute(reader, "_IncreaseRatePercent");
            m_lenderFundingIndicator = (E_YNIndicator) ReadAttribute(reader, "_LenderFundingIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator);
            m_originalBalanceAmount = ReadAttribute(reader, "_OriginalBalanceAmount");
            m_permanentIndicator = (E_YNIndicator) ReadAttribute(reader, "_PermanentIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator);
            m_subsidyCalculationType = (E_BuydownSubsidyCalculationType) ReadAttribute(reader, "_SubsidyCalculationType", EnumMappings.E_BuydownSubsidyCalculationType);
            m_totalSubsidyAmount = ReadAttribute(reader, "_TotalSubsidyAmount");
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "BUYDOWN") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                switch (reader.Name)
                {
                    case "_CONTRIBUTOR":
                        ReadElement(reader, ContributorList);
                        break;
                    case "_SUBSIDY_SCHEDULE":
                        ReadElement(reader, SubsidyScheduleList);
                        break;
                }
                #endregion
            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("BUYDOWN");
            WriteAttribute(writer, "_ID", m_id);
            WriteEnumAttribute(writer, "_BaseDateType", EnumMappings.E_BuydownBaseDateType[(int) m_baseDateType]);
            WriteAttribute(writer, "_BaseDateTypeOtherDescription", m_baseDateTypeOtherDescription);
            WriteAttribute(writer, "_ChangeFrequencyMonths", m_changeFrequencyMonths);
            WriteEnumAttribute(writer, "_ContributorType", EnumMappings.E_BuydownContributorType[(int) m_contributorType]);
            WriteAttribute(writer, "_ContributorTypeOtherDescription", m_contributorTypeOtherDescription);
            WriteAttribute(writer, "_DurationMonths", m_durationMonths);
            WriteAttribute(writer, "_IncreaseRatePercent", m_increaseRatePercent);
            WriteEnumAttribute(writer, "_LenderFundingIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator[(int) m_lenderFundingIndicator]);
            WriteAttribute(writer, "_OriginalBalanceAmount", m_originalBalanceAmount);
            WriteEnumAttribute(writer, "_PermanentIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator[(int) m_permanentIndicator]);
            WriteEnumAttribute(writer, "_SubsidyCalculationType", EnumMappings.E_BuydownSubsidyCalculationType[(int) m_subsidyCalculationType]);
            WriteAttribute(writer, "_TotalSubsidyAmount", m_totalSubsidyAmount);
            WriteElement(writer, m_contributorList);
            WriteElement(writer, m_subsidyScheduleList);
            writer.WriteEndElement();
        }
    }

    public enum E_BuydownBaseDateType
    {
        Undefined
        , NoteDate
        , FirstPaymentDate
        , LastPaymentDate
        , Other
    }

    public enum E_BuydownContributorType
    {
        Undefined
        , Borrower
        , Builder
        , Employer
        , LenderPremiumFinanced
        , NonparentRelative
        , Parent
        , Seller
        , Unassigned
        , UnrelatedFriend
        , Other
        , Financed
        , NonParentRelative
    }

    public enum E_BuydownSubsidyCalculationType
    {
        Undefined
        , DecliningLoanBalance
        , OriginalLoanAmount
    }

    static partial class EnumMappings
    {
        public static readonly string[] E_BuydownBaseDateType = {
                        ""
                         , "NoteDate"
                         , "FirstPaymentDate"
                         , "LastPaymentDate"
                         , "Other"
                         };
        public static readonly string[] E_BuydownContributorType = {
                        ""
                         , "Borrower"
                         , "Builder"
                         , "Employer"
                         , "LenderPremiumFinanced"
                         , "NonparentRelative"
                         , "Parent"
                         , "Seller"
                         , "Unassigned"
                         , "UnrelatedFriend"
                         , "Other"
                         , "Financed"
                         , "NonParentRelative"
                         };
        public static readonly string[] E_BuydownSubsidyCalculationType = {
                        ""
                         , "DecliningLoanBalance"
                         , "OriginalLoanAmount"
                         };
    }
}
