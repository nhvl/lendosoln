// Generated by CodeMonkey on 2/7/2009 2:23:24 AM
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace Mismo.Closing2_4
{
    public class Default : AbstractXmlSerializable
    {
        #region Private Member Variables
        private string m_id = string.Empty;
        private E_YNIndicator m_acknowledgmentOfCashAdvanceAgainstNonHomesteadPropertyIndicator = E_YNIndicator.Undefined;
        private string m_applicationFeesAmount = string.Empty;
        private string m_closingPreparationFeesAmount = string.Empty;
        private string m_lendingInstitutionPostOfficeBoxAddress = string.Empty;
        #endregion

        #region Public Properties
        public string Id
        {
            get { return m_id; }
            set { m_id = value; }
        }
        public E_YNIndicator AcknowledgmentOfCashAdvanceAgainstNonHomesteadPropertyIndicator
        {
            get { return m_acknowledgmentOfCashAdvanceAgainstNonHomesteadPropertyIndicator; }
            set { m_acknowledgmentOfCashAdvanceAgainstNonHomesteadPropertyIndicator = value; }
        }
        public string ApplicationFeesAmount
        {
            get { return m_applicationFeesAmount; }
            set { m_applicationFeesAmount = value; }
        }
        public string ClosingPreparationFeesAmount
        {
            get { return m_closingPreparationFeesAmount; }
            set { m_closingPreparationFeesAmount = value; }
        }
        public string LendingInstitutionPostOfficeBoxAddress
        {
            get { return m_lendingInstitutionPostOfficeBoxAddress; }
            set { m_lendingInstitutionPostOfficeBoxAddress = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "DEFAULT")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_id = ReadAttribute(reader, "_ID");
            m_acknowledgmentOfCashAdvanceAgainstNonHomesteadPropertyIndicator = (E_YNIndicator) ReadAttribute(reader, "_AcknowledgmentOfCashAdvanceAgainstNonHomesteadPropertyIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator);
            m_applicationFeesAmount = ReadAttribute(reader, "_ApplicationFeesAmount");
            m_closingPreparationFeesAmount = ReadAttribute(reader, "_ClosingPreparationFeesAmount");
            m_lendingInstitutionPostOfficeBoxAddress = ReadAttribute(reader, "_LendingInstitutionPostOfficeBoxAddress");
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "DEFAULT") return;

            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("DEFAULT");
            WriteAttribute(writer, "_ID", m_id);
            WriteEnumAttribute(writer, "_AcknowledgmentOfCashAdvanceAgainstNonHomesteadPropertyIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator[(int) m_acknowledgmentOfCashAdvanceAgainstNonHomesteadPropertyIndicator]);
            WriteAttribute(writer, "_ApplicationFeesAmount", m_applicationFeesAmount);
            WriteAttribute(writer, "_ClosingPreparationFeesAmount", m_closingPreparationFeesAmount);
            WriteAttribute(writer, "_LendingInstitutionPostOfficeBoxAddress", m_lendingInstitutionPostOfficeBoxAddress);
            writer.WriteEndElement();
        }
    }
}
