// Generated by CodeMonkey on 2/7/2009 2:23:24 AM
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace Mismo.Closing2_4
{
    public class HomeownersAssociation : AbstractXmlSerializable
    {
        #region Private Member Variables
        private string m_id = string.Empty;
        private string m_city = string.Empty;
        private string m_country = string.Empty;
        private string m_county = string.Empty;
        private string m_name = string.Empty;
        private string m_postalCode = string.Empty;
        private string m_state = string.Empty;
        private string m_streetAddress = string.Empty;
        private string m_streetAddress2 = string.Empty;
        private ContactDetail m_contactDetail = null;
        #endregion

        #region Public Properties
        public string Id
        {
            get { return m_id; }
            set { m_id = value; }
        }
        public string City
        {
            get { return m_city; }
            set { m_city = value; }
        }
        public string Country
        {
            get { return m_country; }
            set { m_country = value; }
        }
        public string County
        {
            get { return m_county; }
            set { m_county = value; }
        }
        public string Name
        {
            get { return m_name; }
            set { m_name = value; }
        }
        public string PostalCode
        {
            get { return m_postalCode; }
            set { m_postalCode = value; }
        }
        public string State
        {
            get { return m_state; }
            set { m_state = value; }
        }
        public string StreetAddress
        {
            get { return m_streetAddress; }
            set { m_streetAddress = value; }
        }
        public string StreetAddress2
        {
            get { return m_streetAddress2; }
            set { m_streetAddress2 = value; }
        }
        public ContactDetail ContactDetail
        {
            get
            {
                if (null == m_contactDetail) m_contactDetail = new ContactDetail();
                return m_contactDetail;
            }
            set { m_contactDetail = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "HOMEOWNERS_ASSOCIATION")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_id = ReadAttribute(reader, "_ID");
            m_city = ReadAttribute(reader, "_City");
            m_country = ReadAttribute(reader, "_Country");
            m_county = ReadAttribute(reader, "_County");
            m_name = ReadAttribute(reader, "_Name");
            m_postalCode = ReadAttribute(reader, "_PostalCode");
            m_state = ReadAttribute(reader, "_State");
            m_streetAddress = ReadAttribute(reader, "_StreetAddress");
            m_streetAddress2 = ReadAttribute(reader, "_StreetAddress2");
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "HOMEOWNERS_ASSOCIATION") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                switch (reader.Name)
                {
                    case "CONTACT_DETAIL":
                        ReadElement(reader, ContactDetail);
                        break;
                }
                #endregion
            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("HOMEOWNERS_ASSOCIATION");
            WriteAttribute(writer, "_ID", m_id);
            WriteAttribute(writer, "_City", m_city);
            WriteAttribute(writer, "_Country", m_country);
            WriteAttribute(writer, "_County", m_county);
            WriteAttribute(writer, "_Name", m_name);
            WriteAttribute(writer, "_PostalCode", m_postalCode);
            WriteAttribute(writer, "_State", m_state);
            WriteAttribute(writer, "_StreetAddress", m_streetAddress);
            WriteAttribute(writer, "_StreetAddress2", m_streetAddress2);
            WriteElement(writer, m_contactDetail);
            writer.WriteEndElement();
        }
    }
}
