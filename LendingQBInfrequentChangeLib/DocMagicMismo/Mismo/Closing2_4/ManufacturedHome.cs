// Generated by CodeMonkey on 2/7/2009 2:23:24 AM
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace Mismo.Closing2_4
{
    public class ManufacturedHome : AbstractXmlSerializable
    {
        #region Private Member Variables
        private string m_id = string.Empty;
        private string m_lengthFeetCount = string.Empty;
        private string m_widthFeetCount = string.Empty;
        private E_YNIndicator m_attachedToFoundationIndicator = E_YNIndicator.Undefined;
        private E_ManufacturedHomeConditionDescriptionType m_conditionDescriptionType = E_ManufacturedHomeConditionDescriptionType.Undefined;
        private string m_hudCertificationLabelIdentifier = string.Empty;
        private string m_makeIdentifier = string.Empty;
        private string m_modelIdentifier = string.Empty;
        private string m_serialNumberIdentifier = string.Empty;
        private E_ManufacturedHomeWidthType m_widthType = E_ManufacturedHomeWidthType.Undefined;
        #endregion

        #region Public Properties
        public string Id
        {
            get { return m_id; }
            set { m_id = value; }
        }
        public string LengthFeetCount
        {
            get { return m_lengthFeetCount; }
            set { m_lengthFeetCount = value; }
        }
        public string WidthFeetCount
        {
            get { return m_widthFeetCount; }
            set { m_widthFeetCount = value; }
        }
        public E_YNIndicator AttachedToFoundationIndicator
        {
            get { return m_attachedToFoundationIndicator; }
            set { m_attachedToFoundationIndicator = value; }
        }
        public E_ManufacturedHomeConditionDescriptionType ConditionDescriptionType
        {
            get { return m_conditionDescriptionType; }
            set { m_conditionDescriptionType = value; }
        }
        public string HudCertificationLabelIdentifier
        {
            get { return m_hudCertificationLabelIdentifier; }
            set { m_hudCertificationLabelIdentifier = value; }
        }
        public string MakeIdentifier
        {
            get { return m_makeIdentifier; }
            set { m_makeIdentifier = value; }
        }
        public string ModelIdentifier
        {
            get { return m_modelIdentifier; }
            set { m_modelIdentifier = value; }
        }
        public string SerialNumberIdentifier
        {
            get { return m_serialNumberIdentifier; }
            set { m_serialNumberIdentifier = value; }
        }
        public E_ManufacturedHomeWidthType WidthType
        {
            get { return m_widthType; }
            set { m_widthType = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "MANUFACTURED_HOME")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_id = ReadAttribute(reader, "_ID");
            m_lengthFeetCount = ReadAttribute(reader, "LengthFeetCount");
            m_widthFeetCount = ReadAttribute(reader, "WidthFeetCount");
            m_attachedToFoundationIndicator = (E_YNIndicator) ReadAttribute(reader, "_AttachedToFoundationIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator);
            m_conditionDescriptionType = (E_ManufacturedHomeConditionDescriptionType) ReadAttribute(reader, "_ConditionDescriptionType", EnumMappings.E_ManufacturedHomeConditionDescriptionType);
            m_hudCertificationLabelIdentifier = ReadAttribute(reader, "_HUDCertificationLabelIdentifier");
            m_makeIdentifier = ReadAttribute(reader, "_MakeIdentifier");
            m_modelIdentifier = ReadAttribute(reader, "_ModelIdentifier");
            m_serialNumberIdentifier = ReadAttribute(reader, "_SerialNumberIdentifier");
            m_widthType = (E_ManufacturedHomeWidthType) ReadAttribute(reader, "_WidthType", EnumMappings.E_ManufacturedHomeWidthType);
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "MANUFACTURED_HOME") return;

            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("MANUFACTURED_HOME");
            WriteAttribute(writer, "_ID", m_id);
            WriteAttribute(writer, "LengthFeetCount", m_lengthFeetCount);
            WriteAttribute(writer, "WidthFeetCount", m_widthFeetCount);
            WriteEnumAttribute(writer, "_AttachedToFoundationIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator[(int) m_attachedToFoundationIndicator]);
            WriteEnumAttribute(writer, "_ConditionDescriptionType", EnumMappings.E_ManufacturedHomeConditionDescriptionType[(int) m_conditionDescriptionType]);
            WriteAttribute(writer, "_HUDCertificationLabelIdentifier", m_hudCertificationLabelIdentifier);
            WriteAttribute(writer, "_MakeIdentifier", m_makeIdentifier);
            WriteAttribute(writer, "_ModelIdentifier", m_modelIdentifier);
            WriteAttribute(writer, "_SerialNumberIdentifier", m_serialNumberIdentifier);
            WriteEnumAttribute(writer, "_WidthType", EnumMappings.E_ManufacturedHomeWidthType[(int) m_widthType]);
            writer.WriteEndElement();
        }
    }

    public enum E_ManufacturedHomeConditionDescriptionType
    {
        Undefined
        , New
        , Used
    }

    public enum E_ManufacturedHomeWidthType
    {
        Undefined
        , MultiWide
        , SingleWide
    }

    static partial class EnumMappings
    {
        public static readonly string[] E_ManufacturedHomeConditionDescriptionType = {
                        ""
                         , "New"
                         , "Used"
                         };
        public static readonly string[] E_ManufacturedHomeWidthType = {
                        ""
                         , "MultiWide"
                         , "SingleWide"
                         };
    }
}
