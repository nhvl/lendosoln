// Generated by CodeMonkey on 2/7/2009 2:23:23 AM
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace Mismo.Closing2_4
{
    public class Asset : AbstractXmlSerializable
    {
        #region Private Member Variables
        private string m_id = string.Empty;
        private string m_borrowerId = string.Empty;
        private string m_assetDescription = string.Empty;
        private string m_automobileMakeDescription = string.Empty;
        private string m_automobileModelYear = string.Empty;
        private string m_lifeInsuranceFaceValueAmount = string.Empty;
        private string m_otherAssetTypeDescription = string.Empty;
        private string m_stockBondMutualFundShareCount = string.Empty;
        private string m_accountIdentifier = string.Empty;
        private string m_cashOrMarketValueAmount = string.Empty;
        private string m_holderCity = string.Empty;
        private string m_holderName = string.Empty;
        private string m_holderPostalCode = string.Empty;
        private string m_holderState = string.Empty;
        private string m_holderStreetAddress = string.Empty;
        private string m_holderStreetAddress2 = string.Empty;
        private E_AssetType m_type = E_AssetType.Undefined;
        private E_YNIndicator m_verifiedIndicator = E_YNIndicator.Undefined;
        #endregion

        #region Public Properties
        public string Id
        {
            get { return m_id; }
            set { m_id = value; }
        }
        public string BorrowerId
        {
            get { return m_borrowerId; }
            set { m_borrowerId = value; }
        }
        public string AssetDescription
        {
            get { return m_assetDescription; }
            set { m_assetDescription = value; }
        }
        public string AutomobileMakeDescription
        {
            get { return m_automobileMakeDescription; }
            set { m_automobileMakeDescription = value; }
        }
        public string AutomobileModelYear
        {
            get { return m_automobileModelYear; }
            set { m_automobileModelYear = value; }
        }
        public string LifeInsuranceFaceValueAmount
        {
            get { return m_lifeInsuranceFaceValueAmount; }
            set { m_lifeInsuranceFaceValueAmount = value; }
        }
        public string OtherAssetTypeDescription
        {
            get { return m_otherAssetTypeDescription; }
            set { m_otherAssetTypeDescription = value; }
        }
        public string StockBondMutualFundShareCount
        {
            get { return m_stockBondMutualFundShareCount; }
            set { m_stockBondMutualFundShareCount = value; }
        }
        public string AccountIdentifier
        {
            get { return m_accountIdentifier; }
            set { m_accountIdentifier = value; }
        }
        public string CashOrMarketValueAmount
        {
            get { return m_cashOrMarketValueAmount; }
            set { m_cashOrMarketValueAmount = value; }
        }
        public string HolderCity
        {
            get { return m_holderCity; }
            set { m_holderCity = value; }
        }
        public string HolderName
        {
            get { return m_holderName; }
            set { m_holderName = value; }
        }
        public string HolderPostalCode
        {
            get { return m_holderPostalCode; }
            set { m_holderPostalCode = value; }
        }
        public string HolderState
        {
            get { return m_holderState; }
            set { m_holderState = value; }
        }
        public string HolderStreetAddress
        {
            get { return m_holderStreetAddress; }
            set { m_holderStreetAddress = value; }
        }
        public string HolderStreetAddress2
        {
            get { return m_holderStreetAddress2; }
            set { m_holderStreetAddress2 = value; }
        }
        public E_AssetType Type
        {
            get { return m_type; }
            set { m_type = value; }
        }
        public E_YNIndicator VerifiedIndicator
        {
            get { return m_verifiedIndicator; }
            set { m_verifiedIndicator = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "ASSET")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_id = ReadAttribute(reader, "_ID");
            m_borrowerId = ReadAttribute(reader, "BorrowerID");
            m_assetDescription = ReadAttribute(reader, "AssetDescription");
            m_automobileMakeDescription = ReadAttribute(reader, "AutomobileMakeDescription");
            m_automobileModelYear = ReadAttribute(reader, "AutomobileModelYear");
            m_lifeInsuranceFaceValueAmount = ReadAttribute(reader, "LifeInsuranceFaceValueAmount");
            m_otherAssetTypeDescription = ReadAttribute(reader, "OtherAssetTypeDescription");
            m_stockBondMutualFundShareCount = ReadAttribute(reader, "StockBondMutualFundShareCount");
            m_accountIdentifier = ReadAttribute(reader, "_AccountIdentifier");
            m_cashOrMarketValueAmount = ReadAttribute(reader, "_CashOrMarketValueAmount");
            m_holderCity = ReadAttribute(reader, "_HolderCity");
            m_holderName = ReadAttribute(reader, "_HolderName");
            m_holderPostalCode = ReadAttribute(reader, "_HolderPostalCode");
            m_holderState = ReadAttribute(reader, "_HolderState");
            m_holderStreetAddress = ReadAttribute(reader, "_HolderStreetAddress");
            m_holderStreetAddress2 = ReadAttribute(reader, "_HolderStreetAddress2");
            m_type = (E_AssetType) ReadAttribute(reader, "_Type", EnumMappings.E_AssetType);
            m_verifiedIndicator = (E_YNIndicator) ReadAttribute(reader, "_VerifiedIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator);
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "ASSET") return;

            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("ASSET");
            WriteAttribute(writer, "_ID", m_id);
            WriteAttribute(writer, "BorrowerID", m_borrowerId);
            WriteAttribute(writer, "AssetDescription", m_assetDescription);
            WriteAttribute(writer, "AutomobileMakeDescription", m_automobileMakeDescription);
            WriteAttribute(writer, "AutomobileModelYear", m_automobileModelYear);
            WriteAttribute(writer, "LifeInsuranceFaceValueAmount", m_lifeInsuranceFaceValueAmount);
            WriteAttribute(writer, "OtherAssetTypeDescription", m_otherAssetTypeDescription);
            WriteAttribute(writer, "StockBondMutualFundShareCount", m_stockBondMutualFundShareCount);
            WriteAttribute(writer, "_AccountIdentifier", m_accountIdentifier);
            WriteAttribute(writer, "_CashOrMarketValueAmount", m_cashOrMarketValueAmount);
            WriteAttribute(writer, "_HolderCity", m_holderCity);
            WriteAttribute(writer, "_HolderName", m_holderName);
            WriteAttribute(writer, "_HolderPostalCode", m_holderPostalCode);
            WriteAttribute(writer, "_HolderState", m_holderState);
            WriteAttribute(writer, "_HolderStreetAddress", m_holderStreetAddress);
            WriteAttribute(writer, "_HolderStreetAddress2", m_holderStreetAddress2);
            WriteEnumAttribute(writer, "_Type", EnumMappings.E_AssetType[(int) m_type]);
            WriteEnumAttribute(writer, "_VerifiedIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator[(int) m_verifiedIndicator]);
            writer.WriteEndElement();
        }
    }

    public enum E_AssetType
    {
        Undefined
        , Automobile
        , Bond
        , BridgeLoanNotDeposited
        , CashOnHand
        , CertificateOfDepositTimeDeposit
        , CheckingAccount
        , EarnestMoneyCashDepositTowardPurchase
        , GiftsTotal
        , GiftsNotDeposited
        , LifeInsurance
        , MoneyMarketFund
        , MutualFund
        , NetWorthOfBusinessOwned
        , OtherLiquidAssets
        , OtherNonLiquidAssets
        , PendingNetSaleProceedsFromRealEstateAssets
        , RelocationMoney
        , RetirementFund
        , SaleOtherAssets
        , SavingsAccount
        , SecuredBorrowedFundsNotDeposited
        , Stock
        , TrustAccount
        , BorrowerEstimatedTotalAssets
        , GrantsNotDeposited
        , RealEstateOwned
    }

    static partial class EnumMappings
    {
        public static readonly string[] E_AssetType = {
                        ""
                         , "Automobile"
                         , "Bond"
                         , "BridgeLoanNotDeposited"
                         , "CashOnHand"
                         , "CertificateOfDepositTimeDeposit"
                         , "CheckingAccount"
                         , "EarnestMoneyCashDepositTowardPurchase"
                         , "GiftsTotal"
                         , "GiftsNotDeposited"
                         , "LifeInsurance"
                         , "MoneyMarketFund"
                         , "MutualFund"
                         , "NetWorthOfBusinessOwned"
                         , "OtherLiquidAssets"
                         , "OtherNonLiquidAssets"
                         , "PendingNetSaleProceedsFromRealEstateAssets"
                         , "RelocationMoney"
                         , "RetirementFund"
                         , "SaleOtherAssets"
                         , "SavingsAccount"
                         , "SecuredBorrowedFundsNotDeposited"
                         , "Stock"
                         , "TrustAccount"
                         , "BorrowerEstimatedTotalAssets"
                         , "GrantsNotDeposited"
                         , "RealEstateOwned"
                         };
    }
}
