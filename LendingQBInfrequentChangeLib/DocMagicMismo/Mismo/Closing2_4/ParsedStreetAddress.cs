// Generated by CodeMonkey on 2/7/2009 2:23:24 AM
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace Mismo.Closing2_4
{
    public class ParsedStreetAddress : AbstractXmlSerializable
    {
        #region Private Member Variables
        private string m_id = string.Empty;
        private string m_apartmentOrUnit = string.Empty;
        private string m_buildingNumber = string.Empty;
        private string m_directionPrefix = string.Empty;
        private string m_directionSuffix = string.Empty;
        private string m_houseNumber = string.Empty;
        private string m_militaryApoFpo = string.Empty;
        private string m_postOfficeBox = string.Empty;
        private string m_ruralRoute = string.Empty;
        private string m_streetName = string.Empty;
        private string m_streetSuffix = string.Empty;
        #endregion

        #region Public Properties
        public string Id
        {
            get { return m_id; }
            set { m_id = value; }
        }
        public string ApartmentOrUnit
        {
            get { return m_apartmentOrUnit; }
            set { m_apartmentOrUnit = value; }
        }
        public string BuildingNumber
        {
            get { return m_buildingNumber; }
            set { m_buildingNumber = value; }
        }
        public string DirectionPrefix
        {
            get { return m_directionPrefix; }
            set { m_directionPrefix = value; }
        }
        public string DirectionSuffix
        {
            get { return m_directionSuffix; }
            set { m_directionSuffix = value; }
        }
        public string HouseNumber
        {
            get { return m_houseNumber; }
            set { m_houseNumber = value; }
        }
        public string MilitaryApoFpo
        {
            get { return m_militaryApoFpo; }
            set { m_militaryApoFpo = value; }
        }
        public string PostOfficeBox
        {
            get { return m_postOfficeBox; }
            set { m_postOfficeBox = value; }
        }
        public string RuralRoute
        {
            get { return m_ruralRoute; }
            set { m_ruralRoute = value; }
        }
        public string StreetName
        {
            get { return m_streetName; }
            set { m_streetName = value; }
        }
        public string StreetSuffix
        {
            get { return m_streetSuffix; }
            set { m_streetSuffix = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "PARSED_STREET_ADDRESS")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_id = ReadAttribute(reader, "_ID");
            m_apartmentOrUnit = ReadAttribute(reader, "_ApartmentOrUnit");
            m_buildingNumber = ReadAttribute(reader, "_BuildingNumber");
            m_directionPrefix = ReadAttribute(reader, "_DirectionPrefix");
            m_directionSuffix = ReadAttribute(reader, "_DirectionSuffix");
            m_houseNumber = ReadAttribute(reader, "_HouseNumber");
            m_militaryApoFpo = ReadAttribute(reader, "_Military_APO_FPO");
            m_postOfficeBox = ReadAttribute(reader, "_PostOfficeBox");
            m_ruralRoute = ReadAttribute(reader, "_RuralRoute");
            m_streetName = ReadAttribute(reader, "_StreetName");
            m_streetSuffix = ReadAttribute(reader, "_StreetSuffix");
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "PARSED_STREET_ADDRESS") return;

            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("PARSED_STREET_ADDRESS");
            WriteAttribute(writer, "_ID", m_id);
            WriteAttribute(writer, "_ApartmentOrUnit", m_apartmentOrUnit);
            WriteAttribute(writer, "_BuildingNumber", m_buildingNumber);
            WriteAttribute(writer, "_DirectionPrefix", m_directionPrefix);
            WriteAttribute(writer, "_DirectionSuffix", m_directionSuffix);
            WriteAttribute(writer, "_HouseNumber", m_houseNumber);
            WriteAttribute(writer, "_Military_APO_FPO", m_militaryApoFpo);
            WriteAttribute(writer, "_PostOfficeBox", m_postOfficeBox);
            WriteAttribute(writer, "_RuralRoute", m_ruralRoute);
            WriteAttribute(writer, "_StreetName", m_streetName);
            WriteAttribute(writer, "_StreetSuffix", m_streetSuffix);
            writer.WriteEndElement();
        }
    }
}
