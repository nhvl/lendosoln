// Generated by CodeMonkey on 2/7/2009 2:23:23 AM
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace Mismo.Closing2_4
{
    public class ContactDetail : AbstractXmlSerializable
    {
        #region Private Member Variables
        private string m_id = string.Empty;
        private string m_firstName = string.Empty;
        private string m_identifier = string.Empty;
        private string m_lastName = string.Empty;
        private string m_middleName = string.Empty;
        private string m_name = string.Empty;
        private string m_nameSuffix = string.Empty;
        private string m_sequenceIdentifier = string.Empty;
        private List<ContactPoint> m_contactPointList = null;
        #endregion

        #region Public Properties
        public string Id
        {
            get { return m_id; }
            set { m_id = value; }
        }
        public string FirstName
        {
            get { return m_firstName; }
            set { m_firstName = value; }
        }
        public string Identifier
        {
            get { return m_identifier; }
            set { m_identifier = value; }
        }
        public string LastName
        {
            get { return m_lastName; }
            set { m_lastName = value; }
        }
        public string MiddleName
        {
            get { return m_middleName; }
            set { m_middleName = value; }
        }
        public string Name
        {
            get { return m_name; }
            set { m_name = value; }
        }
        public string NameSuffix
        {
            get { return m_nameSuffix; }
            set { m_nameSuffix = value; }
        }
        public string SequenceIdentifier
        {
            get { return m_sequenceIdentifier; }
            set { m_sequenceIdentifier = value; }
        }
        public List<ContactPoint> ContactPointList
        {
            get
            {
                if (null == m_contactPointList) m_contactPointList = new List<ContactPoint>();
                return m_contactPointList;
            }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "CONTACT_DETAIL")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_id = ReadAttribute(reader, "_ID");
            m_firstName = ReadAttribute(reader, "_FirstName");
            m_identifier = ReadAttribute(reader, "_Identifier");
            m_lastName = ReadAttribute(reader, "_LastName");
            m_middleName = ReadAttribute(reader, "_MiddleName");
            m_name = ReadAttribute(reader, "_Name");
            m_nameSuffix = ReadAttribute(reader, "_NameSuffix");
            m_sequenceIdentifier = ReadAttribute(reader, "_SequenceIdentifier");
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "CONTACT_DETAIL") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                switch (reader.Name)
                {
                    case "CONTACT_POINT":
                        ReadElement(reader, ContactPointList);
                        break;
                }
                #endregion
            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("CONTACT_DETAIL");
            WriteAttribute(writer, "_ID", m_id);
            WriteAttribute(writer, "_FirstName", m_firstName);
            WriteAttribute(writer, "_Identifier", m_identifier);
            WriteAttribute(writer, "_LastName", m_lastName);
            WriteAttribute(writer, "_MiddleName", m_middleName);
            WriteAttribute(writer, "_Name", m_name);
            WriteAttribute(writer, "_NameSuffix", m_nameSuffix);
            WriteAttribute(writer, "_SequenceIdentifier", m_sequenceIdentifier);
            WriteElement(writer, m_contactPointList);
            writer.WriteEndElement();
        }
    }
}
