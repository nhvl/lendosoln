// Generated by CodeMonkey on 2/7/2009 2:23:24 AM
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace Mismo.Closing2_4
{
    public class TaxableParty : AbstractXmlSerializable
    {
        #region Private Member Variables
        private string m_id = string.Empty;
        private string m_city = string.Empty;
        private string m_country = string.Empty;
        private string m_countryCode = string.Empty;
        private string m_county = string.Empty;
        private string m_postalCode = string.Empty;
        private string m_sequenceIdentifier = string.Empty;
        private string m_state = string.Empty;
        private string m_streetAddress = string.Empty;
        private string m_streetAddress2 = string.Empty;
        private string m_titleDescription = string.Empty;
        private string m_unparsedName = string.Empty;
        private List<PreferredResponse> m_preferredResponseList = null;
        private ContactDetail m_contactDetail = null;
        #endregion

        #region Public Properties
        public string Id
        {
            get { return m_id; }
            set { m_id = value; }
        }
        public string City
        {
            get { return m_city; }
            set { m_city = value; }
        }
        public string Country
        {
            get { return m_country; }
            set { m_country = value; }
        }
        public string CountryCode
        {
            get { return m_countryCode; }
            set { m_countryCode = value; }
        }
        public string County
        {
            get { return m_county; }
            set { m_county = value; }
        }
        public string PostalCode
        {
            get { return m_postalCode; }
            set { m_postalCode = value; }
        }
        public string SequenceIdentifier
        {
            get { return m_sequenceIdentifier; }
            set { m_sequenceIdentifier = value; }
        }
        public string State
        {
            get { return m_state; }
            set { m_state = value; }
        }
        public string StreetAddress
        {
            get { return m_streetAddress; }
            set { m_streetAddress = value; }
        }
        public string StreetAddress2
        {
            get { return m_streetAddress2; }
            set { m_streetAddress2 = value; }
        }
        public string TitleDescription
        {
            get { return m_titleDescription; }
            set { m_titleDescription = value; }
        }
        public string UnparsedName
        {
            get { return m_unparsedName; }
            set { m_unparsedName = value; }
        }
        public List<PreferredResponse> PreferredResponseList
        {
            get
            {
                if (null == m_preferredResponseList) m_preferredResponseList = new List<PreferredResponse>();
                return m_preferredResponseList;
            }
        }
        public ContactDetail ContactDetail
        {
            get
            {
                if (null == m_contactDetail) m_contactDetail = new ContactDetail();
                return m_contactDetail;
            }
            set { m_contactDetail = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "TAXABLE_PARTY")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_id = ReadAttribute(reader, "_ID");
            m_city = ReadAttribute(reader, "_City");
            m_country = ReadAttribute(reader, "_Country");
            m_countryCode = ReadAttribute(reader, "_CountryCode");
            m_county = ReadAttribute(reader, "_County");
            m_postalCode = ReadAttribute(reader, "_PostalCode");
            m_sequenceIdentifier = ReadAttribute(reader, "_SequenceIdentifier");
            m_state = ReadAttribute(reader, "_State");
            m_streetAddress = ReadAttribute(reader, "_StreetAddress");
            m_streetAddress2 = ReadAttribute(reader, "_StreetAddress2");
            m_titleDescription = ReadAttribute(reader, "_TitleDescription");
            m_unparsedName = ReadAttribute(reader, "_UnparsedName");
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "TAXABLE_PARTY") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                switch (reader.Name)
                {
                    case "PREFERRED_RESPONSE":
                        ReadElement(reader, PreferredResponseList);
                        break;
                    case "CONTACT_DETAIL":
                        ReadElement(reader, ContactDetail);
                        break;
                }
                #endregion
            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("TAXABLE_PARTY");
            WriteAttribute(writer, "_ID", m_id);
            WriteAttribute(writer, "_City", m_city);
            WriteAttribute(writer, "_Country", m_country);
            WriteAttribute(writer, "_CountryCode", m_countryCode);
            WriteAttribute(writer, "_County", m_county);
            WriteAttribute(writer, "_PostalCode", m_postalCode);
            WriteAttribute(writer, "_SequenceIdentifier", m_sequenceIdentifier);
            WriteAttribute(writer, "_State", m_state);
            WriteAttribute(writer, "_StreetAddress", m_streetAddress);
            WriteAttribute(writer, "_StreetAddress2", m_streetAddress2);
            WriteAttribute(writer, "_TitleDescription", m_titleDescription);
            WriteAttribute(writer, "_UnparsedName", m_unparsedName);
            WriteElement(writer, m_preferredResponseList);
            WriteElement(writer, m_contactDetail);
            writer.WriteEndElement();
        }
    }
}
