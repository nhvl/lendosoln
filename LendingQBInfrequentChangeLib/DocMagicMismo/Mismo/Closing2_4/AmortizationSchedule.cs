// Generated by CodeMonkey on 2/7/2009 2:23:24 AM
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace Mismo.Closing2_4
{
    public class AmortizationSchedule : AbstractXmlSerializable
    {
        #region Private Member Variables
        private string m_id = string.Empty;
        private string m_endingBalanceAmount = string.Empty;
        private string m_interestRatePercent = string.Empty;
        private string m_loanToValuePercent = string.Empty;
        private string m_miPaymentAmount = string.Empty;
        private string m_paymentAmount = string.Empty;
        private string m_paymentDueDate = string.Empty;
        private string m_paymentNumber = string.Empty;
        private string m_portionOfPaymentDistributedToInterestAmount = string.Empty;
        private string m_portionOfPaymentDistributedToPrincipalAmount = string.Empty;
        #endregion

        #region Public Properties
        public string Id
        {
            get { return m_id; }
            set { m_id = value; }
        }
        public string EndingBalanceAmount
        {
            get { return m_endingBalanceAmount; }
            set { m_endingBalanceAmount = value; }
        }
        public string InterestRatePercent
        {
            get { return m_interestRatePercent; }
            set { m_interestRatePercent = value; }
        }
        public string LoanToValuePercent
        {
            get { return m_loanToValuePercent; }
            set { m_loanToValuePercent = value; }
        }
        public string MiPaymentAmount
        {
            get { return m_miPaymentAmount; }
            set { m_miPaymentAmount = value; }
        }
        public string PaymentAmount
        {
            get { return m_paymentAmount; }
            set { m_paymentAmount = value; }
        }
        public string PaymentDueDate
        {
            get { return m_paymentDueDate; }
            set { m_paymentDueDate = value; }
        }
        public string PaymentNumber
        {
            get { return m_paymentNumber; }
            set { m_paymentNumber = value; }
        }
        public string PortionOfPaymentDistributedToInterestAmount
        {
            get { return m_portionOfPaymentDistributedToInterestAmount; }
            set { m_portionOfPaymentDistributedToInterestAmount = value; }
        }
        public string PortionOfPaymentDistributedToPrincipalAmount
        {
            get { return m_portionOfPaymentDistributedToPrincipalAmount; }
            set { m_portionOfPaymentDistributedToPrincipalAmount = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "AMORTIZATION_SCHEDULE")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_id = ReadAttribute(reader, "_ID");
            m_endingBalanceAmount = ReadAttribute(reader, "_EndingBalanceAmount");
            m_interestRatePercent = ReadAttribute(reader, "_InterestRatePercent");
            m_loanToValuePercent = ReadAttribute(reader, "_LoanToValuePercent");
            m_miPaymentAmount = ReadAttribute(reader, "_MIPaymentAmount");
            m_paymentAmount = ReadAttribute(reader, "_PaymentAmount");
            m_paymentDueDate = ReadAttribute(reader, "_PaymentDueDate");
            m_paymentNumber = ReadAttribute(reader, "_PaymentNumber");
            m_portionOfPaymentDistributedToInterestAmount = ReadAttribute(reader, "_PortionOfPaymentDistributedToInterestAmount");
            m_portionOfPaymentDistributedToPrincipalAmount = ReadAttribute(reader, "_PortionOfPaymentDistributedToPrincipalAmount");
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "AMORTIZATION_SCHEDULE") return;

            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("AMORTIZATION_SCHEDULE");
            WriteAttribute(writer, "_ID", m_id);
            WriteAttribute(writer, "_EndingBalanceAmount", m_endingBalanceAmount);
            WriteAttribute(writer, "_InterestRatePercent", m_interestRatePercent);
            WriteAttribute(writer, "_LoanToValuePercent", m_loanToValuePercent);
            WriteAttribute(writer, "_MIPaymentAmount", m_miPaymentAmount);
            WriteAttribute(writer, "_PaymentAmount", m_paymentAmount);
            WriteAttribute(writer, "_PaymentDueDate", m_paymentDueDate);
            WriteAttribute(writer, "_PaymentNumber", m_paymentNumber);
            WriteAttribute(writer, "_PortionOfPaymentDistributedToInterestAmount", m_portionOfPaymentDistributedToInterestAmount);
            WriteAttribute(writer, "_PortionOfPaymentDistributedToPrincipalAmount", m_portionOfPaymentDistributedToPrincipalAmount);
            writer.WriteEndElement();
        }
    }
}
