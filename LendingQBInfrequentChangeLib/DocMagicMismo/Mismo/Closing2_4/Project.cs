// Generated by CodeMonkey on 2/7/2009 2:23:24 AM
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace Mismo.Closing2_4
{
    public class Project : AbstractXmlSerializable
    {
        #region Private Member Variables
        private string m_id = string.Empty;
        private string m_livingUnitCount = string.Empty;
        private E_ProjectClassificationType m_classificationType = E_ProjectClassificationType.Undefined;
        private string m_classificationTypeOtherDescription = string.Empty;
        private E_ProjectDesignType m_designType = E_ProjectDesignType.Undefined;
        private string m_designTypeOtherDescription = string.Empty;
        #endregion

        #region Public Properties
        public string Id
        {
            get { return m_id; }
            set { m_id = value; }
        }
        public string LivingUnitCount
        {
            get { return m_livingUnitCount; }
            set { m_livingUnitCount = value; }
        }
        public E_ProjectClassificationType ClassificationType
        {
            get { return m_classificationType; }
            set { m_classificationType = value; }
        }
        public string ClassificationTypeOtherDescription
        {
            get { return m_classificationTypeOtherDescription; }
            set { m_classificationTypeOtherDescription = value; }
        }
        public E_ProjectDesignType DesignType
        {
            get { return m_designType; }
            set { m_designType = value; }
        }
        public string DesignTypeOtherDescription
        {
            get { return m_designTypeOtherDescription; }
            set { m_designTypeOtherDescription = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "PROJECT")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_id = ReadAttribute(reader, "_ID");
            m_livingUnitCount = ReadAttribute(reader, "LivingUnitCount");
            m_classificationType = (E_ProjectClassificationType) ReadAttribute(reader, "_ClassificationType", EnumMappings.E_ProjectClassificationType);
            m_classificationTypeOtherDescription = ReadAttribute(reader, "_ClassificationTypeOtherDescription");
            m_designType = (E_ProjectDesignType) ReadAttribute(reader, "_DesignType", EnumMappings.E_ProjectDesignType);
            m_designTypeOtherDescription = ReadAttribute(reader, "_DesignTypeOtherDescription");
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "PROJECT") return;

            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("PROJECT");
            WriteAttribute(writer, "_ID", m_id);
            WriteAttribute(writer, "LivingUnitCount", m_livingUnitCount);
            WriteEnumAttribute(writer, "_ClassificationType", EnumMappings.E_ProjectClassificationType[(int) m_classificationType]);
            WriteAttribute(writer, "_ClassificationTypeOtherDescription", m_classificationTypeOtherDescription);
            WriteEnumAttribute(writer, "_DesignType", EnumMappings.E_ProjectDesignType[(int) m_designType]);
            WriteAttribute(writer, "_DesignTypeOtherDescription", m_designTypeOtherDescription);
            writer.WriteEndElement();
        }
    }

    public enum E_ProjectClassificationType
    {
        Undefined
        , Condominium
        , Cooperative
        , Other
        , PUD
    }

    public enum E_ProjectDesignType
    {
        Undefined
        , Attached
        , Detached
        , GardenProject
        , HighriseProject
        , MidriseProject
        , Other
        , TownhouseRowhouse
    }

    static partial class EnumMappings
    {
        public static readonly string[] E_ProjectClassificationType = {
                        ""
                         , "Condominium"
                         , "Cooperative"
                         , "Other"
                         , "PUD"
                         };
        public static readonly string[] E_ProjectDesignType = {
                        ""
                         , "Attached"
                         , "Detached"
                         , "GardenProject"
                         , "HighriseProject"
                         , "MidriseProject"
                         , "Other"
                         , "TownhouseRowhouse"
                         };
    }
}
