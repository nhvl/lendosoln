// Generated by CodeMonkey on 2/7/2009 2:23:24 AM
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace Mismo.Closing2_4
{
    public class Appraiser : AbstractXmlSerializable
    {
        #region Private Member Variables
        private string m_id = string.Empty;
        private string m_companyName = string.Empty;
        private string m_licenseIdentifier = string.Empty;
        private string m_licenseState = string.Empty;
        private string m_name = string.Empty;
        #endregion

        #region Public Properties
        public string Id
        {
            get { return m_id; }
            set { m_id = value; }
        }
        public string CompanyName
        {
            get { return m_companyName; }
            set { m_companyName = value; }
        }
        public string LicenseIdentifier
        {
            get { return m_licenseIdentifier; }
            set { m_licenseIdentifier = value; }
        }
        public string LicenseState
        {
            get { return m_licenseState; }
            set { m_licenseState = value; }
        }
        public string Name
        {
            get { return m_name; }
            set { m_name = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "APPRAISER")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_id = ReadAttribute(reader, "_ID");
            m_companyName = ReadAttribute(reader, "_CompanyName");
            m_licenseIdentifier = ReadAttribute(reader, "_LicenseIdentifier");
            m_licenseState = ReadAttribute(reader, "_LicenseState");
            m_name = ReadAttribute(reader, "_Name");
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "APPRAISER") return;

            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("APPRAISER");
            WriteAttribute(writer, "_ID", m_id);
            WriteAttribute(writer, "_CompanyName", m_companyName);
            WriteAttribute(writer, "_LicenseIdentifier", m_licenseIdentifier);
            WriteAttribute(writer, "_LicenseState", m_licenseState);
            WriteAttribute(writer, "_Name", m_name);
            writer.WriteEndElement();
        }
    }
}
