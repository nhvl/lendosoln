// Generated by CodeMonkey on 2/7/2009 2:23:24 AM
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace Mismo.Closing2_4
{
    public class Payoff : AbstractXmlSerializable
    {
        #region Private Member Variables
        private string m_id = string.Empty;
        private string m_accountNumberIdentifier = string.Empty;
        private string m_amount = string.Empty;
        private string m_perDiemAmount = string.Empty;
        private string m_sequenceIdentifier = string.Empty;
        private string m_specifiedHudLineNumber = string.Empty;
        private string m_throughDate = string.Empty;
        private Payee m_payee = null;
        #endregion

        #region Public Properties
        public string Id
        {
            get { return m_id; }
            set { m_id = value; }
        }
        public string AccountNumberIdentifier
        {
            get { return m_accountNumberIdentifier; }
            set { m_accountNumberIdentifier = value; }
        }
        public string Amount
        {
            get { return m_amount; }
            set { m_amount = value; }
        }
        public string PerDiemAmount
        {
            get { return m_perDiemAmount; }
            set { m_perDiemAmount = value; }
        }
        public string SequenceIdentifier
        {
            get { return m_sequenceIdentifier; }
            set { m_sequenceIdentifier = value; }
        }
        public string SpecifiedHudLineNumber
        {
            get { return m_specifiedHudLineNumber; }
            set { m_specifiedHudLineNumber = value; }
        }
        public string ThroughDate
        {
            get { return m_throughDate; }
            set { m_throughDate = value; }
        }
        public Payee Payee
        {
            get
            {
                if (null == m_payee) m_payee = new Payee();
                return m_payee;
            }
            set { m_payee = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "PAYOFF")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_id = ReadAttribute(reader, "_ID");
            m_accountNumberIdentifier = ReadAttribute(reader, "_AccountNumberIdentifier");
            m_amount = ReadAttribute(reader, "_Amount");
            m_perDiemAmount = ReadAttribute(reader, "_PerDiemAmount");
            m_sequenceIdentifier = ReadAttribute(reader, "_SequenceIdentifier");
            m_specifiedHudLineNumber = ReadAttribute(reader, "_SpecifiedHUDLineNumber");
            m_throughDate = ReadAttribute(reader, "_ThroughDate");
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "PAYOFF") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                switch (reader.Name)
                {
                    case "PAYEE":
                        ReadElement(reader, Payee);
                        break;
                }
                #endregion
            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("PAYOFF");
            WriteAttribute(writer, "_ID", m_id);
            WriteAttribute(writer, "_AccountNumberIdentifier", m_accountNumberIdentifier);
            WriteAttribute(writer, "_Amount", m_amount);
            WriteAttribute(writer, "_PerDiemAmount", m_perDiemAmount);
            WriteAttribute(writer, "_SequenceIdentifier", m_sequenceIdentifier);
            WriteAttribute(writer, "_SpecifiedHUDLineNumber", m_specifiedHudLineNumber);
            WriteAttribute(writer, "_ThroughDate", m_throughDate);
            WriteElement(writer, m_payee);
            writer.WriteEndElement();
        }
    }
}
