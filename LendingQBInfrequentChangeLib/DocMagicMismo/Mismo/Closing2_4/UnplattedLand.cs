// Generated by CodeMonkey on 2/7/2009 2:23:24 AM
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace Mismo.Closing2_4
{
    public class UnplattedLand : AbstractXmlSerializable
    {
        #region Private Member Variables
        private string m_id = string.Empty;
        private string m_propertyRangeIdentifier = string.Empty;
        private string m_propertySectionIdentifier = string.Empty;
        private string m_propertyTownshipIdentifier = string.Empty;
        private string m_abstractNumberIdentifier = string.Empty;
        private string m_baseIdentifier = string.Empty;
        private E_UnplattedLandDescriptionType m_descriptionType = E_UnplattedLandDescriptionType.Undefined;
        private string m_descriptionTypeOtherDescription = string.Empty;
        private string m_landGrantIdentifier = string.Empty;
        private string m_meridianIdentifier = string.Empty;
        private string m_metesAndBoundsRemainingDescription = string.Empty;
        private string m_quarterSectionIdentifier = string.Empty;
        private string m_sequenceIdentifier = string.Empty;
        #endregion

        #region Public Properties
        public string Id
        {
            get { return m_id; }
            set { m_id = value; }
        }
        public string PropertyRangeIdentifier
        {
            get { return m_propertyRangeIdentifier; }
            set { m_propertyRangeIdentifier = value; }
        }
        public string PropertySectionIdentifier
        {
            get { return m_propertySectionIdentifier; }
            set { m_propertySectionIdentifier = value; }
        }
        public string PropertyTownshipIdentifier
        {
            get { return m_propertyTownshipIdentifier; }
            set { m_propertyTownshipIdentifier = value; }
        }
        public string AbstractNumberIdentifier
        {
            get { return m_abstractNumberIdentifier; }
            set { m_abstractNumberIdentifier = value; }
        }
        public string BaseIdentifier
        {
            get { return m_baseIdentifier; }
            set { m_baseIdentifier = value; }
        }
        public E_UnplattedLandDescriptionType DescriptionType
        {
            get { return m_descriptionType; }
            set { m_descriptionType = value; }
        }
        public string DescriptionTypeOtherDescription
        {
            get { return m_descriptionTypeOtherDescription; }
            set { m_descriptionTypeOtherDescription = value; }
        }
        public string LandGrantIdentifier
        {
            get { return m_landGrantIdentifier; }
            set { m_landGrantIdentifier = value; }
        }
        public string MeridianIdentifier
        {
            get { return m_meridianIdentifier; }
            set { m_meridianIdentifier = value; }
        }
        public string MetesAndBoundsRemainingDescription
        {
            get { return m_metesAndBoundsRemainingDescription; }
            set { m_metesAndBoundsRemainingDescription = value; }
        }
        public string QuarterSectionIdentifier
        {
            get { return m_quarterSectionIdentifier; }
            set { m_quarterSectionIdentifier = value; }
        }
        public string SequenceIdentifier
        {
            get { return m_sequenceIdentifier; }
            set { m_sequenceIdentifier = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "UNPLATTED_LAND")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_id = ReadAttribute(reader, "_ID");
            m_propertyRangeIdentifier = ReadAttribute(reader, "PropertyRangeIdentifier");
            m_propertySectionIdentifier = ReadAttribute(reader, "PropertySectionIdentifier");
            m_propertyTownshipIdentifier = ReadAttribute(reader, "PropertyTownshipIdentifier");
            m_abstractNumberIdentifier = ReadAttribute(reader, "_AbstractNumberIdentifier");
            m_baseIdentifier = ReadAttribute(reader, "_BaseIdentifier");
            m_descriptionType = (E_UnplattedLandDescriptionType) ReadAttribute(reader, "_DescriptionType", EnumMappings.E_UnplattedLandDescriptionType);
            m_descriptionTypeOtherDescription = ReadAttribute(reader, "_DescriptionTypeOtherDescription");
            m_landGrantIdentifier = ReadAttribute(reader, "_LandGrantIdentifier");
            m_meridianIdentifier = ReadAttribute(reader, "_MeridianIdentifier");
            m_metesAndBoundsRemainingDescription = ReadAttribute(reader, "_MetesAndBoundsRemainingDescription");
            m_quarterSectionIdentifier = ReadAttribute(reader, "_QuarterSectionIdentifier");
            m_sequenceIdentifier = ReadAttribute(reader, "_SequenceIdentifier");
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "UNPLATTED_LAND") return;

            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("UNPLATTED_LAND");
            WriteAttribute(writer, "_ID", m_id);
            WriteAttribute(writer, "PropertyRangeIdentifier", m_propertyRangeIdentifier);
            WriteAttribute(writer, "PropertySectionIdentifier", m_propertySectionIdentifier);
            WriteAttribute(writer, "PropertyTownshipIdentifier", m_propertyTownshipIdentifier);
            WriteAttribute(writer, "_AbstractNumberIdentifier", m_abstractNumberIdentifier);
            WriteAttribute(writer, "_BaseIdentifier", m_baseIdentifier);
            WriteEnumAttribute(writer, "_DescriptionType", EnumMappings.E_UnplattedLandDescriptionType[(int) m_descriptionType]);
            WriteAttribute(writer, "_DescriptionTypeOtherDescription", m_descriptionTypeOtherDescription);
            WriteAttribute(writer, "_LandGrantIdentifier", m_landGrantIdentifier);
            WriteAttribute(writer, "_MeridianIdentifier", m_meridianIdentifier);
            WriteAttribute(writer, "_MetesAndBoundsRemainingDescription", m_metesAndBoundsRemainingDescription);
            WriteAttribute(writer, "_QuarterSectionIdentifier", m_quarterSectionIdentifier);
            WriteAttribute(writer, "_SequenceIdentifier", m_sequenceIdentifier);
            writer.WriteEndElement();
        }
    }

    public enum E_UnplattedLandDescriptionType
    {
        Undefined
        , GovernmentSurvey
        , LandGrant
        , MetesAndBounds
        , NativeAmericanLandIdentifier
        , RancheroIdentifier
        , TownshipIdentifier
        , Other
    }

    static partial class EnumMappings
    {
        public static readonly string[] E_UnplattedLandDescriptionType = {
                        ""
                         , "GovernmentSurvey"
                         , "LandGrant"
                         , "MetesAndBounds"
                         , "NativeAmericanLandIdentifier"
                         , "RancheroIdentifier"
                         , "TownshipIdentifier"
                         , "Other"
                         };
    }
}
