// Generated by CodeMonkey on 2/7/2009 2:23:24 AM
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace Mismo.Closing2_4
{
    public class Valuation : AbstractXmlSerializable
    {
        #region Private Member Variables
        private string m_id = string.Empty;
        private E_ValuationAppraisalFormType m_appraisalFormType = E_ValuationAppraisalFormType.Undefined;
        private string m_appraisalFormTypeOtherDescription = string.Empty;
        private string m_appraisalFormVersionIdentifier = string.Empty;
        private E_ValuationAppraisalInspectionType m_appraisalInspectionType = E_ValuationAppraisalInspectionType.Undefined;
        private string m_appraisalInspectionTypeOtherDescription = string.Empty;
        private E_ValuationMethodType m_methodType = E_ValuationMethodType.Undefined;
        private string m_methodTypeOtherDescription = string.Empty;
        private List<Appraiser> m_appraiserList = null;
        #endregion

        #region Public Properties
        public string Id
        {
            get { return m_id; }
            set { m_id = value; }
        }
        public E_ValuationAppraisalFormType AppraisalFormType
        {
            get { return m_appraisalFormType; }
            set { m_appraisalFormType = value; }
        }
        public string AppraisalFormTypeOtherDescription
        {
            get { return m_appraisalFormTypeOtherDescription; }
            set { m_appraisalFormTypeOtherDescription = value; }
        }
        public string AppraisalFormVersionIdentifier
        {
            get { return m_appraisalFormVersionIdentifier; }
            set { m_appraisalFormVersionIdentifier = value; }
        }
        public E_ValuationAppraisalInspectionType AppraisalInspectionType
        {
            get { return m_appraisalInspectionType; }
            set { m_appraisalInspectionType = value; }
        }
        public string AppraisalInspectionTypeOtherDescription
        {
            get { return m_appraisalInspectionTypeOtherDescription; }
            set { m_appraisalInspectionTypeOtherDescription = value; }
        }
        public E_ValuationMethodType MethodType
        {
            get { return m_methodType; }
            set { m_methodType = value; }
        }
        public string MethodTypeOtherDescription
        {
            get { return m_methodTypeOtherDescription; }
            set { m_methodTypeOtherDescription = value; }
        }
        public List<Appraiser> AppraiserList
        {
            get
            {
                if (null == m_appraiserList) m_appraiserList = new List<Appraiser>();
                return m_appraiserList;
            }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "_VALUATION")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_id = ReadAttribute(reader, "_ID");
            m_appraisalFormType = (E_ValuationAppraisalFormType) ReadAttribute(reader, "AppraisalFormType", EnumMappings.E_ValuationAppraisalFormType);
            m_appraisalFormTypeOtherDescription = ReadAttribute(reader, "AppraisalFormTypeOtherDescription");
            m_appraisalFormVersionIdentifier = ReadAttribute(reader, "AppraisalFormVersionIdentifier");
            m_appraisalInspectionType = (E_ValuationAppraisalInspectionType) ReadAttribute(reader, "AppraisalInspectionType", EnumMappings.E_ValuationAppraisalInspectionType);
            m_appraisalInspectionTypeOtherDescription = ReadAttribute(reader, "AppraisalInspectionTypeOtherDescription");
            m_methodType = (E_ValuationMethodType) ReadAttribute(reader, "_MethodType", EnumMappings.E_ValuationMethodType);
            m_methodTypeOtherDescription = ReadAttribute(reader, "_MethodTypeOtherDescription");
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "_VALUATION") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                switch (reader.Name)
                {
                    case "APPRAISER":
                        ReadElement(reader, AppraiserList);
                        break;
                }
                #endregion
            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("_VALUATION");
            WriteAttribute(writer, "_ID", m_id);
            WriteEnumAttribute(writer, "AppraisalFormType", EnumMappings.E_ValuationAppraisalFormType[(int) m_appraisalFormType]);
            WriteAttribute(writer, "AppraisalFormTypeOtherDescription", m_appraisalFormTypeOtherDescription);
            WriteAttribute(writer, "AppraisalFormVersionIdentifier", m_appraisalFormVersionIdentifier);
            WriteEnumAttribute(writer, "AppraisalInspectionType", EnumMappings.E_ValuationAppraisalInspectionType[(int) m_appraisalInspectionType]);
            WriteAttribute(writer, "AppraisalInspectionTypeOtherDescription", m_appraisalInspectionTypeOtherDescription);
            WriteEnumAttribute(writer, "_MethodType", EnumMappings.E_ValuationMethodType[(int) m_methodType]);
            WriteAttribute(writer, "_MethodTypeOtherDescription", m_methodTypeOtherDescription);
            WriteElement(writer, m_appraiserList);
            writer.WriteEndElement();
        }
    }

    public enum E_ValuationAppraisalFormType
    {
        Undefined
        , FNM1004FRE70
        , FNM1004BFRE439
        , FNM1004CFRE70B
        , FNM1004DFRE442
        , ERC2001
        , FNM1073FRE465
        , FNM1075FRE466
        , FNM1025FRE72
        , FNM2000FRE1032
        , FNM2000AFRE1072
        , FNM2055FRE2055
        , FNM2065
        , FNM2075
        , FNM2090
        , FNM2095
        , MobileHome
        , VacantLand
        , FRE2070
        , Other
    }

    public enum E_ValuationAppraisalInspectionType
    {
        Undefined
        , ExteriorAndInterior
        , ExteriorOnly
        , None
        , Other
    }

    public enum E_ValuationMethodType
    {
        Undefined
        , FNM1004
        , EmployeeRelocationCouncil2001
        , FNM1073
        , FNM1025
        , FNM2055Exterior
        , FNM2065
        , FRE2070Interior
        , FRE2070Exterior
        , FNM2075
        , BrokerPriceOpinion
        , AutomatedValuationModel
        , TaxValuation
        , DriveBy
        , FullAppraisal
        , None
        , FNM2055InteriorAndExterior
        , FNM2095Exterior
        , FNM2095InteriorAndExterior
        , PriorAppraisalUsed
        , Form261805
        , Form268712
        , Other
    }

    static partial class EnumMappings
    {
        public static readonly string[] E_ValuationAppraisalFormType = {
                        ""
                         , "FNM1004FRE70"
                         , "FNM1004BFRE439"
                         , "FNM1004CFRE70B"
                         , "FNM1004DFRE442"
                         , "ERC2001"
                         , "FNM1073FRE465"
                         , "FNM1075FRE466"
                         , "FNM1025FRE72"
                         , "FNM2000FRE1032"
                         , "FNM2000AFRE1072"
                         , "FNM2055FRE2055"
                         , "FNM2065"
                         , "FNM2075"
                         , "FNM2090"
                         , "FNM2095"
                         , "MobileHome"
                         , "VacantLand"
                         , "FRE2070"
                         , "Other"
                         };
        public static readonly string[] E_ValuationAppraisalInspectionType = {
                        ""
                         , "ExteriorAndInterior"
                         , "ExteriorOnly"
                         , "None"
                         , "Other"
                         };
        public static readonly string[] E_ValuationMethodType = {
                        ""
                         , "FNM1004"
                         , "EmployeeRelocationCouncil2001"
                         , "FNM1073"
                         , "FNM1025"
                         , "FNM2055Exterior"
                         , "FNM2065"
                         , "FRE2070Interior"
                         , "FRE2070Exterior"
                         , "FNM2075"
                         , "BrokerPriceOpinion"
                         , "AutomatedValuationModel"
                         , "TaxValuation"
                         , "DriveBy"
                         , "FullAppraisal"
                         , "None"
                         , "FNM2055InteriorAndExterior"
                         , "FNM2095Exterior"
                         , "FNM2095InteriorAndExterior"
                         , "PriorAppraisalUsed"
                         , "Form261805"
                         , "Form268712"
                         , "Other"
                         };
    }
}
