// Generated by CodeMonkey on 3/22/2010 6:06:41 PM
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace Mismo.Closing2_6
{
    public class ReoProperty : AbstractXmlSerializable
    {
        #region Private Member Variables
        private string m_ReoId = string.Empty;
        private string m_LiabilityId = string.Empty;
        private string m_BorrowerId = string.Empty;
        private string m_City = string.Empty;
        private E_YNIndicator m_CurrentResidenceIndicator = E_YNIndicator.Undefined;
        private E_ReoPropertyDispositionStatusType m_DispositionStatusType = E_ReoPropertyDispositionStatusType.Undefined;
        private E_ReoPropertyGsePropertyType m_GsePropertyType = E_ReoPropertyGsePropertyType.Undefined;
        private string m_LienInstallmentAmount = string.Empty;
        private string m_LienUpbAmount = string.Empty;
        private string m_MaintenanceExpenseAmount = string.Empty;
        private string m_MarketValueAmount = string.Empty;
        private string m_PostalCode = string.Empty;
        private string m_RentalIncomeGrossAmount = string.Empty;
        private string m_RentalIncomeNetAmount = string.Empty;
        private string m_State = string.Empty;
        private string m_StreetAddress = string.Empty;
        private string m_StreetAddress2 = string.Empty;
        private E_YNIndicator m_SubjectIndicator = E_YNIndicator.Undefined;
        #endregion

        #region Public Properties
        public string ReoId
        {
            get { return m_ReoId; }
            set { m_ReoId = value; }
        }
        public string LiabilityId
        {
            get { return m_LiabilityId; }
            set { m_LiabilityId = value; }
        }
        public string BorrowerId
        {
            get { return m_BorrowerId; }
            set { m_BorrowerId = value; }
        }
        public string City
        {
            get { return m_City; }
            set { m_City = value; }
        }
        public E_YNIndicator CurrentResidenceIndicator
        {
            get { return m_CurrentResidenceIndicator; }
            set { m_CurrentResidenceIndicator = value; }
        }
        public E_ReoPropertyDispositionStatusType DispositionStatusType
        {
            get { return m_DispositionStatusType; }
            set { m_DispositionStatusType = value; }
        }
        public E_ReoPropertyGsePropertyType GsePropertyType
        {
            get { return m_GsePropertyType; }
            set { m_GsePropertyType = value; }
        }
        public string LienInstallmentAmount
        {
            get { return m_LienInstallmentAmount; }
            set { m_LienInstallmentAmount = value; }
        }
        public string LienUpbAmount
        {
            get { return m_LienUpbAmount; }
            set { m_LienUpbAmount = value; }
        }
        public string MaintenanceExpenseAmount
        {
            get { return m_MaintenanceExpenseAmount; }
            set { m_MaintenanceExpenseAmount = value; }
        }
        public string MarketValueAmount
        {
            get { return m_MarketValueAmount; }
            set { m_MarketValueAmount = value; }
        }
        public string PostalCode
        {
            get { return m_PostalCode; }
            set { m_PostalCode = value; }
        }
        public string RentalIncomeGrossAmount
        {
            get { return m_RentalIncomeGrossAmount; }
            set { m_RentalIncomeGrossAmount = value; }
        }
        public string RentalIncomeNetAmount
        {
            get { return m_RentalIncomeNetAmount; }
            set { m_RentalIncomeNetAmount = value; }
        }
        public string State
        {
            get { return m_State; }
            set { m_State = value; }
        }
        public string StreetAddress
        {
            get { return m_StreetAddress; }
            set { m_StreetAddress = value; }
        }
        public string StreetAddress2
        {
            get { return m_StreetAddress2; }
            set { m_StreetAddress2 = value; }
        }
        public E_YNIndicator SubjectIndicator
        {
            get { return m_SubjectIndicator; }
            set { m_SubjectIndicator = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "REO_PROPERTY")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_ReoId = ReadAttribute(reader, "REO_ID");
            m_LiabilityId = ReadAttribute(reader, "LiabilityID");
            m_BorrowerId = ReadAttribute(reader, "BorrowerID");
            m_City = ReadAttribute(reader, "_City");
            m_CurrentResidenceIndicator = (E_YNIndicator) ReadAttribute(reader, "_CurrentResidenceIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator);
            m_DispositionStatusType = (E_ReoPropertyDispositionStatusType) ReadAttribute(reader, "_DispositionStatusType", EnumMappings.E_ReoPropertyDispositionStatusType);
            m_GsePropertyType = (E_ReoPropertyGsePropertyType) ReadAttribute(reader, "_GSEPropertyType", EnumMappings.E_ReoPropertyGsePropertyType);
            m_LienInstallmentAmount = ReadAttribute(reader, "_LienInstallmentAmount");
            m_LienUpbAmount = ReadAttribute(reader, "_LienUPBAmount");
            m_MaintenanceExpenseAmount = ReadAttribute(reader, "_MaintenanceExpenseAmount");
            m_MarketValueAmount = ReadAttribute(reader, "_MarketValueAmount");
            m_PostalCode = ReadAttribute(reader, "_PostalCode");
            m_RentalIncomeGrossAmount = ReadAttribute(reader, "_RentalIncomeGrossAmount");
            m_RentalIncomeNetAmount = ReadAttribute(reader, "_RentalIncomeNetAmount");
            m_State = ReadAttribute(reader, "_State");
            m_StreetAddress = ReadAttribute(reader, "_StreetAddress");
            m_StreetAddress2 = ReadAttribute(reader, "_StreetAddress2");
            m_SubjectIndicator = (E_YNIndicator) ReadAttribute(reader, "_SubjectIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator);
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "REO_PROPERTY") return;

            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("REO_PROPERTY");
            WriteAttribute(writer, "REO_ID", m_ReoId);
            WriteAttribute(writer, "LiabilityID", m_LiabilityId);
            WriteAttribute(writer, "BorrowerID", m_BorrowerId);
            WriteAttribute(writer, "_City", m_City);
            WriteEnumAttribute(writer, "_CurrentResidenceIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator[(int) m_CurrentResidenceIndicator]);
            WriteEnumAttribute(writer, "_DispositionStatusType", EnumMappings.E_ReoPropertyDispositionStatusType[(int) m_DispositionStatusType]);
            WriteEnumAttribute(writer, "_GSEPropertyType", EnumMappings.E_ReoPropertyGsePropertyType[(int) m_GsePropertyType]);
            WriteAttribute(writer, "_LienInstallmentAmount", m_LienInstallmentAmount);
            WriteAttribute(writer, "_LienUPBAmount", m_LienUpbAmount);
            WriteAttribute(writer, "_MaintenanceExpenseAmount", m_MaintenanceExpenseAmount);
            WriteAttribute(writer, "_MarketValueAmount", m_MarketValueAmount);
            WriteAttribute(writer, "_PostalCode", m_PostalCode);
            WriteAttribute(writer, "_RentalIncomeGrossAmount", m_RentalIncomeGrossAmount);
            WriteAttribute(writer, "_RentalIncomeNetAmount", m_RentalIncomeNetAmount);
            WriteAttribute(writer, "_State", m_State);
            WriteAttribute(writer, "_StreetAddress", m_StreetAddress);
            WriteAttribute(writer, "_StreetAddress2", m_StreetAddress2);
            WriteEnumAttribute(writer, "_SubjectIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator[(int) m_SubjectIndicator]);
            writer.WriteEndElement();
        }
    }

    public enum E_ReoPropertyDispositionStatusType
    {
        Undefined
        , PendingSale
        , RetainForRental
        , RetainForPrimaryOrSecondaryResidence
        , Sold
    }

    public enum E_ReoPropertyGsePropertyType
    {
        Undefined
        , SingleFamily
        , Condominium
        , Townhouse
        , Cooperative
        , TwoToFourUnitProperty
        , MultifamilyMoreThanFourUnits
        , ManufacturedMobileHome
        , CommercialNonResidential
        , MixedUseResidential
        , Farm
        , HomeAndBusinessCombined
        , Land
    }

    static partial class EnumMappings
    {
        public static readonly string[] E_ReoPropertyDispositionStatusType = {
                        ""
                         , "PendingSale"
                         , "RetainForRental"
                         , "RetainForPrimaryOrSecondaryResidence"
                         , "Sold"
                         };
        public static readonly string[] E_ReoPropertyGsePropertyType = {
                        ""
                         , "SingleFamily"
                         , "Condominium"
                         , "Townhouse"
                         , "Cooperative"
                         , "TwoToFourUnitProperty"
                         , "MultifamilyMoreThanFourUnits"
                         , "ManufacturedMobileHome"
                         , "CommercialNonResidential"
                         , "MixedUseResidential"
                         , "Farm"
                         , "HomeAndBusinessCombined"
                         , "Land"
                         };
    }
}
