// Generated by CodeMonkey on 3/22/2010 6:06:43 PM
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace Mismo.Closing2_6
{
    public class SignerIdentification : AbstractXmlSerializable
    {
        #region Private Member Variables
        private string m_Id = string.Empty;
        private string m_Description = string.Empty;
        private string m_SequenceIdentifier = string.Empty;
        private E_SignerIdentificationType m_Type = E_SignerIdentificationType.Undefined;
        #endregion

        #region Public Properties
        public string Id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }
        public string Description
        {
            get { return m_Description; }
            set { m_Description = value; }
        }
        public string SequenceIdentifier
        {
            get { return m_SequenceIdentifier; }
            set { m_SequenceIdentifier = value; }
        }
        public E_SignerIdentificationType Type
        {
            get { return m_Type; }
            set { m_Type = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "_SIGNER_IDENTIFICATION")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_Id = ReadAttribute(reader, "_ID");
            m_Description = ReadAttribute(reader, "_Description");
            m_SequenceIdentifier = ReadAttribute(reader, "_SequenceIdentifier");
            m_Type = (E_SignerIdentificationType) ReadAttribute(reader, "_Type", EnumMappings.E_SignerIdentificationType);
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "_SIGNER_IDENTIFICATION") return;

            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("_SIGNER_IDENTIFICATION");
            WriteAttribute(writer, "_ID", m_Id);
            WriteAttribute(writer, "_Description", m_Description);
            WriteAttribute(writer, "_SequenceIdentifier", m_SequenceIdentifier);
            WriteEnumAttribute(writer, "_Type", EnumMappings.E_SignerIdentificationType[(int) m_Type]);
            writer.WriteEndElement();
        }
    }

    public enum E_SignerIdentificationType
    {
        Undefined
        , PersonallyKnown
        , ProvidedIdentification
    }

    static partial class EnumMappings
    {
        public static readonly string[] E_SignerIdentificationType = {
                        ""
                         , "PersonallyKnown"
                         , "ProvidedIdentification"
                         };
    }
}
