// Generated by CodeMonkey on 3/22/2010 6:06:43 PM
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace Mismo.Closing2_6
{
    public class RecordableDocument : AbstractXmlSerializable
    {
        #region Private Member Variables
        private string m_Id = string.Empty;
        private string m_SequenceIdentifier = string.Empty;
        private E_RecordableDocumentType m_Type = E_RecordableDocumentType.Undefined;
        private string m_TypeOtherDescription = string.Empty;
        private AssignFrom m_AssignFrom = null;
        private AssignTo m_AssignTo = null;
        private Default m_Default = null;
        private List<Notary> m_NotaryList = null;
        private PreparedBy m_PreparedBy = null;
        private AssociatedDocument m_AssociatedDocument = null;
        private List<ReturnTo> m_ReturnToList = null;
        private Riders m_Riders = null;
        private SecurityInstrument m_SecurityInstrument = null;
        private List<Trustee> m_TrusteeList = null;
        private List<Witness> m_WitnessList = null;
        #endregion

        #region Public Properties
        public string Id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }
        public string SequenceIdentifier
        {
            get { return m_SequenceIdentifier; }
            set { m_SequenceIdentifier = value; }
        }
        public E_RecordableDocumentType Type
        {
            get { return m_Type; }
            set { m_Type = value; }
        }
        public string TypeOtherDescription
        {
            get { return m_TypeOtherDescription; }
            set { m_TypeOtherDescription = value; }
        }
        public AssignFrom AssignFrom
        {
            get
            {
                if (null == m_AssignFrom) m_AssignFrom = new AssignFrom();
                return m_AssignFrom;
            }
            set { m_AssignFrom = value; }
        }
        public AssignTo AssignTo
        {
            get
            {
                if (null == m_AssignTo) m_AssignTo = new AssignTo();
                return m_AssignTo;
            }
            set { m_AssignTo = value; }
        }
        public Default Default
        {
            get
            {
                if (null == m_Default) m_Default = new Default();
                return m_Default;
            }
            set { m_Default = value; }
        }
        public List<Notary> NotaryList
        {
            get
            {
                if (null == m_NotaryList) m_NotaryList = new List<Notary>();
                return m_NotaryList;
            }
        }
        public PreparedBy PreparedBy
        {
            get
            {
                if (null == m_PreparedBy) m_PreparedBy = new PreparedBy();
                return m_PreparedBy;
            }
            set { m_PreparedBy = value; }
        }
        public AssociatedDocument AssociatedDocument
        {
            get
            {
                if (null == m_AssociatedDocument) m_AssociatedDocument = new AssociatedDocument();
                return m_AssociatedDocument;
            }
            set { m_AssociatedDocument = value; }
        }
        public List<ReturnTo> ReturnToList
        {
            get
            {
                if (null == m_ReturnToList) m_ReturnToList = new List<ReturnTo>();
                return m_ReturnToList;
            }
        }
        public Riders Riders
        {
            get
            {
                if (null == m_Riders) m_Riders = new Riders();
                return m_Riders;
            }
            set { m_Riders = value; }
        }
        public SecurityInstrument SecurityInstrument
        {
            get
            {
                if (null == m_SecurityInstrument) m_SecurityInstrument = new SecurityInstrument();
                return m_SecurityInstrument;
            }
            set { m_SecurityInstrument = value; }
        }
        public List<Trustee> TrusteeList
        {
            get
            {
                if (null == m_TrusteeList) m_TrusteeList = new List<Trustee>();
                return m_TrusteeList;
            }
        }
        public List<Witness> WitnessList
        {
            get
            {
                if (null == m_WitnessList) m_WitnessList = new List<Witness>();
                return m_WitnessList;
            }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "RECORDABLE_DOCUMENT")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_Id = ReadAttribute(reader, "_ID");
            m_SequenceIdentifier = ReadAttribute(reader, "_SequenceIdentifier");
            m_Type = (E_RecordableDocumentType) ReadAttribute(reader, "_Type", EnumMappings.E_RecordableDocumentType);
            m_TypeOtherDescription = ReadAttribute(reader, "_TypeOtherDescription");
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "RECORDABLE_DOCUMENT") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                switch (reader.Name)
                {
                    case "ASSIGN_FROM":
                        ReadElement(reader, AssignFrom);
                        break;
                    case "ASSIGN_TO":
                        ReadElement(reader, AssignTo);
                        break;
                    case "DEFAULT":
                        ReadElement(reader, Default);
                        break;
                    case "NOTARY":
                        ReadElement(reader, NotaryList);
                        break;
                    case "PREPARED_BY":
                        ReadElement(reader, PreparedBy);
                        break;
                    case "_ASSOCIATED_DOCUMENT":
                        ReadElement(reader, AssociatedDocument);
                        break;
                    case "_RETURN_TO":
                        ReadElement(reader, ReturnToList);
                        break;
                    case "RIDERS":
                        ReadElement(reader, Riders);
                        break;
                    case "SECURITY_INSTRUMENT":
                        ReadElement(reader, SecurityInstrument);
                        break;
                    case "TRUSTEE":
                        ReadElement(reader, TrusteeList);
                        break;
                    case "WITNESS":
                        ReadElement(reader, WitnessList);
                        break;
                }
                #endregion
            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("RECORDABLE_DOCUMENT");
            WriteAttribute(writer, "_ID", m_Id);
            WriteAttribute(writer, "_SequenceIdentifier", m_SequenceIdentifier);
            WriteEnumAttribute(writer, "_Type", EnumMappings.E_RecordableDocumentType[(int) m_Type]);
            WriteAttribute(writer, "_TypeOtherDescription", m_TypeOtherDescription);
            WriteElement(writer, m_AssignFrom);
            WriteElement(writer, m_AssignTo);
            WriteElement(writer, m_Default);
            WriteElement(writer, m_NotaryList);
            WriteElement(writer, m_PreparedBy);
            WriteElement(writer, m_AssociatedDocument);
            WriteElement(writer, m_ReturnToList);
            WriteElement(writer, m_Riders);
            WriteElement(writer, m_SecurityInstrument);
            WriteElement(writer, m_TrusteeList);
            WriteElement(writer, m_WitnessList);
            writer.WriteEndElement();
        }
    }

    public enum E_RecordableDocumentType
    {
        Undefined
        , All
        , AssignmentOfMortgage
        , DeedOfTrust
        , Mortgage
        , Other
        , QuitClaimDeed
        , ReleaseOfLien
        , SecurityInstrument
        , SignatureAffidavit
        , WarrantyDeed
    }

    static partial class EnumMappings
    {
        public static readonly string[] E_RecordableDocumentType = {
                        ""
                         , "All"
                         , "AssignmentOfMortgage"
                         , "DeedOfTrust"
                         , "Mortgage"
                         , "Other"
                         , "QuitClaimDeed"
                         , "ReleaseOfLien"
                         , "SecurityInstrument"
                         , "SignatureAffidavit"
                         , "WarrantyDeed"
                         };
    }
}
