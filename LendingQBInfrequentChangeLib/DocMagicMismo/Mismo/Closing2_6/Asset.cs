// Generated by CodeMonkey on 3/22/2010 6:06:39 PM
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace Mismo.Closing2_6
{
    public class Asset : AbstractXmlSerializable
    {
        #region Private Member Variables
        private string m_Id = string.Empty;
        private string m_BorrowerId = string.Empty;
        private string m_AssetDescription = string.Empty;
        private string m_AutomobileMakeDescription = string.Empty;
        private string m_AutomobileModelYear = string.Empty;
        private string m_LifeInsuranceFaceValueAmount = string.Empty;
        private string m_OtherAssetTypeDescription = string.Empty;
        private string m_StockBondMutualFundShareCount = string.Empty;
        private string m_AccountIdentifier = string.Empty;
        private string m_CashOrMarketValueAmount = string.Empty;
        private string m_HolderCity = string.Empty;
        private string m_HolderName = string.Empty;
        private string m_HolderPostalCode = string.Empty;
        private string m_HolderState = string.Empty;
        private string m_HolderStreetAddress = string.Empty;
        private string m_HolderStreetAddress2 = string.Empty;
        private E_AssetType m_Type = E_AssetType.Undefined;
        private E_YNIndicator m_VerifiedIndicator = E_YNIndicator.Undefined;
        private string m_AccountInNameOfDescription = string.Empty;
        #endregion

        #region Public Properties
        public string Id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }
        public string BorrowerId
        {
            get { return m_BorrowerId; }
            set { m_BorrowerId = value; }
        }
        public string AssetDescription
        {
            get { return m_AssetDescription; }
            set { m_AssetDescription = value; }
        }
        public string AutomobileMakeDescription
        {
            get { return m_AutomobileMakeDescription; }
            set { m_AutomobileMakeDescription = value; }
        }
        public string AutomobileModelYear
        {
            get { return m_AutomobileModelYear; }
            set { m_AutomobileModelYear = value; }
        }
        public string LifeInsuranceFaceValueAmount
        {
            get { return m_LifeInsuranceFaceValueAmount; }
            set { m_LifeInsuranceFaceValueAmount = value; }
        }
        public string OtherAssetTypeDescription
        {
            get { return m_OtherAssetTypeDescription; }
            set { m_OtherAssetTypeDescription = value; }
        }
        public string StockBondMutualFundShareCount
        {
            get { return m_StockBondMutualFundShareCount; }
            set { m_StockBondMutualFundShareCount = value; }
        }
        public string AccountIdentifier
        {
            get { return m_AccountIdentifier; }
            set { m_AccountIdentifier = value; }
        }
        public string CashOrMarketValueAmount
        {
            get { return m_CashOrMarketValueAmount; }
            set { m_CashOrMarketValueAmount = value; }
        }
        public string HolderCity
        {
            get { return m_HolderCity; }
            set { m_HolderCity = value; }
        }
        public string HolderName
        {
            get { return m_HolderName; }
            set { m_HolderName = value; }
        }
        public string HolderPostalCode
        {
            get { return m_HolderPostalCode; }
            set { m_HolderPostalCode = value; }
        }
        public string HolderState
        {
            get { return m_HolderState; }
            set { m_HolderState = value; }
        }
        public string HolderStreetAddress
        {
            get { return m_HolderStreetAddress; }
            set { m_HolderStreetAddress = value; }
        }
        public string HolderStreetAddress2
        {
            get { return m_HolderStreetAddress2; }
            set { m_HolderStreetAddress2 = value; }
        }
        public E_AssetType Type
        {
            get { return m_Type; }
            set { m_Type = value; }
        }
        public E_YNIndicator VerifiedIndicator
        {
            get { return m_VerifiedIndicator; }
            set { m_VerifiedIndicator = value; }
        }
        public string AccountInNameOfDescription
        {
            get { return m_AccountInNameOfDescription; }
            set { m_AccountInNameOfDescription = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "ASSET")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_Id = ReadAttribute(reader, "_ID");
            m_BorrowerId = ReadAttribute(reader, "BorrowerID");
            m_AssetDescription = ReadAttribute(reader, "AssetDescription");
            m_AutomobileMakeDescription = ReadAttribute(reader, "AutomobileMakeDescription");
            m_AutomobileModelYear = ReadAttribute(reader, "AutomobileModelYear");
            m_LifeInsuranceFaceValueAmount = ReadAttribute(reader, "LifeInsuranceFaceValueAmount");
            m_OtherAssetTypeDescription = ReadAttribute(reader, "OtherAssetTypeDescription");
            m_StockBondMutualFundShareCount = ReadAttribute(reader, "StockBondMutualFundShareCount");
            m_AccountIdentifier = ReadAttribute(reader, "_AccountIdentifier");
            m_CashOrMarketValueAmount = ReadAttribute(reader, "_CashOrMarketValueAmount");
            m_HolderCity = ReadAttribute(reader, "_HolderCity");
            m_HolderName = ReadAttribute(reader, "_HolderName");
            m_HolderPostalCode = ReadAttribute(reader, "_HolderPostalCode");
            m_HolderState = ReadAttribute(reader, "_HolderState");
            m_HolderStreetAddress = ReadAttribute(reader, "_HolderStreetAddress");
            m_HolderStreetAddress2 = ReadAttribute(reader, "_HolderStreetAddress2");
            m_Type = (E_AssetType) ReadAttribute(reader, "_Type", EnumMappings.E_AssetType);
            m_VerifiedIndicator = (E_YNIndicator) ReadAttribute(reader, "_VerifiedIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator);
            m_AccountInNameOfDescription = ReadAttribute(reader, "_AccountInNameOfDescription");
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "ASSET") return;

            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("ASSET");
            WriteAttribute(writer, "_ID", m_Id);
            WriteAttribute(writer, "BorrowerID", m_BorrowerId);
            WriteAttribute(writer, "AssetDescription", m_AssetDescription);
            WriteAttribute(writer, "AutomobileMakeDescription", m_AutomobileMakeDescription);
            WriteAttribute(writer, "AutomobileModelYear", m_AutomobileModelYear);
            WriteAttribute(writer, "LifeInsuranceFaceValueAmount", m_LifeInsuranceFaceValueAmount);
            WriteAttribute(writer, "OtherAssetTypeDescription", m_OtherAssetTypeDescription);
            WriteAttribute(writer, "StockBondMutualFundShareCount", m_StockBondMutualFundShareCount);
            WriteAttribute(writer, "_AccountIdentifier", m_AccountIdentifier);
            WriteAttribute(writer, "_CashOrMarketValueAmount", m_CashOrMarketValueAmount);
            WriteAttribute(writer, "_HolderCity", m_HolderCity);
            WriteAttribute(writer, "_HolderName", m_HolderName);
            WriteAttribute(writer, "_HolderPostalCode", m_HolderPostalCode);
            WriteAttribute(writer, "_HolderState", m_HolderState);
            WriteAttribute(writer, "_HolderStreetAddress", m_HolderStreetAddress);
            WriteAttribute(writer, "_HolderStreetAddress2", m_HolderStreetAddress2);
            WriteEnumAttribute(writer, "_Type", EnumMappings.E_AssetType[(int) m_Type]);
            WriteEnumAttribute(writer, "_VerifiedIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator[(int) m_VerifiedIndicator]);
            WriteAttribute(writer, "_AccountInNameOfDescription", m_AccountInNameOfDescription);
            writer.WriteEndElement();
        }
    }

    public enum E_AssetType
    {
        Undefined
        , Automobile
        , Bond
        , BridgeLoanNotDeposited
        , CashOnHand
        , CertificateOfDepositTimeDeposit
        , CheckingAccount
        , EarnestMoneyCashDepositTowardPurchase
        , GiftsTotal
        , GiftsNotDeposited
        , LifeInsurance
        , MoneyMarketFund
        , MutualFund
        , NetWorthOfBusinessOwned
        , OtherLiquidAssets
        , OtherNonLiquidAssets
        , PendingNetSaleProceedsFromRealEstateAssets
        , RelocationMoney
        , RetirementFund
        , SaleOtherAssets
        , SavingsAccount
        , SecuredBorrowedFundsNotDeposited
        , Stock
        , TrustAccount
        , BorrowerEstimatedTotalAssets
        , GrantsNotDeposited
        , RealEstateOwned
    }

    static partial class EnumMappings
    {
        public static readonly string[] E_AssetType = {
                        ""
                         , "Automobile"
                         , "Bond"
                         , "BridgeLoanNotDeposited"
                         , "CashOnHand"
                         , "CertificateOfDepositTimeDeposit"
                         , "CheckingAccount"
                         , "EarnestMoneyCashDepositTowardPurchase"
                         , "GiftsTotal"
                         , "GiftsNotDeposited"
                         , "LifeInsurance"
                         , "MoneyMarketFund"
                         , "MutualFund"
                         , "NetWorthOfBusinessOwned"
                         , "OtherLiquidAssets"
                         , "OtherNonLiquidAssets"
                         , "PendingNetSaleProceedsFromRealEstateAssets"
                         , "RelocationMoney"
                         , "RetirementFund"
                         , "SaleOtherAssets"
                         , "SavingsAccount"
                         , "SecuredBorrowedFundsNotDeposited"
                         , "Stock"
                         , "TrustAccount"
                         , "BorrowerEstimatedTotalAssets"
                         , "GrantsNotDeposited"
                         , "RealEstateOwned"
                         };
    }
}
