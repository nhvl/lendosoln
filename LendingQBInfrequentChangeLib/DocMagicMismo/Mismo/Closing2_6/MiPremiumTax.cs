// Generated by CodeMonkey on 3/22/2010 6:06:41 PM
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace Mismo.Closing2_6
{
    public class MiPremiumTax : AbstractXmlSerializable
    {
        #region Private Member Variables
        private string m_Id = string.Empty;
        private string m_CodeAmount = string.Empty;
        private string m_CodePercent = string.Empty;
        private E_MiPremiumTaxCodeType m_CodeType = E_MiPremiumTaxCodeType.Undefined;
        #endregion

        #region Public Properties
        public string Id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }
        public string CodeAmount
        {
            get { return m_CodeAmount; }
            set { m_CodeAmount = value; }
        }
        public string CodePercent
        {
            get { return m_CodePercent; }
            set { m_CodePercent = value; }
        }
        public E_MiPremiumTaxCodeType CodeType
        {
            get { return m_CodeType; }
            set { m_CodeType = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "MI_PREMIUM_TAX")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_Id = ReadAttribute(reader, "_ID");
            m_CodeAmount = ReadAttribute(reader, "_CodeAmount");
            m_CodePercent = ReadAttribute(reader, "_CodePercent");
            m_CodeType = (E_MiPremiumTaxCodeType) ReadAttribute(reader, "_CodeType", EnumMappings.E_MiPremiumTaxCodeType);
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "MI_PREMIUM_TAX") return;

            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("MI_PREMIUM_TAX");
            WriteAttribute(writer, "_ID", m_Id);
            WriteAttribute(writer, "_CodeAmount", m_CodeAmount);
            WriteAttribute(writer, "_CodePercent", m_CodePercent);
            WriteEnumAttribute(writer, "_CodeType", EnumMappings.E_MiPremiumTaxCodeType[(int) m_CodeType]);
            writer.WriteEndElement();
        }
    }

    public enum E_MiPremiumTaxCodeType
    {
        Undefined
        , AllTaxes
        , County
        , Municipal
        , State
    }

    static partial class EnumMappings
    {
        public static readonly string[] E_MiPremiumTaxCodeType = {
                        ""
                         , "AllTaxes"
                         , "County"
                         , "Municipal"
                         , "State"
                         };
    }
}
