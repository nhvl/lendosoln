// Generated by CodeMonkey on 3/22/2010 6:06:43 PM
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace Mismo.Closing2_6
{
    public class PaymentDetails : AbstractXmlSerializable
    {
        #region Private Member Variables
        private string m_Id = string.Empty;
        private string m_FinalPaymentAmount = string.Empty;
        private string m_ScheduledTotalPaymentCount = string.Empty;
        private List<AmortizationSchedule> m_AmortizationScheduleList = null;
        private List<PaymentSchedule> m_PaymentScheduleList = null;
        #endregion

        #region Public Properties
        public string Id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }
        public string FinalPaymentAmount
        {
            get { return m_FinalPaymentAmount; }
            set { m_FinalPaymentAmount = value; }
        }
        public string ScheduledTotalPaymentCount
        {
            get { return m_ScheduledTotalPaymentCount; }
            set { m_ScheduledTotalPaymentCount = value; }
        }
        public List<AmortizationSchedule> AmortizationScheduleList
        {
            get
            {
                if (null == m_AmortizationScheduleList) m_AmortizationScheduleList = new List<AmortizationSchedule>();
                return m_AmortizationScheduleList;
            }
        }
        public List<PaymentSchedule> PaymentScheduleList
        {
            get
            {
                if (null == m_PaymentScheduleList) m_PaymentScheduleList = new List<PaymentSchedule>();
                return m_PaymentScheduleList;
            }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "PAYMENT_DETAILS")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_Id = ReadAttribute(reader, "_ID");
            m_FinalPaymentAmount = ReadAttribute(reader, "FinalPaymentAmount");
            m_ScheduledTotalPaymentCount = ReadAttribute(reader, "ScheduledTotalPaymentCount");
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "PAYMENT_DETAILS") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                switch (reader.Name)
                {
                    case "AMORTIZATION_SCHEDULE":
                        ReadElement(reader, AmortizationScheduleList);
                        break;
                    case "PAYMENT_SCHEDULE":
                        ReadElement(reader, PaymentScheduleList);
                        break;
                }
                #endregion
            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("PAYMENT_DETAILS");
            WriteAttribute(writer, "_ID", m_Id);
            WriteAttribute(writer, "FinalPaymentAmount", m_FinalPaymentAmount);
            WriteAttribute(writer, "ScheduledTotalPaymentCount", m_ScheduledTotalPaymentCount);
            WriteElement(writer, m_AmortizationScheduleList);
            WriteElement(writer, m_PaymentScheduleList);
            writer.WriteEndElement();
        }
    }
}
