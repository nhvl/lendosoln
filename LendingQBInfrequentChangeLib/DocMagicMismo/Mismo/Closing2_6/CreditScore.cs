// Generated by CodeMonkey on 3/22/2010 6:06:42 PM
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace Mismo.Closing2_6
{
    public class CreditScore : AbstractXmlSerializable
    {
        #region Private Member Variables
        private string m_CreditScoreId = string.Empty;
        private string m_CreditReportIdentifier = string.Empty;
        private E_CreditScoreCreditRepositorySourceType m_CreditRepositorySourceType = E_CreditScoreCreditRepositorySourceType.Undefined;
        private string m_CreditRepositorySourceTypeOtherDescription = string.Empty;
        private string m_Date = string.Empty;
        private E_CreditScoreExclusionReasonType m_ExclusionReasonType = E_CreditScoreExclusionReasonType.Undefined;
        private E_YNIndicator m_FactaInquiriesIndicator = E_YNIndicator.Undefined;
        private E_CreditScoreModelNameType m_ModelNameType = E_CreditScoreModelNameType.Undefined;
        private string m_ModelNameTypeOtherDescription = string.Empty;
        private string m_Value = string.Empty;
        private string m_MaximumValue = string.Empty;
        private string m_MinimumValue = string.Empty;
        private List<Factor> m_FactorList = null;
        #endregion

        #region Public Properties
        public string CreditScoreId
        {
            get { return m_CreditScoreId; }
            set { m_CreditScoreId = value; }
        }
        public string CreditReportIdentifier
        {
            get { return m_CreditReportIdentifier; }
            set { m_CreditReportIdentifier = value; }
        }
        public E_CreditScoreCreditRepositorySourceType CreditRepositorySourceType
        {
            get { return m_CreditRepositorySourceType; }
            set { m_CreditRepositorySourceType = value; }
        }
        public string CreditRepositorySourceTypeOtherDescription
        {
            get { return m_CreditRepositorySourceTypeOtherDescription; }
            set { m_CreditRepositorySourceTypeOtherDescription = value; }
        }
        public string Date
        {
            get { return m_Date; }
            set { m_Date = value; }
        }
        public E_CreditScoreExclusionReasonType ExclusionReasonType
        {
            get { return m_ExclusionReasonType; }
            set { m_ExclusionReasonType = value; }
        }
        public E_YNIndicator FactaInquiriesIndicator
        {
            get { return m_FactaInquiriesIndicator; }
            set { m_FactaInquiriesIndicator = value; }
        }
        public E_CreditScoreModelNameType ModelNameType
        {
            get { return m_ModelNameType; }
            set { m_ModelNameType = value; }
        }
        public string ModelNameTypeOtherDescription
        {
            get { return m_ModelNameTypeOtherDescription; }
            set { m_ModelNameTypeOtherDescription = value; }
        }
        public string Value
        {
            get { return m_Value; }
            set { m_Value = value; }
        }
        public string MaximumValue
        {
            get { return m_MaximumValue; }
            set { m_MaximumValue = value; }
        }
        public string MinimumValue
        {
            get { return m_MinimumValue; }
            set { m_MinimumValue = value; }
        }
        public List<Factor> FactorList
        {
            get
            {
                if (null == m_FactorList) m_FactorList = new List<Factor>();
                return m_FactorList;
            }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "CREDIT_SCORE")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_CreditScoreId = ReadAttribute(reader, "CreditScoreID");
            m_CreditReportIdentifier = ReadAttribute(reader, "CreditReportIdentifier");
            m_CreditRepositorySourceType = (E_CreditScoreCreditRepositorySourceType) ReadAttribute(reader, "CreditRepositorySourceType", EnumMappings.E_CreditScoreCreditRepositorySourceType);
            m_CreditRepositorySourceTypeOtherDescription = ReadAttribute(reader, "CreditRepositorySourceTypeOtherDescription");
            m_Date = ReadAttribute(reader, "_Date");
            m_ExclusionReasonType = (E_CreditScoreExclusionReasonType) ReadAttribute(reader, "_ExclusionReasonType", EnumMappings.E_CreditScoreExclusionReasonType);
            m_FactaInquiriesIndicator = (E_YNIndicator) ReadAttribute(reader, "_FACTAInquiriesIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator);
            m_ModelNameType = (E_CreditScoreModelNameType) ReadAttribute(reader, "_ModelNameType", EnumMappings.E_CreditScoreModelNameType);
            m_ModelNameTypeOtherDescription = ReadAttribute(reader, "_ModelNameTypeOtherDescription");
            m_Value = ReadAttribute(reader, "_Value");
            m_MaximumValue = ReadAttribute(reader, "_MaximumValue");
            m_MinimumValue = ReadAttribute(reader, "_MinimumValue");
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "CREDIT_SCORE") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                switch (reader.Name)
                {
                    case "_FACTOR":
                        ReadElement(reader, FactorList);
                        break;
                }
                #endregion
            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("CREDIT_SCORE");
            WriteAttribute(writer, "CreditScoreID", m_CreditScoreId);
            WriteAttribute(writer, "CreditReportIdentifier", m_CreditReportIdentifier);
            WriteEnumAttribute(writer, "CreditRepositorySourceType", EnumMappings.E_CreditScoreCreditRepositorySourceType[(int) m_CreditRepositorySourceType]);
            WriteAttribute(writer, "CreditRepositorySourceTypeOtherDescription", m_CreditRepositorySourceTypeOtherDescription);
            WriteAttribute(writer, "_Date", m_Date);
            WriteEnumAttribute(writer, "_ExclusionReasonType", EnumMappings.E_CreditScoreExclusionReasonType[(int) m_ExclusionReasonType]);
            WriteEnumAttribute(writer, "_FACTAInquiriesIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator[(int) m_FactaInquiriesIndicator]);
            WriteEnumAttribute(writer, "_ModelNameType", EnumMappings.E_CreditScoreModelNameType[(int) m_ModelNameType]);
            WriteAttribute(writer, "_ModelNameTypeOtherDescription", m_ModelNameTypeOtherDescription);
            WriteAttribute(writer, "_Value", m_Value);
            WriteAttribute(writer, "_MaximumValue", m_MaximumValue);
            WriteAttribute(writer, "_MinimumValue", m_MinimumValue);
            WriteElement(writer, m_FactorList);
            writer.WriteEndElement();
        }
    }

    public enum E_CreditScoreCreditRepositorySourceType
    {
        Undefined
        , Equifax
        , Experian
        , MergedData
        , Other
        , TransUnion
    }

    public enum E_CreditScoreExclusionReasonType
    {
        Undefined
        , InvalidScoreRequest
        , NotScoredCreditDataNotAvailable
        , NotScoredFileIsUnderReview
        , NotScoredFileTooLong
        , NotScoredInsufficientCredit
        , NotScoredNoQualifyingAccount
        , NotScoredNoRecentAccountInformation
        , NotScoredSubjectDeceased
        , ScoringNotAvailable
        , UnauthorizedScoreRequest
        , NotScoredRequirementsNotMet
        , NotScoredFileCannotBeScored
    }

    public enum E_CreditScoreModelNameType
    {
        Undefined
        , EquifaxBeacon
        , EquifaxBeaconAuto
        , EquifaxBeaconBankcard
        , EquifaxBeaconInstallment
        , EquifaxBeaconPersonalFinance
        , _EquifaxBeacon5_0
        , _EquifaxBeacon5_0Auto
        , _EquifaxBeacon5_0Bankcard
        , _EquifaxBeacon5_0Installment
        , _EquifaxBeacon5_0PersonalFinance
        , EquifaxDAS
        , EquifaxEnhancedBeacon
        , EquifaxEnhancedDAS
        , EquifaxMarketMax
        , EquifaxMortgageScore
        , EquifaxPinnacle
        , _EquifaxPinnacle2_0
        , ExperianFairIsaac
        , ExperianFairIsaacAdvanced
        , _ExperianFairIsaacAdvanced2_0
        , ExperianFairIsaacAuto
        , ExperianFairIsaacBankcard
        , ExperianFairIsaacInstallment
        , ExperianFairIsaacPersonalFinance
        , ExperianMDSBankruptcyII
        , ExperianNewNationalEquivalency
        , ExperianNewNationalRisk
        , ExperianOldNationalRisk
        , ExperianScorexPLUS
        , FICOExpansionScore
        , FICORiskScoreClassic04
        , FICORiskScoreClassic98
        , FICORiskScoreClassicAuto98
        , FICORiskScoreClassicBankcard98
        , FICORiskScoreClassicInstallmentLoan98
        , FICORiskScoreClassicPersonalFinance98
        , FICORiskScoreNextGen00
        , FICORiskScoreNextGen03
        , Other
        , TransUnionDelphi
        , TransUnionEmpirica
        , TransUnionEmpiricaAuto
        , TransUnionEmpiricaBankcard
        , TransUnionEmpiricaInstallment
        , TransUnionEmpiricaPersonalFinance
        , TransUnionNewDelphi
        , TransUnionPrecision
        , TransUnionPrecision03
        , EquifaxBankruptcyNavigatorIndex02781
        , EquifaxBankruptcyNavigatorIndex02782
        , EquifaxBankruptcyNavigatorIndex02783
        , EquifaxBankruptcyNavigatorIndex02784
        , EquifaxVantageScore
        , ExperianVantageScore
        , TransUnionVantageScore
        , ExperianFICOClassicV3
    }

    static partial class EnumMappings
    {
        public static readonly string[] E_CreditScoreCreditRepositorySourceType = {
                        ""
                         , "Equifax"
                         , "Experian"
                         , "MergedData"
                         , "Other"
                         , "TransUnion"
                         };
        public static readonly string[] E_CreditScoreExclusionReasonType = {
                        ""
                         , "InvalidScoreRequest"
                         , "NotScoredCreditDataNotAvailable"
                         , "NotScoredFileIsUnderReview"
                         , "NotScoredFileTooLong"
                         , "NotScoredInsufficientCredit"
                         , "NotScoredNoQualifyingAccount"
                         , "NotScoredNoRecentAccountInformation"
                         , "NotScoredSubjectDeceased"
                         , "ScoringNotAvailable"
                         , "UnauthorizedScoreRequest"
                         , "NotScoredRequirementsNotMet"
                         , "NotScoredFileCannotBeScored"
                         };
        public static readonly string[] E_CreditScoreModelNameType = {
                        ""
                         , "EquifaxBeacon"
                         , "EquifaxBeaconAuto"
                         , "EquifaxBeaconBankcard"
                         , "EquifaxBeaconInstallment"
                         , "EquifaxBeaconPersonalFinance"
                         , "EquifaxBeacon5.0"
                         , "EquifaxBeacon5.0Auto"
                         , "EquifaxBeacon5.0Bankcard"
                         , "EquifaxBeacon5.0Installment"
                         , "EquifaxBeacon5.0PersonalFinance"
                         , "EquifaxDAS"
                         , "EquifaxEnhancedBeacon"
                         , "EquifaxEnhancedDAS"
                         , "EquifaxMarketMax"
                         , "EquifaxMortgageScore"
                         , "EquifaxPinnacle"
                         , "EquifaxPinnacle2.0"
                         , "ExperianFairIsaac"
                         , "ExperianFairIsaacAdvanced"
                         , "ExperianFairIsaacAdvanced2.0"
                         , "ExperianFairIsaacAuto"
                         , "ExperianFairIsaacBankcard"
                         , "ExperianFairIsaacInstallment"
                         , "ExperianFairIsaacPersonalFinance"
                         , "ExperianMDSBankruptcyII"
                         , "ExperianNewNationalEquivalency"
                         , "ExperianNewNationalRisk"
                         , "ExperianOldNationalRisk"
                         , "ExperianScorexPLUS"
                         , "FICOExpansionScore"
                         , "FICORiskScoreClassic04"
                         , "FICORiskScoreClassic98"
                         , "FICORiskScoreClassicAuto98"
                         , "FICORiskScoreClassicBankcard98"
                         , "FICORiskScoreClassicInstallmentLoan98"
                         , "FICORiskScoreClassicPersonalFinance98"
                         , "FICORiskScoreNextGen00"
                         , "FICORiskScoreNextGen03"
                         , "Other"
                         , "TransUnionDelphi"
                         , "TransUnionEmpirica"
                         , "TransUnionEmpiricaAuto"
                         , "TransUnionEmpiricaBankcard"
                         , "TransUnionEmpiricaInstallment"
                         , "TransUnionEmpiricaPersonalFinance"
                         , "TransUnionNewDelphi"
                         , "TransUnionPrecision"
                         , "TransUnionPrecision03"
                         , "EquifaxBankruptcyNavigatorIndex02781"
                         , "EquifaxBankruptcyNavigatorIndex02782"
                         , "EquifaxBankruptcyNavigatorIndex02783"
                         , "EquifaxBankruptcyNavigatorIndex02784"
                         , "EquifaxVantageScore"
                         , "ExperianVantageScore"
                         , "TransUnionVantageScore"
                         , "ExperianFICOClassicV3"
                         };
    }
}
