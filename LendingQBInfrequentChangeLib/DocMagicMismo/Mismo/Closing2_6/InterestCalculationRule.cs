// Generated by CodeMonkey on 3/22/2010 6:06:40 PM
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace Mismo.Closing2_6
{
    public class InterestCalculationRule : AbstractXmlSerializable
    {
        #region Private Member Variables
        private string m_Id = string.Empty;
        private E_InterestCalculationRuleInterestCalculationBasisDaysInPeriodType m_InterestCalculationBasisDaysInPeriodType = E_InterestCalculationRuleInterestCalculationBasisDaysInPeriodType.Undefined;
        private string m_InterestCalculationBasisDaysInPeriodTypeOtherDescription = string.Empty;
        private E_InterestCalculationRuleInterestCalculationBasisDaysInYearCount m_InterestCalculationBasisDaysInYearCount = E_InterestCalculationRuleInterestCalculationBasisDaysInYearCount.Undefined;
        private E_InterestCalculationRuleInterestCalculationBasisType m_InterestCalculationBasisType = E_InterestCalculationRuleInterestCalculationBasisType.Undefined;
        private string m_InterestCalculationBasisTypeOtherDescription = string.Empty;
        private string m_InterestCalculationEffectiveDate = string.Empty;
        private string m_InterestCalculationEffectiveMonthsCount = string.Empty;
        private string m_InterestCalculationExpirationDate = string.Empty;
        private E_YNIndicator m_InterestCalculationPeriodAdjustmentIndicator = E_YNIndicator.Undefined;
        private E_InterestCalculationRuleInterestCalculationPeriodType m_InterestCalculationPeriodType = E_InterestCalculationRuleInterestCalculationPeriodType.Undefined;
        private E_InterestCalculationRuleInterestCalculationPurposeType m_InterestCalculationPurposeType = E_InterestCalculationRuleInterestCalculationPurposeType.Undefined;
        private string m_InterestCalculationPurposeTypeOtherDescription = string.Empty;
        private E_InterestCalculationRuleInterestCalculationType m_InterestCalculationType = E_InterestCalculationRuleInterestCalculationType.Undefined;
        private string m_InterestCalculationTypeOtherDescription = string.Empty;
        private E_YNIndicator m_InterestInAdvanceIndicator = E_YNIndicator.Undefined;
        private string m_LoanInterestAccrualStartDate = string.Empty;
        #endregion

        #region Public Properties
        public string Id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }
        public E_InterestCalculationRuleInterestCalculationBasisDaysInPeriodType InterestCalculationBasisDaysInPeriodType
        {
            get { return m_InterestCalculationBasisDaysInPeriodType; }
            set { m_InterestCalculationBasisDaysInPeriodType = value; }
        }
        public string InterestCalculationBasisDaysInPeriodTypeOtherDescription
        {
            get { return m_InterestCalculationBasisDaysInPeriodTypeOtherDescription; }
            set { m_InterestCalculationBasisDaysInPeriodTypeOtherDescription = value; }
        }
        public E_InterestCalculationRuleInterestCalculationBasisDaysInYearCount InterestCalculationBasisDaysInYearCount
        {
            get { return m_InterestCalculationBasisDaysInYearCount; }
            set { m_InterestCalculationBasisDaysInYearCount = value; }
        }
        public E_InterestCalculationRuleInterestCalculationBasisType InterestCalculationBasisType
        {
            get { return m_InterestCalculationBasisType; }
            set { m_InterestCalculationBasisType = value; }
        }
        public string InterestCalculationBasisTypeOtherDescription
        {
            get { return m_InterestCalculationBasisTypeOtherDescription; }
            set { m_InterestCalculationBasisTypeOtherDescription = value; }
        }
        public string InterestCalculationEffectiveDate
        {
            get { return m_InterestCalculationEffectiveDate; }
            set { m_InterestCalculationEffectiveDate = value; }
        }
        public string InterestCalculationEffectiveMonthsCount
        {
            get { return m_InterestCalculationEffectiveMonthsCount; }
            set { m_InterestCalculationEffectiveMonthsCount = value; }
        }
        public string InterestCalculationExpirationDate
        {
            get { return m_InterestCalculationExpirationDate; }
            set { m_InterestCalculationExpirationDate = value; }
        }
        public E_YNIndicator InterestCalculationPeriodAdjustmentIndicator
        {
            get { return m_InterestCalculationPeriodAdjustmentIndicator; }
            set { m_InterestCalculationPeriodAdjustmentIndicator = value; }
        }
        public E_InterestCalculationRuleInterestCalculationPeriodType InterestCalculationPeriodType
        {
            get { return m_InterestCalculationPeriodType; }
            set { m_InterestCalculationPeriodType = value; }
        }
        public E_InterestCalculationRuleInterestCalculationPurposeType InterestCalculationPurposeType
        {
            get { return m_InterestCalculationPurposeType; }
            set { m_InterestCalculationPurposeType = value; }
        }
        public string InterestCalculationPurposeTypeOtherDescription
        {
            get { return m_InterestCalculationPurposeTypeOtherDescription; }
            set { m_InterestCalculationPurposeTypeOtherDescription = value; }
        }
        public E_InterestCalculationRuleInterestCalculationType InterestCalculationType
        {
            get { return m_InterestCalculationType; }
            set { m_InterestCalculationType = value; }
        }
        public string InterestCalculationTypeOtherDescription
        {
            get { return m_InterestCalculationTypeOtherDescription; }
            set { m_InterestCalculationTypeOtherDescription = value; }
        }
        public E_YNIndicator InterestInAdvanceIndicator
        {
            get { return m_InterestInAdvanceIndicator; }
            set { m_InterestInAdvanceIndicator = value; }
        }
        public string LoanInterestAccrualStartDate
        {
            get { return m_LoanInterestAccrualStartDate; }
            set { m_LoanInterestAccrualStartDate = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "INTEREST_CALCULATION_RULE")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_Id = ReadAttribute(reader, "_ID");
            m_InterestCalculationBasisDaysInPeriodType = (E_InterestCalculationRuleInterestCalculationBasisDaysInPeriodType) ReadAttribute(reader, "InterestCalculationBasisDaysInPeriodType", EnumMappings.E_InterestCalculationRuleInterestCalculationBasisDaysInPeriodType);
            m_InterestCalculationBasisDaysInPeriodTypeOtherDescription = ReadAttribute(reader, "InterestCalculationBasisDaysInPeriodTypeOtherDescription");
            m_InterestCalculationBasisDaysInYearCount = (E_InterestCalculationRuleInterestCalculationBasisDaysInYearCount) ReadAttribute(reader, "InterestCalculationBasisDaysInYearCount", EnumMappings.E_InterestCalculationRuleInterestCalculationBasisDaysInYearCount);
            m_InterestCalculationBasisType = (E_InterestCalculationRuleInterestCalculationBasisType) ReadAttribute(reader, "InterestCalculationBasisType", EnumMappings.E_InterestCalculationRuleInterestCalculationBasisType);
            m_InterestCalculationBasisTypeOtherDescription = ReadAttribute(reader, "InterestCalculationBasisTypeOtherDescription");
            m_InterestCalculationEffectiveDate = ReadAttribute(reader, "InterestCalculationEffectiveDate");
            m_InterestCalculationEffectiveMonthsCount = ReadAttribute(reader, "InterestCalculationEffectiveMonthsCount");
            m_InterestCalculationExpirationDate = ReadAttribute(reader, "InterestCalculationExpirationDate");
            m_InterestCalculationPeriodAdjustmentIndicator = (E_YNIndicator) ReadAttribute(reader, "InterestCalculationPeriodAdjustmentIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator);
            m_InterestCalculationPeriodType = (E_InterestCalculationRuleInterestCalculationPeriodType) ReadAttribute(reader, "InterestCalculationPeriodType", EnumMappings.E_InterestCalculationRuleInterestCalculationPeriodType);
            m_InterestCalculationPurposeType = (E_InterestCalculationRuleInterestCalculationPurposeType) ReadAttribute(reader, "InterestCalculationPurposeType", EnumMappings.E_InterestCalculationRuleInterestCalculationPurposeType);
            m_InterestCalculationPurposeTypeOtherDescription = ReadAttribute(reader, "InterestCalculationPurposeTypeOtherDescription");
            m_InterestCalculationType = (E_InterestCalculationRuleInterestCalculationType) ReadAttribute(reader, "InterestCalculationType", EnumMappings.E_InterestCalculationRuleInterestCalculationType);
            m_InterestCalculationTypeOtherDescription = ReadAttribute(reader, "InterestCalculationTypeOtherDescription");
            m_InterestInAdvanceIndicator = (E_YNIndicator) ReadAttribute(reader, "InterestInAdvanceIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator);
            m_LoanInterestAccrualStartDate = ReadAttribute(reader, "LoanInterestAccrualStartDate");
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "INTEREST_CALCULATION_RULE") return;

            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("INTEREST_CALCULATION_RULE");
            WriteAttribute(writer, "_ID", m_Id);
            WriteEnumAttribute(writer, "InterestCalculationBasisDaysInPeriodType", EnumMappings.E_InterestCalculationRuleInterestCalculationBasisDaysInPeriodType[(int) m_InterestCalculationBasisDaysInPeriodType]);
            WriteAttribute(writer, "InterestCalculationBasisDaysInPeriodTypeOtherDescription", m_InterestCalculationBasisDaysInPeriodTypeOtherDescription);
            WriteEnumAttribute(writer, "InterestCalculationBasisDaysInYearCount", EnumMappings.E_InterestCalculationRuleInterestCalculationBasisDaysInYearCount[(int) m_InterestCalculationBasisDaysInYearCount]);
            WriteEnumAttribute(writer, "InterestCalculationBasisType", EnumMappings.E_InterestCalculationRuleInterestCalculationBasisType[(int) m_InterestCalculationBasisType]);
            WriteAttribute(writer, "InterestCalculationBasisTypeOtherDescription", m_InterestCalculationBasisTypeOtherDescription);
            WriteAttribute(writer, "InterestCalculationEffectiveDate", m_InterestCalculationEffectiveDate);
            WriteAttribute(writer, "InterestCalculationEffectiveMonthsCount", m_InterestCalculationEffectiveMonthsCount);
            WriteAttribute(writer, "InterestCalculationExpirationDate", m_InterestCalculationExpirationDate);
            WriteEnumAttribute(writer, "InterestCalculationPeriodAdjustmentIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator[(int) m_InterestCalculationPeriodAdjustmentIndicator]);
            WriteEnumAttribute(writer, "InterestCalculationPeriodType", EnumMappings.E_InterestCalculationRuleInterestCalculationPeriodType[(int) m_InterestCalculationPeriodType]);
            WriteEnumAttribute(writer, "InterestCalculationPurposeType", EnumMappings.E_InterestCalculationRuleInterestCalculationPurposeType[(int) m_InterestCalculationPurposeType]);
            WriteAttribute(writer, "InterestCalculationPurposeTypeOtherDescription", m_InterestCalculationPurposeTypeOtherDescription);
            WriteEnumAttribute(writer, "InterestCalculationType", EnumMappings.E_InterestCalculationRuleInterestCalculationType[(int) m_InterestCalculationType]);
            WriteAttribute(writer, "InterestCalculationTypeOtherDescription", m_InterestCalculationTypeOtherDescription);
            WriteEnumAttribute(writer, "InterestInAdvanceIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator[(int) m_InterestInAdvanceIndicator]);
            WriteAttribute(writer, "LoanInterestAccrualStartDate", m_LoanInterestAccrualStartDate);
            writer.WriteEndElement();
        }
    }

    public enum E_InterestCalculationRuleInterestCalculationBasisDaysInPeriodType
    {
        Undefined
        , _30Days
        , DaysBetweenPayments
        , DaysInCalendarMonth
        , Other
    }

    public enum E_InterestCalculationRuleInterestCalculationBasisDaysInYearCount
    {
        Undefined
        , _360
        , _365
        , _365_25
        , _365Or366
    }

    public enum E_InterestCalculationRuleInterestCalculationBasisType
    {
        Undefined
        , AverageBalance
        , EndOfPeriod
        , MaximumBalance
        , Other
    }

    public enum E_InterestCalculationRuleInterestCalculationPeriodType
    {
        Undefined
        , Biweekly
        , Day
        , Month
        , Quarter
        , Semimonthly
        , Week
        , Year
    }

    public enum E_InterestCalculationRuleInterestCalculationPurposeType
    {
        Undefined
        , Draw
        , Other
        , Payoff
        , ServicerAdvance
        , Standard
    }

    public enum E_InterestCalculationRuleInterestCalculationType
    {
        Undefined
        , Compound
        , Other
        , RuleOf78s
        , Simple
    }

    static partial class EnumMappings
    {
        public static readonly string[] E_InterestCalculationRuleInterestCalculationBasisDaysInPeriodType = {
                        ""
                         , "30Days"
                         , "DaysBetweenPayments"
                         , "DaysInCalendarMonth"
                         , "Other"
                         };
        public static readonly string[] E_InterestCalculationRuleInterestCalculationBasisDaysInYearCount = {
                        ""
                         , "360"
                         , "365"
                         , "365.25"
                         , "365Or366"
                         };
        public static readonly string[] E_InterestCalculationRuleInterestCalculationBasisType = {
                        ""
                         , "AverageBalance"
                         , "EndOfPeriod"
                         , "MaximumBalance"
                         , "Other"
                         };
        public static readonly string[] E_InterestCalculationRuleInterestCalculationPeriodType = {
                        ""
                         , "Biweekly"
                         , "Day"
                         , "Month"
                         , "Quarter"
                         , "Semimonthly"
                         , "Week"
                         , "Year"
                         };
        public static readonly string[] E_InterestCalculationRuleInterestCalculationPurposeType = {
                        ""
                         , "Draw"
                         , "Other"
                         , "Payoff"
                         , "ServicerAdvance"
                         , "Standard"
                         };
        public static readonly string[] E_InterestCalculationRuleInterestCalculationType = {
                        ""
                         , "Compound"
                         , "Other"
                         , "RuleOf78s"
                         , "Simple"
                         };
    }
}
