// Generated by CodeMonkey on 3/22/2010 6:06:42 PM
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace Mismo.Closing2_6
{
    public class EscrowAccountActivity : AbstractXmlSerializable
    {
        #region Private Member Variables
        private string m_Id = string.Empty;
        private string m_CurrentBalanceAmount = string.Empty;
        private string m_DisbursementMonth = string.Empty;
        private string m_DisbursementSequenceIdentifier = string.Empty;
        private string m_DisbursementYear = string.Empty;
        private E_EscrowAccountActivityPaymentDescriptionType m_PaymentDescriptionType = E_EscrowAccountActivityPaymentDescriptionType.Undefined;
        private string m_PaymentDescriptionTypeOtherDescription = string.Empty;
        private string m_PaymentFromEscrowAccountAmount = string.Empty;
        private string m_PaymentToEscrowAccountAmount = string.Empty;
        #endregion

        #region Public Properties
        public string Id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }
        public string CurrentBalanceAmount
        {
            get { return m_CurrentBalanceAmount; }
            set { m_CurrentBalanceAmount = value; }
        }
        public string DisbursementMonth
        {
            get { return m_DisbursementMonth; }
            set { m_DisbursementMonth = value; }
        }
        public string DisbursementSequenceIdentifier
        {
            get { return m_DisbursementSequenceIdentifier; }
            set { m_DisbursementSequenceIdentifier = value; }
        }
        public string DisbursementYear
        {
            get { return m_DisbursementYear; }
            set { m_DisbursementYear = value; }
        }
        public E_EscrowAccountActivityPaymentDescriptionType PaymentDescriptionType
        {
            get { return m_PaymentDescriptionType; }
            set { m_PaymentDescriptionType = value; }
        }
        public string PaymentDescriptionTypeOtherDescription
        {
            get { return m_PaymentDescriptionTypeOtherDescription; }
            set { m_PaymentDescriptionTypeOtherDescription = value; }
        }
        public string PaymentFromEscrowAccountAmount
        {
            get { return m_PaymentFromEscrowAccountAmount; }
            set { m_PaymentFromEscrowAccountAmount = value; }
        }
        public string PaymentToEscrowAccountAmount
        {
            get { return m_PaymentToEscrowAccountAmount; }
            set { m_PaymentToEscrowAccountAmount = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "ESCROW_ACCOUNT_ACTIVITY")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_Id = ReadAttribute(reader, "_ID");
            m_CurrentBalanceAmount = ReadAttribute(reader, "_CurrentBalanceAmount");
            m_DisbursementMonth = ReadAttribute(reader, "_DisbursementMonth");
            m_DisbursementSequenceIdentifier = ReadAttribute(reader, "_DisbursementSequenceIdentifier");
            m_DisbursementYear = ReadAttribute(reader, "_DisbursementYear");
            m_PaymentDescriptionType = (E_EscrowAccountActivityPaymentDescriptionType) ReadAttribute(reader, "_PaymentDescriptionType", EnumMappings.E_EscrowAccountActivityPaymentDescriptionType);
            m_PaymentDescriptionTypeOtherDescription = ReadAttribute(reader, "_PaymentDescriptionTypeOtherDescription");
            m_PaymentFromEscrowAccountAmount = ReadAttribute(reader, "_PaymentFromEscrowAccountAmount");
            m_PaymentToEscrowAccountAmount = ReadAttribute(reader, "_PaymentToEscrowAccountAmount");
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "ESCROW_ACCOUNT_ACTIVITY") return;

            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("ESCROW_ACCOUNT_ACTIVITY");
            WriteAttribute(writer, "_ID", m_Id);
            WriteAttribute(writer, "_CurrentBalanceAmount", m_CurrentBalanceAmount);
            WriteAttribute(writer, "_DisbursementMonth", m_DisbursementMonth);
            WriteAttribute(writer, "_DisbursementSequenceIdentifier", m_DisbursementSequenceIdentifier);
            WriteAttribute(writer, "_DisbursementYear", m_DisbursementYear);
            WriteEnumAttribute(writer, "_PaymentDescriptionType", EnumMappings.E_EscrowAccountActivityPaymentDescriptionType[(int) m_PaymentDescriptionType]);
            WriteAttribute(writer, "_PaymentDescriptionTypeOtherDescription", m_PaymentDescriptionTypeOtherDescription);
            WriteAttribute(writer, "_PaymentFromEscrowAccountAmount", m_PaymentFromEscrowAccountAmount);
            WriteAttribute(writer, "_PaymentToEscrowAccountAmount", m_PaymentToEscrowAccountAmount);
            writer.WriteEndElement();
        }
    }

    public enum E_EscrowAccountActivityPaymentDescriptionType
    {
        Undefined
        , Assessment
        , CityPropertyTax
        , CountyPropertyTax
        , EarthquakeInsurance
        , FloodInsurance
        , HazardInsurance
        , Other
        , SchoolPropertyTax
        , TownPropertyTax
        , VillagePropertyTax
        , Windstorm
        , MortgageInsurance
    }

    static partial class EnumMappings
    {
        public static readonly string[] E_EscrowAccountActivityPaymentDescriptionType = {
                        ""
                         , "Assessment"
                         , "CityPropertyTax"
                         , "CountyPropertyTax"
                         , "EarthquakeInsurance"
                         , "FloodInsurance"
                         , "HazardInsurance"
                         , "Other"
                         , "SchoolPropertyTax"
                         , "TownPropertyTax"
                         , "VillagePropertyTax"
                         , "Windstorm"
                         , "MortgageInsurance"
                         };
    }
}
