// Generated by CodeMonkey on 3/22/2010 6:06:42 PM
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace Mismo.Closing2_6
{
    public class Summary : AbstractXmlSerializable
    {
        #region Private Member Variables
        private string m_Id = string.Empty;
        private string m_Amount = string.Empty;
        private E_SummaryAmountType m_AmountType = E_SummaryAmountType.Undefined;
        private string m_AmountTypeOtherDescription = string.Empty;
        #endregion

        #region Public Properties
        public string Id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }
        public string Amount
        {
            get { return m_Amount; }
            set { m_Amount = value; }
        }
        public E_SummaryAmountType AmountType
        {
            get { return m_AmountType; }
            set { m_AmountType = value; }
        }
        public string AmountTypeOtherDescription
        {
            get { return m_AmountTypeOtherDescription; }
            set { m_AmountTypeOtherDescription = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "SUMMARY")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_Id = ReadAttribute(reader, "_ID");
            m_Amount = ReadAttribute(reader, "_Amount");
            m_AmountType = (E_SummaryAmountType) ReadAttribute(reader, "_AmountType", EnumMappings.E_SummaryAmountType);
            m_AmountTypeOtherDescription = ReadAttribute(reader, "_AmountTypeOtherDescription");
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "SUMMARY") return;

            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("SUMMARY");
            WriteAttribute(writer, "_ID", m_Id);
            WriteAttribute(writer, "_Amount", m_Amount);
            WriteEnumAttribute(writer, "_AmountType", EnumMappings.E_SummaryAmountType[(int) m_AmountType]);
            WriteAttribute(writer, "_AmountTypeOtherDescription", m_AmountTypeOtherDescription);
            writer.WriteEndElement();
        }
    }

    public enum E_SummaryAmountType
    {
        Undefined
        , TotalMonthlyIncomeNotIncludingNetRentalIncome
        , SubtotalLiabilitiesPaidByClosingNotIncludingSubjectPropertyLiensBalance
        , TotalPresentHousingExpense
        , TotalLiabilitesBalance
        , SubtotalLiabilitesMonthlyPayment
        , SubtotalOmittedLiabilitesBalance
        , SubtotalOmittedLiabilitiesMonthlyPayment
        , SubtotalLiabilitiesPaidByClosingNotIncludingSubjectPropertyLiensMonthlyPayment
        , SubtotalResubordinatedLiabilitesMonthlyPaymentForSubjectProperty
        , SubtotalSubjectPropertyLiensPaidByClosingBalance
        , SubtotalSubjectPropertyLiensPaidByClosingMonthlyPayment
        , SubtotalLiabilitiesForRentalPropertyBalance
        , SubtotalLiabilitiesForRentalPropertyMonthlyPayment
        , SubtotalResubordinatedLiabilitiesBalanceForSubjectProperty
        , SubtotalLiquidAssetsNotIncludingGift
        , SubtotalNonLiquidAssets
        , TotalLiabilitiesBalance
        , SubtotalLiabilitiesMonthlyPayment
        , SubtotalOmittedLiabilitiesBalance
        , SubtotalResubordinatedLiabilitiesMonthlyPaymentForSubjectProperty
        , UndrawnHELOC
        , Other
    }

    static partial class EnumMappings
    {
        public static readonly string[] E_SummaryAmountType = {
                        ""
                         , "TotalMonthlyIncomeNotIncludingNetRentalIncome"
                         , "SubtotalLiabilitiesPaidByClosingNotIncludingSubjectPropertyLiensBalance"
                         , "TotalPresentHousingExpense"
                         , "TotalLiabilitesBalance"
                         , "SubtotalLiabilitesMonthlyPayment"
                         , "SubtotalOmittedLiabilitesBalance"
                         , "SubtotalOmittedLiabilitiesMonthlyPayment"
                         , "SubtotalLiabilitiesPaidByClosingNotIncludingSubjectPropertyLiensMonthlyPayment"
                         , "SubtotalResubordinatedLiabilitesMonthlyPaymentForSubjectProperty"
                         , "SubtotalSubjectPropertyLiensPaidByClosingBalance"
                         , "SubtotalSubjectPropertyLiensPaidByClosingMonthlyPayment"
                         , "SubtotalLiabilitiesForRentalPropertyBalance"
                         , "SubtotalLiabilitiesForRentalPropertyMonthlyPayment"
                         , "SubtotalResubordinatedLiabilitiesBalanceForSubjectProperty"
                         , "SubtotalLiquidAssetsNotIncludingGift"
                         , "SubtotalNonLiquidAssets"
                         , "TotalLiabilitiesBalance"
                         , "SubtotalLiabilitiesMonthlyPayment"
                         , "SubtotalOmittedLiabilitiesBalance"
                         , "SubtotalResubordinatedLiabilitiesMonthlyPaymentForSubjectProperty"
                         , "UndrawnHELOC"
                         , "Other"
                         };
    }
}
