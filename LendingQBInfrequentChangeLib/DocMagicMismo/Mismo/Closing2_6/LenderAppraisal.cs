// Generated by CodeMonkey on 3/22/2010 6:06:40 PM
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace Mismo.Closing2_6
{
    public class LenderAppraisal : AbstractXmlSerializable
    {
        #region Private Member Variables
        private string m_Id = string.Empty;
        private string m_StaffAppraisalReviewerIdentifier = string.Empty;
        private E_YNIndicator m_StaffAppraisalReviewValuationAdjustmentIndicator = E_YNIndicator.Undefined;
        private string m_StaffAppraisalReviewValueNotificationDate = string.Empty;
        #endregion

        #region Public Properties
        public string Id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }
        public string StaffAppraisalReviewerIdentifier
        {
            get { return m_StaffAppraisalReviewerIdentifier; }
            set { m_StaffAppraisalReviewerIdentifier = value; }
        }
        public E_YNIndicator StaffAppraisalReviewValuationAdjustmentIndicator
        {
            get { return m_StaffAppraisalReviewValuationAdjustmentIndicator; }
            set { m_StaffAppraisalReviewValuationAdjustmentIndicator = value; }
        }
        public string StaffAppraisalReviewValueNotificationDate
        {
            get { return m_StaffAppraisalReviewValueNotificationDate; }
            set { m_StaffAppraisalReviewValueNotificationDate = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "LENDER_APPRAISAL")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_Id = ReadAttribute(reader, "_ID");
            m_StaffAppraisalReviewerIdentifier = ReadAttribute(reader, "StaffAppraisalReviewerIdentifier");
            m_StaffAppraisalReviewValuationAdjustmentIndicator = (E_YNIndicator) ReadAttribute(reader, "StaffAppraisalReviewValuationAdjustmentIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator);
            m_StaffAppraisalReviewValueNotificationDate = ReadAttribute(reader, "StaffAppraisalReviewValueNotificationDate");
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "LENDER_APPRAISAL") return;

            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("LENDER_APPRAISAL");
            WriteAttribute(writer, "_ID", m_Id);
            WriteAttribute(writer, "StaffAppraisalReviewerIdentifier", m_StaffAppraisalReviewerIdentifier);
            WriteEnumAttribute(writer, "StaffAppraisalReviewValuationAdjustmentIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator[(int) m_StaffAppraisalReviewValuationAdjustmentIndicator]);
            WriteAttribute(writer, "StaffAppraisalReviewValueNotificationDate", m_StaffAppraisalReviewValueNotificationDate);
            writer.WriteEndElement();
        }
    }
}
