// Generated by CodeMonkey on 3/22/2010 6:06:41 PM
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace Mismo.Closing2_6
{
    public class Details : AbstractXmlSerializable
    {
        #region Private Member Variables
        private string m_Id = string.Empty;
        private E_YNIndicator m_CondominiumIndicator = E_YNIndicator.Undefined;
        private string m_CondominiumPudDeclarationsDescription = string.Empty;
        private string m_JudicialDistrictName = string.Empty;
        private string m_JudicialDivisionName = string.Empty;
        private E_YNIndicator m_ManufacturedHomeIndicator = E_YNIndicator.Undefined;
        private string m_NfipCommunityIdentifier = string.Empty;
        private string m_NfipCommunityName = string.Empty;
        private E_DetailsNfipCommunityParticipationStatusType m_NfipCommunityParticipationStatusType = E_DetailsNfipCommunityParticipationStatusType.Undefined;
        private string m_NfipCommunityParticipationStatusTypeOtherDescription = string.Empty;
        private string m_NfipFloodZoneIdentifier = string.Empty;
        private E_YNIndicator m_OneToFourFamilyIndicator = E_YNIndicator.Undefined;
        private string m_ProjectName = string.Empty;
        private string m_ProjectTotalSharesCount = string.Empty;
        private string m_PropertyUnincorporatedAreaName = string.Empty;
        private string m_RecordingJurisdictionName = string.Empty;
        private E_DetailsRecordingJurisdictionType m_RecordingJurisdictionType = E_DetailsRecordingJurisdictionType.Undefined;
        private string m_RecordingJurisdictionTypeOtherDescription = string.Empty;
        #endregion

        #region Public Properties
        public string Id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }
        public E_YNIndicator CondominiumIndicator
        {
            get { return m_CondominiumIndicator; }
            set { m_CondominiumIndicator = value; }
        }
        public string CondominiumPudDeclarationsDescription
        {
            get { return m_CondominiumPudDeclarationsDescription; }
            set { m_CondominiumPudDeclarationsDescription = value; }
        }
        public string JudicialDistrictName
        {
            get { return m_JudicialDistrictName; }
            set { m_JudicialDistrictName = value; }
        }
        public string JudicialDivisionName
        {
            get { return m_JudicialDivisionName; }
            set { m_JudicialDivisionName = value; }
        }
        public E_YNIndicator ManufacturedHomeIndicator
        {
            get { return m_ManufacturedHomeIndicator; }
            set { m_ManufacturedHomeIndicator = value; }
        }
        public string NfipCommunityIdentifier
        {
            get { return m_NfipCommunityIdentifier; }
            set { m_NfipCommunityIdentifier = value; }
        }
        public string NfipCommunityName
        {
            get { return m_NfipCommunityName; }
            set { m_NfipCommunityName = value; }
        }
        public E_DetailsNfipCommunityParticipationStatusType NfipCommunityParticipationStatusType
        {
            get { return m_NfipCommunityParticipationStatusType; }
            set { m_NfipCommunityParticipationStatusType = value; }
        }
        public string NfipCommunityParticipationStatusTypeOtherDescription
        {
            get { return m_NfipCommunityParticipationStatusTypeOtherDescription; }
            set { m_NfipCommunityParticipationStatusTypeOtherDescription = value; }
        }
        public string NfipFloodZoneIdentifier
        {
            get { return m_NfipFloodZoneIdentifier; }
            set { m_NfipFloodZoneIdentifier = value; }
        }
        public E_YNIndicator OneToFourFamilyIndicator
        {
            get { return m_OneToFourFamilyIndicator; }
            set { m_OneToFourFamilyIndicator = value; }
        }
        public string ProjectName
        {
            get { return m_ProjectName; }
            set { m_ProjectName = value; }
        }
        public string ProjectTotalSharesCount
        {
            get { return m_ProjectTotalSharesCount; }
            set { m_ProjectTotalSharesCount = value; }
        }
        public string PropertyUnincorporatedAreaName
        {
            get { return m_PropertyUnincorporatedAreaName; }
            set { m_PropertyUnincorporatedAreaName = value; }
        }
        public string RecordingJurisdictionName
        {
            get { return m_RecordingJurisdictionName; }
            set { m_RecordingJurisdictionName = value; }
        }
        public E_DetailsRecordingJurisdictionType RecordingJurisdictionType
        {
            get { return m_RecordingJurisdictionType; }
            set { m_RecordingJurisdictionType = value; }
        }
        public string RecordingJurisdictionTypeOtherDescription
        {
            get { return m_RecordingJurisdictionTypeOtherDescription; }
            set { m_RecordingJurisdictionTypeOtherDescription = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "_DETAILS")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_Id = ReadAttribute(reader, "_ID");
            m_CondominiumIndicator = (E_YNIndicator) ReadAttribute(reader, "CondominiumIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator);
            m_CondominiumPudDeclarationsDescription = ReadAttribute(reader, "CondominiumPUDDeclarationsDescription");
            m_JudicialDistrictName = ReadAttribute(reader, "JudicialDistrictName");
            m_JudicialDivisionName = ReadAttribute(reader, "JudicialDivisionName");
            m_ManufacturedHomeIndicator = (E_YNIndicator) ReadAttribute(reader, "ManufacturedHomeIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator);
            m_NfipCommunityIdentifier = ReadAttribute(reader, "NFIPCommunityIdentifier");
            m_NfipCommunityName = ReadAttribute(reader, "NFIPCommunityName");
            m_NfipCommunityParticipationStatusType = (E_DetailsNfipCommunityParticipationStatusType) ReadAttribute(reader, "NFIPCommunityParticipationStatusType", EnumMappings.E_DetailsNfipCommunityParticipationStatusType);
            m_NfipCommunityParticipationStatusTypeOtherDescription = ReadAttribute(reader, "NFIPCommunityParticipationStatusTypeOtherDescription");
            m_NfipFloodZoneIdentifier = ReadAttribute(reader, "NFIPFloodZoneIdentifier");
            m_OneToFourFamilyIndicator = (E_YNIndicator) ReadAttribute(reader, "OneToFourFamilyIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator);
            m_ProjectName = ReadAttribute(reader, "ProjectName");
            m_ProjectTotalSharesCount = ReadAttribute(reader, "ProjectTotalSharesCount");
            m_PropertyUnincorporatedAreaName = ReadAttribute(reader, "PropertyUnincorporatedAreaName");
            m_RecordingJurisdictionName = ReadAttribute(reader, "RecordingJurisdictionName");
            m_RecordingJurisdictionType = (E_DetailsRecordingJurisdictionType) ReadAttribute(reader, "RecordingJurisdictionType", EnumMappings.E_DetailsRecordingJurisdictionType);
            m_RecordingJurisdictionTypeOtherDescription = ReadAttribute(reader, "RecordingJurisdictionTypeOtherDescription");
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "_DETAILS") return;

            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("_DETAILS");
            WriteAttribute(writer, "_ID", m_Id);
            WriteEnumAttribute(writer, "CondominiumIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator[(int) m_CondominiumIndicator]);
            WriteAttribute(writer, "CondominiumPUDDeclarationsDescription", m_CondominiumPudDeclarationsDescription);
            WriteAttribute(writer, "JudicialDistrictName", m_JudicialDistrictName);
            WriteAttribute(writer, "JudicialDivisionName", m_JudicialDivisionName);
            WriteEnumAttribute(writer, "ManufacturedHomeIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator[(int) m_ManufacturedHomeIndicator]);
            WriteAttribute(writer, "NFIPCommunityIdentifier", m_NfipCommunityIdentifier);
            WriteAttribute(writer, "NFIPCommunityName", m_NfipCommunityName);
            WriteEnumAttribute(writer, "NFIPCommunityParticipationStatusType", EnumMappings.E_DetailsNfipCommunityParticipationStatusType[(int) m_NfipCommunityParticipationStatusType]);
            WriteAttribute(writer, "NFIPCommunityParticipationStatusTypeOtherDescription", m_NfipCommunityParticipationStatusTypeOtherDescription);
            WriteAttribute(writer, "NFIPFloodZoneIdentifier", m_NfipFloodZoneIdentifier);
            WriteEnumAttribute(writer, "OneToFourFamilyIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator[(int) m_OneToFourFamilyIndicator]);
            WriteAttribute(writer, "ProjectName", m_ProjectName);
            WriteAttribute(writer, "ProjectTotalSharesCount", m_ProjectTotalSharesCount);
            WriteAttribute(writer, "PropertyUnincorporatedAreaName", m_PropertyUnincorporatedAreaName);
            WriteAttribute(writer, "RecordingJurisdictionName", m_RecordingJurisdictionName);
            WriteEnumAttribute(writer, "RecordingJurisdictionType", EnumMappings.E_DetailsRecordingJurisdictionType[(int) m_RecordingJurisdictionType]);
            WriteAttribute(writer, "RecordingJurisdictionTypeOtherDescription", m_RecordingJurisdictionTypeOtherDescription);
            writer.WriteEndElement();
        }
    }

    public enum E_DetailsNfipCommunityParticipationStatusType
    {
        Undefined
        , Emergency
        , NonParticipating
        , Other
        , Probation
        , Regular
        , Suspended
        , Unknown
    }

    public enum E_DetailsRecordingJurisdictionType
    {
        Undefined
        , County
        , Parish
        , Other
    }

    static partial class EnumMappings
    {
        public static readonly string[] E_DetailsNfipCommunityParticipationStatusType = {
                        ""
                         , "Emergency"
                         , "NonParticipating"
                         , "Other"
                         , "Probation"
                         , "Regular"
                         , "Suspended"
                         , "Unknown"
                         };
        public static readonly string[] E_DetailsRecordingJurisdictionType = {
                        ""
                         , "County"
                         , "Parish"
                         , "Other"
                         };
    }
}
