// Generated by CodeMonkey on 3/22/2010 6:06:41 PM
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace Mismo.Closing2_6
{
    public class Valuation : AbstractXmlSerializable
    {
        #region Private Member Variables
        private string m_Id = string.Empty;
        private E_ValuationAppraisalFormType m_AppraisalFormType = E_ValuationAppraisalFormType.Undefined;
        private string m_AppraisalFormTypeOtherDescription = string.Empty;
        private string m_AppraisalFormVersionIdentifier = string.Empty;
        private E_ValuationAppraisalInspectionType m_AppraisalInspectionType = E_ValuationAppraisalInspectionType.Undefined;
        private string m_AppraisalInspectionTypeOtherDescription = string.Empty;
        private E_ValuationMethodType m_MethodType = E_ValuationMethodType.Undefined;
        private string m_MethodTypeOtherDescription = string.Empty;
        private List<Appraiser> m_AppraiserList = null;
        #endregion

        #region Public Properties
        public string Id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }
        public E_ValuationAppraisalFormType AppraisalFormType
        {
            get { return m_AppraisalFormType; }
            set { m_AppraisalFormType = value; }
        }
        public string AppraisalFormTypeOtherDescription
        {
            get { return m_AppraisalFormTypeOtherDescription; }
            set { m_AppraisalFormTypeOtherDescription = value; }
        }
        public string AppraisalFormVersionIdentifier
        {
            get { return m_AppraisalFormVersionIdentifier; }
            set { m_AppraisalFormVersionIdentifier = value; }
        }
        public E_ValuationAppraisalInspectionType AppraisalInspectionType
        {
            get { return m_AppraisalInspectionType; }
            set { m_AppraisalInspectionType = value; }
        }
        public string AppraisalInspectionTypeOtherDescription
        {
            get { return m_AppraisalInspectionTypeOtherDescription; }
            set { m_AppraisalInspectionTypeOtherDescription = value; }
        }
        public E_ValuationMethodType MethodType
        {
            get { return m_MethodType; }
            set { m_MethodType = value; }
        }
        public string MethodTypeOtherDescription
        {
            get { return m_MethodTypeOtherDescription; }
            set { m_MethodTypeOtherDescription = value; }
        }
        public List<Appraiser> AppraiserList
        {
            get
            {
                if (null == m_AppraiserList) m_AppraiserList = new List<Appraiser>();
                return m_AppraiserList;
            }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "_VALUATION")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_Id = ReadAttribute(reader, "_ID");
            m_AppraisalFormType = (E_ValuationAppraisalFormType) ReadAttribute(reader, "AppraisalFormType", EnumMappings.E_ValuationAppraisalFormType);
            m_AppraisalFormTypeOtherDescription = ReadAttribute(reader, "AppraisalFormTypeOtherDescription");
            m_AppraisalFormVersionIdentifier = ReadAttribute(reader, "AppraisalFormVersionIdentifier");
            m_AppraisalInspectionType = (E_ValuationAppraisalInspectionType) ReadAttribute(reader, "AppraisalInspectionType", EnumMappings.E_ValuationAppraisalInspectionType);
            m_AppraisalInspectionTypeOtherDescription = ReadAttribute(reader, "AppraisalInspectionTypeOtherDescription");
            m_MethodType = (E_ValuationMethodType) ReadAttribute(reader, "_MethodType", EnumMappings.E_ValuationMethodType);
            m_MethodTypeOtherDescription = ReadAttribute(reader, "_MethodTypeOtherDescription");
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "_VALUATION") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                switch (reader.Name)
                {
                    case "APPRAISER":
                        ReadElement(reader, AppraiserList);
                        break;
                }
                #endregion
            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("_VALUATION");
            WriteAttribute(writer, "_ID", m_Id);
            WriteEnumAttribute(writer, "AppraisalFormType", EnumMappings.E_ValuationAppraisalFormType[(int) m_AppraisalFormType]);
            WriteAttribute(writer, "AppraisalFormTypeOtherDescription", m_AppraisalFormTypeOtherDescription);
            WriteAttribute(writer, "AppraisalFormVersionIdentifier", m_AppraisalFormVersionIdentifier);
            WriteEnumAttribute(writer, "AppraisalInspectionType", EnumMappings.E_ValuationAppraisalInspectionType[(int) m_AppraisalInspectionType]);
            WriteAttribute(writer, "AppraisalInspectionTypeOtherDescription", m_AppraisalInspectionTypeOtherDescription);
            WriteEnumAttribute(writer, "_MethodType", EnumMappings.E_ValuationMethodType[(int) m_MethodType]);
            WriteAttribute(writer, "_MethodTypeOtherDescription", m_MethodTypeOtherDescription);
            WriteElement(writer, m_AppraiserList);
            writer.WriteEndElement();
        }
    }

    public enum E_ValuationAppraisalFormType
    {
        Undefined
        , FNM1004FRE70
        , FNM1004BFRE439
        , FNM1004CFRE70B
        , FNM1004DFRE442
        , ERC2001
        , FNM1073FRE465
        , FNM1075FRE466
        , FNM1025FRE72
        , FNM2000FRE1032
        , FNM2000AFRE1072
        , FNM2055FRE2055
        , FNM2065
        , FNM2075
        , FNM2090
        , FNM2095
        , MobileHome
        , VacantLand
        , FRE2070
        , Other
    }

    public enum E_ValuationAppraisalInspectionType
    {
        Undefined
        , ExteriorAndInterior
        , ExteriorOnly
        , None
        , Other
    }

    public enum E_ValuationMethodType
    {
        Undefined
        , FNM1004
        , EmployeeRelocationCouncil2001
        , FNM1073
        , FNM1025
        , FNM2055Exterior
        , FNM2065
        , FRE2070Interior
        , FRE2070Exterior
        , FNM2075
        , BrokerPriceOpinion
        , AutomatedValuationModel
        , TaxValuation
        , DriveBy
        , FullAppraisal
        , None
        , FNM2055InteriorAndExterior
        , FNM2095Exterior
        , FNM2095InteriorAndExterior
        , PriorAppraisalUsed
        , Form261805
        , Form268712
        , Other
    }

    static partial class EnumMappings
    {
        public static readonly string[] E_ValuationAppraisalFormType = {
                        ""
                         , "FNM1004FRE70"
                         , "FNM1004BFRE439"
                         , "FNM1004CFRE70B"
                         , "FNM1004DFRE442"
                         , "ERC2001"
                         , "FNM1073FRE465"
                         , "FNM1075FRE466"
                         , "FNM1025FRE72"
                         , "FNM2000FRE1032"
                         , "FNM2000AFRE1072"
                         , "FNM2055FRE2055"
                         , "FNM2065"
                         , "FNM2075"
                         , "FNM2090"
                         , "FNM2095"
                         , "MobileHome"
                         , "VacantLand"
                         , "FRE2070"
                         , "Other"
                         };
        public static readonly string[] E_ValuationAppraisalInspectionType = {
                        ""
                         , "ExteriorAndInterior"
                         , "ExteriorOnly"
                         , "None"
                         , "Other"
                         };
        public static readonly string[] E_ValuationMethodType = {
                        ""
                         , "FNM1004"
                         , "EmployeeRelocationCouncil2001"
                         , "FNM1073"
                         , "FNM1025"
                         , "FNM2055Exterior"
                         , "FNM2065"
                         , "FRE2070Interior"
                         , "FRE2070Exterior"
                         , "FNM2075"
                         , "BrokerPriceOpinion"
                         , "AutomatedValuationModel"
                         , "TaxValuation"
                         , "DriveBy"
                         , "FullAppraisal"
                         , "None"
                         , "FNM2055InteriorAndExterior"
                         , "FNM2095Exterior"
                         , "FNM2095InteriorAndExterior"
                         , "PriorAppraisalUsed"
                         , "Form261805"
                         , "Form268712"
                         , "Other"
                         };
    }
}
