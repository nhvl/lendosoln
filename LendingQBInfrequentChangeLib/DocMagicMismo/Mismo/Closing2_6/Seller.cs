// Generated by CodeMonkey on 3/22/2010 6:06:43 PM
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace Mismo.Closing2_6
{
    public class Seller : AbstractXmlSerializable
    {
        #region Private Member Variables
        private string m_Id = string.Empty;
        private E_YNIndicator m_NonPersonEntityIndicator = E_YNIndicator.Undefined;
        private string m_City = string.Empty;
        private string m_Country = string.Empty;
        private string m_FirstName = string.Empty;
        private string m_Identifier = string.Empty;
        private string m_LastName = string.Empty;
        private string m_MiddleName = string.Empty;
        private string m_NameSuffix = string.Empty;
        private string m_PostalCode = string.Empty;
        private string m_SequenceIdentifier = string.Empty;
        private string m_State = string.Empty;
        private string m_StreetAddress = string.Empty;
        private string m_StreetAddress2 = string.Empty;
        private string m_UnparsedName = string.Empty;
        private ContactDetail m_ContactDetail = null;
        private NonPersonEntityDetail m_NonPersonEntityDetail = null;
        private PowerOfAttorney m_PowerOfAttorney = null;
        #endregion

        #region Public Properties
        public string Id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }
        public E_YNIndicator NonPersonEntityIndicator
        {
            get { return m_NonPersonEntityIndicator; }
            set { m_NonPersonEntityIndicator = value; }
        }
        public string City
        {
            get { return m_City; }
            set { m_City = value; }
        }
        public string Country
        {
            get { return m_Country; }
            set { m_Country = value; }
        }
        public string FirstName
        {
            get { return m_FirstName; }
            set { m_FirstName = value; }
        }
        public string Identifier
        {
            get { return m_Identifier; }
            set { m_Identifier = value; }
        }
        public string LastName
        {
            get { return m_LastName; }
            set { m_LastName = value; }
        }
        public string MiddleName
        {
            get { return m_MiddleName; }
            set { m_MiddleName = value; }
        }
        public string NameSuffix
        {
            get { return m_NameSuffix; }
            set { m_NameSuffix = value; }
        }
        public string PostalCode
        {
            get { return m_PostalCode; }
            set { m_PostalCode = value; }
        }
        public string SequenceIdentifier
        {
            get { return m_SequenceIdentifier; }
            set { m_SequenceIdentifier = value; }
        }
        public string State
        {
            get { return m_State; }
            set { m_State = value; }
        }
        public string StreetAddress
        {
            get { return m_StreetAddress; }
            set { m_StreetAddress = value; }
        }
        public string StreetAddress2
        {
            get { return m_StreetAddress2; }
            set { m_StreetAddress2 = value; }
        }
        public string UnparsedName
        {
            get { return m_UnparsedName; }
            set { m_UnparsedName = value; }
        }
        public ContactDetail ContactDetail
        {
            get
            {
                if (null == m_ContactDetail) m_ContactDetail = new ContactDetail();
                return m_ContactDetail;
            }
            set { m_ContactDetail = value; }
        }
        public NonPersonEntityDetail NonPersonEntityDetail
        {
            get
            {
                if (null == m_NonPersonEntityDetail) m_NonPersonEntityDetail = new NonPersonEntityDetail();
                return m_NonPersonEntityDetail;
            }
            set { m_NonPersonEntityDetail = value; }
        }
        public PowerOfAttorney PowerOfAttorney
        {
            get
            {
                if (null == m_PowerOfAttorney) m_PowerOfAttorney = new PowerOfAttorney();
                return m_PowerOfAttorney;
            }
            set { m_PowerOfAttorney = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "SELLER")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_Id = ReadAttribute(reader, "_ID");
            m_NonPersonEntityIndicator = (E_YNIndicator) ReadAttribute(reader, "NonPersonEntityIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator);
            m_City = ReadAttribute(reader, "_City");
            m_Country = ReadAttribute(reader, "_Country");
            m_FirstName = ReadAttribute(reader, "_FirstName");
            m_Identifier = ReadAttribute(reader, "_Identifier");
            m_LastName = ReadAttribute(reader, "_LastName");
            m_MiddleName = ReadAttribute(reader, "_MiddleName");
            m_NameSuffix = ReadAttribute(reader, "_NameSuffix");
            m_PostalCode = ReadAttribute(reader, "_PostalCode");
            m_SequenceIdentifier = ReadAttribute(reader, "_SequenceIdentifier");
            m_State = ReadAttribute(reader, "_State");
            m_StreetAddress = ReadAttribute(reader, "_StreetAddress");
            m_StreetAddress2 = ReadAttribute(reader, "_StreetAddress2");
            m_UnparsedName = ReadAttribute(reader, "_UnparsedName");
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "SELLER") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                switch (reader.Name)
                {
                    case "CONTACT_DETAIL":
                        ReadElement(reader, ContactDetail);
                        break;
                    case "NON_PERSON_ENTITY_DETAIL":
                        ReadElement(reader, NonPersonEntityDetail);
                        break;
                    case "_POWER_OF_ATTORNEY":
                        ReadElement(reader, PowerOfAttorney);
                        break;
                }
                #endregion
            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("SELLER");
            WriteAttribute(writer, "_ID", m_Id);
            WriteEnumAttribute(writer, "NonPersonEntityIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator[(int) m_NonPersonEntityIndicator]);
            WriteAttribute(writer, "_City", m_City);
            WriteAttribute(writer, "_Country", m_Country);
            WriteAttribute(writer, "_FirstName", m_FirstName);
            WriteAttribute(writer, "_Identifier", m_Identifier);
            WriteAttribute(writer, "_LastName", m_LastName);
            WriteAttribute(writer, "_MiddleName", m_MiddleName);
            WriteAttribute(writer, "_NameSuffix", m_NameSuffix);
            WriteAttribute(writer, "_PostalCode", m_PostalCode);
            WriteAttribute(writer, "_SequenceIdentifier", m_SequenceIdentifier);
            WriteAttribute(writer, "_State", m_State);
            WriteAttribute(writer, "_StreetAddress", m_StreetAddress);
            WriteAttribute(writer, "_StreetAddress2", m_StreetAddress2);
            WriteAttribute(writer, "_UnparsedName", m_UnparsedName);
            WriteElement(writer, m_ContactDetail);
            WriteElement(writer, m_NonPersonEntityDetail);
            WriteElement(writer, m_PowerOfAttorney);
            writer.WriteEndElement();
        }
    }
}
