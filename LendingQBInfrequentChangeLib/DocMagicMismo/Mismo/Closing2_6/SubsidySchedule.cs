// Generated by CodeMonkey on 3/22/2010 6:06:40 PM
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace Mismo.Closing2_6
{
    public class SubsidySchedule : AbstractXmlSerializable
    {
        #region Private Member Variables
        private string m_Id = string.Empty;
        private string m_AdjustmentPercent = string.Empty;
        private string m_PeriodIdentifier = string.Empty;
        private string m_PeriodicPaymentEffectiveDate = string.Empty;
        private string m_PeriodicPaymentSubsidyAmount = string.Empty;
        private string m_PeriodicTerm = string.Empty;
        #endregion

        #region Public Properties
        public string Id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }
        public string AdjustmentPercent
        {
            get { return m_AdjustmentPercent; }
            set { m_AdjustmentPercent = value; }
        }
        public string PeriodIdentifier
        {
            get { return m_PeriodIdentifier; }
            set { m_PeriodIdentifier = value; }
        }
        public string PeriodicPaymentEffectiveDate
        {
            get { return m_PeriodicPaymentEffectiveDate; }
            set { m_PeriodicPaymentEffectiveDate = value; }
        }
        public string PeriodicPaymentSubsidyAmount
        {
            get { return m_PeriodicPaymentSubsidyAmount; }
            set { m_PeriodicPaymentSubsidyAmount = value; }
        }
        public string PeriodicTerm
        {
            get { return m_PeriodicTerm; }
            set { m_PeriodicTerm = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "_SUBSIDY_SCHEDULE")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_Id = ReadAttribute(reader, "_ID");
            m_AdjustmentPercent = ReadAttribute(reader, "_AdjustmentPercent");
            m_PeriodIdentifier = ReadAttribute(reader, "_PeriodIdentifier");
            m_PeriodicPaymentEffectiveDate = ReadAttribute(reader, "_PeriodicPaymentEffectiveDate");
            m_PeriodicPaymentSubsidyAmount = ReadAttribute(reader, "_PeriodicPaymentSubsidyAmount");
            m_PeriodicTerm = ReadAttribute(reader, "_PeriodicTerm");
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "_SUBSIDY_SCHEDULE") return;

            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("_SUBSIDY_SCHEDULE");
            WriteAttribute(writer, "_ID", m_Id);
            WriteAttribute(writer, "_AdjustmentPercent", m_AdjustmentPercent);
            WriteAttribute(writer, "_PeriodIdentifier", m_PeriodIdentifier);
            WriteAttribute(writer, "_PeriodicPaymentEffectiveDate", m_PeriodicPaymentEffectiveDate);
            WriteAttribute(writer, "_PeriodicPaymentSubsidyAmount", m_PeriodicPaymentSubsidyAmount);
            WriteAttribute(writer, "_PeriodicTerm", m_PeriodicTerm);
            writer.WriteEndElement();
        }
    }
}
