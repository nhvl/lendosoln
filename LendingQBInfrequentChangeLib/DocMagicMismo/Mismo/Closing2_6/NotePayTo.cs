// Generated by CodeMonkey on 3/22/2010 6:06:40 PM
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace Mismo.Closing2_6
{
    public class NotePayTo : AbstractXmlSerializable
    {
        #region Private Member Variables
        private string m_Id = string.Empty;
        private string m_City = string.Empty;
        private string m_Country = string.Empty;
        private string m_PostalCode = string.Empty;
        private string m_State = string.Empty;
        private string m_StreetAddress = string.Empty;
        private string m_StreetAddress2 = string.Empty;
        private string m_UnparsedName = string.Empty;
        #endregion

        #region Public Properties
        public string Id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }
        public string City
        {
            get { return m_City; }
            set { m_City = value; }
        }
        public string Country
        {
            get { return m_Country; }
            set { m_Country = value; }
        }
        public string PostalCode
        {
            get { return m_PostalCode; }
            set { m_PostalCode = value; }
        }
        public string State
        {
            get { return m_State; }
            set { m_State = value; }
        }
        public string StreetAddress
        {
            get { return m_StreetAddress; }
            set { m_StreetAddress = value; }
        }
        public string StreetAddress2
        {
            get { return m_StreetAddress2; }
            set { m_StreetAddress2 = value; }
        }
        public string UnparsedName
        {
            get { return m_UnparsedName; }
            set { m_UnparsedName = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "NOTE_PAY_TO")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_Id = ReadAttribute(reader, "_ID");
            m_City = ReadAttribute(reader, "_City");
            m_Country = ReadAttribute(reader, "_Country");
            m_PostalCode = ReadAttribute(reader, "_PostalCode");
            m_State = ReadAttribute(reader, "_State");
            m_StreetAddress = ReadAttribute(reader, "_StreetAddress");
            m_StreetAddress2 = ReadAttribute(reader, "_StreetAddress2");
            m_UnparsedName = ReadAttribute(reader, "_UnparsedName");
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "NOTE_PAY_TO") return;

            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("NOTE_PAY_TO");
            WriteAttribute(writer, "_ID", m_Id);
            WriteAttribute(writer, "_City", m_City);
            WriteAttribute(writer, "_Country", m_Country);
            WriteAttribute(writer, "_PostalCode", m_PostalCode);
            WriteAttribute(writer, "_State", m_State);
            WriteAttribute(writer, "_StreetAddress", m_StreetAddress);
            WriteAttribute(writer, "_StreetAddress2", m_StreetAddress2);
            WriteAttribute(writer, "_UnparsedName", m_UnparsedName);
            writer.WriteEndElement();
        }
    }
}
