// Generated by CodeMonkey on 3/22/2010 6:06:41 PM
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace Mismo.Closing2_6
{
    public class MortgageTerms : AbstractXmlSerializable
    {
        #region Private Member Variables
        private string m_Id = string.Empty;
        private string m_ArmTypeDescription = string.Empty;
        private string m_AgencyCaseIdentifier = string.Empty;
        private string m_BaseLoanAmount = string.Empty;
        private string m_BorrowerRequestedLoanAmount = string.Empty;
        private string m_LenderCaseIdentifier = string.Empty;
        private string m_LenderLoanIdentifier = string.Empty;
        private string m_LendersContactPrimaryTelephoneNumber = string.Empty;
        private string m_LoanAmortizationTermMonths = string.Empty;
        private E_MortgageTermsLoanAmortizationType m_LoanAmortizationType = E_MortgageTermsLoanAmortizationType.Undefined;
        private string m_LoanEstimatedClosingDate = string.Empty;
        private E_MortgageTermsMortgageType m_MortgageType = E_MortgageTermsMortgageType.Undefined;
        private string m_NoteRatePercent = string.Empty;
        private string m_OriginalLoanAmount = string.Empty;
        private string m_OtherAmortizationTypeDescription = string.Empty;
        private string m_OtherMortgageTypeDescription = string.Empty;
        private string m_PaymentRemittanceDay = string.Empty;
        private string m_RequestedInterestRatePercent = string.Empty;
        #endregion

        #region Public Properties
        public string Id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }
        public string ArmTypeDescription
        {
            get { return m_ArmTypeDescription; }
            set { m_ArmTypeDescription = value; }
        }
        public string AgencyCaseIdentifier
        {
            get { return m_AgencyCaseIdentifier; }
            set { m_AgencyCaseIdentifier = value; }
        }
        public string BaseLoanAmount
        {
            get { return m_BaseLoanAmount; }
            set { m_BaseLoanAmount = value; }
        }
        public string BorrowerRequestedLoanAmount
        {
            get { return m_BorrowerRequestedLoanAmount; }
            set { m_BorrowerRequestedLoanAmount = value; }
        }
        public string LenderCaseIdentifier
        {
            get { return m_LenderCaseIdentifier; }
            set { m_LenderCaseIdentifier = value; }
        }
        public string LenderLoanIdentifier
        {
            get { return m_LenderLoanIdentifier; }
            set { m_LenderLoanIdentifier = value; }
        }
        public string LendersContactPrimaryTelephoneNumber
        {
            get { return m_LendersContactPrimaryTelephoneNumber; }
            set { m_LendersContactPrimaryTelephoneNumber = value; }
        }
        public string LoanAmortizationTermMonths
        {
            get { return m_LoanAmortizationTermMonths; }
            set { m_LoanAmortizationTermMonths = value; }
        }
        public E_MortgageTermsLoanAmortizationType LoanAmortizationType
        {
            get { return m_LoanAmortizationType; }
            set { m_LoanAmortizationType = value; }
        }
        public string LoanEstimatedClosingDate
        {
            get { return m_LoanEstimatedClosingDate; }
            set { m_LoanEstimatedClosingDate = value; }
        }
        public E_MortgageTermsMortgageType MortgageType
        {
            get { return m_MortgageType; }
            set { m_MortgageType = value; }
        }
        public string NoteRatePercent
        {
            get { return m_NoteRatePercent; }
            set { m_NoteRatePercent = value; }
        }
        public string OriginalLoanAmount
        {
            get { return m_OriginalLoanAmount; }
            set { m_OriginalLoanAmount = value; }
        }
        public string OtherAmortizationTypeDescription
        {
            get { return m_OtherAmortizationTypeDescription; }
            set { m_OtherAmortizationTypeDescription = value; }
        }
        public string OtherMortgageTypeDescription
        {
            get { return m_OtherMortgageTypeDescription; }
            set { m_OtherMortgageTypeDescription = value; }
        }
        public string PaymentRemittanceDay
        {
            get { return m_PaymentRemittanceDay; }
            set { m_PaymentRemittanceDay = value; }
        }
        public string RequestedInterestRatePercent
        {
            get { return m_RequestedInterestRatePercent; }
            set { m_RequestedInterestRatePercent = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "MORTGAGE_TERMS")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_Id = ReadAttribute(reader, "_ID");
            m_ArmTypeDescription = ReadAttribute(reader, "ARMTypeDescription");
            m_AgencyCaseIdentifier = ReadAttribute(reader, "AgencyCaseIdentifier");
            m_BaseLoanAmount = ReadAttribute(reader, "BaseLoanAmount");
            m_BorrowerRequestedLoanAmount = ReadAttribute(reader, "BorrowerRequestedLoanAmount");
            m_LenderCaseIdentifier = ReadAttribute(reader, "LenderCaseIdentifier");
            m_LenderLoanIdentifier = ReadAttribute(reader, "LenderLoanIdentifier");
            m_LendersContactPrimaryTelephoneNumber = ReadAttribute(reader, "LendersContactPrimaryTelephoneNumber");
            m_LoanAmortizationTermMonths = ReadAttribute(reader, "LoanAmortizationTermMonths");
            m_LoanAmortizationType = (E_MortgageTermsLoanAmortizationType) ReadAttribute(reader, "LoanAmortizationType", EnumMappings.E_MortgageTermsLoanAmortizationType);
            m_LoanEstimatedClosingDate = ReadAttribute(reader, "LoanEstimatedClosingDate");
            m_MortgageType = (E_MortgageTermsMortgageType) ReadAttribute(reader, "MortgageType", EnumMappings.E_MortgageTermsMortgageType);
            m_NoteRatePercent = ReadAttribute(reader, "NoteRatePercent");
            m_OriginalLoanAmount = ReadAttribute(reader, "OriginalLoanAmount");
            m_OtherAmortizationTypeDescription = ReadAttribute(reader, "OtherAmortizationTypeDescription");
            m_OtherMortgageTypeDescription = ReadAttribute(reader, "OtherMortgageTypeDescription");
            m_PaymentRemittanceDay = ReadAttribute(reader, "PaymentRemittanceDay");
            m_RequestedInterestRatePercent = ReadAttribute(reader, "RequestedInterestRatePercent");
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "MORTGAGE_TERMS") return;

            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("MORTGAGE_TERMS");
            WriteAttribute(writer, "_ID", m_Id);
            WriteAttribute(writer, "ARMTypeDescription", m_ArmTypeDescription);
            WriteAttribute(writer, "AgencyCaseIdentifier", m_AgencyCaseIdentifier);
            WriteAttribute(writer, "BaseLoanAmount", m_BaseLoanAmount);
            WriteAttribute(writer, "BorrowerRequestedLoanAmount", m_BorrowerRequestedLoanAmount);
            WriteAttribute(writer, "LenderCaseIdentifier", m_LenderCaseIdentifier);
            WriteAttribute(writer, "LenderLoanIdentifier", m_LenderLoanIdentifier);
            WriteAttribute(writer, "LendersContactPrimaryTelephoneNumber", m_LendersContactPrimaryTelephoneNumber);
            WriteAttribute(writer, "LoanAmortizationTermMonths", m_LoanAmortizationTermMonths);
            WriteEnumAttribute(writer, "LoanAmortizationType", EnumMappings.E_MortgageTermsLoanAmortizationType[(int) m_LoanAmortizationType]);
            WriteAttribute(writer, "LoanEstimatedClosingDate", m_LoanEstimatedClosingDate);
            WriteEnumAttribute(writer, "MortgageType", EnumMappings.E_MortgageTermsMortgageType[(int) m_MortgageType]);
            WriteAttribute(writer, "NoteRatePercent", m_NoteRatePercent);
            WriteAttribute(writer, "OriginalLoanAmount", m_OriginalLoanAmount);
            WriteAttribute(writer, "OtherAmortizationTypeDescription", m_OtherAmortizationTypeDescription);
            WriteAttribute(writer, "OtherMortgageTypeDescription", m_OtherMortgageTypeDescription);
            WriteAttribute(writer, "PaymentRemittanceDay", m_PaymentRemittanceDay);
            WriteAttribute(writer, "RequestedInterestRatePercent", m_RequestedInterestRatePercent);
            writer.WriteEndElement();
        }
    }

    public enum E_MortgageTermsLoanAmortizationType
    {
        Undefined
        , AdjustableRate
        , Fixed
        , GraduatedPaymentMortgage
        , GrowingEquityMortgage
        , OtherAmortizationType
        , DailySimpleInterest
        , GraduatedPaymentARM
        , RateImprovementMortgage
        , ReverseMortgage
        , Ruleof78s
        , Step
    }

    public enum E_MortgageTermsMortgageType
    {
        Undefined
        , Conventional
        , FarmersHomeAdministration
        , FHA
        , Other
        , VA
        , HELOC
        , LocalAgency
        , StateAgency
    }

    static partial class EnumMappings
    {
        public static readonly string[] E_MortgageTermsLoanAmortizationType = {
                        ""
                         , "AdjustableRate"
                         , "Fixed"
                         , "GraduatedPaymentMortgage"
                         , "GrowingEquityMortgage"
                         , "OtherAmortizationType"
                         , "DailySimpleInterest"
                         , "GraduatedPaymentARM"
                         , "RateImprovementMortgage"
                         , "ReverseMortgage"
                         , "Ruleof78s"
                         , "Step"
                         };
        public static readonly string[] E_MortgageTermsMortgageType = {
                        ""
                         , "Conventional"
                         , "FarmersHomeAdministration"
                         , "FHA"
                         , "Other"
                         , "VA"
                         , "HELOC"
                         , "LocalAgency"
                         , "StateAgency"
                         };
    }
}
