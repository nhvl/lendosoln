// Generated by CodeMonkey on 3/22/2010 6:06:43 PM
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace Mismo.Closing2_6
{
    public class Trust : AbstractXmlSerializable
    {
        #region Private Member Variables
        private string m_Id = string.Empty;
        private E_YNIndicator m_NonPersonEntityIndicator = E_YNIndicator.Undefined;
        private string m_EstablishedDate = string.Empty;
        private string m_Name = string.Empty;
        private string m_State = string.Empty;
        private E_YNIndicator m_NonObligatedIndicator = E_YNIndicator.Undefined;
        private NonPersonEntityDetail m_NonPersonEntityDetail = null;
        private List<Beneficiary> m_BeneficiaryList = null;
        private List<Trustee> m_TrusteeList = null;
        private List<Grantor> m_GrantorList = null;
        #endregion

        #region Public Properties
        public string Id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }
        public E_YNIndicator NonPersonEntityIndicator
        {
            get { return m_NonPersonEntityIndicator; }
            set { m_NonPersonEntityIndicator = value; }
        }
        public string EstablishedDate
        {
            get { return m_EstablishedDate; }
            set { m_EstablishedDate = value; }
        }
        public string Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }
        public string State
        {
            get { return m_State; }
            set { m_State = value; }
        }
        public E_YNIndicator NonObligatedIndicator
        {
            get { return m_NonObligatedIndicator; }
            set { m_NonObligatedIndicator = value; }
        }
        public NonPersonEntityDetail NonPersonEntityDetail
        {
            get
            {
                if (null == m_NonPersonEntityDetail) m_NonPersonEntityDetail = new NonPersonEntityDetail();
                return m_NonPersonEntityDetail;
            }
            set { m_NonPersonEntityDetail = value; }
        }
        public List<Beneficiary> BeneficiaryList
        {
            get
            {
                if (null == m_BeneficiaryList) m_BeneficiaryList = new List<Beneficiary>();
                return m_BeneficiaryList;
            }
        }
        public List<Trustee> TrusteeList
        {
            get
            {
                if (null == m_TrusteeList) m_TrusteeList = new List<Trustee>();
                return m_TrusteeList;
            }
        }
        public List<Grantor> GrantorList
        {
            get
            {
                if (null == m_GrantorList) m_GrantorList = new List<Grantor>();
                return m_GrantorList;
            }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "TRUST")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_Id = ReadAttribute(reader, "_ID");
            m_NonPersonEntityIndicator = (E_YNIndicator) ReadAttribute(reader, "NonPersonEntityIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator);
            m_EstablishedDate = ReadAttribute(reader, "_EstablishedDate");
            m_Name = ReadAttribute(reader, "_Name");
            m_State = ReadAttribute(reader, "_State");
            m_NonObligatedIndicator = (E_YNIndicator) ReadAttribute(reader, "_NonObligatedIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator);
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "TRUST") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                switch (reader.Name)
                {
                    case "NON_PERSON_ENTITY_DETAIL":
                        ReadElement(reader, NonPersonEntityDetail);
                        break;
                    case "BENEFICIARY":
                        ReadElement(reader, BeneficiaryList);
                        break;
                    case "TRUSTEE":
                        ReadElement(reader, TrusteeList);
                        break;
                    case "GRANTOR":
                        ReadElement(reader, GrantorList);
                        break;
                }
                #endregion
            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("TRUST");
            WriteAttribute(writer, "_ID", m_Id);
            WriteEnumAttribute(writer, "NonPersonEntityIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator[(int) m_NonPersonEntityIndicator]);
            WriteAttribute(writer, "_EstablishedDate", m_EstablishedDate);
            WriteAttribute(writer, "_Name", m_Name);
            WriteAttribute(writer, "_State", m_State);
            WriteEnumAttribute(writer, "_NonObligatedIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator[(int) m_NonObligatedIndicator]);
            WriteElement(writer, m_NonPersonEntityDetail);
            WriteElement(writer, m_BeneficiaryList);
            WriteElement(writer, m_TrusteeList);
            WriteElement(writer, m_GrantorList);
            writer.WriteEndElement();
        }
    }
}
