// Generated by CodeMonkey on 3/22/2010 6:06:42 PM
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace Mismo.Closing2_6
{
    public class InvestorFeature : AbstractXmlSerializable
    {
        #region Private Member Variables
        private string m_Id = string.Empty;
        private string m_CategoryName = string.Empty;
        private string m_Description = string.Empty;
        private string m_Identifier = string.Empty;
        private string m_Name = string.Empty;
        #endregion

        #region Public Properties
        public string Id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }
        public string CategoryName
        {
            get { return m_CategoryName; }
            set { m_CategoryName = value; }
        }
        public string Description
        {
            get { return m_Description; }
            set { m_Description = value; }
        }
        public string Identifier
        {
            get { return m_Identifier; }
            set { m_Identifier = value; }
        }
        public string Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "INVESTOR_FEATURE")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_Id = ReadAttribute(reader, "_ID");
            m_CategoryName = ReadAttribute(reader, "_CategoryName");
            m_Description = ReadAttribute(reader, "_Description");
            m_Identifier = ReadAttribute(reader, "_Identifier");
            m_Name = ReadAttribute(reader, "_Name");
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "INVESTOR_FEATURE") return;

            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("INVESTOR_FEATURE");
            WriteAttribute(writer, "_ID", m_Id);
            WriteAttribute(writer, "_CategoryName", m_CategoryName);
            WriteAttribute(writer, "_Description", m_Description);
            WriteAttribute(writer, "_Identifier", m_Identifier);
            WriteAttribute(writer, "_Name", m_Name);
            writer.WriteEndElement();
        }
    }
}
