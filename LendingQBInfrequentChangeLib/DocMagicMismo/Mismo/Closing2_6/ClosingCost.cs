// Generated by CodeMonkey on 3/22/2010 6:06:43 PM
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace Mismo.Closing2_6
{
    public class ClosingCost : AbstractXmlSerializable
    {
        #region Private Member Variables
        private string m_Id = string.Empty;
        private string m_ContributionAmount = string.Empty;
        private E_YNIndicator m_FinancedIndicator = E_YNIndicator.Undefined;
        private E_ClosingCostFundsType m_FundsType = E_ClosingCostFundsType.Undefined;
        private string m_FundsTypeOtherDescription = string.Empty;
        private E_ClosingCostSourceType m_SourceType = E_ClosingCostSourceType.Undefined;
        private string m_SourceTypeOtherDescription = string.Empty;
        #endregion

        #region Public Properties
        public string Id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }
        public string ContributionAmount
        {
            get { return m_ContributionAmount; }
            set { m_ContributionAmount = value; }
        }
        public E_YNIndicator FinancedIndicator
        {
            get { return m_FinancedIndicator; }
            set { m_FinancedIndicator = value; }
        }
        public E_ClosingCostFundsType FundsType
        {
            get { return m_FundsType; }
            set { m_FundsType = value; }
        }
        public string FundsTypeOtherDescription
        {
            get { return m_FundsTypeOtherDescription; }
            set { m_FundsTypeOtherDescription = value; }
        }
        public E_ClosingCostSourceType SourceType
        {
            get { return m_SourceType; }
            set { m_SourceType = value; }
        }
        public string SourceTypeOtherDescription
        {
            get { return m_SourceTypeOtherDescription; }
            set { m_SourceTypeOtherDescription = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "CLOSING_COST")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_Id = ReadAttribute(reader, "_ID");
            m_ContributionAmount = ReadAttribute(reader, "_ContributionAmount");
            m_FinancedIndicator = (E_YNIndicator) ReadAttribute(reader, "_FinancedIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator);
            m_FundsType = (E_ClosingCostFundsType) ReadAttribute(reader, "_FundsType", EnumMappings.E_ClosingCostFundsType);
            m_FundsTypeOtherDescription = ReadAttribute(reader, "_FundsTypeOtherDescription");
            m_SourceType = (E_ClosingCostSourceType) ReadAttribute(reader, "_SourceType", EnumMappings.E_ClosingCostSourceType);
            m_SourceTypeOtherDescription = ReadAttribute(reader, "_SourceTypeOtherDescription");
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "CLOSING_COST") return;

            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("CLOSING_COST");
            WriteAttribute(writer, "_ID", m_Id);
            WriteAttribute(writer, "_ContributionAmount", m_ContributionAmount);
            WriteEnumAttribute(writer, "_FinancedIndicator", XmlSerializableCommon.EnumMappings.E_YNIndicator[(int) m_FinancedIndicator]);
            WriteEnumAttribute(writer, "_FundsType", EnumMappings.E_ClosingCostFundsType[(int) m_FundsType]);
            WriteAttribute(writer, "_FundsTypeOtherDescription", m_FundsTypeOtherDescription);
            WriteEnumAttribute(writer, "_SourceType", EnumMappings.E_ClosingCostSourceType[(int) m_SourceType]);
            WriteAttribute(writer, "_SourceTypeOtherDescription", m_SourceTypeOtherDescription);
            writer.WriteEndElement();
        }
    }

    public enum E_ClosingCostFundsType
    {
        Undefined
        , BridgeLoan
        , CashOnHand
        , CashOrOtherEquity
        , CheckingSavings
        , Contribution
        , CreditCard
        , DepositOnSalesContract
        , EquityOnPendingSale
        , EquityOnSoldProperty
        , EquityOnSubjectProperty
        , ForgivableSecuredLoan
        , GiftFunds
        , Grant
        , HousingRelocation
        , LifeInsuranceCashValue
        , LotEquity
        , MortgageCreditCertificates
        , Other
        , OtherEquity
        , PledgedCollateral
        , PremiumFunds
        , RentWithOptionToPurchase
        , RetirementFunds
        , SaleOfChattel
        , SalesPriceAdjustment
        , SecondaryFinancing
        , SecuredLoan
        , StocksAndBonds
        , SweatEquity
        , TradeEquity
        , TrustFunds
        , UnsecuredBorrowedFunds
    }

    public enum E_ClosingCostSourceType
    {
        Undefined
        , Borrower
        , CommunityNonProfit
        , Employer
        , FederalAgency
        , LenderPremium
        , LocalAgency
        , MortgageRevenueBond
        , Other
        , PropertySeller
        , Relative
        , ReligiousNonProfit
        , StateAgency
    }

    static partial class EnumMappings
    {
        public static readonly string[] E_ClosingCostFundsType = {
                        ""
                         , "BridgeLoan"
                         , "CashOnHand"
                         , "CashOrOtherEquity"
                         , "CheckingSavings"
                         , "Contribution"
                         , "CreditCard"
                         , "DepositOnSalesContract"
                         , "EquityOnPendingSale"
                         , "EquityOnSoldProperty"
                         , "EquityOnSubjectProperty"
                         , "ForgivableSecuredLoan"
                         , "GiftFunds"
                         , "Grant"
                         , "HousingRelocation"
                         , "LifeInsuranceCashValue"
                         , "LotEquity"
                         , "MortgageCreditCertificates"
                         , "Other"
                         , "OtherEquity"
                         , "PledgedCollateral"
                         , "PremiumFunds"
                         , "RentWithOptionToPurchase"
                         , "RetirementFunds"
                         , "SaleOfChattel"
                         , "SalesPriceAdjustment"
                         , "SecondaryFinancing"
                         , "SecuredLoan"
                         , "StocksAndBonds"
                         , "SweatEquity"
                         , "TradeEquity"
                         , "TrustFunds"
                         , "UnsecuredBorrowedFunds"
                         };
        public static readonly string[] E_ClosingCostSourceType = {
                        ""
                         , "Borrower"
                         , "CommunityNonProfit"
                         , "Employer"
                         , "FederalAgency"
                         , "LenderPremium"
                         , "LocalAgency"
                         , "MortgageRevenueBond"
                         , "Other"
                         , "PropertySeller"
                         , "Relative"
                         , "ReligiousNonProfit"
                         , "StateAgency"
                         };
    }
}
