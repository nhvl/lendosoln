// Generated by CodeMonkey on 3/22/2010 6:06:40 PM
using System;
using System.Collections.Generic;
using System.Xml;
using XmlSerializableCommon;
namespace Mismo.Closing2_6
{
    public class EnergyImprovement : AbstractXmlSerializable
    {
        #region Private Member Variables
        private string m_Id = string.Empty;
        private E_EnergyImprovementType m_Type = E_EnergyImprovementType.Undefined;
        private string m_TypeOtherDescription = string.Empty;
        #endregion

        #region Public Properties
        public string Id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }
        public E_EnergyImprovementType Type
        {
            get { return m_Type; }
            set { m_Type = value; }
        }
        public string TypeOtherDescription
        {
            get { return m_TypeOtherDescription; }
            set { m_TypeOtherDescription = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "ENERGY_IMPROVEMENT")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_Id = ReadAttribute(reader, "_ID");
            m_Type = (E_EnergyImprovementType) ReadAttribute(reader, "_Type", EnumMappings.E_EnergyImprovementType);
            m_TypeOtherDescription = ReadAttribute(reader, "_TypeOtherDescription");
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "ENERGY_IMPROVEMENT") return;

            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("ENERGY_IMPROVEMENT");
            WriteAttribute(writer, "_ID", m_Id);
            WriteEnumAttribute(writer, "_Type", EnumMappings.E_EnergyImprovementType[(int) m_Type]);
            WriteAttribute(writer, "_TypeOtherDescription", m_TypeOtherDescription);
            writer.WriteEndElement();
        }
    }

    public enum E_EnergyImprovementType
    {
        Undefined
        , AdditionalNewFeature
        , InstallationSolar
        , InsulationSealant
        , Other
        , ReplacementMajorSystem
    }

    static partial class EnumMappings
    {
        public static readonly string[] E_EnergyImprovementType = {
                        ""
                         , "AdditionalNewFeature"
                         , "InstallationSolar"
                         , "InsulationSealant"
                         , "Other"
                         , "ReplacementMajorSystem"
                         };
    }
}
