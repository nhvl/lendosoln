﻿using System.Collections.Generic;
using System.Xml;

namespace ComplianceEagle.QuestsoftComplianceReportResponse
{
    public class UserInput : AbstractXmlSerializable
    {
        private List<Option> myOptionList = null;

        private const string userInputElementName = "UserInput";
        private const string sectionAttrName = "Section";
        private const string fieldAttrName = "Field";
        private const string optionalAttrName = "Optional";
        private const string questionAttrName = "Question";

        public string Section { get; set; }
        public string Field { get; set; }
        public string Optional { get; set; }
        public string Question { get; set; }

        public List<Option> OptionList
        {
            get
            {
                if (null == myOptionList)
                {
                    myOptionList = new List<Option>();
                }

                return myOptionList;
            }
        }

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != userInputElementName)
            {
                reader.Read();

                if (reader.EOF)
                {
                    return;
                }
            }

            this.ReadAttributes(reader);

            if (reader.IsEmptyElement)
            {
                return;
            }

            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == userInputElementName)
                {
                    return;
                }

                if (reader.NodeType != XmlNodeType.Element)
                {
                    continue;
                }

                this.ReadElements(reader);
            }
        }

        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement(userInputElementName);
            WriteAttribute(writer, sectionAttrName, this.Section);
            WriteAttribute(writer, fieldAttrName, this.Field);
            WriteAttribute(writer, optionalAttrName, this.Optional);
            WriteAttribute(writer, questionAttrName, this.Question);
            WriteElement(writer, this.OptionList);
            writer.WriteEndElement();
        }

        private void ReadAttributes(XmlReader reader)
        {
            this.Section = ReadAttribute(reader, sectionAttrName);
            this.Field = ReadAttribute(reader, fieldAttrName);
            this.Optional = ReadAttribute(reader, optionalAttrName);
            this.Question = ReadAttribute(reader, questionAttrName);
        }

        private void ReadElements(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "option":
                    ReadElement(reader, this.OptionList);
                    break;
            }
        }
    }
}