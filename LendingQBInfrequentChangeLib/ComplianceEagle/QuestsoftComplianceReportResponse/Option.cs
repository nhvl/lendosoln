﻿using System;
using System.Xml;

namespace ComplianceEagle.QuestsoftComplianceReportResponse
{
    public class Option : AbstractXmlSerializable
    {
        private const string optionElementName = "option";
        private const string selectedAttrName = "selected";
        private const string valueAttrName = "value";

        public string Selected { get; set; }
        public string ValueAttribute { get; set; }
        public string Content { get; set; }

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != optionElementName)
            {
                reader.Read();

                if (reader.EOF)
                {
                    return;
                }
            }

            this.ReadAttributes(reader);

            if (reader.IsEmptyElement)
            {
                return;
            }

            reader.Read();

            try
            {
                // 7/8/15 - reader.ReadElementContentAsString will move the cursor pass the end tag. The parent method is calling method of XmlReader.Read() again.
                // This cause invalid parsing when there are even number of Option element.
                this.Content = reader.ReadContentAsString();
                //this.Content = reader.ReadElementContentAsString();
            }
            catch (InvalidOperationException)
            { }
        }

        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement(optionElementName);
            WriteAttribute(writer, selectedAttrName, this.Selected);
            WriteAttribute(writer, valueAttrName, this.ValueAttribute);
            WriteString(writer, this.Content);
            writer.WriteEndElement();
        }

        private void ReadAttributes(XmlReader reader)
        {
            this.Selected = ReadAttribute(reader, selectedAttrName);
            this.ValueAttribute = ReadAttribute(reader, valueAttrName);
        }
    }
}