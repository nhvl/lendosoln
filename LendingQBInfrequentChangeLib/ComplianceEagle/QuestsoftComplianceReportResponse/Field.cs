﻿using System.Linq;
using System.Xml;

namespace ComplianceEagle.QuestsoftComplianceReportResponse
{
    public class Field : AbstractXmlSerializable
    {
        private const string fieldElementName = "Field";
        private const string nameAttrName = "Name";
        private const string valueAttrName = "Value";

        /// <summary>
        /// A list of field names known to contain PDF data.
        /// </summary>
        private static readonly string[] fieldsContainingPdfs =
        {
            "QMReview.PDF",
            "Cert.PDF"
        };

        /// <summary>
        /// Gets or sets a value indicating whether to write fields containing PDFs.
        /// </summary>
        public bool DoNotWritePdfFields { get; set; }

        public string Name { get; set; }

        public string Value { get; set; }

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != fieldElementName)
            {
                reader.Read();

                if (reader.EOF)
                {
                    return;
                }
            }

            this.ReadAttributes(reader);

            if (reader.IsEmptyElement)
            {
                return;
            }

            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == fieldElementName)
                {
                    return;
                }
            }
        }

        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement(fieldElementName);
            WriteAttribute(writer, nameAttrName, this.Name);

            if (this.DoNotWritePdfFields && fieldsContainingPdfs.Contains(this.Name))
            {
                var redactedMessage = "[PDF value redacted to save memory]";
                WriteAttributeAllowEmpty(writer, valueAttrName, redactedMessage);
            }
            else
            {
                WriteAttributeAllowEmpty(writer, valueAttrName, this.Value);
            }

            writer.WriteEndElement();
        }

        private void ReadAttributes(XmlReader reader)
        {
            this.Name = ReadAttribute(reader, nameAttrName);
            this.Value = ReadAttribute(reader, valueAttrName);
        }
    }
}