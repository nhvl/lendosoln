﻿using System.Xml;

namespace ComplianceEagle.QuestsoftComplianceReportResponse
{
    public class PDF : AbstractXmlSerializable
    {
        private const string pdfElementName = "PDF";

        public string CDataContents { get; set; }

        public bool DoNotExportCDataContents { get; set; }

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != pdfElementName)
            {
                reader.Read();

                if (reader.EOF)
                {
                    return;
                }
            }

            if (reader.IsEmptyElement)
            {
                return;
            }

            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == pdfElementName)
                {
                    return;
                }

                if (reader.NodeType != XmlNodeType.CDATA)
                {
                    continue;
                }

                this.ReadCData(reader);
            }
        }

        public override void WriteXml(XmlWriter writer)
        {
            if (!string.IsNullOrEmpty(this.CDataContents))
            {
                writer.WriteStartElement(pdfElementName);
                if (this.DoNotExportCDataContents)
                {
                    writer.WriteCData("CData block with base64 Encoded file data redacted to save memory.");
                }
                else
                {
                    writer.WriteCData(this.CDataContents);
                }
                writer.WriteEndElement();
            }
        }

        private void ReadCData(XmlReader reader)
        {
            this.CDataContents = reader.Value;
        }
    }
}
