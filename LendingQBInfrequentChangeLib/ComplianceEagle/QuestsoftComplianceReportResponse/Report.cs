﻿namespace ComplianceEagle.QuestsoftComplianceReportResponse
{
    using System.Linq;
    using System.Xml;

    public class Report : AbstractXmlSerializable
    {
        private PDF myPDF = null;
        private Data myData = null;
        private E_CEReportServiceEnum myService = E_CEReportServiceEnum.Undefined;
        private E_CEReportStatusEnum myStatus = E_CEReportStatusEnum.Undefined;
        private E_CEReportContentsEnum myContents = E_CEReportContentsEnum.Undefined;
        
        private const string reportElementName = "Report";
        private const string idAttrName = "ID";
        private const string ceIDAttrName = "CEID";
        private const string vendorTransactionIDAttrName = "VendorTransactionID";
        private const string serviceAttrName = "Service";
        private const string performedAttrName = "Performed";
        private const string statusAttrName = "Status";
        private const string statusExtAttrName = "StatusExt";
        private const string contentsAttrName = "Contents";
        private const string urlAttrName = "URL";
        private const string urlRPTAttrName = "URLRPT";

        public PDF PDF
        {
            get
            {
                if (null == myPDF)
                {
                    myPDF = new PDF();
                }

                return myPDF;
            }
            set { myPDF = value; }
        }

        public Data Data
        {
            get
            {
                if (null == myData)
                {
                    myData = new Data();
                }

                return myData;
            }
            set { myData = value; }
        }

        public string ID { get; set; }

        public string CEID { get; set; }

        public string VendorTransactionID { get; set; }

        public E_CEReportServiceEnum Service
        {
            get { return myService; }
            set { myService = value; }
        }

        public string Performed { get; set; }

        public E_CEReportStatusEnum Status
        {
            get { return myStatus; }
            set { myStatus = value; }
        }

        public string StatusExt { get; set; }

        public E_CEReportContentsEnum Contents
        {
            get { return myContents; }
            set { myContents = value; }
        }

        public string URL { get; set; }

        public string URLRPT { get; set; }

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != reportElementName)
            {
                reader.Read();

                if (reader.EOF)
                {
                    return;
                }
            }

            this.ReadAttributes(reader);

            if (reader.IsEmptyElement)
            {
                return;
            }

            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == reportElementName)
                {
                    return;
                }

                if (reader.NodeType != XmlNodeType.Element)
                {
                    continue;
                }

                this.ReadElements(reader);
            }
        }

        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement(reportElementName);
            WriteAttribute(writer, idAttrName, this.ID);
            WriteAttribute(writer, ceIDAttrName, this.CEID);
            WriteAttribute(writer, vendorTransactionIDAttrName, this.VendorTransactionID);
            WriteAttribute(writer, serviceAttrName, EnumMappings.E_CEReportServiceEnum[(int)this.Service]);
            WriteAttribute(writer, performedAttrName, this.Performed);
            WriteAttribute(writer, statusAttrName, EnumMappings.E_CEReportStatusEnum[(int)this.Status]);
            WriteAttribute(writer, statusExtAttrName, this.StatusExt);
            WriteAttribute(writer, contentsAttrName, EnumMappings.E_CEReportContentsEnum[(int)this.Contents]);
            WriteAttribute(writer, urlAttrName, this.URL);
            WriteAttribute(writer, urlRPTAttrName, this.URLRPT);
            WriteElement(writer, this.PDF);
            WriteElement(writer, this.Data);
            writer.WriteEndElement();
        }

        private void ReadAttributes(XmlReader reader)
        {
            this.ID = ReadAttribute(reader, idAttrName);
            this.CEID = ReadAttribute(reader, ceIDAttrName);
            this.VendorTransactionID = ReadAttribute(reader, vendorTransactionIDAttrName);
            this.Service = (E_CEReportServiceEnum)ReadAttribute(reader, serviceAttrName, EnumMappings.E_CEReportServiceEnum);
            this.Performed = ReadAttribute(reader, performedAttrName);
            this.Status = (E_CEReportStatusEnum)ReadAttribute(reader, statusAttrName, EnumMappings.E_CEReportStatusEnum);
            this.StatusExt = ReadAttribute(reader, statusExtAttrName);
            this.Contents = (E_CEReportContentsEnum)ReadAttribute(reader, contentsAttrName, EnumMappings.E_CEReportContentsEnum);
            this.URL = ReadAttribute(reader, urlAttrName);
            this.URLRPT = ReadAttribute(reader, urlRPTAttrName);
        }

        private void ReadElements(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "PDF":
                    ReadElement(reader, this.PDF);
                    break;
                case "Data":
                    ReadElement(reader, this.Data);
                    break;
            }
        }
    }
}