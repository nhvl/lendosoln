﻿using System.Collections.Generic;
using System.Linq;
using System.Xml;

namespace ComplianceEagle.QuestsoftComplianceReportResponse
{
    public class CEReports : AbstractXmlSerializable
    {
        private List<Report> myReportList = null;
        private CEError myError = null;

        private const string CEReportsElementName = "CEReports";

        public List<Report> ReportList
        {
            get
            {
                if (null == myReportList)
                {
                    myReportList = new List<Report>();
                }

                return myReportList;
            }
        }

        public CEError Error
        {
            get
            {
                if (null == myError)
                {
                    myError = new CEError();
                }

                return myError;
            }
            set { myError = value; }
        }

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != CEReportsElementName)
            {
                reader.Read();

                if (reader.EOF)
                {
                    return;
                }
            }

            if (reader.IsEmptyElement)
            {
                return;
            }

            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == CEReportsElementName)
                {
                    return;
                }

                if (reader.NodeType != XmlNodeType.Element)
                {
                    continue;
                }

                this.ReadElements(reader);
            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartDocument();
            writer.WriteStartElement(CEReportsElementName);
            WriteElement(writer, this.Error);
            WriteElement(writer, this.ReportList);
            writer.WriteEndElement();
            writer.WriteEndDocument();
        }

        /// <summary>
        /// Do not write CData section of PDF element.
        /// </summary>
        /// <param name="writer"></param>
        public void WriteXmlStripOutCData(XmlWriter writer)
        {
            foreach (var report in this.ReportList)
            {
                if (report.PDF != null)
                {
                    report.PDF.DoNotExportCDataContents = true;
                }

                var fieldList = report?.Data?.FieldList ?? Enumerable.Empty<Field>();
                foreach (var field in fieldList)
                {
                    field.DoNotWritePdfFields = true;
                }
            }

            this.WriteXml(writer);

            // Switch back to original value.
            foreach (var report in this.ReportList)
            {
                if (report.PDF != null)
                {
                    report.PDF.DoNotExportCDataContents = false;
                }

                var fieldList = report?.Data?.FieldList ?? Enumerable.Empty<Field>();
                foreach (var field in fieldList)
                {
                    field.DoNotWritePdfFields = false;
                }
            }
        }

        private void ReadElements(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "Error":
                    ReadElement(reader, this.Error);
                    break;
                case "Report":
                    ReadElement(reader, this.ReportList);
                    break;
            }
        }
    }
}
