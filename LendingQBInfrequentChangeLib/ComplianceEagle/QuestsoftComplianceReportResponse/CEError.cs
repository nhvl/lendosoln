﻿using System.Xml;

namespace ComplianceEagle.QuestsoftComplianceReportResponse
{
    public class CEError : AbstractXmlSerializable
    {
        private const string errorElementName = "Error";

        public string ErrorMessage { get; set; }

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != errorElementName)
            {
                reader.Read();

                if (reader.EOF)
                {
                    return;
                }
            }

            if (reader.IsEmptyElement)
            {
                return;
            }

            this.ErrorMessage = reader.ReadElementContentAsString();
        }

        public override void WriteXml(XmlWriter writer)
        {
            if (!string.IsNullOrEmpty(this.ErrorMessage))
            {
                writer.WriteStartElement(errorElementName);
                WriteString(writer, this.ErrorMessage);
                writer.WriteEndElement();
            }
        }
    }
}