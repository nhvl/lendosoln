﻿using System.Collections.Generic;
using System.Xml;

namespace ComplianceEagle.QuestsoftComplianceReportResponse
{
    public class Data : AbstractXmlSerializable
    {
        private List<Field> myFieldList = null;
        private List<UserInput> myUserInputList = null;
        private List<CEError> myErrorList = null;

        private const string dataElementName = "Data";

        public List<Field> FieldList
        {
            get
            {
                if (null == myFieldList)
                {
                    myFieldList = new List<Field>();
                }

                return myFieldList;
            }
        }

        public List<UserInput> UserInputList
        {
            get
            {
                if (null == myUserInputList)
                {
                    myUserInputList = new List<UserInput>();
                }

                return myUserInputList;
            }
        }

        public List<CEError> ErrorList
        {
            get
            {
                if (null == myErrorList)
                {
                    myErrorList = new List<CEError>();
                }

                return myErrorList;
            }
        }

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != dataElementName)
            {
                reader.Read();

                if (reader.EOF)
                {
                    return;
                }
            }

            if (reader.IsEmptyElement)
            {
                return;
            }

            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == dataElementName)
                {
                    return;
                }

                if (reader.NodeType != XmlNodeType.Element)
                {
                    continue;
                }

                this.ReadElements(reader);
            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement(dataElementName);
            WriteElement(writer, this.FieldList);
            WriteElement(writer, this.UserInputList);
            WriteElement(writer, this.ErrorList);
            writer.WriteEndElement();
        }

        private void ReadElements(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "Error":
                    ReadElement(reader, this.ErrorList);
                    break;
                case "Field":
                    ReadElement(reader, this.FieldList);
                    break;
                case "UserInput":
                    ReadElement(reader, this.UserInputList);
                    break;
            }
        }
    }
}