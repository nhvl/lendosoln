﻿using System;
using System.Collections.Generic;
using System.Xml;

namespace ComplianceEagle.QuestsoftComplianceReportRequest
{
    public class LoanData : AbstractXmlSerializable
    {
        private LOAN_PLACEHOLDER myLoanPlaceholder = null;
        private List<KVPair> myKeyValuePairList = null;
        private List<Borrower> myBorrowerList = null;

        private const string loanDataElementName = "LoanData";

        private static string[] myExtraFieldList =
        {
            "GetAgentofRole[Broker].AgentName",
            "GetAgentofRole[FloodProvider].AgentName",
            "GetAgentofRole[FloodProvider].CompanyName",
            "GetAgentOfRole[HazardInsurance].AgentName",
            "GetAgentofRole[ListingAgent].AgentName",
            "GetAgentofRole[LoanOpener].AgentName",
            "GetAgentofRole[LoanOpener].CompanyName",
            "GetAgentofRole[Processor].AgentName",
            "GetAgentofRole[SellingAgent].AgentName",
            "GetAgentofRole[Trustee].AgentName",
            "GetAgentofRole[Trustee].CompanyName",
            "GetPreparerOfForm[FHA92900LtUnderwriter].LicenseNumOfAgent",
            "sApp1003InterviewerLoanOriginatorIdentifier",
            "sArmIndexEffectiveD",
            "sCaseAssignmentD",
            "sChumsIdReviewerAppraisal",
            "sCltvR",
            "sConcurSubFin",
            "sConstructionPeriodIR",
            "sConstructionPeriodMon",
            "sCreditLineAmt",
            "sCreditScoreLpeQual",
            "sDailyPeriodicR",
            "sDaysInYr",
            "sDocMagicApplicationD",
            "sEmployeeLoanRepName",
            "sEmployeeProcessorName",
            "sFfUfMipIsBeingFinanced",
            "sFfUfmipR",
            "sFHAHousingActSection",
            "sFHARiskClassAA",
            "sFHARiskClassRefer",
            "sFHAScoreByTotalTri",
            "sFHASpAfterImprovedVal110pc",
            "sFHASpAsIsVal",
            "sFinMethT",
            "sGfeDiscountPointFProps_BF",
            "sGfeInitialDisclosureD",
            "sGfeLenderCreditFProps_Apr",
            "sGfeRedisclosureD",
            "sHelocAnnualFee",
            "sHelocDrawFee",
            "sHelocMinimumAdvanceAmt",
            "sHmda2018AprRateSpread",
            "sHmda2018CensusTract",
            "sHmda2018CountyCode",
            "sHmdaActionD",
            "sHmdaActionTaken",
            "sHmdaActionTakenT",
            "sHmdaApplicationDate",
            "sHmdaAutomatedUnderwritingSystem1T",
            "sHmdaAutomatedUnderwritingSystem2T",
            "sHmdaAutomatedUnderwritingSystem3T",
            "sHmdaAutomatedUnderwritingSystem4T",
            "sHmdaAutomatedUnderwritingSystem5T",
            "sHmdaAutomatedUnderwritingSystemOtherDescription",
            "sHmdaAutomatedUnderwritingSystemResult1T",
            "sHmdaAutomatedUnderwritingSystemResult2T",
            "sHmdaAutomatedUnderwritingSystemResult3T",
            "sHmdaAutomatedUnderwritingSystemResult4T",
            "sHmdaAutomatedUnderwritingSystemResult5T",
            "sHmdaAutomatedUnderwritingSystemResultOtherDescription",
            "sHmdaBAge",
            "sHmdaBalloonPaymentT",
            "sHmdaBCreditScore",
            "sHmdaBCreditScoreModelOtherDescription",
            "sHmdaBCreditScoreModelT",
            "sHmdaBEnrolledOrPrincipalTribe",
            "sHmdaBEthnicity1T",
            "sHmdaBEthnicity2T",
            "sHmdaBEthnicity3T",
            "sHmdaBEthnicity4T",
            "sHmdaBEthnicity5T",
            "sHmdaBEthnicityCollectedByObservationOrSurname",
            "sHmdaBEthnicityOtherDescription",
            "sHmdaBOtherAsianDescription",
            "sHmdaBOtherPacificIslanderDescription",
            "sHmdaBRace1T",
            "sHmdaBRace2T",
            "sHmdaBRace3T",
            "sHmdaBRace4T",
            "sHmdaBRace5T",
            "sHmdaBRaceCollectedByObservationOrSurname",
            "sHmdaBSexCollectedByObservationOrSurname",
            "sHmdaBSexT",
            "sHmdaBusinessPurposeT",
            "sHmdaCAge",
            "sHmdaCCreditScore",
            "sHmdaCCreditScoreModelOtherDescription",
            "sHmdaCCreditScoreModelT",
            "sHmdaCEnrolledOrPrincipalTribe",
            "sHmdaCensusTract",
            "sHmdaCEthnicity1T",
            "sHmdaCEthnicity2T",
            "sHmdaCEthnicity3T",
            "sHmdaCEthnicity4T",
            "sHmdaCEthnicity5T",
            "sHmdaCEthnicityCollectedByObservationOrSurname",
            "sHmdaCEthnicityOtherDescription",
            "sHmdaCombinedRatio",
            "sHmdaConstructionMethT",
            "sHmdaCOtherAsianDescription",
            "sHmdaCOtherPacificIslanderDescription",
            "sHmdaCountyCode",
            "sHmdaCRace1T",
            "sHmdaCRace2T",
            "sHmdaCRace3T",
            "sHmdaCRace4T",
            "sHmdaCRace5T",
            "sHmdaCRaceCollectedByObservationOrSurname",
            "sHmdaCSexCollectedByObservationOrSurname",
            "sHmdaCSexT",
            "sHmdaDebtRatio",
            "sHmdaDenialReason1",
            "sHmdaDenialReason2",
            "sHmdaDenialReason3",
            "sHmdaDenialReasonOtherDescription",
            "sHmdaDenialReasonT1",
            "sHmdaDenialReasonT2",
            "sHmdaDenialReasonT3",
            "sHmdaDenialReasonT4",
            "sHmdaDeniedFormDoneD",
            "sHmdaDiscountPoints",
            "sHmdaExcludedFromReport",
            "sHmdaHoepaStatusT",
            "sHmdaIncome",
            "sHmdaInitiallyPayableToInstitutionT",
            "sHmdaInterestOnlyPaymentT",
            "sHmdaInterestRate",
            "sHmdaIntroductoryPeriod",
            "sHmdaIsCombinedRatioReliedOn",
            "sHmdaIsCreditScoreReliedOn",
            "sHmdaIsDebtRatioReliedOn",
            "sHmdaIsIncomeReliedOn",
            "sHmdaIsPropertyValueReliedOn",
            "sHmdaLenderCredits",
            "sHmdaLienT",
            "sHmdaLoanAmount",
            "sHmdaLoanPurposeT",
            "sHmdaLoanTypeT",
            "sHmdaManufacturedInterestT",
            "sHmdaManufacturedTypeT",
            "sHmdaMultifamilyUnits",
            "sHmdaNegativeAmortizationT",
            "sHmdaOriginationCharge",
            "sHmdaOtherNonAmortFeatureT",
            "sHmdaPreapprovalT",
            "sHmdaPrepaymentTerm",
            "sHmdaPropertyValue",
            "sHmdaPropT",
            "sHmdaPurchaser2004T",
            "sHmdaPurchaser2015T",
            "sHmdaReverseMortgageT",
            "sHmdaSpAddr",
            "sHmdaSpCity",
            "sHmdaSpState",
            "sHmdaSpZip",
            "sHmdaStateCode",
            "sHmdaSubmissionApplicationT",
            "sHmdaTerm",
            "sHmdaTotalLoanCosts",
            "sHomeownerCounselingOrganizationDisclosureD",
            "sHomeownerCounselingOrganizationLastModifiedD",
            "sInitAPR",
            "sIntentToProceedD",
            "sIsDuUw",
            "sIsLineOfCredit",
            "sIsLpUw",
            "sIsOptionArm",
            "sLAmtCalc",
            "sLastDiscAPR",
            "sLegalEntityIdentifier",
            "sLpTemplateNm",
            "sLT",
            "sMInsRsrvMon",
            "sOccT",
            "sOptionArmInitialFixMinPmtPeriod",
            "sOptionArmMinPayPeriod",
            "sOptionArmPmtDiscount",
            "sOptionArmTeaserR",
            "sOriginatorCompensationPaymentSourceT",
            "sOriginatorCompensationTotalAmount",
            "sPmtAdjCapMon",
            "sPrepmtPenaltyT",
            "sProdIsSpInRuralArea",
            "sProMInsMidptCancel",
            "sProMInsMon",
            "sProMInsR",
            "sQMLoanPassedComplianceChecks",
            "sQMLoanPurchaseAgency",
            "sQMParR",
            "sQMStatusT",
            "sReliedOnCombinedLTVRatio",
            "sReliedOnCreditScoreModeT",
            "sReliedOnDebtRatio",
            "sReliedOnPropertyValue",
            "sReliedOnTotalIncome",
            "sRLckdD",
            "sSchedPmtTot",
            "sSpAge",
            "sSpBathCount",
            "sSpProjectAttachmentT",
            "sSpValuationEffectiveD",
            "sSpValuationMethodT",
            "sStatusD",
            "sStatusT",
            "sSubFin",
            "sTilGfeRd",
            "sTilInitialDisclosureD",
            "sTilRedisclosureD",
            "sTilRedisclosuresReceivedD",
            "sUfmipIsRefundableOnProRataBasis",
            "sUnitsNum",
            "sUniversalLoanIdentifier",
            "sVaRefiWsCashPmtFromVet",
            "sWindInsCompanyNm",
        };

        public LOAN_PLACEHOLDER LoanPlaceholder
        {
            get
            {
                if (null == myLoanPlaceholder)
                {
                    myLoanPlaceholder = new LOAN_PLACEHOLDER();
                }

                return myLoanPlaceholder;
            }
            set { myLoanPlaceholder = value; }
        }

        public List<KVPair> KeyValuePairList
        {
            get
            {
                if (null == myKeyValuePairList)
                {
                    myKeyValuePairList = new List<KVPair>();
                }

                return myKeyValuePairList;
            }
        }

        public List<Borrower> BorrowerList
        {
            get
            {
                if (null == myBorrowerList)
                {
                    myBorrowerList = new List<Borrower>();
                }

                return myBorrowerList;
            }
        }

        public string[] ExtraFieldList
        {
            get
            {
                return myExtraFieldList;
            }
        }

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != loanDataElementName)
            {
                reader.Read();

                if (reader.EOF)
                {
                    return;
                }
            }

            if (reader.IsEmptyElement)
            {
                return;
            }

            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == loanDataElementName)
                {
                    return;
                }

                if (reader.NodeType != XmlNodeType.Element)
                {
                    continue;
                }

                this.ReadElements(reader);
            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement(loanDataElementName);
            WriteElement(writer, this.LoanPlaceholder);
            WriteElement(writer, this.KeyValuePairList);
            WriteElement(writer, this.BorrowerList);
            writer.WriteEndElement();
        }

        private void ReadElements(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "LOAN":
                    ReadElement(reader, this.LoanPlaceholder);
                    break;
                case "KEY":
                    ReadElement(reader, this.KeyValuePairList);
                    break;
                case "BORROWER":
                    ReadElement(reader, this.BorrowerList);
                    break;
                default:
                    throw new Exception("Unhandled " + reader.Name);
            }
        }
    }
}
