﻿using System.Xml;

namespace ComplianceEagle.QuestsoftComplianceReportRequest
{
    public class KVPair : AbstractXmlSerializable
    {
        private const string keyElementName = "KEY";
        private const string nameAttrName = "_Name";
        private const string valueAttrName = "_Value";

        public string Name { get; set; }

        public string Value { get; set; }

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != keyElementName)
            {
                reader.Read();

                if (reader.EOF)
                {
                    return;
                }
            }

            this.ReadAttributes(reader);

            if (reader.IsEmptyElement)
            {
                return;
            }

            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == keyElementName)
                {
                    return;
                }
            }
        }

        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement(keyElementName);
            WriteAttribute(writer, nameAttrName, this.Name);
            WriteAttributeAllowEmpty(writer, valueAttrName, this.Value);
            writer.WriteEndElement();
        }

        private void ReadAttributes(XmlReader reader)
        {
            this.Name = ReadAttribute(reader, nameAttrName);
            this.Value = ReadAttribute(reader, valueAttrName);
        }
    }
}
