﻿using System;
using System.Collections.Generic;
using System.Xml;

namespace ComplianceEagle.QuestsoftComplianceReportRequest
{
    public class Borrower : AbstractXmlSerializable
    {
        private List<KVPair> myKeyValuePairList = null;

        private const string borrowerElementName = "BORROWER";
        private const string borrowerIDAttrName = "BorrowerID";

        private static string[] myextraFieldList = 
        { 
            "aMortgageLates30In12MonthsPmlClientReport",
            "aSpNegCf",
            "aTotI",
            "aVaNetEffectiveI",
        };

        public string BorrowerID { get; set; }

        public List<KVPair> KeyValuePairList
        {
            get
            {
                if (null == myKeyValuePairList)
                {
                    myKeyValuePairList = new List<KVPair>();
                }

                return myKeyValuePairList;
            }
        }

        public string[] ExtraFieldList 
        {
            get
            {
                return myextraFieldList;
            }
        }

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != borrowerElementName)
            {
                reader.Read();

                if (reader.EOF)
                {
                    return;
                }
            }

            this.ReadAttributes(reader);

            if (reader.IsEmptyElement)
            {
                return;
            }

            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == borrowerElementName)
                {
                    return;
                }

                if (reader.NodeType != XmlNodeType.Element)
                {
                    continue;
                }

                this.ReadElements(reader);
            }
        }

        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement(borrowerElementName);
            WriteAttribute(writer, borrowerIDAttrName, this.BorrowerID);
            WriteElement(writer, this.KeyValuePairList);
            writer.WriteEndElement();
        }

        private void ReadAttributes(XmlReader reader)
        {
            this.BorrowerID = ReadAttribute(reader, borrowerIDAttrName);
        }

        private void ReadElements(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "KEY":
                    ReadElement(reader, this.KeyValuePairList);
                    break;
                default:
                    throw new Exception("Unhandled " + reader.Name);
            }
        }
    }
}
