﻿using System.Xml;

namespace ComplianceEagle.QuestsoftComplianceReportRequest
{
    public class Authentication : AbstractXmlSerializable
    {
        private const string authenticationElementName = "Authentication";
        private const string versionAttrName = "Version";
        private const string integratorIDAttrName = "IntegratorID";
        private const string customerIDAttrName = "CustomerID";
        private const string usernameAttrName = "Username";
        private const string passwordAttrName = "Password";

        public string Version { get; set; }
        public string IntegratorID { get; set; }
        public string CustomerID { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != authenticationElementName)
            {
                reader.Read();
                
                if (reader.EOF)
                {
                    return;
                }
            }

            this.ReadAttributes(reader);

            if (reader.IsEmptyElement)
            {
                return;
            }

            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == authenticationElementName)
                {
                    return;
                }

                if (reader.NodeType != XmlNodeType.Element)
                {
                    continue;
                }
            }
        }

        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement(authenticationElementName);
            WriteAttribute(writer, versionAttrName, this.Version);
            WriteAttribute(writer, integratorIDAttrName, this.IntegratorID);
            WriteAttribute(writer, customerIDAttrName, this.CustomerID);
            WriteAttribute(writer, usernameAttrName, this.Username);
            WriteAttribute(writer, passwordAttrName, this.Password);
            writer.WriteEndElement();
        }

        private void ReadAttributes(XmlReader reader)
        {
            this.Version = ReadAttribute(reader, versionAttrName);
            this.IntegratorID = ReadAttribute(reader, integratorIDAttrName);
            this.CustomerID = ReadAttribute(reader, customerIDAttrName);
            this.Username = ReadAttribute(reader, usernameAttrName);
            this.Password = ReadAttribute(reader, passwordAttrName);
        }
    }
}