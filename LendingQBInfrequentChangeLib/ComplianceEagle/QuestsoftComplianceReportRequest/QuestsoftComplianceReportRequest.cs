﻿using System;
using System.Xml;

namespace ComplianceEagle.QuestsoftComplianceReportRequest
{
    public class QuestsoftComplianceReportRequest : AbstractXmlSerializable
    {
        private Authentication myAuthentication = null;
        private General myGeneral = null;
        private LoanData myLoanData = null;

        private const string QSComplianceRequestElementName = "QuestsoftComplianceReportRequest";

        public Authentication Authentication
        {
            get
            {
                if (null == myAuthentication)
                {
                    myAuthentication = new Authentication();
                }

                return myAuthentication;
            }
            set { myAuthentication = value; }
        }

        public General General
        {
            get
            {
                if (null == myGeneral)
                {
                    myGeneral = new General();
                }

                return myGeneral;
            }
            set { myGeneral = value; }
        }

        public LoanData LoanData
        {
            get
            {
                if (null == myLoanData)
                {
                    myLoanData = new LoanData();
                }

                return myLoanData;
            }
            set { myLoanData = value; }
        }

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != QSComplianceRequestElementName)
            {
                reader.Read();

                if (reader.EOF)
                {
                    return;
                }
            }

            if (reader.IsEmptyElement)
            {
                return;
            }

            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == QSComplianceRequestElementName)
                {
                    return;
                }

                if (reader.NodeType != XmlNodeType.Element)
                {
                    continue;
                }

                this.ReadElements(reader);
            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartDocument();
            writer.WriteStartElement(QSComplianceRequestElementName);
            WriteElement(writer, this.Authentication);
            WriteElement(writer, this.General);
            WriteElement(writer, this.LoanData);
            writer.WriteEndElement();
            writer.WriteEndDocument();
        }

        private void ReadElements(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "Authentication":
                    ReadElement(reader, this.Authentication);
                    break;
                case "General":
                    ReadElement(reader, this.General);
                    break;
                case "LoanData":
                    ReadElement(reader, this.LoanData);
                    break;
                default:
                    throw new Exception("Unhandled " + reader.Name);
            }
        }
    }
}
