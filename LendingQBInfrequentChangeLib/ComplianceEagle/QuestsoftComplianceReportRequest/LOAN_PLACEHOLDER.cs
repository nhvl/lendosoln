﻿using System.Xml;

namespace ComplianceEagle.QuestsoftComplianceReportRequest
{
    public class LOAN_PLACEHOLDER : AbstractXmlSerializable
    {
        private const string placeholderElementName = "LOAN_PLACEHOLDER";

        public string Content { get; set; }

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != placeholderElementName)
            {
                reader.Read();
                if (reader.EOF)
                {
                    return;
                }
            }

            if (reader.IsEmptyElement)
            {
                return;
            }

            this.Content = reader.ReadInnerXml();
        }

        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement(placeholderElementName);
            WriteString(writer, this.Content);
            writer.WriteEndElement();
        }
    }
}
