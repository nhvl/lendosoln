﻿namespace ComplianceEagle.QuestsoftComplianceReportRequest
{
    using System.Xml;

    public class General : AbstractXmlSerializable
    {
        private E_RequestMethod myRequestMethod = E_RequestMethod.Undefined;
        private E_LoanStatusCode myLoanStatusCode = E_LoanStatusCode.Undefined;

        private const string generalElementName = "General";
        private const string requestMethodAttrName = "RequestMethod";
        private const string requestCodeAttrName = "RequestCode";
        private const string vendorTransactionIdAttrName = "VendorTransactionID";
        private const string loanIDAttrName = "LoanID";
        private const string branchCodeAttrName = "BranchCode";
        private const string loanStatusCodeAttrName = "LoanStatusCode";
        private const string loanStatusDateAttrName = "LoanStatusDate";

        public E_RequestMethod RequestMethod
        {
            get { return myRequestMethod; }
            set { myRequestMethod = value; }
        }

        public string RequestCode { get; set; }

        public string VendorTransactionId { get; set; }

        public string LoanID { get; set; }

        public string BranchCode { get; set; }

        public E_LoanStatusCode LoanStatusCode
        {
            get { return myLoanStatusCode; }
            set { myLoanStatusCode = value; }
        }

        public string LoanStatusDate { get; set; }

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != generalElementName)
            {
                reader.Read();

                if (reader.EOF)
                {
                    return;
                }
            }

            this.ReadAttributes(reader);

            if (reader.IsEmptyElement)
            {
                return;
            }

            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == generalElementName)
                {
                    return;
                }
            }
        }

        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement(generalElementName);
            WriteAttribute(writer, requestMethodAttrName, EnumMappings.E_RequestMethod[(int)this.RequestMethod]);
            WriteAttribute(writer, requestCodeAttrName, this.RequestCode);
            WriteAttribute(writer, vendorTransactionIdAttrName, this.VendorTransactionId);
            WriteAttribute(writer, loanIDAttrName, this.LoanID);
            WriteAttribute(writer, branchCodeAttrName, this.BranchCode);
            WriteAttribute(writer, loanStatusCodeAttrName, EnumMappings.E_LoanStatusCode[(int)this.LoanStatusCode]);
            WriteAttribute(writer, loanStatusDateAttrName, this.LoanStatusDate);
            writer.WriteEndElement();
        }

        private void ReadAttributes(XmlReader reader)
        {
            this.RequestMethod = (E_RequestMethod)ReadAttribute(reader, requestMethodAttrName, EnumMappings.E_RequestMethod);
            this.RequestCode = ReadAttribute(reader, requestCodeAttrName);
            this.VendorTransactionId = ReadAttribute(reader, vendorTransactionIdAttrName);
            this.LoanID = ReadAttribute(reader, loanIDAttrName);
            this.BranchCode = ReadAttribute(reader, branchCodeAttrName);
            this.LoanStatusCode = (E_LoanStatusCode)ReadAttribute(reader, loanStatusCodeAttrName, EnumMappings.E_LoanStatusCode);
            this.LoanStatusDate = ReadAttribute(reader, loanStatusDateAttrName);
        }
    }
}
