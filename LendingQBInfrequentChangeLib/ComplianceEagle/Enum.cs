﻿namespace ComplianceEagle
{
    using System.Collections.Generic;
    using System.Xml.Serialization;

    public enum E_RequestMethod
    {
        Undefined,
        HTMLPage,   //Full HTML Page - Browser Use (Compliance EAGLE Website)
        HTML,       //Simple HTML Page Report
        HTMLReport, //Simple HTML Report (Results of Transaction)
        HTMLXML,    //Simple HTML Report (Results of Transaction) - Embedded in XML
        PDFReport,  //Simple HTML Report is returned as an embedded PDF document 
        PDFXML,     //Simple HTML Report returned as embedded PDF including XML results
        XMLResults  //XML Results Only
    }

    public enum E_LoanStatusCode
    {
        Undefined,
        Active,                 //The Final Disposition has not been Assigned for this Application
        IncompleteFile,         //Incomplete application.  {Faxed Lock, Duplicate Loan, etc..}
        Closed,                 //The Loan has reached a closed status
        Denied,                 //The loan application was denied by the Lender
        Cancelled,              //The loan application was Cancelled 
        Withdrawn,              //The loan application was withdrawn by the applicant prior to Approval
        WithdrawnAfterApproval  //The loan application was withdrawn by the applicant after Approval
    }

    /// <summary>
    /// A list of services offered by ComplianceEagle.
    /// </summary>
    public enum E_CEReportServiceEnum
    {
        Undefined = 0,
        HMDACheck = 1,
        CRAReview = 2,
        GEOCODE = 3,
        RateSpreadCheck = 4,
        HighCost = 5,
        MaventReview = 6,
        MaventSummary = 7,
        MaventTILADetail = 8,
        MaventOFACSSNDetail = 9,
        MaventLicenseDetail = 10,
        MaventGSEDetail = 11,
        MaventAdditionalDetail = 12,
        MaventRESPADetail = 13,
        MaventQMATRDetail = 14,
        Flood = 15,
        FloodQuickCheck = 16,
        RecordingFee = 17,
        ServiceProviderSearch = 18,
        LEFCheck = 19,
        ExclusionaryList = 20,
        FraudBorrower = 21,
        FraudCollateral = 22,
        FraudComplete = 23,
        MaventNMLS = 24,
        IncomeReview = 25,
        SSNReview = 26,
        QMReview = 27,
        AvailableServices = 28,
        MaventRESPATILADetail = 29,
        AVMReview = 30,
        FHATOTALScorecardReview = 31,
        ComplianceSummaryReport = 32,
        NMLSMCRReview = 33,
        AUSReview = 34
    }

    public enum E_CEReportStatusEnum
    {
        Undefined,
        Pass,
        PASS,
        Fail,
        FAIL,
        FailException,
        FAILEXCEPTION,
        Alert,
        ALERT,
        Warning,
        WARNING,
        Review,
        REVIEW,
        UserInput,
        USERINPUT,
        Delayed,
        DELAYED
    }

    public enum E_CEReportContentsEnum
    {
        Undefined,
        HTMLPage,
        HTML,
        HTMLReport,
        HTMLXML,
        PDFReport,
        PDFXML,
        XMLResults
    }

    /// <summary>
    /// Categories for changes of circumstance. Some are taken from the Code of Federal Regulations 1026.
    /// </summary>
    public enum CEagleChangedCircumstanceCategory
    {
        /// <summary>
        /// Left blank.
        /// </summary>
        [XmlEnum("")]
        Blank,

        /// <summary>
        /// In transactions involving new construction, where the creditor reasonably expects that settlement will occur more than 60 days after the disclosures 
        /// required under paragraph (e)(1)(i) of this section are provided pursuant to paragraph (e)(1)(iii) of this section, the creditor may provide revised disclosures 
        /// to the consumer if the original disclosures required under paragraph (e)(1)(i) of this section state clearly and conspicuously that at any time prior to 60 days 
        /// before consummation, the creditor may issue revised disclosures. If no such statement is provided, the creditor may not issue revised disclosures, 
        /// except as otherwise provided in paragraph (f) of this section.
        /// </summary>
        [XmlEnum("Construction Loan Delayed Settlement")]
        ConstructionLoanDelayedSettlement,

        /// <summary>
        /// The consumer requests revisions to the credit terms or the settlement that cause an estimated charge to increase.
        /// </summary>
        [XmlEnum("Customer Requested")]
        CustomerRequested,

        /// <summary>
        /// The consumer is ineligible for an estimated charge previously disclosed because a changed circumstance, as defined under paragraph (e)(3)(iv)(A) of this section, 
        /// affected the consumer’s creditworthiness or the value of the security for the loan.
        /// </summary>
        [XmlEnum("Eligibility Affected")]
        EligibilityAffected,

        /// <summary>
        /// An extraordinary event beyond the control of any interested party or other unexpected event specific to the consumer or transaction.
        /// </summary>
        [XmlEnum("Extraordinary or Unexpected Event")]
        ExtraordinaryEvent,

        /// <summary>
        /// Information specific to the consumer or transaction that the creditor relied upon when providing the disclosures required under paragraph (e)(1)(i) of this section 
        /// and that was inaccurate or changed after the disclosures were provided.
        /// </summary>
        [XmlEnum("Inaccurate or Changed Information")]
        InaccurateInformation,

        /// <summary>
        /// The consumer indicates an intent to proceed with the transaction more than ten business days after the disclosures required under paragraph (e)(1)(i) of this section 
        /// are provided pursuant to paragraph (e)(1)(iii) of this section.
        /// </summary>
        [XmlEnum("Loan Estimate Expiration")]
        LoanEstimateExpiration,

        /// <summary>
        /// New Information On Which Creditor Did Not Previously Rely - New information specific to the consumer or transaction that the creditor did not rely on when providing 
        /// the original disclosures required under paragraph (e)(1)(i) of this section.
        /// </summary>
        [XmlEnum("New Information")]
        NewInformation,

        /// <summary>
        /// The points or lender credits change because the interest rate was not locked when the disclosures required under paragraph (e)(1)(i) of this section were provided. 
        /// On the date the interest rate is locked, the creditor shall provide a revised version of the disclosures required under paragraph (e)(1)(i) of this section to the 
        /// consumer with the revised interest rate, the points disclosed pursuant to § 1026.37(f)(1), lender credits, and any other interest rate dependent charges and terms.
        /// </summary>
        [XmlEnum("Rate Dependent")]
        RateDependent
    }

    public static class EnumMappings
    {
        public static readonly string[] E_RequestMethod = {
            "",
            "HTML-Page",
            "HTML",
            "HTML-Report",
            "HTML-XML",
            "PDF-Report",
            "PDF-XML",
            "XML-Results"
        };

        public static readonly string[] E_LoanStatusCode = {
            "",
            "Active",
            "Incomplete File",
            "Closed",
            "Denied",
            "Cancelled",
            "Withdrawn",
            "Withdrawn After Approval"
        };

        public static readonly string[] E_CEReportServiceEnum = {
            "",
            "HMDA-Check",
            "CRA-Review",
            "GEOCODE",
            "Rate-Spread-Check",
            "High-Cost",
            "Mavent-Review",
            "Mavent-Summary",
            "Mavent-TILA-Detail",
            "Mavent-OFACSSN-Detail",
            "Mavent-License-Detail",
            "Mavent-GSE-Detail",
            "Mavent-Additional-Detail",
            "Mavent-RESPA-Detail",
            "Mavent-QMATR-Detail",
            "Flood",
            "Flood-Quick-Check",
            "RecordingFee",
            "ServiceProviderSearch",
            "LEF-Check",
            "ExclusionaryList",
            "Fraud-Borrower",
            "Fraud-Collateral",
            "Fraud-Complete",
            "Mavent-NMLS",
            "IncomeReview",
            "SSNReview",
            "QM-Review",
            "AvailableServices",
            "Mavent-RESPATILA-Detail",
            "AVM-Review",
            "FHATOTALScorecard-Review",
            "Compliance Summary Report",
            "MCR-Review",
            "AUS-Review"
        };

        public static readonly string[] E_CEReportStatusEnum = {
            "",
            "Pass",
            "PASS",
            "Fail",
            "FAIL",
            "FailException",
            "FAILEXCEPTION",
            "Alert",
            "ALERT",
            "Warning",
            "WARNING",
            "Review",
            "REVIEW",
            "UserInput",
            "USERINPUT",
            "Delayed",
            "DELAYED"
        };

        public static readonly string[] E_CEReportContentsEnum = {
            "",
            "HTML-Page",
            "HTML",
            "HTML-Report",
            "HTML-XML",
            "PDF-Report",
            "PDF-XML",
            "XML-Results"
        };
    }
}
