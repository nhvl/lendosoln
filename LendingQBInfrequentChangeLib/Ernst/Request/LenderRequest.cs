namespace Ernst.Request
{
    using System.Xml.Serialization;

    public partial class LenderRequest
    {

        [XmlElement(Order = 0)]
        public ErnstDecimal Version { get; set; }

        [XmlIgnore]
        public bool VersionSpecified
        {
            get { return this.Version != null; }
            set { }
        }

        [XmlElement(Order = 1)]
        public Options Options { get; set; }

        [XmlIgnore]
        public bool OptionsSpecified
        {
            get { return this.Options != null; }
            set { }
        }

        [XmlElement(Order = 2)]
        public LenderRequestProperty Property { get; set; }

        [XmlIgnore]
        public bool PropertySpecified
        {
            get { return this.Property != null; }
            set { }
        }

        [XmlElement(Order = 3)]
        public ErnstString LenderAttorney { get; set; }

        [XmlIgnore]
        public bool LenderAttorneySpecified
        {
            get { return this.LenderAttorney != null; }
            set { }
        }

        [XmlElement(Order = 4)]
        public ErnstBoolean LenderRequested { get; set; }

        [XmlIgnore]
        public bool LenderRequestedSpecified
        {
            get { return this.LenderRequested != null; }
            set { }
        }
    }
}
