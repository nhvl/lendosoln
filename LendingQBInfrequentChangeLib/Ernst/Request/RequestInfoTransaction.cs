namespace Ernst.Request
{
    using System.Xml.Serialization;

    public partial class RequestInfoTransaction
    {
        [XmlElement(Order = 0)]
        public ErnstString BusinessChannel { get; set; }

        [XmlIgnore]
        public bool BusinessChannelSpecified
        {
            get { return this.BusinessChannel != null; }
            set { }
        }

        [XmlElement(Order = 1)]
        public ErnstString BusinessLine { get; set; }

        [XmlIgnore]
        public bool BusinessLineSpecified
        {
            get { return this.BusinessLine != null; }
            set { }
        }

        [XmlElement(Order = 2)]
        public ErnstString BusinessArea { get; set; }

        [XmlIgnore]
        public bool BusinessAreaSpecified
        {
            get { return this.BusinessArea != null; }
            set { }
        }

        [XmlElement(Order = 3)]
        public ErnstBoolean RequestAll { get; set; }

        [XmlIgnore]
        public bool RequestAllSpecified
        {
            get { return this.RequestAll != null; }
            set { }
        }

        [XmlElement(Order = 4)]
        public ErnstString OriginationSystem { get; set; }

        [XmlIgnore]
        public bool OriginationSystemSpecified
        {
            get { return this.OriginationSystem != null; }
            set { }
        }
    }
}
