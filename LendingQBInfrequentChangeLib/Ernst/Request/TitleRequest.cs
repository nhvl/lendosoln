namespace Ernst.Request
{
    using System.Collections.Generic;
    using System.Xml.Serialization;

    public partial class TitleRequest
    {

        [XmlElement(Order = 0)]
        public ErnstDecimal Version { get; set; }

        [XmlIgnore]
        public bool VersionSpecified
        {
            get { return this.Version != null; }
            set { }
        }

        [XmlElement(Order = 1)]
        public ErnstString TitleService { get; set; }

        [XmlIgnore]
        public bool TitleServiceSpecified
        {
            get { return this.TitleService != null; }
            set { }
        }

        [XmlElement(Order = 2)]
        public Options Options { get; set; }

        [XmlIgnore]
        public bool OptionsSpecified
        {
            get { return this.Options != null; }
            set { }
        }

        [XmlElement(Order = 3)]
        public ErnstString ProviderID { get; set; }

        [XmlIgnore]
        public bool ProviderIDSpecified
        {
            get { return this.ProviderID != null; }
            set { }
        }

        [XmlElement(Order = 4)]
        public ErnstString UnderwriterID { get; set; }

        [XmlIgnore]
        public bool UnderwriterIDSpecified
        {
            get { return this.UnderwriterID != null; }
            set { }
        }

        [XmlElement(Order = 5)]
        public ErnstString NumberOfSubordinations { get; set; }

        [XmlIgnore]
        public bool NumberOfSubordinationsSpecified
        {
            get { return this.NumberOfSubordinations != null; }
            set { }
        }

        [XmlElement(Order = 6)]
        public TitleRequestProperty Property { get; set; }

        [XmlIgnore]
        public bool PropertySpecified
        {
            get { return this.Property != null; }
            set { }
        }

        [XmlElement(Order = 7)]
        public ErnstEnum<TitleRequestPolicyType> PolicyType { get; set; }

        [XmlIgnore]
        public bool PolicyTypeSpecified
        {
            get { return this.PolicyType != null; }
            set { }
        }

        [XmlElement(Order = 8)]
        public ErnstBoolean UseCommonEndorsements { get; set; }

        [XmlIgnore]
        public bool UseCommonEndorsementsSpecified
        {
            get { return this.UseCommonEndorsements != null; }
            set { }
        }

        [XmlElement(Order = 9)]
        public ErnstBoolean UseSimultaneousRates { get; set; }

        [XmlIgnore]
        public bool UseSimultaneousRatesSpecified
        {
            get { return this.UseSimultaneousRates != null; }
            set { }
        }

        [XmlElement(Order = 10)]
        public TitleRequestItemizedSettlementFees ItemizedSettlementFees { get; set; }

        [XmlIgnore]
        public bool ItemizedSettlementFeesSpecified
        {
            get { return this.ItemizedSettlementFees != null; }
            set { }
        }

        [XmlElement(Order = 11)]
        public TitlePolicy LendersPolicy { get; set; }

        [XmlIgnore]
        public bool LendersPolicySpecified
        {
            get { return this.LendersPolicy != null; }
            set { }
        }

        [XmlElement(Order = 12)]
        public TitlePolicy OwnersPolicy { get; set; }

        [XmlIgnore]
        public bool OwnersPolicySpecified
        {
            get { return this.OwnersPolicy != null; }
            set { }
        }

        [XmlElement(Order = 13)]
        public List<TitleRequestEndorsement> Endorsements { get; set; }

        [XmlIgnore]
        public bool EndorsementsSpecified
        {
            get { return this.Endorsements != null && this.Endorsements.Count > 0; }
            set { }
        }
    }
}
