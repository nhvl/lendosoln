namespace Ernst.Request
{
    using System.Xml.Serialization;

    public partial class RequestInfoLoan
    {
        [XmlElement(Order = 0)]
        public ErnstDate ApplicationDate { get; set; }

        [XmlIgnore]
        public bool ApplicationDateSpecified
        {
            get { return this.ApplicationDate != null; }
            set { }
        }

        [XmlElement(Order = 1)]
        public ErnstBoolean FirstTimeHomeBuyer { get; set; }

        [XmlIgnore]
        public bool FirstTimeHomeBuyerSpecified
        {
            get { return this.FirstTimeHomeBuyer != null; }
            set { }
        }

        [XmlElement(Order = 2)]
        public ErnstBoolean Texas50a6ApplicabilityIndicator { get; set; }

        [XmlIgnore]
        public bool Texas50a6ApplicabilityIndicatorSpecified
        {
            get { return this.Texas50a6ApplicabilityIndicator != null; }
            set { }
        }

        [XmlElement(Order = 3)]
        public ErnstBoolean HELOC { get; set; }

        [XmlIgnore]
        public bool HELOCSpecified
        {
            get { return this.HELOC != null; }
            set { }
        }

        [XmlElement(Order = 4)]
        public ErnstEnum<RequestInfoLoanLoanPurpose> LoanPurpose { get; set; }

        [XmlIgnore]
        public bool LoanPurposeSpecified
        {
            get { return this.LoanPurpose != null; }
            set { }
        }

        [XmlElement(Order = 5)]
        public ErnstEnum<RequestInfoLoanRateType> RateType { get; set; }

        [XmlIgnore]
        public bool RateTypeSpecified
        {
            get { return this.RateType != null; }
            set { }
        }

        [XmlElement(Order = 6)]
        public ErnstEnum<RequestInfoLoanMortgageType> MortgageType { get; set; }

        [XmlIgnore]
        public bool MortgageTypeSpecified
        {
            get { return this.MortgageType != null; }
            set { }
        }

        [XmlElement(Order = 7)]
        public ErnstBoolean RefinanceSameLender { get; set; }

        [XmlIgnore]
        public bool RefinanceSameLenderSpecified
        {
            get { return this.RefinanceSameLender != null; }
            set { }
        }

        [XmlElement(Order = 8)]
        public ErnstBoolean LoanPayoffIndicator { get; set; }

        [XmlIgnore]
        public bool LoanPayoffIndicatorSpecified
        {
            get { return this.LoanPayoffIndicator != null; }
            set { }
        }

        [XmlElement(Order = 9)]
        public ErnstString LoanPayoffCount { get; set; }

        [XmlIgnore]
        public bool LoanPayoffCountSpecified
        {
            get { return this.LoanPayoffCount != null; }
            set { }
        }

        [XmlElement(Order = 10)]
        public ErnstString NumberOfSubordinations { get; set; }

        [XmlIgnore]
        public bool NumberOfSubordinationsSpecified
        {
            get { return this.NumberOfSubordinations != null; }
            set { }
        }

        [XmlElement(Order = 11)]
        public ErnstBoolean Subordination1SameLender { get; set; }

        [XmlIgnore]
        public bool Subordination1SameLenderSpecified
        {
            get { return this.Subordination1SameLender != null; }
            set { }
        }

        [XmlElement(Order = 12)]
        public ErnstBoolean CEMAIndicator { get; set; }

        [XmlIgnore]
        public bool CEMAIndicatorSpecified
        {
            get { return this.CEMAIndicator != null; }
            set { }
        }

        [XmlElement(Order = 13)]
        public ErnstBoolean RegisteredLandIndicator { get; set; }

        [XmlIgnore]
        public bool RegisteredLandIndicatorSpecified
        {
            get { return this.RegisteredLandIndicator != null; }
            set { }
        }

        [XmlElement(Order = 14)]
        public ErnstString BorrowerCount { get; set; }

        [XmlIgnore]
        public bool BorrowerCountSpecified
        {
            get { return this.BorrowerCount != null; }
            set { }
        }

        [XmlElement(Order = 15)]
        public ErnstBoolean Borrower1NonBorrowingSpouse { get; set; }

        [XmlIgnore]
        public bool Borrower1NonBorrowingSpouseSpecified
        {
            get { return this.Borrower1NonBorrowingSpouse != null; }
            set { }
        }

        [XmlElement(Order = 16)]
        public ErnstBoolean Borrower2NonBorrowingSpouse { get; set; }

        [XmlIgnore]
        public bool Borrower2NonBorrowingSpouseSpecified
        {
            get { return this.Borrower2NonBorrowingSpouse != null; }
            set { }
        }

        [XmlElement(Order = 17)]
        public ErnstBoolean TrustIndicator { get; set; }

        [XmlIgnore]
        public bool TrustIndicatorSpecified
        {
            get { return this.TrustIndicator != null; }
            set { }
        }

        [XmlElement(Order = 18)]
        public ErnstBoolean ProjectQuestionnaireIndicator { get; set; }

        [XmlIgnore]
        public bool ProjectQuestionnaireIndicatorSpecified
        {
            get { return this.ProjectQuestionnaireIndicator != null; }
            set { }
        }

        [XmlElement(Order = 19)]
        public ErnstBoolean VestingChangeIndicator { get; set; }

        [XmlIgnore]
        public bool VestingChangeIndicatorSpecified
        {
            get { return this.VestingChangeIndicator != null; }
            set { }
        }

        [XmlElement(Order = 20)]
        public ErnstString TrustCount { get; set; }

        [XmlIgnore]
        public bool TrustCountSpecified
        {
            get { return this.TrustCount != null; }
            set { }
        }

        [XmlElement(Order = 21)]
        public ErnstBoolean BalloonIndicator { get; set; }

        [XmlIgnore]
        public bool BalloonIndicatorSpecified
        {
            get { return this.BalloonIndicator != null; }
            set { }
        }
    }
}
