namespace Ernst.Request
{
    using System.Xml.Serialization;

    public partial class PropertyTaxRequest
    {
        [XmlElement(Order = 0)]
        public ErnstDecimal Version { get; set; }

        [XmlIgnore]
        public bool VersionSpecified
        {
            get { return this.Version != null; }
            set { }
        }

        [XmlElement(Order = 1)]
        public PropertyTaxRequestProperty Property { get; set; }

        [XmlIgnore]
        public bool PropertySpecified
        {
            get { return this.Property != null; }
            set { }
        }
    }
}
