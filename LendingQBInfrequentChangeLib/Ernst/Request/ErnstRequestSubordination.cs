namespace Ernst.Request
{
    using System.Xml.Serialization;

    public partial class ErnstRequestSubordination
    {
        [XmlElement(Order = 0)]
        public ErnstString Pages { get; set; }

        [XmlIgnore]
        public bool PagesSpecified
        {
            get { return this.Pages != null; }
            set { }
        }

        [XmlElement(Order = 1)]
        public ErnstString NumberOfSubordinations { get; set; }

        [XmlIgnore]
        public bool NumberOfSubordinationsSpecified
        {
            get { return this.NumberOfSubordinations != null; }
            set { }
        }

        [XmlElement(Order = 2)]
        public ErnstIndex Index { get; set; }

        [XmlIgnore]
        public bool IndexSpecified
        {
            get { return this.Index != null; }
            set { }
        }
    }
}
