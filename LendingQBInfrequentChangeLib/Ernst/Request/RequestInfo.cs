namespace Ernst.Request
{
    using System.Xml.Serialization;

    public partial class RequestInfo
    {
        [XmlElement(Order = 0)]
        public RequestInfoTransaction Transaction { get; set; }

        [XmlIgnore]
        public bool TransactionSpecified
        {
            get { return this.Transaction != null; }
            set { }
        }

        [XmlElement(Order = 1)]
        public RequestInfoLoan Loan { get; set; }

        [XmlIgnore]
        public bool LoanSpecified
        {
            get { return this.Loan != null; }
            set { }
        }

        [XmlElement(Order = 2)]
        public RequestInfoProperty Property { get; set; }

        [XmlIgnore]
        public bool PropertySpecified
        {
            get { return this.Property != null; }
            set { }
        }
    }
}
