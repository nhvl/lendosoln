namespace Ernst.Request
{
    using System.Xml.Serialization;

    public partial class ErnstIndex
    {
        [XmlElement(Order = 0)]
        public ErnstString NumberOfGrantors { get; set; }

        [XmlIgnore]
        public bool NumberOfGrantorsSpecified
        {
            get { return this.NumberOfGrantors != null; }
            set { }
        }

        [XmlElement(Order = 1)]
        public ErnstString NumberOfGrantees { get; set; }

        [XmlIgnore]
        public bool NumberOfGranteesSpecified
        {
            get { return this.NumberOfGrantees != null; }
            set { }
        }

        [XmlElement(Order = 2)]
        public ErnstString NumberOfSurnames { get; set; }

        [XmlIgnore]
        public bool NumberOfSurnamesSpecified
        {
            get { return this.NumberOfSurnames != null; }
            set { }
        }

        [XmlElement(Order = 3)]
        public ErnstString NumberOfSignatures { get; set; }

        [XmlIgnore]
        public bool NumberOfSignaturesSpecified
        {
            get { return this.NumberOfSignatures != null; }
            set { }
        }
    }
}
