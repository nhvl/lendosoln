namespace Ernst.Request
{
    using System.Xml.Serialization;

    public partial class PropertyGeocode
    {
        [XmlElement(Order = 0)]
        public ErnstBoolean Requested { get; set; }

        [XmlIgnore]
        public bool RequestedSpecified
        {
            get { return this.Requested != null; }
            set { }
        }

        [XmlElement(Order = 1)]
        public ErnstString StreetAddress { get; set; }

        [XmlIgnore]
        public bool StreetAddressSpecified
        {
            get { return this.StreetAddress != null; }
            set { }
        }

        [XmlElement(Order = 2)]
        public ErnstString City { get; set; }

        [XmlIgnore]
        public bool CitySpecified
        {
            get { return this.City != null; }
            set { }
        }

        [XmlElement(Order = 3)]
        public ErnstString State { get; set; }

        [XmlIgnore]
        public bool StateSpecified
        {
            get { return this.State != null; }
            set { }
        }

        [XmlElement(Order = 4)]
        public ErnstString ZipCode { get; set; }

        [XmlIgnore]
        public bool ZipCodeSpecified
        {
            get { return this.ZipCode != null; }
            set { }
        }

        [XmlElement(Order = 5)]
        public ErnstString Longitude { get; set; }

        [XmlIgnore]
        public bool LongitudeSpecified
        {
            get { return this.Longitude != null; }
            set { }
        }

        [XmlElement(Order = 6)]
        public ErnstString Latitude { get; set; }

        [XmlIgnore]
        public bool LatitudeSpecified
        {
            get { return this.Latitude != null; }
            set { }
        }
    }
}
