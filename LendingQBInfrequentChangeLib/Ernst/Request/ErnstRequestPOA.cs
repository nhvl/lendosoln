namespace Ernst.Request
{
    using System.Xml.Serialization;

    public partial class ErnstRequestPOA
    {
        [XmlElement(Order = 0)]
        public ErnstString Pages { get; set; }

        [XmlIgnore]
        public bool PagesSpecified
        {
            get { return this.Pages != null; }
            set { }
        }

        [XmlElement(Order = 1)]
        public ErnstString NumberOfPowersOfAttorney { get; set; }

        [XmlIgnore]
        public bool NumberOfPowersOfAttorneySpecified
        {
            get { return this.NumberOfPowersOfAttorney != null; }
            set { }
        }
    }
}
