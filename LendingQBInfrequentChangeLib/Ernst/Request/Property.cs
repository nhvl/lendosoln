namespace Ernst.Request
{
    using System.Xml.Serialization;

    public partial class Property
    {
        [XmlElement(Order = 0)]
        public ErnstString Page { get; set; }

        [XmlIgnore]
        public bool PageSpecified
        {
            get { return this.Page != null; }
            set { }
        }

        [XmlElement(Order = 1)]
        public ErnstString City { get; set; }

        [XmlIgnore]
        public bool CitySpecified
        {
            get { return this.City != null; }
            set { }
        }

        [XmlElement(Order = 2)]
        public ErnstString County { get; set; }

        [XmlIgnore]
        public bool CountySpecified
        {
            get { return this.County != null; }
            set { }
        }

        [XmlElement(Order = 3)]
        public ErnstString State { get; set; }

        [XmlIgnore]
        public bool StateSpecified
        {
            get { return this.State != null; }
            set { }
        }

        [XmlElement(Order = 4)]
        public ErnstString FullAddress { get; set; }

        [XmlIgnore]
        public bool FullAddressSpecified
        {
            get { return this.FullAddress != null; }
            set { }
        }

        [XmlElement(Order = 5)]
        public ErnstString EstimatedValue { get; set; }

        [XmlIgnore]
        public bool EstimatedValueSpecified
        {
            get { return this.EstimatedValue != null; }
            set { }
        }

        [XmlElement(Order = 6)]
        public ErnstString MortgageAmount { get; set; }

        [XmlIgnore]
        public bool MortgageAmountSpecified
        {
            get { return this.MortgageAmount != null; }
            set { }
        }

        [XmlElement(Order = 7)]
        public ErnstString OriginalDebtAmount { get; set; }

        [XmlIgnore]
        public bool OriginalDebtAmountSpecified
        {
            get { return this.OriginalDebtAmount != null; }
            set { }
        }

        [XmlElement(Order = 8)]
        public ErnstString UnpaidPrincipalBalance { get; set; }

        [XmlIgnore]
        public bool UnpaidPrincipalBalanceSpecified
        {
            get { return this.UnpaidPrincipalBalance != null; }
            set { }
        }

        [XmlElement(Order = 9)]
        public ErnstString Title { get; set; }

        [XmlIgnore]
        public bool TitleSpecified
        {
            get { return this.Title != null; }
            set { }
        }

        [XmlElement(Order = 10)]
        public ErnstString TitleAmount { get; set; }

        [XmlIgnore]
        public bool TitleAmountSpecified
        {
            get { return this.TitleAmount != null; }
            set { }
        }

        [XmlElement(Order = 11)]
        public ErnstString OwnersTitleAmount { get; set; }

        [XmlIgnore]
        public bool OwnersTitleAmountSpecified
        {
            get { return this.OwnersTitleAmount != null; }
            set { }
        }

        [XmlElement(Order = 12)]
        public ErnstString OriginalTitleAmount { get; set; }

        [XmlIgnore]
        public bool OriginalTitleAmountSpecified
        {
            get { return this.OriginalTitleAmount != null; }
            set { }
        }

        [XmlElement(Order = 13)]
        public ErnstString OriginalMortgageDate { get; set; }

        [XmlIgnore]
        public bool OriginalMortgageDateSpecified
        {
            get { return this.OriginalMortgageDate != null; }
            set { }
        }

        [XmlElement(Order = 14)]
        public PropertyStateQuestions StateQuestions { get; set; }

        [XmlIgnore]
        public bool StateQuestionsSpecified
        {
            get { return this.StateQuestions != null; }
            set { }
        }

        [XmlElement(Order = 15)]
        public PropertyGeocode Geocode { get; set; }

        [XmlIgnore]
        public bool GeocodeSpecified
        {
            get { return this.Geocode != null; }
            set { }
        }
    }
}
