namespace Ernst.Request
{
    using System.Xml.Serialization;

    public partial class DocumentRequest
    {
        [XmlElement(Order = 0)]
        public ErnstDecimal Version { get; set; }

        [XmlIgnore]
        public bool VersionSpecified
        {
            get { return this.Version != null; }
            set { }
        }

        [XmlElement(Order = 1)]
        public ErnstEnum<DocumentRequestDocumentFormat> DocumentFormat { get; set; }

        [XmlIgnore]
        public bool DocumentFormatSpecified
        {
            get { return this.DocumentFormat != null; }
            set { }
        }

        [XmlElement(Order = 2)]
        public DocumentRequestProviders Providers { get; set; }

        [XmlIgnore]
        public bool ProvidersSpecified
        {
            get { return this.Providers != null; }
            set { }
        }
    }
}
