namespace Ernst.Request
{
    using System.Xml.Serialization;

    public partial class ErnstRequestRelease
    {
        [XmlElement(Order = 0)]
        public ErnstString Pages { get; set; }

        [XmlIgnore]
        public bool PagesSpecified
        {
            get { return this.Pages != null; }
            set { }
        }

        [XmlElement(Order = 1)]
        public ErnstString NumberOfReleases { get; set; }

        [XmlIgnore]
        public bool NumberOfReleasesSpecified
        {
            get { return this.NumberOfReleases != null; }
            set { }
        }

        [XmlElement(Order = 2)]
        public ErnstIndex Index { get; set; }

        [XmlIgnore]
        public bool IndexSpecified
        {
            get { return this.Index != null; }
            set { }
        }
    }
}
