namespace Ernst.Request
{
    using System.Xml.Serialization;

    public partial class AppraisalRequestAppraisalProduct
    {
        [XmlElement(Order = 0)]
        public ErnstEnum<AppraisalRequestAppraisalProductCode> Code { get; set; }

        [XmlIgnore]
        public bool CodeSpecified
        {
            get { return this.Code != null; }
            set { }
        }

        [XmlElement(Order = 1)]
        public ErnstBoolean Requested { get; set; }

        [XmlIgnore]
        public bool RequestedSpecified
        {
            get { return this.Requested != null; }
            set { }
        }

        [XmlElement(Order = 2)]
        public ErnstString Quantity { get; set; }

        [XmlIgnore]
        public bool QuantitySpecified
        {
            get { return this.Quantity != null; }
            set { }
        }
    }
}
