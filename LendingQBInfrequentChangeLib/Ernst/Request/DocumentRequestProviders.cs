namespace Ernst.Request
{
    using System.Xml.Serialization;

    public partial class DocumentRequestProviders
    {
        [XmlElement(Order = 0)]
        public ErnstBoolean Requested { get; set; }

        [XmlIgnore]
        public bool RequestedSpecified
        {
            get { return this.Requested != null; }
            set { }
        }

        [XmlElement(Order = 1)]
        public ErnstString ProviderID { get; set; }

        [XmlIgnore]
        public bool ProviderIDSpecified
        {
            get { return this.ProviderID != null; }
            set { }
        }

        [XmlElement(Order = 2)]
        public ErnstString BusinessChannel { get; set; }

        [XmlIgnore]
        public bool BusinessChannelSpecified
        {
            get { return this.BusinessChannel != null; }
            set { }
        }

        [XmlElement(Order = 3)]
        public ErnstString LoanPurpose { get; set; }

        [XmlIgnore]
        public bool LoanPurposeSpecified
        {
            get { return this.LoanPurpose != null; }
            set { }
        }

        [XmlElement(Order = 4)]
        public ErnstString MaxNumberOfProviders { get; set; }

        [XmlIgnore]
        public bool MaxNumberOfProvidersSpecified
        {
            get { return this.MaxNumberOfProviders != null; }
            set { }
        }

        [XmlElement(Order = 5)]
        public ErnstString Originator { get; set; }

        [XmlIgnore]
        public bool OriginatorSpecified
        {
            get { return this.Originator != null; }
            set { }
        }

        [XmlElement(Order = 6)]
        public ErnstString Borrower { get; set; }

        [XmlIgnore]
        public bool BorrowerSpecified
        {
            get { return this.Borrower != null; }
            set { }
        }

        [XmlElement(Order = 7)]
        public ErnstString LoanNumber { get; set; }

        [XmlIgnore]
        public bool LoanNumberSpecified
        {
            get { return this.LoanNumber != null; }
            set { }
        }

        [XmlElement(Order = 8)]
        public ErnstString Address1 { get; set; }

        [XmlIgnore]
        public bool Address1Specified
        {
            get { return this.Address1 != null; }
            set { }
        }

        [XmlElement(Order = 9)]
        public ErnstString Address2 { get; set; }

        [XmlIgnore]
        public bool Address2Specified
        {
            get { return this.Address2 != null; }
            set { }
        }

        [XmlElement(Order = 10)]
        public ErnstString City { get; set; }

        [XmlIgnore]
        public bool CitySpecified
        {
            get { return this.City != null; }
            set { }
        }

        [XmlElement(Order = 11)]
        public ErnstString County { get; set; }

        [XmlIgnore]
        public bool CountySpecified
        {
            get { return this.County != null; }
            set { }
        }

        [XmlElement(Order = 12)]
        public ErnstString State { get; set; }

        [XmlIgnore]
        public bool StateSpecified
        {
            get { return this.State != null; }
            set { }
        }

        [XmlElement(Order = 13)]
        public ErnstString Zip { get; set; }

        [XmlIgnore]
        public bool ZipSpecified
        {
            get { return this.Zip != null; }
            set { }
        }
    }
}
