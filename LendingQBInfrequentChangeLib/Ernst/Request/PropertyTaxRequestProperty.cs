namespace Ernst.Request
{
    using System.Xml.Serialization;

    public partial class PropertyTaxRequestProperty
    {
        [XmlElement(Order = 0)]
        public ErnstString StreetAddress { get; set; }

        [XmlIgnore]
        public bool StreetAddressSpecified
        {
            get { return this.StreetAddress != null; }
            set { }
        }

        [XmlElement(Order = 1)]
        public ErnstString City { get; set; }

        [XmlIgnore]
        public bool CitySpecified
        {
            get { return this.City != null; }
            set { }
        }

        [XmlElement(Order = 2)]
        public ErnstString State { get; set; }

        [XmlIgnore]
        public bool StateSpecified
        {
            get { return this.State != null; }
            set { }
        }

        [XmlElement(Order = 3)]
        public ErnstString PostalCode { get; set; }

        [XmlIgnore]
        public bool PostalCodeSpecified
        {
            get { return this.PostalCode != null; }
            set { }
        }
    }
}
