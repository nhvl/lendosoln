namespace Ernst.Request
{
    using System.Xml.Serialization;

    public partial class RequestAuthentication
    {
        [XmlElement(Order = 0)]
        public ErnstString UserID { get; set; }

        [XmlIgnore]
        public bool UserIDSpecified
        {
            get { return this.UserID != null; }
            set { }
        }

        [XmlElement(Order = 1)]
        public ErnstString Password { get; set; }

        [XmlIgnore]
        public bool PasswordSpecified
        {
            get { return this.Password != null; }
            set { }
        }
    }
}
