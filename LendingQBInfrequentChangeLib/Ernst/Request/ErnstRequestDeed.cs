namespace Ernst.Request
{
    using System.Collections.Generic;
    using System.Xml.Serialization;

    public partial class ErnstRequestDeed
    {
        [XmlElement(Order = 0)]
        public ErnstString AmendmentModificationPages { get; set; }

        [XmlIgnore]
        public bool AmendmentModificationPagesSpecified
        {
            get { return this.AmendmentModificationPages != null; }
            set { }
        }

        [XmlElement(Order = 1)]
        public ErnstIndex Index { get; set; }

        [XmlIgnore]
        public bool IndexSpecified
        {
            get { return this.Index != null; }
            set { }
        }

        [XmlElement(Order = 2)]
        public List<ErnstString> Text { get; set; }

        [XmlIgnore]
        public bool TextSpecified
        {
            get { return this.Text != null && this.Text.Count > 0; }
            set { }
        }
    }
}
