namespace Ernst.Request
{
    using System.Xml.Serialization;

    public partial class Block6Request
    {
        [XmlElement(Order = 0)]
        public ErnstDecimal Version { get; set; }

        [XmlIgnore]
        public bool VersionSpecified
        {
            get { return this.Version != null; }
            set { }
        }

        [XmlElement(Order = 1)]
        public Options Options { get; set; }

        [XmlIgnore]
        public bool OptionsSpecified
        {
            get { return this.Options != null; }
            set { }
        }

        [XmlElement(Order = 2)]
        public Block6RequestProperty Property { get; set; }

        [XmlIgnore]
        public bool PropertySpecified
        {
            get { return this.Property != null; }
            set { }
        }

        [XmlElement(Order = 3)]
        public Block6Service Standard { get; set; }

        [XmlIgnore]
        public bool StandardSpecified
        {
            get { return this.Standard != null; }
            set { }
        }

        [XmlElement(Order = 4)]
        public Block6Service Pest { get; set; }

        [XmlIgnore]
        public bool PestSpecified
        {
            get { return this.Pest != null; }
            set { }
        }

        [XmlElement(Order = 5)]
        public Block6Service Water { get; set; }

        [XmlIgnore]
        public bool WaterSpecified
        {
            get { return this.Water != null; }
            set { }
        }

        [XmlElement(Order = 6)]
        public Block6Service Septic { get; set; }

        [XmlIgnore]
        public bool SepticSpecified
        {
            get { return this.Septic != null; }
            set { }
        }

        [XmlElement(Order = 7)]
        public Block6Service Well { get; set; }

        [XmlIgnore]
        public bool WellSpecified
        {
            get { return this.Well != null; }
            set { }
        }

        [XmlElement(Order = 8)]
        public Block6Service Smoke { get; set; }

        [XmlIgnore]
        public bool SmokeSpecified
        {
            get { return this.Smoke != null; }
            set { }
        }

        [XmlElement(Order = 9)]
        public Block6Service Radon { get; set; }

        [XmlIgnore]
        public bool RadonSpecified
        {
            get { return this.Radon != null; }
            set { }
        }

        [XmlElement(Order = 10)]
        public Block6Service Roof { get; set; }

        [XmlIgnore]
        public bool RoofSpecified
        {
            get { return this.Roof != null; }
            set { }
        }

        [XmlElement(Order = 11)]
        public Block6Service Structural { get; set; }

        [XmlIgnore]
        public bool StructuralSpecified
        {
            get { return this.Structural != null; }
            set { }
        }

        [XmlElement(Order = 12)]
        public Block6Service Survey { get; set; }

        [XmlIgnore]
        public bool SurveySpecified
        {
            get { return this.Survey != null; }
            set { }
        }

        [XmlElement(Order = 13)]
        public Block6RequestMold Mold { get; set; }

        [XmlIgnore]
        public bool MoldSpecified
        {
            get { return this.Mold != null; }
            set { }
        }

        [XmlElement(Order = 14)]
        public Block6Service ManufacturedHousing { get; set; }

        [XmlIgnore]
        public bool ManufacturedHousingSpecified
        {
            get { return this.ManufacturedHousing != null; }
            set { }
        }

        [XmlElement(Order = 15)]
        public Block6RequestAsbestos Asbestos { get; set; }

        [XmlIgnore]
        public bool AsbestosSpecified
        {
            get { return this.Asbestos != null; }
            set { }
        }

        [XmlElement(Order = 16)]
        public Block6RequestLead Lead { get; set; }

        [XmlIgnore]
        public bool LeadSpecified
        {
            get { return this.Lead != null; }
            set { }
        }

        [XmlElement(Order = 17)]
        public Block6RequestDrywall Drywall { get; set; }

        [XmlIgnore]
        public bool DrywallSpecified
        {
            get { return this.Drywall != null; }
            set { }
        }

        [XmlElement(Order = 18)]
        public Block6Service NaturalHazardDisclosure { get; set; }

        [XmlIgnore]
        public bool NaturalHazardDisclosureSpecified
        {
            get { return this.NaturalHazardDisclosure != null; }
            set { }
        }

        [XmlElement(Order = 19)]
        public Block6Service GeoTechnicalEngineering { get; set; }

        [XmlIgnore]
        public bool GeoTechnicalEngineeringSpecified
        {
            get { return this.GeoTechnicalEngineering != null; }
            set { }
        }

        [XmlElement(Order = 20)]
        public Block6Service Foundation { get; set; }

        [XmlIgnore]
        public bool FoundationSpecified
        {
            get { return this.Foundation != null; }
            set { }
        }

        [XmlElement(Order = 21)]
        public Block6Service Electrical { get; set; }

        [XmlIgnore]
        public bool ElectricalSpecified
        {
            get { return this.Electrical != null; }
            set { }
        }

        [XmlElement(Order = 22)]
        public Block6Service Plumbing { get; set; }

        [XmlIgnore]
        public bool PlumbingSpecified
        {
            get { return this.Plumbing != null; }
            set { }
        }

        [XmlElement(Order = 23)]
        public Block6Service HeatingCooling { get; set; }

        [XmlIgnore]
        public bool HeatingCoolingSpecified
        {
            get { return this.HeatingCooling != null; }
            set { }
        }

        [XmlElement(Order = 24)]
        public Block6Service EnvironmentalInspection { get; set; }

        [XmlIgnore]
        public bool EnvironmentalInspectionSpecified
        {
            get { return this.EnvironmentalInspection != null; }
            set { }
        }
    }
}
