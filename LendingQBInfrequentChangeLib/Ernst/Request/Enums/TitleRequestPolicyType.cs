﻿namespace Ernst.Request
{
    using System.Xml.Serialization;

    public enum TitleRequestPolicyType
    {
        [XmlEnum("new")]
        New,

        [XmlEnum("reissue")]
        Reissue,
    }
}
