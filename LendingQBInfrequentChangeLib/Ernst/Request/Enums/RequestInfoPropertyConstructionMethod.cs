﻿namespace Ernst.Request
{
    using System.Xml.Serialization;

    public enum RequestInfoPropertyConstructionMethod
    {
        [XmlEnum("LogHome")]
        LogHome,

        [XmlEnum("Manufactured")]
        Manufactured,

        [XmlEnum("MobileHome")]
        MobileHome,

        [XmlEnum("Modular")]
        Modular,

        [XmlEnum("OnFrameModular")]
        OnFrameModular,

        [XmlEnum("SiteBuilt")]
        SiteBuilt,

        [XmlEnum("Other")]
        Other,
    }
}
