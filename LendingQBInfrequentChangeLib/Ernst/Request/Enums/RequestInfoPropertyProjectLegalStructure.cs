﻿namespace Ernst.Request
{
    using System.Xml.Serialization;

    public enum RequestInfoPropertyProjectLegalStructure
    {
        [XmlEnum("Cooperative")]
        Cooperative,

        [XmlEnum("CommonInterestApartment")]
        CommonInterestApartment,

        [XmlEnum("Condominium")]
        Condominium,

        [XmlEnum("Condotel")]
        Condotel,

        [XmlEnum("MixedUseCondo")]
        MixedUseCondo,

        [XmlEnum("Unknown")]
        Unknown,
    }
}
