﻿namespace Ernst.Request
{
    using System.Xml.Serialization;

    public enum RequestInfoPropertyPropertyUse
    {
        [XmlEnum("Investment")]
        Investment,

        [XmlEnum("PrimaryResidence")]
        PrimaryResidence,

        [XmlEnum("SecondHome")]
        SecondHome,

        [XmlEnum("Other")]
        Other,
    }
}
