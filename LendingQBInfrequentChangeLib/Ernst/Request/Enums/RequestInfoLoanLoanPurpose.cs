﻿namespace Ernst.Request
{
    using System.Xml.Serialization;

    public enum RequestInfoLoanLoanPurpose
    {
        [XmlEnum("MortgageModification")]
        MortgageModification,

        [XmlEnum("Other")]
        Other,

        [XmlEnum("Purchase")]
        Purchase,

        [XmlEnum("Refinance")]
        Refinance,

        [XmlEnum("Unknown")]
        Unknown,
    }
}
