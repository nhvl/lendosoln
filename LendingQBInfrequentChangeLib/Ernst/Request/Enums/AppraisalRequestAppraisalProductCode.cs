﻿namespace Ernst.Request
{
    using System.Xml.Serialization;

    public enum AppraisalRequestAppraisalProductCode
    {
        [XmlEnum("AppraisalReport2")]
        AppraisalReport2,

        [XmlEnum("Form1004_InteriorSingleFamily")]
        Form1004_InteriorSingleFamily,

        [XmlEnum("Form1025_InteriorMultiFamily")]
        Form1025_InteriorMultiFamily,

        [XmlEnum("Form1073_InteriorCondominium")]
        Form1073_InteriorCondominium,

        [XmlEnum("Form2055_ExteriorSingleFamily")]
        Form2055_ExteriorSingleFamily,

        [XmlEnum("Form1004_FHAInteriorSingleFamily")]
        Form1004_FHAInteriorSingleFamily,

        [XmlEnum("Form1025_FHAInteriorMultiFamily")]
        Form1025_FHAInteriorMultiFamily,

        [XmlEnum("Form1073_FHAInteriorCondominium")]
        Form1073_FHAInteriorCondominium,

        [XmlEnum("Form2055_FHAExteriorSingleFamily")]
        Form2055_FHAExteriorSingleFamily,

        [XmlEnum("Form2090_InteriorCooperative")]
        Form2090_InteriorCooperative,

        [XmlEnum("Form1004D_UpdateReport")]
        Form1004D_UpdateReport,

        [XmlEnum("Form1004D_UpdateReport2")]
        Form1004D_UpdateReport2,

        [XmlEnum("Form1004D_CompletionReport")]
        Form1004D_CompletionReport,

        [XmlEnum("Form1004D_CompletionReport2")]
        Form1004D_CompletionReport2,

        [XmlEnum("Form1004D_CompletionReport3")]
        Form1004D_CompletionReport3,

        [XmlEnum("Form1004D_DisasterReport")]
        Form1004D_DisasterReport,

        [XmlEnum("Form1004D_DisasterReport2")]
        Form1004D_DisasterReport2,
    }
}
