﻿namespace Ernst.Request
{
    using System.Xml.Serialization;

    public enum TitleRequestPropertyLoanType
    {
        [XmlEnum("sale")]
        Sale,

        [XmlEnum("refinance")]
        Refinance,
    }
}
