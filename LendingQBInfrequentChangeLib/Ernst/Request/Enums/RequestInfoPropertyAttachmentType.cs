﻿namespace Ernst.Request
{
    using System.Xml.Serialization;

    public enum RequestInfoPropertyAttachmentType
    {
        [XmlEnum("Attached")]
        Attached,

        [XmlEnum("Detached")]
        Detached,

        [XmlEnum("SemiDetached")]
        SemiDetached,
    }
}
