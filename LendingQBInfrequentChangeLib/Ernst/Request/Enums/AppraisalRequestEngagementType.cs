﻿namespace Ernst.Request
{
    using System.Xml.Serialization;

    public enum AppraisalRequestEngagementType
    {
        [XmlEnum("AMC")]
        AMC,

        [XmlEnum("Direct")]
        Direct,

        [XmlEnum("Unknown")]
        Unknown,
    }
}
