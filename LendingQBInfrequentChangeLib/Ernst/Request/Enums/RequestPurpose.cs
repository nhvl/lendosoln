﻿namespace Ernst.Request
{
    using System.Xml.Serialization;

    public enum RequestPurpose
    {
        [XmlEnum("GFE")]
        GFE,

        [XmlEnum("HUD1")]
        HUD1,

        [XmlEnum("HUD1A")]
        HUD1A,

        [XmlEnum("Preliminary")]
        Preliminary,

        [XmlEnum("Pricing")]
        Pricing,
    }
}
