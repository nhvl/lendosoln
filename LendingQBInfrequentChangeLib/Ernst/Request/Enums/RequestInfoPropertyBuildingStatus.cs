﻿namespace Ernst.Request
{
    using System.Xml.Serialization;

    public enum RequestInfoPropertyBuildingStatus
    {
        [XmlEnum("Complete")]
        Complete,

        [XmlEnum("Existing")]
        Existing,

        [XmlEnum("Incomplete")]
        Incomplete,

        [XmlEnum("Proposed")]
        Proposed,

        [XmlEnum("SubjectToAlteration")]
        SubjectToAlteration,

        [XmlEnum("SubjectToAlterationImprovementRepairAndRehabilitation")]
        SubjectToAlterationImprovementRepairAndRehabilitation,

        [XmlEnum("SubstantiallyRehabilitated")]
        SubstantiallyRehabilitated,

        [XmlEnum("UnderConstruction")]
        UnderConstruction,

        [XmlEnum("Other")]
        Other,
    }
}
