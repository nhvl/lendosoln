﻿namespace Ernst.Request
{
    using System.Xml.Serialization;

    public enum CreditRequestRequestType
    {
        [XmlEnum("Individual")]
        Individual,

        [XmlEnum("Joint")]
        Joint,
    }
}
