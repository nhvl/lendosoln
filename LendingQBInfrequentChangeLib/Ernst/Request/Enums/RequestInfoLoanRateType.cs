﻿namespace Ernst.Request
{
    using System.Xml.Serialization;

    public enum RequestInfoLoanRateType
    {
        [XmlEnum("AdjustableRate")]
        AdjustableRate,

        [XmlEnum("Fixed")]
        Fixed,

        [XmlEnum("GEM")]
        GEM,

        [XmlEnum("GPM")]
        GPM,

        [XmlEnum("GraduatedPaymentARM")]
        GraduatedPaymentARM,

        [XmlEnum("RateImprovementMortgage")]
        RateImprovementMortgage,

        [XmlEnum("ReverseMortgage")]
        ReverseMortgage,

        [XmlEnum("Step")]
        Step,

        [XmlEnum("Other")]
        Other,
    }
}
