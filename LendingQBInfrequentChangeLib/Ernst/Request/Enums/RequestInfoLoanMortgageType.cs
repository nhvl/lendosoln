﻿namespace Ernst.Request
{
    using System.Xml.Serialization;

    public enum RequestInfoLoanMortgageType
    {
        [XmlEnum("Conventional")]
        Conventional,

        [XmlEnum("FHA")]
        FHA,

        [XmlEnum("StateAgency")]
        StateAgency,

        [XmlEnum("USDARuralDevelopment")]
        USDARuralDevelopment,

        [XmlEnum("VA")]
        VA,
    }
}
