﻿namespace Ernst.Request
{
    using System.Xml.Serialization;

    public enum DocumentRequestDocumentFormat
    {
        [XmlEnum("PDF")]
        PDF,

        [XmlEnum("XML")]
        XML,
    }
}
