namespace Ernst.Request
{
    using System.Collections.Generic;
    using System.Xml.Serialization;

    public partial class AppraisalRequest
    {
        [XmlElement(Order = 0)]
        public ErnstDecimal Version { get; set; }

        [XmlIgnore]
        public bool VersionSpecified
        {
            get { return this.Version != null; }
            set { }
        }

        [XmlElement(Order = 1)]
        public Options Options { get; set; }

        [XmlIgnore]
        public bool OptionsSpecified
        {
            get { return this.Options != null; }
            set { }
        }

        [XmlElement(Order = 2)]
        public ErnstString ProviderID { get; set; }

        [XmlIgnore]
        public bool ProviderIDSpecified
        {
            get { return this.ProviderID != null; }
            set { }
        }

        [XmlElement(Order = 3)]
        public AppraisalRequestProperty Property { get; set; }

        [XmlIgnore]
        public bool PropertySpecified
        {
            get { return this.Property != null; }
            set { }
        }

        [XmlElement(Order = 4)]
        public ErnstDecimal EstimatedValue { get; set; }

        [XmlIgnore]
        public bool EstimatedValueSpecified
        {
            get { return this.EstimatedValue != null; }
            set { }
        }

        [XmlElement(Order = 5)]
        public ErnstDecimal PurchasePriceAmount { get; set; }

        [XmlIgnore]
        public bool PurchasePriceAmountSpecified
        {
            get { return this.PurchasePriceAmount != null; }
            set { }
        }

        [XmlElement(Order = 6)]
        public ErnstDecimal BaseLoanAmount { get; set; }

        [XmlIgnore]
        public bool BaseLoanAmountSpecified
        {
            get { return this.BaseLoanAmount != null; }
            set { }
        }

        [XmlElement(Order = 7)]
        public ErnstEnum<AppraisalRequestEngagementType> EngagementType { get; set; }

        [XmlIgnore]
        public bool EngagementTypeSpecified
        {
            get { return this.EngagementType != null; }
            set { }
        }

        [XmlElement(Order = 8)]
        public ErnstBoolean AppraisalReportRequested { get; set; }

        [XmlIgnore]
        public bool AppraisalReportRequestedSpecified
        {
            get { return this.AppraisalReportRequested != null; }
            set { }
        }

        [XmlElement(Order = 9)]
        public ErnstString AppraisalReportQuantity { get; set; }

        [XmlIgnore]
        public bool AppraisalReportQuantitySpecified
        {
            get { return this.AppraisalReportQuantity != null; }
            set { }
        }

        [XmlElement(Order = 10)]
        public ErnstBoolean AppraisalOtherRequested { get; set; }

        [XmlIgnore]
        public bool AppraisalOtherRequestedSpecified
        {
            get { return this.AppraisalOtherRequested != null; }
            set { }
        }

        [XmlElement(Order = 11)]
        public List<AppraisalRequestAppraisalProduct> AppraisalProducts { get; set; }

        [XmlIgnore]
        public bool AppraisalProductsSpecified
        {
            get { return this.AppraisalProducts != null && this.AppraisalProducts.Count > 0; }
            set { }
        }

        [XmlElement(Order = 12)]
        public ErnstBoolean AppraisalReportReceived { get; set; }

        [XmlIgnore]
        public bool AppraisalReportReceivedSpecified
        {
            get { return this.AppraisalReportReceived != null; }
            set { }
        }

        [XmlElement(Order = 13)]
        public ErnstBoolean AppraisalReport2Requested { get; set; }

        [XmlIgnore]
        public bool AppraisalReport2RequestedSpecified
        {
            get { return this.AppraisalReport2Requested != null; }
            set { }
        }

        [XmlElement(Order = 14)]
        public ErnstBoolean Form1004D_UpdateReportRequested { get; set; }

        [XmlIgnore]
        public bool Form1004D_UpdateReportRequestedSpecified
        {
            get { return this.Form1004D_UpdateReportRequested != null; }
            set { }
        }

        [XmlElement(Order = 15)]
        public ErnstBoolean Form1004D_UpdateReportReceived { get; set; }

        [XmlIgnore]
        public bool Form1004D_UpdateReportReceivedSpecified
        {
            get { return this.Form1004D_UpdateReportReceived != null; }
            set { }
        }

        [XmlElement(Order = 16)]
        public ErnstBoolean Form1004D_UpdateReport2Requested { get; set; }

        [XmlIgnore]
        public bool Form1004D_UpdateReport2RequestedSpecified
        {
            get { return this.Form1004D_UpdateReport2Requested != null; }
            set { }
        }

        [XmlElement(Order = 17)]
        public ErnstBoolean Form1004D_CompletionReportRequested { get; set; }

        [XmlIgnore]
        public bool Form1004D_CompletionReportRequestedSpecified
        {
            get { return this.Form1004D_CompletionReportRequested != null; }
            set { }
        }

        [XmlElement(Order = 18)]
        public ErnstBoolean Form1004D_CompletionReportReceived { get; set; }

        [XmlIgnore]
        public bool Form1004D_CompletionReportReceivedSpecified
        {
            get { return this.Form1004D_CompletionReportReceived != null; }
            set { }
        }

        [XmlElement(Order = 19)]
        public ErnstBoolean Form1004D_CompletionReport2Requested { get; set; }

        [XmlIgnore]
        public bool Form1004D_CompletionReport2RequestedSpecified
        {
            get { return this.Form1004D_CompletionReport2Requested != null; }
            set { }
        }

        [XmlElement(Order = 20)]
        public ErnstBoolean Form1004D_CompletionReport2Received { get; set; }

        [XmlIgnore]
        public bool Form1004D_CompletionReport2ReceivedSpecified
        {
            get { return this.Form1004D_CompletionReport2Received != null; }
            set { }
        }

        [XmlElement(Order = 21)]
        public ErnstBoolean Form1004D_CompletionReport3Requested { get; set; }

        [XmlIgnore]
        public bool Form1004D_CompletionReport3RequestedSpecified
        {
            get { return this.Form1004D_CompletionReport3Requested != null; }
            set { }
        }

        [XmlElement(Order = 22)]
        public ErnstBoolean Form1004D_DisasterReportRequested { get; set; }

        [XmlIgnore]
        public bool Form1004D_DisasterReportRequestedSpecified
        {
            get { return this.Form1004D_DisasterReportRequested != null; }
            set { }
        }

        [XmlElement(Order = 23)]
        public ErnstBoolean Form1004D_DisasterReportReceived { get; set; }

        [XmlIgnore]
        public bool Form1004D_DisasterReportReceivedSpecified
        {
            get { return this.Form1004D_DisasterReportReceived != null; }
            set { }
        }

        [XmlElement(Order = 24)]
        public ErnstBoolean Form1004D_DisasterReport2Requested { get; set; }

        [XmlIgnore]
        public bool Form1004D_DisasterReport2RequestedSpecified
        {
            get { return this.Form1004D_DisasterReport2Requested != null; }
            set { }
        }
    }
}
