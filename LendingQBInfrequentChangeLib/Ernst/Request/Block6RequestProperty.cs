namespace Ernst.Request
{
    using System.Xml.Serialization;

    public partial class Block6RequestProperty
    {
        [XmlElement(Order = 0)]
        public ErnstString Address1 { get; set; }

        [XmlIgnore]
        public bool Address1Specified
        {
            get { return this.Address1 != null; }
            set { }
        }

        [XmlElement(Order = 1)]
        public ErnstString Address2 { get; set; }

        [XmlIgnore]
        public bool Address2Specified
        {
            get { return this.Address2 != null; }
            set { }
        }

        [XmlElement(Order = 2)]
        public ErnstString City { get; set; }

        [XmlIgnore]
        public bool CitySpecified
        {
            get { return this.City != null; }
            set { }
        }

        [XmlElement(Order = 3)]
        public ErnstString County { get; set; }

        [XmlIgnore]
        public bool CountySpecified
        {
            get { return this.County != null; }
            set { }
        }

        [XmlElement(Order = 4)]
        public ErnstString State { get; set; }

        [XmlIgnore]
        public bool StateSpecified
        {
            get { return this.State != null; }
            set { }
        }

        [XmlElement(Order = 5)]
        public ErnstString Zip { get; set; }

        [XmlIgnore]
        public bool ZipSpecified
        {
            get { return this.Zip != null; }
            set { }
        }

        [XmlElement(Order = 6)]
        public ErnstString LotSize { get; set; }

        [XmlIgnore]
        public bool LotSizeSpecified
        {
            get { return this.LotSize != null; }
            set { }
        }

        [XmlElement(Order = 7)]
        public ErnstString SqFootage { get; set; }

        [XmlIgnore]
        public bool SqFootageSpecified
        {
            get { return this.SqFootage != null; }
            set { }
        }

        [XmlElement(Order = 8)]
        public ErnstString YearBuilt { get; set; }

        [XmlIgnore]
        public bool YearBuiltSpecified
        {
            get { return this.YearBuilt != null; }
            set { }
        }
    }
}
