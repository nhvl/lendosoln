namespace Ernst.Request
{
    using System.Xml.Serialization;

    public partial class Block6Service
    {
        [XmlElement(Order = 0)]
        public ErnstBoolean Requested { get; set; }

        [XmlIgnore]
        public bool RequestedSpecified
        {
            get { return this.Requested != null; }
            set { }
        }
    }
}
