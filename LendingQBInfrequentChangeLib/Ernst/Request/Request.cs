namespace Ernst.Request
{
    using System.Xml.Serialization;

    [XmlRoot]
    public partial class Request
    {
        [XmlElement(Order = 0)]
        public ErnstDecimal Version { get; set; }

        [XmlIgnore]
        public bool VersionSpecified
        {
            get { return this.Version != null; }
            set { }
        }

        [XmlElement(Order = 1)]
        public Options Options { get; set; }

        [XmlIgnore]
        public bool OptionsSpecified
        {
            get { return this.Options != null; }
            set { }
        }

        [XmlElement(Order = 2)]
        public RequestAuthentication Authentication { get; set; }

        [XmlIgnore]
        public bool AuthenticationSpecified
        {
            get { return this.Authentication != null; }
            set { }
        }

        [XmlElement(Order = 3)]
        public ErnstDate TransactionDate { get; set; }

        [XmlIgnore]
        public bool TransactionDateSpecified
        {
            get { return this.TransactionDate != null; }
            set { }
        }

        [XmlElement(Order = 4)]
        public ErnstString ClientTransactionID { get; set; }

        [XmlIgnore]
        public bool ClientTransactionIDSpecified
        {
            get { return this.ClientTransactionID != null; }
            set { }
        }

        [XmlElement(Order = 5)]
        public ErnstString IntegratorTransactionID { get; set; }

        [XmlIgnore]
        public bool IntegratorTransactionIDSpecified
        {
            get { return this.IntegratorTransactionID != null; }
            set { }
        }

        [XmlElement(Order = 6)]
        public ErnstBoolean Guarantee { get; set; }

        [XmlIgnore]
        public bool GuaranteeSpecified
        {
            get { return this.Guarantee != null; }
            set { }
        }

        [XmlElement(Order = 7)]
        public ErnstEnum<RequestPurpose> Purpose { get; set; }

        [XmlIgnore]
        public bool PurposeSpecified
        {
            get { return this.Purpose != null; }
            set { }
        }

        [XmlElement(Order = 8)]
        public RequestInfo RequestInfo { get; set; }

        [XmlIgnore]
        public bool RequestInfoSpecified
        {
            get { return this.RequestInfo != null; }
            set { }
        }

        [XmlElement(Order = 9)]
        public ErnstRequest ErnstRequest { get; set; }

        [XmlIgnore]
        public bool ErnstRequestSpecified
        {
            get { return this.ErnstRequest != null; }
            set { }
        }

        [XmlElement(Order = 10)]
        public TitleRequest TitleRequest { get; set; }

        [XmlIgnore]
        public bool TitleRequestSpecified
        {
            get { return this.TitleRequest != null; }
            set { }
        }

        [XmlElement(Order = 11)]
        public Block6Request Block6Request { get; set; }

        [XmlIgnore]
        public bool Block6RequestSpecified
        {
            get { return this.Block6Request != null; }
            set { }
        }

        [XmlElement(Order = 12)]
        public DocumentRequest DocumentRequest { get; set; }

        [XmlIgnore]
        public bool DocumentRequestSpecified
        {
            get { return this.DocumentRequest != null; }
            set { }
        }

        [XmlElement(Order = 13)]
        public PropertyTaxRequest PropertyTaxRequest { get; set; }

        [XmlIgnore]
        public bool PropertyTaxRequestSpecified
        {
            get { return this.PropertyTaxRequest != null; }
            set { }
        }

        [XmlElement(Order = 14)]
        public AppraisalRequest AppraisalRequest { get; set; }

        [XmlIgnore]
        public bool AppraisalRequestSpecified
        {
            get { return this.AppraisalRequest != null; }
            set { }
        }

        [XmlElement(Order = 15)]
        public CreditRequest CreditRequest { get; set; }

        [XmlIgnore]
        public bool CreditRequestSpecified
        {
            get { return this.CreditRequest != null; }
            set { }
        }

        [XmlElement(Order = 16)]
        public LenderRequest LenderRequest { get; set; }

        [XmlIgnore]
        public bool LenderRequestSpecified
        {
            get { return this.LenderRequest != null; }
            set { }
        }
    }
}
