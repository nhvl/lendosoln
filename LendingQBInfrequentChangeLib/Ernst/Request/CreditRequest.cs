namespace Ernst.Request
{
    using System.Xml.Serialization;

    public partial class CreditRequest
    {
        [XmlElement(Order = 0)]
        public ErnstDecimal Version { get; set; }

        [XmlIgnore]
        public bool VersionSpecified
        {
            get { return this.Version != null; }
            set { }
        }

        [XmlElement(Order = 1)]
        public Options Options { get; set; }

        [XmlIgnore]
        public bool OptionsSpecified
        {
            get { return this.Options != null; }
            set { }
        }

        [XmlElement(Order = 2)]
        public ErnstString ProviderID { get; set; }

        [XmlIgnore]
        public bool ProviderIDSpecified
        {
            get { return this.ProviderID != null; }
            set { }
        }

        [XmlElement(Order = 3)]
        public CreditRequestProperty Property { get; set; }

        [XmlIgnore]
        public bool PropertySpecified
        {
            get { return this.Property != null; }
            set { }
        }

        [XmlElement(Order = 4)]
        public ErnstBoolean CreditRequested { get; set; }

        [XmlIgnore]
        public bool CreditRequestedSpecified
        {
            get { return this.CreditRequested != null; }
            set { }
        }

        [XmlElement(Order = 5)]
        public ErnstEnum<CreditRequestRequestType> RequestType { get; set; }

        [XmlIgnore]
        public bool RequestTypeSpecified
        {
            get { return this.RequestType != null; }
            set { }
        }
    }
}
