namespace Ernst.Request
{
    using System.Xml.Serialization;

    public partial class RequestInfoProperty
    {
        [XmlElement(Order = 0)]
        public ErnstEnum<RequestInfoPropertyAttachmentType> AttachmentType { get; set; }

        [XmlIgnore]
        public bool AttachmentTypeSpecified
        {
            get { return this.AttachmentType != null; }
            set { }
        }

        [XmlElement(Order = 1)]
        public ErnstEnum<RequestInfoPropertyBuildingStatus> BuildingStatus { get; set; }

        [XmlIgnore]
        public bool BuildingStatusSpecified
        {
            get { return this.BuildingStatus != null; }
            set { }
        }

        [XmlElement(Order = 2)]
        public ErnstEnum<RequestInfoPropertyConstructionMethod> ConstructionMethod { get; set; }

        [XmlIgnore]
        public bool ConstructionMethodSpecified
        {
            get { return this.ConstructionMethod != null; }
            set { }
        }

        [XmlElement(Order = 3)]
        public ErnstString NumberOfUnits { get; set; }

        [XmlIgnore]
        public bool NumberOfUnitsSpecified
        {
            get { return this.NumberOfUnits != null; }
            set { }
        }

        [XmlElement(Order = 4)]
        public ErnstDate OriginalPurchaseDate { get; set; }

        [XmlIgnore]
        public bool OriginalPurchaseDateSpecified
        {
            get { return this.OriginalPurchaseDate != null; }
            set { }
        }

        [XmlElement(Order = 5)]
        public ErnstEnum<RequestInfoPropertyProjectLegalStructure> ProjectLegalStructure { get; set; }

        [XmlIgnore]
        public bool ProjectLegalStructureSpecified
        {
            get { return this.ProjectLegalStructure != null; }
            set { }
        }

        [XmlElement(Order = 6)]
        public ErnstEnum<RequestInfoPropertyPropertyUse> PropertyUse { get; set; }

        [XmlIgnore]
        public bool PropertyUseSpecified
        {
            get { return this.PropertyUse != null; }
            set { }
        }

        [XmlElement(Order = 7)]
        public ErnstBoolean PUD { get; set; }

        [XmlIgnore]
        public bool PUDSpecified
        {
            get { return this.PUD != null; }
            set { }
        }
    }
}
