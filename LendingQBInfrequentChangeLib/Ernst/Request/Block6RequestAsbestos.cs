namespace Ernst.Request
{
    using System.Xml.Serialization;

    public partial class Block6RequestAsbestos
    {
        [XmlElement(Order = 0)]
        public ErnstBoolean Requested { get; set; }

        [XmlIgnore]
        public bool RequestedSpecified
        {
            get { return this.Requested != null; }
            set { }
        }

        [XmlElement(Order = 1)]
        public ErnstString Samples { get; set; }

        [XmlIgnore]
        public bool SamplesSpecified
        {
            get { return this.Samples != null; }
            set { }
        }
    }
}
