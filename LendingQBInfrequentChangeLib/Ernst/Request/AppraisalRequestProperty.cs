namespace Ernst.Request
{
    using System.Xml.Serialization;

    public partial class AppraisalRequestProperty
    {
        [XmlElement(Order = 0)]
        public ErnstString City { get; set; }

        [XmlIgnore]
        public bool CitySpecified
        {
            get { return this.City != null; }
            set { }
        }

        [XmlElement(Order = 1)]
        public ErnstString County { get; set; }

        [XmlIgnore]
        public bool CountySpecified
        {
            get { return this.County != null; }
            set { }
        }

        [XmlElement(Order = 2)]
        public ErnstString State { get; set; }

        [XmlIgnore]
        public bool StateSpecified
        {
            get { return this.State != null; }
            set { }
        }

        [XmlElement(Order = 3)]
        public ErnstString Zip { get; set; }

        [XmlIgnore]
        public bool ZipSpecified
        {
            get { return this.Zip != null; }
            set { }
        }
    }
}
