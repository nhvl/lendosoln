namespace Ernst.Request
{
    using System.Xml.Serialization;

    public partial class TitleRequestEndorsement
    {
        [XmlElement(Order = 0)]
        public ErnstString Code { get; set; }

        [XmlIgnore]
        public bool CodeSpecified
        {
            get { return this.Code != null; }
            set { }
        }
    }
}
