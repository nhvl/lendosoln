namespace Ernst.Request
{
    using System.Xml.Serialization;

    public partial class ErnstRequestNumberOfPages
    {
        [XmlElement(Order = 0)]
        public ErnstString Mortgage { get; set; }

        [XmlIgnore]
        public bool MortgageSpecified
        {
            get { return this.Mortgage != null; }
            set { }
        }

        [XmlElement(Order = 1)]
        public ErnstString Deed { get; set; }

        [XmlIgnore]
        public bool DeedSpecified
        {
            get { return this.Deed != null; }
            set { }
        }
    }
}
