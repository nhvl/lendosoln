namespace Ernst.Request
{
    using System.Xml.Serialization;

    public partial class TitlePolicy
    {
        [XmlElement(Order = 0)]
        public ErnstBoolean Requested { get; set; }

        [XmlIgnore]
        public bool RequestedSpecified
        {
            get { return this.Requested != null; }
            set { }
        }

        [XmlElement(Order = 1)]
        public ErnstString PolicyCode { get; set; }

        [XmlIgnore]
        public bool PolicyCodeSpecified
        {
            get { return this.PolicyCode != null; }
            set { }
        }

        [XmlElement(Order = 2)]
        public ErnstDecimal PolicyAmount { get; set; }

        [XmlIgnore]
        public bool PolicyAmountSpecified
        {
            get { return this.PolicyAmount != null; }
            set { }
        }

        [XmlElement(Order = 3)]
        public ErnstString YearsSinceLastPolicy { get; set; }

        [XmlIgnore]
        public bool YearsSinceLastPolicySpecified
        {
            get { return this.YearsSinceLastPolicy != null; }
            set { }
        }

        [XmlElement(Order = 4)]
        public ErnstString PriorPolicyCode { get; set; }

        [XmlIgnore]
        public bool PriorPolicyCodeSpecified
        {
            get { return this.PriorPolicyCode != null; }
            set { }
        }

        [XmlElement(Order = 5)]
        public ErnstDecimal PriorPolicyAmount { get; set; }

        [XmlIgnore]
        public bool PriorPolicyAmountSpecified
        {
            get { return this.PriorPolicyAmount != null; }
            set { }
        }
    }
}
