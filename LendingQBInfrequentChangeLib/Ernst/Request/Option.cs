namespace Ernst.Request
{
    using System.Xml.Serialization;

    public partial class Option
    {
        [XmlElement(Order = 0)]
        public ErnstString Name { get; set; }

        [XmlIgnore]
        public bool NameSpecified
        {
            get { return this.Name != null; }
            set { }
        }

        [XmlElement(Order = 1)]
        public ErnstString Value { get; set; }

        [XmlIgnore]
        public bool ValueSpecified
        {
            get { return this.Value != null; }
            set { }
        }
    }
}
