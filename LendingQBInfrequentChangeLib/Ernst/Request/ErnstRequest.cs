namespace Ernst.Request
{
    using System.Xml.Serialization;

    public partial class ErnstRequest
    {
        [XmlElement(Order = 0)]
        public ErnstDecimal Version { get; set; }

        [XmlIgnore]
        public bool VersionSpecified
        {
            get { return this.Version != null; }
            set { }
        }

        [XmlElement(Order = 1)]
        public Options Options { get; set; }

        [XmlIgnore]
        public bool OptionsSpecified
        {
            get { return this.Options != null; }
            set { }
        }

        [XmlElement(Order = 2)]
        public ErnstString TransactionCode { get; set; }

        [XmlIgnore]
        public bool TransactionCodeSpecified
        {
            get { return this.TransactionCode != null; }
            set { }
        }

        [XmlElement(Order = 3)]
        public Property Property { get; set; }

        [XmlIgnore]
        public bool PropertySpecified
        {
            get { return this.Property != null; }
            set { }
        }

        [XmlElement(Order = 4)]
        public ErnstRequestNumberOfPages NumberOfPages { get; set; }

        [XmlIgnore]
        public bool NumberOfPagesSpecified
        {
            get { return this.NumberOfPages != null; }
            set { }
        }

        [XmlElement(Order = 5)]
        public ErnstRequestMortgage Mortgage { get; set; }

        [XmlIgnore]
        public bool MortgageSpecified
        {
            get { return this.Mortgage != null; }
            set { }
        }

        [XmlElement(Order = 6)]
        public ErnstRequestDeed Deed { get; set; }

        [XmlIgnore]
        public bool DeedSpecified
        {
            get { return this.Deed != null; }
            set { }
        }

        [XmlElement(Order = 7)]
        public ErnstRequestAssignment Assignment { get; set; }

        [XmlIgnore]
        public bool AssignmentSpecified
        {
            get { return this.Assignment != null; }
            set { }
        }

        [XmlElement(Order = 8)]
        public ErnstRequestRelease Release { get; set; }

        [XmlIgnore]
        public bool ReleaseSpecified
        {
            get { return this.Release != null; }
            set { }
        }

        [XmlElement(Order = 9)]
        public ErnstRequestSubordination Subordination { get; set; }

        [XmlIgnore]
        public bool SubordinationSpecified
        {
            get { return this.Subordination != null; }
            set { }
        }

        [XmlElement(Order = 10)]
        public ErnstRequestPOA POA { get; set; }

        [XmlIgnore]
        public bool POASpecified
        {
            get { return this.POA != null; }
            set { }
        }

        [XmlElement(Order = 11)]
        public ErnstDate EstimatedClosingDate { get; set; }

        [XmlIgnore]
        public bool EstimatedClosingDateSpecified
        {
            get { return this.EstimatedClosingDate != null; }
            set { }
        }
    }
}
