namespace Ernst.Request
{
    using System.Xml.Serialization;

    public partial class ErnstRequestAssignment
    {
        [XmlElement(Order = 0)]
        public ErnstString Pages { get; set; }

        [XmlIgnore]
        public bool PagesSpecified
        {
            get { return this.Pages != null; }
            set { }
        }

        [XmlElement(Order = 1)]
        public ErnstString NumberOfAssignments { get; set; }

        [XmlIgnore]
        public bool NumberOfAssignmentsSpecified
        {
            get { return this.NumberOfAssignments != null; }
            set { }
        }

        [XmlElement(Order = 2)]
        public ErnstIndex Index { get; set; }

        [XmlIgnore]
        public bool IndexSpecified
        {
            get { return this.Index != null; }
            set { }
        }
    }
}
