﻿namespace Ernst.Request
{
    using System.Collections.Generic;
    using System.Xml.Serialization;

    public partial class Options
    {
        [XmlElement("Option", Order = 0)]
        public List<Option> OptionList { get; set; }

        [XmlIgnore]
        public bool OptionListSpecified
        {
            get { return this.OptionList != null; }
            set { }
        }
    }
}
