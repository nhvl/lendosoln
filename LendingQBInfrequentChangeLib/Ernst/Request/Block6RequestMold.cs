namespace Ernst.Request
{
    using System.Xml.Serialization;

    public partial class Block6RequestMold
    {
        [XmlElement(Order = 0)]
        public ErnstBoolean Requested { get; set; }

        [XmlIgnore]
        public bool RequestedSpecified
        {
            get { return this.Requested != null; }
            set { }
        }

        [XmlElement(Order = 1)]
        public ErnstString AirSamples { get; set; }

        [XmlIgnore]
        public bool AirSamplesSpecified
        {
            get { return this.AirSamples != null; }
            set { }
        }

        [XmlElement(Order = 2)]
        public ErnstString SurfaceSamples { get; set; }

        [XmlIgnore]
        public bool SurfaceSamplesSpecified
        {
            get { return this.SurfaceSamples != null; }
            set { }
        }
    }
}
