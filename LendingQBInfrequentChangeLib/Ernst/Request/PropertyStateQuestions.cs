namespace Ernst.Request
{
    using System.Xml.Serialization;

    public partial class PropertyStateQuestions
    {
        [XmlElement(Order = 0)]
        public ErnstInteger Q1 { get; set; }

        [XmlIgnore]
        public bool Q1Specified
        {
            get { return this.Q1 != null; }
            set { }
        }

        [XmlElement(Order = 1)]
        public ErnstInteger Q2 { get; set; }

        [XmlIgnore]
        public bool Q2Specified
        {
            get { return this.Q2 != null; }
            set { }
        }

        [XmlElement(Order = 2)]
        public ErnstInteger Q3 { get; set; }

        [XmlIgnore]
        public bool Q3Specified
        {
            get { return this.Q3 != null; }
            set { }
        }

        [XmlElement(Order = 3)]
        public ErnstInteger Q4 { get; set; }

        [XmlIgnore]
        public bool Q4Specified
        {
            get { return this.Q4 != null; }
            set { }
        }

        [XmlElement(Order = 4)]
        public ErnstInteger Q5 { get; set; }

        [XmlIgnore]
        public bool Q5Specified
        {
            get { return this.Q5 != null; }
            set { }
        }

        [XmlElement(Order = 5)]
        public ErnstInteger Q6 { get; set; }

        [XmlIgnore]
        public bool Q6Specified
        {
            get { return this.Q6 != null; }
            set { }
        }

        [XmlElement(Order = 6)]
        public ErnstInteger Q7 { get; set; }

        [XmlIgnore]
        public bool Q7Specified
        {
            get { return this.Q7 != null; }
            set { }
        }

        [XmlElement(Order = 7)]
        public ErnstInteger Q8 { get; set; }

        [XmlIgnore]
        public bool Q8Specified
        {
            get { return this.Q8 != null; }
            set { }
        }

        [XmlElement(Order = 8)]
        public ErnstInteger Q9 { get; set; }

        [XmlIgnore]
        public bool Q9Specified
        {
            get { return this.Q9 != null; }
            set { }
        }

        [XmlElement(Order = 9)]
        public ErnstInteger Q10 { get; set; }

        [XmlIgnore]
        public bool Q10Specified
        {
            get { return this.Q10 != null; }
            set { }
        }

        [XmlElement(Order = 10)]
        public ErnstInteger Q11 { get; set; }

        [XmlIgnore]
        public bool Q11Specified
        {
            get { return this.Q11 != null; }
            set { }
        }

        [XmlElement(Order = 11)]
        public ErnstInteger Q12 { get; set; }

        [XmlIgnore]
        public bool Q12Specified
        {
            get { return this.Q12 != null; }
            set { }
        }

        [XmlElement(Order = 12)]
        public ErnstInteger Q13 { get; set; }

        [XmlIgnore]
        public bool Q13Specified
        {
            get { return this.Q13 != null; }
            set { }
        }

        [XmlElement(Order = 13)]
        public ErnstInteger Q14 { get; set; }

        [XmlIgnore]
        public bool Q14Specified
        {
            get { return this.Q14 != null; }
            set { }
        }

        [XmlElement(Order = 14)]
        public ErnstInteger Q15 { get; set; }

        [XmlIgnore]
        public bool Q15Specified
        {
            get { return this.Q15 != null; }
            set { }
        }

        [XmlElement(Order = 15)]
        public ErnstInteger Q16 { get; set; }

        [XmlIgnore]
        public bool Q16Specified
        {
            get { return this.Q16 != null; }
            set { }
        }

        [XmlElement(Order = 16)]
        public ErnstInteger Q17 { get; set; }

        [XmlIgnore]
        public bool Q17Specified
        {
            get { return this.Q17 != null; }
            set { }
        }

        [XmlElement(Order = 17)]
        public ErnstInteger Q18 { get; set; }

        [XmlIgnore]
        public bool Q18Specified
        {
            get { return this.Q18 != null; }
            set { }
        }

        [XmlElement(Order = 18)]
        public ErnstInteger Q19 { get; set; }

        [XmlIgnore]
        public bool Q19Specified
        {
            get { return this.Q19 != null; }
            set { }
        }

        [XmlElement(Order = 19)]
        public ErnstInteger Q20 { get; set; }

        [XmlIgnore]
        public bool Q20Specified
        {
            get { return this.Q20 != null; }
            set { }
        }

        [XmlElement(Order = 20)]
        public ErnstInteger Q21 { get; set; }

        [XmlIgnore]
        public bool Q21Specified
        {
            get { return this.Q21 != null; }
            set { }
        }

        [XmlElement(Order = 21)]
        public ErnstInteger Q22 { get; set; }

        [XmlIgnore]
        public bool Q22Specified
        {
            get { return this.Q22 != null; }
            set { }
        }

        [XmlElement(Order = 22)]
        public ErnstInteger Q23 { get; set; }

        [XmlIgnore]
        public bool Q23Specified
        {
            get { return this.Q23 != null; }
            set { }
        }

        [XmlElement(Order = 23)]
        public ErnstInteger Q24 { get; set; }

        [XmlIgnore]
        public bool Q24Specified
        {
            get { return this.Q24 != null; }
            set { }
        }

        [XmlElement(Order = 24)]
        public ErnstInteger Q25 { get; set; }

        [XmlIgnore]
        public bool Q25Specified
        {
            get { return this.Q25 != null; }
            set { }
        }

        [XmlElement(Order = 25)]
        public ErnstInteger Q26 { get; set; }

        [XmlIgnore]
        public bool Q26Specified
        {
            get { return this.Q26 != null; }
            set { }
        }

        [XmlElement(Order = 26)]
        public ErnstInteger Q27 { get; set; }

        [XmlIgnore]
        public bool Q27Specified
        {
            get { return this.Q27 != null; }
            set { }
        }

        [XmlElement(Order = 27)]
        public ErnstInteger Q28 { get; set; }

        [XmlIgnore]
        public bool Q28Specified
        {
            get { return this.Q28 != null; }
            set { }
        }

        [XmlElement(Order = 28)]
        public ErnstInteger Q29 { get; set; }

        [XmlIgnore]
        public bool Q29Specified
        {
            get { return this.Q29 != null; }
            set { }
        }

        [XmlElement(Order = 29)]
        public ErnstInteger Q30 { get; set; }

        [XmlIgnore]
        public bool Q30Specified
        {
            get { return this.Q30 != null; }
            set { }
        }

        [XmlElement(Order = 30)]
        public ErnstInteger Q31 { get; set; }

        [XmlIgnore]
        public bool Q31Specified
        {
            get { return this.Q31 != null; }
            set { }
        }

        [XmlElement(Order = 31)]
        public ErnstInteger Q32 { get; set; }

        [XmlIgnore]
        public bool Q32Specified
        {
            get { return this.Q32 != null; }
            set { }
        }

        [XmlElement(Order = 32)]
        public ErnstInteger Q33 { get; set; }

        [XmlIgnore]
        public bool Q33Specified
        {
            get { return this.Q33 != null; }
            set { }
        }

        [XmlElement(Order = 33)]
        public ErnstInteger Q34 { get; set; }

        [XmlIgnore]
        public bool Q34Specified
        {
            get { return this.Q34 != null; }
            set { }
        }

        [XmlElement(Order = 34)]
        public ErnstInteger Q35 { get; set; }

        [XmlIgnore]
        public bool Q35Specified
        {
            get { return this.Q35 != null; }
            set { }
        }

        [XmlElement(Order = 35)]
        public ErnstInteger Q36 { get; set; }

        [XmlIgnore]
        public bool Q36Specified
        {
            get { return this.Q36 != null; }
            set { }
        }

        [XmlElement(Order = 36)]
        public ErnstInteger Q37 { get; set; }

        [XmlIgnore]
        public bool Q37Specified
        {
            get { return this.Q37 != null; }
            set { }
        }

        [XmlElement(Order = 37)]
        public ErnstInteger Q38 { get; set; }

        [XmlIgnore]
        public bool Q38Specified
        {
            get { return this.Q38 != null; }
            set { }
        }

        [XmlElement(Order = 38)]
        public ErnstInteger Q39 { get; set; }

        [XmlIgnore]
        public bool Q39Specified
        {
            get { return this.Q39 != null; }
            set { }
        }

        [XmlElement(Order = 39)]
        public ErnstInteger Q40 { get; set; }

        [XmlIgnore]
        public bool Q40Specified
        {
            get { return this.Q40 != null; }
            set { }
        }

        [XmlElement(Order = 40)]
        public ErnstInteger Q41 { get; set; }

        [XmlIgnore]
        public bool Q41Specified
        {
            get { return this.Q41 != null; }
            set { }
        }

        [XmlElement(Order = 41)]
        public ErnstInteger Q42 { get; set; }

        [XmlIgnore]
        public bool Q42Specified
        {
            get { return this.Q42 != null; }
            set { }
        }

        [XmlElement(Order = 42)]
        public ErnstInteger Q43 { get; set; }

        [XmlIgnore]
        public bool Q43Specified
        {
            get { return this.Q43 != null; }
            set { }
        }

        [XmlElement(Order = 43)]
        public ErnstInteger Q44 { get; set; }

        [XmlIgnore]
        public bool Q44Specified
        {
            get { return this.Q44 != null; }
            set { }
        }

        [XmlElement(Order = 44)]
        public ErnstInteger Q45 { get; set; }

        [XmlIgnore]
        public bool Q45Specified
        {
            get { return this.Q45 != null; }
            set { }
        }

        [XmlElement(Order = 45)]
        public ErnstInteger Q46 { get; set; }

        [XmlIgnore]
        public bool Q46Specified
        {
            get { return this.Q46 != null; }
            set { }
        }

        [XmlElement(Order = 46)]
        public ErnstInteger Q47 { get; set; }

        [XmlIgnore]
        public bool Q47Specified
        {
            get { return this.Q47 != null; }
            set { }
        }

        [XmlElement(Order = 47)]
        public ErnstInteger Q48 { get; set; }

        [XmlIgnore]
        public bool Q48Specified
        {
            get { return this.Q48 != null; }
            set { }
        }

        [XmlElement(Order = 48)]
        public ErnstInteger Q49 { get; set; }

        [XmlIgnore]
        public bool Q49Specified
        {
            get { return this.Q49 != null; }
            set { }
        }

        [XmlElement(Order = 49)]
        public ErnstInteger Q50 { get; set; }

        [XmlIgnore]
        public bool Q50Specified
        {
            get { return this.Q50 != null; }
            set { }
        }

        [XmlElement(Order = 50)]
        public ErnstInteger V1 { get; set; }

        [XmlIgnore]
        public bool V1Specified
        {
            get { return this.V1 != null; }
            set { }
        }

        [XmlElement(Order = 51)]
        public ErnstInteger V2 { get; set; }

        [XmlIgnore]
        public bool V2Specified
        {
            get { return this.V2 != null; }
            set { }
        }

        [XmlElement(Order = 52)]
        public ErnstInteger V3 { get; set; }

        [XmlIgnore]
        public bool V3Specified
        {
            get { return this.V3 != null; }
            set { }
        }

        [XmlElement(Order = 53)]
        public ErnstInteger V4 { get; set; }

        [XmlIgnore]
        public bool V4Specified
        {
            get { return this.V4 != null; }
            set { }
        }

        [XmlElement(Order = 54)]
        public ErnstInteger V5 { get; set; }

        [XmlIgnore]
        public bool V5Specified
        {
            get { return this.V5 != null; }
            set { }
        }

        [XmlElement(Order = 55)]
        public ErnstInteger V6 { get; set; }

        [XmlIgnore]
        public bool V6Specified
        {
            get { return this.V6 != null; }
            set { }
        }

        [XmlElement(Order = 56)]
        public ErnstInteger V7 { get; set; }

        [XmlIgnore]
        public bool V7Specified
        {
            get { return this.V7 != null; }
            set { }
        }

        [XmlElement(Order = 57)]
        public ErnstInteger V8 { get; set; }

        [XmlIgnore]
        public bool V8Specified
        {
            get { return this.V8 != null; }
            set { }
        }

        [XmlElement(Order = 58)]
        public ErnstInteger V9 { get; set; }

        [XmlIgnore]
        public bool V9Specified
        {
            get { return this.V9 != null; }
            set { }
        }

        [XmlElement(Order = 59)]
        public ErnstInteger V10 { get; set; }

        [XmlIgnore]
        public bool V10Specified
        {
            get { return this.V10 != null; }
            set { }
        }

        [XmlElement(Order = 60)]
        public ErnstInteger V11 { get; set; }

        [XmlIgnore]
        public bool V11Specified
        {
            get { return this.V11 != null; }
            set { }
        }

        [XmlElement(Order = 61)]
        public ErnstInteger V12 { get; set; }

        [XmlIgnore]
        public bool V12Specified
        {
            get { return this.V12 != null; }
            set { }
        }

        [XmlElement(Order = 62)]
        public ErnstInteger V13 { get; set; }

        [XmlIgnore]
        public bool V13Specified
        {
            get { return this.V13 != null; }
            set { }
        }

        [XmlElement(Order = 63)]
        public ErnstInteger V14 { get; set; }

        [XmlIgnore]
        public bool V14Specified
        {
            get { return this.V14 != null; }
            set { }
        }

        [XmlElement(Order = 64)]
        public ErnstInteger V15 { get; set; }

        [XmlIgnore]
        public bool V15Specified
        {
            get { return this.V15 != null; }
            set { }
        }

        [XmlElement(Order = 65)]
        public ErnstInteger V16 { get; set; }

        [XmlIgnore]
        public bool V16Specified
        {
            get { return this.V16 != null; }
            set { }
        }

        [XmlElement(Order = 66)]
        public ErnstInteger V17 { get; set; }

        [XmlIgnore]
        public bool V17Specified
        {
            get { return this.V17 != null; }
            set { }
        }

        [XmlElement(Order = 67)]
        public ErnstInteger V18 { get; set; }

        [XmlIgnore]
        public bool V18Specified
        {
            get { return this.V18 != null; }
            set { }
        }

        [XmlElement(Order = 68)]
        public ErnstInteger V19 { get; set; }

        [XmlIgnore]
        public bool V19Specified
        {
            get { return this.V19 != null; }
            set { }
        }

        [XmlElement(Order = 69)]
        public ErnstInteger V20 { get; set; }

        [XmlIgnore]
        public bool V20Specified
        {
            get { return this.V20 != null; }
            set { }
        }

        [XmlElement(Order = 70)]
        public ErnstInteger V21 { get; set; }

        [XmlIgnore]
        public bool V21Specified
        {
            get { return this.V21 != null; }
            set { }
        }

        [XmlElement(Order = 71)]
        public ErnstInteger V22 { get; set; }

        [XmlIgnore]
        public bool V22Specified
        {
            get { return this.V22 != null; }
            set { }
        }

        [XmlElement(Order = 72)]
        public ErnstInteger V23 { get; set; }

        [XmlIgnore]
        public bool V23Specified
        {
            get { return this.V23 != null; }
            set { }
        }

        [XmlElement(Order = 73)]
        public ErnstInteger V24 { get; set; }

        [XmlIgnore]
        public bool V24Specified
        {
            get { return this.V24 != null; }
            set { }
        }

        [XmlElement(Order = 74)]
        public ErnstInteger V25 { get; set; }

        [XmlIgnore]
        public bool V25Specified
        {
            get { return this.V25 != null; }
            set { }
        }

        [XmlElement(Order = 75)]
        public ErnstInteger V26 { get; set; }

        [XmlIgnore]
        public bool V26Specified
        {
            get { return this.V26 != null; }
            set { }
        }

        [XmlElement(Order = 76)]
        public ErnstInteger V27 { get; set; }

        [XmlIgnore]
        public bool V27Specified
        {
            get { return this.V27 != null; }
            set { }
        }

        [XmlElement(Order = 77)]
        public ErnstInteger V28 { get; set; }

        [XmlIgnore]
        public bool V28Specified
        {
            get { return this.V28 != null; }
            set { }
        }

        [XmlElement(Order = 78)]
        public ErnstInteger V29 { get; set; }

        [XmlIgnore]
        public bool V29Specified
        {
            get { return this.V29 != null; }
            set { }
        }

        [XmlElement(Order = 79)]
        public ErnstInteger V30 { get; set; }

        [XmlIgnore]
        public bool V30Specified
        {
            get { return this.V30 != null; }
            set { }
        }

        [XmlElement(Order = 80)]
        public ErnstInteger V31 { get; set; }

        [XmlIgnore]
        public bool V31Specified
        {
            get { return this.V31 != null; }
            set { }
        }

        [XmlElement(Order = 81)]
        public ErnstInteger V32 { get; set; }

        [XmlIgnore]
        public bool V32Specified
        {
            get { return this.V32 != null; }
            set { }
        }

        [XmlElement(Order = 82)]
        public ErnstInteger V33 { get; set; }

        [XmlIgnore]
        public bool V33Specified
        {
            get { return this.V33 != null; }
            set { }
        }

        [XmlElement(Order = 83)]
        public ErnstInteger V34 { get; set; }

        [XmlIgnore]
        public bool V34Specified
        {
            get { return this.V34 != null; }
            set { }
        }

        [XmlElement(Order = 84)]
        public ErnstInteger V35 { get; set; }

        [XmlIgnore]
        public bool V35Specified
        {
            get { return this.V35 != null; }
            set { }
        }

        [XmlElement(Order = 85)]
        public ErnstInteger V36 { get; set; }

        [XmlIgnore]
        public bool V36Specified
        {
            get { return this.V36 != null; }
            set { }
        }

        [XmlElement(Order = 86)]
        public ErnstInteger V37 { get; set; }

        [XmlIgnore]
        public bool V37Specified
        {
            get { return this.V37 != null; }
            set { }
        }

        [XmlElement(Order = 87)]
        public ErnstInteger V38 { get; set; }

        [XmlIgnore]
        public bool V38Specified
        {
            get { return this.V38 != null; }
            set { }
        }

        [XmlElement(Order = 88)]
        public ErnstInteger V39 { get; set; }

        [XmlIgnore]
        public bool V39Specified
        {
            get { return this.V39 != null; }
            set { }
        }

        [XmlElement(Order = 89)]
        public ErnstInteger V40 { get; set; }

        [XmlIgnore]
        public bool V40Specified
        {
            get { return this.V40 != null; }
            set { }
        }

        [XmlElement(Order = 90)]
        public ErnstInteger V41 { get; set; }

        [XmlIgnore]
        public bool V41Specified
        {
            get { return this.V41 != null; }
            set { }
        }

        [XmlElement(Order = 91)]
        public ErnstInteger V42 { get; set; }

        [XmlIgnore]
        public bool V42Specified
        {
            get { return this.V42 != null; }
            set { }
        }

        [XmlElement(Order = 92)]
        public ErnstInteger V43 { get; set; }

        [XmlIgnore]
        public bool V43Specified
        {
            get { return this.V43 != null; }
            set { }
        }

        [XmlElement(Order = 93)]
        public ErnstInteger V44 { get; set; }

        [XmlIgnore]
        public bool V44Specified
        {
            get { return this.V44 != null; }
            set { }
        }

        [XmlElement(Order = 94)]
        public ErnstInteger V45 { get; set; }

        [XmlIgnore]
        public bool V45Specified
        {
            get { return this.V45 != null; }
            set { }
        }

        [XmlElement(Order = 95)]
        public ErnstInteger V46 { get; set; }

        [XmlIgnore]
        public bool V46Specified
        {
            get { return this.V46 != null; }
            set { }
        }

        [XmlElement(Order = 96)]
        public ErnstInteger V47 { get; set; }

        [XmlIgnore]
        public bool V47Specified
        {
            get { return this.V47 != null; }
            set { }
        }

        [XmlElement(Order = 97)]
        public ErnstInteger V48 { get; set; }

        [XmlIgnore]
        public bool V48Specified
        {
            get { return this.V48 != null; }
            set { }
        }

        [XmlElement(Order = 98)]
        public ErnstInteger V49 { get; set; }

        [XmlIgnore]
        public bool V49Specified
        {
            get { return this.V49 != null; }
            set { }
        }

        [XmlElement(Order = 99)]
        public ErnstInteger V50 { get; set; }

        [XmlIgnore]
        public bool V50Specified
        {
            get { return this.V50 != null; }
            set { }
        }
    }
}
