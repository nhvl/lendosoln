﻿namespace Ernst.Response
{
    using System.Collections.Generic;
    using System.Xml.Serialization;

    public partial class ProvidersXml
    {
        [XmlElement("Provider")]
        public List<DocumentResponseProvider> ProviderList { get; set; }

        [XmlIgnore]
        public bool ProviderListSpecified
        {
            get { return this.ProviderList != null; }
            set { }
        }
    }
}
