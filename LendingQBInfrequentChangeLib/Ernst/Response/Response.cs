namespace Ernst.Response
{
    using System.Xml.Serialization;

    [XmlRoot]
    public partial class Response
    {
        [XmlElement]
        public ErnstString TransactionID { get; set; }

        [XmlIgnore]
        public bool TransactionIDSpecified
        {
            get { return this.TransactionID != null; }
            set { }
        }

        [XmlElement]
        public ErnstString IntegratorTransactionID { get; set; }

        [XmlIgnore]
        public bool IntegratorTransactionIDSpecified
        {
            get { return this.IntegratorTransactionID != null; }
            set { }
        }

        [XmlElement]
        public ErnstResponse ErnstResponse { get; set; }

        [XmlIgnore]
        public bool ErnstResponseSpecified
        {
            get { return this.ErnstResponse != null; }
            set { }
        }

        [XmlElement]
        public TitleResponse TitleResponse { get; set; }

        [XmlIgnore]
        public bool TitleResponseSpecified
        {
            get { return this.TitleResponse != null; }
            set { }
        }

        [XmlElement]
        public Block6Response Block6Response { get; set; }

        [XmlIgnore]
        public bool Block6ResponseSpecified
        {
            get { return this.Block6Response != null; }
            set { }
        }

        [XmlElement]
        public DocumentResponse DocumentResponse { get; set; }

        [XmlIgnore]
        public bool DocumentResponseSpecified
        {
            get { return this.DocumentResponse != null; }
            set { }
        }

        [XmlElement]
        public PropertyTaxResponse PropertyTaxResponse { get; set; }

        [XmlIgnore]
        public bool PropertyTaxResponseSpecified
        {
            get { return this.PropertyTaxResponse != null; }
            set { }
        }

        [XmlElement]
        public AppraisalResponse AppraisalResponse { get; set; }

        [XmlIgnore]
        public bool AppraisalResponseSpecified
        {
            get { return this.AppraisalResponse != null; }
            set { }
        }

        [XmlElement]
        public CreditResponse CreditResponse { get; set; }

        [XmlIgnore]
        public bool CreditResponseSpecified
        {
            get { return this.CreditResponse != null; }
            set { }
        }

        [XmlElement]
        public LenderResponse LenderResponse { get; set; }

        [XmlIgnore]
        public bool LenderResponseSpecified
        {
            get { return this.LenderResponse != null; }
            set { }
        }

        [XmlElement]
        public Error Error { get; set; }

        [XmlIgnore]
        public bool ErrorSpecified
        {
            get { return this.Error != null; }
            set { }
        }
    }
}
