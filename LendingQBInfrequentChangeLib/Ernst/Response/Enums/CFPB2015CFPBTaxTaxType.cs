﻿namespace Ernst.Response
{
    using System.Xml.Serialization;

    public enum CFPB2015CFPBTaxTaxType
    {
        [XmlEnum("MortageTax")]
        MortgageTax,

        [XmlEnum("DeedTax")]
        DeedTax,

        [XmlEnum("MortgageIntangibleTax")]
        MortgageIntangibleTax,

        [XmlEnum("DeedIntangibleTax")]
        DeedIntangibleTax,

        [XmlEnum("MortgageRecordationTax")]
        MortgageRecordationTax,

        [XmlEnum("DeedRecordationTax")]
        DeedRecordationTax,

        [XmlEnum("MortgageConservationFund")]
        MortgageConservationFund,

        [XmlEnum("DeedConservationFund")]
        DeedConservationFund,

        [XmlEnum("MansionTax")]
        MansionTax,

        [XmlEnum("GrantorTax")]
        GrantorTax,

        [XmlEnum("GranteeTax")]
        GranteeTax,

        [XmlEnum("LandBankTax")]
        LandBankTax,

        [XmlEnum("SalesDisclosureFormFee")]
        SalesDisclosureFormFee,

        [XmlEnum("AuditorTransferFee")]
        AuditorTransferFee,
    }
}
