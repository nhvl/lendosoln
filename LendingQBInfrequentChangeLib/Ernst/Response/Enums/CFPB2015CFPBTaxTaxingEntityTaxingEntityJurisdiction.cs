﻿namespace Ernst.Response
{
    using System.Xml.Serialization;

    public enum CFPB2015CFPBTaxTaxingEntityTaxingEntityJurisdiction
    {
        [XmlEnum("State")]
        State,

        [XmlEnum("County")]
        County,

        [XmlEnum("City")]
        City,
    }
}
