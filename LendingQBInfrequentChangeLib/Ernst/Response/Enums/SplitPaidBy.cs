﻿namespace Ernst.Response
{
    using System.Xml.Serialization;

    public enum SplitPaidBy
    {
        [XmlEnum("Broker")]
        Broker,

        [XmlEnum("Buyer")]
        Buyer,

        [XmlEnum("Correspondent")]
        Correspondent,

        [XmlEnum("Lender")]
        Lender,

        [XmlEnum("Seller")]
        Seller,

        [XmlEnum("ThirdParty")]
        ThirdParty,

        [XmlEnum("Underwriter")]
        Underwriter,

        [XmlEnum("Agent")]
        Agent,
    }
}
