﻿namespace Ernst.Response
{
    using System.Xml.Serialization;

    public enum DocumentResponseProviderType
    {
        [XmlEnum("Title")]
        Title,

        [XmlEnum("Standard")]
        Standard,

        [XmlEnum("Pest")]
        Pest,

        [XmlEnum("Water")]
        Water,

        [XmlEnum("Septic")]
        Septic,

        [XmlEnum("Well")]
        Well,

        [XmlEnum("Smoke")]
        Smoke,

        [XmlEnum("Radon")]
        Radon,

        [XmlEnum("Roof")]
        Roof,

        [XmlEnum("Structural")]
        Structural,

        [XmlEnum("Survey")]
        Survey,

        [XmlEnum("Mold")]
        Mold,

        [XmlEnum("ManufacturedHousing")]
        ManufacturedHousing,

        [XmlEnum("Asbestos")]
        Asbestos,

        [XmlEnum("Lead")]
        Lead,

        [XmlEnum("Drywall")]
        Drywall,

        [XmlEnum("NaturalHazardDisclosure")]
        NaturalHazardDisclosure,

        [XmlEnum("GeoTechnicalEngineering")]
        GeoTechnicalEngineering,

        [XmlEnum("Foundation")]
        Foundation,

        [XmlEnum("Electrical")]
        Electrical,

        [XmlEnum("Plumbing")]
        Plumbing,

        [XmlEnum("HeatingCooling")]
        HeatingCooling,
    }
}
