﻿namespace Ernst.Response
{
    using System.Xml.Serialization;

    public enum DocumentFormat
    {
        [XmlEnum("PDF")]
        PDF,
    }
}
