﻿namespace Ernst.Response
{
    using System.Xml.Serialization;

    public enum DocumentEncoding
    {
        [XmlEnum("Base64")]
        Base64,
    }
}
