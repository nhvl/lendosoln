﻿namespace Ernst.Response
{
    using System.Collections.Generic;
    using System.Xml.Serialization;

    public partial class RecordingFees
    {
        [XmlElement]
        public ErnstString Heading { get; set; }

        [XmlIgnore]
        public bool HeadingSpecified
        {
            get { return this.Heading != null; }
            set { }
        }

        [XmlElement("Fee")]
        public List<RecordingFee> FeeList { get; set; }

        [XmlIgnore]
        public bool FeeListSpecified
        {
            get { return this.FeeList != null; }
            set { }
        }

        [XmlElement]
        public Error Error { get; set; }

        [XmlIgnore]
        public bool ErrorSpecified
        {
            get {  return this.Error != null; }
            set { }
        }
    }
}
