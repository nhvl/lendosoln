namespace Ernst.Response
{
    using System.Xml.Serialization;

    public partial class DataRefacts
    {
        [XmlElement]
        public ErnstString STABBR { get; set; }

        [XmlIgnore]
        public bool STABBRSpecified
        {
            get { return this.STABBR != null; }
            set { }
        }

        [XmlElement]
        public ErnstString ROFFICE { get; set; }

        [XmlIgnore]
        public bool ROFFICESpecified
        {
            get { return this.ROFFICE != null; }
            set { }
        }

        [XmlElement]
        public ErnstString COUNTY { get; set; }

        [XmlIgnore]
        public bool COUNTYSpecified
        {
            get { return this.COUNTY != null; }
            set { }
        }

        [XmlElement]
        public ErnstString PAGE { get; set; }

        [XmlIgnore]
        public bool PAGESpecified
        {
            get { return this.PAGE != null; }
            set { }
        }

        [XmlElement]
        public ErnstString PAGEKEY2 { get; set; }

        [XmlIgnore]
        public bool PAGEKEY2Specified
        {
            get { return this.PAGEKEY2 != null; }
            set { }
        }

        [XmlElement]
        public ErnstString PHONE { get; set; }

        [XmlIgnore]
        public bool PHONESpecified
        {
            get { return this.PHONE != null; }
            set { }
        }

        [XmlElement]
        public ErnstString EXT { get; set; }

        [XmlIgnore]
        public bool EXTSpecified
        {
            get { return this.EXT != null; }
            set { }
        }

        [XmlElement]
        public ErnstString FAX { get; set; }

        [XmlIgnore]
        public bool FAXSpecified
        {
            get { return this.FAX != null; }
            set { }
        }

        [XmlElement]
        public ErnstString TIMEZONE { get; set; }

        [XmlIgnore]
        public bool TIMEZONESpecified
        {
            get { return this.TIMEZONE != null; }
            set { }
        }

        [XmlElement]
        public ErnstString ATTN { get; set; }

        [XmlIgnore]
        public bool ATTNSpecified
        {
            get { return this.ATTN != null; }
            set { }
        }

        [XmlElement]
        public ErnstString DEPT { get; set; }

        [XmlIgnore]
        public bool DEPTSpecified
        {
            get { return this.DEPT != null; }
            set { }
        }

        [XmlElement]
        public ErnstString PAYEE { get; set; }

        [XmlIgnore]
        public bool PAYEESpecified
        {
            get { return this.PAYEE != null; }
            set { }
        }

        [XmlElement]
        public ErnstString FEEPAY { get; set; }

        [XmlIgnore]
        public bool FEEPAYSpecified
        {
            get { return this.FEEPAY != null; }
            set { }
        }

        [XmlElement]
        public ErnstString MADDR1 { get; set; }

        [XmlIgnore]
        public bool MADDR1Specified
        {
            get { return this.MADDR1 != null; }
            set { }
        }

        [XmlElement]
        public ErnstString MADDR2 { get; set; }

        [XmlIgnore]
        public bool MADDR2Specified
        {
            get { return this.MADDR2 != null; }
            set { }
        }

        [XmlElement]
        public ErnstString MCITYST { get; set; }

        [XmlIgnore]
        public bool MCITYSTSpecified
        {
            get { return this.MCITYST != null; }
            set { }
        }

        [XmlElement]
        public ErnstString MCITY { get; set; }

        [XmlIgnore]
        public bool MCITYSpecified
        {
            get { return this.MCITY != null; }
            set { }
        }

        [XmlElement]
        public ErnstString MZIP { get; set; }

        [XmlIgnore]
        public bool MZIPSpecified
        {
            get { return this.MZIP != null; }
            set { }
        }

        [XmlElement]
        public ErnstString CADDR1 { get; set; }

        [XmlIgnore]
        public bool CADDR1Specified
        {
            get { return this.CADDR1 != null; }
            set { }
        }

        [XmlElement]
        public ErnstString CADDR2 { get; set; }

        [XmlIgnore]
        public bool CADDR2Specified
        {
            get { return this.CADDR2 != null; }
            set { }
        }

        [XmlElement]
        public ErnstString CCITYST { get; set; }

        [XmlIgnore]
        public bool CCITYSTSpecified
        {
            get { return this.CCITYST != null; }
            set { }
        }

        [XmlElement]
        public ErnstString CCITY { get; set; }

        [XmlIgnore]
        public bool CCITYSpecified
        {
            get { return this.CCITY != null; }
            set { }
        }

        [XmlElement]
        public ErnstString CZIP { get; set; }

        [XmlIgnore]
        public bool CZIPSpecified
        {
            get { return this.CZIP != null; }
            set { }
        }

        [XmlElement]
        public ErnstString HOURS { get; set; }

        [XmlIgnore]
        public bool HOURSSpecified
        {
            get { return this.HOURS != null; }
            set { }
        }

        [XmlElement]
        public ErnstString TITLE { get; set; }

        [XmlIgnore]
        public bool TITLESpecified
        {
            get { return this.TITLE != null; }
            set { }
        }

        [XmlElement]
        public ErnstString TAT { get; set; }

        [XmlIgnore]
        public bool TATSpecified
        {
            get { return this.TAT != null; }
            set { }
        }

        [XmlElement]
        public ErnstString TATNUM { get; set; }

        [XmlIgnore]
        public bool TATNUMSpecified
        {
            get { return this.TATNUM != null; }
            set { }
        }

        [XmlElement]
        public ErnstString DEED { get; set; }

        [XmlIgnore]
        public bool DEEDSpecified
        {
            get { return this.DEED != null; }
            set { }
        }

        [XmlElement]
        public ErnstString MORTGAGE { get; set; }

        [XmlIgnore]
        public bool MORTGAGESpecified
        {
            get { return this.MORTGAGE != null; }
            set { }
        }

        [XmlElement]
        public ErnstString COUNTYFIPS { get; set; }

        [XmlIgnore]
        public bool COUNTYFIPSSpecified
        {
            get { return this.COUNTYFIPS != null; }
            set { }
        }

        [XmlElement]
        public ErnstString LASTUPD { get; set; }

        [XmlIgnore]
        public bool LASTUPDSpecified
        {
            get { return this.LASTUPD != null; }
            set { }
        }

        [XmlElement]
        public ErnstString DEEDDESC { get; set; }

        [XmlIgnore]
        public bool DEEDDESCSpecified
        {
            get { return this.DEEDDESC != null; }
            set { }
        }

        [XmlElement]
        public ErnstString DEEDFEE { get; set; }

        [XmlIgnore]
        public bool DEEDFEESpecified
        {
            get { return this.DEEDFEE != null; }
            set { }
        }

        [XmlElement]
        public ErnstString DEEDCD { get; set; }

        [XmlIgnore]
        public bool DEEDCDSpecified
        {
            get { return this.DEEDCD != null; }
            set { }
        }

        [XmlElement]
        public ErnstString MORTDESC { get; set; }

        [XmlIgnore]
        public bool MORTDESCSpecified
        {
            get { return this.MORTDESC != null; }
            set { }
        }

        [XmlElement]
        public ErnstString MORTFEE { get; set; }

        [XmlIgnore]
        public bool MORTFEESpecified
        {
            get { return this.MORTFEE != null; }
            set { }
        }

        [XmlElement]
        public ErnstString MORTCD { get; set; }

        [XmlIgnore]
        public bool MORTCDSpecified
        {
            get { return this.MORTCD != null; }
            set { }
        }

        [XmlElement]
        public ErnstString AMFEE { get; set; }

        [XmlIgnore]
        public bool AMFEESpecified
        {
            get { return this.AMFEE != null; }
            set { }
        }

        [XmlElement]
        public ErnstString AMCD { get; set; }

        [XmlIgnore]
        public bool AMCDSpecified
        {
            get { return this.AMCD != null; }
            set { }
        }

        [XmlElement]
        public ErnstString ASSNDESC { get; set; }

        [XmlIgnore]
        public bool ASSNDESCSpecified
        {
            get { return this.ASSNDESC != null; }
            set { }
        }

        [XmlElement]
        public ErnstString ASSNFEE { get; set; }

        [XmlIgnore]
        public bool ASSNFEESpecified
        {
            get { return this.ASSNFEE != null; }
            set { }
        }

        [XmlElement]
        public ErnstString ASSNCD { get; set; }

        [XmlIgnore]
        public bool ASSNCDSpecified
        {
            get { return this.ASSNCD != null; }
            set { }
        }

        [XmlElement]
        public ErnstString SATDESC { get; set; }

        [XmlIgnore]
        public bool SATDESCSpecified
        {
            get { return this.SATDESC != null; }
            set { }
        }

        [XmlElement]
        public ErnstString SATFEE { get; set; }

        [XmlIgnore]
        public bool SATFEESpecified
        {
            get { return this.SATFEE != null; }
            set { }
        }

        [XmlElement]
        public ErnstString SATCD { get; set; }

        [XmlIgnore]
        public bool SATCDSpecified
        {
            get { return this.SATCD != null; }
            set { }
        }

        [XmlElement]
        public ErnstString ADDPGFEE { get; set; }

        [XmlIgnore]
        public bool ADDPGFEESpecified
        {
            get { return this.ADDPGFEE != null; }
            set { }
        }

        [XmlElement]
        public ErnstString ADDPGCD { get; set; }

        [XmlIgnore]
        public bool ADDPGCDSpecified
        {
            get { return this.ADDPGCD != null; }
            set { }
        }

        [XmlElement]
        public ErnstString NONSTDFEE { get; set; }

        [XmlIgnore]
        public bool NONSTDFEESpecified
        {
            get { return this.NONSTDFEE != null; }
            set { }
        }

        [XmlElement]
        public ErnstString NSCD { get; set; }

        [XmlIgnore]
        public bool NSCDSpecified
        {
            get { return this.NSCD != null; }
            set { }
        }

        [XmlElement]
        public ErnstString SMPRNTFEE { get; set; }

        [XmlIgnore]
        public bool SMPRNTFEESpecified
        {
            get { return this.SMPRNTFEE != null; }
            set { }
        }

        [XmlElement]
        public ErnstString SMPRCD { get; set; }

        [XmlIgnore]
        public bool SMPRCDSpecified
        {
            get { return this.SMPRCD != null; }
            set { }
        }

        [XmlElement]
        public ErnstString LEGALFEE { get; set; }

        [XmlIgnore]
        public bool LEGALFEESpecified
        {
            get { return this.LEGALFEE != null; }
            set { }
        }

        [XmlElement]
        public ErnstString LEGALCD { get; set; }

        [XmlIgnore]
        public bool LEGALCDSpecified
        {
            get { return this.LEGALCD != null; }
            set { }
        }

        [XmlElement]
        public ErnstString ADDTYPE1 { get; set; }

        [XmlIgnore]
        public bool ADDTYPE1Specified
        {
            get { return this.ADDTYPE1 != null; }
            set { }
        }

        [XmlElement]
        public ErnstString ADDFEE1 { get; set; }

        [XmlIgnore]
        public bool ADDFEE1Specified
        {
            get { return this.ADDFEE1 != null; }
            set { }
        }

        [XmlElement]
        public ErnstString ADDCD1 { get; set; }

        [XmlIgnore]
        public bool ADDCD1Specified
        {
            get { return this.ADDCD1 != null; }
            set { }
        }

        [XmlElement]
        public ErnstString ADDTYPE2 { get; set; }

        [XmlIgnore]
        public bool ADDTYPE2Specified
        {
            get { return this.ADDTYPE2 != null; }
            set { }
        }

        [XmlElement]
        public ErnstString ADDFEE2 { get; set; }

        [XmlIgnore]
        public bool ADDFEE2Specified
        {
            get { return this.ADDFEE2 != null; }
            set { }
        }

        [XmlElement]
        public ErnstString ADDCD2 { get; set; }

        [XmlIgnore]
        public bool ADDCD2Specified
        {
            get { return this.ADDCD2 != null; }
            set { }
        }

        [XmlElement]
        public ErnstString ADDTYPE3 { get; set; }

        [XmlIgnore]
        public bool ADDTYPE3Specified
        {
            get { return this.ADDTYPE3 != null; }
            set { }
        }

        [XmlElement]
        public ErnstString ADDFEE3 { get; set; }

        [XmlIgnore]
        public bool ADDFEE3Specified
        {
            get { return this.ADDFEE3 != null; }
            set { }
        }

        [XmlElement]
        public ErnstString ADDCD3 { get; set; }

        [XmlIgnore]
        public bool ADDCD3Specified
        {
            get { return this.ADDCD3 != null; }
            set { }
        }

        [XmlElement]
        public ErnstString ADDTYPE4 { get; set; }

        [XmlIgnore]
        public bool ADDTYPE4Specified
        {
            get { return this.ADDTYPE4 != null; }
            set { }
        }

        [XmlElement]
        public ErnstString ADDFEE4 { get; set; }

        [XmlIgnore]
        public bool ADDFEE4Specified
        {
            get { return this.ADDFEE4 != null; }
            set { }
        }

        [XmlElement]
        public ErnstString ADDCD4 { get; set; }

        [XmlIgnore]
        public bool ADDCD4Specified
        {
            get { return this.ADDCD4 != null; }
            set { }
        }

        [XmlElement]
        public ErnstString ADDTYPE5 { get; set; }

        [XmlIgnore]
        public bool ADDTYPE5Specified
        {
            get { return this.ADDTYPE5 != null; }
            set { }
        }

        [XmlElement]
        public ErnstString ADDFEE5 { get; set; }

        [XmlIgnore]
        public bool ADDFEE5Specified
        {
            get { return this.ADDFEE5 != null; }
            set { }
        }

        [XmlElement]
        public ErnstString ADDCD5 { get; set; }

        [XmlIgnore]
        public bool ADDCD5Specified
        {
            get { return this.ADDCD5 != null; }
            set { }
        }

        [XmlElement]
        public ErnstString ADDTYPE6 { get; set; }

        [XmlIgnore]
        public bool ADDTYPE6Specified
        {
            get { return this.ADDTYPE6 != null; }
            set { }
        }

        [XmlElement]
        public ErnstString ADDFEE6 { get; set; }

        [XmlIgnore]
        public bool ADDFEE6Specified
        {
            get { return this.ADDFEE6 != null; }
            set { }
        }

        [XmlElement]
        public ErnstString ADDCD6 { get; set; }

        [XmlIgnore]
        public bool ADDCD6Specified
        {
            get { return this.ADDCD6 != null; }
            set { }
        }

        [XmlElement]
        public ErnstString DESC1 { get; set; }

        [XmlIgnore]
        public bool DESC1Specified
        {
            get { return this.DESC1 != null; }
            set { }
        }

        [XmlElement]
        public ErnstString TAX1 { get; set; }

        [XmlIgnore]
        public bool TAX1Specified
        {
            get { return this.TAX1 != null; }
            set { }
        }

        [XmlElement]
        public ErnstString TAX1CD { get; set; }

        [XmlIgnore]
        public bool TAX1CDSpecified
        {
            get { return this.TAX1CD != null; }
            set { }
        }

        [XmlElement]
        public ErnstString DESC2 { get; set; }

        [XmlIgnore]
        public bool DESC2Specified
        {
            get { return this.DESC2 != null; }
            set { }
        }

        [XmlElement]
        public ErnstString TAX2 { get; set; }

        [XmlIgnore]
        public bool TAX2Specified
        {
            get { return this.TAX2 != null; }
            set { }
        }

        [XmlElement]
        public ErnstString TAX2CD { get; set; }

        [XmlIgnore]
        public bool TAX2CDSpecified
        {
            get { return this.TAX2CD != null; }
            set { }
        }

        [XmlElement]
        public ErnstString DESC3 { get; set; }

        [XmlIgnore]
        public bool DESC3Specified
        {
            get { return this.DESC3 != null; }
            set { }
        }

        [XmlElement]
        public ErnstString TAX3 { get; set; }

        [XmlIgnore]
        public bool TAX3Specified
        {
            get { return this.TAX3 != null; }
            set { }
        }

        [XmlElement]
        public ErnstString TAX3CD { get; set; }

        [XmlIgnore]
        public bool TAX3CDSpecified
        {
            get { return this.TAX3CD != null; }
            set { }
        }

        [XmlElement]
        public ErnstString TAXNOTE { get; set; }

        [XmlIgnore]
        public bool TAXNOTESpecified
        {
            get { return this.TAXNOTE != null; }
            set { }
        }

        [XmlElement]
        public ErnstString SEARCH { get; set; }

        [XmlIgnore]
        public bool SEARCHSpecified
        {
            get { return this.SEARCH != null; }
            set { }
        }

        [XmlElement]
        public ErnstString COPYFEE { get; set; }

        [XmlIgnore]
        public bool COPYFEESpecified
        {
            get { return this.COPYFEE != null; }
            set { }
        }

        [XmlElement]
        public ErnstString COPYCD1 { get; set; }

        [XmlIgnore]
        public bool COPYCD1Specified
        {
            get { return this.COPYCD1 != null; }
            set { }
        }

        [XmlElement]
        public ErnstString COPYFEE2 { get; set; }

        [XmlIgnore]
        public bool COPYFEE2Specified
        {
            get { return this.COPYFEE2 != null; }
            set { }
        }

        [XmlElement]
        public ErnstString COPYCD2 { get; set; }

        [XmlIgnore]
        public bool COPYCD2Specified
        {
            get { return this.COPYCD2 != null; }
            set { }
        }

        [XmlElement]
        public ErnstString CERTFEE { get; set; }

        [XmlIgnore]
        public bool CERTFEESpecified
        {
            get { return this.CERTFEE != null; }
            set { }
        }

        [XmlElement]
        public ErnstString CERTCD { get; set; }

        [XmlIgnore]
        public bool CERTCDSpecified
        {
            get { return this.CERTCD != null; }
            set { }
        }

        [XmlElement]
        public ErnstString DOCSYSTEM { get; set; }

        [XmlIgnore]
        public bool DOCSYSTEMSpecified
        {
            get { return this.DOCSYSTEM != null; }
            set { }
        }

        [XmlElement]
        public ErnstString DSEXAMPLE { get; set; }

        [XmlIgnore]
        public bool DSEXAMPLESpecified
        {
            get { return this.DSEXAMPLE != null; }
            set { }
        }

        [XmlElement]
        public ErnstString FEENOTE1 { get; set; }

        [XmlIgnore]
        public bool FEENOTE1Specified
        {
            get { return this.FEENOTE1 != null; }
            set { }
        }

        [XmlElement]
        public ErnstString FEENOTE2 { get; set; }

        [XmlIgnore]
        public bool FEENOTE2Specified
        {
            get { return this.FEENOTE2 != null; }
            set { }
        }

        [XmlElement]
        public ErnstString FEENOTE3 { get; set; }

        [XmlIgnore]
        public bool FEENOTE3Specified
        {
            get { return this.FEENOTE3 != null; }
            set { }
        }

        [XmlElement]
        public ErnstString RECNOTE1 { get; set; }

        [XmlIgnore]
        public bool RECNOTE1Specified
        {
            get { return this.RECNOTE1 != null; }
            set { }
        }

        [XmlElement]
        public ErnstString RECNOTE2 { get; set; }

        [XmlIgnore]
        public bool RECNOTE2Specified
        {
            get { return this.RECNOTE2 != null; }
            set { }
        }

        [XmlElement]
        public ErnstString RECNOTE3 { get; set; }

        [XmlIgnore]
        public bool RECNOTE3Specified
        {
            get { return this.RECNOTE3 != null; }
            set { }
        }

        [XmlElement]
        public ErnstString BLASSN { get; set; }

        [XmlIgnore]
        public bool BLASSNSpecified
        {
            get { return this.BLASSN != null; }
            set { }
        }

        [XmlElement]
        public ErnstString BLREL { get; set; }

        [XmlIgnore]
        public bool BLRELSpecified
        {
            get { return this.BLREL != null; }
            set { }
        }

        [XmlElement]
        public ErnstString ARNUM { get; set; }

        [XmlIgnore]
        public bool ARNUMSpecified
        {
            get { return this.ARNUM != null; }
            set { }
        }

        [XmlElement]
        public ErnstString ARTYPE { get; set; }

        [XmlIgnore]
        public bool ARTYPESpecified
        {
            get { return this.ARTYPE != null; }
            set { }
        }

        [XmlElement]
        public ErnstString DOCRETURN { get; set; }

        [XmlIgnore]
        public bool DOCRETURNSpecified
        {
            get { return this.DOCRETURN != null; }
            set { }
        }

        [XmlElement]
        public ErnstString SASE { get; set; }

        [XmlIgnore]
        public bool SASESpecified
        {
            get { return this.SASE != null; }
            set { }
        }

        [XmlElement]
        public ErnstString SUBDOCS { get; set; }

        [XmlIgnore]
        public bool SUBDOCSSpecified
        {
            get { return this.SUBDOCS != null; }
            set { }
        }

        [XmlElement]
        public ErnstString LDIND { get; set; }

        [XmlIgnore]
        public bool LDINDSpecified
        {
            get { return this.LDIND != null; }
            set { }
        }

        [XmlElement]
        public ErnstString LDDESC { get; set; }

        [XmlIgnore]
        public bool LDDESCSpecified
        {
            get { return this.LDDESC != null; }
            set { }
        }

        [XmlElement]
        public ErnstString CAPFEE { get; set; }

        [XmlIgnore]
        public bool CAPFEESpecified
        {
            get { return this.CAPFEE != null; }
            set { }
        }

        [XmlElement]
        public ErnstString CAPCD { get; set; }

        [XmlIgnore]
        public bool CAPCDSpecified
        {
            get { return this.CAPCD != null; }
            set { }
        }

        [XmlElement]
        public ErnstString C1 { get; set; }

        [XmlIgnore]
        public bool C1Specified
        {
            get { return this.C1 != null; }
            set { }
        }

        [XmlElement]
        public ErnstString PIN { get; set; }

        [XmlIgnore]
        public bool PINSpecified
        {
            get { return this.PIN != null; }
            set { }
        }

        [XmlElement]
        public ErnstString PINNAME { get; set; }

        [XmlIgnore]
        public bool PINNAMESpecified
        {
            get { return this.PINNAME != null; }
            set { }
        }

        [XmlElement]
        public ErnstString PINON { get; set; }

        [XmlIgnore]
        public bool PINONSpecified
        {
            get { return this.PINON != null; }
            set { }
        }

        [XmlElement]
        public ErnstString PINFORM { get; set; }

        [XmlIgnore]
        public bool PINFORMSpecified
        {
            get { return this.PINFORM != null; }
            set { }
        }

        [XmlElement]
        public ErnstString PINNOTE { get; set; }

        [XmlIgnore]
        public bool PINNOTESpecified
        {
            get { return this.PINNOTE != null; }
            set { }
        }

        [XmlElement]
        public ErnstString WEBADDR { get; set; }

        [XmlIgnore]
        public bool WEBADDRSpecified
        {
            get { return this.WEBADDR != null; }
            set { }
        }

        [XmlElement]
        public ErnstString FIPSB { get; set; }

        [XmlIgnore]
        public bool FIPSBSpecified
        {
            get { return this.FIPSB != null; }
            set { }
        }

        [XmlElement]
        public ErnstString UPDATED { get; set; }

        [XmlIgnore]
        public bool UPDATEDSpecified
        {
            get { return this.UPDATED != null; }
            set { }
        }

        [XmlElement]
        public ErnstString SUBORDDESC { get; set; }

        [XmlIgnore]
        public bool SUBORDDESCSpecified
        {
            get { return this.SUBORDDESC != null; }
            set { }
        }

        [XmlElement]
        public ErnstString SUBORDFEE { get; set; }

        [XmlIgnore]
        public bool SUBORDFEESpecified
        {
            get { return this.SUBORDFEE != null; }
            set { }
        }

        [XmlElement]
        public ErnstString SUBORDCD { get; set; }

        [XmlIgnore]
        public bool SUBORDCDSpecified
        {
            get { return this.SUBORDCD != null; }
            set { }
        }

        [XmlElement]
        public Error Error { get; set; }

        [XmlIgnore]
        public bool ErrorSpecified
        {
            get { return this.Error != null; }
            set { }
        }
    }
}
