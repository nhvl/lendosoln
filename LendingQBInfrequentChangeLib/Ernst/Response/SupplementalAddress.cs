namespace Ernst.Response
{
    using System.Collections.Generic;
    using System.Xml.Serialization;

    public partial class SupplementalAddress
    {
        [XmlElement("Address")]
        public List<SupplementalAddressAddress> AddressList { get; set; }

        [XmlIgnore]
        public bool AddressListSpecified
        {
            get { return this.AddressList != null; }
            set { }
        }

        [XmlElement]
        public Error Error { get; set; }

        [XmlIgnore]
        public bool ErrorSpecified
        {
            get { return this.Error != null; }
        }
    }
}
