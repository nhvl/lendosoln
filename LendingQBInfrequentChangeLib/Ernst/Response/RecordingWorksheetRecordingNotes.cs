namespace Ernst.Response
{
    using System.Collections.Generic;
    using System.Xml.Serialization;

    public partial class RecordingWorksheetRecordingNotes
    {
        [XmlElement]
        public ErnstString Heading { get; set; }

        [XmlIgnore]
        public bool HeadingSpecified
        {
            get { return this.Heading != null; }
            set { }
        }

        [XmlElement("Note")]
        public List<ErnstString> NoteList { get; set; }

        [XmlIgnore]
        public bool NoteListSpecified
        {
            get { return this.NoteList != null; }
            set { }
        }

        [XmlElement]
        public ErnstString DocSystem { get; set; }

        [XmlIgnore]
        public bool DocSystemSpecified
        {
            get { return this.DocSystem != null; }
            set { }
        }

        [XmlElement]
        public ErnstString DSExample { get; set; }

        [XmlIgnore]
        public bool DSExampleSpecified
        {
            get { return this.DSExample != null; }
            set { }
        }

        [XmlElement]
        public Error Error { get; set; }

        [XmlIgnore]
        public bool ErrorSpecified
        {
            get { return this.Error != null; }
            set { }
        }
    }
}
