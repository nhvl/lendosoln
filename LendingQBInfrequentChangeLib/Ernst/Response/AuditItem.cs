namespace Ernst.Response
{
    using System.Xml.Serialization;

    public partial class AuditItem
    {
        [XmlElement]
        public ErnstString Name { get; set; }

        [XmlIgnore]
        public bool NameSpecified
        {
            get { return this.Name != null; }
            set { }
        }

        [XmlElement]
        public ErnstString Value { get; set; }

        [XmlIgnore]
        public bool ValueSpecified
        {
            get { return this.Value != null; }
            set { }
        }
    }
}
