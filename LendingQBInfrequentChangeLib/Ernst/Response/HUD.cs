namespace Ernst.Response
{
    using System.Collections.Generic;
    using System.Xml.Serialization;

    public partial class HUD
    {
        [XmlElement("HUDLine")]
        public List<HUDHUDLine> HUDLineList { get; set; }

        [XmlIgnore]
        public bool HUDLineListSpecified
        {
            get { return this.HUDLineList != null; }
            set { }
        }

        [XmlElement]
        public Error Error { get; set; }

        [XmlIgnore]
        public bool ErrorSpecified
        {
            get { return this.Error != null; }
            set { }
        }
    }
}
