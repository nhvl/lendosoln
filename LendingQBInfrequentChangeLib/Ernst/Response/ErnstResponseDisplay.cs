namespace Ernst.Response
{
    using System.Xml.Serialization;

    public partial class ErnstResponseDisplay
    {
        [XmlElement]
        public Calculation Calculation { get; set; }

        [XmlIgnore]
        public bool CalculationSpecified
        {
            get { return this.Calculation != null; }
            set { }
        }

        [XmlElement]
        public RecordingWorksheet RecordingWorksheet { get; set; }

        [XmlIgnore]
        public bool RecordingWorksheetSpecified
        {
            get { return this.RecordingWorksheet != null; }
            set { }
        }

        [XmlElement]
        public DocumentLinks DocumentLinks { get; set; }

        [XmlIgnore]
        public bool DocumentLinksSpecified
        {
            get { return this.DocumentLinks != null; }
            set { }
        }

        [XmlElement]
        public Error Error { get; set; }

        [XmlIgnore]
        public bool ErrorSpecified
        {
            get { return this.Error != null; }
            set { }
        }
    }
}
