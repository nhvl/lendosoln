namespace Ernst.Response
{
    using System.Xml.Serialization;

    public partial class Block6Response
    {
        [XmlElement]
        public ErnstString TransactionID { get; set; }

        [XmlIgnore]
        public bool TransactionIDSpecified
        {
            get { return this.TransactionID != null; }
            set { }
        }

        [XmlElement]
        public Block6Service Standard { get; set; }

        [XmlIgnore]
        public bool StandardSpecified
        {
            get { return this.Standard != null; }
            set { }
        }

        [XmlElement]
        public Block6Service Pest { get; set; }

        [XmlIgnore]
        public bool PestSpecified
        {
            get { return this.Pest != null; }
            set { }
        }

        [XmlElement]
        public Block6Service Water { get; set; }

        [XmlIgnore]
        public bool WaterSpecified
        {
            get { return this.Water != null; }
            set { }
        }

        [XmlElement]
        public Block6Service Septic { get; set; }

        [XmlIgnore]
        public bool SepticSpecified
        {
            get { return this.Septic != null; }
            set { }
        }

        [XmlElement]
        public Block6Service Well { get; set; }

        [XmlIgnore]
        public bool WellSpecified
        {
            get { return this.Well != null; }
            set { }
        }

        [XmlElement]
        public Block6Service Smoke { get; set; }

        [XmlIgnore]
        public bool SmokeSpecified
        {
            get { return this.Smoke != null; }
            set { }
        }

        [XmlElement]
        public Block6Service Radon { get; set; }

        [XmlIgnore]
        public bool RadonSpecified
        {
            get { return this.Radon != null; }
            set { }
        }

        [XmlElement]
        public Block6Service Roof { get; set; }

        [XmlIgnore]
        public bool RoofSpecified
        {
            get { return this.Roof != null; }
            set { }
        }

        [XmlElement]
        public Block6Service Structural { get; set; }

        [XmlIgnore]
        public bool StructuralSpecified
        {
            get { return this.Structural != null; }
            set { }
        }

        [XmlElement]
        public Block6Service Survey { get; set; }

        [XmlIgnore]
        public bool SurveySpecified
        {
            get { return this.Survey != null; }
            set { }
        }

        [XmlElement]
        public Block6Service Mold { get; set; }

        [XmlIgnore]
        public bool MoldSpecified
        {
            get { return this.Mold != null; }
            set { }
        }

        [XmlElement]
        public Block6Service ManufacturedHousing { get; set; }

        [XmlIgnore]
        public bool ManufacturedHousingSpecified
        {
            get { return this.ManufacturedHousing != null; }
            set { }
        }

        [XmlElement]
        public Block6Service Asbestos { get; set; }

        [XmlIgnore]
        public bool AsbestosSpecified
        {
            get { return this.Asbestos != null; }
            set { }
        }

        [XmlElement]
        public Block6Service Lead { get; set; }

        [XmlIgnore]
        public bool LeadSpecified
        {
            get { return this.Lead != null; }
            set { }
        }

        [XmlElement]
        public Block6Service Drywall { get; set; }

        [XmlIgnore]
        public bool DrywallSpecified
        {
            get { return this.Drywall != null; }
            set { }
        }

        [XmlElement]
        public Block6Service NaturalHazardDisclosure { get; set; }

        [XmlIgnore]
        public bool NaturalHazardDisclosureSpecified
        {
            get { return this.NaturalHazardDisclosure != null; }
            set { }
        }

        [XmlElement]
        public Block6Service GeoTechnicalEngineering { get; set; }

        [XmlIgnore]
        public bool GeoTechnicalEngineeringSpecified
        {
            get { return this.GeoTechnicalEngineering != null; }
            set { }
        }

        [XmlElement]
        public Block6Service Foundation { get; set; }

        [XmlIgnore]
        public bool FoundationSpecified
        {
            get { return this.Foundation != null; }
            set { }
        }

        [XmlElement]
        public Block6Service Electrical { get; set; }

        [XmlIgnore]
        public bool ElectricalSpecified
        {
            get { return this.Electrical != null; }
            set { }
        }

        [XmlElement]
        public Block6Service Plumbing { get; set; }

        [XmlIgnore]
        public bool PlumbingSpecified
        {
            get { return this.Plumbing != null; }
            set { }
        }

        [XmlElement]
        public Block6Service HeatingCooling { get; set; }

        [XmlIgnore]
        public bool HeatingCoolingSpecified
        {
            get { return this.HeatingCooling != null; }
            set { }
        }

        [XmlElement]
        public Block6Service EnvironmentalInspection { get; set; }

        [XmlIgnore]
        public bool EnvironmentalInspectionSpecified
        {
            get { return this.EnvironmentalInspection != null; }
            set { }
        }

        [XmlElement]
        public Audit Audit { get; set; }

        [XmlIgnore]
        public bool AuditSpecified
        {
            get { return this.Audit != null; }
            set { }
        }

        [XmlElement]
        public Error Error { get; set; }

        [XmlIgnore]
        public bool ErrorSpecified
        {
            get { return this.Error != null; }
            set { }
        }
    }
}
