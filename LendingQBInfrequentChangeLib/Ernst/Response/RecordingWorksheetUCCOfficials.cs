﻿namespace Ernst.Response
{
    using System.Collections.Generic;
    using System.Xml.Serialization;

    public partial class RecordingWorksheetUCCOfficials
    {
        [XmlElement]
        public ErnstString Heading { get; set; }

        [XmlIgnore]
        public bool HeadingSpecified
        {
            get { return this.Heading != null; }
            set { }
        }

        [XmlElement("Official")]
        public List<RecordingWorksheetOfficial> OfficialList { get; set; }

        [XmlIgnore]
        public bool OfficialListSpecified
        {
            get { return this.OfficialList != null; }
            set { }
        }

        [XmlElement]
        public Error Error { get; set; }

        [XmlIgnore]
        public bool ErrorSpecified
        {
            get { return this.Error != null; }
            set { }
        }
    }
}
