namespace Ernst.Response
{
    using System.Xml.Serialization;

    public partial class DocumentLinksLink
    {
        [XmlElement]
        public ErnstString Label { get; set; }

        [XmlIgnore]
        public bool LabelSpecified
        {
            get { return this.Label != null; }
            set { }
        }

        [XmlElement]
        public ErnstString URL { get; set; }

        [XmlIgnore]
        public bool URLSpecified
        {
            get { return this.URL != null; }
            set { }
        }

        [XmlElement]
        public Error Error { get; set; }

        [XmlIgnore]
        public bool ErrorSpecified
        {
            get { return this.Error != null; }
            set { }
        }
    }
}
