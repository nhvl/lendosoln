namespace Ernst.Response
{
    using System.Collections.Generic;
    using System.Xml.Serialization;

    public partial class CFPB2015
    {
        [XmlElement("CFPBTax")]
        public List<CFPB2015CFPBTax> CFPBTaxList { get; set; }

        [XmlIgnore]
        public bool CFPBTaxListSpecified
        {
            get { return this.CFPBTaxList != null; }
            set { }
        }
    }
}
