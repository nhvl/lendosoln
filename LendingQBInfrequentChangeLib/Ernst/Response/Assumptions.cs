﻿namespace Ernst.Response
{
    using System.Collections.Generic;
    using System.Xml.Serialization;

    public partial class Assumptions
    {
        [XmlElement("Assumption")]
        public List<ErnstString> AssumptionList { get; set; }

        [XmlIgnore]
        public bool AssumptionsListSpecified
        {
            get { return this.AssumptionList != null; }
            set { }
        }

        [XmlElement]
        public Error Error;

        [XmlIgnore]
        public bool ErrorSpecified
        {
            get { return this.Error != null; }
        }
    }
}
