﻿namespace Ernst.Response
{
    using System.Collections.Generic;
    using System.Xml.Serialization;

    public class TitleResponseEndorsements
    {
        [XmlElement("Endorsement")]
        public List<Fee> EndorsementList { get; set; }

        [XmlIgnore]
        public bool EndorsementListSpecified
        {
            get { return this.EndorsementList != null; }
            set { }
        }
    }
}
