﻿namespace Ernst.Response
{
    using System.Collections.Generic;
    using System.Xml.Serialization;

    public class TitleResponseItemizedSettlementFees
    {
        [XmlElement("SettlementFee")]
        public List<Fee> SettlementFeeList { get; set; }

        [XmlIgnore]
        public bool SettlementFeeListSpecified
        {
            get { return this.SettlementFeeList != null; }
            set { }
        }
    }
}
