namespace Ernst.Response
{
    using System.Xml.Serialization;

    public partial class RequestAssignment
    {
        [XmlElement]
        public Index Index { get; set; }

        [XmlIgnore]
        public bool IndexSpecified
        {
            get { return this.Index != null; }
            set { }
        }

        [XmlElement]
        public Error Error { get; set; }

        [XmlIgnore]
        public bool ErrorSpecified
        {
            get { return this.Error != null; }
            set { }
        }
    }
}
