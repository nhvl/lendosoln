namespace Ernst.Response
{
    using System.Xml.Serialization;

    public partial class Block6ServiceVendor
    {
        [XmlElement]
        public ErnstString Price { get; set; }

        [XmlIgnore]
        public bool PriceSpecified
        {
            get { return this.Price != null; }
            set { }
        }

        [XmlElement]
        public ErnstString ID { get; set; }

        [XmlIgnore]
        public bool IDSpecified
        {
            get { return this.ID != null; }
            set { }
        }

        [XmlElement]
        public ErnstString Name { get; set; }

        [XmlIgnore]
        public bool NameSpecified
        {
            get { return this.Name != null; }
            set { }
        }

        [XmlElement]
        public ErnstString Phone { get; set; }

        [XmlIgnore]
        public bool PhoneSpecified
        {
            get { return this.Phone != null; }
            set { }
        }

        [XmlElement]
        public ErnstString Web { get; set; }

        [XmlIgnore]
        public bool WebSpecified
        {
            get { return this.Web != null; }
            set { }
        }

        [XmlElement]
        public ErnstString Address { get; set; }

        [XmlIgnore]
        public bool AddressSpecified
        {
            get { return this.Address != null; }
            set { }
        }

        [XmlElement]
        public ErnstString City { get; set; }

        [XmlIgnore]
        public bool CitySpecified
        {
            get { return this.City != null; }
            set { }
        }

        [XmlElement]
        public ErnstString State { get; set; }

        [XmlIgnore]
        public bool StateSpecified
        {
            get { return this.State != null; }
            set { }
        }

        [XmlElement]
        public ErnstString Zip { get; set; }

        [XmlIgnore]
        public bool ZipSpecified
        {
            get { return this.Zip != null; }
            set { }
        }
    }
}
