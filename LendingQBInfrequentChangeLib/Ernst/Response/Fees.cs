﻿namespace Ernst.Response
{
    using System.Collections.Generic;
    using System.Xml.Serialization;

    public partial class Fees
    {
        [XmlElement("Fee")]
        public List<Fee> FeeList { get; set; }

        [XmlIgnore]
        public bool FeeListSpecified
        {
            get { return this.FeeList != null; }
            set { }
        }
    }
}
