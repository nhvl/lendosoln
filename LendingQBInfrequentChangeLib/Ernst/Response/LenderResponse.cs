namespace Ernst.Response
{
    using System.Collections.Generic;
    using System.Xml.Serialization;

    public partial class LenderResponse
    {
        [XmlElement]
        public ErnstBoolean CallForQuoteLender { get; set; }

        [XmlIgnore]
        public bool CallForQuoteLenderSpecified
        {
            get { return this.CallForQuoteLender != null; }
            set { }
        }

        [XmlElement]
        public List<Fee> LenderFees { get; set; }

        [XmlIgnore]
        public bool LenderFeesSpecified
        {
            get { return this.LenderFees != null; }
            set { }
        }

        [XmlElement]
        public Provider Provider { get; set; }

        [XmlIgnore]
        public bool ProviderSpecified
        {
            get { return this.Provider != null; }
            set { }
        }

        [XmlElement]
        public List<ErnstString> Notes { get; set; }

        [XmlIgnore]
        public bool NotesSpecified
        {
            get { return this.Notes != null; }
            set { }
        }

        [XmlElement]
        public CallForQuote CallForQuote { get; set; }

        [XmlIgnore]
        public bool CallForQuoteSpecified
        {
            get { return this.CallForQuote != null; }
            set { }
        }

        [XmlElement]
        public Audit Audit { get; set; }

        [XmlIgnore]
        public bool AuditSpecified
        {
            get { return this.Audit != null; }
            set { }
        }

        [XmlElement]
        public Error Error { get; set; }

        [XmlIgnore]
        public bool ErrorSpecified
        {
            get { return this.Error != null; }
            set { }
        }
    }
}
