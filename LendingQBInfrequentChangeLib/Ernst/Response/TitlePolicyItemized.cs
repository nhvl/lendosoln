﻿namespace Ernst.Response
{
    using System.Collections.Generic;
    using System.Xml.Serialization;

    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.DocumentationRules", "*", Justification = "Temporary while developing.")]
    public class TitlePolicyItemized
    {
        [XmlElement("Charge")]
        public List<Fee> ChargeList { get; set; }

        [XmlIgnore]
        public bool ChargeListSpecified
        {
            get { return this.ChargeList != null; }
            set { }
        }
    }
}
