﻿namespace Ernst.Response
{
    using System.Collections.Generic;
    using System.Xml.Serialization;

    public partial class Notes
    {
        [XmlElement("Note")]
        public List<ErnstString> NoteList { get; set; }

        [XmlIgnore]
        public bool NoteListSpecified
        {
            get { return this.NoteList != null; }
            set { }
        }
    }
}
