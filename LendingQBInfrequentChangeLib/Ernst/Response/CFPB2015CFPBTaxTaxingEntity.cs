namespace Ernst.Response
{
    using System.Xml.Serialization;

    public partial class CFPB2015CFPBTaxTaxingEntity
    {
        [XmlElement]
        public ErnstEnum<CFPB2015CFPBTaxTaxingEntityTaxingEntityJurisdiction> TaxingEntityJurisdiction { get; set; }

        [XmlIgnore]
        public bool TaxingEntityJurisdictionSpecified
        {
            get { return this.TaxingEntityJurisdiction != null; }
            set { }
        }

        [XmlElement]
        public ErnstString TaxingEntityName { get; set; }

        [XmlIgnore]
        public bool TaxingEntityNameSpecified
        {
            get { return this.TaxingEntityName != null; }
            set { }
        }

        [XmlElement]
        public ErnstString TaxAmount { get; set; }

        [XmlIgnore]
        public bool TaxAmountSpecified
        {
            get { return this.TaxAmount != null; }
            set { }
        }

        [XmlElement]
        public ErnstString BuyerSplit { get; set; }

        [XmlIgnore]
        public bool BuyerSplitSpecified
        {
            get { return this.BuyerSplit != null; }
            set { }
        }

        [XmlElement]
        public ErnstString SellerSplit { get; set; }

        [XmlIgnore]
        public bool SellerSplitSpecified
        {
            get { return this.SellerSplit != null; }
            set { }
        }

        [XmlElement]
        public ErnstString LenderSplit { get; set; }

        [XmlIgnore]
        public bool LenderSplitSpecified
        {
            get { return this.LenderSplit != null; }
            set { }
        }
    }
}
