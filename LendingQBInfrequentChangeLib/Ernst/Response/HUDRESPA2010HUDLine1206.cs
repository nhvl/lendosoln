namespace Ernst.Response
{
    using System.Xml.Serialization;

    public partial class HUDRESPA2010HUDLine1206
    {
        [XmlElement]
        public ErnstString LineDescription { get; set; }

        [XmlIgnore]
        public bool LineDescriptionSpecified
        {
            get { return this.LineDescription != null; }
            set { }
        }

        [XmlElement]
        public ErnstString NotorialArchive { get; set; }

        [XmlIgnore]
        public bool NotorialArchiveSpecified
        {
            get { return this.NotorialArchive != null; }
            set { }
        }

        [XmlElement]
        public ErnstString ResidentialMortgageFee { get; set; }

        [XmlIgnore]
        public bool ResidentialMortgageFeeSpecified
        {
            get { return this.ResidentialMortgageFee != null; }
            set { }
        }

        [XmlElement]
        public ErnstString DeedAmount { get; set; }

        [XmlIgnore]
        public bool DeedAmountSpecified
        {
            get { return this.DeedAmount != null; }
            set { }
        }

        [XmlElement]
        public ErnstString MortgageAmount { get; set; }

        [XmlIgnore]
        public bool MortgageAmountSpecified
        {
            get { return this.MortgageAmount != null; }
            set { }
        }

        [XmlElement]
        public ErnstString IntangibleTax { get; set; }

        [XmlIgnore]
        public bool IntangibleTaxSpecified
        {
            get { return this.IntangibleTax != null; }
            set { }
        }

        [XmlElement]
        public ErnstString BuyerSplit { get; set; }

        [XmlIgnore]
        public bool BuyerSplitSpecified
        {
            get { return this.BuyerSplit != null; }
            set { }
        }

        [XmlElement]
        public ErnstString SellerSplit { get; set; }

        [XmlIgnore]
        public bool SellerSplitSpecified
        {
            get { return this.SellerSplit != null; }
            set { }
        }

        [XmlElement]
        public ErnstString LenderSplit { get; set; }

        [XmlIgnore]
        public bool LenderSplitSpecified
        {
            get { return this.LenderSplit != null; }
            set { }
        }
    }
}
