namespace Ernst.Response
{
    using System.Xml.Serialization;

    public partial class CalculationMortgage
    {
        [XmlElement]
        public ErnstString IndexFee { get; set; }

        [XmlIgnore]
        public bool IndexFeeSpecified
        {
            get { return this.IndexFee != null; }
            set { }
        }

        [XmlElement]
        public ErnstString Fee { get; set; }

        [XmlIgnore]
        public bool FeeSpecified
        {
            get { return this.Fee != null; }
            set { }
        }

        [XmlElement]
        public ErnstString Tax { get; set; }

        [XmlIgnore]
        public bool TaxSpecified
        {
            get { return this.Tax != null; }
            set { }
        }

        [XmlElement]
        public ErnstString ConservationFund { get; set; }

        [XmlIgnore]
        public bool ConservationFundSpecified
        {
            get { return this.ConservationFund != null; }
            set { }
        }

        [XmlElement]
        public ErnstString NotorialArchive { get; set; }

        [XmlIgnore]
        public bool NotorialArchiveSpecified
        {
            get { return this.NotorialArchive != null; }
            set { }
        }

        [XmlElement]
        public ErnstString ResidentialMortgageFee { get; set; }

        [XmlIgnore]
        public bool ResidentialMortgageFeeSpecified
        {
            get { return this.ResidentialMortgageFee != null; }
            set { }
        }

        [XmlElement]
        public ErnstString IntangibleTax { get; set; }

        [XmlIgnore]
        public bool IntangibleTaxSpecified
        {
            get { return this.IntangibleTax != null; }
            set { }
        }

        [XmlElement]
        public ErnstString RoundedMortgage { get; set; }

        [XmlIgnore]
        public bool RoundedMortgageSpecified
        {
            get { return this.RoundedMortgage != null; }
            set { }
        }

        [XmlElement]
        public ErnstString StateTax { get; set; }

        [XmlIgnore]
        public bool StateTaxSpecified
        {
            get { return this.StateTax != null; }
            set { }
        }

        [XmlElement]
        public ErnstString CountyTax { get; set; }

        [XmlIgnore]
        public bool CountyTaxSpecified
        {
            get { return this.CountyTax != null; }
            set { }
        }

        [XmlElement]
        public ErnstString CityTax { get; set; }

        [XmlIgnore]
        public bool CityTaxSpecified
        {
            get { return this.CityTax != null; }
            set { }
        }

        [XmlElement]
        public ErnstString AmendmentModificationFee { get; set; }

        [XmlIgnore]
        public bool AmendmentModificationFeeSpecified
        {
            get { return this.AmendmentModificationFee != null; }
            set { }
        }

        [XmlElement]
        public ErnstString BuyerSplit { get; set; }

        [XmlIgnore]
        public bool BuyerSplitSpecified
        {
            get { return this.BuyerSplit != null; }
            set { }
        }

        [XmlElement]
        public ErnstString LenderSplit { get; set; }

        [XmlIgnore]
        public bool LenderSplitSpecified
        {
            get { return this.LenderSplit != null; }
            set { }
        }

        [XmlElement]
        public ErnstString MortgageRecordationTax { get; set; }

        [XmlIgnore]
        public bool MortgageRecordationTaxSpecified
        {
            get { return this.MortgageRecordationTax != null; }
            set { }
        }

        [XmlElement]
        public Error Error { get; set; }

        [XmlIgnore]
        public bool ErrorSpecified
        {
            get { return this.Error != null; }
            set { }
        }
    }
}
