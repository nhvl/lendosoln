namespace Ernst.Response
{
    using System.Xml.Serialization;

    public partial class CalculationDeed
    {
        [XmlElement]
        public ErnstString IndexFee { get; set; }

        [XmlIgnore]
        public bool IndexFeeSpecified
        {
            get { return this.IndexFee != null; }
            set { }
        }

        [XmlElement]
        public ErnstString Fee { get; set; }

        [XmlIgnore]
        public bool FeeSpecified
        {
            get { return this.Fee != null; }
            set { }
        }

        [XmlElement]
        public ErnstString FormFee { get; set; }

        [XmlIgnore]
        public bool FormFeeSpecified
        {
            get { return this.FormFee != null; }
            set { }
        }

        [XmlElement]
        public ErnstString Tax { get; set; }

        [XmlIgnore]
        public bool TaxSpecified
        {
            get { return this.Tax != null; }
            set { }
        }

        [XmlElement]
        public ErnstString ConservationFund { get; set; }

        [XmlIgnore]
        public bool ConservationFundSpecified
        {
            get { return this.ConservationFund != null; }
            set { }
        }

        [XmlElement]
        public ErnstString NotorialArchive { get; set; }

        [XmlIgnore]
        public bool NotorialArchiveSpecified
        {
            get { return this.NotorialArchive != null; }
            set { }
        }

        [XmlElement]
        public ErnstString RoundedDeed { get; set; }

        [XmlIgnore]
        public bool RoundedDeedSpecified
        {
            get { return this.RoundedDeed != null; }
            set { }
        }

        [XmlElement]
        public ErnstString StateTax { get; set; }

        [XmlIgnore]
        public bool StateTaxSpecified
        {
            get { return this.StateTax != null; }
            set { }
        }

        [XmlElement]
        public ErnstString DeedRecordationTax { get; set; }

        [XmlIgnore]
        public bool DeedRecordationTaxSpecified
        {
            get { return this.DeedRecordationTax != null; }
            set { }
        }

        [XmlElement]
        public ErnstString StateMansionTax { get; set; }

        [XmlIgnore]
        public bool StateMansionTaxSpecified
        {
            get { return this.StateMansionTax != null; }
            set { }
        }

        [XmlElement]
        public ErnstString StateNonMansionTax { get; set; }

        [XmlIgnore]
        public bool StateNonMansionTaxSpecified
        {
            get { return this.StateNonMansionTax != null; }
            set { }
        }

        [XmlElement]
        public ErnstString BuyerSplitState { get; set; }

        [XmlIgnore]
        public bool BuyerSplitStateSpecified
        {
            get { return this.BuyerSplitState != null; }
            set { }
        }

        [XmlElement]
        public ErnstString SellerSplitState { get; set; }

        [XmlIgnore]
        public bool SellerSplitStateSpecified
        {
            get { return this.SellerSplitState != null; }
            set { }
        }

        [XmlElement]
        public ErnstString BuyerSplitDeedRecordationTax { get; set; }

        [XmlIgnore]
        public bool BuyerSplitDeedRecordationTaxSpecified
        {
            get { return this.BuyerSplitDeedRecordationTax != null; }
            set { }
        }

        [XmlElement]
        public ErnstString SellerSplitDeedRecordationTax { get; set; }

        [XmlIgnore]
        public bool SellerSplitDeedRecordationTaxSpecified
        {
            get { return this.SellerSplitDeedRecordationTax != null; }
            set { }
        }

        [XmlElement]
        public ErnstString CountyTax { get; set; }

        [XmlIgnore]
        public bool CountyTaxSpecified
        {
            get { return this.CountyTax != null; }
            set { }
        }

        [XmlElement]
        public ErnstString BuyerSplitCounty { get; set; }

        [XmlIgnore]
        public bool BuyerSplitCountySpecified
        {
            get { return this.BuyerSplitCounty != null; }
            set { }
        }

        [XmlElement]
        public ErnstString SellerSplitCounty { get; set; }

        [XmlIgnore]
        public bool SellerSplitCountySpecified
        {
            get { return this.SellerSplitCounty != null; }
            set { }
        }

        [XmlElement]
        public ErnstString CityTax { get; set; }

        [XmlIgnore]
        public bool CityTaxSpecified
        {
            get { return this.CityTax != null; }
            set { }
        }

        [XmlElement]
        public ErnstString BuyerSplitCity { get; set; }

        [XmlIgnore]
        public bool BuyerSplitCitySpecified
        {
            get { return this.BuyerSplitCity != null; }
            set { }
        }

        [XmlElement]
        public ErnstString SellerSplitCity { get; set; }

        [XmlIgnore]
        public bool SellerSplitCitySpecified
        {
            get { return this.SellerSplitCity != null; }
            set { }
        }

        [XmlElement]
        public ErnstString BuyerSplit { get; set; }

        [XmlIgnore]
        public bool BuyerSplitSpecified
        {
            get { return this.BuyerSplit != null; }
            set { }
        }

        [XmlElement]
        public ErnstString BuyerSplitRate { get; set; }

        [XmlIgnore]
        public bool BuyerSplitRateSpecified
        {
            get { return this.BuyerSplitRate != null; }
            set { }
        }

        [XmlElement]
        public ErnstString SellerSplit { get; set; }

        [XmlIgnore]
        public bool SellerSplitSpecified
        {
            get { return this.SellerSplit != null; }
            set { }
        }

        [XmlElement]
        public ErnstString SellerSplitRate { get; set; }

        [XmlIgnore]
        public bool SellerSplitRateSpecified
        {
            get { return this.SellerSplitRate != null; }
            set { }
        }

        [XmlElement]
        public ErnstString BuyerSellerNote { get; set; }

        [XmlIgnore]
        public bool BuyerSellerNoteSpecified
        {
            get { return this.BuyerSellerNote != null; }
            set { }
        }

        [XmlElement]
        public ErnstString RoundedDeedState { get; set; }

        [XmlIgnore]
        public bool RoundedDeedStateSpecified
        {
            get { return this.RoundedDeedState != null; }
            set { }
        }

        [XmlElement]
        public ErnstString RoundedDeedCounty { get; set; }

        [XmlIgnore]
        public bool RoundedDeedCountySpecified
        {
            get { return this.RoundedDeedCounty != null; }
            set { }
        }

        [XmlElement]
        public ErnstString RoundedDeedCity { get; set; }

        [XmlIgnore]
        public bool RoundedDeedCitySpecified
        {
            get { return this.RoundedDeedCity != null; }
            set { }
        }

        [XmlElement]
        public ErnstString AmendmentModificationFee { get; set; }

        [XmlIgnore]
        public bool AmendmentModificationFeeSpecified
        {
            get { return this.AmendmentModificationFee != null; }
            set { }
        }

        [XmlElement]
        public Error Error { get; set; }

        [XmlIgnore]
        public bool ErrorSpecified
        {
            get { return this.Error != null; }
            set { }
        }
    }
}
