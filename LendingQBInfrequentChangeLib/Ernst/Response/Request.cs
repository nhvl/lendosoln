namespace Ernst.Response
{
    using System.Xml.Serialization;

    public partial class Request
    {
        [XmlElement]
        public ErnstString ClientTransactionID { get; set; }

        [XmlIgnore]
        public bool ClientTransactionIDSpecified
        {
            get { return this.ClientTransactionID != null; }
            set { }
        }

        [XmlElement]
        public ErnstString TransactionID { get; set; }

        [XmlIgnore]
        public bool TransactionIDSpecified
        {
            get { return this.TransactionID != null; }
            set { }
        }

        [XmlElement]
        public ErnstString Page { get; set; }

        [XmlIgnore]
        public bool PageSpecified
        {
            get { return this.Page != null; }
            set { }
        }

        [XmlElement]
        public ErnstString City { get; set; }

        [XmlIgnore]
        public bool CitySpecified
        {
            get { return this.City != null; }
            set { }
        }

        [XmlElement]
        public ErnstString County { get; set; }

        [XmlIgnore]
        public bool CountySpecified
        {
            get { return this.County != null; }
            set { }
        }

        [XmlElement]
        public ErnstString State { get; set; }

        [XmlIgnore]
        public bool StateSpecified
        {
            get { return this.State != null; }
            set { }
        }

        [XmlElement]
        public ErnstString TransactionCode { get; set; }

        [XmlIgnore]
        public bool TransactionCodeSpecified
        {
            get { return this.TransactionCode != null; }
            set { }
        }

        [XmlElement]
        public ErnstString DeedNumberOfPages { get; set; }

        [XmlIgnore]
        public bool DeedNumberOfPagesSpecified
        {
            get { return this.DeedNumberOfPages != null; }
            set { }
        }

        [XmlElement]
        public ErnstString MortgageNumberOfPages { get; set; }

        [XmlIgnore]
        public bool MortgageNumberOfPagesSpecified
        {
            get { return this.MortgageNumberOfPages != null; }
            set { }
        }

        [XmlElement]
        public ErnstString MortgageAmount { get; set; }

        [XmlIgnore]
        public bool MortgageAmountSpecified
        {
            get { return this.MortgageAmount != null; }
            set { }
        }

        [XmlElement]
        public ErnstString OriginalDebtAmount { get; set; }

        [XmlIgnore]
        public bool OriginalDebtAmountSpecified
        {
            get { return this.OriginalDebtAmount != null; }
            set { }
        }

        [XmlElement]
        public ErnstString UnpaidPrincipalBalance { get; set; }

        [XmlIgnore]
        public bool UnpaidPrincipalBalanceSpecified
        {
            get { return this.UnpaidPrincipalBalance != null; }
            set { }
        }

        [XmlElement]
        public ErnstString EstimatedValue { get; set; }

        [XmlIgnore]
        public bool EstimatedValueSpecified
        {
            get { return this.EstimatedValue != null; }
            set { }
        }

        [XmlElement]
        public ErnstString Title { get; set; }

        [XmlIgnore]
        public bool TitleSpecified
        {
            get { return this.Title != null; }
            set { }
        }

        [XmlElement]
        public ErnstString TitleAmount { get; set; }

        [XmlIgnore]
        public bool TitleAmountSpecified
        {
            get { return this.TitleAmount != null; }
            set { }
        }

        [XmlElement]
        public ErnstString OwnersTitleAmount { get; set; }

        [XmlIgnore]
        public bool OwnersTitleAmountSpecified
        {
            get { return this.OwnersTitleAmount != null; }
            set { }
        }

        [XmlElement]
        public ErnstString OriginalTitleAmount { get; set; }

        [XmlIgnore]
        public bool OriginalTitleAmountSpecified
        {
            get { return this.OriginalTitleAmount != null; }
            set { }
        }

        [XmlElement]
        public ErnstString OriginalMortgageDate { get; set; }

        [XmlIgnore]
        public bool OriginalMortgageDateSpecified
        {
            get { return this.OriginalMortgageDate != null; }
            set { }
        }

        [XmlElement]
        public ErnstString AssignmentPages { get; set; }

        [XmlIgnore]
        public bool AssignmentPagesSpecified
        {
            get { return this.AssignmentPages != null; }
            set { }
        }

        [XmlElement]
        public ErnstString AssignmentNumber { get; set; }

        [XmlIgnore]
        public bool AssignmentNumberSpecified
        {
            get { return this.AssignmentNumber != null; }
            set { }
        }

        [XmlElement]
        public ErnstString ReleasePages { get; set; }

        [XmlIgnore]
        public bool ReleasePagesSpecified
        {
            get { return this.ReleasePages != null; }
            set { }
        }

        [XmlElement]
        public ErnstString ReleaseNumber { get; set; }

        [XmlIgnore]
        public bool ReleaseNumberSpecified
        {
            get { return this.ReleaseNumber != null; }
            set { }
        }

        [XmlElement]
        public ErnstString SubordinationPages { get; set; }

        [XmlIgnore]
        public bool SubordinationPagesSpecified
        {
            get { return this.SubordinationPages != null; }
            set { }
        }

        [XmlElement]
        public ErnstString SubordinationNumber { get; set; }

        [XmlIgnore]
        public bool SubordinationNumberSpecified
        {
            get { return this.SubordinationNumber != null; }
            set { }
        }

        [XmlElement]
        public ErnstString POAPages { get; set; }

        [XmlIgnore]
        public bool POAPagesSpecified
        {
            get { return this.POAPages != null; }
            set { }
        }

        [XmlElement]
        public RequestMortgage Mortgage { get; set; }

        [XmlIgnore]
        public bool MortgageSpecified
        {
            get { return this.Mortgage != null; }
            set { }
        }

        [XmlElement]
        public RequestDeed Deed { get; set; }

        [XmlIgnore]
        public bool DeedSpecified
        {
            get { return this.Deed != null; }
            set { }
        }

        [XmlElement]
        public RequestAssignment Assignment { get; set; }

        [XmlIgnore]
        public bool AssignmentSpecified
        {
            get { return this.Assignment != null; }
            set { }
        }

        [XmlElement]
        public RequestRelease Release { get; set; }

        [XmlIgnore]
        public bool ReleaseSpecified
        {
            get { return this.Release != null; }
            set { }
        }

        [XmlElement]
        public RequestSubordination Subordination { get; set; }

        [XmlIgnore]
        public bool SubordinationSpecified
        {
            get { return this.Subordination != null; }
            set { }
        }

        [XmlElement]
        public RequestStateQuestions StateQuestions { get; set; }

        [XmlIgnore]
        public bool StateQuestionsSpecified
        {
            get { return this.StateQuestions != null; }
            set { }
        }

        [XmlElement]
        public Error Error { get; set; }

        [XmlIgnore]
        public bool ErrorSpecified
        {
            get { return this.Error != null; }
            set { }
        }
    }
}
