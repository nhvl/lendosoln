namespace Ernst.Response
{
    using System.Xml.Serialization;

    public partial class Document
    {
        [XmlElement]
        public ErnstEnum<DocumentFormat> Format { get; set; }

        [XmlIgnore]
        public bool FormatSpecified
        {
            get { return this.Format != null; }
            set { }
        }

        [XmlElement]
        public ErnstEnum<DocumentEncoding> Encoding { get; set; }

        [XmlIgnore]
        public bool EncodingSpecified
        {
            get { return this.Encoding != null; }
            set { }
        }

        [XmlElement]
        public ErnstString Content { get; set; }

        [XmlIgnore]
        public bool ContentSpecified
        {
            get { return this.Content != null; }
            set { }
        }
    }
}
