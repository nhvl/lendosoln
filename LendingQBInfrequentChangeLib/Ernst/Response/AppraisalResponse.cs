namespace Ernst.Response
{
    using System.Xml.Serialization;

    public partial class AppraisalResponse
    {
        [XmlElement]
        public ErnstBoolean CallForQuoteAppraisal { get; set; }

        [XmlIgnore]
        public bool CallForQuoteAppraisalSpecified
        {
            get { return this.CallForQuoteAppraisal != null; }
            set { }
        }

        [XmlElement]
        public Fees AppraisalFees { get; set; }

        [XmlIgnore]
        public bool AppraisalFeesSpecified
        {
            get { return this.AppraisalFees != null; }
            set { }
        }

        [XmlElement]
        public Provider Provider { get; set; }

        [XmlIgnore]
        public bool ProviderSpecified
        {
            get { return this.Provider != null; }
            set { }
        }

        [XmlElement]
        public Notes Notes { get; set; }

        [XmlIgnore]
        public bool NotesSpecified
        {
            get { return this.Notes != null; }
            set { }
        }

        [XmlElement]
        public CallForQuote CallForQuote { get; set; }

        [XmlIgnore]
        public bool CallForQuoteSpecified
        {
            get { return this.CallForQuote != null; }
            set { }
        }

        [XmlElement]
        public Audit Audit { get; set; }

        [XmlIgnore]
        public bool AuditSpecified
        {
            get { return this.Audit != null; }
            set { }
        }

        [XmlElement]
        public Error Error { get; set; }

        [XmlIgnore]
        public bool ErrorSpecified
        {
            get { return this.Error != null; }
            set { }
        }
    }
}
