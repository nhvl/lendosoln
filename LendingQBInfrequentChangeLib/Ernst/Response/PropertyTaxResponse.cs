namespace Ernst.Response
{
    using System.Collections.Generic;
    using System.Xml.Serialization;

    public partial class PropertyTaxResponse
    {
        [XmlElement]
        public ErnstString TotalTaxAmount { get; set; }

        [XmlIgnore]
        public bool TotalTaxAmountSpecified
        {
            get { return this.TotalTaxAmount != null; }
            set { }
        }

        [XmlElement]
        public List<TaxAuthorityType> TaxAuthorities { get; set; }

        [XmlIgnore]
        public bool TaxAuthoritiesSpecified
        {
            get { return this.TaxAuthorities != null; }
            set { }
        }

        [XmlElement]
        public ErnstString Name { get; set; }

        [XmlIgnore]
        public bool NameSpecified
        {
            get { return this.Name != null; }
            set { }
        }

        [XmlElement]
        public ErnstString Message { get; set; }

        [XmlIgnore]
        public bool MessageSpecified
        {
            get { return this.Message != null; }
            set { }
        }
    }
}
