namespace Ernst.Response
{
    using System.Xml.Serialization;

    public partial class RecordingWorksheetSearches
    {
        [XmlElement]
        public ErnstString Heading { get; set; }

        [XmlIgnore]
        public bool HeadingSpecified
        {
            get { return this.Heading != null; }
            set { }
        }

        [XmlElement]
        public ErnstString Note { get; set; }

        [XmlIgnore]
        public bool NoteSpecified
        {
            get { return this.Note != null; }
            set { }
        }

        [XmlElement]
        public RecordingFee Fee { get; set; }

        [XmlIgnore]
        public bool FeeSpecified
        {
            get { return this.Fee != null; }
            set { }
        }

        [XmlElement]
        public Error Error { get; set; }

        [XmlIgnore]
        public bool ErrorSpecified
        {
            get { return this.Error != null; }
            set { }
        }
    }
}
