namespace Ernst.Response
{
    using System.Xml.Serialization;

    public partial class HUDHUDLine
    {
        [XmlElement]
        public ErnstString LineNumber { get; set; }

        [XmlIgnore]
        public bool LineNumberSpecified
        {
            get { return this.LineNumber != null; }
            set { }
        }

        [XmlElement]
        public ErnstString LineDescription { get; set; }

        [XmlIgnore]
        public bool LineDescriptionSpecified
        {
            get { return this.LineDescription != null; }
            set { }
        }

        [XmlElement]
        public ErnstString TitlePremium { get; set; }

        [XmlIgnore]
        public bool TitlePremiumSpecified
        {
            get { return this.TitlePremium != null; }
            set { }
        }

        [XmlElement]
        public ErnstString OwnersTitlePremium { get; set; }

        [XmlIgnore]
        public bool OwnersTitlePremiumSpecified
        {
            get { return this.OwnersTitlePremium != null; }
            set { }
        }

        [XmlElement]
        public ErnstString DeedAmount { get; set; }

        [XmlIgnore]
        public bool DeedAmountSpecified
        {
            get { return this.DeedAmount != null; }
            set { }
        }

        [XmlElement]
        public ErnstString MortgageAmount { get; set; }

        [XmlIgnore]
        public bool MortgageAmountSpecified
        {
            get { return this.MortgageAmount != null; }
            set { }
        }

        [XmlElement]
        public ErnstString ReleaseAmount { get; set; }

        [XmlIgnore]
        public bool ReleaseAmountSpecified
        {
            get { return this.ReleaseAmount != null; }
            set { }
        }
    }
}
