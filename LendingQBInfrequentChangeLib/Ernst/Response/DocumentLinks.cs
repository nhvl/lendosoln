namespace Ernst.Response
{
    using System.Collections.Generic;
    using System.Xml.Serialization;

    public partial class DocumentLinks
    {
        [XmlElement("Link")]
        public List<DocumentLinksLink> LinkList { get; set; }

        [XmlIgnore]
        public bool LinkListSpecified
        {
            get { return this.LinkList != null; }
            set { }
        }

        [XmlElement]
        public Error Error { get; set; }

        [XmlIgnore]
        public bool ErrorSpecified
        {
            get { return this.Error != null; }
            set { }
        }
    }
}
