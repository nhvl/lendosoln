namespace Ernst.Response
{
    using System.Xml.Serialization;

    public partial class Block6Service
    {
        [XmlElement]
        public ErnstString Name { get; set; }

        [XmlIgnore]
        public bool NameSpecified
        {
            get { return this.Name != null; }
            set { }
        }

        [XmlElement]
        public Block6ServiceVendor Vendor { get; set; }

        [XmlIgnore]
        public bool VendorSpecified
        {
            get { return this.Vendor != null; }
            set { }
        }
    }
}
