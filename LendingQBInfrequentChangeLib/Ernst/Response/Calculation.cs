namespace Ernst.Response
{
    using System.Xml.Serialization;

    public partial class Calculation
    {
        [XmlElement]
        public CalculationDeed Deed { get; set; }

        [XmlIgnore]
        public bool DeedSpecified
        {
            get { return this.Deed != null; }
            set { }
        }

        [XmlElement]
        public CalculationMortgage Mortgage { get; set; }

        [XmlIgnore]
        public bool MortgageSpecified
        {
            get { return this.Mortgage != null; }
            set { }
        }

        [XmlElement]
        public CalculationAssignment Assignment { get; set; }

        [XmlIgnore]
        public bool AssignmentSpecified
        {
            get { return this.Assignment != null; }
            set { }
        }

        [XmlElement]
        public CalculationRelease Release { get; set; }

        [XmlIgnore]
        public bool ReleaseSpecified
        {
            get { return this.Release != null; }
            set { }
        }

        [XmlElement]
        public CalculationSubordination Subordination { get; set; }

        [XmlIgnore]
        public bool SubordinationSpecified
        {
            get { return this.Subordination != null; }
            set { }
        }

        [XmlElement]
        public CalculationPOA POA { get; set; }

        [XmlIgnore]
        public bool POASpecified
        {
            get { return this.POA != null; }
            set { }
        }

        [XmlElement]
        public ErnstString TitleInsurance { get; set; }

        [XmlIgnore]
        public bool TitleInsuranceSpecified
        {
            get { return this.TitleInsurance != null; }
            set { }
        }

        [XmlElement]
        public ErnstString OwnersTitleInsurance { get; set; }

        [XmlIgnore]
        public bool OwnersTitleInsuranceSpecified
        {
            get { return this.OwnersTitleInsurance != null; }
            set { }
        }

        [XmlElement]
        public HUD HUD { get; set; }

        [XmlIgnore]
        public bool HUDSpecified
        {
            get { return this.HUD != null; }
            set { }
        }

        [XmlElement]
        public HUDRESPA2010 HUDRESPA2010 { get; set; }

        [XmlIgnore]
        public bool HUDRESPA2010Specified
        {
            get { return this.HUDRESPA2010 != null; }
            set { }
        }

        [XmlElement]
        public CFPB2015 CFPB2015 { get; set; }

        [XmlIgnore]
        public bool CFPB2015Specified
        {
            get { return this.CFPB2015 != null; }
            set { }
        }

        [XmlElement]
        public GFERESPA2010 GFERESPA2010 { get; set; }

        [XmlIgnore]
        public bool GFERESPA2010Specified
        {
            get { return this.GFERESPA2010 != null; }
            set { }
        }

        [XmlElement]
        public ErnstString MortgageFee { get; set; }

        [XmlIgnore]
        public bool MortgageFeeSpecified
        {
            get { return this.MortgageFee != null; }
            set { }
        }

        [XmlElement]
        public ErnstString MortgageTax { get; set; }

        [XmlIgnore]
        public bool MortgageTaxSpecified
        {
            get { return this.MortgageTax != null; }
            set { }
        }

        [XmlElement]
        public ErnstString IntangibleTax { get; set; }

        [XmlIgnore]
        public bool IntangibleTaxSpecified
        {
            get { return this.IntangibleTax != null; }
            set { }
        }

        [XmlElement]
        public ErnstString MortgageNote { get; set; }

        [XmlIgnore]
        public bool MortgageNoteSpecified
        {
            get { return this.MortgageNote != null; }
            set { }
        }

        [XmlElement]
        public ErnstString DeedFee { get; set; }

        [XmlIgnore]
        public bool DeedFeeSpecified
        {
            get { return this.DeedFee != null; }
            set { }
        }

        [XmlElement]
        public ErnstString DeedTax { get; set; }

        [XmlIgnore]
        public bool DeedTaxSpecified
        {
            get { return this.DeedTax != null; }
            set { }
        }

        [XmlElement]
        public ErnstString DeedNote { get; set; }

        [XmlIgnore]
        public bool DeedNoteSpecified
        {
            get { return this.DeedNote != null; }
            set { }
        }

        [XmlElement]
        public Assumptions Assumptions { get; set; }

        [XmlIgnore]
        public bool AssumptionsSpecified
        {
            get { return this.Assumptions != null; }
            set { }
        }

        [XmlElement]
        public Error Error { get; set; }

        [XmlIgnore]
        public bool ErrorSpecified
        {
            get { return this.Error != null; }
            set { }
        }
    }
}
