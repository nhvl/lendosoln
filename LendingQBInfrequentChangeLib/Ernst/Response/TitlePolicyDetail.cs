namespace Ernst.Response
{
    using System.Collections.Generic;
    using System.Xml.Serialization;

    public partial class TitlePolicyDetail
    {
        [XmlElement]
        public ErnstString Name { get; set; }

        [XmlIgnore]
        public bool NameSpecified
        {
            get { return this.Name != null; }
            set { }
        }

        [XmlElement]
        public ErnstDecimal Premium { get; set; }

        [XmlIgnore]
        public bool PremiumSpecified
        {
            get { return this.Premium != null; }
            set { }
        }


        [XmlElement]
        public ErnstDecimal SalesTax { get; set; }

        [XmlIgnore]
        public bool SalesTaxSpecified
        {
            get { return this.SalesTax != null; }
            set { }
        }

        [XmlElement]
        public Splits Splits { get; set; }

        [XmlIgnore]
        public bool SplitsSpecified
        {
            get { return this.Splits != null; }
            set { }
        }

        [XmlElement]
        public TitlePolicyItemized ItemizedCharges { get; set; }

        [XmlIgnore]
        public bool ItemizedChargesSpecified
        {
            get { return this.ItemizedCharges != null; }
            set { }
        }

        [XmlElement]
        public ErnstBoolean CallForQuote { get; set; }

        [XmlIgnore]
        public bool CallForQuoteSpecified
        {
            get { return this.CallForQuote != null; }
            set { }
        }
    }
}
