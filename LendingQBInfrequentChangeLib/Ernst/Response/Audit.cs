namespace Ernst.Response
{
    using System.Collections.Generic;
    using System.Xml.Serialization;

    public partial class Audit
    {
        [XmlElement("Item")]
        public List<AuditItem> ItemList { get; set; }

        [XmlIgnore]
        public bool ItemListSpecified
        {
            get { return this.ItemList != null; }
            set { }
        }
    }
}
