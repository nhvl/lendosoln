namespace Ernst.Response
{
    using System.Xml.Serialization;

    public partial class GFERESPA2010
    {
        [XmlElement]
        public GFELine GFE4 { get; set; }

        [XmlIgnore]
        public bool GFE4Specified
        {
            get { return this.GFE4 != null; }
            set { }
        }

        [XmlElement]
        public GFELine GFE5 { get; set; }

        [XmlIgnore]
        public bool GFE5Specified
        {
            get { return this.GFE5 != null; }
            set { }
        }

        [XmlElement]
        public GFELine GFE7 { get; set; }

        [XmlIgnore]
        public bool GFE7Specified
        {
            get { return this.GFE7 != null; }
            set { }
        }

        [XmlElement]
        public GFELine GFE8 { get; set; }

        [XmlIgnore]
        public bool GFE8Specified
        {
            get { return this.GFE8 != null; }
            set { }
        }

        [XmlElement]
        public Error Error { get; set; }

        [XmlIgnore]
        public bool ErrorSpecified
        {
            get { return this.Error != null; }
            set { }
        }
    }
}
