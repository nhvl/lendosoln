namespace Ernst.Response
{
    using System.Collections.Generic;
    using System.Xml.Serialization;

    public partial class TitleResponse
    {
        [XmlElement]
        public ErnstBoolean CallForQuoteSettlement { get; set; }

        [XmlIgnore]
        public bool CallForQuoteSettlementSpecified
        {
            get { return this.CallForQuoteSettlement != null; }
            set { }
        }

        [XmlElement]
        public TitleResponseItemizedSettlementFees ItemizedSettlementFees { get; set; }

        [XmlIgnore]
        public bool ItemizedSettlementFeesSpecified
        {
            get { return this.ItemizedSettlementFees != null; }
            set { }
        }

        [XmlElement]
        public TitlePolicy LendersPolicy { get; set; }

        [XmlIgnore]
        public bool LendersPolicySpecified
        {
            get { return this.LendersPolicy != null; }
            set { }
        }

        [XmlElement]
        public TitlePolicy OwnersPolicy { get; set; }

        [XmlIgnore]
        public bool OwnersPolicySpecified
        {
            get { return this.OwnersPolicy != null; }
            set { }
        }

        [XmlElement]
        public ErnstBoolean CallForQuoteEndorsement { get; set; }

        [XmlIgnore]
        public bool CallForQuoteEndorsementSpecified
        {
            get { return this.CallForQuoteEndorsement != null; }
            set { }
        }

        [XmlElement]
        public TitleResponseEndorsements Endorsements { get; set; }

        [XmlIgnore]
        public bool EndorsementsSpecified
        {
            get { return this.Endorsements != null; }
            set { }
        }

        [XmlElement]
        public Provider Provider { get; set; }

        [XmlIgnore]
        public bool ProviderSpecified
        {
            get { return this.Provider != null; }
            set { }
        }

        [XmlElement]
        public Notes Notes { get; set; }

        [XmlIgnore]
        public bool NotesSpecified
        {
            get { return this.Notes != null; }
            set { }
        }

        [XmlElement]
        public TitleResponseCallForQuote CallForQuote { get; set; }

        [XmlIgnore]
        public bool CallForQuoteSpecified
        {
            get { return this.CallForQuote != null; }
            set { }
        }

        [XmlElement]
        public Audit Audit { get; set; }

        [XmlIgnore]
        public bool AuditSpecified
        {
            get { return this.Audit != null; }
            set { }
        }

        [XmlElement]
        public Error Error { get; set; }

        [XmlIgnore]
        public bool ErrorSpecified
        {
            get { return this.Error != null; }
            set { }
        }
    }
}
