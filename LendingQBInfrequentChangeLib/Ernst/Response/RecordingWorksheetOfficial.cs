namespace Ernst.Response
{
    using System.Xml.Serialization;

    public partial class RecordingWorksheetOfficial
    {
        [XmlElement]
        public ErnstString Description { get; set; }

        [XmlIgnore]
        public bool DescriptionSpecified
        {
            get { return this.Description != null; }
            set { }
        }

        [XmlElement]
        public ErnstString Name { get; set; }

        [XmlIgnore]
        public bool NameSpecified
        {
            get { return this.Name != null; }
            set { }
        }

        [XmlElement]
        public ErnstString Title { get; set; }

        [XmlIgnore]
        public bool TitleSpecified
        {
            get { return this.Title != null; }
            set { }
        }

        [XmlElement]
        public Error Error { get; set; }

        [XmlIgnore]
        public bool ErrorSpecified
        {
            get { return this.Error != null; }
            set { }
        }
    }
}
