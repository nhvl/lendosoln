namespace Ernst.Response
{
    using System.Xml.Serialization;

    public partial class CallForQuote
    {
        [XmlElement]
        public ErnstString Message { get; set; }

        [XmlIgnore]
        public bool MessageSpecified
        {
            get { return this.Message != null; }
            set { }
        }
    }
}
