namespace Ernst.Response
{
    using System.Xml.Serialization;

    public partial class DataRetax
    {
        [XmlElement]
        public ErnstString TAX1 { get; set; }

        [XmlIgnore]
        public bool TAX1Specified
        {
            get { return this.TAX1 != null; }
            set { }
        }

        [XmlElement]
        public ErnstString TAX1CD { get; set; }

        [XmlIgnore]
        public bool TAX1CDSpecified
        {
            get { return this.TAX1CD != null; }
            set { }
        }
    }
}
