namespace Ernst.Response
{
    using System.Xml.Serialization;

    public partial class DocumentResponse
    {
        [XmlElement]
        public Document Providers { get; set; }

        [XmlIgnore]
        public bool ProvidersSpecified
        {
            get { return this.Providers != null; }
            set { }
        }

        [XmlElement]
        public ProvidersXml ProvidersXml { get; set; }

        [XmlIgnore]
        public bool ProvidersXmlSpecified
        {
            get { return this.ProvidersXml != null; }
            set { }
        }
    }
}
