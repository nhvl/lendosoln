namespace Ernst.Response
{
    using System.Xml.Serialization;

    public partial class HUDRESPA2010
    {
        [XmlElement]
        public BasicHUDLine HUDLine1101 { get; set; }

        [XmlIgnore]
        public bool HUDLine1101Specified
        {
            get { return this.HUDLine1101 != null; }
            set { }
        }

        [XmlElement]
        public BasicHUDLine HUDLine1103 { get; set; }

        [XmlIgnore]
        public bool HUDLine1103Specified
        {
            get { return this.HUDLine1103 != null; }
            set { }
        }

        [XmlElement]
        public BasicHUDLine HUDLine1104 { get; set; }

        [XmlIgnore]
        public bool HUDLine1104Specified
        {
            get { return this.HUDLine1104 != null; }
            set { }
        }

        [XmlElement]
        public BasicHUDLine HUDLine1105 { get; set; }

        [XmlIgnore]
        public bool HUDLine1105Specified
        {
            get { return this.HUDLine1105 != null; }
            set { }
        }

        [XmlElement]
        public BasicHUDLine HUDLine1106 { get; set; }

        [XmlIgnore]
        public bool HUDLine1106Specified
        {
            get { return this.HUDLine1106 != null; }
            set { }
        }

        [XmlElement]
        public BasicHUDLine HUDLine1201 { get; set; }

        [XmlIgnore]
        public bool HUDLine1201Specified
        {
            get { return this.HUDLine1201 != null; }
            set { }
        }

        [XmlElement]
        public ExtendedHUDLine HUDLine1202 { get; set; }

        [XmlIgnore]
        public bool HUDLine1202Specified
        {
            get { return this.HUDLine1202 != null; }
            set { }
        }

        [XmlElement]
        public BasicHUDLine HUDLine1203 { get; set; }

        [XmlIgnore]
        public bool HUDLine1203Specified
        {
            get { return this.HUDLine1203 != null; }
            set { }
        }

        [XmlElement]
        public ExtendedHUDLine HUDLine1204 { get; set; }

        [XmlIgnore]
        public bool HUDLine1204Specified
        {
            get { return this.HUDLine1204 != null; }
            set { }
        }

        [XmlElement]
        public ExtendedHUDLine HUDLine1205 { get; set; }

        [XmlIgnore]
        public bool HUDLine1205Specified
        {
            get { return this.HUDLine1205 != null; }
            set { }
        }

        [XmlElement]
        public HUDRESPA2010HUDLine1206 HUDLine1206 { get; set; }

        [XmlIgnore]
        public bool HUDLine1206Specified
        {
            get { return this.HUDLine1206 != null; }
            set { }
        }

        [XmlElement]
        public BasicHUDLine ComparisonOfGFEandHUD1Charges_1201 { get; set; }

        [XmlIgnore]
        public bool ComparisonOfGFEandHUD1Charges_1201Specified
        {
            get { return this.ComparisonOfGFEandHUD1Charges_1201 != null; }
            set { }
        }

        [XmlElement]
        public BasicHUDLine ComparisonOfGFEandHUD1Charges_1203 { get; set; }

        [XmlIgnore]
        public bool ComparisonOfGFEandHUD1Charges_1203Specified
        {
            get { return this.ComparisonOfGFEandHUD1Charges_1203 != null; }
            set { }
        }

        [XmlElement]
        public Error Error { get; set; }

        [XmlIgnore]
        public bool ErrorSpecified
        {
            get { return this.Error != null; }
            set { }
        }
    }
}
