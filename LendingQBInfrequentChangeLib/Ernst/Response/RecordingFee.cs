namespace Ernst.Response
{
    using System.Collections.Generic;
    using System.Xml.Serialization;

    public partial class RecordingFee
    {
        [XmlElement]
        public ErnstString Description { get; set; }

        [XmlIgnore]
        public bool DescriptionSpecified
        {
            get { return this.Description != null; }
            set { }
        }

        [XmlElement]
        public ErnstString Amount { get; set; }

        [XmlIgnore]
        public bool AmountSpecified
        {
            get { return this.Amount != null; }
            set { }
        }

        [XmlElement]
        public ErnstString Label { get; set; }

        [XmlIgnore]
        public bool LabelSpecified
        {
            get { return this.Label != null; }
            set { }
        }

        [XmlElement]
        public Error Error { get; set; }

        [XmlIgnore]
        public bool ErrorSpecified
        {
            get { return this.Error != null; }
            set { }
        }

        [XmlElement]
        public List<ErnstString> Text { get; set; }

        [XmlIgnore]
        public bool TextSpecified
        {
            get { return this.Text != null; }
            set { }
        }
    }
}
