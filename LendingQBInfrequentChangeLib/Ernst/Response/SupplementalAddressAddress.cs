namespace Ernst.Response
{
    using System.Xml.Serialization;

    public partial class SupplementalAddressAddress
    {
        [XmlElement]
        public ErnstString Type { get; set; }

        [XmlIgnore]
        public bool TypeSpecified
        {
            get { return this.Type != null; }
            set { }
        }

        [XmlElement]
        public ErnstString Name { get; set; }

        [XmlIgnore]
        public bool NameSpecified
        {
            get { return this.Name != null; }
            set { }
        }

        [XmlElement]
        public ErnstString Address1 { get; set; }

        [XmlIgnore]
        public bool Address1Specified
        {
            get { return this.Address1 != null; }
            set { }
        }

        [XmlElement]
        public ErnstString Address2 { get; set; }

        [XmlIgnore]
        public bool Address2Specified
        {
            get { return this.Address2 != null; }
            set { }
        }

        [XmlElement]
        public ErnstString City { get; set; }

        [XmlIgnore]
        public bool CitySpecified
        {
            get { return this.City != null; }
            set { }
        }

        [XmlElement]
        public ErnstString State { get; set; }

        [XmlIgnore]
        public bool StateSpecified
        {
            get { return this.State != null; }
            set { }
        }

        [XmlElement]
        public ErnstString Zip { get; set; }

        [XmlIgnore]
        public bool ZipSpecified
        {
            get { return this.Zip != null; }
            set { }
        }

        [XmlElement]
        public ErnstString Phone { get; set; }

        [XmlIgnore]
        public bool PhoneSpecified
        {
            get { return this.Phone != null; }
            set { }
        }

        [XmlElement]
        public ErnstString Fax { get; set; }

        [XmlIgnore]
        public bool FaxSpecified
        {
            get { return this.Fax != null; }
            set { }
        }

        [XmlElement]
        public ErnstString Email { get; set; }

        [XmlIgnore]
        public bool EmailSpecified
        {
            get { return this.Email != null; }
            set { }
        }

        [XmlElement]
        public Error Error { get; set; }

        [XmlIgnore]
        public bool ErrorSpecified
        {
            get { return this.Error != null; }
            set { }
        }
    }
}
