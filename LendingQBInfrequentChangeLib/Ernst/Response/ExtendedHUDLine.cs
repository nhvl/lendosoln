namespace Ernst.Response
{
    using System.Xml.Serialization;

    public partial class ExtendedHUDLine
    {
        [XmlElement]
        public BasicHUDLine Deed2010 { get; set; }

        [XmlIgnore]
        public bool Deed2010Specified
        {
            get { return this.Deed2010 != null; }
            set { }
        }

        [XmlElement]
        public MortgageLine Mortgage2010 { get; set; }

        [XmlIgnore]
        public bool Mortgage2010Specified
        {
            get { return this.Mortgage2010 != null; }
            set { }
        }

        [XmlElement]
        public BasicHUDLine Release2010 { get; set; }

        [XmlIgnore]
        public bool Release2010Specified
        {
            get { return this.Release2010 != null; }
            set { }
        }

        [XmlElement]
        public BasicHUDLine Assignment2010 { get; set; }

        [XmlIgnore]
        public bool Assignment2010Specified
        {
            get { return this.Assignment2010 != null; }
            set { }
        }

        [XmlElement]
        public BasicHUDLine POA2010 { get; set; }

        [XmlIgnore]
        public bool POA2010Specified
        {
            get { return this.POA2010 != null; }
            set { }
        }

        [XmlElement]
        public BasicHUDLine Subordination2010 { get; set; }

        [XmlIgnore]
        public bool Subordination2010Specified
        {
            get { return this.Subordination2010 != null; }
            set { }
        }

        [XmlElement]
        public BasicHUDLine Notorial2010 { get; set; }

        [XmlIgnore]
        public bool Notorial2010Specified
        {
            get { return this.Notorial2010 != null; }
            set { }
        }

        [XmlElement]
        public BasicHUDLine ResidentialMortgage2010 { get; set; }

        [XmlIgnore]
        public bool ResidentialMortgage2010Specified
        {
            get { return this.ResidentialMortgage2010 != null; }
            set { }
        }

        [XmlElement]
        public BasicHUDLine Other2010 { get; set; }

        [XmlIgnore]
        public bool Other2010Specified
        {
            get { return this.Other2010 != null; }
            set { }
        }
    }
}
