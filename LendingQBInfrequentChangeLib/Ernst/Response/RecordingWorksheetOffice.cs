namespace Ernst.Response
{
    using System.Xml.Serialization;

    public partial class RecordingWorksheetOffice
    {
        [XmlElement]
        public ErnstString County { get; set; }

        [XmlIgnore]
        public bool CountySpecified
        {
            get { return this.County != null; }
            set { }
        }

        [XmlElement]
        public ErnstString CourierAddress1 { get; set; }

        [XmlIgnore]
        public bool CourierAddress1Specified
        {
            get { return this.CourierAddress1 != null; }
            set { }
        }

        [XmlElement]
        public ErnstString CourierAddress2 { get; set; }

        [XmlIgnore]
        public bool CourierAddress2Specified
        {
            get { return this.CourierAddress2 != null; }
            set { }
        }

        [XmlElement]
        public ErnstString CourierCityStateZIP { get; set; }

        [XmlIgnore]
        public bool CourierCityStateZIPSpecified
        {
            get { return this.CourierCityStateZIP != null; }
            set { }
        }

        [XmlElement]
        public ErnstString Department { get; set; }

        [XmlIgnore]
        public bool DepartmentSpecified
        {
            get { return this.Department != null; }
            set { }
        }

        [XmlElement]
        public ErnstString Fax { get; set; }

        [XmlIgnore]
        public bool FaxSpecified
        {
            get { return this.Fax != null; }
            set { }
        }

        [XmlElement]
        public ErnstString Header { get; set; }

        [XmlIgnore]
        public bool HeaderSpecified
        {
            get { return this.Header != null; }
            set { }
        }

        [XmlElement]
        public ErnstString Jurisdiction { get; set; }

        [XmlIgnore]
        public bool JurisdictionSpecified
        {
            get { return this.Jurisdiction != null; }
            set { }
        }

        [XmlElement]
        public ErnstString MailingAddress1 { get; set; }

        [XmlIgnore]
        public bool MailingAddress1Specified
        {
            get { return this.MailingAddress1 != null; }
            set { }
        }

        [XmlElement]
        public ErnstString MailingAddress2 { get; set; }

        [XmlIgnore]
        public bool MailingAddress2Specified
        {
            get { return this.MailingAddress2 != null; }
            set { }
        }

        [XmlElement]
        public ErnstString MailingCityStateZIP { get; set; }

        [XmlIgnore]
        public bool MailingCityStateZIPSpecified
        {
            get { return this.MailingCityStateZIP != null; }
            set { }
        }

        [XmlElement]
        public ErnstString MoneyOrder { get; set; }

        [XmlIgnore]
        public bool MoneyOrderSpecified
        {
            get { return this.MoneyOrder != null; }
            set { }
        }

        [XmlElement]
        public ErnstString Payee { get; set; }

        [XmlIgnore]
        public bool PayeeSpecified
        {
            get { return this.Payee != null; }
            set { }
        }

        [XmlElement]
        public ErnstString Phone { get; set; }

        [XmlIgnore]
        public bool PhoneSpecified
        {
            get { return this.Phone != null; }
            set { }
        }

        [XmlElement]
        public ErnstString PhoneExt { get; set; }

        [XmlIgnore]
        public bool PhoneExtSpecified
        {
            get { return this.PhoneExt != null; }
            set { }
        }

        [XmlElement]
        public ErnstString RecordingOffice { get; set; }

        [XmlIgnore]
        public bool RecordingOfficeSpecified
        {
            get { return this.RecordingOffice != null; }
            set { }
        }

        [XmlElement]
        public ErnstString TimeZone { get; set; }

        [XmlIgnore]
        public bool TimeZoneSpecified
        {
            get { return this.TimeZone != null; }
            set { }
        }

        [XmlElement]
        public ErnstString Website { get; set; }

        [XmlIgnore]
        public bool WebsiteSpecified
        {
            get { return this.Website != null; }
            set { }
        }

        [XmlElement]
        public Error Error { get; set; }

        [XmlIgnore]
        public bool ErrorSpecified
        {
            get { return this.Error != null; }
            set { }
        }
    }
}
