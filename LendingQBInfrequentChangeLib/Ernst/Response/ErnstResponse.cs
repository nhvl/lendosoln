namespace Ernst.Response
{
    using System.Xml.Serialization;

    public partial class ErnstResponse
    {
        [XmlElement]
        public Request Request { get; set; }

        [XmlIgnore]
        public bool RequestSpecified
        {
            get { return this.Request != null; }
            set { }
        }

        [XmlElement]
        public Data Data { get; set; }

        [XmlIgnore]
        public bool DataSpecified
        {
            get { return this.Data != null; }
            set { }
        }

        [XmlElement]
        public SupplementalAddress SupplementalAddress { get; set; }

        [XmlIgnore]
        public bool SupplementalAddressSpecified
        {
            get { return this.SupplementalAddress != null; }
            set { }
        }

        [XmlElement]
        public ErnstResponseDisplay Display { get; set; }

        [XmlIgnore]
        public bool DisplaySpecified
        {
            get { return this.Display != null; }
            set { }
        }

        [XmlElement]
        public Audit Audit { get; set; }

        [XmlIgnore]
        public bool AuditSpecified
        {
            get { return this.Audit != null; }
            set { }
        }

        [XmlElement]
        public Error Error { get; set; }

        [XmlIgnore]
        public bool ErrorSpecified
        {
            get { return this.Error != null; }
            set { }
        }
    }
}
