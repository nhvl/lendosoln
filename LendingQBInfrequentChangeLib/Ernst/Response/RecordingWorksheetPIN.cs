namespace Ernst.Response
{
    using System.Collections.Generic;
    using System.Xml.Serialization;

    public partial class RecordingWorksheetPIN
    {
        [XmlElement]
        public ErnstString PINRequired { get; set; }

        [XmlIgnore]
        public bool PINRequiredSpecified
        {
            get { return this.PINRequired != null; }
            set { }
        }

        [XmlElement]
        public ErnstString PINNote { get; set; }

        [XmlIgnore]
        public bool PINNoteSpecified
        {
            get { return this.PINNote != null; }
            set { }
        }

        [XmlElement]
        public ErnstString PINName { get; set; }

        [XmlIgnore]
        public bool PINNameSpecified
        {
            get { return this.PINName != null; }
            set { }
        }

        [XmlElement]
        public ErnstString PINForm { get; set; }

        [XmlIgnore]
        public bool PINFormSpecified
        {
            get { return this.PINForm != null; }
            set { }
        }

        [XmlElement]
        public Error Error { get; set; }

        [XmlIgnore]
        public bool ErrorSpecified
        {
            get { return this.Error != null; }
            set { }
        }

        [XmlElement]
        public List<ErnstString> Text { get; set; }

        [XmlIgnore]
        public bool TextSpecified
        {
            get { return this.Text != null; }
            set { }
        }
    }
}
