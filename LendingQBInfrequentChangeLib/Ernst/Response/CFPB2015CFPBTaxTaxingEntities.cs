﻿namespace Ernst.Response
{
    using System.Collections.Generic;
    using System.Xml.Serialization;

    public class CFPB2015CFPBTaxTaxingEntities
    {
        [XmlElement("TaxingEntity")]
        public List<CFPB2015CFPBTaxTaxingEntity> TaxingEntityList { get; set; }

        [XmlIgnore]
        public bool TaxingEntityListSpecified
        {
            get { return this.TaxingEntityList != null; }
            set { }
        }
    }
}
