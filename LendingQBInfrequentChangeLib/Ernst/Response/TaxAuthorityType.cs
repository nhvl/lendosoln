namespace Ernst.Response
{
    using System.Xml.Serialization;

    public partial class TaxAuthorityType
    {
        [XmlElement]
        public ErnstString Name { get; set; }

        [XmlIgnore]
        public bool NameSpecified
        {
            get { return this.Name != null; }
            set { }
        }

        [XmlElement]
        public ErnstString Type { get; set; }

        [XmlIgnore]
        public bool TypeSpecified
        {
            get { return this.Type != null; }
            set { }
        }

        [XmlElement]
        public ErnstDecimal TaxAmount { get; set; }

        [XmlIgnore]
        public bool TaxAmountSpecified
        {
            get { return this.TaxAmount != null; }
            set { }
        }
    }
}
