namespace Ernst.Response
{
    using System.Collections.Generic;
    using System.Xml.Serialization;

    public partial class RequestMortgage
    {
        [XmlElement]
        public ErnstString AmendmentModificationPages { get; set; }

        [XmlIgnore]
        public bool AmendmentModificationPagesSpecified
        {
            get { return this.AmendmentModificationPages != null; }
            set { }
        }

        [XmlElement]
        public Index Index { get; set; }

        [XmlIgnore]
        public bool IndexSpecified
        {
            get { return this.Index != null; }
            set { }
        }

        [XmlElement]
        public Error Error { get; set; }

        [XmlIgnore]
        public bool ErrorSpecified
        {
            get { return this.Error != null; }
            set { }
        }

        [XmlElement]
        public List<ErnstString> Text { get; set; }

        [XmlIgnore]
        public bool TextSpecified
        {
            get { return this.Text != null; }
            set { }
        }
    }
}
