namespace Ernst.Response
{
    using System.Xml.Serialization;

    public partial class Data
    {
        [XmlElement]
        public DataRefacts Refacts { get; set; }

        [XmlIgnore]
        public bool RefactsSpecified
        {
            get { return this.Refacts != null; }
            set { }
        }

        [XmlElement]
        public DataRetax Retax { get; set; }

        [XmlIgnore]
        public bool RetaxSpecified
        {
            get { return this.Retax != null; }
            set { }
        }

        [XmlElement]
        public Error Error { get; set; }

        [XmlIgnore]
        public bool ErrorSpecified
        {
            get { return this.Error != null; }
            set { }
        }
    }
}
