namespace Ernst.Response
{
    using System.Xml.Serialization;

    public partial class Error
    {
        [XmlElement]
        public ErnstString Code { get; set; }

        [XmlIgnore]
        public bool CodeSpecified
        {
            get { return this.Code != null; }
            set { }
        }

        [XmlElement]
        public ErnstString Description { get; set; }

        [XmlIgnore]
        public bool DescriptionSpecified
        {
            get { return this.Description != null; }
            set { }
        }
    }
}
