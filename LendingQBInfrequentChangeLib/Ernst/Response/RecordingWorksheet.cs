namespace Ernst.Response
{
    using System.Xml.Serialization;

    public partial class RecordingWorksheet
    {
        [XmlElement]
        public RecordingWorksheetOffice Office { get; set; }

        [XmlIgnore]
        public bool OfficeSpecified
        {
            get { return this.Office != null; }
            set { }
        }

        [XmlElement]
        public RecordingFees BasicRecordingFees { get; set; }

        [XmlIgnore]
        public bool BasicRecordingFeesSpecified
        {
            get { return this.BasicRecordingFees != null; }
            set { }
        }

        [XmlElement]
        public RecordingFees AdditionalRecordingFees { get; set; }

        [XmlIgnore]
        public bool AdditionalRecordingFeesSpecified
        {
            get { return this.AdditionalRecordingFees != null; }
            set { }
        }

        [XmlElement]
        public RecordingWorksheetRecordingFeeNotes RecordingFeeNotes { get; set; }

        [XmlIgnore]
        public bool RecordingFeeNotesSpecified
        {
            get { return this.RecordingFeeNotes != null; }
            set { }
        }

        [XmlElement]
        public RecordingWorksheetTaxes Taxes { get; set; }

        [XmlIgnore]
        public bool TaxesSpecified
        {
            get { return this.Taxes != null; }
            set { }
        }

        [XmlElement]
        public RecordingWorksheetSearches Searches { get; set; }

        [XmlIgnore]
        public bool SearchesSpecified
        {
            get { return this.Searches != null; }
            set { }
        }

        [XmlElement]
        public RecordingWorksheetRecordingNotes RecordingNotes { get; set; }

        [XmlIgnore]
        public bool RecordingNotesSpecified
        {
            get { return this.RecordingNotes != null; }
            set { }
        }

        [XmlElement]
        public ErnstString LDDescription { get; set; }

        [XmlIgnore]
        public bool LDDescriptionSpecified
        {
            get { return this.LDDescription != null; }
            set { }
        }

        [XmlElement]
        public RecordingWorksheetPIN PIN { get; set; }

        [XmlIgnore]
        public bool PINSpecified
        {
            get { return this.PIN != null; }
            set { }
        }

        [XmlElement]
        public ErnstString OnlineSearching { get; set; }

        [XmlIgnore]
        public bool OnlineSearchingSpecified
        {
            get { return this.OnlineSearching != null; }
            set { }
        }

        [XmlElement]
        public ErnstString ConformedCopies { get; set; }

        [XmlIgnore]
        public bool ConformedCopiesSpecified
        {
            get { return this.ConformedCopies != null; }
            set { }
        }

        [XmlElement]
        public ErnstString SASE { get; set; }

        [XmlIgnore]
        public bool SASESpecified
        {
            get { return this.SASE != null; }
            set { }
        }

        [XmlElement]
        public RecordingWorksheetUCCOfficials UCCOfficials { get; set; }

        [XmlIgnore]
        public bool UCCOfficialsSpecified
        {
            get { return this.UCCOfficials != null; }
            set { }
        }

        [XmlElement]
        public Error Error { get; set; }

        [XmlIgnore]
        public bool ErrorSpecified
        {
            get { return this.Error != null; }
            set { }
        }
    }
}
