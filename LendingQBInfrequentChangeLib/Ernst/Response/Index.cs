namespace Ernst.Response
{
    using System.Xml.Serialization;

    public partial class Index
    {
        [XmlElement]
        public ErnstString NumberOfGrantors { get; set; }

        [XmlIgnore]
        public bool NumberOfGrantorsSpecified
        {
            get { return this.NumberOfGrantors != null; }
            set { }
        }

        [XmlElement]
        public ErnstString NumberOfGrantees { get; set; }

        [XmlIgnore]
        public bool NumberOfGranteesSpecified
        {
            get { return this.NumberOfGrantees != null; }
            set { }
        }

        [XmlElement]
        public ErnstString NumberOfSurnames { get; set; }

        [XmlIgnore]
        public bool NumberOfSurnamesSpecified
        {
            get { return this.NumberOfSurnames != null; }
            set { }
        }

        [XmlElement]
        public ErnstString NumberOfSignatures { get; set; }

        [XmlIgnore]
        public bool NumberOfSignaturesSpecified
        {
            get { return this.NumberOfSignatures != null; }
            set { }
        }

        [XmlElement]
        public Error Error { get; set; }

        [XmlIgnore]
        public bool ErrorSpecified
        {
            get { return this.Error != null; }
            set { }
        }
    }
}
