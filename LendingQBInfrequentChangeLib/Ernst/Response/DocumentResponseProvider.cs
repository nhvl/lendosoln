namespace Ernst.Response
{
    using System.Xml.Serialization;

    public partial class DocumentResponseProvider
    {
        [XmlElement]
        public ErnstEnum<DocumentResponseProviderType> Type { get; set; }

        [XmlIgnore]
        public bool TypeSpecified
        {
            get { return this.Type != null; }
            set { }
        }


        [XmlElement]
        public ErnstString Name { get; set; }

        [XmlIgnore]
        public bool NameSpecified
        {
            get { return this.Name != null; }
            set { }
        }

        [XmlElement]
        public ErnstString Phone { get; set; }

        [XmlIgnore]
        public bool PhoneSpecified
        {
            get { return this.Phone != null; }
            set { }
        }

        [XmlElement]
        public ErnstString Web { get; set; }

        [XmlIgnore]
        public bool WebSpecified
        {
            get { return this.Web != null; }
            set { }
        }

        [XmlElement]
        public ErnstString Address { get; set; }

        [XmlIgnore]
        public bool AddressSpecified
        {
            get { return this.Address != null; }
            set { }
        }

        [XmlElement]
        public ErnstString City { get; set; }

        [XmlIgnore]
        public bool CitySpecified
        {
            get { return this.City != null; }
            set { }
        }

        [XmlElement]
        public ErnstString State { get; set; }

        [XmlIgnore]
        public bool StateSpecified
        {
            get { return this.State != null; }
            set { }
        }

        [XmlElement]
        public ErnstString Zip { get; set; }

        [XmlIgnore]
        public bool ZipSpecified
        {
            get { return this.Zip != null; }
            set { }
        }
    }
}
