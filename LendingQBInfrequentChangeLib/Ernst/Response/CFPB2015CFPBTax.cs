namespace Ernst.Response
{
    using System.Xml.Serialization;

    public partial class CFPB2015CFPBTax
    {
        [XmlElement]
        public ErnstEnum<CFPB2015CFPBTaxTaxType> TaxType { get; set; }

        [XmlIgnore]
        public bool TaxTypeSpecified
        {
            get { return this.TaxType != null; }
            set { }
        }

        [XmlElement]
        public ErnstString TotalTaxAmount { get; set; }

        [XmlIgnore]
        public bool TotalTaxAmountSpecified
        {
            get { return this.TotalTaxAmount != null; }
            set { }
        }

        [XmlElement]
        public ErnstString TotalBuyerSplit { get; set; }

        [XmlIgnore]
        public bool TotalBuyerSplitSpecified
        {
            get { return this.TotalBuyerSplit != null; }
            set { }
        }

        [XmlElement]
        public ErnstString TotalSellerSplit { get; set; }

        [XmlIgnore]
        public bool TotalSellerSplitSpecified
        {
            get { return this.TotalSellerSplit != null; }
            set { }
        }

        [XmlElement]
        public ErnstString TotalLenderSplit { get; set; }

        [XmlIgnore]
        public bool TotalLenderSplitSpecified
        {
            get { return this.TotalLenderSplit != null; }
            set { }
        }

        [XmlElement]
        public CFPB2015CFPBTaxTaxingEntities TaxingEntities { get; set; }

        [XmlIgnore]
        public bool TaxingEntitiesSpecified
        {
            get { return this.TaxingEntities != null; }
            set { }
        }
    }
}
