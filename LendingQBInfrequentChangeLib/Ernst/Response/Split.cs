namespace Ernst.Response
{
    using System.Xml.Serialization;

    public partial class Split
    {
        [XmlElement]
        public ErnstEnum<SplitPaidBy> PaidBy { get; set; }

        [XmlIgnore]
        public bool PaidBySpecified
        {
            get { return this.PaidBy != null; }
            set { }
        }

        [XmlElement]
        public ErnstDecimal Amount { get; set; }

        [XmlIgnore]
        public bool AmountSpecified
        {
            get { return this.Amount != null; }
            set { }
        }

        [XmlElement]
        public ErnstDecimal SalesTax { get; set; }

        [XmlIgnore]
        public bool SalesTaxSpecified
        {
            get { return this.SalesTax != null; }
            set { }
        }
    }
}
