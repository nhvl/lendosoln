namespace Ernst.Response
{
    using System.Xml.Serialization;

    public partial class Fee
    {
        [XmlElement]
        public ErnstString Code { get; set; }

        [XmlIgnore]
        public bool CodeSpecified
        {
            get { return this.Code != null; }
            set { }
        }

        [XmlElement]
        public ErnstString Name { get; set; }

        [XmlIgnore]
        public bool NameSpecified
        {
            get { return this.Name != null; }
            set { }
        }

        [XmlElement]
        public ErnstString MismoFeeType { get; set; }

        [XmlIgnore]
        public bool MismoFeeTypeSpecified
        {
            get { return this.MismoFeeType != null; }
            set { }
        }

        [XmlElement]
        public ErnstString HUDLineNumber { get; set; }

        [XmlIgnore]
        public bool HUDLineNumberSpecified
        {
            get { return this.HUDLineNumber != null; }
            set { }
        }

        [XmlElement]
        public ErnstDecimal Amount { get; set; }

        [XmlIgnore]
        public bool AmountSpecified
        {
            get { return this.Amount != null; }
            set { }
        }

        [XmlElement]
        public ErnstDecimal SalesTax { get; set; }

        [XmlIgnore]
        public bool SalesTaxSpecified
        {
            get { return this.SalesTax != null; }
            set { }
        }

        [XmlElement]
        public Splits Splits { get; set; }

        [XmlIgnore]
        public bool SplitsSpecified
        {
            get { return this.Splits != null; }
            set { }
        }

        [XmlElement]
        public ErnstBoolean CallForQuote { get; set; }

        [XmlIgnore]
        public bool CallForQuoteSpecified
        {
            get { return this.CallForQuote != null; }
            set { }
        }

        [XmlElement]
        public ErnstBoolean PrepaidFinanceCharge { get; set; }

        [XmlIgnore]
        public bool PrepaidFinanceChargeSpecified
        {
            get { return this.PrepaidFinanceCharge != null; }
            set { }
        }

        [XmlElement]
        public ErnstString LenderPercentPaid { get; set; }

        [XmlIgnore]
        public bool LenderPercentPaidSpecified
        {
            get { return this.LenderPercentPaid != null; }
            set { }
        }
    }
}
