namespace Ernst.Response
{
    using System.Xml.Serialization;

    public partial class RequestStateQuestions
    {
        [XmlElement]
        public ErnstBoolean Q1 { get; set; }

        [XmlIgnore]
        public bool Q1Specified
        {
            get { return this.Q1 != null; }
            set { }
        }

        [XmlElement]
        public ErnstBoolean Q2 { get; set; }

        [XmlIgnore]
        public bool Q2Specified
        {
            get { return this.Q2 != null; }
            set { }
        }

        [XmlElement]
        public ErnstBoolean Q3 { get; set; }

        [XmlIgnore]
        public bool Q3Specified
        {
            get { return this.Q3 != null; }
            set { }
        }

        [XmlElement]
        public ErnstBoolean Q4 { get; set; }

        [XmlIgnore]
        public bool Q4Specified
        {
            get { return this.Q4 != null; }
            set { }
        }

        [XmlElement]
        public ErnstBoolean Q5 { get; set; }

        [XmlIgnore]
        public bool Q5Specified
        {
            get { return this.Q5 != null; }
            set { }
        }

        [XmlElement]
        public ErnstBoolean Q6 { get; set; }

        [XmlIgnore]
        public bool Q6Specified
        {
            get { return this.Q6 != null; }
            set { }
        }

        [XmlElement]
        public ErnstBoolean Q7 { get; set; }

        [XmlIgnore]
        public bool Q7Specified
        {
            get { return this.Q7 != null; }
            set { }
        }

        [XmlElement]
        public ErnstBoolean Q8 { get; set; }

        [XmlIgnore]
        public bool Q8Specified
        {
            get { return this.Q8 != null; }
            set { }
        }

        [XmlElement]
        public ErnstBoolean Q9 { get; set; }

        [XmlIgnore]
        public bool Q9Specified
        {
            get { return this.Q9 != null; }
            set { }
        }

        [XmlElement]
        public ErnstBoolean Q10 { get; set; }

        [XmlIgnore]
        public bool Q10Specified
        {
            get { return this.Q10 != null; }
            set { }
        }

        [XmlElement]
        public ErnstBoolean Q11 { get; set; }

        [XmlIgnore]
        public bool Q11Specified
        {
            get { return this.Q11 != null; }
            set { }
        }

        [XmlElement]
        public ErnstBoolean Q12 { get; set; }

        [XmlIgnore]
        public bool Q12Specified
        {
            get { return this.Q12 != null; }
            set { }
        }

        [XmlElement]
        public ErnstBoolean Q13 { get; set; }

        [XmlIgnore]
        public bool Q13Specified
        {
            get { return this.Q13 != null; }
            set { }
        }

        [XmlElement]
        public ErnstBoolean Q14 { get; set; }

        [XmlIgnore]
        public bool Q14Specified
        {
            get { return this.Q14 != null; }
            set { }
        }

        [XmlElement]
        public ErnstBoolean Q15 { get; set; }

        [XmlIgnore]
        public bool Q15Specified
        {
            get { return this.Q15 != null; }
            set { }
        }

        [XmlElement]
        public ErnstBoolean Q16 { get; set; }

        [XmlIgnore]
        public bool Q16Specified
        {
            get { return this.Q16 != null; }
            set { }
        }

        [XmlElement]
        public ErnstBoolean Q17 { get; set; }

        [XmlIgnore]
        public bool Q17Specified
        {
            get { return this.Q17 != null; }
            set { }
        }

        [XmlElement]
        public ErnstBoolean Q18 { get; set; }

        [XmlIgnore]
        public bool Q18Specified
        {
            get { return this.Q18 != null; }
            set { }
        }

        [XmlElement]
        public ErnstBoolean Q19 { get; set; }

        [XmlIgnore]
        public bool Q19Specified
        {
            get { return this.Q19 != null; }
            set { }
        }

        [XmlElement]
        public ErnstBoolean Q20 { get; set; }

        [XmlIgnore]
        public bool Q20Specified
        {
            get { return this.Q20 != null; }
            set { }
        }

        [XmlElement]
        public ErnstBoolean Q21 { get; set; }

        [XmlIgnore]
        public bool Q21Specified
        {
            get { return this.Q21 != null; }
            set { }
        }

        [XmlElement]
        public ErnstBoolean Q22 { get; set; }

        [XmlIgnore]
        public bool Q22Specified
        {
            get { return this.Q22 != null; }
            set { }
        }

        [XmlElement]
        public ErnstBoolean Q23 { get; set; }

        [XmlIgnore]
        public bool Q23Specified
        {
            get { return this.Q23 != null; }
            set { }
        }

        [XmlElement]
        public ErnstBoolean Q24 { get; set; }

        [XmlIgnore]
        public bool Q24Specified
        {
            get { return this.Q24 != null; }
            set { }
        }

        [XmlElement]
        public ErnstBoolean Q25 { get; set; }

        [XmlIgnore]
        public bool Q25Specified
        {
            get { return this.Q25 != null; }
            set { }
        }

        [XmlElement]
        public ErnstBoolean Q26 { get; set; }

        [XmlIgnore]
        public bool Q26Specified
        {
            get { return this.Q26 != null; }
            set { }
        }

        [XmlElement]
        public ErnstBoolean Q27 { get; set; }

        [XmlIgnore]
        public bool Q27Specified
        {
            get { return this.Q27 != null; }
            set { }
        }

        [XmlElement]
        public ErnstBoolean Q28 { get; set; }

        [XmlIgnore]
        public bool Q28Specified
        {
            get { return this.Q28 != null; }
            set { }
        }

        [XmlElement]
        public ErnstBoolean Q29 { get; set; }

        [XmlIgnore]
        public bool Q29Specified
        {
            get { return this.Q29 != null; }
            set { }
        }

        [XmlElement]
        public ErnstBoolean Q30 { get; set; }

        [XmlIgnore]
        public bool Q30Specified
        {
            get { return this.Q30 != null; }
            set { }
        }

        [XmlElement]
        public ErnstBoolean Q31 { get; set; }

        [XmlIgnore]
        public bool Q31Specified
        {
            get { return this.Q31 != null; }
            set { }
        }

        [XmlElement]
        public ErnstBoolean Q32 { get; set; }

        [XmlIgnore]
        public bool Q32Specified
        {
            get { return this.Q32 != null; }
            set { }
        }

        [XmlElement]
        public ErnstBoolean Q33 { get; set; }

        [XmlIgnore]
        public bool Q33Specified
        {
            get { return this.Q33 != null; }
            set { }
        }

        [XmlElement]
        public ErnstBoolean Q34 { get; set; }

        [XmlIgnore]
        public bool Q34Specified
        {
            get { return this.Q34 != null; }
            set { }
        }

        [XmlElement]
        public ErnstBoolean Q35 { get; set; }

        [XmlIgnore]
        public bool Q35Specified
        {
            get { return this.Q35 != null; }
            set { }
        }

        [XmlElement]
        public ErnstBoolean Q36 { get; set; }

        [XmlIgnore]
        public bool Q36Specified
        {
            get { return this.Q36 != null; }
            set { }
        }

        [XmlElement]
        public ErnstBoolean Q37 { get; set; }

        [XmlIgnore]
        public bool Q37Specified
        {
            get { return this.Q37 != null; }
            set { }
        }

        [XmlElement]
        public ErnstBoolean Q38 { get; set; }

        [XmlIgnore]
        public bool Q38Specified
        {
            get { return this.Q38 != null; }
            set { }
        }

        [XmlElement]
        public ErnstBoolean Q39 { get; set; }

        [XmlIgnore]
        public bool Q39Specified
        {
            get { return this.Q39 != null; }
            set { }
        }

        [XmlElement]
        public ErnstBoolean Q40 { get; set; }

        [XmlIgnore]
        public bool Q40Specified
        {
            get { return this.Q40 != null; }
            set { }
        }

        [XmlElement]
        public ErnstBoolean Q41 { get; set; }

        [XmlIgnore]
        public bool Q41Specified
        {
            get { return this.Q41 != null; }
            set { }
        }

        [XmlElement]
        public ErnstBoolean Q42 { get; set; }

        [XmlIgnore]
        public bool Q42Specified
        {
            get { return this.Q42 != null; }
            set { }
        }

        [XmlElement]
        public ErnstBoolean Q43 { get; set; }

        [XmlIgnore]
        public bool Q43Specified
        {
            get { return this.Q43 != null; }
            set { }
        }

        [XmlElement]
        public ErnstBoolean Q44 { get; set; }

        [XmlIgnore]
        public bool Q44Specified
        {
            get { return this.Q44 != null; }
            set { }
        }

        [XmlElement]
        public ErnstBoolean Q45 { get; set; }

        [XmlIgnore]
        public bool Q45Specified
        {
            get { return this.Q45 != null; }
            set { }
        }

        [XmlElement]
        public ErnstBoolean Q46 { get; set; }

        [XmlIgnore]
        public bool Q46Specified
        {
            get { return this.Q46 != null; }
            set { }
        }

        [XmlElement]
        public ErnstBoolean Q47 { get; set; }

        [XmlIgnore]
        public bool Q47Specified
        {
            get { return this.Q47 != null; }
            set { }
        }

        [XmlElement]
        public ErnstBoolean Q48 { get; set; }

        [XmlIgnore]
        public bool Q48Specified
        {
            get { return this.Q48 != null; }
            set { }
        }

        [XmlElement]
        public ErnstBoolean Q49 { get; set; }

        [XmlIgnore]
        public bool Q49Specified
        {
            get { return this.Q49 != null; }
            set { }
        }

        [XmlElement]
        public ErnstBoolean Q50 { get; set; }

        [XmlIgnore]
        public bool Q50Specified
        {
            get { return this.Q50 != null; }
            set { }
        }

        [XmlElement]
        public ErnstInteger V1 { get; set; }

        [XmlIgnore]
        public bool V1Specified
        {
            get { return this.V1 != null; }
            set { }
        }

        [XmlElement]
        public ErnstInteger V2 { get; set; }

        [XmlIgnore]
        public bool V2Specified
        {
            get { return this.V2 != null; }
            set { }
        }

        [XmlElement]
        public ErnstInteger V3 { get; set; }

        [XmlIgnore]
        public bool V3Specified
        {
            get { return this.V3 != null; }
            set { }
        }

        [XmlElement]
        public ErnstInteger V4 { get; set; }

        [XmlIgnore]
        public bool V4Specified
        {
            get { return this.V4 != null; }
            set { }
        }

        [XmlElement]
        public ErnstInteger V5 { get; set; }

        [XmlIgnore]
        public bool V5Specified
        {
            get { return this.V5 != null; }
            set { }
        }

        [XmlElement]
        public ErnstInteger V6 { get; set; }

        [XmlIgnore]
        public bool V6Specified
        {
            get { return this.V6 != null; }
            set { }
        }

        [XmlElement]
        public ErnstInteger V7 { get; set; }

        [XmlIgnore]
        public bool V7Specified
        {
            get { return this.V7 != null; }
            set { }
        }

        [XmlElement]
        public ErnstInteger V8 { get; set; }

        [XmlIgnore]
        public bool V8Specified
        {
            get { return this.V8 != null; }
            set { }
        }

        [XmlElement]
        public ErnstInteger V9 { get; set; }

        [XmlIgnore]
        public bool V9Specified
        {
            get { return this.V9 != null; }
            set { }
        }

        [XmlElement]
        public ErnstInteger V10 { get; set; }

        [XmlIgnore]
        public bool V10Specified
        {
            get { return this.V10 != null; }
            set { }
        }

        [XmlElement]
        public ErnstInteger V11 { get; set; }

        [XmlIgnore]
        public bool V11Specified
        {
            get { return this.V11 != null; }
            set { }
        }

        [XmlElement]
        public ErnstInteger V12 { get; set; }

        [XmlIgnore]
        public bool V12Specified
        {
            get { return this.V12 != null; }
            set { }
        }

        [XmlElement]
        public ErnstInteger V13 { get; set; }

        [XmlIgnore]
        public bool V13Specified
        {
            get { return this.V13 != null; }
            set { }
        }

        [XmlElement]
        public ErnstInteger V14 { get; set; }

        [XmlIgnore]
        public bool V14Specified
        {
            get { return this.V14 != null; }
            set { }
        }

        [XmlElement]
        public ErnstInteger V15 { get; set; }

        [XmlIgnore]
        public bool V15Specified
        {
            get { return this.V15 != null; }
            set { }
        }

        [XmlElement]
        public ErnstInteger V16 { get; set; }

        [XmlIgnore]
        public bool V16Specified
        {
            get { return this.V16 != null; }
            set { }
        }

        [XmlElement]
        public ErnstInteger V17 { get; set; }

        [XmlIgnore]
        public bool V17Specified
        {
            get { return this.V17 != null; }
            set { }
        }

        [XmlElement]
        public ErnstInteger V18 { get; set; }

        [XmlIgnore]
        public bool V18Specified
        {
            get { return this.V18 != null; }
            set { }
        }

        [XmlElement]
        public ErnstInteger V19 { get; set; }

        [XmlIgnore]
        public bool V19Specified
        {
            get { return this.V19 != null; }
            set { }
        }

        [XmlElement]
        public ErnstInteger V20 { get; set; }

        [XmlIgnore]
        public bool V20Specified
        {
            get { return this.V20 != null; }
            set { }
        }

        [XmlElement]
        public ErnstInteger V21 { get; set; }

        [XmlIgnore]
        public bool V21Specified
        {
            get { return this.V21 != null; }
            set { }
        }

        [XmlElement]
        public ErnstInteger V22 { get; set; }

        [XmlIgnore]
        public bool V22Specified
        {
            get { return this.V22 != null; }
            set { }
        }

        [XmlElement]
        public ErnstInteger V23 { get; set; }

        [XmlIgnore]
        public bool V23Specified
        {
            get { return this.V23 != null; }
            set { }
        }

        [XmlElement]
        public ErnstInteger V24 { get; set; }

        [XmlIgnore]
        public bool V24Specified
        {
            get { return this.V24 != null; }
            set { }
        }

        [XmlElement]
        public ErnstInteger V25 { get; set; }

        [XmlIgnore]
        public bool V25Specified
        {
            get { return this.V25 != null; }
            set { }
        }

        [XmlElement]
        public ErnstInteger V26 { get; set; }

        [XmlIgnore]
        public bool V26Specified
        {
            get { return this.V26 != null; }
            set { }
        }

        [XmlElement]
        public ErnstInteger V27 { get; set; }

        [XmlIgnore]
        public bool V27Specified
        {
            get { return this.V27 != null; }
            set { }
        }

        [XmlElement]
        public ErnstInteger V28 { get; set; }

        [XmlIgnore]
        public bool V28Specified
        {
            get { return this.V28 != null; }
            set { }
        }

        [XmlElement]
        public ErnstInteger V29 { get; set; }

        [XmlIgnore]
        public bool V29Specified
        {
            get { return this.V29 != null; }
            set { }
        }

        [XmlElement]
        public ErnstInteger V30 { get; set; }

        [XmlIgnore]
        public bool V30Specified
        {
            get { return this.V30 != null; }
            set { }
        }

        [XmlElement]
        public ErnstInteger V31 { get; set; }

        [XmlIgnore]
        public bool V31Specified
        {
            get { return this.V31 != null; }
            set { }
        }

        [XmlElement]
        public ErnstInteger V32 { get; set; }

        [XmlIgnore]
        public bool V32Specified
        {
            get { return this.V32 != null; }
            set { }
        }

        [XmlElement]
        public ErnstInteger V33 { get; set; }

        [XmlIgnore]
        public bool V33Specified
        {
            get { return this.V33 != null; }
            set { }
        }

        [XmlElement]
        public ErnstInteger V34 { get; set; }

        [XmlIgnore]
        public bool V34Specified
        {
            get { return this.V34 != null; }
            set { }
        }

        [XmlElement]
        public ErnstInteger V35 { get; set; }

        [XmlIgnore]
        public bool V35Specified
        {
            get { return this.V35 != null; }
            set { }
        }

        [XmlElement]
        public ErnstInteger V36 { get; set; }

        [XmlIgnore]
        public bool V36Specified
        {
            get { return this.V36 != null; }
            set { }
        }

        [XmlElement]
        public ErnstInteger V37 { get; set; }

        [XmlIgnore]
        public bool V37Specified
        {
            get { return this.V37 != null; }
            set { }
        }

        [XmlElement]
        public ErnstInteger V38 { get; set; }

        [XmlIgnore]
        public bool V38Specified
        {
            get { return this.V38 != null; }
            set { }
        }

        [XmlElement]
        public ErnstInteger V39 { get; set; }

        [XmlIgnore]
        public bool V39Specified
        {
            get { return this.V39 != null; }
            set { }
        }

        [XmlElement]
        public ErnstInteger V40 { get; set; }

        [XmlIgnore]
        public bool V40Specified
        {
            get { return this.V40 != null; }
            set { }
        }

        [XmlElement]
        public ErnstInteger V41 { get; set; }

        [XmlIgnore]
        public bool V41Specified
        {
            get { return this.V41 != null; }
            set { }
        }

        [XmlElement]
        public ErnstInteger V42 { get; set; }

        [XmlIgnore]
        public bool V42Specified
        {
            get { return this.V42 != null; }
            set { }
        }

        [XmlElement]
        public ErnstInteger V43 { get; set; }

        [XmlIgnore]
        public bool V43Specified
        {
            get { return this.V43 != null; }
            set { }
        }

        [XmlElement]
        public ErnstInteger V44 { get; set; }

        [XmlIgnore]
        public bool V44Specified
        {
            get { return this.V44 != null; }
            set { }
        }

        [XmlElement]
        public ErnstInteger V45 { get; set; }

        [XmlIgnore]
        public bool V45Specified
        {
            get { return this.V45 != null; }
            set { }
        }

        [XmlElement]
        public ErnstInteger V46 { get; set; }

        [XmlIgnore]
        public bool V46Specified
        {
            get { return this.V46 != null; }
            set { }
        }

        [XmlElement]
        public ErnstInteger V47 { get; set; }

        [XmlIgnore]
        public bool V47Specified
        {
            get { return this.V47 != null; }
            set { }
        }

        [XmlElement]
        public ErnstInteger V48 { get; set; }

        [XmlIgnore]
        public bool V48Specified
        {
            get { return this.V48 != null; }
            set { }
        }

        [XmlElement]
        public ErnstInteger V49 { get; set; }

        [XmlIgnore]
        public bool V49Specified
        {
            get { return this.V49 != null; }
            set { }
        }

        [XmlElement]
        public ErnstInteger V50 { get; set; }

        [XmlIgnore]
        public bool V50Specified
        {
            get { return this.V50 != null; }
            set { }
        }

        [XmlElement]
        public Error Error { get; set; }

        [XmlIgnore]
        public bool ErrorSpecified
        {
            get { return this.Error != null; }
            set { }
        }
    }
}
