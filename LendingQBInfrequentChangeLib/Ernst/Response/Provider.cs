namespace Ernst.Response
{
    using System.Xml.Serialization;

    public partial class Provider
    {
        [XmlElement]
        public ErnstString ID { get; set; }

        [XmlIgnore]
        public bool IDSpecified
        {
            get { return this.ID != null; }
            set { }
        }

        [XmlElement]
        public ErnstString Name { get; set; }

        [XmlIgnore]
        public bool NameSpecified
        {
            get { return this.Name != null; }
            set { }
        }

        [XmlElement]
        public ErnstString Alias { get; set; }

        [XmlIgnore]
        public bool AliasSpecified
        {
            get { return this.Alias != null; }
            set { }
        }

        [XmlElement]
        public ErnstString Address { get; set; }

        [XmlIgnore]
        public bool AddressSpecified
        {
            get { return this.Address != null; }
            set { }
        }

        [XmlElement]
        public ErnstString City { get; set; }

        [XmlIgnore]
        public bool CitySpecified
        {
            get { return this.City != null; }
            set { }
        }

        [XmlElement]
        public ErnstString State { get; set; }

        [XmlIgnore]
        public bool StateSpecified
        {
            get { return this.State != null; }
            set { }
        }

        [XmlElement]
        public ErnstString Zip { get; set; }

        [XmlIgnore]
        public bool ZipSpecified
        {
            get { return this.Zip != null; }
            set { }
        }

        [XmlElement]
        public ErnstString Phone { get; set; }

        [XmlIgnore]
        public bool PhoneSpecified
        {
            get { return this.Phone != null; }
            set { }
        }

        [XmlElement]
        public ErnstString Website { get; set; }

        [XmlIgnore]
        public bool WebsiteSpecified
        {
            get { return this.Website != null; }
            set { }
        }

        [XmlElement]
        public ErnstString Disclaimer { get; set; }

        [XmlIgnore]
        public bool DisclaimerSpecified
        {
            get { return this.Disclaimer != null; }
            set { }
        }
    }
}
