namespace Ernst.Response
{
    using System.Xml.Serialization;

    public partial class GFELine
    {
        [XmlElement]
        public ErnstString LineDescription { get; set; }

        [XmlIgnore]
        public bool LineDescriptionSpecified
        {
            get { return this.LineDescription != null; }
            set { }
        }

        [XmlElement]
        public ErnstString Amount { get; set; }

        [XmlIgnore]
        public bool AmountSpecified
        {
            get { return this.Amount != null; }
            set { }
        }
    }
}
