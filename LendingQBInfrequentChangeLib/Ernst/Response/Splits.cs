﻿namespace Ernst.Response
{
    using System.Collections.Generic;
    using System.Xml.Serialization;

    public partial class Splits
    {
        [XmlElement("Split")]
        public List<Split> SplitList;

        [XmlIgnore]
        public bool SplitListSpecified
        {
            get { return this.SplitList != null; }
            set { }
        }
    }
}
