namespace Ernst.Response
{
    using System.Xml.Serialization;

    public partial class CalculationAssignment
    {
        [XmlElement]
        public ErnstString IndexFee { get; set; }

        [XmlIgnore]
        public bool IndexFeeSpecified
        {
            get { return this.IndexFee != null; }
            set { }
        }

        [XmlElement]
        public ErnstString Fee { get; set; }

        [XmlIgnore]
        public bool FeeSpecified
        {
            get { return this.Fee != null; }
            set { }
        }

        [XmlElement]
        public Error Error { get; set; }

        [XmlIgnore]
        public bool ErrorSpecified
        {
            get { return this.Error != null; }
            set { }
        }
    }
}
