﻿namespace Ernst
{
    using System.Xml.Serialization;

    /// <summary>
    /// A serialization class that encapsulates a decimal value for transmission to Ernst.
    /// </summary>
    public class ErnstBoolean
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ErnstBoolean"/> class.
        /// </summary>
        /// <remarks>A parameterless constructor is required for serialization.</remarks>
        public ErnstBoolean()
        {
        }

        /// <summary>
        /// Ernst represents true with the bit "1".
        /// </summary>
        [XmlIgnore]
        public const string TrueValue = "1";

        /// <summary>
        /// Ernst represents false with the bit "0".
        /// </summary>
        [XmlIgnore]
        public const string FalseValue = "0";

        /// <summary>
        /// Initializes a new instance of the <see cref="ErnstBoolean"/> class.
        /// </summary>
        /// <param name="value">A decimal value.</param>
        public ErnstBoolean(bool value)
        {
            this.BooleanValue = value;
        }

        /// <summary>
        /// Gets or sets the boolean value.
        /// </summary>
        [XmlIgnore]
        public bool BooleanValue { get; set; }

        /// <summary>
        /// Gets or sets the string representation of the boolean value.
        /// </summary>
        [XmlText(Type = typeof(string))]
        public string Value
        {
            get
            {
                return this.BooleanValue ? TrueValue : FalseValue;
            }

            set
            {
                this.BooleanValue = (value == TrueValue);
            }
        }

        /// <summary>
        /// Implicitly wraps a <see cref="bool"/> value in an <see cref="ErnstBoolean"/>.
        /// </summary>
        /// <param name="value">The decimal value.</param>
        public static implicit operator ErnstBoolean(bool value)
        {
            return new ErnstBoolean(value);
        }

        /// <summary>
        /// Implicitly unwraps a <see cref="bool"/> value from an <see cref="ErnstBoolean"/>.
        /// </summary>
        /// <param name="ernstDecimal">The wrapped value.</param>
        public static implicit operator bool (ErnstBoolean ernstIndicator)
        {
            return ernstIndicator.BooleanValue;
        }

        /// <summary>
        /// Implicitly unwraps a <see cref="bool?"/> value from an <see cref="ErnstBoolean"/>, handling when <param name="ernstIndicator"/> is null.
        /// </summary>
        /// <param name="ernstDecimal">The wrapped value.</param>
        public static implicit operator bool? (ErnstBoolean ernstIndicator)
        {
            return ernstIndicator?.BooleanValue;
        }
    }
}
