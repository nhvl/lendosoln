﻿namespace Ernst
{
    using System;
    using System.Xml.Serialization;

    /// <summary>
    /// A serialization class that encapsulates a decimal value for transmission to Ernst.
    /// </summary>
    public class ErnstDate
    {
        /// <summary>
        /// The date format for Ernst requests.
        /// </summary>
        /// <remarks>Ernst prefers leading zeros to be omitted when possible.</remarks>
        private const string DateFormat = "M/d/yyyy";

        /// <summary>
        /// Initializes a new instance of the <see cref="ErnstDate"/> class.
        /// </summary>
        /// <remarks>A parameterless constructor is required for serialization.</remarks>
        public ErnstDate()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ErnstDate"/> class.
        /// </summary>
        /// <param name="value">A DateTime value.</param>
        public ErnstDate(DateTime value)
        {
            this.DateTimeValue = value;
        }

        /// <summary>
        /// Gets or sets the DateTime value.
        /// </summary>
        [XmlIgnore]
        public DateTime DateTimeValue { get; set; }

        /// <summary>
        /// Gets or sets the string representation of the DateTime value.
        /// </summary>
        [XmlText(Type = typeof(string))]
        public string Value
        {
            get
            {
                return this.DateTimeValue.ToString(DateFormat);
            }

            set
            {
                this.DateTimeValue = DateTime.Parse(value);
            }
        }

        /// <summary>
        /// Implicitly wraps a <see cref="DateTime"/> value in an <see cref="ErnstDate"/>.
        /// </summary>
        /// <param name="value">The DateTime value.</param>
        public static implicit operator ErnstDate(DateTime value)
        {
            return new ErnstDate(value);
        }

        /// <summary>
        /// Implicitly unwraps a <see cref="DateTime"/> value from an <see cref="ErnstDate"/>.
        /// </summary>
        /// <param name="ernstDateTime">The wrapped value.</param>
        public static implicit operator DateTime(ErnstDate ernstDateTime)
        {
            return ernstDateTime.DateTimeValue;
        }
    }
}
