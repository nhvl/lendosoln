﻿namespace Ernst
{
    using System.Xml.Serialization;

    /// <summary>
    /// A serialization class that encapsulates a int value for transmission to Ernst.
    /// </summary>
    public class ErnstInteger
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ErnstInteger"/> class.
        /// </summary>
        /// <remarks>A parameterless constructor is required for serialization.</remarks>
        public ErnstInteger()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ErnstInteger"/> class.
        /// </summary>
        /// <param name="value">A int value.</param>
        public ErnstInteger(int value)
        {
            this.IntegerValue = value;
        }

        /// <summary>
        /// Gets or sets the integer value.
        /// </summary>
        [XmlIgnore]
        public int IntegerValue { get; set; }

        /// <summary>
        /// Gets or sets the string representation of the boolean value.
        /// </summary>
        [XmlText(Type = typeof(string))]
        public string Value
        {
            get
            {
                return this.IntegerValue.ToString();
            }

            set
            {
                this.IntegerValue = int.Parse(value);
            }
        }

        /// <summary>
        /// Implicitly wraps a <see cref="int"/> value in an <see cref="ErnstInteger"/>.
        /// </summary>
        /// <param name="value">The int value.</param>
        public static implicit operator ErnstInteger(int value)
        {
            return new ErnstInteger(value);
        }

        /// <summary>
        /// Implicitly unwraps a <see cref="int"/> value from an <see cref="ErnstInteger"/>.
        /// </summary>
        /// <param name="ErnstInteger">The wrapped value.</param>
        public static implicit operator int (ErnstInteger ErnstInteger)
        {
            return ErnstInteger.IntegerValue;
        }
    }
}
