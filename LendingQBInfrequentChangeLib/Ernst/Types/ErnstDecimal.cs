﻿namespace Ernst
{
    using System.Xml.Serialization;

    /// <summary>
    /// A serialization class that encapsulates a decimal value for transmission to Ernst.
    /// </summary>
    public class ErnstDecimal
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ErnstDecimal"/> class.
        /// </summary>
        /// <remarks>A parameterless constructor is required for serialization.</remarks>
        public ErnstDecimal()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ErnstDecimal"/> class.
        /// </summary>
        /// <param name="value">A decimal value.</param>
        public ErnstDecimal(decimal value)
        {
            this.DecimalValue = value;
        }

        /// <summary>
        /// Gets or sets the decimal value.
        /// </summary>
        [XmlIgnore]
        public decimal DecimalValue { get; set; }

        /// <summary>
        /// Gets or sets the string representation of the boolean value.
        /// </summary>
        [XmlText(Type = typeof(string))]
        public string Value
        {
            get
            {
                return this.DecimalValue.ToString();
            }

            set
            {
                this.DecimalValue = decimal.Parse(value);
            }
        }

        /// <summary>
        /// Implicitly wraps a <see cref="decimal"/> value in an <see cref="ErnstDecimal"/>.
        /// </summary>
        /// <param name="value">The decimal value.</param>
        public static implicit operator ErnstDecimal(decimal value)
        {
            return new ErnstDecimal(value);
        }

        /// <summary>
        /// Implicitly unwraps a <see cref="decimal"/> value from an <see cref="ErnstDecimal"/>.
        /// </summary>
        /// <param name="ernstDecimal">The wrapped value.</param>
        public static implicit operator decimal(ErnstDecimal ernstDecimal)
        {
            return ernstDecimal.DecimalValue;
        }
    }
}
