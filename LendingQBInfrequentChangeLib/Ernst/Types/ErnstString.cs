﻿namespace Ernst
{
    using System.Xml.Serialization;

    /// <summary>
    /// A serialization class that encapsulates a decimal value for transmission to Ernst.
    /// </summary>
    public class ErnstString
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ErnstString"/> class.
        /// </summary>
        /// <remarks>A parameterless constructor is required for serialization.</remarks>
        public ErnstString()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ErnstString"/> class.
        /// </summary>
        /// <param name="value">A decimal value.</param>
        public ErnstString(string value)
        {
            this.Value = value;
        }

        /// <summary>
        /// Gets or sets the string value.
        /// </summary>
        [XmlText(Type = typeof(string))]
        public string Value { get; set; }

        /// <summary>
        /// Implicitly wraps a <see cref="string"/> value in an <see cref="ErnstString"/>.
        /// </summary>
        /// <param name="value">The decimal value.</param>
        public static implicit operator ErnstString(string value)
        {
            return new ErnstString(value);
        }

        /// <summary>
        /// Implicitly unwraps a <see cref="string"/> value from an <see cref="ErnstString"/>.
        /// </summary>
        /// <param name="ernstDecimal">The wrapped value.</param>
        public static implicit operator string (ErnstString ernstString)
        {
            return ernstString?.Value;
        }
    }
}
