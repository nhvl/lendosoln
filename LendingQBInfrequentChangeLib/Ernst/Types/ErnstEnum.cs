﻿namespace Ernst
{
    using System;
    using System.Reflection;
    using System.Xml.Serialization;

    /// <summary>
    /// A serialization class that encapsulates an enumeration value for transmission to Ernst.
    /// </summary>
    /// <typeparam name="TEnum">An enumeration type.</typeparam>
    public class ErnstEnum<TEnum> where TEnum : struct
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ErnstEnum"/> class.
        /// </summary>
        /// <remarks>A parameterless constructor is required for serialization.</remarks>
        public ErnstEnum()
        {
        }

        /// <summary>
        /// The encapsulated enum value.
        /// </summary>
        [XmlIgnore]
        private TEnum enumValue;

        /// <summary>
        /// Initializes a new instance of the <see cref="ErnstEnum"/> class.
        /// </summary>
        /// <param name="value">The enum value.</param>
        public ErnstEnum(TEnum value)
        {
            this.ValidateEnum(value);
            this.EnumValue = value;
        }

        /// <summary>
        /// Gets or sets the enumerated value of this <see cref="ErnstEnum{TEnum}"/>.
        /// </summary>
        [XmlIgnore]
        public TEnum EnumValue
        {
            get
            {
                return this.enumValue;
            }

            set
            {
                this.ValidateEnum(value);
                this.enumValue = value;
            }
        }

        /// <summary>
        /// Gets the enumerated value as a string for serialization.
        /// </summary>
        /// <value>The enumerated value as a string for serialization.</value>
        [XmlText(Type = typeof(string))]
        public string Value
        {
            get
            {
                return ConvertEnumToString(this.EnumValue as Enum);
            }
            set
            {
                TEnum enumValue;
                Enum.TryParse(value, out enumValue);
                this.ValidateEnum(enumValue);
                this.EnumValue = enumValue;
            }
        }

        /// <summary>
        /// Implicitly wraps an enum value in an <see cref="ErnstEnum{TEnum}"/>.
        /// </summary>
        /// <param name="value">The enum value to convert.</param>
        public static implicit operator ErnstEnum<TEnum>(TEnum value)
        {
            return new ErnstEnum<TEnum>(value);
        }

        public static implicit operator TEnum(ErnstEnum<TEnum> ernstEnum)
        {
            return ernstEnum.EnumValue;
        }

        /// <summary>
        /// Validates that the <see cref="TEnum"/> is an <see cref="Enum"/>.
        /// </summary>
        /// <param name="value">The enum value.</param>
        /// <exception cref="InvalidOperationException">If <see cref="TEnum"/> is not an <see cref="Enum"/>.</exception>
        private void ValidateEnum(TEnum value)
        {
            if (!typeof(TEnum).IsEnum)
            {
                throw new InvalidOperationException(this.GetType() + ": Type of TEnum must be an Enum type. Type was " + typeof(TEnum));
            }
        }

        /// <summary>
        /// Converts the enum value to the appropriate string, using the <see cref="XmlEnumAttribute"/>
        /// name if applicable.
        /// </summary>
        /// <param name="e">The enumeration value.</param>
        /// <returns>The string value of the enum.</returns>
        private static string ConvertEnumToString(Enum e)
        {
            Type t = e.GetType();

            FieldInfo info = t.GetField(e.ToString("G"));

            if (!info.IsDefined(typeof(XmlEnumAttribute), false))
            {
                return e.ToString("G");
            }

            object[] o = info.GetCustomAttributes(typeof(XmlEnumAttribute), false);
            var att = (XmlEnumAttribute)o[0];
            return att.Name;
        }
    }
}
