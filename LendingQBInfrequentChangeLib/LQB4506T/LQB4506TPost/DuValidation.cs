﻿namespace LQB4506T.LQB4506TPost
{
    using System.Xml;

    /// <summary>
    /// Represents DU Validation information relating to a 4506-T request.
    /// </summary>
    public class DuValidation : LQB4506T.AbstractXmlSerializable
    {
        /// <summary>
        /// Gets or sets the DU third party service provider name for the 4506-T vendor.
        /// </summary>
        /// <value>4506-T Service Provider Name.</value>
        public string ServiceProviderName
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the reference number for sending to Fannie Mae's DU service.
        /// </summary>
        /// <value>DU reference number.</value>
        public string ReferenceNumber
        {
            get; set;
        }

        /// <summary>
        /// Reads the next "DUValidation" element in the given <see cref="XmlReader"/> into this object's members.
        /// </summary>
        /// <param name="reader">The reader to read from.</param>
        public override void ReadXml(XmlReader reader)
        {
            // Consume opening tag
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "DUValidation")
            {
                reader.Read();
                if (reader.EOF)
                {
                    return;
                }
            }

            // Read data. Both attributes are required if DUValidation is specified.
            this.ServiceProviderName = this.ReadAttribute(reader, "_ServiceProviderName");
            this.ReferenceNumber = this.ReadAttribute(reader, "_ReferenceNumber");
            if (string.IsNullOrEmpty(this.ServiceProviderName) || string.IsNullOrEmpty(this.ReferenceNumber))
            {
                throw new XmlException("If the DUValidation element is provided, both _ServiceProviderName and _ReferenceNumber are required.");
            }

            // Consume closing tag
            if (reader.IsEmptyElement)
            {
                return;
            }

            while (!(reader.NodeType == XmlNodeType.EndElement && reader.Name == "DUValidation"))
            {
                reader.Read();
            }
        }

        /// <summary>
        /// Writes the DUValidation element to the specified <see cref="XmlWriter"/>.
        /// </summary>
        /// <param name="writer">The XmlWriter to write the element to.</param>
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("DUValidation");
            this.WriteAttribute(writer, "_ServiceProviderName", this.ServiceProviderName);
            this.WriteAttribute(writer, "_ReferenceNumber", this.ReferenceNumber);
            writer.WriteEndElement();
        }
    }
}