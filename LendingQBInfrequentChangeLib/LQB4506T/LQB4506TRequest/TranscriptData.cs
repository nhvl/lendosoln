// Generated by CodeMonkey on 7/31/2013 7:10:17 PM
// Tool locates in svn://11.12.13.13:9123/PetProjects/DavidDao/SimpleXsdConverter
using System;
using System.Collections.Generic;
using System.Xml;

namespace LQB4506T.LQB4506TRequest
{
    public class TranscriptData : LQB4506T.AbstractXmlSerializable
    {
        #region Private Member Variables
        private BorrInfo m_BorrInfo = null;
        private CurrentAddress m_CurrentAddress = null;
        private PreviousAddress m_PreviousAddress = null;
        private ThirdPartyMailTo m_ThirdPartyMailTo = null;
        private TranscriptOptions m_TranscriptOptions = null;
        #endregion

        #region Public Properties
        public BorrInfo BorrInfo
        {
            get
            {
                if (null == m_BorrInfo) m_BorrInfo = new BorrInfo();
                return m_BorrInfo;
            }
            set { m_BorrInfo = value; }
        }
        public CurrentAddress CurrentAddress
        {
            get
            {
                if (null == m_CurrentAddress) m_CurrentAddress = new CurrentAddress();
                return m_CurrentAddress;
            }
            set { m_CurrentAddress = value; }
        }
        public PreviousAddress PreviousAddress
        {
            get
            {
                if (null == m_PreviousAddress) m_PreviousAddress = new PreviousAddress();
                return m_PreviousAddress;
            }
            set { m_PreviousAddress = value; }
        }
        public ThirdPartyMailTo ThirdPartyMailTo
        {
            get
            {
                if (null == m_ThirdPartyMailTo) m_ThirdPartyMailTo = new ThirdPartyMailTo();
                return m_ThirdPartyMailTo;
            }
            set { m_ThirdPartyMailTo = value; }
        }
        public TranscriptOptions TranscriptOptions
        {
            get
            {
                if (null == m_TranscriptOptions) m_TranscriptOptions = new TranscriptOptions();
                return m_TranscriptOptions;
            }
            set { m_TranscriptOptions = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "TranscriptData")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "TranscriptData") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                switch (reader.Name)
                {
                    case "BorrInfo":
                        ReadElement(reader, BorrInfo);
                        break;
                    case "Current_Address":
                        ReadElement(reader, CurrentAddress);
                        break;
                    case "Previous_Address":
                        ReadElement(reader, PreviousAddress);
                        break;
                    case "Third_Party_Mail_To":
                        ReadElement(reader, ThirdPartyMailTo);
                        break;
                    case "TranscriptOptions":
                        ReadElement(reader, TranscriptOptions);
                        break;
                }
                #endregion
            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("TranscriptData");
            WriteElement(writer, m_BorrInfo);
            WriteElement(writer, m_CurrentAddress);
            WriteElement(writer, m_PreviousAddress);
            WriteElement(writer, m_ThirdPartyMailTo);
            WriteElement(writer, m_TranscriptOptions);
            writer.WriteEndElement();
        }
    }
}
