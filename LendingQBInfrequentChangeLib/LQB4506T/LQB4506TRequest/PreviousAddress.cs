// Generated by CodeMonkey on 7/31/2013 7:10:17 PM
// Tool locates in svn://11.12.13.13:9123/PetProjects/DavidDao/SimpleXsdConverter
using System;
using System.Collections.Generic;
using System.Xml;

namespace LQB4506T.LQB4506TRequest
{
    public class PreviousAddress : LQB4506T.AbstractXmlSerializable
    {
        #region Private Member Variables
        #endregion

        #region Public Properties
        public string StreetAddr { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "Previous_Address")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            StreetAddr = ReadAttribute(reader, "_StreetAddr");
            City = ReadAttribute(reader, "_City");
            State = ReadAttribute(reader, "_State");
            Zip = ReadAttribute(reader, "_ZIP");
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "Previous_Address") return;

            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("Previous_Address");
            WriteAttribute(writer, "_StreetAddr", StreetAddr);
            WriteAttribute(writer, "_City", City);
            WriteAttribute(writer, "_State", State);
            WriteAttribute(writer, "_ZIP", Zip);
            writer.WriteEndElement();
        }
    }
}
