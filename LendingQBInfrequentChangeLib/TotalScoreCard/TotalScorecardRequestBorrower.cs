﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TotalScoreCard
{
    public class TotalScorecardRequestBorrower : AbstractXmlSerializable
    {
        public string Ssn { get; set; }
        public string BorrAge { get; set; }
        public string BorrName { get; set; }

        public List<E_TotalScorecardBorrRace> RaceList = new List<E_TotalScorecardBorrRace>();

        public void AddRace(E_TotalScorecardBorrRace race)
        {
            RaceList.Add(race);
        }

        public E_TotalScorecardBorrSex BorrSex { get; set; }
        public E_TotalScorecardRequestYN BorrEthnicity { get; set; }
        public string BorrBirthDate { get; set; }

        public override void ReadXml(System.Xml.XmlReader reader)
        {
            throw new NotImplementedException();
        }

        public override void WriteXml(System.Xml.XmlWriter writer)
        {
            writer.WriteStartElement("borrower");
            SafeElementWrite(writer, "ssn", Ssn);
            SafeElementWrite(writer, "borr_age", BorrAge);
            SafeElementWrite(writer, "borr_name", BorrName);
            foreach (var o in RaceList)
            {
                SafeElementWrite(writer, "borr_race", o, EnumMapping.Map_TotalScorecardBorrRace);
            }
            SafeElementWrite(writer, "borr_sex", BorrSex, EnumMapping.Map_TotalScorecardBorrSex);
            SafeElementWrite(writer, "borr_ethnicity", BorrEthnicity, EnumMapping.Map_TotalScorecardRequestYN);
            SafeElementWrite(writer, "borr_birth_date", BorrBirthDate);
            writer.WriteEndElement(); // </borrower>
        }
    }
}
