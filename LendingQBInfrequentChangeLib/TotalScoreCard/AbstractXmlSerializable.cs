﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace TotalScoreCard
{
    public abstract class AbstractXmlSerializable : IXmlSerializable
    {
        protected void SafeElementWrite(XmlWriter writer, string elementName, string value)
        {
            if (string.IsNullOrEmpty(value))
                return;

            writer.WriteElementString(elementName, value);

        }

        protected void SafeElementWrite<T>(XmlWriter writer, string elementName, T value, Dictionary<T, string> map)
        {
            string desc = map[value];
            if (string.IsNullOrEmpty(desc))
                return;

            writer.WriteElementString(elementName, desc);
        }

        #region IXmlSerializable Members
        public System.Xml.Schema.XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        public abstract void ReadXml(XmlReader reader);
        public abstract void WriteXml(XmlWriter writer);
        #endregion
    }
}
