﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace TotalScoreCard
{
    #region Enum
    public enum E_TotalScorecardRequestCreditReportType
    {
        Undefined = 0,
        Fannie
    }
    public enum E_TotalScorecardRequestAmortType
    {
        Undefined = 0,
        Arm,
        Buydown,
        Other

    }
    public enum E_TotalScorecardRequestBorrMaritalStatus
    {
        Undefined = 0,
        Married,
        Separated,
        Unmarried
    }

    public enum E_TotalScorecardRequestYN
    {
        Undefined = 0,
        Yes,
        No
    }
    public enum E_TotalScorecardLoanPurpose
    {
        Undefined = 0,
        PurchaseExistingHousePreviouslyOccupied,
        FinanceImprovementsExistingProperty,
        Refinance,
        PurchaseNewCondoUnit,
        PurchaseExistingCondoUnit,
        PurchaseExistingHouseNotPreviouslyOccupied,
        ConstructHome,
        HUDOnly
    }
    public enum E_TotalScorecardBorrType
    {
        Undefined = 0,
        Occupant,
        Landlord,
        Reserved_3,
        Reserved_4,
        EscrowCommitment,
        Reserved_6,
        Corporation,
        GovernmentAgency
    }
    public enum E_TotalScorecardGiftSource
    {
        Undefined = 0,
        NA,
        Relative,
        GovernmentAssistance,
        Employer,
        NonprofitReligious
    }
    public enum E_TotalScorecardCounselType
    {
        Undefined = 0,
        NA,
        NotCounseled,
        HUDApprovedCounselingAgency
    }
    public enum E_TotalScorecardSecondaryFinancingSrc
    {
        Undefined = 0,
        NA = 1,
        GovernmentNonprofitInstrumentality = 2,
        NonprofitNotInstrumentalityOfGovernment = 3,
        PrivateOrganizations = 4,
        Lender = 5,
        GovernmentStateOrLocal = 6,
        NonProfitInstrumentalityOfGovernment = 7,
        FamilyRelative = 8,
        FederalGovernment = 9
    }
    public enum E_TotalScorecardArmType
    {
        Undefined = 0,
        OneYearArm,
        HybridArm,
        NotAnArm,
    }
    public enum E_TotalScorecardRefinanceType
    {
        Undefined = 0,
        Unknown,
        Conventional,
        HOPEForHomeowners,
        NotRefinance,
        PriorFHA,
        StreamlineRefinance
    }
    public enum E_TotalScorecardBorrRace
    {
        Undefined = 0,
        NotApplicable,
        White,
        BlackOrAfricanAmerican,
        AmericanIndian,
        Asian,
        NativeHawaiian,
        NotDisclosed
    }
    public enum E_TotalScorecardBorrSex
    {
        Undefined = 0,
        Male,
        Female
    }
    #endregion

    #region Enum Mapping
    public static class EnumMapping
    {
        public static Dictionary<E_TotalScorecardRequestCreditReportType, string> Map_TotalScorecardRequestCreditReportType = new Dictionary<E_TotalScorecardRequestCreditReportType, string>()
        {
            { E_TotalScorecardRequestCreditReportType.Undefined, string.Empty },
            { E_TotalScorecardRequestCreditReportType.Fannie, "FANNIE" },
        };

        public static Dictionary<E_TotalScorecardRequestAmortType, string> Map_TotalScorecardRequestAmortType = new Dictionary<E_TotalScorecardRequestAmortType, string>()
        {
            { E_TotalScorecardRequestAmortType.Undefined, string.Empty },
            { E_TotalScorecardRequestAmortType.Arm, "A" },
            { E_TotalScorecardRequestAmortType.Buydown, "B" },
            { E_TotalScorecardRequestAmortType.Other, " " },
        };

        public static Dictionary<E_TotalScorecardRequestBorrMaritalStatus, string> Map_TotalScorecardRequestBorrMaritalStatus = new Dictionary<E_TotalScorecardRequestBorrMaritalStatus, string>()
        {
            { E_TotalScorecardRequestBorrMaritalStatus.Undefined, string.Empty },
            { E_TotalScorecardRequestBorrMaritalStatus.Married, "1" },
            { E_TotalScorecardRequestBorrMaritalStatus.Separated, "2" },
            { E_TotalScorecardRequestBorrMaritalStatus.Unmarried, "3" },
        };

        public static Dictionary<E_TotalScorecardRequestYN, string> Map_TotalScorecardRequestYN = new Dictionary<E_TotalScorecardRequestYN, string>()
        {
            { E_TotalScorecardRequestYN.Undefined, string.Empty },
            { E_TotalScorecardRequestYN.Yes, "Y" },
            { E_TotalScorecardRequestYN.No, "N" },
        };

        public static Dictionary<E_TotalScorecardLoanPurpose, string> Map_TotalScorecardLoanPurpose = new Dictionary<E_TotalScorecardLoanPurpose, string>()
        {
            { E_TotalScorecardLoanPurpose.Undefined, string.Empty },
            { E_TotalScorecardLoanPurpose.PurchaseExistingHousePreviouslyOccupied, "1" },
            { E_TotalScorecardLoanPurpose.FinanceImprovementsExistingProperty, "2" },
            { E_TotalScorecardLoanPurpose.Refinance, "3" },
            { E_TotalScorecardLoanPurpose.PurchaseNewCondoUnit, "4" },
            { E_TotalScorecardLoanPurpose.PurchaseExistingCondoUnit, "5" },
            { E_TotalScorecardLoanPurpose.PurchaseExistingHouseNotPreviouslyOccupied, "6" },
            { E_TotalScorecardLoanPurpose.ConstructHome, "7" },
            { E_TotalScorecardLoanPurpose.HUDOnly, "8" },
        };

        public static Dictionary<E_TotalScorecardBorrType, string> Map_TotalScorecardBorrType = new Dictionary<E_TotalScorecardBorrType, string>()
        {
            { E_TotalScorecardBorrType.Undefined, string.Empty },
            { E_TotalScorecardBorrType.Occupant, "1" },
            { E_TotalScorecardBorrType.Landlord, "2" },
            { E_TotalScorecardBorrType.Reserved_3, "3" },
            { E_TotalScorecardBorrType.Reserved_4, "4" },
            { E_TotalScorecardBorrType.EscrowCommitment, "5" },
            { E_TotalScorecardBorrType.Reserved_6, "6" },
            { E_TotalScorecardBorrType.Corporation, "7" },
            { E_TotalScorecardBorrType.GovernmentAgency, "8" },
        };

        public static Dictionary<E_TotalScorecardGiftSource, string> Map_TotalScorecardGiftSource = new Dictionary<E_TotalScorecardGiftSource, string>()
        {
            { E_TotalScorecardGiftSource.Undefined, string.Empty },
            { E_TotalScorecardGiftSource.NA, "00" },
            { E_TotalScorecardGiftSource.Relative, "01" },
            { E_TotalScorecardGiftSource.GovernmentAssistance, "03" },
            { E_TotalScorecardGiftSource.Employer, "06" },
            { E_TotalScorecardGiftSource.NonprofitReligious, "15" },
        };

        public static Dictionary<E_TotalScorecardCounselType, string> Map_TotalScorecardCounselType = new Dictionary<E_TotalScorecardCounselType, string>()
        {
            { E_TotalScorecardCounselType.Undefined, string.Empty },
            { E_TotalScorecardCounselType.NA, " " },
            { E_TotalScorecardCounselType.NotCounseled, "A" },
            { E_TotalScorecardCounselType.HUDApprovedCounselingAgency, "D" },
        };

        public static Dictionary<E_TotalScorecardSecondaryFinancingSrc, string> Map_TotalScorecardSecondaryFinancingSrc = new Dictionary<E_TotalScorecardSecondaryFinancingSrc, string>()
        {
            { E_TotalScorecardSecondaryFinancingSrc.Undefined, string.Empty },
            { E_TotalScorecardSecondaryFinancingSrc.NA, "00" },
            { E_TotalScorecardSecondaryFinancingSrc.GovernmentNonprofitInstrumentality, "01" },
            { E_TotalScorecardSecondaryFinancingSrc.NonprofitNotInstrumentalityOfGovernment, "02" },
            { E_TotalScorecardSecondaryFinancingSrc.PrivateOrganizations, "03" },
            { E_TotalScorecardSecondaryFinancingSrc.Lender, "04" },

            { E_TotalScorecardSecondaryFinancingSrc.GovernmentStateOrLocal, "07" },
            { E_TotalScorecardSecondaryFinancingSrc.NonProfitInstrumentalityOfGovernment, "08" },
            { E_TotalScorecardSecondaryFinancingSrc.FamilyRelative, "09" },
            { E_TotalScorecardSecondaryFinancingSrc.FederalGovernment, "10" }
        };

        public static Dictionary<E_TotalScorecardArmType, string> Map_TotalScorecardArmType = new Dictionary<E_TotalScorecardArmType, string>()
        {
            { E_TotalScorecardArmType.Undefined, string.Empty },
            { E_TotalScorecardArmType.OneYearArm, "1" },
            { E_TotalScorecardArmType.HybridArm, "H" },
            { E_TotalScorecardArmType.NotAnArm, "N" },
        };

        public static Dictionary<E_TotalScorecardRefinanceType, string> Map_TotalScorecardRefinanceType = new Dictionary<E_TotalScorecardRefinanceType, string>()
        {
            { E_TotalScorecardRefinanceType.Undefined, string.Empty },
            { E_TotalScorecardRefinanceType.Unknown, " " },
            { E_TotalScorecardRefinanceType.Conventional, "C" },
            { E_TotalScorecardRefinanceType.HOPEForHomeowners, "H" },
            { E_TotalScorecardRefinanceType.NotRefinance, "N" },
            { E_TotalScorecardRefinanceType.PriorFHA, "R" },
            { E_TotalScorecardRefinanceType.StreamlineRefinance, "S" },
        };

        public static Dictionary<E_TotalScorecardBorrRace, string> Map_TotalScorecardBorrRace = new Dictionary<E_TotalScorecardBorrRace, string>()
        {
            { E_TotalScorecardBorrRace.Undefined, string.Empty },
            { E_TotalScorecardBorrRace.NotApplicable, "0" },
            { E_TotalScorecardBorrRace.White, "1" },
            { E_TotalScorecardBorrRace.BlackOrAfricanAmerican, "2" },
            { E_TotalScorecardBorrRace.AmericanIndian, "3" },
            { E_TotalScorecardBorrRace.Asian, "4" },
            { E_TotalScorecardBorrRace.NativeHawaiian, "5" },
            { E_TotalScorecardBorrRace.NotDisclosed, "9" },
        };

        public static Dictionary<E_TotalScorecardBorrSex, string> Map_TotalScorecardBorrSex = new Dictionary<E_TotalScorecardBorrSex, string>()
        {
            { E_TotalScorecardBorrSex.Undefined, string.Empty },
            { E_TotalScorecardBorrSex.Male, "1" },
            { E_TotalScorecardBorrSex.Female, "2" },
        };





    }
    #endregion
    public class TotalScorecardRequest : AbstractXmlSerializable
    {
        public string MonthlyIncome { get; set; }
        public string AppraisedValue { get; set; }
        public string AssetsAfterClsg { get; set; }
        public string FhaCaseNumber { get; set; }
        public string LoanAmount { get; set; }
        public string Piti { get; set; }
        public string Mip { get; set; }
        public string Term { get; set; }
        public string SalePrice { get; set; }
        public string Applicants { get; set; }
        public string LoanNumber { get; set; }
        public string Ltv { get; set; }
        public string FrontEndRatio { get; set; }
        public string BackEndRatio { get; set; }
        public E_TotalScorecardRequestAmortType AmortType { get; set; }
        public string UnderwritingPi { get; set; }
        public string ContractPi { get; set; }
        public string UnderwritingInterest { get; set; }
        public string ContractInterest { get; set; }
        public E_TotalScorecardRequestBorrMaritalStatus BorrMaritalStatus { get; set; }
        public E_TotalScorecardRequestYN FirstTimeBuyer { get; set; }
        public E_TotalScorecardLoanPurpose LoanPurpose { get; set; }
        public string PropAddress { get; set; }
        public string PropCity { get; set; }
        public string PropState { get; set; }
        public string PropZip { get; set; }
        public string PropCounty { get; set; }
        public E_TotalScorecardRequestYN SelfEmploy { get; set; }
        public E_TotalScorecardRequestYN Veteran { get; set; }
        public string BorrClsgCosts { get; set; }
        public E_TotalScorecardBorrType BorrType { get; set; }
        public string MonthlyExpense { get; set; }
        public E_TotalScorecardRequestYN Eem { get; set; }
        public string FirstPayDate { get; set; }
        public string GiftAmt { get; set; }
        public E_TotalScorecardGiftSource GiftSource { get; set; }
        public string Dependents { get; set; }
        public string ReqInvest { get; set; }
        public E_TotalScorecardRequestYN Solar { get; set; }
        public string TotalReq { get; set; }
        public string YearsAtJob { get; set; }
        public string YearsRenting { get; set; }
        public string ClsgCosts { get; set; }
        public string UnpaidBalance { get; set; }
        public string MortgageBasis { get; set; }
        public string AssetsAvail { get; set; }
        public string TotalFixed { get; set; }
        public E_TotalScorecardCounselType CounselType { get; set; }
        public string ClosingDate { get; set; }
        public string MaturityDate { get; set; }
        public string BuydownInterest { get; set; }
        public string SecondaryFinancingAmt { get; set; }
        public E_TotalScorecardSecondaryFinancingSrc SecondaryFinancingSrc { get; set; }
        public string EemEscrowAmt { get; set; }
        public string SellerConcessions { get; set; }
        public string LenderId { get; set; }
        public string SponsorId { get; set; }
        public string Version { get; set; }
        public string DownPayment { get; set; }
        public string BaseMortgageAmount { get; set; }
        public string BaseLtv { get; set; }
        public E_TotalScorecardRequestYN ManufacturedHousing { get; set; }
        public string LivingUnits { get; set; }
        public E_TotalScorecardArmType ArmType { get; set; }
        public E_TotalScorecardRefinanceType RefinanceType { get; set; }
        public string SponsoredOriginatorEIN { get; set; } // 11/15/2010 dd - OPM 54063
        public E_TotalScorecardRequestYN CondoInd { get; set; }

        private List<TotalScorecardRequestBorrower> m_borrowerList = new List<TotalScorecardRequestBorrower>();
        public void AddBorrower(TotalScorecardRequestBorrower borr)
        {
            m_borrowerList.Add(borr);
        }
        //4/18/2015 - IR - OPM 175978: Allow accessing of borrowers when converting to SOAP request.
        public IEnumerable<TotalScorecardRequestBorrower> RetrieveBorrowers()
        {
            return m_borrowerList;
        }

        private List<TotalScorecardRequestCreditReport> m_creditReportList = new List<TotalScorecardRequestCreditReport>();
        public void AddCreditReport(TotalScorecardRequestCreditReport creditReport)
        {
            m_creditReportList.Add(creditReport);
        }
        //4/18/2015 - IR - OPM 175978: Allow accessing of credit reports when converting to SOAP request.
        public IEnumerable<TotalScorecardRequestCreditReport> RetrieveCreditReports()
        {
            return m_creditReportList;
        }

        public override void ReadXml(XmlReader reader)
        {
            // 8/27/2009 dd - No need to convert XML to object.
            throw new NotImplementedException();
        }


        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("document");

            foreach (var o in m_creditReportList)
            {
                o.WriteXml(writer);
            }
            foreach (var o in m_borrowerList)
            {
                o.WriteXml(writer);
            }
            SafeElementWrite(writer, "monthly_income", MonthlyIncome);
            SafeElementWrite(writer, "appraised_value", AppraisedValue);
            SafeElementWrite(writer, "assets_after_clsg", AssetsAfterClsg);
            SafeElementWrite(writer, "fha_case_number", FhaCaseNumber);
            SafeElementWrite(writer, "loan_amount", LoanAmount);
            SafeElementWrite(writer, "piti", Piti);
            SafeElementWrite(writer, "mip", Mip);
            SafeElementWrite(writer, "term", Term);
            SafeElementWrite(writer, "sale_price", SalePrice);
            SafeElementWrite(writer, "applicants", Applicants);
            SafeElementWrite(writer, "loan_number", LoanNumber);
            SafeElementWrite(writer, "ltv", Ltv);
            SafeElementWrite(writer, "front_end_ratio", FrontEndRatio);
            SafeElementWrite(writer, "back_end_ratio", BackEndRatio);
            SafeElementWrite(writer, "amort_type", AmortType, EnumMapping.Map_TotalScorecardRequestAmortType);
            SafeElementWrite(writer, "underwriting_pi", UnderwritingPi);
            SafeElementWrite(writer, "contract_pi", ContractPi);
            SafeElementWrite(writer, "underwriting_interest", UnderwritingInterest);
            SafeElementWrite(writer, "contract_interest", ContractInterest);
            SafeElementWrite(writer, "borr_marital_status", BorrMaritalStatus, EnumMapping.Map_TotalScorecardRequestBorrMaritalStatus);
            SafeElementWrite(writer, "first_time_buyer", FirstTimeBuyer, EnumMapping.Map_TotalScorecardRequestYN);
            SafeElementWrite(writer, "loan_purpose", LoanPurpose, EnumMapping.Map_TotalScorecardLoanPurpose);
            SafeElementWrite(writer, "prop_address", PropAddress);
            SafeElementWrite(writer, "prop_city", PropCity);
            SafeElementWrite(writer, "prop_state", PropState);
            SafeElementWrite(writer, "prop_zip", PropZip);
            SafeElementWrite(writer, "prop_county", PropCounty);
            SafeElementWrite(writer, "self_employ", SelfEmploy, EnumMapping.Map_TotalScorecardRequestYN);
            SafeElementWrite(writer, "veteran", Veteran, EnumMapping.Map_TotalScorecardRequestYN);
            SafeElementWrite(writer, "borr_clsg_costs", BorrClsgCosts);
            SafeElementWrite(writer, "borr_type", BorrType, EnumMapping.Map_TotalScorecardBorrType);
            SafeElementWrite(writer, "monthly_expense", MonthlyExpense);
            SafeElementWrite(writer, "eem", Eem, EnumMapping.Map_TotalScorecardRequestYN);
            SafeElementWrite(writer, "first_pay_date", FirstPayDate);
            SafeElementWrite(writer, "gift_amt", GiftAmt);
            SafeElementWrite(writer, "gift_source", GiftSource, EnumMapping.Map_TotalScorecardGiftSource);
            SafeElementWrite(writer, "dependents", Dependents);
            SafeElementWrite(writer, "req_invest", ReqInvest);
            SafeElementWrite(writer, "solar", Solar, EnumMapping.Map_TotalScorecardRequestYN);
            SafeElementWrite(writer, "total_req", TotalReq);
            SafeElementWrite(writer, "years_at_job", YearsAtJob);
            SafeElementWrite(writer, "years_renting", YearsRenting);
            SafeElementWrite(writer, "clsg_costs", ClsgCosts);
            SafeElementWrite(writer, "unpaid_balance", UnpaidBalance);
            SafeElementWrite(writer, "mortgage_basis", MortgageBasis);
            SafeElementWrite(writer, "assets_avail", AssetsAvail);
            SafeElementWrite(writer, "total_fixed", TotalFixed);
            SafeElementWrite(writer, "counsel_type", CounselType, EnumMapping.Map_TotalScorecardCounselType);
            SafeElementWrite(writer, "closing_date", ClosingDate);
            SafeElementWrite(writer, "maturity_date", MaturityDate);
            SafeElementWrite(writer, "buydown_interest", BuydownInterest);
            SafeElementWrite(writer, "secondary_financing_amt", SecondaryFinancingAmt);
            SafeElementWrite(writer, "secondary_financing_src", SecondaryFinancingSrc, EnumMapping.Map_TotalScorecardSecondaryFinancingSrc);
            SafeElementWrite(writer, "eem_escrow_amt", EemEscrowAmt);
            SafeElementWrite(writer, "seller_concessions", SellerConcessions);
            SafeElementWrite(writer, "version", Version);
            SafeElementWrite(writer, "lender_id", LenderId);
            SafeElementWrite(writer, "sponsor_id", SponsorId);
            SafeElementWrite(writer, "down_payment", DownPayment);
            SafeElementWrite(writer, "base_mortgage_amount", BaseMortgageAmount);
            SafeElementWrite(writer, "base_LTV", BaseLtv);
            SafeElementWrite(writer, "manufactured_housing", ManufacturedHousing, EnumMapping.Map_TotalScorecardRequestYN);
            SafeElementWrite(writer, "living_units", LivingUnits);
            SafeElementWrite(writer, "ARM_type", ArmType, EnumMapping.Map_TotalScorecardArmType);
            SafeElementWrite(writer, "refinance_type", RefinanceType, EnumMapping.Map_TotalScorecardRefinanceType);
            if (string.IsNullOrEmpty(SponsoredOriginatorEIN) == false)
            {
                writer.WriteStartElement("LOAN_ORIGINATOR");
                writer.WriteAttributeString("_EmployerIdentificationNumber", SponsoredOriginatorEIN);
                writer.WriteEndElement();
            }
            SafeElementWrite(writer, "condo_ind", CondoInd, EnumMapping.Map_TotalScorecardRequestYN);
            writer.WriteEndElement(); // </document>
        }

    }
}
