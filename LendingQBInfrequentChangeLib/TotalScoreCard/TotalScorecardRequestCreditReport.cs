﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TotalScoreCard
{
    public class TotalScorecardRequestCreditReport : AbstractXmlSerializable
    {
        public E_TotalScorecardRequestCreditReportType CreditReportType { get; set; }
        public string CreditReportData { get; set; }


        public override void ReadXml(System.Xml.XmlReader reader)
        {
            throw new NotImplementedException();
        }

        public override void WriteXml(System.Xml.XmlWriter writer)
        {
            writer.WriteStartElement("creditreport");
            SafeElementWrite(writer, "creditreporttype", CreditReportType, EnumMapping.Map_TotalScorecardRequestCreditReportType);
            SafeElementWrite(writer, "creditreportdata", CreditReportData);
            writer.WriteEndElement(); // </creditreport>
        }
    }
}
