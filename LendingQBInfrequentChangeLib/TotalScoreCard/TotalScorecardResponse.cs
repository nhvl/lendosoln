﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace TotalScoreCard
{
    public class TotalScorecardResponse : AbstractXmlSerializable
    {
        public string NumReviews { get; set; }
        public string ErrCde { get; set; }
        public string ReviewRules { get; set; }
        public string PostReview { get; set; }
        public string PreReview { get; set; }
        public string Version { get; set; }
        public string LoanNumber { get; set; }
        public string MipBumpIndicator { get; set; }

        private List<TotalScorecardResponseBorrower> m_borrowerList = null;

        public List<TotalScorecardResponseBorrower> BorrowerList
        {
            get { return m_borrowerList; }
            set { m_borrowerList = value; }
        }

        private void Initialize()
        {
            NumReviews = string.Empty;
            ErrCde = string.Empty;
            ReviewRules = string.Empty;
            PostReview = string.Empty;
            PreReview = string.Empty;
            Version = string.Empty;
            LoanNumber = string.Empty;
            MipBumpIndicator = string.Empty;
            m_borrowerList = new List<TotalScorecardResponseBorrower>();
        }

        public override void ReadXml(XmlReader reader)
        {
            Initialize();
            while (reader.NodeType != XmlNodeType.Element || reader.Name.ToLower() != "document")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            if (reader.IsEmptyElement) return;

            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name.ToLower() == "document") return;
                if (reader.NodeType != XmlNodeType.Element) continue;

                switch (reader.Name.ToLower())
                {
                    case "numreviews":
                        NumReviews = reader.ReadElementString();
                        break;
                    case "errcde":
                        ErrCde = reader.ReadElementString();
                        break;
                    case "reviewrules":
                        ReviewRules = reader.ReadElementString();
                        break;
                    case "postreview":
                        PostReview = reader.ReadElementString();
                        break;
                    case "prereview":
                        PreReview = reader.ReadElementString();
                        break;
                    case "version":
                        Version = reader.ReadElementString();
                        break;
                    case "loan_number":
                        LoanNumber = reader.ReadElementString();
                        break;
                    case "mipbumpindicator":
                        MipBumpIndicator = reader.ReadElementString();
                        break;
                    case "borrowers":
                        ReadBorrowers(reader);
                        break;

                }
            }
        }

        private void ReadBorrowers(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name.ToLower() != "borrowers")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            if (reader.IsEmptyElement) return;

            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name.ToLower() == "borrowers") return;
                if (reader.NodeType != XmlNodeType.Element) continue;

                switch (reader.Name.ToLower())
                {
                    case "borrower":
                        TotalScorecardResponseBorrower borr = new TotalScorecardResponseBorrower();
                        borr.ReadXml(reader);
                        m_borrowerList.Add(borr);
                        break;
                }
            }
        }

        public override void WriteXml(XmlWriter writer)
        {
            throw new NotImplementedException();
        }
    }
}
