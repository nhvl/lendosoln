﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace TotalScoreCard
{
    public class TotalScorecardResponseBorrower : AbstractXmlSerializable
    {
        public string LFico { get; set; }
        public string Ssn { get; set; }
        public string Repository { get; set; }

        private void Initialize()
        {
            LFico = string.Empty;
            Ssn = string.Empty;
            Repository = string.Empty;
        }

        public override void ReadXml(System.Xml.XmlReader reader)
        {
            Initialize();
            while (reader.NodeType != XmlNodeType.Element || reader.Name.ToLower() != "borrower")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            if (reader.IsEmptyElement) return;

            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name.ToLower() == "borrower") return;
                if (reader.NodeType != XmlNodeType.Element) continue;

                switch (reader.Name.ToLower())
                {
                    case "lfico":
                        LFico = reader.ReadElementString();
                        break;
                    case "ssn":
                        Ssn = reader.ReadElementString();
                        break;
                    case "repository":
                        Repository = reader.ReadElementString();
                        break;
                }
            }
        }

        public override void WriteXml(System.Xml.XmlWriter writer)
        {
            throw new NotImplementedException();
        }
    }
}
