﻿using System;
using System.Globalization;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;

namespace FFIECGeocode
{
    public class Geocode
    {
        public string MsaCode;
        public string StateCode;
        public string CountyCode;
        public string TractCode;
        public bool IsError;
        public string ErrorMessage;

        /// <summary>
        /// Website is used to get geocode. IP Address: 132.200.48.131 -> 132.200.48.132
        /// </summary>
        private const string Url = "https://geomap.ffiec.gov/FFIECGeocMap/GeocodeMap1.aspx";

        public Geocode(string sHmdaActionD, string sSpAddr, string sSpCity, string sSpState,
                       string sSpZip)
        {
            var iCensusYear = sHmdaActionD.Trim();
            var sSingleLine = sSpAddr.Trim() + ",%20" + sSpCity.Trim() + ",%20" + sSpState.Trim() + "%20" + sSpZip.Trim();

            MsaCode = StateCode = CountyCode = TractCode = ErrorMessage = "";
            IsError = false;
            
            //GET data of the webpage (use Fiddler)
            var request1 = SetUpRequestMessage(Url);
            string response;
            using (var streamWriter = new StreamWriter(request1.GetRequestStream()))
            {
                streamWriter.Write("");
                streamWriter.Flush();
                streamWriter.Close();
                var response1 = (HttpWebResponse)request1.GetResponse();
                using (var sr1 = new StreamReader(response1.GetResponseStream()))
                {
                    response = sr1.ReadToEnd();
                }
            }
            var years = ExtractYear(response);
            if ((iCensusYear != null) && (iCensusYear.Trim() != ""))
            {
                iCensusYear = iCensusYear.Trim();
                //only get the year
                iCensusYear = iCensusYear.Substring(iCensusYear.Length - 4);
                if (int.Parse(iCensusYear) < years[years.Length - 1])
                {
                    iCensusYear = years[years.Length - 1].ToString(CultureInfo.InvariantCulture);
                }
                else
                {
                    if (int.Parse(iCensusYear) > years[0])
                    {
                        iCensusYear = years[0].ToString(CultureInfo.InvariantCulture);
                    }
                    else
                    {
                        foreach (var y in years)
                        {
                            if (int.Parse(iCensusYear) != y) continue;
                            break;
                        }
                    }
                }
            }
            else
            {
                iCensusYear = years[0].ToString(CultureInfo.InvariantCulture);
            }
            sSingleLine = sSingleLine.Trim();
            var postData = "{\"sSingleLine\":\"" + System.Web.HttpUtility.UrlDecode(sSingleLine) + "\"," +
                           "\"iCensusYear\":\"" + iCensusYear + "\"}";

            //POST data to server
            var request2 = SetUpRequestMessage(Url + "/GetGeocodeData");
            request2.Headers.Add("X-Requested-With", "XMLHttpRequest");
            request2.Accept = "application/json, text/javascript, */*; q=0.01";
            request2.ContentLength = postData.Length;
            request2.ServicePoint.Expect100Continue = false;

            string returnvalue;
            using (var streamWriter = new StreamWriter(request2.GetRequestStream()))
            {
                streamWriter.Write(postData);
                streamWriter.Flush();
                streamWriter.Close();

                var httpResponse = (HttpWebResponse)request2.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    returnvalue = streamReader.ReadToEnd();
                }
            }
            ExtractResult(returnvalue);
        }
        /// <summary>
        /// Generate Common Headers of a request message
        /// </summary>
        /// <returns>The HttpWebRequest variable with necessary information</returns>
        private static HttpWebRequest SetUpRequestMessage(string url)
        {
            //Default method is GET
            var request1 = (HttpWebRequest) WebRequest.Create(url);
            request1.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            request1.Referer = Url;
            request1.Method = "POST";
            request1.ContentType = "application/json; charset=utf-8";
            request1.Accept = "text/html, application/xhtml+xml, */*";
            request1.Headers.Add("Accept-Language", "en-US");
            request1.Headers.Add("DNT", "1");
            request1.Headers.Add("Pragma", "no-cache");
            request1.KeepAlive = true;
            //http://stackoverflow.com/questions/239725/cannot-set-some-http-headers-when-using-system-net-webrequest
            //Set Up UserAgent
            request1.UserAgent = "Mozilla/5.0";
            //Cookie is necessary when communcating with FFIEC

            request1.CookieContainer = new CookieContainer();
            //Add special cookie to request message.
            //http://www.statscrop.com/www/ffiec.gov
            request1.CookieContainer.Add(new Cookie("BIGipServerwww.ffiec.gov_http.app~www.ffiec.gov_http_pool",
                                                    "2638334084.20480.0000"
                                             ) { Domain = request1.RequestUri.Host });
            return request1;
        }
        private void ExtractResult(string s)
        {
            if (s.Contains("\"sStatus\":\"Y\""))
            {
                MsaCode = ExtractInnerText(s, "sMSACode");
                StateCode = ExtractInnerText(s, "sStateCode");
                CountyCode = ExtractInnerText(s, "sCountyCode");
                TractCode = ExtractInnerText(s, "sTractCode");
                IsError = false;
            }
            else
            {
                IsError = true;
                ErrorMessage = ExtractInnerText(s, "sMsg");
            }
        }

        private static string ExtractInnerText(string s, string id)
        {
            const string valueDelimiter = "\"";

            var namePosition = s.IndexOf(id, StringComparison.Ordinal);
            var valuePosition = s.IndexOf(valueDelimiter, namePosition, StringComparison.Ordinal);

            var startPosition = valuePosition +
                                valueDelimiter.Length + 2;
            var endPosition = s.IndexOf(valueDelimiter, startPosition, StringComparison.Ordinal);

            var regex = new Regex("(<.*?>\\s*)+", RegexOptions.Singleline);
            var resultText =
                regex.Replace(s.Substring(startPosition, endPosition - startPosition), " ").
                    Trim();

            return resultText;
        }
        //get the list of available year
        private static int[] ExtractYear(string s)
        {
            const string selectNameDelimiter = "yearSelect";
            const string valueDelimiter = "value=\"";
            const string endDelimiter = "</select>";

            var selectNamePosition = s.IndexOf(selectNameDelimiter, StringComparison.Ordinal);
            var endPosition = s.IndexOf(endDelimiter, selectNamePosition, StringComparison.Ordinal);
            var years = new int[0];
            var startPosition = selectNamePosition;
            while (true)
            {
                var optionValuePosition = s.IndexOf(valueDelimiter, startPosition, StringComparison.Ordinal);

                var optionStartPosition = optionValuePosition +
                                          valueDelimiter.Length;
                var optionEndPosition = s.IndexOf("\"", optionStartPosition, StringComparison.Ordinal);
                var year = HttpUtility.UrlEncode(
                    s.Substring(
                        optionStartPosition,
                        optionEndPosition - optionStartPosition
                        )
                    ).Trim();
                if (optionEndPosition > endPosition)
                {
                    break;
                }
                int num1;
                var res = int.TryParse(year, out num1);
                if (!res) break;

                startPosition = optionEndPosition;
                var temp = new int[years.Length + 1];
                years.CopyTo(temp, 0);
                temp[temp.Length - 1] = int.Parse(year);
                years = temp;
            }
            return years;
        }
    }
}
