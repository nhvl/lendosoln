﻿namespace LqbCommunication
{
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// A vendor communication request.
    /// </summary>
    [XmlRoot("LQBCommunication")]
    public partial class LqbCommunication {

        /// <summary>
        /// Transaction metadata associated with the request.
        /// </summary>
        [XmlElement("TransactionInfo", Order = 0)]
        public TransactionInfo TransactionInfo;

        /// <summary>
        /// The party sending the message.
        /// </summary>
        [XmlElement("SendingParty", Order = 1)]
        public string SendingParty;

        /// <summary>
        /// The party receiving the message.
        /// </summary>
        [XmlElement("ReceivingParty", Order = 2)]
        public string ReceivingParty;

        /// <summary>
        /// The subject of the message.
        /// </summary>
        [XmlElement("Subject", Order = 3)]
        public string Subject;

        /// <summary>
        /// The content of the message as a CDATA XML element.
        /// </summary>
        [XmlElement("Message", Order = 4)]
        public XmlNode Message;

        /// <summary>
        /// The postback URL for the vendor to respond to.
        /// </summary>
        [XmlElement("PostBackUrl", Order = 5)]
        public string PostBackUrl;

        /// <summary>
        /// The string data of the message. Will automatically convert
        /// input to CDATA.
        /// </summary>
        [XmlIgnore]
        public string Message_rep
        {
            get
            {
                return this.Message.Value;
            }

            set
            {
                var doc = new XmlDocument();
                this.Message = doc.CreateCDataSection(value);
            }
        }
    }
}