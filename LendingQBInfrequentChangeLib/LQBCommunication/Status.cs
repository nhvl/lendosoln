﻿namespace LqbCommunication
{
    using System.Xml.Serialization;

    /// <summary>
    /// Used to respond with the status of a request.
    /// </summary>
    public enum Status
    {

        /// <summary>
        /// The request was received and processed successfully.
        /// </summary>
        [XmlEnum("OK")]
        Ok,

        /// <summary>
        /// There was an error with the request.
        /// </summary>
        [XmlEnum("ERROR")]
        Error,
    }
}