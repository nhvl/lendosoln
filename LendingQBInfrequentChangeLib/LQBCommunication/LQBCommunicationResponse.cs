﻿namespace LqbCommunication
{
    using System.Xml.Serialization;

    /// <summary>
    /// A response to a vendor communication request.
    /// </summary>
    [XmlRoot("LQBCommunicationResponse")]
    public partial class LqbCommunicationResponse
    {
        /// <summary>
        /// The status of the received request.
        /// </summary>
        [XmlElement("Status", Order = 0)]
        public Status Status;

        /// <summary>
        /// An error message, if applicable.
        /// </summary>
        [XmlElement("Message", Order = 1)]
        public string Message;
    }
}