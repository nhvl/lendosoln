﻿namespace LqbCommunication
{
    using System.Xml.Serialization;

    /// <summary>
    /// Transaction metadata for a communication request.
    /// </summary>
    public partial class TransactionInfo
    {
        /// <summary>
        /// The integration framework associated with the request.
        /// </summary>
        [XmlAttribute("Framework")]
        public string Framework;

        /// <summary>
        /// The loan ID associated with the request.
        /// </summary>
        [XmlAttribute("LoanID")]
        public string LoanId;

        /// <summary>
        /// The order number associated with the request.
        /// </summary>
        [XmlAttribute("OrderNumber")]
        public string OrderNumber;
    }
}