﻿namespace OpenFinancialExchange
{
    /// <summary>
    /// Death Master verification status.
    /// </summary>
    public enum E_DMSTATUS
    {
        /// <summary>
        /// Death Master not ordered.
        /// </summary>
        NOTORDERED = 0,

        /// <summary>
        /// Death Master ordered, but not received.
        /// </summary>
        PENDING = 1,

        /// <summary>
        /// Death Master available.
        /// </summary>
        PRESENT = 2,

        /// <summary>
        /// Death Master ordered, but SSN was not found.
        /// </summary>
        NOTPRESENT = 3
    }
}
