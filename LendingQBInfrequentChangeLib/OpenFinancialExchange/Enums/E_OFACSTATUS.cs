﻿namespace OpenFinancialExchange
{
    /// <summary>
    /// Office of Foreign Assets Control verification status.
    /// </summary>
    public enum E_OFACSTATUS
    {
        /// <summary>
        /// OFAC not ordered.
        /// </summary>
        NOTORDERED = 0,

        /// <summary>
        /// OFAC ordered, not received.
        /// </summary>
        PENDING = 1,

        /// <summary>
        /// OFAC available.
        /// </summary>
        PRESENT = 2,

        /// <summary>
        /// OFAC ordered, SSN not found.
        /// </summary>
        NOTPRESENT = 3
    }
}
