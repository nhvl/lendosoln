﻿namespace OpenFinancialExchange
{
    /// <summary>
    /// Equifax verification order status.
    /// </summary>
    public enum E_STATUSCODE
    {
        /// <summary>
        /// A successful order.
        /// </summary>
        /// <remarks>A successful order will have a status of 0.</remarks>
        Success = 0,

        /// <summary>
        /// An unsuccessful order.
        /// </summary>
        /// <remarks>An unsuccessful order will have a non-zero integer status.</remarks>
        Error = 1
    }
}
