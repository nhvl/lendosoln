﻿namespace OpenFinancialExchange
{
    /// <summary>
    /// Credit header verification status.
    /// </summary>
    public enum E_CHSTATUS
    {
        /// <summary>
        /// CH not ordered.
        /// </summary>
        NOTORDERED = 0,

        /// <summary>
        /// CH ordered, not received.
        /// </summary>
        PENDING = 1,

        /// <summary>
        /// CH available.
        /// </summary>
        RETURNED = 2
    }
}
