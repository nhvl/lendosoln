﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenFinancialExchange
{
    public enum E_VERIFICATIONATTACHMENTTYPE
    {
        /// <summary>
        /// The attachment type was not initialized or could not be parsed.
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// Authentication for Social Security verification.
        /// </summary>
        SSAAUTHFORM,

        /// <summary>
        /// IRS form 1040 individual income tax return.
        /// </summary>
        IRS1040,

        /// <summary>
        /// IRS form 1099 other income return.
        /// </summary>
        IRS1099,

        /// <summary>
        /// IRS form W-2 wage and income transcript.
        /// </summary>
        IRSW2,

        /// <summary>
        /// IRS form 1065 partnership tax return.
        /// </summary>
        IRS1065,

        /// <summary>
        /// IRS form 1120 corporation tax return.
        /// </summary>
        IRS1120,

        /// <summary>
        /// All IRS income.
        /// </summary>
        IRS_ALL_INCOME,

        /// <summary>
        /// Form 1040 record of account transcript. (Includes tax return and tax account transcript)
        /// </summary>
        ROA1040,

        /// <summary>
        /// Form 1040 tax account transcript.
        /// </summary>
        RAT1040,

        /// <summary>
        /// Form 1120 record of account transcript. (Includes tax return and tax account transcript)
        /// </summary>
        ROA1120,

        /// <summary>
        /// Form 1120 tax account transcript.
        /// </summary>
        RAT1120,

        /// <summary>
        /// Form 1065 record of account transcript. (Includes tax return and tax account transcript)
        /// </summary>
        ROA1065,

        /// <summary>
        /// Form 1065 tax account transcript.
        /// </summary>
        RAT1065,

        /// <summary>
        /// IRS form 990 tax-exempt organization tax return.
        /// </summary>
        IRS990,

        /// <summary>
        /// IRS schedule K-1 individual in a partnership form.
        /// </summary>
        K1,

        /// <summary>
        /// IRS form 4506-T.
        /// </summary>
        IRS4506T,

        /// <summary>
        /// IRS form 1120S S-corporation tax return.
        /// </summary>
        IRS1120S,

        /// <summary>
        /// Form 1120S record of account transcript. (Includes tax return and tax account transcript)
        /// </summary>
        ROA1120S,

        /// <summary>
        /// Form 1120S tax account transcript.
        /// </summary>
        RAT1120S
    }
}
