﻿namespace OpenFinancialExchange
{
    /// <summary>
    /// A yes or no answer.
    /// </summary>
    public enum E_YESNO
    {
        /// <summary>
        /// An undefined answer.
        /// </summary>
        Undefined = 0,

        /// <summary>
        /// The answer "yes".
        /// </summary>
        YES = 1,

        /// <summary>
        /// The answer "no".
        /// </summary>
        NO = 2
    }
}
