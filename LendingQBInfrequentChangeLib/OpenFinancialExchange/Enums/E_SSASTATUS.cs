﻿namespace OpenFinancialExchange
{
    /// <summary>
    /// Social Security Administration verification status.
    /// </summary>
    public enum E_SSASTATUS
    {
        /// <summary>
        /// SSA not ordered.
        /// </summary>
        NOTORDERED = 0,

        /// <summary>
        /// SSA ordered, not received.
        /// </summary>
        PENDING = 1,

        /// <summary>
        /// SSA response complete. Last Name, First Name, Date of Birth, SSN match.
        /// </summary>
        MATCH = 2,

        /// <summary>
        /// SSA response complete. Last Name, First Name, Date of Birth, SSN do not match.
        /// </summary>
        NOMATCH = 3,
    }
}
