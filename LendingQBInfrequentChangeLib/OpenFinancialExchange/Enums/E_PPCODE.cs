﻿namespace OpenFinancialExchange
{
    /// <summary>
    /// Codes indicating the Permissible Purpose for ordering verification.
    /// </summary>
    public enum E_PPCODE
    {
        /// <summary>
        /// Unknown purpose type which will not be serialized.
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// Employee's application for credit.
        /// </summary>
        PPCREDIT = 14,

        /// <summary>
        /// Employment purposes.
        /// </summary>
        PPEMPLOY = 1,

        /// <summary>
        /// Employee's eligibility for a benefit granted by a governmental agency; we are required
        /// by law to consider the employee's financial responsibility or status.
        /// </summary>
        PPGOVRNM = 2,

        /// <summary>
        /// Determine child support payments - I represent a state or local child support enforcement agency.
        /// </summary>
        PPCHLDSP = 3,

        /// <summary>
        /// Performing a review or collection of the employee's account.
        /// </summary>
        PPREVIEW = 4,

        /// <summary>
        /// Employee has issued me written instruction to obtain this information.
        /// </summary>
        PPWRTTEN = 5,

        /// <summary>
        /// Under a court order or a Federal grand jury subpoena.
        /// </summary>
        PPCRTORD = 6,

        /// <summary>
        /// Underwriting insurance in response to the employee's application.
        /// </summary>
        PPINSRNC = 7,

        /// <summary>
        /// As a potential investor or servicer to assess prepayment risks of issuing credit to the
        /// employee, or as a current insurer to assess the employee's existing credit.
        /// </summary>
        PPASSESS = 8,

        /// <summary>
        /// I have a legitimate business need for the information in connection with a business
        /// transaction initiated by the employee, or to review the employee's account to
        /// determine whether the employee continues to meet the terms of the account.
        /// </summary>
        PPBUSNSS = 9,

        /// <summary>
        /// I am with a governmental agency which needs the information for counterterrorism purposes.
        /// </summary>
        PPSECRTY = 10,

        /// <summary>
        /// Employment purposes.
        /// </summary>
        PPEMPLOYA = 11,

        /// <summary>
        /// Determine child support payments - I represent a state or local child support enforcement agency.
        /// </summary>
        PPCHLDSPA = 12,

        /// <summary>
        /// Determine child support payments - I represent a state or local child support enforcement agency.
        /// </summary>
        PPCHLDSPB = 13,
    }
}
