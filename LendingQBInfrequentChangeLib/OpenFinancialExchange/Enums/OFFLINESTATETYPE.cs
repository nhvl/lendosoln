﻿namespace OpenFinancialExchange
{
    /// <summary>
    /// Equifax uses thes states to communicate the status of a 4506-T order.
    /// </summary>
    public enum OFFLINESTATETYPE
    {
        /// <summary>
        /// The enum value is uninitialized or could not be parsed from a string.
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// The order is still active and waiting for either:
        /// - E-signature completion (not part of the initial integration)
        /// - IRS fulfillment
        /// </summary>
        PENDING = 1,

        /// <summary>
        /// The order was cancelled by the user or Equifax Verification Services' quality control check.
        /// This will be accompanied by a message detailing the cancellation reasons.
        /// </summary>
        CANCELLED = 2,

        /// <summary>
        /// The order was rejected by the IRS.
        /// This will be accompanied by a message detailing the rejection reasons.
        /// </summary>
        REJECTED = 3,

        /// <summary>
        /// The order completed successfully.
        /// </summary>
        COMPLETE = 4
    }
}
