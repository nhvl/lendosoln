﻿namespace OpenFinancialExchange
{
    /// <summary>
    /// Specifies the type of TRV orders with stated income or stated docs.
    /// </summary>
    public enum E_STATEDDOCOPTION
    {
        /// <summary>
        /// The order is a default order and doesn't have stated options.
        /// </summary>
        NONSTATEDDOC,

        /// <summary>
        /// Denotes a stated doc order.
        /// </summary>
        STATEDDOC,

        /// <summary>
        /// Denotes a stated income order.
        /// </summary>
        STATEDINCOME,

        /// <summary>
        /// Denotes a stated wage order.
        /// </summary>
        STATEDWAGE
    }
}
