﻿namespace OpenFinancialExchange
{
    using System;
    using System.Xml;

    public class IDENTITYCHECKSTATE : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return nameof(IDENTITYCHECKSTATE); }
        }

        private DMSTATUS dmstatus;

        public DMSTATUS DMSTATUS
        {
            get
            {
                if (this.dmstatus == null)
                {
                    this.dmstatus = new DMSTATUS();
                }

                return this.dmstatus;
            }

            set
            {
                this.dmstatus = value;
            }
        }

        private SSASTATUS ssastatus;

        public SSASTATUS SSASTATUS
        {
            get
            {
                if (this.ssastatus == null)
                {
                    this.ssastatus = new SSASTATUS();
                }

                return this.ssastatus;
            }

            set
            {
                this.ssastatus = value;
            }
        }

        private OFACSTATUS ofacstatus;

        public OFACSTATUS OFACSTATUS
        {
            get
            {
                if (this.ofacstatus == null)
                {
                    this.ofacstatus = new OFACSTATUS();
                }

                return this.ofacstatus;
            }

            set
            {
                this.ofacstatus = value;
            }
        }

        private CHSTATUS chstatus;

        public CHSTATUS CHSTATUS
        {
            get
            {
                if (this.chstatus == null)
                {
                    this.chstatus = new CHSTATUS();
                }

                return this.chstatus;
            }

            set
            {
                this.chstatus = value;
            }
        }

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case nameof(this.DMSTATUS):
                    ReadElement(reader, this.DMSTATUS);
                    break;
                case nameof(this.SSASTATUS):
                    ReadElement(reader, this.SSASTATUS);
                    break;
                case nameof(this.OFACSTATUS):
                    ReadElement(reader, this.OFACSTATUS);
                    break;
                case nameof(this.CHSTATUS):
                    ReadElement(reader, this.CHSTATUS);
                    break;
                default:
                    throw new ArgumentException($"Element {reader.Name} is not handled.");
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElement(writer, this.dmstatus);
            WriteElement(writer, this.ssastatus);
            WriteElement(writer, this.ofacstatus);
            WriteElement(writer, this.chstatus);
        }
    }
}
