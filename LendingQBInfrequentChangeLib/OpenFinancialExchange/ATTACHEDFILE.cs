﻿namespace OpenFinancialExchange
{
    using System;
    using System.Xml;

    public class ATTACHEDFILE : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return nameof(ATTACHEDFILE); }
        }

        public string ATTACHMENTNAME { get; set; }

        public string ATTACHMENTENCODING { get; set; }

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case nameof(this.ATTACHMENTNAME):
                    this.ATTACHMENTNAME = ReadElementString(reader);
                    break;
                case nameof(this.ATTACHMENTENCODING):
                    this.ATTACHMENTENCODING = ReadElementString(reader);
                    break;
                default:
                    throw new ArgumentException($"Element {reader.Name} is not handled.");
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElementString(writer, nameof(this.ATTACHMENTENCODING), this.ATTACHMENTENCODING);
            WriteElementString(writer, nameof(this.ATTACHMENTNAME), this.ATTACHMENTNAME);
        }
    }
}
