﻿namespace OpenFinancialExchange
{
    using System;
    using System.Xml;

    public class TRNPURPOSE : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return nameof(TRNPURPOSE); }
        }

        public E_PPCODE CODE { get; set; }

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case nameof(this.CODE):
                    this.CODE = (E_PPCODE)Enum.Parse(typeof(E_PPCODE), ReadElementString(reader));
                    break;
                default:
                    throw new ArgumentException($"Element {reader.Name} is not handled.");
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElementEnum(writer, nameof(this.CODE), this.CODE);
        }
    }
}
