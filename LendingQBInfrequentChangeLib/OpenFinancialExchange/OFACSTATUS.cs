﻿namespace OpenFinancialExchange
{
    using System;
    using System.Xml;

    public class OFACSTATUS : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return nameof(OFACSTATUS); }
        }

        public E_OFACSTATUS OFFLINESTATETYPE;

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case nameof(this.OFFLINESTATETYPE):
                    this.OFFLINESTATETYPE = (E_OFACSTATUS)Enum.Parse(typeof(E_OFACSTATUS), ReadElementString(reader), true);
                    break;
                default:
                    throw new ArgumentException($"Element {reader.Name} is not handled.");
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            this.WriteElementEnum(writer, nameof(this.OFFLINESTATETYPE), this.OFFLINESTATETYPE);
        }
    }
}