﻿namespace OpenFinancialExchange
{
    using System;
    using System.Xml;

    public class CHSTATUS : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return nameof(CHSTATUS); }
        }

        public E_CHSTATUS OFFLINESTATETYPE;

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case nameof(this.OFFLINESTATETYPE):
                    this.OFFLINESTATETYPE = (E_CHSTATUS)Enum.Parse(typeof(E_CHSTATUS), ReadElementString(reader), true);
                    break;
                default:
                    throw new ArgumentException($"Element {reader.Name} is not handled.");
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            this.WriteElementEnum(writer, nameof(this.OFFLINESTATETYPE), this.OFFLINESTATETYPE);
        }
    }
}
