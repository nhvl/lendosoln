﻿namespace OpenFinancialExchange
{
    using System;
    using System.Xml;

    public class IDENTITYREPORT : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return nameof(IDENTITYREPORT); }
        }

        private ATTACHEDFILES attachedfiles;

        public ATTACHEDFILES ATTACHEDFILES
        {
            get
            {
                if (this.attachedfiles == null)
                {
                    this.attachedfiles = new ATTACHEDFILES();
                }

                return this.attachedfiles;
            }

            set
            {
                this.attachedfiles = value;
            }
        }

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case nameof(this.ATTACHEDFILES):
                    ReadElement(reader, this.ATTACHEDFILES);
                    break;
                default:
                    throw new ArgumentException($"Element {reader.Name} is unhandled.");
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElement(writer, this.ATTACHEDFILES);
        }
    }
}
