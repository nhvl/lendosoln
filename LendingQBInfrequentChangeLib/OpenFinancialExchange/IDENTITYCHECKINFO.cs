﻿namespace OpenFinancialExchange
{
    using System;
    using System.Xml;

    public class IDENTITYCHECKINFO : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return nameof(IDENTITYCHECKINFO); }
        }

        private IDENTITYCHECKSTATE identitycheckstate;

        public IDENTITYCHECKSTATE IDENTITYCHECKSTATE
        {
            get
            {
                if (this.identitycheckstate == null)
                {
                    this.identitycheckstate = new IDENTITYCHECKSTATE();
                }

                return this.identitycheckstate;
            }

            set
            {
                this.identitycheckstate = value;
            }
        }

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case nameof(this.IDENTITYCHECKSTATE):
                    ReadElement(reader, this.IDENTITYCHECKSTATE);
                    break;
                default:
                    throw new ArgumentException($"Element {reader.Name} is not handled.");
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElement(writer, this.IDENTITYCHECKSTATE);
        }
    }
}
