﻿namespace OpenFinancialExchange
{
    using System;
    using System.Xml;

    public class VERIFICATIONATTACHMENTTYPE : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return nameof(VERIFICATIONATTACHMENTTYPE); }
        }

        public E_VERIFICATIONATTACHMENTTYPE ATTACHMENTTYPE { get; set; }

        protected override void ReadElement(XmlReader reader)
        {
            if (!string.IsNullOrEmpty(reader.Name))
            {
                this.ATTACHMENTTYPE = (E_VERIFICATIONATTACHMENTTYPE)Enum.Parse(typeof(E_VERIFICATIONATTACHMENTTYPE), reader.Name);
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteRequiredElement(writer, this.ATTACHMENTTYPE.ToString("g"), string.Empty);
        }
    }
}
