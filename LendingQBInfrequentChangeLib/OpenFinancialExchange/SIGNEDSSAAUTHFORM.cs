﻿namespace OpenFinancialExchange
{
    using System;
    using System.Xml;

    public class SIGNEDSSAAUTHFORM : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return nameof(SIGNEDSSAAUTHFORM); }
        }

        private VERIFICATIONATTACHMENT verificationattachment;

        public VERIFICATIONATTACHMENT VERIFICATIONATTACHMENT
        {
            get
            {
                if (this.verificationattachment == null)
                {
                    this.verificationattachment = new VERIFICATIONATTACHMENT();
                }

                return this.verificationattachment;
            }

            set
            {
                this.verificationattachment = value;
            }
        }

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case nameof(this.VERIFICATIONATTACHMENT):
                    ReadElement(reader, this.VERIFICATIONATTACHMENT);
                    break;
                default:
                    throw new ArgumentException($"Element {reader.Name} is not handled.");
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElement(writer, this.VERIFICATIONATTACHMENT);
        }
    }
}
