﻿namespace OpenFinancialExchange
{
    using System;
    using System.Xml;

    public class DMSTATUS : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return nameof(DMSTATUS); }
        }

        public E_DMSTATUS OFFLINESTATETYPE;

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case nameof(this.OFFLINESTATETYPE):
                    this.OFFLINESTATETYPE = (E_DMSTATUS)Enum.Parse(typeof(E_DMSTATUS), ReadElementString(reader), true);
                    break;
                default:
                    throw new ArgumentException($"Element {reader.Name} is not handled.");
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            this.WriteElementEnum(writer, nameof(this.OFFLINESTATETYPE), this.OFFLINESTATETYPE);
        }
    }
}