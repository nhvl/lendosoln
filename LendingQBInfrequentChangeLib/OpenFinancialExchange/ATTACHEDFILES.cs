﻿namespace OpenFinancialExchange
{
    using System;
    using System.Collections.Generic;
    using System.Xml;

    public class ATTACHEDFILES : AbstractXmlSerializable
    {
        public ATTACHEDFILES(List<ATTACHEDFILE> attachedFileList)
        {
            this.attachedfileList = attachedFileList;
        }

        public ATTACHEDFILES()
        {
        }

        public override string XmlElementName
        {
            get { return nameof(ATTACHEDFILES); }
        }

        private List<ATTACHEDFILE> attachedfileList;

        public List<ATTACHEDFILE> ATTACHEDFILEList
        {
            get
            {
                if (this.attachedfileList == null)
                {
                    this.attachedfileList = new List<ATTACHEDFILE>();
                }

                return this.attachedfileList;
            }
        }

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case nameof(ATTACHEDFILE):
                    ReadElement(reader, this.ATTACHEDFILEList);
                    break;
                default:
                    throw new ArgumentException($"Element {reader.Name} is not handled.");
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElement(writer, this.attachedfileList);
        }
    }
}
