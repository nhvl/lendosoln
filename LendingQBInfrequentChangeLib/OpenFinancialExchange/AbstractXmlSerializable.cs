﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;

namespace OpenFinancialExchange
{

    public abstract class AbstractXmlSerializable : IXmlSerializable
    {
        protected virtual string Prefix { get { return string.Empty; } }

        protected virtual string Namespace { get { return string.Empty; } }

        public abstract string XmlElementName { get; }

        protected string ReadElementString(XmlReader reader)
        {
            string str = reader.ReadString();
            if (str == null)
            {
                str = string.Empty;
            }
            return str;
        }

        protected TEnum? ReadElementEnum<TEnum>(XmlReader reader) where TEnum : struct
        {
            TEnum returnValue;
            string value = reader.ReadString();
            if (Enum.TryParse(value, out returnValue))
            {
                return returnValue;
            }
            else
            {
                return null;
            }
        }

        protected E_YESNO ReadElementYesNo(XmlReader reader)
        {
            string value = reader.ReadString();

            if (string.IsNullOrEmpty(value))
            {
                return E_YESNO.Undefined;
            }

            return (E_YESNO)Enum.Parse(typeof(E_YESNO), value, true);
        }

        protected E_YESNO ReadElementYN(XmlReader reader)
        {
            string value = reader.ReadString();

            if (value.Trim().Equals("Y", StringComparison.OrdinalIgnoreCase))
            {
                return E_YESNO.YES;
            }
            else if (value.Trim().Equals("N", StringComparison.OrdinalIgnoreCase))
            {
                return E_YESNO.NO;
            }
            return E_YESNO.Undefined;
        }

        protected string ReadAttribute(XmlReader reader, string attrName)
        {
            string value = reader.GetAttribute(attrName);
            if (string.IsNullOrEmpty(value))
            {
                return string.Empty;
            }
            return value;
        }

        protected void WriteAttribute(XmlWriter writer, string attrName, string value)
        {
            if (string.IsNullOrEmpty(value) == false)
            {
                writer.WriteAttributeString(attrName, value);
            }
        }

        protected void ReadAttribute(XmlReader reader, string attrName, out string result)
        {
            result = ReadAttribute(reader, attrName);
        }

        protected void WriteElementString(XmlWriter writer, string name, string value)
        {
            if (string.IsNullOrEmpty(value) == false)
            {
                writer.WriteElementString(name, value);
            }
        }

        protected void WriteString(XmlWriter writer, string value)
        {
            if (string.IsNullOrEmpty(value) == false)
            {
                writer.WriteString(value);
            }
        }
        protected void WriteElement(XmlWriter writer, IXmlSerializable el)
        {
            if (null != el)
            {
                el.WriteXml(writer);
            }
        }

        protected void WriteRequiredElement(XmlWriter writer, string elName, string value)
        {
            writer.WriteElementString(elName, value);
        }

        protected void WriteElementEnum(XmlWriter writer, string elName, Enum value)
        {
            if (null != value)
            {
                writer.WriteElementString(elName, value.ToString());
            }
        }

        protected void WriteElement<T>(XmlWriter writer, List<T> list) where T : IXmlSerializable
        {
            if (null == list)
                return;

            foreach (IXmlSerializable o in list)
            {
                WriteElement(writer, o);
            }
        }

        protected void ReadElement(XmlReader reader, IXmlSerializable el)
        {
            if (null == el)
            {
                return;
            }
            el.ReadXml(reader);
        }
        protected void ReadElement<T>(XmlReader reader, List<T> list) where T : IXmlSerializable, new()
        {
            if (null == list)
                return;

            T _o = new T();
            _o.ReadXml(reader);
            list.Add(_o);
        }

        protected virtual void ReadAttributes(XmlReader reader)
        {
        }

        protected virtual void ReadElement(XmlReader reader)
        {
        }
        protected virtual void ReadTextContent(XmlReader reader)
        {
        }
        protected virtual void WriteXmlImpl(XmlWriter writer)
        {
        }
        #region IXmlSerializable Members

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        public void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.LocalName != XmlElementName)
            {
                reader.Read();
                if (reader.EOF)
                {
                    return;
                }
            }
            ReadAttributes(reader);

            if (reader.IsEmptyElement) return;

            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.Text)
                {
                    ReadTextContent(reader);
                    continue;
                }
                if (reader.NodeType == XmlNodeType.EndElement && reader.LocalName == XmlElementName)
                {
                    return;
                }
                if (reader.NodeType != XmlNodeType.Element)
                {
                    continue;
                }

                ReadElement(reader);
            }
        }

        public virtual void WriteXml(System.Xml.XmlWriter writer)
        {
            if (string.IsNullOrEmpty(Prefix) == false && string.IsNullOrEmpty(Namespace) == false)
            {
                writer.WriteStartElement(Prefix, XmlElementName, Namespace);
            }
            else if (string.IsNullOrEmpty(Namespace) == false)
            {
                writer.WriteStartElement(XmlElementName, Namespace);
            }
            else
            {
                writer.WriteStartElement(XmlElementName);
            }

            WriteXmlImpl(writer);
            writer.WriteEndElement();
        }

        /// <summary>
        /// Writes the <see cref="AbstractXmlSerializable"/> instance to a string and returns the string.
        /// </summary>
        /// <returns>The whole xml structure of the XML-serializable object as an XML string.</returns>
        public string ToString(bool includeXmlDeclaration = true)
        {
            var xmlSettings = new XmlWriterSettings
            {
                OmitXmlDeclaration = !includeXmlDeclaration,
                Encoding = new UTF8Encoding(false)
            };

            using (MemoryStream stream = new MemoryStream())
            using (XmlWriter xmlWriter = XmlWriter.Create(stream, xmlSettings))
            {
                this.WriteXml(xmlWriter);
                xmlWriter.Flush();
                return Encoding.UTF8.GetString(stream.ToArray());
            }
        }

        /// <summary>
        /// Explicitly overrides the default ToString() implementation to avoid unexpected results.
        /// </summary>
        /// <returns>The object as a string.</returns>
        public override string ToString()
        {
            return this.ToString(true);
        }

        #endregion
    }
}
