﻿namespace OpenFinancialExchange.Request
{
    using System;
    using System.Xml;

    public class EIVTAXTRANSCRIPTTRNRQ : AbstractTransactionRequest
    {
        public override string XmlElementName
        {
            get { return nameof(EIVTAXTRANSCRIPTTRNRQ); }
        }

        public EIVTAXTRANSCRIPTTRNRQ() : base()
        {
            this.useTrnPurpose = true;
        }

        public string PLATFORM { get; set; }

        public string INTERMEDIARY { get; set; }

        public string ENDUSER { get; set; }

        private EIVTAXTRANSCRIPTRQ eivtaxtranscriptrq;

        public EIVTAXTRANSCRIPTRQ EIVTAXTRANSCRIPTRQ
        {
            get
            {
                if (this.eivtaxtranscriptrq == null)
                {
                    this.eivtaxtranscriptrq = new EIVTAXTRANSCRIPTRQ();
                }

                return this.eivtaxtranscriptrq;
            }

            set
            {
                this.eivtaxtranscriptrq = value;
            }
        }

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case nameof(this.PLATFORM):
                    this.PLATFORM = ReadElementString(reader);
                    return;
                case nameof(this.INTERMEDIARY):
                    this.INTERMEDIARY = ReadElementString(reader);
                    return;
                case nameof(this.ENDUSER):
                    this.ENDUSER = ReadElementString(reader);
                    return;
                case nameof(this.EIVTAXTRANSCRIPTRQ):
                    ReadElement(reader, this.EIVTAXTRANSCRIPTRQ);
                    return;
            }
            base.ReadElement(reader);
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElementString(writer, nameof(this.TRNUID), this.TRNUID);
            WriteElementString(writer, nameof(this.CLTCOOKIE), this.CLTCOOKIE);
            WriteElementString(writer, nameof(this.PLATFORM), this.PLATFORM);
            WriteElementString(writer, nameof(this.INTERMEDIARY), this.INTERMEDIARY);
            WriteElementString(writer, nameof(this.ENDUSER), this.ENDUSER);
            WriteElement(writer, this.TRNPURPOSE);
            WriteElement(writer, this.EIVTAXTRANSCRIPTRQ);
        }
    }
}
