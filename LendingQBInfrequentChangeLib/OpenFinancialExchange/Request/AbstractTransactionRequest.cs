﻿using System;
using System.Xml;

namespace OpenFinancialExchange.Request
{
    /// <summary>
    /// An abstract class extracting out some of the most commonly repeated elements of OFX transaction requests.
    /// </summary>
    public abstract class AbstractTransactionRequest : AbstractXmlSerializable
    {
        /// <summary>
        /// Mandatory transaction ID. Use sLenderCaseNum in most cases.
        /// </summary>
        public string TRNUID { get; set; }

        /// <summary>
        /// Optional transaction cookie that will be echoed back in responses.
        /// We can use sLId or sLRefNum here to uniquely identify a loan in our system.
        /// </summary>
        public string CLTCOOKIE { get; set; }

        /// <summary>
        /// Turns on or off "accepting" the TRNPURPOSE element when parsing, and serializing it on output. 
        /// For schema integrity since that element isn't accepted in all transaction requests.
        /// </summary>
        public bool useTrnPurpose = false;

        private TRNPURPOSE trnpurpose;

        /// <summary>
        /// Transaction purpose.
        /// </summary>
        public TRNPURPOSE TRNPURPOSE
        {
            get
            {
                if (this.trnpurpose == null)
                {
                    this.trnpurpose = new TRNPURPOSE();
                }

                return this.trnpurpose;
            }

            set
            {
                this.trnpurpose = value;
            }
        }

        protected override void ReadElement(XmlReader reader)
        {
            if (reader.Name == nameof(this.TRNUID))
            {
                this.TRNUID = ReadElementString(reader);
                return;
            }

            if (reader.Name == nameof(this.CLTCOOKIE))
            {
                this.CLTCOOKIE = ReadElementString(reader);
                return;
            }

            if (useTrnPurpose && reader.Name == nameof(this.TRNPURPOSE))
            {
                this.CLTCOOKIE = ReadElementString(reader);
                return;
            }

            throw new ArgumentException($"Element {reader.Name} is not handled.");
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElementString(writer, nameof(this.TRNUID), this.TRNUID);
            WriteElementString(writer, nameof(this.CLTCOOKIE), this.CLTCOOKIE);
            if (this.useTrnPurpose)
            {
                WriteElement(writer, this.trnpurpose);
            }
        }
    }
}
