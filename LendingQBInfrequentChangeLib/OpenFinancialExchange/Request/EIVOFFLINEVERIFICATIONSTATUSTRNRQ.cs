﻿namespace OpenFinancialExchange.Request
{
    using System;
    using System.Xml;

    public class EIVOFFLINEVERIFICATIONSTATUSTRNRQ : AbstractTransactionRequest
    {
        public override string XmlElementName
        {
            get { return nameof(EIVOFFLINEVERIFICATIONSTATUSTRNRQ); }
        }

        private EIVOFFLINEVERIFICATIONSTATUSRQ eivofflineverificationstatusrq;

        public EIVOFFLINEVERIFICATIONSTATUSRQ EIVOFFLINEVERIFICATIONSTATUSRQ
        {
            get
            {
                if (this.eivofflineverificationstatusrq == null)
                {
                    this.eivofflineverificationstatusrq = new EIVOFFLINEVERIFICATIONSTATUSRQ();
                }

                return this.eivofflineverificationstatusrq;
            }

            set
            {
                this.eivofflineverificationstatusrq = value;
            }
        }

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case nameof(this.EIVOFFLINEVERIFICATIONSTATUSRQ):
                    ReadElement(reader, this.EIVOFFLINEVERIFICATIONSTATUSRQ);
                    return;
            }
            base.ReadElement(reader);
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            base.WriteXmlImpl(writer);
            WriteElement(writer, this.EIVOFFLINEVERIFICATIONSTATUSRQ);
        }
    }
}
