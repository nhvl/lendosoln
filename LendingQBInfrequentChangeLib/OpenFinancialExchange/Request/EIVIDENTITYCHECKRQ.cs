﻿namespace OpenFinancialExchange.Request
{
    using System;
    using System.Xml;

    public class EIVIDENTITYCHECKRQ : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return nameof(EIVIDENTITYCHECKRQ); }
        }

        private SIGNEDSSAAUTHFORM signedssaauthform;

        public SIGNEDSSAAUTHFORM SIGNEDSSAAUTHFORM
        {
            get
            {
                if (this.signedssaauthform == null)
                {
                    this.signedssaauthform = new SIGNEDSSAAUTHFORM();
                }

                return this.signedssaauthform;
            }

            set
            {
                this.signedssaauthform = value;
            }
        }

        public string FIRSTNAME { get; set; }

        public string LASTNAME { get; set; }

        public string SSN { get; set; }

        public string DATEBIRTH { get; set; }

        public string IMMUTABLEID { get; set; }

        public string LOANID { get; set; }

        public string ADDR1 { get; set; }

        public string CITY { get; set; }

        public string STATE { get; set; }

        public string POSTALCODE { get; set; }

        public string PHONENUMBER { get; set; }

        public string CREDITHEADER { get; set; }

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case nameof(this.SIGNEDSSAAUTHFORM):
                    ReadElement(reader, this.SIGNEDSSAAUTHFORM);
                    break;
                case nameof(this.FIRSTNAME):
                    this.FIRSTNAME = ReadElementString(reader);
                    break;
                case nameof(this.LASTNAME):
                    this.LASTNAME = ReadElementString(reader);
                    break;
                case nameof(this.SSN):
                    this.SSN = ReadElementString(reader);
                    break;
                case nameof(this.DATEBIRTH):
                    this.DATEBIRTH = ReadElementString(reader);
                    break;
                case nameof(this.IMMUTABLEID):
                    this.IMMUTABLEID = ReadElementString(reader);
                    break;
                case nameof(this.LOANID):
                    this.LOANID = ReadElementString(reader);
                    break;
                case nameof(this.ADDR1):
                    this.ADDR1 = ReadElementString(reader);
                    break;
                case nameof(this.CITY):
                    this.CITY = ReadElementString(reader);
                    break;
                case nameof(this.STATE):
                    this.STATE = ReadElementString(reader);
                    break;
                case nameof(this.POSTALCODE):
                    this.POSTALCODE = ReadElementString(reader);
                    break;
                case nameof(this.PHONENUMBER):
                    this.PHONENUMBER = ReadElementString(reader);
                    break;
                case nameof(this.CREDITHEADER):
                    this.CREDITHEADER = ReadElementString(reader);
                    break;
                default:
                    throw new ArgumentException($"Element {reader.Name} is not handled.");
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElementString(writer, nameof(this.FIRSTNAME), this.FIRSTNAME);
            WriteElementString(writer, nameof(this.LASTNAME), this.LASTNAME);
            WriteElementString(writer, nameof(this.SSN), this.SSN);
            WriteElementString(writer, nameof(this.DATEBIRTH), this.DATEBIRTH);
            WriteElementString(writer, nameof(this.IMMUTABLEID), this.IMMUTABLEID);
            WriteElementString(writer, nameof(this.LOANID), this.LOANID);
            WriteElementString(writer, nameof(this.ADDR1), this.ADDR1);
            WriteElementString(writer, nameof(this.CITY), this.CITY);
            WriteElementString(writer, nameof(this.STATE), this.STATE);
            WriteElementString(writer, nameof(this.POSTALCODE), this.POSTALCODE);
            WriteElementString(writer, nameof(this.PHONENUMBER), this.PHONENUMBER);
            WriteRequiredElement(writer, nameof(this.CREDITHEADER), this.CREDITHEADER);
            WriteElement(writer, this.SIGNEDSSAAUTHFORM);
        }
    }
}
