﻿namespace OpenFinancialExchange.Request
{
    using System;
    using System.Xml;

    public class EIVIDENTITYCHECKRETRIEVERESULTTRNRQ : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return nameof(EIVIDENTITYCHECKRETRIEVERESULTTRNRQ); }
        }

        private TRNPURPOSE trnpurpose;

        public TRNPURPOSE TRNPURPOSE
        {
            get
            {
                if (this.trnpurpose == null)
                {
                    this.trnpurpose = new TRNPURPOSE();
                }

                return this.trnpurpose;
            }

            set
            {
                this.trnpurpose = value;
            }
        }

        private EIVIDENTITYCHECKRETRIEVERESULTRQ eividentitycheckretrieveresultrq;

        public EIVIDENTITYCHECKRETRIEVERESULTRQ EIVIDENTITYCHECKRETRIEVERESULTRQ
        {
            get
            {
                if (this.eividentitycheckretrieveresultrq == null)
                {
                    this.eividentitycheckretrieveresultrq = new EIVIDENTITYCHECKRETRIEVERESULTRQ();
                }

                return this.eividentitycheckretrieveresultrq;
            }

            set
            {
                this.eividentitycheckretrieveresultrq = value;
            }
        }

        public string TRNUID { get; set; }

        public string CLTCOOKIE { get; set; }

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case nameof(this.TRNPURPOSE):
                    ReadElement(reader, this.TRNPURPOSE);
                    break;
                case nameof(this.EIVIDENTITYCHECKRETRIEVERESULTRQ):
                    ReadElement(reader, this.EIVIDENTITYCHECKRETRIEVERESULTRQ);
                    break;
                case nameof(this.TRNUID):
                    this.TRNUID = ReadElementString(reader);
                    break;
                case nameof(this.CLTCOOKIE):
                    this.CLTCOOKIE = ReadElementString(reader);
                    break;
                default:
                    throw new ArgumentException($"Element {reader.Name} is not handled.");
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElementString(writer, nameof(this.TRNUID), this.TRNUID);
            WriteElementString(writer, nameof(this.CLTCOOKIE), this.CLTCOOKIE);
            WriteElement(writer, this.trnpurpose);
            WriteElement(writer, this.eividentitycheckretrieveresultrq);
        }
    }
}
