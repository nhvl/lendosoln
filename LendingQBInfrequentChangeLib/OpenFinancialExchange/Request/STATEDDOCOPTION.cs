﻿namespace OpenFinancialExchange
{
    using System;
    using System.Xml;

    public class STATEDDOCOPTION : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return nameof(STATEDDOCOPTION); }
        }

        public E_STATEDDOCOPTION DOCOPTION { get; set; }

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case nameof(this.DOCOPTION):
                    this.DOCOPTION = (E_STATEDDOCOPTION)Enum.Parse(typeof(E_STATEDDOCOPTION), ReadElementString(reader));
                    break;
                default:
                    throw new ArgumentException($"Element {reader.Name} is not handled.");
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteRequiredElement(writer, this.DOCOPTION.ToString("G"), string.Empty);
        }
    }
}
