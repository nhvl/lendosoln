﻿namespace OpenFinancialExchange
{
    using System;
    using System.Xml;

    public class EMPLOYEEDETAILS : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return this.ElementName ?? nameof(EMPLOYEEDETAILS); }
        }

        public string ElementName { get; set; }

        public string EIVEMPLOYEEID { get; set; }

        public string FIRSTNAME { get; set; }

        public string LASTNAME { get; set; }


        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case nameof(this.EIVEMPLOYEEID):
                    this.EIVEMPLOYEEID = ReadElementString(reader);
                    break;
                case nameof(this.FIRSTNAME):
                    this.FIRSTNAME = ReadElementString(reader);
                    break;
                case nameof(this.LASTNAME):
                    this.LASTNAME = ReadElementString(reader);
                    break;
                default:
                    throw new ArgumentException($"Element {reader.Name} is not handled.");
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElementString(writer, nameof(this.EIVEMPLOYEEID), this.EIVEMPLOYEEID.Replace("-", ""));
            WriteElementString(writer, nameof(this.FIRSTNAME), this.FIRSTNAME);
            WriteElementString(writer, nameof(this.LASTNAME), this.LASTNAME);
        }
    }
}
