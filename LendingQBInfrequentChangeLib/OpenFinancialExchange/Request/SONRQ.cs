﻿namespace OpenFinancialExchange.Request
{
    using System;
    using System.Xml;

    public class SONRQ : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return nameof(SONRQ); }
        }

        public string DTCLIENT { get; set; }

        public string USERID { get; set; }

        public string USERPASS { get; set; }

        public string LANGUAGE { get; set; }

        public string APPID { get; set; }

        public string APPVER { get; set; }

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case nameof(this.DTCLIENT):
                    this.DTCLIENT = ReadElementString(reader);
                    break;
                case nameof(this.USERID):
                    this.USERID = ReadElementString(reader);
                    break;
                case nameof(this.USERPASS):
                    this.USERPASS = ReadElementString(reader);
                    break;
                case nameof(this.LANGUAGE):
                    this.LANGUAGE = ReadElementString(reader);
                    break;
                case nameof(this.APPID):
                    this.APPID = ReadElementString(reader);
                    break;
                case nameof(this.APPVER):
                    this.APPVER = ReadElementString(reader);
                    break;
                default:
                    throw new ArgumentException($"Element {reader.Name} is not handled.");
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElementString(writer, nameof(this.DTCLIENT), this.DTCLIENT);
            WriteElementString(writer, nameof(this.USERID), this.USERID);
            WriteElementString(writer, nameof(this.USERPASS), this.USERPASS);
            WriteElementString(writer, nameof(this.LANGUAGE), this.LANGUAGE);
            WriteElementString(writer, nameof(this.APPID), this.APPID);
            WriteElementString(writer, nameof(this.APPVER), this.APPVER);
        }
    }
}
