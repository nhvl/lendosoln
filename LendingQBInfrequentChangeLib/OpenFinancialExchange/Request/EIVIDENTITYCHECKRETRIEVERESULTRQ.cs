﻿namespace OpenFinancialExchange.Request
{
    using System;
    using System.Xml;

    public class EIVIDENTITYCHECKRETRIEVERESULTRQ : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return nameof(EIVIDENTITYCHECKRETRIEVERESULTRQ); }
        }

        public string SRVRTID;

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case nameof(this.SRVRTID):
                    this.SRVRTID = ReadElementString(reader);
                    break;
                default:
                    throw new ArgumentException($"Element {reader.Name} is not handled.");
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElementString(writer, nameof(this.SRVRTID), this.SRVRTID);
        }
    }
}
