﻿namespace OpenFinancialExchange
{
    using System;
    using System.Xml;

    public class BUSINESSDETAILS : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get
            {
                return nameof(BUSINESSDETAILS);
            }
        }

        public string FEDEIN { get; set; }

        public string BUSINESSNAME { get; set; }

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case nameof(this.FEDEIN):
                    this.FEDEIN = ReadElementString(reader);
                    break;
                case nameof(this.BUSINESSNAME):
                    this.BUSINESSNAME = ReadElementString(reader);
                    break;
                default:
                    throw new ArgumentException($"Element {reader.Name} is not handled.");
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElementString(writer, nameof(this.FEDEIN), this.FEDEIN.Replace("-", ""));
            WriteElementString(writer, nameof(this.BUSINESSNAME), this.BUSINESSNAME);
        }
    }
}
