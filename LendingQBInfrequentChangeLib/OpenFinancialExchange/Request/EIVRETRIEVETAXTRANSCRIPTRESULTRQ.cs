﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace OpenFinancialExchange.Request
{
    public class EIVRETRIEVETAXTRANSCRIPTRESULTRQ : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get
            {
                return nameof(EIVRETRIEVETAXTRANSCRIPTRESULTRQ);
            }
        }

        public string SRVRTID;

        // Special - use the presence or absence of this element to indicate a bool.
        public bool COMPLETEDREQUESTSONLY = false;

        public string TRANSCRIPTFORMAT = "PDF";

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case nameof(SRVRTID):
                    this.SRVRTID = ReadElementString(reader);
                    break;
                case nameof(COMPLETEDREQUESTSONLY):
                    this.COMPLETEDREQUESTSONLY = true;
                    break;
                case nameof(TRANSCRIPTFORMAT):
                    this.TRANSCRIPTFORMAT = "PDF";
                    break;
                default:
                    throw new ArgumentException($"Element {reader.Name} is not handled.");
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElementString(writer, nameof(this.SRVRTID), this.SRVRTID);
            if (this.COMPLETEDREQUESTSONLY)
            {
                WriteRequiredElement(writer, nameof(COMPLETEDREQUESTSONLY), null);
            }
            WriteElementString(writer, nameof(this.TRANSCRIPTFORMAT), this.TRANSCRIPTFORMAT);
        }
    }
}
