﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace OpenFinancialExchange.Request
{
    public class EIVRETRIEVETAXTRANSCRIPTRESULTTRNRQ : AbstractTransactionRequest
    {
        public override string XmlElementName { get { return nameof(EIVRETRIEVETAXTRANSCRIPTRESULTTRNRQ); } }

        private EIVRETRIEVETAXTRANSCRIPTRESULTRQ eivretrievetaxtranscriptresultrq;

        public EIVRETRIEVETAXTRANSCRIPTRESULTRQ EIVRETRIEVETAXTRANSCRIPTRESULTRQ
        {
            get
            {
                if (this.eivretrievetaxtranscriptresultrq == null)
                {
                    this.eivretrievetaxtranscriptresultrq = new EIVRETRIEVETAXTRANSCRIPTRESULTRQ();
                }

                return this.eivretrievetaxtranscriptresultrq;
            }

            set
            {
                this.eivretrievetaxtranscriptresultrq = value;
            }
        }

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case nameof(this.EIVRETRIEVETAXTRANSCRIPTRESULTRQ):
                    ReadElement(reader, this.EIVRETRIEVETAXTRANSCRIPTRESULTRQ);
                    return;
            }
            base.ReadElement(reader);
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            base.WriteXmlImpl(writer);
            this.WriteElement(writer, this.eivretrievetaxtranscriptresultrq);
        }
    }
}
