﻿namespace OpenFinancialExchange.Request
{
    using System;
    using System.Collections.Generic;
    using System.Xml;

    public class EIVTAXTRANSCRIPTRQ : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return nameof(EIVTAXTRANSCRIPTRQ); }
        }

        List<string> taxyear;
        public List<string> TAXYEAR
        {
            get
            {
                if (this.taxyear == null)
                {
                    this.taxyear = new List<string>();
                }

                return this.taxyear;
            }

            set
            {
                this.taxyear = value;
            }
        }

        public STATEDDOCOPTION STATEDDOCOPTION { get; set; }

        public string BORROWEREMAIL { get; set; }

        private EMPLOYEEDETAILS employeedetails;
        public EMPLOYEEDETAILS EMPLOYEEDETAILS
        {
            get
            {
                if (employeedetails == null)
                {
                    employeedetails = new EMPLOYEEDETAILS();
                }
                return employeedetails;
            }
            set { this.employeedetails = value; }
        }

        private EMPLOYEEDETAILS employee2details;
        public EMPLOYEEDETAILS EMPLOYEE2DETAILS
        {
            get
            {
                if (employee2details == null)
                {
                    employee2details = new EMPLOYEEDETAILS { ElementName = nameof(this.EMPLOYEE2DETAILS) };
                }
                return employee2details;
            }
            set { this.employee2details = value; }
        }

        private BUSINESSDETAILS businessdetails;
        public BUSINESSDETAILS BUSINESSDETAILS
        {
            get
            {
                if (businessdetails == null)
                {
                    businessdetails = new BUSINESSDETAILS();
                }
                return businessdetails;
            }
            set { this.businessdetails = value; }
        }

        private BORROWERADDRESS borroweraddress;
        public BORROWERADDRESS BORROWERADDRESS
        {
            get
            {
                if (borroweraddress == null)
                {
                    borroweraddress = new BORROWERADDRESS();
                }
                return borroweraddress;
            }
            set { this.borroweraddress = value; }
        }

        public VERIFICATIONATTACHMENTS VERIFICATIONATTACHMENTS { get; set; }

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case nameof(this.TAXYEAR):
                    this.TAXYEAR.Add(ReadElementString(reader));
                    break;
                case nameof(this.STATEDDOCOPTION):
                    ReadElement(reader, this.STATEDDOCOPTION);
                    break;
                case nameof(this.BORROWEREMAIL):
                    this.BORROWEREMAIL = ReadElementString(reader);
                    break;
                case nameof(this.EMPLOYEEDETAILS):
                    ReadElement(reader, this.EMPLOYEEDETAILS);
                    break;
                case nameof(this.EMPLOYEE2DETAILS):
                    ReadElement(reader, this.EMPLOYEE2DETAILS);
                    break;
                case nameof(this.BUSINESSDETAILS):
                    ReadElement(reader, this.BUSINESSDETAILS);
                    break;
                case nameof(this.VERIFICATIONATTACHMENTS):
                    ReadElement(reader, this.VERIFICATIONATTACHMENTS);
                    break;
                case nameof(this.BORROWERADDRESS):
                    ReadElement(reader, this.BORROWERADDRESS);
                    break;
                default:
                    throw new ArgumentException($"Element {reader.Name} is not handled.");
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElement(writer, this.employeedetails);
            WriteElement(writer, this.employee2details);
            WriteElement(writer, this.businessdetails);
            foreach (string taxYear in TAXYEAR)
            {
                WriteElementString(writer, nameof(this.TAXYEAR), DateTime.Parse(taxYear).Year.ToString());
            }

            WriteElement(writer, this.STATEDDOCOPTION);
            WriteElement(writer, this.VERIFICATIONATTACHMENTS);
            //WriteElement(writer, this.BUSINESSTITLE);
            WriteElementString(writer, nameof(this.BORROWEREMAIL), this.BORROWEREMAIL);
            WriteElement(writer, borroweraddress);
        }
    }
}
