﻿namespace OpenFinancialExchange.Request
{
    using System;
    using System.Xml;

    public class EIVIDENTITYCHECKTRNRQ : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return nameof(EIVIDENTITYCHECKTRNRQ); }
        }

        private EIVIDENTITYCHECKRQ eividentitycheckrq;

        public EIVIDENTITYCHECKRQ EIVIDENTITYCHECKRQ
        {
            get
            {
                if (this.eividentitycheckrq == null)
                {
                    this.eividentitycheckrq = new EIVIDENTITYCHECKRQ();
                }

                return this.eividentitycheckrq;
            }

            set
            {
                this.eividentitycheckrq = value;
            }
        }

        private TRNPURPOSE trnpurpose;

        public TRNPURPOSE TRNPURPOSE
        {
            get
            {
                if (this.trnpurpose == null)
                {
                    this.trnpurpose = new TRNPURPOSE();
                }

                return this.trnpurpose;
            }

            set
            {
                this.trnpurpose = value;
            }
        }

        public string TRNUID { get; set; }

        public string CLTCOOKIE { get; set; }

        public string PLATFORM { get; set; }

        public string INTERMEDIARY { get; set; }

        public string ENDUSER { get; set; }

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case nameof(this.EIVIDENTITYCHECKRQ):
                    ReadElement(reader, this.EIVIDENTITYCHECKRQ);
                    break;
                case nameof(this.TRNPURPOSE):
                    ReadElement(reader, this.TRNPURPOSE);
                    break;
                case nameof(this.TRNUID):
                    this.TRNUID = ReadElementString(reader);
                    break;
                case nameof(this.CLTCOOKIE):
                    this.CLTCOOKIE = ReadElementString(reader);
                    break;
                case nameof(this.PLATFORM):
                    this.PLATFORM = ReadElementString(reader);
                    break;
                case nameof(this.INTERMEDIARY):
                    this.INTERMEDIARY = ReadElementString(reader);
                    break;
                case nameof(this.ENDUSER):
                    this.ENDUSER = ReadElementString(reader);
                    break;
                default:
                    throw new ArgumentException($"Element {reader.Name} is not handled.");
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElementString(writer, nameof(this.TRNUID), this.TRNUID);
            WriteElementString(writer, nameof(this.CLTCOOKIE), this.CLTCOOKIE);
            WriteElementString(writer, nameof(this.PLATFORM), this.PLATFORM);
            WriteElementString(writer, nameof(this.INTERMEDIARY), this.INTERMEDIARY);
            WriteElementString(writer, nameof(this.ENDUSER), this.ENDUSER);
            WriteElement(writer, this.TRNPURPOSE);
            WriteElement(writer, this.EIVIDENTITYCHECKRQ);
        }
    }
}
