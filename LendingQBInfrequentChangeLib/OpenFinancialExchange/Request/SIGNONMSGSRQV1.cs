﻿namespace OpenFinancialExchange.Request
{
    using System;
    using System.Xml;

    public class SIGNONMSGSRQV1 : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return nameof(SIGNONMSGSRQV1); }
        }

        private SONRQ sonrq;

        public SONRQ SONRQ
        {
            get
            {
                if (this.sonrq == null)
                {
                    this.sonrq = new SONRQ();
                }

                return this.sonrq;
            }

            set
            {
                this.sonrq = value;
            }
        }

        protected override void ReadElement(XmlReader reader)
        {
            switch(reader.Name)
            {
                case nameof(this.SONRQ):
                    ReadElement(reader, this.SONRQ);
                    break;
                default:
                    throw new ArgumentException($"Element {reader.Name} is not handled.");
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElement(writer, this.sonrq);
        }
    }
}
