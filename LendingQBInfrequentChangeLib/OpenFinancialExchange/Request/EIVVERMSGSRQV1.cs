﻿namespace OpenFinancialExchange.Request
{
    using System;
    using System.Xml;

    public class EIVVERMSGSRQV1 : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return nameof(EIVVERMSGSRQV1); }
        }

        private EIVIDENTITYCHECKTRNRQ eividentitychecktrnrq;

        public EIVIDENTITYCHECKTRNRQ EIVIDENTITYCHECKTRNRQ
        {
            get
            {
                if (this.eividentitychecktrnrq == null)
                {
                    this.eividentitychecktrnrq = new EIVIDENTITYCHECKTRNRQ();
                }

                return this.eividentitychecktrnrq;
            }

            set
            {
                this.eividentitychecktrnrq = value;
            }
        }

        private EIVOFFLINEVERIFICATIONSTATUSTRNRQ eivofflineverificationstatustrnrq;

        public EIVOFFLINEVERIFICATIONSTATUSTRNRQ EIVOFFLINEVERIFICATIONSTATUSTRNRQ
        {
            get
            {
                if (this.eivofflineverificationstatustrnrq == null)
                {
                    this.eivofflineverificationstatustrnrq = new EIVOFFLINEVERIFICATIONSTATUSTRNRQ();
                }

                return this.eivofflineverificationstatustrnrq;
            }

            set
            {
                this.eivofflineverificationstatustrnrq = value;
            }
        }

        private EIVIDENTITYCHECKRETRIEVERESULTTRNRQ eividentitycheckretrieveresulttrnrq;

        public EIVIDENTITYCHECKRETRIEVERESULTTRNRQ EIVIDENTITYCHECKRETRIEVERESULTTRNRQ
        {
            get
            {
                if (this.eividentitycheckretrieveresulttrnrq == null)
                {
                    this.eividentitycheckretrieveresulttrnrq = new EIVIDENTITYCHECKRETRIEVERESULTTRNRQ();
                }

                return this.eividentitycheckretrieveresulttrnrq;
            }

            set
            {
                this.eividentitycheckretrieveresulttrnrq = value;
            }
        }

        private EIVTAXTRANSCRIPTTRNRQ eivtaxtranscripttrnrq;

        /// <summary>
        /// TRV/4506-T tax transcript transaction initial request.
        /// </summary>
        public EIVTAXTRANSCRIPTTRNRQ EIVTAXTRANSCRIPTTRNRQ
        {
            get
            {
                if (this.eivtaxtranscripttrnrq == null)
                {
                    this.eivtaxtranscripttrnrq = new EIVTAXTRANSCRIPTTRNRQ();
                }

                return this.eivtaxtranscripttrnrq;
            }

            set
            {
                this.eivtaxtranscripttrnrq = value;
            }
        }

        private EIVRETRIEVETAXTRANSCRIPTRESULTTRNRQ eivretrievetaxtranscriptresulttrnrq;

        /// <summary>
        /// TRV/4506-T tax transcript transaction result retrieval request.
        /// </summary>
        public EIVRETRIEVETAXTRANSCRIPTRESULTTRNRQ EIVRETRIEVETAXTRANSCRIPTRESULTTRNRQ
        {
            get
            {
                if (this.eivretrievetaxtranscriptresulttrnrq == null)
                {
                    this.eivretrievetaxtranscriptresulttrnrq = new EIVRETRIEVETAXTRANSCRIPTRESULTTRNRQ();
                }

                return this.eivretrievetaxtranscriptresulttrnrq;
            }

            set
            {
                this.eivretrievetaxtranscriptresulttrnrq = value;
            }
        }

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case nameof(EIVIDENTITYCHECKTRNRQ):
                    ReadElement(reader, this.EIVIDENTITYCHECKTRNRQ);
                    break;
                case nameof(this.EIVOFFLINEVERIFICATIONSTATUSTRNRQ):
                    ReadElement(reader, this.EIVOFFLINEVERIFICATIONSTATUSTRNRQ);
                    break;
                case nameof(this.EIVIDENTITYCHECKRETRIEVERESULTTRNRQ):
                    ReadElement(reader, this.EIVIDENTITYCHECKRETRIEVERESULTTRNRQ);
                    break;
                case nameof(this.EIVTAXTRANSCRIPTTRNRQ):
                    ReadElement(reader, this.EIVTAXTRANSCRIPTTRNRQ);
                    break;
                case nameof(this.EIVRETRIEVETAXTRANSCRIPTRESULTTRNRQ):
                    ReadElement(reader, this.EIVRETRIEVETAXTRANSCRIPTRESULTTRNRQ);
                    break;
                default:
                    throw new ArgumentException($"Element {reader.Name} is not handled.");
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElement(writer, this.eividentitychecktrnrq);
            WriteElement(writer, this.eivofflineverificationstatustrnrq);
            WriteElement(writer, this.eividentitycheckretrieveresulttrnrq);
            WriteElement(writer, this.eivtaxtranscripttrnrq);
            WriteElement(writer, this.eivretrievetaxtranscriptresulttrnrq);
        }
    }
}
