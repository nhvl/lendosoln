﻿namespace OpenFinancialExchange
{
    using System;
    using System.Xml;
    using OpenFinancialExchange.Request;
    using OpenFinancialExchange.Response;

    /// <summary>
    /// Represents the base level of our implementation of the Open Financial Exchange XML format. 
    /// See http://www.ofx.net/downloads/OFX%202.2.pdf for the current OFX specification (7/11/2017).
    /// </summary>
    public class OFX : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return nameof(OFX); }
        }

        private SIGNONMSGSRQV1 signonmsgsrqv1;

        public SIGNONMSGSRQV1 SIGNONMSGSRQV1
        {
            get
            {
                if (this.signonmsgsrqv1 == null)
                {
                    this.signonmsgsrqv1 = new SIGNONMSGSRQV1();
                }

                return this.signonmsgsrqv1;
            }

            set
            {
                this.signonmsgsrqv1 = value;
            }
        }

        private SIGNONMSGSRSV1 signonmsgsrsv1;

        public SIGNONMSGSRSV1 SIGNONMSGSRSV1
        {
            get
            {
                if (this.signonmsgsrsv1 == null)
                {
                    this.signonmsgsrsv1 = new SIGNONMSGSRSV1();
                }

                return this.signonmsgsrsv1;
            }

            set
            {
                this.signonmsgsrsv1 = value;
            }
        }

        private EIVVERMSGSRQV1 eivvermsgsrqv1;

        public EIVVERMSGSRQV1 EIVVERMSGSRQV1
        {
            get
            {
                if (this.eivvermsgsrqv1 == null)
                {
                    this.eivvermsgsrqv1 = new EIVVERMSGSRQV1();
                }

                return this.eivvermsgsrqv1;
            }

            set
            {
                this.eivvermsgsrqv1 = value;
            }
        }

        private EIVVERMSGSRSV1 eivvermsgsrsv1;
        public EIVVERMSGSRSV1 EIVVERMSGSRSV1
        {
            get
            {
                if (this.eivvermsgsrsv1 == null)
                {
                    this.eivvermsgsrsv1 = new EIVVERMSGSRSV1();
                }

                return this.eivvermsgsrsv1;
            }

            set
            {
                this.eivvermsgsrsv1 = value;
            }
        }

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case nameof(this.SIGNONMSGSRQV1):
                    ReadElement(reader, this.SIGNONMSGSRQV1);
                    break;
                case nameof(this.EIVVERMSGSRQV1):
                    ReadElement(reader, this.EIVVERMSGSRQV1);
                    break;
                case nameof(this.SIGNONMSGSRSV1):
                    ReadElement(reader, this.SIGNONMSGSRSV1);
                    break;
                case nameof(this.EIVVERMSGSRSV1):
                    ReadElement(reader, this.EIVVERMSGSRSV1);
                    break;
                default:
                    throw new ArgumentException($"Element {reader.Name} is not handled.");
            }
        }

        /// <summary>
        /// Specifies the OFX declaration version. OFX 2.x uses "200", which is the default value for this property.
        /// </summary>
        /// <value>OFX header version.</value>
        public int OFXHEADER { get; set; } = 200;

        /// <summary>
        /// Specifies the version number of the OFX data. Equifax Verification Services and other services seem to use "201", so that's the default here.
        /// </summary>
        public int VERSION { get; set; } = 201;

        ////// We don't have any use for the following OFX header parts:
        ////public E_SECURITY SECURITY { get; set; } = E_SECURITY.NONE;
        ////public string NEWFILEUID { get; set; };
        ////public string OLDFILEUID { get; set; };

        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteProcessingInstruction("OFX", $"OFXHEADER=\"{OFXHEADER}\" VERSION=\"{VERSION}\" SECURITY=\"NONE\" OLDFILEUID=\"NONE\" NEWFILEUID=\"NONE\"");
            base.WriteXml(writer);
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElement(writer, this.signonmsgsrqv1);
            WriteElement(writer, this.eivvermsgsrqv1);
            WriteElement(writer, this.signonmsgsrsv1);
            WriteElement(writer, this.eivvermsgsrsv1);
        }
    }
}
