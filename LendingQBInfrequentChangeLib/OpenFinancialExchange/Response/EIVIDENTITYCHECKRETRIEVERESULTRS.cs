﻿namespace OpenFinancialExchange.Response
{
    using System;
    using System.Xml;

    public class EIVIDENTITYCHECKRETRIEVERESULTRS : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return nameof(EIVIDENTITYCHECKRETRIEVERESULTRS); }
        }

        private ORDERDATA orderdata;

        public ORDERDATA ORDERDATA
        {
            get
            {
                if (this.orderdata == null)
                {
                    this.orderdata = new ORDERDATA();
                }

                return this.orderdata;
            }

            set
            {
                this.orderdata = value;
            }
        }

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case nameof(this.ORDERDATA):
                    ReadElement(reader, this.ORDERDATA);
                    break;
                default:
                    throw new ArgumentException($"Element {reader.Name} is not handled.");
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElement(writer, this.ORDERDATA);
        }
    }
}
