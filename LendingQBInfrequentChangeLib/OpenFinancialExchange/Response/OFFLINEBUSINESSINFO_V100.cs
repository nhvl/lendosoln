﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace OpenFinancialExchange.Response
{
    public class OFFLINEBUSINESSINFO_V100 : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get
            {
                return nameof(OFFLINEBUSINESSINFO_V100);
            }
        }

        public string FEDEIN { get; set; }

        public string BUSINESSNAME { get; set; }

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case nameof(this.FEDEIN):
                    this.FEDEIN = ReadElementString(reader);
                    break;
                case nameof(this.BUSINESSNAME):
                    this.BUSINESSNAME = ReadElementString(reader);
                    break;
                default:
                    throw new ArgumentException($"Element {reader.Name} is not handled.");
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElementString(writer, nameof(this.FEDEIN), this.FEDEIN);
            WriteElementString(writer, nameof(this.BUSINESSNAME), this.BUSINESSNAME);
        }
    }
}
