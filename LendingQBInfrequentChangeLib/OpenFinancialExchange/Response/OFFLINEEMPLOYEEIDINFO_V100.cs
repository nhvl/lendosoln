﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace OpenFinancialExchange.Response
{
    public class OFFLINEEMPLOYEEIDINFO_V100 : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get
            {
                return nameof(OFFLINEEMPLOYEEIDINFO_V100);
            }
        }

        public string PERSONALIDENTIFIER { get; set; }

        public string GIVENNAME { get; set; }

        public string MIDDLENAME { get; set; }

        public string FAMILYNAME { get; set; }

        public string DTBIRTH { get; set; }

        public string POSITIONTITLE { get; set; }

        public const string POSITIONTITLE_ElementName = "POSITION-TITLE";

        public string EMPLOYMENTTYPE { get; set; }

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case nameof(this.PERSONALIDENTIFIER):
                    this.PERSONALIDENTIFIER = ReadElementString(reader);
                    break;
                case nameof(this.GIVENNAME):
                    this.GIVENNAME = ReadElementString(reader);
                    break;
                case nameof(this.MIDDLENAME):
                    this.MIDDLENAME = ReadElementString(reader);
                    break;
                case nameof(this.FAMILYNAME):
                    this.FAMILYNAME = ReadElementString(reader);
                    break;
                case nameof(this.DTBIRTH):
                    this.DTBIRTH = ReadElementString(reader);
                    break;
                case nameof(POSITIONTITLE_ElementName):
                    this.POSITIONTITLE = ReadElementString(reader);
                    break;
                case nameof(this.EMPLOYMENTTYPE):
                    this.EMPLOYMENTTYPE = ReadElementString(reader);
                    break;
                default:
                    throw new ArgumentException($"Element {reader.Name} is not handled.");
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElementString(writer, nameof(this.PERSONALIDENTIFIER), this.PERSONALIDENTIFIER);
            WriteElementString(writer, nameof(this.GIVENNAME), this.GIVENNAME);
            WriteElementString(writer, nameof(this.MIDDLENAME), this.MIDDLENAME);
            WriteElementString(writer, nameof(this.FAMILYNAME), this.FAMILYNAME);
            WriteElementString(writer, nameof(this.DTBIRTH), this.DTBIRTH);
            WriteElementString(writer, POSITIONTITLE_ElementName, this.POSITIONTITLE);
            WriteElementString(writer, nameof(this.EMPLOYMENTTYPE), this.EMPLOYMENTTYPE);
        }
    }
}
