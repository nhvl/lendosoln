﻿using System;
using System.Xml;

namespace OpenFinancialExchange.Response
{
    public class EIVRETRIEVETAXTRANSCRIPTRESULTRS : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get
            {
                return nameof(EIVRETRIEVETAXTRANSCRIPTRESULTRS);
            }
        }

        public string SRVRTID { get; set; }

        /// <summary>
        /// This is its own XML sub-content that we don't want to bother with at the moment, so store it as a string.
        /// </summary>
        public string EIVTAXTRANSCRIPTS { get; set; }

        public string COMPLETENESS  { get; set; }

        private ATTACHEDFILES attachedfiles;
        public ATTACHEDFILES ATTACHEDFILES
        {
            get
            {
                if (attachedfiles == null)
                {
                    attachedfiles = new ATTACHEDFILES();
                }
                return attachedfiles;
            }
            set
            {
                this.attachedfiles = value;
            }
        }

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case nameof(this.SRVRTID):
                    this.SRVRTID = ReadElementString(reader);
                    break;
                case nameof(this.EIVTAXTRANSCRIPTS):
                    this.EIVTAXTRANSCRIPTS = reader.ReadOuterXml() ?? string.Empty;
                    break;
                case nameof(this.COMPLETENESS):
                    this.COMPLETENESS = ReadElementString(reader);
                    break;
                case nameof(this.ATTACHEDFILES):
                    ReadElement(reader, this.ATTACHEDFILES);
                    break;
                default:
                    throw new ArgumentException($"Element {reader.Name} is not handled.");
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElementString(writer, nameof(this.SRVRTID), this.SRVRTID);
            writer.WriteRaw(this.EIVTAXTRANSCRIPTS);
            WriteElementString(writer, nameof(this.COMPLETENESS), this.COMPLETENESS);
            WriteElement(writer, ATTACHEDFILES);
        }
    }
}
