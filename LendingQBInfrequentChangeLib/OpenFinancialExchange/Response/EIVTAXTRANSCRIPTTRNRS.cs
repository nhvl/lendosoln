﻿using System;
using System.Xml;

namespace OpenFinancialExchange.Response
{
    public class EIVTAXTRANSCRIPTTRNRS : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get
            {
                return nameof(EIVTAXTRANSCRIPTTRNRS);
            }
        }

        public string TRNUID
        {
            get; set;
        }

        public string MASTERSRVRTID
        {
            get; set;
        }

        public STATUS STATUS { get; set; } = new STATUS();

        public EIVTAXTRANSCRIPTRS EIVTAXTRANSCRIPTRS { get; set; } = new EIVTAXTRANSCRIPTRS();

        public string CLTCOOKIE { get; set; }

        public TRNPURPOSE TRNPURPOSE { get; set; } = new TRNPURPOSE();

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case nameof(this.TRNUID):
                    this.TRNUID = ReadElementString(reader);
                    break;
                case nameof(this.MASTERSRVRTID):
                    this.MASTERSRVRTID = ReadElementString(reader);
                    break;
                case nameof(this.TRNPURPOSE):
                    ReadElement(reader, this.TRNPURPOSE);
                    break;
                case nameof(this.STATUS):
                    ReadElement(reader, this.STATUS);
                    break;
                case nameof(this.EIVTAXTRANSCRIPTRS):
                    ReadElement(reader, this.EIVTAXTRANSCRIPTRS);
                    break;
                case nameof(this.CLTCOOKIE):
                    this.CLTCOOKIE = ReadElementString(reader);
                    break;
                default:
                    throw new ArgumentException($"Element {reader.Name} is not handled.");
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElementString(writer, nameof(this.TRNUID), this.TRNUID);
            WriteElement(writer, this.STATUS);
            WriteElementString(writer, nameof(this.MASTERSRVRTID), this.MASTERSRVRTID);
            WriteElement(writer, this.TRNPURPOSE);
            WriteElementString(writer, nameof(this.CLTCOOKIE), this.CLTCOOKIE);
            WriteElement(writer, this.EIVTAXTRANSCRIPTRS);
        }
    }
}
