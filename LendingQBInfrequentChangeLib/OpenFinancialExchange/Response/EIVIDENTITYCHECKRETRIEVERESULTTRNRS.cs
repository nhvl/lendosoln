﻿namespace OpenFinancialExchange.Response
{
    using System;
    using System.Xml;

    public class EIVIDENTITYCHECKRETRIEVERESULTTRNRS : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return nameof(EIVIDENTITYCHECKRETRIEVERESULTTRNRS); }
        }

        private STATUS status;

        public STATUS STATUS
        {
            get
            {
                if (this.status == null)
                {
                    this.status = new STATUS();
                }

                return this.status;
            }

            set
            {
                this.status = value;
            }
        }

        private EIVIDENTITYCHECKRETRIEVERESULTRS eividentitycheckretrieveresultrs;

        public EIVIDENTITYCHECKRETRIEVERESULTRS EIVIDENTITYCHECKRETRIEVERESULTRS
        {
            get
            {
                if (this.eividentitycheckretrieveresultrs == null)
                {
                    this.eividentitycheckretrieveresultrs = new EIVIDENTITYCHECKRETRIEVERESULTRS();
                }

                return this.eividentitycheckretrieveresultrs;
            }

            set
            {
                this.eividentitycheckretrieveresultrs = value;
            }
        }

        public string TRNUID;

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case nameof(this.STATUS):
                    ReadElement(reader, this.STATUS);
                    break;
                case nameof(this.EIVIDENTITYCHECKRETRIEVERESULTRS):
                    ReadElement(reader, this.EIVIDENTITYCHECKRETRIEVERESULTRS);
                    break;
                case nameof(this.TRNUID):
                    this.TRNUID = ReadElementString(reader);
                    break;
                default:
                    throw new ArgumentException($"Element {reader.Name} is not handled.");
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElement(writer, this.STATUS);
            WriteElement(writer, this.EIVIDENTITYCHECKRETRIEVERESULTRS);
            WriteElementString(writer, nameof(this.TRNUID), this.TRNUID);
        }
    }
}
