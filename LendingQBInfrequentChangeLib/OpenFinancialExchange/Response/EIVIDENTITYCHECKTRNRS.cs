﻿namespace OpenFinancialExchange.Response
{
    using System;
    using System.Xml;

    public class EIVIDENTITYCHECKTRNRS : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return nameof(EIVIDENTITYCHECKTRNRS); }
        }

        public string TRNUID;

        private STATUS status;

        public STATUS STATUS
        {
            get
            {
                if (this.status == null)
                {
                    this.status = new STATUS();
                }

                return this.status;
            }

            set
            {
                this.status = value;
            }
        }

        private EIVIDENTITYCHECKRS eividentitycheckrs;

        public EIVIDENTITYCHECKRS EIVIDENTITYCHECKRS
        {
            get
            {
                if (this.eividentitycheckrs == null)
                {
                    this.eividentitycheckrs = new EIVIDENTITYCHECKRS();
                }

                return this.eividentitycheckrs;
            }

            set
            {
                this.eividentitycheckrs = value;
            }
        }

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case nameof(this.TRNUID):
                    this.TRNUID = ReadElementString(reader);
                    break;
                case nameof(this.STATUS):
                    ReadElement(reader, this.STATUS);
                    break;
                case nameof(this.EIVIDENTITYCHECKRS):
                    ReadElement(reader, this.EIVIDENTITYCHECKRS);
                    break;
                default:
                    throw new ArgumentException($"Element {reader.Name} is not handled.");
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElementString(writer, nameof(this.TRNUID), this.TRNUID);
            WriteElement(writer, this.STATUS);
            WriteElement(writer, this.EIVIDENTITYCHECKRS);
        }
    }
}
