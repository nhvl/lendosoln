﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace OpenFinancialExchange.Response
{
    public class EIVTAXTRANSCRIPTRS : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get
            {
                return nameof(EIVTAXTRANSCRIPTRS);
            }
        }

        public int COMPLETENESS { get; set; }

        public string SRVRTID { get; set; }

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case nameof(this.COMPLETENESS):
                    this.COMPLETENESS = int.Parse(ReadElementString(reader));
                    break;
                case nameof(this.SRVRTID):
                    this.SRVRTID = ReadElementString(reader);
                    break;
                default:
                    throw new ArgumentException($"Element {reader.Name} is not handled.");
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElementString(writer, nameof(this.COMPLETENESS), this.COMPLETENESS.ToString());
            WriteElementString(writer, nameof(this.SRVRTID), this.SRVRTID);
        }
    }
}
