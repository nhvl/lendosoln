﻿using System;
using System.Xml;

namespace OpenFinancialExchange.Response
{
    public class OFFLINEVERIFICATIONSTATE : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get
            {
                return nameof(OFFLINEVERIFICATIONSTATE);
            }
        }

        public OFFLINESTATETYPE OFFLINESTATETYPE { get; set; }

        public string OFFLINESTATEMESSAGE { get; set; }

        public DateTime? DTOFFLINESTATE { get; set; }

        public E_YESNO AUTHORIZATIONFORMPRESENT { get; set; }

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case nameof(this.OFFLINESTATETYPE):
                    this.OFFLINESTATETYPE = ReadElementEnum<OFFLINESTATETYPE>(reader) ?? OFFLINESTATETYPE.Unknown;
                    break;
                case nameof(this.OFFLINESTATEMESSAGE):
                    this.OFFLINESTATEMESSAGE = ReadElementString(reader);
                    break;
                case nameof(this.DTOFFLINESTATE):
                    DateTime dtOfflineState;
                    if (DateTime.TryParseExact(ReadElementString(reader), "yyyyMMddHHmmss", null, System.Globalization.DateTimeStyles.None, out dtOfflineState))
                    {
                        this.DTOFFLINESTATE = dtOfflineState;
                    }
                    else
                    {
                        this.DTOFFLINESTATE = null;
                    }
                    break;
                case nameof(this.AUTHORIZATIONFORMPRESENT):
                    this.AUTHORIZATIONFORMPRESENT = ReadElementYN(reader);
                    break;
                default:
                    throw new ArgumentException($"Element {reader.Name} is not handled.");
            }
        }
    }
}
