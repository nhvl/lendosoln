﻿namespace OpenFinancialExchange.Response
{
    using System;
    using System.Xml;

    public class SONRS : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return nameof(SONRS); }
        }

        private STATUS status;

        public STATUS STATUS
        {
            get
            {
                if (this.status == null)
                {
                    this.status = new STATUS();
                }

                return this.status;
            }
        }

        public string DTSERVER { get; set; }

        public string LANGUAGE { get; set; }

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case nameof(this.STATUS):
                    ReadElement(reader, this.STATUS);
                    break;
                case nameof(this.DTSERVER):
                    this.DTSERVER = ReadElementString(reader);
                    break;
                case nameof(this.LANGUAGE):
                    this.LANGUAGE = ReadElementString(reader);
                    break;
                default:
                    throw new ArgumentException($"Element {reader.Name} is not handled.");
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElement(writer, this.status);
            WriteElementString(writer, nameof(this.DTSERVER), this.DTSERVER);
            WriteElementString(writer, nameof(this.LANGUAGE), this.LANGUAGE);
        }
    }
}
