﻿namespace OpenFinancialExchange.Response
{
    using System;
    using System.Xml;

    public class EIVVERMSGSRSV1 : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return nameof(EIVVERMSGSRSV1); }
        }

        private EIVIDENTITYCHECKTRNRS eividentitychecktrnrs;

        public EIVIDENTITYCHECKTRNRS EIVIDENTITYCHECKTRNRS
        {
            get
            {
                if (this.eividentitychecktrnrs == null)
                {
                    this.eividentitychecktrnrs = new EIVIDENTITYCHECKTRNRS();
                }

                return this.eividentitychecktrnrs;
            }

            set
            {
                this.eividentitychecktrnrs = value;
            }
        }

        private EIVOFFLINEVERIFICATIONSTATUSTRNRS eivofflineverificationstatustrnrs;

        public EIVOFFLINEVERIFICATIONSTATUSTRNRS EIVOFFLINEVERIFICATIONSTATUSTRNRS
        {
            get
            {
                if (this.eivofflineverificationstatustrnrs == null)
                {
                    this.eivofflineverificationstatustrnrs = new EIVOFFLINEVERIFICATIONSTATUSTRNRS();
                }

                return this.eivofflineverificationstatustrnrs;
            }

            set
            {
                this.eivofflineverificationstatustrnrs = value;
            }
        }

        private EIVIDENTITYCHECKRETRIEVERESULTTRNRS eividentitycheckretrieveresulttrnrs;

        public EIVIDENTITYCHECKRETRIEVERESULTTRNRS EIVIDENTITYCHECKRETRIEVERESULTTRNRS
        {
            get
            {
                if (this.eividentitycheckretrieveresulttrnrs == null)
                {
                    this.eividentitycheckretrieveresulttrnrs = new EIVIDENTITYCHECKRETRIEVERESULTTRNRS();
                }

                return this.eividentitycheckretrieveresulttrnrs;
            }

            set
            {
                this.eividentitycheckretrieveresulttrnrs = value;
            }
        }

        private EIVTAXTRANSCRIPTTRNRS eivtaxtranscripttrnrs;
        public EIVTAXTRANSCRIPTTRNRS EIVTAXTRANSCRIPTTRNRS
        {
            get
            {
                if (this.eivtaxtranscripttrnrs == null)
                {
                    this.eivtaxtranscripttrnrs = new EIVTAXTRANSCRIPTTRNRS();
                }

                return this.eivtaxtranscripttrnrs;
            }

            set
            {
                this.eivtaxtranscripttrnrs = value;
            }
        }

        public EIVRETRIEVETAXTRANSCRIPTRESULTTRNRS EIVRETRIEVETAXTRANSCRIPTRESULTTRNRS { get; set; } = new EIVRETRIEVETAXTRANSCRIPTRESULTTRNRS();

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case nameof(this.EIVIDENTITYCHECKTRNRS):
                    ReadElement(reader, this.EIVIDENTITYCHECKTRNRS);
                    break;
                case nameof(this.EIVOFFLINEVERIFICATIONSTATUSTRNRS):
                    ReadElement(reader, this.EIVOFFLINEVERIFICATIONSTATUSTRNRS);
                    break;
                case nameof(this.EIVIDENTITYCHECKRETRIEVERESULTTRNRS):
                    ReadElement(reader, this.EIVIDENTITYCHECKRETRIEVERESULTTRNRS);
                    break;
                case nameof(this.EIVTAXTRANSCRIPTTRNRS):
                    ReadElement(reader, this.EIVTAXTRANSCRIPTTRNRS);
                    break;
                case nameof(this.EIVRETRIEVETAXTRANSCRIPTRESULTTRNRS):
                    ReadElement(reader, EIVRETRIEVETAXTRANSCRIPTRESULTTRNRS);
                    break;
                default:
                    throw new ArgumentException($"Element {reader.Name} is not handled.");
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElement(writer, this.EIVIDENTITYCHECKTRNRS);
            WriteElement(writer, this.EIVOFFLINEVERIFICATIONSTATUSTRNRS);
            WriteElement(writer, this.EIVIDENTITYCHECKRETRIEVERESULTTRNRS);
            WriteElement(writer, this.EIVTAXTRANSCRIPTTRNRS);
            WriteElement(writer, this.EIVRETRIEVETAXTRANSCRIPTRESULTTRNRS);
        }
    }
}
