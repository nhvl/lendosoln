﻿using System;
using System.Xml;

namespace OpenFinancialExchange.Response
{
    public class OFFLINETAXTRANSCRIPTINFO : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get
            {
                return nameof(OFFLINETAXTRANSCRIPTINFO);
            }
        }

        public string TRNUID { get; set; }

        public string SRVRTID { get; set; }

        public DateTime? DTREQUEST { get; set; }

        public OFFLINEBUSINESSINFO_V100 OFFLINEBUSINESSINFO_V100 { get; set; } = new OFFLINEBUSINESSINFO_V100();

        public OFFLINEEMPLOYEEIDINFO_V100 OFFLINEEMPLOYEEIDINFO_V100 { get; set; } = new OFFLINEEMPLOYEEIDINFO_V100();

        public OFFLINEVERIFICATIONSTATE OFFLINEVERIFICATIONSTATE { get; set; } = new OFFLINEVERIFICATIONSTATE();

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case nameof(this.TRNUID):
                    this.TRNUID = ReadElementString(reader);
                    break;
                case nameof(this.SRVRTID):
                    this.SRVRTID = ReadElementString(reader);
                    break;
                case nameof(DTREQUEST):
                    DateTime requestDate;
                    if (DateTime.TryParseExact(ReadElementString(reader), "yyyyMMddHHmmss", null, System.Globalization.DateTimeStyles.None, out requestDate))
                    {
                        this.DTREQUEST = requestDate;
                    }
                    else
                    {
                        this.DTREQUEST = null;
                    }
                    break;
                case nameof(OFFLINEBUSINESSINFO_V100):
                    ReadElement(reader, this.OFFLINEBUSINESSINFO_V100);
                    break;
                case nameof(OFFLINEEMPLOYEEIDINFO_V100):
                    ReadElement(reader, this.OFFLINEEMPLOYEEIDINFO_V100);
                    break;
                case nameof(OFFLINEVERIFICATIONSTATE):
                    ReadElement(reader, this.OFFLINEVERIFICATIONSTATE);
                    break;
                default:
                    throw new ArgumentException($"Element {reader.Name} is not handled.");
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElementString(writer, nameof(this.TRNUID), this.TRNUID);
            WriteElementString(writer, nameof(this.SRVRTID), this.SRVRTID);
            WriteElementString(writer, nameof(this.DTREQUEST), this.DTREQUEST?.ToString("yyyyMMddHHmmss"));
            WriteElement(writer, OFFLINEBUSINESSINFO_V100);
            WriteElement(writer, OFFLINEVERIFICATIONSTATE);
        }
    }
}
