﻿namespace OpenFinancialExchange.Response
{
    using System;
    using System.Xml;

    public class SIGNONMSGSRSV1 : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return nameof(SIGNONMSGSRSV1); }
        }

        private SONRS sonrs;

        public SONRS SONRS
        {
            get
            {
                if (this.sonrs == null)
                {
                    this.sonrs = new SONRS();
                }

                return this.sonrs;
            }

            set
            {
                this.sonrs = value;
            }
        }

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case nameof(SONRS):
                    ReadElement(reader, this.SONRS);
                    break;
                default:
                    throw new ArgumentException($"Element {reader.Name} is not handled.");
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElement(writer, this.sonrs);
        }
    }
}
