﻿using System;
using System.Xml;

namespace OpenFinancialExchange.Response
{
    public class EIVRETRIEVETAXTRANSCRIPTRESULTTRNRS : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get
            {
                return nameof(EIVRETRIEVETAXTRANSCRIPTRESULTTRNRS);
            }
        }

        public string TRNUID { get; set; }

        public STATUS STATUS { get; set; } = new STATUS();

        public EIVRETRIEVETAXTRANSCRIPTRESULTRS EIVRETRIEVETAXTRANSCRIPTRESULTRS { get; set; } = new EIVRETRIEVETAXTRANSCRIPTRESULTRS();

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case nameof(this.TRNUID):
                    this.TRNUID = ReadElementString(reader);
                    break;
                case nameof(this.STATUS):
                    ReadElement(reader, this.STATUS);
                    break;
                case nameof(this.EIVRETRIEVETAXTRANSCRIPTRESULTRS):
                    ReadElement(reader, this.EIVRETRIEVETAXTRANSCRIPTRESULTRS);
                    break;
                default:
                    throw new ArgumentException($"Element {reader.Name} is not handled.");
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElementString(writer, nameof(this.TRNUID), this.TRNUID);
            WriteElement(writer, this.STATUS);
            WriteElement(writer, this.EIVRETRIEVETAXTRANSCRIPTRESULTRS);
        }
    }
}
