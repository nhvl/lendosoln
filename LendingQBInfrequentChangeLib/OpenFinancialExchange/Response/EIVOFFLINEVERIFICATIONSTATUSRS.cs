﻿namespace OpenFinancialExchange.Response
{
    using System;
    using System.Xml;

    public class EIVOFFLINEVERIFICATIONSTATUSRS : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return nameof(EIVOFFLINEVERIFICATIONSTATUSRS); }
        }

        private IDENTITYCHECKINFO identitycheckinfo;

        public IDENTITYCHECKINFO IDENTITYCHECKINFO
        {
            get
            {
                if (this.identitycheckinfo == null)
                {
                    this.identitycheckinfo = new IDENTITYCHECKINFO();
                }

                return this.identitycheckinfo;
            }

            set
            {
                this.identitycheckinfo = value;
            }
        }

        public OFFLINETAXTRANSCRIPTINFO OFFLINETAXTRANSCRIPTINFO { get; set; } = new OFFLINETAXTRANSCRIPTINFO();

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case nameof(this.IDENTITYCHECKINFO):
                    ReadElement(reader, this.IDENTITYCHECKINFO);
                    break;
                case nameof(this.OFFLINETAXTRANSCRIPTINFO):
                    ReadElement(reader, this.OFFLINETAXTRANSCRIPTINFO);
                    break;
                default:
                    throw new ArgumentException($"Element {reader.Name} is not handled.");
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElement(writer, this.IDENTITYCHECKINFO);
            WriteElement(writer, this.OFFLINETAXTRANSCRIPTINFO);
        }
    }
}
