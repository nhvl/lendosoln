﻿namespace OpenFinancialExchange.Response
{
    using System;
    using System.Xml;

    public class EIVOFFLINEVERIFICATIONSTATUSTRNRS : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return nameof(EIVOFFLINEVERIFICATIONSTATUSTRNRS); }
        }

        public string TRNUID { get; set; }

        private STATUS status;

        public STATUS STATUS
        {
            get
            {
                if (this.status == null)
                {
                    this.status = new STATUS();
                }

                return this.status;
            }

            set
            {
                this.status = value;
            }
        }

        private EIVOFFLINEVERIFICATIONSTATUSRS eivofflineverificationstatusrs;

        public EIVOFFLINEVERIFICATIONSTATUSRS EIVOFFLINEVERIFICATIONSTATUSRS
        {
            get
            {
                if (this.eivofflineverificationstatusrs == null)
                {
                    this.eivofflineverificationstatusrs = new EIVOFFLINEVERIFICATIONSTATUSRS();
                }

                return this.eivofflineverificationstatusrs;
            }

            set
            {
                this.eivofflineverificationstatusrs = value;
            }
        }

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case nameof(this.TRNUID):
                    this.TRNUID = ReadElementString(reader);
                    break;
                case nameof(this.STATUS):
                    ReadElement(reader, this.STATUS);
                    break;
                case nameof(this.EIVOFFLINEVERIFICATIONSTATUSRS):
                    ReadElement(reader, this.EIVOFFLINEVERIFICATIONSTATUSRS);
                    break;
                default:
                    throw new ArgumentException($"Element {reader.Name} is not handled.");
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElementString(writer, nameof(this.TRNUID), this.TRNUID);
            WriteElement(writer, this.STATUS);
            WriteElement(writer, this.EIVOFFLINEVERIFICATIONSTATUSRS);
        }
    }
}
