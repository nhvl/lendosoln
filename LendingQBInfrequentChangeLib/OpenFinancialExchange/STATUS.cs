﻿namespace OpenFinancialExchange
{
    using System;
    using System.Xml;

    public class STATUS : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return nameof(STATUS); }
        }

        public E_STATUSCODE CODE;

        public string SEVERITY;

        public string MESSAGE;

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case nameof(this.CODE):
                    this.CODE = ReadStatusCode(ReadElementString(reader));
                    break;
                case nameof(this.SEVERITY):
                    this.SEVERITY = ReadElementString(reader);
                    break;
                case nameof(this.MESSAGE):
                    this.MESSAGE = ReadElementString(reader);
                    break;
                default:
                    throw new ArgumentException($"Element {reader.Name} is not handled.");
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElementEnum(writer, nameof(this.CODE), this.CODE);
            WriteElementString(writer, nameof(this.SEVERITY), this.SEVERITY);
            WriteElementString(writer, nameof(this.MESSAGE), this.MESSAGE);
        }

        /// <summary>
        /// Parses out the status code. A success will always be the integer 0, while
        /// an error can be any other integer.
        /// </summary>
        /// <param name="code">The status code.</param>
        /// <returns>The enum representing the status code.</returns>
        private E_STATUSCODE ReadStatusCode(string code)
        {
            if (code == "0")
            {
                return E_STATUSCODE.Success;
            }

            return E_STATUSCODE.Error;
        }
    }
}
