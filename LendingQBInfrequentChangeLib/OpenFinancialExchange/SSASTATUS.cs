﻿namespace OpenFinancialExchange
{
    using System;
    using System.Xml;

    public class SSASTATUS : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return nameof(SSASTATUS); }
        }

        public E_SSASTATUS OFFLINESTATETYPE;

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case nameof(this.OFFLINESTATETYPE):
                    this.OFFLINESTATETYPE = (E_SSASTATUS)Enum.Parse(typeof(E_SSASTATUS), ReadElementString(reader), true);
                    break;
                default:
                    throw new ArgumentException($"Element {reader.Name} is not handled.");
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            this.WriteElementEnum(writer, nameof(this.OFFLINESTATETYPE), this.OFFLINESTATETYPE);
        }
    }
}