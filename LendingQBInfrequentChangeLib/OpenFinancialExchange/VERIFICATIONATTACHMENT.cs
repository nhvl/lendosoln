﻿namespace OpenFinancialExchange
{
    using System;
    using System.Xml;

    public class VERIFICATIONATTACHMENT : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return nameof(VERIFICATIONATTACHMENT); }
        }

        private VERIFICATIONATTACHMENTTYPE verificationattachmenttype;

        public VERIFICATIONATTACHMENTTYPE VERIFICATIONATTACHMENTTYPE
        {
            get
            {
                if (this.verificationattachmenttype == null)
                {
                    this.verificationattachmenttype = new VERIFICATIONATTACHMENTTYPE();
                }

                return this.verificationattachmenttype;
            }

            set
            {
                this.verificationattachmenttype = value;
            }
        }

        private ATTACHEDFILES attachedfiles;

        public ATTACHEDFILES ATTACHEDFILES
        {
            get
            {
                if (this.attachedfiles == null)
                {
                    this.attachedfiles = new ATTACHEDFILES();
                }

                return this.attachedfiles;
            }

            set
            {
                this.attachedfiles = value;
            }
        }

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case nameof(this.VERIFICATIONATTACHMENTTYPE):
                    ReadElement(reader, this.VERIFICATIONATTACHMENTTYPE);
                    break;
                case nameof(this.ATTACHEDFILES):
                    ReadElement(reader, this.ATTACHEDFILES);
                    break;
                default:
                    throw new ArgumentException($"Element {reader.Name} is not handled.");
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElement(writer, this.VERIFICATIONATTACHMENTTYPE);
            WriteElement(writer, this.ATTACHEDFILES);
        }
    }
}
