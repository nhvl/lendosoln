﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace OpenFinancialExchange
{
    public class VERIFICATIONATTACHMENTS : AbstractXmlSerializable
    {
        public VERIFICATIONATTACHMENTS(List<VERIFICATIONATTACHMENT> list)
        {
            this.VERIFICATIONATTACHMENTList = list;
        }

        public List<VERIFICATIONATTACHMENT> VERIFICATIONATTACHMENTList
        {
            get; private set;
        }

        public override string XmlElementName
        {
            get { return nameof(VERIFICATIONATTACHMENTS); }
        }

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case nameof(VERIFICATIONATTACHMENT):
                    var attachment = new VERIFICATIONATTACHMENT();
                    ReadElement(reader, attachment);
                    this.VERIFICATIONATTACHMENTList.Add(attachment);
                    break;
                default:
                    throw new ArgumentException($"Element {reader.Name} is not handled.");
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            foreach (VERIFICATIONATTACHMENT attachment in this.VERIFICATIONATTACHMENTList)
            {
                WriteElement(writer, attachment);
            }
        }
    }
}
