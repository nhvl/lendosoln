﻿namespace OpenFinancialExchange
{
    using System;
    using System.Xml;

    public class ORDERDATA : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return nameof(ORDERDATA); }
        }

        public string FIRSTNAME;

        public string LASTNAME;

        public string SSN;

        public string IMMUTABLEID;

        public string LOANID;

        private IDENTITYCHECKSTATE identitycheckstate;

        public IDENTITYCHECKSTATE IDENTITYCHECKSTATE
        {
            get
            {
                if (this.identitycheckstate == null)
                {
                    this.identitycheckstate = new IDENTITYCHECKSTATE();
                }

                return this.identitycheckstate;
            }

            set
            {
                this.identitycheckstate = value;
            }
        }

        public string SSARETDATE;

        public E_YESNO SSAFORMAUTHORIZED;

        public string SSAFORMREJREASON;

        private IDENTITYREPORT identityreport;

        public IDENTITYREPORT IDENTITYREPORT
        {
            get
            {
                if (this.identityreport == null)
                {
                    this.identityreport = new IDENTITYREPORT();
                }

                return this.identityreport;
            }

            set
            {
                this.identityreport = value;
            }
        }

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case nameof(this.FIRSTNAME):
                    this.FIRSTNAME = ReadElementString(reader);
                    break;
                case nameof(this.LASTNAME):
                    this.LASTNAME = ReadElementString(reader);
                    break;
                case nameof(this.SSN):
                    this.SSN = ReadElementString(reader);
                    break;
                case nameof(this.IMMUTABLEID):
                    this.IMMUTABLEID = ReadElementString(reader);
                    break;
                case nameof(this.LOANID):
                    this.LOANID = ReadElementString(reader);
                    break;
                case nameof(this.IDENTITYCHECKSTATE):
                    ReadElement(reader, this.IDENTITYCHECKSTATE);
                    break;
                case nameof(this.SSARETDATE):
                    this.SSARETDATE = ReadElementString(reader);
                    break;
                case nameof(this.SSAFORMAUTHORIZED):
                    string stringVal = ReadElementString(reader);
                    bool value;
                    if (!bool.TryParse(stringVal, out value))
                    {
                        this.SSAFORMAUTHORIZED = E_YESNO.Undefined;
                    }

                    this.SSAFORMAUTHORIZED = value ? E_YESNO.YES : E_YESNO.NO;
                    break;
                case nameof(this.SSAFORMREJREASON):
                    this.SSAFORMREJREASON = ReadElementString(reader);
                    break;
                case nameof(this.IDENTITYREPORT):
                    ReadElement(reader, this.IDENTITYREPORT);
                    break;
                default:
                    throw new ArgumentException($"Element {reader.Name} is not handled.");
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElementString(writer, nameof(this.FIRSTNAME), this.FIRSTNAME);
            WriteElementString(writer, nameof(this.LASTNAME), this.LASTNAME);
            WriteElementString(writer, nameof(this.SSN), this.SSN);
            WriteElementString(writer, nameof(this.IMMUTABLEID), this.IMMUTABLEID);
            WriteElementString(writer, nameof(this.LOANID), this.LOANID);
            WriteElement(writer, this.IDENTITYCHECKSTATE);
            WriteElementString(writer, nameof(this.SSARETDATE), this.SSARETDATE);
            WriteElementEnum(writer, nameof(this.SSAFORMAUTHORIZED), this.SSAFORMAUTHORIZED);
            WriteElementString(writer, nameof(this.SSAFORMREJREASON), this.SSAFORMREJREASON);
            WriteElement(writer, this.IDENTITYREPORT);
        }
    }
}
