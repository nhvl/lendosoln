﻿namespace OpenFinancialExchange 
{
    using System;
    using System.Xml;
    using System.Collections.Generic;

    public class BORROWERADDRESS : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return nameof(BORROWERADDRESS); }
        }

        public string ADDR1 { get; set; }

        public string CITY { get; set; }

        public string STATE { get; set; }

        public string POSTALCODE { get; set; }

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case nameof(this.ADDR1):
                    this.ADDR1 = ReadElementString(reader);
                    break;
                case nameof(this.CITY):
                    this.CITY = ReadElementString(reader);
                    break;
                case nameof(this.STATE):
                    this.STATE = ReadElementString(reader);
                    break;
                case nameof(this.POSTALCODE):
                    this.POSTALCODE = ReadElementString(reader);
                    break;
                default:
                    throw new ArgumentException($"Element {reader.Name} is not handled.");
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElementString(writer, nameof(this.ADDR1), this.ADDR1);
            WriteElementString(writer, nameof(this.CITY), this.CITY);
            WriteElementString(writer, nameof(this.STATE), this.STATE);
            WriteElementString(writer, nameof(this.POSTALCODE), this.POSTALCODE);
        }
    }
}
