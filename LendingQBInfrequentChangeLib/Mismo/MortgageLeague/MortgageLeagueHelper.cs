using System;

using Mismo.MismoXmlElement;

namespace Mismo.MortgageLeague
{
	/// <summary>
	/// Summary description for MortgageLeagueHelper.
	/// </summary>
	public class MortgageLeagueHelper
	{

        public static XERequestGroup CreateRetrieveRequest(string loginNm, Guid passcode, Guid loanID, string brokerName, string loanOfficerName, string loanOfficerEmail, string loanOfficerPhoneNumber) 
        {
            XERequestGroup requestGroup = new XERequestGroup();

            XERequestingParty requestingParty = CreateRequestingParty(brokerName, loanOfficerName, loanOfficerEmail, loanOfficerPhoneNumber);

            requestGroup.AddRequestingParty(requestingParty);

            XERequest request = requestGroup.CreateRequestHelper("", loginNm, passcode.ToString());

            XERequestData requestData = CreateRetrieveRequestData(loanID);
            request.AddRequestData(requestData);
            return requestGroup;
        }
        public static XERequestGroup CreateSubmitRequest(string loginNm, Guid passcode, string instantViewID, string reportID, string brokerName, string loanOfficerName, string loanOfficerEmail, string loanOfficerPhoneNumber) 
        {
            XERequestGroup requestGroup = new XERequestGroup();

            XERequestingParty requestingParty = CreateRequestingParty(brokerName, loanOfficerName, loanOfficerEmail, loanOfficerPhoneNumber);

            requestGroup.AddRequestingParty(requestingParty);

            XERequest request = requestGroup.CreateRequestHelper("", loginNm, passcode.ToString());

            XERequestData requestData = CreateSubmitRequestData(instantViewID, reportID);
            request.AddRequestData(requestData);
            return requestGroup;
        }
        private static XERequestData CreateRetrieveRequestData(Guid loanID) 
        {
            XERequestData requestData = new XERequestData();

            XEMortgageLeagueRequest mortgageLeagueRequest = new XEMortgageLeagueRequest();
            mortgageLeagueRequest.ActionType = E_XEMortgageLeagueRequestActionType.Retrieve;
            mortgageLeagueRequest.LoanID = loanID;

            requestData.Content = mortgageLeagueRequest;

            return requestData;
        }

        private static XERequestData CreateSubmitRequestData(string instantViewID, string reportID) 
        {
            XERequestData requestData = new XERequestData();

            XEMortgageLeagueRequest mortgageLeagueRequest = new XEMortgageLeagueRequest();
            mortgageLeagueRequest.ActionType = E_XEMortgageLeagueRequestActionType.SubmitNew;
            
            XEMclInstantViewID mclInstantViewID = new XEMclInstantViewID();
            mclInstantViewID.InstantViewPassword = instantViewID;
            mclInstantViewID.ReportID = reportID;

            mortgageLeagueRequest.MclInstantViewID = mclInstantViewID;

            requestData.Content = mortgageLeagueRequest;

            return requestData;
        }
        private static XERequestingParty CreateRequestingParty(string brokerName, string officerName, string email, string phone) 
        {
            XERequestingParty requestingParty = new XERequestingParty();
            requestingParty.Name = brokerName;

            XEContactDetail xeContactDetail = new XEContactDetail();
            xeContactDetail.Name = officerName;
            xeContactDetail.AddWorkEmail(email);
            xeContactDetail.AddWorkPhone(phone);

            requestingParty.AddContactDetail(xeContactDetail);

            return requestingParty;
        }


	}
}
