using System;
using System.Xml;

namespace Mismo.MortgageLeague
{
    public class MortgageLeagueErrorCode 
    {
        public const string InvalidAuthentication = "001";
        public const string MissingData = "002";
        public const string UnexpectedError = "999";
        
    }
	/// <summary>
	/// Summary description for XEMortgageLeagueResponse.
	/// </summary>
	public class XEMortgageLeagueResponse : Mismo.Common.AbstractXmlNode
	{
        #region Schema
        //  <xs:element name="MORTGAGE_LEAGUE_RESPONSE">
        //      <xs:complexType>
        //          <xs:sequence>
        //              <xs:element ref="URL"/>
        //              <xs:element ref="ERROR_MESSAGE"/>
        //          </xs:sequence>
        //          <xs:attribute name="_Status">
        //          <xs:attribute name="_LoanNumber" type="xs:string"/>
        //          <xs:attribute name="_LoanID" type="xs:string"/>
        //      </xs:complexType>
        //  </xs:element>
        #endregion

        #region Private Member Variables
        private string m_url;
        private XEErrorMessage m_errorMessage;
        private E_XEMortgageLeagueResponseStatusType m_status;
        private string m_loanNumber;
        private Guid m_loanID;
        #endregion

        #region Public Properties
        public string Url 
        { 
            get { return m_url; } 
            set { m_url = value; } 
        } 
        public XEErrorMessage ErrorMessage 
        { 
            get { return m_errorMessage; } 
            set { m_errorMessage = value; } 
        } 
        public E_XEMortgageLeagueResponseStatusType Status 
        { 
            get { return m_status; } 
            set { m_status = value; } 
        } 
        public string LoanNumber 
        { 
            get { return m_loanNumber; } 
            set { m_loanNumber = value; } 
        } 
        public Guid LoanID 
        {
            get { return m_loanID; }
            set { m_loanID = value; }
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("MORTGAGE_LEAGUE_RESPONSE");
            WriteAttribute(writer, "_Status", m_status);
            WriteAttribute(writer, "_LoanNumber", m_loanNumber);
            WriteAttribute(writer, "_LoanID", m_loanID.ToString());
            WriteElement(writer, "URL", m_url);

            if (null != m_errorMessage)
                m_errorMessage.GenerateXml(writer);

            writer.WriteEndElement(); //</MORTGAGE_LEAGUE_RESPONSE>
        }

        public override void Parse(XmlElement el) 
        {
            if (null == el)
                return;

            XmlElement o = (XmlElement) el.SelectSingleNode("URL");
            if (null != o)
                m_url = o.InnerText;
            else
                m_url = null;

            m_errorMessage = null;
            o = (XmlElement) el.SelectSingleNode("ERROR_MESSAGE");
            if (null != o) 
            {
                m_errorMessage = new XEErrorMessage();
                m_errorMessage.Parse(o);
            }

            switch (el.GetAttribute("_Status")) 
            {
                case "Error":
                    m_status = E_XEMortgageLeagueResponseStatusType.Error;
                    break;
                case "OK":
                default:
                    m_status = E_XEMortgageLeagueResponseStatusType.OK;
                    break;
            }

            m_loanNumber = el.GetAttribute("_LoanNumber");
            m_loanID = Guid.Empty;
            try 
            {
                m_loanID = new Guid(el.GetAttribute("_LoanID"));
            } 
            catch {}


        }
        #endregion

        public void SetErrorMessage(string code, string errorMessage) 
        {
            m_status = E_XEMortgageLeagueResponseStatusType.Error;
            m_errorMessage = new XEErrorMessage();
            m_errorMessage.Code = code;
            m_errorMessage.Message = errorMessage;
        }
        public string GetErrorCode() 
        {
            if (m_errorMessage != null)
                return m_errorMessage.Code;
            else
                return "";
        }
        public string GetErrorMessage() 
        {
            if (m_errorMessage != null)
                return m_errorMessage.Message;
            else
                return "";
        }
	}
    public enum E_XEMortgageLeagueResponseStatusType 
    {
        OK,
        Error
    }
}
