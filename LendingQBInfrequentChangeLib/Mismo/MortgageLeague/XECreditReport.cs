using System;
using System.Xml;

namespace Mismo.MortgageLeague
{
	public class XECreditReport : Mismo.Common.AbstractXmlNode
	{
        #region Schema
        //  <xs:element name="CREDIT_REPORT">
        //      <xs:complexType>
        //          <xs:sequence>
        //              <xs:element ref="CREDIT_REPORT_DOCUMENT"/>
        //          </xs:sequence>
        //          <xs:attribute name="_Type">
        //      </xs:complexType>
        //  </xs:element>
        #endregion

        #region Private Member Variables
        private string m_creditReportDocument;
        #endregion

        #region Public Properties
        public string CreditReportDocument 
        {
            get { return m_creditReportDocument; }
            set { m_creditReportDocument = value; }
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("CREDIT_REPORT");
            writer.WriteStartElement("CREDIT_REPORT_DOCUMENT");
            writer.WriteCData(m_creditReportDocument);
            writer.WriteEndElement(); // </CREDIT_REPORT_DOCUMENT>
            writer.WriteEndElement(); // </CREDIT_REPORT>
        }

        public override void Parse(XmlElement el) 
        {
            if (null == el)
                return;

            XmlElement o = (XmlElement) el.SelectSingleNode("CREDIT_REPORT_DOCUMENT");
            if (null != o)
                m_creditReportDocument = o.InnerText;
            else 
                m_creditReportDocument = null;
        }
        #endregion
	}
}
