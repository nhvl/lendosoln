using System;
using System.Xml;

namespace Mismo.MortgageLeague
{
	/// <summary>
	/// Summary description for ERROR_MESSAGE.
	/// </summary>
	public class XEErrorMessage : Mismo.Common.AbstractXmlNode
	{
        #region Schema
        //  <xs:element name="ERROR_MESSAGE">
        //      <xs:complexType>
        //          <xs:attribute name="_Code" type="xs:string"/>
        //          <xs:attribute name="_Message" type="xs:string"/>
        //      </xs:complexType>
        //  </xs:element>
        #endregion

        #region Private Member Variables
        private string m_code;
        private string m_message;
        #endregion
        #region Public Properties
        public string Code 
        {
            get { return m_code; }
            set { m_code = value; }
        }
        public string Message 
        {
            get { return m_message; }
            set { m_message = value; }
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("ERROR_MESSAGE");
            WriteAttribute(writer, "_Code", m_code);
            WriteAttribute(writer, "_Message", m_message);
            writer.WriteEndElement(); // </ERROR_MESSAGE>
        }
        public override void Parse(XmlElement el) 
        {
            if (null == el)
                return;

            m_code = el.GetAttribute("_Code");
            m_message = el.GetAttribute("_Message");
        }
        #endregion
	}
}
