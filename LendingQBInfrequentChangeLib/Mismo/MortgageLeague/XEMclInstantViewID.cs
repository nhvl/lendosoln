using System;
using System.Xml;

namespace Mismo.MortgageLeague
{

	public class XEMclInstantViewID : Mismo.Common.AbstractXmlNode
	{
        #region Schema
        //  <xs:element name="MCL_INSTANT_VIEW_ID">
        //      <xs:complexType>
        //          <xs:attribute name="_InstantViewPassword" type="xs:string"/>
        //          <xs:attribute name="_ReportID" type="xs:string"/>
        //      </xs:complexType>
        //  </xs:element>
        #endregion

        #region Private Member Variables
        private string m_instantViewPassword;
        private string m_reportID;
        #endregion

        #region Public Properties
        public string InstantViewPassword 
        { 
            get { return m_instantViewPassword; } 
            set { m_instantViewPassword = value; } 
        } 
        public string ReportID 
        { 
            get { return m_reportID; } 
            set { m_reportID = value; } 
        } 


        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("MCL_INSTANT_VIEW_ID");
            WriteAttribute(writer, "_InstantViewPassword", m_instantViewPassword);
            WriteAttribute(writer, "_ReportID", m_reportID);
            writer.WriteEndElement(); //</MCL_INSTANT_VIEW_ID>
        }

        public override void Parse(XmlElement el) 
        {
            if (null == el)
                return;

            m_instantViewPassword = el.GetAttribute("_InstantViewPassword");
            m_reportID = el.GetAttribute("_ReportID");
        }
        #endregion
	}
}
